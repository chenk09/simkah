<!--
DIPINDAHKAN KE DALAM TABEL
<div class='control-group'>
    <?php echo CHtml::label('Rekening Debit','rekening debit',array('class'=>'control-label')) ?>
    <div class="controls">
        <?php
            $this->widget('MyJuiAutoComplete',
                array(
                    'name' => 'rekDebit',
                    'id' => 'rekDebit',
                    'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>'Kredit')),
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ){
                            return false;
                        }',
                        'select' => 'js:function( event, ui ){
                            $(this).val(ui.item.value);
                            var data = {
                                rincianobyek_id:ui.item.rincianobyek_id,
                                obyek_id:ui.item.obyek_id,
                                jenis_id:ui.item.jenis_id,
                                kelompok_id:ui.item.kelompok_id,
                                struktur_id:ui.item.struktur_id,
                                status:"debit"
                            };
                            getDataRekeningFromGrid(data);                            
                            return false;
                        }'
                    ),
                    'htmlOptions' => array(
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                        'placeholder'=>'Ketikan Nama Rekening',
                        'class'=>'span3',
                        'style'=>'width:150px;',
                    ),
                    'tombolDialog' => array(
                        'idDialog' => 'dialogRekDebit'
                    ),
                )
            );
        ?>
    </div>
</div>
<div class='control-group'>
    <?php echo CHtml::label('Rekening Kredit','rekening kredit',array('class'=>'control-label')) ?>
    <div class="controls">
        <?php
            $this->widget('MyJuiAutoComplete',
                array(
                    'name' => 'rekKredit',
                    'id' => 'rekKredit',
                    'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>'Kredit')),
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ){
                            return false;
                        }',
                        'select' => 'js:function( event, ui ){
                            $(this).val(ui.item.value);
                            var data = {
                                rincianobyek_id:ui.item.rincianobyek_id,
                                obyek_id:ui.item.obyek_id,
                                jenis_id:ui.item.jenis_id,
                                kelompok_id:ui.item.kelompok_id,
                                struktur_id:ui.item.struktur_id,
                                status:"kredit"
                            };
                            getDataRekeningFromGrid(data);                            
                            return false;
                        }'
                    ),
                    'htmlOptions' => array(
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                        'placeholder'=>'Ketikan Nama Rekening',
                        'class'=>'span3',
                        'style'=>'width:150px;',
                    ),
                    'tombolDialog' => array(
                        'idDialog' => 'dialogRekKredit'
                    ),
                )
            );
        ?>
    </div>
</div>-->
<table id="tblInputRekening" class="table table-bordered table-condensed" style="width:630px; overflow-x: scroll;">
    <thead>
        <tr>
            <!--<th width="10">No.</th>-->
            <th colspan="5" width="50">Kode Rekening</th>
            <th>Nama Rekening</th>
            <th width="50">Debit</th>
            <th width="50">Kredit</th>
            <th width="10">Batal</th>
        </tr>
    </thead>
    <tbody>
        <?php $this->renderPartial('billingKasir.views.daftarFakturPembelian._rowUraian',array('form'=>$form, 'modUraian'=>$modUraian)); ?>
    </tbody>
</table>

<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRekKredit',
    'options'=>array(
        'title'=>'Daftar Rekening Kredit',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>400,
        'resizable'=>false,
    ),
));
$modRekKredit = new RekeningakuntansiV('search');
$modRekKredit->unsetAttributes();
$account = "K";
if(isset($_GET['RekeningakuntansiV'])) {
    $modRekKredit->attributes = $_GET['RekeningakuntansiV'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'rekkredit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRekKredit->searchAccounts($account),
	'filter'=>$modRekKredit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kode Rekening</center>',
                'start'=>1, //indeks kolom 3
                'end'=>5, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Rek. 1',
                    'value'=>'$data->kdstruktur',
                ),
                array(
                    'header'=>'Rek. 2',
                    'value'=>'$data->kdkelompok',
                ),
                array(
                    'header'=>'Rek. 3',
                    'value'=>'$data->kdjenis',
                ),
                array(
                    'header'=>'Rek. 4',
                    'value'=>'$data->kdobyek',
                ),
                array(
                    'header'=>'Rek. 5',
                    'value'=>'$data->kdrincianobyek',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'value'=>'$data->nmrincianobyek',
                ),
                array(
                    'header'=>'Nama Lain',
                    'value'=>'$data->nmrincianobyeklain',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'value'=>'$data->rincianobyek_nb',
                ),
            
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                        var data = {
                                            rincianobyek_id:$data->rincianobyek_id,
                                            obyek_id:$data->obyek_id,
                                            jenis_id:$data->jenis_id,
                                            kelompok_id:$data->kelompok_id,
                                            struktur_id:$data->struktur_id,
                                            status:\"kredit\"
                                        };
                                        getDataRekeningFromGrid(data);
                                        $(\"#dialogRekKredit\").dialog(\"close\");    
                                        return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>

<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRekDebit',
    'options'=>array(
        'title'=>'Daftar Rekening Debit',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modRekKredit = new RekeningakuntansiV('search');
$modRekKredit->unsetAttributes();
$account = "D";
if(isset($_GET['RekeningakuntansiV'])) {
    $modRekKredit->attributes = $_GET['RekeningakuntansiV'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'rekdedit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRekKredit->searchAccounts($account),
	'filter'=>$modRekKredit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kode Rekening</center>',
                'start'=>1, //indeks kolom 3
                'end'=>5, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Rek. 1',
                    'value'=>'$data->kdstruktur',
                ),
                array(
                    'header'=>'Rek. 2',
                    'value'=>'$data->kdkelompok',
                ),
                array(
                    'header'=>'Rek. 3',
                    'value'=>'$data->kdjenis',
                ),
                array(
                    'header'=>'Rek. 4',
                    'value'=>'$data->kdobyek',
                ),
                array(
                    'header'=>'Rek. 5',
                    'value'=>'$data->kdrincianobyek',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'value'=>'$data->nmrincianobyek',
                ),
                array(
                    'header'=>'Nama Lain',
                    'value'=>'$data->nmrincianobyeklain',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'value'=>'$data->rincianobyek_nb',
                ),
            
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                        var data = {
                                            rincianobyek_id:$data->rincianobyek_id,
                                            obyek_id:$data->obyek_id,
                                            jenis_id:$data->jenis_id,
                                            kelompok_id:$data->kelompok_id,
                                            struktur_id:$data->struktur_id,
                                            status:\"debit\"
                                        };
                                        getDataRekeningFromGrid(data);
                                        $(\"#dialogRekDebit\").dialog(\"close\");    
                                        return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>
<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRekDebitKredit',
    'options'=>array(
        'title'=>'Daftar Rekening Debit dan Kredit',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>400,
        'resizable'=>false,
    ),
));
echo CHtml::hiddenField('row',0,array('readonly'=>true)); //untuk mencatat asal baris di klik
$modRekKredit = new RekeningakuntansiV('searchAccounts');
$modRekKredit->unsetAttributes();
if(isset($_GET['RekeningakuntansiV'])) {
    $modRekKredit->attributes = $_GET['RekeningakuntansiV'];
//    $modRekKredit->rincianobyek_nb = $_GET['RekeningakuntansiV']['rincianobyek_nb'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'rekkreditdebit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRekKredit->searchAccounts(),
	'filter'=>$modRekKredit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//        JIKA INI DI AKTIFKAN MAKA FILTER AKAN HILANG
//        'mergeHeaders'=>array(
//            array(
//                'name'=>'<center>Kode Rekening</center>',
//                'start'=>1, //indeks kolom 3
//                'end'=>5, //indeks kolom 4
//            ),
//        ),
	'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Rek. 1',
                    'name'=>'kdstruktur',
                    'value'=>'$data->kdstruktur',
                ),
                array(
                    'header'=>'Rek. 2',
                    'name'=>'kdkelompok',
                    'value'=>'$data->kdkelompok',
                ),
                array(
                    'header'=>'Rek. 3',
                    'name'=>'kdjenis',
                    'value'=>'$data->kdjenis',
                ),
                array(
                    'header'=>'Rek. 4',
                    'name'=>'kdobyek',
                    'value'=>'$data->kdobyek',
                ),
                array(
                    'header'=>'Rek. 5',
                    'name'=>'kdrincianobyek',
                    'value'=>'$data->kdrincianobyek',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'name'=>'nmrincianobyek',
                    'value'=>'$data->nmrincianobyek',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'nmrincianobyeklain',
                    'value'=>'$data->nmrincianobyeklain',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'name'=>'rincianobyek_nb',
                    'value'=>'$data->rincianobyek_nb',
                    'filter'=>  CHtml::dropDownList('RekeningakuntansiV[rincianobyek_nb]', $modRekKredit->rincianobyek_nb, array('D'=>'Debit', 'K'=>'Kredit'), array('empty'=>'-- Pilih --', 'style'=>'width:64px;'))
                ),
            
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebitKredit",
                                    "onClick" =>"
                                        var data = {
                                            rincianobyek_id:$data->rincianobyek_id,
                                            obyek_id:$data->obyek_id,
                                            jenis_id:$data->jenis_id,
                                            kelompok_id:$data->kelompok_id,
                                            struktur_id:$data->struktur_id,
                                            nmrincianobyek:\"$data->nmrincianobyek\",
                                            kdstruktur:\"$data->kdstruktur\",
                                            kdkelompok:\"$data->kdkelompok\",
                                            kdjenis:\"$data->kdjenis\",
                                            kdobyek:\"$data->kdobyek\",
                                            kdrincianobyek:\"$data->kdrincianobyek\",
                                            saldodebit:\"$data->saldodebit\",
                                            saldokredit:\"$data->saldokredit\",
                                            status:\"debit\"
                                        };
                                        var row = $(\"#dialogRekDebitKredit #row\").val();
                                        editDataRekeningFromGrid(data, row);
                                        $(\"#dialogRekDebitKredit\").dialog(\"close\");    
                                        return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>
<?php 
    $modNewRekenings = array();
    $modNewRekenings[0] = new RekeningakuntansiV;
?>
<script type="text/javascript">
    var trRekening=new String(<?php echo CJSON::encode($this->renderPartial('billingKasir.views.returTagihanPasien.rekening.__formKodeRekening',array('model'=>$modNewRekenings,'key'=>99,'removeButton'=>true),true));?>);
    function setDialogRekening(obj){
        var row = $(obj).parents('tr').find('#row').val();
        $('#dialogRekDebitKredit #row').val(row);
        $('#dialogRekDebitKredit').dialog('open');
    }
    function getDataRekeningFromGrid(params, row)
    {
        $.post('<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/AmbilDataRekening');?>', {id_rekening:params},
            function(data){
                $("#tblInputRekening > tbody").append(data.replace());
                alert("test");
                $("#tblInputRekening > tbody > tr").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                        $(this).val("");
                                                                                        return false;
                                                                                    },'select':function( event, ui ) {
                                                                                        $(this).val(ui.item.value);
                                                                                        var data = {
                                                                                            rincianobyek_id:ui.item.rincianobyek_id,
                                                                                            obyek_id:ui.item.obyek_id,
                                                                                            jenis_id:ui.item.jenis_id,
                                                                                            kelompok_id:ui.item.kelompok_id,
                                                                                            struktur_id:ui.item.struktur_id,
                                                                                            nmrincianobyek:ui.item.nmrincianobyek,
                                                                                            kdstruktur:ui.item.kdstruktur,
                                                                                            kdkelompok:ui.item.kdkelompok,
                                                                                            kdjenis:ui.item.kdjenis,
                                                                                            kdobyek:ui.item.kdobyek,
                                                                                            kdrincianobyek:ui.item.kdrincianobyek,
                                                                                            saldodebit:ui.item.saldodebit,
                                                                                            saldokredit:ui.item.saldokredit,
                                                                                            status:"debit"
                                                                                        };
                                                                                        var row = $(this).parents("tr").find("#row").val();
                                                                                        editDataRekeningFromGrid(data, row);
                                                                                        return false;
                                                                                    },'source':function(request, response) {
                                                                                                    $.ajax({
                                                                                                        url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                                                                                        dataType: "json",
                                                                                                        data: {
                                                                                                            term: request.term,
                                                                                                        },
                                                                                                        success: function (data) {
                                                                                                            response(data);
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                    });  
                $("#tblInputRekening > tbody > tr").find('.currency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
                setTimeout(function(){renameRowRekening()},1000);
        }, "json");
    }
    function editDataRekeningFromGrid(params, row){
        $("#RekeningakuntansiV_"+row+"_struktur_id").val(params.struktur_id);
        $("#RekeningakuntansiV_"+row+"_kelompok_id").val(params.kelompok_id);
        $("#RekeningakuntansiV_"+row+"_jenis_id").val(params.jenis_id);
        $("#RekeningakuntansiV_"+row+"_obyek_id").val(params.obyek_id);
        $("#RekeningakuntansiV_"+row+"_rincianobyek_id").val(params.rincianobyek_id);
        
        $("#RekeningakuntansiV_"+row+"_kdstruktur").val(params.kdstruktur);
        $("#RekeningakuntansiV_"+row+"_kdkelompok").val(params.kdkelompok);
        $("#RekeningakuntansiV_"+row+"_kdjenis").val(params.kdjenis);
        $("#RekeningakuntansiV_"+row+"_kdobyek").val(params.kdobyek);
        $("#RekeningakuntansiV_"+row+"_kdrincianobyek").val(params.kdrincianobyek);
        $("#RekeningakuntansiV_"+row+"_rekDebitKredit").val(params.nmrincianobyek);
        $("#RekeningakuntansiV_"+row+"_nama_rekening").val(params.nmrincianobyek);
        
        $("#RekeningakuntansiV_"+row+"_saldodebit").val(formatNumber(parseFloat(params.saldodebit)));
        $("#RekeningakuntansiV_"+row+"_saldokredit").val(formatNumber(parseFloat(params.saldokredit)));
    }
    function addDataRekening(obj){
        $("#tblInputRekening > tbody").append(trRekening.replace());
        $("#tblInputRekening > tbody > tr:last").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                $(this).val("");
                                                                                return false;
                                                                            },'select':function( event, ui ) {
                                                                                $(this).val(ui.item.value);
                                                                                var data = {
                                                                                    rincianobyek_id:ui.item.rincianobyek_id,
                                                                                    obyek_id:ui.item.obyek_id,
                                                                                    jenis_id:ui.item.jenis_id,
                                                                                    kelompok_id:ui.item.kelompok_id,
                                                                                    struktur_id:ui.item.struktur_id,
                                                                                    nmrincianobyek:ui.item.nmrincianobyek,
                                                                                    kdstruktur:ui.item.kdstruktur,
                                                                                    kdkelompok:ui.item.kdkelompok,
                                                                                    kdjenis:ui.item.kdjenis,
                                                                                    kdobyek:ui.item.kdobyek,
                                                                                    kdrincianobyek:ui.item.kdrincianobyek,
                                                                                    saldodebit:ui.item.saldodebit,
                                                                                    saldokredit:ui.item.saldokredit,
                                                                                    status:"debit"
                                                                                };
                                                                                var row = $(this).parents("tr").find("#row").val();
                                                                                editDataRekeningFromGrid(data, row);
                                                                                return false;
                                                                            },'source':function(request, response) {
                                                                                            $.ajax({
                                                                                                url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                                                                                dataType: "json",
                                                                                                data: {
                                                                                                    term: request.term,
                                                                                                },
                                                                                                success: function (data) {
                                                                                                    response(data);
                                                                                                }
                                                                                            })
                                                                                        }
                                                                            });  
        $("#tblInputRekening > tbody > tr:last").find('.currency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
        setTimeout(function(){renameRowRekening()},1000);
    }
    function getDataRekening(params){
        $("#tblInputRekening > tbody").find('tr').detach();
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/GetDataRekeningByJnsPenerimaan');?>', {jenispenerimaan_id:params},
            function(data){
                $("#tblInputRekening > tbody").append(data.replace());
                $("#tblInputRekening > tbody").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                        $(this).val("");
                                                                                        return false;
                                                                                    },'select':function( event, ui ) {
                                                                                        $(this).val(ui.item.value);
                                                                                        var data = {
                                                                                            rincianobyek_id:ui.item.rincianobyek_id,
                                                                                            obyek_id:ui.item.obyek_id,
                                                                                            jenis_id:ui.item.jenis_id,
                                                                                            kelompok_id:ui.item.kelompok_id,
                                                                                            struktur_id:ui.item.struktur_id,
                                                                                            nmrincianobyek:ui.item.nmrincianobyek,
                                                                                            kdstruktur:ui.item.kdstruktur,
                                                                                            kdkelompok:ui.item.kdkelompok,
                                                                                            kdjenis:ui.item.kdjenis,
                                                                                            kdobyek:ui.item.kdobyek,
                                                                                            kdrincianobyek:ui.item.kdrincianobyek,
                                                                                            saldodebit:ui.item.saldodebit,
                                                                                            saldokredit:ui.item.saldokredit,
                                                                                            status:"debit"
                                                                                        };
                                                                                        var row = $(this).parents("tr").find("#row").val();
                                                                                        editDataRekeningFromGrid(data, row);
                                                                                        return false;
                                                                                    },'source':function(request, response) {
                                                                                                    $.ajax({
                                                                                                        url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                                                                                        dataType: "json",
                                                                                                        data: {
                                                                                                            term: request.term,
                                                                                                        },
                                                                                                        success: function (data) {
                                                                                                            response(data);
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                    });  
                $("#tblInputRekening > tbody").find('.currency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
                setTimeout(function(){renameRowRekening()},1000);
        }, "json");    
    }

    function getDataRekeningPengeluaran(params){
        $("#tblInputRekening > tbody").find('tr').detach();
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/GetDataRekeningByJnsPengeluaran');?>', {jenispengeluaran_id:params},
            function(data){
                $("#tblInputRekening > tbody").append(data.replace());
                $("#tblInputRekening > tbody").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                        $(this).val("");
                                                                                        return false;
                                                                                    },'select':function( event, ui ) {
                                                                                        $(this).val(ui.item.value);
                                                                                        var data = {
                                                                                            rincianobyek_id:ui.item.rincianobyek_id,
                                                                                            obyek_id:ui.item.obyek_id,
                                                                                            jenis_id:ui.item.jenis_id,
                                                                                            kelompok_id:ui.item.kelompok_id,
                                                                                            struktur_id:ui.item.struktur_id,
                                                                                            nmrincianobyek:ui.item.nmrincianobyek,
                                                                                            kdstruktur:ui.item.kdstruktur,
                                                                                            kdkelompok:ui.item.kdkelompok,
                                                                                            kdjenis:ui.item.kdjenis,
                                                                                            kdobyek:ui.item.kdobyek,
                                                                                            kdrincianobyek:ui.item.kdrincianobyek,
                                                                                            saldodebit:ui.item.saldodebit,
                                                                                            saldokredit:ui.item.saldokredit,
                                                                                            status:"debit"
                                                                                        };
                                                                                        var row = $(this).parents("tr").find("#row").val();
                                                                                        editDataRekeningFromGrid(data, row);
                                                                                        return false;
                                                                                    },'source':function(request, response) {
                                                                                                    $.ajax({
                                                                                                        url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                                                                                        dataType: "json",
                                                                                                        data: {
                                                                                                            term: request.term,
                                                                                                        },
                                                                                                        success: function (data) {
                                                                                                            response(data);
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                    });  
                $("#tblInputRekening > tbody").find('.currency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
                setTimeout(function(){renameRowRekening()},1000);
        }, "json");    
    }

     function getDataRekeningPengeluaranGaji(params){
        $("#tblInputRekening > tbody").find('tr').detach();
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/GetDataRekeningByJnsPengeluaranGaji');?>', {jenispengeluaran_id:params},
            function(data){
                $("#tblInputRekening > tbody").append(data.replace());
                $("#tblInputRekening > tbody").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                        $(this).val("");
                                                                                        return false;
                                                                                    },'select':function( event, ui ) {
                                                                                        $(this).val(ui.item.value);
                                                                                        var data = {
                                                                                            rincianobyek_id:ui.item.rincianobyek_id,
                                                                                            obyek_id:ui.item.obyek_id,
                                                                                            jenis_id:ui.item.jenis_id,
                                                                                            kelompok_id:ui.item.kelompok_id,
                                                                                            struktur_id:ui.item.struktur_id,
                                                                                            nmrincianobyek:ui.item.nmrincianobyek,
                                                                                            kdstruktur:ui.item.kdstruktur,
                                                                                            kdkelompok:ui.item.kdkelompok,
                                                                                            kdjenis:ui.item.kdjenis,
                                                                                            kdobyek:ui.item.kdobyek,
                                                                                            kdrincianobyek:ui.item.kdrincianobyek,
                                                                                            saldodebit:ui.item.saldodebit,
                                                                                            saldokredit:ui.item.saldokredit,
                                                                                            status:"debit"
                                                                                        };
                                                                                        var row = $(this).parents("tr").find("#row").val();
                                                                                        editDataRekeningFromGrid(data, row);
                                                                                        return false;
                                                                                    },'source':function(request, response) {
                                                                                                    $.ajax({
                                                                                                        url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                                                                                        dataType: "json",
                                                                                                        data: {
                                                                                                            term: request.term,
                                                                                                        },
                                                                                                        success: function (data) {
                                                                                                            response(data);
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                    });  
                $("#tblInputRekening > tbody").find('.currency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
                setTimeout(function(){renameRowRekening()},1000);
        }, "json");    
    }
    
    function renameRowRekening(){
        var i = 0;
        var namaModel = "RekeningakuntansiV";
        $("#tblInputRekening > tbody").find('tr').each(
            function()
            {
                $(this).find('#noUrut').val(i+1);
                $(this).find('#row').val(i);
                $(this).find('.currency').each(function(){this.value = formatNumber(this.value)});

                $(this).find('input[name$="[kdstruktur]"]').attr('name',namaModel+'['+i+'][kdstruktur]')
                        .attr('id',namaModel+'_'+i+'_kdstruktur');
                $(this).find('input[name$="[struktur_id]"]').attr('name',namaModel+'['+i+'][struktur_id]')
                        .attr('id',namaModel+'_'+i+'_struktur_id');
                $(this).find('input[name$="[kdkelompok]"]').attr('name',namaModel+'['+i+'][kdkelompok]')
                        .attr('id',namaModel+'_'+i+'_kdkelompok');
                $(this).find('input[name$="[kelompok_id]"]').attr('name',namaModel+'['+i+'][kelompok_id]')
                        .attr('id',namaModel+'_'+i+'_kelompok_id');
                $(this).find('input[name$="[kdjenis]"]').attr('name',namaModel+'['+i+'][kdjenis]')
                        .attr('id',namaModel+'_'+i+'_kdjenis');
                $(this).find('input[name$="[jenis_id]"]').attr('name',namaModel+'['+i+'][jenis_id]')
                        .attr('id',namaModel+'_'+i+'_jenis_id');
                $(this).find('input[name$="[kdobyek]"]').attr('name',namaModel+'['+i+'][kdobyek]')
                        .attr('id',namaModel+'_'+i+'_kdobyek');
                $(this).find('input[name$="[obyek_id]"]').attr('name',namaModel+'['+i+'][obyek_id]')
                        .attr('id',namaModel+'_'+i+'_obyek_id');
                $(this).find('input[name$="[kdrincianobyek]"]').attr('name',namaModel+'['+i+'][kdrincianobyek]')
                        .attr('id',namaModel+'_'+i+'_kdrincianobyek');
                $(this).find('input[name$="[rincianobyek_id]"]').attr('name',namaModel+'['+i+'][rincianobyek_id]')
                        .attr('id',namaModel+'_'+i+'_rincianobyek_id');
                $(this).find('input[name$="[nama_rekening]"]').attr('name',namaModel+'['+i+'][nama_rekening]')
                        .attr('id',namaModel+'_'+i+'_nama_rekening');
                $(this).find('input[name$="[rekDebitKredit]"]').attr('name',namaModel+'['+i+'][rekDebitKredit]')
                        .attr('id',namaModel+'_'+i+'_rekDebitKredit');
                $(this).find('input[name$="[saldodebit]"]').attr('name',namaModel+'['+i+'][saldodebit]')
                        .attr('id',namaModel+'_'+i+'_saldodebit');
                $(this).find('input[name$="[saldokredit]"]').attr('name',namaModel+'['+i+'][saldokredit]')
                        .attr('id',namaModel+'_'+i+'_saldokredit');
                                
                i++;

            }
        );
    }

    function removeDataRekening(obj){
        $(obj).parent().parent('tr').detach();
        setTimeout(function(){renameRowRekening()},1000);
    }

    function getDataRekeningReturPembelian(){
        $("#tblInputRekening > tbody").find('tr').detach();
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/GetDataRekeningReturPembelian');?>', {},
            function(data){
                $("#tblInputRekening > tbody").append(data.replace());
                $("#tblInputRekening > tbody").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                $(this).val("");
                return false;
            },'select':function( event, ui ) {
                $(this).val(ui.item.value);
                var data = {
                    rincianobyek_id:ui.item.rincianobyek_id,
                    obyek_id:ui.item.obyek_id,
                    jenis_id:ui.item.jenis_id,
                    kelompok_id:ui.item.kelompok_id,
                    struktur_id:ui.item.struktur_id,
                    nmrincianobyek:ui.item.nmrincianobyek,
                    kdstruktur:ui.item.kdstruktur,
                    kdkelompok:ui.item.kdkelompok,
                    kdjenis:ui.item.kdjenis,
                    kdobyek:ui.item.kdobyek,
                    kdrincianobyek:ui.item.kdrincianobyek,
                    saldodebit:ui.item.saldodebit,
                    saldokredit:ui.item.saldokredit,
                    status:"debit"
                };
                var row = $(this).parents("tr").find("#row").val();
                editDataRekeningFromGrid(data, row);
                return false;
            },'source':function(request, response) {
                            $.ajax({
                                url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                dataType: "json",
                                data: {
                                    term: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                            })
                        }
            });  
            $("#tblInputRekening > tbody").find('.currency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
            setTimeout(function(){renameRowRekening()},1000);
        }, "json");    
    }
</script>