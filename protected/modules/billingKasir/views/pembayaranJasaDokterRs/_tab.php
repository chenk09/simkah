<?php 
$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'Jasa Medis', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'btn-jasamedis','class'=>'active','onclick'=>'ubahTab("jasamedis");')),
        array('label'=>'Jasa Rujukan', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'btn-jasarujuk','class'=>'active','onclick'=>'ubahTab("jasarujuk");')),
        array('label'=>'Jasa Anastesi', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'btn-jasaanastesi','class'=>'active','onclick'=>'ubahTab("jasaanastesi");')),
        array('label'=>'Jasa Operator Bedah', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'btn-jasaoperatorbedah','class'=>'active','onclick'=>'ubahTab("jasaoperatorbedah");')),
        //array('label'=>'Jasa Paramedik', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'btn-jasaparamedik','class'=>'active','onclick'=>'ubahTab("jasaparamedik");')),
        array('label'=>'Jasa Sarana', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'btn-jasasarana','class'=>'active','onclick'=>'ubahTab("jasasarana");')),
        //array('label'=>'Jasa Remun', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'btn-jasaremun','class'=>'active','onclick'=>'ubahTab("jasaremun");')),
    ),
));
?>