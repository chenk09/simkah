<script>
function ubahTab(nama_tab){ //nama_tab = "jasamedis" | "jasarujuk" | "jasaanastesi" (opsi)
    $("#tab").val(nama_tab);
    $(".active").removeClass('active');
    $("#btn-"+nama_tab).addClass('active');
    if(nama_tab == "jasarujuk"){
        $("#formTglPenunjang").show();
        $("#formTglPendaftaran").find('input').each(function(){
            $(this).val("");
        });
		$('.rujuk').hide();
        $("#formTglPendaftaran").hide();
    }else{
        $("#formTglPendaftaran").show();
        $("#formTglPenunjang").find('input').each(function(){
            $(this).val("");
        });
		$('.rujuk').show();
        $("#formTglPenunjang").hide();
    }
    bersihTabelDetail();
    bersihFormPembayaran();
}
<?php echo isset($_GET['id']) ? '' : 'ubahTab("jasamedis"); //default'; ?>

function addDetail(){
    var pegawai_id = $('#BKPembayaranjasaT_pegawai_id').val();
    var tglAwalPenunjang = $('#BKPembayaranjasaT_tglAwalPenunjang').val();
    var tglAkhirPenunjang = $('#BKPembayaranjasaT_tglAkhirPenunjang').val();
    var tglAwalPendaftaran = $('#BKPembayaranjasaT_tglAwalPendaftaran').val();
    var tglAkhirPendaftaran = $('#BKPembayaranjasaT_tglAkhirPendaftaran').val();
    var tab = $('#tab').val();
    var urlPost = {};
    urlPost["jasamedis"] = "<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasaMedisRs'); ?>";
    urlPost["jasarujuk"] = "<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasaRujukRs'); ?>";
    urlPost["jasaanastesi"] = "<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasaAnastesiRs'); ?>";
    urlPost["jasaoperatorbedah"] = "<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasaOperatorRs'); ?>";
    urlPost["jasaparamedik"] = "<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasaParamedisRs'); ?>";
    urlPost["jasasarana"] = "<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasaSaranaRs'); ?>";
    urlPost["jasaremun"] = "<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasaRemunRs'); ?>";
    if(tglAwalPenunjang.length > 0 && tglAkhirPenunjang.length > 0 && pegawai_id.length > 0){
        var tglAwal = tglAwalPenunjang;
        var tglAkhir = tglAkhirPenunjang;
    }else if(tglAwalPendaftaran.length > 0 && tglAkhirPendaftaran.length > 0 && pegawai_id.length > 0){
        var tglAwal = tglAwalPendaftaran;
        var tglAkhir = tglAkhirPendaftaran;
    }else{
        alert ("Silahkan isi form dengan benar ! Dokter dan Tanggal wajib diisi !");
        return false;
    }
    bersihTabelDetail();
    bersihFormPembayaran();
    $('#tabelDetail').addClass('srbacLoading');
    $.post(urlPost[tab], {
        pegawai_id: pegawai_id, 
        tgl_awal:tglAwal, 
        tgl_akhir:tglAkhir},
        function(data){
            if (data.tr == ""){
                alert('Data tidak ditemukan !');
                $('#tabelDetail').removeClass('srbacLoading');
                return false;
            }else{
                $('#tabelDetail tbody').append(data.tr);
                $("#tabelDetail tbody tr .currency").each(function(){
                    $(this).maskMoney({"defaultZero":true,"allowZero":true,"decimal":"","thousands":",","precision":0,"symbol":null});
                });    
                formatNumbers();
                hitungSemua();
                $('#tabelDetail').removeClass('srbacLoading');
            }
        }, "json");
    return false;
}
function formatNumbers(){
    $('.currency').each(function(){this.value = formatNumber(this.value)});
}
formatNumbers();
function unformatNumbers(){
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
}
function bersihTabelDetail(){
    $('#tabelDetail tbody').html("");
}
function bersihFormPembayaran(){
    $('#formPembayaran .currency').each(function(){
        $(this).val(0);
    });
}
function hitungSemua(){
    unformatNumbers();
    var totTarif = 0;
    var totJasa = 0;
    var totBayar = 0;
    var totSisa = 0;
    $('#tabelDetail tbody tr').each(function(){
        if($(this).find('input[name$="[pilihDetail]"]').is(":checked")){ //hitung yang dicheck aja
            var jmltarif = parseFloat($(this).find('input[name$="[jumahtarif]"]').val());
            var jmljasa = parseFloat($(this).find('input[name$="[jumlahjasa]"]').val());
            var jmlbayar = parseFloat($(this).find('input[name$="[jumlahbayar]"]').val());
            var jmlsisa = parseFloat(jmljasa - jmlbayar);
            $(this).find('input[name$="[sisajasa]"]').val(jmlsisa);
            totTarif += jmltarif;
            totJasa += jmljasa;
            totBayar += jmlbayar;
            totSisa += jmlsisa;
        }
    });
    $("#BKPembayaranjasaT_totaltarif").val(totTarif);
    $("#BKPembayaranjasaT_totaljasa").val(totJasa);
    $("#BKPembayaranjasaT_totalbayarjasa").val(totBayar);
    $("#BKPembayaranjasaT_totalsisajasa").val(totSisa);
    formatNumbers();
}
function checkAll(obj){
    if($(obj).is(':checked')){
        $('#tabelDetail tbody tr').each(function(){
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).attr('checked',true)});
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).val(1)});
        });
    }else{
        $('#tabelDetail tbody tr').each(function(){
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).removeAttr('checked')});
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).val("")});
        });
    }
    hitungSemua();
}
function checkIni(obj){
    if($(obj).is(':checked')){
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).attr('checked',true)});
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).val(1)});
    }else{
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).removeAttr('checked')});
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).val("")});
        $('#pilihSemua').removeAttr('checked');
    }
    hitungSemua();
}
<?php echo isset($_GET['id']) ? '' : 'hitungSemua();'; ?>

function print(caraPrint) 
{
    <?php if (!empty($model->pembayaranjasa_id)){?>
        window.open('<?php echo Yii::app()->createUrl($this->module->id.'/'.$this->id.'/Print', array('id'=>$model->pembayaranjasa_id)); ?>'+'&caraPrint=' + caraPrint,'printwin','left=100,top=100,width=980,height=400,scrollbars=1');
    <?php } ?>
}
</script>