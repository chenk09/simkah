
<?php 
if (isset($caraPrint)){
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data['judulLaporan'].'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');     
    }     
}
?>
<?php $this->renderPartial('application.views.headerReport.headerDefault',array('colspan'=>10)); ?>

<table class="table table-condensed">
    <tr>
        <td>NO. NOTA</td>
        <td>: <?php echo (($model->tandabuktikeluar_id == NULL ) ? "-" : $model->tandabuktikeluar->nokaskeluar); ?></td>
    </tr>
    <tr>
        <td>NO. RETUR</td>
        <td>: <?php echo ($model->returresep->noreturresep == NULL ) ? "-": $model->returresep->noreturresep; ?></td>
    </tr>
    <tr>
        <td>NAMA PASIEN</td>
        <td>: <?php echo ($model->returresep_id == NULL) ? "-" : $model->returresep->penjualanresep->pendaftaran->pasien->nama_pasien; ?></td>
    </tr>
    <tr>
        <td>TGL RETUR</td>
        <td>: <?php echo ($model->returresep_id == NULL ) ? "-" : $model->returresep->tglretur; ?></td>
    </tr>
</table>

<table class="table table-condensed table-striped table-bordered" style='margin-top:5px;'>
    <thead>

        <tr>
            <th>No.</th><th>Uraian</th><th>Total Harga Jual</th><th>Administrasi Retur</th><th>Total Harga Retur</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $counter = 1;

        $totalSubTotal = 0;
            $subTotal = $model->totaloaretur - $model->biayaadministrasi;
            $totalSubTotal += $subTotal;

            ?>

                <tr>
                    <td><?php echo ($counter++); ?></td>
                    <td><?php echo "Obat Alkes";  ?><?php //echo '/'.$rincian->daftartindakan_nama ?></td>
                    <td style="text-align: right;"><?php echo MyFunction::formatNumber($model->totaloaretur); ?></td>
                    <td style="text-align: right;"><?php echo MyFunction::formatNumber($model->biayaadministrasi); ?></td>
                    <td style="text-align: right;"><?php echo MyFunction::formatNumber($subTotal); ?></td>
                </tr>
                <?php 

        ?>
        
        <tr>
            <td style="text-align: right;" colspan="4">TOTAL</td>
            <td style="text-align: right;"><?php echo MyFunction::formatNumber($totalSubTotal); //echo MyFunction::formatNumber($pembayaran->totaliurbiaya); ?></td>
        </tr>
    </tbody>
</table>

<table>
    <tr><td><?php echo Yii::app()->user->getState('kabupaten_nama').', '.date('d-M-Y'); ?></td></tr>
    <tr><td>Yang membuat,</td></tr>
    <tr><td>Petugas I</td><td></td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td><?php echo Yii::app()->user->getState('nama_pegawai'); ?></td><td></tr>
</table>

<?php 
echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'disabled'=>false,'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp";                 

$urlPrint=  Yii::app()->createAbsoluteUrl('billingKasir/kwitansi/viewRincian&id='.$model->returbayarpelayanan_id);
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}

JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
?>
