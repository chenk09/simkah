<legend class="rim2">Informasi Retur Penjualan Obat</legend>
<?php
$this->widget('bootstrap.widgets.BootAlert');

Yii::app()->clientScript->registerScript('cariPasien', "
$('#search').submit(function(){
	$.fn.yiiGridView.update('informasipenjualanresep-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'search',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'method'=>'get',
        'htmlOptions'=>array(),
));

    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'informasipenjualanresep-grid',
	'dataProvider'=>$modInfoReturPenjualanObat->searchReturPenjualan(),
//        'filter'=>$modInfo,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header'=>'Tgl Retur Pelayanan',
                'type'=>'raw',
                'value'=>'$data->tglreturpelayanan',
            ),
             array(
                'header'=>'No Retur Pembayaran',
                'type'=>'raw',
                'value'=>'$data->noreturbayar',
            ),
            array(
                'header'=>'No Retur Resep',
                'type'=>'raw',
                'value'=>'$data->returresep->noreturresep',
            ),
            array(
                'header'=>'Jenis Penjualan',
                'type'=>'raw',
                'value'=>'$data->returresep->penjualanresep->jenispenjualan',
            ),
            array(
                'header'=>'Nama Pasien',
                'type'=>'raw',
                'value'=>'$data->returresep->penjualanresep->pendaftaran->pasien->nama_pasien',
            ),
            array(
                'header'=>'Total Retur Obat',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totaloaretur - $data->biayaadministrasi)',
            ),
           array(
                  'header'=>'No Tanda Bukti Keluar',
                  'type'=>'raw',
                  'value'=>'(($data->tandabuktikeluar_id !=0) ? CHtml::Link("<i class=\"icon-print\"></i>","",
                                        array("class"=>"", 
                                              "href"=>"",
                                              "onclick"=>"printKasir($data->returbayarpelayanan_id,$data->tandabuktikeluar_id);return false",
                                              "rel"=>"tooltip",
                                              "title"=>"Klik untuk print Kwitansi",
                                        )).$data->tandabuktikeluar->nokaskeluar : "")'
              ),
            array(
                'header'=>'Rincian Retur Penjualan',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("informasiReturObatAlkes/detailRetur",array("id"=>$data->returbayarpelayanan_id)),

                         array("class"=>"", 
                                  "target"=>"iframeRincianReturObat",
                                  "onclick"=>"$(\"#dialogRincianReturObat\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat Rincian Retur Obat",
                            ))',          
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
             array(
                'header'=>'Pembayaran',
                'type'=>'raw',
                'value'=>'(($data->tandabuktikeluar_id == 0) ? CHtml::Link("<i class=\"icon-list-silver\"></i>",Yii::app()->createAbsoluteUrl("billingKasir/informasiReturObatAlkes/pembayaran",array("idReturBayar"=>$data->returbayarpelayanan_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeReturPembayaran",
                                  "onclick"=>"$(\"#dialogReturPembayaran\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk membayar retur obat alkes",
                            )) : "Sudah Diretur")',          
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
//            array(
//                'header'=>'Jenis Penjualan',
//                'type'=>'raw',
//                'value'=>'$data->noreturresep',
//            ),
//            array(
//                'header'=>'Mengetahui',
//                'type'=>'raw',
//                'value'=>'$data->pegawai->nama_pegawai',
//            ),
//            array(
//                'header'=>'Pegawai Retur',
//                'type'=>'raw',
//                'value'=>'$data->pegawairetur->nama_pegawai',
//            ),
//            
//            array(
//                'header'=>'Detail Penjualan',
//                'type'=>'raw', 
//                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("informasiReturPenjualan/detailReturPenjualan",array("id"=>$data->returresep_id)),
//                            array("class"=>"", 
//                                  "target"=>"iframeDetailRetur",
//                                  "onclick"=>"$(\"#dialogDetailRetur\").dialog(\"open\");",
//                                  "rel"=>"tooltip",
//                                  "title"=>"Klik untuk lihat Detail Retur Penjualan",
//                            ))',
//                'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
//            ),
            
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <table class="table-condensed">
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::label('Tgl Retur','tglawal',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modInfoReturPenjualanObat,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                                    'maxDate' => 'd',
                                                    //
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label(' Sampai dengan','tgl_akhir', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modInfoReturPenjualanObat,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                                    'minDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('No Retur','noreturbayar',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($modInfoReturPenjualanObat,'noreturbayar',array('class'=>'span3')); ?>
                    </div>
                </div>
            </td>
            <td>
                <?php //echo $form->textFieldRow($modInfoReturPenjualan,'no_rekam_medik',array('class'=>'span3 numbersOnly','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php //echo $form->textFieldRow($modInfoReturPenjualan,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
        </tr>
    </table>
</fieldset>

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
            <?php  
                $this->widget('TipsMasterData',array()); 
            ?>
    </div>

<?php $this->endWidget(); ?>
<?php Yii::app()->clientScript->registerScript('',"
function printKasir(idReturBayar,idTandaBukti)
{
    if(idTandaBukti!=''){ 
             window.open('".Yii::app()->createUrl('print/bayarReturPenjualanObat')."&idReturBayar='+idReturBayar+'&idTandaBukti='+idTandaBukti,'printwin','left=100,top=100,width=400,height=400,scrollbars=1');
    }     
}",  CClientScript::POS_HEAD);
?>
<?php 
//================= Dialog Rincian Retur Penjualan Obat Alkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianReturObat',
    'options'=>array(
        'title'=>'Detail Rincian Retur Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>false,
    ),
));
?>
<iframe src="" name="iframeRincianReturObat" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
//========= End dialog Rincian Retur Penjualan Obat Alkes =============================
 ?>

<?php 
//================== Dialog Rincian Pembayaran Retur Obat =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogReturPembayaran',
    'options'=>array(
        'title'=>'Pembayaran Retur Penjualan Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>false,
    ),
));
?>
<iframe src="" name="iframeReturPembayaran" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
//========= End Dialog Rincian Pembayaran Retur Obat =============================
 ?>