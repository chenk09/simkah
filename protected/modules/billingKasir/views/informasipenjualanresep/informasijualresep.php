<?php
$this->breadcrumbs=array(
	'Daftar Pasien'=>array('/billingKasir/daftarPasien'),
	'PasienRJ',
);?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'method'=>'GET',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
<legend class="rim2">Informasi Penjualan Obat Alkes</legend>
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchPenjualanBebasLuar(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                      array(
                          'header'=>'No Resep / Struk',
                          'value'=>'$data->noresep',
                          'type'=>'raw',
                      ),
                      array(
                          'header'=>'Total Harga Jual',
                          'value'=>'"Rp. ".MyFunction::formatNumber($data->totalhargajual)',
                          'type'=>'raw',
                      ),
                      array(
                          'header'=>'Tanggal Penjualan',
                          'value'=>'$data->tglpenjualan',
                      ),
                      'jenispenjualan',
                        array(
                          'header'=>'No BKM / No Faktur',
                          'type'=>'raw',
                          'value'=>'((!empty($data->NoFaktur)) ? 
                                        CHtml::Link("<i class=\"icon-print\"></i> $data->NoBkm","",
                                        array("class"=>"", 
                                              "href"=>"",
                                              "onclick"=>"printKasir($data->penjualanresep_id,$data->tandaBuktiBayar,\"PRINT\");return false",
                                              "rel"=>"tooltip",
                                              "title"=>"Klik untuk print BKM",
                                        ))."<br>"
                                        .CHtml::Link("<i class=\"icon-print\"></i> $data->NoFaktur","",
                                        array("class"=>"", 
                                              "href"=>"",
                                              "onclick"=>"printFaktur($data->penjualanresep_id,$data->tandaBuktiBayar,\"PRINT\");return false",
                                              "rel"=>"tooltip",
                                              "title"=>"Klik untuk print faktur",
                                        )) : "Belum Lunas")'
                      ),
                      array(
                            'header'=>'Rincian Penjualan',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("informasipenjualanresep/fakturPembayaranApotek",array("id"=>$data->penjualanresep_id, "idTandaBukti"=>$data->tandaBuktiBayar)),
                                        array("class"=>"", 
                                              "target"=>"iframeRincianTagihan",
                                              "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
                                              "rel"=>"tooltip",
                                              "title"=>"Klik untuk melihat Rincian Tagihan",
                                        ))',          
                            'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                      ),
                      
                        array(
                            'header'=>'Pembayaran',
                            'type'=>'raw',
                            'value'=>'((empty($data->NoFaktur)) ? CHtml::Link("<i class=\"icon-list-silver\"></i>",Yii::app()->createAbsoluteUrl("farmasiApotek/pembayaranLangsung/index",array("idPenjualanResep"=>$data->penjualanresep_id,"frame"=>true)),
                                        array("class"=>"", 
                                              "target"=>"iframePembayaran",
                                              "onclick"=>"$(\"#dialogPembayaranKasir\").dialog(\"open\");",
                                              "rel"=>"tooltip",
                                              "title"=>"Klik untuk membayar ke kasir",
                                        )) : "Sudah Lunas")',          
                            'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                        ),
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>
</fieldset>
<?php echo $this->renderPartial('_formKriteriaPencarian', array('model'=>$model,'form'=>$form),true);  ?> 
<?php Yii::app()->clientScript->registerScript('',"
function printKasir(idPenjualanResep,idTandaBukti,caraPrint)
{
    if(idTandaBukti!=''){ 
             window.open('".Yii::app()->createUrl('billingKasir/informasipenjualanresep/buktiKasMasukFarmasi')."&idPenjualanResep='+idPenjualanResep+'&idTandaBukti='+idTandaBukti+'&caraPrint='+caraPrint,'printwin','left=100,top=100,width=840,height=400,scrollbars=1');
    }     
}
function printFaktur(idPenjualanResep,idTandaBukti,caraPrint)
{
    if(idTandaBukti!=''){ 
             window.open('".Yii::app()->createUrl('billingKasir/informasipenjualanresep/fakturPembayaranApotek')."&id='+idPenjualanResep+'&idTandaBukti='+idTandaBukti+'&caraPrint='+caraPrint,'printwin','left=100,top=100,width=840,height=400,scrollbars=1');
    }     
}
",  CClientScript::POS_HEAD);
?>

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
						<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPembayaranKasir',
    'options'=>array(
        'title'=>'Pembayaran',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
        'close'=>"js:function(){ $.fn.yiiGridView.update('pencarianpasien-grid', {
                        data: $('#caripasien-form').serialize()
                    }); }",
    ),
));
?>
<iframe src="" name="iframePembayaran" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianTagihan',
    'options'=>array(
        'title'=>'Rincian Penjualan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianTagihan" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>