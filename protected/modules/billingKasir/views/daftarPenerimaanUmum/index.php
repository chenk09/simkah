<?php
$this->breadcrumbs=array(
	'Daftar Penerimaan Umum',
);

Yii::app()->clientScript->registerScript('search', "
$('#penerimaan-t-search').submit(function(){
	$.fn.yiiGridView.update('daftarpenerimaan-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<legend class="rim2">Informasi Penerimaan Umum</legend>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'penerimaan-t-search',
        'type'=>'horizontal',
)); ?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarpenerimaan-m-grid',
	'dataProvider'=>$modPenerimaan->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            'tglpenerimaan',
            'nopenerimaan',
            array(
                'header'=>'Nama Penerimaan',
                'value'=>'$data->jenispenerimaan->jenispenerimaan_nama',
            ),
            'namapenandatangan',
            'kelompoktransaksi',
            'volume',
            'satuanvol',
            array('name'=>'hargasatuan',
                  'value'=>'"Rp. ".MyFunction::formatNumber($data->hargasatuan)'
            ),
            array('name'=>'totalharga',
                  'value'=>'"Rp. ".MyFunction::formatNumber($data->totalharga)'
            ),
            array( 
                'header'=>'Retur Penerimaan Umum',
                'type'=>'raw',
                'htmlOptions' => array(
                    'style' => 'width: 100px; text-align: center;',
                ),
                'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("returPenerimaanUmum/index",array("frame"=>1,"idPenerimaan"=>$data->penerimaanumum_id)) ,array("title"=>"Klik Untuk Meretur Penerimaan Umum","target"=>"iframeRetur", "onclick"=>"$(\"#dialogRetur\").dialog(\"open\");", "rel"=>"tooltip"))',
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<legend class="rim">Pencarian</legend>
<table class="table-condensed">
    <tr>
        <td>
            <div class="control-group ">
                <?php $modPenerimaan->tglAwal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenerimaan->tglAwal, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                <?php echo CHtml::label('Tgl Penerimaan','tglPenerimaan', array('class'=>'control-label inline')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPenerimaan,
                                        'attribute'=>'tglAwal',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                        )); 
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php $modPenerimaan->tglAkhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenerimaan->tglAkhir, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label inline')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPenerimaan,
                                        'attribute'=>'tglAkhir',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'minDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                        )); 
                    ?>
                </div>
            </div>
            <?php echo $form->textFieldRow($modPenerimaan,'nopenerimaan',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        </td>
        <td>
            <?php echo $form->textFieldRow($modPenerimaan,'namapenandatangan',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->textFieldRow($modPenerimaan,'nippenandatangan',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->textFieldRow($modPenerimaan,'kelompoktransaksi',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
	 			<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>
<?php $this->endWidget(); ?>

<?php
// ===========================Dialog Retur=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogRetur',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Retur Penerimaan Umum',
                        'autoOpen'=>false,
                        'minWidth'=>1100,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="iframeRetur" width="100%" height="550">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Retur================================
