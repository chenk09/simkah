<?php
echo CHtml::css('.control-label{
        float:left; 
        text-align: right; 
        width:50%;
        color:black;
        padding-right:10px;
        font-size:8pt;
    }
    body{
        font-size:8pt;
    }
    td .uang{
        text-align:right;
    }
    .border{
        border:1px solid;
    }
');
?>
<?php
if (isset($caraPrint)){
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data['judulHalaman'].'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');     
    }else if($caraPrint == 'PRINT'){
        echo CHtml::css('.control-label{
                float:left; 
                text-align: right; 
                width:50%;
                color:black;
                padding-right:10px;
                font-size:11pt;
            }
            td, th{
                font-size:11pt;
            }
            
        ');
    }
//    echo $this->renderPartial('application.views.headerReport.headerDefault', array('judulLaporan'=>$data['judulLaporan']));      
    
}
?>
<br><br><br><br>
<table width="74%" style="margin:0px;" cellpadding="0" cellspacing="0">
    <tr><td><center><b><u><?php echo strtoupper($data['judulHalaman']) ?></u></b></center></td></tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0">
                    <td width="50%">
                        <label class='control-label'>
                            No. RM / No. Pend :
                        </label>
                            <?php echo CHtml::encode($modPendaftaran->pasien->no_rekam_medik); ?> / 
                            <?php echo CHtml::encode($modPendaftaran->no_pendaftaran); ?>
                    </td>
                    <Td width="5%"></td>
                    <td>
                        <label class='control-label'>
                            Nama PJP :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->penanggungjawab_id) > 0)
                            {
                                echo CHtml::encode($modPendaftaran->penanggungJawab->nama_pj);
                            }else{
                                echo CHtml::encode($modPendaftaran->pasien->nama_pasien);
                            }
                        ?>
                    </td>
                </tr>
                <tr>

                <tr>
                    <td>
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('nama_pasien')); ?>:
                        </label>
                        <?php echo CHtml::encode($modPendaftaran->pasien->nama_pasien); ?>
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            Alamat PJP :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->penanggungjawab_id) > 0)
                            {
                                echo CHtml::encode($modPendaftaran->penanggungJawab->nama_pj);
                            }else{
                                echo CHtml::encode($modPendaftaran->pasien->alamat_pasien);
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('jeniskelamin')); ?>:
                        </label>
                            <?php echo CHtml::encode($modPendaftaran->pasien->jeniskelamin); ?>
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('alamat_pasien')); ?>:
                        </label>
                            <?php echo CHtml::encode($modPendaftaran->pasien->alamat_pasien); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->getAttributeLabel('umur')); ?>:
                        </label>
                            <?php echo CHtml::encode($modPendaftaran->umur); ?>
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            Cara Bayar - Penjamin :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->carabayar_id)  && strlen($modPendaftaran->penjamin_id) > 0)
                            {
                                echo CHtml::encode($modPendaftaran->carabayar->carabayar_nama)." - ". CHtml::encode($modPendaftaran->penjamin->penjamin_nama);
                            }else{
                                echo '-'."/"."-";
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Unit Pelayanan :</label>
                            <?php echo CHtml::encode($modPendaftaran->instalasi->instalasi_nama); ?>
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            Nama Rujukan :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->rujukan->nama_perujuk)> 0)
                            {
                                echo CHtml::encode($modPendaftaran->rujukan->nama_perujuk);
                            }else{
                                echo '-';
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Dokter Pemeriksa :</label>
                        <?php echo CHtml::encode($modPendaftaran->dokter->nama_pegawai); ?>                        
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            Rujukan Dari :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->rujukan_id)> 0)
                            {
                                echo CHtml::encode($modPendaftaran->rujukan->asalrujukan->asalrujukan_nama);
                            }else{
                                echo '-';
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Tgl Pemeriksaan :</label>
                        <?php
                            if(strlen($modPendaftaran->tgl_pendaftaran) > 0)
                            {
                                echo CHtml::encode(substr($modPendaftaran->tgl_pendaftaran,0,11));
                            }else{
                                echo '-';
                            }
                        ?>                      
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            No Rujukan :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->rujukan_id)> 0)
                            {
                                echo CHtml::encode($modPendaftaran->rujukan->no_rujukan);
                            }else{
                                echo '-';
                            }
                        ?>
                    </td>
                </tr>
                
            </table>            
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" style='margin-left:auto; margin-right:auto;'> <!--  class='table table-striped table-bordered table-condensed' -->
                <thead class="border">
                    <tr>
                        <th>No.</th>
                        <th>Tanggal</th>
                        <th>No. Resep</th>
                        <th>Nama Items</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Total</th>
                        <!--<th>Status</th>-->
                    </tr>
                </thead>
    <?php
        $totalSeluruh = 0;
        $totalObat = 0;
        $totalGas = 0;
        $totalAlkes = 0;
        $totalAdmin = 0;
        $kelompokObat = 0;
        $kelompokAlkes = 0;
        $kelompokGas = 0;
        $adminGas = 0;
        $adminObat = 0;
        $adminAlkes = 0;
        $data_obat = array();
        $data_alkes = array();
        $data_gas = array();

        foreach ($modRincian as $i => $mod){
            //PEMBULATAN KEATAS HARGA SATUAN DESIMAL 2 ANGKA
            $mod->hargasatuan_oa = round($mod->hargasatuan_oa, 2, PHP_ROUND_HALF_UP);
            
            if((strtolower($mod->jenisobatalkes_nama) == 'gas medis')){
                if($mod->qty_oa > 0){
                    $data_gas[$kelompokGas] = $mod;
                    $kelompokGas++;
                    $totalGas += ($mod->qty_oa * $mod->hargasatuan_oa);
                    $adminGas += ($mod->biayaservice + $mod->biayaadministrasi + $mod->biayakonseling);
                }
            } else 
            if((strtolower($mod->jenisobatalkes_nama) == 'alkes')){
                if($mod->qty_oa > 0){
                    $data_alkes[$kelompokAlkes] = $mod;
                    $kelompokAlkes++;
                    $totalAlkes += ($mod->qty_oa * $mod->hargasatuan_oa);
                    $adminAlkes += ($mod->biayaservice + $mod->biayaadministrasi + $mod->biayakonseling);
                }
            }else{
                if($mod->qty_oa > 0){
                    $data_obat[$kelompokObat] = $mod;
                    $kelompokObat++;
                    $totalObat += ($mod->qty_oa * $mod->hargasatuan_oa);
                    $adminObat += ($mod->biayaservice + $mod->biayaadministrasi + $mod->biayakonseling);
                }
            }
        }
        $totalAdmin = $adminGas + $adminAlkes + $adminObat;
        $totalSeluruh = ($totalObat + $totalAlkes + $totalGas);

        
    ?>
        <?php
        if($kelompokObat > 0){
            echo "<tr><td colspan = '7'><b>Kelompok : Obat</b></td></tr>";
//            for($i = 0;$i < $kelompokObat;$i++){
            $format = new CustomFormat();
            foreach($data_obat AS $i => $data){
            ?>
                <tr>
                    <td><?php echo ($i+1); ?></td>
                    <td><?php echo substr($format->formatDateINAtimeNew($data_obat[$i]->tglpenjualan),0,11);?></td>
                    <td><?php echo $data_obat[$i]->noresep;?></td>
                    <td><?php echo $data_obat[$i]->obatalkes_nama;?></td>
                    <!--<td class="uang"><?php // echo number_format($data_obat[$i]->qty_oa,2,',','.');?></td>-->
                    <td class="uang"><?php echo ($data_obat[$i]->qty_oa);?></td>
                    <td class="uang"><?php echo number_format($data_obat[$i]->hargasatuan_oa,2,',','.');?></td>
                    <td class="uang"><?php echo number_format(($data_obat[$i]->qty_oa * $data_obat[$i]->hargasatuan_oa),2,',','.');?></td>
                    <!--<td><?php echo (empty($data_alkes[$i]->oasudahbayar_id)) ? "Belum Lunas" : "Sudah Lunas";?></td>-->
                </tr>
            <?php } ?>
            <tr class="border">
                <td colspan="6"></td><td class="uang"><b><?php echo number_format($totalObat,2,',','.'); ?></b></td>
            </tr>
        <?php } ?>
        <?php
        if($kelompokAlkes > 0){
            echo "<tr><td colspan = '7'><b>Kelompok : Alat Kesehatan</b></td></tr>";
//            for($i = 0;$i < $kelompokAlkes;$i++){
            foreach($data_alkes AS $i => $data){
            ?>
                <tr>
                    <td><?php echo ($i+1); ?></td>
                    <td><?php echo substr($format->formatDateINAtimeNew($data_alkes[$i]->tglpenjualan),0,11);?></td>
                    <td><?php echo $data_alkes[$i]->noresep;?></td>
                    <td><?php echo $data_alkes[$i]->obatalkes_nama;?></td>
                    <!--<td class="uang"><?php // echo number_format($data_alkes[$i]->qty_oa,2,',','.');?></td>-->
                    <td class="uang"><?php echo ($data_alkes[$i]->qty_oa);?></td>
                    <td class="uang"><?php echo number_format($data_alkes[$i]->hargasatuan_oa,2,',','.');?></td>
                    <td class="uang"><?php echo number_format(($data_alkes[$i]->qty_oa * $data_alkes[$i]->hargasatuan_oa),2,',','.');?></td>
                    <!--<td><?php echo (empty($data_alkes[$i]->oasudahbayar_id)) ? "Belum Lunas" : "Sudah Lunas";?></td>-->
                </tr>
            <?php } ?>
            <tr class="border">
                <td colspan="6"></td><td class="uang"><b><?php echo number_format($totalAlkes,2,',','.'); ?></b></td>
            </tr>
        <?php } ?>
        <?php
        if($kelompokGas > 0){
            echo "<tr><td colspan = '7'><b>Kelompok : Gas Medis</b></td></tr>";
//            for($i = 0;$i < $kelompokGas;$i++){
            foreach($data_gas AS $i => $data){
            ?>
                <tr>
                    <td><?php echo ($i+1); ?></td>
                    <td><?php echo substr($format->formatDateINAtimeNew($data_gas[$i]->tglpenjualan),0,11);?></td>
                    <td><?php echo $data_gas[$i]->noresep;?></td>
                    <td><?php echo $data_gas[$i]->obatalkes_nama;?></td>
                    <!--<td class="uang"><?php // echo number_format($data_gas[$i]->qty_oa,2,',','.');?></td>-->
                    <td class="uang"><?php echo ($data_gas[$i]->qty_oa);?></td>
                    <td class="uang"><?php echo number_format($data_gas[$i]->hargasatuan_oa,2,',','.');?></td>
                    <td class="uang"><?php echo number_format(($data_gas[$i]->qty_oa * $data_gas[$i]->hargasatuan_oa),2,',','.');?></td>
                    <!--<td><?php echo (empty($data_gas[$i]->oasudahbayar_id)) ? "Belum Lunas" : "Sudah Lunas";?></td>-->
                </tr>
            <?php } ?>
            <tr class="border">
                <td colspan="6"></td><td class="uang"><b><?php echo number_format($totalGas,2,',','.'); ?></b></td>
            </tr>
        <?php }?>        
        <tfoot>
            <tr>
                <td colspan="6" class="uang"><b>Total Tagihan :</b></td>
                <td class="uang"><b><?php echo number_format($totalSeluruh,2,',','.'); ?></b></td>
            </tr>
            <tr>
                <td colspan="6" class="uang"><b>YANFAR :</b></td>
                <td class="uang"><b><?php echo number_format($totalAdmin,2,',','.'); ?></b></td>
            </tr>
            <tr>
                <td colspan="6" class="uang"><b>Total Tangungan :</b></td>
                <td class="uang"><b><?php echo number_format(($totalSeluruh + $totalAdmin),2,',','.'); ?></b></td>
            </tr>
        </tfoot>
            </table>
        </td>
    </tr>
</table>
<?php if ($caraPrint == 'PRINT') { ?>

<table width="80%" style="margin-top:20px;">
    <tr>
        <td width="50%" align="center"></td>
        <td width="50%" align="center">
            Tasikmalaya, <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-mm-dd hh:mm:ss')); ?><br>
            Petugas,
            <div style="margin-top:50px;"></div><?php echo $data['nama_pegawai']; ?>
        </td>
    </tr>
</table>
<?php }
else { 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/rincianTagihanFarmasi/rincian');
$idPendaftaran = $modPendaftaran->pendaftaran_id;
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/&id=${idPendaftaran}&caraPrint="+caraPrint,"",'location=_new, width=1100px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);         
 } ?>
