<style>
    .jarak{
        margin-top: 50px;
    }
    body{
        font-family: tahoma;
        font-size: 11px;
    }
    .info td{
        font-size: 11px;
    }
    table td {
        vertical-align: top;
    }
    .grid td{
        border-top: 0.5px solid;
        border-collapse: collapse;
        /*border:1px solid #000;*/
        font-size: 11px;
    }
    .grid th{
        border-collapse: collapse;
        font-size: 12px;
        font-family: tahoma;
        border-collapse: collapse;
        /*border-top: 0.5px solid;*/
    }
    .grid td,th{
        padding: 5px;
    } 
</style>
<div>&nbsp;</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="info" style="page-break-inside: avoid;">
    <tr>
        <!-- DITAMPILKAN DI MPDF
        <td width="100">No. RM / Reg</td>
        <td width="35%"> :
            <?php echo CHtml::encode($modPendaftaran->pasien->no_rekam_medik); ?> / 
            <?php echo CHtml::encode($modPendaftaran->no_pendaftaran); ?>            
        </td>
        -->
        <td width="100">Nama Pasien</td>
        <td width="35%">: <?php echo CHtml::encode($modPendaftaran->pasien->nama_pasien); ?></td>        
        <td width="20%">Nama PJP</td>
        <td>:
            <?php
                if(strlen($modPendaftaran->penanggungjawab_id) > 0)
                {
                    echo CHtml::encode($modPendaftaran->penanggungJawab->nama_pj);
                }else{
                    echo CHtml::encode($modPendaftaran->pasien->nama_pasien);
                }
            ?>        
        </td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>:
            <?php echo CHtml::encode($modPendaftaran->pasien->jeniskelamin); ?> / 
            <?php echo CHtml::encode($modPendaftaran->umur); ?>
        </td>
        <td>Alamat PJP</td>
        <td rowspan="2" valign="top">:
            <?php echo CHtml::encode($modPendaftaran->pasien->alamat_pasien); ?></td>
    </tr>
    <tr>
        <td>Unit Pelayanan</td>
        <td>: 
            <?php echo CHtml::encode($modPendaftaran->instalasi->instalasi_nama); ?>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Dokter Pemeriksa</td>
        <td>: 
            <?php echo CHtml::encode($modPendaftaran->dokter->nama_pegawai); ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Tgl Perawatan</td>
        <td>: 
            <?php
                if(strlen($modPendaftaran->tgl_pendaftaran) > 0)
                {
                    echo CHtml::encode($modPendaftaran->tgl_pendaftaran);
                }else{
                    echo '-';
                }
            ?>        
        </td>
        <td>Perusahaan Penjamin</td>
        <td>: <?php echo CHtml::encode($modRincian[0]->penjamin_nama); ?></td>
    </tr>
</table>
<div style="clear: both"></div>
<div align="center"><?php echo strtoupper($data['judulHalaman']) ?></div>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid">
    <thead class="border">
        <tr>
            <th width="5">No.</th>
            <th width="12%">Tanggal</th>
            <th>No. Resep</th>
            <th>Nama Items</th>
            <th width="5%">Qty</th>
            <th width="10%">Harga</th>
            <th width="10%">Total</th>
        </tr>
    </thead>
</table>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid">
    <tbody>
    <?php
        $totalSeluruh = 0;
        $totalObat = 0;
        $totalGas = 0;
        $totalAlkes = 0;
        $totalAdmin = 0;
        $kelompokObat = 0;
        $kelompokAlkes = 0;
        $kelompokGas = 0;
        $adminGas = 0;
        $adminObat = 0;
        $adminAlkes = 0;
        $data_obat = array();
        $data_alkes = array();
        $data_gas = array();
        foreach ($modRincian as $i => $mod){
            //PEMBULATAN KEATAS HARGA SATUAN DESIMAL 2 ANGKA
            $mod->hargasatuan_oa = round($mod->hargasatuan_oa, 2, PHP_ROUND_HALF_UP);
            
            if((strtolower($mod->jenisobatalkes_nama) == 'gas medis')){
                if($mod->qty_oa > 0){
                    $data_gas[$kelompokGas] = $mod;
                    $kelompokGas++;
                    $totalGas += ($mod->qty_oa * $mod->hargasatuan_oa);
                    $adminGas += ($mod->biayaservice + $mod->biayaadministrasi + $mod->biayakonseling);
                }
            } else 
            if((strtolower($mod->jenisobatalkes_nama) == 'alkes')){
                if($mod->qty_oa > 0){
                    $data_alkes[$kelompokAlkes] = $mod;
                    $kelompokAlkes++;
                    $totalAlkes += ($mod->qty_oa * $mod->hargasatuan_oa);
                    $adminAlkes += ($mod->biayaservice + $mod->biayaadministrasi + $mod->biayakonseling);
                }
            }else{
                if($mod->qty_oa > 0){
                    $data_obat[$kelompokObat] = $mod;
                    $kelompokObat++;
                    $totalObat += ($mod->qty_oa * $mod->hargasatuan_oa);
                    $adminObat += ($mod->biayaservice + $mod->biayaadministrasi + $mod->biayakonseling);
                }
            }
        }
        $totalAdmin = $adminGas + $adminAlkes + $adminObat;
        $totalSeluruh += ($totalObat + $totalAlkes + $totalGas);

        
    ?>

    <?php
    if($kelompokObat > 0){
        echo '<tr><td colspan ="7" style=""><b>Kelompok : Obat</b></td></tr>';
//        for($i = 0;$i < $kelompokObat;$i++){
        foreach($data_obat AS $i => $value){
        ?>
            <tr style="font-:">
                <td valign="top"><?php echo ($i+1); ?></td>
                <td valign="top"><?php echo date('d/m/Y', strtotime($modRincian[$i]->tglpenjualan));?></td>
                <td valign="top"><?php echo $data_obat[$i]->noresep;?></td>
                <td valign="top"><?php echo $data_obat[$i]->obatalkes_nama;?></td>
                <!--<td valign="top" class="uang"><?php // echo number_format($data_obat[$i]->qty_oa,2,',','.');?></td>-->
                <td valign="top" class="uang"><?php echo ($data_obat[$i]->qty_oa);?></td>
                <td valign="top" class="uang"><?php echo number_format($data_obat[$i]->hargasatuan_oa,2,',','.');?></td>
                <td valign="top" class="uang"><?php echo number_format(($data_obat[$i]->qty_oa * $data_obat[$i]->hargasatuan_oa),2,',','.');?></td>
            </tr>
        <?php } ?>
        <tr class="border">
            <td colspan="6">Total Biaya</td><td class="uang"><b><?php echo number_format($totalObat,2,',','.'); ?></b></td>
        </tr>
    <?php }?>

    <?php
    if($kelompokAlkes > 0){
        echo '<tr><td colspan ="7"><b>Kelompok : Alat Kesehatan</b></td></tr>';
//        for($i = 0;$i < $kelompokAlkes;$i++){
        foreach($data_alkes AS $i => $value){
    ?>
            <tr>
                <td valign="top"><?php echo ($i+1); ?></td>
                <td valign="top"><?php echo date('d/m/Y', strtotime($modRincian[$i]->tglpenjualan));?></td>
                <td valign="top"><?php echo $data_alkes[$i]->noresep;?></td>
                <td valign="top"><?php echo $data_alkes[$i]->obatalkes_nama;?></td>
                <!--<td valign="top" class="uang"><?php // echo number_format($data_alkes[$i]->qty_oa,2,',','.');?></td>-->
                <td valign="top" class="uang"><?php echo ($data_alkes[$i]->qty_oa);?></td>
                <td valign="top" class="uang"><?php echo number_format($data_alkes[$i]->hargasatuan_oa,2,',','.');?></td>
                <td valign="top" class="uang"><?php echo number_format(($data_alkes[$i]->qty_oa * $data_alkes[$i]->hargasatuan_oa),2,',','.');?></td>
            </tr>
    <?php
        }
    ?>
        <tr class="border">
            <td colspan="6">Total Biaya</td><td class="uang"><b><?php echo number_format($totalAlkes,2,',','.'); ?></b></td>
        </tr>
    <?php    
    }
    ?>
    <?php
        if($kelompokGas > 0){
            echo '<tr><td colspan ="7"><b>Kelompok : Gas Medis</b></td></tr>';
//            for($i = 0;$i < $kelompokGas;$i++){
            foreach($data_gas AS $i => $value){
            ?>
                <tr>
                    <td valign="top"><?php echo ($i+1); ?></td>
                    <td valign="top"><?php echo $data_gas[$i]->tglpenjualan;?></td>
                    <td valign="top"><?php echo $data_gas[$i]->noresep;?></td>
                    <td valign="top"><?php echo $data_gas[$i]->obatalkes_nama;?></td>
                    <!--<td valign="top" class="uang"><?php // echo number_format($data_gas[$i]->qty_oa,2,',','.');?></td>-->
                    <td valign="top" class="uang"><?php echo ($data_gas[$i]->qty_oa);?></td>
                    <td valign="top" class="uang"><?php echo number_format($data_gas[$i]->hargasatuan_oa,2,',','.');?></td>
                    <td valign="top" class="uang"><?php echo number_format(($data_gas[$i]->qty_oa * $data_gas[$i]->hargasatuan_oa),2,',','.');?></td>
                </tr>
            <?php } ?>
            <tr class="border">
                <td colspan="6"></td><td class="uang"><b><?php echo number_format($totalGas,2,',','.'); ?></b></td>
            </tr>
        <?php }?>
    </tbody>
</table>
<br>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="info">
    <tr>
        <td width="50%" align="center">
            Tasikmalaya, <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-mm-dd hh:mm:ss')); ?><br>
            Petugas,
            <div class="jarak">&nbsp;</div>
            <div class="jarak">&nbsp;</div>
            <div class="jarak">&nbsp;</div>
            <?php echo $data['nama_pegawai']; ?>            
        </td>
        <td valign="top" align="center">
            <table width="280" cellpadding="0" cellspacing="0" border="0" class="footer">
                <tfoot>
                <tr>
                    <td class="uang"><b>Total Tagihan</b></td>
                    <td width="35%" class="uang" align="right"><b><?php echo number_format($totalSeluruh,2,',','.'); ?></b></td>
                </tr>
                <tr>
                    <td class="uang"><b>YANFAR</b></td>
                    <td class="uang" align="right"><b><?php echo number_format($totalAdmin,2,',','.'); ?></b></td>
                </tr>
                <tr>
                    <td class="uang"><b>Total Tangungan</b></td>
                    <td class="uang" align="right"><b><?php echo number_format(($totalSeluruh + $totalAdmin),2,',','.'); ?></b></td>
                </tr>
                </tfoot>    
            </table>            
        </td>
    </tr>
</table>

<script type="text/javascript">

    function insertCetakan()
    {
        var params = {jenis_cetakan:'<?=Params::jenisCetakan($data['jenis_cetakan'])?>',pendaftaran_id:'<?=$modPendaftaran->pendaftaran_id?>'};
        
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/updateJumlahCetakan');?>", {id:params},
            function(data)
            {
                if(data.status == 'not')
                {
                    console.log('insert cetakan data error');
                }else{
                    $('#cetakan_jum').text('Cetakan Ke ' + data.jumlah);
                }
            }, "json"
        );
    }
    insertCetakan();

</script>