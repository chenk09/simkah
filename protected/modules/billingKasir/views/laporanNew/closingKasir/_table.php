<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'laporanclosingkasir-m-grid',
	'dataProvider'=>$model->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Penerimaan</center>',
                'start'=>4, 
                'end'=>5, 
            ),
            array(
                'name'=>'<center>Banyaknya</center>',
                'start'=>9, 
                'end'=>11, 
            ),
        ),
	'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
                'tglclosingkasir',
                array(
                    'name'=>'closingdari',
                    'header'=>'Closing Dari <br>Sampai Dengan',
                    'type'=>'raw',
                    'value'=>'$data->closingdari." <br> ".$data->sampaidengan',
                ),
//                'nama_pegawai',
//                'shift_nama',
                array(
                    'name'=>'closingsaldoawal',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->closingsaldoawal)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'name'=>'terimauangmuka',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->terimauangmuka)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'name'=>'terimauangpelayanan',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->terimauangpelayanan)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'name'=>'totalpengeluaran',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->totalpengeluaran)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'name'=>'nilaiclosingtrans',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->nilaiclosingtrans)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'name'=>'totalsetoran',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->totalsetoran)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'name'=>'jmltransaksi',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->jmltransaksi)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'name'=>'jmluanglogam',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->jmluanglogam)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'name'=>'jmluangkertas',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->jmluangkertas)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'name'=>'piutang',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->piutang)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                'keterangan_closing',
                
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>