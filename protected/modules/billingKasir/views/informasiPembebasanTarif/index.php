<?php
$this->breadcrumbs=array(
	'Informasi Pembebasan Tarif',
);

Yii::app()->clientScript->registerScript('search', "
$('#divSearch-form form').submit(function(){
	$.fn.yiiGridView.update('informasipembebasantarif-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
<legend class="rim2">Informasi Pembebasan Tarif</legend>
<div id="divSearch-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'informasipembebasantarif-t-search',
        'type'=>'horizontal',
)); ?>
    <?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'informasipembebasantarif-m-grid',
	'dataProvider'=>$model->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    'tglpembebasan',
                    'nama_pegawai',
                    array(
                        'name'=>'nama_pasien',
                        'header'=>'Nama Pasien / No. RM',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien." / ".$data->no_rekam_medik',
                    ),
                    'daftartindakan_nama',
                    'komponentarif_nama',
                    array(
                        'name'=>'jmlpembebasan',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->jmlpembebasan)',
                    )
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Pembebasan',  CHtml::activeId($model, 'tglAwal'), array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'dd M yy',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                    </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Sampai Dengan',CHtml::activeId($model, 'tglAkhir'), array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'dd M yy',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                    </div>
            </div> 
            <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3')); ?>
            <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3')); ?>
        </td>
        <td>
              <?php echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span3')); ?>
              <?php echo $form->textFieldRow($model,'daftartindakan_nama',array('class'=>'span3')); ?>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onclick'=>'resetForm();')); ?>
	 			<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>
<?php $this->endWidget(); ?>
    
</div>
</fieldset>
<script>
function resetForm(){
    window.open("<?php echo $this->createUrl("/".$this->route); ?>", "_self");
}
</script>
    