<legend class="rim2">Informasi Pasien Rawat Inap Sedang Dirawat</legend>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>

<?php
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$url = Yii::app()->createUrl('billingKasir/pasienRawatInapSedangDirawat/frameGrafik&id=1');

Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
//echo Yii::app()->user->getState('ruangan_id');
?>
<?php
$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'daftarPasien-grid',
    'dataProvider' => $model->searchRI(),
//                'filter'=>$model,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Tgl Admisi / Masuk Kamar',
            'type' => 'raw',
            'value' => '$data->tglAdmisiMasukKamar'
        ),
//                    'ruangan_nama',
        array(
            'name' => 'caramasuk_nama',
            'type' => 'raw',
            'value' => '$data->caramasuk_nama',
        ),
        array(
            'header' => 'No RM / No Pendaftaran',
            'type' => 'raw',
            'value' => '$data->noRmNoPend',
        ),
        array(
            'header' => 'Nama Pasien / Alias',
            'value' => '$data->namaPasienNamaBin'
        ),
        array(
            'name' => 'jeniskelamin',
            'value' => '$data->jeniskelamin',
        ),
        array(
            'name' => 'umur',
            'type' => 'raw',
            'value' => 'CHtml::hiddenField("RIInfokunjunganriV[$data->pendaftaran_id][idPendaftaran]", $data->pendaftaran_id, array("id"=>"idPendaftaran","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span3"))."".$data->umur',
        ),
        array(
            'name' => 'Dokter',
            'type' => 'raw',
            'value' => '$data->nama_pegawai',
        ),
        array(
            'header' => 'Cara Bayar / Penjamin',
            'value' => '$data->caraBayarPenjamin',
        ),
        array(
            'name' => 'kelaspelayanan_nama',
//                        'type'=>'raw',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;'),
//            'footerHtmlOptions' => array('colspan' => 0, 'style' => 'text-align:right;font-style:italic;'),
//            'footer' => 'Jumlah Total',
        ),
        array(
            'name' => 'totaltagihan',
            'type' => 'raw',
            'value' => '"Rp. ".MyFunction::formatNumber($data->totaltagihan)',
            // 'value'=>'MyFunction::formatNumber($data->totaltagihan)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
//            'footerHtmlOptions' => array('style' => 'text-align:right;'),
//           // 'footer' => 'sum(totaltagihan)',
//           'footer'=>number_format($model->getTotaltagihan()),

        ),
        array(
            'header' => 'Deposit',
            'name' => 'totaluangmuka',
            'type' => 'raw',
            //    'value'=>'$data->totaluangmuka',
            //  'value'=>'MyFunction::formatNumber($data->totaluangmuka)',
            'value' => '"Rp. ".MyFunction::formatNumber($data->totaluangmuka)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
//            'footerHtmlOptions' => array('style' => 'text-align:right;'),
//            'footer' => 'sum(totaluangmuka)',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
?>

<?php echo $this->renderPartial('_formPencarian', array('model' => $model)); ?>
