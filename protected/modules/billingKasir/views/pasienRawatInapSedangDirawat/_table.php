<?php 
    $table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchRIprint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchRIprint();
         $template = "{pager}{summary}\n{items}";
    }
?>


<?php $this->widget($table,array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
           array(
                       'header'=>'Tgl Admisi / Masuk Kamar',
                        'type'=>'raw',
                        'value'=>'$data->tglAdmisiMasukKamar'
                    ),
//                    'ruangan_nama',
                    array(
                       'name'=>'caramasuk_nama',
                        'type'=>'raw',
                        'value'=>'$data->caramasuk_nama',
                    ),
                    array(
                       'header'=>'No RM / No Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->noRmNoPend',
                    ),
                    array(
                        'header'=>'Nama Pasien / Alias',
                        'value'=>'$data->namaPasienNamaBin'
                    ),
                    array(
                        'name'=>'jeniskelamin',
                        'value'=>'$data->jeniskelamin',
                    ),
                    array(
                        'name'=>'umur',
                        'type'=>'raw',
                        'value'=>'CHtml::hiddenField("RIInfokunjunganriV[$data->pendaftaran_id][idPendaftaran]", $data->pendaftaran_id, array("id"=>"idPendaftaran","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span3"))."".$data->umur',
                    ),
                    array(
                       'name'=>'Dokter',
                        'type'=>'raw',
                        'value'=>'$data->nama_pegawai',
                    ),
                    array(
                        'header'=>'Cara Bayar / Penjamin',
                        'value'=>'$data->caraBayarPenjamin',
                    ),
                    array(
                       'name'=>'kelaspelayanan_nama',
                        'type'=>'raw',
                        'value'=>'$data->kelaspelayanan_nama',
                    ),
            array(
                       'name'=>'totaltagihan',
                        'type'=>'raw',
                       // 'value'=>'$data->totaltagihan',
                 'value'=>'MyFunction::formatNumber($data->totaltagihan)',
                    ),
            array(
                        'header'=>'Deposit',
                       'name'=>'totaluangmuka',
                        'type'=>'raw',
                    //    'value'=>'$data->totaluangmuka',
                 'value'=>'MyFunction::formatNumber($data->totaluangmuka)',
                    ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>