<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('tableTrans', {
        data: $(this).serialize()
    });
    $.fn.yiiGridView.update('tableRekap', {
        data: $(this).serialize()
    });
    $.fn.yiiGridView.update('tableDetail', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Laboratorium</legend>
<div class="search-form">
    <?php
        $this->renderPartial('billingKasir.views.laporan.laboratoirum/_search',
            array(
                'model'=>$model,
            )
        ); 
    ?>
</div>
<div class="tab">
    <?php
        $this->widget('bootstrap.widgets.BootMenu',array(
            'type'=>'tabs',
            'stacked'=>false,
            'htmlOptions'=>array('id'=>'tabmenu'),
            'items'=>array(
                array('label'=>'Transaksi Laboratorium','url'=>'javascript:tab(0);', 'itemOptions'=>array("index"=>0),'active'=>true),
                array('label'=>'Rekap / Pasien','url'=>'javascript:tab(1);', 'itemOptions'=>array("index"=>1)),
                array('label'=>'Laboratorium Per-Registrasi','url'=>'javascript:tab(2);', 'itemOptions'=>array("index"=>2)),
            ),
        ))
    ?>
    <div id="div_reportTranasksi">
        <legend class="rim">Transaksi Laboratorium</legend>
        <fieldset>
            <?php
                $this->renderPartial('billingKasir.views.laporan.laboratoirum/_tableTransaksi',
                    array(
                        'model'=>$model
                    )
                );
            ?>
        </fieldset>
    </div>
    <div id="div_rekap">
        <legend class="rim">Rekap / Pasien</legend>
        <fieldset>
            <?php
                /*
				$this->renderPartial('billingKasir.views.laporan.laboratoirum/_tableRekap',
                    array(
                        'model'=>$model
                    )
                );
				*/
            ?>
        </fieldset>
    </div>
    <div id="div_per_registrasi">
        <legend class="rim">Laboratoium Per-Registrasi</legend>
        <fieldset>
            <?php
				/*
                $this->renderPartial('billingKasir.views.laporan.laboratoirum/_tablePerRegistrasi',
                    array(
                        'model'=>$model
                    )
                );
				*/
            ?>
        </fieldset>
    </div>
</div>
<?php

$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanLab');

$js = <<< JSCRIPT
$(document).ready(function() {
    $("#tabmenu").children("li").children("a").click(function() {
        $("#tabmenu").children("li").attr('class','');
        $(this).parents("li").attr('class','active');
        $(".icon-pencil").remove();
        $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
    });
    $("#BKLaporanLaboratorium_filter_tab").val("trans");
    
    $("#div_reportTranasksi").show();
    $("#div_rekap").hide();
    $("#div_per_registrasi").hide();
});

function tab(index){
    $(this).hide();
    $(".btn-group").show();
    if (index==0){
        $("#BKLaporanLaboratorium_filter_tab").val("trans");
        $("#div_reportTranasksi").show();
        $("#div_rekap").hide();
        $("#div_per_registrasi").hide();
    }else if(index==1){
        $("#BKLaporanLaboratorium_filter_tab").val("rekap");
        $("#div_reportTranasksi").hide();
        $("#div_rekap").show();
        $("#div_per_registrasi").hide();
    }else if(index==2){
        $(".btn-group").hide();
        $("#BKLaporanLaboratorium_filter_tab").val("per_reg");
        $("#div_reportTranasksi").hide();
        $("#div_rekap").hide();
        $("#div_per_registrasi").show();
    }
}

function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#searchLaporan').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}

function onReset(){
    setTimeout(
        function(){
            $.fn.yiiGridView.update('tableTrans', {
                data: $("#searchLaporan").serialize()
            });
            $.fn.yiiGridView.update('tableRekap', {
                data: $("#searchLaporan").serialize()
            });
            $.fn.yiiGridView.update('tableDetail', {
                data: $("#searchLaporan").serialize()
            });
        }, 1000
    );
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
?>