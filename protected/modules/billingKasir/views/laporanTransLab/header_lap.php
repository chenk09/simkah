<?php
$controller = Yii::app()->controller->action->id;
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('table_rekap',{
        data: $(this).serialize()
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Laboratorium</legend>
<div class="search-form">
    <?php
        $this->renderPartial('billingKasir.views.laporanTransLab/_search',
            array(
                'model'=>$model,
                'view' => $view,
            )
        ); 
    ?>
</div>
<div class="tab">
    <?php
        $this->widget('bootstrap.widgets.BootMenu',array(
            'type'=>'tabs',
            'stacked'=>false,
            'htmlOptions'=>array('id'=>'tabmenu'),
            'items'=>array(
                array(
					'label'=>'Transaksi Laboratorium',
					'url'=>array('index', 'view'=>'trans'),
					'active'=>($view == 'trans' ? true : false )
				),
                array(
					'label'=>'Rekap / Pasien',
					'url'=>array('index', 'view'=>'pasien'),
					'active'=>($view == 'pasien' ? true : false )
				),
                array(
					'label'=>'Laboratorium Per-Registrasi',
					'url'=>array('index', 'view'=>'reg'),
					'active'=>($view == 'reg' ? true : false )
				),
            ),
        ))
    ?>
</div>