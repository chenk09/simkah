<?php
    if($caraPrint == 'EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $data['judulLaporan'] .'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');
    }
    
    echo $this->renderPartial('application.views.headerReport.headerLaporan',
        array(
            'judulLaporan'=>$data['judulLaporan'],
            'periode'=>$data['periode']
        )
    );
?>

<table width="100%" class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th width="100">No. Transaksi</th>
            <th width="100">Tgl. Transaksi</th>
            <th>Items</th>
            <th width="50">Qty</th>
            <th width="100">Pasien</th>
            <th width="100">SubTotal</th>
            <th width="100">Tanggungan P3</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $cols = '';
            $jum_norm = 0;
            foreach($row as $val)
            {
                $cols .= '<tr>';
                $cols .= '<td colspan="7">
                    <div><label style="margin-right:20px;">No. RM</label> : ('. $val['no_rm'] .') '. $val['nama'] .'</div>
                </td>';
                $cols .= '<tr>';
                foreach($val['data_transaksi'] as $value)
                {
                    $cols .= '<tr>';
                    $cols .= '<td colspan="7">
                        <div><label style="margin-right:10px;">Registrasi</label> : '. $value['no_reg'] .' ('. $value['tgl_reg'] .')</div>
                    </td>';
                    $cols .= '<tr>';
                    
                    $col = '';
                    $jum_noreg = 0;
                    foreach($value['value'] as $values)
                    {
                        $col .= '<tr>';
                        $col .= '<td>'. $values['no_trans'] .'</td>';
                        $col .= '<td>'. $values['tgl_trans'] .'</td>';
                        $col .= '<td>'. $values['nama_item'] .'</td>';
                        $col .= '<td>'. $values['qty'] .'</td>';
                        $col .= '<td>'. $values['tarif'] .'</td>';
                        $col .= '<td>'. $values['sub_total'] .'</td>';
                        $col .= '<td>'. $values['tanggungan_p3'] .'</td>';
                        $col .= '<tr>';
                        $jum_noreg++;
                    }
                    $cols .= $col;
                    $cols .= '<tr>';
                    $cols .= '<td colspan="7" align="right">Jumlah Transaksi Registrasi '. $value['no_reg'] .' : '. $jum_noreg .'</td>';
                    $cols .= '<tr>';
                    $jum_norm += $jum_noreg;
                }
                $cols .= '<tr>';
                $cols .= '<td colspan="7" align="right">Jumlah Transaksi No. RM '. $val['no_rm'] . ' : ' . $jum_norm .'</td>';
                $cols .= '<tr>';
            }
            echo ($cols);
        ?>
    </tbody>
</table>