<?php
if (isset($caraPrint)){
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data['judulLaporan'].'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');     
    }
    echo $this->renderPartial('application.views.headerReport.headerDefault', array('judulLaporan'=>$data['judulLaporan']));      
}
?>
<?php
echo CHtml::css('.control-label{
        float:left; 
        text-align: right; 
        width:120px;
        color:black;
        padding-right:10px;
    }
    table{
        font-size:11px;
    }
');
?>

<table width="100%" style="margin:0px;" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0">
                    <td width="50%">
                        <label class='control-label'>
                            No. RM / No. Pend :
                        </label>
                            <?php echo CHtml::encode($modPendaftaran->pasien->no_rekam_medik); ?> / 
                            <?php echo CHtml::encode($modPendaftaran->no_pendaftaran); ?>
                    </td>
                    <Td width="5%"></td>
                    <td>
                        <label class='control-label'>
                            Nama PJP :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->penanggungjawab_id) > 0)
                            {
                                echo CHtml::encode($modPendaftaran->penanggungJawab->nama_pj);
                            }else{
                                echo CHtml::encode($modPendaftaran->pasien->nama_pasien);
                            }
                        ?>
                    </td>
                </tr>
                <tr>

                <tr>
                    <td>
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('nama_pasien')); ?>:
                        </label>
                        <?php echo CHtml::encode($modPendaftaran->pasien->nama_pasien); ?>
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            Alamat PJP :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->penanggungjawab_id) > 0)
                            {
                                echo CHtml::encode($modPendaftaran->penanggungJawab->nama_pj);
                            }else{
                                echo CHtml::encode($modPendaftaran->pasien->alamat_pasien);
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('jeniskelamin')); ?>:
                        </label>
                            <?php echo CHtml::encode($modPendaftaran->pasien->jeniskelamin); ?>
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('alamat_pasien')); ?>:
                        </label>
                            <?php echo CHtml::encode($modPendaftaran->pasien->alamat_pasien); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('no_telepon_pasien')); ?>:
                        </label>
                            <?php echo CHtml::encode($modPendaftaran->pasien->no_telepon_pasien); ?>
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            Cara Bayar - Penjamin :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->carabayar_id)  && strlen($modPendaftaran->penjamin_id) > 0)
                            {
                                echo CHtml::encode($modPendaftaran->carabayar->carabayar_nama)." - ". CHtml::encode($modPendaftaran->penjamin->penjamin_nama);
                            }else{
                                echo '-'."/"."-";
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Umur :</label>
                        <?php echo CHtml::encode($modPendaftaran->umur); ?>
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            Nama Rujukan :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->rujukan->nama_perujuk)> 0)
                            {
                                echo CHtml::encode($modPendaftaran->rujukan->nama_perujuk);
                            }else{
                                echo '-';
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Unit Pelayanan :</label>
                        <?php echo CHtml::encode($modPendaftaran->instalasi->instalasi_nama); ?>
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            Rujukan Dari :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->rujukan_id)> 0)
                            {
                                echo CHtml::encode($modPendaftaran->rujukan->asalrujukan->asalrujukan_nama);
                            }else{
                                echo '-';
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Dokter Pemeriksa :</label>
                        <?php echo CHtml::encode($modPendaftaran->dokter->nama_pegawai); ?>                        
                    </td>
                    <Td></td>
                    <td>   
                        <label class='control-label'>
                            No Rujukan :
                        </label>
                        <?php
                            if(strlen($modPendaftaran->rujukan_id)> 0)
                            {
                                echo CHtml::encode($modPendaftaran->rujukan->no_rujukan);
                            }else{
                                echo '-';
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Tgl Perawatan :</label>
                        <?php echo CHtml::encode(date("d M Y",strtotime($modRincian[0]->tgl_tindakan))." - ".date("d M Y",strtotime($modRincian[count($modRincian)-1]->tgl_tindakan))); ?>                  
                    </td>
                </tr>
            </table>            
        </td>
    </tr>
    <tr>
        <td>
            <div align="center" style="border-bottom: 1px solid #000000;padding: 10px;margin-bottom: 15px;">
                <?php echo strtoupper($data['judulLaporan']);?>
            </div>
            <?php
                $totalbiayaadminfarmasi = 0;
                $row = array();
                foreach($modRincian as $i=>$val)
                {
                    //PEMBULATAN KEATAS HARGA SATUAN DESIMAL 2 ANGKA
                    $val->tarif_satuan = round($val->tarif_satuan, 2, PHP_ROUND_HALF_UP);
            
                    $ruangan_id = $val->ruangan_id;
                    $row[$ruangan_id]['nama'] = $val->ruangan_nama;
                    if($val->instalasi_id == Params::INSTALASI_ID_LAB || $val->instalasi_id == Params::INSTALASI_ID_RAD){
                        $row[$ruangan_id]['nama'] = "LABORATORIUM & RADIOLOGI";
                    }
                    $row[$ruangan_id]['ruangan_id'] = $val->ruangan_id;
                    $row[$ruangan_id]['pendaftaran_id'] = $val->pendaftaran_id;
                    $row[$ruangan_id]['kategori'][$i]['ruangan_id'] = $val->ruangan_id;
                    $row[$ruangan_id]['kategori'][$i]['nama_pegawai'] = $val->nama_pegawai;
                    $row[$ruangan_id]['kategori'][$i]['tindakanpelayanan_id'] = $val->tindakanpelayanan_id;
                    $row[$ruangan_id]['kategori'][$i]['daftartindakan_nama'] = $val->daftartindakan_nama;
                    $row[$ruangan_id]['kategori'][$i]['tgl_tindakan'] = date('d-m-Y',  strtotime($val->tgl_tindakan));
                    $row[$ruangan_id]['kategori'][$i]['kelas'] = (strtolower($val->kelaspelayanan_nama) == 'tanpa kelas') ? "-" : $val->kelaspelayanan_nama;
                    $row[$ruangan_id]['kategori'][$i]['harga'] = $val->tarif_satuan;
                    $row[$ruangan_id]['kategori'][$i]['qty'] = $val->qty_tindakan;
                    $row[$ruangan_id]['kategori'][$i]['total'] = ($row[$ruangan_id]['kategori'][$i]['harga'] * $row[$ruangan_id]['kategori'][$i]['qty']);
                    $row[$ruangan_id]['kategori'][$i]['harga_dokter'] = (isset($val->tarif_medis) ? $val->tarif_medis : 0);
                    $row[$ruangan_id]['kategori'][$i]['total_dokter'] = (isset($val->tarif_medis) ? ($val->qty_tindakan * $val->tarif_medis) : 0);
                    $row[$ruangan_id]['kategori'][$i]['subsidiasuransi_tindakan'] = (isset($val->subsidiasuransi_tindakan) ? ($val->subsidiasuransi_tindakan) : 0);
                    $row[$ruangan_id]['kategori'][$i]['subsidipemerintah_tindakan'] = (isset($val->subsidipemerintah_tindakan) ? ($val->subsidipemerintah_tindakan) : 0);
                    $row[$ruangan_id]['kategori'][$i]['subsisidirumahsakit_tindakan'] = (isset($val->subsisidirumahsakit_tindakan) ? ($val->subsisidirumahsakit_tindakan) : 0);
                    $row[$ruangan_id]['kategori'][$i]['iurbiaya_tindakan'] = (isset($val->iurbiaya_tindakan) ? ($val->iurbiaya_tindakan) : 0);
                    $obatalkes = ObatalkespasienT::model()->findByPk($val->tindakanpelayanan_id);
                    $row[$ruangan_id]['kategori'][$i]['noresep'] = $obatalkes->penjualanresep->noresep;
                    $row[$ruangan_id]['kategori'][$i]['tgltransaksi'] = date('d-m-Y',strtotime($obatalkes->penjualanresep->tglpenjualan));
                    $row[$ruangan_id]['kategori'][$i]['jenisobatalkes'] = $obatalkes->obatalkes->jenisobatalkes->jenisobatalkes_nama;
                    $tindakan = TindakanpelayananT::model()->findByPk($val->tindakanpelayanan_id);
                    if(!empty($tindakan->detailhasilpemeriksaanlab_id)){
                        $row[$ruangan_id]['kategori'][$i]['tglpenunjang'] = date('d-m-Y',strtotime($tindakan->detailhasilpemeriksaanlab->update_time));
                    }else if(!empty($tindakan->hasilpemeriksaanrad_id)){
                        $row[$ruangan_id]['kategori'][$i]['tglpenunjang'] = date('d-m-Y',strtotime($tindakan->hasilpemeriksaanrad->update_time));
                    }else{
                        $row[$ruangan_id]['kategori'][$i]['tglpenunjang'] = date('d-m-Y',strtotime($tindakan->pasienmasukpenunjang->tglmasukpenunjang));
                    }
                    $row[$ruangan_id]['kategori'][$i]['nopenunjang'] = $tindakan->pasienmasukpenunjang->no_masukpenunjang;
                    //Total biaya racik dll
                    $totalbiayaadminfarmasi += ($val->biayaadministrasi + $val->biayaservice + $val->biayakonseling) ;
                }
            ?>
            <table width="100%" style='margin-left:auto; margin-right:auto;' class='table table-striped table-bordered table-condensed'>
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Kelas</th>
                        <th>Harga (Rp)</th>
                        <th>Banyak</th>
                        <th>Total Biaya</th>
                    </tr>
                </thead>
                <?php
                    $cols = '';
                    $total_biaya = 0;
                    $iurBiaya = 0;
                    $subsidiAsuransi = 0;
                    $subsidiPemerintah = 0;
                    $subsidiRumahSakit = 0;
                    $tampilHeaderLabRad = true;
                    $totalLabRad = 0;
                    $tampilTotalLabRad = true;
                    foreach($row as $key=>$values)
                    {
                        $modRuangan = RuanganM::model()->findByPK($key); 
                        if($modRuangan->instalasi_id == Params::INSTALASI_ID_RI){
                            $modPasienAdmisi = PasienadmisiT::model()->findByAttributes(array(
                                'pendaftaran_id'=>$values['pendaftaran_id'],
                            ));
                            $modKamarRuangan = KamarruanganM::model()->findByPk($modPasienAdmisi->kamarruangan_id);
                            $cols .= '<tr>';
                            $cols .= '<td colspan=6><b>'.strtoupper($values['nama']) .' Kamar ' . $modKamarRuangan->kamarruangan_nokamar . ' Bed ' . $modKamarRuangan->kamarruangan_nobed .'</b></td>';
                            $cols .= '</tr>';
                        }else{
                            if($modRuangan->ruangan_id == Params::RUANGAN_ID_LAB || $modRuangan->ruangan_id == Params::RUANGAN_ID_RAD){
                                if($tampilHeaderLabRad == true){
                                    $cols .= '<td colspan=6><b>'.strtoupper($values['nama']) .'</b></td>';
                                    $cols .= '</tr>';
                                    $tampilHeaderLabRad = false; //tampilkan header sekali
                                }
                            }else{
                                $cols .= '<td colspan=6><b>'.strtoupper($values['nama']) .'</b></td>';
                                $cols .= '</tr>';
                            }
                        }
                        $col = '';
                        $tampilAdminFarmasi = true;
                        $total = 0;
                        $tempAdminFarmasi = 0;
                        foreach($values['kategori'] as $x => $val)
                        {
                            if(
                                $values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_DEPO || 
                                $values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_RJ || 
                                $values['ruangan_id'] == Params::RUANGAN_ID_LAB ||
                                $values['ruangan_id'] == Params::RUANGAN_ID_RAD
                            ){
                                if($values['ruangan_id'] == Params::RUANGAN_ID_LAB || $values['ruangan_id'] == Params::RUANGAN_ID_RAD){ //FORMAT LAB
                                        if(!empty($val['nopenunjang'])){
                                            $col .= '<tr style="page-break-inside: avoid;">';
                                            $col .= '<td width="130" style="padding-left:10">'.$values['kategori'][$x]['tglpenunjang'].' /'.$val['nopenunjang'].'</td>';
                                            $col .= '<td>'.$val['daftartindakan_nama'].'</td>';
                                            $col .= '<td width="80" style="text-align:center;">'. $val['kelas'] .'</td>';
                                            $col .= '<td width="80" align="right" class="uang"></td>'; //merge tarif dokter '. number_format($values['kategori'][$x]['harga'] + $values['kategori'][$x]['harga_dokter'],0,'','.') .'
                                            $col .= '<td width="50" align="right" class="uang"></td>'; //$values['kategori'][$x]['qty']
                                            $col .= '<td width="80" align="right" class="uang">'. number_format($values['kategori'][$x]['total'],2,',','.') .'</td>'; //  + $values['kategori'][$x]['total_dokter']
                                            $col .= '</tr>';
                                            $totalLabRad += ($values['kategori'][$x]['total']); //TOTAL DOKTER TERMASUK KE HARGA SATUAN >> + $values['kategori'][$x]['total_dokter']
                                        }
                                        //Total Lab Rad Footer
                                        if(($values['kategori'][$x]['ruangan_id'] == Params::RUANGAN_ID_RAD && $values['kategori'][$x+1]['ruangan_id'] != Params::RUANGAN_ID_RAD)){
                                            if($tampilTotalLabRad == true){
                                                $col .= '<tr>';
                                                $col .= '<td colspan=5 class="total">Total Biaya :</td>';
                                                $col .= '<td class="total" align="right"><b>'. number_format($totalLabRad,2,',','.') .'</b></td>';
                                                $col .= '</tr>';
                                                $tampilTotalLabRad = false;
                                            }
                                        }
                                }else{ //FORMAT APOTEK
                                    if($totalbiayaadminfarmasi > 0 && $tampilAdminFarmasi == true){
                                        $col .= '<tr>';
                                        $col .= '<td width="10">&nbsp;</td>';
                                        $col .= '<td >YANFAR</td>';
                                        $col .= '<td width="80" style="text-align:center;">'. $val['kelas'] .'</td>';
                                        $col .= '<td width="80" style="text-align:right;">'.number_format($totalbiayaadminfarmasi,2,',','.').'</td>';
                                        $col .= '<td width="5" style="text-align:right;">1</td>';
                                        $col .= '<td width="80" style="text-align:right;">'.number_format($totalbiayaadminfarmasi,2,',','.').'</td>';
                                        $col .= '</tr>';
                                        $tampilAdminFarmasi = false;
                                        $tempAdminFarmasi = $totalbiayaadminfarmasi;
                                    }
                                    $col .= '<tr style="page-break-inside: avoid;">';
                                    $col .= '<td width="130" style="padding-left:10">'.$values['kategori'][$x]['tgltransaksi'].' /'.$values['kategori'][$x]['noresep'].'</td>';
                                    $col .= '<td>'.$val['daftartindakan_nama'].'</td>';
                                    $col .= '<td width="80" style="text-align:center;">'. $val['kelas'] .'</td>';
                                    $col .= '<td width="80" align="right" class="uang">'.number_format($val['harga'],2,',','.').'</td>'; 
                                    $col .= '<td width="50" align="right" class="uang">'.number_format($val['qty'],2,',','.').'</td>';
                                    $col .= '<td width="80" align="right" class="uang">'. number_format($values['kategori'][$x]['total'],2,',','.') .'</td>';
                                    $col .= '</tr>';
                                }
                                
                            }else{ //FORMAT RI/RJ/RD/Gizi/IBS
                                $col .= '<tr style="page-break-inside: avoid;">';
                                $col .= '<td width="70">'.$val['tgl_tindakan'].'</td>';
                                $col .= '<td >'. $val['daftartindakan_nama'] .'</td>';
                                $col .= '<td width="80" style="text-align:center;">'. $val['kelas'] .'</td>';
                                $col .= '<td width="80" align="right" class="uang">'. number_format($val['harga'],2,',','.') .'</td>'; //TOTAL DOKTER TERMASUK KE TARIF SATUAN  + $val['harga_dokter']
                                $col .= '<td width="50" align="right" class="uang">'. $val['qty'] .'</td>';
                                $col .= '<td width="80" align="right" class="uang">'. number_format($val['total'],2,',','.') .'</td>'; //TOTAL DOKTER TERMASUK KE TARIF SATUAN + $val['total_dokter']
                                $col .= '</tr>';

                                $tempAdminFarmasi = 0;
                                if(strlen($val['nama_pegawai']) > 0)
                                {
                                    if(strtoupper($values['nama']) != 'PENDAFTARAN'){
                                        if($values['ruangan_id'] == Params::RUANGAN_ID_IBS || $values['ruangan_id'] == Params::RUANGAN_ID_GIZI){
                                            //hide dokter untuk IBS dan Gizi
                                        }else{
                                            $col .= '<tr>';
                                            $col .= '<td>&nbsp;</td>';
                                            $col .= '<td>'. $val['nama_pegawai'] .'</td>';
                                            $col .= '<td style="text-align:center;">'. $val['kelas'] .'</td>';
            //                                KARNA tarif_satuan SUDAH TERMASUK tarif_medis, dll
            //                                $col .= '<td class="uang" align="right">'. number_format($val['harga_dokter'],0,'','.') .'</td>';
            //                                $col .= '<td class="uang" align="right">'. $val['qty'] .'</td>';
            //                                $col .= '<td class="uang" align="right">'. number_format($val['total_dokter'],0,'','.') .'</td>';
                                            $col .= '<td class="uang" align="right"></td>';
                                            $col .= '<td class="uang" align="right"></td>';
                                            $col .= '<td class="uang" align="right"></td>';
                                            $col .= '</tr>'; 
                                        }
                                    }   
                                }
                            }
                            
                            $total += ($val['qty'] * $val['harga']); //TOTAL DOKTER TERMASUK KE TARIF SATUAN >> + ($val['qty'] * $val['harga_dokter']
                            $subsidiAsuransi += $val['subsidiasuransi_tindakan'];
                            $subsidiPemerintah += $val['subsidipemerintah_tindakan'];
                            $subsidiRumahSakit += $val['subsisidirumahsakit_tindakan'];
                            $iurBiaya += $val['iurbiaya_tindakan'];
                        }
                        
                        if(($values['ruangan_id'] == Params::RUANGAN_ID_LAB ||
                        $values['ruangan_id'] == Params::RUANGAN_ID_RAD)){ 
                            //UNTUK TOTAL LAB RAD CODENYA ADA DI ATAS
                        }else{
                            //JANGAN DI BULATKAN >> $total = round($total + $tempAdminFarmasi);
                            $total = $total + $tempAdminFarmasi;
                            $col .= '<tr>';
                            $col .= '<td colspan=5 class="total">Total Biaya :</td>';
                            $col .= '<td class="total" align="right"><b>'. number_format($total,2,',','.') .'</b></td>';
                            $col .= '</tr>';
                        }
                        $cols .= $col;
                        $total_biaya += $total;
                    }
                    echo($cols);
                    //HARUS DARI DATABASE >> $iurBiaya = $total - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit);  //karena $iurBiaya yang diambil di tindakanpelayanan_t sering tidak sama dengan total biaya - subsidi 
                ?>
                 <tfoot>
                    <tr>
                        <td colspan="5"><div class='pull-right'>Total Biaya Keselurahan</div></td>
                        <td style="text-align:right;"><?php echo number_format($total_biaya,2,',','.'); ?></td>
                    </tr>
                    <tr>
                        <td colspan="5"><div class='pull-right'>Subsidi Asuransi</div></td>
                        <td style="text-align:right;"><?php echo number_format($subsidiAsuransi,2,',','.'); ?></td>
                    </tr>
                    <tr>
                        <td colspan="5"><div class='pull-right'>Subsidi Pemerintah</div></td>
                        <td style="text-align:right;"><?php echo number_format($subsidiPemerintah,2,',','.'); ?></td>
                    </tr>
                    <tr>
                        <td colspan="5"><div class='pull-right'>Subsidi Rumah Sakit</div></td>
                        <td style="text-align:right;"><?php echo number_format($subsidiRumahSakit,2,',','.'); ?></td>
                    </tr>
                    <!--
                    <tr>
                        <td colspan="6"><div class='pull-right'>Iur Biaya</div></td>
                        <td style="text-align:right;"><?php //echo number_format($iurBiaya,2,',','.');?></td>
                    </tr>
                    -->
                    <tr>
                        <td colspan="5"><div class='pull-right'>Tanggungan Pasien</div></td>
                        <td style="text-align:right;"><?php echo number_format(abs($total_biaya-$subsidiAsuransi-$subsidiPemerintah-$subsidiRumahSakit),2,',','.');?></td>
                    </tr>
                </tfoot>
                
            </table>
        </td>
    </tr>
</table>
<?php if (isset($caraPrint)) { ?>

<table width="100%" style="margin-top:20px;">
    <tr>
        <td width="50%" align="left" align="top">
            <table width="50%">
                <tr>
                    <td width="50%" align="center">
                        <div>Tasikmalaya, <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-mm-dd hh:mm:ss')); ?></div>
                        <div>Petugas RSJK</div>
                        <div style="margin-top:60px;"><?php echo $data['nama_pegawai']; ?></div>
                    </td>
                </tr>
            </table>
        </td>
        <td align="right" valign="top">
            <table width="50%">
                <tr>
                    <td width="50%">Total Biaya</td>
                    <td width="3%">:</td>
                    <td><?php echo $total_biaya; ?></td>
                </tr>
                <tr>
                    <td>Deposit</td>
                    <td>:</td>
                    <td><?php echo $data['uang_cicilan']; ?></td>
                </tr>
                <tr>
                    <td>Tanggungan Pasien</td>
                    <td>:</td>
                    <td>
                        <?php 
                            $kembalian = $total_biaya;
                            if($data['uang_cicilan'] > 0){
                                if($data['uang_cicilan'] < $total_biaya)
                                {
                                    $kembalian = $total_biaya - $data['uang_cicilan'];
                                }                                            
                            }
                            echo $kembalian;
                        ?>
                    </td>
                </tr>
            </table>                        
        </td>
    </tr>
</table>
<?php } else { 

//        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'printPDF(\'PDF\')'))."&nbsp&nbsp"; 
        $this->widget('TipsMasterData',array('type'=>'admin'));
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        if(!empty($modRincian[0]->tindakansudahbayar_id)){//sudah bayar / lunas
            $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/rincianKasirSudahBayarPrint');
        }else{
            $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/rincianKasirBaruPrint');
        }
$idPendaftaran = $modPendaftaran->pendaftaran_id;
$idpembayaran = $modPendaftaran->pembayaranpelayanan_id;
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/&id=${idPendaftaran}&caraPrint="+caraPrint,"",'location=_new, width=1100px');
}
function printPDF(caraPrint)
{
    window.open("${urlPrint}&id=${idPendaftaran}&idpembayaran=${idpembayaran}&caraPrint="+caraPrint,"",'location=_new, width=1100px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);         
 } ?>
