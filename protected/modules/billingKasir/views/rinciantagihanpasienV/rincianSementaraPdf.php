<style>
    body{
        font-family: tahoma;
        font-size: 11px;
    }
    .info td{
        font-size: 11px;
    }
    table td {
        vertical-align: top;
    }
    .grid td{
        border-top: 0.5px solid;
        border-collapse: collapse;
        /*border:1px solid #000;*/
        font-size: 11px;
    }
    .grid th{
        border-collapse: collapse;
        font-size: 12px;
        font-family: tahoma;
        border-collapse: collapse;
        /*border-top: 0.5px solid;*/
    }
    .grid td,th{
        padding: 5px;
    } 
</style>
<div style="float: left;width: 50%">
    <table width="100%" cellpadding="0" cellspacing="0" border="" class="info" style="page-break-inside: avoid;">
    
<!--    DITAMPILKAN DI MPDF NYA     -->
        <tr>
            <td width="100">No. RM / Reg</td>
            <td width="35%"> :
                <?php echo CHtml::encode($modPendaftaran->pasien->no_rekam_medik); ?> / 
                <?php echo CHtml::encode($modPendaftaran->no_pendaftaran); ?>            
            </td>
        </tr>
        <tr>
            <td width="100">Nama Pasien</td>
            <td width="35%">: <?php echo CHtml::encode($modPendaftaran->pasien->nama_pasien); ?></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:
                <?php echo CHtml::encode($modPendaftaran->pasien->jeniskelamin); ?>
            </td>
        </tr>
         <tr>
            <td>Umur</td>
            <td>:
                <?php echo CHtml::encode($modPendaftaran->umur); ?>
            </td>
        </tr>
        <tr>
            <td>Unit Pelayanan</td>
            <td>: 
                <?php echo CHtml::encode($modPendaftaran->instalasi->instalasi_nama); ?>
            </td>
        </tr>
        <tr>
            <td>Dokter Pemeriksa</td>
            <td>: 
                <?php echo CHtml::encode($modPendaftaran->dokter->nama_pegawai); ?>
            </td>
        </tr> 
        <tr>
            <td width="100">Tgl Perawatan</td>
            <td> : 
                <?php 
                $format = new CustomFormat();
                if (empty($modPasienpulang->tglpasienpulang)){
                   // echo CHtml::encode(date("d M Y",strtotime($modPendaftaran->tgl_pendaftaran)));
                    echo substr($modPendaftaran->tgl_pendaftaran, 0,11);
                }else {
                    // echo CHtml::encode(date("d M Y",strtotime($modPendaftaran->tgl_pendaftaran))." - ".date("d M Y",strtotime($modPasienpulang->tglpasienpulang)));           
                    echo substr($modPendaftaran->tgl_pendaftaran, 0,11)." - ".substr($modPasienpulang->tglpasienpulang,0,11);
                }
              ?> 
            </td>
        </tr>  
    </table>
</div>
<div style="float: right;width:48%;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="info" style="page-break-inside: avoid;">
        <tr>
            <td>Nama PJP</td>
            <td width="160"> :
                <?php
                    if(strlen($modPendaftaran->penanggungjawab_id) > 0)
                    {
                        echo CHtml::encode($modPendaftaran->penanggungJawab->nama_pj);
                    }else{
                        echo CHtml::encode($modPendaftaran->pasien->nama_pasien);
                    }
                ?>
            </td>

        </tr>

        <tr>
            <td>Alamat</td>
            <td>: <?php echo CHtml::encode($modPendaftaran->pasien->alamat_pasien);
                if(strlen($modPendaftaran->pasien->rt)> 0){
                    echo '&nbsp; RT '.CHtml::encode($modPendaftaran->pasien->rt);
                }else{
                    echo '';
                }
                if(strlen($modPendaftaran->pasien->rw)> 0){
                    echo '&nbsp; RW '.CHtml::encode($modPendaftaran->pasien->rw); 
                }else{
                    echo '';
                }
                if(strlen($modPendaftaran->pasien->kelurahan)> 0){
                    echo '<br>'.'&nbsp; Kel'.' '.CHtml::encode($modPendaftaran->pasien->kelurahan); 
                }else{
                    echo '';
                }
                if(strlen($modPendaftaran->pasien->kecamatan)> 0){
                    echo '<br>'.'&nbsp; Kac.'.' '.CHtml::encode($modPendaftaran->pasien->kecamatan); 
                }else{
                    echo '';
                }
                 ?>
            </td>
        </tr>

        <tr>
            <td>Nomor Telepon</td>
            <td width="160"> :
                <?php
                    if(strlen($modPendaftaran->pasien->no_telepon_pasien) > 0)
                    {
                        echo CHtml::encode($modPendaftaran->pasien->no_telepon_pasien).' /';
                    }else{
                        echo '- /';
                    }
                     if(strlen($modPendaftaran->pasien->no_mobile_pasien) > 0)
                    {
                        echo CHtml::encode($modPendaftaran->pasien->no_mobile_pasien);
                    }else{
                        echo '-';
                    }
                ?>
            </td>
        </tr>

        <tr>
                <td>   
                        <label class='control-label'>
                            Dokter Perujuk
                        </label></td>
                        <td>: 
                        <?php
                            if(strlen($modPendaftaran->rujukan->nama_perujuk)> 0)
                            {
                                echo CHtml::encode($modPendaftaran->rujukan->nama_perujuk);
                            }else{
                                echo '-';
                            }
                        ?>
                </td>
        </tr>       
         <tr>
            <td>Perusahaan Penjamin</td>
            <td> : <?php echo CHtml::encode($modRincian[0]->penjamin_nama); ?></td>
        </tr> 
    </table>
</div>
<div style="clear: both"></div>
<!-- <div style="float: left;width:48%;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="info" style="page-break-inside: avoid;">
        <tr>
            <td width="100">Tgl Perawatan</td>
            <td> : <?php //echo CHtml::encode(date("d-m-Y",strtotime($modRincian[0]->tgl_tindakan))." - ".date("d-m-Y",strtotime($modRincian[count($modRincian)-1]->tgl_tindakan))); ?></td>
        </tr>
    </table>    
</div>
 --><!-- <div style="float: right;width:48%;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="info" style="page-break-inside: avoid;">
        <tr>
            <td>Perusahaan Penjamin</td>
            <td width="160"> : <?php //echo CHtml::encode($modRincian[0]->penjamin_nama); ?></td>
        </tr>
    </table>
</div> -->
<div style="clear: both"></div>
<br>
<div align="center" style="text-align: center;">
    <center><b><?php echo strtoupper($data['judulPrint']); ?></b></center>
</div>
    <?php
        $format = new CustomFormat();
        $totalbiayaadminfarmasi = 0;
        $row = array();
        foreach($modRincian as $i=>$val)
        {
            //PEMBULATAN KEATAS HARGA SATUAN DESIMAL 2 ANGKA
            $val->tarif_satuan = round($val->tarif_satuan, 2, PHP_ROUND_HALF_UP);
                    
            $ruangan_id = $val->ruangan_id;
            $row[$ruangan_id]['nama'] = $val->ruangan_nama;
//            if($val->instalasi_id == Params::INSTALASI_ID_LAB || $val->instalasi_id == Params::INSTALASI_ID_RAD){
//                $row[$ruangan_id]['nama'] = "LABORATORIUM & RADIOLOGI";
//            }
            if($val->instalasi_id == Params::INSTALASI_ID_LAB){
                $row[$ruangan_id]['nama'] = "LABORATORIUM";
            }
            if($val->instalasi_id == Params::INSTALASI_ID_RAD){
                $row[$ruangan_id]['nama'] = "RADIOLOGI";
            }
            $row[$ruangan_id]['ruangan_id'] = $val->ruangan_id;
            $row[$ruangan_id]['pendaftaran_id'] = $val->pendaftaran_id;
            $row[$ruangan_id]['kategori'][$i]['ruangan_id'] = $val->ruangan_id;
            $row[$ruangan_id]['kategori'][$i]['nama_pegawai'] = $val->nama_pegawai;
            $row[$ruangan_id]['kategori'][$i]['tindakanpelayanan_id'] = $val->tindakanpelayanan_id;
            $row[$ruangan_id]['kategori'][$i]['daftartindakan_nama'] = $val->daftartindakan_nama;
            // $row[$ruangan_id]['kategori'][$i]['tgl_tindakan'] = date('d-m-Y',  strtotime($val->tgl_tindakan));
            $row[$ruangan_id]['kategori'][$i]['tgl_tindakan'] = substr($val->tgl_tindakan,0,11);
            $row[$ruangan_id]['kategori'][$i]['kelas'] = (strtolower($val->kelaspelayanan_nama) == 'tanpa kelas') ? "-" : $val->kelaspelayanan_nama;
            $row[$ruangan_id]['kategori'][$i]['harga'] = $val->tarif_satuan;
            $row[$ruangan_id]['kategori'][$i]['qty'] = $val->qty_tindakan;
            $row[$ruangan_id]['kategori'][$i]['total'] = ($row[$ruangan_id]['kategori'][$i]['harga'] * $row[$ruangan_id]['kategori'][$i]['qty']);
            $row[$ruangan_id]['kategori'][$i]['harga_dokter'] = (isset($val->tarif_medis) ? $val->tarif_medis : 0);
            $row[$ruangan_id]['kategori'][$i]['total_dokter'] = (isset($val->tarif_medis) ? ($val->qty_tindakan * $val->tarif_medis) : 0);
            $row[$ruangan_id]['kategori'][$i]['subsidiasuransi_tindakan'] = (isset($val->subsidiasuransi_tindakan) ? ($val->subsidiasuransi_tindakan) : 0);
            $row[$ruangan_id]['kategori'][$i]['subsidipemerintah_tindakan'] = (isset($val->subsidipemerintah_tindakan) ? ($val->subsidipemerintah_tindakan) : 0);
            $row[$ruangan_id]['kategori'][$i]['subsisidirumahsakit_tindakan'] = (isset($val->subsisidirumahsakit_tindakan) ? ($val->subsisidirumahsakit_tindakan) : 0);
            $row[$ruangan_id]['kategori'][$i]['iurbiaya_tindakan'] = (isset($val->iurbiaya_tindakan) ? ($val->iurbiaya_tindakan) : 0);
            $obatalkes = ObatalkespasienT::model()->findByPk($val->tindakanpelayanan_id);
            $row[$ruangan_id]['kategori'][$i]['noresep'] = $obatalkes->penjualanresep->noresep;
            // $row[$ruangan_id]['kategori'][$i]['tgltransaksi'] = date('d-m-Y',strtotime($obatalkes->penjualanresep->tglpenjualan));
            $row[$ruangan_id]['kategori'][$i]['tgltransaksi'] = substr($obatalkes->penjualanresep->tglpenjualan,0,11);

            $row[$ruangan_id]['kategori'][$i]['jenisobatalkes'] = $obatalkes->obatalkes->jenisobatalkes->jenisobatalkes_nama;
            $tindakan = TindakanpelayananT::model()->findByPk($val->tindakanpelayanan_id);
            if(!empty($tindakan->detailhasilpemeriksaanlab_id)){
                $row[$ruangan_id]['kategori'][$i]['tglpenunjang'] = date('d M Y',strtotime($tindakan->detailhasilpemeriksaanlab->update_time));
                // $row[$ruangan_id]['kategori'][$i]['tglpenunjang'] = $format->formatDateMediumForUserDMY(substr($tindakan->detailhasilpemeriksaanlab->update_time,0,11));
                
            }else if(!empty($tindakan->hasilpemeriksaanrad_id)){
                // $row[$ruangan_id]['kategori'][$i]['tglpenunjang'] = date('d-m-Y',strtotime($tindakan->hasilpemeriksaanrad->update_time));
                $row[$ruangan_id]['kategori'][$i]['tglpenunjang'] = substr($tindakan->hasilpemeriksaanrad->update_time,0,11);

            }else{
                $row[$ruangan_id]['kategori'][$i]['tglpenunjang'] = date('d M Y',strtotime($tindakan->pasienmasukpenunjang->tglmasukpenunjang));
                // $row[$ruangan_id]['kategori'][$i]['tglpenunjang'] = $format->formatDateMediumForUserDMY(substr($tindakan->pasienmasukpenunjang->update_time,0,11));

            }
            $row[$ruangan_id]['kategori'][$i]['nopenunjang'] = $tindakan->pasienmasukpenunjang->no_masukpenunjang;
            //Total biaya racik dll
            $totalbiayaadminfarmasi += ($val->biayaadministrasi + $val->biayaservice + $val->biayakonseling) ;
        }
    ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid">
    <thead>
        <tr>
            <th width="10">&nbsp;</th>
            <th >&nbsp;</th>
            <th width="80">Kelas</th>
            <th width="80">Harga (Rp)</th>
            <th width="50">Banyak</th>
            <th width="80">Total (Rp)</th>
        </tr>
    </thead>
</table>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid">
    <tbody>
        <?php
            $icek = 0; //dipakai untuk mengatur tampilan pertama kolom tabel sesuai ukurannya dengan yg bawah
            $cols = '';
            $total_biaya = 0;
            $iurBiaya = 0;
            $subsidiAsuransi = 0;
            $subsidiPemerintah = 0;
            $subsidiRumahSakit = 0;
//            $tampilHeaderLabRad = true;
            $tampilHeaderLab = true;
            $tampilHeaderRad = true;
//            $totalLabRad = 0;
            $totalLab = 0;
            $totalRad = 0;
//            $tampilTotalLabRad = true;
            $tampilTotalLab = true;
            $tampilTotalRad = true;
            $jenisobatalkes_temp = '';
            foreach($row as $key=>$values)
            {
                $icek++;
                $modRuangan = RuanganM::model()->findByPK($key); 
                if($modRuangan->instalasi_id == Params::INSTALASI_ID_RI)
                {
                    $modPasienAdmisi = PasienadmisiT::model()->findByAttributes(array(
                        'pendaftaran_id'=>$values['pendaftaran_id'],
                    ));
//                    $modKamarRuangan = KamarruanganM::model()->findByPk($modPasienAdmisi->kamarruangan_id);
                    $modKamarRuangan = MasukkamarT::model()->findByAttributes(array('pasienadmisi_id'=>$modPasienAdmisi->pasienadmisi_id, 'ruangan_id'=>$values['ruangan_id']));
		    if($icek==1){
                        $cols .= '<tr style="page-break-inside: avoid;">';
                    }else{
                        $cols .= '</tbody>
                                </table>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid">
                                <tbody>
                                <tr style="page-break-inside: avoid;">';
                    }
                    
                    $cols .= '<td colspan=6><b>'.strtoupper($values['nama']) .' Kamar ' . $modKamarRuangan->kamarruangan->kamarruangan_nokamar . ' Bed ' . $modKamarRuangan->kamarruangan->kamarruangan_nobed .'</b></td>';
                    $cols .= '</tr>';
                }else{
                    if($icek==1){
                        $cols .= '<tr style="page-break-inside: avoid;">';
                    }else{
                        $cols .= '</tbody>
                                </table>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid">
                                <tbody>
                                <tr style="page-break-inside: avoid;">';
                    }
                    if($modRuangan->ruangan_id == Params::RUANGAN_ID_LAB){
                        if($tampilHeaderLab == true){
                            $cols .= '<td colspan=6><b>'.strtoupper($values['nama']) .'</b></td>';
                            $cols .= '</tr>';
                            $tampilHeaderLab = false; //tampilkan header sekali
                        }
                    }else if($modRuangan->ruangan_id == Params::RUANGAN_ID_RAD){
                        if($tampilHeaderRad == true){
                            $cols .= '<td colspan=6><b>'.strtoupper($values['nama']) .'</b></td>';
                            $cols .= '</tr>';
                            $tampilHeaderRad = false; //tampilkan header sekali
                        }
                    }else{
                        $cols .= '<td colspan=6><b>'.strtoupper($values['nama']) .'</b></td>';
                        $cols .= '</tr>';
                    }
                }
                
                $col = '';
                $tampilAdminFarmasi = true;
                $total = 0;
                $tempAdminFarmasi = 0;
                foreach($values['kategori'] as $x => $val)
                {
                    if(
                        $values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_DEPO || 
                        $values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_RJ || 
                        $values['ruangan_id'] == Params::RUANGAN_ID_LAB ||
                        $values['ruangan_id'] == Params::RUANGAN_ID_RAD
                    )
                    {
                        if($values['ruangan_id'] == Params::RUANGAN_ID_LAB){ //FORMAT LAB
                            if(!empty($val['nopenunjang'])){
                                $col .= '<tr style="page-break-inside: avoid;">';
                                $col .= '<td width="130" style="padding-left:10">'.$values['kategori'][$x]['tglpenunjang'].' /'.$val['nopenunjang'].'</td>';
                                $col .= '<td>'.$val['daftartindakan_nama'].'</td>';
                                $col .= '<td width="80" style="text-align:center;">'. $val['kelas'] .'</td>';
                                $col .= '<td width="80" align="right" class="uang"></td>'; //merge tarif dokter '. number_format($values['kategori'][$x]['harga'] + $values['kategori'][$x]['harga_dokter'],0,'','.') .'
                                $col .= '<td width="50" align="right" class="uang"></td>'; //$values['kategori'][$x]['qty']
                                $col .= '<td width="80" align="right" class="uang">'. number_format($values['kategori'][$x]['total'],2,',','.') .'</td>';
                                $col .= '</tr>';
                                $totalLab += ($values['kategori'][$x]['total']);
                            }
                        }else if($values['ruangan_id'] == Params::RUANGAN_ID_RAD){
                            if(!empty($val['nopenunjang'])){
                                $col .= '<tr style="page-break-inside: avoid;">';
                                $col .= '<td width="130" style="padding-left:10">'.$values['kategori'][$x]['tglpenunjang'].' /'.$val['nopenunjang'].'</td>';
                                $col .= '<td>'.$val['daftartindakan_nama'].'</td>';
                                $col .= '<td width="80" style="text-align:center;">'. $val['kelas'] .'</td>';
                                $col .= '<td width="80" align="right" class="uang"></td>'; //merge tarif dokter '. number_format($values['kategori'][$x]['harga'] + $values['kategori'][$x]['harga_dokter'],0,'','.') .'
                                $col .= '<td width="50" align="right" class="uang"></td>'; //$values['kategori'][$x]['qty']
                                $col .= '<td width="80" align="right" class="uang">'. number_format($values['kategori'][$x]['total'],2,',','.') .'</td>';
                                $col .= '</tr>';
                                $totalRad += ($values['kategori'][$x]['total']);
                            }
                        }else{ //FORMAT APOTEK
                            if($jenisobatalkes_temp != $val['jenisobatalkes']){
                                $col .= '<tr>';
                                $col .= '<td colspan=6><b>'.$val['jenisobatalkes'].'</b></td>';
                                $col .= '</tr>';
                                $jenisobatalkes_temp = $val['jenisobatalkes'];
                            }
                                    
                            if($totalbiayaadminfarmasi > 0 && $tampilAdminFarmasi == true){
                                $col .= '<tr>';
                                $col .= '<td width="10">&nbsp;</td>';
                                $col .= '<td >YANFAR</td>';
                                $col .= '<td width="80" style="text-align:center;">'. $val['kelas'] .'</td>';
                                $col .= '<td width="80" style="text-align:right;">'.number_format($totalbiayaadminfarmasi,2,',','.').'</td>';
                                $col .= '<td width="5" style="text-align:right;">1</td>';
                                $col .= '<td width="80" style="text-align:right;">'.number_format($totalbiayaadminfarmasi,2,',','.').'</td>';
                                $col .= '</tr>';
                                $tampilAdminFarmasi = false;
                                $tempAdminFarmasi = $totalbiayaadminfarmasi;
                            }
                            
                            $col .= '<tr style="page-break-inside: avoid;">';
                            $col .= '<td width="130" style="padding-left:10">'.$values['kategori'][$x]['tgltransaksi'].' /'.$values['kategori'][$x]['noresep'].'</td>';
                            $col .= '<td>'.$val['daftartindakan_nama'].'</td>';
                            $col .= '<td width="80" style="text-align:center;">'. $val['kelas'] .'</td>';
                            $col .= '<td width="80" align="right" class="uang">'.number_format($val['harga'],2,',','.').'</td>'; 
//                            $col .= '<td width="50" align="right" class="uang">'.number_format($val['qty'],2,',','.').'</td>';
                            $col .= '<td width="50" align="right" class="uang">'.($val['qty']).'</td>';
                            $col .= '<td width="80" align="right" class="uang">'. number_format($val['total'],2,',','.') .'</td>';
                            $col .= '</tr>';
                            
                        }
                    }else{ //FORMAT RI/RJ/RD
                        $col .= '<tr style="page-break-inside: avoid;">';
                        $col .= '<td width="70">'.$val['tgl_tindakan'].'</td>';
                        $col .= '<td >'. $val['daftartindakan_nama'] .'</td>';
                        $col .= '<td width="80" style="text-align:center;">'. $val['kelas'] .'</td>';
                        $col .= '<td width="80" align="right" class="uang">'. number_format($val['harga'],2,',','.') .'</td>'; //merge tarif dokter
                        $col .= '<td width="50" align="right" class="uang">'. $val['qty'] .'</td>';
                        $col .= '<td width="80" align="right" class="uang">'. number_format($val['total'],2,',','.') .'</td>';
                        $col .= '</tr>';

                        $tempAdminFarmasi = 0;
                        if(strlen($val['nama_pegawai']) > 0)
                        {
                            if(strtoupper($values['nama']) != 'PENDAFTARAN'){
                                if($values['ruangan_id'] == Params::RUANGAN_ID_IBS || $values['ruangan_id'] == Params::RUANGAN_ID_GIZI){
                                    //hide dokter untuk IBS dan Gizi
                                }else{
                                    $col .= '<tr>';
                                    $col .= '<td>&nbsp;</td>';
                                    $col .= '<td>'. $val['nama_pegawai'] .'</td>';
                                    $col .= '<td style="text-align:center;">'. $val['kelas'] .'</td>';
        //                                KARNA tarif_satuan SUDAH TERMASUK tarif_medis, dll
        //                                $col .= '<td class="uang" align="right">'. number_format($val['harga_dokter'],0,'','.') .'</td>';
        //                                $col .= '<td class="uang" align="right">'. $val['qty'] .'</td>';
        //                                $col .= '<td class="uang" align="right">'. number_format($val['total_dokter'],0,'','.') .'</td>';
                                    $col .= '<td class="uang" align="right"></td>';
                                    $col .= '<td class="uang" align="right"></td>';
                                    $col .= '<td class="uang" align="right"></td>';
                                    $col .= '</tr>'; 
                                }
                            }   
                        }
                    }
                    $total += ($val['qty'] * $val['harga']);//$val['total'] + $val['total_dokter'];
                    $subsidiAsuransi += $val['subsidiasuransi_tindakan'];
                    $subsidiPemerintah += $val['subsidipemerintah_tindakan'];
                    $subsidiRumahSakit += $val['subsisidirumahsakit_tindakan'];
                    $iurBiaya += $val['iurbiaya_tindakan'];
                }
                
//              JANGAN DIBULATKAN >>  $total = round($total + $tempAdminFarmasi);
                $total = $total + $tempAdminFarmasi;
                if(($values['ruangan_id'] == Params::RUANGAN_ID_LAB)){
                    //Total Lab Rad Footer
//                    if(($row[$key]['ruangan_id'] == Params::RUANGAN_ID_LAB && ($row[$key+1]['ruangan_id'] != Params::RUANGAN_ID_LAB && $row[$key+1]['ruangan_id'] != Params::RUANGAN_ID_RAD)) ||
//                        ($row[$key]['ruangan_id'] == Params::RUANGAN_ID_RAD && $row[$key+1]['ruangan_id'] != Params::RUANGAN_ID_RAD)){
                        if($tampilTotalLab == true){
                            $col .= '<tr>';
                            $col .= '<td colspan=5 class="total">Total Biaya :</td>';
                            $col .= '<td class="total" align="right"><b>'. number_format($totalLab,2,',','.') .'</b></td>';
                            $col .= '</tr>';
                            $tampilTotalLab = false;
                        }
//                    }
                }else if($values['ruangan_id'] == Params::RUANGAN_ID_RAD){
//                    if(($row[$key]['ruangan_id'] == Params::RUANGAN_ID_LAB && ($row[$key+1]['ruangan_id'] != Params::RUANGAN_ID_LAB && $row[$key+1]['ruangan_id'] != Params::RUANGAN_ID_RAD)) ||
//                        ($row[$key]['ruangan_id'] == Params::RUANGAN_ID_RAD && $row[$key+1]['ruangan_id'] != Params::RUANGAN_ID_RAD)){
                        if($tampilTotalRad == true){
                            $col .= '<tr>';
                            $col .= '<td colspan=5 class="total">Total Biaya :</td>';
                            $col .= '<td class="total" align="right"><b>'. number_format($totalRad,2,',','.') .'</b></td>';
                            $col .= '</tr>';
                            $tampilTotalRad = false;
                        }
//                    }
                }else{
                    $col .= '<tr>';
                    $col .= '<td colspan=5 class="total">Total Biaya :</td>';
                    $col .= '<td class="total" align="right"><b>'. number_format($total,2,',','.') .'</b></td>';
                    $col .= '</tr>';
                }
                $cols .= $col;
                $total_biaya += $total;
            }
            echo($cols);
            //HARUS DARI DATABASE >> $iurBiaya = $total_biaya - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit);  //karena $iurBiaya yang diambil di tindakanpelayanan_t sering tidak sama dengan total biaya - subsidi 
    ?>
    </tbody>
</table>
<br>
<table width="100%" cellpadding="0" cellspacing="0" border="0" style="page-break-inside: avoid;">
    <tr>
        <td width="50%" align="center" valign="top">
            <table width="100%">
                <tr>
                    <td align="center">
                        <div>Tasikmalaya, <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-mm-dd hh:mm:ss')); ?></div>
                        <div>Petugas RSJK</div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div>
                            <?php echo $data['nama_pegawai']; ?>
                        </div>
                    </td>
                </tr>
            </table>            
        </td>
        <td width="50%" align="right" valign="top">
            <table width="100%" style="text-align: right;">
                <tr>
                    <td><b>Total Biaya</b></td>
                    <td width="3%">:</td>
                    <td class="totalSeluruh"><?php echo number_format($total_biaya,2,',','.'); ?></td>
                </tr>
                <?php
                    $total_asuransi = 0;
                    $jml_penjamin = 0;
                    if (count($modPenjamins) > 0){
                        foreach ($modPenjamins as $hitung){
                            $total_asuransi += $hitung->jmlpiutangasuransi;
                            $jml_penjamin++;
                        }
                        if ($total_asuransi > $total_biaya && $jml_penjamin <= 2){                                
                            foreach ($modPenjamins as $i=>$penjamin){
                                echo '<tr>';
                                    echo '<td><b>Subsidi '.$penjamin->penjamin->penjamin_nama.'</b></td>';
                                    echo '<td width="3%">:</td>';
                                    if ($i == 0){
                                        if ($modPenjamins[0]->jmlpiutangasuransi > $total_biaya){
                                            $asuransi_pertama = $total_biaya;
                                            $asuransi_kedua = 0;
                                        } else {
                                            $asuransi_pertama = $penjamin->jmlpiutangasuransi;
                                            $asuransi_kedua = $total_biaya - $modPenjamins[0]->jmlpiutangasuransi;
                                        }
                                        echo '<td class="totalSeluruh">'.number_format($asuransi_pertama,2,',','.').'</td>';
                                    } else {
                                        echo '<td class="totalSeluruh">'.number_format($asuransi_kedua,2,',','.').'</td>';
                                    }                                        
                                echo '</tr>';                                
                            }
                        } else {
                            foreach ($modPenjamins as $penjamin){
                                echo '<tr>';
                                    echo '<td><b>Subsidi '.$penjamin->penjamin->penjamin_nama.'</b></td>';
                                    echo '<td width="3%">:</td>';
                                    echo '<td class="totalSeluruh">'.number_format($penjamin->jmlpiutangasuransi,2,',','.').'</td>';
                                echo '</tr>';                                
                            }
                        }
                    } else {
                        echo '<tr>';
                            echo '<td><b>Subsidi Asuransi</b></td>';
                            echo '<td width="3%">:</td>';
                            echo '<td class="totalSeluruh">'.number_format($subsidiAsuransi,2,',','.').'</td>';
                        echo '</tr>';
                    }
                ?>
                <!-- Membuat ambigu user dan pasien
                <tr>
                    <td><b>Subsidi Pemerintah</b></td>
                    <td width="3%">:</td>
                    <td class="totalSeluruh"><?php echo number_format($subsidiPemerintah,2,',','.'); ?></td>
                </tr>
                -->
                <tr>
                    <td><b>Subsidi Rumah Sakit</b></td>
                    <td width="3%">:</td>
                    <td class="totalSeluruh"><?php echo number_format($subsidiRumahSakit,2,',','.'); ?></td>
                </tr>
<!--                <tr>
                    <td><b>Iur Biaya</b></td>
                    <td width="3%">:</td>
                    <td class="totalSeluruh"><?php //echo number_format($iurBiaya,0,'','.'); ?></td>
                </tr>-->
                <tr>
                    <td><b>Deposit</b></td>
                    <td>:</td>
                    <td class="totalSeluruh"><?php echo number_format($data['uang_cicilan'],2,',','.'); ?></td>
                </tr>
                <tr>
                    <td><b>Tanggungan Pasien</b></td>
                    <td>:</td>
                    <td class="totalSeluruh">
                        <?php
                        
                            $tanggungan_pasien = $total_biaya-$subsidiRumahSakit-$data['uang_cicilan']-$total_asuransi;
                            
//                            if($data['uang_cicilan'] > 0){
//                                 if(($subsidiAsuransi+$subsidiPemerintah+$subsidiRumahSakit+$data['uang_cicilan']) >= $total_biaya)
//                                 {
//                                     $kembalian=0;
//                                 }else {
//                                    $kembalian = $kembalian - $data['uang_cicilan'];
//                                 }                                            
//                            }
                                                            
                            echo number_format((($tanggungan_pasien > 0)?$tanggungan_pasien:0),2,',','.');	
                        ?>
                    </td>
                </tr>
            </table>
            <div id="cetakan_jum" style="margin-top: 40px;font-size: 11px"></div>
        </td>
    </tr>
</table>
<script type="text/javascript">

    function insertCetakan()
    {
        var params = {jenis_cetakan:'<?=Params::jenisCetakan($data['jenis_cetakan'])?>',pendaftaran_id:'<?=$modPendaftaran->pendaftaran_id?>'};
        
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/updateJumlahCetakan');?>", {id:params},
            function(data)
            {
                if(data.status == 'not')
                {
                    console.log('insert cetakan data error');
                }else{
                    $('#cetakan_jum').text('Cetakan Ke ' + data.jumlah);
                }
            }, "json"
        );
    }
    insertCetakan();

</script>