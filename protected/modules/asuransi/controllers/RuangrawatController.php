<?php
class RuangrawatController extends Controller{
	
	public $path_view = 'asuransi.views.ruangrawat.';
	
	public function actionIndex(){
		
		$this->render($this->path_view.'index',array(
			
		));
	}
	
	/**
	* set bpjs Interface
	*/
	public function actionBpjsInterface()
	{
		if(Yii::app()->getRequest()->getIsAjaxRequest()) {
			if(empty( $_GET['param'] ) OR $_GET['param'] === ''){
				die('param can\'not empty value');
			}else{
				$param = $_GET['param'];
			}

 //                if(empty( $_GET['server'] ) OR $_GET['server'] === ''){
 //                    
 //                }else{
 //                    $server = 'http://'.$_GET['server'];
 //                }

			$bpjs = new Bpjs();

			switch ($param) {
				case '1':
					$start = 1;
					$limit = 10;
					print_r( $bpjs->search_ruangrawat("",$start, $limit) );
					break;
				default:
					die('error number, please check your parameter option');
					break;
			}
			Yii::app()->end();
		}
	}
	
	/**
	* @param type $faskes - katakunci
	*/
	public function actionPrintData($faskes = null)
	{
		$this->layout='//layouts/printWindows';
		$format = new CustomFormat;

		$judul_print = 'DATA RUANG RAWAT';
		$this->render($this->path_view.'print', array(
			'format'=>$format,
			'judul_print'=>$judul_print,
		));
	} 
	
    public function actionSetFormRuangrawat()
    {
        if(Yii::app()->request->isAjaxRequest) { 
            $ruangrawatList = $_POST['ruangrawatList'];
			$form = '';
			$pesan = '';
            if(count($ruangrawatList) > 0){
                foreach($ruangrawatList AS $i => $ruangrawat){
					$kode = $ruangrawat['kode'];
					$nama = $ruangrawat['nama'];
                    $form .= $this->renderPartial($this->path_view.'_rowDetail', array(
						'kode'=>$kode,
						'nama'=>$nama,
					), true);
                }
            }else{
                $pesan = "Data tidak ada!";
            }
            
            echo CJSON::encode(array('form'=>$form, 'pesan'=>$pesan));
            Yii::app()->end(); 
        }
    }
}