<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SuratRujukanController extends Controller{
    
    public $deleterujukan = false;
    
    public function actionIndex($id=null){
	$model = new ARRujukankeluarBpjsT;
        $modSep = new ARSepT;
        $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
        if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
            $model->create_user_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
        }else{
            $model->create_user_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
        }
        $model->no_rujukan = "-Otomatis-";
        $model->tgl_dirujuk = date('Y-m-d');
        $model->jenisfaskes = 1;
        
        if(!empty($id)){
            $model = ARRujukankeluarBpjsT::model()->findByPk($id);
            $modSep = ARSepT::model()->findByAttributes(array('sep_id'=>$model->sep_id));
        }
        
        if(isset($_POST['ARRujukankeluarBpjsT'])){
            try {
                $transaction = Yii::app()->db->beginTransaction();
                $model->attributes = $_POST['ARRujukankeluarBpjsT'];
                $sep_id = $_POST['sep_id'];
                $modSep = ARSepT::model()->findByPk($sep_id);
                
                $model->pendaftaran_id = $modSep->pendaftaran_id;
                $model->pasien_id = $modSep->pasien_id;
                $model->sep_id = $modSep->sep_id;
                $model->no_rujukan = $_POST['ARRujukankeluarBpjsT']['no_rujukan'];
                $model->ppk_dirujuk = $_POST['ARRujukankeluarBpjsT']['ppk_dirujuk'];
                $model->ppk_dirujuk_nama = $_POST['ARRujukankeluarBpjsT']['ppk_dirujuk_nama'];
                $model->poli_rujukan_nama = $_POST['ARRujukankeluarBpjsT']['poli_rujukan_nama'];
                $model->jenisfaskes = $_POST['ARRujukankeluarBpjsT']['jenisfaskes'];
                $model->create_time = date('Y-m-d H:i:s');
                $model->create_pemakai_id = Yii::app()->user->id;

                if($model->validate()){
                    if($model->save()){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success', "Data rujukan berhasil disimpan");
                        $this->redirect(array('index', 'id' => $model->rujukankeluar_bpjs_id, 'sukses' => 1));
                    }else{
                        $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data rujukan gagal disimpan ! ");
                    }
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data rujukan gagal disimpan ! " . MyExceptionMessage::getMessage($e, true));
            }
        }
        $this->render('index',array(
            'model'=>$model,
            'modSep'=>$modSep,
        ));
    }
    
    public function actionUpdate($id=null){
        $this->layout = '//layouts/frameDialog';
	$model = new RujukankeluarBpjsT;
        $modSep = new ARSepT;
        $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
        if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
            $model->update_user_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
        }else{
            $model->update_user_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
        }
        $model->no_rujukan = "-Otomatis-";
        $model->tgl_dirujuk = date('Y-m-d');
        $model->jenisfaskes = 1;
        
        if(!empty($id)){
            $model = RujukankeluarBpjsT::model()->findByPk($id);
            $modSep = ARSepT::model()->findByAttributes(array('sep_id'=>$model->sep_id));
        }
        
        if(isset($_POST['RujukankeluarBpjsT'])){
            try {
                $transaction = Yii::app()->db->beginTransaction();
                $model->attributes = $_POST['RujukankeluarBpjsT'];
                
                $model->pendaftaran_id = $modSep->pendaftaran_id;
                $model->pasien_id = $modSep->pasien_id;
                $model->sep_id = $modSep->sep_id;
                $model->ppk_dirujuk = $_POST['RujukankeluarBpjsT']['ppk_dirujuk'];
                $model->ppk_dirujuk_nama = $_POST['RujukankeluarBpjsT']['ppk_dirujuk_nama'];
                $model->poli_rujukan_nama = $_POST['RujukankeluarBpjsT']['poli_rujukan_nama'];
                $model->jenisfaskes = $_POST['RujukankeluarBpjsT']['jenisfaskes'];
                $model->update_time = date('Y-m-d H:i:s');
                $model->update_loginpemakai_id = Yii::app()->user->id;

                if($model->validate()){
                    if($model->save()){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success', "Data rujukan berhasil disimpan");
                        $this->redirect(array('update', 'id' => $model->rujukankeluar_bpjs_id, 'sukses' => 1));
                    }else{
                        $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data rujukan gagal disimpan ! ");
                    }
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data rujukan gagal disimpan ! " . MyExceptionMessage::getMessage($e, true));
            }
        }
        $this->render('update',array(
            'model'=>$model,
            'modSep'=>$modSep,
        ));
    }
    
    public function actionAdmin() {
        $model = new ARRujukankeluarBpjsT;
        $model->unsetAttributes();  // clear any default values
        $model->tgl_awal = date('Y-m-d');
        $model->tgl_akhir = date('Y-m-d');
        if (isset($_GET['ARRujukankeluarBpjsT'])) {
            $model->attributes = $_GET['ARRujukankeluarBpjsT'];
            $model->nosep = $_GET['ARRujukankeluarBpjsT']['nosep'];
            $model->nokartuasuransi = $_GET['ARRujukankeluarBpjsT']['nokartuasuransi'];
            $model->no_rekam_medik = $_GET['ARRujukankeluarBpjsT']['no_rekam_medik'];
            $model->nama_pasien = $_GET['ARRujukankeluarBpjsT']['nama_pasien'];
            $model->no_rujukan = $_GET['ARRujukankeluarBpjsT']['no_rujukan'];
            $model->tgl_awal = isset($_GET['ARRujukankeluarBpjsT']['tgl_awal']) ? ($_GET['ARRujukankeluarBpjsT']['tgl_awal']) : null;
            $model->tgl_akhir = isset($_GET['ARRujukankeluarBpjsT']['tgl_akhir']) ? ($_GET['ARRujukankeluarBpjsT']['tgl_akhir']) : null;
        }
        $this->render('admin', array(
            'model' => $model,
        ));
    }
        
    public function actionSetFormPoli() {
        if (Yii::app()->request->isAjaxRequest) {
            $poliList = $_POST['poliList'];
            $form = '';
            $pesan = '';
            if (count($poliList) > 0) {
                foreach ($poliList AS $i => $poli) {
                    $kdPoli = $poli['kode'];
                    $nmPoli = $poli['nama'];
                    $form .= 
                    "<tr>
                        <td>
                            <a class='btn-small' href='javascript:void(0);' onclick=\" $('#ARRujukankeluarBpjsT_poli_rujukan').val('".$kdPoli."');$('#ARRujukankeluarBpjsT_poli_rujukan_nama').val('".$nmPoli."');$('#dialogPoli').dialog('close'); \">
                            <i class='icon-check'></i></a>
                        </td>
                        <td>
                            <span id='kdPoli' name=[ii][kdPoli]'>".$kdPoli."</span>
                        </td>
                        <td>
                            <span id='nmPoli' name=[ii][nmPoli]'>".$nmPoli."</span>
                        </td>
                    </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }
    
    public function actionSetFormDiagnosa() {
        if (Yii::app()->request->isAjaxRequest) {
            $diagnosaList = $_POST['diagnosaList'];
            $form = '';
            $pesan = '';
            if (count($diagnosaList) > 0) {
                foreach ($diagnosaList AS $i => $diagnosa) {
                    $kddiagnosa = $diagnosa['kode'];
                    $nmdiagnosa = $diagnosa['nama'];
                    $form .= 
                    "<tr>
                        <td>
                            <a class='btn-small' href='javascript:void(0);' onclick=\" $('#ARRujukankeluarBpjsT_diagnosa_rujukan_kode').val('".$kddiagnosa."');$('#ARRujukankeluarBpjsT_diagnosa_rujukan_nama').val('".$nmdiagnosa."');$('#dialogDiagnosaBpjs').dialog('close'); \">
                            <i class='icon-check'></i></a>
                        </td>
                        <td>
                            <span id='kdPoli' name=[ii][kdPoli]'>".$kddiagnosa."</span>
                        </td>
                        <td>
                            <span id='nmPoli' name=[ii][nmPoli]'>".$nmdiagnosa."</span>
                        </td>
                    </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }
    
    public function actionSetFormFaskes() {
        if (Yii::app()->request->isAjaxRequest) {
            $faskesList = $_POST['faskesList'];
            $form = '';
            $pesan = '';
            if (count($faskesList) > 0) {
                foreach ($faskesList AS $i => $faskes) {
                    $kdfaskes = $faskes['kode'];
                    $nmfaskes = $faskes['nama'];
                    $form .= 
                    "<tr>
                        <td>
                            <a class='btn-small' href='javascript:void(0);' onclick=\"$('#ARRujukankeluarBpjsT_ppk_dirujuk').val('".$kdfaskes."');$('#ARRujukankeluarBpjsT_ppk_dirujuk_nama').val('".$nmfaskes."');$('#dialogPpk').dialog('close'); \">
                            <i class='icon-check'></i></a>
                        </td>
                        <td>
                            <span id='kdPoli' name=[ii][kdPoli]'>".$kdfaskes."</span>
                        </td>
                        <td>
                            <span id='nmPoli' name=[ii][nmPoli]'>".$nmfaskes."</span>
                        </td>
                    </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }
    
    /**
     * set bpjs Interface
     */
    public function actionBpjsInterface() {
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            if (empty($_GET['param']) OR $_GET['param'] === '') {
                die('param can\'not empty value');
            } else {
                $param = $_GET['param'];
            }
            $jenis_rujukan = isset($_GET['jenis_rujukan'])? $_GET['jenis_rujukan'] : 1;

            $bpjs = new Bpjs();

            switch ($param) {
                case '1':
                    $noSep = $_GET['noSep'];
                    $tglRujukan = date('Y-m-d', strtotime($_GET['tglRujukan']));
                    $ppkDirujuk = $_GET['ppkDirujuk'];
                    $jnsPelayanan = $_GET['jnsPelayanan'];
                    $catatan = $_GET['catatan'];
                    $diagRujukan = $_GET['diagRujukan'];
                    $tipeRujukan = $_GET['tipeRujukan'];
                    $poliRujukan = $_GET['poliRujukan'];
                    $user = $_GET['user'];
                    
                    print_r($bpjs->insert_rujukan_bpjs($noSep,$tglRujukan,$ppkDirujuk,$jnsPelayanan,$catatan,$diagRujukan,$tipeRujukan,$poliRujukan,$user));
                    break;
                case '2':
                    $noSep = $_GET['noSep'];
                    $tglRujukan = date('Y-m-d', strtotime($_GET['tglRujukan']));
                    $ppkDirujuk = $_GET['ppkDirujuk'];
                    $jnsPelayanan = $_GET['jnsPelayanan'];
                    $catatan = $_GET['catatan'];
                    $diagRujukan = $_GET['diagRujukan'];
                    $tipeRujukan = $_GET['tipeRujukan'];
                    $poliRujukan = $_GET['poliRujukan'];
                    $user = $_GET['user'];
                    $noRujukan = $_GET['noRujukan'];
                    
                    print_r($bpjs->update_rujukan_bpjs($noRujukan,$noSep,$tglRujukan,$ppkDirujuk,$jnsPelayanan,$catatan,$diagRujukan,$tipeRujukan,$poliRujukan,$user));
                    break;
                default:
                    die('error number, please check your parameter option');
                    break;
            }
            Yii::app()->end();
        }
    }
    
    public function actionPrintRujukan($id) {
        $this->layout = '//layouts/printWindows';
        $model = ARRujukankeluarBpjsT::model()->findByPk($id);
        $judul_print = 'SURAT RUJUKAN RUMAH SAKIT';
        $this->render('printRujukan', array(
            'judul_print' => $judul_print,
            'model' => $model,
        ));
    }
    
    public function actionSetKunjunganSEP(){
        if (Yii::app()->request->isAjaxRequest) {
            $sep_id = $_GET['sep_id'];
            $modSep = ARSepT::model()->findByPk($sep_id);
            
            $returnVal = array();
            $returnVal['sep_id'] = $modSep->sep_id;
            $returnVal['no_sep'] = $modSep->no_sep;
            $returnVal['no_pendaftaran'] = $modSep->pendaftaran->no_pendaftaran;
            $returnVal['no_rekam_medik'] = $modSep->pasien->no_rekam_medik;
            $returnVal['tgl_pendaftaran'] = $modSep->pendaftaran->tgl_pendaftaran;
            $returnVal['nama_pasien'] = $modSep->pasien->nama_pasien;
            $returnVal['tanggal_lahir'] = $modSep->pasien->tanggal_lahir;
            $returnVal['jeniskelamin'] = $modSep->pasien->jeniskelamin;
            $returnVal['alamat_pasien'] = $modSep->pasien->alamat_pasien;
            
            echo CJSON::encode($returnVal);
            Yii::app()->end();
        }
    }
    
    public function actionAutoCompleteSEP(){
        if (Yii::app()->request->isAjaxRequest) {
            $returnVal = array();
            $nosep = isset($_GET['no_sep']) ? $_GET['no_sep'] : null;

            $criteria = new CDbCriteria();
            $criteria->compare('LOWER(no_sep)', strtolower($nosep), true);
            $criteria->order = 'no_sep';
            $criteria->limit = 5;
            $models = ARSepT::model()->findAll($criteria);
            foreach ($models as $i => $model) {
                $attributes = $model->attributeNames();
                foreach ($attributes as $j => $attribute) {
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->no_sep . ' - ' . $model->pasien->nama_pasien  . " - " . $model->pasien->tanggal_lahir;
                $returnVal[$i]['value'] = $model->sep_id;
            }
            echo CJSON::encode($returnVal);
        } else
            throw new CHttpException(403, 'Tidak dapat mengurai data');
        Yii::app()->end();
    }
    
    /**
     * Menghapus data Rujukan
     */
    public function actionHapusRujukan($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $data['sukses'] = 0;
            $model = ARRujukankeluarBpjsT::model()->findByPk($id);
            $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
            if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
                $nama = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
            }else{
                $nama = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
            }
            $bpjs = new Bpjs();
            $transaction = Yii::app()->db->beginTransaction();
            $reqRujukan = json_decode($bpjs->delete_rujukan($model->no_rujukan, $nama), true);
            if ($reqRujukan['metaData']['code'] == 200) {
                if ($model->delete()) {
                    $this->deleterujukan = true;
                    $transaction->commit();
                }
            } else {
                $this->deleterujukan = false;
                $transaction->rollback();
            }

            if ($this->deleterujukan == false) {
                $data['status'] = 'Data gagal dihapus karena '.$reqRujukan['metaData']['message'];
            } else {
                $data['status'] = 'Data Rujukan berhasil dihapus';
            }

            echo CJSON::encode($data);
        }
    }
    
    public function actionPrint() {
        $model = new ARRujukankeluarBpjsT;
        $model->tgl_awal = date('Y-m-d');
        $model->tgl_akhir = date('Y-m-d');
        if (isset($_REQUEST['ARRujukankeluarBpjsT'])) {
            $model->attributes = $_REQUEST['ARRujukankeluarBpjsT'];
            $model->nosep = $_REQUEST['ARRujukankeluarBpjsT']['nosep'];
            $model->nokartuasuransi = $_REQUEST['ARRujukankeluarBpjsT']['nokartuasuransi'];
            $model->no_rekam_medik = $_REQUEST['ARRujukankeluarBpjsT']['no_rekam_medik'];
            $model->nama_pasien = $_REQUEST['ARRujukankeluarBpjsT']['nama_pasien'];
            $model->no_rujukan = $_REQUEST['ARRujukankeluarBpjsT']['no_rujukan'];
            $model->tgl_awal = isset($_REQUEST['ARRujukankeluarBpjsT']['tgl_awal']) ? ($_REQUEST['ARRujukankeluarBpjsT']['tgl_awal']) : null;
            $model->tgl_akhir = isset($_REQUEST['ARRujukankeluarBpjsT']['tgl_akhir']) ? ($_REQUEST['ARRujukankeluarBpjsT']['tgl_akhir']) : null;
        }
        
        $judulLaporan = 'Data Rujukan';
        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == 'PRINT') {
            $this->layout = '//layouts/printWindows';
            $this->render('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas'); //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas'); //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->mirrorMargins = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
}

