<?php

class SepController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $defaultAction = 'admin';
    public $septersimpan = false;
    public $updatesep = false;
    public $deletesep = false;
    public $bridgingsep = true;
    public $bridginglaporansep = true;
    public $laporanseptersimpan = true;
    public $succesSaveStok = true; //looping
    public $succesKembaliStok = true; //looping

    /**
     * Menampilkan detail data.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $model = $this->loadModel($id);
        $bpjs = new Bpjs();

        $this->render('viewSep', array(
            'model' => $model,
        ));
    }

    /**
     * Membuat dan menyimpan data baru.
     */
    public function actionCreate($id = null) {
        $status = '';
        $model = new ARSepT;
        $modInfoKunjungan = new ARInfokunjunganrsSepV;
        $model->ppkpelayanan = Yii::app()->user->getState('kode_ppk_bpjs');
        $model->ppkpelayanan_nama = Yii::app()->user->getState('nama_ppk_pelayanan');
        $model->tglsep = date('Y-m-d');
        $model->carabayar_id = 18;
        $model->jenisrujukan_kode_bpjs = 1;
        $model->polieksekutif = 0;
        $model->cob_bpjs = 0;
        $model->lakalantas_kode = 0;
        $model->cob_status = "TIDAK";
        $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
        if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
            $model->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
        }else{
            $model->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
        }
        
        if (!empty($id)) {
            $model = ARSepT::model()->findByPk($id);
            $modInfoKunjungan = ARInfokunjunganrsSepV::model()->findByAttributes(array('pendaftaran_id' => $model->pendaftaran_id));
        }
        if (isset($_POST['ARSepT'])) {
            
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes = $_POST['ARSepT'];
                $model = $this->simpanSep_baru($model, $_POST['ARSepT']);
               
                if ($model) {
                    if ($this->bridgingsep == false) {
                        $status = 'Data gagal disimpan karena koneksi server BPJS terputus! Silahkan hubungi admin SIMRS';
                    } else if ($this->septersimpan == false) {
                        $status = 'Data gagal disimpan karna kesalahan data / database!';
                    } else {
                        $status = 'Data SEP berhasil disimpan';
                    }
                    if ($this->septersimpan && $this->bridgingsep) {
                        $transaction->commit();
//                        Yii::app()->user->setFlash('success', $status);
                        $this->redirect(array('create', 'id' => $model->sep_id, 'sukses' => 1));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', $status);
                    }
                }
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data SEP gagal disimpan ! " . MyExceptionMessage::getMessage($e, true));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modInfoKunjungan' => $modInfoKunjungan,
        ));
    }

    /**
     * Melihat daftar data.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('ARSepT');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Pengaturan data.
     */
    public function actionAdmin() {
        $format = new CustomFormat();
        $model = new ARSepT;
        $model->unsetAttributes();  // clear any default values
        $model->tgl_awal = date('Y-m-d');
        $model->tgl_akhir = date('Y-m-d');
        if (isset($_GET['ARSepT'])) {
            $model->attributes = $_GET['ARSepT'];
            $model->tgl_awal = isset($_GET['ARSepT']['tgl_awal']) ? $_GET['ARSepT']['tgl_awal'] : null;
            $model->tgl_akhir = isset($_GET['ARSepT']['tgl_akhir']) ? $_GET['ARSepT']['tgl_akhir'] : null;
        }
        $this->render('admin_new', array(
            'model' => $model,
        ));
    }

    /**
     * Memanggil data dari model.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = ARSepT::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'assep-t-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Mencetak data
     */
    public function actionPrint() {
        $model = new ARSepT;
        $model->attributes = $_REQUEST['ARSepT'];
        $model->tgl_awal = $_REQUEST['ARSepT']['tgl_awal'];
        $model->tgl_akhir = $_REQUEST['ARSepT']['tgl_akhir'];
        $judulLaporan = 'Data SEP';
        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == 'PRINT') {
            $this->layout = '//layouts/printWindows';
            $this->render('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas'); //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas'); //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->mirrorMargins = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    /**
     * set bpjs Interface
     */
    public function actionBpjsInterface() {
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            if (empty($_GET['param']) OR $_GET['param'] === '') {
                die('param can\'not empty value');
            } else {
                $param = $_GET['param'];
            }
            $jenis_rujukan = isset($_GET['jenis_rujukan'])? $_GET['jenis_rujukan'] : 1;

            $bpjs = new Bpjs();

            switch ($param) {
                case '1':
                    $query = $_GET['query'];
                    print_r($bpjs->search_kartu($query));
                    break;
                case '2':
                    $query = $_GET['query'];
                    print_r($bpjs->search_nik($query));
                    break;
                case '3':
                    $query = $_GET['query'];
                    if($jenis_rujukan==1){
                        print_r($bpjs->search_rujukan_no_rujukan($query));
                    }else{
                        print_r($bpjs->search_rujukan_no_rujukan_rs($query));
                    }
                    break;
                case '4':
                    $query = $_GET['query'];
                    print_r($bpjs->search_rujukan_no_bpjs($query));
                    break;
                case '13':
                    $format = new CustomFormat();
                    $noMR = $_GET['noMR'];
                    $noKartu = $_GET['noKartu'];
                    $tglSep = date('Y-m-d', strtotime($_GET['tglSep']));
                    $ppkPelayanan = $_GET['ppkPelayanan'];
                    $jnsPelayanan = $_GET['jnsPelayanan'];
                    $klsRawat = $_GET['klsRawat'];
                    $asalRujukan = $_GET['asalRujukan'];
                    $tglRujukan = date('Y-m-d', strtotime($_GET['tglRujukan']));
                    $noRujukan = $_GET['noRujukan'];
                    $ppkRujukan = $_GET['ppkRujukan'];
                    $catatan = $_GET['catatan'];
                    $diagAwal = $_GET['diagAwal'];
                    $tujuan = $_GET['tujuan'];
                    $eksekutif = $_GET['eksekutif'];
                    $cob = $_GET['cob'];
                    $lakaLantas = $_GET['lakaLantas'];
                    $penjamin = $_GET['penjamin'];
                    $lokasiLaka = $_GET['lokasiLaka'];
                    $noTelp = $_GET['noTelp'];
                    $user = $_GET['user'];
                    
                    print_r($bpjs->create_sep_new($noKartu,$tglSep,$ppkPelayanan,$jnsPelayanan,$klsRawat,$noMR,$asalRujukan,$tglRujukan,$noRujukan,$ppkRujukan,$catatan,$diagAwal,$tujuan,$eksekutif,$cob,$lakaLantas,$penjamin,$lokasiLaka,$noTelp,$user));
                    break;
                case '14':
                    $format = new CustomFormat();
                    $noMR = $_GET['noMR'];
                    $noKartu = $_GET['noKartu'];
                    $tglSep = date('Y-m-d', strtotime($_GET['tglSep']));
                    $ppkPelayanan = $_GET['ppkPelayanan'];
                    $jnsPelayanan = $_GET['jnsPelayanan'];
                    $klsRawat = $_GET['klsRawat'];
                    $asalRujukan = $_GET['asalRujukan'];
                    $tglRujukan = date('Y-m-d', strtotime($_GET['tglRujukan']));
                    $noRujukan = $_GET['noRujukan'];
                    $ppkRujukan = $_GET['ppkRujukan'];
                    $catatan = $_GET['catatan'];
                    $diagAwal = $_GET['diagAwal'];
                    $tujuan = $_GET['tujuan'];
                    $eksekutif = $_GET['eksekutif'];
                    $cob = $_GET['cob'];
                    $lakaLantas = $_GET['lakaLantas'];
                    $penjamin = $_GET['penjamin'];
                    $lokasiLaka = $_GET['lokasiLaka'];
                    $noTelp = $_GET['noTelp'];
                    $user = $_GET['user'];
                    $noSep = $_GET['noSep'];
                    
                    print_r($bpjs->update_sep_new($noSep,$noKartu,$tglSep,$ppkPelayanan,$jnsPelayanan,$klsRawat,$noMR,$asalRujukan,$tglRujukan,$noRujukan,$ppkRujukan,$catatan,$diagAwal,$tujuan,$eksekutif,$cob,$lakaLantas,$penjamin,$lokasiLaka,$noTelp,$user));
                    break;
                case '15':
                    $tglPulang = date('Y-m-d', strtotime($_GET['tglPulang']));
                    $user = $_GET['user'];
                    $noSep = $_GET['noSep'];
                    
                    print_r($bpjs->update_sep_pulang($noSep,$tglPulang,$user));
                    break;
                case '100':
                    print_r($bpjs->help());
                    break;
                default:
                    die('error number, please check your parameter option');
                    break;
            }
            Yii::app()->end();
        }
    }

    public function simpanSep_baru($model, $postSep) {

        $model->attributes = $postSep;
        $model->tglsep = date('Y-m-d H:i:s', strtotime($postSep['tglsep']));
        $model->pasien_id = $_POST['pasien_id'];
        $model->pendaftaran_id = $_POST['pendaftaran_id'];
        $model->jnspelayanan_kode = $postSep['jnspelayanan_kode'];
        if($model->jnspelayanan_kode == 2){
            $model->jnspelayanan_nama = "Rawat Jalan";
        }else if($model->jnspelayanan_kode == 1){
            $model->jnspelayanan_nama = "Rawat Inap";
        }
        $model->hakkelas_kode = $postSep['hakkelas_kode'];
        $model->kelasrawat_kode = $postSep['kelasrawat_kode'];
        $model->kelasrawat_nama = $model->hakkelas_kode;
        $model->hakkelas_nama = $model->hakkelas_kode;
        $model->lakalantas_nama = $model->lakalantas_kode;
        $model->politujuan_kode = $postSep['politujuan_kode'];
        $model->jenisrujukan_kode_bpjs = $postSep['jenisrujukan_kode_bpjs'];
        $model->jenisrujukan_nama_bpjs = ($postSep['jenisrujukan_kode_bpjs']==1)? "PCare" : "Rumah Sakit";
        $model->ppkrujukanasal_kode = $postSep['ppkrujukanasal_kode'];
        $model->jenispeserta_bpjs_kode = $postSep['jenispeserta_bpjs_kode'];
        $model->jenispeserta_bpjs_nama = $postSep['jenispeserta_bpjs_nama'];
        $model->no_asuransi_cob = $postSep['no_asuransi_cob'];
        $model->nama_asuransi_cob = $postSep['nama_asuransi_cob'];
        $model->tglrujukan_bpjs = date('Y-m-d H:i:s', strtotime($model->tglrujukan_bpjs));
        
        $model->create_time = date('Y-m-d H:i:s');
        $model->create_loginpemakai_id = Yii::app()->user->id;
        $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

        if ($model->save()) {
            $modPasien = PasienM::model()->findByAttributes(array('no_rekam_medik' => $model->no_rekam_medik));
            if (isset($modPasien)) {
                $pasien_id = $modPasien->pasien_id;
                $modPendaftaran = PendaftaranT::model()->findByPk($_POST['pendaftaran_id']);

                $modPendaftaran->sep_id = $model->sep_id;
                $modPendaftaran->update();
            }
            $this->septersimpan = true;
        } else {
            $this->septersimpan = false;
        }

        
        return $model;
    }

    /**
     * untuk menampilkan pasien lama dari autocomplete
     * 1. no_rekam_medik
     */
    public function actionAutocompletePasien() {
        if (Yii::app()->request->isAjaxRequest) {
            $returnVal = array();
            $no_rekam_medik = isset($_GET['no_rekam_medik']) ? $_GET['no_rekam_medik'] : null;

            $criteria = new CDbCriteria();
            $criteria->compare('LOWER(no_rekam_medik)', strtolower($no_rekam_medik), true);
            $criteria->addCondition('ispasienluar = FALSE');
            $criteria->order = 'no_rekam_medik, nama_pasien';
            $criteria->limit = 50;
            $models = PasienM::model()->findAll($criteria);
            foreach ($models as $i => $model) {
                $attributes = $model->attributeNames();
                foreach ($attributes as $j => $attribute) {
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->no_rekam_medik . ' - ' . $model->nama_pasien . (!empty($model->nama_bin) ? "(" . $model->nama_bin . ")" : "") . " - " . (!empty($model->nama_ayah) ? $model->nama_ayah : "(nama ayah tidak ada)") . " - " . ($model->tanggal_lahir);
                $returnVal[$i]['value'] = $model->no_rekam_medik;
            }
            echo CJSON::encode($returnVal);
        } else
            throw new CHttpException(403, 'Tidak dapat mengurai data');
        Yii::app()->end();
    }

    /**
     * @param type $sep_id
     */
    public function actionPrintSep($sep_id) {
        $this->layout = '//layouts/printWindows';
        $modSep = ARSepT::model()->findByPk($sep_id);
        $modInfoKunjungan = PendaftaranT::model()->findByPk($modSep->pendaftaran_id);
        
        $judul_print = 'SURAT ELIGIBILITAS PESERTA';
        $this->render('printSep', array(
            'modSep' => $modSep,
            'modInfoKunjungan' => $modInfoKunjungan,
            'judul_print' => $judul_print,
        ));
    }

    /**
     * Ubah Tanggal Pulang
     */
    public function actionUbahTanggalPulang($sep_id) {
        $this->layout = '//layouts/frameDialog';
        $modSep = ARSepT::model()->findByPk($sep_id);
        $bpjs = new Bpjs();
        $status = '';
        if (isset($_POST['ARSepT'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $modSep->attributes = $_POST['ARSepT'];
                $modSep->tglpulang = ($_POST['ARSepT']['tglpulang']);
                $attributes = array('tglpulang' => $modSep->tglpulang);
                $reqSep = json_decode($bpjs->update_tanggal_pulang_sep($modSep->nosep, $modSep->tglpulang, $modSep->ppkpelayanan), true);
                if ($reqSep['metadata']['code'] == 200) {
                    $this->bridgingsep = true;
                    if ($modSep->update()) {
                        $this->updatesep = true;
                    }
                } else {
                    $this->bridgingsep = false;
                }

                if ($this->bridgingsep == false) {
                    $status = 'Data gagal diubah karena koneksi server BPJS terputus! Silahkan hubungi admin SIMRS';
                } else if ($this->updatesep == false) {
                    $status = 'Data gagal diubah karna kesalahan data / database!';
                } else {
                    $status = 'Data SEP berhasil diubah';
                }
                if ($this->updatesep && $this->bridgingsep) {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', "<strong>Berhasil</strong>" . $status);
                } else {
                    Yii::app()->user->setFlash('error', '<strong>Gagal</strong>' . $status);
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', '<strong>Gagal</strong> Data Gagal disimpan' . MyExceptionMessage::getMessage($exc));
            }
        }

        $this->render('_formUbahTanggal', array(
            'modSep' => $modSep,
        ));
    }

    /**
     * Menghapus data SEP
     */
    public function actionHapusSEP($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $data['sukses'] = 0;
            $data['status'] = '';
            $model = $this->loadModel($id);
            $modUser = LoginpemakaiK::model()->findByPk($model->create_loginpemakai_id);
            $nama = (isset($modUser->user_pemakai_bpjs)&&!empty($modUser->user_pemakai_bpjs))? $modUser->user_pemakai_bpjs : $modUser->nama_pemakai ;
            $bpjs = new Bpjs();
            $transaction = Yii::app()->db->beginTransaction();
            $reqSep = json_decode($bpjs->delete_transaksi_sep($model->no_sep, $nama), true);
            if ($reqSep['metaData']['code'] == 200) {
                $this->bridgingsep = true;
                PendaftaranT::model()->updateAll(array('sep_id' => null), 'sep_id = ' . $model->sep_id);
                ARRujukankeluarBpjsT::model()->model()->deleteAll('sep_id='.$model->sep_id.''); 
                ARPengajuanapprovalsepT::model()->model()->deleteAll('sep_id='.$model->sep_id.''); 
                if ($model->delete()) {
                    $this->deletesep = true;
                    $transaction->commit();
                }
            } else {
                $this->bridgingsep = false;
                $transaction->rollback();
            }

            if ($this->bridgingsep == false) {
                $data['status'] = 'Data gagal dihapus karena '.$reqSep['metaData']['message'];
            } else {
                $data['status'] = 'Data SEP berhasil dihapus';
            }

            echo CJSON::encode($data);
        }
    }

    /**
     * Mengurai data pasien berdasarkan:
     * - instalasi_id
     * - pendaftaran_id
     * - pasienadmisi_id
     * - no_pendaftaran
     * - no_rekam_medik
     * @throws CHttpException
     */
    public function actionGetDataInfoPasien() {
        if (Yii::app()->request->isAjaxRequest) {
            $instalasi_id = isset($_POST['instalasi_id']) ? $_POST['instalasi_id'] : null;
            $pendaftaran_id = isset($_POST['pendaftaran_id']) ? $_POST['pendaftaran_id'] : null;
            $pasienadmisi_id = isset($_POST['pasienadmisi_id']) ? $_POST['pasienadmisi_id'] : null;
            $no_pendaftaran = isset($_POST['no_pendaftaran']) ? $_POST['no_pendaftaran'] : null;
            $no_rekam_medik = isset($_POST['no_rekam_medik']) ? $_POST['no_rekam_medik'] : null;
            $returnVal = array();
            $criteria = new CDbCriteria();
            if (!empty($pendaftaran_id)) {
                $criteria->addCondition("pendaftaran_id = " . $pendaftaran_id);
            }
            if (!empty($pasienadmisi_id) && $pasienadmisi_id !== 'null') {
                $criteria->addCondition("pasienadmisi_id = " . $pasienadmisi_id);
            }
            $criteria->compare('LOWER(no_pendaftaran)', strtolower(trim($no_pendaftaran)));
            $criteria->compare('LOWER(no_rekam_medik)', strtolower(trim($no_rekam_medik)));
            $model = ARInfokunjunganrsSepV::model()->find($criteria);
            $attributes = $model->attributeNames();
            foreach ($attributes as $j => $attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }
            echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }
    
    public function actionAutocompleteInfoPasien() {
        if(Yii::app()->request->isAjaxRequest) {
            $returnVal = array();
            $instalasi_id = isset($_GET['instalasi_id']) ? $_GET['instalasi_id'] : null;
            $pendaftaran_id = isset($_GET['pendaftaran_id']) ? $_GET['pendaftaran_id'] : null;
            $pasienadmisi_id = isset($_GET['pasienadmisi_id']) ? $_GET['pasienadmisi_id'] : null;
            $no_pendaftaran = isset($_GET['no_pendaftaran']) ? $_GET['no_pendaftaran'] : null;
            $no_rekam_medik = isset($_GET['no_rekam_medik']) ? $_GET['no_rekam_medik'] : null;
            $nama_pasien = isset($_GET['nama_pasien']) ? $_GET['nama_pasien'] : null;
            $criteria = new CDbCriteria();
            if(!empty($instalasi_id)){
                $criteria->addCondition('instalasi_id = '.$instalasi_id);
            }
            $criteria->compare('LOWER(no_rekam_medik)', strtolower($no_rekam_medik), true);
            $criteria->compare('LOWER(no_pendaftaran)', strtolower($no_pendaftaran), true);
            $criteria->compare('LOWER(nama_pasien)', strtolower($nama_pasien), true);
            $criteria->limit = 5;
            $models = ARInfokunjunganrsSepV::model()->findAll($criteria);
            foreach($models as $i=>$model)
            {
                $attributes = $model->attributeNames();
                foreach($attributes as $j=>$attribute) {                    
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->no_pendaftaran.' - '.$model->nama_pasien;
                $returnVal[$i]['value'] = $model->no_pendaftaran;
            }

            echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }
    
    public function actionSetFormPoli() {
        if (Yii::app()->request->isAjaxRequest) {
            $poliList = $_POST['poliList'];
            $form = '';
            $pesan = '';
            if (count($poliList) > 0) {
                foreach ($poliList AS $i => $poli) {
                    $kdPoli = $poli['kode'];
                    $nmPoli = $poli['nama'];
                    $form .= 
                    "<tr>
                        <td>
                            <a class='btn-small' href='javascript:void(0);' onclick=\"$('#ARSepT_politujuan_kode').val('".$kdPoli."');$('#ARSepT_politujuan_nama').val('".$nmPoli."');$('#dialogPoli').dialog('close'); \">
                            <i class='icon-check'></i></a>
                        </td>
                        <td>
                            <span id='kdPoli' name=[ii][kdPoli]'>".$kdPoli."</span>
                        </td>
                        <td>
                            <span id='nmPoli' name=[ii][nmPoli]'>".$nmPoli."</span>
                        </td>
                    </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }
    
    public function actionsetFormDiagnosa() {
        if (Yii::app()->request->isAjaxRequest) {
            $diagnosaList = $_POST['diagnosaList'];
            $form = '';
            $pesan = '';
            if (count($diagnosaList) > 0) {
                foreach ($diagnosaList AS $i => $diagnosa) {
                    $kddiagnosa = $diagnosa['kode'];
                    $nmdiagnosa = $diagnosa['nama'];
                    $form .= 
                    "<tr>
                        <td>
                            <a class='btn-small' href='javascript:void(0);' onclick=\"$('#ARSepT_diagnosaawal_kode').val('".$kddiagnosa."');$('#ARSepT_diagnosaawal_nama').val('".$nmdiagnosa."');$('#dialogDiagnosaBpjs').dialog('close'); \">
                            <i class='icon-check'></i></a>
                        </td>
                        <td>
                            <span id='kdPoli' name=[ii][kdPoli]'>".$kddiagnosa."</span>
                        </td>
                        <td>
                            <span id='nmPoli' name=[ii][nmPoli]'>".$nmdiagnosa."</span>
                        </td>
                    </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }
    
    public function actionsetFormFaskes() {
        if (Yii::app()->request->isAjaxRequest) {
            $faskesList = $_POST['faskesList'];
            $form = '';
            $pesan = '';
            if (count($faskesList) > 0) {
                foreach ($faskesList AS $i => $faskes) {
                    $kdfaskes = $faskes['kode'];
                    $nmfaskes = $faskes['nama'];
                    $form .= 
                    "<tr>
                        <td>
                            <a class='btn-small' href='javascript:void(0);' onclick=\" $('#ARSepT_ppkrujukanasal_kode').val('".$kdfaskes."');$('#ARSepT_ppkrujukanasal_nama').val('".$nmfaskes."');$('#dialogPpk').dialog('close'); \">
                            <i class='icon-check'></i></a>
                        </td>
                        <td>
                            <span id='kdPoli' name=[ii][kdPoli]'>".$kdfaskes."</span>
                        </td>
                        <td>
                            <span id='nmPoli' name=[ii][nmPoli]'>".$nmfaskes."</span>
                        </td>
                    </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }
    
    public function actionUpdateSEP($sep_id){
        $this->layout = '//layouts/frameDialog';
        $model = ARSepT::model()->findByPk($sep_id);
        $modInfoKunjungan = ARInfokunjunganrsSepV::model()->findByAttributes(array('pendaftaran_id' => $model->pendaftaran_id));
        $model->ppkpelayanan = Yii::app()->user->getState('kode_ppk_bpjs');
        $model->ppkpelayanan_nama = Yii::app()->user->getState('nama_ppk_pelayanan');
        $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
        if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
            $model->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
        }else{
            $model->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
        }
        
        if($model->cob_bpjs==1){
            $model->cob_status = "YA";
        }else{
            $model->cob_status = "TIDAK";
        }
        
        if(isset($_POST['ARSepT'])){
            
            $model->attributes = $_POST['ARSepT'];
            $model->jnspelayanan_kode = $_POST['ARSepT']['jnspelayanan_kode'];
            if($model->jnspelayanan_kode == 2){
                $model->jnspelayanan_nama = "Rawat Jalan";
            }else if($model->jnspelayanan_kode == 1){
                $model->jnspelayanan_nama = "Rawat Inap";
            }
            $model->update_time = date('Y-m-d H:i:s');
            $model->upate_loginpemakai_id = Yii::app()->user->id;
            $model->update();
                        
            if($model->update()){
                $this->redirect(array('UpdateSEP', 'sep_id' => $sep_id,'sukses' => 1));
            }else{
                Yii::app()->user->setFlash('error', "SEP gagal disimpan");
            }
        }
        
        $this->render('_formUpdate', array(
            'model' => $model,
            'modInfoKunjungan' => $modInfoKunjungan,
            
        ));
    }
    
    public function actionUpdateTglPulang($sep_id){
        $this->layout = '//layouts/frameDialog';
        $model = ARSepT::model()->findByPk($sep_id);
        $modInfoKunjungan = ARInfokunjunganrsSepV::model()->findByAttributes(array('pendaftaran_id' => $model->pendaftaran_id));
        $model->ppkpelayanan = Yii::app()->user->getState('kode_ppk_bpjs');
        $model->ppkpelayanan_nama = Yii::app()->user->getState('nama_ppk_pelayanan');
        $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
        if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
            $model->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
        }else{
            $model->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
        }
        
        if(isset($_POST['ARSepT'])){
            
            $model->attributes = $_POST['ARSepT'];
            $model->tanggalpulang_sep = $_POST['ARSepT']['tanggalpulang_sep'];
            $model->update_time = date('Y-m-d H:i:s');
            $model->upate_loginpemakai_id = Yii::app()->user->id;
            if($model->update()){
                $this->redirect(array('UpdateTglPulang', 'sep_id' => $sep_id, 'sukses' => 1));
            }else{
                Yii::app()->user->setFlash('error', "SEP gagal disimpan");
            }
        }
        
        $this->render('_formUpdatePulang', array(
            'model' => $model,
            'modInfoKunjungan' => $modInfoKunjungan,
        ));
    }

}
