<?php
class ProcedureController extends Controller{
	
	public $path_view = 'asuransi.views.procedure.';
	
	public function actionIndex(){
		
		$this->render($this->path_view.'index',array(
			
		));
	}
	
	/**
	* set bpjs Interface
	*/
	public function actionBpjsInterface()
	{
		if(Yii::app()->getRequest()->getIsAjaxRequest()) {
			if(empty( $_GET['param'] ) OR $_GET['param'] === ''){
				die('param can\'not empty value');
			}else{
				$param = $_GET['param'];
			}

			$bpjs = new Bpjs();

			switch ($param) {
				case '1':
					$query = $_GET['query'];
					$query = explode(" ",$query);
					$query = $query[0];
					$start = 1;
					$limit = 10;
					print_r( $bpjs->search_procedure($query,$start, $limit) );
					break;
				default:
					die('error number, please check your parameter option');
					break;
			}
			Yii::app()->end();
		}
	}
	
	/**
	* @param type $faskes - katakunci
	*/
	public function actionPrintData($procedure = null)
	{
		$this->layout='//layouts/printWindows';
		$format = new CustomFormat;
		$data = '';
		if(!empty($procedure)){
			$data = 'PROCEDURE TINDAKAN';
		}
		$judul_print = 'DATA '.$data.'';
		$this->render($this->path_view.'print', array(
			'format'=>$format,
			'judul_print'=>$judul_print,
		));
	} 
	
    public function actionSetFormProcedure()
    {
        if(Yii::app()->request->isAjaxRequest) { 
            $procedureList = $_POST['procedureList'];
			$form = '';
			$pesan = '';
            if(count($procedureList) > 0){
                foreach($procedureList AS $i => $procedure){
					$kode = $procedure['kode'];
					$nama = $procedure['nama'];
                    $form .= $this->renderPartial($this->path_view.'_rowDetail', array(
						'kode'=>$kode,
						'nama'=>$nama,
					), true);
                }
            }else{
                $pesan = "Data tidak ada!";
            }
            
            echo CJSON::encode(array('form'=>$form, 'pesan'=>$pesan));
            Yii::app()->end(); 
        }
    }
}