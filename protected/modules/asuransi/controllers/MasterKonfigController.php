<?php

class MasterKonfigController extends Controller
{
    public function actionIndex()
    {
        $model = ARKonfigBpjsInacbgK::model()->findByPk(1);
        
        if(isset($_POST['ARKonfigBpjsInacbgK'])){
            try{
                $transaction = Yii::app()->db->beginTransaction();
                $model->attributes = $_POST['ARKonfigBpjsInacbgK'];
                $model->user_bpjs_name = $_POST['ARKonfigBpjsInacbgK']['user_bpjs_name'];
                $model->secret_bpjs_key = $_POST['ARKonfigBpjsInacbgK']['secret_bpjs_key'];
                $model->bpjs_host_tester = $_POST['ARKonfigBpjsInacbgK']['bpjs_host_tester'];
                $model->bpjs_host_production = $_POST['ARKonfigBpjsInacbgK']['bpjs_host_production'];
                $model->bpjs_service_name = $_POST['ARKonfigBpjsInacbgK']['bpjs_service_name'];
                $model->bpjs_port = $_POST['ARKonfigBpjsInacbgK']['bpjs_port'];
                $model->is_bridging = $_POST['ARKonfigBpjsInacbgK']['is_bridging'];
                $model->kode_ppk_bpjs = $_POST['ARKonfigBpjsInacbgK']['kode_ppk_bpjs'];
                $model->nama_ppk_pelayanan = $_POST['ARKonfigBpjsInacbgK']['nama_ppk_pelayanan'];
                $model->konfig_bpjs_inacbg_aktif = $_POST['ARKonfigBpjsInacbgK']['konfig_bpjs_inacbg_aktif'];
                $model->is_tester = $_POST['ARKonfigBpjsInacbgK']['is_tester'];
                $model->host_url_inacbg = $_POST['ARKonfigBpjsInacbgK']['host_url_inacbg'];
                $model->add_payment_pct = $_POST['ARKonfigBpjsInacbgK']['add_payment_pct'];
                $model->encryption_key_inacbg = $_POST['ARKonfigBpjsInacbgK']['encryption_key_inacbg'];
                $model->kode_tarifinacbgs_1 = $_POST['ARKonfigBpjsInacbgK']['kode_tarifinacbgs_1'];
                $model->nama_tarifinacbgs_1 = $_POST['ARKonfigBpjsInacbgK']['nama_tarifinacbgs_1'];

                if($model->save()){
                    Yii::app()->user->setFlash('success','<strong>Berhasil</strong>Data Berhasil disimpan');
                    $transaction->commit();
                } else {
                    $transaction->rollback();
                }
            } catch (Exception $ex) {
                Yii::app()->user->setFlash('error', '<strong>Gagal</strong> Data Gagal disimpan'.MyExceptionMessage::getMessage($ex));
                $transaction->rollback();
            }
            
        }
        $this->render('index', array('model'=>$model));
    }
    
}