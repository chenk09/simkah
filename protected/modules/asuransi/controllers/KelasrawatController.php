<?php
class KelasrawatController extends Controller{
	
	public $path_view = 'asuransi.views.kelasrawat.';
	
	public function actionIndex(){
		
		$this->render($this->path_view.'index',array(
			
		));
	}
	
	/**
	* set bpjs Interface
	*/
	public function actionBpjsInterface()
	{
		if(Yii::app()->getRequest()->getIsAjaxRequest()) {
			if(empty( $_GET['param'] ) OR $_GET['param'] === ''){
				die('param can\'not empty value');
			}else{
				$param = $_GET['param'];
			}

 //                if(empty( $_GET['server'] ) OR $_GET['server'] === ''){
 //                    
 //                }else{
 //                    $server = 'http://'.$_GET['server'];
 //                }

			$bpjs = new Bpjs();

			switch ($param) {
				case '1':
					$start = 1;
					$limit = 10;
					print_r( $bpjs->search_kelas_rawat("",$start, $limit) );
					break;
				default:
					die('error number, please check your parameter option');
					break;
			}
			Yii::app()->end();
		}
	}
	
	/**
	* @param type $faskes - katakunci
	*/
	public function actionPrintData($faskes = null)
	{
		$this->layout='//layouts/printWindows';
		$format = new CustomFormat;

		$judul_print = 'DATA KELAS RAWAT';
		$this->render($this->path_view.'print', array(
			'format'=>$format,
			'judul_print'=>$judul_print,
		));
	} 
	
    public function actionSetFormKelasrawat()
    {
        if(Yii::app()->request->isAjaxRequest) { 
            $kelasrawatList = $_POST['kelasrawatList'];
			$form = '';
			$pesan = '';
            if(count($kelasrawatList) > 0){
                foreach($kelasrawatList AS $i => $kelasrawat){
					$kode = $kelasrawat['kode'];
					$nama = $kelasrawat['nama'];
                    $form .= $this->renderPartial($this->path_view.'_rowDetail', array(
						'kode'=>$kode,
						'nama'=>$nama,
					), true);
                }
            }else{
                $pesan = "Data tidak ada!";
            }
            
            echo CJSON::encode(array('form'=>$form, 'pesan'=>$pesan));
            Yii::app()->end(); 
        }
    }
}