<?php

class InacbgNewController extends Controller {

    public $path_view_inacbg = 'asuransi.views.inacbgNew.';
    public $inacbgtersimpan = false;
    public $diagnosainacbgtersimpan = false;
    public $bridgingfinalisasiberhasil = true;
    public $updatefinalisasitersimpan = true;

    public function actionIndex() {
        $model = new ARInacbgT();
        $model->is_naikkelas = false;
        $model->is_rawatintesif = false;
        $model->is_finalisasi = false;
        $model->is_grouping = false;
        $model->is_terkirim = false;
        $modSEP = new ARSepT();
        $modPendaftaran = new PendaftaranT();
        $modPasien = new PasienM();
        $modPasienAdmisi = new PasienadmisiT();
        $modPasienPulang = new PasienpulangT();
        $modInfoSep = new ARInfosepinacbgV;
        $moddiagnosaicdx = array();
        $moddiagnosaicdix = array();
        $modDiagnosaInacbg = array();
        $modinasiscbg = new ARInasiscbgT();
        $modinasiscmg = new ARInasiscmgT();
        $modUser = LoginpemakaiK::model()->findByPk(Yii::app()->user->id); //untuk get nik coder

        if (!empty($_GET['inacbg_id'])) {
            $model = ARInacbgT::model()->findByPk($_GET['inacbg_id']);
            $modSEP = ARSepT::model()->findByPK($model->sep_id);
            $modPendaftaran = PendaftaranT::model()->findByPK($model->pendaftaran_id);
            $modPasien = PasienM::model()->findByPK($model->pasien_id);
            if (!empty($model->pasienadmisi_id)) {
                $modPasienAdmisi = PasienadmisiT::model()->findByPK($model->pasienadmisi_id);
            }
            if (!empty($model->pasienpulang_id)) {
                $modPasienPulang = PasienpulangT::model()->findByPK($model->pasienpulang_id);
            }
            $modInfoSep = ARInfosepinacbgV::model()->findByAttributes(array('no_sep'=>$modSEP->no_sep));
            $moddiagnosaicdx = ARDiagnosaxInacbgsT::model()->findAllByAttributes(array('inacbg_id'=>$model->inacbg_id));
            $moddiagnosaicdix = ARDiagnosaixInacbgT::model()->findAllByAttributes(array('inacbg_id'=>$model->inacbg_id));
            $modinasiscbg = ARInasiscbgT::model()->findByAttributes(array('inacbg_id'=>$model->inacbg_id));
            if(!isset($modinasiscbg->inacbg_id)){
                $modinasiscbg = new ARInasiscbgT();
            }
            $modinasiscmg = ARInasiscmgT::model()->findByAttributes(array('inacbg_id'=>$model->inacbg_id));
            if(!isset($modinasiscmg->inacbg_id)){
                $modinasiscmg = new ARInasiscmgT();
            }
        }

        if (isset($_POST['ARInacbgT'])) {
            
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes = $_POST['ARInacbgT'];
                $model = $this->simpanInacbg($model, $_POST['ARInacbgT']);

                if(isset($_POST['DiagnosaM'])){
                    $moddiagnosaicdx = $this->simpanDagnosaX($moddiagnosaicdx,$model,$_POST['DiagnosaM']);
                }

                if(isset($_POST['DiagnosaicdixM'])){
                    $moddiagnosaicdix = $this->simpanDagnosaIx($moddiagnosaicdix,$model,$_POST['DiagnosaicdixM']);
                }
                
                if(isset($_POST['ARInasiscbgT'])){
                    $modinasiscbg = $this->simpanCbg($modinasiscbg,$model,$_POST['ARInasiscbgT']);
                }
                
                if(isset($_POST['ARInasiscmgT'])){
                    $modinasiscmg = $this->simpanCmg($modinasiscmg,$model,$_POST['ARInasiscmgT']);
                }
                
                if ($this->inacbgtersimpan) {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', "Data berhasil disimpan");
                    $this->redirect(array('index', 'inacbg_id' => $model->inacbg_id, 'sukses' => 1));
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($ex, true));
            }
        }

        if(!isset($_GET['ubah'])){
            $target = 'index';
        }else{
            $target = 'indexUpdate';
        }

        $this->render($this->path_view_inacbg . $target, array(
            'model' => $model,
            'modSEP' => $modSEP,
            'modPendaftaran' => $modPendaftaran,
            'modPasien' => $modPasien,
            'modPasienAdmisi' => $modPasienAdmisi,
            'modPasienPulang' => $modPasienPulang,
            'modDiagnosaInacbg' => $modDiagnosaInacbg,
//            'modInfoSep' => $modInfoSep,
            'moddiagnosaicdx' => $moddiagnosaicdx,
            'moddiagnosaicdix' => $moddiagnosaicdix,
            'modinasiscbg' => $modinasiscbg,
            'modinasiscmg' => $modinasiscmg,
            'modUser' => $modUser,
        ));
    }
    
    public function actionAdmin(){
        $model = new ARInacbgT();
        $model->tgl_awal = date('Y-m-d');
        $model->tgl_akhir = date('Y-m-d');
        if (isset($_GET['ARInacbgT'])) {
            $model->attributes = $_GET['ARInacbgT'];
            $model->nosep = $_GET['ARInacbgT']['nosep'];
            $model->no_pendaftaran = $_GET['ARInacbgT']['no_pendaftaran'];
            $model->nokartuasuransi = $_GET['ARInacbgT']['nokartuasuransi'];
            $model->nama_pasien = $_GET['ARInacbgT']['nama_pasien'];
            $model->tgl_awal = isset($_GET['ARInacbgT']['tgl_awal']) ? date('Y-m-d', strtotime($_GET['ARInacbgT']['tgl_awal'])) : null;
            $model->tgl_akhir = isset($_GET['ARInacbgT']['tgl_akhir']) ? date('Y-m-d', strtotime($_GET['ARInacbgT']['tgl_akhir'])) : null;
        }
        
        $this->render($this->path_view_inacbg . 'admin', array(
            'model' => $model,
        ));
    }
    
    public function actionKirimOnline(){
        $model = new ARInacbgT();
        $model->tgl_awal = date('Y-m-d');
        $model->tgl_akhir = date('Y-m-d');
        $model->jenis = 1;
        $model->jenisrawat_inacbg = 3;
        if (isset($_GET['ARInacbgT'])) {
            $model->attributes = $_GET['ARInacbgT'];
            $model->tgl_awal = $_GET['ARInacbgT']['tgl_awal'];
            $model->tgl_akhir = $_GET['ARInacbgT']['tgl_akhir'];
            $model->jenisrawat_inacbg = $_GET['ARInacbgT']['jenisrawat_inacbg'];
            $model->tglrawat_keluar = $_GET['ARInacbgT']['tglrawat_keluar'];
            $model->jenis = $_GET['ARInacbgT']['jenis'];
        }
        
        $this->render($this->path_view_inacbg . 'kirimOnline', array(
            'model' => $model,
        ));
    }

    /**
     * untuk menampilkan data sep dari autocomplete
     * 1. nosep
     */
    public function actionAutocompleteSEP() {
        if (Yii::app()->request->isAjaxRequest) {
            $returnVal = array();
            $nosep = isset($_GET['no_sep']) ? $_GET['no_sep'] : null;
            $no_pendaftaran = isset($_GET['no_pendaftaran']) ? $_GET['no_pendaftaran'] : null;

            $criteria = new CDbCriteria();
            $criteria->compare('LOWER(no_sep)', strtolower($nosep), true);
            $criteria->compare('LOWER(no_pendaftaran)', strtolower($no_pendaftaran), true);
            $criteria->order = 'no_sep';
            $criteria->limit = 5;
            $models = ARInfosepinacbgV::model()->findAll($criteria);
            foreach ($models as $i => $model) {
                $attributes = $model->attributeNames();
                foreach ($attributes as $j => $attribute) {
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->no_sep . "-" . $model->no_pendaftaran;
                $returnVal[$i]['value'] = $model->no_sep;
            }
            echo CJSON::encode($returnVal);
        } else
            throw new CHttpException(403, 'Tidak dapat mengurai data');
        Yii::app()->end();
    }

    public function actionAutoCompleteDiagnosa() {
        if (Yii::app()->request->isAjaxRequest) {
            $returnVal = array();
            $diagnosa = isset($_GET['diagnosa']) ? $_GET['diagnosa'] : null;

            $criteria = new CDbCriteria();
            $criteria->compare("LOWER(replace(diagnosa_kode, '.', ''))", strtolower(str_replace('.', '', $diagnosa)), true);
            $criteria->order = 'diagnosa_kode';
            $criteria->addCondition('diagnosa_aktif IS TRUE');
            $criteria->limit = 10;
            $models = DiagnosaM::model()->findAll($criteria);
            foreach ($models as $i => $model) {
                $attributes = $model->attributeNames();
                foreach ($attributes as $j => $attribute) {
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->diagnosa_kode . " - " . $model->diagnosa_namalainnya;
            }
            echo CJSON::encode($returnVal);
        } else
            throw new CHttpException(403, 'Tidak dapat mengurai data');
        Yii::app()->end();
    }
    
    public function actionAutoCompleteProsedur() {
        if (Yii::app()->request->isAjaxRequest) {
            $returnVal = array();
            $diagnosa = isset($_GET['diagnosa']) ? $_GET['diagnosa'] : null;

            $criteria = new CDbCriteria();
            $criteria->compare("LOWER(replace(diagnosaicdix_kode, '.', ''))", strtolower(str_replace('.', '', $diagnosa)), true);
            $criteria->order = 'diagnosaicdix_kode';
            $criteria->addCondition('diagnosaicdix_aktif IS TRUE');
            $criteria->limit = 10;
            $models = DiagnosaicdixM::model()->findAll($criteria);
            foreach ($models as $i => $model) {
                $attributes = $model->attributeNames();
                foreach ($attributes as $j => $attribute) {
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->diagnosaicdix_kode . " - " . $model->diagnosaicdix_namalainnya;
            }
            echo CJSON::encode($returnVal);
        } else
            throw new CHttpException(403, 'Tidak dapat mengurai data');
        Yii::app()->end();
    }

    /**
     * Mengurai data Kunjungan Pasien berdasarkan pendaftaran_id
     * @throws CHttpException
     */
    public function actionGetDataKunjunganPasien() {
        if (Yii::app()->request->isAjaxRequest) {
            $returnVal = array();
            $nosep = isset($_POST['nosep']) ? $_POST['nosep'] : null;

            $criteria = new CDbCriteria();
            $criteria->addCondition("no_sep = '" . $nosep . "'");
            $criteria->order = 'no_sep';
            $models = ARInfosepinacbgV::model()->findAll($criteria);
            foreach ($models as $i => $model) {
                $attributes = $model->attributeNames();
                foreach ($attributes as $j => $attribute) {
                    $returnVal["$attribute"] = $model->$attribute;
                }
                $returnVal["namaasuransi_cob"] = $model->namaasuransi_cob;
            }
            if (!empty($returnVal['pendaftaran_id'])) {
                $modPendaftaran = PendaftaranT::model()->findByPk($returnVal['pendaftaran_id']);
                $returnVal["carabayar_id"] = $modPendaftaran->carabayar_id;
            }
            if (!empty($returnVal['no_sep'])) {
                $modSep = SepT::model()->findByAttributes(array('no_sep' => $returnVal['no_sep']));
                $returnVal["klsrawat"] = $modSep->kelasrawat_kode;
            }


            echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }

    public function simpanInacbg($model, $postInacbg) {
        $modPendaftaran = PendaftaranT::model()->findByPk($_POST['pendaftaran_id']);
                
        $model->attributes = $postInacbg;
        $model->sep_id = $modPendaftaran->sep_id;
        $model->no_sep = $_POST['no_sep'];
        $model->pendaftaran_id = $modPendaftaran->pendaftaran_id;
        $model->pasien_id = $modPendaftaran->pasien_id;
        $model->jenisrawat_inacbg = $postInacbg['jenisrawat_inacbg'];
        $model->hak_kelasrawat_inacbg = $postInacbg['hak_kelasrawat_inacbg'];
        $model->is_naikkelas = $postInacbg['is_naikkelas'];
        $model->is_rawatintesif = $postInacbg['is_naikkelas'];
        $model->tglrawat_masuk = $_POST['tgl_pendaftaran'];
        $model->tglrawat_keluar = $_POST['tglpulang'];
        $model->adlscore_subaccute = $postInacbg['adlscore_subaccute'];
        $model->adlscore_chronic = $postInacbg['adlscore_chronic'];
        $model->naik_kelasrawat_inacbg = ($model->is_naikkelas==1)?  $postInacbg['naik_kelasrawat_inacbg'] : 0;
        $model->lamarawat_naikkelas = ($model->is_naikkelas==1)?  $postInacbg['lamarawat_naikkelas'] : 0;
        $model->lamarawat_icu = ($model->is_rawatintesif==1)?  $postInacbg['lamarawat_icu'] : 0;
        $model->ventilator_icu = ($model->is_rawatintesif==1)?  $postInacbg['ventilator_icu'] : 0;
        $model->total_lamarawat = $_POST['los'];
        $model->umur_pasien = $postInacbg['umur_pasien'];
        $model->berat_lahir = $postInacbg['berat_lahir'];
        $model->nama_dpjp = $postInacbg['nama_dpjp'];
        $model->cara_pulang = $postInacbg['cara_pulang'];
        $model->tarif_prosedur_nonbedah = $postInacbg['tarif_prosedur_nonbedah'];
        $model->tarif_prosedur_bedah = $postInacbg['tarif_prosedur_bedah'];
        $model->tarif_konsultasi = $postInacbg['tarif_konsultasi'];
        $model->tarif_tenaga_ahli = $postInacbg['tarif_tenaga_ahli'];
        $model->tarif_keperawatan = $postInacbg['tarif_keperawatan'];
        $model->tarif_penunjang = $postInacbg['tarif_penunjang'];
        $model->tarif_radiologi = $postInacbg['tarif_radiologi'];
        $model->tarif_laboratorium = $postInacbg['tarif_laboratorium'];
        $model->tarif_pelayanan_darah = $postInacbg['tarif_pelayanan_darah'];
        $model->tarif_rehabilitasi = $postInacbg['tarif_rehabilitasi'];
        $model->tarif_akomodasi = $postInacbg['tarif_akomodasi'];
        $model->tarif_rawat_intensif = $postInacbg['tarif_rawat_intensif'];
        $model->tarif_obat = $postInacbg['tarif_obat'];
        $model->tarif_alkes = $postInacbg['tarif_alkes'];
        $model->tarif_bhp = $postInacbg['tarif_bhp'];
        $model->tarif_sewa_alat = $postInacbg['tarif_sewa_alat'];
        $model->total_tarif_rs = $postInacbg['total_tarif_rs'];
        $penjamin = null;
        $cob = null;
        if(!empty($_POST['carabayar_id'])){
            $penjamin = PenjaminBpjsM::model()->findByAttributes(array('penjamin_bpjs_kode'=>$_POST['carabayar_id']));
        }
        if(!empty($_POST['cob'])){
            $cob = CobBpjsM::model()->findByAttributes(array('cob_bpjs_kode'=>$_POST['cob']));
        }
        $model->jaminan_id = $penjamin->penjamin_bpjs_kode;
        $modCaraBayar = CarabayarM::model()->findByPk($model->jaminan_id);
        $model->jaminan_nama = $modCaraBayar->carabayar_nama;
        $model->cob_id = $cob;
        $model->create_coder_nik = "1"; //dari login
        $model->is_finalisasi = $postInacbg['is_finalisasi'];
        $model->is_grouping = $postInacbg['is_grouping'];
        $model->is_terkirim = $postInacbg['is_terkirim'];
        $model->create_tanggal = date('Y-m-d H:i:s');
        $model->create_loginpemakai_id = Yii::app()->user->id;
        $model->create_ruangan_id = Yii::app()->user->getState('ruangan_id');
        
        if ($model->save()) {
            $this->inacbgtersimpan = true;
        } else {
            $this->inacbgtersimpan = false;
        }
        
        return $model;
    }

    public function simpanDiagnosaInacbg($model, $postDiagnosa, $detail) {
        $modDiagnosa = ARDiagnosaM::model()->findByPk($detail['diagnosa_id']);
        $modDiagnosaInacbg = new ARDiagnosainacbgT();

        $modDiagnosaInacbg->attributes = $postDiagnosa;
        $modDiagnosaInacbg->pasienmorbiditas_id = $detail['pasienmorbiditas_id'];
        $modDiagnosaInacbg->inacbg_id = $model->inacbg_id;
        $modDiagnosaInacbg->pendaftaran_id = $model->pendaftaran_id;
        $modDiagnosaInacbg->kodediagnosainacbg = isset($modDiagnosa->diagnosa_kode) ? $modDiagnosa->diagnosa_kode : "";
        $modDiagnosaInacbg->namadiagnosainacbg = isset($modDiagnosa->diagnosa_nama) ? $modDiagnosa->diagnosa_nama : "";
        $modDiagnosaInacbg->create_time = date('Y-m-d H:i:s');
        $modDiagnosaInacbg->create_loginpemakai_id = Yii::app()->user->id;
        $modDiagnosaInacbg->create_ruangan = Yii::app()->user->getState('ruangan_id');

        if ($modDiagnosaInacbg->save()) {
            $this->diagnosainacbgtersimpan = true;
        } else {
            $this->diagnosainacbgtersimpan = false;
        }

        return $modDiagnosaInacbg;
    }

    public function actionInacbgInterface(){
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            if (empty($_GET['param']) OR $_GET['param'] === '') {
                die('param can\'not empty value');
            } else {
                $param = $_GET['param'];
            }

            $inacbg = new Inacbg();

            switch ($param) {
                case '1':
                    $nomor_rm = $_GET['nomor_rm'];
                    $nama_pasien = $_GET['nama_pasien'];
                    $modPasien = PasienM::model()->findByAttributes(array('no_rekam_medik'=>$nomor_rm));
                    $tgl_lahir = date('Y-m-d', strtotime($modPasien->tanggal_lahir));
                    $gender = ($modPasien->jeniskelamin=='LAKI-LAKI')? 1 : 2;
                    
                    $nomor_sep = $_GET['nomor_sep'];
                    $nomor_kartu = $_GET['nomor_kartu'];
                    $tgl_masuk = date('Y-m-d', strtotime($_GET['tgl_masuk']));
                    $tgl_pulang = date('Y-m-d', strtotime($_GET['tgl_pulang']));
                    $jenis_rawat = $_GET['jenis_rawat'];
                    $kelas_rawat = $_GET['kelas_rawat'];
                    $adl_sub_acute = $_GET['adl_sub_acute'];
                    $adl_chronic = $_GET['adl_chronic'];
                    $icu_indikator = $_GET['icu_indikator'];
                    $icu_los = ($icu_indikator==1)? $_GET['icu_los'] : 0;
                    $ventilator_hour = ($icu_indikator==1)? $_GET['ventilator_hour'] : 0;
                    $upgrade_class_ind = $_GET['upgrade_class_ind'];
                    $upgrade_class_class = ($upgrade_class_ind==1)? $_GET['upgrade_class_class'] : 0;
                    $upgrade_class_los = ($upgrade_class_ind==1)? $_GET['upgrade_class_los'] : 0;
                    $add_payment_pct = $_GET['add_payment_pct'];
                    $birth_weight = $_GET['birth_weight'];
                    $discharge_status = $_GET['discharge_status'];
                    $diagnosa = str_replace(',', '#', $_GET['diagnosa']);
                    $procedure = str_replace(',', '#', $_GET['procedure']);
                    $prosedur_non_bedah = $_GET['prosedur_non_bedah'];
                    $prosedur_bedah = $_GET['prosedur_bedah'];
                    $konsultasi = $_GET['konsultasi'];
                    $tenaga_ahli = $_GET['tenaga_ahli'];
                    $keperawatan = $_GET['keperawatan'];
                    $penunjang = $_GET['penunjang'];
                    $radiologi = $_GET['radiologi'];
                    $laboratorium = $_GET['laboratorium'];
                    $pelayanan_darah = $_GET['pelayanan_darah'];
                    $rehabilitasi = $_GET['rehabilitasi'];
                    $kamar = $_GET['kamar'];
                    $rawat_intensif = $_GET['rawat_intensif'];
                    $obat = $_GET['obat'];
                    $alkes = $_GET['alkes'];
                    $bmhp = $_GET['bmhp'];
                    $sewa_alat = $_GET['sewa_alat'];
                    $tarif_poli_eks = $_GET['tarif_poli_eks'];
                    $nama_dokter = $_GET['nama_dokter'];
                    $kode_tarif = $_GET['kode_tarif'];
                    $payor_id = $_GET['payor_id'];
                    $payor_cd = PenjaminBpjsM::model()->findByPk($payor_id)->penjamin_bpjs_nama;
                    $cob_cd = $_GET['cob_cd'];
                    $coder_nik = $_GET['coder_nik'];
                    
                    print_r($inacbg->UpdateDataKlaim($nomor_sep,$nomor_kartu,$tgl_masuk,$tgl_pulang,$jenis_rawat,$kelas_rawat,$adl_sub_acute,$adl_chronic,$icu_indikator,$icu_los,$ventilator_hour,$upgrade_class_ind,$upgrade_class_class,$upgrade_class_los,$add_payment_pct,$birth_weight,$discharge_status,$diagnosa,$procedure,$prosedur_non_bedah,$prosedur_bedah,$konsultasi,$tenaga_ahli,$keperawatan,$penunjang,$radiologi,$laboratorium,$pelayanan_darah,$rehabilitasi,$kamar,$rawat_intensif,$obat,$alkes,$bmhp,$sewa_alat,$tarif_poli_eks,$nama_dokter,$kode_tarif,$payor_id,$payor_cd,$cob_cd,$coder_nik,$nomor_rm,$nama_pasien,$tgl_lahir,$gender));
                    break;
                case '2':
                    $nomor_sep = $_GET['nomor_sep'];
                    $kode = str_replace(',', '#', $_GET['kode']);
                    
                    print_r($inacbg->GroupingStage2($nomor_sep,$kode));
                    break;
                case '3':
                    $nomor_sep = $_GET['nomor_sep'];
                    $coder_nik = $_GET['coder_nik'];
                    
                    print_r($inacbg->FinalisasiKlaim($nomor_sep,$coder_nik));
                    break;
                case '4':
                case '7'://ulangi grouping perlu edit dulu kemidian grouping kembali oleh klien
                    $nomor_sep = $_GET['nomor_sep'];
                    
                    print_r($inacbg->EditUlangKlaim($nomor_sep));
                    break;
                case '5':
                    $nomor_sep = $_GET['nomor_sep'];
                    $coder_nik = $_GET['coder_nik'];
                    
                    print_r($inacbg->MenghapusKlaim($nomor_sep,$coder_nik));
                    break;
                case '6':
                    $nomor_sep = $_GET['nomor_sep'];
                    
                    print_r($inacbg->CetakKlaim($nomor_sep));
                    break;
                default:
                    die('error number, please check your parameter option');
                    break;
            }
            Yii::app()->end();
        }
    }
    
    public function actionSetDiagnosa() {
        if (Yii::app()->request->isAjaxRequest) {
            $pendaftaran_id = (isset($_POST['pendaftaran_id']) ? $_POST['pendaftaran_id'] : null);
            $form = '';

            $modDiagnosa = InfodiagnosaicdxinacbgV::model()->findAllByAttributes(array('pendaftaran_id' => $pendaftaran_id));
            $form = $this->renderPartial('_rowDiagnosa', array('modDiagnosa' => $modDiagnosa), true);

            $data['form'] = $form;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actionTambahDiagnosa() {
        if (Yii::app()->request->isAjaxRequest) {
            $diagnosa_id = (isset($_POST['diagnosa_id']) ? $_POST['diagnosa_id'] : null);
            $jumlah_diagnosa = (isset($_POST['jumlah_diagnosa']) ? $_POST['jumlah_diagnosa'] : 0);
            $form = '';
            
            $value = new DiagnosaM;
            $modDiagnosa = DiagnosaM::model()->findByPk($diagnosa_id);
            
            $value->diagnosa_id = $modDiagnosa->diagnosa_id;
            $value->diagnosa_kode = $modDiagnosa->diagnosa_kode;
            if($jumlah_diagnosa > 0){
                $jenis = "Sekunder";
                $value->kelompokdiagnosa_id = Params::KELOMPOKDIAGNOSA_TAMBAHAN;
            }else{
                $jenis = "Primer";
                $value->kelompokdiagnosa_id = Params::KELOMPOKDIAGNOSA_UTAMA;
            }
            
            $form = "
                <tr>
                    <td>
                        ".CHtml::activeHiddenField($value, '[ii]diagnosa_id',array('class'=>'span3'))."
                        ".CHtml::activeHiddenField($value, '[ii]diagnosa_kode',array('class'=>'span3'))."
                        ".CHtml::activeHiddenField($value, '[ii]kelompokdiagnosa_id',array('class'=>'span3'))."
                    <span id='no' name='[ii][no]'></span>			
                    </td>
                    <td>		
                        <span id='kodeDiagnosa' name='[ii][diagnosa_kode]'>".$modDiagnosa->diagnosa_kode."</span>			
                        
                        <span id='jenis' name='[ii][jenis]'>[".$jenis."]</span>			
                    </td>
                    <td>
                        <span id='namaDiagnosa' name='[ii][diagnosa_nama]'>".$modDiagnosa->diagnosa_nama."</span>
                    </td>	
                    <td>
                        <span id='namaLainnya' name='[ii][diagnosa_namalainnya]'>".$modDiagnosa->diagnosa_namalainnya."</span>
                    </td>	
                    <td class='hapus_diagnosa'>
                        <a onclick='bataldiagnosa(this);return false;' rel='tooltip' href='javascript:void(0);' title='lik untuk membatalkan diagnosa ini'><i class='icon-remove'></i></a>
                    </td>	
                </tr>
            ";

            $data['form'] = $form;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actionTambahProsedur() {
        if (Yii::app()->request->isAjaxRequest) {
            $diagnosa_id = (isset($_POST['diagnosa_id']) ? $_POST['diagnosa_id'] : null);
            $form = '';
            
            $value = new DiagnosaicdixM;
            $modDiagnosa = DiagnosaicdixM::model()->findByPk($diagnosa_id);
            
            $value->diagnosaicdix_id = $modDiagnosa->diagnosaicdix_id;
            $value->diagnosaicdix_kode = $modDiagnosa->diagnosaicdix_kode;
            
            $form = "
                <tr>
                    <td>
                        ".CHtml::activeHiddenField($value, '[ii]diagnosaicdix_id',array('class'=>'span3'))."
                        ".CHtml::activeHiddenField($value, '[ii]diagnosaicdix_kode',array('class'=>'span3'))."
                    <span id='no' name='[ii][no]'></span>			
                    </td>
                    <td>		
                        <span id='kodeDiagnosa' name='[ii][diagnosaicdix_kode]'>".$modDiagnosa->diagnosaicdix_kode."</span>			
                    </td>
                    <td>
                        <span id='namaDiagnosa' name='[ii][diagnosaicdix_nama]'>".$modDiagnosa->diagnosaicdix_nama."</span>
                    </td>	
                    <td>
                        <span id='namaLainnya' name='[ii][diagnosaicdix_namalainnya]'>".$modDiagnosa->diagnosaicdix_namalainnya."</span>
                    </td>	
                    <td class='hapus_diagnosa'>
                        <a onclick='bataldiagnosa(this);return false;' rel='tooltip' href='javascript:void(0);' title='lik untuk membatalkan diagnosa ini'><i class='icon-remove'></i></a>
                    </td>	
                </tr>
            ";

            $data['form'] = $form;
            echo json_encode($data);
            Yii::app()->end();
        }
    }

    public function actionSetProsedur() {
        if (Yii::app()->request->isAjaxRequest) {
            $pendaftaran_id = (isset($_POST['pendaftaran_id']) ? $_POST['pendaftaran_id'] : null);
            $form = '';

            $modProsedur = InfodiagnosaicdixinacbgV::model()->findAllByAttributes(array('pendaftaran_id' => $pendaftaran_id));
            $form = $this->renderPartial('_rowProsedur', array('modDiagnosa' => $modProsedur), true);

            $data['form'] = $form;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function simpanDagnosaX($moddiagnosaicdx,$model,$post){
        if(count($post)>0){
            ARDiagnosaxInacbgsT::model()->deleteAllByAttributes(array('inacbg_id' => $model->inacbg_id));
            foreach ($post as $key => $value) {
                $modDiagnosa = DiagnosaM::model()->findByPk($value['diagnosa_id']);
                $moddiagnosaicdx = new ARDiagnosaxInacbgsT();
                $moddiagnosaicdx->inacbg_id = $model->inacbg_id;
                $moddiagnosaicdx->sep_id = $model->sep_id;
                $moddiagnosaicdx->pendaftaran_id = $model->pendaftaran_id;
                $moddiagnosaicdx->diagnosax_kode = $modDiagnosa->diagnosa_kode;
                $moddiagnosaicdx->diagnosax_nama = $modDiagnosa->diagnosa_nama;
                $moddiagnosaicdx->diagnosax_type = $value['kelompokdiagnosa_id'];
                $moddiagnosaicdx->create_loginpemakai_id = Yii::app()->user->id;
                $moddiagnosaicdx->create_time = date('Y-m-d H:i:s');
                $moddiagnosaicdx->create_ruangan_id = Yii::app()->user->getState('ruangan_id');
                $moddiagnosaicdx->save();
            }
        }
        return $moddiagnosaicdx;
    }
    
    public function simpanDagnosaIx($moddiagnosaicdix,$model,$post){
        if(count($post)>0){
            ARDiagnosaixInacbgT::model()->deleteAllByAttributes(array('inacbg_id' => $model->inacbg_id));
            foreach ($post as $key => $value) {
                $modDiagnosa = DiagnosaicdixM::model()->findByPk($value['diagnosaicdix_id']);
                $moddiagnosaicdix = new ARDiagnosaixInacbgT();
                $moddiagnosaicdix->inacbg_id = $model->inacbg_id;
                $moddiagnosaicdix->sep_id = $model->sep_id;
                $moddiagnosaicdix->pendaftaran_id = $model->pendaftaran_id;
                $moddiagnosaicdix->diagnosaix_kode = $modDiagnosa->diagnosaicdix_kode;
                $moddiagnosaicdix->diagnosaix_nama = $modDiagnosa->diagnosaicdix_nama;
                $moddiagnosaicdix->create_loginpemakai_id = Yii::app()->user->id;
                $moddiagnosaicdix->create_time = date('Y-m-d H:i:s');
                $moddiagnosaicdix->create_ruangan_id = Yii::app()->user->getState('ruangan_id');
                $moddiagnosaicdix->save();
            }
        }
        return $moddiagnosaicdix;
    }
    
    public function simpanCbg($modinasiscbg,$model,$post){
        $modinasiscbg = new ARInasiscbgT();
        $modinasiscbg->attributes = $post;
        
        $modinasiscbg->pendaftaran_id = $model->pendaftaran_id;
        $modinasiscbg->inacbg_id = $model->inacbg_id;
        $modinasiscbg->inasiscbg_tgl = date('Y-m-d');
        $modinasiscbg->create_time = date('Y-m-d H:i:s');
        $modinasiscbg->create_loginpemakai_id = Yii::app()->user->id;
        $modinasiscbg->create_ruangan = Yii::app()->user->getState('ruangan_id');
        
        if($modinasiscbg->validate()){
            ARInasiscbgT::model()->deleteAllByAttributes(array('inacbg_id' => $model->inacbg_id));
            $modinasiscbg->save();
        }
        
        return $modinasiscbg;
    }
    
    public function simpanCmg($modinasiscmg,$model,$post){
        $modinasiscmg = new ARInasiscmgT();
        $modinasiscmg->attributes = $post;
        
        $modinasiscmg->pendaftaran_id = $model->pendaftaran_id;
        $modinasiscmg->inacbg_id = $model->inacbg_id;
        $modinasiscmg->inasiscmg_tgl = date('Y-m-d');
        if(isset($post['nama_spesialprosedure']) && $post['nama_spesialprosedure'] != "#"){
            $modinasiscmg->nama_spesialprosedure = $post['nama_spesialprosedure'];
            $modinasiscmg->nama_spesialprosedure_temp = $post['nama_spesialprosedure_temp'];
        }
        if(isset($post['nama_spesialprosthesis']) && $post['nama_spesialprosthesis'] != "#"){
            $modinasiscmg->nama_spesialprosthesis = $post['nama_spesialprosthesis'];
            $modinasiscmg->nama_spesialprosthesis_temp = $post['nama_spesialprosthesis_temp'];
        }
        if(isset($post['nama_spesialinvestigation']) && $post['nama_spesialinvestigation'] != "#"){
            $modinasiscmg->nama_spesialinvestigation = $post['nama_spesialinvestigation'];
            $modinasiscmg->nama_spesialinvestigation_temp = $post['nama_spesialinvestigation_temp'];
        }
        if(isset($post['nama_spesialdrug']) && $post['nama_spesialdrug'] != "#"){
            $modinasiscmg->nama_spesialdrug = $post['nama_spesialdrug'];
            $modinasiscmg->nama_spesialdrug_temp = $post['nama_spesialdrug_temp'];
        }
        
        $modinasiscmg->create_time = date('Y-m-d H:i:s');
        $modinasiscmg->create_loginpemakai_id = Yii::app()->user->id;
        $modinasiscmg->create_ruangan = Yii::app()->user->getState('ruangan_id');
        
        if($modinasiscmg->validate()){
            ARInasiscmgT::model()->deleteAllByAttributes(array('inacbg_id' => $model->inacbg_id));
            $modinasiscmg->save();
        }
        
        return $modinasiscmg;
    }
    
    /**
     * Menghapus data Klaim
     */
    public function actionHapusKlaim() {
        if (Yii::app()->request->isAjaxRequest) {
            $sukses = false;
            $data['sukses'] = 0;
            $data['status'] = '';
            $inacbg_id = $_GET['inacbg_id'];
            $model = ARInacbgT::model()->findByPk($inacbg_id);
            $modSep = SepT::model()->findByPk($model->sep_id);
            $modUser = LoginpemakaiK::model()->findByPk($model->create_loginpemakai_id);
            $coder_nik = $modUser->coder_nik;
            $inacbg = new Inacbg();
            $transaction = Yii::app()->db->beginTransaction();
            $reqSep = json_decode($inacbg->MenghapusKlaim($modSep->nosep, $coder_nik), true);
            if ($reqSep['metadata']['code'] == 200) {
                $sukses = true;
                ARDiagnosaixInacbgT::model()->deleteAllByAttributes(array('inacbg_id' => $model->inacbg_id));
                ARDiagnosaxInacbgsT::model()->deleteAllByAttributes(array('inacbg_id' => $model->inacbg_id));
                InasiscbgT::model()->deleteAllByAttributes(array('inacbg_id' => $model->inacbg_id));
                InasiscmgT::model()->deleteAllByAttributes(array('inacbg_id' => $model->inacbg_id));
                if ($model->delete()) {
                    $transaction->commit();
                }
            } else {
                $sukses = false;
                $transaction->rollback();
            }

            if ($sukses == false) {
                $data['status'] = 'Data gagal dihapus karena '.$reqSep['metadata']['message'];
            } else {
                $data['status'] = 'Data Klaim berhasil dihapus';
            }

            echo CJSON::encode($data);
        }
    }
    
    /**
     * Mengirim data Klaim
     */
    public function actionKirimKlaim() {
        if (Yii::app()->request->isAjaxRequest) {
            $sukses = false;
            $data['sukses'] = 0;
            $data['status'] = '';
            $inacbg_id = $_GET['inacbg_id'];
            $model = ARInacbgT::model()->findByPk($inacbg_id);
            $modSep = SepT::model()->findByPk($model->sep_id);
            $inacbg = new Inacbg();
            $transaction = Yii::app()->db->beginTransaction();
            $reqSep = json_decode($inacbg->KirimKlaimIndividual($modSep->nosep), true);
            if ($reqSep['metadata']['code'] == 200) {
                $sukses = true;
                $model->is_terkirim = TRUE;
                if($model->update()) {
                    $transaction->commit();
                }
            } else {
                $sukses = false;
                $transaction->rollback();
            }

            if ($sukses == false) {
                $data['status'] = 'Data gagal dikirim karena '.$reqSep['metadata']['message'];
            } else {
                $data['status'] = 'Data Klaim berhasil dkirim';
            }

            echo CJSON::encode($data);
        }
    }
    
    /**
     * Mengirim data Klaim
     */
    public function actionKirimKlaimKolektif() {
        if (Yii::app()->request->isAjaxRequest) {
            $sukses = false;
            $data['sukses'] = 0;
            $data['status'] = '';

            $start_dt = $_GET['start_dt'];
            $stop_dt = $_GET['stop_dt'];
            $jenis_rawat = $_GET['jenis_rawat'];
            $date_type = $_GET['date_type'];
            
            $criteria = new CDbCriteria;
            if($date_type == 1){
                $criteria->addBetweenCondition('DATE(tglrawat_keluar)', $start_dt, $stop_dt);
            }else{
                $criteria->addCondition("DATE(create_tanggal) BETWEEN '".$start_dt."' AND '".$stop_dt."' OR DATE(update_tanggal) BETWEEN '".$start_dt."' AND '".$stop_dt."'");
            }
            if($jenis_rawat == 1 || $jenis_rawat == 2){
                $criteria->addCondition("jenisrawat_inacbg = ".$jenis_rawat);
            }
            
            $inacbg = new Inacbg();
            $transaction = Yii::app()->db->beginTransaction();
            $reqSep = json_decode($inacbg->KirimKlaimPeriode($start_dt,$stop_dt,$jenis_rawat,$date_type), true);
            if ($reqSep['metadata']['code'] == 200) {
                $sukses = true;
                $model->is_terkirim = TRUE;
                $update = $model = ARInacbgT::model()->updateAll(array('is_terkirim'=>TRUE),$criteria);
                if($update) {
                    $transaction->commit();
                }
            } else {
                $sukses = false;
                $transaction->rollback();
            }

            if ($sukses == false) {
                $data['status'] = 'Data gagal dikirim karena '.$reqSep['metadata']['message'];
            } else {
                $data['status'] = 'Data Klaim berhasil dkirim';
            }

            echo CJSON::encode($data);
        }
    }

}
