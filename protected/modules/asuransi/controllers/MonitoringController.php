<?php

class MonitoringController extends Controller {

    public function actionIndex() {
        
        $this->render('index', array(
            
        ));
    }
    
    public function actionIndexKlaim() {
        
        $this->render('indexKlaim', array(
            
        ));
    }
    
    /**
    * set bpjs Interface
    */
    public function actionBpjsInterface()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            if(empty( $_GET['param'] ) OR $_GET['param'] === ''){
                    die('param can\'not empty value');
            }else{
                    $param = $_GET['param'];
            }

            $bpjs = new Bpjs();

            switch ($param) {
                    case '1':
                        $query1 = $_GET['query1'];
                        $query2 = $_GET['query2'];
                        print_r( $bpjs->search_monitoring_kunjungan($query1,$query2));
                        break;
                    case '2':
                        $query1 = $_GET['query1'];
                        $query2 = $_GET['query2'];
                        $query3 = $_GET['query2'];
                        print_r( $bpjs->search_monitoring_klaim($query1,$query2,$query3));
                        break;
                    default:
                        die('error number, please check your parameter option');
                        break;
            }
            Yii::app()->end();
        }
    }
    
    public function actionSetFormKunjungan()
    {
        if(Yii::app()->request->isAjaxRequest) { 
            $kunjunganList = $_POST['kunjunganList'];
            $form = '';
            $pesan = '';
            if(count($kunjunganList) > 0){
                foreach($kunjunganList AS $i => $kunjungan){
                    $form .= $this->renderPartial('_rowDetailKunjungan', array(
                        'model'=>$kunjungan,
                    ), true);
                }
            }else{
                $pesan = "Data tidak ada!";
            }
            
            echo CJSON::encode(array('form'=>$form, 'pesan'=>$pesan));
            Yii::app()->end(); 
        }
    }
    
    public function actionSetFormKlaim()
    {
        if(Yii::app()->request->isAjaxRequest) { 
            $klaimList = $_POST['klaimList'];
            $form = '';
            $pesan = '';
            $no = 1;
            if(count($klaimList) > 0){
                foreach($klaimList AS $i => $klaim){
                    $form .= $this->renderPartial('_rowDetailKlaim', array(
                        'model'=>$klaim,
                        'no'=>$no,
                    ), true);
                    $no++;
                }
            }else{
                $pesan = "Data tidak ada!";
            }
            
            echo CJSON::encode(array('form'=>$form, 'pesan'=>$pesan));
            Yii::app()->end(); 
        }
    }
    
    public function actionPrintDataKunjungan($param1 = null, $param2 = null)
    {
            $this->layout='//layouts/printWindows';
            $data = '';
            $data = 'MONITORING KUNJUNGAN';
            $judul_print = 'DATA '.$data.'';
            $this->render('printKunjungan', array(
                    'judul_print'=>$judul_print,
                    'param1'=>$param1,
                    'param2'=>$param2,
            ));
    }
    
    public function actionPrintDataKlaim($param1 = null, $param2 = null, $param3 = null)
    {
            $this->layout='//layouts/printWindows';
            $data = '';
            $data = 'MONITORING KLAIM';
            $judul_print = 'DATA '.$data.'';
            $this->render('printKunjungan', array(
                    'judul_print'=>$judul_print,
                    'param1'=>$param1,
                    'param2'=>$param2,
                    'param3'=>$param3,
            ));
    }

}
