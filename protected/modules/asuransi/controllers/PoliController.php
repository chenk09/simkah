<?php

class PoliController extends Controller {

    public $path_view = 'asuransi.views.poli.';

    public function actionIndex() {

        $this->render($this->path_view . 'index', array(
        ));
    }

    /**
     * set bpjs Interface
     */
    public function actionBpjsInterface() {
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            if (empty($_GET['param']) OR $_GET['param'] === '') {
                die('param can\'not empty value');
            } else {
                $param = $_GET['param'];
            }

            $bpjs = new Bpjs();

            switch ($param) {
                case '1':
                    $query = $_GET['query'];
                    $query = explode(" ", $query);
                    $query = $query[0];
                    print_r($bpjs->search_poli($query));
                    break;
                default:
                    die('error number, please check your parameter option');
                    break;
            }
            Yii::app()->end();
        }
    }

    /**
     * @param type $poli - katakunci
     */
    public function actionPrintData($poli = null) {
        $this->layout='//layouts/printWindows';
        $format = new CustomFormat;
        $data = '';
        if (!empty($poli)) {
            $data = 'POLI';
        }
        $judul_print = 'DATA ' . $data . '';
        $this->render($this->path_view . 'print', array(
            'format' => $format,
            'judul_print' => $judul_print,
        ));
    }

    public function actionSetFormPoli() {
        if (Yii::app()->request->isAjaxRequest) {
            $poliList = $_POST['poliList'];
            $form = '';
            $pesan = '';
            if (count($poliList) > 0) {
                foreach ($poliList AS $i => $poli) {
                    $kdPoli = $poli['kode'];
                    $nmPoli = $poli['nama'];
                    $form .= $this->renderPartial($this->path_view . '_rowDetail', array(
                        'kdPoli' => $kdPoli,
                        'nmPoli' => $nmPoli
                            ), true);
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }

}
