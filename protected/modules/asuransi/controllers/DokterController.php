<?php
class DokterController extends Controller{
	
	public $path_view = 'asuransi.views.dokter.';
	
	public function actionIndex(){
		
		$this->render($this->path_view.'index',array(
			
		));
	}
	
	/**
	* set bpjs Interface
	*/
	public function actionBpjsInterface()
	{
		if(Yii::app()->getRequest()->getIsAjaxRequest()) {
			if(empty( $_GET['param'] ) OR $_GET['param'] === ''){
				die('param can\'not empty value');
			}else{
				$param = $_GET['param'];
			}

 //                if(empty( $_GET['server'] ) OR $_GET['server'] === ''){
 //                    
 //                }else{
 //                    $server = 'http://'.$_GET['server'];
 //                }

			$bpjs = new Bpjs();

			switch ($param) {
				case '1':
					$query = $_GET['query'];
					$query = explode(" ",$query);
					$query = $query[0];
					$start = 1;
					$limit = 10;
					print_r( $bpjs->search_dokter($query,$start, $limit) );
					break;
				default:
					die('error number, please check your parameter option');
					break;
			}
			Yii::app()->end();
		}
	}
	
	/**
	* @param type $faskes - katakunci
	*/
	public function actionPrintData($dokter = null)
	{
		$this->layout='//layouts/printWindows';
		$format = new CustomFormat;
		$data = '';
		if(!empty($dokter)){
			$data = 'DOKTER DPJP';
		}
		$judul_print = 'DATA '.$data.'';
		$this->render($this->path_view.'print', array(
			'format'=>$format,
			'judul_print'=>$judul_print,
		));
	} 
	
    public function actionSetFormDokter()
    {
        if(Yii::app()->request->isAjaxRequest) { 
            $dokterList = $_POST['dokterList'];
			$form = '';
			$pesan = '';
            if(count($dokterList) > 0){
                foreach($dokterList AS $i => $dokter){
					$kode = $dokter['kode'];
					$nama = $dokter['nama'];
                    $form .= $this->renderPartial($this->path_view.'_rowDetail', array(
						'kode'=>$kode,
						'nama'=>$nama,
					), true);
                }
            }else{
                $pesan = "Data tidak ada!";
            }
            
            echo CJSON::encode(array('form'=>$form, 'pesan'=>$pesan));
            Yii::app()->end(); 
        }
    }
}