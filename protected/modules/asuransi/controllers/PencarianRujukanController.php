<?php
class PencarianRujukanController extends Controller{
	
    protected $path_view = 'asuransi.views.pencarianRujukan.';

    public function actionIndex() {

        $this->render($this->path_view . 'index', array(
        ));
    }

    /**
     * set bpjs Interface
     */
    public function actionBpjsInterface() {
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            if (empty($_GET['param']) OR $_GET['param'] === '') {
                die('param can\'not empty value');
            } else {
                $param = $_GET['param'];
                $jenisfaskes = $_GET['jenisfaskes'];
            }

            $bpjs = new Bpjs();

            switch ($param) {
                case '1':
                    $query = $_GET['query'];
                    print_r($bpjs->search_rujukan_norujukan($query,$jenisfaskes));
                    break;
                case '2':
                    $query = $_GET['query'];
                    print_r($bpjs->search_rujukan_peserta($query,$jenisfaskes));
                    break;
                case '100':
                    print_r($bpjs->help());
                    break;
                default:
                    die('error number, please check your parameter option');
                    break;
            }
            Yii::app()->end();
        }
    }

    /**
     * @param type $sep_id
     */
    public function actionPrintPesertaBpjs($nokartu = null, $nonik = null) {
        $this->layout = '//layouts/printWindows';
        $format = new CustomFormat;
        
        $jenisPencarian = '';
        if(!empty($nokartu)){
            $jenisPencarian = "RUJUKAN";
        }else{
            $jenisPencarian = "PESERTA";
        }
        
        $judul_print = 'DATA PESERTA BPJS';
        $this->render($this->path_view . 'print', array(
            'format' => $format,
            'judul_print' => $judul_print,
            'jenisPencarian' => $jenisPencarian,
        ));
    }
}