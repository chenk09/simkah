<?php
class PencarianSepController extends Controller{
	
	protected $path_view = 'asuransi.views.pencarianSep.';
	
	public function actionIndex()
	{
	
		$this->render($this->path_view.'index',array(
			
		));
	}
	
	/**
	* set bpjs Interface
	*/
	public function actionBpjsInterface()
	{
		if(Yii::app()->getRequest()->getIsAjaxRequest()) {
			if(empty( $_GET['param'] ) OR $_GET['param'] === ''){
				die('param can\'not empty value');
			}else{
				$param = $_GET['param'];
			}

 //                if(empty( $_GET['server'] ) OR $_GET['server'] === ''){
 //                    
 //                }else{
 //                    $server = 'http://'.$_GET['server'];
 //                }

			$bpjs = new Bpjs();

			switch ($param) {
				case '1':
					$query = $_GET['query'];
					print_r( $bpjs->search_sep($query) );
					break;
				default:
					die('error number, please check your parameter option');
					break;
			}
			Yii::app()->end();
		}
	}
	
	/**
	* @param type $sep_id
	*/
	public function actionPrintPecarianSep($nokartu=null,$nonik=null)
	{
		$this->layout='//layouts/printWindows';
		$format = new CustomFormat;
		
		$judul_print = 'DATA SEP BPJS';
		$this->render($this->path_view.'printPencarianSep', array(
			'format'=>$format,
			'judul_print'=>$judul_print,
		));
	} 
}