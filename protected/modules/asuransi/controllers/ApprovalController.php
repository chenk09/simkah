<?php

class ApprovalController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $defaultAction = 'admin';
    public $septersimpan = false;
    public $updateapprove = false;
    public $deleteapprove = false;
    public $brigingapprove = true;

    /**
     * Membuat dan menyimpan data baru.
     */
    public function actionCreate($id = null) {
        $status = '';
        $model = new ARPengajuanapprovalsepT;
        $modInfoKunjungan = new ARInfokunjunganrsSepV;
        $model->kode_ppk_pelayanan = Yii::app()->user->getState('kode_ppk_bpjs');
        $model->nama_ppk_pelayanan = Yii::app()->user->getState('nama_ppk_pelayanan');
        $model->tgl_sep = date('Y-m-d');
        $model->carabayar_id = 18;
        $model->jenisrujukan = 1;
        $model->poli_eksekutif = 0;
        $model->cob = 0;
        $model->lakalantas = 0;
        $model->cob_status = "TIDAK";
        $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
        if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
            $model->userpembuat_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
            $model->user_approval_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
        }else{
            $model->userpembuat_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
            $model->user_approval_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
        }
        
        if (!empty($id)) {
            $model = ARPengajuanapprovalsepT::model()->findByPk($id);
            $model->lakalantas = ($model->lakalantas == false)? 0 : 1;
            $model->cob = ($model->cob == false)? 0 : 1;
            $model->poli_eksekutif = ($model->poli_eksekutif == false)? 0 : 1;
            $model->jenisrujukan = ($model->jenisrujukan == 1)? 1 : 2;
            $modInfoKunjungan = ARInfokunjunganrsSepV::model()->findByAttributes(array('pendaftaran_id' => $model->pendaftaran_id));
        }

        if (isset($_POST['ARPengajuanapprovalsepT'])) {
            
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes = $_POST['ARPengajuanapprovalsepT'];
                $model = $this->simpanApproval($model, $_POST['ARPengajuanapprovalsepT']);
                
                if ($model) {
                    if ($this->brigingapprove == false) {
                        $status = 'Data gagal disimpan karena koneksi server BPJS terputus! Silahkan hubungi admin SIMRS';
                    } else if ($this->septersimpan == false) {
                        $status = 'Data gagal disimpan karna kesalahan data / database!';
                    } else {
                        $status = 'Data Approve berhasil disimpan';
                    }
                    if ($this->septersimpan && $this->brigingapprove) {
                        $transaction->commit();
//                        Yii::app()->user->setFlash('success', $status);
                        $this->redirect(array('create', 'id' => $model->pengajuanapprovalsep_id, 'sukses' => 1));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', $status);
                    }
                }
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data SEP gagal disimpan ! " . MyExceptionMessage::getMessage($e, true));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modInfoKunjungan' => $modInfoKunjungan,
        ));
    }
    
    /**
     * Membuat dan menyimpan data baru.
     */
    public function actionCreateSep($id = null) {
        $this->layout = '//layouts/frameDialog';
        $status = '';
        $model = new ARPengajuanapprovalsepT;
        $modInfoKunjungan = new ARInfokunjunganrsSepV;
        $model->kode_ppk_pelayanan = Yii::app()->user->getState('kode_ppk_bpjs');
        $model->nama_ppk_pelayanan = Yii::app()->user->getState('nama_ppk_pelayanan');
        $model->tgl_sep = date('Y-m-d');
        $model->carabayar_id = 18;
        $model->jenisrujukan = 1;
        $model->poli_eksekutif = 0;
        $model->cob = 0;
        
        $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
        if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
            $model->userpembuat_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
            $model->user_approval_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
        }else{
            $model->userpembuat_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
            $model->user_approval_bpjs = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
        }
        
        if (!empty($id)) {
            $model = ARPengajuanapprovalsepT::model()->findByPk($id);
            $model->lakalantas = ($model->lakalantas == false)? 0 : 1;
            $model->cob = ($model->cob == false)? 0 : 1;
            $model->poli_eksekutif = ($model->poli_eksekutif == false)? 0 : 1;
            $model->jenisrujukan = ($model->jenisrujukan == 1)? 1 : 2;
            $modInfoKunjungan = ARInfokunjunganrsSepV::model()->findByAttributes(array('pendaftaran_id' => $model->pendaftaran_id));
            $model->lakalantas = 0;
            if($model->cob==1){
                $model->cob_status = "YA";
            }else{
                $model->cob_status = "TIDAK";
            }
        }

        if (isset($_POST['ARPengajuanapprovalsepT'])) {
            
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->attributes = $_POST['ARPengajuanapprovalsepT'];
                $model = $this->simpanApprovalSep($model, $_POST['ARPengajuanapprovalsepT']);
                
                if ($model) {
                    if ($this->brigingapprove == false) {
                        $status = 'Data gagal disimpan karena koneksi server BPJS terputus! Silahkan hubungi admin SIMRS';
                    } else if ($this->septersimpan == false) {
                        $status = 'Data gagal disimpan karna kesalahan data / database!';
                    } else {
                        $status = 'Data Approve berhasil disimpan';
                    }
                    if ($this->septersimpan && $this->brigingapprove) {
                        $transaction->commit();
//                        Yii::app()->user->setFlash('success', $status);
                        $this->redirect(array('CreateSep', 'id' => $model->pengajuanapprovalsep_id, 'sukses' => 1));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', $status);
                    }
                }
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data SEP gagal disimpan ! " . MyExceptionMessage::getMessage($e, true));
            }
        }

        $this->render('create_sep', array(
            'model' => $model,
            'modInfoKunjungan' => $modInfoKunjungan,
        ));
    }

    /**
     * Melihat daftar data.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('ARPengajuanapprovalsepT');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Pengaturan data.
     */
    public function actionAdmin() {
        $format = new CustomFormat();
        $model = new ARPengajuanapprovalsepT;
        $model->unsetAttributes();  // clear any default values
        $model->tgl_awal = date('Y-m-d');
        $model->tgl_akhir = date('Y-m-d');
        if (isset($_GET['ARPengajuanapprovalsepT'])) {
            $model->attributes = $_GET['ARPengajuanapprovalsepT'];
            $model->tgl_awal = isset($_GET['ARPengajuanapprovalsepT']['tgl_awal']) ? $_GET['ARPengajuanapprovalsepT']['tgl_awal'] : null;
            $model->tgl_akhir = isset($_GET['ARPengajuanapprovalsepT']['tgl_akhir']) ? $_GET['ARPengajuanapprovalsepT']['tgl_akhir'] : null;
            $model->no_pendaftaran = $_GET['ARPengajuanapprovalsepT']['no_pendaftaran'];
            $model->no_kartu_bpjs = $_GET['ARPengajuanapprovalsepT']['no_kartu_bpjs'];
        }
        $this->render('admin_new', array(
            'model' => $model,
        ));
    }

    /**
     * Memanggil data dari model.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = ARPengajuanapprovalsepT::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'assep-t-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Mencetak data
     */
    public function actionPrint() {
        $model = new ARPengajuanapprovalsepT;
        $model->attributes = $_REQUEST['ARPengajuanapprovalsepT'];
        $model->tgl_awal = $_REQUEST['ARPengajuanapprovalsepT']['tgl_awal'];
        $model->tgl_akhir = $_REQUEST['ARPengajuanapprovalsepT']['tgl_akhir'];
        $judulLaporan = 'Data Approve';
        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == 'PRINT') {
            $this->layout = '//layouts/printWindows';
            $this->render('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas'); //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas'); //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->mirrorMargins = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    /**
     * set bpjs Interface
     */
    public function actionBpjsInterface() {
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            if (empty($_GET['param']) OR $_GET['param'] === '') {
                die('param can\'not empty value');
            } else {
                $param = $_GET['param'];
            }
            $jenis_rujukan = isset($_GET['jenis_rujukan'])? $_GET['jenis_rujukan'] : 1;

            $bpjs = new Bpjs();

            switch ($param) {
                case '1':
                    $query = $_GET['query'];
                    print_r($bpjs->search_kartu($query));
                    break;
                case '2':
                    $query = $_GET['query'];
                    print_r($bpjs->search_nik($query));
                    break;
                case '3':
                    $query = $_GET['query'];
                    if($jenis_rujukan==1){
                        print_r($bpjs->search_rujukan_no_rujukan($query));
                    }else{
                        print_r($bpjs->search_rujukan_no_rujukan_rs($query));
                    }
                    break;
                case '4':
                    $query = $_GET['query'];
                    print_r($bpjs->search_rujukan_no_bpjs($query));
                    break;
                case '13':
                    $format = new CustomFormat();
                    $noMR = $_GET['noMR'];
                    $noKartu = $_GET['noKartu'];
                    $tglSep = date('Y-m-d', strtotime($_GET['tglSep']));
                    $ppkPelayanan = $_GET['ppkPelayanan'];
                    $jnsPelayanan = $_GET['jnsPelayanan'];
                    $klsRawat = $_GET['klsRawat'];
                    $asalRujukan = $_GET['asalRujukan'];
                    $tglRujukan = date('Y-m-d', strtotime($_GET['tglRujukan']));
                    $noRujukan = $_GET['noRujukan'];
                    $ppkRujukan = $_GET['ppkRujukan'];
                    $catatan = $_GET['catatan'];
                    $diagAwal = $_GET['diagAwal'];
                    $tujuan = $_GET['tujuan'];
                    $eksekutif = $_GET['eksekutif'];
                    $cob = $_GET['cob'];
                    $lakaLantas = $_GET['lakaLantas'];
                    $penjamin = $_GET['penjamin'];
                    $lokasiLaka = $_GET['lokasiLaka'];
                    $noTelp = $_GET['noTelp'];
                    $user = $_GET['user'];
                    
                    print_r($bpjs->pengajuan_approval($noKartu,$tglSep,$ppkPelayanan,$jnsPelayanan,$klsRawat,$noMR,$asalRujukan,$tglRujukan,$noRujukan,$ppkRujukan,$catatan,$diagAwal,$tujuan,$eksekutif,$cob,$lakaLantas,$penjamin,$lokasiLaka,$noTelp,$user));
                    break;
                case '14':
                    $format = new CustomFormat();
                    $noKartu = $_GET['noKartu'];
                    $tglSep = date('Y-m-d', strtotime($_GET['tglSep']));
                    $jnsPelayanan = $_GET['jnsPelayanan'];
                    $catatan = $_GET['catatan'];
                    $user = $_GET['user'];
                    
                    print_r($bpjs->approval_sep($noKartu,$tglSep,$jnsPelayanan,$catatan,$user));
                    break;
                case '15':
                    $format = new CustomFormat();
                    $noMR = $_GET['noMR'];
                    $noKartu = $_GET['noKartu'];
                    $tglSep = date('Y-m-d', strtotime($_GET['tglSep']));
                    $ppkPelayanan = $_GET['ppkPelayanan'];
                    $jnsPelayanan = $_GET['jnsPelayanan'];
                    $klsRawat = $_GET['klsRawat'];
                    $asalRujukan = $_GET['asalRujukan'];
                    $tglRujukan = date('Y-m-d', strtotime($_GET['tglRujukan']));
                    $noRujukan = $_GET['noRujukan'];
                    $ppkRujukan = $_GET['ppkRujukan'];
                    $catatan = $_GET['catatan'];
                    $diagAwal = $_GET['diagAwal'];
                    $tujuan = $_GET['tujuan'];
                    $eksekutif = $_GET['eksekutif'];
                    $cob = $_GET['cob'];
                    $lakaLantas = $_GET['lakaLantas'];
                    $penjamin = $_GET['penjamin'];
                    $lokasiLaka = $_GET['lokasiLaka'];
                    $noTelp = $_GET['noTelp'];
                    $user = $_GET['user'];
                    
                    print_r($bpjs->create_sep_new($noKartu,$tglSep,$ppkPelayanan,$jnsPelayanan,$klsRawat,$noMR,$asalRujukan,$tglRujukan,$noRujukan,$ppkRujukan,$catatan,$diagAwal,$tujuan,$eksekutif,$cob,$lakaLantas,$penjamin,$lokasiLaka,$noTelp,$user));
                    break;
                case '100':
                    print_r($bpjs->help());
                    break;
                default:
                    die('error number, please check your parameter option');
                    break;
            }
            Yii::app()->end();
        }
    }

    public function simpanApproval($model, $postApp) {

        $model->attributes = $postApp;
        $model->tgl_sep = date('Y-m-d H:i:s', strtotime($model->tgl_sep));
        $model->pendaftaran_id = $_POST['pendaftaran_id'];
        $model->jenis_pelayanan = $postApp['jnspelayanan_kode'];
        
        $model->namapeserta_bpjs = $postApp['namapeserta_bpjs'];
        $model->jenispeserta_bpjs_kode = $postApp['jenispeserta_bpjs_kode'];
        $model->jenispeserta_bpjs_nama = $postApp['jenispeserta_bpjs_nama'];
        $model->jenis_pelayanan = $postApp['jenis_pelayanan'];
        $model->hakkelas_kode = $postApp['hakkelas_kode'];
        $model->diagnosa_awal_nama = $postApp['diagnosa_awal_nama'];
        $model->politujuan = $postApp['politujuan'];
        $model->politujuan_nama = $postApp['politujuan_nama'];
        $model->no_asuransi_cob = $postApp['no_asuransi_cob'];
        $model->nama_asuransi_cob = $postApp['nama_asuransi_cob'];
        $model->lokasilakalantas = isset($postApp['lokasilakalantas'])? $postApp['lokasilakalantas'] : null;
        $model->penjamin = isset($postApp['penjamin'])? $postApp['penjamin'] : null;
        $model->asal_rujukan = ($postApp['jenisrujukan']==1)? "PCare" : "Rumah Sakit";
        $model->tgl_rujukan = date('Y-m-d H:i:s', strtotime($model->tgl_rujukan));
        $model->create_time = date('Y-m-d H:i:s');
        $model->create_loginpemakai_id = Yii::app()->user->id;
        
        if ($model->save()) {
            $this->septersimpan = true;
        } else {
            $this->septersimpan = false;
        }

        return $model;
    }
    
    public function simpanApprovalSep($model, $postApp) {

        $model->attributes = $postApp;
        $model->jenis_pelayanan = $postApp['jnspelayanan_kode'];
        
        $model->jenis_pelayanan = $postApp['jenis_pelayanan'];
        $model->diagnosa_awal_nama = $postApp['diagnosa_awal_nama'];
        $model->politujuan = $postApp['politujuan'];
        $model->politujuan_nama = $postApp['politujuan_nama'];
        $model->lokasilakalantas = isset($postApp['lokasilakalantas'])? $postApp['lokasilakalantas'] : null;
        $model->penjamin = isset($postApp['penjamin'])? $postApp['penjamin'] : null;
        $model->asal_rujukan = ($postApp['jenisrujukan']==1)? "PCare" : "Rumah Sakit";
        
        $modSep = new ARSepT;
        $modSep->attributes = $model;
        if($model->jenis_pelayanan == 2){
            $modSep->jnspelayanan_nama = "Rawat Jalan";
        }else if($model->jenis_pelayanan == 1){
            $modSep->jnspelayanan_nama = "Rawat Inap";
        }
        $modSep->tglsep = $model->tgl_sep;
        $modSep->no_sep = $postApp['no_sep'];
        $modSep->pasien_id = $_POST['pasien_id'];
        $modSep->pendaftaran_id = $model->pendaftaran_id;
        $modSep->nopeserta_bpjs = $model->no_kartu_bpjs;
        $modSep->namapeserta_bpjs = $model->namapeserta_bpjs;
        $modSep->hakkelas_kode = $model->hakkelas_kode;
        $modSep->hakkelas_nama = $model->hakkelas_kode;
        $modSep->notelpon_peserta = $model->no_telepon_pasien;
        $modSep->norujukan_bpjs = $model->no_rujukan;
        $modSep->tglrujukan_bpjs = $model->tgl_rujukan;
        $modSep->jenisrujukan_kode_bpjs = $model->jenisrujukan;
        $modSep->jenisrujukan_nama_bpjs = $model->asal_rujukan;
        $modSep->ppkrujukanasal_kode = $model->kode_ppk_rujukan;
        $modSep->ppkrujukanasal_nama = $model->nama_ppk_rujukan;
        $modSep->jnspelayanan_kode = $model->jenis_pelayanan;
        $modSep->diagnosaawal_kode = $model->diagnosa_awal;
        $modSep->diagnosaawal_nama = $model->diagnosa_awal_nama;
        $modSep->politujuan_kode = $model->politujuan;
        $modSep->politujuan_nama = $model->politujuan_nama;
        $modSep->kelasrawat_kode = $modSep->kelas_tanggungan;
        $modSep->kelasrawat_nama = $modSep->kelas_tanggungan;
        $modSep->cob_bpjs = $model->cob;
        $modSep->polieksekutif = $model->poli_eksekutif;
        $modSep->lakalantas_kode = $model->lakalantas;
        $modSep->lakalantas_nama = $model->lakalantas;
        $modSep->penjaminlakalantas = $model->penjamin;
        $modSep->lokasilakalantas = $model->lokasilakalantas;
        $modSep->catatan_sep = $model->catatan;
        $modSep->jenispeserta_bpjs_kode = $model->jenispeserta_bpjs_kode;
        $modSep->jenispeserta_bpjs_nama = $model->jenispeserta_bpjs_nama;
        $modSep->no_asuransi_cob = $model->no_asuransi_cob;
        $modSep->nama_asuransi_cob = $model->nama_asuransi_cob;
        $modSep->create_time = date('Y-m-d H:i:s');
        $modSep->create_loginpemakai_id = Yii::app()->user->id;
        $modSep->create_ruangan = Yii::app()->user->getState('ruangan_id');

        if ($modSep->save()) {
            $model->sep_id = $modSep->sep_id;
            $model->save();

            $modPendaftaran = PendaftaranT::model()->findByPk($model->pendaftaran_id);
            $modPendaftaran->sep_id = $modSep->sep_id;
            $modPendaftaran->update();
            
            $this->septersimpan = true;
        } else {
            $this->septersimpan = false;
        }

        return $model;
    }

    /**
     * untuk menampilkan pasien lama dari autocomplete
     * 1. no_rekam_medik
     */
    public function actionAutocompletePasien() {
        if (Yii::app()->request->isAjaxRequest) {
            $format = new MyFormatter();
            $returnVal = array();
            $no_rekam_medik = isset($_GET['no_rekam_medik']) ? $_GET['no_rekam_medik'] : null;

            $criteria = new CDbCriteria();
            $criteria->compare('LOWER(no_rekam_medik)', strtolower($no_rekam_medik), true);
            $criteria->addCondition('ispasienluar = FALSE');
            $criteria->order = 'no_rekam_medik, nama_pasien';
            $criteria->limit = 50;
            $models = PasienM::model()->findAll($criteria);
            foreach ($models as $i => $model) {
                $attributes = $model->attributeNames();
                foreach ($attributes as $j => $attribute) {
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->no_rekam_medik . ' - ' . $model->nama_pasien . (!empty($model->nama_bin) ? "(" . $model->nama_bin . ")" : "") . " - " . (!empty($model->nama_ayah) ? $model->nama_ayah : "(nama ayah tidak ada)") . " - " . $format->formatDateTimeForUser($model->tanggal_lahir);
                $returnVal[$i]['value'] = $model->no_rekam_medik;
            }
            echo CJSON::encode($returnVal);
        } else
            throw new CHttpException(403, 'Tidak dapat mengurai data');
        Yii::app()->end();
    }

    /**
     * @param type $sep_id
     */
    public function actionPrintSep($sep_id) {
        $this->layout = '//layouts/printWindows';
        $modSep = ARSepT::model()->findByPk($sep_id);
        $modInfoKunjungan = PendaftaranT::model()->findByPk($modSep->pendaftaran_id);
        
        $judul_print = 'SURAT ELIGIBILITAS PESERTA';
        $this->render('printSep', array(
            'modSep' => $modSep,
            'modInfoKunjungan' => $modInfoKunjungan,
            'judul_print' => $judul_print,
        ));
    }

    /**
     * Ubah Tanggal Pulang
     */
    public function actionApprove($id) {
        $this->layout = '//layouts/frameDialog';
        $model = ARPengajuanapprovalsepT::model()->findByPk($id);
        $modInfoKunjungan = ARInfokunjunganrsSepV::model()->findByAttributes(array('pendaftaran_id'=>$model->pendaftaran_id));
        $bpjs = new Bpjs();
        $status = '';
        if (isset($_POST['ARPengajuanapprovalsepT'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model->is_approval = TRUE;
                if($model->save()){
                    $transaction->commit();
                    $this->redirect(array('Approve', 'id' => $id, 'sukses' => 1));
                }else{
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error', "Data Gagal disimpan");
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', '<strong>Gagal</strong> Data Gagal disimpan' . MyExceptionMessage::getMessage($exc));
            }
        }

        $this->render('_approve', array(
            'model' => $model,
            'modInfoKunjungan' => $modInfoKunjungan,
        ));
    }

    /**
     * Mengurai data pasien berdasarkan:
     * - instalasi_id
     * - pendaftaran_id
     * - pasienadmisi_id
     * - no_pendaftaran
     * - no_rekam_medik
     * @throws CHttpException
     */
    public function actionGetDataInfoPasien() {
        if (Yii::app()->request->isAjaxRequest) {
            $instalasi_id = isset($_POST['instalasi_id']) ? $_POST['instalasi_id'] : null;
            $pendaftaran_id = isset($_POST['pendaftaran_id']) ? $_POST['pendaftaran_id'] : null;
            $pasienadmisi_id = isset($_POST['pasienadmisi_id']) ? $_POST['pasienadmisi_id'] : null;
            $no_pendaftaran = isset($_POST['no_pendaftaran']) ? $_POST['no_pendaftaran'] : null;
            $no_rekam_medik = isset($_POST['no_rekam_medik']) ? $_POST['no_rekam_medik'] : null;
            $returnVal = array();
            $criteria = new CDbCriteria();
            if (!empty($pendaftaran_id)) {
                $criteria->addCondition("pendaftaran_id = " . $pendaftaran_id);
            }
            if (!empty($pasienadmisi_id) && $pasienadmisi_id !== 'null') {
                $criteria->addCondition("pasienadmisi_id = " . $pasienadmisi_id);
            }
            $criteria->compare('LOWER(no_pendaftaran)', strtolower(trim($no_pendaftaran)));
            $criteria->compare('LOWER(no_rekam_medik)', strtolower(trim($no_rekam_medik)));
            $model = ARInfokunjunganrsSepV::model()->find($criteria);
            $attributes = $model->attributeNames();
            foreach ($attributes as $j => $attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }
            echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }
    
    public function actionAutocompleteInfoPasien() {
        if(Yii::app()->request->isAjaxRequest) {
            $returnVal = array();
            $instalasi_id = isset($_GET['instalasi_id']) ? $_GET['instalasi_id'] : null;
            $pendaftaran_id = isset($_GET['pendaftaran_id']) ? $_GET['pendaftaran_id'] : null;
            $pasienadmisi_id = isset($_GET['pasienadmisi_id']) ? $_GET['pasienadmisi_id'] : null;
            $no_pendaftaran = isset($_GET['no_pendaftaran']) ? $_GET['no_pendaftaran'] : null;
            $no_rekam_medik = isset($_GET['no_rekam_medik']) ? $_GET['no_rekam_medik'] : null;
            $nama_pasien = isset($_GET['nama_pasien']) ? $_GET['nama_pasien'] : null;
            $criteria = new CDbCriteria();
            if(!empty($instalasi_id)){
                $criteria->addCondition('instalasi_id = '.$instalasi_id);
            }
            $criteria->compare('LOWER(no_rekam_medik)', strtolower($no_rekam_medik), true);
            $criteria->compare('LOWER(no_pendaftaran)', strtolower($no_pendaftaran), true);
            $criteria->compare('LOWER(nama_pasien)', strtolower($nama_pasien), true);
            $criteria->limit = 5;
            $models = ARInfokunjunganrsSepV::model()->findAll($criteria);
            foreach($models as $i=>$model)
            {
                $attributes = $model->attributeNames();
                foreach($attributes as $j=>$attribute) {                    
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->no_pendaftaran.' - '.$model->nama_pasien;
                $returnVal[$i]['value'] = $model->no_pendaftaran;
            }

            echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }
    
    public function actionSetFormPoli() {
        if (Yii::app()->request->isAjaxRequest) {
            $poliList = $_POST['poliList'];
            $form = '';
            $pesan = '';
            if (count($poliList) > 0) {
                foreach ($poliList AS $i => $poli) {
                    $kdPoli = $poli['kode'];
                    $nmPoli = $poli['nama'];
                    $form .= 
                    "<tr>
                        <td>
                            <a class='btn-small' href='javascript:void(0);' onclick=\"$('#ARPengajuanapprovalsepT_politujuan').val('".$kdPoli."');$('#ARPengajuanapprovalsepT_politujuan_nama').val('".$nmPoli."');$('#dialogPoli').dialog('close'); \">
                            <i class='icon-check'></i></a>
                        </td>
                        <td>
                            <span id='kdPoli' name=[ii][kdPoli]'>".$kdPoli."</span>
                        </td>
                        <td>
                            <span id='nmPoli' name=[ii][nmPoli]'>".$nmPoli."</span>
                        </td>
                    </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }
    
    public function actionsetFormDiagnosa() {
        if (Yii::app()->request->isAjaxRequest) {
            $diagnosaList = $_POST['diagnosaList'];
            $form = '';
            $pesan = '';
            if (count($diagnosaList) > 0) {
                foreach ($diagnosaList AS $i => $diagnosa) {
                    $kddiagnosa = $diagnosa['kode'];
                    $nmdiagnosa = $diagnosa['nama'];
                    $form .= 
                    "<tr>
                        <td>
                            <a class='btn-small' href='javascript:void(0);' onclick=\"$('#ARPengajuanapprovalsepT_diagnosa_awal').val('".$kddiagnosa."');$('#ARPengajuanapprovalsepT_diagnosa_awal_nama').val('".$nmdiagnosa."');$('#dialogDiagnosaBpjs').dialog('close'); \">
                            <i class='icon-check'></i></a>
                        </td>
                        <td>
                            <span id='kdPoli' name=[ii][kdPoli]'>".$kddiagnosa."</span>
                        </td>
                        <td>
                            <span id='nmPoli' name=[ii][nmPoli]'>".$nmdiagnosa."</span>
                        </td>
                    </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }
    
    public function actionsetFormFaskes() {
        if (Yii::app()->request->isAjaxRequest) {
            $faskesList = $_POST['faskesList'];
            $form = '';
            $pesan = '';
            if (count($faskesList) > 0) {
                foreach ($faskesList AS $i => $faskes) {
                    $kdfaskes = $faskes['kode'];
                    $nmfaskes = $faskes['nama'];
                    $form .= 
                    "<tr>
                        <td>
                            <a class='btn-small' href='javascript:void(0);' onclick=\" $('#ARPengajuanapprovalsepT_kode_ppk_rujukan').val('".$kdfaskes."');$('#ARPengajuanapprovalsepT_nama_ppk_rujukan').val('".$nmfaskes."');$('#dialogPpk').dialog('close'); \">
                            <i class='icon-check'></i></a>
                        </td>
                        <td>
                            <span id='kdPoli' name=[ii][kdPoli]'>".$kdfaskes."</span>
                        </td>
                        <td>
                            <span id='nmPoli' name=[ii][nmPoli]'>".$nmfaskes."</span>
                        </td>
                    </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }
    
}
