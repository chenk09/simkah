<script type="text/javascript">
function cariDataMonitoringKunjungan(param){
    var param1 = $('#tgl_sep').val();
    var param2 = $('#jenis_pelayanan').val();
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	
    var setting = {
        url : "<?php echo $this->createUrl('bpjsInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param='+ param + '&query1=' + param1 + '&query2=' +param2,
        beforeSend: function(){
            $("#animation").addClass("loader");
        },
        success: function(data){
            $("#animation").removeClass("loader");
            var obj = JSON.parse(data);
            var obj1 = JSON.parse(data);
            if(obj1.metaData.message != 'Sukses'){
                alert(obj1.metaData.message);
                $("#table-kunjungan > tbody").detach();
                $("#animation").removeClass("loader");
            }else{
                var list = obj.response.sep;
                $.ajax({
                    type:'POST',
                    url:'<?php echo $this->createUrl('SetFormKunjungan'); ?>',
                    data: {kunjunganList:list},//
                    dataType: "json",
                    success:function(data){
                            $("#table-kunjungan > tbody > tr").remove();
                            $('#table-kunjungan > tbody').append(data.form);
                            renameInputRow($("#table-kunjungan"));    
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
                });
                // OVERWRITES old selecor
                jQuery.expr[':'].contains = function(a, i, m) {
                  return jQuery(a).text().toUpperCase()
                          .indexOf(m[3].toUpperCase()) >= 0;
                };
            }
            
        },
        error: function(data){
            $("#animation").removeClass("loader");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

function cariDataMonitoringKlaim(param){
    var param1 = $('#tgl_pulang').val();
    var param2 = $('#jenis_pelayanan').val();
    var param3 = $('#status_klaim').val();
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	
    var setting = {
        url : "<?php echo $this->createUrl('bpjsInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param='+ param + '&query1=' + param1 + '&query2=' +param2+ '&param3=' +param3,
        beforeSend: function(){
            $("#animation").addClass("loader");
        },
        success: function(data){
            $("#animation").removeClass("loader");
            var obj = JSON.parse(data);
            var obj1 = JSON.parse(data);
            if(obj1.metaData.message != 'Sukses'){
                alert(obj1.metaData.message);
                $("#table-klaim > tbody").detach();
                $("#animation").removeClass("loader");
            }else{
                var list = obj.response.klaim;
                $.ajax({
                    type:'POST',
                    url:'<?php echo $this->createUrl('SetFormKlaim'); ?>',
                    data: {kunjunganList:list},//
                    dataType: "json",
                    success:function(data){
                            $("#table-klaim > tbody > tr").remove();
                            $('#table-klaim > tbody').append(data.form);
                            renameInputRow($("#table-klaim"));    
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
                });
                // OVERWRITES old selecor
                jQuery.expr[':'].contains = function(a, i, m) {
                  return jQuery(a).text().toUpperCase()
                          .indexOf(m[3].toUpperCase()) >= 0;
                };
            }
        },
        error: function(data){
            $("#animation").removeClass("loader");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

/**
* rename input grid
*/ 
function renameInputRow(obj_table){
    var row = 0;
    $(obj_table).find("tbody > tr").each(function(){
        $(this).find("#no_urut").val(row+1);
        $(this).find('span').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("name","["+row+"]["+old_name_arr[2]+"]");
            }
        });
        $(this).find('input,select,textarea').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("id",old_name_arr[0]+"_"+row+"_"+old_name_arr[2]);
                $(this).attr("name",old_name_arr[0]+"["+row+"]["+old_name_arr[2]+"]");
            }
        });
        row++;
    });
    
}

function printData(caraPrint){
    var param1 = $('#tgl_sep').val();
    var param2 = $('#jenis_pelayanan').val();
    window.open('<?php echo $this->createUrl('PrintDataKunjungan'); ?>&param1='+param1+'&param2='+param2+'&caraPrint='+caraPrint,'printwin','left=100,top=100,width=1000,height=640');
}
function printDataKlaim(caraPrint){
    var param1 = $('#tgl_pulang').val();
    var param2 = $('#jenis_pelayanan').val();
    var param3 = $('#status_klaim').val();
    window.open('<?php echo $this->createUrl('PrintDataKlaim'); ?>&param1='+param1+'&param2='+param2+'&param3='+param3+'&caraPrint='+caraPrint,'printwin','left=100,top=100,width=1000,height=640');
}
</script>