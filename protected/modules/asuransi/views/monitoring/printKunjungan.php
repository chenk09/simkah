<?php
if (isset($caraPrint)){
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$judul_print.'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');     
    }
}
?>
<?php
if (!isset($_GET['frame'])){
    echo $this->renderPartial('_headerPrint'); 
}
?>
<table width="100%" style="margin:0px;" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" valign="middle" colspan="3">
			<b><?php echo $judul_print ?></b>
		</td>
	</tr>
</table><br/>
<div class="block-tabel">
    <table class="items table table-striped table-condensed" id="table-kunjungan">
        <thead>
            <tr>
                <th>Tgl SEP</th>
                <th>No. SEP</th>
                <th>No. Kartu</th>
                <th>Nama Peserta</th>
                <th>Jenis Pelayanan</th>
                <th>Poli/Ruangan</th>
                <th>Tgl Pulang</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<script type="text/javascript">
function cariDataMonitoringKunjungan(param){
    var param1 = '<?php echo $_GET['param1']; ?>';
    var param2 = '<?php echo $_GET['param2']; ?>';
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	
    var setting = {
        url : "<?php echo $this->createUrl('bpjsInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param='+ param + '&query1=' + param1 + '&query2=' +param2,
        beforeSend: function(){
            $("#table-kunjungan").addClass("animation-loading");
        },
        success: function(data){
            $("#table-kunjungan").removeClass("animation-loading");
            var obj = JSON.parse(data);
            var obj1 = JSON.parse(data);
            if(obj1.metaData.message != 'Sukses'){
                alert(obj1.metaData.message);
                $("#table-kunjungan > tbody").detach();
                $("#table-kunjungan").removeClass("animation-loading");
            }else{
                var list = obj.response.sep;
                $.ajax({
                    type:'POST',
                    url:'<?php echo $this->createUrl('SetFormKunjungan'); ?>',
                    data: {kunjunganList:list},//
                    dataType: "json",
                    success:function(data){
                            $("#table-kunjungan > tbody > tr").remove();
                            $('#table-kunjungan > tbody').append(data.form);
                            renameInputRow($("#table-kunjungan"));    
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
                });
                // OVERWRITES old selecor
                jQuery.expr[':'].contains = function(a, i, m) {
                  return jQuery(a).text().toUpperCase()
                          .indexOf(m[3].toUpperCase()) >= 0;
                };
            }
            
        },
        error: function(data){
            $("#table-kunjungan").removeClass("animation-loading");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

/**
* rename input grid
*/ 
function renameInputRow(obj_table){
    var row = 0;
    $(obj_table).find("tbody > tr").each(function(){
        $(this).find("#no_urut").val(row+1);
        $(this).find('span').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("name","["+row+"]["+old_name_arr[2]+"]");
            }
        });
        $(this).find('input,select,textarea').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("id",old_name_arr[0]+"_"+row+"_"+old_name_arr[2]);
                $(this).attr("name",old_name_arr[0]+"["+row+"]["+old_name_arr[2]+"]");
            }
        });
        row++;
    });
    
}
$(document).ready(function(){
    var param1 = '<?php echo $_GET['param1']; ?>';
    var param2 = '<?php echo $_GET['param2']; ?>';
    if(param1 != '' && param2 != ''){
        cariDataMonitoringKunjungan(1);
        $('#tabel-kunjungan').show();
    }
});
</script>