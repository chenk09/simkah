<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div class="white-container">
    <legend class="rim2">Monitoring <b> Klaim </b></legend>
        <?php
            $this->widget('bootstrap.widgets.BootMenu',array(
                'type'=>'tabs',
                'stacked'=>false,
                'htmlOptions'=>array('id'=>'tabmenu'),
                'items'=>array(
                    array('label'=>'Data Kunjungan','url'=>Yii::app()->createUrl(Yii::app()->controller->module->id."/Monitoring/Index")),
                    array('label'=>'Data Kalim','url'=>Yii::app()->createUrl(Yii::app()->controller->module->id."/Monitoring/IndexKlaim"),'active'=>true),
                ),
            ))
        ?>
        <div class="biru">
        <div class="white">
	<?php 
		$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
			'id'=>'pencarian-fasilitas-kesehatan-form',
			'enableAjaxValidation'=>false,
			'type'=>'horizontal',
			'htmlOptions'=>array(
				'onKeyPress'=>'return disableKeyPress(event);',
				'onsubmit'=>'return requiredCheck(this);'),
			'focus'=>'#',
		)); 
	?>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>	
	<?php // $this->renderPartial('_formProcedure', array('form'=>$form)); ?>
        <fieldset class="box search-form">
            <legend class="rim"><i class="icon-white icon-search"></i> Pencarian</legend>
            <div class="row-fluid">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">
                            <?php echo CHtml::label('Tanggal Pulang','',array('class'=>'control-label')); ?>
                            <div class="controls">
                                <?php   
                                   $this->widget('MyDateTimePicker',array(
                                        'name'=>'tgl_pulang',
                                        'mode'=>'date',
                                        'options'=> array(
                                             'dateFormat'=>'yy-mm-dd',
                                             'maxDate' => 'd',
                                        ),
                                         'value'=> date('Y-m-d'),
                                        'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        ),
                               )); ?>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="control-group ">
                            <?php echo CHtml::label("Jenis Pelayanan", 'Jenis Pelayanan', array('class'=>'control-label'))?>
                            <div class="controls">
                            <?php echo CHtml::dropDownList('jenis_pelayanan','jenis_pelayanan', array('2'=>'Rawat Jalan','1'=>'Rawat Inap'), array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event)",
                                )); ?>
                            </div>		
                        </div>
                        <div class="control-group ">
                            <?php echo CHtml::label("Status Klaim", 'Status Klaim', array('class'=>'control-label'))?>
                            <div class="controls">
                            <?php echo CHtml::dropDownList('status_klaim','status_klaim', array('1'=>'Proses Verifikasi','2'=>'Pending Verifikasi','3'=>'Klaim'), array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event)",
                                )); ?>
                            </div>		
                        </div>
                    </div>
                </div>
                <div tyle="margin-left: 60px;">
                        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'cariDataMonitoringKlaim(2);return false;')); ?>
                        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
                </div>
            </div>
        </fieldset>
        <div id="animation"></div>
        <fieldset class="box search-form">
            <legend class="rim"><i class="icon-white icon-search"></i> Tabel Klaim</legend>
            <div class="block-tabel">
                <table class="items table table-striped table-condensed" id="table-klaim">
                    <thead>
                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">No. FPK</th>
                            <th rowspan="2">Tgl SEP</th>
                            <th rowspan="2">Poli/Ruangan</th>
                            <th rowspan="2">Kelas Rawat</th>
                            <th rowspan="2">Kode INACBg</th>
                            <th rowspan="2">Nama INACBg</th>
                            <th colspan="8" style="text-align: center">Peserta</th>
                            <th rowspan="2" style="text-align: center">Status</th>
                        </tr>
                        <tr>
                            <th>No. Kartu</th>
                            <th>No. MR</th>
                            <th>Nama Peserta</th>
                            <th>By Pengajuan</th>
                            <th>By Disetujui</th>
                            <th>By Tarif Grouper</th>
                            <th>By Tarif RS</th>
                            <th>By Top Up</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </fieldset>
	<div class="form-actions">
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary-blue','type'=>'button','disabled'=>false,'onclick'=>'printDataKlaim(\'PRINT\')')); ?>
	</div>
        </div>
        </div>
</div>
<?php $this->endWidget(); ?>
<?php $this->renderPartial('_jsFunctions',array());?>