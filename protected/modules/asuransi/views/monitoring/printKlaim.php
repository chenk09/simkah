<?php
if (isset($caraPrint)){
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$judul_print.'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');     
    }
}
?>
<?php
if (!isset($_GET['frame'])){
    echo $this->renderPartial('_headerPrint'); 
}
?>
<table width="100%" style="margin:0px;" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" valign="middle" colspan="3">
			<b><?php echo $judul_print ?></b>
		</td>
	</tr>
</table><br/>
<div class="block-tabel">
    <table class="items table table-striped table-condensed" id="table-klaim">
        <thead>
            <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">No. FPK</th>
                <th rowspan="2">Tgl SEP</th>
                <th rowspan="2">Poli/Ruangan</th>
                <th rowspan="2">Kelas Rawat</th>
                <th rowspan="2">Kode INACBg</th>
                <th rowspan="2">Nama INACBg</th>
                <th colspan="8" style="text-align: center">Peserta</th>
                <th rowspan="2" style="text-align: center">Status</th>
            </tr>
            <tr>
                <th>No. Kartu</th>
                <th>No. MR</th>
                <th>Nama Peserta</th>
                <th>By Pengajuan</th>
                <th>By Disetujui</th>
                <th>By Tarif Grouper</th>
                <th>By Tarif RS</th>
                <th>By Top Up</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<script type="text/javascript">
function cariDataMonitoringKlaim(param){
    var param1 = '<?php echo $_GET['param1']; ?>';
    var param2 = '<?php echo $_GET['param2']; ?>';
    var param3 = '<?php echo $_GET['param3']; ?>';
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	
    var setting = {
        url : "<?php echo $this->createUrl('bpjsInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param='+ param + '&query1=' + param1 + '&query2=' +param2+ '&param3=' +param3,
        beforeSend: function(){
            $("#table-klaim").addClass("animation-loading");
        },
        success: function(data){
            $("#table-table").removeClass("animation-loading");
            var obj = JSON.parse(data);
            var obj1 = JSON.parse(data);
            if(obj1.metaData.message != 'Sukses'){
                alert(obj1.metaData.message);
                $("#table-klaim > tbody").detach();
                $("#table-klaim").removeClass("animation-loading");
            }else{
                var list = obj.response.klaim;
                $.ajax({
                    type:'POST',
                    url:'<?php echo $this->createUrl('SetFormKlaim'); ?>',
                    data: {kunjunganList:list},//
                    dataType: "json",
                    success:function(data){
                            $("#table-klaim > tbody > tr").remove();
                            $('#table-klaim > tbody').append(data.form);
                            renameInputRow($("#table-klaim"));    
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
                });
                // OVERWRITES old selecor
                jQuery.expr[':'].contains = function(a, i, m) {
                  return jQuery(a).text().toUpperCase()
                          .indexOf(m[3].toUpperCase()) >= 0;
                };
            }
        },
        error: function(data){
            $("#table-klaim").removeClass("animation-loading");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

/**
* rename input grid
*/ 
function renameInputRow(obj_table){
    var row = 0;
    $(obj_table).find("tbody > tr").each(function(){
        $(this).find("#no_urut").val(row+1);
        $(this).find('span').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("name","["+row+"]["+old_name_arr[2]+"]");
            }
        });
        $(this).find('input,select,textarea').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("id",old_name_arr[0]+"_"+row+"_"+old_name_arr[2]);
                $(this).attr("name",old_name_arr[0]+"["+row+"]["+old_name_arr[2]+"]");
            }
        });
        row++;
    });
    
}
$(document).ready(function(){
	var param1 = '<?php echo $_GET['param1']; ?>';
	var param2 = '<?php echo $_GET['param2']; ?>';
	var param3 = '<?php echo $_GET['param3']; ?>';
	if(param1 != '' && param2 != '' && param3 != ''){
            cariDataMonitoringKlaim(2);
            $('#tabel-klaim').show();
	}
});
</script>