<div class="row-fluid">
	<div class="span6">
		<div class="control-group ">
			<?php echo CHtml::label('Kata Kunci (Kode/Nama)','',array('class'=>'control-label')); ?>
			<div class="controls">
				<?php echo CHtml::textField('katakunci','',array('class'=>'span3','placeholder'=>'Ketikan kata kunci')); ?>
				<?php echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                array('onclick'=>'cariDataProcedure();return false;',
                                          'class'=>'btn btn-mini btn-primary btn-katakunci',
                                          'onkeypress'=>"cariDataProcedure();return false;",
                                          'rel'=>"tooltip",
                                          'title'=>"Klik untuk mencari data Procedure Tindakan",)); ?>
			</div>
		</div>
	</div>
    <div class="span6"></div>
</div>