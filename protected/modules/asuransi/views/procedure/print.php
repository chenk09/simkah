<?php
if (isset($caraPrint)){
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$judul_print.'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');     
    }
}
?>
<?php
if (!isset($_GET['frame'])){
    echo $this->renderPartial($this->path_view.'_headerPrint'); 
}
?>
<table width="100%" style="margin:0px;" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" valign="middle" colspan="3">
			<b><?php echo $judul_print ?></b>
		</td>
	</tr>
</table><br/>
<div class="block-tabel">
	<table class="items table table-striped table-condensed" id="table-procedure">
		<thead>
			<tr>
				<th>Kode</th>
				<!--<th>Kode Provider</th>-->
				<th>Nama</th>
				<!--<th>Nama Provider</th>-->
			</tr>
		</thead>
		<tbody>
			
		</tbody>
	</table>
</div>
<script type="text/javascript">
function cariDataProcedure(){
	var katakunci = '<?php echo $_GET['procedure']; ?>';
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	
        isi = "";
	if(katakunci != ''){
		var isi = katakunci;
		var aksi = 1; // 1 untuk mencari data fasilitas kesehatan
	}
	
    if (isi=="") {alert('Isi Kata Kunci terlebih dahulu!'); return false;};
    var setting = {
        url : "<?php echo $this->createUrl('bpjsInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param='+ aksi + '&query=' + isi,
        beforeSend: function(){
            $("#table-procedure").addClass("animation-loading");
        },
        success: function(data){
            $("#table-procedure").removeClass("animation-loading");
            var obj = JSON.parse(data);
            var obj1 = JSON.parse(data);
            if(obj1.metaData.message != 'Sukses'){
                alert(obj1.metaData.message);
            }
            var list = obj.response.procedure;
                    $.ajax({
                            type:'POST',
                            url:'<?php echo $this->createUrl('SetFormProcedure'); ?>',
                            data: {procedureList:list},//
                            dataType: "json",
                            success:function(data){
                                    $("#table-procedure > tbody > tr").remove();
                                    $('#table-procedure > tbody').append(data.form);
                                    renameInputRow($("#table-procedure"));    
                            },
                            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
                    });
            // OVERWRITES old selecor
            jQuery.expr[':'].contains = function(a, i, m) {
              return jQuery(a).text().toUpperCase()
                      .indexOf(m[3].toUpperCase()) >= 0;
            };
        },
        error: function(data){
            $("#table-procedure").removeClass("animation-loading");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

/**
* rename input grid
*/ 
function renameInputRow(obj_table){
    var row = 0;
    $(obj_table).find("tbody > tr").each(function(){
        $(this).find("#no_urut").val(row+1);
        $(this).find('span').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("name","["+row+"]["+old_name_arr[2]+"]");
            }
        });
        $(this).find('input,select,textarea').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("id",old_name_arr[0]+"_"+row+"_"+old_name_arr[2]);
                $(this).attr("name",old_name_arr[0]+"["+row+"]["+old_name_arr[2]+"]");
            }
        });
        row++;
    });
    
}
$(document).ready(function(){
	var faskes = '<?php echo $_GET['procedure']; ?>';
	if(faskes != ''){
		cariDataProcedure();
		$('#tabel-procedure').show();
	}
});
</script>