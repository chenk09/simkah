
<?php 
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
    $template = "{items}";
}
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');   
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>''));      

$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'penjamin_bpjs_id',
		array(
                        'header'=>'ID',
                        'value'=>'$data->penjamin_bpjs_id',
                ),
		'penjamin_bpjs_kode',
		'penjamin_bpjs_nama',
//		'penjamin_bpjs_aktif',
                array(
                    'header'=>'Aktif',
                    'value'=>'($data->penjamin_bpjs_aktif==1)? "YA" : "TIDAK"',
                    'filter'=>false,
                ),
 
        ),
    )); 
?>