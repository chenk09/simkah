<?php
$this->breadcrumbs=array(
	'Penjamin Bpjs Inacbg'=>array('index'),
	$model->penjamin_bpjs_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Penjamin Bpjs Inacbg #'.$model->penjamin_bpjs_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Penjamin Bpjs Inacbg', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Penjamin Bpjs Inacbg', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Penjamin Bpjs Inacbg', 'icon'=>'pencil','url'=>array('update','id'=>$model->penjamin_bpjs_id))) :  '' ;
                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Penjamin Bpjs Inacbg','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->penjamin_bpjs_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Penjamin Bpjs Inacbg', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'penjamin_bpjs_id',
		'penjamin_bpjs_kode',
		'penjamin_bpjs_nama',
		'penjamin_bpjs_aktif',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>