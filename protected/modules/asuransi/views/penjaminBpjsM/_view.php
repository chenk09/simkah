<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('penjamin_bpjs_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->penjamin_bpjs_id),array('view','id'=>$data->penjamin_bpjs_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penjamin_bpjs_kode')); ?>:</b>
	<?php echo CHtml::encode($data->penjamin_bpjs_kode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penjamin_bpjs_nama')); ?>:</b>
	<?php echo CHtml::encode($data->penjamin_bpjs_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penjamin_bpjs_aktif')); ?>:</b>
	<?php echo CHtml::encode($data->penjamin_bpjs_aktif); ?>
	<br />


</div>