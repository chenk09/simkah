<div class="row-fluid">
	<div class="span6">
		<div class="control-group">
			<label class="control-label">No. SEP</label>
			<div class="controls">
                            <?php echo CHtml::textField('no_sep','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label">Tanggal SEP</label>
			<div class="controls">
                            <?php echo CHtml::textField('tgl_sep','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label">Jenis Pelayanan</label>
			<div class="controls">
                            <?php echo CHtml::textField('jns_pelayanan','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label">Poli Pelayanan</label>
			<div class="controls">
                            <?php echo CHtml::textField('poli_pelayanan','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label">Poli Eksekutif</label>
			<div class="controls">
                            <?php echo CHtml::textField('poli_eksekutif','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label">Kelas Rawat</label>
			<div class="controls">
                            <?php echo CHtml::textField('kls_rawat','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label">Diagnosa</label>
			<div class="controls">
                            <?php echo CHtml::textField('diagnosa','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label">Penjamin</label>
			<div class="controls">
                            <?php echo CHtml::textField('penjamin','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label">Asuransi</label>
			<div class="controls">
                            <?php echo CHtml::textField('asuransi','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>		
	</div>
	<div class="span6">
		<div class="control-group">
			<label class="control-label">No. Kartu</label>
			<div class="controls">
                            <?php echo CHtml::textField('no_kartu','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">No. Rekam Medik</label>
			<div class="controls">
                            <?php echo CHtml::textField('no_rm','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Nama</label>
			<div class="controls">
                            <?php echo CHtml::textField('nama','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Tanggal Lahir</label>
			<div class="controls">
                            <?php echo CHtml::textField('tgl_lahir','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Jenis Kelamin</label>
			<div class="controls">
                            <?php echo CHtml::textField('jns_kelamin','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Hak Akses</label>
			<div class="controls">
                            <?php echo CHtml::textField('hak_akses','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Jenis Peserta</label>
			<div class="controls">
                            <?php echo CHtml::textField('jns_peserta','',array('class'=>'span3', 'readonly'=>true)); ?>
			</div>
		</div>
	</div>
</div>