	<div class="row-fluid">
	<div class="span8">
		<div class="control-group ">
			<label class="control-label">
				<?php
					echo "Nomor SEP"; 
				?>
			</label>
			<div class="controls">
				<?php echo CHtml::textField('nomorsep','',array('class'=>'span3')); ?>
				<?php echo CHtml::htmlButton('Cari <i class="icon-search icon-white"></i>',
						array('onclick'=>'cariDataSep();return false;',
							  'class'=>'btn btn-mini btn-primary btn-nomorkartu',
							  'onkeypress'=>"cariDataSep();return false;",
							  'rel'=>"tooltip",
							  'title'=>"Klik untuk mencari data peserta BPJS berdasarkan Nomor Kartu Peserta BPJS",)); ?>
				<?php echo CHtml::htmlButton('Ulang',
						array('onclick'=>'clearData();return false;',
							  'class'=>'btn btn-mini btn-primary-blue btn-nomorkartu',
							  'onkeypress'=>"clearData();return false;",
							  'rel'=>"tooltip",
							  'title'=>"Klik untuk ulang pencarian",)); ?>
			</div>
		</div>
	</div>
	<div class="span4">
		
	</div>
</div>