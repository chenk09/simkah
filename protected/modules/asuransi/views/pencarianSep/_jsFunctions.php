<script type="text/javascript">
function setPencarian(obj){
	if($(obj).val() == 'radio_nomorkartu'){
		$('#radio_nomorkartu').attr('checked',true);
		$('#radio_nik').attr('checked',false);
		$('#nomorkartupeserta').removeAttr('disabled',true);
		$('#nik').attr('disabled',true);
		$("#pencarian-peserta-bpjs-form .btn-nomorkartu").removeAttr('disabled',true);
		$("#pencarian-peserta-bpjs-form .btn-nik").attr('disabled',true);
		$('#nik').val('');
		clearData();
	}else{
		$('#radio_nomorkartu').removeAttr('checked',true);
		$('#radio_nik').attr('checked',true);
		$('#nomorkartupeserta').attr('disabled',true);
		$('#nik').removeAttr('disabled',true);
		$("#pencarian-peserta-bpjs-form .btn-nomorkartu").attr('disabled',true);
		$("#pencarian-peserta-bpjs-form .btn-nik").removeAttr('disabled',true);
		$('#nomorkartupeserta').val('');
		clearData();
	}
}	

/**
 * fungsi pencarian peserta BPJS berdasarkan Nomor Kartu
 */
function cariDataSep()
{   
    var nomorsep = $('#nomorsep').val();
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	var isi = "";
	if(nomorsep != ''){
		isi = nomorsep;
		var aksi = 1; // 1 untuk mencari data peserta berdasarkan Nomor Kartu Peserta
	}
    if (isi=="") {alert('Isi data Nomor SEP terlebih dahulu!'); return false;};
    var setting = {
        url : "<?php echo $this->createUrl('bpjsInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param='+ aksi + '&query=' + isi,
        beforeSend: function(){
            $("#animation").addClass("loader");
        },
        success: function(data){
            $("#animation").removeClass("loader");
            var obj = JSON.parse(data);
            if(obj.response!=null){
                var peserta = obj.response.peserta;
                
                $("#no_sep").val(obj.response.noSep);
                $("#tgl_sep").val(obj.response.tglSep);
                $("#jns_pelayanan").val(obj.response.jnsPelayanan);
                $("#poli_pelayanan").val(obj.response.poli);
                $("#poli_eksekutif").val(obj.response.poliEksekutif);
                $("#kls_rawat").val(obj.response.kelasRawat);
                $("#diagnosa").val(obj.response.diagnosa);
                $("#penjamin").val(obj.response.penjamin);
                $("#asuransi").val(peserta.asuransi);
                $("#no_kartu").val(peserta.noKartu);
                $("#no_rm").val(peserta.noMr);
                $("#nama").val(peserta.nama);
                $("#tgl_lahir").val(peserta.tglLahir);
                $("#jns_kelamin").val(peserta.kelamin);
                $("#hak_akses").val(peserta.hakKelas);
                $("#jns_peserta").val(peserta.jnsPeserta);
                
                jQuery.expr[':'].contains = function(a, i, m) {
                  return jQuery(a).text().toUpperCase()
                          .indexOf(m[3].toUpperCase()) >= 0;
                };
            }else{
                if(obj.metaData.message=="OK"){
                    alert(obj.metaData.code);
                }else{
                    alert(obj.metaData.message);
                }
                clearData();
            }
        },
        error: function(data){
            $("#animation").removeClass("loader");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

/**
* untuk set ulang form data peserta
* */
function clearData(){
    $("#nomorsep").val('');
    $("#no_sep").val('');
    $("#tgl_sep").val('');
    $("#jns_pelayanan").val('');
    $("#poli_pelayanan").val('');
    $("#poli_eksekutif").val('');
    $("#kls_rawat").val('');
    $("#diagnosa").val('');
    $("#penjamin").val('');
    $("#asuransi").val('');
    $("#no_kartu").val('');
    $("#no_rm").val('');
    $("#nama").val('');
    $("#tgl_lahir").val('');
    $("#jns_kelamin").val('');
    $("#hak_akses").val('');
    $("#jns_peserta").val('');
}

function printPesertaBpjs(caraPrint){
	var nokartu = $('#nomorkartupeserta').val();
	var nonik = $('#nik').val();
	window.open('<?php echo $this->createUrl('PrintPesertaBpjs'); ?>&nokartu='+nokartu+'&nonik='+nonik+'&caraPrint='+caraPrint,'printwin','left=100,top=100,width=1000,height=640');
}
</script>