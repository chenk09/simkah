<script type="text/javascript">
$("#pencarian-peserta-bpjs-form .btn-nomorkartu").attr('disabled',true);
$("#pencarian-peserta-bpjs-form .btn-nik").attr('disabled',true);
function setPencarian(obj){
	if($(obj).val() == 'radio_nomorkartu'){
		$('#radio_nomorkartu').attr('checked',true);
		$('#radio_nik').attr('checked',false);
		$('#nomorkartupeserta').removeAttr('disabled',true);
		$('#nik').attr('disabled',true);
		$("#pencarian-peserta-bpjs-form .btn-nomorkartu").removeAttr('disabled',true);
		$("#pencarian-peserta-bpjs-form .btn-nik").attr('disabled',true);
		$('#nik').val('');
		clearDataPeserta();
	}else{
		$('#radio_nomorkartu').removeAttr('checked',true);
		$('#radio_nik').attr('checked',true);
		$('#nomorkartupeserta').attr('disabled',true);
		$('#nik').removeAttr('disabled',true);
		$("#pencarian-peserta-bpjs-form .btn-nomorkartu").attr('disabled',true);
		$("#pencarian-peserta-bpjs-form .btn-nik").removeAttr('disabled',true);
		$('#nomorkartupeserta').val('');
		clearDataPeserta();
	}
}	

/**
 * fungsi pencarian peserta BPJS berdasarkan Nomor Kartu
 */
function cariPesertaBpjsNoKartu()
{   
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	var norujukan = $('#nomorkartupeserta').val();
	var nopeserta = $('#nik').val();
	var jenisfaskes = $('#jenisfaskes').val();
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	
	if(norujukan != ''){
		var isi = norujukan;
		var aksi = 1; // 1 untuk mencari data peserta berdasarkan Nomor Rujukan
	}else{
		var isi = nopeserta;
		var aksi = 2;  // 2 untuk mencari data peserta berdasarkan PEsertas
	}
    if (isi=="") {alert('Isi data Nomor Kartu Peserta terlebih dahulu!'); return false;};
    var setting = {
        url : "<?php echo $this->createUrl('bpjsInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param='+ aksi + '&query=' +isi+ '&jenisfaskes=' +jenisfaskes,
        beforeSend: function(){
            $("#animation").addClass("loader");
        },
        success: function(data){
            $("#animation").removeClass("loader");
            var obj = JSON.parse(data);
            if(obj.response!=null){
                var rujukan = obj.response.rujukan;
                if(obj.metaData.code == '201'){
                    myAlert(obj.metaData.message);
                }else{
                    //Data Rujukan
                    $("#tglKunjungan").text(rujukan.tglKunjungan);
                    $("#noKunjungan").text(rujukan.noKunjungan);
                    $("#kdPoli").text(rujukan.poliRujukan.kode);
                    $("#nmPoli").text(rujukan.poliRujukan.nama);
//					$("#keluhan").text(peserta.keluhan);
                    $("#nmDiag").text(rujukan.diagnosa.kode+'-'+rujukan.diagnosa.nama);
//					$("#pemFisikLain").text(peserta.pemFisikLain);
                    $("#catatan").text(rujukan.keluhan);
                    //End Data Rujukan
                    //Data Peserta
                    $("#noKartu").text(rujukan.peserta.noKartu);
                    $("#nama").text(rujukan.peserta.nama);
                    $("#tglLahir").text(rujukan.peserta.tglLahir);
                    $("#nik").text(rujukan.peserta.nik);
                    if(rujukan.peserta.sex=="P"){
                        $("#sex").text("Perempuan");
                    }else{
                        $("#sex").text("Laki-laki");
                    }
                    $("#kdProvider").text(rujukan.peserta.provUmum.kdProvider);
                    $("#nmProvider").text(rujukan.peserta.provUmum.nmProvider);
                    $("#kdCabang").text(rujukan.peserta.provUmum.kdProvider);
                    $("#nmCabang").text(rujukan.peserta.provUmum.nmProvider);
                    $("#kdKelas").text(rujukan.peserta.hakKelas.kode);
                    $("#nmKelas").text(rujukan.peserta.hakKelas.keterangan);
                    $("#kdJenisPeserta").text(rujukan.peserta.jenisPeserta.kode);
                    $("#nmJenisPeserta").text(rujukan.peserta.jenisPeserta.keterangan);
                    $("#keterangan").text(rujukan.peserta.statusPeserta.keterangan);
                    $("#tglCetakKartu").text(rujukan.peserta.tglCetakKartu);
                    $("#tglTAT").text(rujukan.peserta.tglTAT);
                    $("#tglTMT").text(rujukan.peserta.tglTMT);
                    $("#noMr").text(rujukan.peserta.noMr);
                    $("#umurSekarang").text(rujukan.peserta.umur.umurSekarang);
                    $("#umurSaatPelayanan").text(rujukan.peserta.umur.umurSaatPelayanan);
                    $("#nama_asuransi_cob").text(rujukan.peserta.cob.nmAsuransi);
                    $("#no_asuransi_cob").text(rujukan.peserta.cob.noAsuransi);
                    $("#dinsos").text(rujukan.peserta.informasi.dinsos);
                    $("#nosktm").text(rujukan.peserta.informasi.noSKTM);
                    $("#prb").text(rujukan.peserta.informasi.prolanisPRB);
                    //End Data Peserta
                    $("#.btn-primary-blue").removeAttr('disabled',true);
                    // OVERWRITES old selecor
                    jQuery.expr[':'].contains = function(a, i, m) {
                      return jQuery(a).text().toUpperCase()
                              .indexOf(m[3].toUpperCase()) >= 0;
                    };
                }
                // OVERWRITES old selecor
                jQuery.expr[':'].contains = function(a, i, m) {
                  return jQuery(a).text().toUpperCase()
                          .indexOf(m[3].toUpperCase()) >= 0;
                };
            }else{
                clearDataPeserta();
                alert(obj.metaData.message);
            }
        },
        error: function(data){
            $("#animation").removeClass("loader");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

/**
 * fungsi pencarian peserta BPJS berdasarkan NIK
 */
function cariPesertaBpjsNIK()
{   
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	var nokartu = $('#nomorkartupeserta').val();
	var nonik = $('#nik').val();
        var jenisfaskes = $('#jenisfaskes').val();
    if (<?php echo (Yii::app()->user->getState('is_bridging')==TRUE)?1:0; ?>) {}else{alert('Fitur Bridging tidak aktif!'); return false;}
	
	if(nokartu != ''){
		var isi = nokartu;
		var aksi = 1; // 1 untuk mencari data peserta berdasarkan Nomor Kartu Peserta
	}else{
		var isi = nonik;
		var aksi = 2;  // 2 untuk mencari data peserta berdasarkan NIK
	}
    if (isi=="") {alert('Isi data Nomor Kartu Peserta terlebih dahulu!'); return false;};
    var setting = {
        url : "<?php echo $this->createUrl('bpjsInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param='+ aksi + '&query=' +isi+ '&jenisfaskes=' +jenisfaskes,
        beforeSend: function(){
            $("#animation").addClass("loader");
        },
        success: function(data){
            $("#animation").addClass("loader");
            var obj = JSON.parse(data);
            if(obj.response!=null){
                var rujukan = obj.response.rujukan;
                if(obj.metaData.code == '201'){
                    myAlert(obj.metaData.message);
                }else{
                    //Data Rujukan
                    $("#tglKunjungan").text(rujukan.tglKunjungan);
                    $("#noKunjungan").text(rujukan.noKunjungan);
                    $("#kdPoli").text(rujukan.poliRujukan.kode);
                    $("#nmPoli").text(rujukan.poliRujukan.nama);
//					$("#keluhan").text(peserta.keluhan);
                    $("#nmDiag").text(rujukan.diagnosa.kode+'-'+rujukan.diagnosa.nama);
//					$("#pemFisikLain").text(peserta.pemFisikLain);
                    $("#catatan").text(rujukan.keluhan);
                    //End Data Rujukan
                    //Data Peserta
                    $("#noKartu").text(rujukan.peserta.noKartu);
                    $("#nama").text(rujukan.peserta.nama);
                    $("#tglLahir").text(rujukan.peserta.tglLahir);
                    $("#nik").text(rujukan.peserta.nik);
                    if(rujukan.peserta.sex=="P"){
                        $("#sex").text("Perempuan");
                    }else{
                        $("#sex").text("Laki-laki");
                    }
                    $("#kdProvider").text(rujukan.peserta.provUmum.kdProvider);
                    $("#nmProvider").text(rujukan.peserta.provUmum.nmProvider);
                    $("#kdCabang").text(rujukan.peserta.provUmum.kdProvider);
                    $("#nmCabang").text(rujukan.peserta.provUmum.nmProvider);
                    $("#kdKelas").text(rujukan.peserta.hakKelas.kode);
                    $("#nmKelas").text(rujukan.peserta.hakKelas.keterangan);
                    $("#kdJenisPeserta").text(rujukan.peserta.jenisPeserta.kode);
                    $("#nmJenisPeserta").text(rujukan.peserta.jenisPeserta.keterangan);
                    $("#keterangan").text(rujukan.peserta.statusPeserta.keterangan);
                    $("#tglCetakKartu").text(rujukan.peserta.tglCetakKartu);
                    $("#tglTAT").text(rujukan.peserta.tglTAT);
                    $("#tglTMT").text(rujukan.peserta.tglTMT);
                    $("#noMr").text(rujukan.peserta.noMr);
                    $("#umurSekarang").text(rujukan.peserta.umur.umurSekarang);
                    $("#umurSaatPelayanan").text(rujukan.peserta.umur.umurSaatPelayanan);
                    $("#nama_asuransi_cob").text(rujukan.peserta.cob.nmAsuransi);
                    $("#no_asuransi_cob").text(rujukan.peserta.cob.noAsuransi);
                    $("#dinsos").text(rujukan.peserta.informasi.dinsos);
                    $("#nosktm").text(rujukan.peserta.informasi.noSKTM);
                    $("#prb").text(rujukan.peserta.informasi.prolanisPRB);
                    //End Data Peserta
                    $(".btn-primary-blue").removeAttr('disabled',true);
                    // OVERWRITES old selecor
                    jQuery.expr[':'].contains = function(a, i, m) {
                      return jQuery(a).text().toUpperCase()
                              .indexOf(m[3].toUpperCase()) >= 0;
                    };
                }
                // OVERWRITES old selecor
                jQuery.expr[':'].contains = function(a, i, m) {
                  return jQuery(a).text().toUpperCase()
                          .indexOf(m[3].toUpperCase()) >= 0;
                };
            }else{
                clearDataPeserta();
                alert(obj.metaData.message);
            }
        },
        error: function(data){
            $("#animation").addClass("loader");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

/**
* untuk set ulang form data peserta
* */
function clearDataPeserta(){
	$("#noKartu").text('');
	$("#nama").text('');
	$("#tglLahir").text('');
	$("#nik_peserta").text('');
	$("#sex").text('');
	$("#kdProvider").text('');
	$("#nmProvider").text('');
	$("#kdCabang").text('');
	$("#nmCabang").text('');
	$("#kdKelas").text('');
	$("#nmKelas").text('');
	$("#kdJenisPeserta").text('');
	$("#nmJenisPeserta").text('');
	$("#keterangan").text('');
	$("#tglCetakKartu").text('');
	$("#tglTAT").text('');
	$("#tglTMT").text('');
	$("#noMr").text('');
        $("#nama_asuransi_cob").text('');
        $("#no_asuransi_cob").text('');
        $("#dinsos").text('');
        $("#nosktm").text('');
        $("#prb").text('');
	$("#pencarian-peserta-bpjs-form .btn-primary-blue").attr('disabled',true);
	$("#pencarian-peserta-bpjs-form .btn-riwayat").attr('disabled',true);
}

/**
 * fungsi pencarian peserta BPJS
 */
function lihatRiwayat()
{   
    $('#dialogRiwayatPesertaBpjs').dialog('open');
	$("#data-peserta").removeClass("animation-loading");
}

function printPesertaBpjs(caraPrint){
	var nokartu = $('#nomorkartupeserta').val();
	var nonik = $('#nik').val();
	window.open('<?php echo $this->createUrl('PrintPesertaBpjs'); ?>&nokartu='+nokartu+'&nonik='+nonik+'&caraPrint='+caraPrint,'printwin','left=100,top=100,width=1000,height=640');
}
</script>