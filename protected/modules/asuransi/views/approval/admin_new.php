<?php
$this->breadcrumbs=array(
	'Assep Ts'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('assep-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="white-container">
	<legend class="rim2">Approval <b>(SEP)</b></legend>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
        
	<div class="block-tabel">
		<h6 class="rim2">Tabel Approval (SEP)</h6>
	<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
		'id'=>'assep-t-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'template'=>"{summary}\n{items}\n{pager}",
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'columns'=>array(
                    array(
			'header'=>'No.',
			'value' => '($this->grid->dataProvider->pagination) ? 
					($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
					: ($row+1)',
			'type'=>'raw',
			'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                        'header'=>'Tanggal SEP',
                        'type'=>'raw',
                        'value'=>'isset($data->sep->tglsep) ? $data->sep->tglsep : ""',
                    ),
                    array(
                        'header'=>'No. SEP',
                        'type'=>'raw',
                        'value'=>'isset($data->sep->no_sep)? $data->sep->no_sep : ""',
                    ),
                    array(
                        'header'=>'No. Peserta',
                        'type'=>'raw',
                        'value'=>'$data->no_kartu_bpjs',
                    ),
                    array(
                        'header'=>'No. Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->no_pendaftaran',
                    ),
                    array(
                        'header'=>'No. RM',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->pasien->no_rekam_medik',
                    ),
                    array(
                        'header'=>'Nama Pasien/Peserta',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->pasien->nama_pasien',
                    ),
                    array(
                        'header'=>'Jenis Pelayanan',
                        'type'=>'raw',
                        'value'=>'($data->jenis_pelayanan==2)? "Rawat Jalan" : "Rawat Inap"',
                    ),
                    array(
                        'header'=>'Approve',
                        'type'=>'raw',
                        'value'=>'($data->is_approval==TRUE)? "Sudah Approve" : CHtml::link("<i class=icon-edit></i> ", Yii::app()->createUrl("/'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/Approve", array("id"=>$data->pengajuanapprovalsep_id)),
                                array(
                                "target"=>"frameApprove",
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk Approve SEP",
                                "onclick"=>"$(\'#dialogApprove\').dialog(\'open\');"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'Buat SEP',
                        'type'=>'raw',
                        'value'=>'(!empty($data->sep_id))? "SEP Sudah Terbuat" : ($data->is_approval==FALSE) ? "Belum Aprove" : CHtml::link("<i class=icon-edit></i> ", Yii::app()->createUrl("/'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/CreateSep", array("id"=>$data->pengajuanapprovalsep_id)),
                                array(
                                "target"=>"frameCreateSEP",
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk Buat SEP",
                                "onclick"=>"$(\'#dialogCreateSEP\').dialog(\'open\');"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'Print SEP',
                        'type'=>'raw',
                        'value'=>'(empty($data->sep_id))? "SEP Belum Terbuat" : CHtml::link("<i class=icon-edit></i> ", "#",
                                array(
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk Buat SEP",
                                "onclick"=>"lihatSEP($data->sep_id);return false;"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    ),
		),
		'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
	)); ?>
</div>
        <fieldset class="box search-form">
            <legend class="rim"><i class="icon-white icon-search"></i> Pencarian</legend>
            <?php $this->renderPartial('_search',array(
                    'model'=>$model,
            )); ?>
        </fieldset>
<?php 
	echo CHtml::link(Yii::t('mds','{icon} Buat Pengajuan Approval',array('{icon}'=>'<i class="icon-plus icon-white"></i>')),$this->createUrl('create',array('modul_id'=> Yii::app()->session['modul_id'])), array('class'=>'btn btn-success'))."&nbsp&nbsp"; 
	echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
	echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
	echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
	$urlPrint= $this->createUrl('print');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#assep-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);    
?></div>
<script type="text/javascript">	
function hapusSEP(obj){
    var answer = confirm('Yakin akan menghapus data SEP ini?');
    if(answer){ 
        $.ajax({
            type:'GET',
            url:obj.href,
            data: {},//
            dataType: "json",
            success:function(data){
                    $.fn.yiiGridView.update('assep-t-grid');
                    if(data.sukses > 0){
                            alert(data.status);
                    }else{
                            alert(data.status);
                    }
            },
            error: function (jqXHR, textStatus, errorThrown) { alert(data.status); console.log(errorThrown);}
        });
    }
    return false;
}		

function lihatSEP(sep_id){
  window.open('<?php echo $this->createUrl('printSep'); ?>&sep_id='+sep_id,'printwin','left=100,top=100,width=860,height=480');
}
</script>
<?php 
// Dialog untuk ubah tanggal pulang =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
	'id' => 'dialogUbahTanggalPulang',
	'options' => array(
		'title' => 'Ubah Tanggal Pulang',
		'autoOpen' => false,
		'modal' => true,
		'zIndex'=>1002,
		'width' => 900,
		'height' => 500,
		'resizable' => true,
				'close'=>"js:function(){ $.fn.yiiGridView.update('assep-t-grid', {
						data: $('#assep-t-search').serialize()
					}); }",
	),
));
?>
<iframe name='frameUbahTanggalPulang' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogSEP',
    'options' => array(
        'title' => 'Laporan SEP',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 470,
        'resizable' => false,
    ),
));
?>
<iframe name='frameSEP' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
	'id' => 'dialogApprove',
	'options' => array(
		'title' => 'Approve SEP',
		'autoOpen' => false,
		'modal' => true,
		'width' => 850,
		'height' => 350,
		'resizable' => true,
                    'close'=>"js:function(){ $.fn.yiiGridView.update('assep-t-grid', {
                            data: $('#assep-t-search').serialize()
                    }); }",
	),
));
?>
<iframe name='frameApprove' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
	'id' => 'dialogCreateSEP',
	'options' => array(
		'title' => 'Buat SEP',
		'autoOpen' => false,
		'modal' => true,
		'width' => 850,
		'height' => 650,
		'resizable' => true,
                    'close'=>"js:function(){ $.fn.yiiGridView.update('assep-t-grid', {
                            data: $('#assep-t-search').serialize()
                    }); }",
	),
));
?>
<iframe name='frameCreateSEP' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>