<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div class="white-container">
	<legend class="rim2">Pencarian <b>Peserta BPJS</b></legend>
	<?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'id' => 'pencarian-peserta-bpjs-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event);', 'onsubmit'=>'return requiredCheck(this);'),
    ));
    ?>
	<fieldset class="box">
		<legend class="rim">Data Pencarian</legend>
		<?php $this->renderPartial($this->path_view.'_formPencarian',array('form'=>$form)); ?>
	</fieldset>
	<div id="animation"></div>
	<fieldset class="box" id="data-peserta">
		<legend class="rim">Data peserta</legend>
		<?php $this->renderPartial($this->path_view.'_formDataPeserta',array('form'=>$form)); ?>
	</fieldset>
	
	<div class="form-actions">
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary-blue','type'=>'button','disabled'=>true,'onclick'=>'printPesertaBpjs(\'PRINT\')')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
<?php $this->renderPartial($this->path_view.'_jsFunctions'); ?>