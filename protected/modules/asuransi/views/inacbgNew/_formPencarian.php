<?php
$readonly = TRUE;
?>
<div class = "span6">
    <div class="control-group">
        <?php echo CHtml::label("No. SEP <font style=color:red;> * </font>", 'no_sep', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'no_sep',
                    'value' => $modInfoSep->no_sep,
                    'source' => 'js: function(request, response) {
                        $.ajax({
                                url: "' . $this->createUrl('AutoCompleteSep') . '",
                                dataType: "json",
                                data: {
                                    no_sep: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                        })
                     }',
                    'options' => array(
                        'minLength' => 4,
                        'focus' => 'js:function( event, ui ) {
                            $(this).val( "");
                            return false;
                         }',
                        'select' => 'js:function( event, ui ) {
                            $(this).val(ui.item.no_sep);
                            $("#no_pendaftaran").val(ui.item.no_pendaftaran);
                            setKunjunganSep(ui.item.no_sep);
                            return false;
                        }',
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogPasien'),
                    'htmlOptions' => array('placeholder' => 'Ketik No SEP', 'rel' => 'tooltip', 'title' => 'Ketik nama pasien untuk mencari data kunjungan',
                        'onkeyup' => "return $(this).focusNextInputField(event)",
                    ),
                ));
            ?>
        </div>
    </div>
</div>
<div class="span6">
    <div class="control-group">
        <?php echo CHtml::hiddenField('pendaftaran_id',$modInfoSep->pendaftaran_id,array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
        <?php echo CHtml::label("No. Pendaftaran <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'no_pendaftaran',
                    'value' => $modInfoSep->no_pendaftaran,
                    'source' => 'js: function(request, response) {
                        $.ajax({
                                url: "' . $this->createUrl('AutoCompleteSep') . '",
                                dataType: "json",
                                data: {
                                    no_pendaftaran: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                        })
                     }',
                    'options' => array(
                        'minLength' => 4,
                        'focus' => 'js:function( event, ui ) {
                            $(this).val( "");
                            return false;
                         }',
                        'select' => 'js:function( event, ui ) {
                            $(this).val(ui.item.no_pendaftaran);
                            $("#pendaftaran_id").val(ui.item.pendaftaran_id);
                            $("#no_sep").val(ui.item.no_sep);
                            setKunjunganSep(ui.item.no_sep);
                            return false;
                        }',
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogPasien'),
                    'htmlOptions' => array('placeholder' => 'Ketik No Pendaftaran', 'rel' => 'tooltip', 'title' => 'Ketik nama pasien untuk mencari data kunjungan',
                        'onkeyup' => "return $(this).focusNextInputField(event)",
                    ),
                ));
            ?>
        </div>
    </div>
</div>

<?php
//========= Dialog buat cari data pendaftaran / kunjungan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPasien',
    'options' => array(
        'title' => 'Pencarian Data SEP Pasien',
        'autoOpen' => false,
        'modal' => true,
        'width' => 980,
        'height' => 480,
        'resizable' => false,
    ),
));

$modDialogKunjungan = new ARInfosepinacbgV('searchDialog');
$modDialogKunjungan->unsetAttributes();
$modDialogKunjungan->tgl_pendaftaran = date('Y-m-d');
if (isset($_GET['ARInfosepinacbgV'])) {
    $modDialogKunjungan->attributes = $_GET['ARInfosepinacbgV'];
}

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'datakunjungan-grid',
    'dataProvider' => $modDialogKunjungan->searchDialog(),
    'filter' => $modDialogKunjungan,
    'template' => "{items}\n{pager}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                "id" => "selectPendaftaran",
                "onClick" => "
                    $(\"#no_sep\").val(\"$data->no_sep\");
                    $(\"#no_pendaftaran\").val(\"$data->no_pendaftaran\");
                    $(\"#pendaftaran_id\").val(\"$data->pendaftaran_id\");
                    setKunjunganSep(\"$data->no_sep\");
                    $(\"#dialogPasien\").dialog(\"close\");
                "))',
        ),
        'no_sep',
        'no_pendaftaran',
        array(
            'header' => 'Tanggal Pendaftaran',
            'type' => 'raw',
            'value' => '$data->tgl_pendaftaran',
            'filter' => $this->widget('MyDateTimePicker', array(
                'model' => $modDialogKunjungan,
                'attribute' => 'tgl_pendaftaran',
                'mode' => 'date', //date / datetime
//                'gridFilter' => true,
                'options' => array(
                    'dateFormat' =>'yy-mm-dd',
                    'maxDate' => 'd',
                ),
                'htmlOptions' => array('readonly' => true, 'class' => "span2",
                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                    ), true),
        ),
        array(
            'name' => 'no_rekam_medik',
            'type' => 'raw',
            'value' => '$data->no_rekam_medik',
        ),
        'nama_pasien',
        array(
            'header' => 'Jenis Pelayanan',
            'type' => 'raw',
            'value' => '($data->jnspelayanan_nama==1)? "Rawat Inap" : "Rawat Jalan"',
        ),
        
    ),
    'afterAjaxUpdate' => 'function(id, data){
				jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});
				jQuery("#' . CHtml::activeId($modDialogKunjungan, 'tgl_pendaftaran') . '").datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional["id"], {"dateFormat":"yy-mm-dd","maxDate":"d","timeText":"Waktu","hourText":"Jam","minuteText":"Menit","secondText":"Detik","showSecond":true,"timeOnlyTitle":"Pilih Waktu","timeFormat":"hh:mm:ss","changeYear":true,"changeMonth":true,"showAnim":"fold","yearRange":"-80y:+20y"}));
				jQuery("#' . CHtml::activeId($modDialogKunjungan, 'tgl_pendaftaran') . '_date").on("click", function(){jQuery("#' . CHtml::activeId($modDialogKunjungan, 'tgl_pendaftaran') . '").datepicker("show");});
			}',
));

$this->endWidget();
////======= end pendaftaran dialog =============
?>