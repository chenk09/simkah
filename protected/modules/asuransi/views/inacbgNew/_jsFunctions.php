<script type="text/javascript">
    function setKunjunganSep(nosep){
        $("#animation_kujungan").addClass("loader"); 
        $.ajax({
                type:'POST',
                url:'<?php echo $this->createUrl('getDataKunjunganPasien'); ?>',
                data: {nosep:nosep},
                dataType: "json",
                success:function(data){
                    $("#nokartuasuransi").val(data.nopeserta_bpjs);
                    $("#no_sep").val(data.no_sep);
                    $("#nosep1").val(data.no_sep);
                    $("#no_pendaftaran").val(data.no_pendaftaran);
                    $("#no_pendaftaran1").val(data.no_pendaftaran);
                    $("#no_rekam_medik").val(data.no_rekam_medik);
                    $("#nama_pasien").val(data.nama_pasien);
                    $("#tgl_pendaftaran").val(data.tgl_pendaftaran);
                    $("#tglpulang").val(data.tglpulang);
//                    $("#carabayar_id").val(data.carabayar_id);
                    $("#cob").val(data.namaasuransi_cob);
                    $("#los").val(data.los);
                    if(data.jnspelayanan_nama==="Rawat Inap"){
                        $("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>").html("<option value='1'>Kelas I</option><option value='2'>Kelas II</option><option value='3'>Kelas III</option>");
                        $("#hak_kelas").val(data.klsrawat);
                    }else{
                        $("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>").html("<option value='3'>Regular</option><option value='1'>Eksekutif</option>");
                        $("#los").val(1); //karena RJ hanya 1 hari
                    }
                    $("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>").val(data.klsrawat);
                    $("#<?php echo CHtml::activeId($model,'tarif_prosedur_nonbedah'); ?>").val(data.prosedurenonbedah);
                    $("#<?php echo CHtml::activeId($model,'tarif_prosedur_bedah'); ?>").val(data.prosedurebedah);
                    $("#<?php echo CHtml::activeId($model,'tarif_konsultasi'); ?>").val(data.konsultasi);
                    $("#<?php echo CHtml::activeId($model,'tarif_tenaga_ahli'); ?>").val(data.tenagaahli);
                    $("#<?php echo CHtml::activeId($model,'tarif_keperawatan'); ?>").val(data.keperawatan);
                    $("#<?php echo CHtml::activeId($model,'tarif_penunjang'); ?>").val(data.penunjang);
                    $("#<?php echo CHtml::activeId($model,'tarif_radiologi'); ?>").val(data.radiologi);
                    $("#<?php echo CHtml::activeId($model,'tarif_laboratorium'); ?>").val(data.laboratorium);
                    $("#<?php echo CHtml::activeId($model,'tarif_pelayanan_darah'); ?>").val(data.pelayanandarah);
                    $("#<?php echo CHtml::activeId($model,'tarif_rehabilitasi'); ?>").val(data.rehabilitasi);
                    $("#<?php echo CHtml::activeId($model,'tarif_akomodasi'); ?>").val(data.kamar_akomodasi);
                    $("#<?php echo CHtml::activeId($model,'tarif_rawat_intensif'); ?>").val(data.rawatintensif);
                    $("#<?php echo CHtml::activeId($model,'tarif_obat'); ?>").val(data.obat);
                    $("#<?php echo CHtml::activeId($model,'tarif_alkes'); ?>").val(data.alkes);
                    $("#<?php echo CHtml::activeId($model,'tarif_bhp'); ?>").val(data.bmhp);
                    $("#<?php echo CHtml::activeId($model,'tarif_sewa_alat'); ?>").val(data.sewaalat);
                    $("#<?php echo CHtml::activeId($model,'total_tarif_rs'); ?>").val(data.total);
                    if(data.jnspelayanan_nama==="Rawat Inap"){
                        $("#ARInacbgT_jenisrawat_inacbg_0").attr('checked', 'checked');
                    }else{
                        $("#ARInacbgT_jenisrawat_inacbg_1").attr('checked', 'checked');
                    }
                    $("#<?php echo CHtml::activeId($model,'nama_dpjp'); ?>").html("<option value='"+data.nama_pegawai+"'>"+data.nama_pegawai+"</option>");
                    
                    setNaikKelas($('input:radio[name="ARInacbgT[jenisrawat_inacbg]"]:checked'));
                    setHakKelas($('input:radio[name="ARInacbgT[jenisrawat_inacbg]"]:checked'));
                    setNaikKelasRawat($("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>"));
                    formatNumberSemua();
                    renameInput($("#table-diagnosa"));
                    renameInput($("#table-prosedur"));
                    
                    $("#animation_kujungan").removeClass("loader");
                },
                error: function (jqXHR, textStatus, errorThrown) { 
                    alert("Data Kunjungan Pasien tidak ditemukan !"); 
                    $("#animation_kujungan").removeClass("loader");
                }
        });	
    }
    function setKunjunganSepAfterInsert(nosep){
        $.ajax({
                type:'POST',
                url:'<?php echo $this->createUrl('getDataKunjunganPasien'); ?>',
                data: {nosep:nosep},
                dataType: "json",
                success:function(data){
                    $("#nokartuasuransi").val(data.nopeserta_bpjs);
                    $("#no_sep").val(data.no_sep);
                    $("#nosep1").val(data.no_sep);
                    $("#no_pendaftaran").val(data.no_pendaftaran);
                    $("#no_pendaftaran1").val(data.no_pendaftaran);
                    $("#no_rekam_medik").val(data.no_rekam_medik);
                    $("#nama_pasien").val(data.nama_pasien);
                    $("#tgl_pendaftaran").val(data.tgl_pendaftaran);
                    $("#tglpulang").val(data.tglpulang);
                    
                    setNaikKelas($('input:radio[name="ARInacbgT[jenisrawat_inacbg]"]:checked'));
                    setHakKelas($('input:radio[name="ARInacbgT[jenisrawat_inacbg]"]:checked'));
                    setNaikKelasRawat($("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>"));
                    formatNumberSemua();
                    renameInput($("#table-diagnosa"));
                    renameInput($("#table-prosedur"));
                },
                error: function (jqXHR, textStatus, errorThrown) { 
                    alert("Data Kunjungan Pasien tidak ditemukan !"); 
                }
        });	
    }
    
    function tambahDiagnosa(obj){
        var diagnosa_id = $(obj).parents('fieldset').find('#diagnosa_id').val();
        if(diagnosa_id == ''){
            alert("Pilih diagnosa dahulu");
            return false;
        }
        var jumlah_diagnosa = 0;
        var ada = false;
        $("#table-diagnosa").find("tbody > tr").each(function(){
            jumlah_diagnosa++;
        });
        $("#table-diagnosa").find('input[name$="[diagnosa_id]"]').each(function(){
            if(diagnosa_id == $(this).val()){
                alert("Diagnosa sudah ada pada tabel");
                ada = true;
                return false;
            }
        });
        if(ada==false){
            $.ajax({
                type:'POST',
                url:'<?php echo $this->createUrl('TambahDiagnosa'); ?>',
                data: {diagnosa_id:diagnosa_id,jumlah_diagnosa:jumlah_diagnosa},
                dataType: "json",
                success:function(data){
                    $("#table-diagnosa > tbody").append(data.form);
                    renameInput($("#table-diagnosa"));
                    $(obj).parents('fieldset').find('#namaDiagnosa').val('');
                    $(obj).parents('fieldset').find('#diagnosa_id').val('');
                },
                error: function (jqXHR, textStatus, errorThrown) { 
                    alert("Data Kunjungan Pasien tidak ditemukan !"); 
                }
            });
        }
    }
    
    function tambahProsedur(obj){
        var diagnosa_id = $(obj).parents('fieldset').find('#diagnosaicdix_id').val();
        if(diagnosa_id == ''){
            alert("Pilih procedure dahulu");
            return false;
        }
        var ada = false;
        $("#table-prosedur").find('input[name$="[diagnosaicdix_id]"]').each(function(){
            if(diagnosa_id == $(this).val()){
                alert("Procedure sudah ada pada tabel");
                ada = true;
                return false;
            }
        });
        if(ada==false){
            $.ajax({
                type:'POST',
                url:'<?php echo $this->createUrl('TambahProsedur'); ?>',
                data: {diagnosa_id:diagnosa_id},
                dataType: "json",
                success:function(data){
                    $("#table-prosedur > tbody").append(data.form);
                    renameInput($("#table-prosedur"));
                    $(obj).parents('fieldset').find('#namaDiagnosaicdix').val('');
                    $(obj).parents('fieldset').find('#diagnosaicdix_id').val('');
                },
                error: function (jqXHR, textStatus, errorThrown) { 
                    alert("Data Kunjungan Pasien tidak ditemukan !"); 
                }
            });
        }
    }
    
    function cekEksekutif(obj){
        if($(obj).val() == 1){
            $(".eksekutif").show();
        }else{
            $(".eksekutif").hide();
        }
    }
    
    function setNaikKelasRawat(obj){
        var isi = "";
        if($(obj).val() == 1){
            isi = "<option value=''>-Pilih-</option><option value='vvip'>VVIP</option><option value='vip'>VIP</option>";
        }else if($(obj).val() == 2){
            isi = "<option value=''>-Pilih-</option><option value='vvip'>VVIP</option><option value='vip'>VIP</option><option value='1'>Kelas I</option>";
        }else if($(obj).val() == 3){
            isi = "<option value=''>-Pilih-</option><option value='vvip'>VVIP</option><option value='vip'>VIP</option><option value='1'>Kelas I</option><option value='2'>Kelas II</option>";
        }else{
            isi = "<option value=''>-Pilih-</option>";
        }
        $("#<?php echo CHtml::activeId($model,'naik_kelasrawat_inacbg'); ?>").html(isi);
    }
    
    function setHakKelas(obj){
        if($(obj).val() == 1){
            $("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>").html("<option value='1'>Kelas I</option><option value='2'>Kelas II</option><option value='3'>Kelas III</option>");
            $("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>").val($("#hak_kelas").val());
            $(".eksekutif").hide();
        }else{
            $("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>").html("<option value='3' selected>Regular</option><option value='1'>Eksekutif</option>");
        }
    }
    
    function setNaikKelas(obj){
        if($(obj).val() == 1){
            $(".pelayanan-ri").show();
        }else{
            $(".pelayanan-ri").hide();
        }
    }
    
    function cekNaikKelas(obj){
        if($(obj).is(":checked")){
            $(".naik-kelas").show();
            setNaikKelasRawat($("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>"));
        }else{
            $(".naik-kelas").hide();
        }
    }
    
    function cekInsentif(obj){
        if($(obj).is(":checked")){
            $(".intensif").show();
        }else{
            $(".intensif").hide();
        }
    }
    
    function bataldiagnosa(obj){
        var answer = confirm("Apakah anda akan membatalkan diagnosa ini?");
        if(answer){
            
            $(obj).parents('tr').detach();
            renameInput($("#table-diagnosa"));
            
            var ii = 1;
            $("#table-diagnosa").find("tbody > tr").each(function(){
                if(ii == 1){
                    $(this).find("#jenis").text("Primer");
                    $(this).find('input[name$="[kelompokdiagnosa_id]"]').val(2);
                }else{
                    $(this).find("#jenis").text("Sekunder");
                    $(this).find('input[name$="[kelompokdiagnosa_id]"]').val(3);
                }
                
                ii++;
            })
        } 
        
    }
    
    function batalprosedur(obj){
        var answer = confirm("Apakah anda akan membatalkan prosedur ini?");
        if(answer){
            $(obj).parents('tr').detach();
            renameInput($("#table-prosedur"));
        }
    }
    
    function renameInput(obj_table){
        var row = 0;
        $(obj_table).find("tbody > tr").each(function(){
            $(this).find("#no").text(row+1);
            $(this).find('span').each(function(){ //element <input>
                var old_name = $(this).attr("name").replace(/]/g,"");
                var old_name_arr = old_name.split("[");
                if(old_name_arr.length == 3){
                    $(this).attr("name","["+row+"]["+old_name_arr[2]+"]");
                }
            });
            $(this).find('input,select,textarea').each(function(){ //element <input>
                var old_name = $(this).attr("name").replace(/]/g,"");
                var old_name_arr = old_name.split("[");
                if(old_name_arr.length == 3){
                    $(this).attr("id",old_name_arr[0]+"_"+row+"_"+old_name_arr[2]);
                    $(this).attr("name",old_name_arr[0]+"["+row+"]["+old_name_arr[2]+"]");
                }
            });
            row++;
        });

    }

function setKunjunganPasien(pendaftaran_id){
$("#data-kunjungan-pasien").addClass("animation-loading"); 
	$.ajax({
		type:'POST',
		url:'<?php echo $this->createUrl('getDataKunjunganPasien'); ?>',
		data: {pendaftaran_id:pendaftaran_id},
		dataType: "json",
		success:function(data){
			$('#<?php echo CHtml::activeId($modPendaftaran,'no_pendaftaran'); ?>').val(data.no_pendaftaran);
			$('#<?php echo CHtml::activeId($modPendaftaran,'tgl_pendaftaran'); ?>').val(data.tgl_pendaftaran);
			$('#<?php echo CHtml::activeId($modPasien,'no_rekam_medik'); ?>').val(data.no_rekam_medik);
			$('#<?php echo CHtml::activeId($modPasien,'nama_pasien'); ?>').val(data.nama_pasien);
			$('#<?php echo CHtml::activeId($modPasien,'jeniskelamin'); ?>').val(data.jeniskelamin);
			$('#<?php echo CHtml::activeId($modPasien,'tanggal_lahir'); ?>').val(data.tanggal_lahir);
			$('#<?php echo CHtml::activeId($modPasien,'pasien_id'); ?>').val(data.pasien_id);
			$('#<?php echo CHtml::activeId($modPendaftaran,'instalasi_nama'); ?>').val(data.instalasi_nama);
			$('#<?php echo CHtml::activeId($modPendaftaran,'ruangan_nama'); ?>').val(data.ruangan_nama);
			$('#<?php echo CHtml::activeId($modPasienAdmisi,'tgladmisi'); ?>').val(data.tgladmisi);
			$('#<?php echo CHtml::activeId($modPasienAdmisi,'ruangan_nama'); ?>').val(data.ruangan_admisi);
			$('#<?php echo CHtml::activeId($modPendaftaran,'kelaspelayanan_nama'); ?>').val(data.kelaspelayanan_nama);
			$('#<?php echo CHtml::activeId($modPasienAdmisi,'kamarruangan_nama'); ?>').val(data.kamarruangan_nama);
			$('#<?php echo CHtml::activeId($modPasienAdmisi,'tglpulang'); ?>').val(data.tglpulang);
			$('#<?php echo CHtml::activeId($modPasienPulang,'carakeluar_nama'); ?>').val(data.carakeluar_nama);
			$('#<?php echo CHtml::activeId($modPasienPulang,'kondisikeluar_nama'); ?>').val(data.kondisikeluar_nama);
			$('#<?php echo CHtml::activeId($modPendaftaran,'keterangan'); ?>').val(data.keterangan_pendaftaran);			
			$('#<?php echo CHtml::activeId($modPasienAdmisi,'pasienadmisi_id'); ?>').val(data.pasienadmisi_id);			
			$('#<?php echo CHtml::activeId($modPasienPulang,'pasienpulang_id'); ?>').val(data.pasienpulang_id);			
			$('#<?php echo CHtml::activeId($model,'totaltarif'); ?>').val(data.tarif_tindakan);			
			$('#table-diagnosa tbody').append(data.tr);			
//			setDiagnosa(data.pendaftaran_id);
			formatNumberSemua();
			window.scrollBy(0,380); //<<RND-820 (custom)
			$("#data-kunjungan-pasien").removeClass("animation-loading");
		},
		error: function (jqXHR, textStatus, errorThrown) { alert("Data Kunjungan Pasien tidak ditemukan !"); $("#data-sep").removeClass("animation-loading");}
	});	
}

/**
 * class integer di unformat 
 * @returns {undefined}
 */
function unformatNumberSemua(){
    $(".integer").each(function(){
        $(this).val(parseInt(unformatNumber($(this).val())));
    });
}
/**
 * class integer di format kembali
 * @returns {undefined}
 */
function formatNumberSemua(){
    $(".integer").each(function(){
        $(this).val(formatUang($(this).val()));
    });
}

function ProsesKlaim(param){
    /*
    param = 1 untuk grouping 1
    param = 2 untuk grouping 2
    param = 3 untuk finalisasi klaim
    param = 4 untuk ubah klaim
    param = 5 untuk hapus klaim
    param = 6 untuk print klaim
    param = 7 untuk ulangi klaim grouping, ini proses udah transaksi klaim
    */
    
    unformatNumberSemua();
    var diag = [];
    var prosedur = [];
    var i1 = 0;
    var i2 = 0;
    $("#table-diagnosa").find('input[name$="[diagnosa_kode]"]').each(function(){
        diag[i1] = $(this).val();
        i1++;
    });
    
    $("#table-prosedur").find('input[name$="[diagnosaicdix_kode]"]').each(function(){
        prosedur[i2] = $(this).val();
        i2++;
    });
    
    var nomor_rm = $("#no_rekam_medik").val();
    var nama_pasien = $("#nama_pasien").val();
    var nomor_sep = $("#nosep1").val();
    var nomor_kartu = $("#nokartuasuransi").val();
    var tgl_masuk = $("#tgl_pendaftaran").val();
    var tgl_pulang = $("#tglpulang").val();
    var los = $("#los").val();
    var jenis_rawat = $('input:radio[name="ARInacbgT[jenisrawat_inacbg]"]:checked').val();
    var kelas_rawat = $('#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>').val();
    var adl_sub_acute = $('#<?php echo CHtml::activeId($model,'adlscore_subaccute'); ?>').val();
    var adl_chronic = $('#<?php echo CHtml::activeId($model,'adlscore_chronic'); ?>').val();
    var icu_los = $('#<?php echo CHtml::activeId($model,'lamarawat_icu'); ?>').val();
    var ventilator_hour = $('#<?php echo CHtml::activeId($model,'ventilator_icu'); ?>').val();
    if($('#<?php echo CHtml::activeId($model,'is_rawatintesif'); ?>').is(":checked")){
        var icu_indikator = 1;
        if(icu_los > los){
            alert("Lama hari rawat intensif lebih dari lama rawat");
            return false;
        }
    }else{
        var icu_indikator = 0;
    }
    
    var upgrade_class_class = $('#<?php echo CHtml::activeId($model,'naik_kelasrawat_inacbg'); ?>').val();
    var upgrade_class_los = $('#<?php echo CHtml::activeId($model,'lamarawat_naikkelas'); ?>').val();
    if($('#<?php echo CHtml::activeId($model,'is_naikkelas'); ?>').is(":checked")){
        var upgrade_class_ind = 1;
        if(upgrade_class_los==0){
            alert("Lama hari naik kelas masih kosong");
            return false;
        }
        if(upgrade_class_los > los){
            alert("Lama hari naik kelas lebih dari lama rawat");
            return false;
        }
    }else{
        var upgrade_class_ind = 0;
    }
    
    var add_payment_pct = "<?php echo Yii::app()->user->getState('add_payment_pct'); ?>";
    var birth_weight = $('#<?php echo CHtml::activeId($model,'berat_lahir'); ?>').val();
    var discharge_status = $('#<?php echo CHtml::activeId($model,'cara_pulang'); ?>').val();
    var diagnosa = diag;
    var procedure = prosedur;
    var prosedur_non_bedah = $('#<?php echo CHtml::activeId($model,'tarif_prosedur_nonbedah'); ?>').val();
    var prosedur_bedah = $('#<?php echo CHtml::activeId($model,'tarif_prosedur_bedah'); ?>').val();
    var konsultasi = $('#<?php echo CHtml::activeId($model,'tarif_konsultasi'); ?>').val();
    var tenaga_ahli = $('#<?php echo CHtml::activeId($model,'tarif_tenaga_ahli'); ?>').val();
    var keperawatan = $('#<?php echo CHtml::activeId($model,'tarif_keperawatan'); ?>').val();
    var penunjang = $('#<?php echo CHtml::activeId($model,'tarif_penunjang'); ?>').val();
    var radiologi = $('#<?php echo CHtml::activeId($model,'tarif_radiologi'); ?>').val();
    var laboratorium = $('#<?php echo CHtml::activeId($model,'tarif_laboratorium'); ?>').val();
    var pelayanan_darah = $('#<?php echo CHtml::activeId($model,'tarif_pelayanan_darah'); ?>').val();
    var rehabilitasi = $('#<?php echo CHtml::activeId($model,'tarif_rehabilitasi'); ?>').val();
    var kamar = $('#<?php echo CHtml::activeId($model,'tarif_akomodasi'); ?>').val();
    var rawat_intensif = $('#<?php echo CHtml::activeId($model,'tarif_rawat_intensif'); ?>').val();
    var obat = $('#<?php echo CHtml::activeId($model,'tarif_obat'); ?>').val();
    var alkes = $('#<?php echo CHtml::activeId($model,'tarif_alkes'); ?>').val();
    var bmhp = $('#<?php echo CHtml::activeId($model,'tarif_bhp'); ?>').val();
    var sewa_alat = $('#<?php echo CHtml::activeId($model,'tarif_sewa_alat'); ?>').val();
    var tarif_poli_eks = $('#<?php echo CHtml::activeId($model,'tarif_poli_eksekutif'); ?>').val();
    var nama_dokter = $('#<?php echo CHtml::activeId($model,'nama_dpjp'); ?>').val();
    var kode_tarif = "<?php echo Yii::app()->user->getState('kode_tarifinacbgs_1'); ?>";;//nanti ambil dari profil rs
    var payor_id = $('#carabayar_id').val();//maping antara jaminaninacbg dengan carabayar
    var payor_cd = "JKN";//maping antara jaminaninacbg dengan carabayar
    var cob_cd = "";//master blm ada
    var coder_nik = '<?php echo $modUser->coder_nik; ?>';//ambil nanti dari login pemakain
    
    var tarif_rs_total = unformatNumber($('#<?php echo CHtml::activeId($model,'total_tarif_rs'); ?>').val());
    
    if (nomor_sep=="" || nomor_kartu=="") {alert('Isi Data SEP terlebih dahulu!'); return false;};
    if (tarif_rs_total <= 0){alert('Isi Data Tarif dengan benar!'); return false;};
    if (nama_dokter==""){alert('Pilih nama dokter DPJP!'); return false;};
    if (discharge_status==""){alert('Cara pulang pasien masih kosong!'); return false;};
    if (payor_id==""){alert('Penjamin pasien masih kosong!'); return false;};
    
    var setting = {
        url : "<?php echo $this->createUrl('inacbgInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param='+param+'&nomor_sep='+nomor_sep+'&nomor_kartu='+nomor_kartu+'&tgl_masuk='+tgl_masuk+'&tgl_pulang='+tgl_pulang+'&jenis_rawat='+jenis_rawat+'&kelas_rawat='+kelas_rawat+'&adl_sub_acute='+adl_sub_acute+'&adl_chronic='+adl_chronic+'&icu_indikator='+icu_indikator+'&icu_los='+icu_los+'&ventilator_hour='+ventilator_hour+'&upgrade_class_ind='+upgrade_class_ind+'&upgrade_class_class='+upgrade_class_class+'&upgrade_class_los='+upgrade_class_los+'&add_payment_pct='+add_payment_pct+'&birth_weight='+birth_weight+'&discharge_status='+discharge_status+'&diagnosa='+diagnosa+'&procedure='+procedure+'&prosedur_non_bedah='+prosedur_non_bedah+'&prosedur_bedah='+prosedur_bedah+'&konsultasi='+konsultasi+'&tenaga_ahli='+tenaga_ahli+'&keperawatan='+keperawatan+'&penunjang='+penunjang+'&radiologi='+radiologi+'&laboratorium='+laboratorium+'&pelayanan_darah='+pelayanan_darah+'&rehabilitasi='+rehabilitasi+'&kamar='+kamar+'&rawat_intensif='+rawat_intensif+'&obat='+obat+'&alkes='+alkes+'&bmhp='+bmhp+'&sewa_alat='+sewa_alat+'&tarif_poli_eks='+tarif_poli_eks+'&nama_dokter='+nama_dokter+'&kode_tarif='+kode_tarif+'&payor_id='+payor_id+'&payor_cd='+payor_cd+'&cob_cd='+cob_cd+'&coder_nik='+coder_nik+'&nomor_rm='+nomor_rm+'&nama_pasien='+nama_pasien,
        beforeSend: function(){
            $("#animation").addClass("loader");
        },
        success: function(data){
            $("#animation").removeClass("loader");
            
            var obj = JSON.parse(data);
            
            if(param == 6 && obj.metadata.code == '200'){ //cetak klaim
                formatNumberSemua();
                window.open('data:application/pdf;base64,'+obj.data,'printwin','left=100,top=100,width=860,height=480');
                return false;
            }
            
            if(obj.metadata.code != '200'){
                alert(obj.metadata.message);
            }else{
                if(param != 1){
                    alert(obj.metadata.message);
                }
                if (obj.response != null) {
                    $("#grouping").show();
                    if (typeof obj.response.cbg !== 'undefined') {
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namaprosedur'); ?>').val(obj.response.cbg.description);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodeprosedur'); ?>').val(obj.response.cbg.code);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonprosedur'); ?>').val(obj.response.cbg.tariff);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namaprosedur'); ?>').removeAttr("disabled");
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodeprosedur'); ?>').removeAttr("disabled");
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonprosedur'); ?>').removeAttr("disabled");
                    }else{
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namaprosedur'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodeprosedur'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonprosedur'); ?>').val(0);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namaprosedur'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodeprosedur'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonprosedur'); ?>').attr('disabled','disabled');
                    }
                    if (typeof obj.response.sub_acute !== 'undefined') {
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namasubaccute'); ?>').val(obj.response.sub_acute.description);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodesubaccute'); ?>').val(obj.response.sub_acute.code);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonaccute'); ?>').val(obj.response.sub_acute.tariff);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namasubaccute'); ?>').removeAttr("disabled");
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodesubaccute'); ?>').removeAttr("disabled");
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonaccute'); ?>').removeAttr("disabled");
                    }else{
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namasubaccute'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodesubaccute'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonaccute'); ?>').val(0);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namasubaccute'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodesubaccute'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonaccute'); ?>').attr('disabled','disabled');
                    }
                    if (typeof obj.response.chronic !== 'undefined') {
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namachronic'); ?>').val(obj.response.chronic.description);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodechronic'); ?>').val(obj.response.chronic.code);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonchronic'); ?>').val(obj.response.chronic.tariff);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namachronic'); ?>').removeAttr("disabled");
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodechronic'); ?>').removeAttr("disabled");
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonchronic'); ?>').removeAttr("disabled");
                    }else{
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namachronic'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodechronic'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonchronic'); ?>').val(0);
                        $('#<?php echo CHtml::activeId($modinasiscbg,'namachronic'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'kodechronic'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscbg,'plafonchronic'); ?>').attr('disabled','disabled');
                    }
                    
                    if(typeof obj.special_cmg_option !== 'undefined'){
                        var spesialprosedure = "";
                        var spesialprosthesis = "";
                        var spesialinvestigation = "";
                        var spesialdrug = "";
                        for(var i = 0; i < obj.special_cmg_option.length; i++) {
                            var cmg = obj.special_cmg_option[i];
                            
                            if(cmg.type == "Special Procedure"){
                                spesialprosedure += "<option value='"+cmg.code+"'>"+cmg.description+"</option>"
                            }
                            if(cmg.type == "Special Prosthesis"){
                                spesialprosthesis += "<option value='"+cmg.code+"'>"+cmg.description+"</option>"
                            }
                            if(cmg.type == "Special Investigation"){
                                spesialinvestigation += "<option value='"+cmg.code+"'>"+cmg.description+"</option>"
                            }
                            if(cmg.type == "Special Drug"){
                                spesialdrug += "<option value='"+cmg.code+"'>"+cmg.description+"</option>"
                            }
                        }
                        if(spesialprosedure != ""){
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosedure'); ?>').html("<option value='#'>None</option>"+spesialprosedure);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosedure'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosedure'); ?>').val('-');
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosedure'); ?>').val(0);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosedure'); ?>').attr('disabled','disabled');
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosedure'); ?>').attr('disabled','disabled');
                            <?php if(!empty($modinasiscmg->nama_spesialprosedure)){ ?>
                                $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosedure'); ?>').val('<?php echo $modinasiscmg->nama_spesialprosedure;?>');
                            <?php } ?>
                        }
                        if(spesialprosthesis != ""){
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosthesis'); ?>').html("<option value='#'>None</option>"+spesialprosthesis);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosthesis'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosthesis'); ?>').val('-');
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosthesis'); ?>').val(0);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosthesis'); ?>').attr('disabled','disabled');
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosthesis'); ?>').attr('disabled','disabled');
                            <?php if(!empty($modinasiscmg->nama_spesialprosthesis)){ ?>
                                $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosthesis'); ?>').val('<?php echo $modinasiscmg->nama_spesialprosthesis;?>');
                            <?php } ?>
                        }
                        if(spesialinvestigation != ""){
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialinvestigation'); ?>').html("<option value='#'>None</option>"+spesialinvestigation);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialinvestigation'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialinvestigation'); ?>').val('-');
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialinvestigation'); ?>').val(0);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialinvestigation'); ?>').attr('disabled','disabled');
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialinvestigation'); ?>').attr('disabled','disabled');
                            <?php if(!empty($modinasiscmg->nama_spesialinvestigation)){ ?>
                                $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialinvestigation'); ?>').val('<?php echo $modinasiscmg->nama_spesialinvestigation;?>');
                            <?php } ?>
                        }
                        if(spesialdrug != ""){
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialdrug'); ?>').html("<option value='#'>None</option>"+spesialdrug);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialdrug'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialdrug'); ?>').val('-');
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialdrug'); ?>').val(0);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialdrug'); ?>').attr('disabled','disabled');
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialdrug'); ?>').attr('disabled','disabled');
                            <?php if(!empty($modinasiscmg->nama_spesialdrug)){ ?>
                                $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialdrug'); ?>').val('<?php echo $modinasiscmg->nama_spesialdrug;?>');
                            <?php } ?>
                        }
                        grouper2($("#no_sep"),"AA"); //pemicu agar grouper 2 berjalan
                    }
                }
                
                if(param==1){ //saat grouping
                    $("#btn_finalisasi").removeAttr("disabled");
                    $("#btn_finalisasi").attr('onclick','ProsesKlaim(3);');
                    $("#btn_hapus").removeAttr("disabled");
                    $("#btn_hapus").attr('onclick','ProsesKlaim(5);');
                    $("#btn_simpan").attr('disabled','disabled');
                    $('#<?php echo CHtml::activeId($model,'is_grouping'); ?>').val(1);
                    $('#<?php echo CHtml::activeId($model,'is_finalisasi'); ?>').val(0);
                    $('#<?php echo CHtml::activeId($model,'is_terkirim'); ?>').val(0);
                }else if(param==3){ //saat finalisasi
                    $("#btn_grouping").attr('disabled','disabled');
                    $("#btn_finalisasi").attr('disabled','disabled');
                    $("#btn_finalisasi").removeAttr("onclick");
                    $("#btn_edit_ulang").removeAttr("disabled");
                    $("#btn_edit_ulang").attr('onclick','ProsesKlaim(4);');
                    $("#btn_cetak").removeAttr("disabled");
                    $("#btn_cetak").attr('onclick','ProsesKlaim(6);');
                    $("#btn_hapus").attr('disabled','disabled');
                    $("#btn_hapus").removeAttr("onclick");
                    $("#btn_simpan").removeAttr("disabled");
                    $(".special_cmg").each(function(){
                        $(this).attr('style','display:none;');
                    });
                    $(".special_cmg_nama").each(function(){
                        $(this).removeAttr('style');
                    });
                    $(".hapus_diagnosa").attr('style','display:none;');
                    $('#<?php echo CHtml::activeId($model,'is_grouping'); ?>').val(1);
                    $('#<?php echo CHtml::activeId($model,'is_finalisasi'); ?>').val(1);
                    $('#<?php echo CHtml::activeId($model,'is_terkirim'); ?>').val(0);
                }else if(param==4){ //saat ubah klaim
                    $("#btn_grouping").removeAttr("disabled");
                    $("#btn_grouping").attr('onclick','ProsesKlaim(1);');
                    $("#btn_finalisasi").removeAttr("disabled");
                    $("#btn_finalisasi").attr('onclick','ProsesKlaim(3);');
                    $("#btn_edit_ulang").attr('disabled','disabled');
                    $("#btn_edit_ulang").removeAttr("onclick");
                    $("#btn_cetak").attr('disabled','disabled');
                    $("#btn_cetak").removeAttr("onclick");
                    $("#btn_hapus").removeAttr("disabled");
                    $("#btn_hapus").attr('onclick','ProsesKlaim(5);');
                    $("#btn_simpan").attr('disabled','disabled');
                    $(".special_cmg").each(function(){
                        $(this).removeAttr('style');
                    });
                    $(".special_cmg_nama").each(function(){
                        $(this).attr('style','display:none;');
                    });
                    $(".hapus_diagnosa").removeAttr('style');
                    $('#<?php echo CHtml::activeId($model,'is_grouping'); ?>').val(1);
                    $('#<?php echo CHtml::activeId($model,'is_finalisasi'); ?>').val(0);
                    $('#<?php echo CHtml::activeId($model,'is_terkirim'); ?>').val(0);
                }else if(param==5 || param==7){ //saat hapus klaim
                    $("#btn_grouping").removeAttr("disabled");
                    $("#btn_grouping").attr('onclick','ProsesKlaim(1);');
                    $("#btn_finalisasi").attr('disabled','disabled');
                    $("#btn_finalisasi").removeAttr("onclick");
                    $("#btn_edit_ulang").attr('disabled','disabled');
                    $("#btn_edit_ulang").removeAttr("onclick");
                    $("#btn_hapus").attr('disabled','disabled');
                    $("#btn_hapus").removeAttr("onclick");
                    $("#btn_simpan").attr('disabled','disabled');
                    $(".special_cmg").each(function(){
                        $(this).removeAttr('style');
                    });
                    $(".special_cmg_nama").each(function(){
                        $(this).attr('style','display:none;');
                    });
                    $(".hapus_diagnosa").removeAttr('style');

                    resetHasilGrouper();
                    $('#<?php echo CHtml::activeId($model,'is_grouping'); ?>').val(1);
                    $('#<?php echo CHtml::activeId($model,'is_finalisasi'); ?>').val(0);
                    $('#<?php echo CHtml::activeId($model,'is_terkirim'); ?>').val(0);

                    if(param==7){ //grouping kembali setelah ulangi, khusus dari ubah klaim
                        ProsesKlaim(1);
                    }
                }
            }
            
            hitungTotalGroping();
            
            formatNumberSemua();
            $("#animation").removeClass("loader");
            
            if(jenis_rawat == 1){
                var rawat = "Rawat Inap";
            }else{
                var rawat = "Rawat Jalan";
            }
            isi2 = rawat+' Kelas '+kelas_rawat+' ('+los+' Hari) ';
            isi1 = coder_nik+' @ <?php echo date('d M Y');?> ** kelas C ** Tarif:<?php echo Yii::app()->user->getState('nama_tarifinacbgs_1'); ?>';
            
            $("#info").val(isi1);
            $("#jenisrawat_grouper").val(isi2);
        },
        error: function(data){
            $("#animation").removeClass("loader");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

function grouper2(objek,jenis){
    unformatNumberSemua();
    var kode = [];
    var i1 = 0;
    
    $(".special_cmg").each(function(){
        if($(this).val() != "#"){
            kode[i1] = $(this).val();
        }
        i1++;
    });
    
    var nomor_sep = $("#nosep1").val();
    
    var setting = {
        url : "<?php echo $this->createUrl('inacbgInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param=2&nomor_sep='+nomor_sep+'&kode='+kode,
        beforeSend: function(){
            $("#animation").addClass("loader");
        },
        success: function(data){
            $("#animation").removeClass("loader");
            var obj = JSON.parse(data);
            if(obj.metadata.code != '200'){
                alert(obj.metadata.message);
            }else{
                if($(objek).val() != "#"){
                    for(var i = 0; i < obj.response.special_cmg.length; i++) {
                        var cmg = obj.response.special_cmg[i];
                        if(cmg.type == "Special Procedure"){
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosedure'); ?>').val(cmg.code);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosedure'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosedure'); ?>').val(cmg.tariff);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosedure'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosedure_temp'); ?>').val(cmg.description);
                        }
                        if(cmg.type == "Special Prosthesis"){
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosthesis'); ?>').val(cmg.code);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosthesis'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosthesis'); ?>').val(cmg.tariff);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosthesis'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosthesis_temp'); ?>').val(cmg.description);
                        }
                        if(cmg.type == "Special Investigation"){
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialinvestigation'); ?>').val(cmg.code);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialinvestigation'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialinvestigation'); ?>').val(cmg.tariff);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialinvestigation'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialinvestigation_temp'); ?>').val(cmg.description);
                        }
                        if(cmg.type == "Special Drug"){
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialdrug'); ?>').val(cmg.code);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialdrug'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialdrug'); ?>').val(cmg.tariff);
                            $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialdrug'); ?>').removeAttr("disabled");
                            $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialdrug_temp'); ?>').val(cmg.description);
                        }
                    }
                }else{
                    if(jenis == "procedure"){
                        $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosedure'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosedure'); ?>').val(0);
                        $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosedure'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosedure'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosedure_temp'); ?>').val('-');
                    }
                    if(jenis == "prosthesis"){
                        $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosthesis'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosthesis'); ?>').val(0);
                        $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosthesis'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosthesis'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosthesis_temp'); ?>').val('-');
                    }
                    if(jenis == "investigation"){
                        $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialinvestigation'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialinvestigation'); ?>').val(0);
                        $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialinvestigation'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialinvestigation'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialinvestigation_temp'); ?>').val('-');
                    }
                    if(jenis == "drug"){
                        $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialdrug'); ?>').val('-');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialdrug'); ?>').val(0);
                        $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialdrug'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialdrug'); ?>').attr('disabled','disabled');
                        $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialdrug_temp'); ?>').val('-');
                    }
                }
            }
            
            hitungTotalGroping();
            
            formatNumberSemua();
            $("#animation").removeClass("loader");
        },
        error: function(data){
            $("#animation").removeClass("loader");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

function hitungTotalGroping(){
    total = 0;
    $(".total").each(function(){
        total = total+unformatNumber($(this).val());
    });
    $("#total_grouping").val(total);
}

function resetHasilGrouper(){
    $('#<?php echo CHtml::activeId($modinasiscbg,'namachronic'); ?>').val('-');
    $('#<?php echo CHtml::activeId($modinasiscbg,'kodechronic'); ?>').val('-');
    $('#<?php echo CHtml::activeId($modinasiscbg,'plafonchronic'); ?>').val(0);
    $('#<?php echo CHtml::activeId($modinasiscbg,'namachronic'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscbg,'kodechronic'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscbg,'plafonchronic'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosedure'); ?>').val('-');
    $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosedure'); ?>').val(0);
    $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosedure'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosedure'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosedure_temp'); ?>').val('-');
    $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosthesis'); ?>').val('-');
    $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosthesis'); ?>').val(0);
    $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialprosthesis'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialprosthesis'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialprosthesis_temp'); ?>').val('-');
    $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialinvestigation'); ?>').val('-');
    $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialinvestigation'); ?>').val(0);
    $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialinvestigation'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialinvestigation'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialinvestigation_temp'); ?>').val('-');
    $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialdrug'); ?>').val('-');
    $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialdrug'); ?>').val(0);
    $('#<?php echo CHtml::activeId($modinasiscmg,'kode_spesialdrug'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscmg,'plafon_spesialdrug'); ?>').attr('disabled','disabled');
    $('#<?php echo CHtml::activeId($modinasiscmg,'nama_spesialdrug_temp'); ?>').val('-');

    $("#grouping").hide();
}

function KirimKlaim(inacbg_id){
    confirm("Yakin akan mengirimkan onine data Klaim ini?","Perhatian!",
        function(r){
            if(r){ 
                $("#animation").addClass("loader");
                
                $.ajax({
                    type:'GET',
                    url: "<?php echo $this->createUrl('kirimKlaim'); ?>",
                    data: 'inacbg_id='+inacbg_id,
                    dataType: "json",
                    success:function(data){
                        $("#animation").removeClass("loader");
                        if(data.sukses > 0){
                            alert(data.status);
                            $("#btn_kirim").attr('disabled','disabled');
                            $("#btn_kirim").removeAttr("onclick");
                        }else{
                            alert(data.status);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(data.status); console.log(errorThrown);
                        $("#animation").removeClass("loader");
                    }
                });
            }
        }
    );
    return false;
}

function hitungTotalTarif(obj){
    var totalSemua = 0;
    $(".tarif").each(
    function(){
        totalSemua = totalSemua + unformatNumber($(this).val());
    });
    $('#<?php echo CHtml::activeId($model,'total_tarif_rs'); ?>').val(formatNumber(totalSemua));
}

$(document).ready(function(){
	var sep_id = '<?php isset($model->sep_id) ? $model->sep_id : null; ?>';
	var pendaftaran_id = '<?php isset($model->pendaftaran_id) ? $model->pendaftaran_id : null; ?>';
//	setSEP(sep_id);
        
        $(".intensif").hide();
        $(".naik-kelas").hide();
        $(".pelayanan-ri").hide();
        $(".eksekutif").hide();
        
        <?php if(!empty($model->inacbg_id)){?>
        setNaikKelas($('input:radio[name="ARInacbgT[jenisrawat_inacbg]"]:checked'));
        setNaikKelasRawat($("#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>"));
        cekNaikKelas($('input:checkbox[name="ARInacbgT[is_naikkelas]"]:checked'));
        cekInsentif($('input:checkbox[name="ARInacbgT[is_rawatintesif]"]:checked'));
        $("#<?php echo CHtml::activeId($model,'nama_dpjp'); ?>").html("<option value='<?php echo $model->nama_dpjp; ?>'><?php echo $model->nama_dpjp; ?></option>");
        $(".hapus_diagnosa").attr('style','display:none;');
        renameInput($("#table-diagnosa"));
        renameInput($("#table-prosedur"));
        $(".add-on").attr("style","display:none");
        <?php }?>
        unformatNumberSemua();
        hitungTotalGroping();
//        formatNumberSemua();
        setKunjunganSepAfterInsert('<?php echo $model->no_sep; ?>');
        jenis_rawat = $('#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>').val();
        kelas_rawat = $('#<?php echo CHtml::activeId($model,'hak_kelasrawat_inacbg'); ?>').val();
        los = $("#los").val();
        if(jenis_rawat == 1){
            var rawat = "Rawat Inap";
        }else{
            var rawat = "Rawat Jalan";
        }
        isi2 = rawat+' Kelas '+kelas_rawat+' ('+los+' Hari) ';
        isi1 = <?php echo $modUser->coder_nik; ?>+' @ <?php echo date('d M Y');?> ** kelas C ** Tarif:<?php echo Yii::app()->user->getState('nama_tarifinacbgs_1'); ?>';

        $("#info").val(isi1);
        $("#jenisrawat_grouper").val(isi2);
        $('input:radio[name="ARInacbgT[jenisrawat_inacbg]"]').click(function(){
            return false;
        });
});
</script>