<style>
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 30px;
        height: 30px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
         margin: auto;
    }

    /* Safari */
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
</style>
<?php
$this->breadcrumbs = array(
    'Assep Ts' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('assep-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="white-container">
    <legend class="rim2"><b>Kirim Kolektif Online Klaim INA-CBG's</b></legend>
    <?php $this->widget('bootstrap.widgets.BootAlert'); ?>

    <?php // echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
    <!--<div class="cari-lanjut search-form" style="display:none">-->
    <?php
    // $this->renderPartial('_search',array(
//		'model'=>$model,
//	)); 
    ?>
    <!--</div> search-form -->

    <div class="block-tabel">
        <h6 class="rim2">Tabel Klaim INA-CBG's</h6>
        <?php
        $this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp', array(
            'id' => 'assep-t-grid',
            'dataProvider' => $model->searchInformasiKirimOnline(),
            'filter' => $model,
            'template' => "{summary}\n{items}\n{pager}",
            'itemsCssClass' => 'table table-striped table-bordered table-condensed',
            'columns' => array(
                array(
                    'header' => 'No.',
                    'value' => '($this->grid->dataProvider->pagination) ? 
					($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
					: ($row+1)',
                    'type' => 'raw',
                    'htmlOptions' => array('style' => 'text-align:right;'),
                ),
                array(
                    'header' => 'Tanggal Masuk',
                    'type' => 'raw',
                    'value' => '$data->tglrawat_masuk',
                ),
                array(
                    'header' => 'Tanggal Pulang',
                    'type' => 'raw',
                    'value' => '$data->tglrawat_keluar',
                ),
                array(
                    'header' => 'No. SEP',
                    'type' => 'raw',
                    'value' => '$data->sep->no_sep',
                ),
                array(
                    'header' => 'No. Pendaftaran / Nama Pasien',
                    'type' => 'raw',
                    'value' => '$data->pendaftaran->no_pendaftaran." / ".$data->pasien->nama_pasien',
                ),
                array(
                    'header' => 'Jenis',
                    'type' => 'raw',
                    'value' => '($data->jenisrawat_inacbg==2)? "RJ" : "RI"',
                ),
                array(
                    'header' => 'CBG',
                    'type' => 'raw',
                    'value' => '$data->getCmg($data->inacbg_id)',
                ),
                array(
                    'header'=>'Status',
                    'type'=>'raw',
                    'value'=>'($data->is_terkirim)? "Terkirim" : ($data->is_finalisasi)? "Final" : "-"',
                ),
                array(
                    'header' => 'Special Group',
                    'type' => 'raw',
                    'value' => 'number_format($data->getTArifKlaim($data->inacbg_id))',
                    'htmlOptions' => array('style' => 'text-align:right;'),
                ),
                array(
                    'header' => 'Tarif RS',
                    'type' => 'raw',
                    'value' => 'number_format($data->total_tarif_rs)',
                    'htmlOptions' => array('style' => 'text-align:right;'),
                ),
            ),
            'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
        ));
        ?>
    </div>
    <div id="animation"></div>
    <fieldset class="box search-form">
        <legend class="rim"><i class="icon-white icon-search"></i> Pencarian</legend>
        <?php
        $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
            'id' => 'assep-t-search',
            'type' => 'horizontal',
        ));
        ?>

        <div class="row-fluid">
            <div class="span4">
                <div class="control-group">
                        <?php echo CHtml::label('Filter', '', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php
                        echo $form->dropDownList($model, 'jenis', array('1'=>'Tanggal Pulang','2'=>'Tanggal Grouping'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);"));
                        ?>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                        <?php echo CHtml::label('Dari Tanggal', '', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tgl_awal',
                            'mode' => 'date',
                            'options' => array(
                                //								'dateFormat'=>Params::DATE_FORMAT,
                                'dateFormat' => 'yy-mm-dd',
                                'maxDate' => 'd',
                            ),
                            'htmlOptions' => array('class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                            ),
                        ));
                        ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo CHtml::label('Sampai Tanggal', '', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tgl_akhir',
                            'mode' => 'date',
                            'options' => array(
                                //								'dateFormat'=>Params::DATE_FORMAT,
                                'dateFormat' => 'yy-mm-dd',
                                'maxDate' => 'd',
                            ),
                            'htmlOptions' => array('class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                            ),
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                        <?php echo CHtml::label('Jenis Rawat', '', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php
                        echo $form->dropDownList($model, 'jenisrawat_inacbg', array('1'=>'Ranap','2'=>'Rajal','3'=>'Ranap & Rajal'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);"));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Reset', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-danger', 'type' => 'reset')); ?>
        </div>

<?php $this->endWidget(); ?>

    </fieldset>
<?php
echo CHtml::link(Yii::t('mds', '{icon} Kirim Klaim (Online)', array('{icon}' => '<i class="icon-check icon-white"></i>')), '#', array('class' => 'btn btn-success','onclick'=>'KirimKlaim();return false;')) . "&nbsp&nbsp";
?>

    <script type="text/javascript">

        function KirimKlaim() {
            var start_dt = $("#<?php echo CHtml::activeId($model,'tglrawat_keluar'); ?>").val();
            var stop_dt = $("#<?php echo CHtml::activeId($model,'tglrawat_keluar'); ?>").val();
            var jenis_rawat = $("#<?php echo CHtml::activeId($model,'jenisrawat_inacbg'); ?>").val();
            var date_type = $("#<?php echo CHtml::activeId($model,'jenis'); ?>").val();
            var answer = confirm('Yakin akan mengirimkan data Klaim online kolektif?');
                if(answer){
                    if (r) {
                        $("#animation").addClass("loader");
                        $.ajax({
                            type: 'GET',
                            url: "<?php echo $this->createUrl('kirimKlaimKolektif'); ?>",
                            data: 'start_dt=' + start_dt + '&stop_dt=' + stop_dt + '&jenis_rawat=' + jenis_rawat + '&date_type=' + date_type,
                            dataType: "json",
                            success: function (data) {
                                $("#animation").removeClass("loader");
                                if (data.sukses > 0) {
                                    myAlert(data.status);
                                } else {
                                    myAlert(data.status);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $("#animation").removeClass("loader");
                                console.log(errorThrown);
                            }
                        });
                    }
                }
            return false;
        }
        
    </script>
