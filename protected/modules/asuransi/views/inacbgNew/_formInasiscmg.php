<?php 
if(!empty($modinasiscmg->inasiscmg_id)){
    $show1 = 'display: none;';
    $show2 = '';
}else{
    $show1 = '';
    $show2 = 'display: none;';
}
?>
<div class = "span12">
    <div class="control-group">
        <?php echo CHtml::label("Special Procedure", 'Special Procedure', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList($modinasiscmg,'nama_spesialprosedure',array("#"=>'None'), array('onkeyup'=>"return $(this).focusNextInputField(event)",'class'=>'span6 special_cmg','disabled'=>true,'onchange'=>'grouper2(this,"procedure");', 'style'=>$show1)); ?>
            <?php
                echo $form->textField($modinasiscmg,'nama_spesialprosedure_temp', array('value'=>(!empty($modinasiscmg->nama_spesialprosedure_temp))? $modinasiscmg->nama_spesialprosedure_temp : "-",'class' => 'span6 special_cmg_nama', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly' => true, 'style'=>$show2));
            ?>
            <?php echo CHtml::label("Kode", 'Kode', array()); ?>
            <?php
                echo $form->textField($modinasiscmg,'kode_spesialprosedure', array('value'=>(!empty($modinasiscmg->kode_spesialprosedure))? $modinasiscmg->kode_spesialprosedure : "-",'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Rp.", 'Rp.', array()); ?>
            <?php
                echo $form->textField($modinasiscmg,'plafon_spesialprosedure', array('class' => 'span4 integer total', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Special Prosthesis", 'Special Prosthesis', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList($modinasiscmg,'nama_spesialprosthesis',array("#"=>'None'), array('onkeyup'=>"return $(this).focusNextInputField(event)",'class'=>'span6 special_cmg','disabled'=>true,'onchange'=>'grouper2(this,"prosthesis");', 'style'=>$show1)); ?>
            <?php
                echo $form->textField($modinasiscmg,'nama_spesialprosthesis_temp', array('value'=>(!empty($modinasiscmg->nama_spesialprosthesis_temp))? $modinasiscmg->nama_spesialprosthesis_temp : "-",'class' => 'span6 special_cmg_nama', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly' => true, 'style'=>$show2));
            ?>
            <?php echo CHtml::label("Kode", 'Kode', array()); ?>
            <?php
                echo $form->textField($modinasiscmg,'kode_spesialprosthesis', array('value'=>(!empty($modinasiscmg->kode_spesialprosthesis))? $modinasiscmg->kode_spesialprosthesis : "-",'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Rp.", 'Rp.', array()); ?>
            <?php
                echo $form->textField($modinasiscmg,'plafon_spesialprosthesis', array('class' => 'span4 integer total', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Special Investigation", 'Special Investigation', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList($modinasiscmg,'nama_spesialinvestigation',array("#"=>'None'), array('onkeyup'=>"return $(this).focusNextInputField(event)",'class'=>'span6 special_cmg','disabled'=>true,'onchange'=>'grouper2(this,"investigation");', 'style'=>$show1)); ?>
            <?php
                echo $form->textField($modinasiscmg,'nama_spesialinvestigation_temp', array('value'=>(!empty($modinasiscmg->nama_spesialinvestigation_temp))? $modinasiscmg->nama_spesialinvestigation_temp : "-",'class' => 'span6 special_cmg_nama', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly' => true, 'style'=>$show2));
            ?>
            <?php echo CHtml::label("Kode", 'Kode', array()); ?>
            <?php
                echo $form->textField($modinasiscmg,'kode_spesialinvestigation', array('value'=>(!empty($modinasiscmg->kode_spesialinvestigation))? $modinasiscmg->kode_spesialinvestigation : "-",'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Rp.", 'Rp.', array()); ?>
            <?php
                echo $form->textField($modinasiscmg,'plafon_spesialinvestigation', array('class' => 'span4 integer total', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Special Drug", 'Special Drug', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList($modinasiscmg,'nama_spesialdrug',array("#"=>'None'), array('onkeyup'=>"return $(this).focusNextInputField(event)",'class'=>'span6 special_cmg','disabled'=>true,'onchange'=>'grouper2(this,"drug");', 'style'=>$show1)); ?>
            <?php
                echo $form->textField($modinasiscmg,'nama_spesialdrug_temp', array('value'=>(!empty($modinasiscmg->nama_spesialdrug_temp))? $modinasiscmg->nama_spesialdrug_temp : "-",'class' => 'span6 special_cmg_nama', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly' => true, 'style'=>$show2));
            ?>
            <?php echo CHtml::label("Kode", 'Kode', array()); ?>
            <?php
                echo $form->textField($modinasiscmg,'kode_spesialdrug', array('value'=>(!empty($modinasiscmg->kode_spesialdrug))? $modinasiscmg->kode_spesialdrug : "-",'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Rp.", 'Rp.', array()); ?>
            <?php
                echo $form->textField($modinasiscmg,'plafon_spesialdrug', array('class' => 'span4 integer total', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
        </div>
    </div>
</div>