<?php
$this->breadcrumbs=array(
	'Assep Ts'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('assep-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="white-container">
	<legend class="rim2"><b>INA-CBG's (Groupper)</b></legend>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

	<?php // echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
	<!--<div class="cari-lanjut search-form" style="display:none">-->
	<?php // $this->renderPartial('_search',array(
//		'model'=>$model,
//	)); ?>
	<!--</div> search-form -->
        
	<div class="block-tabel">
		<h6 class="rim2">Tabel INA-CBG's (Groupper)</h6>
	<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
		'id'=>'assep-t-grid',
		'dataProvider'=>$model->searchInformasi(),
		'filter'=>$model,
		'template'=>"{summary}\n{items}\n{pager}",
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'columns'=>array(
                    array(
			'header'=>'No.',
			'value' => '($this->grid->dataProvider->pagination) ? 
					($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
					: ($row+1)',
			'type'=>'raw',
			'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                        'header'=>'No. Pendaftaran / Nama Pasien',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->no_pendaftaran." / ".$data->pasien->nama_pasien',
                    ),
                    array(
                        'header'=>'No. SEP',
                        'type'=>'raw',
                        'value'=>'$data->sep->no_sep',
                    ),
                    array(
                        'header'=>'Tanggal Masuk',
                        'type'=>'raw',
                        'value'=>'$data->tglrawat_masuk',
                    ),
                    array(
                        'header'=>'Tanggal Pulang',
                        'type'=>'raw',
                        'value'=>'$data->tglrawat_keluar',
                    ),
                    array(
                        'header'=>'Jaminan',
                        'type'=>'raw',
                        'value'=>'$data->jaminan_nama',
                    ),
                    array(
                        'header'=>'Jenis',
                        'type'=>'raw',
                        'value'=>'($data->jenisrawat_inacbg==2)? "RJ" : "RI"',
                    ),
                    array(
                        'header'=>'CBG',
                        'type'=>'raw',
                        'value'=>'$data->getCbg($data->inacbg_id)',
                    ),
                    array(
                        'header'=>'Status',
                        'type'=>'raw',
                        'value'=>'($data->is_terkirim)? "Terkirim" : ($data->is_finalisasi)? "Final" : "-"',
                    ),
                    array(
                        'header'=>'Cetak',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=icon-print></i> ", "#",
                                array(
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk cetak Klaim",
                                "onclick"=>"cetakKlaim($data->NoSep);return false;"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'Ubah Ulang',
                        'type'=>'raw',
                        'value'=>'($data->is_terkirim)? "Sudah Terkirim" : CHtml::link("<i class=icon-edit></i> ", Yii::app()->createUrl("/'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/index", array("inacbg_id"=>$data->inacbg_id,"ubah"=>1)),
                                array(
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk mengubah Klaim",
                                ))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'Kirim Online',
                        'type'=>'raw',
                        'value'=>'($data->is_terkirim)? "Sudah Terkirim" : CHtml::link("<i class=icon-upload></i> ", "#",
                                array(
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk kirim online Klaim",
                                "onclick"=>"KirimKlaim($data->inacbg_id);return false;"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'Batal',
                        'type'=>'raw',
                        'value'=>'($data->is_terkirim)? "Sudah Terkirim" : CHtml::link("<i class=icon-trash></i> ", "#",
                                array(
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk batal Klaim",
                                "onclick"=>"hapusKlaim($data->inacbg_id);return false;"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
		),
		'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
	)); ?>
</div>
        <fieldset class="box search-form">
            <legend class="rim"><i class="icon-white icon-search"></i> Pencarian</legend>
            <?php $this->renderPartial('_searchKlaim',array(
                    'model'=>$model,
            )); ?>
        </fieldset>
        <?php
        echo CHtml::link(Yii::t('mds','{icon} Buat Klaim Baru',array('{icon}'=>'<i class="icon-plus icon-white"></i>')),$this->createUrl('index',array('modul_id'=> Yii::app()->session['modul_id'])), array('class'=>'btn btn-success'))."&nbsp&nbsp"; 
        ?>

<script type="text/javascript">
function cetakKlaim(nomor_sep){
    
    var setting = {
        url : "<?php echo $this->createUrl('inacbgInterface'); ?>",
        type : 'GET',
        dataType : 'html',
        data : 'param=6&nomor_sep='+nomor_sep,
        beforeSend: function(){
            $("#assep-t-grid").addClass("animation-loading");
        },
        success: function(data){
            $("#assep-t-grid").removeClass("animation-loading");
            var obj = JSON.parse(data);
            
            if(obj.metadata.code == '200'){ //cetak klaim
                window.open('data:application/pdf;base64,'+obj.data,'printwin','left=100,top=100,width=860,height=480');
                return false;
            }else{
                myAlert(obj.metadata.message);
            }
        },
        error: function(data){
            $("#assep-t-grid").removeClass("animation-loading");
        }
    }
    
    if(typeof ajax_request !== 'undefined') 
        ajax_request.abort();
    ajax_request = $.ajax(setting);
}

function hapusKlaim(inacbg_id){
    myConfirm("Yakin akan membatalkan dan hapus data Klaim ini?","Perhatian!",
        function(r){
            if(r){ 
                $("#assep-t-grid").addClass("animation-loading");
                $.ajax({
                    type:'GET',
                    url: "<?php echo $this->createUrl('hapusKlaim'); ?>",
                    data: 'inacbg_id='+inacbg_id,//
                    dataType: "json",
                    success:function(data){
                        $.fn.yiiGridView.update('assep-t-grid');
                        if(data.sukses > 0){
                                myAlert(data.status);
                        }else{
                                myAlert(data.status);
                        }
                        $("#assep-t-grid").removeClass("animation-loading");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        myAlert(data.status); console.log(errorThrown);
                        $("#assep-t-grid").removeClass("animation-loading");
                    }
                });
            }
        }
    );
    return false;
}

function KirimKlaim(inacbg_id){
    myConfirm("Yakin akan mengirimkan onine data Klaim ini?","Perhatian!",
        function(r){
            if(r){ 
                $("#assep-t-grid").addClass("animation-loading");
                $.ajax({
                    type:'GET',
                    url: "<?php echo $this->createUrl('kirimKlaim'); ?>",
                    data: 'inacbg_id='+inacbg_id,
                    dataType: "json",
                    success:function(data){
                        $("#assep-t-grid").removeClass("animation-loading");
                        if(data.sukses > 0){
                            myAlert(data.status);
                        }else{
                            myAlert(data.status);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        myAlert(data.status); console.log(errorThrown);
                        $("#assep-t-grid").removeClass("animation-loading");
                    }
                });
            }
        }
    );
    return false;
}
</script>
