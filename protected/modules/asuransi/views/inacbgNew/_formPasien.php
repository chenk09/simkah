<?php
$readonly = TRUE;
?>
<div class = "span6">
    <div class="control-group">
        <?php 
        echo CHtml::label("No. Kartu JKN <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); 
        echo CHtml::hiddenField('tgl_pendaftaran', $modInfoSep->tgl_pendaftaran, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
        echo CHtml::hiddenField('tglpulang', $modInfoSep->tglpulang, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
        ?>
        <div class="controls">
            <?php
                echo CHtml::textField('nokartuasuransi', $modInfoSep->nokartuasuransi, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("No. SEP <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                echo CHtml::textField('nosep1', $modInfoSep->nosep, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("No. Pendaftaran <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                echo CHtml::textField('no_pendaftaran1', $modInfoSep->no_pendaftaran, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("No. RM <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                echo CHtml::textField('no_rekam_medik', $modInfoSep->no_rekam_medik, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
            ?>
        </div>
    </div>
</div>
<div class="span6">
    <div class="control-group">
        <?php echo CHtml::label("Nama Pasien <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                echo CHtml::textField('nama_pasien', $modInfoSep->nama_pasien, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Jaminan <font style=color:red;> * </font>", 'penjamin_id', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
            echo CHtml::dropDownList('carabayar_id',$model->jaminan_id, CHtml::listData(PenjaminBpjsM::model()->findAll('penjamin_bpjs_aktif IS TRUE'), 'penjamin_bpjs_kode', 'penjamin_bpjs_nama'), array('empty' => '-- Pilih --',
                'onkeyup' => "return $(this).focusNextInputField(event)",
                'class' => 'span3',
            ));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("COB", 'cob', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            echo CHtml::dropDownList('cob',$model->cob_id, CHtml::listData(CobBpjsM::model()->findAll('cob_bpjs_aktif IS TRUE'), 'cob_bpjs_kode', 'cob_bpjs_nama'), array('empty' => '-- Pilih --',
                'onkeyup' => "return $(this).focusNextInputField(event)",
                'class' => 'span3',
            ));
            ?>
        </div>
    </div>
</div>