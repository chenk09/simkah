<div id="form-prosedur">
<?php echo CHtml::hiddenField('diagnosaicdix_id', '', array('readonly' => true)) ?>
<div class = "span6">
    <div class="control-group">
        <?php echo CHtml::label("Prosedur", 'Prosedur', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'namaDiagnosaicdix',
                    'value' => '',
                    'source' => 'js: function(request, response) {
                        $.ajax({
                                url: "' . $this->createUrl('AutoCompleteProsedur') . '",
                                dataType: "json",
                                data: {
                                    diagnosa: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                        })
                     }',
                    'options' => array(
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ) {
                            $(this).val( "");
                            return false;
                         }',
                        'select' => 'js:function( event, ui ) {
                            $(this).val(ui.item.diagnosaicdix_kode);
                            $("#diagnosaicdix_id").val(ui.item.diagnosaicdix_id);
                            return false;
                        }',
                    ),
                    'htmlOptions' => array('placeholder' => 'Ketik Kode Diagnosa', 'rel' => 'tooltip', 'title' => 'Ketik nama pasien untuk mencari data kunjungan',
                        'onkeyup' => "return $(this).focusNextInputField(event)",'class'=>'span3'
                    ),
                ));
            ?>
            
        </div>
        
    </div>
</div>
<div class = "span6">
    <?php echo CHtml::htmlButton('Tambah',
                array('onclick'=>'tambahProsedur(this);return false;',
                'class'=>'btn-primary',
                'onkeypress'=>"tambahProsedur(this);return false;",
                'rel'=>"tooltip",
                'title'=>"Klik untuk menambahkan ke tabel diagnosa",)); ?>
</div>
<div class = "span12">
    <h6>Tabel <b>Prosedur</b></h6>
</div>
<div class="block-tabel">
	<table class="items table table-striped table-condensed" id="table-prosedur">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Kode Procedure</th>
                    <th>Nama Procedure</th>
                    <th>Nama Lain</th>
                    <th class="hapus_diagnosa">Hapus</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $diagnosa = new DiagnosaicdixM;
                if(count($moddiagnosaicdix) > 0){
                    foreach ($moddiagnosaicdix as $i=>$value) {
                        $modDiagnosa = DiagnosaicdixM::model()->findByAttributes(array('diagnosaicdix_kode'=>$value->diagnosaix_kode));
                        echo "
                        <tr>
                            <td>
                                ".CHtml::activeHiddenField($diagnosa, '[ii]diagnosaicdix_id',array('class'=>'span3','value'=>$modDiagnosa->diagnosaicdix_id))."
                                ".CHtml::activeHiddenField($diagnosa, '[ii]diagnosaicdix_kode',array('class'=>'span3','value'=>$modDiagnosa->diagnosaicdix_kode))."
                            <span id='no' name='[ii][no]'>".($i+1)."</span>			
                            </td>
                            <td>		
                                <span id='kodeDiagnosa' name='[ii][diagnosaicdix_kode]'>".$modDiagnosa->diagnosaicdix_kode."</span>			
                            </td>
                            <td>
                                <span id='namaDiagnosa' name='[ii][diagnosaicdix_nama]'>".$modDiagnosa->diagnosaicdix_nama."</span>
                            </td>	
                            <td>
                                <span id='namaLainnya' name='[ii][diagnosaicdix_namalainnya]'>".$modDiagnosa->diagnosaicdix_namalainnya."</span>
                            </td>	
                            <td class='hapus_diagnosa'>
                                <a onclick='bataldiagnosa(this);return false;' rel='tooltip' href='javascript:void(0);' title='lik untuk membatalkan diagnosa ini'><i class='icon-remove'></i></a>
                            </td>
                        </tr>";
                    }
                }
                ?>
            </tbody>
	</table>
</div>	