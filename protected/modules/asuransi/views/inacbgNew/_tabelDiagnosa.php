<div id="form-diagnosa">
<?php echo CHtml::hiddenField('diagnosa_id', '', array('readonly' => true)) ?>
<div class = "span6">
    <div class="control-group">
        <?php echo CHtml::label("Diagnosa", 'Diagnosa', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'namaDiagnosa',
                    'value' => '',
                    'source' => 'js: function(request, response) {
                        $.ajax({
                                url: "' . $this->createUrl('AutoCompleteDiagnosa') . '",
                                dataType: "json",
                                data: {
                                    diagnosa: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                        })
                     }',
                    'options' => array(
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ) {
                            $(this).val( "");
                            return false;
                         }',
                        'select' => 'js:function( event, ui ) {
                            $(this).val(ui.item.diagnosa_kode);
                            $("#diagnosa_id").val(ui.item.diagnosa_id);
                            return false;
                        }',
                    ),
                    'htmlOptions' => array('placeholder' => 'Ketik Kode Diagnosa', 'rel' => 'tooltip', 'title' => 'Ketik nama pasien untuk mencari data kunjungan',
                        'onkeyup' => "return $(this).focusNextInputField(event)",'class'=>'span3'
                    ),
                ));
            ?>
            
        </div>

    </div>
</div>
<div class = "span6">
    <?php echo CHtml::htmlButton('Tambah',
                array('onclick'=>'tambahDiagnosa(this);return false;',
                'class'=>'btn-primary',
                'onkeypress'=>"tambahDiagnosa(this);return false;",
                'rel'=>"tooltip",
                'title'=>"Klik untuk menambahkan ke tabel diagnosa",)); ?>
</div>
<div class = "span12">
    <h6>Tabel <b>Diagnosa</b></h6>
</div>
<div class="block-tabel">
	<table class="items table table-striped table-condensed" id="table-diagnosa">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Kode Diagnosa</th>
                    <th>Nama Diagnosa</th>
                    <th>Nama Lain</th>
                    <th class="hapus_diagnosa">Hapus</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $diagnosa = new DiagnosaM;
                if(count($moddiagnosaicdx) > 0){
                    foreach ($moddiagnosaicdx as $i=>$value) {
                        $modDiagnosa = DiagnosaM::model()->findByAttributes(array('diagnosa_kode'=>$value->diagnosax_kode));
                        echo "
                        <tr>
                            <td>
                                ".CHtml::activeHiddenField($diagnosa, '[ii]diagnosa_id',array('class'=>'span3','value'=>$modDiagnosa->diagnosa_id))."
                                ".CHtml::activeHiddenField($diagnosa, '[ii]diagnosa_kode',array('class'=>'span3','value'=>$modDiagnosa->diagnosa_kode))."
                                ".CHtml::activeHiddenField($diagnosa, '[ii]kelompokdiagnosa_id',array('class'=>'span3','value'=>$value->diagnosax_type))."
                            <span id='no' name='[ii][no]'>".($i+1)."</span>			
                            </td>
                            <td>		
                                <span id='kodeDiagnosa' name='[ii][diagnosa_kode]'>".$modDiagnosa->diagnosa_kode."</span>			
                            </td>
                            <td>
                                <span id='namaDiagnosa' name='[ii][diagnosa_nama]'>".$modDiagnosa->diagnosa_nama."</span>
                            </td>	
                            <td>
                                <span id='namaLainnya' name='[ii][diagnosa_namalainnya]'>".$modDiagnosa->diagnosa_namalainnya."</span>
                            </td>	
                            <td class='hapus_diagnosa'>
                                <a onclick='bataldiagnosa(this);return false;' rel='tooltip' href='javascript:void(0);' title='lik untuk membatalkan diagnosa ini'><i class='icon-remove'></i></a>
                            </td>	
                        </tr>";
                    }
                }
                ?>
            </tbody>
	</table>
</div>	