<style>
    .integer{
        text-align : right;
    }
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 30px;
        height: 30px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
         margin: auto;
    }

    /* Safari */
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
</style>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.integer',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<div class="white-container">
	<legend  class="rim2">Ubah Ulang INA-CBG's <b>(Groupper)</b></legend>
	<?php 
		$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
			'id'=>'inacbg-t-form',
			'enableAjaxValidation'=>false,
			'type'=>'horizontal',
			'htmlOptions'=>array(
				'onKeyPress'=>'return disableKeyPress(event);',
				'onsubmit'=>'return requiredCheck(this);'),
			'focus'=>'#',
		)); 
	?>
	<?php 
		if(isset($_GET['sukses'])){ 
			Yii::app()->user->setFlash('success', "Data Klaim berhasil diubah !");
		}
	?>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
	
        <div class="row-fluid" id="action">
                <?php
                echo $form->hiddenField($model,'is_finalisasi', array('class' => 'span1 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => true));
                echo $form->hiddenField($model,'is_grouping', array('class' => 'span1 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => true));
                echo $form->hiddenField($model,'is_terkirim', array('class' => 'span1 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => true));
                ?>
		
		<div class="span12 inacbg">
			<fieldset class="box" id="data-pasien">
				<legend class="rim">Data Pasien</legend>
                                <div class="row-fluid">
                                    <div class = "span6">
                                        <div class="control-group">
                                            <?php echo CHtml::label("No. SEP <font style=color:red;> * </font>", 'no_sep', array('class' => 'control-label required')); ?>
                                            <div class="controls">
                                                <?php
                                                    echo CHtml::textField('no_sep', $modInfoSep->no_sep, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => true));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "span6">
                                        <div class="control-group">
                                            <?php echo CHtml::hiddenField('pendaftaran_id',$modPendaftaran->pendaftaran_id,array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
                                            <?php echo CHtml::label("No. Pendaftaran <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
                                            <div class="controls">
                                                <?php
                                                    echo CHtml::textField('no_pendaftaran', $modInfoSep->no_pendaftaran, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => true));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <?php $this->renderPartial($this->path_view_inacbg.'_formPasien',array('form'=>$form,'modInfoSep'=>$modInfoSep,'model'=>$model)); ?>
                                </div>
			</fieldset>
		</div>
                <fieldset><div id="animation_kujungan"></div></fieldset>
                <div class="span12 inacbg">
			<fieldset class="box" id="data-kunjungan-pasien">
				<legend class="rim">Data Kunjungan Pasien</legend>
                                <div class="row-fluid">
                                    <?php $this->renderPartial($this->path_view_inacbg.'_formKunjungan',array('form'=>$form,'model'=>$model,'modInfoSep'=>$modInfoSep)); ?>
                                </div>
			</fieldset>
		</div>
                <div class="span12 inacbg">
			<fieldset class="box" id="data-tarif-rs">
				<legend class="rim">Tarif Rumah Sakit</legend>
                                <div class="row-fluid">
                                    <?php $this->renderPartial($this->path_view_inacbg.'_formTarifRS',array('form'=>$form,'model'=>$model,'modInfoSep'=>$modInfoSep)); ?>
                                </div>
			</fieldset>
		</div>
                <div class="span12 inacbg">
			<fieldset class="box" id="data-diagnosa-rocedure">
				<legend class="rim">Diagnosa dan Prosedur</legend>
                                <div class="row-fluid">
                                    <?php $this->renderPartial($this->path_view_inacbg.'_tabelDiagnosa',array('form'=>$form,'model'=>$model,'modInfoSep'=>$modInfoSep,'moddiagnosaicdx'=>$moddiagnosaicdx)); ?>
                                </div>
                                <div class="row-fluid">
                                    <?php $this->renderPartial($this->path_view_inacbg.'_tabelProsedur',array('form'=>$form,'model'=>$model,'modInfoSep'=>$modInfoSep,'moddiagnosaicdix'=>$moddiagnosaicdix)); ?>
                                </div>
			</fieldset>
		</div>
                
                <?php if(isset($modinasiscbg->inacbg_id) && !empty($modinasiscbg->inacbg_id)){?>
                    <div class="span12 inacbg" id="grouping" style="display: visible;">
                <?php }else{?>
                    <div class="span12 inacbg" id="grouping" style="display: none;">
                <?php }?>
			<fieldset class="box" id="data-kunjungan-pasien">
				<legend class="rim">Hasil Grouper</legend>
                                <div class="row-fluid">
                                    <div class = "span12">
                                        <div class="control-group">
                                            <?php echo CHtml::label("Info", 'info', array('class' => 'control-label')); ?>
                                            <div class="controls">
                                                <?php
                                                    echo CHtml::textField('info','', array('class' => 'span12', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => true));
                                                ?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <?php echo CHtml::label("Jenis Rawat", 'Jenis Rawat', array('class' => 'control-label')); ?>
                                            <div class="controls">
                                                <?php
                                                    echo CHtml::textField('jenisrawat_grouper','', array('class' => 'span12', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => true));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <?php $this->renderPartial($this->path_view_inacbg.'_formInasiscbg',array('form'=>$form,'model'=>$model,'modinasiscbg' => $modinasiscbg,)); ?>
                                </div>
                                <div class="row-fluid">
                                    <?php $this->renderPartial($this->path_view_inacbg.'_formInasiscmg',array('form'=>$form,'model'=>$model,'modinasiscmg' => $modinasiscmg,)); ?>
                                </div>
                                <div class="row-fluid">
                                    <div class = "span12">
                                        <div class="control-group" style="margin-left: 770px">
                                            <div class="controls">
                                                <?php echo CHtml::label("Total", 'Total', array()); ?>
                                                <?php echo CHtml::label("Rp.", 'Rp.', array()); ?>
                                                <?php
                                                    echo CHtml::textField('total_grouping','total_grouping', array('class' => 'span4 integer', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
			</fieldset>
		</div>
                <fieldset><div id="animation"></div></fieldset>
                <div class="form-actions">
			<?php 
				$disabledSave = isset($_GET['sukses']) ? true : false;
				echo CHtml::htmlButton(Yii::t('mds','{icon} Save Perubahan',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),array('id'=>'btn_simpan','disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'formSubmit(this,event);unformatNumberSemua();', 'onkeypress'=>'formSubmit(this,event);unformatNumberSemua();')); 
				echo "&nbsp;";
                                if($disabledSave){
                                    echo CHtml::link(Yii::t('mds', '{icon} Grouping', array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 'javascript:void(0);', array('id'=>'btn_grouping','class'=>'btn btn-info','disabled'=>$disabledSave,));
                                }else{
                                    echo CHtml::link(Yii::t('mds', '{icon} Grouping', array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 'javascript:void(0);', array('id'=>'btn_grouping','class'=>'btn btn-info','disabled'=>true,));
                                }
                                echo "&nbsp;";
                                echo CHtml::link(Yii::t('mds', '{icon} Finalisasi', array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 'javascript:void(0);', array('id'=>'btn_finalisasi','class'=>'btn btn-info', 'disabled'=>'true'));
                                echo "&nbsp;";
                                if($disabledSave){
                                    echo CHtml::link(Yii::t('mds', '{icon} Cetak Klaim', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('id'=>'btn_cetak','class'=>'btn btn-info','onclick'=>'ProsesKlaim(6);'));
                                }else{
                                    echo CHtml::link(Yii::t('mds', '{icon} Cetak Klaim', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('id'=>'btn_cetak','class'=>'btn btn-info','onclick'=>'ProsesKlaim(6);'));
                                }
                                echo "&nbsp;";
                                echo CHtml::link(Yii::t('mds', '{icon} Ulang Klaim Grouping', array('{icon}'=>'<i class="icon-pencil icon-white"></i>')), 'javascript:void(0);', array('id'=>'btn_edit_ulang','class'=>'btn btn-info','onclick'=>'ProsesKlaim(7);hitungTotalGroping();'));
                                echo "&nbsp;";
                                if($disabledSave){
                                    echo CHtml::link(Yii::t('mds', '{icon} Kirim Online Klaim', array('{icon}'=>'<i class="icon-send icon-white"></i>')), 'javascript:void(0);', array('id'=>'btn_kirim','class'=>'btn btn-info', 'oncick'=>'KirimKlaim('.$model->inacbg_id.')'));
                                }else{
                                    echo CHtml::link(Yii::t('mds', '{icon} Kirim Online Klaim', array('{icon}'=>'<i class="icon-send icon-white"></i>')), 'javascript:void(0);', array('id'=>'btn_kirim','class'=>'btn btn-info', 'disabled'=>'true'));
                                }
                                echo "&nbsp;";
                                echo CHtml::link(Yii::t('mds','{icon} Ulang',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                    $this->createUrl('index',array('inacbg_id'=>$_GET['inacbg_id'],'ubah'=>1)), 
                                    array('class'=>'btn btn-danger',
                                              'onclick'=>'return refreshForm(this);'));
			?> 
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
<?php $this->renderPartial($this->path_view_inacbg.'_jsFunctions',array('model'=>$model,
			'modSEP'=>$modSEP,
			'modPendaftaran'=>$modPendaftaran,
			'modPasien'=>$modPasien,
			'modPasienAdmisi'=>$modPasienAdmisi,
			'modPasienPulang'=>$modPasienPulang,
			'modDiagnosaInacbg'=>$modDiagnosaInacbg,
                        'modinasiscbg' => $modinasiscbg,
                        'modinasiscmg' => $modinasiscmg,
                        'modUser' => $modUser,
    )); ?>