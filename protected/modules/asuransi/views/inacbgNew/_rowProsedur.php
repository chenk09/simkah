<?php 
if(count($modDiagnosa)){
    $i = 1;
    foreach ($modDiagnosa as $value) {
?>
<tr>
    <td>		
        <?php echo CHtml::activeHiddenField($value, '[ii]diagnosaicdix_id',array('class'=>'span3')); ?>
        <?php echo CHtml::activeHiddenField($value, '[ii]diagnosaicdix_kode',array('class'=>'span3')); ?>
        <span id="no" name="[ii][no]"><?php echo $i; ?></span>			
    </td>
    <td>		
        <span id="kodeDiagnosa" name="[ii][diagnosaicdix_kode]"><?php echo isset($value->diagnosaicdix_kode) ? $value->diagnosaicdix_kode : ""; ?></span>			
    </td>
    <td>
        <span id="namaDiagnosa" name="[ii][diagnosaicdix_nama]"><?php echo isset($value->diagnosaicdix_nama) ? $value->diagnosaicdix_nama : ""; ?></span>
    </td>	
    <td>
        <span id="namaLainnya" name="[ii][diagnosaicdix_namalainnya]"><?php echo isset($value->diagnosaicdix_namalainnya) ? $value->diagnosaicdix_namalainnya : ""; ?></span>
    </td>	
    <td>
    	<a onclick="batalprosedur(this);return false;" rel="tooltip" href="javascript:void(0);" title="Klik untuk membatalkan prosedur ini"><i class="icon-remove"></i></a>
    </td>	
</tr>
<?php
    $i++;
    }
}
?>