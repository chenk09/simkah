<div class = "span12">
    <div class="control-group">
        <?php echo CHtml::label("Group", 'Group', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($modinasiscbg,'namaprosedur', array('value'=>(!empty($modinasiscbg->namaprosedur))? $modinasiscbg->namaprosedur : "-",'class' => 'span6', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Kode", 'Kode', array()); ?>
            <?php
                echo $form->textField($modinasiscbg,'kodeprosedur', array('value'=>(!empty($modinasiscbg->kodeprosedur))? $modinasiscbg->kodeprosedur : "-",'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Rp.", 'Rp.', array()); ?>
            <?php
                echo $form->textField($modinasiscbg,'plafonprosedur', array('class' => 'span4 integer total', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Sub Acute", 'Sub Acute', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($modinasiscbg,'namasubaccute', array('value'=>(!empty($modinasiscbg->namasubaccute))? $modinasiscbg->namasubaccute : "-",'class' => 'span6', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Kode", 'Kode', array()); ?>
            <?php
                echo $form->textField($modinasiscbg,'kodesubaccute', array('value'=>(!empty($modinasiscbg->kodesubaccute))? $modinasiscbg->kodesubaccute : "-",'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Rp.", 'Rp.', array()); ?>
            <?php
                echo $form->textField($modinasiscbg,'plafonaccute', array('class' => 'span4 integer total', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Chronic", 'Chronic', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($modinasiscbg,'namachronic', array('value'=>(!empty($modinasiscbg->namachronic))? $modinasiscbg->namachronic : "-",'class' => 'span6', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Kode", 'Kode', array()); ?>
            <?php
                echo $form->textField($modinasiscbg,'kodechronic', array('value'=>(!empty($modinasiscbg->kodechronic))? $modinasiscbg->kodechronic : "-",'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
            <?php echo CHtml::label("Rp.", 'Rp.', array()); ?>
            <?php
                echo $form->textField($modinasiscbg,'plafonchronic', array('class' => 'span4 integer total', 'onkeyup' => "return $(this).focusNextInputField(event);",'disabled' => true,'readonly' => true));
            ?>
        </div>
    </div>
</div>