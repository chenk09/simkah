<div class = "span6">
    <div class="control-group">
        <?php echo CHtml::label("Procedure Non Bedah", 'Procedure Non Bedah', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_prosedur_nonbedah', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Konsultasi", 'Konsultasi', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model, 'tarif_konsultasi', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Keperawatan", 'Keperawatan', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model, 'tarif_keperawatan', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Radiologi", 'Radiologi', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model, 'tarif_radiologi', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Pelayanan Darah", 'Pelayanan Darah', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_pelayanan_darah', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Kamar / Akomodasi", 'Kamar / Akomodasi', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_akomodasi', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Obat", 'Obat', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_obat', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("BHP", 'BHP', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_bhp', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
</div>
<div class="span6">
    <div class="control-group">
        <?php echo CHtml::label("Procedure Bedah", 'Procedure Bedah', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_prosedur_bedah', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Tenaga Ahli", 'Tenaga Ahli', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_tenaga_ahli', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Penunjang", 'Penunjang', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_penunjang', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Labolatorium", 'Labolatorium', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_laboratorium', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Rehabilitasi", 'Rehabilitasi', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_rehabilitasi', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Rawat Intensif", 'Rawat Intensif', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_rawat_intensif', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Alkes", 'Alkes', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_alkes', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Sewa Alat", 'Sewa Alkes', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'tarif_sewa_alat', array('class' => 'span3 integer tarif', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>false,'onblur'=>'hitungTotalTarif(this)'));
            ?>
        </div>
    </div>
</div>
<div class = "span12">
    <div class="control-group" style="margin-left: 350px;">
        <?php echo CHtml::label("TARIF RUMAH SAKIT", 'TARIF RUMAH SAKIT', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'total_tarif_rs', array('class' => 'span3 integer', 'onkeyup' => "return $(this).focusNextInputField(event);",'readonly'=>true));
            ?>
        </div>
    </div>
    <div class="control-group" style="margin-left: 150px;">
        <div class="controls">
            <?php
                echo CHtml::checkBox('setuju','', array('onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false));
            ?>
            <span>Menyatakan benar bahwa data tarif tersebut diatas adalah benar dan sesuai dengan kondisi Sebenarnya</span>
        </div>
    </div>
</div>