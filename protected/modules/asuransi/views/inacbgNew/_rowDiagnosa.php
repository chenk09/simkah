<?php 
if(count($modDiagnosa)){
    $i = 1;
    foreach ($modDiagnosa as $value) {
?>
<tr>
    <td>		
        <?php echo CHtml::activeHiddenField($value, '[ii]diagnosa_id',array('class'=>'span3')); ?>
        <?php echo CHtml::activeHiddenField($value, '[ii]diagnosa_kode',array('class'=>'span3')); ?>
        <?php echo CHtml::activeHiddenField($value, '[ii]kelompokdiagnosa_id',array('class'=>'span3')); ?>
        <span id="no" name="[ii][no]"><?php echo $i; ?></span>			
    </td>
    <td>		
        <span id="kodeDiagnosa" name="[ii][diagnosa_kode]"><?php echo isset($value->diagnosa_kode) ? $value->diagnosa_kode : ""; ?></span>			
        &nbsp;
        <span id='jenis' name='[ii][jenis]'>[<?php echo ($value->kelompokdiagnosa_id==2)? "Primer" : "Sekunder"; ?>]</span>
    </td>
    <td>
        <span id="namaDiagnosa" name="[ii][diagnosa_nama]"><?php echo isset($value->diagnosa_nama) ? $value->diagnosa_nama : ""; ?></span>
    </td>	
    <td>
        <span id="namaLainnya" name="[ii][diagnosa_namalainnya]"><?php echo isset($value->diagnosa_namalainnya) ? $value->diagnosa_namalainnya : ""; ?></span>
    </td>	
    <td>
    	<a onclick="bataldiagnosa(this);return false;" rel="tooltip" href="javascript:void(0);" title="Klik untuk membatalkan diagnosa ini"><i class="icon-remove"></i></a>
    </td>	
</tr>
<?php
    $i++;
    }
}
?>