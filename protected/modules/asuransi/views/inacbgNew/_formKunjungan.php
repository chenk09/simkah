<div class="span6">
    <div class="control-group">
        <?php 
        echo CHtml::label("Jenis Perawatan <span class='required'>*</span>", 'Jenis Perawatan', array('class' => 'control-label')); 
        echo CHtml::hiddenField('hak_kelas', 'hak_kelas',array('class'=>'span3'));
        ?>
        <div class="controls form-inline">
            <?php echo $form->radioButtonList($model,'jenisrawat_inacbg',array("1"=>"Rawat Inap&nbsp;&nbsp;","2"=>"Rawat Jalan"), array('onkeyup'=>"return $(this).focusNextInputField(event)",'class'=>'required','onchange'=>'setNaikKelas(this);setHakKelas(this);')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("LOS", 'los', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo CHtml::textField('los', '', array('class' => 'span1', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => true));
            ?>
        </div>
    </div>
    <div class="pelayanan-ri">
        <fieldset class="box">
        <div class="control-group">
            <?php echo CHtml::label("Naik Kelas", 'Naik Kelas', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                    echo $form->checkBox($model,'is_naikkelas', array('onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false,'onclick'=>'cekNaikKelas(this)'));
                ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>

            <?php echo CHtml::label("Ada Rawat Intensif", 'Ada Rawat Intensif', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                    echo $form->checkBox($model,'is_rawatintesif', array('onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false,'onclick'=>'cekInsentif(this)'));
                ?>
            </div>
        </div>
        <div class="control-group naik-kelas">
            <?php echo CHtml::label("Naik Kelas Rawat", 'Naik Kelas Rawat', array('class' => 'control-label')); ?>
            <div class="controls form-inline">
                <?php echo $form->dropDownList($model,'naik_kelasrawat_inacbg',array(), array('onkeyup'=>"return $(this).focusNextInputField(event)",'empty'=>'-Pilih-','class'=>'span3',)); ?>
            </div>
        </div>
        <div class="control-group naik-kelas">
            <?php echo CHtml::label("Lama Rawat", 'Lama Rawat', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                    echo $form->textField($model,'lamarawat_naikkelas', array('class' => 'span1 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false))." Hari";
                ?>
            </div>
        </div>
        <div class="control-group intensif">
            <?php echo CHtml::label("Rawat Intensif", 'Rawat Intensif', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                    echo $form->textField($model,'lamarawat_icu', array('class' => 'span1 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false))." Hari";
                ?>
            </div>
            <?php echo CHtml::label("Ventilator", 'Ventilator', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                    echo $form->textField($model,'ventilator_icu', array('class' => 'span1 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false));
                ?>
            </div>
        </div>
        </fieldset>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Sub Accute", 'Sub Accute', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'adlscore_subaccute', array('class' => 'span3 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false,'rel' => 'tooltip', 'title' => 'Inputan berupa nilai antara 12 sampai 60'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("DPJP", 'DPJP', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList($model,'nama_dpjp', array(), array('class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event)",
                )); 
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Jenis Tarif", 'Jenis Tarif', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo CHtml::textField('jenis_tarif', '', array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => true));
            ?>
        </div>
    </div>
</div>
<div class="span6">
    <div class="control-group ">
        <?php echo CHtml::label("Kelas Rawat <span class='required'>*</span>", 'kelastanggungan', array('class'=>'control-label'))?>
        <div class="controls">
        <?php echo $form->dropDownList($model,'hak_kelasrawat_inacbg', array('1'=>'Kelas I','2'=>'Kelas II','3'=>'Kelas III'), array('empty'=>'-Pilih-','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event)",'onchange'=>'setNaikKelasRawat(this);cekEksekutif(this)'
            )); 
        ?>
        </div>		
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Umur/Berat", 'umur', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'umur_pasien', array('class' => 'span1 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false))." Tahun / ";
                echo $form->textField($model,'berat_lahir', array('class' => 'span1 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false))." Gram";
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Chronic", 'Chronic', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
                echo $form->textField($model,'adlscore_chronic', array('class' => 'span3 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false,'rel' => 'tooltip', 'title' => 'Inputan berupa nilai antara 12 sampai 60'));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Cara Pulang", 'Cara Pulang', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList($model,'cara_pulang', array('1'=>"Atas persetujuan dokter",'2'=>"Dirujuk",'3'=>"Atas permintaan sendiri",'4'=>"Meninggal",'5'=>"Lain-lain"), array('class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event)",
                )); 
            ?>
        </div>
    </div>
    <div class="control-group eksekutif">
        <?php echo CHtml::label("Tarif Poli Eks.", 'Cara Pulang', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php 
            echo $form->textField($model,'tarif_poli_eksekutif', array('class' => 'span3 integer', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => false));
            ?>
        </div>
    </div>
</div>