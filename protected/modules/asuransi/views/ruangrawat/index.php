<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div class="white-container">
	<legend class="rim2">Ruang <b> Rawat </b></legend>
	<?php 
		$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
			'id'=>'pencarian-fasilitas-kesehatan-form',
			'enableAjaxValidation'=>false,
			'type'=>'horizontal',
			'htmlOptions'=>array(
				'onKeyPress'=>'return disableKeyPress(event);',
				'onsubmit'=>'return requiredCheck(this);'),
			'focus'=>'#',
		)); 
	?>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>	
	<?php $this->renderPartial($this->path_view.'_formRuangrawat', array('form'=>$form)); ?>
	<div class="form-actions">
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary-blue','type'=>'button','disabled'=>false,'onclick'=>'printData(\'PRINT\')')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
<?php $this->renderPartial($this->path_view.'_jsFunctions',array());?>