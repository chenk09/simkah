<div class="white-container">
	<legend class="rim2">Konfigurasi BPJS & INACBG</legend>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
        <?php
        $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
            'id' => 'sakonfig-k-form',
            'enableAjaxValidation' => false,
            'type' => 'horizontal',
            'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
            'focus' => '#',
                ));
        ?>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>	
        
    <div class="row-fluid">
	<div class="span4">
            <div class="control-group ">
                <?php echo CHtml::label('User Name BPJS', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'user_bpjs_name', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Secret Key BPJS', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'secret_bpjs_key', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Is Tester', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->checkBox($model,'is_tester', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('BPJS Host Tester', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'bpjs_host_tester', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('BPJS Host Production', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'bpjs_host_production', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('BPJS Service Name', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'bpjs_service_name', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group ">
                <?php echo CHtml::label('BPJS Port', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'bpjs_port', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Is Briging BPJS', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->checkBox($model,'is_bridging', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Kode PPK', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'kode_ppk_bpjs', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Nama PPK', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'nama_ppk_pelayanan', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Is INACBG Akif', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->checkBox($model,'konfig_bpjs_inacbg_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group ">
                <?php echo CHtml::label('Url Host Inacbg', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'host_url_inacbg', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);",'rel'=>'tooltip','title'=>'Contoh : http://alamat_server_aplikasi/E-Klaim/ws.php')); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Key Enkrip Inacbg', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'encryption_key_inacbg', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);",'rel'=>'tooltip','title'=>'Key setup simrs inacbg')); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Add Payment Pct', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'add_payment_pct', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);",'rel'=>'tooltip','title'=>'Biaya naik kelas vip konfig inacbg')); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Kode Tarif RS', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'kode_tarifinacbgs_1', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);",'rel'=>'tooltip','title'=>'Kode Tarif RS')); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Nama Tarif RS', '', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $form->textField($model, 'nama_tarifinacbgs_1', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);",'rel'=>'tooltip','title'=>'Nama Tarif RS')); ?>
                </div>
            </div>
        </div>
    </div>
        
	<div class="form-actions">
            <?php
            echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')) :
                            Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)'));
            ?>
            <?php
            echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), Yii::app()->createUrl($this->module->id . '/' . MasterKonfig . '/index'), array('class' => 'btn btn-danger',
                'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;'));
            ?>
	</div>
</div>
<?php $this->endWidget(); ?>