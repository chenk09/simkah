<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<?php
$this->breadcrumbs=array(
	'Assep Ts'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('assep-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="white-container">
        <div id="animation"></div>
	<legend class="rim2">Surat Eligibilitas Peserta <b>(SEP)</b></legend>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
        
	<div class="block-tabel">
		<h6 class="rim2">Tabel Surat Eligibilitas Peserta (SEP)</h6>
	<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
		'id'=>'assep-t-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'template'=>"{summary}\n{items}\n{pager}",
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'columns'=>array(
                    array(
			'header'=>'No.',
			'value' => '($this->grid->dataProvider->pagination) ? 
					($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
					: ($row+1)',
			'type'=>'raw',
			'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                        'header'=>'Tanggal SEP',
                        'type'=>'raw',
                        'value'=>'isset($data->tglsep) ? $data->tglsep : ""',
                    ),
                    array(
                        'header'=>'No. SEP',
                        'type'=>'raw',
                        'value'=>'$data->no_sep',
                    ),
                    array(
                        'header'=>'No. Peserta',
                        'type'=>'raw',
                        'value'=>'$data->nopeserta_bpjs',
                    ),
                    array(
                        'header'=>'No. Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->no_pendaftaran',
                    ),
                    array(
                        'header'=>'No. RM',
                        'type'=>'raw',
                        'value'=>'$data->pasien->no_rekam_medik',
                    ),
                    array(
                        'header'=>'Nama Pasien/Peserta',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->pasien->nama_pasien',
                    ),
                    array(
                        'header'=>'Jenis Pelayanan',
                        'type'=>'raw',
                        'value'=>'$data->jnspelayanan_nama',
                    ),
                    array(
                        'header'=>'Lihat',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=icon-edit></i> ", "#",
                                array(
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk melihat SEP",
                                "onclick"=>"lihatSEP($data->sep_id);return false;"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'Update SEP',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=icon-edit></i> ", Yii::app()->createUrl("/'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/updateSEP", array("sep_id"=>$data->sep_id)),
                                array(
                                "target"=>"frameUbahSEP",
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk mengubah SEP",
                                "onclick"=>"$(\'#dialogUbahSEP\').dialog(\'open\');"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'Update Tanggal Pulang',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=icon-edit></i> ", Yii::app()->createUrl("/'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/UpdateTglPulang", array("sep_id"=>$data->sep_id)),
                                array(
                                "target"=>"frameUbahTanggalPulang",
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk mengubah Tanggal Pulang",
                                "onclick"=>"$(\'#dialogUbahTanggalPulang\').dialog(\'open\');"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>Yii::t('zii','Delete'),
                        'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{delete}',
                        'buttons'=>array(
                            'delete' => array (
                                    'label'=>"<i class='icon-form-trash'></i>",
                                    'options'=>array('title'=>'Klik untuk menghapus data SEP'),
                                    'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/hapusSEP",array("id"=>$data->sep_id))',
                                    'click'=>'function(){hapusSEP(this);return false;}',
                            ),
                        )
                    ),
		),
		'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
	)); ?>
</div>
        <fieldset class="box search-form">
            <legend class="rim"><i class="icon-white icon-search"></i> Pencarian</legend>
            <?php $this->renderPartial('_search_sep',array(
                    'model'=>$model,
            )); ?>
        </fieldset>
<?php 
	echo CHtml::link(Yii::t('mds','{icon} Tambah SEP',array('{icon}'=>'<i class="icon-plus icon-white"></i>')),$this->createUrl('create',array('modul_id'=> Yii::app()->session['modul_id'])), array('class'=>'btn btn-success'))."&nbsp&nbsp"; 
	echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
	echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
	echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
	$urlPrint= $this->createUrl('print');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#assep-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);    
?></div>
<script type="text/javascript">	
function hapusSEP(obj){
    var answer = confirm('Yakin akan menghapus data SEP ini?');
    if(answer){
        $("#animation").addClass("loader");
        $.ajax({
            type:'GET',
            url:obj.href,
            data: {},//
            dataType: "json",
            success:function(data){
                    $("#animation").removeClass("loader");
                    $.fn.yiiGridView.update('assep-t-grid');
                    if(data.sukses > 0){
                            alert(data.status);
                    }else{
                            alert(data.status);
                    }
            },
            error: function (jqXHR, textStatus, errorThrown) { 
                $("#animation").removeClass("loader");
                alert(data.status); console.log(errorThrown);
            }
        });
    }
    return false;
}		

function lihatSEP(sep_id){
  window.open('<?php echo $this->createUrl('printSep'); ?>&sep_id='+sep_id,'printwin','left=100,top=100,width=860,height=480');
}
</script>
<?php 
// Dialog untuk ubah tanggal pulang =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
	'id' => 'dialogUbahTanggalPulang',
	'options' => array(
		'title' => 'Ubah Tanggal Pulang',
		'autoOpen' => false,
		'modal' => true,
		'zIndex'=>1002,
		'width' => 900,
		'height' => 500,
		'resizable' => true,
				'close'=>"js:function(){ $.fn.yiiGridView.update('assep-t-grid', {
						data: $('#assep-t-search').serialize()
					}); }",
	),
));
?>
<iframe name='frameUbahTanggalPulang' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
        'id' => 'dialogSEP',
        'options' => array(
            'title' => 'Laporan SEP',
            'autoOpen' => false,
            'modal' => true,
            'width' => 900,
            'height' => 470,
            'resizable' => false,
        ),
    ));
    ?>
    <iframe name='frameSEP' width="100%" height="100%"></iframe>
    <?php $this->endWidget(); ?>

<?php 
// Dialog untuk ubah tanggal pulang =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
	'id' => 'dialogUbahSEP',
	'options' => array(
		'title' => 'Ubah Data SEP',
		'autoOpen' => false,
		'modal' => true,
		'zIndex'=>1002,
		'width' => 900,
		'height' => 450,
		'resizable' => true,
                    'close'=>"js:function(){ $.fn.yiiGridView.update('assep-t-grid', {
                            data: $('#assep-t-search').serialize()
                    }); }",
	),
));
?>
<iframe name='frameUbahSEP' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>