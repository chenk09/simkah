<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'assep-t-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
//	'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event);', 'onsubmit'=>'return requiredCheck(this);'),
	'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event);'),
	'focus'=>'#',
)); ?>
        <?php
	if (isset($_GET['sukses'])) {
		Yii::app()->user->setFlash('success', "SEP berhasil disimpan !");
	}
	?>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

	<!--<div class="row-fluid">-->
        <fieldset class="box">
            <legend class="rim">Data Pencarian Pasien</legend>
            <div class="row-fluid">
                <?php echo $this->renderPartial('_formRekamMedik_new',array('form'=>$form,'model'=>$model,'modInfoKunjungan'=>$modInfoKunjungan,)); ?>
            </div>
        </fieldset>
        <div id="animation"></div>
        <fieldset class="box" id="content-bpjs">
            <legend class="rim">Data SEP</legend>
            <div class="row-fluid">
                <?php echo $this->renderPartial('_formSEP_new',array('form'=>$form,'model'=>$model,'modRujukanBpjs'=>$modRujukanBpjs,'modAsuransiPasien'=>$modAsuransiPasien,'modAsuransiPasienBpjs'=>$modAsuransiPasienBpjs)); ?>
            </div>
        </fieldset>
	<!--</div>-->
	<div class="row-fluid">
		<div class="form-actions">
			<?php
				$sukses = isset($_GET['sukses']) ? $_GET['sukses'] : null;
				$disabledSave = isset($_GET['id']) ? true : ($sukses == 1) ? true : false;
			?>
			<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','disabled'=>$disabledSave, 'onclick'=>'cekInput(this,13);return false;')); ?>
			<?php echo CHtml::link(Yii::t('mds','{icon} Ulang',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
					$this->createUrl('create'), 
					array('class'=>'btn btn-danger',
						  'onclick'=>'if(!confirm("'.Yii::t('mds','Anda yakin akan mengulang?').'")) return false;')); ?>
			<?php
				if(Yii::app()->user->getState('is_bridging')){
					if (isset($model->sep_id)) {
						echo CHtml::link(Yii::t('mds', '{icon} Print SEP', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('class'=>'btn btn-info','onclick'=>"printSEP();return false",'disabled'=>FALSE  ));
					}else{
						echo CHtml::link(Yii::t('mds', '{icon} Print SEP', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('rel'=>'tooltip','title'=>'Belum memiliki No. SEP!','class'=>'btn btn-info','onclick'=>"return false",'disabled'=>true, 'style'=>'cursor:not-allowed;'));
					}
				}else{
					echo CHtml::link(Yii::t('mds', '{icon} Print SEP', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('rel'=>'tooltip','title'=>'Fitur Bridging tidak aktif!','class'=>'btn btn-info','onclick'=>"return false",'disabled'=>true, 'style'=>'cursor:not-allowed;'));
				}
			?>
			<?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan SEP',array('{icon}'=>'<i class="icon-folder-open icon-white"></i>')),$this->createUrl('admin',array('modul_id'=> Yii::app()->session['modul_id'])), array('class'=>'btn btn-success')); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPoli',
    'options' => array(
        'title' => 'Referensi Poli BPJS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 960,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('_pencarianPoli');
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDiagnosaBpjs',
    'options' => array(
        'title' => 'Referensi Diagnosa BPJS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 960,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('_pencarianDiagnosa');
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPpk',
    'options' => array(
        'title' => 'Referensi PPK Rujukan/Faskes',
        'autoOpen' => false,
        'modal' => true,
        'width' => 960,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('_pencarianPpk');
$this->endWidget();
?>