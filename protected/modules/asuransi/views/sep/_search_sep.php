<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'assep-t-search',
	'type'=>'horizontal',
)); ?>

	<div class="row-fluid">
		<div class="span6">
			<div class="control-group">
				<?php echo CHtml::label('Tanggal SEP','',array('class'=>'control-label')); ?>
				<div class="controls">
					 <?php   
						$this->widget('MyDateTimePicker',array(
							'model'=>$model,
							'attribute'=>'tgl_awal',
							'mode'=>'date',
							'options'=> array(
//								'dateFormat'=>Params::DATE_FORMAT,
								'dateFormat'=>'yy-mm-dd',
								'maxDate' => 'd',
							),
							'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
							),
					)); ?>
				</div>
			</div>
			<div class="control-group">
                                <?php echo CHtml::label('Sampai Dengan','',array('class'=>'control-label')); ?>
                                <div class="controls">
                                         <?php   
                                                $this->widget('MyDateTimePicker',array(
                                                        'model'=>$model,
                                                        'attribute'=>'tgl_akhir',
                                                        'mode'=>'date',
                                                        'options'=> array(
    //                                                            'dateFormat'=>Params::DATE_FORMAT,
                                                                'dateFormat'=>'yy-mm-dd',
                                                                'maxDate' => 'd',
                                                        ),
                                                        'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                        )); ?>
                                </div>
                        </div>

			<?php echo $form->textFieldRow($model,'no_sep',array('class'=>'span3','maxlength'=>100)); ?>
			
			<?php echo $form->textFieldRow($model,'nopeserta_bpjs',array('class'=>'span3','maxlength'=>50)); ?>

		</div>
                <div class="span6">
                    <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','maxlength'=>100)); ?>
                </div>
		
	</div>
	<div class="form-actions">
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
	</div>

<?php $this->endWidget(); ?>
