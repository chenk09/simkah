<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<?php
$this->breadcrumbs=array(
	'Assep Ts'=>array('index'),
	'Create',
);
?>
<div class="white-container">
	<legend class="rim2">Tambah Surat Eligibilitas Peserta <b>(SEP)</b></legend>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

	<?php echo $this->renderPartial('_form', array('model'=>$model,'modInfoKunjungan'=>$modInfoKunjungan,)); ?>
	<?php echo $this->renderPartial('_jsFunctions',array('model'=>$model)); ?>
</div>