<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'assep-t-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
//	'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event);', 'onsubmit'=>'return requiredCheck(this);'),
    'htmlOptions' => array('onKeyPress' => ''),
    'focus' => '#',
));
?>
    <?php
    if (isset($_GET['sukses'])) {
        Yii::app()->user->setFlash('success', "SEP berhasil disimpan !");
    }
    ?>
    <?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset class="box" id="content-bpjs">
        <div id="animation"></div>
	<div class="row-fluid ">
                <div class="span6">
                    <?php echo $form->hiddenField($model,'notelpon_peserta',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"")); ?>
                <div class="control-group ">
                    <label class="control-label">
                        No. SEP
                    </label>
                    <div class="controls">
                    <?php echo $form->textField($model, 'no_sep', array('placeholder' => 'No. SEP Otomatis', 'class' => 'span3', 'readonly' => true, 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo $form->labelEx($model, 'tglsep', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'tglsep', array('readonly'=>true,'placeholder' => 'Ketik No. Peserta', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo CHtml::label("Poli Tujuan <span class='required'>*</span> ", 'no_rujukan', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'politujuan_kode', array('readonly'=>true,'placeholder' => 'Poli Tujuan', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?><br>
                        <?php echo $form->textField($model, 'politujuan_nama', array('readonly'=>true,'placeholder' => 'Poli Tujuan', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group form-inline">
                    <?php echo CHtml::label("Poli Eksekutif", 'Eksekutif', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        echo $form->radioButtonList($model, 'polieksekutif', array("1" => "YA&nbsp;&nbsp;", "0" => "TIDAK"), array('onkeyup' => "",'disabled'=>true));
                        ?>
                    </div>
                </div>
                <div class="control-group ">
                        <?php echo CHtml::label("Kelas Tanggungan <span class='required'>*</span>", 'kelastanggungan', array('class' => 'control-label')) ?>
                    <div class="controls">
                    <?php echo $form->dropDownList($model, 'hakkelas_kode', array('1' => 'Kelas I', '2' => 'Kelas II', '3' => 'Kelas III'), array('empty' => '-Pilih-', 'class' => 'span3 required', 'onkeyup' => "", 'disabled'=>true
                    ));
                    ?>
                    </div>		
                </div>
                
            </div>
            <div class="span6">
                <div class="control-group">
                        <?php echo CHtml::label("Diagnosa <span class='required'>*</span> ", 'no_rujukan', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'diagnosaawal_kode', array('readonly'=>true,'placeholder' => 'Diagnosa Awal', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?><br>
                        <?php echo $form->textField($model, 'diagnosaawal_nama', array('readonly'=>true,'placeholder' => 'Diagnosa Awal', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo CHtml::label("No. Kartu BPJS <span class='required'>*</span> ", 'nopeserta', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'nopeserta_bpjs', array('readonly'=>true,'placeholder' => 'Ketik No. Peserta', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label required">
                        Tanggal Pulang
                        <span class="required">*</span>
                    </label>
                    <div class="controls">
                        <?php   
                            $this->widget('MyDateTimePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'tanggalpulang_sep',
                                    'mode'=>'date',
                                    'options'=> array(
                                            'dateFormat'=>'yy-mm-dd',
                                            'showOn' => false,
                                            'maxDate' => 'd',
                                            'yearRange'=> "-150:+0",
                                    ),
                                    'htmlOptions'=>array('placeholder'=>'00/00/0000','class'=>'dtPicker2 span2 datetime required','onkeyup'=>""
                                    ),
                        )); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label("User Pembuat SEP", 'pembuat_sep', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'pembuat_sep', array('readonly' => true, 'placeholder' => 'Pembuat SEP', 'class' => 'span3', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <?php echo $form->textArea($model, 'catatan_sep', array('placeholder' => '', 'class' => 'span3', 'onkeyup' => "",'style'=>"display:none")); ?>

            </div>
	</div>
    </fieldset>
    <div class="form-actions">
        <?php
            $sukses = isset($_GET['sukses']) ? $_GET['sukses'] : null;
            $disabledSave = isset($_GET['id']) ? true : ($sukses == 1) ? true : false;
        ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','disabled'=>$disabledSave, 'onclick'=>'cekInput(this,15);return false;')); ?>

    </div>
<?php $this->endWidget(); ?>
<?php echo $this->renderPartial('_jsFunctions',array('model'=>$model)); ?>

<script>
$(document).ready(function(){
    setLakaLantas($('#ARSepT_is_lakalantas'));
});
</script>