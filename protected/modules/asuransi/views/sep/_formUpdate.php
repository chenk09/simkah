<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'assep-t-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
//	'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event);', 'onsubmit'=>'return requiredCheck(this);'),
    'htmlOptions' => array('onKeyPress' => ''),
    'focus' => '#',
));
?>
    <?php
    if (isset($_GET['sukses'])) {
        Yii::app()->user->setFlash('success', "SEP berhasil disimpan !");
    }
    ?>
    <?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset class="box" id="content-bpjs">
        <div id="animation"></div>
	<div class="row-fluid ">
                <div class="span6">
                <?php echo CHtml::hiddenField('no_rekam_medik', $modInfoKunjungan->no_rekam_medik, array('readonly' => true, 'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);")); ?>
                <div class="control-group">
                        <?php echo CHtml::label("No. Kartu BPJS <span class='required'>*</span> ", 'nopeserta', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'nopeserta_bpjs', array('readonly'=>true,'placeholder' => 'Ketik No. Peserta', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>

                <div class="control-group ">
                        <?php echo CHtml::label("Jenis/Asal Rujukan", 'no_rujukan', array('class' => 'control-label')) ?>
                    <div class="controls form-inline">
                        <?php
                        echo $form->radioButtonList($model, 'jenisrujukan_kode_bpjs', array("1" => "PCare&nbsp;&nbsp;", "2" => "Rumah Sakit"), array('onkeyup' => ""));
                        ?>
                    </div>		
                </div>
                <div class="control-group">
                        <?php echo CHtml::label("No.Rujukan Faskes 1<span class='required'>*</span> <i class=\"icon-search\" onclick=\"getRujukanNoRujukan($('#" . CHtml::activeId($modRujukanBpjs, "no_rujukan") . "').val());\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek rujukan\"></i>", 'no_rujukan', array('class' => 'control-label')) ?>
                    <div class="controls">
                    <?php echo $form->textField($model, 'norujukan_bpjs', array('placeholder' => 'No. Rujukan', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label">
                        No. SEP
                    </label>
                    <div class="controls">
                    <?php echo $form->textField($model, 'no_sep', array('placeholder' => 'No. SEP Otomatis', 'class' => 'span3', 'readonly' => true, 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo $form->labelEx($model, 'tglsep', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'tglsep', array('readonly'=>true,'placeholder' => 'Ketik No. Peserta', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($model, 'namapeserta_bpjs', array('placeholder' => 'Nama Lengkap Pemilik Asuransi', 'class' => 'span3', 'readonly' => true, 'onkeyup' => "", 'maxlength' => 50)); ?>
                <div class="control-group ">
                    <?php echo CHtml::label("Kode PPK Pelayanan", 'ppkpelayanan', array('class' => 'control-label')) ?>
                    <div class="controls">
                    <?php echo $form->textField($model, 'ppkpelayanan', array('class' => 'span3', 'onkeyup' => "", 'readonly' => true, 'maxlength' => 50)); ?>
                    </div>		
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label("Nama PPK Pelayanan", 'ppkpelayanan', array('class' => 'control-label')) ?>
                    <div class="controls">
                    <?php echo $form->textField($model, 'ppkpelayanan_nama', array('class' => 'span3', 'onkeyup' => "", 'readonly' => true, 'maxlength' => 50)); ?>
                    </div>		
                </div>
                        <?php echo $form->dropDownListRow($model, 'jnspelayanan_kode', array('2'=>"Rawat Jalan",'1'=>"Rawat Inap"), array('empty' => '--Pilih--', 'class' => 'span3','disabled'=>true)); ?>
                <div class="control-group ">
                        <?php echo CHtml::label("Kelas Rawat <span class='required'>*</span>", 'kelastanggungan', array('class' => 'control-label')) ?>
                    <div class="controls">
                    <?php echo $form->dropDownList($model, 'kelasrawat_kode', array('1' => 'Kelas I', '2' => 'Kelas II', '3' => 'Kelas III'), array('empty' => '-Pilih-', 'class' => 'span3 required', 'onkeyup' => "",
                    ));
                    ?>
                    <?php echo $form->hiddenField($model,'hakkelas_kode',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
                    </div>		
                </div>

                <div class="control-group">
                    <?php echo CHtml::label("Kode PPK Rujukan <span class='required'>*</span><i class=\"icon-search\" onclick=\"$('#dialogPpk').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek ppk rujukan\"></i>", 'no_rujukan', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'ppkrujukanasal_kode', array('placeholder' => 'Kode PPK Rujukan', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label("Nama PPK Rujukan <span class='required'>*</span><i class=\"icon-search\" onclick=\"$('#dialogPpk').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek ppk rujukan\"></i>", 'no_rujukan', array('class' => 'control-label')) ?>
                    <div class="controls">
                    <?php echo $form->textField($model, 'ppkrujukanasal_nama', array('placeholder' => 'Nama PPK Rujukan', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label class="control-label required">
                        Tanggal Rujukan
                        <span class="required">*</span>
                    </label>
                    <div class="controls">
                        <?php   
                            $this->widget('MyDateTimePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'tglrujukan_bpjs',
                                    'mode'=>'date',
                                    'options'=> array(
                                        'dateFormat'=>"yy-mm-dd",
                                        'showOn' => false,
                                        'maxDate' => 'd',
                                        'yearRange'=> "-150:+0",
                                    ),
                                    'htmlOptions'=>array('placeholder'=>'00/00/0000','class'=>'dtPicker2 span2 datetime','onkeyup'=>""
                                    ),
                        )); ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo CHtml::label("Poli Tujuan <span class='required'>*</span> ", 'no_rujukan', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'politujuan_kode', array('readonly'=>true,'placeholder' => 'Poli Tujuan', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo CHtml::label("Diagnosa Awal <span class='required'>*</span> <i class=\"icon-search\" onclick=\"$('#dialogDiagnosaBpjs').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek rujukan\"></i>", 'no_rujukan', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'diagnosaawal_kode', array('placeholder' => 'Diagnosa Awal', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group form-inline">
                    <?php echo CHtml::label("Poli Eksekutif", 'polieksekutif', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        echo $form->radioButtonList($model, 'polieksekutif', array("1" => "YA&nbsp;&nbsp;", "0" => "TIDAK"), array('onkeyup' => ""));
                        ?>
                    </div>
                </div>
                 <div class="control-group form-inline">
                    <?php echo CHtml::label("COB", 'COB', array('class'=>'control-label'))?>
                    <div class="controls">
                            <?php echo $form->textField($model,'cob_status',array('class'=>'span1', 'onkeyup'=>"return $(this).focusNextInputField(event);",'readonly'=>true)); ?>
                            <?php echo $form->hiddenField($model,'cob_bpjs',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", 'readonl'=>true)); ?>
                    </div>
                </div>
                <div class="control-group form-inline">
                    <?php echo CHtml::label("Laka Lantas", 'Lantas', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        echo $form->radioButtonList($model, 'lakalantas_kode', array("1" => "YA&nbsp;&nbsp;", "0" => "TIDAK"), array('onkeyup' => "", 'onchange' => 'setLakaLantas(this)'));
                        ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label("Penjamin", 'Penjamin', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->dropDownList($model, 'penjaminlakalantas', array('1' => 'Jasa Raharja PT', '2' => 'BPJS Ketenagakerjaan', '3' => 'TASPEN', '4' => 'ASABRI PT'), array('empty' => '-- Pilih --', 'class' => 'span3', 'onkeyup' => "",
                        ));
                        ?>
                    </div>		
                </div>
                <div class="control-group">
                        <?php echo CHtml::label("Lokasi Laka Lantas", 'lokasi_lakalantas', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'lokasilakalantas', array('placeholder' => 'Lokasi laka lantas', 'class' => 'span3', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo CHtml::label("No. Telepon Peserta <span class='required'>*</span>", 'no_telpon_peserta', array('class' => 'control-label')) ?>
                    <div class="controls">
                <?php echo $form->textField($model, 'notelpon_peserta', array('placeholder' => 'Telepon peserta', 'class' => 'span3 required', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label("User Pembuat SEP", 'pembuat_sep', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model, 'pembuat_sep', array('readonly' => true, 'placeholder' => 'Pembuat SEP', 'class' => 'span3', 'onkeyup' => "", 'maxlength' => 50)); ?>
                    </div>
                </div>
                <?php echo $form->textAreaRow($model, 'catatan_sep', array('placeholder' => '', 'class' => 'span3', 'onkeyup' => "")); ?>

            </div>
	</div>
    </fieldset>
    <div class="form-actions">
        <?php
            $sukses = isset($_GET['sukses']) ? $_GET['sukses'] : null;
            $disabledSave = isset($_GET['id']) ? true : ($sukses == 1) ? true : false;
        ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','disabled'=>$disabledSave, 'onclick'=>'cekInput(this,14);return false;')); ?>

    </div>
<?php $this->endWidget(); ?>
<?php echo $this->renderPartial('_jsFunctions',array('model'=>$model)); ?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPoli',
    'options' => array(
        'title' => 'Referensi Poli BPJS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 500,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('_pencarianPoli');
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDiagnosaBpjs',
    'options' => array(
        'title' => 'Referensi Diagnosa BPJS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 500,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('_pencarianDiagnosa');
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPpk',
    'options' => array(
        'title' => 'Referensi PPK Rujukan/Faskes',
        'autoOpen' => false,
        'modal' => true,
        'width' => 500,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('_pencarianPpk');
$this->endWidget();
?>
<script>
$(document).ready(function(){
    setLakaLantas($('input:radio[name="ARSepT[lakalantas_kode]"]:checked'));
});
</script>