<?php
$this->breadcrumbs=array(
	'Cob Bpjs Inacbg'=>array('index'),
	$model->cob_bpjs_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Cob Bpjs Inacbg #'.$model->cob_bpjs_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Cob Bpjs Inacbg', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Cob Bpjs Inacbg', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Cob Bpjs Inacbg', 'icon'=>'pencil','url'=>array('update','id'=>$model->cob_bpjs_id))) :  '' ;
                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Cob Bpjs Inacbg','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->cob_bpjs_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Cob Bpjs Inacbg', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'cob_bpjs_id',
		'cob_bpjs_kode',
		'cob_bpjs_nama',
		'cob_bpjs_aktif',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>