<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('cob_bpjs_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->cob_bpjs_id),array('view','id'=>$data->cob_bpjs_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cob_bpjs_kode')); ?>:</b>
	<?php echo CHtml::encode($data->cob_bpjs_kode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cob_bpjs_nama')); ?>:</b>
	<?php echo CHtml::encode($data->cob_bpjs_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cob_bpjs_aktif')); ?>:</b>
	<?php echo CHtml::encode($data->cob_bpjs_aktif); ?>
	<br />


</div>