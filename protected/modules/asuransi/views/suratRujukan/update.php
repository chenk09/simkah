<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<!--<div class="white-container">-->
	<!--<legend class="rim2">Tambah Surat Rujukan Keluar</legend>-->
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rujukan-t-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
//	'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event);', 'onsubmit'=>'return requiredCheck(this);'),
	'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event);'),
	'focus'=>'#',
)); ?>
        <?php
	if (isset($_GET['sukses'])) {
		Yii::app()->user->setFlash('success', "Rujukan berhasil disimpan !");
	}
	?>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

	<!--<div class="row-fluid">-->
        <?php 
        $readonly = TRUE;
        ?>
        <fieldset class="box">
            <legend class="rim">Pembuat Rujukan Keluar</legend>
            <div class="row-fluid">
                <div class = "span6">
                    <?php echo CHtml::hiddenField('sep_id',$modSep->sep_id,array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
                    <div class="control-group">
                        <?php echo CHtml::label("No. SEP <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
                        <div class="controls">
                            <?php
                                echo CHtml::textField('no_sep', $modSep->no_sep, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
                            ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("No. Pendaftaran <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
                        <div class="controls">
                            <?php
                                echo CHtml::textField('no_pendaftaran', $modSep->pendaftaran->no_pendaftaran, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
                            ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("No. Rekam Medik <font style=color:red;> * </font>", 'no_rekam_medik', array('class' => 'control-label required')); ?>
                        <div class="controls">
                            <?php
                                echo CHtml::textField('no_rekam_medik', $modSep->pasien->no_rekam_medik, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
                            ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label('Tgl. Pendaftaran', 'tgl_pendaftaran', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo CHtml::textField('tgl_pendaftaran', $modSep->pendaftaran->tgl_pendaftaran, array('readonly' => true, 'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);")); ?>
                        </div>
                    </div>
                </div>
                <div class = "span6">
                    <div class="control-group">
                        <?php echo CHtml::label("Nama Pasien <font style=color:red;> * </font>", 'nama_pasien', array('class' => 'control-label required')); ?>
                        <div class="controls">
                            <?php
                                echo CHtml::textField('nama_pasien', $modSep->pasien->nama_pasien, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
                            ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label('Tanggal Lahir', 'tanggal_lahir', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo CHtml::textField('tanggal_lahir', $modSep->pasien->tanggal_lahir, array('readonly' => true, 'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);")); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("Jenis Kelamin", 'jeniskelamin', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo CHtml::textField('jeniskelamin', $modSep->pasien->jeniskelamin, array('readonly' => true, 'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);")); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("Alamat Pasien", 'alamat_pasien', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo CHtml::textArea('alamat_pasien', $modSep->pasien->alamat_pasien, array('readonly' => true, 'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);")); ?>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <div id="animation"></div>
        <fieldset class="box" id="content-bpjs">
            <legend class="rim">Data Rujukan</legend>
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group ">
                        <?php echo CHtml::label("No. Rujukan <span class='required'>*</span>", 'norujukan', array('class'=>'control-label required'))?>
                        <div class="controls">
                            <?php echo $form->textField($model,'no_rujukan',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                        </div>		
                    </div>
                    <div class="control-group ">
                        <?php echo CHtml::label("Tanggal Rujukan <span class='required'>*</span>", 'norujukan', array('class'=>'control-label required'))?>
                        <div class="controls">
                            <?php echo $form->textField($model,'tgl_dirujuk',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                        </div>		
                    </div>
                    <div class="control-group ">
                        <?php echo CHtml::label("Jenis Faskes", 'Jenis Faskes', array('class'=>'control-label'))?>
                        <div class="controls form-inline">
                            <?php 
                            echo $form->radioButtonList($model,'jenisfaskes',array("1"=>"PCare&nbsp;&nbsp;","2"=>"Rumah Sakit"), array('onkeyup'=>"return $(this).focusNextInputField(event)"));
                            ?>
                        </div>		
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("Kode PPK Dirujuk ke <span class='required'>*</span><i class=\"icon-search\" onclick=\"$('#dialogPpk').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek ppk rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
                        <div class="controls">
                            <?php echo $form->textField($model,'ppk_dirujuk',array('placeholder'=>'Kode PPK Rujukan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("Nama PPK Dirujuk ke <span class='required'>*</span><i class=\"icon-search\" onclick=\"$('#dialogPpk').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek ppk rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
                        <div class="controls">
                            <?php echo $form->textField($model,'ppk_dirujuk_nama',array('placeholder'=>'Nama PPK Rujukan','class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("Diagnosa Awal <span class='required'>*</span> <i class=\"icon-search\" onclick=\"$('#dialogDiagnosaBpjs').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
                        <div class="controls">
                                <?php echo $form->hiddenField($model,'diagnosa_rujukan_nama',array('readonly'=>true,'placeholder'=>'Poli Tujuan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
                                <?php echo $form->textField($model,'diagnosa_rujukan_kode',array('placeholder'=>'Diagnosa Awal','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                        </div>
                    </div>

                </div>
                <div class="span6">
                    <div class="control-group">
                        <?php echo CHtml::label("Jenis Pelayanan <span class='required'>*</span>", 'Pelayanan', array('class'=>'control-label'))?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model,'jnspelayanan',  array('2'=>"Rawat Jalan",'1'=>"Rawat Inap"),array('empty'=>'--Pilih--','class'=>'span3 required')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("Tipe Rujukan <span class='required'>*</span>", 'Tipe Rujukan', array('class'=>'control-label'))?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model,'tipe_rujukan',  array('0'=>"Penuh",'1'=>"Partial",'2'=>"Rujuk Balik"),array('empty'=>'--Pilih--','class'=>'span3 required')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("Poli Rujukan <span class='required'>*</span> <i class=\"icon-search\" onclick=\"$('#dialogPoli').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
                        <div class="controls">
                                <?php echo $form->hiddenField($model,'poli_rujukan_nama',array('readonly'=>true,'placeholder'=>'Poli Tujuan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                                <?php echo $form->textField($model,'poli_rujukan',array('placeholder'=>'Poli Tujuan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("User BPJS", 'userinput_bpjs', array('class'=>'control-label'))?>
                        <div class="controls">
                                <?php echo $form->textField($model,'create_user_bpjs',array('readonly'=>true,'placeholder'=>'Pembuat SEP','class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label("Catatan <span class='required'>*</span>", 'catatandokterperujuk', array('class'=>'control-label'))?>
                        <div class="controls">
                                <?php echo $form->textArea($model,'catatan_rujukan', array('placeholder'=>'','class'=>'span3 required','onkeyup'=>"return $(this).focusNextInputField(event)")); ?>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
	<!--</div>-->
	<div class="row-fluid">
		<div class="form-actions">
			<?php
				$sukses = isset($_GET['sukses']) ? $_GET['sukses'] : null;
				$disabledSave = ($sukses == 1)? true : false;
			?>
			<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','disabled'=>$disabledSave, 'onclick'=>'cekInput(2);return false;')); ?>
			<?php echo CHtml::link(Yii::t('mds','{icon} Ulang',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
					$this->createUrl('update'), 
					array('class'=>'btn btn-danger',
                                                'onclick'=>'if(!confirm("'.Yii::t('mds','Anda yakin akan mengulang?').'")) return false;')); ?>
			<?php
				if(Yii::app()->user->getState('is_bridging')){
					if (isset($model->rujukankeluar_bpjs_id)) {
						echo CHtml::link(Yii::t('mds', '{icon} Print Rujukan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('class'=>'btn btn-info','onclick'=>"printRujukan();return false",'disabled'=>FALSE  ));
					}else{
						echo CHtml::link(Yii::t('mds', '{icon} Print Rujukan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('rel'=>'tooltip','title'=>'Belum memiliki No. Rujukan!','class'=>'btn btn-info','onclick'=>"return false",'disabled'=>true, 'style'=>'cursor:not-allowed;'));
					}
				}else{
					echo CHtml::link(Yii::t('mds', '{icon} Print Rujukan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('rel'=>'tooltip','title'=>'Fitur Bridging tidak aktif!','class'=>'btn btn-info','onclick'=>"return false",'disabled'=>true, 'style'=>'cursor:not-allowed;'));
				}
			?>
		</div>
	</div>
<?php $this->endWidget(); ?>
<!--</div>-->
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPoli',
    'options' => array(
        'title' => 'Referensi Poli BPJS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 500,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('asuransi.views.sep._pencarianPoli');
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDiagnosaBpjs',
    'options' => array(
        'title' => 'Referensi Diagnosa BPJS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 500,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('asuransi.views.sep._pencarianDiagnosa');
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPpk',
    'options' => array(
        'title' => 'Referensi PPK Rujukan/Faskes',
        'autoOpen' => false,
        'modal' => true,
        'width' => 500,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('asuransi.views.sep._pencarianPpk');
$this->endWidget();
?>
<?php 
echo $this->renderPartial('_jsFunctions',array('model'=>$model));
?>