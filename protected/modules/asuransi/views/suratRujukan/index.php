<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div class="white-container">
	<legend class="rim2">Tambah Surat Rujukan Keluar</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rujukan-t-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
//	'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event);', 'onsubmit'=>'return requiredCheck(this);'),
	'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event);'),
	'focus'=>'#',
)); ?>
        <?php
	if (isset($_GET['sukses'])) {
		Yii::app()->user->setFlash('success', "Rujukan berhasil disimpan !");
	}
	?>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

	<!--<div class="row-fluid">-->
        <fieldset class="box">
            <legend class="rim">Pembuat Rujukan Keluar</legend>
            <div class="row-fluid">
                <?php echo $this->renderPartial('_formKunjungan',array('form'=>$form,'model'=>$model,'modSep'=>$modSep,)); ?>
            </div>
        </fieldset>
        <div id="animation"></div>
        <fieldset class="box" id="content-bpjs">
            <legend class="rim">Data Rujukan</legend>
            <div class="row-fluid">
                <?php echo $this->renderPartial('_formRujukan',array('form'=>$form,'model'=>$model)); ?>
            </div>
        </fieldset>
	<!--</div>-->
	<div class="row-fluid">
		<div class="form-actions">
			<?php
				$sukses = isset($_GET['sukses']) ? $_GET['sukses'] : null;
				$disabledSave = isset($_GET['id']) ? true : ($sukses == 1) ? true : false;
			?>
			<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','disabled'=>$disabledSave, 'onclick'=>'cekInput(1);return false;')); ?>
			<?php echo CHtml::link(Yii::t('mds','{icon} Ulang',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
					$this->createUrl('index'), 
					array('class'=>'btn btn-danger',
                                                'onclick'=>'if(!confirm("'.Yii::t('mds','Anda yakin akan mengulang?').'")) return false;')); ?>
			<?php
				if(Yii::app()->user->getState('is_bridging')){
					if (isset($model->rujukankeluar_bpjs_id)) {
						echo CHtml::link(Yii::t('mds', '{icon} Print Rujukan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('class'=>'btn btn-info','onclick'=>"printRujukan();return false",'disabled'=>FALSE  ));
					}else{
						echo CHtml::link(Yii::t('mds', '{icon} Print Rujukan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('rel'=>'tooltip','title'=>'Belum memiliki No. Rujukan!','class'=>'btn btn-info','onclick'=>"return false",'disabled'=>true, 'style'=>'cursor:not-allowed;'));
					}
				}else{
					echo CHtml::link(Yii::t('mds', '{icon} Print Rujukan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0);', array('rel'=>'tooltip','title'=>'Fitur Bridging tidak aktif!','class'=>'btn btn-info','onclick'=>"return false",'disabled'=>true, 'style'=>'cursor:not-allowed;'));
				}
			?>
			<?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan Rujukan',array('{icon}'=>'<i class="icon-folder-open icon-white"></i>')),$this->createUrl('admin',array('modul_id'=> Yii::app()->session['modul_id'])), array('class'=>'btn btn-success')); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
</div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPoli',
    'options' => array(
        'title' => 'Referensi Poli BPJS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 960,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('asuransi.views.sep._pencarianPoli');
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDiagnosaBpjs',
    'options' => array(
        'title' => 'Referensi Diagnosa BPJS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 960,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('asuransi.views.sep._pencarianDiagnosa');
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPpk',
    'options' => array(
        'title' => 'Referensi PPK Rujukan/Faskes',
        'autoOpen' => false,
        'modal' => true,
        'width' => 960,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('asuransi.views.sep._pencarianPpk');
$this->endWidget();
?>
<?php 
echo $this->renderPartial('_jsFunctions',array('model'=>$model));
?>