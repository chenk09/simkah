<script type="text/javascript">	
    
    function cekInput(param){
        var status = 0;
        var nosep = $("#no_sep").val();
        if(!requiredCheck("#rujukan-t-form")){
            
        }else{
            status = 1;
        }
        $("#animation").addClass("loader");

        var noSep = $('#no_sep').val();
        var tglRujukan = $("#<?php echo CHtml::activeId($model, 'tgl_dirujuk') ?>").val();
        var ppkDirujuk = $("#<?php echo CHtml::activeId($model, 'ppk_dirujuk') ?>").val();
        var jnsPelayanan = $("#<?php echo CHtml::activeId($model, 'jnspelayanan') ?>").val();
        var catatan = $("#<?php echo CHtml::activeId($model, 'catatan_rujukan') ?>").val();
        var diagRujukan = $("#<?php echo CHtml::activeId($model, 'diagnosa_rujukan_kode') ?>").val();
        var tipeRujukan = $("#<?php echo CHtml::activeId($model, 'tipe_rujukan') ?>").val();
        var poliRujukan = $("#<?php echo CHtml::activeId($model, 'poli_rujukan') ?>").val();
        var user = $("#<?php echo CHtml::activeId($model, 'create_user_bpjs') ?>").val();
        var noRujukan = $("#<?php echo CHtml::activeId($model, 'no_rujukan') ?>").val();
        
        if(status==1){
            $("#animation").addClass("loader");
            var setting = {
                    url: "<?php echo $this->createUrl('bpjsInterface'); ?>",
                    type: 'GET',
                    dataType: 'html',
                    data: 'param=' +param+ '&noSep='+noSep+'&tglRujukan='+tglRujukan+'&ppkDirujuk='+ppkDirujuk+'&jnsPelayanan='+jnsPelayanan+'&jnsPelayanan='+jnsPelayanan+'&catatan='+catatan+'&diagRujukan='+diagRujukan+'&tipeRujukan='+tipeRujukan+'&poliRujukan='+poliRujukan+'&user='+user+'&noRujukan='+noRujukan,
                    beforeSend: function () {
                        $("#animation").addClass("loader");
                    },
                    success: function (data) {
                        var obj = JSON.parse(data);
                        if(obj.metaData.code != '200'){
                            alert(obj.metaData.message);
                        }else{
                            if (obj.response != null) {
                                if(param==1){//insert
                                    var rujukan = obj.response.rujukan;
                                    $("#<?php echo CHtml::activeId($model, 'no_rujukan') ?>").val(rujukan.noRujukan);
                                }else{//update

                                }
                                $("#rujukan-t-form").submit();
                            }
                        }
                        $("#animation").removeClass("loader");
                    },
                    error: function (data) {
                        $("#animation").removeClass("loader");
                    }
            }
            if (typeof ajax_request !== 'undefined')
            ajax_request.abort();
            ajax_request = $.ajax(setting);
        }
    }
    
    function requiredCheck(obj){
        var kosong = 0;
        $(obj).find('input,select,textarea').each(function(){
            if($(this).parents(".control-group").find("label").hasClass('required') === true ){
                $(this).parents(".control-group").removeClass("error").removeClass("success");
            }
        });
        $(obj).find('input,select,textarea').each(function(){
            if($(this).parents(".control-group").find("label").hasClass('required') === true || $(this).hasClass('required')){
                if(($(this).val() === "")){
                    if($(this).is(":hidden")){ //untuk element type:hidden 
                        var radio_checked = false;
                        $(this).parent().find(".radio").each(function(){ //mengecek element radio button
                            if($(this).find("input").is(":checked")){
                                radio_checked = true;
                            }
                        });
                        if(radio_checked == false){
                            $(this).parents(".control-group").addClass("error");
                            $(this).addClass("error");
                            kosong ++;
                        }else{
                            $(this).parents(".control-group").removeClass("error");
                            $(this).removeClass("error");
                        }
                    }else{
                        $(this).parents(".control-group").addClass("error");
                        $(this).addClass("error");
                        kosong ++;
                    }
                }else{
                    $(this).parents(".control-group").removeClass("error");
                                    $(this).removeClass("error");
                }
            }
        });
        if(kosong > 0){
            window.parent.alert("Silahkan isi yang bertanda bintang * !");//("+kosong+" input)
            return false;
        }else{
            return true;
        }
    }
    
    function printRujukan(){
        window.open('<?php echo $this->createUrl('printRujukan',array('id'=>$model->rujukankeluar_bpjs_id)); ?>','printwin','left=100,top=100,width=860,height=480');
    }
    
    function cekFaskes(kode){
        var setting = {
            url: "<?php echo $this->createUrl('CekFaskes'); ?>",
            type: 'GET',
            dataType: 'html',
            data: 'kode=' +kode,
            beforeSend: function () {
                
            },
            success: function (data) {
                
            },
            error: function (data) {
                
            }
        }
        if (typeof ajax_request !== 'undefined')
        ajax_request.abort();
        ajax_request = $.ajax(setting);
    }
    
    function setKunjunganSEP(sep_id){
        var setting = {
                url: "<?php echo $this->createUrl('setKunjunganSEP'); ?>",
                type: 'GET',
                dataType: 'html',
                data: 'sep_id=' +sep_id,
                beforeSend: function () {
                    
                },
                success: function (data) {
                    var obj = JSON.parse(data);
                    $("#no_sep").val(obj.no_sep);
                    $("#sep_id").val(obj.sep_id);
                    $("#no_pendaftaran").val(obj.no_pendaftaran);
                    $("#no_rekam_medik").val(obj.no_rekam_medik);
                    $("#tgl_pendaftaran").val(obj.tgl_pendaftaran);
                    $("#nama_pasien").val(obj.nama_pasien);
                    $("#tanggal_lahir").val(obj.tanggal_lahir);
                    $("#jeniskelamin").val(obj.jeniskelamin);
                    $("#alamat_pasien").val(obj.alamat_pasien);
                },
                error: function (data) {
                    
                }
        }
        if (typeof ajax_request !== 'undefined')
        ajax_request.abort();
        ajax_request = $.ajax(setting);
    }

</script>