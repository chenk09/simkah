<div class="span6">
    <div class="control-group ">
        <?php echo CHtml::label("No. Rujukan <span class='required'>*</span>", 'norujukan', array('class'=>'control-label required'))?>
        <div class="controls">
            <?php echo $form->textField($model,'no_rujukan',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
        </div>		
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model,'tgl_dirujuk',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php   
                $this->widget('MyDateTimePicker',array(
                        'model'=>$model,
                        'attribute'=>'tgl_dirujuk',
                        'mode'=>'date',
                        'options'=> array(
                            'showOn' => false,
                            'dateFormat' => 'yy-mm-dd',
                            'maxDate' => 'd',
                            'yearRange'=> "-150:+0",
                        ),
                        'htmlOptions'=>array('placeholder'=>'00/00/0000 00:00:00','class'=>'dtPicker3 span3 datetime','onkeyup'=>"return $(this).focusNextInputField(event)"
                        ),
            )); ?>
        </div>
    </div>
    <div class="control-group ">
        <?php echo CHtml::label("Jenis Faskes", 'Jenis Faskes', array('class'=>'control-label'))?>
        <div class="controls form-inline">
            <?php 
            echo $form->radioButtonList($model,'jenisfaskes',array("1"=>"PCare&nbsp;&nbsp;","2"=>"Rumah Sakit"), array('onkeyup'=>"return $(this).focusNextInputField(event)"));
            ?>
        </div>		
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Kode PPK Dirujuk ke <span class='required'>*</span><i class=\"icon-search\" onclick=\"$('#dialogPpk').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek ppk rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
        <div class="controls">
            <?php echo $form->textField($model,'ppk_dirujuk',array('placeholder'=>'Kode PPK Rujukan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Nama PPK Dirujuk ke <span class='required'>*</span><i class=\"icon-search\" onclick=\"$('#dialogPpk').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek ppk rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
        <div class="controls">
            <?php echo $form->textField($model,'ppk_dirujuk_nama',array('placeholder'=>'Nama PPK Rujukan','class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Diagnosa Awal <span class='required'>*</span> <i class=\"icon-search\" onclick=\"$('#dialogDiagnosaBpjs').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
        <div class="controls">
                <?php echo $form->hiddenField($model,'diagnosa_rujukan_nama',array('readonly'=>true,'placeholder'=>'Poli Tujuan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textField($model,'diagnosa_rujukan_kode',array('placeholder'=>'Diagnosa Awal','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
        </div>
    </div>
    
</div>
<div class="span6">
    <div class="control-group">
        <?php echo CHtml::label("Jenis Pelayanan <span class='required'>*</span>", 'Pelayanan', array('class'=>'control-label'))?>
        <div class="controls">
            <?php echo $form->dropDownList($model,'jnspelayanan',  array('2'=>"Rawat Jalan",'1'=>"Rawat Inap"),array('empty'=>'--Pilih--','class'=>'span3 required')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Tipe Rujukan <span class='required'>*</span>", 'Tipe Rujukan', array('class'=>'control-label'))?>
        <div class="controls">
            <?php echo $form->dropDownList($model,'tipe_rujukan',  array('0'=>"Penuh",'1'=>"Partial",'2'=>"Rujuk Balik"),array('empty'=>'--Pilih--','class'=>'span3 required')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Poli Rujukan <span class='required'>*</span> <i class=\"icon-search\" onclick=\"$('#dialogPoli').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
        <div class="controls">
                <?php echo $form->hiddenField($model,'poli_rujukan_nama',array('readonly'=>true,'placeholder'=>'Poli Tujuan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                <?php echo $form->textField($model,'poli_rujukan',array('placeholder'=>'Poli Tujuan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("User BPJS", 'userinput_bpjs', array('class'=>'control-label'))?>
        <div class="controls">
                <?php echo $form->textField($model,'create_user_bpjs',array('readonly'=>true,'placeholder'=>'Pembuat SEP','class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Catatan <span class='required'>*</span>", 'catatandokterperujuk', array('class'=>'control-label'))?>
        <div class="controls">
                <?php echo $form->textArea($model,'catatan_rujukan', array('placeholder'=>'','class'=>'span3 required','onkeyup'=>"return $(this).focusNextInputField(event)")); ?>
        </div>
    </div>
</div>