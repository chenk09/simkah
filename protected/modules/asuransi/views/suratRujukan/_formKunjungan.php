<?php
$readonly = TRUE;
?>
<div class = "span6">
    <?php echo CHtml::hiddenField('sep_id',$modSep->sep_id,array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
    <div class="control-group">
        <?php echo CHtml::label("No. SEP <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'no_sep',
                    'value' => $modSep->no_sep,
                    'source' => 'js: function(request, response) {
                        $.ajax({
                                url: "' . $this->createUrl('AutoCompleteSEP') . '",
                                dataType: "json",
                                data: {
                                    no_sep: request.term,
                                },
                                success: function (data) {
                                    response(data);
                                }
                        })
                     }',
                    'options' => array(
                        'minLength' => 6,
                        'focus' => 'js:function( event, ui ) {
                            $(this).val( "");
                            return false;
                         }',
                        'select' => 'js:function( event, ui ) {
                            setKunjunganSEP(ui.item.sep_id);
                            $(this).val(ui.item.no_sep);
                            return false;
                        }',
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogPasien'),
                    'htmlOptions' => array('placeholder' => 'Ketik No Sep minimal 6 digit', 'rel' => 'tooltip', 'title' => 'Ketik nama pasien untuk mencari data kunjungan',
                        'onkeyup' => "return $(this).focusNextInputField(event)",
                    ),
                ));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("No. Pendaftaran <font style=color:red;> * </font>", 'no_pendaftaran', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                echo CHtml::textField('no_pendaftaran', $modSep->no_pendaftaran, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("No. Rekam Medik <font style=color:red;> * </font>", 'no_rekam_medik', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                echo CHtml::textField('no_rekam_medik', $modSep->no_rekam_medik, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label('Tgl. Pendaftaran', 'tgl_pendaftaran', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo CHtml::textField('tgl_pendaftaran', $modSep->tgl_pendaftaran, array('readonly' => true, 'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);")); ?>
        </div>
    </div>
</div>
<div class = "span6">
    <div class="control-group">
        <?php echo CHtml::label("Nama Pasien <font style=color:red;> * </font>", 'nama_pasien', array('class' => 'control-label required')); ?>
        <div class="controls">
            <?php
                echo CHtml::textField('nama_pasien', $modSep->nama_pasien, array('class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);", 'readonly' => $readonly));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label('Tanggal Lahir', 'tanggal_lahir', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo CHtml::textField('tanggal_lahir', $modSep->tanggal_lahir, array('readonly' => true, 'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);")); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Jenis Kelamin", 'jeniskelamin', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo CHtml::textField('jeniskelamin', $modSep->jeniskelamin, array('readonly' => true, 'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);")); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo CHtml::label("Alamat Pasien", 'alamat_pasien', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo CHtml::textArea('alamat_pasien', $modSep->alamat_pasien, array('readonly' => true, 'class' => 'span3', 'onkeyup' => "return $(this).focusNextInputField(event);")); ?>
        </div>
    </div>
</div>

<?php
//========= Dialog buat cari data pendaftaran / kunjungan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPasien',
    'options' => array(
        'title' => 'Pencarian Data SEP Pasien',
        'autoOpen' => false,
        'modal' => true,
        'width' => 980,
        'height' => 480,
        'resizable' => false,
    ),
));
$modDialogKunjungan = new ARSepT('searchDialog');
$modDialogKunjungan->unsetAttributes();
$modDialogKunjungan->tglsep = date('Y-m-d');
if (isset($_GET['ARSepT'])) {
    $modDialogKunjungan->attributes = $_GET['ARSepT'];
}

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'datakunjungan-grid',
    'dataProvider' => $modDialogKunjungan->searchDialog(),
    'filter' => $modDialogKunjungan,
    'template' => "{items}\n{pager}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                "id" => "selectPendaftaran",
                "onClick" => "
                    setKunjunganSEP(\"$data->sep_id\");
                    $(\"#dialogPasien\").dialog(\"close\");
                "))',
        ),
        array(
            'header'=>'No. SEP',
            'type'=>'raw',
            'value'=>'$data->no_sep',
        ),
        array(
            'header'=>'No. SEP',
            'type'=>'raw',
            'value'=>'$data->pendaftaran->no_pendaftaran',
        ),
        array(
            'header' => 'Tanggal SEP',
            'type' => 'raw',
            'value' => '$data->tglsep',
            'filter' => $this->widget('MyDateTimePicker', array(
                'model' => $modDialogKunjungan,
                'attribute' => 'tglsep',
                'mode' => 'date', //date / datetime
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'maxDate' => 'd',
                ),
                'htmlOptions' => array('readonly' => true, 'class' => "span2",
                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                    ), true),
        ),
        array(
            'header'=>'No. RM',
            'type'=>'raw',
            'value' => '$data->pasien->no_rekam_medik',
        ),
        array(
            'header'=>'Nama Pasien',
            'type'=>'raw',
            'value' => '$data->pasien->nama_pasien',
        ),
        array(
            'header'=>'Jenis Kelamin',
            'type'=>'raw',
            'value' => '$data->pasien->jeniskelamin',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){
				jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});
				jQuery("#' . CHtml::activeId($modDialogKunjungan, 'tglsep') . '").datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional["id"], {"dateFormat":"dd M yy","maxDate":"d","timeText":"Waktu","hourText":"Jam","minuteText":"Menit","secondText":"Detik","showSecond":true,"timeOnlyTitle":"Pilih Waktu","timeFormat":"hh:mm:ss","changeYear":true,"changeMonth":true,"showAnim":"fold","yearRange":"-80y:+20y"}));
				jQuery("#' . CHtml::activeId($modDialogKunjungan, 'tglsep') . '_date").on("click", function(){jQuery("#' . CHtml::activeId($modDialogKunjungan, 'tgl_pendaftaran') . '").datepicker("show");});
			}',
));

$this->endWidget();
////======= end pendaftaran dialog =============
?>