<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
   margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<?php
$this->breadcrumbs=array(
	'Assep Ts'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('rujukan-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="white-container">
        <div id="animation"></div>
	<legend class="rim2">Surat Rujukan <b>(BPJS)</b></legend>
	<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

	<?php // echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
	<!--<div class="cari-lanjut search-form" style="display:none">-->
	<?php // $this->renderPartial('_search',array(
//		'model'=>$model,
//	)); ?>
	<!--</div> search-form -->
        
	<div class="block-tabel">
		<h6 class="rim2">Tabel Surat Rujukan (SEP)</h6>
	<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
		'id'=>'rujukan-t-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'template'=>"{summary}\n{items}\n{pager}",
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'columns'=>array(
                    array(
			'header'=>'No.',
			'value' => '($this->grid->dataProvider->pagination) ? 
					($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
					: ($row+1)',
			'type'=>'raw',
			'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                        'header'=>'Tanggal Dirujuk',
                        'type'=>'raw',
                        'value'=>'isset($data->tgl_dirujuk) ? ($data->tgl_dirujuk) : ""',
                    ),
                    array(
                        'header'=>'No. Rujukan',
                        'type'=>'raw',
                        'value'=>'$data->no_rujukan',
                    ),
                    array(
                        'header'=>'No. SEP',
                        'type'=>'raw',
                        'value'=>'$data->sep->no_sep',
                    ),
                    array(
                        'header'=>'No. Peserta',
                        'type'=>'raw',
                        'value'=>'$data->sep->nopeserta_bpjs',
                    ),
                    array(
                        'header'=>'No. Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->no_pendaftaran',
                    ),
                    array(
                        'header'=>'No. RM',
                        'type'=>'raw',
                        'value'=>'$data->pasien->no_rekam_medik',
                    ),
                    array(
                        'header'=>'Nama Pasien/Peserta',
                        'type'=>'raw',
                        'value'=>'$data->pasien->nama_pasien',
                    ),
                    array(
                        'header'=>'Jenis Pelayanan',
                        'type'=>'raw',
                        'value'=>'($data->jnspelayanan==2)? "Rawat Jalan" : "Rawat Inap"',
                    ),
                    array(
                        'header'=>'Lihat',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=icon-edit></i> ", "#",
                                array(
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk melihat Rujukan",
                                "onclick"=>"lihatRujukan($data->rujukankeluar_bpjs_id);return false;"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'Update Rujukan',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=icon-edit></i> ", Yii::app()->createUrl("/'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/update", array("id"=>$data->rujukankeluar_bpjs_id)),
                                array(
                                "target"=>"frameUbahRujukan",
                                "rel"=>"tooltip",
                                "title"=>"Klik untuk mengubah Rujukan",
                                "onclick"=>"$(\'#dialogUbahRujukan\').dialog(\'open\');"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>Yii::t('zii','Delete'),
                        'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{delete}',
                        'buttons'=>array(
                            'delete' => array (
                                    'label'=>"<i class='icon-trash'></i>",
                                    'options'=>array('title'=>'Klik untuk menghapus data Rujukan'),
                                    'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/hapusRujukan",array("id"=>$data->rujukankeluar_bpjs_id))',
                                    'click'=>'function(){hapusRujukan(this);return false;}',
                            ),
                        )
                    ),
		),
		'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
	)); ?>
</div>
        <fieldset class="box search-form">
            <legend class="rim"><i class="icon-white icon-search"></i> Pencarian</legend>
            <?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
                    'action'=>Yii::app()->createUrl($this->route),
                    'method'=>'get',
                    'id'=>'rujukan-t-search',
                    'type'=>'horizontal',
            )); ?>

                    <div class="row-fluid">
                            <div class="span6">
                                    <div class="control-group">
                                            <?php echo CHtml::label('Tanggal Rujukan','',array('class'=>'control-label')); ?>
                                            <div class="controls">
                                                     <?php   
                                                            $this->widget('MyDateTimePicker',array(
                                                                    'model'=>$model,
                                                                    'attribute'=>'tgl_awal',
                                                                    'mode'=>'date',
                                                                    'options'=> array(
            //								'dateFormat'=>Params::DATE_FORMAT,
                                                                            'dateFormat'=>'yy-mm-dd',
                                                                            'maxDate' => 'd',
                                                                    ),
                                                                    'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                                    ),
                                                    )); ?>
                                            </div>
                                    </div>
                                    <div class="control-group">
                                            <?php echo CHtml::label('Sampai Dengan','',array('class'=>'control-label')); ?>
                                            <div class="controls">
                                                     <?php   
                                                            $this->widget('MyDateTimePicker',array(
                                                                    'model'=>$model,
                                                                    'attribute'=>'tgl_akhir',
                                                                    'mode'=>'date',
                                                                    'options'=> array(
                //                                                            'dateFormat'=>Params::DATE_FORMAT,
                                                                            'dateFormat'=>'yy-mm-dd',
                                                                            'maxDate' => 'd',
                                                                    ),
                                                                    'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                                    ),
                                                    )); ?>
                                            </div>
                                    </div>
                            <div class="control-group">
                                    <?php echo CHtml::label('No. Rujukan','',array('class'=>'control-label')); ?>
                                <div class="controls">
                                    <?php echo $form->textField($model,'no_rujukan',array('class'=>'span3','maxlength'=>100)); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                    <?php echo CHtml::label('No. SEP','',array('class'=>'control-label')); ?>
                                <div class="controls">
                                    <?php echo $form->textField($model,'nosep',array('class'=>'span3','maxlength'=>100)); ?>
                                </div>
                            </div>
                                    
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <?php echo CHtml::label('No. Asuransi','',array('class'=>'control-label')); ?>
                                    <div class="controls">
                                        <?php echo $form->textField($model,'nokartuasuransi',array('class'=>'span3','maxlength'=>100)); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <?php echo CHtml::label('No. RM','',array('class'=>'control-label')); ?>
                                    <div class="controls">
                                        <?php echo $form->textField($model,'no_rekam_medik',array('class'=>'span3','maxlength'=>100)); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <?php echo CHtml::label('Nama Pasien','',array('class'=>'control-label')); ?>
                                    <div class="controls">
                                        <?php echo $form->textField($model,'nama_pasien',array('class'=>'span3','maxlength'=>100)); ?>
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="form-actions">
                            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
                            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
                    </div>

            <?php $this->endWidget(); ?>
        </fieldset>
<?php 
	echo CHtml::link(Yii::t('mds','{icon} Tambah Rujukan',array('{icon}'=>'<i class="icon-plus icon-white"></i>')),$this->createUrl('index',array('modul_id'=> Yii::app()->session['modul_id'])), array('class'=>'btn btn-success'))."&nbsp&nbsp"; 
	echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
	echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
	echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
	$urlPrint= $this->createUrl('print');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#rujukan-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);    
?></div>
<script type="text/javascript">
function hapusRujukan(obj){
    var answer = confirm('Yakin akan menghapus data Rujukan ini?');
    if(answer){ 
        $("#animation").addClass("loader");
        $.ajax({
            type:'GET',
            url:obj.href,
            data: {},//
            dataType: "json",
            success:function(data){
                    $("#animation").removeClass("loader");
                    $.fn.yiiGridView.update('rujukan-t-grid');
                    if(data.sukses > 0){
                            alert(data.status);
                    }else{
                            alert(data.status);
                    }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#animation").removeClass("loader");
                alert(data.status); console.log(errorThrown);
            }
        });
    }
    return false;
}	

function lihatRujukan(rujukankeluar_bpjs_id){
  window.open('<?php echo $this->createUrl('PrintRujukan'); ?>&id='+rujukankeluar_bpjs_id,'printwin','left=100,top=100,width=860,height=480');
}
</script>
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
        'id' => 'dialogSEP',
        'options' => array(
            'title' => 'Laporan SEP',
            'autoOpen' => false,
            'modal' => true,
            'width' => 900,
            'height' => 550,
            'resizable' => false,
        ),
    ));
    ?>
    <iframe name='frameSEP' width="100%" height="100%"></iframe>
    <?php $this->endWidget(); ?>

<?php 
// Dialog untuk ubah tanggal pulang =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
	'id' => 'dialogUbahRujukan',
	'options' => array(
		'title' => 'Ubah Data Rujukan',
		'autoOpen' => false,
		'modal' => true,
		'zIndex'=>1002,
		'width' => 900,
		'height' => 600,
		'resizable' => true,
                    'close'=>"js:function(){ $.fn.yiiGridView.update('rujukan-t-grid', {
                            data: $('#rujukan-t-search').serialize()
                    }); }",
	),
));
?>
<iframe name='frameUbahRujukan' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>