<?php $data=ProfilrumahsakitM::model()->findByPk(Params::DEFAULT_PROFIL_RUMAH_SAKIT); ?>
<style>
    .barcode-label{
        margin-top:-20px;
        z-index: 1;
        text-align: center;
        letter-spacing: 10px;
    }
    td, th{
        font-size: 11pt !important;
        padding: 4px;
    }
    body{
        width: 21.7cm;
    }
	.barcode{
        width:100px;
        border: 0px solid;
        margin:0px;
        padding:0px;
        /*top:8px;*/
        overflow: hidden;
        position: absolute;
        filter: gray;
		z-index: 2;
    }
	.sep_id{
		width:100px;
		margin-top:10px;
		z-index: 1;
		text-align:center;
	}
</style>
<?php //echo $this->renderPartial('pendaftaranPenjadwalan.views.pendaftaranRawatJalan._headerPrintStatus'); ?>

<table width="100%">
    <tr>
        <td rowspan='2'><img src="<?php echo Yii::app()->getBaseUrl('webroot').'/images/BPJS.jpg'; ?>" width="200"></td>
        <td colspan='5' align='center' style="font-weight:bold"><?php echo $judul_print; ?><br><?php // echo $data->nama_rumahsakit; ?></td>
        <td rowspan='2'></td>
    </tr>
    <tr>
        <td colspan='5' align='center' style="font-weight:bold"></td>
    </tr>
    <tr>
        <td width="18%">Kepada Yth</td>
        <td width="2%">:</td>
        <td width="30%"><b><?php echo $model->ppk_dirujuk_nama; ?></b></td>
        <td width="5%"></td>
        <td width="18%">No. Rujukan</td>
        <td width="2%">:</td>
        <td width="30%"><b><?php echo $model->no_rujukan; ?></b></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td><?php echo $model->poli_rujukan_nama; ?></td>
        <td></td>
        <td>Asal Rumah Sakit</td>
        <td>:</td>
        <td><?php echo $data->nama_rumahsakit?></td>
    </tr>
    <tr>
        <td colspan="7">Mohon pemeriksaan dan penanganan lebih lanjut penderita :<br></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>:</td>
        <td><?php echo $model->pasien->nama_pasien;?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>No. Kartu BPJS</td>
        <td>:</td>
        <td><?php echo $model->sep->nopeserta_bpjs; ?></td>
        <td></td>
        <td>Jenis Kelamin</td>
        <td>:</td>
        <td><?php echo $model->pasien->jeniskelamin; ?></td>
    </tr>
    <tr>
        <td>Diagnosa</td>
        <td>:</td>
        <td><?php echo $model->diagnosa_rujukan_kode." ".$model->diagnosa_rujukan_nama; ?></td>
        <td></td>
        <td>Jenis Rawat</td>
        <td>:</td>
        <td><?php echo ($model->jnspelayanan==2)? "Rawat Jalan" : "Rawat Inap"; ?></td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td>:</td>
        <td><?php echo $model->catatan_rujukan; ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td></td>
        <td colspan="3" style="text-align: center"><?php echo date('Y-m-d', strtotime($model->tgl_dirujuk));?><br>Mengetahui<br><br><br></td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td></td>
        <td colspan="3" style="text-align: center">___________________</td>
    </tr>
	
</table>
