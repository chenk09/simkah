<?php

/**
 * This is the model class for table "konfig_bpjs_inacbg_k".
 *
 * The followings are the available columns in table 'konfig_bpjs_inacbg_k':
 * @property integer $konfig_bpjs_inacbg_id
 * @property string $user_bpjs_name
 * @property string $secret_bpjs_key
 * @property string $bpjs_host_tester
 * @property string $bpjs_host_production
 * @property string $bpjs_service_name
 * @property string $bpjs_port
 * @property boolean $is_bridging
 * @property string $kode_ppk_bpjs
 * @property string $nama_ppk_pelayanan
 * @property boolean $konfig_bpjs_inacbg_aktif
 */
class ARKonfigBpjsInacbgK extends KonfigBpjsInacbgK
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KonfigBpjsInacbgK the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('konfig_bpjs_inacbg_id',$this->konfig_bpjs_inacbg_id);
		$criteria->compare('LOWER(user_bpjs_name)',strtolower($this->user_bpjs_name),true);
		$criteria->compare('LOWER(secret_bpjs_key)',strtolower($this->secret_bpjs_key),true);
		$criteria->compare('LOWER(bpjs_host_tester)',strtolower($this->bpjs_host_tester),true);
		$criteria->compare('LOWER(bpjs_host_production)',strtolower($this->bpjs_host_production),true);
		$criteria->compare('LOWER(bpjs_service_name)',strtolower($this->bpjs_service_name),true);
		$criteria->compare('LOWER(bpjs_port)',strtolower($this->bpjs_port),true);
		$criteria->compare('is_bridging',$this->is_bridging);
		$criteria->compare('LOWER(kode_ppk_bpjs)',strtolower($this->kode_ppk_bpjs),true);
		$criteria->compare('LOWER(nama_ppk_pelayanan)',strtolower($this->nama_ppk_pelayanan),true);
		$criteria->compare('konfig_bpjs_inacbg_aktif',$this->konfig_bpjs_inacbg_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('konfig_bpjs_inacbg_id',$this->konfig_bpjs_inacbg_id);
		$criteria->compare('LOWER(user_bpjs_name)',strtolower($this->user_bpjs_name),true);
		$criteria->compare('LOWER(secret_bpjs_key)',strtolower($this->secret_bpjs_key),true);
		$criteria->compare('LOWER(bpjs_host_tester)',strtolower($this->bpjs_host_tester),true);
		$criteria->compare('LOWER(bpjs_host_production)',strtolower($this->bpjs_host_production),true);
		$criteria->compare('LOWER(bpjs_service_name)',strtolower($this->bpjs_service_name),true);
		$criteria->compare('LOWER(bpjs_port)',strtolower($this->bpjs_port),true);
		$criteria->compare('is_bridging',$this->is_bridging);
		$criteria->compare('LOWER(kode_ppk_bpjs)',strtolower($this->kode_ppk_bpjs),true);
		$criteria->compare('LOWER(nama_ppk_pelayanan)',strtolower($this->nama_ppk_pelayanan),true);
		$criteria->compare('konfig_bpjs_inacbg_aktif',$this->konfig_bpjs_inacbg_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}