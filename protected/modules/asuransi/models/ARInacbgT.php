<?php
class ARInacbgT extends InacbgT
{
        public $totaltarif,$tarif_poli_eksekutif;
        public $is_finalisasi,$is_grouping,$is_terkirim;
        public $tgl_awal,$tgl_akhir,$nosep,$no_pendaftaran,$no_rekam_medik,$nama_pasien,$nokartuasuransi;
        public $jenis;
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InacbgT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchInformasi()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            $criteria->with = array('pendaftaran','sep','pasien');
            $criteria->addBetweenCondition('DATE(tglrawat_keluar)', $this->tgl_awal, $this->tgl_akhir);
//            $criteria->compare('LOWER(sep.no_sep)',strtolower($this->no_sep),true);
//            $criteria->compare('LOWER(pendaftaran.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
//            $criteria->compare('LOWER(sep.nokartuasuransi)',strtolower($this->nokartuasuransi),true);
//            $criteria->compare('LOWER(pasien.nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->limit=10;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
        public function searchInformasiKirimOnline()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            $criteria->with = array('pendaftaran','sep','pasien');
            if($this->jenis == 1){
                $criteria->addBetweenCondition('DATE(tglrawat_keluar)', $this->tgl_awal, $this->tgl_akhir);
            }else{
                $criteria->addCondition("DATE(create_tanggal) BETWEEN '".$this->tgl_awal."' AND '".$this->tgl_akhir."'");
            }
            if($this->jenisrawat_inacbg == 1 || $this->jenisrawat_inacbg == 2){
                $criteria->addCondition("jenisrawat_inacbg = ".$this->jenisrawat_inacbg);
            }
            $criteria->compare('LOWER(sep.no_sep)',strtolower($this->no_sep),true);
            $criteria->compare('LOWER(pendaftaran.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(sep.nokartuasuransi)',strtolower($this->nokartuasuransi),true);
            $criteria->compare('LOWER(pasien.nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->limit=10;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
        public function getCbg($inacbg_id=null){
            if(!empty($inacbg_id)){
                $modCbg = InasiscbgT::model()->findByAttributes(array('inacbg_id'=>$inacbg_id));
                return $modCbg->kodeprosedur;
            } else {
                return "";
            }
        }
        
        public function getCmg($inacbg_id=null){
            $cmg = "";
            if(!empty($inacbg_id)){
                $modCmg = InasiscmgT::model()->findByAttributes(array('inacbg_id'=>$inacbg_id));
                if(isset($modCmg->inasiscmg_id)){
                    $cmg .= !empty($modCmg->kode_spesialprosedure)? $modCmg->kode_spesialprosedure.", " : "";
                    $cmg .= !empty($modCmg->kode_spesialprosthesis)? $modCmg->kode_spesialprosthesis.", " : "";
                    $cmg .= !empty($modCmg->kode_spesialinvestigation)? $modCmg->kode_spesialinvestigation.", " : "";
                    $cmg .= !empty($modCmg->kode_spesialdrug)? $modCmg->kode_spesialdrug.", " : "";
                }
            }
            return $cmg;
        }
        
        public function getTArifKlaim($inacbg_id=null){
            $total = 0;
            if(!empty($inacbg_id)){
                $modCmg = InasiscmgT::model()->findByAttributes(array('inacbg_id'=>$inacbg_id));
                if(isset($modCmg->inasiscmg_id)){
                    $total += ($modCmg->plafon_spesialprosedure+$modCmg->plafon_spesialprosthesis+$modCmg->plafon_spesialinvestigation+$modCmg->plafon_spesialdrug);
                }
                $modCbg = InasiscbgT::model()->findByAttributes(array('inacbg_id'=>$inacbg_id));
                if(isset($modCbg->inasiscbg_id)){
                    $total += ($modCbg->plafonprosedur+$modCbg->plafonaccute+$modCbg->plafonchronic);
                }
            }
            return $total;
        }
        
        public function getTArifKlaimSemua(){
            $total = 0;
            if(!empty($inacbg_id)){
                $modCmg = InasiscmgT::model()->findByAttributes(array('inacbg_id'=>$inacbg_id));
                if(isset($modCmg->inasiscmg_id)){
                    $total += ($modCmg->plafon_spesialprosedure+$modCmg->plafon_spesialprosthesis+$modCmg->plafon_spesialinvestigation+$modCmg->plafon_spesialdrug);
                }
                $modCbg = InasiscbgT::model()->findByAttributes(array('inacbg_id'=>$inacbg_id));
                if(isset($modCbg->inasiscbg_id)){
                    $total += ($modCbg->plafonprosedur+$modCbg->plafonaccute+$modCbg->plafonchronic);
                }
            }
            return $total;
        }
        
        public function getNoSep(){
            if(!empty($this->sep_id)){
                $modSep = ARSepT::model()->findByPk($this->sep_id);
                return "'".$modSep->no_sep."'";
            } else {
                return "";
            }
        }


}