<?php

/**
 * This is the model class for table "pengajuanapprovalsep_t".
 *
 * The followings are the available columns in table 'pengajuanapprovalsep_t':
 * @property integer $pengajuanapprovalsep_id
 * @property integer $pendaftaran_id
 * @property string $no_kartu_bpjs
 * @property string $tgl_sep
 * @property string $kode_ppk_pelayanan
 * @property string $nama_ppk_pelayanan
 * @property string $jenis_pelayanan
 * @property string $kelas_tanggungan
 * @property string $asal_rujukan
 * @property string $no_rujukan
 * @property string $kode_ppk_rujukan
 * @property string $nama_ppk_rujukan
 * @property string $tgl_rujukan
 * @property string $diagnosa_awal
 * @property boolean $poli_eksekutif
 * @property boolean $cob
 * @property boolean $lakalantas
 * @property string $penjamin
 * @property string $no_telepon_pasien
 * @property string $userpembuat_bpjs
 * @property string $catatan
 * @property string $create_time
 * @property integer $create_loginpemakai_id
 * @property boolean $is_approval
 * @property integer $sep_id
 * @property string $user_approval_bpjs
 *
 * The followings are the available model relations:
 * @property PendaftaranT $pendaftaran
 */
class ARPengajuanapprovalsepT extends PengajuanapprovalsepT
{
        public $carabayar_id,$tgl_awal,$tgl_akhir,$no_pendaftaran,$no_rekam_medik,
                $nama_pasien,$no_sep,$tglsep,$namapeserta_bpjs;
                
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PengajuanapprovalsepT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('pendaftaran');
                $criteria->addCondition("DATE(tgl_sep) BETWEEN '".$this->tgl_awal."' AND '".$this->tgl_akhir."' OR DATE(t.create_time) BETWEEN '".$this->tgl_awal."' AND '".$this->tgl_akhir."'");
		$criteria->compare('pengajuanapprovalsep_id',$this->pengajuanapprovalsep_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_kartu_bpjs)',strtolower($this->no_kartu_bpjs),true);
		$criteria->compare('LOWER(pendaftaran.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(tgl_sep)',strtolower($this->tgl_sep),true);
		$criteria->compare('LOWER(kode_ppk_pelayanan)',strtolower($this->kode_ppk_pelayanan),true);
		$criteria->compare('LOWER(nama_ppk_pelayanan)',strtolower($this->nama_ppk_pelayanan),true);
		$criteria->compare('LOWER(jenis_pelayanan)',strtolower($this->jenis_pelayanan),true);
		$criteria->compare('LOWER(kelas_tanggungan)',strtolower($this->kelas_tanggungan),true);
		$criteria->compare('LOWER(asal_rujukan)',strtolower($this->asal_rujukan),true);
		$criteria->compare('LOWER(no_rujukan)',strtolower($this->no_rujukan),true);
		$criteria->compare('LOWER(kode_ppk_rujukan)',strtolower($this->kode_ppk_rujukan),true);
		$criteria->compare('LOWER(nama_ppk_rujukan)',strtolower($this->nama_ppk_rujukan),true);
		$criteria->compare('LOWER(tgl_rujukan)',strtolower($this->tgl_rujukan),true);
		$criteria->compare('LOWER(diagnosa_awal)',strtolower($this->diagnosa_awal),true);
		$criteria->compare('poli_eksekutif',$this->poli_eksekutif);
		$criteria->compare('cob',$this->cob);
		$criteria->compare('lakalantas',$this->lakalantas);
		$criteria->compare('LOWER(penjamin)',strtolower($this->penjamin),true);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(userpembuat_bpjs)',strtolower($this->userpembuat_bpjs),true);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
//		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('is_approval',$this->is_approval);
		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('LOWER(user_approval_bpjs)',strtolower($this->user_approval_bpjs),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pengajuanapprovalsep_id',$this->pengajuanapprovalsep_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_kartu_bpjs)',strtolower($this->no_kartu_bpjs),true);
		$criteria->compare('LOWER(tgl_sep)',strtolower($this->tgl_sep),true);
		$criteria->compare('LOWER(kode_ppk_pelayanan)',strtolower($this->kode_ppk_pelayanan),true);
		$criteria->compare('LOWER(nama_ppk_pelayanan)',strtolower($this->nama_ppk_pelayanan),true);
		$criteria->compare('LOWER(jenis_pelayanan)',strtolower($this->jenis_pelayanan),true);
		$criteria->compare('LOWER(kelas_tanggungan)',strtolower($this->kelas_tanggungan),true);
		$criteria->compare('LOWER(asal_rujukan)',strtolower($this->asal_rujukan),true);
		$criteria->compare('LOWER(no_rujukan)',strtolower($this->no_rujukan),true);
		$criteria->compare('LOWER(kode_ppk_rujukan)',strtolower($this->kode_ppk_rujukan),true);
		$criteria->compare('LOWER(nama_ppk_rujukan)',strtolower($this->nama_ppk_rujukan),true);
		$criteria->compare('LOWER(tgl_rujukan)',strtolower($this->tgl_rujukan),true);
		$criteria->compare('LOWER(diagnosa_awal)',strtolower($this->diagnosa_awal),true);
		$criteria->compare('poli_eksekutif',$this->poli_eksekutif);
		$criteria->compare('cob',$this->cob);
		$criteria->compare('lakalantas',$this->lakalantas);
		$criteria->compare('LOWER(penjamin)',strtolower($this->penjamin),true);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(userpembuat_bpjs)',strtolower($this->userpembuat_bpjs),true);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('is_approval',$this->is_approval);
		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('LOWER(user_approval_bpjs)',strtolower($this->user_approval_bpjs),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}