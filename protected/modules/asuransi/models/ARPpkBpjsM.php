<?php

/**
 * This is the model class for table "ppk_bpjs_m".
 *
 * The followings are the available columns in table 'ppk_bpjs_m':
 * @property integer $ppk_bpjs_id
 * @property string $ppk_bpjs_kode
 * @property string $ppk_bpjs_nama
 * @property boolean $ppk_bpjs_aktif
 */
class ARPpkBpjsM extends PpkBpjsM
{
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ppk_bpjs_id',$this->ppk_bpjs_id);
		$criteria->compare('LOWER(ppk_bpjs_kode)',strtolower($this->ppk_bpjs_kode),true);
		$criteria->compare('LOWER(ppk_bpjs_nama)',strtolower($this->ppk_bpjs_nama),true);
		$criteria->compare('ppk_bpjs_aktif',$this->ppk_bpjs_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('ppk_bpjs_id',$this->ppk_bpjs_id);
		$criteria->compare('LOWER(ppk_bpjs_kode)',strtolower($this->ppk_bpjs_kode),true);
		$criteria->compare('LOWER(ppk_bpjs_nama)',strtolower($this->ppk_bpjs_nama),true);
		$criteria->compare('ppk_bpjs_aktif',$this->ppk_bpjs_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}