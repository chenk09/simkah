<?php

/**
 * This is the model class for table "sep_t".
 *
 * The followings are the available columns in table 'sep_t':
 * @property integer $sep_id
 * @property string $tglsep
 * @property string $no_sep
 * @property integer $pendaftaran_id
 * @property integer $pasien_id
 * @property string $nopeserta_bpjs
 * @property string $namapeserta_bpjs
 * @property integer $hakkelas_kode
 * @property string $hakkelas_nama
 * @property string $notelpon_peserta
 * @property string $norujukan_bpjs
 * @property string $tglrujukan_bpjs
 * @property integer $jenisrujukan_kode_bpjs
 * @property string $jenisrujukan_nama_bpjs
 * @property integer $ppkrujukanasal_kode
 * @property string $ppkrujukanasal_nama
 * @property integer $jnspelayanan_kode
 * @property string $jnspelayanan_nama
 * @property string $diagnosaawal_kode
 * @property string $diagnosaawal_nama
 * @property integer $politujuan_kode
 * @property string $politujuan_nama
 * @property integer $kelasrawat_kode
 * @property string $kelasrawat_nama
 * @property string $tanggalpulang_sep
 * @property string $cob_bpjs
 * @property integer $polieksekutif
 * @property integer $lakalantas_kode
 * @property string $lakalantas_nama
 * @property string $penjaminlakalantas
 * @property string $lokasilakalantas
 * @property string $catatan_sep
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $upate_loginpemakai_id
 * @property integer $create_ruangan
 */
class ARSepT extends SepT
{
        public $tgl_awal,$tgl_akhir,$no_pendaftaran,$no_rekam_medik,$nama_pasien;
        public $ppkpelayanan,$ppkpelayanan_nama,$carabayar_id,$jenisrujukan,$lakalantas,$pembuat_sep;
        public $tgl_pendaftaran,$tanggal_lahir,$jeniskelamin,$alamat_pasien;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SepT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sep_id',$this->sep_id);
//		$criteria->compare('LOWER(tglsep)',strtolower($this->tglsep),true);
                $criteria->addBetweenCondition('DATE(tglsep)', $this->tgl_awal, $this->tgl_akhir);
		$criteria->compare('LOWER(no_sep)',strtolower($this->no_sep),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(nopeserta_bpjs)',strtolower($this->nopeserta_bpjs),true);
		$criteria->compare('LOWER(namapeserta_bpjs)',strtolower($this->namapeserta_bpjs),true);
		$criteria->compare('hakkelas_kode',$this->hakkelas_kode);
		$criteria->compare('LOWER(hakkelas_nama)',strtolower($this->hakkelas_nama),true);
		$criteria->compare('LOWER(notelpon_peserta)',strtolower($this->notelpon_peserta),true);
		$criteria->compare('LOWER(norujukan_bpjs)',strtolower($this->norujukan_bpjs),true);
		$criteria->compare('LOWER(tglrujukan_bpjs)',strtolower($this->tglrujukan_bpjs),true);
		$criteria->compare('jenisrujukan_kode_bpjs',$this->jenisrujukan_kode_bpjs);
		$criteria->compare('LOWER(jenisrujukan_nama_bpjs)',strtolower($this->jenisrujukan_nama_bpjs),true);
		$criteria->compare('ppkrujukanasal_kode',$this->ppkrujukanasal_kode);
		$criteria->compare('LOWER(ppkrujukanasal_nama)',strtolower($this->ppkrujukanasal_nama),true);
		$criteria->compare('jnspelayanan_kode',$this->jnspelayanan_kode);
		$criteria->compare('LOWER(jnspelayanan_nama)',strtolower($this->jnspelayanan_nama),true);
		$criteria->compare('LOWER(diagnosaawal_kode)',strtolower($this->diagnosaawal_kode),true);
		$criteria->compare('LOWER(diagnosaawal_nama)',strtolower($this->diagnosaawal_nama),true);
		$criteria->compare('politujuan_kode',$this->politujuan_kode);
		$criteria->compare('LOWER(politujuan_nama)',strtolower($this->politujuan_nama),true);
		$criteria->compare('kelasrawat_kode',$this->kelasrawat_kode);
		$criteria->compare('LOWER(kelasrawat_nama)',strtolower($this->kelasrawat_nama),true);
		$criteria->compare('LOWER(tanggalpulang_sep)',strtolower($this->tanggalpulang_sep),true);
		$criteria->compare('LOWER(cob_bpjs)',strtolower($this->cob_bpjs),true);
		$criteria->compare('polieksekutif',$this->polieksekutif);
		$criteria->compare('lakalantas_kode',$this->lakalantas_kode);
		$criteria->compare('LOWER(lakalantas_nama)',strtolower($this->lakalantas_nama),true);
		$criteria->compare('LOWER(penjaminlakalantas)',strtolower($this->penjaminlakalantas),true);
		$criteria->compare('LOWER(lokasilakalantas)',strtolower($this->lokasilakalantas),true);
		$criteria->compare('LOWER(catatan_sep)',strtolower($this->catatan_sep),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('upate_loginpemakai_id',$this->upate_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchDialog()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sep_id',$this->sep_id);
                $criteria->addBetweenCondition('DATE(tglsep)', $this->tglsep, $this->tglsep);
		$criteria->compare('LOWER(no_sep)',strtolower($this->no_sep),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(nopeserta_bpjs)',strtolower($this->nopeserta_bpjs),true);
		$criteria->compare('LOWER(namapeserta_bpjs)',strtolower($this->namapeserta_bpjs),true);
		$criteria->compare('hakkelas_kode',$this->hakkelas_kode);
		$criteria->compare('LOWER(hakkelas_nama)',strtolower($this->hakkelas_nama),true);
		$criteria->compare('LOWER(notelpon_peserta)',strtolower($this->notelpon_peserta),true);
		$criteria->compare('LOWER(norujukan_bpjs)',strtolower($this->norujukan_bpjs),true);
		$criteria->compare('LOWER(tglrujukan_bpjs)',strtolower($this->tglrujukan_bpjs),true);
		$criteria->compare('jenisrujukan_kode_bpjs',$this->jenisrujukan_kode_bpjs);
		$criteria->compare('LOWER(jenisrujukan_nama_bpjs)',strtolower($this->jenisrujukan_nama_bpjs),true);
		$criteria->compare('ppkrujukanasal_kode',$this->ppkrujukanasal_kode);
		$criteria->compare('LOWER(ppkrujukanasal_nama)',strtolower($this->ppkrujukanasal_nama),true);
		$criteria->compare('jnspelayanan_kode',$this->jnspelayanan_kode);
		$criteria->compare('LOWER(jnspelayanan_nama)',strtolower($this->jnspelayanan_nama),true);
		$criteria->compare('LOWER(diagnosaawal_kode)',strtolower($this->diagnosaawal_kode),true);
		$criteria->compare('LOWER(diagnosaawal_nama)',strtolower($this->diagnosaawal_nama),true);
		$criteria->compare('politujuan_kode',$this->politujuan_kode);
		$criteria->compare('LOWER(politujuan_nama)',strtolower($this->politujuan_nama),true);
		$criteria->compare('kelasrawat_kode',$this->kelasrawat_kode);
		$criteria->compare('LOWER(kelasrawat_nama)',strtolower($this->kelasrawat_nama),true);
		$criteria->compare('LOWER(tanggalpulang_sep)',strtolower($this->tanggalpulang_sep),true);
		$criteria->compare('LOWER(cob_bpjs)',strtolower($this->cob_bpjs),true);
		$criteria->compare('polieksekutif',$this->polieksekutif);
		$criteria->compare('lakalantas_kode',$this->lakalantas_kode);
		$criteria->compare('LOWER(lakalantas_nama)',strtolower($this->lakalantas_nama),true);
		$criteria->compare('LOWER(penjaminlakalantas)',strtolower($this->penjaminlakalantas),true);
		$criteria->compare('LOWER(lokasilakalantas)',strtolower($this->lokasilakalantas),true);
		$criteria->compare('LOWER(catatan_sep)',strtolower($this->catatan_sep),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('upate_loginpemakai_id',$this->upate_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('sep_id',$this->sep_id);
//		$criteria->compare('LOWER(tglsep)',strtolower($this->tglsep),true);
                $criteria->addBetweenCondition('DATE(tglsep)', $this->tgl_awal, $this->tgl_akhir);
		$criteria->compare('LOWER(no_sep)',strtolower($this->no_sep),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(nopeserta_bpjs)',strtolower($this->nopeserta_bpjs),true);
		$criteria->compare('LOWER(namapeserta_bpjs)',strtolower($this->namapeserta_bpjs),true);
		$criteria->compare('hakkelas_kode',$this->hakkelas_kode);
		$criteria->compare('LOWER(hakkelas_nama)',strtolower($this->hakkelas_nama),true);
		$criteria->compare('LOWER(notelpon_peserta)',strtolower($this->notelpon_peserta),true);
		$criteria->compare('LOWER(norujukan_bpjs)',strtolower($this->norujukan_bpjs),true);
		$criteria->compare('LOWER(tglrujukan_bpjs)',strtolower($this->tglrujukan_bpjs),true);
		$criteria->compare('jenisrujukan_kode_bpjs',$this->jenisrujukan_kode_bpjs);
		$criteria->compare('LOWER(jenisrujukan_nama_bpjs)',strtolower($this->jenisrujukan_nama_bpjs),true);
		$criteria->compare('ppkrujukanasal_kode',$this->ppkrujukanasal_kode);
		$criteria->compare('LOWER(ppkrujukanasal_nama)',strtolower($this->ppkrujukanasal_nama),true);
		$criteria->compare('jnspelayanan_kode',$this->jnspelayanan_kode);
		$criteria->compare('LOWER(jnspelayanan_nama)',strtolower($this->jnspelayanan_nama),true);
		$criteria->compare('LOWER(diagnosaawal_kode)',strtolower($this->diagnosaawal_kode),true);
		$criteria->compare('LOWER(diagnosaawal_nama)',strtolower($this->diagnosaawal_nama),true);
		$criteria->compare('politujuan_kode',$this->politujuan_kode);
		$criteria->compare('LOWER(politujuan_nama)',strtolower($this->politujuan_nama),true);
		$criteria->compare('kelasrawat_kode',$this->kelasrawat_kode);
		$criteria->compare('LOWER(kelasrawat_nama)',strtolower($this->kelasrawat_nama),true);
		$criteria->compare('LOWER(tanggalpulang_sep)',strtolower($this->tanggalpulang_sep),true);
		$criteria->compare('LOWER(cob_bpjs)',strtolower($this->cob_bpjs),true);
		$criteria->compare('polieksekutif',$this->polieksekutif);
		$criteria->compare('lakalantas_kode',$this->lakalantas_kode);
		$criteria->compare('LOWER(lakalantas_nama)',strtolower($this->lakalantas_nama),true);
		$criteria->compare('LOWER(penjaminlakalantas)',strtolower($this->penjaminlakalantas),true);
		$criteria->compare('LOWER(lokasilakalantas)',strtolower($this->lokasilakalantas),true);
		$criteria->compare('LOWER(catatan_sep)',strtolower($this->catatan_sep),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('upate_loginpemakai_id',$this->upate_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}