<?php

/**
 * This is the model class for table "infosepinacbg_v".
 *
 * The followings are the available columns in table 'infosepinacbg_v':
 * @property string $no_sep
 * @property string $nopeserta_bpjs
 * @property integer $pendaftaran_id
 * @property string $no_pendaftaran
 * @property integer $pasien_id
 * @property string $no_rekam_medik
 * @property string $nama_pasien
 * @property integer $jnspelayanan
 * @property integer $hakkelas_kode
 * @property string $tgl_pendaftaran
 * @property string $tglpulang
 * @property integer $los
 * @property string $nama_pegawai
 * @property string $carakeluar
 * @property string $tgl_lahir
 * @property double $prosedurenonbedah
 * @property double $prosedurebedah
 * @property double $konsultasi
 * @property double $tenagaahli
 * @property double $keperawatan
 * @property double $penunjang
 * @property double $radiologi
 * @property double $laboratorium
 * @property double $pelayanandarah
 * @property double $rehabilitasi
 * @property double $kamar_akomodasi
 * @property double $rawatintensif
 * @property double $obat
 * @property double $alkes
 * @property double $bmhp
 * @property double $sewaalat
 * @property double $total
 */
class ARInfosepinacbgV extends InfosepinacbgV
{
        public $pendaftaran_id;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InfosepinacbgV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(no_sep)',strtolower($this->no_sep),true);
		$criteria->compare('LOWER(nopeserta_bpjs)',strtolower($this->nopeserta_bpjs),true);
		if(!empty($this->pendaftaran_id)){
			$criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
		}
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		if(!empty($this->pasien_id)){
			$criteria->addCondition('pasien_id = '.$this->pasien_id);
		}
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		if(!empty($this->jnspelayanan)){
			$criteria->addCondition('jnspelayanan = '.$this->jnspelayanan);
		}
		if(!empty($this->hakkelas_kode)){
			$criteria->addCondition('hakkelas_kode = '.$this->hakkelas_kode);
		}
//		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('LOWER(tglpulang)',strtolower($this->tglpulang),true);
		if(!empty($this->los)){
			$criteria->addCondition('los = '.$this->los);
		}
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(carakeluar)',strtolower($this->carakeluar),true);
		$criteria->compare('LOWER(tgl_lahir)',strtolower($this->tgl_lahir),true);
		$criteria->compare('prosedurenonbedah',$this->prosedurenonbedah);
		$criteria->compare('prosedurebedah',$this->prosedurebedah);
		$criteria->compare('konsultasi',$this->konsultasi);
		$criteria->compare('tenagaahli',$this->tenagaahli);
		$criteria->compare('keperawatan',$this->keperawatan);
		$criteria->compare('penunjang',$this->penunjang);
		$criteria->compare('radiologi',$this->radiologi);
		$criteria->compare('laboratorium',$this->laboratorium);
		$criteria->compare('pelayanandarah',$this->pelayanandarah);
		$criteria->compare('rehabilitasi',$this->rehabilitasi);
		$criteria->compare('kamar_akomodasi',$this->kamar_akomodasi);
		$criteria->compare('rawatintensif',$this->rawatintensif);
		$criteria->compare('obat',$this->obat);
		$criteria->compare('alkes',$this->alkes);
		$criteria->compare('bmhp',$this->bmhp);
		$criteria->compare('sewaalat',$this->sewaalat);
		$criteria->compare('total',$this->total);

		return $criteria;
	}
        
        
        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function searchDialog()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $this->tgl_pendaftaran = $this->tgl_pendaftaran;
            $criteria->addBetweenCondition('DATE(tgl_pendaftaran)', $this->tgl_pendaftaran, $this->tgl_pendaftaran);
            $criteria->limit=10;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=10;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }


        public function searchPrint()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
}