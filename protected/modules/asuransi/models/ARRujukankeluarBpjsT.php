<?php

/**
 * This is the model class for table "rujukankeluar_bpjs_t".
 *
 * The followings are the available columns in table 'rujukankeluar_bpjs_t':
 * @property integer $rujukankeluar_bpjs_id
 * @property integer $sep_id
 * @property integer $pendaftaran_id
 * @property integer $pasien_id
 * @property string $tgl_dirujuk
 * @property string $ppk_dirujuk
 * @property integer $jnspelayanan
 * @property string $diagnosa_rujukan_kode
 * @property string $diagnosa_rujukan_nama
 * @property integer $tipe_rujukan
 * @property string $poli_rujukan
 * @property string $catatan_rujukan
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_pemakai_id
 * @property integer $update_loginpemakai_id
 * @property string $create_user_bpjs
 * @property string $update_user_bpjs
 */
class ARRujukankeluarBpjsT extends RujukankeluarBpjsT
{
        public $tgl_awal,$tgl_akhir,$nokartuasuransi,$no_rekam_medik,$nama_pasien,$nosep,$no_pendaftaran;
                
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('pendaftaran','pasien','sep');
                $criteria->addBetweenCondition('DATE(tgl_dirujuk)', $this->tgl_awal, $this->tgl_akhir);
                $criteria->compare('LOWER(sep.nopeserta_bpjs)',strtolower($this->nokartuasuransi),true);
                $criteria->compare('LOWER(sep.no_sep)',strtolower($this->nosep),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('rujukankeluar_bpjs_id',$this->rujukankeluar_bpjs_id);
		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
//		$criteria->compare('LOWER(tgl_dirujuk)',strtolower($this->tgl_dirujuk),true);
		$criteria->compare('LOWER(ppk_dirujuk)',strtolower($this->ppk_dirujuk),true);
		$criteria->compare('LOWER(no_rujukan)',strtolower($this->no_rujukan),true);
		$criteria->compare('jnspelayanan',$this->jnspelayanan);
		$criteria->compare('LOWER(diagnosa_rujukan_kode)',strtolower($this->diagnosa_rujukan_kode),true);
		$criteria->compare('LOWER(diagnosa_rujukan_nama)',strtolower($this->diagnosa_rujukan_nama),true);
		$criteria->compare('tipe_rujukan',$this->tipe_rujukan);
		$criteria->compare('LOWER(poli_rujukan)',strtolower($this->poli_rujukan),true);
		$criteria->compare('LOWER(catatan_rujukan)',strtolower($this->catatan_rujukan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_pemakai_id',$this->create_pemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('LOWER(create_user_bpjs)',strtolower($this->create_user_bpjs),true);
		$criteria->compare('LOWER(update_user_bpjs)',strtolower($this->update_user_bpjs),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
                $criteria->with = array('pendaftaran','pasien','sep');
                $criteria->addBetweenCondition('DATE(tgl_dirujuk)', $this->tgl_awal, $this->tgl_akhir);
                $criteria->compare('LOWER(sep.nopeserta_bpjs)',strtolower($this->nokartuasuransi),true);
                $criteria->compare('LOWER(sep.nosep)',strtolower($this->nosep),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('rujukankeluar_bpjs_id',$this->rujukankeluar_bpjs_id);
		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
//		$criteria->compare('LOWER(tgl_dirujuk)',strtolower($this->tgl_dirujuk),true);
		$criteria->compare('LOWER(ppk_dirujuk)',strtolower($this->ppk_dirujuk),true);
                $criteria->compare('LOWER(no_rujukan)',strtolower($this->no_rujukan),true);
		$criteria->compare('jnspelayanan',$this->jnspelayanan);
		$criteria->compare('LOWER(diagnosa_rujukan_kode)',strtolower($this->diagnosa_rujukan_kode),true);
		$criteria->compare('LOWER(diagnosa_rujukan_nama)',strtolower($this->diagnosa_rujukan_nama),true);
		$criteria->compare('tipe_rujukan',$this->tipe_rujukan);
		$criteria->compare('LOWER(poli_rujukan)',strtolower($this->poli_rujukan),true);
		$criteria->compare('LOWER(catatan_rujukan)',strtolower($this->catatan_rujukan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_pemakai_id',$this->create_pemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('LOWER(create_user_bpjs)',strtolower($this->create_user_bpjs),true);
		$criteria->compare('LOWER(update_user_bpjs)',strtolower($this->update_user_bpjs),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}