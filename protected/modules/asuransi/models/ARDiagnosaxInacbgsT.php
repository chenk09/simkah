<?php

/**
 * This is the model class for table "diagnosax_inacbgs_t".
 *
 * The followings are the available columns in table 'diagnosax_inacbgs_t':
 * @property integer $diagnosax_inacbg_id
 * @property integer $inacbg_id
 * @property integer $sep_id
 * @property integer $pendaftaran_id
 * @property string $diagnosax_kode
 * @property string $diagnosax_nama
 * @property string $diagnosax_type
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $update_loginpemakai_id
 * @property integer $create_ruangan_id
 * @property integer $update_ruangan_id
 *
 * The followings are the available model relations:
 * @property InacbgT $inacbg
 * @property PendaftaranT $pendaftaran
 * @property SepT $sep
 */
class ARDiagnosaxInacbgsT extends DiagnosaxInacbgsT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DiagnosaxInacbgsT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		if(!empty($this->diagnosax_inacbg_id)){
			$criteria->addCondition('diagnosax_inacbg_id = '.$this->diagnosax_inacbg_id);
		}
		if(!empty($this->inacbg_id)){
			$criteria->addCondition('inacbg_id = '.$this->inacbg_id);
		}
		if(!empty($this->sep_id)){
			$criteria->addCondition('sep_id = '.$this->sep_id);
		}
		if(!empty($this->pendaftaran_id)){
			$criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
		}
		$criteria->compare('LOWER(diagnosax_kode)',strtolower($this->diagnosax_kode),true);
		$criteria->compare('LOWER(diagnosax_nama)',strtolower($this->diagnosax_nama),true);
		$criteria->compare('LOWER(diagnosax_type)',strtolower($this->diagnosax_type),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		if(!empty($this->create_loginpemakai_id)){
			$criteria->addCondition('create_loginpemakai_id = '.$this->create_loginpemakai_id);
		}
		if(!empty($this->update_loginpemakai_id)){
			$criteria->addCondition('update_loginpemakai_id = '.$this->update_loginpemakai_id);
		}
		if(!empty($this->create_ruangan_id)){
			$criteria->addCondition('create_ruangan_id = '.$this->create_ruangan_id);
		}
		if(!empty($this->update_ruangan_id)){
			$criteria->addCondition('update_ruangan_id = '.$this->update_ruangan_id);
		}

		return $criteria;
	}
        
        
        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=10;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }


        public function searchPrint()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
}