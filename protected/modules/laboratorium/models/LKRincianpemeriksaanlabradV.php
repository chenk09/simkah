 <?php

/**
 * This is the model class for table "rincianpemeriksaanlabrad_v".
 *
 * The followings are the available columns in table 'rincianpemeriksaanlabrad_v':
 * @property integer $profilrs_id
 * @property integer $pasien_id
 * @property string $no_rekam_medik
 * @property string $tgl_rekam_medik
 * @property string $jenisidentitas
 * @property string $no_identitas_pasien
 * @property string $namadepan
 * @property string $nama_pasien
 * @property string $nama_bin
 * @property string $jeniskelamin
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $alamat_pasien
 * @property integer $rt
 * @property integer $rw
 * @property string $statusperkawinan
 * @property string $agama
 * @property string $golongandarah
 * @property string $rhesus
 * @property integer $anakke
 * @property integer $jumlah_bersaudara
 * @property string $no_telepon_pasien
 * @property string $no_mobile_pasien
 * @property string $warga_negara
 * @property string $photopasien
 * @property string $alamatemail
 * @property integer $pendaftaran_id
 * @property string $no_pendaftaran
 * @property string $tgl_pendaftaran
 * @property string $umur
 * @property string $no_asuransi
 * @property string $namapemilik_asuransi
 * @property string $nopokokperusahaan
 * @property string $namaperusahaan
 * @property string $tglselesaiperiksa
 * @property integer $tindakanpelayanan_id
 * @property integer $penjamin_id
 * @property string $penjamin_nama
 * @property integer $carabayar_id
 * @property string $carabayar_nama
 * @property integer $kelaspelayanan_id
 * @property string $kelaspelayanan_nama
 * @property integer $instalasi_id
 * @property string $instalasi_nama
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 * @property string $tgl_tindakan
 * @property integer $daftartindakan_id
 * @property string $daftartindakan_kode
 * @property string $daftartindakan_nama
 * @property integer $tipepaket_id
 * @property string $tipepaket_nama
 * @property double $tarif_rsakomodasi
 * @property double $tarif_medis
 * @property double $tarif_paramedis
 * @property double $tarif_bhp
 * @property double $tarif_satuan
 * @property double $tarif_tindakan
 * @property string $satuantindakan
 * @property integer $qty_tindakan
 * @property boolean $cyto_tindakan
 * @property double $tarifcyto_tindakan
 * @property double $discount_tindakan
 * @property double $pembebasan_tindakan
 * @property double $subsidiasuransi_tindakan
 * @property double $subsidipemerintah_tindakan
 * @property double $subsisidirumahsakit_tindakan
 * @property double $iurbiaya_tindakan
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 * @property integer $jeniskasuspenyakit_id
 * @property string $jeniskasuspenyakit_nama
 * @property integer $pembayaranpelayanan_id
 * @property integer $pegawai_id
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property integer $gelarbelakang_id
 * @property string $gelarbelakang_nama
 * @property integer $ruanganpendaftaran_id
 * @property integer $tindakansudahbayar_id
 * @property string $jenispemeriksaanlab_nama
 * @property integer $pemeriksaanlab_id
 * @property string $pemeriksaanlab_kode
 * @property integer $pemeriksaanlab_urutan
 * @property string $pemeriksaanlab_nama
 * @property string $no_masukpenunjang
 * @property string $tglmasukpenunjang
 * @property string $kunjungan
 * @property string $statusperiksa
 */
class LKRincianpemeriksaanlabradV extends RincianpemeriksaanlabradV
{
    public $tglAwal, $tglAkhir, $totaltagihan, $statusBayar,$pasienadmisi_id;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return RincianpemeriksaanlabradV the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchRincian()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('profilrs_id',$this->profilrs_id);
        $criteria->compare('pasien_id',$this->pasien_id);
        $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
        $criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
        $criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
        $criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
        $criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
        $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
        $criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
        $criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
        $criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
        $criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
        $criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
        $criteria->compare('rt',$this->rt);
        $criteria->compare('rw',$this->rw);
        $criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
        $criteria->compare('LOWER(agama)',strtolower($this->agama),true);
        $criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
        $criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
        $criteria->compare('anakke',$this->anakke);
        $criteria->compare('jumlah_bersaudara',$this->jumlah_bersaudara);
        $criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
        $criteria->compare('LOWER(no_mobile_pasien)',strtolower($this->no_mobile_pasien),true);
        $criteria->compare('LOWER(warga_negara)',strtolower($this->warga_negara),true);
        $criteria->compare('LOWER(photopasien)',strtolower($this->photopasien),true);
        $criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
        $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
        $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
        $criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
        $criteria->compare('LOWER(umur)',strtolower($this->umur),true);
        $criteria->compare('LOWER(no_asuransi)',strtolower($this->no_asuransi),true);
        $criteria->compare('LOWER(namapemilik_asuransi)',strtolower($this->namapemilik_asuransi),true);
        $criteria->compare('LOWER(nopokokperusahaan)',strtolower($this->nopokokperusahaan),true);
        $criteria->compare('LOWER(namaperusahaan)',strtolower($this->namaperusahaan),true);
        $criteria->compare('LOWER(tglselesaiperiksa)',strtolower($this->tglselesaiperiksa),true);
        $criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
        $criteria->compare('penjamin_id',$this->penjamin_id);
        $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
        $criteria->compare('carabayar_id',$this->carabayar_id);
        $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
        $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
        $criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
        $criteria->compare('instalasi_id',$this->instalasi_id);
        $criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
        $criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
        $criteria->compare('daftartindakan_id',$this->daftartindakan_id);
        $criteria->compare('LOWER(daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
        $criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
        $criteria->compare('tipepaket_id',$this->tipepaket_id);
        $criteria->compare('LOWER(tipepaket_nama)',strtolower($this->tipepaket_nama),true);
        $criteria->compare('tarif_rsakomodasi',$this->tarif_rsakomodasi);
        $criteria->compare('tarif_medis',$this->tarif_medis);
        $criteria->compare('tarif_paramedis',$this->tarif_paramedis);
        $criteria->compare('tarif_bhp',$this->tarif_bhp);
        $criteria->compare('tarif_satuan',$this->tarif_satuan);
        $criteria->compare('tarif_tindakan',$this->tarif_tindakan);
        $criteria->compare('LOWER(satuantindakan)',strtolower($this->satuantindakan),true);
        $criteria->compare('qty_tindakan',$this->qty_tindakan);
        $criteria->compare('cyto_tindakan',$this->cyto_tindakan);
        $criteria->compare('tarifcyto_tindakan',$this->tarifcyto_tindakan);
        $criteria->compare('discount_tindakan',$this->discount_tindakan);
        $criteria->compare('pembebasan_tindakan',$this->pembebasan_tindakan);
        $criteria->compare('subsidiasuransi_tindakan',$this->subsidiasuransi_tindakan);
        $criteria->compare('subsidipemerintah_tindakan',$this->subsidipemerintah_tindakan);
        $criteria->compare('subsisidirumahsakit_tindakan',$this->subsisidirumahsakit_tindakan);
        $criteria->compare('iurbiaya_tindakan',$this->iurbiaya_tindakan);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
        $criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
        $criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
        $criteria->compare('pembayaranpelayanan_id',$this->pembayaranpelayanan_id);
        $criteria->compare('pegawai_id',$this->pegawai_id);
        $criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
        $criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
        $criteria->compare('gelarbelakang_id',$this->gelarbelakang_id);
        $criteria->compare('LOWER(gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
        $criteria->compare('ruanganpendaftaran_id',$this->ruanganpendaftaran_id);
        $criteria->compare('tindakansudahbayar_id',$this->tindakansudahbayar_id);
        $criteria->compare('LOWER(jenispemeriksaanlab_nama)',strtolower($this->jenispemeriksaanlab_nama),true);
        $criteria->compare('pemeriksaanlab_id',$this->pemeriksaanlab_id);
        $criteria->compare('LOWER(pemeriksaanlab_kode)',strtolower($this->pemeriksaanlab_kode),true);
        $criteria->compare('pemeriksaanlab_urutan',$this->pemeriksaanlab_urutan);
        $criteria->compare('LOWER(pemeriksaanlab_nama)',strtolower($this->pemeriksaanlab_nama),true);
        $criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
        $criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
        $criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
        $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    
    public function getSubTotal()
    {
        return ($this->tarif_satuan*$this->qty_tindakan)+$this->tarifcyto_tindakan+$this->discount_tindakan;
    }    
    
    public function getNamaPerujuk()
    {
        
        $pendaftaran = LKPendaftaranMp::model()->findByPk($this->pendaftaran_id);
        $data = LKRujukanT::model()->findByPk($pendaftaran->rujukan_id);
        return $data->nama_perujuk;
    } 
    
    public function getAlamatPerujuk()
    {
        
        $pendaftaran = LKPendaftaranMp::model()->findByPk($this->pendaftaran_id);
        $data = LKRujukanT::model()->findByPk($pendaftaran->rujukan_id);
        $modelRujukanDari = RujukandariM::model()->findByPk($data->rujukandari_id);
        return $modelRujukanDari->alamatlengkap . ' ' . $modelRujukanDari->notelp;
    }
    
    public function getNamaDokter()
    {
        return ($this->gelardepan." ".$this->nama_pegawai.", ".$this->gelarbelakang_nama);
    }
} 
?>
