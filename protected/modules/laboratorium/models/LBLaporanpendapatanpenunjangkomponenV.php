<?php
class LBLaporanpendapatanpenunjangkomponenV extends LaporanpendapatanpenunjangkomponenV
{
        public $tarif;
        public $tick,$data,$jumlah;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
         public function getNamaModel() {
            return __CLASS__;
        }

        public function searchPendapatanR()
        {
                $criteria = new CDbCriteria;

                $criteria = $this->functionCriteria();
                $criteria->limit = 10;

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        
        public function searchPrintPendapatanR()
        {
            $criteria = new CDbCriteria;

            $criteria = $this->functionCriteria();

            return new CActiveDataProvider($this, array(
                'pagination' => false,
                'criteria' => $criteria,
            ));
        }
        
        public function searchGrafik()
        {
            $criteria = new CDbCriteria;

            $criteria->select='count(pendaftaran_id) as jumlah, kelaspelayanan_nama as data';
            $criteria = $this->functionCriteria();

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }
        
        protected function functionCriteria() {
            $criteria=new CDbCriteria;

            $criteria->group = "pendaftaran_id,carabayar_id, penjamin_id, dokterpemeriksa1_id, no_rekam_medik,nama_pasien, no_pendaftaran,nama_pegawai,kelaspelayanan_nama,carabayar_nama,penjamin_nama";
            $criteria->select = $criteria->group;
            $criteria->order = $criteria->group;
            
            $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('profilrs_id',$this->profilrs_id);
            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
            $criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
            $criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
            $criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
            $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
            $criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
            $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(umur)',strtolower($this->umur),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            if (is_array($this->kelaspelayanan_id)){
                $criteria->compare('kelaspelayanan_id', $this->kelaspelayanan_id);
            }
            else{
                $criteria->addCondition('kelaspelayanan_id is null');
            }
            $criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
            $criteria->compare('ruangan_id',Yii::app()->user->getState('ruangan_id'));
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
            $criteria->compare('tindakansudahbayar_id',$this->tindakansudahbayar_id);
            $criteria->compare('LOWER(dokterpemeriksa1_id)',strtolower($this->dokterpemeriksa1_id),true);
            $criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
            $criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
            $criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
            $criteria->compare('rujukan_id',$this->rujukan_id);
            $criteria->compare('tindakankomponen_id',$this->tindakankomponen_id);
            $criteria->compare('komponentarif_id',$this->komponentarif_id);
            $criteria->compare('LOWER(komponentarif_nama)',strtolower($this->komponentarif_nama),true);
            $criteria->compare('tarif_tindakankomp',$this->tarif_tindakankomp);
            $criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);

            return $criteria;
        }
        public function getTarif($kolom = null, $sum = false){
            $format = new CustomFormat();
            $criteria=new CDbCriteria();
            $criteria->group = "pendaftaran_id,carabayar_id, penjamin_id, dokterpemeriksa1_id,no_rekam_medik,nama_pasien, no_pendaftaran,nama_pegawai,kelaspelayanan_nama,carabayar_nama,penjamin_nama";
                       
            if($kolom == "total"){
                $criteria->addCondition('komponentarif_id = '.Params::komponenTarif('total_tarif'));
            }else if($kolom == "perujuk"){
                $criteria->addCondition('komponentarif_id = '.Params::komponenTarif('jasa_perujuk'));
            }else if($kolom == "pembaca"){
                $criteria->addCondition('komponentarif_id = '.Params::komponenTarif('jasa_pembaca'));
            }else if($kolom == "analisrad"){
                $criteria->addCondition('komponentarif_id = '.Params::komponenTarif('jasa_prod_analisrad'));
            }else if($kolom == "insentif"){
                $criteria->addCondition('komponentarif_id = '.Params::komponenTarif('jasa_insentif'));
            }else if($kolom == "dokterlab"){
                $criteria->addCondition('komponentarif_id = '.Params::komponenTarif('jasa_dokter_lab'));
            }
            
            if(isset($_GET['LBLaporanpendapatanpenunjangkomponenV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpendapatanpenunjangkomponenV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpendapatanpenunjangkomponenV']['tglAkhir']);
                $criteria->addBetweenCondition('tgl_pendaftaran',$tglAwal,$tglAkhir);
            }
            $criteria->select = $criteria->group.',sum(tarif_tindakankomp) AS tarif';
            $criteria->order = $criteria->group;
            $modKomponen = $this->model()->findAll($criteria);
            $totKomponen = 0;
            
            if($sum == true){
                foreach($modKomponen as $key=>$komponen){
                    $totKomponen += $komponen->tarif;
                }
            }else{
                    $criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
                    $criteria->addCondition('dokterpemeriksa1_id = '.$this->dokterpemeriksa1_id);
                    $criteria->addCondition('carabayar_id = '.$this->carabayar_id);
                    $criteria->addCondition('penjamin_id = '.$this->penjamin_id);
                    $totKomponen = $this->model()->find($criteria)->tarif;
//                    $totKomponen = 3;
                    
            }
            
            return $totKomponen;
        }
}