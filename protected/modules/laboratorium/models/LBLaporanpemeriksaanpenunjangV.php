<?php
class LBLaporanpemeriksaanpenunjangV extends LaporanpemeriksaanpenunjangV
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchGrafik()
        {
            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();
            $criteria->select = "count(date_part('Month',tgl_tindakan)) AS jumlah, TO_CHAR(tgl_tindakan,'Mon') AS data";
            $criteria->group = "TO_CHAR(tgl_tindakan,'Mon')";
            $criteria->order = null;
            return new CActiveDataProvider($this,
                array(
                    'criteria' => $criteria,
                )
            );
        }

        public function searchTableLaporan()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();

            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                        'pagination'=>false,
                    ));
        }

        public function searchPrintLaporan() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria = $this->functionCriteria();

            return new CActiveDataProvider($this, array(
                        'pagination' => false,
                        'criteria' => $criteria,
                    ));
        }

        protected function functionCriteria() {
            $criteria = new CDbCriteria();

            $ruangan = array(Params::RUANGAN_ID_LAB,Params::RUANGAN_ID_RAD);
            $criteria->select = 'daftartindakan_kode, daftartindakan_nama,tarif_satuan, 
                                sum(qty_tindakan) as qty_tindakan,
                                sum(tarif_satuan * qty_tindakan) as total';
            $criteria->group = 'daftartindakan_kode, daftartindakan_nama, tarif_satuan';
            $criteria->addBetweenCondition('tgl_tindakan',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
            $criteria->compare('daftartindakan_id',$this->daftartindakan_id);
            $criteria->compare('LOWER(daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
            $criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
            $criteria->compare('LOWER(daftartindakan_katakunci)',strtolower($this->daftartindakan_katakunci),true);
            $criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
            $criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
            $criteria->compare('tarif_satuan',$this->tarif_satuan);
            $criteria->compare('qty_tindakan',$this->qty_tindakan);
            $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
            $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
            $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
            $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
            $criteria->compare('tindakansudahbayar_id',$this->tindakansudahbayar_id);
            $criteria->addInCondition('create_ruangan',$ruangan);
            $criteria->order = 't.daftartindakan_kode ASC';

            return $criteria;
        }   
        
        public function getTotal()
        {
            return $this->tarif_satuan * $this->qty_tindakan;
        }  
}

?>