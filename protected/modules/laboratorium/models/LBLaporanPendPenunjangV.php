<?php
class LBLaporanPendPenunjangV extends LaporanPendPenunjangV{
    public $tglAwal, $tglAkhir, $bulan, $asal;
    public $filter_tab, $pend_seharusnya, $pend_sebenarnya, $sisa;
	
    public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
    
	public function getNamaModel()
    {
        return __CLASS__;
    }
	
	private function functionCriteria()
	{
		$criteria = new CDbCriteria;
        $criteria->group = 'nama_pasien, no_pendaftaran, no_rekam_medik, carabayar_nama, penjamin_nama, tglmasukpenunjang';
		$criteria->select = $criteria->group . ', sum(tarif_tindakan) as pend_seharusnya, sum(iurbiaya_tindakan) as pend_sebenarnya, sum(tarif_tindakan - iurbiaya_tindakan) as sisa';
        $criteria->addBetweenCondition('tglmasukpenunjang', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('no_pendaftaran', $this->no_pendaftaran);
		$criteria->addInCondition('ruangan_id',array(
			Params::RUANGAN_ID_LAB,
			Params::RUANGAN_ID_RAD,
		));
		return $criteria;
	}
	
	public function searchTable()
    {
		$criteria = new CDbCriteria;
		$criteria = $this->functionCriteria();
		$criteria->order = "tglmasukpenunjang DESC";
		$criteria->addCondition("no_rekam_medik NOT LIKE '%LB%'");
        return new CActiveDataProvider($this,
            array(
                'criteria' => $criteria,
            )
        );		
    }
	
	public function searchPrint()
    {
		$criteria = new CDbCriteria;
		$criteria = $this->functionCriteria();
		$criteria->order = "tglmasukpenunjang DESC";
		$criteria->addCondition("no_rekam_medik NOT LIKE '%LB%'");
        return new CActiveDataProvider($this,
            array(
                'pagination' => false,
                'criteria' => $criteria,
            )
        );		
    }
	
	public function searchPendapatanLuar()
	{
		$criteria = new CDbCriteria;
		$criteria = $this->functionCriteria();
		$criteria->addCondition("no_rekam_medik LIKE '%LB%'");
		$criteria->order = "tglmasukpenunjang DESC";
        return new CActiveDataProvider($this,
            array(
                'criteria' => $criteria,
            )
        );
	}
	
	public function searchPendapatanLuarPrint()
	{
		$criteria = new CDbCriteria;
		$criteria = $this->functionCriteria();
		$criteria->addCondition("no_rekam_medik LIKE '%LB%'");
		$criteria->order = "tglmasukpenunjang DESC";
        return new CActiveDataProvider($this,
            array(
				'pagination' => false,
                'criteria' => $criteria,
            )
        );
	}
	
	
}