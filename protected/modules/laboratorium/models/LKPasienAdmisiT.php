<?php

/**
 * This is the model class for table "pasienadmisi_t".
 *
 * The followings are the available columns in table 'pasienadmisi_t':
 * @property integer $pasienadmisi_id
 * @property integer $penjamin_id
 * @property integer $kelaspelayanan_id
 * @property integer $caramasuk_id
 * @property integer $pendaftaran_id
 * @property integer $kamarruangan_id
 * @property integer $pegawai_id
 * @property integer $pasien_id
 * @property integer $ruangan_id
 * @property integer $carabayar_id
 * @property integer $bookingkamar_id
 * @property string $tgladmisi
 * @property string $tglpendaftaran
 * @property string $tglpulang
 * @property string $kunjungan
 * @property boolean $statuskeluar
 * @property boolean $rawatgabung
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class LKPasienAdmisiT extends PasienadmisiT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PasienadmisiT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}