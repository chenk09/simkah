<?php
class LBLaporanjasadokterlabradV extends LaporanjasadokterlabradV
{
	public $pilihDokter,$nama_perujuk,$namapegawai,$namaperujuk,$tarif_tindakan,$jasadokter;
	public $rujukandari_id;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
	public function searchLaporan()
	{
		// Warning: Please modify the following code to remove attributes that
            // should not be searched.
			
		$criteria=new CDbCriteria;
		// EHJ-3606
		$criteria->select = 'no_pendaftaran, nama_pegawai, namaperujuk, nama_pasien, alamat_pasien, penjamin_nama, carabayar_nama, carabayar_id, no_masukpenunjang,tglmasukpenunjang, penjamin_id,sum(tarif_tindakan) as tarif_tindakan,sum(jasadokter) as jasadokter, sum(jumlahuangmuka) as jumlahuangmuka';
		$criteria->group = 'no_pendaftaran, nama_pegawai, namaperujuk, nama_pasien, alamat_pasien, penjamin_nama, carabayar_nama, carabayar_id, no_masukpenunjang,tglmasukpenunjang, penjamin_id';
		// end EHJ-3606
		$criteria->addBetweenCondition('DATE(tglmasukpenunjang)',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		
		if(isset($_GET['LBLaporanjasadokterlabradV']['pilihDokter']) && ($_GET['LBLaporanjasadokterlabradV']['pilihDokter']== "RUJUKANLUAR"))
		{
			$criteria->compare('LOWER(namaperujuk)',strtolower($this->namaperujuk),true);
		}else{
			$criteria->compare('LOWER(nama_pegawai)',strtolower($this->namapegawai),true);
		}
		
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
		$criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->order = 'no_pendaftaran ASC';
		
            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                    ));
	}
        
	public function searchPrintLaporan()
	{
		// Warning: Please modify the following code to remove attributes that
            // should not be searched.
			
		$criteria=new CDbCriteria;
		// EHJ-3606
		$criteria->select = 'no_pendaftaran, nama_pegawai, namaperujuk, nama_pasien, alamat_pasien, penjamin_nama, carabayar_nama, carabayar_id, no_masukpenunjang,tglmasukpenunjang, penjamin_id,sum(tarif_tindakan) as tarif_tindakan,sum(jasadokter) as jasadokter, sum(jumlahuangmuka) as jumlahuangmuka';
		$criteria->group = 'no_pendaftaran, nama_pegawai, namaperujuk, nama_pasien, alamat_pasien, penjamin_nama, carabayar_nama, carabayar_id, no_masukpenunjang,tglmasukpenunjang, penjamin_id';
		// end EHJ-3606
		$criteria->addBetweenCondition('DATE(tglmasukpenunjang)', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(namaperujuk)','testing', true);
		/*
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		
		if(isset($_GET['LBLaporanjasadokterlabradV']['pilihDokter']) && ($_GET['LBLaporanjasadokterlabradV']['pilihDokter']== "RUJUKANLUAR"))
		{
			$criteria->compare('LOWER(namaperujuk)',strtolower($this->namaperujuk),true);
		}else{
			$criteria->compare('LOWER(nama_pegawai)',strtolower($this->namapegawai),true);
		}
		
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
		$criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		*/
		$criteria->order = 'no_pendaftaran ASC';
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
        
	public function getNamaModel(){
		return __CLASS__;
	}

}

?>