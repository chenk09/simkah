<?php

class LKPemeriksaanlabdetM extends PemeriksaanlabdetM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnamnesaT the static model class
	 */
	public $pemeriksaanlab_nama;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
	public function searchTable()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;

                                $criteria->with = array('nilairujukan','pemeriksaanlab');
                                $criteria->order = 't.pemeriksaanlab_id, nilairujukan.kelompokdet, nilairujukan.kelompokumur, nilairujukan.nilairujukan_jeniskelamin';
		$criteria->compare('LOWER(pemeriksaanlab.pemeriksaanlab_nama)',strtolower($this->pemeriksaanlab_nama),true);
		$criteria->compare('t.nilairujukan_id',$this->nilairujukan_id);
		$criteria->compare('t.pemeriksaanlab_id',$this->pemeriksaanlab_id);
		$criteria->compare('pemeriksaanlabdet_nourut',$this->pemeriksaanlabdet_nourut);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pemeriksaanlabdet_id',$this->pemeriksaanlabdet_id);
		$criteria->compare('nilairujukan_id',$this->nilairujukan_id);
		$criteria->compare('pemeriksaanlab_id',$this->pemeriksaanlab_id);
		$criteria->compare('pemeriksaanlabdet_nourut',$this->pemeriksaanlabdet_nourut);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function getPemeriksaanlabItems()
        {
            return PemeriksaanlabM::model()->findAll('pemeriksaanlab_aktif=TRUE ORDER BY pemeriksaanlab_nama');
        }
        
        public function getNilairujukanItems()
        {
            return NilairujukanM::model()->findAll('nilairujukan_aktif=TRUE ORDER BY namapemeriksaandet');
        }
}