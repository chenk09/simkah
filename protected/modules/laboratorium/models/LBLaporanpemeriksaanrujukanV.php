<?php
class LBLaporanpemeriksaanrujukanV extends LaporanpemeriksaanrujukanV
{
        public $no_pendaftaran;
	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}
        
        public function searchGrafik()
        {
            $cond = array(
                "DATE(pasienmasukpenunjang_t.tglmasukpenunjang) BETWEEN '". $this->tglAwal ."' AND '". $this->tglAkhir ."'"
            );            
            $query = "
                SELECT
                    COUNT(tindakanpelayanan_t.daftartindakan_id) AS jumlah,
                    CASE WHEN pasienmasukpenunjang_t.pasienkirimkeunitlain_id IS NULL 
                        THEN 'RUJUKAN LUAR' ELSE 
                        CASE WHEN pasienmasukpenunjang_t.pasienkirimkeunitlain_id IS NOT NULL 
                        THEN 'RUJUKAN RS'
                        END
                    END AS data
                FROM tindakanpelayanan_t
                JOIN daftartindakan_m ON tindakanpelayanan_t.daftartindakan_id = daftartindakan_m.daftartindakan_id
                JOIN pasienmasukpenunjang_t ON 
                    tindakanpelayanan_t.pasienmasukpenunjang_id = pasienmasukpenunjang_t.pasienmasukpenunjang_id
                JOIN pendaftaran_t ON pasienmasukpenunjang_t.pendaftaran_id = pendaftaran_t.pendaftaran_id
                ". (count($cond) > 0 ? " WHERE " . implode(" AND ", $cond) : "" ) ."
                GROUP BY data
            ";
            $data = Yii::app()->db->createCommand($query)->queryAll();
            return new CArrayDataProvider($data);
            /*
            $criteria = new CDbCriteria;
            $criteria->select = "count(date_part('Month',tglmasukpenunjang)) as jumlah, TO_CHAR(tglmasukpenunjang,'Mon') as data";
            $criteria->group = 'tglmasukpenunjang';
            $criteria = $this->functionCriteria();
            
            return new CActiveDataProvider($this,
                array(
                    'criteria' => $criteria,
                    'pagination' => false,
                )
            );
            */
        }
        
        public function searchTableLaporan()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();
            return new CActiveDataProvider($this,
                array(
                    'criteria' => $criteria,
                )
            );
        }

        public function searchPrintLaporan()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.
            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();
            return new CActiveDataProvider($this,
                array(
                    'pagination' => false,
                    'criteria' => $criteria,
                )
            );
        }

        protected function functionCriteria()
        {
            $criteria = new CDbCriteria();
            $criteria->addBetweenCondition('tglmasukpenunjang',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
            $criteria->compare('daftartindakan_id',$this->daftartindakan_id);
            $criteria->compare('LOWER(daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
            $criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
            $criteria->compare('LOWER(daftartindakan_katakunci)',strtolower($this->daftartindakan_katakunci),true);
            $criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
            $criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
            $criteria->compare('tarif_satuan',$this->tarif_satuan);
            $criteria->compare('qty_tindakan',$this->qty_tindakan);
            $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
            $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
            $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
            $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
            $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
            $criteria->compare('tindakansudahbayar_id',$this->tindakansudahbayar_id);
            $criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
            $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('rujukan_id',$this->rujukan_id);
            $criteria->compare('asalrujukan_id',$this->asalrujukan_id);
            $criteria->compare('LOWER(asalrujukan_nama)',strtolower($this->asalrujukan_nama),true);
            $criteria->compare('LOWER(asalrujukan_institusi)',strtolower($this->asalrujukan_institusi),true);
            $criteria->compare('LOWER(no_rujukan)',strtolower($this->no_rujukan),true);
            $criteria->compare('LOWER(nama_perujuk)',strtolower($this->nama_perujuk),true);
            $criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
            $criteria->order = "pendaftaran_id";
            return $criteria;
        }
        
        public function getTotal()
        {
            return $this->tarif_satuan * $this->qty_tindakan;
        }  
               
}
?>