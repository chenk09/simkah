<?php

/**
 * This is the model class for table "informasitariflabrad_v".
 *
 * The followings are the available columns in table 'informasitariflabrad_v':
 * @property string $daftartindakan_kode
 * @property string $daftartindakan_nama
 * @property string $daftartindakan_namalainnya
 * @property string $daftartindakan_katakunci
 * @property integer $perdatarif_id
 * @property string $perdanama_sk
 * @property string $noperda
 * @property string $tglperda
 * @property string $perdatentang
 * @property string $ditetapkanoleh
 * @property string $tempatditetapkan
 * @property integer $jenistarif_id
 * @property string $jenistarif_nama
 * @property integer $tariftindakan_id
 * @property integer $komponentarif_id
 * @property string $komponentarif_nama
 * @property double $harga_tariftindakan
 * @property integer $persendiskon_tind
 * @property double $hargadiskon_tind
 * @property integer $persencyto_tind
 * @property integer $jeniskelas_id
 * @property string $jeniskelas_nama
 * @property integer $kelaspelayanan_id
 * @property string $kelaspelayanan_nama
 * @property string $kelaspelayanan_namalainnya
 * @property integer $daftartindakan_id
 * @property string $jenispemeriksaanlab_nama
 * @property integer $pemeriksaanlab_id
 * @property string $pemeriksaanlab_kode
 * @property string $pemeriksaanlab_nama
 * @property string $pemeriksaanlab_namalainnya
 */
class LKInformasitariflabradV extends InformasitariflabradV
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return InformasitariflabradV the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    
    /**
     * @author Ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
     * searchTarifLabRad() digunakan untuk pencarian tarif pada Informasi Pencarian 
     */
    public function searchTarifLabRad()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('LOWER(daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
        $criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
        $criteria->compare('LOWER(daftartindakan_namalainnya)',strtolower($this->daftartindakan_namalainnya),true);
        $criteria->compare('LOWER(daftartindakan_katakunci)',strtolower($this->daftartindakan_katakunci),true);
        $criteria->compare('perdatarif_id',$this->perdatarif_id);
        $criteria->compare('LOWER(perdanama_sk)',strtolower($this->perdanama_sk),true);
        $criteria->compare('LOWER(noperda)',strtolower($this->noperda),true);
        $criteria->compare('LOWER(tglperda)',strtolower($this->tglperda),true);
        $criteria->compare('LOWER(perdatentang)',strtolower($this->perdatentang),true);
        $criteria->compare('LOWER(ditetapkanoleh)',strtolower($this->ditetapkanoleh),true);
        $criteria->compare('LOWER(tempatditetapkan)',strtolower($this->tempatditetapkan),true);
        $criteria->compare('jenistarif_id',$this->jenistarif_id);
        $criteria->compare('LOWER(jenistarif_nama)',strtolower($this->jenistarif_nama),true);
        $criteria->compare('tariftindakan_id',$this->tariftindakan_id);
        $criteria->compare('komponentarif_id',$this->komponentarif_id);
        $criteria->compare('LOWER(komponentarif_nama)',strtolower($this->komponentarif_nama),true);
        $criteria->compare('harga_tariftindakan',$this->harga_tariftindakan);
        $criteria->compare('persendiskon_tind',$this->persendiskon_tind);
        $criteria->compare('hargadiskon_tind',$this->hargadiskon_tind);
        $criteria->compare('persencyto_tind',$this->persencyto_tind);
        $criteria->compare('jeniskelas_id',$this->jeniskelas_id);
        $criteria->compare('LOWER(jeniskelas_nama)',strtolower($this->jeniskelas_nama),true);
        $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
        $criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
        $criteria->compare('LOWER(kelaspelayanan_namalainnya)',strtolower($this->kelaspelayanan_namalainnya),true);
        $criteria->compare('daftartindakan_id',$this->daftartindakan_id);
        $criteria->compare('LOWER(jenispemeriksaanlab_nama)',strtolower($this->jenispemeriksaanlab_nama),true);
        $criteria->compare('pemeriksaanlab_id',$this->pemeriksaanlab_id);
        $criteria->compare('LOWER(pemeriksaanlab_kode)',strtolower($this->pemeriksaanlab_kode),true);
        $criteria->compare('LOWER(pemeriksaanlab_nama)',strtolower($this->pemeriksaanlab_nama),true);
        $criteria->compare('LOWER(pemeriksaanlab_namalainnya)',strtolower($this->pemeriksaanlab_namalainnya),true);
        
        $criteria->addCondition('kelaspelayanan_id = '. Params::kelasPelayanan('tanapa_kelas'));
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
        $criteria->compare('LOWER(daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
        $criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
        $criteria->compare('LOWER(daftartindakan_namalainnya)',strtolower($this->daftartindakan_namalainnya),true);
        $criteria->compare('LOWER(daftartindakan_katakunci)',strtolower($this->daftartindakan_katakunci),true);
        $criteria->compare('perdatarif_id',$this->perdatarif_id);
        $criteria->compare('LOWER(perdanama_sk)',strtolower($this->perdanama_sk),true);
        $criteria->compare('LOWER(noperda)',strtolower($this->noperda),true);
        $criteria->compare('LOWER(tglperda)',strtolower($this->tglperda),true);
        $criteria->compare('LOWER(perdatentang)',strtolower($this->perdatentang),true);
        $criteria->compare('LOWER(ditetapkanoleh)',strtolower($this->ditetapkanoleh),true);
        $criteria->compare('LOWER(tempatditetapkan)',strtolower($this->tempatditetapkan),true);
        $criteria->compare('jenistarif_id',$this->jenistarif_id);
        $criteria->compare('LOWER(jenistarif_nama)',strtolower($this->jenistarif_nama),true);
        $criteria->compare('tariftindakan_id',$this->tariftindakan_id);
        $criteria->compare('komponentarif_id',$this->komponentarif_id);
        $criteria->compare('LOWER(komponentarif_nama)',strtolower($this->komponentarif_nama),true);
        $criteria->compare('harga_tariftindakan',$this->harga_tariftindakan);
        $criteria->compare('persendiskon_tind',$this->persendiskon_tind);
        $criteria->compare('hargadiskon_tind',$this->hargadiskon_tind);
        $criteria->compare('persencyto_tind',$this->persencyto_tind);
        $criteria->compare('jeniskelas_id',$this->jeniskelas_id);
        $criteria->compare('LOWER(jeniskelas_nama)',strtolower($this->jeniskelas_nama),true);
        $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
        $criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
        $criteria->compare('LOWER(kelaspelayanan_namalainnya)',strtolower($this->kelaspelayanan_namalainnya),true);
        $criteria->compare('daftartindakan_id',$this->daftartindakan_id);
        $criteria->compare('LOWER(jenispemeriksaanlab_nama)',strtolower($this->jenispemeriksaanlab_nama),true);
        $criteria->compare('pemeriksaanlab_id',$this->pemeriksaanlab_id);
        $criteria->compare('LOWER(pemeriksaanlab_kode)',strtolower($this->pemeriksaanlab_kode),true);
        $criteria->compare('LOWER(pemeriksaanlab_nama)',strtolower($this->pemeriksaanlab_nama),true);
        $criteria->compare('LOWER(pemeriksaanlab_namalainnya)',strtolower($this->pemeriksaanlab_namalainnya),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        /**
         * Get Jenis Pemeriksaan untuk dropdownlist pencarian tarif
         */
        public function getJenisPemeriksaan(){
            return JenispemeriksaanlabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true), array('order'=>'jenispemeriksaanlab_urutan ASC'));
        }
} 
?>
