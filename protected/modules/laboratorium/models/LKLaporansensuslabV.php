<?php

class LKLaporansensuslabV extends LaporansensuslabV {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchGrafik() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();
        
        $criteria->select = 'count(tglmasukpenunjang) as jumlah, kunjungan as data';
        $criteria->group = 'kunjungan';
        if ($this->pilihan == 'carabayar'){
            if (!empty($this->penjamin_id)) {
                $criteria->select .= ', penjamin_nama as tick';
                $criteria->group .= ', penjamin_nama';
            } else if (!empty($this->carabayar_id)) {
                $criteria->select .= ', penjamin_nama as tick';
                $criteria->group .= ', penjamin_nama';
            } else {
                $criteria->select .= ', carabayar_nama as tick';
                $criteria->group .= ', carabayar_nama';
            }
        }
        else{
            $criteria->select .= ', jenispemeriksaanlab_nama as tick';
            $criteria->group .= ', jenispemeriksaanlab_nama';
        }

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchTable() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchPrint() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();
        
        return new CActiveDataProvider($this, array(
                    'pagination' => false,
                    'criteria' => $criteria,
                ));
    }

    protected function functionCriteria() {
        $criteria = new CDbCriteria();
        
//        $criteria->select = 'no_rekam_medik, no_masukpenunjang, tglmasukpenunjang, rt, rw, instalasiasal_nama, carabayar_nama, penjamin_nama, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, alamat_pasien, umur, ruanganasal_nama, pendaftaran_id';
//        $criteria->group = 'no_rekam_medik, no_masukpenunjang, tglmasukpenunjang, rt, rw, instalasiasal_nama, carabayar_nama, penjamin_nama, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, alamat_pasien, umur, ruanganasal_nama, pendaftaran_id';
        
        if (!is_array($this->kunjungan)){
            $this->kunjungan = 0;
        }
        if ($this->pilihan == 'jenis'){
            if (!is_array($this->jenispemeriksaanlab_id)){
                $this->jenispemeriksaanlab_id = 0;
            }
        }
        $criteria->addBetweenCondition('date(tglmasukpenunjang)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('ruanganpenunj_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->compare('kunjungan', $this->kunjungan);
        $criteria->compare('jenispemeriksaanlab_id', $this->jenispemeriksaanlab_id);
        $criteria->compare('jenispemeriksaanlab_nama', $this->jenispemeriksaanlab_nama);
        $criteria->compare('pemeriksaanlab_id', $this->pemeriksaanlab_id);
        $criteria->compare('pemeriksaanlab_nama', $this->pemeriksaanlab_nama, true);
        $criteria->compare('penjamin_id', $this->penjamin_id);
        $criteria->compare('carabayar_id', $this->carabayar_id);

        return $criteria;
    }

        public function getNamaModel(){
            return __CLASS__;
        }
}

?>
