<?php

class LKPemeriksaanLabM extends PemeriksaanlabM
{
        public $isChecked = false; //check / uncheck pada pemilihan pemeriksaan (update pemeriksaan)
        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnamnesaT the static model class
	 */
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getJenispemeriksaanLABItems() {
            return LKJenisPemeriksaanLabM::model()->findAll('jenispemeriksaanlab_aktif=TRUE ORDER BY jenispemeriksaanlab_nama');
        }

}