<?php
class LBLaporandetailpemeriksaanpenunjangV extends LaporandetailpemeriksaanpenunjangV
{
        public $total, $pilihDokter,$nama_perujuk,$namapegawai;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchGrafik()
        {
            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();
            $criteria->select = "count(date_part('Month',tglmasukpenunjang)) AS jumlah, TO_CHAR(tglmasukpenunjang,'Mon') AS data";
            $criteria->group = "TO_CHAR(tglmasukpenunjang,'Mon')";
            $criteria->order = null;
            return new CActiveDataProvider($this,
                array(
                    'criteria' => $criteria,
                )
            );
        }

        public function searchTableLaporan()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();

            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                    ));
        }

        public function searchPrintLaporan() {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria = new CDbCriteria;

            $criteria = $this->functionCriteria();

            return new CActiveDataProvider($this, array(
                        'pagination' => false,
                        'criteria' => $criteria,
                    ));
        }

        public function functionCriteria() {
            $criteria = new CDbCriteria();

            $ruangan = array(Params::RUANGAN_ID_LAB,Params::RUANGAN_ID_RAD);
            $criteria->select = 'daftartindakan_kode, daftartindakan_nama, nama_pegawai, carabayar_nama, carabayar_id, no_masukpenunjang,tglmasukpenunjang, penjamin_id,
                                sum(tarif_satuan * qty_tindakan) as total';
            $criteria->group = 'daftartindakan_kode, daftartindakan_nama,no_masukpenunjang, nama_pegawai, carabayar_id, carabayar_nama, penjamin_id, tglmasukpenunjang';
            $criteria->addBetweenCondition('tglmasukpenunjang',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
            $criteria->compare('daftartindakan_id',$this->daftartindakan_id);
            $criteria->compare('LOWER(daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
            $criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
            $criteria->compare('LOWER(daftartindakan_katakunci)',strtolower($this->daftartindakan_katakunci),true);
            $criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
            $criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
            $criteria->compare('tarif_satuan',$this->tarif_satuan);
            $criteria->compare('qty_tindakan',$this->qty_tindakan);
            $criteria->compare('tindakansudahbayar_id',$this->tindakansudahbayar_id);
            $criteria->compare('rujukan_id',$this->rujukan_id);
            $criteria->compare('LOWER(nama_pegawai)',strtolower($this->namapegawai),true);
            $criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_perujuk),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('ruangan_id',$this->ruangan_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->order = 't.daftartindakan_kode ASC';

            return $criteria;
        }   
        
        public function getTotal()
        {
            return $this->tarif_satuan * $this->qty_tindakan;
        }  
        
        public function getNamaModel() {
            return __CLASS__;
        }
}