<?php

class LKPembayaranpelayananT extends PembayaranpelayananT
{
    public $tglAwal;
    public $tglAkhir;
    public $no_rekam_medik;
    public $no_pendaftaran;
    public $nama_pasien;
    public $nama_bin;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PembayaranpelayananT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'pendaftaran'=>array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
                    'pasien'=>array(self::BELONGS_TO, 'PasienM', 'pasien_id'),
                    'ruangan'=>array(self::BELONGS_TO, 'RuanganM', 'ruangan_id'),
                    'tandabuktibayar'=>array(self::BELONGS_TO, 'TandabuktibayarT', 'tandabuktibayar_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pembayaranpelayanan_id' => 'Pembayaranpelayanan',
			'pembebasantarif_id' => 'Pembebasantarif',
			'suratketjaminan_id' => 'Suratketjaminan',
			'pasien_id' => 'Pasien',
			'carabayar_id' => 'Carabayar',
			'penjamin_id' => 'Penjamin',
			'ruangan_id' => 'Ruangan',
			'tandabuktibayar_id' => 'Tandabuktibayar',
			'pendaftaran_id' => 'Pendaftaran',
			'pasienadmisi_id' => 'Pasienadmisi',
			'nopembayaran' => 'Nopembayaran',
			'tglpembayaran' => 'Tglpembayaran',
			'noresep' => 'Noresep',
			'nosjp' => 'Nosjp',
			'totalbiayaoa' => 'Totalbiayaoa',
			'totalbiayatindakan' => 'Totalbiayatindakan',
			'totalbiayapelayanan' => 'Totalbiayapelayanan',
			'totalsubsidiasuransi' => 'Totalsubsidiasuransi',
			'totalsubsidipemerintah' => 'Totalsubsidipemerintah',
			'totalsubsidirs' => 'Totalsubsidirs',
			'totaliurbiaya' => 'Totaliurbiaya',
			'totalbayartindakan' => 'Totalbayartindakan',
			'totaldiscount' => 'Totaldiscount',
			'totalpembebasan' => 'Totalpembebasan',
			'totalsisatagihan' => 'Totalsisatagihan',
			'ruanganpelakhir_id' => 'Ruanganpelakhir',
			'statusbayar' => 'Statusbayar',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchPasienSudahBayar()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                $criteria->addCondition('tandabuktibayar.returbayarpelayanan_id IS NULL');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.penjamin_id', $this->penjamin_id,true);
                $criteria->addBetweenCondition('DATE(tandabuktibayar.tglbuktibayar)', $this->tglAwal, $this->tglAkhir);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchPasienBerdasarkanPenjamin()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                $criteria->addCondition('tandabuktibayar.returbayarpelayanan_id IS NULL');
                $criteria->addCondition('pendaftaran.penjamin_id != 117');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.penjamin_id', $this->penjamin_id,true);
//                $criteria->addBetweenCondition('DATE(tandabuktibayar.tglbuktibayar)', $this->tglAwal, $this->tglAkhir);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));            
        }
        
        public function searchPasienBerdasarkanUmum()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                $criteria->addCondition('tandabuktibayar.returbayarpelayanan_id IS NULL');
                $criteria->addCondition('pendaftaran.penjamin_id = 117');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.penjamin_id', $this->penjamin_id,true);
//                $criteria->addBetweenCondition('DATE(tandabuktibayar.tglbuktibayar)', $this->tglAwal, $this->tglAkhir);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));            
        }
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchPasienBerhutang()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->addBetweenCondition('tandabuktibayar.tglbuktibayar', $this->tglAwal, $this->tglAkhir);
                $criteria->addCondition('totalsisatagihan > 0');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getNamaModel()
        {
            return __CLASS__;
        }
        
}
