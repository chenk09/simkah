<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ROLaporanpasienpenunjangV
 *
 * @author sujana
 */
class LKLaporanpasienpenunjangV extends LaporanpasienpenunjangV {

    public $bulan,$tahun,$tgl;
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchTable() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchGrafik() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria = MyFunction::criteriaGrafik1($this, 'tick');
//        if (!empty($criteria->group) &&(!empty($this->pilihanx))){
//            $criteria->group .=',';
//        }
        $criteria->select .= ', kunjungan as data';
        $criteria->group .= ', kunjungan';
        $criteria->addBetweenCondition('date(tglmasukpenunjang)', $this->tglAwal, $this->tglAkhir);
                $criteria->compare('carabayar_id', $this->carabayar_id);
        $criteria->compare('LOWER(carabayar_nama)', strtolower($this->carabayar_nama), true);
        $criteria->compare('penjamin_id', $this->penjamin_id);
        $criteria->compare('LOWER(penjamin_nama)', strtolower($this->penjamin_nama), true);
        $criteria->compare('propinsi_id', $this->propinsi_id);
        $criteria->compare('LOWER(propinsi_nama)', strtolower($this->propinsi_nama), true);
        $criteria->compare('kabupaten_id', $this->kabupaten_id);
        $criteria->compare('LOWER(kabupaten_nama)', strtolower($this->kabupaten_nama), true);
        $criteria->compare('kecamatan_id', $this->kecamatan_id);
        $criteria->compare('LOWER(kecamatan_nama)', strtolower($this->kecamatan_nama), true);
        $criteria->compare('kelurahan_id', $this->kelurahan_id);
        $criteria->compare('LOWER(kelurahan_nama)', strtolower($this->kelurahan_nama), true);
        $criteria->compare('ruanganpenunj_id', Yii::app()->user->getState('ruangan_id'));
        if(!is_array($this->kunjungan)){
            $this->kunjungan = 0;
        }
        $criteria->compare('kunjungan', $this->kunjungan);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchPrint() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => false
                ));
    }

    protected function functionCriteria() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        if(!is_array($this->kunjungan)){
            $this->kunjungan = 0;
        }
        $criteria->addBetweenCondition('date(tglmasukpenunjang)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('pasien_id', $this->pasien_id);
        $criteria->compare('LOWER(jenisidentitas)', strtolower($this->jenisidentitas), true);
        $criteria->compare('LOWER(no_identitas_pasien)', strtolower($this->no_identitas_pasien), true);
        $criteria->compare('LOWER(namadepan)', strtolower($this->namadepan), true);
        $criteria->compare('LOWER(nama_pasien)', strtolower($this->nama_pasien), true);
        $criteria->compare('LOWER(nama_bin)', strtolower($this->nama_bin), true);
        $criteria->compare('LOWER(jeniskelamin)', strtolower($this->jeniskelamin), true);
        $criteria->compare('LOWER(tempat_lahir)', strtolower($this->tempat_lahir), true);
        $criteria->compare('LOWER(tanggal_lahir)', strtolower($this->tanggal_lahir), true);
        $criteria->compare('LOWER(alamat_pasien)', strtolower($this->alamat_pasien), true);
        $criteria->compare('rt', $this->rt);
        $criteria->compare('rw', $this->rw);
        $criteria->compare('LOWER(agama)', strtolower($this->agama), true);
        $criteria->compare('LOWER(golongandarah)', strtolower($this->golongandarah), true);
        $criteria->compare('LOWER(photopasien)', strtolower($this->photopasien), true);
        $criteria->compare('LOWER(alamatemail)', strtolower($this->alamatemail), true);
        $criteria->compare('LOWER(statusrekammedis)', strtolower($this->statusrekammedis), true);
        $criteria->compare('LOWER(statusperkawinan)', strtolower($this->statusperkawinan), true);
        $criteria->compare('LOWER(no_rekam_medik)', strtolower($this->no_rekam_medik), true);
        $criteria->compare('LOWER(tgl_rekam_medik)', strtolower($this->tgl_rekam_medik), true);
        $criteria->compare('pendaftaran_id', $this->pendaftaran_id);
        $criteria->compare('LOWER(no_pendaftaran)', strtolower($this->no_pendaftaran), true);
        $criteria->compare('LOWER(tgl_pendaftaran)', strtolower($this->tgl_pendaftaran), true);
        $criteria->compare('LOWER(no_urutantri)', strtolower($this->no_urutantri), true);
        $criteria->compare('LOWER(transportasi)', strtolower($this->transportasi), true);
        $criteria->compare('LOWER(keadaanmasuk)', strtolower($this->keadaanmasuk), true);
        $criteria->compare('LOWER(statuspasien)', strtolower($this->statuspasien), true);
        $criteria->compare('alihstatus', $this->alihstatus);
        $criteria->compare('byphone', $this->byphone);
        $criteria->compare('kunjunganrumah', $this->kunjunganrumah);
        $criteria->compare('LOWER(statusmasuk)', strtolower($this->statusmasuk), true);
        $criteria->compare('LOWER(umur)', strtolower($this->umur), true);
        $criteria->compare('LOWER(no_asuransi)', strtolower($this->no_asuransi), true);
        $criteria->compare('LOWER(namapemilik_asuransi)', strtolower($this->namapemilik_asuransi), true);
        $criteria->compare('LOWER(nopokokperusahaan)', strtolower($this->nopokokperusahaan), true);
        $criteria->compare('shift_id', $this->shift_id);
        $criteria->compare('ruanganasal_id', $this->ruanganasal_id);
        $criteria->compare('LOWER(ruanganasal_nama)', strtolower($this->ruanganasal_nama), true);
        $criteria->compare('jeniskasuspenyakit_id', $this->jeniskasuspenyakit_id);
        $criteria->compare('LOWER(jeniskasuspenyakit_nama)', strtolower($this->jeniskasuspenyakit_nama), true);
        $criteria->compare('kelaspelayanan_id', $this->kelaspelayanan_id);
        $criteria->compare('LOWER(kelaspelayanan_nama)', strtolower($this->kelaspelayanan_nama), true);
        $criteria->compare('LOWER(no_masukpenunjang)', strtolower($this->no_masukpenunjang), true);
        $criteria->compare('LOWER(tglmasukpenunjang)', strtolower($this->tglmasukpenunjang), true);
        $criteria->compare('LOWER(no_urutperiksa)', strtolower($this->no_urutperiksa), true);
        $criteria->compare('LOWER(statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('ruanganpenunj_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->compare('LOWER(ruanganpenunj_nama)', strtolower($this->ruanganpenunj_nama), true);
        $criteria->compare('instalasiasal_id', $this->instalasiasal_id);
        $criteria->compare('LOWER(instalasiasal_nama)', strtolower($this->instalasiasal_nama), true);
        $criteria->compare('pasienadmisi_id', $this->pasienadmisi_id);
        $criteria->compare('kunjungan', $this->kunjungan);
        $criteria->compare('LOWER(create_time)', strtolower($this->create_time), true);
        $criteria->compare('LOWER(update_time)', strtolower($this->update_time), true);
        $criteria->compare('LOWER(create_loginpemakai_id)', strtolower($this->create_loginpemakai_id), true);
        $criteria->compare('LOWER(update_loginpemakai_id)', strtolower($this->update_loginpemakai_id), true);
        $criteria->compare('LOWER(create_ruangan)', strtolower($this->create_ruangan), true);
        $criteria->compare('pasienkirimkeunitlain_id', $this->pasienkirimkeunitlain_id);
        $criteria->compare('pegawai_id', $this->pegawai_id);
        $criteria->compare('LOWER(gelardepan)', strtolower($this->gelardepan), true);
        $criteria->compare('LOWER(nama_pegawai)', strtolower($this->nama_pegawai), true);
        $criteria->compare('gelarbelakang_id', $this->gelarbelakang_id);
        $criteria->compare('LOWER(gelarbelakang_nama)', strtolower($this->gelarbelakang_nama), true);
        $criteria->compare('carabayar_id', $this->carabayar_id);
        $criteria->compare('LOWER(carabayar_nama)', strtolower($this->carabayar_nama), true);
        $criteria->compare('penjamin_id', $this->penjamin_id);
        $criteria->compare('LOWER(penjamin_nama)', strtolower($this->penjamin_nama), true);
        $criteria->compare('propinsi_id', $this->propinsi_id);
        $criteria->compare('LOWER(propinsi_nama)', strtolower($this->propinsi_nama), true);
        $criteria->compare('kabupaten_id', $this->kabupaten_id);
        $criteria->compare('LOWER(kabupaten_nama)', strtolower($this->kabupaten_nama), true);
        $criteria->compare('kecamatan_id', $this->kecamatan_id);
        $criteria->compare('LOWER(kecamatan_nama)', strtolower($this->kecamatan_nama), true);
        $criteria->compare('kelurahan_id', $this->kelurahan_id);
        $criteria->compare('LOWER(kelurahan_nama)', strtolower($this->kelurahan_nama), true);

        return $criteria;
    }
    
//    public function searchPasienDBD()
//    {
//             $cond = array(
//                "DATE(pendaftaran_t.tgl_pendaftaran) BETWEEN '". $this->tglAwal ."' AND '". $this->tglAkhir ."'",
//                "pasienmasukpenunjang_t.ruangan_id = 18"
//            );
//            $query = "select 
//                            pasien_m.no_rekam_medik,
//                            pasien_m.nama_pasien,
//                            pasien_m.jeniskelamin,
//                            pasien_m.alamat_pasien,
//                            kabupaten_m.kabupaten_nama,
//                            propinsi_m.propinsi_nama,
//                            pendaftaran_t.tgl_pendaftaran,
//                            pendaftaran_t.no_pendaftaran,
//                            pendaftaran_t.umur,
//                            pasienmasukpenunjang_t.no_masukpenunjang
//                    from tindakanpelayanan_t b
//                    LEFT JOIN pasienmasukpenunjang_t 
//                            ON b.pasienmasukpenunjang_id = pasienmasukpenunjang_t.pasienmasukpenunjang_id
//                    LEFT JOIN pendaftaran_t 
//                            ON pasienmasukpenunjang_t.pendaftaran_id = pendaftaran_t.pendaftaran_id
//                    LEFT JOIN pasien_m 
//                            ON pendaftaran_t.pasien_id = pasien_m.pasien_id
//                    LEFT JOIN detailhasilpemeriksaanlab_t 
//                            ON b.tindakanpelayanan_id = detailhasilpemeriksaanlab_t.tindakanpelayanan_id	
//                    LEFT JOIN hasilpemeriksaanlab_t 
//                            ON detailhasilpemeriksaanlab_t.hasilpemeriksaanlab_id = hasilpemeriksaanlab_t.hasilpemeriksaanlab_id
//                    LEFT JOIN pemeriksaanlabdet_m 
//                            ON detailhasilpemeriksaanlab_t.pemeriksaanlabdet_id = pemeriksaanlabdet_m.pemeriksaanlabdet_id
//                    LEFT JOIN pemeriksaanlab_m
//                            ON pemeriksaanlabdet_m.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id
//                    LEFT JOIN daftartindakan_m 
//                            ON daftartindakan_m.daftartindakan_id = b.daftartindakan_id 
//                    LEFT JOIN jenispemeriksaanlab_m 
//                            ON pemeriksaanlab_m.jenispemeriksaanlab_id = jenispemeriksaanlab_m.jenispemeriksaanlab_id
//                    LEFT JOIN kabupaten_m 
//                            ON pasien_m.kabupaten_id = kabupaten_m.kabupaten_id
//                    LEFT JOIN propinsi_m 
//                            ON pasien_m.propinsi_id = propinsi_m.propinsi_id
//                    ". (count($cond) > 0 ? " WHERE " . implode(" AND ", $cond) : "" ) ."	
//                    GROUP BY 
//                            pasien_m.no_rekam_medik,
//                            pasien_m.nama_pasien,
//                            pasien_m.jeniskelamin,
//                            pasien_m.alamat_pasien,
//                            kabupaten_m.kabupaten_nama,
//                            propinsi_m.propinsi_nama,
//                            pendaftaran_t.tgl_pendaftaran,
//                            pendaftaran_t.no_pendaftaran,
//                            pendaftaran_t.umur,
//                            pasienmasukpenunjang_t.no_masukpenunjang";
//            $data = Yii::app()->db->createCommand($query)->queryAll();
//            return new CArrayDataProvider($data);
//    }
    
//    public function searchPrintPasienDBD()
//    {
//           $cond = array(
//                "DATE(pendaftaran_t.tgl_pendaftaran) BETWEEN '". $this->tglAwal ."' AND '". $this->tglAkhir ."'",
//                "pasienmasukpenunjang_t.ruangan_id = 18"
//            );
//            $query = "select 
//                            pasien_m.no_rekam_medik,
//                            pasien_m.nama_pasien,
//                            pasien_m.jeniskelamin,
//                            pasien_m.alamat_pasien,
//                            kabupaten_m.kabupaten_nama,
//                            propinsi_m.propinsi_nama,
//                            pendaftaran_t.tgl_pendaftaran,
//                            pendaftaran_t.no_pendaftaran,
//                            pendaftaran_t.umur,
//                            pasienmasukpenunjang_t.no_masukpenunjang
//                    from tindakanpelayanan_t b
//                    LEFT JOIN pasienmasukpenunjang_t 
//                            ON b.pasienmasukpenunjang_id = pasienmasukpenunjang_t.pasienmasukpenunjang_id
//                    LEFT JOIN pendaftaran_t 
//                            ON pasienmasukpenunjang_t.pendaftaran_id = pendaftaran_t.pendaftaran_id
//                    LEFT JOIN pasien_m 
//                            ON pendaftaran_t.pasien_id = pasien_m.pasien_id
//                    LEFT JOIN detailhasilpemeriksaanlab_t 
//                            ON b.tindakanpelayanan_id = detailhasilpemeriksaanlab_t.tindakanpelayanan_id	
//                    LEFT JOIN hasilpemeriksaanlab_t 
//                            ON detailhasilpemeriksaanlab_t.hasilpemeriksaanlab_id = hasilpemeriksaanlab_t.hasilpemeriksaanlab_id
//                    LEFT JOIN pemeriksaanlabdet_m 
//                            ON detailhasilpemeriksaanlab_t.pemeriksaanlabdet_id = pemeriksaanlabdet_m.pemeriksaanlabdet_id
//                    LEFT JOIN pemeriksaanlab_m
//                            ON pemeriksaanlabdet_m.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id
//                    LEFT JOIN daftartindakan_m 
//                            ON daftartindakan_m.daftartindakan_id = b.daftartindakan_id 
//                    LEFT JOIN jenispemeriksaanlab_m 
//                            ON pemeriksaanlab_m.jenispemeriksaanlab_id = jenispemeriksaanlab_m.jenispemeriksaanlab_id
//                    LEFT JOIN kabupaten_m 
//                            ON pasien_m.kabupaten_id = kabupaten_m.kabupaten_id
//                    LEFT JOIN propinsi_m 
//                            ON pasien_m.propinsi_id = propinsi_m.propinsi_id
//                    ". (count($cond) > 0 ? " WHERE " . implode(" AND ", $cond) : "" ) ."	
//                    GROUP BY 
//                            pasien_m.no_rekam_medik,
//                            pasien_m.nama_pasien,
//                            pasien_m.jeniskelamin,
//                            pasien_m.alamat_pasien,
//                            kabupaten_m.kabupaten_nama,
//                            propinsi_m.propinsi_nama,
//                            pendaftaran_t.tgl_pendaftaran,
//                            pendaftaran_t.no_pendaftaran,
//                            pendaftaran_t.umur,
//                            pasienmasukpenunjang_t.no_masukpenunjang";
//            $data = Yii::app()->db->createCommand($query)->queryAll();
//            return new CArrayDataProvider($data,array('pagination'=>false));
//    }
    
    
    public function searchPasienDBD()
    {
        $criteria = new CDbCriteria;

        $criteria = $this->searchCriteria();
        $criteria->limit = 10;

        return new CActiveDataProvider($this,
            array(
                'criteria' => $criteria,
            )
        );
    }
        
        
    public function searchPrintPasienDBD()
    {
        $criteria = new CDbCriteria;

        $criteria = $this->searchCriteria();

        return new CActiveDataProvider($this, array(
            'pagination' => false,
            'criteria' => $criteria,
        ));
    }
        
    public function searchGrafikDBD()
    {
        $criteria = new CDbCriteria;

        $criteria->select='count(pendaftaran_id) as jumlah, kelaspelayanan_nama as data';
        $criteria = $this->searchCriteria();

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
        
    protected function searchCriteria() {
        $criteria=new CDbCriteria;

        $criteria->group = "t.pendaftaran_id,t.no_rekam_medik,t.nama_pasien,t.jeniskelamin,t.alamat_pasien,t.kabupaten_nama,
                            t.propinsi_nama,t.tgl_pendaftaran,t.no_pendaftaran,t.umur,t.no_masukpenunjang,t.pasienmasukpenunjang_id";
        $criteria->select = $criteria->group;
        $criteria->order = $criteria->group;

        $pemeriksaanlab_id = array(418,419);
        $criteria->addBetweenCondition('t.tglmasukpenunjang',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('LOWER(t.jenisidentitas)',strtolower($this->jenisidentitas),true);
        $criteria->compare('LOWER(t.no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
        $criteria->compare('LOWER(t.namadepan)',strtolower($this->namadepan),true);
        $criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
        $criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
        $criteria->compare('LOWER(t.jeniskelamin)',strtolower($this->jeniskelamin),true);
        $criteria->compare('LOWER(t.tempat_lahir)',strtolower($this->tempat_lahir),true);
        $criteria->compare('LOWER(t.tanggal_lahir)',strtolower($this->tanggal_lahir),true);
        $criteria->compare('LOWER(t.alamat_pasien)',strtolower($this->alamat_pasien),true);
        $criteria->compare('t.rt',$this->rt);
        $criteria->compare('t.rw',$this->rw);
        $criteria->compare('LOWER(t.agama)',strtolower($this->agama),true);
        $criteria->compare('LOWER(t.golongandarah)',strtolower($this->golongandarah),true);
        $criteria->compare('LOWER(t.photopasien)',strtolower($this->photopasien),true);
        $criteria->compare('LOWER(t.alamatemail)',strtolower($this->alamatemail),true);
        $criteria->compare('LOWER(t.statusrekammedis)',strtolower($this->statusrekammedis),true);
        $criteria->compare('LOWER(t.statusperkawinan)',strtolower($this->statusperkawinan),true);
        $criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
        $criteria->compare('LOWER(t.tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
        $criteria->compare('t.pendaftaran_id',$this->pendaftaran_id);
        $criteria->compare('LOWER(t.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
        $criteria->compare('LOWER(t.no_urutantri)',strtolower($this->no_urutantri),true);
        $criteria->compare('LOWER(t.transportasi)',strtolower($this->transportasi),true);
        $criteria->compare('LOWER(t.keadaanmasuk)',strtolower($this->keadaanmasuk),true);
        $criteria->compare('LOWER(t.statuspasien)',strtolower($this->statuspasien),true);
        $criteria->compare('t.alihstatus',$this->alihstatus);
        $criteria->compare('t.byphone',$this->byphone);
        $criteria->compare('t.kunjunganrumah',$this->kunjunganrumah);
        $criteria->compare('LOWER(t.statusmasuk)',strtolower($this->statusmasuk),true);
        $criteria->compare('LOWER(t.umur)',strtolower($this->umur),true);
        $criteria->compare('LOWER(t.no_asuransi)',strtolower($this->no_asuransi),true);
        $criteria->compare('LOWER(t.namapemilik_asuransi)',strtolower($this->namapemilik_asuransi),true);
        $criteria->compare('LOWER(t.nopokokperusahaan)',strtolower($this->nopokokperusahaan),true);
        $criteria->compare('t.shift_id',$this->shift_id);
        $criteria->compare('t.ruanganasal_id',$this->ruanganasal_id);
        $criteria->compare('LOWER(t.ruanganasal_nama)',strtolower($this->ruanganasal_nama),true);
        $criteria->compare('t.jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
        $criteria->compare('LOWER(t.jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
        $criteria->compare('t.kelaspelayanan_id',$this->kelaspelayanan_id);
        $criteria->compare('LOWER(t.kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
        $criteria->compare('LOWER(t.no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
        $criteria->compare('LOWER(t.tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
        $criteria->compare('LOWER(t.no_urutperiksa)',strtolower($this->no_urutperiksa),true);
        $criteria->compare('LOWER(t.statusperiksa)',strtolower($this->statusperiksa),true);
        $criteria->compare('t.ruanganpenunj_id',$this->ruanganpenunj_id);
        $criteria->compare('LOWER(t.ruanganpenunj_nama)',strtolower($this->ruanganpenunj_nama),true);
        $criteria->compare('t.instalasiasal_id',$this->instalasiasal_id);
        $criteria->compare('LOWER(t.instalasiasal_nama)',strtolower($this->instalasiasal_nama),true);
        $criteria->compare('t.pasienadmisi_id',$this->pasienadmisi_id);
        $criteria->compare('LOWER(t.kunjungan)',strtolower($this->kunjungan),true);
        $criteria->compare('LOWER(t.create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(t.update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(t.create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(t.update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(t.create_ruangan)',strtolower($this->create_ruangan),true);
        $criteria->compare('t.pasienkirimkeunitlain_id',$this->pasienkirimkeunitlain_id);
        $criteria->compare('t.pegawai_id',$this->pegawai_id);
        $criteria->compare('LOWER(t.gelardepan)',strtolower($this->gelardepan),true);
        $criteria->compare('LOWER(t.nama_pegawai)',strtolower($this->nama_pegawai),true);
        $criteria->compare('t.gelarbelakang_id',$this->gelarbelakang_id);
        $criteria->compare('LOWER(t.gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
        $criteria->compare('t.carabayar_id',$this->carabayar_id);
        $criteria->compare('LOWER(t.carabayar_nama)',strtolower($this->carabayar_nama),true);
        $criteria->compare('t.penjamin_id',$this->penjamin_id);
        $criteria->compare('LOWER(t.penjamin_nama)',strtolower($this->penjamin_nama),true);
        $criteria->compare('t.propinsi_id',$this->propinsi_id);
        $criteria->compare('LOWER(t.propinsi_nama)',strtolower($this->propinsi_nama),true);
        $criteria->compare('t.kabupaten_id',$this->kabupaten_id);
        $criteria->compare('LOWER(t.kabupaten_nama)',strtolower($this->kabupaten_nama),true);
        $criteria->compare('t.kecamatan_id',$this->kecamatan_id);
        $criteria->compare('LOWER(t.kecamatan_nama)',strtolower($this->kecamatan_nama),true);
        $criteria->compare('t.kelurahan_id',$this->kelurahan_id);
        $criteria->compare('t.pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
        $criteria->compare('LOWER(t.kelurahan_nama)',strtolower($this->kelurahan_nama),true);
        $criteria->order = 't.nama_pasien asc';
        $criteria->addInCondition('detail.pemeriksaanlab_id',$pemeriksaanlab_id);
        
        $criteria->join = 'JOIN tindakanpelayanan_t tindakan ON t.pasienmasukpenunjang_id = tindakan.pasienmasukpenunjang_id
                           JOIN detailhasilpemeriksaanlab_t detail ON detail.tindakanpelayanan_id = tindakan.tindakanpelayanan_id';

        return $criteria;
    }
    
    public function getNamaModel(){
        return __CLASS__;
    }
}

?>
