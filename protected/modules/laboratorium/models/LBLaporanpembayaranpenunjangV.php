<?php
class LBLaporanpembayaranpenunjangV extends LaporanpembayaranpenunjangV
{
        public $total,$totaldiscount,$totalsisatagihan,$totalbayartindakan,$administrasi,$nama_pasien,
               $jumlah,$no_pendaftaran;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchTableLaporan()
        {
            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();

            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                    ));
        }

        public function searchPrintLaporan() 
        {
            $criteria = new CDbCriteria;

            $criteria = $this->functionCriteria();
            $criteria->order('pendaftaran_id');

            return new CActiveDataProvider($this, array(
                        'pagination' => false,
                        'criteria' => $criteria,
                    ));
        }

        protected function functionCriteria() {
            $criteria = new CDbCriteria();

            if($this->daftartindakan_nama == 'Biaya Administrasi'){
                $adm = 'Biaya Administrasi';
                $criteria->select = 't.nama_pasien,t.pendaftaran_id,t.alamat_pasien,t.tgl_pendaftaran,t.no_pendaftaran,pegawai_m.nama_pegawai,
                                sum(t.tarif_satuan * t.qty_tindakan) as administrasi,
                                sum(t.totaldiscount) as totaldiscount,
                                sum(t.totalsisatagihan) as totalsisatagihan,
                                sum(t.totalbayartindakan) as totalbayartindakan,
                                CASE daftartindakan_m.daftartindakan_nama
                               WHEN "Biaya Administrasi" THEN SUM(tindakanpelayanan_t.qty_tindakan*tindakanpelayanan_t.tarif_satuan) END AS administrasi';
                $criteria->compare('LOWER(daftartindakan_nama)',strtolower($adm));
                $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran));
            }
            $criteria->select = 't.nama_pasien,t.pendaftaran_id,t.alamat_pasien,t.tgl_pendaftaran,t.no_pendaftaran,pegawai_m.nama_pegawai,
                                sum(t.tarif_satuan * t.qty_tindakan) as total,
                                sum(t.totaldiscount) as totaldiscount,
                                sum(t.totalsisatagihan) as totalsisatagihan,
                                sum(t.totalbayartindakan) as totalbayartindakan';
            $criteria->group = 't.nama_pasien,t.pendaftaran_id,t.alamat_pasien,t.tgl_pendaftaran,t.no_pendaftaran,pegawai_m.nama_pegawai';
            $criteria->addBetweenCondition('DATE(tindakanpelayanan_t.tgl_tindakan)',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('tindakanpelayanan_t.ruangan_id',Yii::app()->user->getState('ruangan_id'));
            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran));
            $criteria->join = 'JOIN tindakanpelayanan_t on t.tindakanpelayanan_id = tindakanpelayanan_t.tindakanpelayanan_id
                               JOIN daftartindakan_m on tindakanpelayanan_t.daftartindakan_id = daftartindakan_m.daftartindakan_id
                               JOIN pasienmasukpenunjang_t on tindakanpelayanan_t.pasienmasukpenunjang_id = pasienmasukpenunjang_t.pasienmasukpenunjang_id
                               JOIN tindakansudahbayar_t on tindakanpelayanan_t.tindakansudahbayar_id = tindakansudahbayar_t.tindakansudahbayar_id
                               JOIN pembayaranpelayanan_t on tindakansudahbayar_t.pembayaranpelayanan_id = pembayaranpelayanan_t.pembayaranpelayanan_id
                               JOIN loginpemakai_k ON pembayaranpelayanan_t.create_loginpemakai_id = loginpemakai_k.loginpemakai_id
                               JOIN pegawai_m ON loginpemakai_k.pegawai_id = pegawai_m.pegawai_id
                              ';

            return $criteria;
        } 
        
        protected function criteriaBayarPeriksa() {
            $criteria = new CDbCriteria();

            $ruangan = array(Params::RUANGAN_ID_LAB,Params::RUANGAN_ID_RAD);

            if($this->daftartindakan_nama == 'Biaya Administrasi'){
                $adm = 'Biaya Administrasi';
                $criteria->select = 'nama_pasien,pendaftaran_id,alamat_pasien,tgl_pendaftaran,no_pendaftaran,nama_pegawai,
                                sum(tarif_satuan * t.qty_tindakan) as administrasi,
                                sum(totaldiscount) as totaldiscount,
                                sum(totalsisatagihan) as totalsisatagihan,
                                sum(totalbayartindakan) as totalbayartindakan,
                                CASE daftartindakan_nama
                               WHEN "Biaya Administrasi" THEN SUM(qty_tindakan*tarif_satuan) END AS administrasi';
                $criteria->addBetweenCondition('tglpembayaran',$this->tglAwal,$this->tglAkhir,true);
                $criteria->compare('LOWER(daftartindakan_nama)',strtolower($adm));
                $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran));
                $criteria->addInCondition('create_ruangan');
                $criteria->addInCondition('ruangan_id',$ruangan);
            }
            $criteria->select = 'nama_pasien,pendaftaran_id,alamat_pasien,tgl_pendaftaran,no_pendaftaran,nama_pegawai,
                                sum(tarif_satuan * qty_tindakan) as total,
                                sum(totaldiscount) as totaldiscount,
                                sum(totalsisatagihan) as totalsisatagihan,
                                sum(totalbayartindakan) as totalbayartindakan';
            $criteria->group = 'nama_pasien,pendaftaran_id,alamat_pasien,tgl_pendaftaran,no_pendaftaran,nama_pegawai';
            $criteria->addBetweenCondition('tglpembayaran',$this->tglAwal,$this->tglAkhir,true);
//            $criteria->compare('ruangan_id',Yii::app()->user->getState('ruangan_id'));
            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran));
            $criteria->addInCondition('ruangan_id',$ruangan);

            return $criteria;
        } 
        
        public function searchLaporanPembayaranPemeriksaan()
        {
            $criteria = new CDbCriteria;
            $criteria = $this->criteriaBayarPeriksa();

            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                    ));
        }

        public function searchPrintLaporanPembayaranPemeriksaan() 
        {
            $criteria = new CDbCriteria;

            $criteria = $this->criteriaBayarPeriksa();
            $criteria->limit = -1;

            return new CActiveDataProvider($this, array(
                        'pagination' => false,
                        'criteria' => $criteria,
                    ));
        }
        public function getTotal()
        {
            return $this->tarif_satuan * $this->qty_tindakan;
        } 
        
}
?>