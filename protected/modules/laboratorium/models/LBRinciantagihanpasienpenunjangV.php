<?php

/**
 * This is the model class for table "tindakankomponen_t".
 *
 * The followings are the available columns in table 'tindakankomponen_t':
 * @property integer $tindakankomponen_id
 * @property integer $tindakanpelayanan_id
 * @property integer $komponentarif_id
 * @property double $tarif_tindakankomp
 * @property double $tarifcyto_tindakankomp
 * @property double $subsidiasuransikomp
 * @property double $subsidipemerintahkomp
 * @property double $subsidirumahsakitkomp
 * @property double $iurbiayakomp
 */
class LBRinciantagihanpasienpenunjangV extends RinciantagihanpasienpenunjangV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TindakankomponenT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchRincianTagihan()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->select = 't.*,pendaftaran_t.*,pasien_m.*';
		$criteria->compare('t.profilrs_id',$this->profilrs_id);
		$criteria->compare('t.pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(t.tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('LOWER(t.jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(t.no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
		$criteria->compare('LOWER(t.namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(t.jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(t.tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(t.tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(t.alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('t.rt',$this->rt);
		$criteria->compare('t.rw',$this->rw);
		$criteria->compare('LOWER(t.statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(t.agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(t.golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(t.rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('t.anakke',$this->anakke);
		$criteria->compare('t.jumlah_bersaudara',$this->jumlah_bersaudara);
		$criteria->compare('LOWER(t.no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(t.no_mobile_pasien)',strtolower($this->no_mobile_pasien),true);
		$criteria->compare('LOWER(t.warga_negara)',strtolower($this->warga_negara),true);
		$criteria->compare('LOWER(t.photopasien)',strtolower($this->photopasien),true);
		$criteria->compare('LOWER(t.alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('t.pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(t.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(t.tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('LOWER(t.umur)',strtolower($this->umur),true);
		$criteria->compare('LOWER(t.no_asuransi)',strtolower($this->no_asuransi),true);
		$criteria->compare('LOWER(t.namapemilik_asuransi)',strtolower($this->namapemilik_asuransi),true);
		$criteria->compare('LOWER(t.nopokokperusahaan)',strtolower($this->nopokokperusahaan),true);
		$criteria->compare('LOWER(t.namaperusahaan)',strtolower($this->namaperusahaan),true);
		$criteria->compare('LOWER(t.tglselesaiperiksa)',strtolower($this->tglselesaiperiksa),true);
		$criteria->compare('t.tindakanpelayanan_id',$this->tindakanpelayanan_id);
		$criteria->compare('t.penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(t.penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('t.carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(t.carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('t.kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('LOWER(t.kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
		$criteria->compare('t.instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(t.instalasi_nama)',strtolower($this->instalasi_nama),true);
		$criteria->compare('t.ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(t.ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(t.tgl_tindakan)',strtolower($this->tgl_tindakan),true);
		$criteria->compare('t.daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('LOWER(t.daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
		$criteria->compare('LOWER(t.daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
		$criteria->compare('t.tipepaket_id',$this->tipepaket_id);
		$criteria->compare('LOWER(t.tipepaket_nama)',strtolower($this->tipepaket_nama),true);
		$criteria->compare('t.daftartindakan_karcis',$this->daftartindakan_karcis);
		$criteria->compare('t.daftartindakan_visite',$this->daftartindakan_visite);
		$criteria->compare('t.daftartindakan_konsul',$this->daftartindakan_konsul);
		$criteria->compare('t.tarif_rsakomodasi',$this->tarif_rsakomodasi);
		$criteria->compare('t.tarif_medis',$this->tarif_medis);
		$criteria->compare('t.tarif_paramedis',$this->tarif_paramedis);
		$criteria->compare('t.tarif_bhp',$this->tarif_bhp);
		$criteria->compare('t.tarif_satuan',$this->tarif_satuan);
		$criteria->compare('t.tarif_tindakan',$this->tarif_tindakan);
		$criteria->compare('LOWER(t.satuantindakan)',strtolower($this->satuantindakan),true);
		$criteria->compare('t.qty_tindakan',$this->qty_tindakan);
		$criteria->compare('t.cyto_tindakan',$this->cyto_tindakan);
		$criteria->compare('t.tarifcyto_tindakan',$this->tarifcyto_tindakan);
		$criteria->compare('t.discount_tindakan',$this->discount_tindakan);
		$criteria->compare('t.pembebasan_tindakan',$this->pembebasan_tindakan);
		$criteria->compare('t.subsidiasuransi_tindakan',$this->subsidiasuransi_tindakan);
		$criteria->compare('t.subsidipemerintah_tindakan',$this->subsidipemerintah_tindakan);
		$criteria->compare('t.subsisidirumahsakit_tindakan',$this->subsisidirumahsakit_tindakan);
		$criteria->compare('t.iurbiaya_tindakan',$this->iurbiaya_tindakan);
		$criteria->compare('LOWER(t.create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(t.update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(t.create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(t.update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(t.create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('t.jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
		$criteria->compare('LOWER(t.jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
		$criteria->compare('t.pembayaranpelayanan_id',$this->pembayaranpelayanan_id);
		$criteria->compare('t.kategoritindakan_id',$this->kategoritindakan_id);
		$criteria->compare('LOWER(t.kategoritindakan_nama)',strtolower($this->kategoritindakan_nama),true);
		$criteria->compare('t.pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(t.gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(t.nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('t.gelarbelakang_id',$this->gelarbelakang_id);
		$criteria->compare('LOWER(t.gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
		$criteria->compare('t.ruanganpendaftaran_id',$this->ruanganpendaftaran_id);
		$criteria->compare('t.pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
		$criteria->compare('LOWER(t.no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
		$criteria->compare('LOWER(t.tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
		$criteria->compare('t.ruanganpenunjang_id', $this->ruanganpenunjang_id);
                $criteria->compare('t.ruanganpendaftaran_id',18);
		$criteria->compare('t.tindakansudahbayar_id',$this->tindakansudahbayar_id);
                $criteria->addBetweenCondition('t.tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
                $criteria->join = 'LEFT JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id LEFT JOIN pasien_m ON pasien_m.pasien_id = t.pasien_id';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}