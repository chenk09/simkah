<?php

class PendaftaranPeriksaController extends SBaseController
{
        public $successSave = false;
        public $successSaveTindakanPelayanan = true;
        public $successSavepasienKirimKeUnitLain=false;
        public $successSaveHasilPemeriksaan = true; //Diset true karena akan di looping pada simpan hasil pemeriksaan dan di tambahkan operator AND
        public $successSaveHasilPemeriksaanDet = true; //Diset true karena akan di looping pada simpan detail hasil pemeriksaan dan di tambahkan operator AND
        public $successSavePasienMasukPenunjang=false;
        
	public function actionIndex()
	{
            $modDaftarTindakan = new DaftartindakanM();
            $modPendaftaran = new LKPendaftaranT;
            $modPasien = new LKPasienM;
            $modPeriksaLab = LKPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true));
            $modJenisPeriksaLab = LKJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan'));
             
            if(isset($_POST['PemeriksaanLab'])){
//                echo "a";exit;
                $idPasienKirimKeUnitLain = $_POST['idPasienKirimKeunitLain'];
                $idPendaftaran = $_POST['LKPendaftaranT']['pendaftaran_id'];
                $modPendaftaran = LKPendaftaranT::model()->findByPk($idPendaftaran);
                $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
                $transaction = Yii::app()->db->beginTransaction();
                try{
//                    echo "b";exit;
                        $modPasienMasukPenunjang=$this->savePasienMasukPenunjang($modPasien,$modPendaftaran,$idPasienKirimKeUnitLain);
                        $this->saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$_POST['PemeriksaanLab']);
    
                        if ($this->successSaveHasilPemeriksaan && $this->successSavePasienMasukPenunjang){
//                            echo "c";exit;
                            LKPasienKirimKeUnitLainT::model()->updateByPk($idPasienKirimKeUnitLain, array('pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id));
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Berhasil Disimpan");
                            $this->redirect(array('daftarPasien/index'));
                        } else {
//                            echo "d";exit;
                            Yii::app()->user->setFlash('error',"Data gagal disimpan");
                        }
                }
                catch(Exception $exc){
//                    echo "f";exit;
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
                        
            $this->render('index',array('modPeriksaLab'=>$modPeriksaLab,
                                        'modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modJenisPeriksaLab'=>$modJenisPeriksaLab,
                                        'modDaftarTindakan'=>$modDaftarTindakan));
	}
        
       
        
        protected function saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$pemeriksaanLab)
        {
              foreach($pemeriksaanLab as $jenisKelompok => $value){
                if($jenisKelompok == Params::LAB_PATOLOGI){
                    //echo 'Patologi Klinik <pre>'.print_r($value,1).'</pre>';
                    $modHasilPemeriksaan = new LKHasilPemeriksaanLabT;
                    $modHasilPemeriksaan->unsetAttributes(); // untuk membersihkan attribute kalau sebelumnya di pakai
                    $modHasilPemeriksaan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modHasilPemeriksaan->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                    $modHasilPemeriksaan->pasien_id = $modPasien->pasien_id;
                    $modHasilPemeriksaan->nohasilperiksalab = Generator::noHasilPemeriksaanLK();
                    $modHasilPemeriksaan->tglhasilpemeriksaanlab = date('Y-m-d H:i:s');
                    $modHasilPemeriksaan->hasil_kelompokumur = Generator::kelompokUmurNama($modPasien->tanggal_lahir);
                    $modHasilPemeriksaan->hasil_jeniskelamin = $modPasien->jeniskelamin;
                    $modHasilPemeriksaan->statusperiksahasil = Params::statusPeriksaLK(1);
                    if($modHasilPemeriksaan->save()){
                        $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
                        
                        $daftartindakan_id = "";
                        foreach ($value as $pemeriksaanId => $dataPeriksa){
                            if($dataPeriksa['daftartindakan_id'] == Params::PAKET_LAB){
                                if($daftartindakan_id != $dataPeriksa['daftartindakan_id'])
                                {
                                    $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                                    $daftartindakan_id = $dataPeriksa['daftartindakan_id'];
                                }
                            }else{
                                $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                            }
                            
                            $modPemeriksaanLab = PemeriksaanlabM::model()->findAllByAttributes(array(
                                'daftartindakan_id'=>$dataPeriksa['daftartindakan_id']
                            ));
                            foreach ($modPemeriksaanLab as $detailPemeriksaanLab){
                                $details = PemeriksaanlabdetM::model()->pemeriksaanDetail($modHasilPemeriksaan, $detailPemeriksaanLab['pemeriksaanlab_id']);
                                if(!$details){ //Jika tidak memiliki nilai rujukan
                                    $this->successSaveHasilPemeriksaanDet = false;
                                }
                                foreach ($details as $k => $detail) {
                                    $PemeriksaanLabDet = $detailPemeriksaanLab['pemeriksaanlab_id'];
                                    $modDetailHasilPemeriksaan = new LKDetailHasilPemeriksaanLabT;
                                    $modDetailHasilPemeriksaan->hasilpemeriksaanlab_id = $modHasilPemeriksaan->hasilpemeriksaanlab_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlabdet_id = $detail->pemeriksaanlabdet_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlab_id = $detailPemeriksaanLab['pemeriksaanlab_id'];
                                    $modDetailHasilPemeriksaan->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                                    if($modDetailHasilPemeriksaan->save()){ //Jika Detail Pemeriksaan berhasil Disimpan Ke Hasil Pemeriksaan
                                        $this->successSaveHasilPemeriksaanDet = $this->successSaveHasilPemeriksaanDet && true;
                                    }else{ //Jika Ada yang tidak disimpan
                                        $this->successSaveHasilPemeriksaanDet = false;
                                        Yii::app()->user->setFlash('error',"Data Detail Hasil Pemeriksaan ID=".$pemeriksaanId." gagal disimpan");
                                    }
                                }
                            }
                        }
                    } else 
                        $this->successSaveHasilPemeriksaan = false;

                } else {
                    //echo 'Anatomi <pre>'.print_r($value,1).'</pre>';
                    foreach ($value as $pemeriksaanId => $dataPeriksa) {
                        $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                        $modHasilPemeriksaanPA = new LKHasilPemeriksaanPAT;
                        $modHasilPemeriksaanPA->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                        $modHasilPemeriksaanPA->pemeriksaanlab_id = $pemeriksaanId;
                        $modHasilPemeriksaanPA->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modHasilPemeriksaanPA->pasien_id = $modTindakanPelayananT->pasien_id;
                        $modHasilPemeriksaanPA->pendaftaran_id = $modTindakanPelayananT->pendaftaran_id;
                        $modHasilPemeriksaanPA->nosediaanpa = Generator::noSediaanPA();
                        $modHasilPemeriksaanPA->tglperiksapa = $modTindakanPelayananT->tgl_tindakan;
                        if($modHasilPemeriksaanPA->validate()){
                            if($modHasilPemeriksaanPA->save()){
                                 $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
                            }else{
                                 $this->successSaveHasilPemeriksaan = false;                   
                            }  
                        }
                        
                    }
                }
            }
            
        }
        
         
        protected function savePasienMasukPenunjang($modPasien,$modPendaftaran,$idPasienKirimKeUnitLain)
        {
            $modPasienMasukPenunjang = new LKPasienMasukPenunjangT;
            $modPasienMasukPenunjang->pasienkirimkeunitlain_id = $idPasienKirimKeUnitLain;
            $modPasienMasukPenunjang->pasien_id = $modPasien->pasien_id;
            $modPasienMasukPenunjang->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
            if(!empty($modPendaftaran->pasienadmisi_id)){ ////Jika Paien Berasal dari Rawat Inap
                $modPasienMasukPenunjang->pasienadmisi_id=$modPendaftaran->pasienadmisi_id;
            }else{ ////Jika Pasien berasal dari RJ ataw RD
                $modPasienMasukPenunjang->pendaftaran_id=$modPendaftaran->pendaftaran_id;
            }
            $modPasienMasukPenunjang->pegawai_id=$modPendaftaran->pegawai_id;
            $modPasienMasukPenunjang->kelaspelayanan_id=$modPendaftaran->kelaspelayanan_id;
            $modPasienMasukPenunjang->ruangan_id = Params::RUANGAN_ID_LAB;
            $modPasienMasukPenunjang->no_masukpenunjang=Generator::noMasukPenunjang('LK');
            $modPasienMasukPenunjang->tglmasukpenunjang=date('Y-m-d H:i:s');
            $modPasienMasukPenunjang->no_urutperiksa=Generator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienMasukPenunjang->ruangan_id);
            $modPasienMasukPenunjang->kunjungan=$modPendaftaran->kunjungan;
            $modPasienMasukPenunjang->statusperiksa=$modPendaftaran->statusperiksa;
            $modPasienMasukPenunjang->ruanganasal_id=$modPendaftaran->ruangan_id;
            if($modPasienMasukPenunjang->validate()){//Jika proses Palidasi Pasien Masuk Penunjang Berhasil
                if($modPasienMasukPenunjang->save()){//Jika $modPasienMasukPenunjang Berhasil Disimpan
                     $this->successSavePasienMasukPenunjang=true;
                }else{//Jika $modPasienMasukPenunjang Gagal Disimpan
                     $this->successSavePasienMasukPenunjang=false;
                     Yii::app()->user->setFlash('error',"Data Pasien Masuk Pennunjang Gagal Disimpan.");

                }
            }else{//Jika proses Palidasi Pasien Masuk Penunjang Berhasil Gagal
               $this->successSavePasienMasukPenunjang=false;
               Yii::app()->user->setFlash('error',"Data Pasien Masuk Pennunjang Tidak Tervalidasi.");

            }
            return $modPasienMasukPenunjang;    
        }
        
        protected function saveTindakanPelayanan($modPendaftaran,$modPasien,$dataPemeriksaan, $modPasienMasukPenunjang)
        {       
                
                $modTindakanPelayananT = new LKTindakanPelayananT;
                $modTindakanPelayananT->penjamin_id = $modPendaftaran->penjamin_id;
                $modTindakanPelayananT->pasien_id = $modPasien->pasien_id;
                $modTindakanPelayananT->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                $modTindakanPelayananT->instalasi_id = Params::INSTALASI_ID_LAB;
                $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                $modTindakanPelayananT->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayananT->daftartindakan_id = $dataPemeriksaan['daftartindakan_id'];
                $modTindakanPelayananT->carabayar_id = $modPendaftaran->carabayar_id;
                $modTindakanPelayananT->qty_tindakan = $dataPemeriksaan['qty_tindakan'];
                $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayananT->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayananT->tarif_satuan = $dataPemeriksaan['tarif_tindakan'];
                $modTindakanPelayananT->tarif_tindakan = $modTindakanPelayananT->tarif_satuan * $modTindakanPelayananT->qty_tindakan;
                $modTindakanPelayananT->satuantindakan = $dataPemeriksaan['satuantindakan'];
                $cyto_tindakan = $modTindakanPelayananT->cyto_tindakan = $dataPemeriksaan['cyto_tindakan'];
                $modTindakanPelayananT->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
                $modTindakanPelayananT->discount_tindakan = 0;
                $modTindakanPelayananT->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                
                $modTindakanPelayananT->subsidiasuransi_tindakan = 0;
                $modTindakanPelayananT->subsidipemerintah_tindakan = 0;
                $modTindakanPelayananT->subsisidirumahsakit_tindakan = 0;
                $modTindakanPelayananT->iurbiaya_tindakan = 0;
                $modTindakanPelayananT->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayananT->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                
                if($cyto_tindakan){
                    $modTindakanPelayananT->tarifcyto_tindakan = $dataPemeriksaan['tarif_cyto'];;
                }else{
                    $modTindakanPelayananT->tarifcyto_tindakan = 0;
                }
                
//                $tarifTindakan= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayananT->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayananT->kelaspelayanan_id.'');
                $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$dataPemeriksaan['daftartindakan_id'], 'kelaspelayanan_id'=>$modTindakanPelayananT->kelaspelayanan_id));
                
                
                foreach($tarifTindakan as $dataTarif):
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                           $modTindakanPelayananT->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                    }
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                       $modTindakanPelayananT->tarif_medis=$dataTarif['harga_tariftindakan'];
                    }
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                       $modTindakanPelayananT->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                    }
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                       $modTindakanPelayananT->tarif_bhp=$dataTarif['harga_tariftindakan'];
                   }
                endforeach;
                if($modTindakanPelayananT->save()){//Jika Tindakan Pelayanan Berhasil tersimpan
                    $idPeriksaLab = $dataPemeriksaan['pemeriksaanlab_id'];
                    $jumlahTindKomponen = COUNT($_POST['LKTindakanKomponenT'][$idPeriksaLab]);
                    for($j=0; $j<$jumlahTindKomponen; $j++):
                        $modTindakanKomponen = new LKTindakanKomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['komponentarif_id'];
                        $modTindakanKomponen->tarif_tindakankomp = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'] * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $modTindakanPelayananT->tarif_tindakan;
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                    endfor;
                    
                    $this->successSaveTindakanPelayanan = $this->successSaveTindakanPelayanan && true;
                } else {
                    $this->successSaveTindakanPelayanan = false;
                }
                
                return $modTindakanPelayananT;
        }
        
        function actionDaftarTindakan(){
            
            $this->layout = 'frameDialog';
            $modDaftarTindakan = new DaftartindakanM();
            
            $this->render('_formDaftarTindakan',array(
                            'modDaftarTindakan'=>$modDaftarTindakan,
            ));
        }
}