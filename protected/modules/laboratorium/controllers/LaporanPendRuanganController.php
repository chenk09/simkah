<?php
class LaporanPendRuanganController extends SBaseController{
	public function actionIndex($view = 'rs')
	{
		$model = new LBLaporanPendPenunjangV;
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
		$model->filter_tab = $view;
		
		if(isset($_GET["LBLaporanPendPenunjangV"]))
		{
			$model->attributes = $_GET["LBLaporanPendPenunjangV"];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanPendPenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanPendPenunjangV']['tglAkhir']);
		}
		
		if($view == 'rs'){
			$this->render('pendapatan_rs', array(
				'model' => $model,
			));
		}elseif($view == 'luar')
		{
			$this->render('pendapatan_luar', array(
				'model' => $model,
			));
		}
	}
	
	public function actionCetak()
	{
		$model = new LBLaporanPendPenunjangV;
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
		$model->filter_tab = "rs";
		
		if(isset($_GET["LBLaporanPendPenunjangV"]))
		{
			$model->attributes = $_GET["LBLaporanPendPenunjangV"];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanPendPenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanPendPenunjangV']['tglAkhir']);
			$model->filter_tab = $_GET['LBLaporanPendPenunjangV']['filter_tab'];
		}

		if($_GET["caraPrint"] == 'pdf')
		{
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage("L", '', '', '', '', 5, 5, 5, 5, 5, 5);
			if($model->filter_tab == 'rs'){
				$mpdf->WriteHTML($this->renderPartial('pendapatan_rs', array(
					'model' => $model,
					'caraPrint'=>$_GET["caraPrint"]
				), true));
			}elseif($model->filter_tab == 'luar')
			{
				$mpdf->WriteHTML($this->renderPartial('pendapatan_luar', array(
					'model' => $model,
					'caraPrint'=>$_GET["caraPrint"]
				), true));
			}
            $mpdf->Output();			
		}else{
			if($_GET["caraPrint"] == 'print'){
				$this->layout = "//layouts/printWindows";
			}else $this->layout = '//layouts/printExcel';
			
			if($model->filter_tab == 'rs'){
				$this->render('pendapatan_rs', array(
					'model' => $model,
					'caraPrint'=>$_GET["caraPrint"]
				));
			}elseif($model->filter_tab == 'luar')
			{
				$this->render('pendapatan_luar', array(
					'model' => $model,
					'caraPrint'=>$_GET["caraPrint"]
				));
			}			
		}
	}
	
}