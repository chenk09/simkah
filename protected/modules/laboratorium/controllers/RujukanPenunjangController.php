<?php

class RujukanPenunjangController extends SBaseController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        if(isset($_GET['ajax']) && $_GET['ajax']=='pasienpenunjangrujukan-m-grid')
        {
            $format = new CustomFormat;
            echo $format->formatDateTimeMediumForDB($_GET['tglAkhir']);
            $criteria->compare('LOWER(no_pendaftaran)', strtolower($_GET['noPendaftaran']),true);
            $criteria->compare('LOWER(nama_pasien)', strtolower($_GET['namaPasien']),true);
            $criteria->compare('LOWER(no_rekam_medik)', strtolower($_GET['noRekamMedik']),true);
            if($_GET['cbTglMasuk'])
                $criteria->addBetweenCondition('tgl_kirimpasien', "'".$format->formatDateTimeMediumForDB($_GET['tglAwal'])."'", "'".$format->formatDateTimeMediumForDB($_GET['tglAkhir'])."'");
        } else {
            $criteria->addBetweenCondition('DATE(tgl_kirimpasien)', date('Y-m-d', strtotime('-5 days')), date('Y-m-d'));
        }
        $criteria->compare('instalasi_id', Yii::app()->user->getState('instalasi_id'));
        $dataProvider = new CActiveDataProvider(PasienkirimkeunitlainV::model(),
            array(
                'criteria'=>$criteria,
            )
        );
        $this->render('index',array('dataProvider'=>$dataProvider));
    }
    
    public function actionBatalPemeriksaan()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            //                echo "a";exit;
            $transaction = Yii::app()->db->beginTransaction();
            $pesan = 'success';
            $status = 'ok';

            try{
                $idPendaftaran = $_POST['idPendaftaran'];
                $idKirimUnit = $_POST['idKirimUnit'];

                /*
                 * cek data pendaftaran &  pasien masuk penunjang
                 */
                $pasienMasukPenunjang = PasienmasukpenunjangT::model()->findByAttributes(
                    array(
                        'pendaftaran_id'=>$idPendaftaran,
                        'pasienkirimkeunitlain_id'=>$idKirimUnit,
                        'ruangan_id'=>PARAMS::RUANGAN_ID_LAB
                    )
                );
                
                if(count($pasienMasukPenunjang) > 0){
                    $pendaftaran = PendaftaranT::model()->findByPk($pasienMasukPenunjang->pendaftaran_id);
                    $pendaftaran_id = $pasienMasukPenunjang->pendaftaran_id;
                    $pasien_id = $pasienMasukPenunjang->pasien_id;
                    $pasienmasukpenunjang_id = $pasienMasukPenunjang->pasienmasukpenunjang_id;
                    $pasienkirimkeunitlain_id = $pasienMasukPenunjang->pasienkirimkeunitlain_id;
                }else{
                    $pendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
                    $pendaftaran_id = $pendaftaran->pendaftaran_id;
                    $pasien_id = $pendaftaran->pasien_id;
                    $pasienmasukpenunjang_id = null;
                    $pasienkirimkeunitlain_id = $idKirimUnit;
                }
                
//                echo $pasienMasukPenunjang->pendaftaran_id.'<br/>';
//                echo $pasienMasukPenunjang->pasien_id.'<br/>';
//                echo $pasienMasukPenunjang->pasienmasukpenunjang_id;exit;
                /** end cek data pendaftaran & pasien masuk penunjang **/

                if(empty($pasienkirimkeunitlain_id))
                {
                    $model = new PasienbatalperiksaR();
                    $model->pendaftaran_id = $pendaftaran_id;
                    $model->pasien_id = $pasien_id;
                    $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                    $model->tglbatal = date('Y-m-d');
                    $model->keterangan_batal = "Batal Laboratorium";
                    $model->create_time = date('Y-m-d H:i:s');
                    $model->update_time = null;
                    $model->create_loginpemakai_id = Yii::app()->user->id;
                    $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                    if(!$model->save())
                    {
                        $status = 'not';
                        $pesan = 'exist';
                        $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                    }

                    if(empty($pendaftaran->pembayaranpelayanan_id)){
                        $attributes = array(
                            'pasienbatalperiksa_id' => $model->pasienbatalperiksa_id,
                            'update_time' => date('Y-m-d H:i:s'),
                            'update_loginpemakai_id' => Yii::app()->user->id
                        );
                        $pendaftaran = LKPendaftaranT::model()->updateByPk($idPendaftaran, $attributes);

                        $attributes = array(
                            'pasienkirimkeunitlain_id' => $pasienMasukPenunjang->pasienkirimkeunitlain_id
                        );
                        $Perminataan_penunjang = PermintaankepenunjangT::model()->deleteAllByAttributes($attributes);
                        $pesan = 'success';
                        $status = 'ok';
                        $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                    }else{
                        $pesan = 'exist';
                        $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                        </b> sudah melakukan pembayaran pemeriksaan </div>";

                    }  
                }else{
                     /*
                     * cek data tindakan_pelayanan
                     */
                    $attributes = array(
                        'pasienmasukpenunjang_id' => $pasienMasukPenunjang->pasienmasukpenunjang_id,
                    );
//                        echo $pasienMasukPenunjang->pasienmasukpenunjang_id;exit;
                    $tindakan = LKTindakanPelayananT::model()->findAllByAttributes($attributes);
                    if(count($tindakan) > 0)
                    {
                        if(empty($tindakan->tindakansudahbayar_id)){
                            $findHasil = HasilpemeriksaanlabT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));
                                        if(empty($pendaftaran->pembayaranpelayanan_id)){
                                            $model = new PasienbatalperiksaR();
                                            $model->pendaftaran_id = $pendaftaran_id;
                                            $model->pasien_id = $pasien_id;
                                            $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                                            $model->pasienkirimkeunitlain_id = $pasienkirimkeunitlain_id;
                                            $model->tglbatal = date('Y-m-d');
                                            $model->keterangan_batal = "Batal Laboratorium";
                                            $model->create_time = date('Y-m-d H:i:s');
                                            $model->update_time = null;
                                            $model->create_loginpemakai_id = Yii::app()->user->id;
                                            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                                            if(!$model->save())
                                            {
                                                $status = 'not';
                                                $pesan = 'exist';
                                                $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                                            }
                                            $attributes = array(
                                                'statusperiksa' => 'BATAL PERIKSA',
                                                'update_time' => date('Y-m-d H:i:s'),
                                                'update_loginpemakai_id' => Yii::app()->user->id
                                            );
                                            $penunjang = PasienmasukpenunjangT::model()->updateByPk($pasienMasukPenunjang->pasienmasukpenunjang_id, $attributes);

                                            $pesan = 'success';
                                            $status = 'ok';
                                            $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                                    }else{
                                        $pesan = 'exist';
                                        $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                                        </b> sudah melakukan pembayaran pemeriksaan </div>";
                                    }                     
                        }else{
                            $pesan = 'exist';
                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                            </b> sudah melakukan pembayaran pemeriksaan </div>";
                        }
                    }else{
                        if(empty($tindakan->tindakansudahbayar_id)){
                            $findHasil = HasilpemeriksaanlabT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));
                                        $model = new PasienbatalperiksaR();
                                        $model->pendaftaran_id = $pendaftaran_id;
                                        $model->pasien_id = $pasien_id;
                                        $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                                        $model->pasienkirimkeunitlain_id = $pasienkirimkeunitlain_id;
                                        $model->tglbatal = date('Y-m-d');
                                        $model->keterangan_batal = "Batal Laboratorium";
                                        $model->create_time = date('Y-m-d H:i:s');
                                        $model->update_time = null;
                                        $model->create_loginpemakai_id = Yii::app()->user->id;
                                        $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                                        if(!$model->save())
                                        {
                                            $status = 'not';
                                            $pesan = 'exist';
                                            $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                                        }

                                        if(empty($pendaftaran->pembayaranpelayanan_id)){
                                            $attributes = array(
                                                'statusperiksa' => 'BATAL PERIKSA',
                                                'update_time' => date('Y-m-d H:i:s'),
                                                'update_loginpemakai_id' => Yii::app()->user->id
                                            );
                                            $penunjang = PasienmasukpenunjangT::model()->updateByPk($pasienMasukPenunjang->pasienmasukpenunjang_id, $attributes);

                                             $pesan = 'success';
                                             $status = 'ok';
                                             $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                                    }else{
                                        $pesan = 'exist';
                                        $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                                        </b> sudah melakukan pembayaran pemeriksaan </div>";
                                    }                      
                        }else{
                            $pesan = 'exist';
                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                            </b> sudah melakukan pembayaran pemeriksaan </div>";
                        }
                    }
                }

                /*
                 * kondisi_commit
                 */
                if($status == 'ok')
                {
                    $transaction->commit();
                }else{
                    $transaction->rollback();
                }   
            }catch(Exception $ex){
                print_r($ex);
                $status = 'not';
                $transaction->rollback();
            }

            $data['pesan'] = $pesan;
            $data['status'] = $status;
            $data['keterangan'] = $keterangan;

            echo json_encode($data);
            Yii::app()->end();
        }
    }
    public function actionbatalRujuk()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idPendaftaran = $_POST['idPendaftaran'];
            $idKirimUnit = $_POST['idKirimUnit'];
            PermintaankepenunjangT::model()->deleteAllByAttributes(array('pasienkirimkeunitlain_id'=>$idKirimUnit));
            PasienkirimkeunitlainT::model()->deleteByPk($idKirimUnit);
            $data['status'] = 'ok';
            $data['keterangan']= "<div class='flash-success'>pasien berhasil dibatalkan</div>";

            echo json_encode($data);
             Yii::app()->end();
            }
        }
}