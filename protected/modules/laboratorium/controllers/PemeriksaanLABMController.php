
<?php

class PemeriksaanLABMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
        public $pathView = 'laboratorium.views.pemeriksaanLABM.';
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render($this->pathView.'view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new LKNilaiRujukanM;
                $modPemeriksaanLab = new LKPemeriksaanLabM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
//                $modPemeriksaanLab = new SAPemeriksaanLABM;
//                            $modPemeriksaanLab->attributes=$_POST['SAPemeriksaanLABM'];
//                            $modPemeriksaanLab->pemeriksaanlab_aktif=TRUE;
//                            echo '<pre>';
//                            echo print_r($modPemeriksaanLab->attributes);exit();
//                            $modPemeriksaanLab->save();
                            
                if(isset($_POST['LKPemeriksaanLabM'])){
//                    $transcaction=Yii::app()->db->beginTransaction();
//                    try {
                            $modPemeriksaanLab = new LKPemeriksaanLabM;
                            $modPemeriksaanLab->attributes=$_POST['LKPemeriksaanLabM'];
                            $modPemeriksaanLab->pemeriksaanlab_aktif=TRUE;
                            if ($modPemeriksaanLab->save()){
//                                $jumlahJenisKelaminNilaiRujukan=COUNT($_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin']);
//
//                                for($i=0; $i<=$jumlahJenisKelaminNilaiRujukan; $i++):
//                                   if ($_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i]!='N'){
//                                       $jumlahKelompokUmur=COUNT($_POST['LKNilaiRujukanM']['kelompokumur'][''.$_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i].'']);
//                                      for($j=0; $j<=$jumlahKelompokUmur; $j++):
//                                        if($_POST['LKNilaiRujukanM']['nilairujukan_nama'][$_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i]][$j]!=''){ 
//                                            $jenisKelamin=$_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i];
//
//                                            $model=new LKNilaiRujukanM;
//                                            $model->attributes=$_POST['LKNilaiRujukanM'];
//                                            $model->nilairujukan_jeniskelamin=$_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i];
//                                            $model->pemeriksaanlab_id=$modPemeriksaanLab->pemeriksaanlab_id;
//                                            $model->kelompokumur=$_POST['LKNilaiRujukanM']['kelompokumur'][$jenisKelamin][$j];
//                                            $model->nilairujukan_min=$_POST['LKNilaiRujukanM']['nilairujukan_min'][$jenisKelamin][$j];
//                                            $model->nilairujukan_max=$_POST['LKNilaiRujukanM']['nilairujukan_max'][$jenisKelamin][$j];
//                                            $model->nilairujukan_nama=$_POST['LKNilaiRujukanM']['nilairujukan_nama'][$jenisKelamin][$j];
//                                            $model->nilairujukan_aktif=TRUE;
//                                            $model->save(); 
//                                        }
//                                     endfor;  
//                                     $j=0;
//                                }
//                                endfor;
//                                $model->save();
//                                $transcaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');  
                            $this->redirect(array('admin','id'=>$modPemeriksaanLab->pemeriksaanlab_id));

                            }
                            
//
//                    }
//                    catch (Exception $ext){
//                         $transcaction->rollback();
//                         Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ext,true));  
//                    }
                }    
			
		

		$this->render($this->pathView.'create',array(
			'model'=>$model,'modPemeriksaanLab'=>$modPemeriksaanLab
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$modPemeriksaanLab=$this->loadModel($id);
                                               
                if(isset($_POST['LKPemeriksaanLabM'])){
                 
                    $transcaction=Yii::app()->db->beginTransaction();
                    try {
                            $modPemeriksaanLab->attributes=$_POST['LKPemeriksaanLabM'];
                            $modPemeriksaanLab->pemeriksaanlab_aktif=TRUE;
                            if($modPemeriksaanLab->save()){
                                $transcaction->commit();
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');   
                                $this->redirect(array('admin','id'=>$modPemeriksaanLab->pemeriksaanlab_id));
                            }else{
                                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');   
                            }
                    }
                    catch (Exception $ext){
                         $transcaction->rollback();
                         Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ext,true));  
                    }
                }  
                
		$this->render($this->pathView.'update',array(
			'modPemeriksaanLab'=>$modPemeriksaanLab
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('LKPemeriksaanLabM');
		$this->render($this->pathView.'index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new LKPemeriksaanLabM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['LKPemeriksaanLabM']))
		{
			$model->attributes=$_GET['LKPemeriksaanLabM'];
			$model->daftartindakan_nama=$_GET['LKPemeriksaanLabM']['daftartindakan_nama'];
		}


		$this->render($this->pathView.'admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=LKPemeriksaanLabM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sapemeriksaan-labm-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionDelete()
	{
		//if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		if(Yii::app()->request->isPostRequest)
		{
			$id = $_POST['id'];
            $this->loadModel($id)->delete();
            if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                        ));
                    exit;
                }
	                    
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

     /**
     *Mengubah status aktif
     * @param type $id 
     */
    public function actionRemoveTemporary()
    {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                    SAPropinsiM::model()->updateByPk($id, array('propinsi_aktif'=>false));
//                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
              
        
        $id = $_POST['id'];   
        if(isset($_POST['id']))
        {
           $update = PemeriksaanlabM::model()->updateByPk($id,array('pemeriksaanlab_aktif'=>false));
           if($update)
            {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
             }
        } else {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
        }

    }
        
        public function actionPrint()
        {
            $model= new LKPemeriksaanLabM;
            $model->unsetAttributes();  // clear any default values
			if(isset($_GET['LKPemeriksaanLabM']))
			{
				$model->attributes=$_GET['LKPemeriksaanLabM'];
				$model->daftartindakan_nama=$_GET['LKPemeriksaanLabM']['daftartindakan_nama'];
			}
			// echo "</prev>";
			// print_r($model->attributes);
			// exit();
			
            $judulLaporan='Data Pemeriksaan Lab';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
