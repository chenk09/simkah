<?php
/**
* modules/laboratorium/controllers.InputPemeriksaanController.php
* Updated by    : Hardi
* Date          : 21-04-2014
* Issue         : EHJ-EHJ-1578
* Deskripsi     : Update nama_perujuk dan rujukandari_id id tabel rujukan_t
**/
?>

<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class InputPemeriksaanController extends SBaseController
{
//        public $successSave = false;
        public $successSaveTindakan = false;
        public $successUpdateTindakan = true; // di set true karna akan di looping dengan '&&'
//        public $successSavepasienKirimKeUnitLain=false;
        public $successSaveHasilPemeriksaan=false;
        public $successUpdateHasilPemeriksaan=true; // di set true karna akan di looping dengan '&&'
        public $successUpdateHasilPemeriksaanDet=true; // di set true karna akan di looping dengan '&&'
        public $successSaveBiayaAdministrasi = false;
        public $is_succes = true;
//        public $successSavePasienMasukPenunjang=false;
        
	public function actionIndex($idPendaftaran,$idPasienMasukPenunjang)
	{
            $modPendaftaran = LKPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modPeriksaLab = LKPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true));
            $modPermintaan = LKPermintaanKePenunjangT::model()->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            $modJenisPeriksaLab = LKJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan'));
            
            if(!empty($_POST['pemeriksaanLab'])){
                $transaction = Yii::app()->db->beginTransaction();
                try{
                        $modPasienMasukPenunjang = LKPasienMasukPenunjangT::model()->findByPk($idPasienMasukPenunjang);
//                        $this->saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$_POST['PemeriksaanLab']);
                        PendaftaranPasienLuarController::saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$_POST['PemeriksaanLab']);
//                        $this->saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$_POST['LKDetailHasilPemeriksaanLabT']);
    
                        if ($this->successSaveTindakan && $this->successSaveHasilPemeriksaan){
//                            LKPasienKirimKeUnitLainT::model()->updateByPk($idPasienKirimKeUnitLain, array('pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id));
                            //set null pembayaran supaya muncul di informasi belum bayar
                            PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id, 
                                array('pembayaranpelayanan_id'=>null)
                            );
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Berhasil Disimpan");
                            $this->redirect($this->createUrl('DaftarPasien/index',array('modulId'=>Yii::app()->session['modulId'])));
                        } else {
                            $transaction->rollback();
//                            Yii::app()->user->setFlash('error',"Data gagal disimpan");
                            if (!($this->successSaveTindakan)) Yii::app()->user->setFlash('error',"Data Tindakan Gagal Disimpan !");
                            if (!($this->successSaveHasilPemeriksaan)) Yii::app()->user->setFlash('error',"Data Pemeriksaan Gagal Disimpan !");
                        }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
                        
            $this->render('index',array('modPeriksaLab'=>$modPeriksaLab,
                                        'modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modPermintaan'=>$modPermintaan,
                                        'modJenisPeriksaLab'=>$modJenisPeriksaLab));
	}
        /**
         * actionUpdate untuk update pemeriksaan yg telah di input di pendaftaran pasien luar (lab)
         * @author ichan | Ihsan Fauzi Rahman <ichan90@yahoo.co.id>
         * @param type $idPendaftaran
         * @param type $idPasienMasukPenunjang
         */
        
        public function actionUpdate($idPendaftaran,$idPasienMasukPenunjang)
	{
            $modPendaftaran = LKPendaftaranT::model()->with('kasuspenyakit')->findByPk(
                $idPendaftaran
            );
            
            if(!empty($modPendaftaran->pasienadmisi_id)){
                $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                if(!empty($modPasienAdmisi->pasienpulang_id)){
                    $status = "SUDAH PULANG";
                }
            }else{
                $status = $modPendaftaran->statusperiksa;
            }
            
            if($status == "SUDAH PULANG"){
                echo "<script>
                        alert('Maaf, status pasien SUDAH PULANG tidak bisa menambahkan tindakan transaksi. ');
                        window.top.location.href='".Yii::app()->createUrl('laboratorium/DaftarPasien/index')."';
                    </script>";
            }
            $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $rujukanM = LKRujukanT::model()->findByPk($modPendaftaran->rujukan_id);
            $modPasienMasukPenunjang = LKPasienMasukPenunjangT::model()->findByPk(
                $idPasienMasukPenunjang
            );
            $format = new CustomFormat();

            $modPeriksaLab = LKPemeriksaanLabM::model()->findAll(
                'pemeriksaanlab_id NOT IN (351,353,354,370,371,374,375,471,472,473,474,475,476)' //pemeriksaanlab yang tidak ditampilkan karena paket
            );
//            $modPeriksaLab = LKPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true));
            $modPermintaan = LKPermintaanKePenunjangT::model()->findAllByAttributes(
                array(
                    'pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain
                )
            );
            $modJenisPeriksaLab = LKJenisPemeriksaanLabM::model()->findAllByAttributes(
                array(
                    'jenispemeriksaanlab_aktif'=>true
                ),
                array(
                    'order'=>'jenispemeriksaanlab_urutan'
                )
            );
            $modHasilPeriksa = LKHasilPemeriksaanLabT::model()->findByAttributes(
                array(
                    'pendaftaran_id'=>$idPendaftaran, 
                    'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang
                )
            );
            $modHasilPeriksaDetails = DetailhasilpemeriksaanlabT::model()->findAllByAttributes(
                array(
                    'hasilpemeriksaanlab_id'=>$modHasilPeriksa->hasilpemeriksaanlab_id
                )
            );
            $modTindakanPelayan= TindakanpelayananT::model()->findByAttributes(
                array(
                    'pendaftaran_id'=>$idPendaftaran, 
                    'pasien_id'=>$modPendaftaran->pasien_id, 
                    'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang, 
                    'daftartindakan_id'=>2003
                )
            );
            
            $modTindakanPelayan->tgl_tindakan = $format->formatDateTimeMediumForDB($modTindakanPelayan->tgl_tindakan);
            $modTindakanPelayan->create_time = date('Y-m-d H:i:s');
            $modTindakanPelayan->update_time = date('Y-m-d H:i:s');
            
            // echo $tgl;exit();
            //Menandai LKPemeriksaanLabM yang sudah ada (isChecked) di DetailhasilpemeriksaanlabT
            foreach($modHasilPeriksaDetails as $i => $dataHasil)
            {
                foreach($modPeriksaLab as $j => $dataPeriksa)
                {
                    if($dataHasil->pemeriksaanlab_id == $dataPeriksa->pemeriksaanlab_id)
                    {
                        $dataPeriksa->isChecked = true;
                    }
                }
            }
            
            $modKirimKeUnitLain = PasienkirimkeunitlainT::model()->findByAttributes(
                array(
                    'pendaftaran_id'=>$idPendaftaran,
                )
            );
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            if(isset($_POST['RekeningakuntansiV'])){
                $postRekenings = $_POST['RekeningakuntansiV'];
            }
            if(empty($modPendaftaran->pembayaranpelayanan_id)){
                $status = false;
            }else{
                $status = true;
            }
            if(isset($_POST['pemeriksaanLab']))
            {
                $transaction = Yii::app()->db->beginTransaction();
                try{
                        //*** START dipakai untuk merubah dokter perujuk dari ruangan jika tidak sesuai dengan blangko lab
                        if(isset($_POST['PasienkirimkeunitlainT']['pegawai_id']) && !empty($_POST['PasienkirimkeunitlainT']['pegawai_id']))
                        {
                            $modKirimKeUnitLain->pegawai_id = $_POST['PasienkirimkeunitlainT']['pegawai_id'];
                            $modKirimKeUnitLain->save();
                        }
                        
                        //*** START dipakai untuk merubah dokter perujuk dari ruangan jika tidak sesuai dengan blangko lab > PASIEN LUAR
                        if(isset($_POST['LKRujukanT']))
                        {
                            $rujukanM->nama_perujuk = $_POST['LKRujukanT']['nama_perujuk'];
                            $rujukandariM = RujukandariM::model()->findByAttributes(
                                array(
                                    'namaperujuk'=>$_POST['LKRujukanT']['nama_perujuk'],
                                )
                            );

                            $rujukanM->rujukandari_id = $rujukandariM->rujukandari_id;

                            $rujukanM->save();
                        }
                        //*** END -------------------------------------------------------
                        
                        
                        //inisialisasi array
                        $data_pemeriksaan = array();
                        foreach ($_POST['PemeriksaanLab'] as $key=>$val)
                        {
                            foreach ($val as $k=>$v)
                            {
                                $id_ = $v['daftartindakan_id'];
                                $data_pemeriksaanLab[$id_] = $v['pemeriksaanlab_id'];
                            }
                        }

                        //update biaya administrasi
                        if(isset($_POST['biayaAdministrasi'])){

                            $updateBiayaAdministrasi = TindakanpelayananT::model()->findByAttributes(array(
                                                        'pendaftaran_id'=>$modPendaftaran->pendaftaran_id,
                                                        'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id,
                                                        'daftartindakan_id'=>2003,
                                                    ),
                                                    'tindakansudahbayar_id IS NULL'
                                                );
                             $updateBiayaAdministrasi->tgl_tindakan = $format->formatDateTimeMediumForDB($updateBiayaAdministrasi->tgl_tindakan);
                            $updateBiayaAdministrasi->create_time = date('Y-m-d H:i:s');
                            $updateBiayaAdministrasi->update_time = date('Y-m-d H:i:s');

                            // echo "<pre>"; print_r($updateBiayaAdministrasi->attributes);exit();

                            if(isset($updateBiayaAdministrasi)){

                                $updateBiayaAdministrasi->tarif_satuan = $_POST['biayaAdministrasi'];
                                $updateBiayaAdministrasi->qty_tindakan = 1;
                                $updateBiayaAdministrasi->tarif_tindakan = $updateBiayaAdministrasi->qty_tindakan * $updateBiayaAdministrasi->tarif_satuan;
                                $updateBiayaAdministrasi->cyto_tindakan = false;
                                $updateBiayaAdministrasi->update();
                            }
                        }
                        $ceTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(
                            array(
                                'pendaftaran_id'=>$modPendaftaran->pendaftaran_id,
                                'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id
                            ),
                            'tindakansudahbayar_id IS NULL AND daftartindakan_id <> 2003 AND ruangan_id = 18'
                        );

                        
                        if(count($_POST['pemeriksaanLab']) < count($ceTindakanPelayanan) && count($_POST['pemeriksaanLab']) != 0)
                        {
                            
                            foreach ($ceTindakanPelayanan as $key=>$val)
                            {
                                if(!array_key_exists($val->daftartindakan_id, $data_pemeriksaanLab))
                                {
                                    $ceDetPemeriksaan = DetailhasilpemeriksaanlabT::model()->findAllByAttributes(
                                        array(
                                            'tindakanpelayanan_id'=>$val->tindakanpelayanan_id
                                        )
                                    );
                                    $is_exist = TRUE;
                                    foreach ($ceDetPemeriksaan as $k_=>$v_)
                                    {
                                        if(strlen(trim($v_->hasilpemeriksaan)) > 0)
                                        {
                                            $is_exist = FALSE;
                                            break;
                                        }
                                    }
                                    
                                    if($is_exist)
                                    {
                                       
                                        DetailhasilpemeriksaanlabT::model()->deleteAll(
                                            'tindakanpelayanan_id = ' . $val->tindakanpelayanan_id
                                        );
                                        $ceTindakanPelayanan[$key]->delete();
                                    }else{
                                       
                                        $this->is_succes = FALSE;
                                        Yii::app()->user->setFlash('error',"Data tidak bisa di hapus, hasil sudah terisi !!");
                                    } 
                                                                    
                                    /*
                                    if(strlen(trim($ceDetPemeriksaan->nilairujukan)) == 0)
                                    {
                                        $ceDetPemeriksaan->delete();
                                        $val->delete();
                                        $ceTindakanPelayanan[$key]->delete();
                                    }else{
                                        $this->is_succes = FALSE;
                                        Yii::app()->user->setFlash('error',"Data tidak bisa di hapus, hasil sudah terisi !!");
                                    }
                                     * 
                                     */
                                   
                                }
                              
                            }
                        }else{
                            
                            if(count($_POST['pemeriksaanLab']) > count($ceTindakanPelayanan))
                            {
                                $data_pelayanan = array();
                                foreach ($ceTindakanPelayanan as $key=>$val)
                                {
                                    $rows = $val->daftartindakan_id;
                                    $data_pelayanan[$rows] = 'yes';
                                }
                                
                                foreach($data_pemeriksaanLab as $key=>$val)
                                {
                                    if(!array_key_exists($key, $data_pelayanan))
                                    {
                                        $modTindakanPelayan = New LKTindakanPelayananT;
                                        $modTindakanPelayan->penjamin_id = $modPendaftaran->penjamin_id;
                                        $modTindakanPelayan->pasien_id = $modPasien->pasien_id;
                                        $modTindakanPelayan->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                                        $modTindakanPelayan->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                                        $modTindakanPelayan->instalasi_id = Params::INSTALASI_ID_LAB;
                                        $modTindakanPelayan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                                        $modTindakanPelayan->shift_id = Yii::app()->user->getState('shift_id');
                                        $modTindakanPelayan->daftartindakan_id = $key;
                                        $modTindakanPelayan->carabayar_id = $modPendaftaran->carabayar_id;
                                        $modTindakanPelayan->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
                                        $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
                                        $modTindakanPelayan->tarif_satuan = $_POST['PemeriksaanLab']['PATOLOGI KLINIK'][$val]['tarif_tindakan'];
                                        $modTindakanPelayan->qty_tindakan = $_POST['PemeriksaanLab']['PATOLOGI KLINIK'][$val]['qty_tindakan'];
                                        $modTindakanPelayan->tarif_tindakan = $_POST['PemeriksaanLab']['PATOLOGI KLINIK'][$val]['tarif_tindakan'];
                                        $modTindakanPelayan->satuantindakan = Params::SATUAN_TINDAKAN_PENDAFTARAN;
                                        $modTindakanPelayan->cyto_tindakan = 0;
                                        $modTindakanPelayan->tarifcyto_tindakan = 0;
                                        $modTindakanPelayan->dokterpemeriksa1_id = $modPasienMasukPenunjang->pegawai_id;
                                        $modTindakanPelayan->dokterpemeriksa2_id = isset($_POST['PasienkirimkeunitlainT']['pegawai_id']) ? $_POST['PasienkirimkeunitlainT']['pegawai_id'] : null;
                                        $modTindakanPelayan->discount_tindakan = 0;
                                        $modTindakanPelayan->subsidiasuransi_tindakan = 0;
                                        $modTindakanPelayan->subsidipemerintah_tindakan = 0;
                                        $modTindakanPelayan->subsisidirumahsakit_tindakan = 0;
                                        $modTindakanPelayan->iurbiaya_tindakan = 0;
                                        $modTindakanPelayan->ruangan_id = Yii::app()->user->getState('ruangan_id');

                                        $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(
                                            array(
                                                'daftartindakan_id'=>$modTindakanPelayan->daftartindakan_id, 
                                                'kelaspelayanan_id'=>$modTindakanPelayan->kelaspelayanan_id
                                            )
                                        );
                                        foreach($tarifTindakan AS $dataTarif):
                                            if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS)
                                            {
                                                $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                                            }
                                            if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS)
                                            {
                                                $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                                            }
                                            if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS)
                                            {
                                                $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                                            }
                                            if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP)
                                            {
                                                $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                                            }
                                        endforeach;

                                        if($modTindakanPelayan->save())
                                        {
                                            
                                            $tindakanKomponen= LKTarifTindakanM::model()->findAll(
                                                'daftartindakan_id = '.$modTindakanPelayan->daftartindakan_id.' AND kelaspelayanan_id = '.$modTindakanPelayan->kelaspelayanan_id
                                            );
                                            $jumlahKomponen = count($tindakanKomponen);
                                            foreach ($tindakanKomponen AS $tampilKomponen):
                                                    $modTindakanKomponen = new LKTindakanKomponenT;
                                                    $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                                                    $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                                                    $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                                                    $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                                                    $modTindakanKomponen->tarifcyto_tindakankomp=0;
                                                    $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                                                    $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                                                    $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                                                    $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                                                    if(!$modTindakanKomponen->save())
                                                    {
                                                        Yii::app()->user->setFlash('error',"Data komponen pelayanan gagal disimpan");
                                                        $this->is_succes = false;
                                                        exit;
                                                    }
                                            endforeach;
                                            
                                            if(isset($postRekenings)){
                                                $modJurnalRekening = TindakanController::saveJurnalRekening();
                                                //update jurnalrekening_id
                                                $modTindakanPelayan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                                                $modTindakanPelayan->save();
                                                $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanPelayan, 'tm');
                                                $this->is_succes = $saveDetailJurnal;
                                            }
                                            
                                            $modHasilPemeriksaan = LKHasilPemeriksaanLabT::model()->findByAttributes(
                                                array(
                                                    'pendaftaran_id'=>$modPendaftaran->pendaftaran_id,
                                                    'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id
                                                )
                                            );
                                            $modPemeriksaanLab = PemeriksaanlabM::model()->findAllByAttributes(array(
                                                'daftartindakan_id'=>$modTindakanPelayan->daftartindakan_id
                                            ));
                                            foreach($modPemeriksaanLab as $detailPemeriksaanLab){
                                                $details = PemeriksaanlabdetM::model()->pemeriksaanDetail($modHasilPemeriksaan, $detailPemeriksaanLab['pemeriksaanlab_id']);
                                               // BYPASS CEK NILAI RUJUKAN
                                               // if(!$details)
                                               // {
                                               //     Yii::app()->user->setFlash('error',"Ada data yang tidak memiliki nilai rujukan !");
                                               //     $this->is_succes = false;
                                               //     exit;
                                               // }  else {
                                                    foreach ($details as $k => $detail)
                                                    {
                                                        $modDetailHasilPemeriksaan = new LKDetailHasilPemeriksaanLabT;
                                                        $modDetailHasilPemeriksaan->hasilpemeriksaanlab_id = $modHasilPemeriksaan->hasilpemeriksaanlab_id;
                                                        $modDetailHasilPemeriksaan->pemeriksaanlabdet_id = $detail->pemeriksaanlabdet_id;
                                                        $modDetailHasilPemeriksaan->pemeriksaanlab_id = $val;
                                                        $modDetailHasilPemeriksaan->tindakanpelayanan_id = $modTindakanPelayan->tindakanpelayanan_id;
                                                        $modDetailHasilPemeriksaan->update_time = date('Y-m-d H:i:s');
                                                        $modDetailHasilPemeriksaan->update_loginpemakai_id = Yii::app()->user->id;                                                    
                                                        if(!$modDetailHasilPemeriksaan->save())
                                                        {
                                                            Yii::app()->user->setFlash('error',"Ada data yang tidak memiliki nilai rujukan !");
                                                            $this->is_succes = false;
                                                            exit;
                                                        }
                                                        //update relasi tindakanpelayanan_t -> detailhasilpemeriksaanlab_t
                                                        //DICOMMENT KARENA 1 TINDAKAN BISA MEMILIKI BEBERAPA DETAIL HASIL LAB >> TindakanpelayananT::model()->updateByPk($modDetailHasilPemeriksaan->tindakanpelayanan_id, array('detailhasilpemeriksaanlab_id'=>$modDetailHasilPemeriksaan->detailhasilpemeriksaanlab_id));
                                                    }                                                
                                               // }
                                            }
                                        }else{
                                            Yii::app()->user->setFlash('error',"Data pelayanan tindakan gagal disimpan");
                                            $this->is_succes = false;
                                            exit;
                                        }
                                    }
                                }
                            }
                        }
                        
                        if ($this->is_succes)
                        {
                            // echo '<pre>'; print_r($modTindakanPelayan->attributes);exit();

                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Berhasil Disimpan");
                            $this->redirect(
                                $this->createUrl(
                                    'inputPemeriksaan/update',
                                    array(
                                        'idPendaftaran'=>$idPendaftaran,
                                        'idPasienMasukPenunjang'=>$idPasienMasukPenunjang
                                    )
                                )
                            );
                        }
                }
                catch(Exception $exc){
                    // echo '<pre>'; print_r($modTindakanPelayan->attributes);exit();

                        // echo"<pre>";
                        // print_r($modHasilPemeriksaan->attributes);
                        // exit();
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
                        
            $this->render('update',
                array(
                    'modPeriksaLab'=>$modPeriksaLab,
                    'modPendaftaran'=>$modPendaftaran,
                    'modPasien'=>$modPasien,
                    'rujukanM'=>$rujukanM,
                    'modHasilPeriksa'=>$modHasilPeriksa,
                    'modHasilPeriksaDetails'=>$modHasilPeriksaDetails,
                    'modJenisPeriksaLab'=>$modJenisPeriksaLab,
                    'modKirimKeUnitLain'=>$modKirimKeUnitLain,
                    'modTindakanPelayan'=>$modTindakanPelayan,
                    'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                    'modRekenings'=>$modRekenings,
                    'status'=>$status,
                )
            );
	}
        
        /**
         * updateHasilPemeriksaan untuk transaksi update pemeriksaan
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         * @param type $modPasienMasukPenunjang
         * @param type $modPendaftaran
         * @param type $modPasien
         * @param type $pemeriksaanLab
         */
        
        protected function updateHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$pemeriksaanLab)
        {
            foreach($pemeriksaanLab as $jenisKelompok => $value) {
                if($jenisKelompok == Params::LAB_PATOLOGI){
                    $modHasilPemeriksaan = LKHasilPemeriksaanLabT::model()->findByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id, 'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id));
                    $modHasilPemeriksaan->update_time = date('Y-m-d H:i:s');
                    $modHasilPemeriksaan->update_loginpemakai_id = Yii::app()->user->id;
                    if($modHasilPemeriksaan->update()){
                        $this->successUpdateHasilPemeriksaan = $this->successUpdateHasilPemeriksaan && true;
                        $daftartindakan_id = "";
                        foreach ($value as $pemeriksaanId => $dataPeriksa)
                        {
                            if($dataPeriksa['daftartindakan_id'] == Params::PAKET_LAB){
                                if($daftartindakan_id != $dataPeriksa['daftartindakan_id'])
                                {
                                    $modTindakanPelayananT = $this->updateTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                                    $daftartindakan_id = $dataPeriksa['daftartindakan_id'];
                                }
                            }else{
                                $modTindakanPelayananT = $this->updateTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                            }
                            
                            $details = PemeriksaanlabdetM::model()->pemeriksaanDetail($modHasilPemeriksaan, $pemeriksaanId);
                            
                            if(!$details){ //Jika tidak memiliki nilai rujukan
                                $this->successUpdateHasilPemeriksaanDet = false;
                                Yii::app()->user->setFlash('error',"Ada data yang tidak memiliki nilai rujukan !");
                            }
                            foreach ($details as $k => $detail) {
                                $modDetailHasilPemeriksaan = DetailhasilpemeriksaanlabT::model()->findByAttributes(array('hasilpemeriksaanlab_id'=>$modHasilPemeriksaan->hasilpemeriksaanlab_id, 'pemeriksaanlab_id'=>$pemeriksaanId, 'pemeriksaanlabdet_id'=>$detail->pemeriksaanlabdet_id, 'tindakanpelayanan_id'=>$modTindakanPelayananT->tindakanpelayanan_id));
                                if(empty($modDetailHasilPemeriksaan->detailhasilpemeriksaanlab_id)){ //jika data tidak ditemukan di DetailhasilpemeriksaanlabT
                                    $modDetailHasilPemeriksaan = new LKDetailHasilPemeriksaanLabT;
                                    $modDetailHasilPemeriksaan->hasilpemeriksaanlab_id = $modHasilPemeriksaan->hasilpemeriksaanlab_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlabdet_id = $detail->pemeriksaanlabdet_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlab_id = $pemeriksaanId;
                                    $modDetailHasilPemeriksaan->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                                }
                                $modDetailHasilPemeriksaan->update_time = date('Y-m-d H:i:s');
                                $modDetailHasilPemeriksaan->update_loginpemakai_id = Yii::app()->user->id;
                                if($modDetailHasilPemeriksaan->save()){ //Jika Detail Pemeriksaan berhasil Disimpan Ke Hasil Pemeriksaan
                                    $this->successUpdateHasilPemeriksaanDet = $this->successUpdateHasilPemeriksaanDet && true;
                                    //update tindakanpelayanan_t
                                    
                                }else{ //Jika Ada yang tidak disimpan
                                    $this->successUpdateHasilPemeriksaanDet = false;
                                    Yii::app()->user->setFlash('error',"Data Detail Hasil Pemeriksaan ID=".$pemeriksaanId." gagal disimpan");
                                }
                            }
                        }
                    } else 
                        $this->successUpdateHasilPemeriksaan = false;

                } else {
                    //echo 'Anatomi <pre>'.print_r($value,1).'</pre>';
                    foreach ($value as $pemeriksaanId => $dataPeriksa) {
                        $modTindakanPelayananT = $this->updateTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                        $modHasilPemeriksaanPA = new LKHasilPemeriksaanPAT;
                        $modHasilPemeriksaanPA->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                        $modHasilPemeriksaanPA->pemeriksaanlab_id = $pemeriksaanId;
                        $modHasilPemeriksaanPA->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modHasilPemeriksaanPA->pasien_id = $modTindakanPelayananT->pasien_id;
                        $modHasilPemeriksaanPA->pendaftaran_id = $modTindakanPelayananT->pendaftaran_id;
                        $modHasilPemeriksaanPA->nosediaanpa = Generator::noSediaanPA();
                        $modHasilPemeriksaanPA->tglperiksapa = $modTindakanPelayananT->tgl_tindakan;
                        if($modHasilPemeriksaanPA->validate()){
                            if($modHasilPemeriksaanPA->save()){
                                 $this->successUpdateHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
                            }else{
                                 $this->successUpdateHasilPemeriksaan = false;                   
                            }  
                        }
                        
                    }
                }
            }
            
        }
        
        protected function updateTindakanPelayanan($modPendaftaran,$modPasien,$dataPemeriksaan, $modPasienMasukPenunjang)
        {       
            $modTindakanPelayananT = new LKTindakanPelayananT();    
            $modTindakanPelayananT = LKTindakanPelayananT::model()->findByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id,'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id,'daftartindakan_id'=>$dataPemeriksaan['daftartindakan_id']));
            if(empty($modTindakanPelayananT->tindakanpelayanan_id)){ //Jika daftartindakan_id tidak ditemukan di LKTindakanPelayananT
                $modTindakanPelayananT = new LKTindakanPelayananT;
                $modTindakanPelayananT->daftartindakan_id = $dataPemeriksaan['daftartindakan_id']; //daftar tindakan unik
            }
                $modTindakanPelayananT->penjamin_id = $modPendaftaran->penjamin_id;
                $modTindakanPelayananT->pasien_id = $modPasien->pasien_id;
                $modTindakanPelayananT->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                $modTindakanPelayananT->instalasi_id = Params::INSTALASI_ID_LAB;
                $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                $modTindakanPelayananT->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayananT->carabayar_id = $modPendaftaran->carabayar_id;
                $modTindakanPelayananT->qty_tindakan = $dataPemeriksaan['qty_tindakan'];
                $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayananT->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayananT->tarif_satuan = $dataPemeriksaan['tarif_tindakan'];
                $modTindakanPelayananT->tarif_tindakan = $modTindakanPelayananT->tarif_satuan * $modTindakanPelayananT->qty_tindakan;
                $modTindakanPelayananT->satuantindakan = $dataPemeriksaan['satuantindakan'];
                $modTindakanPelayananT->cyto_tindakan = $dataPemeriksaan['cyto_tindakan'];
                $cyto_tindakan = $modTindakanPelayananT->cyto_tindakan;
                $modTindakanPelayan->dokterpemeriksa1_id = $modPasienMasukPenunjang->pegawai_id;
				$modTindakanPelayan->dokterpemeriksa2_id = isset($_POST['PasienkirimkeunitlainT']['pegawai_id']) ? $_POST['PasienkirimkeunitlainT']['pegawai_id'] : null;
                $modTindakanPelayananT->discount_tindakan = 0;
                $modTindakanPelayananT->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                $modTindakanPelayananT->subsidiasuransi_tindakan = 0;
                $modTindakanPelayananT->subsidipemerintah_tindakan = 0;
                $modTindakanPelayananT->subsisidirumahsakit_tindakan = 0;
                $modTindakanPelayananT->iurbiaya_tindakan = 0;
                $modTindakanPelayananT->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayananT->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                
                if($cyto_tindakan){
                    $modTindakanPelayananT->tarifcyto_tindakan = $dataPemeriksaan['tarif_cyto'];;
                }else{
                    $modTindakanPelayananT->tarifcyto_tindakan = 0;
                }
            
                $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$dataPemeriksaan['daftartindakan_id'], 'kelaspelayanan_id'=>$modTindakanPelayananT->kelaspelayanan_id));
                
                
                foreach($tarifTindakan as $dataTarif):
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                           $modTindakanPelayananT->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                    }
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                       $modTindakanPelayananT->tarif_medis=$dataTarif['harga_tariftindakan'];
                    }
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                       $modTindakanPelayananT->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                    }
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                       $modTindakanPelayananT->tarif_bhp=$dataTarif['harga_tariftindakan'];
                   }
                endforeach;
            
                if($modTindakanPelayananT->save()){//Jika Tindakan Pelayanan Berhasil tersimpan
                    $idPeriksaLab = $dataPemeriksaan['pemeriksaanlab_id'];
                    $jumlahTindKomponen = COUNT($_POST['LKTindakanKomponenT'][$idPeriksaLab]);
                    for($j=0; $j<$jumlahTindKomponen; $j++):
                        $modTindakanKomponen = LKTindakanKomponenT::model()->findByAttributes(array('tindakanpelayanan_id'=>$modTindakanPelayananT->tindakanpelayanan_id));
                        if($modTindakanKomponen->komponentarif_id != $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['komponentarif_id']){ //jika belum ada komponentarif_id
                            $modTindakanKomponen = new LKTindakanKomponenT;
                            $modTindakanKomponen->komponentarif_id = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['komponentarif_id'];
                        }
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->tarif_tindakankomp = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'] * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $modTindakanPelayananT->tarif_tindakan;
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                    endfor;

                    $this->successUpdateTindakan = $this->successUpdateTindakan && true;
                } else {
                    $this->successUpdateTindakan = false;
                }

                return $modTindakanPelayananT;
        }
		
		public function saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienMasukPenunjang, $tarifKarcisLab)
        {
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New LKTindakanPelayananT;
            $modTindakanPelayan->penjamin_id=$modPendaftaran->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
            $modTindakanPelayan->pasienmasukpenunjang_id=$modPasienMasukPenunjang->pasienmasukpenunjang_id;
            $modTindakanPelayan->kelaspelayanan_id=$modPendaftaran->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id = Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id = Params::TARIF_ADMINISTRASI_LAB_ID;
            $modTindakanPelayan->carabayar_id = $modPendaftaran->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan = $tarifKarcisLab;
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$modPendaftaran->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            $modTindakanPelayan->ruangan_id = $modPendaftaran->ruangan_id;
             
            $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$modTindakanPelayan->daftartindakan_id, 'kelaspelayanan_id'=>$modTindakanPelayan->kelaspelayanan_id));
            foreach($tarifTindakan AS $dataTarif):
                 if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                        $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                        $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                        $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                        $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                    }
            endforeach;
            
            if($modTindakanPelayan->save())
            { 
                   $tindakanKomponen= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayan->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                   $jumlahKomponen=COUNT($tindakanKomponen);

                   foreach ($tindakanKomponen AS $tampilKomponen):
                           $modTindakanKomponen=new LKTindakanKomponenT;
                           $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                           $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                           $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                           $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                           $modTindakanKomponen->tarifcyto_tindakankomp=0;
                           $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                           $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                           $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                           $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                           if($modTindakanKomponen->save()){
                               $cekTindakanKomponen++;
                           }
                   endforeach;
                   if($cekTindakanKomponen!=$jumlahKomponen){
                          $this->successSaveBiayaAdministrasi=false;
                    }else
                        $this->successSaveBiayaAdministrasi=true;
           } 
           
        }
		
        /**
         * hapusPemeriksaanUnchecked untuk memncari dan menghapus pemeriksaan yang di uncheck
         */
        protected function hapusPemeriksaanUnchecked($modPendaftaran, $modPasienMasukPenunjang, $postUpdate){
            $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(
                array(
                    'pendaftaran_id'=>$modPendaftaran->pendaftaran_id,
                    'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id
                )
            );
            foreach($modTindakanPelayanan as $i => $mod)
            {
                $adaDipost = false;
                foreach($postUpdate['PATOLOGI KLINIK'] AS $i => $val)
                { 
                    // bandingkan dengan daftartindakan_id yang di post
                    if($mod->daftartindakan_id == $val['daftartindakan_id']){
                        $adaDipost = true;
                    }
                }
                
                if(!$adaDipost)
                {
                    //jika tidak ada di post
                    $modHasilPemeriksaan = HasilpemeriksaanlabT::model()->findByAttributes(
                        array(
                            'pendaftaran_id'=>$modPendaftaran->pendaftaran_id, 
                            'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id
                        )
                    );
                    
                    //hapus detail pemeriksaan
                    $modDetailHasilPemeriksaan = DetailhasilpemeriksaanlabT::model()->findByAttributes(
                        array(
                            'hasilpemeriksaanlab_id'=>$modHasilPemeriksaan->hasilpemeriksaanlab_id,
                            'tindakanpelayanan_id'=>$mod->tindakanpelayanan_id
                        )
                    );
                    DetailhasilpemeriksaanlabT::model()->updateAll(
                        array(
                            'tindakanpelayanan_id' =>null
                        ),
                        'hasilpemeriksaanlab_id = '. $modHasilPemeriksaan->hasilpemeriksaanlab_id
                    );
                    $deleteHasilPemeriksaanDet = DetailhasilpemeriksaanlabT::model()->deleteAll(
                        'hasilpemeriksaanlab_id = '.$modHasilPemeriksaan->hasilpemeriksaanlab_id
                    );
                    
                    //hapus hasil pemeriksaan
                    if($deleteHasilPemeriksaanDet)
                    {
//                        $deleteHasilPemeriksaan = HasilpemeriksaanlabT::model()->deleteAll('pendaftaran_id = '.$modPendaftaran->pendaftaran_id.' AND pasienmasukpenunjang_id = '.$modPasienMasukPenunjang->pasienmasukpenunjang_id.' AND hasilpemeriksaanlab_id = '.$modDetailHasilPemeriksaan->hasilpemeriksaanlab_id);
//                        //hapus tindakan pelayanan
//                        if($deleteHasilPemeriksaan){
//                        $deleteTindakanPelayanan = 
                            TindakanpelayananT::model()->deleteAll(
                                'pendaftaran_id = '.$modPendaftaran->pendaftaran_id.' AND pasienmasukpenunjang_id = '.$modPasienMasukPenunjang->pasienmasukpenunjang_id.' AND tindakanpelayanan_id = '.$mod->tindakanpelayanan_id.' AND daftartindakan_id = '.$mod->daftartindakan_id
                            );
//                        }
                    }
                }
            }
        }
        //== end by ichan
        /* gunakan fungsi yang di PendaftaranPasienLuarController
        protected function saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$pemeriksaanLab)
        {
            $this->successSaveTindakan = true;
            foreach($pemeriksaanLab as $jenisKelompok => $value) {
                if($jenisKelompok == Params::LAB_PATOLOGI){
                    //echo 'Patologi Klinik <pre>'.print_r($value,1).'</pre>';
                    $modHasilPemeriksaan = new LKHasilPemeriksaanLabT;
                    $modHasilPemeriksaan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modHasilPemeriksaan->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                    $modHasilPemeriksaan->pasien_id = $modPasien->pasien_id;
                    $modHasilPemeriksaan->nohasilperiksalab = Generator::noHasilPemeriksaanLK();
                    $modHasilPemeriksaan->tglhasilpemeriksaanlab = date('Y-m-d H:i:s');
                    $modHasilPemeriksaan->hasil_kelompokumur = Generator::kelompokUmurNama($modPasien->tanggal_lahir);
                    $modHasilPemeriksaan->hasil_jeniskelamin = $modPasien->jeniskelamin;
                    $modHasilPemeriksaan->statusperiksahasil = Params::statusPeriksaLK(1);
                    //$modHasilPemeriksaan->printhasillab = null;
                    if($modHasilPemeriksaan->save()){
                        $daftartindakan_id = "";
                        foreach ($value as $pemeriksaanId => $dataPeriksa){
                            if($dataPeriksa['daftartindakan_id'] == Params::PAKET_LAB){
                                    if($daftartindakan_id != $dataPeriksa['daftartindakan_id'])
                                    {
//                                            $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                                            $modTindakanPelayananT = PendaftaranPasienLuarController::saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                                            $daftartindakan_id = $dataPeriksa['daftartindakan_id'];
                                    }
                            }else{
//                                    $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                                    $modTindakanPelayananT = PendaftaranPasienLuarController::saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                            }
                            $modPemeriksaanLab = PemeriksaanlabM::model()->findAllByAttributes(array(
                                    'daftartindakan_id'=>$dataPeriksa['daftartindakan_id']
                            ));
                            foreach ($modPemeriksaanLab as $detailPemeriksaanLab){
                                    //print_r($detailPemeriksaanLab->attributes);
                                    $details = PemeriksaanlabdetM::model()->pemeriksaanDetail($modHasilPemeriksaan, $detailPemeriksaanLab['pemeriksaanlab_id']);
    //                                BYPASS
    //                                if(!$details){ //Jika tidak memiliki nilai rujukan
    //                                    $this->successSaveHasilPemeriksaan = false;
    //                                    Yii::app()->user->setFlash('error',"Ada data yang tidak memiliki nilai rujukan !");
    //                                }

                                    foreach ($details as $k => $detail) {
                                            $modDetailHasilPemeriksaan = new LKDetailHasilPemeriksaanLabT;
                                            $modDetailHasilPemeriksaan->hasilpemeriksaanlab_id = $modHasilPemeriksaan->hasilpemeriksaanlab_id;
                                            $modDetailHasilPemeriksaan->pemeriksaanlabdet_id = $detail->pemeriksaanlabdet_id;
                                            $modDetailHasilPemeriksaan->pemeriksaanlab_id = $detailPemeriksaanLab['pemeriksaanlab_id'];
                                            $modDetailHasilPemeriksaan->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                                            if($modDetailHasilPemeriksaan->save()){ //Jika Detail Pemeriksaan berhasil Disimpan Ke Hasil Pemeriksaan
                                                    $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
                                            }else{ //Jika Ada yang tidak disimpan
                                                    $this->successSaveHasilPemeriksaan = false;
                                                    Yii::app()->user->setFlash('error',"Data Detail Hasil Pemeriksaan ID=".$pemeriksaanId." gagal disimpan");
                                            }
                                    }
                            }
                        }
                        $this->successSaveTindakan = $this->successSaveTindakan && true;

                    } else {
                        $this->successSaveTindakan = false;
                    }
                    
                    $this->successSaveHasilPemeriksaan=true;
                } else {
                    foreach ($value as $pemeriksaanId => $dataPeriksa) {
//                        $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                        $modTindakanPelayananT = PendaftaranPasienLuarController::saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                        $modHasilPemeriksaanPA = new LKHasilPemeriksaanPAT;
                        $modHasilPemeriksaanPA->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                        $modHasilPemeriksaanPA->pemeriksaanlab_id = $pemeriksaanId;
                        $modHasilPemeriksaanPA->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modHasilPemeriksaanPA->pasien_id = $modTindakanPelayananT->pasien_id;
                        $modHasilPemeriksaanPA->pendaftaran_id = $modTindakanPelayananT->pendaftaran_id;
                        $modHasilPemeriksaanPA->nosediaanpa = Generator::noSediaanPA();
                        $modHasilPemeriksaanPA->tglperiksapa = $modTindakanPelayananT->tgl_tindakan;
                        if($modHasilPemeriksaanPA->validate()){
                            if($modHasilPemeriksaanPA->save()){
                                 $this->successSaveHasilPemeriksaan=true;
                            }else{
                                 $this->successSaveHasilPemeriksaan=false;                   
                            }  
                        }
                        
                    }
                }
            }
        }
        
        protected function saveTindakanPelayanan($modPendaftaran,$modPasien,$dataPemeriksaan, $modPasienMasukPenunjang)
        {
                $modTindakanPelayananT = new LKTindakanPelayananT;
                $modTindakanPelayananT->penjamin_id = $modPendaftaran->penjamin_id;
                $modTindakanPelayananT->pasien_id = $modPasien->pasien_id;
                $modTindakanPelayananT->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                $modTindakanPelayananT->instalasi_id = Params::INSTALASI_ID_LAB;
                $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                $modTindakanPelayananT->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayananT->daftartindakan_id = $dataPemeriksaan['daftartindakan_id'];
                $modTindakanPelayananT->carabayar_id = $modPendaftaran->carabayar_id;
                $modTindakanPelayananT->qty_tindakan = $dataPemeriksaan['qty_tindakan'];
                $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayananT->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayananT->tarif_satuan = $dataPemeriksaan['tarif_tindakan'];
                $modTindakanPelayananT->tarif_tindakan = $modTindakanPelayananT->tarif_satuan * $modTindakanPelayananT->qty_tindakan;
                $modTindakanPelayananT->satuantindakan = $dataPemeriksaan['satuantindakan'];
                $modTindakanPelayananT->cyto_tindakan = $dataPemeriksaan['cyto_tindakan'];
                $modTindakanPelayananT->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
                $modTindakanPelayananT->discount_tindakan = 0;
                $modTindakanPelayananT->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                $modTindakanPelayananT->tarifcyto_tindakan = 0;
                $modTindakanPelayananT->subsidiasuransi_tindakan = 0;
                $modTindakanPelayananT->subsidipemerintah_tindakan = 0;
                $modTindakanPelayananT->subsisidirumahsakit_tindakan = 0;
                $modTindakanPelayananT->iurbiaya_tindakan = 0;
                $modTindakanPelayananT->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayananT->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;

                $tarifTindakan= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayananT->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayananT->kelaspelayanan_id.'');
                foreach($tarifTindakan as $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayananT->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayananT->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayananT->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayananT->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;

                if($modTindakanPelayananT->save()){//Jika Tindakan Pelayanan Berhasil tersimpan
                    $idPeriksaLab = $dataPemeriksaan['pemeriksaanlab_id'];
                    $jumlahTindKomponen = COUNT($_POST['LKTindakanKomponenT'][$idPeriksaLab]);
                    for($j=0; $j<$jumlahTindKomponen; $j++):
                        $modTindakanKomponen = new LKTindakanKomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['komponentarif_id'];
                        $modTindakanKomponen->tarif_tindakankomp = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'] * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $modTindakanPelayananT->tarif_tindakan;
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                    endfor;
                }
                
                return $modTindakanPelayananT;
        }
         * 
         */
        /**
         * Dipakai di Update Tindakan Laboratorium : laboratorium/inputPemeriksaan/update
         * delete tindakan
         * perbaikan = hapus tindakan paket & detail pemeriksaan
         */
         public function actionAjaxDeleteTindakan()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $konfigSys = KonfigsystemK::model()->find();
            $idTindakan = $_POST['idTindakan'];
            $idPendaftaran = $_POST['idPendaftaran'];
            $idPenunjang = $_POST['idPenunjang'];
            $idPemeriksaanLab = $_POST['idPemeriksaanLab'];
            
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $criteria = new CDbCriteria();
                $criteria->addCondition('pendaftaran_id = '.$idPendaftaran);
                $criteria->addCondition('pasienmasukpenunjang_id = '.$idPenunjang);
                $criteria->addCondition('daftartindakan_id = '.$idTindakan);
                $criteria->addCondition('tindakansudahbayar_id IS NULL');
                $modTindakanPelayanan = TindakanpelayananT::model()->findAll($criteria);
                //hapus relasi tindakanpelayanan_t dan detailhasilpemeriksaanlab_t
                TindakanpelayananT::model()->updateAll(array('detailhasilpemeriksaanlab_id'=>null),$criteria);
                $data['success'] = true;
                $data['status'] = '';
                if(count($modTindakanPelayanan) > 0){
                    $s=0;
                    foreach($modTindakanPelayanan as $i => $mod)
                    {
                        $modHasilPemeriksaan = HasilpemeriksaanlabT::model()->findByAttributes(
                            array(
                                'pendaftaran_id'=>$idPendaftaran, 
                                'pasienmasukpenunjang_id'=>$idPenunjang
                            )
                        );
                        //hapus detail pemeriksaan
                        $deleteHasilPemeriksaanDet = DetailhasilpemeriksaanlabT::model()->deleteAll(
                            'hasilpemeriksaanlab_id = '.$modHasilPemeriksaan->hasilpemeriksaanlab_id.' AND '.
                            'tindakanpelayanan_id = '. $mod->tindakanpelayanan_id
                        );
                        if($deleteHasilPemeriksaanDet)
                        {
                            $data['success'] = true;
                            //DINONAKTIFKAN KARENA PENGHAPUSAN DI ATUR DI RELASI TABEL : CASCADE ON DELETE
//                            if($konfigSys->isdeljurnaltransaksi == true){
//                                $updateTindakan = TindakanpelayananT::model()->findByPk($mod->tindakanpelayanan_id);
//                                if(isset($updateTindakan->jurnalrekening_id)){
//                                    $jurnalrekId = $updateTindakan->jurnalrekening_id;
//                                    $updateTindakan->jurnalrekening_id = null;
//                                    $updateTindakan->save();
//                                    // hapus jurnaldetail_t dan jurnalrekening_t
//                                    if($jurnalrekId){
//                                        JurnaldetailT::model()->deleteAllByAttributes(array('jurnalrekening_id'=>$jurnalrekId));
//                                        JurnalrekeningT::model()->deleteByPk($jurnalrekId);
//                                    }
//                                }
//                            }
                            //hapus tindakanpelayanan_t
                            $sukses_pembalik =  TindakanController::jurnalPembalikTindakan($mod['tindakanpelayanan_id']);
                            if (!$sukses_pembalik) {
                                $s++;
                            }

                            TindakanpelayananT::model()->deleteAll(
                                'pendaftaran_id = '.$idPendaftaran.' AND pasienmasukpenunjang_id = '.$idPenunjang.' AND tindakanpelayanan_id = '.$mod->tindakanpelayanan_id.' AND daftartindakan_id = '.$mod->daftartindakan_id
                            );
                        }
                    }

                    if ($s==0) {
                        $data['success'] = true;
                    }else{
                        $data['success'] = false;
                    }

                }else{
                    $data['success'] = false;
                    $data['status'] = 'Tindakan sudah dibayarkan.';
                }
                
                if ($data['success'] == true){
                    $transaction->commit();
                }else{
                    $data['success'] = false;
                    $transaction->rollback();
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                echo MyExceptionMessage::getMessage($exc,true);
                $data['success'] = false;
            }
            echo json_encode($data);
             Yii::app()->end();
            }
        }
}