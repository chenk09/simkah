<?php
Yii::import('rawatJalan.controllers.TindakanController'); //UNTUK MENGAMBIL FUNCTION saveJurnalRekening
class PendaftaranPasienLuarController extends SBaseController
{
        public $successSave = false;
        public $successSaveAdmisi = true; //variabel untuk validasi admisi
        public $successSaveRujukan = false; //variabel untuk validasi data opsional (rujukan)
        public $successSavePJ = false; //variabel untuk validasi data opsional (penanggung jawab)
        public $successSaveSample = false; //untuk status simpan sample
        public $successSaveHasilPemeriksaan = true; //Diset true karena akan di looping pada simpan hasil pemeriksaan dan di tambahkan operator AND
        public $successSaveHasilPemeriksaanDet = true; //Diset true karena akan di looping pada simpan detail hasil pemeriksaan dan di tambahkan operator AND
        public $successSaveTindakanKomponen = false;
        public $successSaveTindakanPelayanan = true; //Diset true karena akan di looping pada simpan tindakan dan di tambahkan operator AND
        public $successSaveTindakanRad = false;
        public $successSaveBiayaAdministrasi = false;
        public $successSavePasienPenunjang = false;
        
        protected function performAjaxValidation($model)
        {
            if(isset($_POST['ajax']) && $_POST['ajax']==='pppendaftaran-mp-form')
            {
              echo CActiveForm::validate($modTindakanPelayananT,$modTindakanKomponen,$modHasilPemeriksaan,$model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienPenunjang,$modPengambilanSample);
              Yii::app()->end();
            }   
        }
        /**
     * @return array action filters
     */
//  FILTER MENGGUNAKAN SRBAC
//  public function filters()
//  {
//      return array(
//          'accessControl', // perform access control for CRUD operations
//      );
//  }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//  public function accessRules()
//  {
//      return array(
//          array('allow',  // allow all users to perform 'index' and 'view' actions
//              'actions'=>array(),
//              'users'=>array('@'),
//          ),
//          array('allow', // allow authenticated user to perform 'create' and 'update' actions
//              'actions'=>array('index','actionPrint'),
//              'users'=>array('@'),
//          ),
//          array('allow', // allow admin user to perform 'admin' and 'delete' actions
//              'actions'=>array(),
//              'users'=>array('@'),
//          ),
//          array('deny',  // deny all users
//              'users'=>array('*'),
//          ),
//      );
//  }
          /**
         * action index digunakan di pendaftaran lab pasien luar
         * digunakan di :
         * 1. Laboratorium -> pendaftaran 
         * @param int $id pendaftaran_id mengambil nilai get dari index pendaftaran
         */
    public function actionIndex($id=null)
    {
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));} 
            
            $insert_notifikasi = new MyFunction();
            $format = new CustomFormat();
            $this->pageTitle = Yii::app()->name." - Pendaftaran Pasien Laboratorium";
            $model=new LKPendaftaranMp;
            $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $model->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->umur = "00 Thn 00 Bln 00 Hr";
            $model->thn = '00';
            $model->bln = '00';
            $model->hr = '00';
            $model->pakeSample = (!empty($model->pakeSample)) ? $model->pakeSample : false;
            $model->isPemeriksaanLab = (($model->isPemeriksaanLab == false) && ($model->isPemeriksaanRad !=  true)) ? true : $model->isPemeriksaanLab;
            $model->pegawai_id = Params::DEFAULT_DOKTER_LAB; //dr. Dewi Kania Yulianti
            $tarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>Params::TARIF_ADMINISTRASI_LAB_ID));
            $model->tarifAdministrasi = $tarif->harga_tariftindakan;
            $model->carabayar_id = Params::DEFAULT_CARABAYAR;
            $model->penjamin_id = Params::DEFAULT_PENJAMIN;
            $modPasien = new LKPasienM;
            $modPasien->propinsi_id = Yii::app()->user->getState('propinsi_id');
            $modPasien->kabupaten_id = YIi::app()->user->getState('kabupaten_id');
            $modPasien->jenisidentitas = "LAINNYA";
//            $modPasien->tanggal_lahir = date('d M Y');
            $modPenanggungJawab = new LKPenanggungJawabM;
            $modRujukan = new LKRujukanT;
//            $modRujukan->tanggal_rujukan = date ('d M Y H:i:s');
            $modPasienPenunjang = new LKPasienMasukPenunjangT;
            $modPengambilanSample = new LKPengambilanSampleT;
            $modPengambilanSample->no_pengambilansample = Generator::noPengambilanSample();
            $modPeriksaLab = LKPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true),array('order'=>'pemeriksaanlab_id, pemeriksaanlab_urutan'));
            $modJenisPeriksaLab = LKJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan'));
            $modHasilPemeriksaan= new LKHasilPemeriksaanLabT;
            $modTindakanPelayananT = new LKTindakanPelayananT;
            $modTindakanKomponen = new LKTindakanKomponenT;
            $modDetailHasilPemeriksaanLabT = new LKDetailHasilPemeriksaanLabT;
            $modSampleLab = new SamplelabM;
//            $modRincian = new LKRinciantagihanpasienpenunjangV;
            $modRincian = new LKRincianpemeriksaanlabradV;
            
            $modPemeriksaanlabs = null;
            
            //============== Model Radiologi =============
            $modRadiologi = new ROPendaftaranMp();
            $arrInputPemeriksaan = array();
            $modRadiologi->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
            $modRadiologi->ruangan_id = Params::RUANGAN_ID_RAD;
            $modRadiologi->pegawai_id = Params::DEFAULT_DOKTER_RAD; // dr. Wahyu Rinto
            $modRadiologi->tgl_pendaftaran = date('d M Y H:i:s');
            $modRadiologi->umur = "00 Thn 00 Bln 00 Hr";
            $modPeriksaRad = PemeriksaanradM::model()->findAllByAttributes(array('pemeriksaanrad_aktif'=>true),array('order'=>'pemeriksaanrad_jenis, pemeriksaanrad_urutan ASC')); //
            $modPasienPenunjangRad = new ROPasienMasukPenunjangT;
            
            $modRujukan->no_rujukan = "-";
            

            
           
            if(!isset($_POST['isPemeriksaanLab'])){
                if(empty($_POST['isPemeriksaanLab']['jeniskasuspenyakit_id'])){
                    if (isset($_POST['ROPendaftaranMp'])){
                        $model->jeniskasuspenyakit_id = $_POST['ROPendaftaranMp']['jeniskasuspenyakit_id'];
                        $model->kelaspelayanan_id = $_POST['ROPendaftaranMp']['kelaspelayanan_id'];
                        $model->pegawai_id = $_POST['ROPendaftaranMp']['pegawai_id'];
                    }
                }
            }
            
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            
            if (isset($_POST['LKPendaftaranMp'])){


                // echo "<pre>"; print_r($model);exit();
                $model->attributes = $_POST['LKPendaftaranMp'];
                if(isset($_POST['LKPasienM']))
                {
                    $modPasien->attributes = $_POST['LKPasienM'];
                }
                $modPenanggungJawab->attributes = $_POST['LKPenanggungJawabM'];
                $modRujukan->attributes = $_POST['LKRujukanT'];
                if(isset($_POST['LKRujukanT']['tanggal_rujukan'])){
                    $modRujukan->tanggal_rujukan = $format->formatDateTimeMediumForDB(trim($_POST['LKRujukanT']['tanggal_rujukan']));
                }
                //========== Copy Pendaftaran Lab ke Rad ========
                if(isset($_POST['ROPendaftaranMp']))
                {
                    $modRadiologi->attributes = $_POST['ROPendaftaranMp'];
                    $modRadiologi->no_asuransi = $_POST['LKPendaftaranMp']['no_asuransi'];
                    $modRadiologi->namapemilik_asuransi = $_POST['LKPendaftaranMp']['namapemilik_asuransi'];
                    $modRadiologi->nopokokperusahaan = $_POST['LKPendaftaranMp']['nopokokperusahaan'];
                    $modRadiologi->namaperusahaan = $_POST['LKPendaftaranMp']['namaperusahaan'];
                    $modRadiologi->kelastanggungan_id = $_POST['LKPendaftaranMp']['kelastanggungan_id'];
                    $modRadiologi->kunjungan = $model->kunjungan;
                    $modRadiologi->statusperiksa = $model->statusperiksa;
                    //========== End Copy Pendaftaran Lab ke Rad ==========

                    //===== End Mengembalikan nilai dari form Rad =====
                    $modRadiologi->ruangan_id = Params::RUANGAN_ID_RAD;
                    $modRadiologi->jeniskasuspenyakit_id = $_POST['ROPendaftaranMp']['jeniskasuspenyakit_id'];
                    $modRadiologi->kelaspelayanan_id = $_POST['ROPendaftaranMp']['kelaspelayanan_id'];
                    $modRadiologi->pegawai_id = $_POST['ROPendaftaranMp']['pegawai_id'];
    //                $modRadiologi->carabayar_id = $_POST['ROPendaftaranMp']['carabayar_id'];
    //                $modRadiologi->penjamin_id = $_POST['ROPendaftaranMp']['penjamin_id'];
                    $modRadiologi->carabayar_id = $_POST['LKPendaftaranMp']['carabayar_id'];
                    $modRadiologi->penjamin_id = $_POST['LKPendaftaranMp']['penjamin_id'];                    
                }
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    if(isset($_POST['LKPasienM']))
                    {
                        if($_POST['LKPasienM']['namadepan'] == 'Tn.')
                        {
                            if($_POST['LKPasienM']['jeniskelamin'] != 'LAKI-LAKI')
                            {
                                 $modPasien->addError('jeniskelamin', 'pilih jenis kelamin yang sesuai');
                                 Yii::app()->user->setFlash('danger',"Inputan jenis kelamin kurang tepat, tolong di betulkan");
                            }
                        }
                        else
                        {
                            if($_POST['LKPasienM']['namadepan'] == 'Ny.' || $_POST['LKPasienM']['namadepan'] == 'Nn')
                            {
                                if($_POST['LKPasienM']['jeniskelamin'] != 'PEREMPUAN')
                                {
                                     $modPasien->addError('jeniskelamin', 'pilih jenis kelamin yang sesuai');
                                     Yii::app()->user->setFlash('danger',"Inputan jenis kelamin kurang tepat, tolong di betulkan");
                                }
                            }
                        }


                        if($_POST['LKPasienM']['statusperkawinan'] != null){
                            if($_POST['LKPasienM']['statusperkawinan'] == 'BELUM KAWIN')
                            {
                                if($_POST['LKPasienM']['namadepan'] == 'BY. Ny.')
                                {
                                    $modPasien->addError('statusperkawinan', 'pilih status perkawinan yang sesuai');
                                    Yii::app()->user->setFlash('danger',"Inputan status perkawinan kurang tepat, tolong di betulkan");
                                }
                            }                        
                        }

                        if($_POST['LKPasienM']['pekerjaan_id'] != null){
                            if($_POST['LKPasienM']['pekerjaan_id'] == '12')
                            {
                                if($_POST['LKPasienM']['namadepan'] != 'BY. Ny.')
                                {
                                    $modPasien->addError('pendidikan_id', 'pilih pekerjaan yang sesuai');
                                    Yii::app()->user->setFlash('danger',"Inputan pekerjaan kurang tepat, tolong di betulkan");
                                }
                            }                        
                        }
                    }
                    
                    //test post disini
//                    echo "<pre>";
//                    print_r($_POST['LKPasienM']);
//                    EXIT();
                    //==Penyimpanan dan Update Pasien===========================
//                     $modPemeriksaanlabs = $_POST['PemeriksaanLab'];
                    if(!isset($_POST['isPasienLama'])){
                        if(isset($_POST['caraAmbilPhoto'])){
                            if($_POST['caraAmbilPhoto']=='file'){//Jika User Mengambil photo pegawai dengan cara upload file
                               $photo = CUploadedFile::getInstance($modPasien, 'photopasien');
                            }else{
                              $photo=$_POST['LKPendaftaranMp']['tempPhoto'];
                            }
                            $modPasien = $this->savePasien($_POST['LKPasienM'],$photo,$_POST['caraAmbilPhoto']);
                        }else
                            $modPasien = $this->savePasien($_POST['LKPasienM'],$photo,null);

                    }else{
                        $model->isPasienLama = true;
                        if (!empty($_POST['noRekamMedik'])){
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = LKPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            if(!empty($modPasien) && isset($_POST['isUpdatePasien'])) {
                                if(isset($_POST['caraAmbilPhoto'])){
                                    if($_POST['caraAmbilPhoto']=='file'){//Jika User Mengambil photo pegawai dengan cara upload file
                                       $photo = CUploadedFile::getInstance($modPasien, 'photopasien');
                                    }else{
                                      $photo=$_POST['LKPendaftaranMp']['tempPhoto'];
                                    }

                                   $modPasien = $this->updatePasien($modPasien,$_POST['LKPasienM'],$photo,$_POST['caraAmbilPhoto']);
                                }
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }
                    
                    //==Akhir Penyimpanan dan Update Pasien=====================
                    
                    $modRujukan = $this->saveRujukan($_POST['LKRujukanT']); //Save Rujukan
                    
                    //===penyimpanan Penanggung Jawab===========================                   
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['LKPenanggungJawabM']);
                    }
//                    ===Akhir Penyimpanan Penanggung Jawab=====================                    
                                        
                      //==Simpan Transaksi Pendaftaran ==
                    if(isset($_POST['isPemeriksaanLab'])){
                        $modelNew = $this->savePendaftaranMP($model,$modPasien,$modRujukan,$modPenanggungJawab);
                    }else{
                        $modelNew = $this->savePendaftaranMP($modRadiologi,$modPasien,$modRujukan,$modPenanggungJawab);
                    }
                    $model->attributes = $modelNew->attributes;
                    $modRadiologi->attributes = $modelNew->attributes;
                    
                    // ======== Simpan Form Pemeriksaan Laboratorium ======
                    if(isset($_POST['isPemeriksaanLab'])){
                        $model->isPemeriksaanLab = true;
                        $modPasienPenunjang = $this->savePasienPenunjang($model,$modPasien);//Simpan Psien Masuk Penunjang

                        //==Awal Simpan Jika Pake Sample
                        if(isset($_POST['pakeSample'])){
                            $model->pakeSample = true;
                            $data_sample = $this->tabularSample($_POST['LKPengambilanSampleT']);
                            $modPengambilanSample = $this->savePengambilanSample($_POST['LKPengambilanSampleT'],$modPasienPenunjang);
                        }else{
                            $this->successSaveSample = true; //jika pakeSample unchek maka dianggap penyimpanan berhasil
                            $model->pakeSample = false;
                        }
                        //==Akhir Simpan Jika Pake Sample===========================
                        $modUbahCaraBayar = new LKUbahCaraBayarR;
                        $this->saveUbahCaraBayar($model, $modUbahCaraBayar);//Simpan Ubah Cara Bayar                     

                        $this->saveHasilPemeriksaan($modPasienPenunjang,$model,$modPasien,$_POST['PemeriksaanLab'], $_POST['RekeningakuntansiV']);//Simpan Hasil Pemeriksaan
                        
                        if(isset($_POST['isAdminLab']))
                        {
                            $model->ruangan_id = Params::RUANGAN_ID_LAB;
                            $this->saveBiayaAdministrasi($modPasien, $model, $modPasienPenunjang, $_POST['tarifKarcisLab'], $_POST['RekeningakuntansiV']);
                        }

//                        $this->saveKarcis($modPasien,$model);
                        
                        if (count($modPasien) != 1){ //Jika Pasien Tidak Ada
                            $modPasien = New LKPasienM;
                            $this->successSave = false;
                            $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
                            Yii::app()->user->setFlash('warning',"Data Pasien tidak ditemukan isi Nomor Rekam Medik dengan benar");
                        }
                    }else{
                        //Jika tidak memilih pemeriksaan lab
                        $this->successSavePasienPenunjang = true;
                        $this->successSaveSample = true;
                        $this->successSaveHasilPemeriksaan = true;
                        $this->successSaveTindakanPelayanan = true;
                        $model->isPemeriksaanLab = false;
                        
                    }
                    // ======== Akhir Simpan Form Pemeriksaan Laboratorium ======
                    // ======== Simpan Form Pemeriksaan Radiologi ======
                    if(isset($_POST['isPemeriksaanRad'])){

                        $model->isPemeriksaanRad = true;
                        $modRadiologi->attributes = $model->attributes;
                        $modRadiologi->ruangan_id = Params::RUANGAN_ID_RAD;
                        $modRadiologi->jeniskasuspenyakit_id = $_POST['ROPendaftaranMp']['jeniskasuspenyakit_id'];
                        $modRadiologi->kelaspelayanan_id = $_POST['ROPendaftaranMp']['kelaspelayanan_id'];
                        $modRadiologi->pegawai_id = $_POST['ROPendaftaranMp']['pegawai_id'];
//                        $modRadiologi->carabayar_id = $_POST['ROPendaftaranMp']['carabayar_id'];
//                        $modRadiologi->penjamin_id = $_POST['ROPendaftaranMp']['penjamin_id'];
                        $modRadiologi->carabayar_id = $_POST['LKPendaftaranMp']['carabayar_id'];
                        $modRadiologi->penjamin_id = $_POST['LKPendaftaranMp']['penjamin_id'];
                        
                        $modPasienPenunjangRad = $this->savePasienPenunjangRad($modRadiologi,$modPasien);
                    
                        $this->saveTindakanPelayananRad($modPasien, $modRadiologi, $modPasienPenunjangRad, $_POST['RekeningakuntansiV']);

                        $modUbahCaraBayarRad = new ROUbahCaraBayarR;
                        $this->saveUbahCaraBayar($modRadiologi, $modUbahCaraBayarRad);//Simpan Ubah Cara Bayar
                        
                        if(isset($_POST['isAdminRad']))
                        {
                            $model->ruangan_id = Params::RUANGAN_ID_RAD;
                            $this->saveBiayaAdministrasi($modPasien,$model, $modPasienPenunjangRad, $_POST['tarifKarcisRad'],$_POST['RekeningakuntansiV']);
                        }
                    }else{
                        $model->isPemeriksaanRad = false;
                    }
                    // ======== Akhir Simpan Form Pemeriksaan Radiologi ======

                    $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                    $params['create_time'] = date( 'Y-m-d H:i:s');
                    $params['create_loginpemakai_id'] = Yii::app()->user->id;
                    $params['instalasi_id'] = ($model->ruangan_id == 18 ? 5 : 6);
                    $params['modul_id'] = ($model->ruangan_id == 18 ? 8 : 9);
                    $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $model->no_pendaftaran . '-' . $modPasien->nama_pasien;
                    $params['create_ruangan'] = $model->ruangan_id;
                    $params['judulnotifikasi'] = ($model->isPasienLama == true ? 'Pendaftaran Pasien Lama' : 'Pendaftaran Pasien Baru' );
                    $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                    
                    if($this->successSave && $this->successSaveTindakanPelayanan && $this->successSaveHasilPemeriksaan
                            && $this->successSaveRujukan && $this->successSaveSample && $this->successSaveBiayaAdministrasi && $this->successSavePasienPenunjang){   
                        $transaction->commit();  
                        $status = 1; //Berhasil Di Simpan
                        if(!($this->successSaveHasilPemeriksaanDet))
                        {
                            $status = 2; //Ada Yang Gagal Karena Tdk Memiliki Nilai Rujukan
                        }
                        $this->redirect(array('index','id'=>$model->pendaftaran_id, 'status'=>$status ));
                    } else {
                        $model->isNewRecord = true;
                        $transaction->rollback();
//                        Yii::app()->user->setFlash('error',"Data Gagal disimpan");
                        if(!($this->successSave)) Yii::app()->user->setFlash('error',"Data Pasien Gagal disimpan");
                        if(!($this->successSaveTindakanPelayanan)) Yii::app()->user->setFlash('error',"Data Tindakan Pelayanan Gagal disimpan");
                        if(!($this->successSaveHasilPemeriksaan)) Yii::app()->user->setFlash('error',"Data Hasil Pemeriksaan Gagal disimpan");
                        if(!($this->successSaveRujukan)) Yii::app()->user->setFlash('error',"Data Rujukan Gagal disimpan");
                        if(!($this->successSaveSample)) Yii::app()->user->setFlash('error',"Data Sample Gagal disimpan");
                        if(!($this->successSaveBiayaAdministrasi)) Yii::app()->user->setFlash('error',"Data Biaya Administrasi Gagal disimpan");
                        if(!($this->successSavePasienPenunjang)) Yii::app()->user->setFlash('error',"Data Pasien Masuk Penunjang Gagal disimpan");
                    }
                    
                   
                }
                catch(Exception $exc){
                    $model->isNewRecord = true;
                    $transaction->rollback();
//                    Yii::app()->user->setFlash('error',"Data Gagal disimpan");
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            if(isset($_POST['permintaanPenunjang'])) {
                foreach($_POST['permintaanPenunjang'] as $i=>$item) {
                    $arrInputPemeriksaan[$i] = $item;
                }
            }
            
            $model->isRujukan = true;
            $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            $model->pakeSample = (isset($_POST['pakeSample']) || $model->pakeSample) ? true : false;
            
            
            if (isset($id)){
              $modPendaftaran = LKPendaftaranMp::model()->findByPk($id);
              $modPMPLab = LKPasienMasukPenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id,'pasien_id'=>$modPendaftaran->pasien_id,'ruangan_id'=>  Params::RUANGAN_ID_LAB));
              $modPMPRad = LKPasienMasukPenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id,'pasien_id'=>$modPendaftaran->pasien_id,'ruangan_id'=>  Params::RUANGAN_ID_RAD));
              $model->attributes = $modPendaftaran->attributes;
              $model->attributes = $modPMPLab->attributes;
              $model->isNewRecord = false;
              $modRadiologi->attributes = $modPendaftaran->attributes;
              $modRadiologi->attributes = $modPMPRad->attributes;
              $modPasien    = LKPasienM::model()->findByPk($model->pasien_id);
              $modRujukan   = LKRujukanT::model()->findByPk($model->rujukan_id);
              $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
              $model->isPemeriksaanLab = true;
              $model->isPemeriksaanRad = true;
              if($model->pakeSample == true){
                $modPengambilanSample = PengambilansampleT::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$modPenunjang->pasienmasukpenunjang_id));
                $modSampleLab = SamplelabM::model()->findByPk($modPengambilanSample->samplelab_id);
                $modPengambilanSample->samplelab_nama = $modSampleLab->samplelab_nama;
              }
//              $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
              $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'pemeriksaanlab_urutan'));

              if (isset($model->penanggungjawab_id))
                $modPenanggungJawab = PenanggungjawabM::model()->findByPk($model->penanggungjawab_id);
            }
            $pendaftaran = PendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$model->pasien_id),array('order'=>'pendaftaran_id desc'));
                $umur = explode(" ",$pendaftaran[0]->umur); 
                // echo $pendaftaran[0]->umur;exit;
                $thn = $umur[0];
                $bln = $umur[2];
                $hr = $umur[4];
                
                // $model->umur = '00 Thn 00 Bln 00 Hr';
                $model->thn = $thn;
                $model->bln = $bln;
                $model->hr = $hr;
            
            $this->render('index',array(
                'model'=>$model, 
                'modPasien'=>$modPasien, 
                'modPenanggungJawab'=>$modPenanggungJawab,
                'modRujukan'=>$modRujukan,
                'modPengambilanSample'=>$modPengambilanSample,
                'modPasienPenunjang'=>$modPasienPenunjang,
                'modPeriksaLab'=>$modPeriksaLab,
                'modJenisPeriksaLab'=>$modJenisPeriksaLab,
                'modHasilPemeriksaan'=>$modHasilPemeriksaan,
                'modTindakanKomponen'=>$modTindakanKomponen,
                'modTindakanPelayananT'=>$modTindakanPelayananT,
                'modDetailHasilPemeriksaanLabT'=>$modDetailHasilPemeriksaanLabT,
                'modSampleLab' => $modSampleLab,
                'modRincian' => $modRincian,
                //Radiologi
                'modRadiologi'=>$modRadiologi,
                'modPeriksaRad'=>$modPeriksaRad,
                'arrInputPemeriksaan'=>$arrInputPemeriksaan,  
                'modRekenings'=>$modRekenings,
                 
            ));
    }
        
        public function tabularSample($modSample){
            $modPengambilanSample = null;
            foreach ($modSample as $key => $value) {
                $modPengambilanSample[$key] = new PengambilansampleT();
                $modPengambilanSample[$key]->attributes = $value; 
            }
            return $modPengambilanSample;
        }


        public function savePasien($attrPasien,$photo,$caraAmbilPhoto)
        {
            $format = new CustomFormat();
            $modPasien = new LKPasienM;
            $modPasien->jenisidentitas = (empty($_POST['LKPasienM']['jenisidentitas'])) ? "LAINNYA" : $_POST['LKPasienM']['jenisidentitas'];
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modPasien->no_rekam_medik = Generator::noRekamMedikPenunjang(Params::singkatanNoPendaftaranLab());
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = 'AKTIF';
            $modPasien->create_ruangan =Yii::app()->user->getState('ruangan_id'); //Params::RUANGAN_ID_LAB
            $modPasien->ispasienluar = 1;
            $modPasien->create_loginpemakai_id = Yii::app()->user->id;
            $modPasien->create_time = date('Y-m-d');
            $modPasien->tanggal_lahir = $format->formatDateMediumForDB($modPasien->tanggal_lahir);

            if(($caraAmbilPhoto=='file') && (!empty($_POST['LKPasienM']['photopasien']))){//Jika User Mengambil photo pegawai dengan cara upload file
                 
                  $gambar=$modPasien->photopasien;
                  if(!empty($model->photopasien)){//Klo User Memasukan Logo
                         $modPasien->photopasien =$random.$modPasien->photopasien;

                         Yii::import("ext.EPhpThumb.EPhpThumb");

                         $thumb=new EPhpThumb();
                         $thumb->init(); //this is needed

                         $fullImgName =$modPasien->photopasien;   
                         $fullImgSource = Params::pathPasienDirectory().$fullImgName;
                         $fullThumbSource = Params::pathPasienTumbsDirectory().'kecil_'.$fullImgName;

                         if($modPasien->save()){
                            $gambar->saveAs($fullImgSource);
                            $thumb->create($fullImgSource)
                                  ->resize(200,200)
                                  ->save($fullThumbSource);
                         }else{
//                             echo "gagal Ipload";//exit;
                                Yii::app()->user->setFlash("warning","Foto Gagal di-upload !");
                             }
                    }else{
                        $modPasien->save();
                    }
                    
              }else{
                  
                 $modPasien->photopasien=$photo;
                 if($modPasien->validate())
                    {
                        $modPasien->save();
                    }                    
                 else 
                    {
 
                         unlink(Params::pathPasienDirectory().$photo);
                         unlink(Params::pathPasienTumbsDirectory().$photo);
                    }
               }
                

            return $modPasien;
        }
        
        public function updatePasien($modPasien,$attrPasien,$photo,$caraAmbilPhoto)
        {
            $modPasienupdate = $modPasien;
            $modPasienupdate->attributes = $attrPasien;
            $modPasien->jenisidentitas = (empty($_POST['LKPasienM']['jenisidentitas'])) ? "LAINNYA" : $_POST['LKPasienM']['jenisidentitas'];
            $modPasien->ispasienluar = 1;
            $modPasienupdate->kelompokumur_id = Generator::kelompokUmur($modPasienupdate->tanggal_lahir);
            $temppPhoto=$modPasienupdate->photopasien;
            if($caraAmbilPhoto=='file')//Jika User Mengambil photo pegawai dengan cara upload file
              { 
                  $gambar=$modPasien->photopasien;

                  if(!empty($model->photopasien))//Klo User Memasukan Logo
                  { 
                         $modPasien->photopasien =$random.$modPasien->photopasien;

                         Yii::import("ext.EPhpThumb.EPhpThumb");

                         $thumb=new EPhpThumb();
                         $thumb->init(); //this is needed

                         $fullImgName =$modPasien->photopasien;   
                         $fullImgSource = Params::pathPasienDirectory().$fullImgName;
                         $fullThumbSource = Params::pathPasienTumbsDirectory().'kecil_'.$fullImgName;

                         if($modPasien->save()){
                                   if(!empty($temppPhoto)){
                                        unlink(Params::pathPasienDirectory().$photo);
                                        unlink(Params::pathPasienTumbsDirectory().$photo);
                                   } 
                                   $gambar->saveAs($fullImgSource);
                                   $thumb->create($fullImgSource)
                                         ->resize(200,200)
                                         ->save($fullThumbSource);
                              }
                    }
              }   
             else 
              {
                 $modPasien->photopasien=$photo;
                 if($modPasien->validate())
                    {
//                        $modPasien->save();
                        $modPasien->update();
                    }
                 else 
                    {
                         unlink(Params::pathPegawaiDirectory().$photo);
                         unlink(Params::pathPegawaiTumbsDirectory().$photo);
                    }
               }
            
            return $modPasienupdate;
        }
        
        public function saveRujukan($attrRujukan)
        {
            $modRujukan = new LKRujukanT;
            $modRujukan->attributes = $attrRujukan;
            $modRujukan->nama_perujuk = $attrRujukan['nama_perujuk'];
//            $modRujukan->tanggal_rujukan = ($attrRujukan['tanggal_rujukan'] == '') ? date('Y-m-d') : $attrRujukan['tanggal_rujukan'] ;
            if($modRujukan->validate()) {
                // form inputs are valid, do something here
                $modRujukan->save();
                $this->successSaveRujukan = TRUE;
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
//                $modRujukan->tanggal_rujukan = Yii::app()->dateFormatter->formatDateTime(
//                                                                CDateTimeParser::parse($modRujukan->tanggal_rujukan, 'yyyy-MM-dd'),'medium',null);
                $this->successSaveRujukan = FALSE;
            }
            return $modRujukan;
        }
        
        public function savePenanggungJawab($attrPenanggungJawab)
        {
            $modPenanggungJawab = new LKPenanggungJawabM;
            $modPenanggungJawab->attributes = $attrPenanggungJawab;
            if($modPenanggungJawab->validate()) {
                // form inputs are valid, do something here
                $modPenanggungJawab->save();
                $this->successSavePJ = TRUE;
            } else {
               $this->successSavePJ = FALSE;
            }

            return $modPenanggungJawab;
        }
        
        public function savePendaftaranMP($model, $modPasien, $modRujukan, $modPenanggungJawab){
            $modelNew = new LKPendaftaranMp;
            $modelNew->attributes = $model->attributes;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            $modelNew->ruangan_id=$model->ruangan_id;
            $modelNew->instalasi_id =Yii::app()->user->getState('instalasi_id');
            $modelNew->no_pendaftaran = Generator::noPendaftaran(Params::singkatanNoPendaftaranLab());
            $modelNew->no_urutantri = Generator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = Generator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
            $modelNew->statuspasien = Generator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = Generator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->statusmasuk = Params::statusMasuk('rujukan');
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            
            if ($modelNew->validate()){
                $modelNew->Save();
                $this->successSave = true;
            }else{
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'), 'medium', null);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
            
            return $modelNew;
        }
        
        public function savePengambilanSample($attrSample,$modPasienPenunjang){
            $modPengambilanSample = new LKPengambilanSampleT;
            $modPengambilanSample->tglpengambilansample = date('Y-m-d H:i:s');
            $modPengambilanSample->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
            $modPengambilanSample->attributes = $attrSample;
            if ($modPengambilanSample->validate()){
                $modPengambilanSample->Save();
                $this->successSaveSample = true;
            }else{
                $this->successSaveSample = false;
                $modPengambilanSample->tglpengambilansample = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPengambilanSample->tglpengambilansample, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPengambilanSample;
        }
        
        
        
        public function savePasienPenunjang($attrPendaftaran,$attrPasien){
            
            $modPasienPenunjang = new LKPasienMasukPenunjangT;
            $modPasienPenunjang->attributes = $attrPasien->attributes;
            $modPasienPenunjang->attributes = $attrPendaftaran->attributes;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_LAB;
//            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang(Yii::app()->user->getState('nopendaftaran_lab'));
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang('LK');
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->tglmasukpenunjang = $attrPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->save();
                $this->successSavePasienPenunjang = true;
            } else {
                $this->successSavePasienPenunjang = false;
//                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
//                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }
        
        public function saveUbahCaraBayar($model, $modUbahCaraBayar) 
        {
            $modUbahCaraBayar->pendaftaran_id = $model->pendaftaran_id;
            $modUbahCaraBayar->carabayar_id = $model->carabayar_id;
            $modUbahCaraBayar->penjamin_id = $model->penjamin_id;
            $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d');
            $modUbahCaraBayar->alasanperubahan = 'x';
            if($modUbahCaraBayar->validate())
            {
                // form inputs are valid, do something here
                $modUbahCaraBayar->save();
            }
  
        }
        /**
         * saveHasilPemeriksaan digunakan juga di:
         * - InputPemeriksaanController
         */
        public function saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$pemeriksaanLab, $postRekenings = array())
        {
            foreach($pemeriksaanLab as $jenisKelompok => $value){
                if($jenisKelompok == Params::LAB_PATOLOGI){
                    //echo 'Patologi Klinik <pre>'.print_r($value,1).'</pre>';
                    $modHasilPemeriksaan = new LKHasilPemeriksaanLabT;
                    $modHasilPemeriksaan->unsetAttributes(); // untuk membersihkan attribute kalau sebelumnya di pakai
                    $modHasilPemeriksaan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modHasilPemeriksaan->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                    $modHasilPemeriksaan->pasien_id = $modPasien->pasien_id;
                    $modHasilPemeriksaan->nohasilperiksalab = Generator::noHasilPemeriksaanLK();
                    $modHasilPemeriksaan->tglhasilpemeriksaanlab = date('Y-m-d H:i:s');
                    $modHasilPemeriksaan->hasil_kelompokumur = Generator::kelompokUmurNama($modPasien->tanggal_lahir);
                    $modHasilPemeriksaan->hasil_jeniskelamin = $modPasien->jeniskelamin;
                    $modHasilPemeriksaan->statusperiksahasil = Params::statusPeriksaLK(1);
                    if($modHasilPemeriksaan->save()){
                        $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
                        
                        $daftartindakan_id = "";
                        foreach ($value as $pemeriksaanId => $dataPeriksa){
                            if($dataPeriksa['daftartindakan_id'] == Params::PAKET_LAB){
                                if($daftartindakan_id != $dataPeriksa['daftartindakan_id'])
                                {
//                                    $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                                    $modTindakanPelayananT = PendaftaranPasienLuarController::saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang, $postRekenings);
                                    $daftartindakan_id = $dataPeriksa['daftartindakan_id'];
                                }
                            }else{
//                                $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                                $modTindakanPelayananT = PendaftaranPasienLuarController::saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang, $postRekenings);
                            }
                            
                            $modPemeriksaanLab = PemeriksaanlabM::model()->findAllByAttributes(array(
                                'daftartindakan_id'=>$dataPeriksa['daftartindakan_id']
                            ));
                            foreach ($modPemeriksaanLab as $detailPemeriksaanLab){
                                $details = PemeriksaanlabdetM::model()->pemeriksaanDetail($modHasilPemeriksaan, $detailPemeriksaanLab['pemeriksaanlab_id']);
//                                BYPASS
//                                if(!$details){ //Jika tidak memiliki nilai rujukan
//                                    $this->successSaveHasilPemeriksaanDet = false;
//                                }
                                foreach ($details as $k => $detail) {
                                    $modDetailHasilPemeriksaan = new LKDetailHasilPemeriksaanLabT;
                                    $modDetailHasilPemeriksaan->hasilpemeriksaanlab_id = $modHasilPemeriksaan->hasilpemeriksaanlab_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlabdet_id = $detail->pemeriksaanlabdet_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlab_id = $detailPemeriksaanLab['pemeriksaanlab_id'];
                                    $modDetailHasilPemeriksaan->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                                    if($modDetailHasilPemeriksaan->save()){ //Jika Detail Pemeriksaan berhasil Disimpan Ke Hasil Pemeriksaan
                                        $this->successSaveHasilPemeriksaanDet = $this->successSaveHasilPemeriksaanDet && true;
                                    }else{ //Jika Ada yang tidak disimpan
                                        $this->successSaveHasilPemeriksaanDet = false;
                                        Yii::app()->user->setFlash('error',"Data Detail Hasil Pemeriksaan ID=".$pemeriksaanId." gagal disimpan");
                                    }
                                }
                            }
                        }
                    } else 
                        $this->successSaveHasilPemeriksaan = false;

                } else {
                    //echo 'Anatomi <pre>'.print_r($value,1).'</pre>';
                    foreach ($value as $pemeriksaanId => $dataPeriksa) {
//                        $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                        $modTindakanPelayananT = PendaftaranPasienLuarController::saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang, $postRekenings);
                        $modHasilPemeriksaanPA = new LKHasilPemeriksaanPAT;
                        $modHasilPemeriksaanPA->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                        $modHasilPemeriksaanPA->pemeriksaanlab_id = $pemeriksaanId;
                        $modHasilPemeriksaanPA->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modHasilPemeriksaanPA->pasien_id = $modTindakanPelayananT->pasien_id;
                        $modHasilPemeriksaanPA->pendaftaran_id = $modTindakanPelayananT->pendaftaran_id;
                        $modHasilPemeriksaanPA->nosediaanpa = Generator::noSediaanPA();
                        $modHasilPemeriksaanPA->tglperiksapa = $modTindakanPelayananT->tgl_tindakan;
                        if($modHasilPemeriksaanPA->validate()){
                            if($modHasilPemeriksaanPA->save()){
                                 $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
                            }else{
                                 $this->successSaveHasilPemeriksaan = false;                   
                            }  
                        }
                        
                    }
                }
            }
            
        }
        
        /**
         * saveTindakanPelayanan digunakan juga di:
         * - InputPemeriksaanController
         */
        public function saveTindakanPelayanan($modPendaftaran,$modPasien,$dataPemeriksaan, $modPasienMasukPenunjang, $postRekenings = array())
        {       
                
                $modTindakanPelayananT = new LKTindakanPelayananT;
                $modTindakanPelayananT->penjamin_id = $modPendaftaran->penjamin_id;
                $modTindakanPelayananT->pasien_id = $modPasien->pasien_id;
                $modTindakanPelayananT->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                $modTindakanPelayananT->instalasi_id = Params::INSTALASI_ID_LAB;
                $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                $modTindakanPelayananT->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayananT->daftartindakan_id = $dataPemeriksaan['daftartindakan_id'];
                $modTindakanPelayananT->carabayar_id = $modPendaftaran->carabayar_id;
                $modTindakanPelayananT->qty_tindakan = $dataPemeriksaan['qty_tindakan'];
                $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayananT->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayananT->tarif_satuan = $dataPemeriksaan['tarif_tindakan'];
                $modTindakanPelayananT->tarif_tindakan = $modTindakanPelayananT->tarif_satuan * $modTindakanPelayananT->qty_tindakan;
                $modTindakanPelayananT->satuantindakan = $dataPemeriksaan['satuantindakan'];
                $cyto_tindakan = $modTindakanPelayananT->cyto_tindakan = $dataPemeriksaan['cyto_tindakan'];
                $modTindakanPelayananT->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
                $modTindakanPelayananT->discount_tindakan = 0;
                $modTindakanPelayananT->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                
                $modTindakanPelayananT->subsidiasuransi_tindakan = 0;
                $modTindakanPelayananT->subsidipemerintah_tindakan = 0;
                $modTindakanPelayananT->subsisidirumahsakit_tindakan = 0;
                $modTindakanPelayananT->iurbiaya_tindakan = 0;
                $modTindakanPelayananT->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayananT->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                
                if($cyto_tindakan){
                    $modTindakanPelayananT->tarifcyto_tindakan = $dataPemeriksaan['tarif_cyto'];;
                }else{
                    $modTindakanPelayananT->tarifcyto_tindakan = 0;
                }
                
//                $tarifTindakan= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayananT->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayananT->kelaspelayanan_id.'');
                $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$dataPemeriksaan['daftartindakan_id'], 'kelaspelayanan_id'=>$modTindakanPelayananT->kelaspelayanan_id));
                
                
                foreach($tarifTindakan as $dataTarif):
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                           $modTindakanPelayananT->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                    }
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                       $modTindakanPelayananT->tarif_medis=$dataTarif['harga_tariftindakan'];
                    }
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                       $modTindakanPelayananT->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                    }
                    if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                       $modTindakanPelayananT->tarif_bhp=$dataTarif['harga_tariftindakan'];
                   }
                endforeach;
                
                if($modTindakanPelayananT->save()){//Jika Tindakan Pelayanan Berhasil tersimpan
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $modTindakanPelayananT->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $modTindakanPelayananT->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanPelayananT, 'tm');
                    }
                    
                    $idPeriksaLab = $dataPemeriksaan['pemeriksaanlab_id'];
                    $jumlahTindKomponen = COUNT($_POST['LKTindakanKomponenT'][$idPeriksaLab]);
                    for($j=0; $j<$jumlahTindKomponen; $j++):
                        $modTindakanKomponen = new LKTindakanKomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['komponentarif_id'];
                        $modTindakanKomponen->tarif_tindakankomp = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'] * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $modTindakanPelayananT->tarif_tindakan;
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                    endfor;
                    
                    $this->successSaveTindakanPelayanan = $this->successSaveTindakanPelayanan && true;
                } else {
                    $this->successSaveTindakanPelayanan = false;
                }
                
                return $modTindakanPelayananT;
        }
        //===== Simpan untuk Radiologi ======
        protected function saveTindakanPelayananRad($modPasien,$modPendaftaran,$modPasienPenunjangRad, $postRekenings = array())
        {
            $validTRad = true;
            foreach($_POST['permintaanPenunjang'] as $i=>$item)
            {
                if(!empty($item)){
                    if(isset($item['cbPemeriksaan'])){
                        $modTindakans[$i] = new TindakanpelayananT;
                        $modTindakans[$i]->attributes=$item;
                        $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                        $modTindakans[$i]->pasien_id = $modPasien->pasien_id;
                        $modTindakans[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                        $modTindakans[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                        $modTindakans[$i]->instalasi_id = $modPendaftaran->instalasi_id;
                        $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                        $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                        $modTindakans[$i]->pasienmasukpenunjang_id = $modPasienPenunjangRad->pasienmasukpenunjang_id;
                        $modTindakans[$i]->daftartindakan_id = $item['idDaftarTindakan'];
                        $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                        $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                        $modTindakans[$i]->tgl_tindakan = date('Y-m-d H:i:s');
                        $modTindakans[$i]->tarif_satuan = $item['inputtarifpemeriksaanrad'];
                        $modTindakans[$i]->tarif_tindakan = $item['inputtarifpemeriksaanrad'] * $item['inputqty'];
                        $modTindakans[$i]->satuantindakan = $item['satuan'];
                        $modTindakans[$i]->qty_tindakan = $item['inputqty'];
                        $modTindakans[$i]->cyto_tindakan = $item['cyto'];
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['cyto']) ? (($item['persencyto'] / 100) * $modTindakans[$i]->tarif_tindakan) : 0;
                        $modTindakans[$i]->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
                        $modTindakans[$i]->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
//                        $modTindakans[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $modTindakans[$i]->ruangan_id = Params::RUANGAN_ID_RAD;
                        $modTindakans[$i]->discount_tindakan = 0;
                        $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                        $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                        $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                        $modTindakans[$i]->iurbiaya_tindakan = 0;
                        $validTRad = $modTindakans[$i]->validate() && $validTRad;
                        $idPemeriksaanRad[$i] = $item['inputpemeriksaanrad'];
                    }
                    
                }
            }
            
            if($validTRad){
                
                foreach($modTindakans as $i=>$tindakan){
                    $statusTindakan = $tindakan->save();
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening($tindakan->ruangan_id);
                        //update jurnalrekening_id
                        $tindakan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $tindakan->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $tindakan, 'tm');
                    }
                    
                    $statusSaveKomponen = $this->saveTindakanKomponenRad($tindakan);
                    
                    $statusSaveHasilPeriksa = $this->saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad[$i]);
                    
                    if($statusTindakan && $statusSaveKomponen && $statusSaveHasilPeriksa) {
                        $this->successSaveTindakanRad = $this->successSaveTindakanRad && true;
                    } else {
                        $this->successSaveTindakanRad = false;
                    }
                }
                
            } else {
                
                $this->successSaveTindakanRad = false;
            }
           
            return $modTindakans;
        }
        
        protected function savePasienPenunjangRad($modPendaftaran,$modPasien){ // Untuk Radiologi
            
            $modPasienPenunjang = new ROPasienMasukPenunjangT;
            
            $modPasienPenunjang->pasien_id = $modPasien->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $modPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//            $modPasienPenunjang->ruangan_id = $modPendaftaran->ruangan_id;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_RAD;
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang('RO');
            $modPasienPenunjang->tglmasukpenunjang = $modPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $modPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $modPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $modPendaftaran->ruangan_id;
            $modPasienPenunjang->create_time = $modPendaftaran->create_time;
            $modPasienPenunjang->update_time = $modPendaftaran->update_time;
            $modPasienPenunjang->create_loginpemakai_id = $modPendaftaran->create_loginpemakai_id;
            $modPasienPenunjang->update_loginpemakai_id = $modPendaftaran->update_loginpemakai_id;
            $modPasienPenunjang->create_ruangan = Params::RUANGAN_ID_RAD;
            
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            return $modPasienPenunjang;
        }
        
        protected function saveTindakanKomponenRad($tindakan) //Untuk Radiologi
        {   
            
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->compare('daftartindakan_id', $tindakan->daftartindakan_id);
            $criteria->compare('kelaspelayanan_id', $tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = new TindakankomponenT;
                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen->validate() && $valid;
                
                if($valid){
                    $modTindakanKomponen->save();
                }
            }
            
            return $valid;
        }
        
        protected function saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad) // Untuk Radiologi
        {
            
            $valid = true;
            for($j=0;$j<$tindakan->qty_tindakan;$j++){
                $modHasil = new ROHasilPemeriksaanRadT;
                $modHasil->unsetAttributes(); // untuk membersihkan attribute kalau sebelumnya di pakai
                $modHasil->pendaftaran_id = $tindakan->pendaftaran_id;
                $modHasil->pasienadmisi_id = $tindakan->pasienadmisi_id;
                $modHasil->pasien_id = $tindakan->pasien_id;
                $modHasil->pasienmasukpenunjang_id = $tindakan->pasienmasukpenunjang_id;
                $modHasil->tglpemeriksaanrad = $tindakan->tgl_tindakan;
                $modHasil->pemeriksaanrad_id = $idPemeriksaanRad;
                $modHasil->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                
                $valid = $modHasil->validate() && $valid;
                
                if($valid){
                    $modHasil->save();
                    TindakanpelayananT::model()->updateByPk($tindakan->tindakanpelayanan_id, array('hasilpemeriksaanrad_id'=>$modHasil->hasilpemeriksaanrad_id));
                }
            }
             
            return $valid;
        }
        
        //===== End Simpan untuk Radiologi ======
        public function saveKarcis($modPasien,$model)
        {
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New LKTindakanPelayananT;
            $modTindakanPelayan->penjamin_id=$model->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
            $modTindakanPelayan->kelaspelayanan_id=$model->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id=Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id=$model->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id=$_POST['TindakanPelayananT']['idTindakan'];
            $modTindakanPelayan->carabayar_id=$model->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$model->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan=$_POST['TindakanPelayananT']['tarifSatuan'];
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$model->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            
//            $modTindakanPelayan->ruangan_id = $model->ruangan_id;
            $modTindakanPelayan->ruangan_id = Params::RUANGAN_ID_LAB;
             
            if(!empty($_POST['TindakanPelayananT']['idKarcis'])){
                $modTindakanPelayan->karcis_id=$_POST['TindakanPelayananT']['idKarcis'];
                $modTindakanPelayan->tipepaket_id = $this->tipePaketKarcis($model, $_POST['TindakanPelayananT']['idKarcis'], $_POST['TindakanPelayananT']['idTindakan']);
            }

//                $tarifTindakan= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$modTindakanPelayan->daftartindakan_id, 'kelaspelayanan_id'=>$modTindakanPelayan->kelaspelayanan_id));
                foreach($tarifTindakan AS $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;
                if($modTindakanPelayan->save()){ 
                       $tindakanKomponen= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                       $jumlahKomponen=COUNT($tindakanKomponen);

                       foreach ($tindakanKomponen AS $tampilKomponen):
                               $modTindakanKomponen=new LKTindakanKomponenT;
                               $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                               $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                               $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                               $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                               $modTindakanKomponen->tarifcyto_tindakankomp=0;
                               $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                               $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                               $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                               $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                               if($modTindakanKomponen->save()){
                                   $cekTindakanKomponen++;
                               }
                       endforeach;
                       if($cekTindakanKomponen!=$jumlahKomponen){
                              $this->successSaveTindakanKomponen=false;
                           }
               } 
           
        }

        public function saveBiayaAdministrasi($modPasien, $model, $modPasienPenunjang, $tarifKarcisLab, $postRekenings = array())
        {
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New LKTindakanPelayananT;
            $modTindakanPelayan->penjamin_id=$model->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
            $modTindakanPelayan->pasienmasukpenunjang_id=$modPasienPenunjang->pasienmasukpenunjang_id;
            $modTindakanPelayan->kelaspelayanan_id=$model->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id = Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id = $model->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id = Params::TARIF_ADMINISTRASI_LAB_ID;
            $modTindakanPelayan->carabayar_id = $model->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$model->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan = $tarifKarcisLab;
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$model->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            $modTindakanPelayan->ruangan_id = $model->ruangan_id;


             
            $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$modTindakanPelayan->daftartindakan_id, 'kelaspelayanan_id'=>$modTindakanPelayan->kelaspelayanan_id));
            foreach($tarifTindakan AS $dataTarif):
                 if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                        $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                        $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                        $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                        $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                    }
            endforeach;
            
            if($modTindakanPelayan->save()){ 
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening($modTindakanPelayan->ruangan_id);
                        //update jurnalrekening_id
                        $modTindakanPelayan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $modTindakanPelayan->save();
                        $saveDetailJurnal = self::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanPelayan, 'tm');
                    }
                
                   $tindakanKomponen= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayan->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                   $jumlahKomponen=COUNT($tindakanKomponen);

                   foreach ($tindakanKomponen AS $tampilKomponen):
                           $modTindakanKomponen=new LKTindakanKomponenT;
                           $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                           $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                           $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                           $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                           $modTindakanKomponen->tarifcyto_tindakankomp=0;
                           $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                           $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                           $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                           $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                           if($modTindakanKomponen->save()){
                               $cekTindakanKomponen++;
                           }
                   endforeach;
                   if($cekTindakanKomponen!=$jumlahKomponen){
                          $this->successSaveBiayaAdministrasi=false;
                    }else
                        $this->successSaveBiayaAdministrasi=true;
           } 
           
        }

        /** save jurnal detail untuk biaya administrasi lab dan rad saja
        */
        public function saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanObat, $jenisPelayanan, $jenisSimpan = null){
            $valid = true;
            $modJurnalPosting = null;

            if($jenisSimpan == 'posting')
            {
                $modJurnalPosting = new JurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                }
            }
            foreach($postRekenings AS $i => $rekening){
                //insert berdasarkan daftartindakan_id atau obatalkes_id dgn melihat jnspelayanan
                $ruangansama=false;
                if($modJurnalRekening->ruangan_id==params::RUANGAN_ID_LAB){//untuk biaya administrasi lab
                    if ($i==0 || $i==1) {
                        $ruangansama=true;
                    }
                }else{//untuk biaya administrasi radiologi
                    if ($i==2 || $i==3){
                        $ruangansama=true;
                    }
                }
                $idOaTindakanSama = false;
                if(isset($modTindakanObat->daftartindakan_id)){
                    if(trim($rekening['daftartindakan_id'] == $modTindakanObat->daftartindakan_id))
                        $idOaTindakanSama = true;
                    $nama_tindakan = $modTindakanObat->daftartindakan->daftartindakan_nama;
                }else if(isset($modTindakanObat->obatalkes_id)){
                    if(trim($rekening['obatalkes_id'] == $modTindakanObat->obatalkes_id))
                        $idOaTindakanSama = true;
                    $nama_tindakan = $modTindakanObat->obatalkes->obatalkes_kode." - ".$modTindakanObat->obatalkes->obatalkes_nama;
                }
                if(strtolower(trim($rekening['jnspelayanan'])) == strtolower(trim($jenisPelayanan)) 
                    && $idOaTindakanSama && $ruangansama){
                    
                    $model[$i] = new JurnaldetailT();
                    $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
                    $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
                    $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                    $model[$i]->uraiantransaksi = $nama_tindakan;
                    $model[$i]->saldodebit = $rekening['saldodebit'];
                    $model[$i]->saldokredit = $rekening['saldokredit'];
                    $model[$i]->nourut = $i+1;
                    $model[$i]->rekening1_id = $rekening['struktur_id'];
                    $model[$i]->rekening2_id = $rekening['kelompok_id'];
                    $model[$i]->rekening3_id = $rekening['jenis_id'];
                    $model[$i]->rekening4_id = $rekening['obyek_id'];
                    $model[$i]->rekening5_id = $rekening['rincianobyek_id'];
                    $model[$i]->catatan = "";
                    if($model[$i]->validate())
                    {
                        $model[$i]->save();
                    }else{
//                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $model[$i]->getErrors();
                        $valid = false;
                        break;
                    }
                }
            }
            return $valid;        
        }
        
        
        public function tipePaketKarcis($modPendaftaran,$idKarcis,$idTindakan)
        {
            $criteria = new CDbCriteria;
            $criteria->with = array('tipepaket');
            $criteria->compare('daftartindakan_id', $idTindakan);
            $criteria->compare('tipepaket.carabayar_id', $modPendaftaran->carabayar_id);
            $criteria->compare('tipepaket.penjamin_id', $modPendaftaran->penjamin_id);
            $criteria->compare('tipepaket.kelaspelayanan_id', $modPendaftaran->kelaspelayanan_id);
            $paket = PaketpelayananM::model()->find($criteria)->tipepaket_id;
            if(empty($paket)) $paket = Params::TIPEPAKET_NONPAKET;
            
            return $paket;
        }
        
        public function actionPrint($id_pendaftaran,$id_pasienpenunjang = "", $labelOnly = null, $caraPrint){ 
            $judulLaporan = 'Informasi Pendaftaran Laboratorium';
            $modPendaftaran = PendaftaranT::model()->findByPk($id_pendaftaran);            
            $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $labelOnly = false;
            
            if(!empty($id_pasienpenunjang) && Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_LAB)
            { 
                $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                                    ), array('order'=>'pemeriksaanlab_urutan'));
            }else{
                $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    ), array('order'=>'pemeriksaanlab_urutan'));
            }
            $modPasienMasukPenunjang = PasienmasukpenunjangT::model()->findByPk($id_pasienpenunjang);
            // $modPasienMasukPenunjang = DateTime::createFromFormat('Y-m-d H:i:s')->format('d-m-Y');

             //$modPasienMasukPenunjang->tglmasukpenunjang = date('d M Y H:i:s');
             // echo $modPasienMasukPenunjang->tglmasukpenunjang; exit;
            // DIGANTI DENGAN $modRTagihan >>> $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $modPendaftaran->pendaftaran_id), array('order'=>'ruangan_id'));
//            $modRTagihan = new LKRinciantagihanpasienpenunjangV('searchDataPemeriksaanLabRad');
//            $dataProvider = $modRTagihan->searchDataPemeriksaanLabRad($modPendaftaran->pendaftaran_id)->getData();
//            foreach($dataProvider as $key => $data) //Menguraikan data provider menjadi array object
//                $modRincPeriksa[$key] = $data;
//            $modRincian = RinciantagihapasienpenunjangduaV::model()->findAllByAttributes(array(
        
            
            if(!empty($id_pasienpenunjang) && Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_LAB)
            {
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran,
//                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                ));
            }else{
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran
                ));
            }

            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modPasien'=>$modPasien,
                                            'modPendaftaran'=>$modPendaftaran, 
                                            'modRincian'=>$modRincian,
                                            'modTindakanPelayanan'=>$modTindakanPelayanan,
                                            'labelOnly'=>$labelOnly,
                                            'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                    ));
            }
           
        }
        //label baru LAB 25-04-2018 Akreditasi 
         public function actionPrint_old_2($id_pendaftaran,$id_pasienpenunjang = "", $labelOnly = null, $caraPrint){ 
            $judulLaporan = 'Informasi Pendaftaran Laboratorium';
            $modPendaftaran = PendaftaranT::model()->findByPk($id_pendaftaran);            
            $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $labelOnly = false;
            
            if(!empty($id_pasienpenunjang) && Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_LAB)
            { 
                $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                                    ), array('order'=>'pemeriksaanlab_urutan'));
            }else{
                $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    ), array('order'=>'pemeriksaanlab_urutan'));
            }
            $modPasienMasukPenunjang = PasienmasukpenunjangT::model()->findByPk($id_pasienpenunjang);
            // $modPasienMasukPenunjang = DateTime::createFromFormat('Y-m-d H:i:s')->format('d-m-Y');

             //$modPasienMasukPenunjang->tglmasukpenunjang = date('d M Y H:i:s');
             // echo $modPasienMasukPenunjang->tglmasukpenunjang; exit;
            // DIGANTI DENGAN $modRTagihan >>> $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $modPendaftaran->pendaftaran_id), array('order'=>'ruangan_id'));
//            $modRTagihan = new LKRinciantagihanpasienpenunjangV('searchDataPemeriksaanLabRad');
//            $dataProvider = $modRTagihan->searchDataPemeriksaanLabRad($modPendaftaran->pendaftaran_id)->getData();
//            foreach($dataProvider as $key => $data) //Menguraikan data provider menjadi array object
//                $modRincPeriksa[$key] = $data;
//            $modRincian = RinciantagihapasienpenunjangduaV::model()->findAllByAttributes(array(
        
            
            if(!empty($id_pasienpenunjang) && Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_LAB)
            {
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran,
//                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                ));
            }else{
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran
                ));
            }

            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print_old_2',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modPasien'=>$modPasien,
                                            'modPendaftaran'=>$modPendaftaran, 
                                            'modRincian'=>$modRincian,
                                            'modTindakanPelayanan'=>$modTindakanPelayanan,
                                            'labelOnly'=>$labelOnly,
                                            'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                    ));
            }
           
        }
        
//label baru RADIOLOGI 25-04-2018 Akreditasi 
          public function actionPrint_old_1($id_pendaftaran,$id_pasienpenunjang = "", $labelOnly = null, $caraPrint){ 
            $judulLaporan = 'Informasi Pendaftaran Laboratorium';
            $modPendaftaran = PendaftaranT::model()->findByPk($id_pendaftaran);            
            $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $labelOnly = false;
            
            if(!empty($id_pasienpenunjang) && Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_LAB)
            { 
                $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                                    ), array('order'=>'pemeriksaanlab_urutan'));
            }else{
                $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    ), array('order'=>'pemeriksaanlab_urutan'));
            }
            $modPasienMasukPenunjang = PasienmasukpenunjangT::model()->findByPk($id_pasienpenunjang);
            // $modPasienMasukPenunjang = DateTime::createFromFormat('Y-m-d H:i:s')->format('d-m-Y');

             //$modPasienMasukPenunjang->tglmasukpenunjang = date('d M Y H:i:s');
             // echo $modPasienMasukPenunjang->tglmasukpenunjang; exit;
            // DIGANTI DENGAN $modRTagihan >>> $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $modPendaftaran->pendaftaran_id), array('order'=>'ruangan_id'));
//            $modRTagihan = new LKRinciantagihanpasienpenunjangV('searchDataPemeriksaanLabRad');
//            $dataProvider = $modRTagihan->searchDataPemeriksaanLabRad($modPendaftaran->pendaftaran_id)->getData();
//            foreach($dataProvider as $key => $data) //Menguraikan data provider menjadi array object
//                $modRincPeriksa[$key] = $data;
//            $modRincian = RinciantagihapasienpenunjangduaV::model()->findAllByAttributes(array(
        
            
            if(!empty($id_pasienpenunjang) && Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_LAB)
            {
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran,
//                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                ));
            }else{
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran
                ));
            }

            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print_old_1',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modPasien'=>$modPasien,
                                            'modPendaftaran'=>$modPendaftaran, 
                                            'modRincian'=>$modRincian,
                                            'modTindakanPelayanan'=>$modTindakanPelayanan,
                                            'labelOnly'=>$labelOnly,
                                            'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                    ));
            }
           
        }
        public function actionPrintTindakan($id_pendaftaran,$id_pasienpenunjang = "",$caraPrint = ""){
            $judulLaporan = 'Informasi Pendaftaran Laboratorium';
            $modPendaftaran = PendaftaranT::model()->findByPk($id_pendaftaran);            
            $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
//            $modPasienPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id_pendaftaran));
            if(!empty($id_pasienpenunjang)){
                $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                                    ), array('order'=>'pemeriksaanlab_urutan'));
            }else{
                $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    ), array('order'=>'pemeriksaanlab_urutan'));
            }
            
            if(!empty($id_pasienpenunjang)){
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran,
                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                ));
            }else{
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran
                ));
            }
            
//            $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $modPendaftaran->pendaftaran_id), array('order'=>'ruangan_id'));
//            $modRTagihan = new LKRinciantagihanpasienpenunjangV('searchDataPemeriksaanLabRad');
//            $dataProvider = $modRTagihan->searchDataPemeriksaanLabRad($modPendaftaran->pendaftaran_id)->getData();
//            $dataProvider = $modRTagihan->searchTest()->getData();
//            foreach($dataProvider as $key => $data) //Menguraikan data provider menjadi array object
//                $modRincPeriksa[$key] = $data;

            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('PrintTindakan',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modPasien'=>$modPasien,
                                            'modPendaftaran'=>$modPendaftaran, 
                                            'modRincian'=>$modRincian,
                                            'modTindakanPelayanan'=>$modTindakanPelayanan,
                    ));
            }
           
        }
        
}
