<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class MasukPenunjangController extends SBaseController
{
        public $successSave = false;
        public $successSaveTindakan = true;
        public $successSavepasienKirimKeUnitLain=false;
        public $successSaveHasilPemeriksaan=true; // Untuk Looping
        public $successSavePasienMasukPenunjang=false;
        public $successSaveBiayaAdministrasi = false;
        
	public function actionIndex($idPasienKirimKeUnitLain,$idPendaftaran)
	{
            $modPendaftaran = LKPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modPeriksaLab = LKPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true),array('order'=>'pemeriksaanlab_id, pemeriksaanlab_urutan'));
            $modPermintaan = LKPermintaanKePenunjangT::model()->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            $modJenisPeriksaLab = LKJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan'));
            $modPasienMasukPenunjang = LKPasienMasukPenunjangT::model()->findByAttributes(array('pasien_id'=>$modPendaftaran->pasien_id, 'pendaftaran_id'=>$idPendaftaran));
            $modRiwayatKirimKeUnitLain = LKPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                      'ruangan_id'=>Params::RUANGAN_ID_LAB,
                                                                                                      'pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain),
                                                                                                  'pasienmasukpenunjang_id IS NULL');
            $modRiwayatPenunjang = LKPasienMasukPenunjangT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                             'ruangan_id'=>Params::RUANGAN_ID_LAB,));
            if(!empty($modPendaftaran->pasienadmisi_id)){
                $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                if(!empty($modPasienAdmisi->pasienpulang_id)){
                    $status = "SUDAH PULANG";
                }
            }else{
                $status = $modPendaftaran->statusperiksa;
            }
            
            if($status == "SUDAH PULANG"){
                echo "<script>
                        alert('Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan pemeriksaan pasien. ');
                        window.top.location.href='".Yii::app()->createUrl('laboratorium/RujukanPenunjang/index')."';
                    </script>";
            }
            $modKirimKeUnitLain = PasienkirimkeunitlainT::model()->findByPk($idPasienKirimKeUnitLain); 
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            if(!empty($_POST['pemeriksaanLab'])){
                //echo '<pre>'.print_r($_POST,1).'</pre>';exit;
                $transaction = Yii::app()->db->beginTransaction();
                try{
                        $modPasienMasukPenunjang=$this->savePasienMasukPenunjang($modPasien,$modPendaftaran,$modKirimKeUnitLain);
                        // if ($_POST['biayaAdministrasi'] > 0){
                        //     $this->saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienMasukPenunjang, $_POST['biayaAdministrasi']);    
                        // }
                        // print_r($_POST['biayaAdministrasi']);
                        // exit();
                        $this->saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienMasukPenunjang, $_POST['biayaAdministrasi'],$_POST['RekeningakuntansiV']);
                        $this->saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$_POST['PemeriksaanLab'],$_POST['LKPasienMasukPenunjangT'],$_POST['RekeningakuntansiV']);
                        //*** START dipakai untuk merubah dokter perujuk dari ruangan jika tidak sesuai dengan blangko lab
                        if(isset($_POST['PasienkirimkeunitlainT']['pegawai_id']) && !empty($_POST['PasienkirimkeunitlainT']['pegawai_id'])){
                            $modKirimKeUnitLain->pegawai_id = $_POST['PasienkirimkeunitlainT']['pegawai_id'];
                            $modKirimKeUnitLain->save();
                        }
                        //*** END -------------------------------------------------------
                        if ($this->successSaveHasilPemeriksaan && $this->successSavePasienMasukPenunjang){
                            LKPasienKirimKeUnitLainT::model()->updateByPk($idPasienKirimKeUnitLain, array('pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id));
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Berhasil Disimpan");
//                            $this->redirect(array('DaftarPasien/index','succes'=>1));
//                            $this->redirect(array('RujukanPenunjang/index&idPendaftaran='.$idPendaftaran));
                            $this->redirect(array('DaftarPasien/index', "id"=>$idPendaftaran,"caraPrint"=>"PRINT", 'status'=>1, "idPasienPenunjang"=>$modPasienMasukPenunjang->pasienmasukpenunjang_id));
                        } else {
                            Yii::app()->user->setFlash('error',"Data gagal disimpan");
                            $transaction->rollback();
                        }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            $this->render('index',array('modPermintaan'=>$modPermintaan,
                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
                                        'modPeriksaLab'=>$modPeriksaLab,
                                        'modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modRiwayatPenunjang'=>$modRiwayatPenunjang,
                                        'modJenisPeriksaLab'=>$modJenisPeriksaLab,
                                        'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                                        'modKirimKeUnitLain'=>$modKirimKeUnitLain,
                                        'modRekenings'=>$modRekenings,
                ));
	}
        
        protected function saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$pemeriksaanLab,$kirimKeunitlain, $postRekenings = array())
        {
            //echo "<pre>test";
            //print_r($pemeriksaanLab);

            foreach($pemeriksaanLab as $jenisKelompok => $value){
                if($jenisKelompok == Params::LAB_PATOLOGI){
                    //echo 'Patologi Klinik <pre>'.print_r($value,1).'</pre>';
                    $modHasilPemeriksaan = new LKHasilPemeriksaanLabT;
                    $modHasilPemeriksaan->unsetAttributes(); // untuk membersihkan attribute kalau sebelumnya di pakai
                    $modHasilPemeriksaan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modHasilPemeriksaan->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                    $modHasilPemeriksaan->pasien_id = $modPasien->pasien_id;
                    $modHasilPemeriksaan->nohasilperiksalab = KeyGenerator::noHasilPemeriksaanLK();
                    $modHasilPemeriksaan->tglhasilpemeriksaanlab = date('Y-m-d H:i:s');
                    $modHasilPemeriksaan->hasil_kelompokumur = KeyGenerator::kelompokUmurNama($modPasien->tanggal_lahir);
                    $modHasilPemeriksaan->hasil_jeniskelamin = $modPasien->jeniskelamin;
                    $modHasilPemeriksaan->statusperiksahasil = Params::statusPeriksaLK(1);
                    
                    if($modHasilPemeriksaan->save()){
                        $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
                        $daftartindakan_id = "";
                        foreach ($value as $pemeriksaanId => $dataPeriksa){
                            if($dataPeriksa['daftartindakan_id'] == Params::PAKET_LAB){
                                if($daftartindakan_id != $dataPeriksa['daftartindakan_id'])
                                {
                                    $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang,$postRekenings );
                                    $daftartindakan_id = $dataPeriksa['daftartindakan_id'];
                                }
                            }else{
                                $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang,$postRekenings);
                            }
                            $modPemeriksaanLab = PemeriksaanlabM::model()->findAllByAttributes(array(
                                'daftartindakan_id'=>$dataPeriksa['daftartindakan_id']
                            ));
                            foreach ($modPemeriksaanLab as $detailPemeriksaanLab){
                                //print_r($detailPemeriksaanLab->attributes);
                                $details = PemeriksaanlabdetM::model()->pemeriksaanDetail($modHasilPemeriksaan, $detailPemeriksaanLab['pemeriksaanlab_id']);
//                                BYPASS
//                                if(!$details){ //Jika tidak memiliki nilai rujukan
//                                    $this->successSaveHasilPemeriksaan = false;
//                                    Yii::app()->user->setFlash('error',"Ada data yang tidak memiliki nilai rujukan !");
//                                }
                                
                                foreach ($details as $k => $detail) {
                                    //$PemeriksaanLabDet = $detailPemeriksaanLab['pemeriksaanlab_id'];
                                    $modDetailHasilPemeriksaan = new LKDetailHasilPemeriksaanLabT;
                                    $modDetailHasilPemeriksaan->hasilpemeriksaanlab_id = $modHasilPemeriksaan->hasilpemeriksaanlab_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlabdet_id = $detail->pemeriksaanlabdet_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlab_id = $detailPemeriksaanLab['pemeriksaanlab_id'];
                                    $modDetailHasilPemeriksaan->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                                    if($modDetailHasilPemeriksaan->save()){ //Jika Detail Pemeriksaan berhasil Disimpan Ke Hasil Pemeriksaan
                                        $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
                                    }else{ //Jika Ada yang tidak disimpan
                                        $this->successSaveHasilPemeriksaan = false;
                                        Yii::app()->user->setFlash('error',"Data Detail Hasil Pemeriksaan ID=".$pemeriksaanId." gagal disimpan");
                                    }
                                }
                            }
                        }
                    } else{
                        $this->successSaveHasilPemeriksaan = false;
                    }
                    
                } else {
                    //echo 'Anatomi <pre>'.print_r($value,1).'</pre>';
                    foreach ($value as $pemeriksaanId => $dataPeriksa) {
                        $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang, $postRekenings);
                        $modHasilPemeriksaanPA = new LKHasilPemeriksaanPAT;
                        $modHasilPemeriksaanPA->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                        $modHasilPemeriksaanPA->pemeriksaanlab_id = $pemeriksaanId;
                        $modHasilPemeriksaanPA->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modHasilPemeriksaanPA->pasien_id = $modTindakanPelayananT->pasien_id;
                        $modHasilPemeriksaanPA->pendaftaran_id = $modTindakanPelayananT->pendaftaran_id;
                        $modHasilPemeriksaanPA->nosediaanpa = KeyGenerator::noSediaanPA();
                        $modHasilPemeriksaanPA->tglperiksapa = $modTindakanPelayananT->tgl_tindakan;
                        if($modHasilPemeriksaanPA->validate()){
                            if($modHasilPemeriksaanPA->save()){
                                 $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
                            }else{
                                 $this->successSaveHasilPemeriksaan = false;
                            }  
                        }
                        
                    }
                }
            }
        }

        // protected function saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$pemeriksaanLab)
        // {
        //     foreach($pemeriksaanLab as $jenisKelompok => $value)
        //     {
        //         if($jenisKelompok == Params::LAB_PATOLOGI)
        //         {
        //             //echo 'Patologi Klinik <pre>'.print_r($value,1).'</pre>';
        //             $modHasilPemeriksaan = new LKHasilPemeriksaanLabT;
        //             $modHasilPemeriksaan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
        //             $modHasilPemeriksaan->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
        //             $modHasilPemeriksaan->pasien_id = $modPasien->pasien_id;
        //             $modHasilPemeriksaan->nohasilperiksalab = Generator::noHasilPemeriksaanLK();
        //             $modHasilPemeriksaan->tglhasilpemeriksaanlab = date('Y-m-d H:i:s');
        //             $modHasilPemeriksaan->hasil_kelompokumur = Generator::kelompokUmurNama($modPasien->tanggal_lahir);
        //             $modHasilPemeriksaan->hasil_jeniskelamin = $modPasien->jeniskelamin;
        //             $modHasilPemeriksaan->statusperiksahasil = Params::statusPeriksaLK(1);
                    
        //             if($modHasilPemeriksaan->save()){
        //                 $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
        //                 foreach ($value as $pemeriksaanId => $dataPeriksa)
        //                 {
        //                     $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
        //                     if($pemeriksaanId != 'persencyto')
        //                     {
        //                         $details = PemeriksaanlabdetM::model()->pemeriksaanDetail($modHasilPemeriksaan, $pemeriksaanId);
        //                         foreach ($details as $k => $detail)
        //                         {
        //                             $modDetailHasilPemeriksaan = new LKDetailHasilPemeriksaanLabT;
        //                             $modDetailHasilPemeriksaan->hasilpemeriksaanlab_id = $modHasilPemeriksaan->hasilpemeriksaanlab_id;
        //                             $modDetailHasilPemeriksaan->pemeriksaanlabdet_id = $detail->pemeriksaanlabdet_id;
        //                             $modDetailHasilPemeriksaan->pemeriksaanlab_id = $pemeriksaanId;
        //                             $modDetailHasilPemeriksaan->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
        //                             if($modDetailHasilPemeriksaan->save()){
        //                                 $this->successSaveHasilPemeriksaan = $this->successSaveHasilPemeriksaan && true;
        //                             }else{
        //                                 $this->successSaveHasilPemeriksaan = false;
        //                             }
        //                         }
        //                     }
        //                 }
        //             }else{
        //                 $this->successSaveHasilPemeriksaan = false;
        //             }
        //         } else {
        //             foreach ($value as $pemeriksaanId => $dataPeriksa) {
        //                 $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
        //                 $modHasilPemeriksaanPA = new LKHasilPemeriksaanPAT;
        //                 $modHasilPemeriksaanPA->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
        //                 $modHasilPemeriksaanPA->pemeriksaanlab_id = $pemeriksaanId;
        //                 $modHasilPemeriksaanPA->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
        //                 $modHasilPemeriksaanPA->pasien_id = $modTindakanPelayananT->pasien_id;
        //                 $modHasilPemeriksaanPA->pendaftaran_id = $modTindakanPelayananT->pendaftaran_id;
        //                 $modHasilPemeriksaanPA->nosediaanpa = Generator::noSediaanPA();
        //                 $modHasilPemeriksaanPA->tglperiksapa = $modTindakanPelayananT->tgl_tindakan;
        //                 if($modHasilPemeriksaanPA->validate()){
        //                     if($modHasilPemeriksaanPA->save()){
        //                          $this->successSaveHasilPemeriksaan=$this->successSaveHasilPemeriks && true;
        //                     }else{
        //                          $this->successSaveHasilPemeriksaan=false;                   
        //                     }  
        //                 }
        //             }
        //         }
        //     }
        // }
        
        protected function saveTindakanPelayanan($modPendaftaran,$modPasien,$dataPemeriksaan, $modPasienMasukPenunjang,$postRekenings = array())
        {
                $persenRujukanIn = Yii::app()->user->getState('persentasirujin');
                if($persenRujukanIn==0){
                    $persenRujukanIn = KelaspelayananM::model()->findByPk($modPendaftaran->kelaspelayanan_id)->persentasirujin;
                }
                $tarifRujukanIn = round($dataPemeriksaan['tarif_tindakan'] * $persenRujukanIn / 100);
                $modTindakanPelayananT = new LKTindakanPelayananT;
                $modTindakanPelayananT->penjamin_id = $modPendaftaran->penjamin_id;
                $modTindakanPelayananT->pasien_id = $modPasien->pasien_id;
//                $modTindakanPelayananT->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                $modTindakanPelayananT->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas'); //Semua pasien lab & rad tanpa kelas
                $modTindakanPelayananT->instalasi_id = Params::INSTALASI_ID_LAB;
                $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                $modTindakanPelayananT->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayananT->daftartindakan_id = $dataPemeriksaan['daftartindakan_id'];
                $modTindakanPelayananT->carabayar_id = $modPendaftaran->carabayar_id;
                $modTindakanPelayananT->qty_tindakan = $dataPemeriksaan['qty_tindakan'];
                $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayananT->tgl_tindakan = date('Y-m-d H:i:s');
                $tarif = $modTindakanPelayananT->tarif_satuan = $dataPemeriksaan['tarif_tindakan'] + $tarifRujukanIn;
                $modTindakanPelayananT->tarif_tindakan = $modTindakanPelayananT->tarif_satuan * $modTindakanPelayananT->qty_tindakan;
                $modTindakanPelayananT->satuantindakan = $dataPemeriksaan['satuantindakan'];
                $cyto_tindakan = $modTindakanPelayananT->cyto_tindakan = $dataPemeriksaan['cyto_tindakan'];
                $modTindakanPelayananT->dokterpemeriksa1_id=$_POST['LKPasienMasukPenunjang']['pegawai_id'];
                $modTindakanPelayananT->dokterpemeriksa2_id = $_POST['PasienkirimkeunitlainT']['pegawai_id'];
                $modTindakanPelayananT->discount_tindakan = 0;
                $modTindakanPelayananT->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                $modTindakanPelayananT->tarifcyto_tindakan = 0;
                $modTindakanPelayananT->subsidiasuransi_tindakan = 0;
                $modTindakanPelayananT->subsidipemerintah_tindakan = 0;
                $modTindakanPelayananT->subsisidirumahsakit_tindakan = 0;
                $modTindakanPelayananT->iurbiaya_tindakan = 0;
                $modTindakanPelayananT->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayananT->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                
                
                if($cyto_tindakan){
                    $tarifTindakan  = LKTarifTindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modTindakanPelayananT->daftartindakan_id,'kelaspelayanan_id'=>$modTindakanPelayananT->kelaspelayanan_id, 'komponentarif_id'=>6));
//                    $harga          = $tarifTindakan->harga_tariftindakan;
                    $persen_cyto    = $tarifTindakan->persencyto_tind;
                    $tarif_cyto     = ($tarif*$persen_cyto)/100;
                    
                    $modTindakanPelayananT->tarifcyto_tindakan = $tarif_cyto;
                    
                }else{
                    $modTindakanPelayananT->tarifcyto_tindakan = 0;
                }
                $tarifTindakan= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayananT->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayananT->kelaspelayanan_id.'');
                foreach($tarifTindakan as $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayananT->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayananT->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayananT->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayananT->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;
                
                if($modTindakanPelayananT->save()){//Jika Tindakan Pelayanan Berhasil 
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $modTindakanPelayananT->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $modTindakanPelayananT->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanPelayananT, 'tm');
                    }
                    $idPeriksaLab = $dataPemeriksaan['pemeriksaanlab_id'];
                    $jumlahTindKomponen = COUNT($_POST['LKTindakanKomponenT'][$idPeriksaLab]);
                    for($j=0; $j<$jumlahTindKomponen; $j++):
                        $modTindakanKomponen = new LKTindakanKomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['komponentarif_id'];
                        $modTindakanKomponen->tarif_tindakankomp = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'] * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'];
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                    endfor;
                        // -- menyimpan komponen jasa rujukan penunjang
                        $modTindakanKomponen = new LKTindakanKomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = Params::KOMPONENTARIF_ID_JASA_RUJPEN;
                        $modTindakanKomponen->tarif_tindakankomp = $tarifRujukanIn * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $tarifRujukanIn;
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                }
                
                return $modTindakanPelayananT;
        }
		
		public function saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienMasukPenunjang, $tarifKarcisLab, $postRekenings = array())
        {
            // print_r($tarifKarcisLab);
            // exit();
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New LKTindakanPelayananT;
            $modTindakanPelayan->penjamin_id=$modPendaftaran->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
            $modTindakanPelayan->pasienmasukpenunjang_id=$modPasienMasukPenunjang->pasienmasukpenunjang_id;
            $modTindakanPelayan->kelaspelayanan_id=$modPendaftaran->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id = Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id = Params::TARIF_ADMINISTRASI_LAB_ID;
            $modTindakanPelayan->carabayar_id = $modPendaftaran->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan = $tarifKarcisLab;
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$modPendaftaran->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
//            $modTindakanPelayan->ruangan_id = $modPendaftaran->ruangan_id; -- salah harusnya ngambil dari session
            $modTindakanPelayan->ruangan_id = Yii::app()->user->getState('ruangan_id');
             
            $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$modTindakanPelayan->daftartindakan_id, 'kelaspelayanan_id'=>$modTindakanPelayan->kelaspelayanan_id));
            foreach($tarifTindakan AS $dataTarif):
                 if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                        $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                        $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                        $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                        $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                    }
            endforeach;
            
            if($modTindakanPelayan->save()){ 
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $modTindakanPelayan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $modTindakanPelayan->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanPelayan, 'tm');
                    }
                
                   $tindakanKomponen= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayan->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                   $jumlahKomponen=COUNT($tindakanKomponen);

                   foreach ($tindakanKomponen AS $tampilKomponen):
                           $modTindakanKomponen=new LKTindakanKomponenT;
                           $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                           $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                           $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                           $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                           $modTindakanKomponen->tarifcyto_tindakankomp=0;
                           $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                           $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                           $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                           $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                           if($modTindakanKomponen->save()){
                               $cekTindakanKomponen++;
                           }
                   endforeach;
                   if($cekTindakanKomponen!=$jumlahKomponen){
                          $this->successSaveBiayaAdministrasi=false;
                    }else
                        $this->successSaveBiayaAdministrasi=true;
           } 
           
        }

        protected function savePasienMasukPenunjang($modPasien,$modPendaftaran,$modKirimKeUnitLain)
        {
            $modPasienMasukPenunjang = new LKPasienMasukPenunjangT;
            $modPasienMasukPenunjang->pasienkirimkeunitlain_id = $modKirimKeUnitLain->pasienkirimkeunitlain_id;
            $modPasienMasukPenunjang->ruanganasal_id=$modKirimKeUnitLain->create_ruangan;
            $modPasienMasukPenunjang->pasien_id = $modPasien->pasien_id;
            $modPasienMasukPenunjang->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
            if(!empty($modPendaftaran->pasienadmisi_id)){ ////Jika Paien Berasal dari Rawat Inap
                $modPasienMasukPenunjang->pasienadmisi_id=$modPendaftaran->pasienadmisi_id;
                $modPasienMasukPenunjang->pendaftaran_id=$modPendaftaran->pendaftaran_id;
            }else{ ////Jika Pasien berasal dari RJ ataw RD
                $modPasienMasukPenunjang->pendaftaran_id=$modPendaftaran->pendaftaran_id;
            }
//            $modPasienMasukPenunjang->pegawai_id=$modPendaftaran->pegawai_id;
            $modPasienMasukPenunjang->pegawai_id = $_POST['LKPasienMasukPenunjang']['pegawai_id'];
//            $modPasienMasukPenunjang->kelaspelayanan_id=$modPendaftaran->kelaspelayanan_id;
            $modPasienMasukPenunjang->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas'); //karena semua pasien lab/rad itu tanpa kelas
            $modPasienMasukPenunjang->ruangan_id = Params::RUANGAN_ID_LAB;
            $modPasienMasukPenunjang->no_masukpenunjang=KeyGenerator::noMasukPenunjang('LK');
            $modPasienMasukPenunjang->tglmasukpenunjang=date('Y-m-d H:i:s');
            $modPasienMasukPenunjang->no_urutperiksa=KeyGenerator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienMasukPenunjang->ruangan_id);
            $modPasienMasukPenunjang->kunjungan=$modPendaftaran->kunjungan;
            $modPasienMasukPenunjang->statusperiksa=$modPendaftaran->statusperiksa;
            
            if($modPasienMasukPenunjang->validate()){//Jika proses Palidasi Pasien Masuk Penunjang Berhasil
                if($modPasienMasukPenunjang->save()){//Jika $modPasienMasukPenunjang Berhasil Disimpan
                     $this->successSavePasienMasukPenunjang=true;
                }else{//Jika $modPasienMasukPenunjang Gagal Disimpan
                     $this->successSavePasienMasukPenunjang=false;
                     Yii::app()->user->setFlash('error',"Data Pasien Masuk Pennunjang Gagal Disimpan.");

                }
            }else{//Jika proses Palidasi Pasien Masuk Penunjang Berhasil Gagal
               $this->successSavePasienMasukPenunjang=false;
               Yii::app()->user->setFlash('error',"Data Pasien Masuk Pennunjang Tidak Tervalidasi.");

            }
            return $modPasienMasukPenunjang;    
        }
}
