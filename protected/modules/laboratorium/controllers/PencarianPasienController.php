<?php

class PencarianPasienController extends SBaseController
{
	
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';
        public $successSavepasienKirimKeUnitLain=false;
        public $successSaveHasilPemeriksaan=false;
        public $successSavePasienMasukPenunjang=false;
        public $successSaveBiayaAdministrasi = false;
        public $errorMessages = 'error<br/>';


	/**
	 * @return array action filters
	 */
//	DI COMMENT KARENA FILTER USER / ROLE MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view','DaftarDariPasienRujukan','actionPrintLabel'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

        public function actionIndex()
        {
                $format = new CustomFormat();
                $modRJ = new LKInfoKunjunganRJV;
                $modRI = new LKInfoKunjunganRIV;
                $modRD = new LKInfoKunjunganRDV;
                $cekRJ = true;
                $cekRI = false;
                $cekRD = false;
//                $modRJ->tglAwal = date("Y-m-d").' 00:00:00';
//                $modRJ->tglAkhir =date('Y-m-d H:i:s');
//                $modRI->tglAwal = date("Y-m-d").' 00:00:00';
//                $modRI->tglAkhir = date('Y-m-d H:i:s');
//                $modRD->tglAwal = date("Y-m-d").' 00:00:00';
//                $modRD->tglAkhir =date('Y-m-d H:i:s');

//                $modRJ->tglAwal = date("d M Y").' 00:00:00';
                $modRJ->tglAwal = date('d M Y', strtotime('-5 days')).' 00:00:00';
                $modRJ->tglAkhir = date('d M Y H:i:s');
                $modRI->tglAwal = date('d M Y', strtotime('-5 days')).' 00:00:00';
                $modRI->tglAkhir = date('d M Y H:i:s');
                $modRD->tglAwal = date('d M Y', strtotime('-5 days')).' 00:00:00';
                $modRD->tglAkhir = date('d M Y H:i:s');
       
                
                if(isset ($_POST['instalasi']))
                {
                    switch ($_POST['instalasi']) {
                        case 'RJ':
                            $cekRJ = true;
                            $cekRI = false;
                            $cekRD = false;
                            $modRJ->attributes = $_POST['LKInfoKunjunganRJV'];
                            if(!empty($_POST['LKInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKInfoKunjunganRJV']['tglAwal']);
                            }
                            if(!empty($_POST['LKInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKInfoKunjunganRJV']['tglAkhir']);
                            }
                            
                            break;
                            
                        case 'RI':
                            $cekRI = true;
                            $cekRJ = false;
                            $cekRD = false;
                            $modRI->attributes = $_POST['LKInfoKunjunganRIV'];
                            if(!empty($_POST['LKInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKInfoKunjunganRIV']['tglAwal']);
                            }
                            if(!empty($_POST['LKInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKInfoKunjunganRIV']['tglAkhir']);
                            }
                            break;
                            
                        case 'RD':
                            $cekRD = true;
                            $cekRI = false;
                            $cekRJ = false;
                            $modRD->attributes = $_POST['LKInfoKunjunganRDV'];
                            if(!empty($_POST['LKInfoKunjunganRDV']['tglAwal']))
                            {
                                $modRD->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKInfoKunjunganRDV']['tglAwal']);
                            }
                            if(!empty($_POST['LKInfoKunjunganRDV
                                ']['tglAwal']))
                            {
                                $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKInfoKunjunganRDV']['tglAkhir']);
                            }
                           
                            break;
                    }
                }
                
                $this->render('index',array(
                                 'modRJ'=>$modRJ,
                                 'modRI'=>$modRI,
                                 'modRD'=>$modRD,
                                 'cekRJ'=>$cekRJ,
                                 'cekRI'=>$cekRI,
                                 'cekRD'=>$cekRD,
                ));
        }
        
        public function actionDaftarDariPasienRujukan($idPendaftaran,$idPasien,$idPasienAdmisi)
        {
            if(empty($idPasienAdmisi)){//Jika Pasien Bukan dari RI
                $modPendaftaran=LKPendaftaranT::model()->findByPk($idPendaftaran);
            }else{
                $modPendaftaran=LKPendaftaranT::model()->findByAttributes(array('pasienadmisi_id'=>$idPasienAdmisi));
                $modPasienAdmisi = LKPasienAdmisiT::model()->findByPk($idPasienAdmisi);
                $modPendaftaran->attributes = $modPasienAdmisi->attributes;
            }
            
            if(!empty($modPendaftaran->pasienadmisi_id)){
                $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                if(!empty($modPasienAdmisi->pasienpulang_id)){
                    $status = "SUDAH PULANG";
                }
            }else{
                $status = $modPendaftaran->statusperiksa;
            }
            
            if($status == "SUDAH PULANG"){
                echo "<script>
                        alert('Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan pemeriksaan ke laboratorium. ');
                        window.top.location.href='".Yii::app()->createUrl('laboratorium/PencarianPasien/index')."';
                    </script>";
            }
            
            $modPasien=LKPasienM::model()->findByPk($idPasien);
            $modPeriksaLab = LKPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true),array('order'=>'pemeriksaanlab_urutan'));
            $modJenisPeriksaLab = LKJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan'));
            $modPasienMasukPenunjang = new LKPasienMasukPenunjangT;
			$modRiwayatKirimKeUnitLain = new LKPasienKirimKeUnitLainT;
            //==== Rincian Untuk Print Label dan Print Daftar Pemeriksaan ===
            $modRincian = new LKRincianpemeriksaanlabradV;
            $modRincian = LKRincianpemeriksaanlabradV::model()->findAllByAttributes(array('pendaftaran_id' => $modPendaftaran->pendaftaran_id), array('order'=>'pemeriksaanlab_urutan'));
            //==== End Rincian ===
            if(!empty($_POST['PemeriksaanLab'])){
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    $modPasienMasukPenunjang = $this->savePasienMasukPenunjang($modPasien,$modPendaftaran);
					$this->saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienMasukPenunjang, $_POST['biayaAdministrasi']);
                    if ($this->successSavepasienKirimKeUnitLain && $this->successSaveHasilPemeriksaan
                        && $this->successSavePasienMasukPenunjang){
                            $transaction->commit();
//                            $transaction->rollback(); //TEST
                            Yii::app()->user->setFlash('success',"Data Berhasil Disimpan");
                            $this->redirect(array('index', "id"=>$idPendaftaran,"idPasienpenunjang"=>$modPasienMasukPenunjang->pasienmasukpenunjang_id,"caraPrint"=>"PRINT", 'status'=>1));
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan");
                            Yii::app()->user->setFlash('info',$this->errorMessages);
                        }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
          
            $this->render('daftarDariPasienRujukan',array(
                                 'modPeriksaLab'=>$modPeriksaLab,
                                 'modJenisPeriksaLab'=>$modJenisPeriksaLab,
                                 'modPendaftaran'=>$modPendaftaran, 
                                 'modPasien'=>$modPasien,
                                 'idPasienAdmisi'=>$idPasienAdmisi,
                                 'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
								 'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
                ));
        }
        
        protected function savePasienMasukPenunjang($modPasien,$modPendaftaran)
        {
            $modPasienMasukPenunjang = new LKPasienMasukPenunjangT;
            $modPasienMasukPenunjang->pasien_id = $modPasien->pasien_id;
            if(!empty($modPendaftaran->pasienadmisi_id)){ ////Jika Pasien Berasal dari Rawat Inap
                $modPasienMasukPenunjang->pasienadmisi_id=$modPendaftaran->pasienadmisi_id;
                $modPasienMasukPenunjang->pendaftaran_id=$modPendaftaran->pendaftaran_id;
                $modPasienMasukPenunjang->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
                $no_pendaftaran = $modPendaftaran->no_pendaftaran;
                $modPasienMasukPenunjang->statusperiksa=$modPendaftaran->statusperiksa;
            }else{ ////Jika Pasien berasal dari RJ ataw RD
                $modPasienMasukPenunjang->pendaftaran_id=$modPendaftaran->pendaftaran_id;
                $modPasienMasukPenunjang->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
                $no_pendaftaran = $modPendaftaran->no_pendaftaran;
                $modPasienMasukPenunjang->statusperiksa=$modPendaftaran->statusperiksa;
            }
            $modPasienMasukPenunjang->pegawai_id=$_POST['LKPasienMasukPenunjang']['pegawai_id'];
            $modPasienMasukPenunjang->kelaspelayanan_id= Params::KELASPELAYANAN_ID_TANPA_KELAS;
            $modPasienMasukPenunjang->ruangan_id = Params::RUANGAN_ID_LAB;
            $modPasienMasukPenunjang->no_masukpenunjang=Generator::noMasukPenunjang('LK');
            $modPasienMasukPenunjang->tglmasukpenunjang=date('Y-m-d H:i:s');
            $modPasienMasukPenunjang->no_urutperiksa=Generator::noAntrianPenunjang($no_pendaftaran, $modPasienMasukPenunjang->ruangan_id);
            $modPasienMasukPenunjang->kunjungan=$modPendaftaran->kunjungan;
            $modPasienMasukPenunjang->ruanganasal_id=$modPendaftaran->ruangan_id;
            if($modPasienMasukPenunjang->validate()){//Jika proses Palidasi Pasien Masuk Penunjang Berhasil
//               echo "<pre>";
//               print_r($modPasienMasukPenunjang->attributes);
//               exit;
                if($modPasienMasukPenunjang->save()){//Jika $modPasienMasukPenunjang Berhasil Disimpan
                     $this->successSavePasienMasukPenunjang=true;
                     //Yii::app()->user->setFlash('success',"Data Pasien Masuk Penunjang berhasil Disimpan.");
                     $this->savePasienKirimKeUnitLain($modPasien,$modPendaftaran,$modPasienMasukPenunjang);
                     $this->saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$_POST['PemeriksaanLab']);//Simpan Hasil Pemeriksaan
                }else{//Jika $modPasienMasukPenunjang Gagal Disimpan
                     $this->successSavePasienMasukPenunjang=false;
                     //Yii::app()->user->setFlash('error',"Data Pasien Masuk Pennunjang Gagal Disimpan.");
                     $this->errorMessages .= "Data Pasien Masuk Penunjang Gagal Disimpan.<br/>";
                }
            }else{//Jika proses Palidasi Pasien Masuk Penunjang Berhasil Gagal
               $this->successSavePasienMasukPenunjang=false;
               //Yii::app()->user->setFlash('error',"Data Pasien Masuk Pennunjang Tidak Tervalidasi.");
               $this->errorMessages .= "Data Pasien Masuk Penunjang Tidak Tervalidasi.</br>";
            }
            return $modPasienMasukPenunjang;
        }
        
        protected function savePasienKirimKeUnitLain($modPasien,$modPendaftaran,$modPasienMasukPenunjang)
        {
            $modPasienKirimKeUnitLain = new LKPasienKirimKeUnitLainT;
            $modPasienKirimKeUnitLain->kelaspelayanan_id=Params::KELASPELAYANAN_ID_TANPA_KELAS;
            $modPasienKirimKeUnitLain->instalasi_id=Params::INSTALASI_ID_LAB;
            $modPasienKirimKeUnitLain->pasien_id=$modPasien->pasien_id;
            $modPasienKirimKeUnitLain->pasienmasukpenunjang_id=$modPasienMasukPenunjang->pasienmasukpenunjang_id;
            $modPasienKirimKeUnitLain->ruangan_id= Params::RUANGAN_ID_LAB;
            $modPasienKirimKeUnitLain->pegawai_id= $_POST['LKPasienKirimKeUnitLainT']['pegawai_id'];;
            $modPasienKirimKeUnitLain->pendaftaran_id= $modPendaftaran->pendaftaran_id;
            $modPasienKirimKeUnitLain->nourut='001';
            $modPasienKirimKeUnitLain->tgl_kirimpasien= date('Y-m-d H:i:s');
            if($modPasienKirimKeUnitLain->validate()){
                if($modPasienKirimKeUnitLain->save()){
                       $this->successSavepasienKirimKeUnitLain=true;
					   LKPasienMasukPenunjangT::model()->updateByPk($modPasienMasukPenunjang->pasienmasukpenunjang_id, array('pasienkirimkeunitlain_id'=>$modPasienKirimKeUnitLain->pasienkirimkeunitlain_id));
                       //Yii::app()->user->setFlash('success',"Data Pasien Masuk Penunjang berhasil Disimpan.");
                }else{
                     $this->successSavepasienKirimKeUnitLain=false;
                     //Yii::app()->user->setFlash('error',"Data Pasien Kirim Ke Unit Lain Gagal Disimpan.");
                     $this->errorMessages .= "Data Pasien Kirim Ke Unit Lain Gagal Disimpan. <br/>";
                }
            }else{
                     $this->successSavepasienKirimKeUnitLain=false;
                     //Yii::app()->user->setFlash('error',"Data Pasien Kirim Ke Unit Lain Tidak Valid");
                     $this->errorMessages .= "Data Pasien Kirim Ke Unit Lain Tidak Valid. <br/>";
            }
            return $modPasienKirimKeUnitLain;
           
        }
        
        
        protected function saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$pemeriksaanLab)
        {
            foreach($pemeriksaanLab as $jenisKelompok => $value) {
                if($jenisKelompok == Params::LAB_PATOLOGI){
                    //echo 'Patologi Klinik <pre>'.print_r($value,1).'</pre>';
                    $modHasilPemeriksaan = new LKHasilPemeriksaanLabT;
                    $modHasilPemeriksaan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modHasilPemeriksaan->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                    $modHasilPemeriksaan->pasien_id = $modPasien->pasien_id;
                    $modHasilPemeriksaan->nohasilperiksalab = Generator::noHasilPemeriksaanLK();
                    $modHasilPemeriksaan->tglhasilpemeriksaanlab = date('Y-m-d H:i:s');
                    $modHasilPemeriksaan->hasil_kelompokumur = Generator::kelompokUmurNama($modPasien->tanggal_lahir);
                    $modHasilPemeriksaan->hasil_jeniskelamin = $modPasien->jeniskelamin;
                    $modHasilPemeriksaan->statusperiksahasil = Params::statusPeriksaLK(1);
                    if($modHasilPemeriksaan->save()){
                        $this->successSaveHasilPemeriksaan = true;
                        
                        foreach ($value as $pemeriksaanId => $dataPeriksa) {
                            $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                            $details = PemeriksaanlabdetM::model()->pemeriksaanDetail($modHasilPemeriksaan, $pemeriksaanId);
                            if (count($details) > 0)
                            {
                                foreach ($details as $k => $detail) {
                                    $modDetailHasilPemeriksaan = new LKDetailHasilPemeriksaanLabT;
                                    $modDetailHasilPemeriksaan->hasilpemeriksaanlab_id = $modHasilPemeriksaan->hasilpemeriksaanlab_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlabdet_id = $detail->pemeriksaanlabdet_id;
                                    $modDetailHasilPemeriksaan->pemeriksaanlab_id = $pemeriksaanId;
                                    $modDetailHasilPemeriksaan->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                                    $modDetailHasilPemeriksaan->save();
                                }
                            }
                            else{
                                $this->successSaveHasilPemeriksaan = false;
                                $this->errorMessages = 'Detail Pemeriksaan Laboratorium belum diisi';
                            }
                        }
                    } else 
                        $this->successSaveHasilPemeriksaan = false;
                    
                } else {
                    //echo 'Anatomi <pre>'.print_r($value,1).'</pre>';
                    foreach ($value as $pemeriksaanId => $dataPeriksa) {
                        $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                        $modHasilPemeriksaanPA = new LKHasilPemeriksaanPAT;
                        $modHasilPemeriksaanPA->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                        $modHasilPemeriksaanPA->pemeriksaanlab_id = $pemeriksaanId;
                        $modHasilPemeriksaanPA->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modHasilPemeriksaanPA->pasien_id = $modTindakanPelayananT->pasien_id;
                        $modHasilPemeriksaanPA->pendaftaran_id = $modTindakanPelayananT->pendaftaran_id;
                        $modHasilPemeriksaanPA->nosediaanpa = Generator::noSediaanPA();
                        $modHasilPemeriksaanPA->tglperiksapa = $modTindakanPelayananT->tgl_tindakan;
                        if($modHasilPemeriksaanPA->validate()){
                            if($modHasilPemeriksaanPA->save()){
                                 $this->successSaveHasilPemeriksaan=true;
                            }else{
                                 $this->successSaveHasilPemeriksaan=false;                   
                            }  
                        }
                    }
                }
            }
        }
        
        protected function saveTindakanPelayanan($modPendaftaran,$modPasien,$dataPemeriksaan, $modPasienMasukPenunjang)
        {
                $persenRujukanIn = Yii::app()->user->getState('persentasirujin');
                if($persenRujukanIn==0){
                    if(!empty($modPasienMasukPenunjang->pasienadmisi_id))
                        $persenRujukanIn = KelaspelayananM::model()->findByPk($modPendaftaran->kelaspelayanan_id)->persentasirujin;
                }
                $tarifRujukanIn = ($dataPemeriksaan['tarif_tindakan'] * $persenRujukanIn / 100);
                $modTindakanPelayananT = new LKTindakanPelayananT;
                $modTindakanPelayananT->penjamin_id = $modPendaftaran->penjamin_id;
                $modTindakanPelayananT->pasien_id = $modPasien->pasien_id;
                $modTindakanPelayananT->kelaspelayanan_id = Params::KELASPELAYANAN_ID_TANPA_KELAS;
                $modTindakanPelayananT->instalasi_id = Params::INSTALASI_ID_LAB;
                $modTindakanPelayananT->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayananT->daftartindakan_id = $dataPemeriksaan['daftartindakan_id'];
                $modTindakanPelayananT->carabayar_id = $modPendaftaran->carabayar_id;
                $modTindakanPelayananT->qty_tindakan = $dataPemeriksaan['qty_tindakan'];
                if(!empty($modPendaftaran->pasienadmisi_id)){ ////Jika Paien Berasal dari Rawat Inap
                    $modTindakanPelayananT->pasienadmisi_id = $modPendaftaran->pasienadmisi_id;
                    $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                }else{ ////Jika Pasien berasal dari RJ ataw RD
                    $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                }
                
                $modTindakanPelayananT->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayananT->tarif_satuan = $dataPemeriksaan['tarif_tindakan'] + $tarifRujukanIn;
                $modTindakanPelayananT->tarif_tindakan = $modTindakanPelayananT->tarif_satuan * $modTindakanPelayananT->qty_tindakan;
                $modTindakanPelayananT->satuantindakan = $dataPemeriksaan['satuantindakan'];
                $modTindakanPelayananT->cyto_tindakan = $dataPemeriksaan['cyto_tindakan'];
                $modTindakanPelayananT->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;                
                $modTindakanPelayananT->dokterpemeriksa2_id = $_POST['LKPasienKirimKeUnitLainT']['pegawai_id'];
                $modTindakanPelayananT->discount_tindakan = 0;
                $modTindakanPelayananT->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                $modTindakanPelayananT->tarifcyto_tindakan = 0;
                $modTindakanPelayananT->subsidiasuransi_tindakan = 0;
                $modTindakanPelayananT->subsidipemerintah_tindakan = 0;
                $modTindakanPelayananT->subsisidirumahsakit_tindakan = 0;
                $modTindakanPelayananT->iurbiaya_tindakan = 0;
                $modTindakanPelayananT->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayananT->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;

//                echo "DOkter 1".$modTindakanPelayananT->dokterpemeriksa1_id." Dokter 2".$modTindakanPelayananT->dokterpemeriksa2_id;exit;
                $tarifTindakan= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayananT->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayananT->kelaspelayanan_id.'');
                foreach($tarifTindakan as $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayananT->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayananT->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayananT->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayananT->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;

                if($modTindakanPelayananT->save()){//Jika Tindakan Pelayanan Berhasil tersimpan
                    $idPeriksaLab = $dataPemeriksaan['pemeriksaanlab_id'];
                    $jumlahTindKomponen = COUNT($_POST['LKTindakanKomponenT'][$idPeriksaLab]);
                    for($j=0; $j<$jumlahTindKomponen; $j++):
                        $modTindakanKomponen = new LKTindakanKomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['komponentarif_id'];
                        $modTindakanKomponen->tarif_tindakankomp = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'] * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'];
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                    endfor;
                        // -- menyimpan komponen jasa rujukan penunjang
//                        $modTindakanKomponen = new LKTindakanKomponenT;
//                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
//                        $modTindakanKomponen->komponentarif_id = Params::KOMPONENTARIF_ID_JASA_RUJPEN;
//                        $modTindakanKomponen->tarif_tindakankomp = $tarifRujukanIn * $modTindakanPelayananT->qty_tindakan;
//                        $modTindakanKomponen->tarif_kompsatuan = $tarifRujukanIn;
//                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
//                        $modTindakanKomponen->subsidiasuransikomp = 0;
//                        $modTindakanKomponen->subsidipemerintahkomp = 0;
//                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
//                        $modTindakanKomponen->iurbiayakomp = 0;
//                        $modTindakanKomponen->save();
                }
                
                return $modTindakanPelayananT;
        } 
		
		public function saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienMasukPenunjang, $tarifKarcisLab)
        {
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New LKTindakanPelayananT;
            $modTindakanPelayan->penjamin_id=$modPendaftaran->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
            $modTindakanPelayan->pasienmasukpenunjang_id=$modPasienMasukPenunjang->pasienmasukpenunjang_id;
            $modTindakanPelayan->kelaspelayanan_id=Params::KELASPELAYANAN_ID_TANPA_KELAS;
            $modTindakanPelayan->instalasi_id = Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id = Params::TARIF_ADMINISTRASI_LAB_ID;
            $modTindakanPelayan->carabayar_id = $modPendaftaran->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan = $tarifKarcisLab;
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$modPendaftaran->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            $modTindakanPelayan->ruangan_id = $modPendaftaran->ruangan_id;
             
            $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$modTindakanPelayan->daftartindakan_id, 'kelaspelayanan_id'=>$modTindakanPelayan->kelaspelayanan_id));
            foreach($tarifTindakan AS $dataTarif):
                 if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                        $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                        $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                        $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                        $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                    }
            endforeach;
            
            if($modTindakanPelayan->save()){ 
                   $tindakanKomponen= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayan->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                   $jumlahKomponen=COUNT($tindakanKomponen);

                   foreach ($tindakanKomponen AS $tampilKomponen):
                           $modTindakanKomponen=new LKTindakanKomponenT;
                           $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                           $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                           $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                           $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                           $modTindakanKomponen->tarifcyto_tindakankomp=0;
                           $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                           $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                           $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                           $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                           if($modTindakanKomponen->save()){
                               $cekTindakanKomponen++;
                           }
                   endforeach;
                   if($cekTindakanKomponen!=$jumlahKomponen){
                          $this->successSaveBiayaAdministrasi=false;
                    }else
                        $this->successSaveBiayaAdministrasi=true;
           } 
           
        }
        
        
    public function actionBatalPemeriksaan()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $transaction = Yii::app()->db->beginTransaction();
            $pesan = 'success';
            $status = 'ok';

            try{
                $idPendaftaran = $_POST['idPendaftaran'];

                /*
                 * cek data pendaftaran pasien masuk penunjang
                 */
                $pasienMasukPenunjang = PasienmasukpenunjangT::model()->findByAttributes(
                    array(
                        'pendaftaran_id'=>$idPendaftaran
                    )
                );

                $model = new PasienbatalperiksaR();
                $model->pendaftaran_id = $idPendaftaran;
                $model->pasien_id = $pasienMasukPenunjang->pasien_id;
                $model->tglbatal = date('Y-m-d');
                $model->keterangan_batal = "Batal Laboratorium";
                $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if(!$model->save())
                {
                    $status = 'not';
                }

                if($pasienMasukPenunjang->pasienkirimkeunitlain_id == null)
                {
                    $attributes = array(
                        'pasienbatalperiksa_id' => $model->pasienbatalperiksa_id,
                        'update_time' => date('Y-m-d H:i:s'),
                        'update_loginpemakai_id' => Yii::app()->user->id
                    );
                    $pendaftaran = LKPendaftaranT::model()->updateByPk($idPendaftaran, $attributes);
                    
                    $attributes = array(
                        'pasienkirimkeunitlain_id' => $pasienMasukPenunjang->pasienkirimkeunitlain_id
                    );
                    $Perminataan_penunjang = PermintaankepenunjangT::model()->deleteAllByAttributes($attributes);
                }

                $attributes = array(
                    'statusperiksa' => 'BATAL PERIKSA',
                    'update_time' => date('Y-m-d H:i:s'),
                    'update_loginpemakai_id' => Yii::app()->user->id
                );
                $penunjang = PasienmasukpenunjangT::model()->updateByPk($pasienMasukPenunjang->pasienmasukpenunjang_id, $attributes);
                if(!$penunjang)
                {
                    $status = 'not';
                }
                

                /*
                 * cek data tindakan_pelayanan
                 */
                $attributes = array(
                    'pasienmasukpenunjang_id' => $pasienMasukPenunjang->pasienmasukpenunjang_id,
                    'tindakansudahbayar_id' => null
                );
                $tindakan = LKTindakanPelayananT::model()->findAllByAttributes($attributes);
                if(count($tindakan) > 0)
                {
                    foreach($tindakan as $val=>$key)
                    {
                        $attributes = array(
                            'tindakanpelayanan_id' => $key->tindakanpelayanan_id
                        );
                        $hapus_det_tindakan = LKDetailHasilPemeriksaanLabT::model()->deleteAllByAttributes($attributes);
                    }
                    
                    $attributes = array(
                        'pasienmasukpenunjang_id' => $pasienMasukPenunjang->pasienmasukpenunjang_id
                    );
                    $hapus_tindakan = LKTindakanPelayananT::model()->deleteAllByAttributes($attributes);
                    if(!$hapus_tindakan)
                    {
                        $status = 'not';
                    }
                }else{
                    $pesan = 'exist';
                }

                /*
                 * kondisi_commit
                 */
                if($status == 'ok')
                {
    //                        $transaction->commit();
                }else{
                    $transaction->rollback();
                }

            }catch(Exception $ex){
                print_r($ex);
                $status = 'not';
                $transaction->rollback();
            }            
            $data = array(
                'pesan'=>'succes',
                'status'=>'ok'
            );
            echo json_encode($data);
            Yii::app()->end();            
        }
    }
        
}
