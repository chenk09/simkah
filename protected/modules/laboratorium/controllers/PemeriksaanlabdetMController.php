<?php

class PemeriksaanlabdetMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
        public $pathView = 'laboratorium.views.pemeriksaanlabdetM.'; 
	public $layout='//layouts/column1';
                public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','DeleteFromUpdate'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $data = LKPemeriksaanlabdetM::model()->findByPK($id);
                $model = new LKPemeriksaanlabdetM('search');
                $model->pemeriksaanlab_id = $data->pemeriksaanlab_id;
		$this->render($this->pathView.'view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new LKPemeriksaanlabdetM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LKPemeriksaanlabdetM']))
		{
                    $modDetails = $this->validasiTabular($model,$_POST['LKPemeriksaanlabdetM']);
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $jumlahsave = 0;
                        foreach ($modDetails as $j=>$row)
                        {
                            if ($row->save()) {$jumlahsave++;}
                        }
                        if ($jumlahsave == COUNT($_POST['LKPemeriksaanlabdetM'])) {
                        	// echo '<pre>'.print_r($modDetails);exit();
                            $transaction->commit();
                            Yii::app()->user->setFlash('success','<strong>Berhasil</strong> Data berhasil disimpan');
                        } else {
                            throw new Exception('error');
                        }
                    }
                    catch (Exception $ex) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($ex));
                    }
                    $this->redirect(array('admin'));
		}

                    $this->render($this->pathView.'create',array(
                                'model'=>$model,
                                'modDetails'=>$modDetails,
		));
	}
                
         protected function validasiTabular($model, $data) {
            foreach ($data as $i=>$row) {
                if(is_array($row)){
                $modDetails[$i] = new LKPemeriksaanlabdetM;
                $modDetails[$i]->pemeriksaanlab_id = $_POST['pemeriksaanlab_id'];
                $modDetails[$i]->attributes = $row;
                $modDetails[$i]->validate();
                           // echo '<pre>'.print_r($modDetails[$i]->attributes);
                }
            }
    //                    echo count($modDetails);
                       // exit();
            return $modDetails;
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
		// $model= new LKPemeriksaanlabdetM;
        $modDetails = LKPemeriksaanlabdetM::model()->findAllByAttributes(array('pemeriksaanlab_id'=>$model->pemeriksaanlab_id));
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LKPemeriksaanlabdetM']))
		{
                    $modDetails = $this->validasiTabular($model,$_POST['LKPemeriksaanlabdetM']);
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $jumlahsave = 0;
                        PemeriksaanlabdetM::model()->deleteAllByAttributes(array('pemeriksaanlab_id'=>$_POST['pemeriksaanlab_id']));

                        foreach ($modDetails as $j=>$row)
                        {
                            if ($row->save()) {$jumlahsave++;}
                        }

                        if ($jumlahsave==COUNT($_POST['LKPemeriksaanlabdetM'])) {
                            $transaction->commit();
                            Yii::app()->user->setFlash('success','<strong>Berhasil</strong> Data berhasil disimpan');
                            $this->redirect(array('admin'));
                        } 
                        else {
                            $transaction->rollback();
                            // throw new Exception('error');
                        }
                    }
                    catch (Exception $ex) {
                        $transaction->rollback();

                        Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($ex));
                    }
		}

                    $this->render($this->pathView.'update',array(
                                'model'=>$model,
                                'modDetails'=>$modDetails
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
                $id = $_POST['id'];
                $detailHasilPemeriksaan = DetailhasilpemeriksaanlabT::model()->findByAttributes(array('pemeriksaanlabdet_id'=>$id));
                if ($detailHasilPemeriksaan){
                        	echo CJSON::encode(array(
                                	'status'=>'error',
                                ));
                            exit();
                }else{
                    $this->loadModel($id)->delete();
                    if (Yii::app()->request->isAjaxRequest)
                        {
                            echo CJSON::encode(array(
                                'status'=>'proses_form', 
                                'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                                ));
                            exit();               
                        }
                }
	                    
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
        
	public function actionDeleteFromUpdate($link,$id)
	{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			// $this->loadModel($id)->delete();
   //                      $this->redirect($link==$id ? array('admin') : array("update&id=$link"));
		$detailHasilPemeriksaan = DetailhasilpemeriksaanlabT::model()->findByAttributes(array('pemeriksaanlabdet_id'=>$id));
        if ($detailHasilPemeriksaan){
                	echo "<script>
                		alert('Data gagal dihapus karena data digunakan oleh Hasil Pemeriksaan Lab.');
                		window.top.location.href='".Yii::app()->createUrl('laboratorium/pemeriksaanlabdetM/update&id='.$id)."';
                	</script>";
        }else{
            $this->loadModel($id)->delete();
          	$this->redirect($link==$id ? array('admin') : array("update&id=$link"));
        }
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('LKPemeriksaanlabdetM');
		$this->render($this->pathView.'index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new LKPemeriksaanlabdetM('search');
        $modPemeriksaanLab = new LKPemeriksaanLabM();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['LKPemeriksaanlabdetM'])){
			$model->attributes=$_GET['LKPemeriksaanlabdetM'];
			$model->pemeriksaanlab_nama=$_GET['LKPemeriksaanlabdetM']['pemeriksaanlab_nama'];
		}
		$this->render($this->pathView.'admin',array(
			'model'=>$model,
			'modPemeriksaanLab'=>$modPemeriksaanLab,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=LKPemeriksaanlabdetM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pemeriksaanlabdet-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            // $model= new LKPemeriksaanlabdetM;
            // $model->attributes=$_REQUEST['LKPemeriksaanlabdetM'];
			$model=new LKPemeriksaanlabdetM;
	        $modPemeriksaanLab = new LKPemeriksaanLabM();
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['LKPemeriksaanlabdetM'])){
				$model->attributes=$_GET['LKPemeriksaanlabdetM'];
				$model->pemeriksaanlab_nama=$_GET['LKPemeriksaanlabdetM']['pemeriksaanlab_nama'];
			}            
            $judulLaporan='Data LKPemeriksaanlabdetM';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
