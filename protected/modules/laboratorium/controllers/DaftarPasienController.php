<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class DaftarPasienController extends SBaseController
{
        public $successPengambilanSample = false;
        public $successKirimSample = false;
        public $pathView = 'laboratorium.views.daftarPasien.';
        /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','actionPrint','updateSample','hasilPemeriksaan','print','ubahPasien','statusPeriksa'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('BatalPeriksaPasienLuar'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex()
	{
            // //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));} 
                $modPasienMasukPenunjang = new LKPasienMasukPenunjangV;
                $format = new CustomFormat();
                $modPasienMasukPenunjang->statusperiksahasil = 'SEDANG';
//                $modPasienMasukPenunjang->tglAwal=date('d M Y').' 00:00:00';
                $modPasienMasukPenunjang->tglAwal=date('d M Y', strtotime('-5 days')).' 00:00:00';
                $modPasienMasukPenunjang->tglAkhir=date('d M Y H:i:s');
                if(isset ($_REQUEST['LKPasienMasukPenunjangV'])){
                    $modPasienMasukPenunjang->attributes=$_REQUEST['LKPasienMasukPenunjangV'];
                    $modPasienMasukPenunjang->statusperiksahasil = $_REQUEST['LKPasienMasukPenunjangV']['statusperiksahasil'];
                    $modPasienMasukPenunjang->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKPasienMasukPenunjangV']['tglAwal']);
                    $modPasienMasukPenunjang->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKPasienMasukPenunjangV']['tglAkhir']);
                }
		$this->render('index',array('modPasienMasukPenunjang'=>$modPasienMasukPenunjang));
	}
        
        public function actionUpdateSample($idPengambilanSample,$idPasienMasukPenunjang,$idPendaftaran)
        {
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));} 
            $format = new CustomFormat();
            $modPasienMasukPenunjang=LKPasienMasukPenunjangV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            if($idPengambilanSample==''){//Jika Belum Mengisi Tabel Pengambilasan Sample
                $modPengambilanSample = new LKPengambilanSampleT;
                $modPengambilanSample->no_pengambilansample = Generator::noPengambilanSample();
                $modKirimSample = new LKKirimSampleLabT;
            }else{//Jika Sudah Mengisi Tabel Pengambilan Sample
                $modPengambilanSample=LKPengambilanSampleT::model()->findByPk($idPengambilanSample);
                $cekKirimSample=LKKirimSampleLabT::model()->findByPk($modPengambilanSample->kirimsamplelab_id);
                if(COUNT($cekKirimSample)>0){//Jika Sample Sudah Pernah Dikirim
                    $modKirimSample=$cekKirimSample;
                    $modKirimSample->isKirimSample=TRUE;
                    $modKirimSample->tglkirimsample = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-MM-dd hh:mm:ss'));
                }else{//Jika Sample Belum Dikirm
                    $modKirimSample=new LKKirimSampleLabT;
                    $modKirimSample->isKirimSample=FALSE;

                }
            }
                $modPengambilanSample->tglpengambilansample = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-MM-dd hh:mm:ss'));
           
            
            if(isset($_POST['LKPengambilanSampleT'])){
                $transaction = Yii::app()->db->beginTransaction();
                try{ 
                    if(!empty($_POST['isKirimSample'])){//Jika User MengisiKan Kirim Sample

                       $modKirimSample->attributes=$_POST['LKKirimSampleLabT'];
                       $modKirimSample->tglkirimsample=$format->formatDateTimeMediumForDB($_POST['LKKirimSampleLabT']['tglkirimsample']);
                       $modKirimSample->save();
                       if($modKirimSample->validate()){
                            $modKirimSample->save();
                            $modPengambilanSample->kirimsamplelab_id=$modKirimSample->kirimsamplelab_id;
                            $this->successKirimSample = TRUE;
                        }
                    }else{
                       $this->successKirimSample = TRUE;                        
                    }
                        
                        $modPengambilanSample->attributes=$_POST['LKPengambilanSampleT'];
                        $modPengambilanSample->pasienmasukpenunjang_id=$idPasienMasukPenunjang;
                        $modPengambilanSample->tglpengambilansample= $format->formatDateTimeMediumForDB($_POST['LKPengambilanSampleT']['tglpengambilansample']);
                        if($modPengambilanSample->validate()){
                            $modPengambilanSample->save();
                            LKKirimSampleLabT::model()->updateByPk($modKirimSample->kirimsamplelab_id,array('pengambilansample_id'=>$modPengambilanSample->pengambilansample_id));
                            $this->successPengambilanSample = TRUE;                        
                            //Update status periksa
                            $modHasilpemeriksaanLab=LKHasilPemeriksaanLabT::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang, 'pendaftaran_id'=>$idPendaftaran));    
                            $modHasilpemeriksaanLab->statusperiksahasil=Params::statusPeriksaLK(2); // SEDANG
//                            $modHasilpemeriksaanLab->statusperiksahasil=Params::statusPeriksaLK(1); // BELUM
                            $modHasilpemeriksaanLab->update();
                        }
                        
                        if($this->successKirimSample && $this->successPengambilanSample){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data berhasil Disimpan");
                            $this->redirect(array('index'));
                        }
                }catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            $this->render('updateSample',array('modPengambilanSample'=>$modPengambilanSample,
                                               'modKirimSample'=>$modKirimSample,
                                               'modPasienMasukPenunjang'=>$modPasienMasukPenunjang));
        }
        
        
        
        public function actionHasilPemeriksaan($idPendaftaran,$idPasien,$idPasienMasukPenunjang)
        {
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));} 
            $modPasienMasukPenunjang=LKPasienMasukPenunjangV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $modPasienKirimKeUnitLain=LKPasienKirimKeUnitLainT::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $format = new CustomFormat();
            $modHasilpemeriksaanLab = LKHasilPemeriksaanLabT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                    'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang,
                                                    'pasien_id'=>$idPasien));
            //Criteria untuk menampilkan detail hasil pemeriksaan berdasarkan jenis kelamin dan kelompok umur
            $criteria = new CDbCriteria();
            $kelompokUmur = (strtolower($modPasienMasukPenunjang->golonganumur_nama)) == 'bayi' ? 'dewasa' : 'dewasa';
            $criteria->together = true;
            $criteria->with = array('pemeriksaanlab', 'pemeriksaandetail', 'pemeriksaandetail.nilairujukan');
            $criteria->compare('hasilpemeriksaanlab_id', $modHasilpemeriksaanLab->hasilpemeriksaanlab_id);
            //---- Saat masukan hasil gagal simpan karena jenis kelamin
            //$criteria->compare('LOWER(nilairujukan_jeniskelamin)', strtolower($modPasienMasukPenunjang->jeniskelamin));
            $criteria->compare('LOWER(kelompokumur)', strtolower($kelompokUmur));
            $criteria->order = "pemeriksaanlab_urutan, pemeriksaanlabdet_nourut ASC";
            $modDetailHasilPemeriksaanLab =  LKDetailHasilPemeriksaanLabT::model()->findAll($criteria);
//            echo $kelompokUmur;exit;
//            echo $modDetailHasilPemeriksaanLab[0]->pemeriksaandetail->nilairujukan->kelompokumur;exit;

            // echo $modPasienMasukPenunjang->golonganumur_nama;exit();
//            $modDetailHasilPemeriksaanLab =  LKDetailHasilPemeriksaanLabT::model()->with('pemeriksaanlab','pemeriksaandetail','pemeriksaandetail.nilairujukan')->findAllByAttributes(array('hasilpemeriksaanlab_id'=>$modHasilpemeriksaanLab->hasilpemeriksaanlab_id), array('order'=>'pemeriksaanlab_urutan, pemeriksaanlabdet_nourut'));
            $modNilaiRujukan = LKNilaiRujukanM::model()->findByAttributes(array('kelompokumur'=>strtoupper($modHasilpemeriksaanLab->hasil_kelompokumur),
                                                                                'nilairujukan_jeniskelamin'=>strtoupper($modHasilpemeriksaanLab->hasil_jeniskelamin)));
            $modHasilPemeriksaanPA=LKHasilPemeriksaanPAT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                            'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang,
                                                                                            'pasien_id'=>$idPasien));
            $modHasilpemeriksaanLab->tglpengambilanhasil = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modHasilpemeriksaanLab->tglpengambilanhasil, 'yyyy-MM-dd hh:mm:ss'));
            $modPendaftaran = LKPendaftaranMp::model()->findByPk($idPendaftaran);
            //$modRujukanT = LKRujukanT::model()->findByPk($modPendaftaran->rujukan_id);

            //jika belum ada hasil/pemeriksaan, maka input/pilih dulu pemeriksaannya
            if(empty($modDetailHasilPemeriksaanLab)){
                Yii::app()->user->setFlash('error',"Kelompok Umur ".$kelompokUmur." tidak ada dalam nilai rujukan, silahkan diinput terlebih dahulu");
                $criteria = new CDbCriteria();
                $kelompokUmur = 'dewasa';
                $criteria->together = true;
                $criteria->with = array('pemeriksaanlab', 'pemeriksaandetail', 'pemeriksaandetail.nilairujukan');
                $criteria->compare('hasilpemeriksaanlab_id', $modHasilpemeriksaanLab->hasilpemeriksaanlab_id);
                $criteria->compare('LOWER(nilairujukan_jeniskelamin)', strtolower($modPasienMasukPenunjang->jeniskelamin));
                $criteria->compare('LOWER(kelompokumur)', strtolower($kelompokUmur));
                $criteria->order = "pemeriksaanlab_urutan, pemeriksaanlabdet_nourut ASC";
                $modDetailHasilPemeriksaanLab =  LKDetailHasilPemeriksaanLabT::model()->findAll($criteria);
                // $this->redirect($this->createUrl('InputPemeriksaan/Update',array('idPendaftaran'=>$idPendaftaran,
                //     'idPasienMasukPenunjang'=>$idPasienMasukPenunjang,
                //             'modulId'=>Yii::app()->session['modulId'])));
            }
            if(isset($_POST['LKRujukanT'])){ // Update Dokter Perujuk pada RujukanT
                $modRujukanT->rujukandari_id = $_POST['LKRujukanT']['rujukandari_id'];
                $modRujukanDari = RujukandariM::model()->findByPk($modRujukanT->rujukandari_id);
                $modRujukanT->nama_perujuk = $modRujukanDari->namaperujuk;
                $modRujukanT->update();
            }
            if(isset($_POST['LKDetailHasilPemeriksaanLabT'])){

                $transaction = Yii::app()->db->beginTransaction();
                try{
                        $jumlahDetalHasilPemesiksaan=COUNT($_POST['LKDetailHasilPemeriksaanLabT']['detailhasilpemeriksaanlab_id']);

                        for($j=0; $j<$jumlahDetalHasilPemesiksaan; $j++):
                           $idHasilPemeriksaan=$_POST['LKDetailHasilPemeriksaanLabT']['detailhasilpemeriksaanlab_id'][$j]; 
                           $modDetailHasilPemeriksaanLab=LKDetailHasilPemeriksaanLabT::model()->findByPk($idHasilPemeriksaan);
                           $modDetailHasilPemeriksaanLab->hasilpemeriksaan=$_POST['LKDetailHasilPemeriksaanLabT']['hasilpemeriksaan'][$j];
                           $modDetailHasilPemeriksaanLab->nilairujukan=$_POST['LKDetailHasilPemeriksaanLabT']['nilairujukan'][$j];
                           $modDetailHasilPemeriksaanLab->hasilpemeriksaan_satuan=$_POST['LKDetailHasilPemeriksaanLabT']['hasilpemeriksaan_satuan'][$j];
                           $modDetailHasilPemeriksaanLab->hasilpemeriksaan_metode=$_POST['LKDetailHasilPemeriksaanLabT']['hasilpemeriksaan_metode'][$j];
                           $modDetailHasilPemeriksaanLab->is_print = $_POST['LKDetailHasilPemeriksaanLabT']['is_print'][$j];
                           $modDetailHasilPemeriksaanLab->update();
                        endfor;
                        
                    $modHasilpemeriksaanLab=LKHasilPemeriksaanLabT::model()->findByPk($_POST['LKHasilPemeriksaanLabT']['hasilpemeriksaanlab_id']);    
                    $modHasilpemeriksaanLab->catatanlabklinik=$_POST['LKHasilPemeriksaanLabT']['catatanlabklinik'];
                    $modHasilpemeriksaanLab->tglpengambilanhasil= $format->formatDateTimeMediumForDB($_POST['LKHasilPemeriksaanLabT']['tglpengambilanhasil']);
                    $modHasilpemeriksaanLab->statusperiksahasil=Params::statusPeriksaLK(2);
//                    $modHasilpemeriksaanLab->printhasillab=false;
                    $modHasilpemeriksaanLab->update();
                    
                    if(!empty($_POST['LKHasilPemeriksaanPAT']))
                        $this->saveHasilPemeriksaan($_POST['LKHasilPemeriksaanPAT']);
                    //Update dokter pemeriksa (pegawai_id) pada pasien masuk penunjang
                    LKPasienMasukPenunjangT::model()->updateByPk($idPasienMasukPenunjang,array('pegawai_id'=>$_POST['LKPasienMasukPenunjangT']['pegawai_id']));
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data Hasil Pemeriksaan berhasil Disimpan");
//                    $this->refresh();
//                    $this->redirect($this->createUrl("index")); 
                    $this->redirect($this->createUrl("/laboratorium/daftarPasien/Details", array('idPendaftaran'=>$idPendaftaran,'idPasien'=>$idPasien,'idPasienMasukPenunjang'=>$idPasienMasukPenunjang,'caraPrint'=>'PRINT'))); 
                }catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            $this->render('hasilPemeriksaan',array('modHasilpemeriksaanLab'=>$modHasilpemeriksaanLab,
                                                   'modNilaiRujukan'=>$modNilaiRujukan,
                                                   'modDetailHasilPemeriksaanLab'=>$modDetailHasilPemeriksaanLab,
                                                   'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                                                   'modHasilPemeriksaanPA'=>$modHasilPemeriksaanPA,
                                                   'modPasienKirimKeUnitLain'=>$modPasienKirimKeUnitLain,
                                                   'modRincian'=>$modRincian,
                                                   'modRujukanT'=>$modRujukanT,));

        }
        public function actionApprovePemeriksaan($idPasienMasukPenunjang,$idPendaftaran)
        {
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));} 
            $modHasilpemeriksaanLab=LKHasilPemeriksaanLabT::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang, 'pendaftaran_id'=>$idPendaftaran));    
            $modHasilpemeriksaanLab->statusperiksahasil=Params::statusPeriksaLK(3); // SUDAH
        
            if ($modHasilpemeriksaanLab->update())
                Yii::app()->user->setFlash('success',"Pemeriksaan Berhasil Di Approve !");
            else
                Yii::app()->user->setFlash('error',"Pemeriksaan Gagal Di Approve !");
            $this->redirect(array('index'));
        }

        protected function saveHasilPemeriksaan($arrHasil)
        {
            foreach($arrHasil as $i => $hasil) {
                LKHasilPemeriksaanPAT::model()->updateByPk($hasil['hasilpemeriksaanpa_id'], 
                                                            array('makroskopis'=>$hasil['makroskopis'],
                                                                  'mikroskopis'=>$hasil['mikroskopis'],
                                                                  'catatanpa'=>$hasil['catatanpa'],
                                                                  'saranpa'=>$hasil['saranpa'],));
            }
        }

        public function actionCancelPemeriksaan($idPasienMasukPenunjang,$idPendaftaran)
        {
            $modHasilpemeriksaanLab=LKHasilPemeriksaanLabT::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang, 'pendaftaran_id'=>$idPendaftaran));    
            $modHasilpemeriksaanLab->statusperiksahasil=Params::statusPeriksaLK(2); // SEDANG
            if ($modHasilpemeriksaanLab->update())
                Yii::app()->user->setFlash('success',"Pemeriksaan Berhasil Dibatalkan !");
            else
                Yii::app()->user->setFlash('error',"Pemeriksaan Gagal Di Dibatalkan !");
            $this->redirect(array('index'));
        }
        
        
	public function actionDetails($idPendaftaran, $idPasien, $idPasienMasukPenunjang)
	{
            $this->layout = '//layouts/frameDialog';

            $cek_penunjang = LKPasienMasukPenunjangV::model()->findAllByAttributes(
                array('pendaftaran_id'=>$idPendaftaran)
            );
            
            $data_rad = array();
            if(count($cek_penunjang) > 1)
            {
                $masukpenunjangRad = LKPasienMasukPenunjangV::model()->findByAttributes(
                    array(
                        'pendaftaran_id'=>$idPendaftaran,                        
                        'ruangan_id'=>Params::RUANGAN_ID_RAD
                    )
                );

                $modHasilPeriksaRad = HasilpemeriksaanradV::model()->findAllByAttributes(
                    array(
                        'pasienmasukpenunjang_id'=>$masukpenunjangRad->pasienmasukpenunjang_id
                    ),
                    array(
                        'order'=>'pemeriksaanrad_urutan'
                    )
                );
                
                foreach($modHasilPeriksaRad as $i=>$val)
                {
                    $data_rad[] = array(
                        'pemeriksaan'=>$val['pemeriksaanrad_nama'],
                        'hasil'=>'Hasil terlampir'
                    );
                }
                    
            }
            
            $masukpenunjang = LKPasienMasukPenunjangV::model()->findByAttributes(
                array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang)
            );
            
            
            $pemeriksa = PegawaiM::model()->findByPk($masukpenunjang->pegawai_id);
            
            $modHasilPeriksa = HasilpemeriksaanlabV::model()->findByAttributes(
                array(
                    'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang
                )
            );
            
            /**
             * Ditambahkan oleh MIranitha Fasha
             * Issue    : EHJ-2934
             * Date     : 25 September 2014 
             */
                $kelompokumur = 'DEWASA';
                
            //SINTAKS AWAL
            // $query1 = "
            //     SELECT * FROM detailhasilpemeriksaanlab_t 
            //     JOIN pemeriksaanlab_m ON detailhasilpemeriksaanlab_t.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id 
            //     JOIN pemeriksaanlabdet_m ON detailhasilpemeriksaanlab_t.pemeriksaanlabdet_id = pemeriksaanlabdet_m.pemeriksaanlabdet_id 
            //     JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
            //     JOIN nilairujukan_m ON nilairujukan_m.nilairujukan_id = pemeriksaanlabdet_m.nilairujukan_id
            //     WHERE detailhasilpemeriksaanlab_t.hasilpemeriksaanlab_id = '". $modHasilPeriksa->hasilpemeriksaanlab_id ."'
            //     ORDER BY jenispemeriksaanlab_m.jenispemeriksaanlab_urutan,pemeriksaanlab_urutan, pemeriksaanlabdet_nourut, kelompokdet
            // ";

            //UNTUK MELAKUKAN PERUBAHAN ORDER BY MOHON DIKONFIRMASI TERLEBIH DAHULU
            //BY RAFA - 6 FEB 2014
            //ok
            
            /**
             * Dicoment oleh Miranitha Fasha
             * Issue    : EHJ-2934
             * Date     : 25 September 2014 10:07 AM
             */
//             $query = "
//                SELECT * FROM detailhasilpemeriksaanlab_t 
//                JOIN pemeriksaanlab_m ON detailhasilpemeriksaanlab_t.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id 
//                JOIN pemeriksaanlabdet_m ON detailhasilpemeriksaanlab_t.pemeriksaanlabdet_id = pemeriksaanlabdet_m.pemeriksaanlabdet_id 
//                JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
//                JOIN nilairujukan_m ON nilairujukan_m.nilairujukan_id = pemeriksaanlabdet_m.nilairujukan_id
//                WHERE nilairujukan_m.kelompokumur = 'DEWASA' and detailhasilpemeriksaanlab_t.hasilpemeriksaanlab_id = '". $modHasilPeriksa->hasilpemeriksaanlab_id ."' AND is_print is TRUE
//                ORDER BY pemeriksaanlab_urutan, jenispemeriksaanlab_m.jenispemeriksaanlab_urutan, pemeriksaanlabdet_nourut, kelompokdet
//            ";
             /**
             * Diubah oleh Miranitha Fasha
             * Issue    : EHJ-2934
             * Date     : 25 September 2014 10:07 AM
             * Description : untuk menampilkan hasil referensisesuai golongan umur pasien
             */
             $query = "
                SELECT * FROM detailhasilpemeriksaanlab_t 
                JOIN pemeriksaanlab_m ON detailhasilpemeriksaanlab_t.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id 
                JOIN pemeriksaanlabdet_m ON detailhasilpemeriksaanlab_t.pemeriksaanlabdet_id = pemeriksaanlabdet_m.pemeriksaanlabdet_id 
                JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
                JOIN nilairujukan_m ON nilairujukan_m.nilairujukan_id = pemeriksaanlabdet_m.nilairujukan_id
                WHERE nilairujukan_m.kelompokumur ILIKE '%".$kelompokumur."%' and detailhasilpemeriksaanlab_t.hasilpemeriksaanlab_id = '". $modHasilPeriksa->hasilpemeriksaanlab_id ."' AND is_print is TRUE
                ORDER BY pemeriksaanlab_urutan, jenispemeriksaanlab_m.jenispemeriksaanlab_urutan, pemeriksaanlabdet_nourut, kelompokdet
            ";
            //DIEDIT OLEH ANDRE 23 SEP 2014
            $detailHasil = Yii::app()->db->createCommand($query)->queryAll();
            
            $data = array();
            $kelompokDet = null;
            $idx = 0;
            $temp = '';

            foreach ($detailHasil as $i => $detail)
            {
                $id_jenisPeriksa = $detail['jenispemeriksaanlab_id'];
                $jenisPeriksa = $detail['jenispemeriksaanlab_nama'];
                $kelompokDet = $detail['kelompokdet'];
                if($id_jenisPeriksa == '72')
                {
                    $query = "
                        SELECT jenispemeriksaanlab_m.* FROM pemeriksaanlabdet_m
                        JOIN pemeriksaanlab_m ON pemeriksaanlabdet_m.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id
                        JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
                        WHERE nilairujukan_id = ". $detail['nilairujukan_id'] ." AND pemeriksaanlab_m.jenispemeriksaanlab_id <> ". $id_jenisPeriksa ."
                    ";
                    $rec = Yii::app()->db->createCommand($query)->queryRow();
                    $id_jenisPeriksa = $rec['jenispemeriksaanlab_id'];
                    $jenisPeriksa = $rec['jenispemeriksaanlab_nama'];
                }
                
                if($temp != $kelompokDet)
                {
                    $idx = 0;
                }
                
                $data[$id_jenisPeriksa]['tittle'] = $jenisPeriksa;
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['id'] = $id_jenisPeriksa;
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['nama'] = $jenisPeriksa;
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['kelompok'] = $kelompokDet;                
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['kelompok'] = $kelompokDet;
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan_det'] = $detail['pemeriksaanlab_nama'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan'] = $detail['namapemeriksaandet'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['id_pemeriksaan'] = $detail['nilairujukan_id'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['normal'] = $detail['nilairujukan_nama'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['metode'] = $detail['nilairujukan_metode'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['hasil'] = $detail['hasilpemeriksaan'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['nilairujukan'] = $detail['nilairujukan'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['satuan'] = $detail['hasilpemeriksaan_satuan'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['keterangan'] = $detail['nilairujukan_keterangan'];
                $temp = $kelompokDet;
                $idx++;
            }

            $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            if(substr($modPendaftaran->no_pendaftaran, 0, 2) != 'LB'){
                $pasienKirimKeunitLain = PasienkirimkeunitlainT::model()->findByAttributes(array(
                    'pasienmasukpenunjang_id'=>$masukpenunjang->pasienmasukpenunjang_id,
                ));
                $modPegawai = PegawaiM::model()->findByPk($pasienKirimKeunitLain->pegawai_id);
                $masukpenunjang->namaperujuk = $modPegawai->nama_pegawai;// . ', ' . $modPegawai->gelarbelakang->gelarbelakang_nama;
                $masukpenunjang->alamatlengkapperujuk = 'RS Jasa Kartini Tasikmalaya';
            }
			
            $enableprint = true;
			// Kondisi Print FALSE (Jika pasien berasal dari pendaftaran luar dengan carabayar membayar yang belum bayar) ATAU (jika pasien rawat jalan yang carabayarnya umum yang belum bayar)
//			if(empty($modPendaftaran->pembayaranpelayanan_id)){
//				if((($modPendaftaran->pasien->ispasienluar)&&($modPendaftaran->carabayar_id==Params::DEFAULT_CARABAYAR)) || (($modPendaftaran->instalasi_id==Params::INSTALASI_ID_RJ)&&($modPendaftaran->carabayar_id == Params::DEFAULT_CARABAYAR))){
//					$enableprint = false;
//				}
//			}
//			
			// Kondisi Print FALSE Jika pasien berasal dari pendaftaran luar dengan carabayar membayar yang belum bayar
			if(empty($modPendaftaran->pembayaranpelayanan_id)){
				if(($modPendaftaran->pasien->ispasienluar)&&($modPendaftaran->carabayar_id==Params::DEFAULT_CARABAYAR)){
					$enableprint = false;
				}
			}
			
            $this->render('details',
                array(
                   'modHasilPeriksa'=>$modHasilPeriksa,
                   'masukpenunjang'=>$masukpenunjang,
                   'pemeriksa'=>$pemeriksa,
                   'data'=>$data,
                   'data_rad'=>$data_rad,
				   'enableprint'=>$enableprint,
                )
            );
	}
        
        public function actionPrintHasil($idPasienMasukPenunjang, $idPendaftaran, $caraPrint)
        {
            $cek_penunjang = LKPasienMasukPenunjangV::model()->findAllByAttributes(
                array('pendaftaran_id'=>$idPendaftaran)
            );
            
            $data_rad = array();
            if(count($cek_penunjang) > 1)
            {
                $masukpenunjangRad = LKPasienMasukPenunjangV::model()->findByAttributes(
                    array(
                        'pendaftaran_id'=>$idPendaftaran,                        
                        'ruangan_id'=>Params::RUANGAN_ID_RAD
                    )
                );
                
                $modHasilPeriksaRad = HasilpemeriksaanradV::model()->findAllByAttributes(
                    array(
                        'pasienmasukpenunjang_id'=>$masukpenunjangRad->pasienmasukpenunjang_id
                    ),
                    array(
                        'order'=>'pemeriksaanrad_urutan'
                    )
                );
                
                foreach($modHasilPeriksaRad as $i=>$val)
                {
                    $data_rad[] = array(
                        'pemeriksaan'=>$val['pemeriksaanrad_nama'],
                        'hasil'=>'Hasil terlampir'
                    );
                }
                    
            }            
            
            $masukpenunjang = LKPasienMasukPenunjangV::model()->findByAttributes(
                array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang)
            );
            $pemeriksa = PegawaiM::model()->findByPk($masukpenunjang->pegawai_id);
            
            $modHasilPeriksa = HasilpemeriksaanlabV::model()->findByAttributes(
                array(
                    'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang
                )
            );
            
            /**
             * Ditambahkan oleh MIranitha Fasha
             * Issue    : EHJ-2934
             * Date     : 25 September 2014 
             */
                $kelompokumur = 'DEWASA';
            
            /**
             * Dicoment oleh Miranitha Fasha
             * Issue :  EHJ-2934
             * Date  : 25 September 2014
             */
//            $query = "
//                SELECT * FROM detailhasilpemeriksaanlab_t 
//                JOIN pemeriksaanlab_m ON detailhasilpemeriksaanlab_t.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id 
//                JOIN pemeriksaanlabdet_m ON detailhasilpemeriksaanlab_t.pemeriksaanlabdet_id = pemeriksaanlabdet_m.pemeriksaanlabdet_id 
//                JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
//                JOIN nilairujukan_m ON nilairujukan_m.nilairujukan_id = pemeriksaanlabdet_m.nilairujukan_id
//                WHERE detailhasilpemeriksaanlab_t.hasilpemeriksaanlab_id = '". $modHasilPeriksa->hasilpemeriksaanlab_id ."' AND is_print is TRUE
//                ORDER BY jenispemeriksaanlab_m.jenispemeriksaanlab_urutan,pemeriksaanlab_urutan, pemeriksaanlabdet_nourut, kelompokdet
//            ";
            /**
             * diganti oleh Miranitha Fasha
             * Issue    : EHJ-2934
             * Date     : 25 September 2014
             */
            $query = "
                SELECT * FROM detailhasilpemeriksaanlab_t 
                JOIN pemeriksaanlab_m ON detailhasilpemeriksaanlab_t.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id 
                JOIN pemeriksaanlabdet_m ON detailhasilpemeriksaanlab_t.pemeriksaanlabdet_id = pemeriksaanlabdet_m.pemeriksaanlabdet_id 
                JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
                JOIN nilairujukan_m ON nilairujukan_m.nilairujukan_id = pemeriksaanlabdet_m.nilairujukan_id
                WHERE nilairujukan_m.kelompokumur ILIKE '%".$kelompokumur."%' and detailhasilpemeriksaanlab_t.hasilpemeriksaanlab_id = '". $modHasilPeriksa->hasilpemeriksaanlab_id ."' AND is_print is TRUE
                ORDER BY pemeriksaanlab_urutan, jenispemeriksaanlab_m.jenispemeriksaanlab_urutan, pemeriksaanlabdet_nourut, kelompokdet
            ";
            $detailHasil = Yii::app()->db->createCommand($query)->queryAll();
            
            $data = array();
            $kelompokDet = null;
            $idx = 0;
            $temp = '';

            foreach ($detailHasil as $i => $detail)
            {
                $id_jenisPeriksa = $detail['jenispemeriksaanlab_id'];
                $jenisPeriksa = $detail['jenispemeriksaanlab_nama'];
                $kelompokDet = $detail['kelompokdet'];
                if($id_jenisPeriksa == '72')
                {
                    $query = "
                        SELECT jenispemeriksaanlab_m.* FROM pemeriksaanlabdet_m
                        JOIN pemeriksaanlab_m ON pemeriksaanlabdet_m.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id
                        JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
                        WHERE nilairujukan_id = ". $detail['nilairujukan_id'] ." AND pemeriksaanlab_m.jenispemeriksaanlab_id <> ". $id_jenisPeriksa ."
                    ";
                    $rec = Yii::app()->db->createCommand($query)->queryRow();
                    $id_jenisPeriksa = $rec['jenispemeriksaanlab_id'];
                    $jenisPeriksa = $rec['jenispemeriksaanlab_nama'];
                }
                
                if($temp != $kelompokDet)
                {
                    $idx = 0;
                }
                $data[$id_jenisPeriksa]['tittle'] = $jenisPeriksa;
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['id'] = $id_jenisPeriksa;
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['nama'] = $jenisPeriksa;
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['kelompok'] = $kelompokDet;                
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['kelompok'] = $kelompokDet;
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan_det'] = $detail['pemeriksaanlab_nama'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan'] = $detail['namapemeriksaandet'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['id_pemeriksaan'] = $detail['nilairujukan_id'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['normal'] = $detail['nilairujukan_nama'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['metode'] = $detail['nilairujukan_metode'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['hasil'] = $detail['hasilpemeriksaan'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['nilairujukan'] = $detail['nilairujukan'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['satuan'] = $detail['hasilpemeriksaan_satuan'];
                $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['keterangan'] = $detail['nilairujukan_keterangan'];
                
                $temp = $kelompokDet;
                $idx++;
            }
            
            $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            if(substr($modPendaftaran->no_pendaftaran, 0, 2) != 'LB'){
                $pasienKirimKeunitLain = PasienkirimkeunitlainT::model()->findByAttributes(array(
                    'pasienmasukpenunjang_id'=>$masukpenunjang->pasienmasukpenunjang_id,
                ));
                $modPegawai = PegawaiM::model()->findByPk($pasienKirimKeunitLain->pegawai_id);
                $masukpenunjang->namaperujuk = $modPegawai->nama_pegawai;// . ', ' . $modPegawai->gelarbelakang->gelarbelakang_nama;
                $masukpenunjang->alamatlengkapperujuk = 'RS Jasa Kartini Tasikmalaya';
            }
            $judulLaporan = 'hasil_pemeriksaan_lab';
            if($caraPrint == 'PRINT')
            {
                $this->layout='//layouts/printWindows';
                $this->render('print_hasil',
                    array(
                       'judulLaporan'=>$judulLaporan,
                       'modHasilPeriksa'=>$modHasilPeriksa,
                       'detailHasil'=>$detailHasil,
                       'caraPrint'=>$caraPrint,
                       'hasil'=>$data, 
                       'masukpenunjang'=>$masukpenunjang,
                       'pemeriksa'=>$pemeriksa,
                       'data'=>$data
                    )
                );
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',
                    array(
                        'judulLaporan'=>$judulLaporan,
                        'caraPrint'=>$caraPrint,
                        'modHasilPeriksa'=>$modHasilPeriksa,
                        'detailHasil'=>$detailHasil,
                        'hasil'=>$hasil, 
                        'masukpenunjang'=>$masukpenunjang,
                        'pemeriksa'=>$pemeriksa,
                        'data'=>$data
                    )
                );
            }
            else if($_REQUEST['caraPrint'] == 'PDF')
            {
                $this->layout = '//layouts/frameDialog';
                
//                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $ukuranKertasPDF = 'LAB';                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');  
                $header = 1.14 * 72 / (72/25.4);
                $mpdf = new MyPDF(
                        '',
                        $ukuranKertasPDF, //format A4 Or
                        11, //Font SIZE
                        'san-serif', //default font family
                        3, //15 margin_left
                        3, //15 margin right
                        36, //16 margin top
                        30, // margin bottom
                        '', // 9 margin header
                        '', // 9 margin footer
                        'P' // L - landscape, P - portrait
                ); 
//                $mpdf = new mPDF('',    // mode - default ''
//'A4',    // format - A4, for example, default ''
//0,     // font size - default 0
//'',    // default font family
//'',    // 15 margin_left
//'',    // 15 margin right
//25,     // 16 margin top
//55,    // margin bottom
//'',     // 9 margin header
//'',     // 9 margin footer
//'L');  // L - landscape, P - portrait
//                $mpdf->useOddEven = 2;  
//                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
//                $mpdf->WriteHTML($stylesheet,1);
                /*
                 * cara ambil margin
                 * tinggi_header * 72 / (72/25.4)
                 *  tinggi_header = inchi
                 */
                
                //$mpdf->AddPage($posisi,'','','','', 3, 8, $header, 5,0,0);
                $footer = '
                <table width="100%" style="vertical-align: top; font-family:tahoma;font-size: 8pt;">
                <tr>
                    <td width="50%">'. $masukpenunjang->no_pendaftaran ."  ". $masukpenunjang->nama_pasien .'</td>
                    <td width="50%" align="right">{PAGENO} / {nb}</td>
                </tr>
                <tr>
                    <td width="50%">&nbsp;</td>
                    <td width="50%" align="right">&nbsp;</td>
                </tr>                
                <tr>
                    <td width="50%">&nbsp;</td>
                    <td width="50%" align="right">&nbsp;</td>
                </tr>
                </table>
                ';
                $mpdf->SetHTMLFooter($footer);                
                $mpdf->WriteHTML(
                    $this->renderPartial('print_hasil',
                        array(
                            'caraPrint'=>$caraPrint,
                            'judulLaporan'=>$judulLaporan,
                            'modHasilPeriksa'=>$modHasilPeriksa,
                            'detailHasil'=>$detailHasil,
                            'hasil'=>$hasil, 
                            'masukpenunjang'=>$masukpenunjang,
                            'pemeriksa'=>$pemeriksa,
                            'data'=>$data,
                            'data_rad'=>$data_rad
                        ), true
                    )
                );
                $mpdf->Output();
            }
        }
        
        public function actionPrint($idPasienMasukPenunjang, $idPendaftaran,$caraPrint)
        {
            $judulLaporan = 'Laporan Detail Permintaan Penawaran';
//            $masukpenunjang = PasienmasukpenunjangT::model()->findByPk($idPasienMasukPenunjang);  
            $masukpenunjang = LKPasienMasukPenunjangV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));  
            $pemeriksa = PegawaiM::model()->findByPk($masukpenunjang->pegawai_id);            
            $modHasilPeriksa = HasilpemeriksaanlabV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $modHasilpemeriksaanLab=LKHasilPemeriksaanLabT::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang, 'pendaftaran_id'=>$idPendaftaran));    
            $modHasilpemeriksaanLab->printhasillab=true;
            $modHasilpemeriksaanLab->update();
            $detailHasil = DetailhasilpemeriksaanlabT::model()->findAllByAttributes(array('hasilpemeriksaanlab_id'=>$modHasilPeriksa->hasilpemeriksaanlab_id),array('order'=>'detailhasilpemeriksaanlab_id'));

            $data = array();
            $kelompokDet = null;
            $idx = 0;
            $temp = '';
            foreach ($detailHasil as $i => $detail)
            {
                $id_jenisPeriksa = $detail->pemeriksaanlab->jenispemeriksaan->jenispemeriksaanlab_id;
                $jenisPeriksa = $detail->pemeriksaanlab->jenispemeriksaan->jenispemeriksaanlab_nama;
                $kelompokDet = $detail->pemeriksaandetail->nilairujukan->kelompokdet;
                
                if($temp != $kelompokDet)
                {
                    $idx = 0;
                }
                
                $data[$id_jenisPeriksa][$kelompokDet]['id'] = $id_jenisPeriksa;
                $data[$id_jenisPeriksa][$kelompokDet]['nama'] = $jenisPeriksa;
                $data[$id_jenisPeriksa][$kelompokDet]['kelompok'] = $kelompokDet;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['kelompok'] = $kelompokDet;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan_det'] = $detail->pemeriksaanlab->pemeriksaanlab_nama;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan'] = $detail->pemeriksaandetail->nilairujukan->namapemeriksaandet;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['id_pemeriksaan'] = $detail->pemeriksaandetail->nilairujukan->nilairujukan_id;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['normal'] = $detail->pemeriksaandetail->nilairujukan->nilairujukan_nama;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['metode'] = $detail->pemeriksaandetail->nilairujukan->nilairujukan_metode;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['hasil'] = $detail->hasilpemeriksaan;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['nilairujukan'] = $detail->nilairujukan;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['satuan'] = $detail->hasilpemeriksaan_satuan;
                $data[$id_jenisPeriksa][$kelompokDet]['pemeriksaan'][$idx]['keterangan'] = $detail->pemeriksaandetail->nilairujukan->nilairujukan_keterangan;
                
                $temp = $kelompokDet;
                $idx++;
            }            
            
            $hasil = array();
            foreach ($detailHasil as $i => $detail) {
                $jenisPeriksa = $detail->pemeriksaanlab->jenispemeriksaan->jenispemeriksaanlab_nama;
                $kelompokDet = $detail->pemeriksaandetail->nilairujukan->kelompokdet;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['namapemeriksaan'] = $detail->pemeriksaandetail->nilairujukan->namapemeriksaandet;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['hasil'] = $detail->hasilpemeriksaan;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['nilairujukan'] = $detail->nilairujukan;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['satuan'] = $detail->hasilpemeriksaan_satuan;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['keterangan'] = $detail->pemeriksaandetail->nilairujukan->nilairujukan_keterangan;
            }
            
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
//                $this->render('Print',array('judulLaporan'=>$judulLaporan,
//                                            'caraPrint'=>$caraPrint,
//                                            'modPermintaanPenawaran'=>$modPermintaanPenawaran,
//                                            'modPermintaanPenawaranDetails'=>$modPermintaanPenawaranDetails));
                
                $this->render('Print',
                    array(
                        'judulLaporan'=>$judulLaporan,
                        'caraPrint'=>$caraPrint,
                        'modHasilPeriksa'=>$modHasilPeriksa,
                        'detailHasil'=>$detailHasil,
                        'hasil'=>$hasil, 
                        'masukpenunjang'=>$masukpenunjang,
                        'pemeriksa'=>$pemeriksa,
                        'data'=>$data
                    )
                );
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',
                    array(
                        'judulLaporan'=>$judulLaporan,
                        'caraPrint'=>$caraPrint,
                        'modHasilPeriksa'=>$modHasilPeriksa,
                        'detailHasil'=>$detailHasil,
                        'hasil'=>$hasil, 
                        'masukpenunjang'=>$masukpenunjang,
                        'pemeriksa'=>$pemeriksa,
                        'data'=>$data
                    )
                );
            }
            else if($_REQUEST['caraPrint']=='PDF')
            {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML(
                    $this->renderPartial('Print',
                        array(
                            'judulLaporan'=>$judulLaporan,
                            'caraPrint'=>$caraPrint,
                            'modHasilPeriksa'=>$modHasilPeriksa,
                            'detailHasil'=>$detailHasil,
                            'hasil'=>$hasil, 
                            'masukpenunjang'=>$masukpenunjang,
                            'pemeriksa'=>$pemeriksa,
                            'data'=>$data
                        ), true
                    )
                );
                
                $mpdf->Output();
            }   
        }
        
        public function actionBatalPeriksaPasienLuar()
        {
            if(Yii::app()->request->isAjaxRequest)
            {
                $transaction = Yii::app()->db->beginTransaction();
                $pesan = 'success';
                $status = 'ok';

                try{
                    $idPendaftaran = $_POST['idPendaftaran'];
                    $idPenunjang = $_POST['idPenunjang'];

                    /*
                     * cek data pendaftaran &  pasien masuk penunjang
                     */
                    $pasienMasukPenunjang = PasienmasukpenunjangT::model()->findByAttributes(
                        array(
                            'pasienmasukpenunjang_id'=>$idPenunjang,
                            'ruangan_id'=>PARAMS::RUANGAN_ID_LAB
                        )
                    );

                    if(count($pasienMasukPenunjang) > 0){
                        $pendaftaran = PendaftaranT::model()->findByPk($pasienMasukPenunjang->pendaftaran_id);
                        $pendaftaran_id = $pasienMasukPenunjang->pendaftaran_id;
                        $pasien_id = $pasienMasukPenunjang->pasien_id;
                        $pasienmasukpenunjang_id = $pasienMasukPenunjang->pasienmasukpenunjang_id;
                        $pasienkirimkeunitlain_id = $pasienMasukPenunjang->pasienkirimkeunitlain_id;
                    }else{
                        $pendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
                        $pendaftaran_id = $pendaftaran->pendaftaran_id;
                        $pasien_id = $pendaftaran->pasien_id;
                        $pasienmasukpenunjang_id = null;
                        $pasienkirimkeunitlain_id = $idKirimUnit;
                    }
                    /** end cek data pendaftaran & pasien masuk penunjang **/

                    if(empty($pasienMasukPenunjang->pasienkirimkeunitlain_id))
                    {
                        $model = new PasienbatalperiksaR();
                        $model->pendaftaran_id = $pendaftaran_id;
                        $model->pasien_id = $pasien_id;
                        $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                        $model->tglbatal = date('Y-m-d');
                        $model->keterangan_batal = "Batal Laboratorium";
                        $model->create_time = date('Y-m-d H:i:s');
                        $model->update_time = null;
                        $model->create_loginpemakai_id = Yii::app()->user->id;
                        $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                        if(!$model->save())
                        {
                            $status = 'not';
                            $pesan = 'exist';
                            $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                        }

                        if(empty($pendaftaran->pembayaranpelayanan_id)){
                            $attributes = array(
                                'pasienbatalperiksa_id' => $model->pasienbatalperiksa_id,
                                'update_time' => date('Y-m-d H:i:s'),
                                'update_loginpemakai_id' => Yii::app()->user->id
                            );
                            $pendaftaran = LKPendaftaranT::model()->updateByPk($idPendaftaran, $attributes);

                            $attributes = array(
                                'pasienkirimkeunitlain_id' => $pasienMasukPenunjang->pasienkirimkeunitlain_id
                            );
                            $Perminataan_penunjang = PermintaankepenunjangT::model()->deleteAllByAttributes($attributes);
                            
                            $attributes = array(
                            'pasienmasukpenunjang_id' => $pasienMasukPenunjang->pasienmasukpenunjang_id,
                            );
                            $modTin = LKTindakanPelayananT::model()->findAllByAttributes($attributes);
                            $s=0;
                            foreach ($modTin as $i => $val) {
                                
                                    $sukses_pembalik =  TindakanController::jurnalPembalikTindakan($val['tindakanpelayanan_id']);
                                    if (!$sukses_pembalik) {
                                        $s++;
                                    }
                                
                            }

                            if ($s==0) {
                                $pesan = 'success';
                                $status = 'ok';
                                $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                            }else{
                                $pesan = 'exist';
                                $status = 'not';
                                $keterangan = "<div class='flash-success'>Data Gagal disimpan</div>";
                            }
                        }else{
                            $pesan = 'exist';
                            $status = 'not';
                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                            </b> sudah melakukan pembayaran pemeriksaan </div>";

                        }  
                    }else{
                         /*
                         * cek data tindakan_pelayanan
                         */
                        $attributes = array(
                            'pasienmasukpenunjang_id' => $pasienMasukPenunjang->pasienmasukpenunjang_id,
                        );
                        $tindakan = LKTindakanPelayananT::model()->findAllByAttributes($attributes);
                        if(count($tindakan) > 0)
                        {
                            if(empty($tindakan->tindakansudahbayar_id)){
                                $findHasil = HasilpemeriksaanlabT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));
                                            if(empty($pendaftaran->pembayaranpelayanan_id)){
                                            $model = new PasienbatalperiksaR();
                                            $model->pendaftaran_id = $pendaftaran_id;
                                            $model->pasien_id = $pasien_id;
                                            $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                                            $model->pasienkirimkeunitlain_id = $pasienkirimkeunitlain_id;
                                            $model->tglbatal = date('Y-m-d');
                                            $model->keterangan_batal = "Batal Laboratorium";
                                            $model->create_time = date('Y-m-d H:i:s');
                                            $model->update_time = null;
                                            $model->create_loginpemakai_id = Yii::app()->user->id;
                                            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                                            if(!$model->save())
                                            {
                                                $status = 'not';
                                                $pesan = 'exist';
                                                $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                                            }
                                            $attributes = array(
                                                'statusperiksa' => 'BATAL PERIKSA',
                                                'update_time' => date('Y-m-d H:i:s'),
                                                'update_loginpemakai_id' => Yii::app()->user->id
                                            );
                                            $penunjang = PasienmasukpenunjangT::model()->updateByPk($pasienMasukPenunjang->pasienmasukpenunjang_id, $attributes);
                                            $s=0;
                                            foreach ($tindakan as $i => $val) {
                                                
                                                    $sukses_pembalik =  TindakanController::jurnalPembalikTindakan($val['tindakanpelayanan_id']);
                                                    if (!$sukses_pembalik) {
                                                        $s++;
                                                    }
                                                
                                            }

                                            if ($s==0) {
                                                $pesan = 'success';
                                                $status = 'ok';
                                                $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                                            }else{
                                                $pesan = 'exist';
                                                $status = 'not';
                                                $keterangan = "<div class='flash-success'>Data Gagal disimpan</div>";
                                            }

                                            
                                    }else{
                                        $pesan = 'exist';
                                        $status = 'not';
                                        $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                                        </b> sudah melakukan pembayaran pemeriksaan </div>";
                                    }                     
                            }else{
                                $pesan = 'exist';
                                $status = 'not';
                                $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                                </b> sudah melakukan pembayaran pemeriksaan </div>";
                            }
                        }else{
                            if(empty($tindakan->tindakansudahbayar_id)){
                                $findHasil = HasilpemeriksaanlabT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));
                                            $model = new PasienbatalperiksaR();
                                            $model->pendaftaran_id = $pendaftaran_id;
                                            $model->pasien_id = $pasien_id;
                                            $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                                            $model->pasienkirimkeunitlain_id = $pasienkirimkeunitlain_id;
                                            $model->tglbatal = date('Y-m-d');
                                            $model->keterangan_batal = "Batal Laboratorium";
                                            $model->create_time = date('Y-m-d H:i:s');
                                            $model->update_time = null;
                                            $model->create_loginpemakai_id = Yii::app()->user->id;
                                            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                                            if(!$model->save())
                                            {
                                                $status = 'not';
                                                $pesan = 'exist';
                                                $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                                            }

                                            if(empty($pendaftaran->pembayaranpelayanan_id)){
                                                $attributes = array(
                                                    'statusperiksa' => 'BATAL PERIKSA',
                                                    'update_time' => date('Y-m-d H:i:s'),
                                                    'update_loginpemakai_id' => Yii::app()->user->id
                                                );
                                                $penunjang = PasienmasukpenunjangT::model()->updateByPk($pasienMasukPenunjang->pasienmasukpenunjang_id, $attributes);
                                                $s=0;
                                                foreach ($tindakan as $i => $val) {
                                                    
                                                        $sukses_pembalik =  TindakanController::jurnalPembalikTindakan($val['tindakanpelayanan_id']);
                                                        if (!$sukses_pembalik) {
                                                            $s++;
                                                        }
                                                    
                                                }

                                                if ($s==0) {
                                                    $pesan = 'success';
                                                    $status = 'ok';
                                                    $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                                                }else{
                                                    $pesan = 'exist';
                                                    $status = 'not';
                                                    $keterangan = "<div class='flash-success'>Data Gagal disimpan</div>";
                                                }
                                        }else{
                                            $pesan = 'exist';
                                            $status = 'not';
                                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                                            </b> sudah melakukan pembayaran pemeriksaan </div>";
                                        }                      
                            }else{
                                $pesan = 'exist';
                                $status = 'not';
                                $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaranT->pendaftaran->pasien->nama_pasien." 
                                                </b> sudah melakukan pembayaran pemeriksaan </div>";
                            }
                        }
                    }

                    /*
                     * kondisi_commit
                     */
                    if($status == 'ok')
                    {
                        $transaction->commit();
                    }else{
                        $transaction->rollback();
                    }   
                }catch(Exception $ex){
                    print_r($ex);
                    $status = 'not';
                    $transaction->rollback();
                }

                $data['pesan'] = $pesan;
                $data['status'] = $status;
                $data['keterangan'] = $keterangan;

                echo json_encode($data);
                Yii::app()->end();
            }
        }
        
        public function actionBatalPeriksaPasienLuarTidakDipakai()
        {            
            $ajax = Yii::app()->request->isAjaxRequest;
            // if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){
            //     throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));
            // } 
            if($ajax){
                $idPendaftaran  = $_POST['idpendaftaran'];
                $pendaftaran    = PendaftaranT::model()->findByPk($idPendaftaran);
                $pasien         = PasienM::model()->findByPk($pendaftaran->pasien_id);
                $pasienMasukPenunjang   = PasienmasukpenunjangT::model()->findByAttributes(array('pasien_id'=>$pendaftaran->pasien_id));
                $hasilPemeriksaanLab    = HasilpemeriksaanlabT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
                
                $pasienKirimKeUnitLain  = PasienkirimkeunitlainT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran, 'instalasi_id'=>5));
                $detailHasilPemeriksaanLab = DetailhasilpemeriksaanlabT::model()->findAllByAttributes(array('hasilpemeriksaanlab_id'=>$hasilPemeriksaanLab->hasilpemeriksaanlab_id));
                
                $cekPasien = substr($pendaftaran->no_pendaftaran, 0,2);
//                jika pasien berasal dari pendaftaran pasien luar
                if($cekPasien=='LB'){
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
    //                   update rujuakan_id = null terlebih dahulu di pendaftaran_t
                        $updateRujuakan             = PendaftaranT::model()->updateByPk($idPendaftaran, array('rujukan_id'=>null));
    //                    delete tabel rujukan sesuai dengan id_rujuan yang berada di pendaftaran_t
                        $deletePasienRujukan        = RujukanT::model()->deleteByPk($pendaftaran->rujukan_id); 
    //                    delete penagambilan sample berdasarkan pasienmasukpenunjang_id di pasienmasukpenunjang_t
                        $deletePengambilanSample    = PengambilansampleT::model()->deleteAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));                    
    //                    delete detailhasilpemeriksaanlab_t berdasarkan dengan hasilpemeriksaanlab_id
                        $deleteDetailPemeriksaanLab= DetailhasilpemeriksaanlabT::model()->deleteAllByAttributes(array('hasilpemeriksaanlab_id'=>$hasilPemeriksaanLab->hasilpemeriksaanlab_id));
    //                    delete hasilpemeriksaanlab_t berdasarkan pendaftaran_id
                        $deleteHasilPemeriksaanLab  = HasilpemeriksaanlabT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
    //                    delete uabahcarabayar_t berdasarkan pendaftaran_id
                        $deleteUbahCarabayar        = UbahcarabayarR::model()->deleteAll('pendaftaran_id = '.$idPendaftaran);
    //                    delete tindakanpelayanan_t berdasarkan pendaftaran_id
                        $deleteTindakanPelayanan    = TindakanpelayananT::model()->deleteAll('pendaftaran_id = '.$idPendaftaran);
    //                    delete pasienmasuk penunjang berdasarkan pendaftaran_t
                        $deletePasienMasukPenunjang = PasienmasukpenunjangT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));   
    //                    delete pendaftaran berdasarkan id_pendaftaran
                        $deletePendaftaran          = PendaftaranT::model()->deleteByPk($idPendaftaran);
    //                    delete pasie_m berdasarkan pasien_id
//                        $deletePasien               = PasienM::model()->deleteByPk($pasien->pasien_id);
    //                    
    //                    $delete = $deletePasienRujukan && $deletePengambilanSample && $deleteUbahCarabayar && $deleteHasilPemeriksaanLab && $deleteTindakanPelayanan && $deletePasien && $deletePendaftaran;

                        if($deletePasien && $pendaftaran){
                            $data['status'] = 'success';
                            $transaction->commit();
                        }else{
                            $data['status'] = 'gagal';
                            $transaction->rollback();
                            throw new Exception("Pasien tidak bisa dibatalkan");
                        }

                    }catch(Exception $ex){
                         $transaction->rollback();
                         $data['status'] = 'gagal';
                         $data['info'] = $ex;
                     }      
                    
                }else{
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
//                        echo "pasienkirimke unit lain->".$pasienKirimKeUnitLain->pasienmasukpenunjang_id;
//                        echo "----pasien masuk penunjang ->".$pasienKirimKeUnitLain->pasienmasukpenunjang_id;
                        $updatePasienKirimKeUnitLain = PasienkirimkeunitlainT::model()->updateByPk(
                            $pasienKirimKeUnitLain->pasienkirimkeunitlain_id,
                            array('pasienmasukpenunjang_id'=>null)
                        );
                       
                        $deleteDetailPemeriksaanLab    = DetailhasilpemeriksaanlabT::model()->deleteAllByAttributes(
                            array('hasilpemeriksaanlab_id'=>$hasilPemeriksaanLab->hasilpemeriksaanlab_id)
                        );
                            
                        foreach ($detailHasilPemeriksaanLab as $key=>$deleteDetailHasil)
                        {
                            $deleteTindakanPelayanan = TindakanpelayananT::model()->deleteByPk($deleteDetailHasil->tindakanpelayanan_id);
                        }
                        
                        $deleteHasilPemeriksaanLab = HasilpemeriksaanlabT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
//                        $deletePasienMasukPenunjang = PasienmasukpenunjangT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$idPendaftaran)); 
                        $deletePasienMasukPenunjang = PasienmasukpenunjangT::model()->deleteByPk($pasienKirimKeUnitLain->pasienmasukpenunjang_id); 
                        if($deletePasienMasukPenunjang){
                            $data['status'] = 'success';
                                $transaction->commit();
                        }else{
                            $data['status'] = 'gagal';
                            $transaction->rollback();
                            throw new Exception("Pasien tidak bisa dibatalkan");
                        }

                    }catch(Exception $ex){
                         $transaction->rollback();
                         $data['status'] = 'gagal';
                         $data['info'] = $ex;
                     }              
                    
                }
                 
                echo json_encode($data);
                Yii::app()->end();
            }
        }
        
        public function actionUbahPasien($id,$idpendaftaran)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $model = LKPasienM::model()->findByPk($id);
                 $modPendaftaran = LKPendaftaranT::model()->findByPk($idpendaftaran);
                 $modPasien = LKPasienM::model()->findByPk($_POST['pasien_id']);     
                $format = new CustomFormat();
                $temLogo=$model->photopasien;
                $model->update_time = date ('Y-m-d');
                $model->update_loginpemakai_id = Yii::app()->user->id;
                $model->tgl_rekam_medik = $format->formatDateMediumForUser($model->tgl_rekam_medik);
                $model->tgl_rekam_medik = date('d M Y',strtotime($model->tgl_rekam_medik));
                $pendaftaran = PendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$id),array('order'=>'pendaftaran_id desc'));
                $umur = explode(" ",$pendaftaran[0]->umur);
                $thn = $umur[0];
                $bln = $umur[2];
                $hr = $umur[4];
                
                // $model->umur = '00 Thn 00 Bln 00 Hr';
                $model->thn = $thn;
                $model->bln = $bln;
                $model->hr = $hr;
                if(isset($_POST['LKPasienM'])) {                   
                    $random=rand(0000000,9999999);
                    $model->attributes = $_POST['LKPasienM'];
                    $model->umur = $_POST['LKPasienM']['umur'];
                    $model->tanggal_lahir = $format->formatDateMediumForDB($model->tanggal_lahir);
                    $model->kelompokumur_id = Generator::kelompokUmur($model->tanggal_lahir);
                    $model->photopasien = CUploadedFile::getInstance($model, 'photopasien');
                    $gambar=$model->photopasien;
                    $model->tgl_rekam_medik  = $format->formatDateMediumForDB($model->tgl_rekam_medik);
                    $umurBaru = $_POST['LKPasienM']['thn']." Thn ".$_POST['LKPasienM']['bln']." Bln ".$_POST['LKPasienM']['hr']." Hr ";
                    if(!empty($model->photopasien)) { //if user input the photo of patient
                        $model->photopasien =$random.$model->photopasien;

                         Yii::import("ext.EPhpThumb.EPhpThumb");

                         $thumb=new EPhpThumb();
                         $thumb->init(); //this is needed

                         $fullImgName =$model->photopasien;   
                         $fullImgSource = Params::pathPasienDirectory().$fullImgName;
                         $fullThumbSource = Params::pathPasienTumbsDirectory().'kecil_'.$fullImgName;

                         if($model->save()) {
                            if(!empty($temLogo)) { 
                               if(file_exists(Params::pathPasienDirectory().$temLogo))
                                    unlink(Params::pathPasienDirectory().$temLogo);
                               if(file_exists(Params::pathPasienTumbsDirectory().'kecil_'.$temLogo))
                                    unlink(Params::pathPasienTumbsDirectory().'kecil_'.$temLogo);
                            }
                            $gambar->saveAs($fullImgSource);
                            $thumb->create($fullImgSource)
                                 ->resize(200,200)
                                 ->save($fullThumbSource);
                            // LKPendaftaranT::model()->updateByPk($idpendaftaran);
                            PendaftaranT::model()->updateByPk($pendaftaran[0]->pendaftaran_id,array('umur'=>$umurBaru));
//                            $model->updateByPk($id, array('tgl_rekam_medik'=>$model->tgl_rekam_medik));
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->refresh();
//                            $this->redirect(array('cariPasien'));
                          } else {
                              // LKPendaftaranT::model()->updateByPk($idpendaftaran);
                              LKPendaftaranT::model()->updateByPk($pendaftaran[0]->pendaftaran_id,array('umur'=>$umurBaru));
                              // $model->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
                               Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
                               $this->refresh();
                          }
                    } else { //if user not input the photo
                       $model->photopasien=$temLogo;
                       if($model->save()) {
                            // LKPendaftaranT::model()->updateByPk($idpendaftaran);
                            LKPendaftaranT::model()->updateByPk($pendaftaran[0]->pendaftaran_id,array('umur'=>$umurBaru));
                            // $model->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
//                            $model->updateByPk($id, array('tgl_rekam_medik'=>$model->tgl_rekam_medik));
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->refresh();
//                            $this->redirect(array('cariPasien'));
                       }
                    }

                }
		$this->render($this->pathView.'ubahPasien',array('model'=>$model));
	}
        
        /**
         * actionPrintKartuGolonganDarah
         * @param type $idPendaftaran
         * @param type $idPasienMasukPenunjang
         * @param type $caraPrint
         */
        public function actionPrintKartuGolonganDarah($idPasienMasukPenunjang, $idPendaftaran, $caraPrint = null){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);            
            $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modHasilPemeriksaan = HasilpemeriksaanlabT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran, 'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $modPeriksaGolonganDarah = DetailhasilpemeriksaanlabT::model()->findByAttributes(array('hasilpemeriksaanlab_id' => $modHasilPemeriksaan->hasilpemeriksaanlab_id, 'pemeriksaanlab_id'=>Params::PERIKSA_GOLONGANDARAH_ID));
            $modPeriksaRhesus = DetailhasilpemeriksaanlabT::model()->findByAttributes(array('hasilpemeriksaanlab_id' => $modHasilPemeriksaan->hasilpemeriksaanlab_id,'pemeriksaanlab_id' =>  Params::PERIKSA_RHESUS_ID));
            if($modPeriksaGolonganDarah){
                if(empty($modPeriksaGolonganDarah->hasilpemeriksaan)){
                    echo "Hasil pemeriksaan golongan darah masih kosong !";
                }else{
                    if($_REQUEST['caraPrint'] == 'PDF')
                    {
                        $this->layout = '//layouts/frameDialog';

        //                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                        $ukuranKertasPDF = 'KGDLAB';                  //Ukuran Kertas Pdf
                        $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('',$ukuranKertasPDF); 
                        $mpdf->useOddEven = 2;  
                        $stylesheet = $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet,1);
                        /*
                         * cara ambil margin
                         * tinggi_header * 72 / (72/25.4)
                         *  tinggi_header = inchi
                         */
                        // $header = 0.4 * 72 / (72/33.4);
                        $header = 0.4 * 72 / (77/43.4);
                        $mpdf->AddPage($posisi,'','','','', 3, 8, $header, 2,0,0);
                        $mpdf->WriteHTML(
                            $this->renderPartial('PrintKartuGolonganDarah',array(
                                                'caraPrint'=>$caraPrint,
                                                'modPasien'=>$modPasien,
                                                'modPendaftaran'=>$modPendaftaran, 
                                                'modHasilPemeriksaan'=>$modHasilPemeriksaan,
                                                'modPeriksaGolonganDarah'=>$modPeriksaGolonganDarah,
                                                'modPeriksaRhesus'=>$modPeriksaRhesus,
                            ),true)
                        );
                        $mpdf->Output();
                    }else if($caraPrint=='PRINT') {
                        $this->layout='//layouts/printWindows';
                        $this->render('PrintKartuGolonganDarah',array(
                                                'caraPrint'=>$caraPrint,
                                                'modPasien'=>$modPasien,
                                                'modPendaftaran'=>$modPendaftaran, 
                                                'modHasilPemeriksaan'=>$modHasilPemeriksaan,
                                                'modPeriksaGolonganDarah'=>$modPeriksaGolonganDarah,
                                                'modPeriksaRhesus'=>$modPeriksaRhesus,
                        ));
                    }
                    
                }
            }else{
                echo "Pasien ".$modPasien->no_rekam_medik." - ".$modPasien->nama_pasien." tidak melakukan pemeriksaan golongan darah";
            }
        }
        
        public function actionStatusPeriksa()
        {
            if(Yii::app()->request->isAjaxRequest){
                $status = (isset($_POST['statusperiksa']) ? $_POST['statusperiksa'] : null);
                $pendaftaran_id = (isset($_POST['pendaftaran_id']) ? $_POST['pendaftaran_id'] : null);
                $pasienmasukpenunjang_id = (isset($_POST['pasienmasukpenunjang_id']) ? $_POST['pasienmasukpenunjang_id'] : null);
                $pasienkirimkeunitlain_id = (isset($_POST['pasienkirimkeunitlain_id']) ? $_POST['pasienkirimkeunitlain_id'] : null);
                
                $statuspasien='';
                $modPendaftaran = PendaftaranT::model()->findByPk($pendaftaran_id);
                if(!empty($modPendaftaran->pasienadmisi_id)){
                    $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                    if(!empty($modPasienAdmisi->pasienpulang_id)){
                        $statuspasien = 'SUDAH PULANG';
                    }
                }else{
                   $statuspasien = $modPendaftaran->statusperiksa;
                }
                
                $data['pendaftaran_id'] = $pendaftaran_id;
                $data['pasienmasukpenunjang_id'] = $pasienmasukpenunjang_id;
                $data['pasienadmisi_id'] = $modPendaftaran->pasienadmisi_id;
                $data['pasien_id'] = $modPendaftaran->pasien_id;
                $data['pasienkirimkeunitlain_id'] = $pasienkirimkeunitlain_id;
                $data['statuspasien'] = $statuspasien;
                
                echo json_encode($data);
                Yii::app()->end();
            }
        }
}