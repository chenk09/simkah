<?php

class ActionAutoCompleteController extends SBaseController
{
    public function actionDaftarPasien()
    {
        if(Yii::app()->request->isAjaxRequest) {
                $criteria = new CDbCriteria();
                $criteria->compare('LOWER(no_rekam_medik)', strtolower($_GET['term']), true);
                $criteria->compare('ruangan_id', Params::RUANGAN_ID_LAB);
                //$criteria->order = 'tglmasukpenunjang DESC';
                //$models = PasienmasukpenunjangV::model()->findAll($criteria);
                $models = PasienkirimkeunitlainV::model()->findAll($criteria);
                $returnVal = array();
                foreach($models as $i=>$model)
                {
                    $attributes = $model->attributeNames();
                    foreach($attributes as $j=>$attribute) {                        
                        $returnVal[$i]["$attribute"] = $model->$attribute;
                    }
                    $returnVal[$i]['label'] = $model->no_rekam_medik.' - '.$model->ruanganasal_nama.' - '.$model->tgl_pendaftaran;
                    $returnVal[$i]['value'] = $model->no_rekam_medik;
                }

                echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }
    
    function actionGetNamaDaftarTindakan(){
        if(Yii::app()->request->isAjaxRequest) {
                $criteria = new CDbCriteria();
                $criteria->compare('LOWER(pemeriksaanlab_nama)', strtolower($_GET['term']), true);
//                $criteria->compare('ruangan_id', Params::RUANGAN_ID_LAB);
                //$criteria->order = 'tglmasukpenunjang DESC';
                //$models = PasienmasukpenunjangV::model()->findAll($criteria);
                $model = PemeriksaanlabM::model()->findAll($criteria);
                $data = array();
                foreach($model as $i=>$models)
                {
                    $attributes = $models->attributeNames();
                    foreach($attributes as $j=>$attribute) {                        
                        $data[$i]["$attribute"] = $models->$attribute;
                    }
                    $data[$i]['label'] = $models->pemeriksaanlab_nama;
                    $data[$i]['value'] = $models->pemeriksaanlab_nama;
                }

                echo json_encode($data);
        }
        Yii::app()->end();
    }
    
    function actionGetKodeDaftarTindakan(){
        if(Yii::app()->request->isAjaxRequest) {
                $criteria = new CDbCriteria();
                $criteria->compare('LOWER(pemeriksaanlab_kode)', strtolower($_GET['term']), true);
//                $criteria->compare('ruangan_id', Params::RUANGAN_ID_LAB);
                //$criteria->order = 'tglmasukpenunjang DESC';
                //$models = PasienmasukpenunjangV::model()->findAll($criteria);
                $modelTindakan = PemeriksaanlabM::model()->findAll($criteria);
                $returnTindakan = array();
                foreach($modelTindakan as $i=>$tindakan)
                {
                    $attributes = $tindakan->attributeNames();
                    foreach($attributes as $j=>$attribute) {                        
                        $returnTindakan[$i]["$attribute"] = $tindakan->$attribute;
                    }
                    $returnTindakan[$i]['label'] = $tindakan->pemeriksaanlab_kode." - ".$tindakan->pemeriksaanlab_nama;
                    $returnTindakan[$i]['value'] = $tindakan->pemeriksaanlab_kode;
                }

                echo CJSON::encode($returnTindakan);
        }
        Yii::app()->end();
    }
}
?>
