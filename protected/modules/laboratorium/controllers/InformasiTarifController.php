<?php

class InformasiTarifController extends SBaseController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','detailsTarif'),
//				'users'=>array('@'),
//			),
//		);
//	}
        
	public function actionIndex()
	{
//            $modTarifTindakanRuanganV = new LKTarifTindakanPerdaRuanganV('search');
                $modTarifLabRad = new LKInformasitariflabradV('searchTarifLabRad');

                if(isset($_GET['LKInformasitariflabradV'])){
                    $modTarifLabRad->attributes=$_GET['LKInformasitariflabradV'];

                }
                $this->render('index',array('modTarifLabRad'=>$modTarifLabRad));
	}
	        
        public function actionDetailsTarif($idKelasPelayanan,$idDaftarTindakan, $idKategoriTindakan){
            
            $this->layout='//layouts/frameDialog';
            if($idKelasPelayanan!=''){
            $modTarifTindakan= LKTariftindakanM::model()->with('komponentarif')->findAll('kelaspelayanan_id='.$idKelasPelayanan.' AND 
                                                               daftartindakan_id='.$idDaftarTindakan.'
                                                               AND t.komponentarif_id!='.Params::KOMPONENTARIF_ID_TOTAL.'');
            }else{ 
                $modTarifTindakan=  LKTariftindakanM::model()->with('komponentarif')->findAll('daftartindakan_id='.$idDaftarTindakan.'
                                                               AND t.komponentarif_id!='.Params::KOMPONENTARIF_ID_TOTAL.'
                                                               AND kelaspelayanan_id isNull');
            }
            $modTarif = TariftindakanperdaruanganV::model()->find('daftartindakan_id = '.$idDaftarTindakan.' and kelaspelayanan_id = '.$idKelasPelayanan.' and kategoritindakan_id = '.$idKategoriTindakan);
            $jumlahTarifTindakan=COUNT($modTarifTindakan);
            
            $this->render('detailsTarif',array('modTarif'=>$modTarif,
                                                'modTarifTindakan'=>$modTarifTindakan,
                                                'jumlahTarifTindakan'=>$jumlahTarifTindakan));
            
            
        }

}