<?php

class RinciantagihanpasienpenunjangVController extends SBaseController
{
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	public function actionIndex()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new PasienmasukpenunjangV('search');
                $format = new CustomFormat();
                $model->tglAwal = date("d M Y").' 00:00:00';
                $model->tglAkhir = date('d M Y h:i:s');
		$model->unsetAttributes();  // clear any default values
                
		if(isset($_GET['PasienmasukpenunjangV'])){
			$model->attributes=$_GET['PasienmasukpenunjangV'];
                        $format = new CustomFormat();
                        $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PasienmasukpenunjangV']['tglAwal']);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PasienmasukpenunjangV']['tglAkhir']);

                }

		$this->render('laboratorium.views.rinciantagihanpasienpenunjangV.index',array(
			'model'=>$model,
		));
	}
        
        public function actionRincian($id){
            $model = new LKRinciantagihanpasienpenunjangV('search');
//            $model->ruangan_id = 10;
//            echo 'a'.print_r($model->getAttributes());
            $this->layout = '//layouts/frameDialog';
            $data['judulLaporan'] = 'Rincian Tagihan Pasien';
            $modPendaftaran = LKPendaftaranT::model()->findByPk($id);
            $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
            if(empty($modPenunjang)){
                $criteria = new CDbCriteria;
                $criteria->with = array('pasienadmisi');
                $criteria->compare('pasienadmisi.pendaftaran_id', $id);
                $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                $modPenunjang = PasienmasukpenunjangT::model()->find($criteria);
            }
            $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $id,'ruangan_id'=>Yii::app()->user->getState('ruangan_id')), array('order'=>'ruangan_id'));
            
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
//            $modRincian->pendaftaran_id = $id;
            $this->render('laboratorium.views.rinciantagihanpasienpenunjangV.rincian', array('modPenunjang'=>$modPenunjang,'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data));
        }

        public function actionkwitansiOld($idPembayaranPelayanan)
        {
        	// if(!empty($_GET['frame']))
                $this->layout = '//layouts/frameDialog';
            $judulKwitansi = '----- KWITANSI -----';
            $format = new CustomFormat();
            $modBayar = PembayaranpelayananT::model()->findByPk($idPembayaranPelayanan);
            $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
            $criteria = new CdbCriteria();
            $criteria->addCondition('pembayaranpelayanan_id = '.$idPembayaranPelayanan);
            $tindakanSudahBayar = TindakansudahbayarT::model()->findAll($criteria);
            if(!empty($modBayar->pendaftaran_id)){
                $modPendaftaran = PendaftaranT::model()->findByPk($modBayar->pendaftaran_id);
                $modPendaftaran->tgl_pendaftaran = $format->formatDateTimeMediumForDB($modBayar->pendaftaran->tgl_pendaftaran);
            }else{
                $modPendaftaran = new PendaftaranT;
            }
            $rincianpembayaran = array();
            $tindakan = array();
                
            if (count($tindakanSudahBayar) > 0){
                $totalTindakan=0;
                foreach ($tindakanSudahBayar as $key => $value) {
                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['kelompoktindakan'] = $value->daftartindakan->kelompoktindakan->kelompoktindakan_nama;
                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['harga'] += $value->jmlbiaya_tindakan;
                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['discount'] += $value->tindakanpelayanan->discount_tindakan;
                    $totalTindakan += ($value->jmlbiaya_tindakan - $value->tindakanpelayanan->discount_tindakan);
                }
                $rincianpembayaran['tindakan'] = $tindakan;
                $rincianpembayaran['tindakan']['totalTindakan'] = $totalTindakan;
            }
            $oaSudahBayar = OasudahbayarT::model()->findAll($criteria);
            $oa = array();
            if (count($oaSudahBayar) > 0 ){
                $totalOa=0;
                foreach ($oaSudahBayar as $key => $value) {
                        $oa[0]['kelompoktindakan'] = $value->obatalkes->jenisobatalkes->jenisobatalkes_nama;
                        $oa[0]['harga'] += ($value->obatalkespasien->hargasatuan_oa * $value->obatalkespasien->qty_oa);
                        $discount = ($value->obatalkespasien->discount > 0 ) ? $value->obatalkespasien->discount/100 : 0 ;
                        $oa[0]['discount'] += ($discount*$value->obatalkespasien->hargasatuan_oa * $value->obatalkespasien->qty_oa);
                        $oa[0]['biayaadministrasi'] += $value->obatalkespasien->biayaadministrasi;
                        $oa[0]['biayaservice'] += $value->obatalkespasien->biayaservice;
                        $oa[0]['biayakonseling'] += $value->obatalkespasien->biayakonseling;
                        $totalOa += ($oa[0]['harga'] - $oa[0]['discount'] + $oa[0]['biayaadministrasi'] + $oa[0]['biayaservice'] + $oa[0]['biayakonseling']);
                }
                $rincianpembayaran['oa'] = $oa;
                $rincianpembayaran['oa']['totalOa'] = $totalOa;
            }
            if($modTandaBukti->jmlpembayaran == 0){ //jika jmlpembayaran nol
                $modTandaBukti->jmlpembayaran = $rincianpembayaran['tindakan']['totalTindakan'] + $rincianpembayaran['oa']['totalOa'];
            }
		
            //Jika ada perubahan nama pembayar (darinama_bkm)
            if(isset($_POST['TandabuktibayarT'])){
                if(!empty($_POST['TandabuktibayarT']['darinama_bkm'])){
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $updateSukses = TandabuktibayarT::model()->updateByPk($modBayar->tandabuktibayar_id, array('darinama_bkm'=>$_POST['TandabuktibayarT']['darinama_bkm']));
                        if($updateSukses){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', 'Data berhasil disimpan !');
                            $this->refresh();
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', 'Data gagal disimpan !');
                        }
                    }catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }
            }
            
            $this->render('laboratorium.views.rinciantagihanpasienpenunjangV.kwitansi',array('pembayarans'=>$pembayarans,
                                       'judulKwitansi'=>$judulKwitansi,
                                       'modPendaftaran'=>$modPendaftaran,
                                       'rincianpembayaran'=>$rincianpembayaran,
                                       'modTandaBukti'=>$modTandaBukti,
                                       'modBayar'=>$modBayar,
                                       'model'=>$model,
//                                       'pembayaranpelayanan'=>$pembayaranpelayanan
                                        ));
        } 
        
        public function actionkwitansi($idPendaftaran)
        {
            $this->layout = '//layouts/frameDialog';
            $judulKwitansi = '----- KWITANSI -----';
            $format = new CustomFormat();
            $modPendaftaran = LKPendaftaranT::model()->findByPk($idPendaftaran);
            $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            if(empty($modPenunjang)){
                $criteria = new CDbCriteria;
                $criteria->compare('pendaftaran_id', $idPendaftaran);
                $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                $modPenunjang = PasienmasukpenunjangT::model()->find($criteria);
            }
            $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array(
                'pendaftaran_id' => $idPendaftaran,
                'ruangan_id'=>Yii::app()->user->getState('ruangan_id'),
                'pembayaranpelayanan_id'=>null
            ), array('order'=>'ruangan_id'));
		
            $rincianpembayaran = array();
            if (count($modRincian) > 0){
                $totalTindakan=0;
                foreach ($modRincian as $key => $value) {
                    $totalTindakan += ($value->tarif_satuan * $value->tarif_tindakan);
                }
                $rincianpembayaran['totalTindakan'] = $totalTindakan;
            }
            
            $this->render('laboratorium.views.rinciantagihanpasienpenunjangV.kwitansi',array('pembayarans'=>$pembayarans,
                                       'judulKwitansi'=>$judulKwitansi,
                                       'modPendaftaran'=>$modPendaftaran,
                                       'rincianpembayaran'=>$rincianpembayaran,
                                       'modPenunjang'=>$modPenunjang,
                                       'modRincian'=>$modRincian,
//                                       'model'=>$model,
//                                       'pembayaranpelayanan'=>$pembayaranpelayanan
                                        ));
        } 


//         public function actionPrintKwitansiOld($idPembayaranPelayanan)
//        {
//            $judulKwitansi = '----- KWITANSI -----';
//            $format = new CustomFormat();
//            $modBayar = PembayaranpelayananT::model()->findByPk($idPembayaranPelayanan);
//            $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
//            $criteria = new CdbCriteria();
//            $criteria->addCondition('pembayaranpelayanan_id = '.$idPembayaranPelayanan);
//            $tindakanSudahBayar = TindakansudahbayarT::model()->findAll($criteria);
//            if(!empty($modBayar->pendaftaran_id)){
//                $modPendaftaran = PendaftaranT::model()->findByPk($modBayar->pendaftaran_id);
//                $modPendaftaran->tgl_pendaftaran = $format->formatDateTimeMediumForDB($modBayar->pendaftaran->tgl_pendaftaran);
//            }else{
//                $modPendaftaran = new PendaftaranT;
//            }
//            $rincianpembayaran = array();
//            $tindakan = array();
//                
//            if (count($tindakanSudahBayar) > 0){
//                $totalTindakan=0;
//                foreach ($tindakanSudahBayar as $key => $value) {
//                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['kelompoktindakan'] = $value->daftartindakan->kelompoktindakan->kelompoktindakan_nama;
//                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['harga'] += $value->jmlbiaya_tindakan;
//                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['discount'] += $value->tindakanpelayanan->discount_tindakan;
//                    $totalTindakan += ($value->jmlbiaya_tindakan - $value->tindakanpelayanan->discount_tindakan);
//                }
//                $rincianpembayaran['tindakan'] = $tindakan;
//                $rincianpembayaran['tindakan']['totalTindakan'] = $totalTindakan;
//            }
//            $oaSudahBayar = OasudahbayarT::model()->findAll($criteria);
//            $oa = array();
//            if (count($oaSudahBayar) > 0 ){
//                $totalOa=0;
//                foreach ($oaSudahBayar as $key => $value) {
//                        $oa[0]['kelompoktindakan'] = $value->obatalkes->jenisobatalkes->jenisobatalkes_nama;
//                        $oa[0]['harga'] += ($value->obatalkespasien->hargasatuan_oa * $value->obatalkespasien->qty_oa);
//                        $discount = ($value->obatalkespasien->discount > 0 ) ? $value->obatalkespasien->discount/100 : 0 ;
//                        $oa[0]['discount'] += ($discount*$value->obatalkespasien->hargasatuan_oa * $value->obatalkespasien->qty_oa);
//                        $oa[0]['biayaadministrasi'] += $value->obatalkespasien->biayaadministrasi;
//                        $oa[0]['biayaservice'] += $value->obatalkespasien->biayaservice;
//                        $oa[0]['biayakonseling'] += $value->obatalkespasien->biayakonseling;
//                        $totalOa += ($oa[0]['harga'] - $oa[0]['discount'] + $oa[0]['biayaadministrasi'] + $oa[0]['biayaservice'] + $oa[0]['biayakonseling']);
//                }
//                $rincianpembayaran['oa'] = $oa;
//                $rincianpembayaran['oa']['totalOa'] = $totalOa;
//            }
//            if($modTandaBukti->jmlpembayaran == 0){ //jika jmlpembayaran nol
//                $modTandaBukti->jmlpembayaran = $rincianpembayaran['tindakan']['totalTindakan'] + $rincianpembayaran['oa']['totalOa'];
//            }
//            
//            $caraPrint=$_REQUEST['caraPrint'];
//            // if($caraPrint=='PRINT') {
//            //     $this->layout='//layouts/printWindows';
//            //     $this->render('billingKasir.views.kwitansi.viewKwitansi', array('model'=>$model,'pembayarans'=>$pembayarans, 'modPendaftaran'=>$modPendaftaran, 'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint, 'rincianpembayaran'=>$rincianpembayaran,
//            //                            'modTandaBukti'=>$modTandaBukti,
//            //                            'modBayar'=>$modBayar));
//            //     //$this->render('rincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
//            // }
//            // else if($caraPrint=='EXCEL') {
//            //     $this->layout='//layouts/printExcel';
//            //     $this->render('billingKasir.views.kwitansi.viewKwitansi',array('model'=>$model,'pembayarans'=>$pembayarans, 'modPendaftaran'=>$modPendaftaran, 'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint,'rincianpembayaran'=>$rincianpembayaran,
//            //                            'modTandaBukti'=>$modTandaBukti,
//            //                            'modBayar'=>$modBayar));
//            // }
//            if($_REQUEST['caraPrint']=='PDF') {
////                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
//                $ukuranKertasPDF = 'KWLAB';                  //Ukuran Kertas Pdf
//                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
//                $mpdf = new MyPDF('',$ukuranKertasPDF); 
//                $mpdf->useOddEven = 2;  
//                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
//                $mpdf->WriteHTML($stylesheet,1); 
//                /*
//                 * cara ambil margin
//                 * tinggi_header * 72 / (72/25.4)
//                 *  tinggi_header = inchi
//                 */
//                $header = 0.78 * 72 / (72/25.4);
//                $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
//                 $mpdf->WriteHTML(
//                    $this->renderPartial(
//                        'laboratorium.views.rinciantagihanpasienpenunjangV.printKwitansi',
//                        array(
//                            'model'=>$model,
//                            'pembayarans'=>$pembayarans,
//                            'modPendaftaran'=>$modPendaftaran,
//                            'judulKwitansi'=>$judulKwitansi,
//                            'caraPrint'=>$caraPrint,
//                            'rincianpembayaran'=>$rincianpembayaran,
//                                       'modTandaBukti'=>$modTandaBukti,
//                                       'modBayar'=>$modBayar
//                        ),true
//                    )
//                );
//                $mpdf->Output();
//            }                       
//        }
         public function actionPrintKwitansi($idPendaftaran)
        {
            $judulKwitansi = '----- KWITANSI -----';
            $format = new CustomFormat();
            $modPendaftaran = LKPendaftaranT::model()->findByPk($idPendaftaran);
            $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            $namapasien = $_GET['namapasien'];
            if(empty($modPenunjang)){
                $criteria = new CDbCriteria;
                $criteria->compare('pendaftaran_id', $idPendaftaran);
                $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                $modPenunjang = PasienmasukpenunjangT::model()->find($criteria);
            }
            $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array(
                'pendaftaran_id' => $idPendaftaran,
                'ruangan_id'=>Yii::app()->user->getState('ruangan_id'),
                'pembayaranpelayanan_id'=>null
            ), array('order'=>'ruangan_id'));

            $rincianpembayaran = array();
            if (count($modRincian) > 0){
                $totalTindakan=0;
                foreach ($modRincian as $key => $value) {
                    $totalTindakan += ($value->tarif_satuan * $value->tarif_tindakan);
                }
                $rincianpembayaran['totalTindakan'] = $totalTindakan;
            }
            
            $caraPrint=$_REQUEST['caraPrint'];
            // if($caraPrint=='PRINT') {
            //     $this->layout='//layouts/printWindows';
            //     $this->render('billingKasir.views.kwitansi.viewKwitansi', array('model'=>$model,'pembayarans'=>$pembayarans, 'modPendaftaran'=>$modPendaftaran, 'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint, 'rincianpembayaran'=>$rincianpembayaran,
            //                            'modTandaBukti'=>$modTandaBukti,
            //                            'modBayar'=>$modBayar));
            //     //$this->render('rincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            // }
            // else if($caraPrint=='EXCEL') {
            //     $this->layout='//layouts/printExcel';
            //     $this->render('billingKasir.views.kwitansi.viewKwitansi',array('model'=>$model,'pembayarans'=>$pembayarans, 'modPendaftaran'=>$modPendaftaran, 'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint,'rincianpembayaran'=>$rincianpembayaran,
            //                            'modTandaBukti'=>$modTandaBukti,
            //                            'modBayar'=>$modBayar));
            // }
            if($_REQUEST['caraPrint']=='PDF') {
//                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $ukuranKertasPDF = 'KWLAB';                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1); 
                /*
                 * cara ambil margin
                 * tinggi_header * 72 / (72/25.4)
                 *  tinggi_header = inchi
                 */
                $header = 0.78 * 72 / (72/25.4);
                $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                 $mpdf->WriteHTML(
                    $this->renderPartial(
                        'laboratorium.views.rinciantagihanpasienpenunjangV.printKwitansi',
                        array(
                             'modPendaftaran'=>$modPendaftaran,
                             'rincianpembayaran'=>$rincianpembayaran,
                             'modPenunjang'=>$modPenunjang,
                             'modRincian'=>$modRincian,
                             'namapasien'=>$namapasien,
                        ),true
                    )
                );
                $mpdf->Output();
            }                       
        }

        public function terbilang($x, $style=4, $strcomma=",") {
        if ($x < 0) {
            $result = "minus " . trim($this->ctword($x));
        } else {
            $arrnum = explode("$strcomma", $x);
            $arrcount = count($arrnum);
            if ($arrcount == 1) {
                $result = trim($this->ctword($x));
            } else if ($arrcount > 1) {
                $result = trim($this->ctword($arrnum[0])) . " koma " . trim($this->ctword($arrnum[1]));
            }
        }
        switch ($style) {
            case 1: //1=uppercase  dan
                $result = strtoupper($result);
                break;
            case 2: //2= lowercase
                $result = strtolower($result);
                break;
            case 3: //3= uppercase on first letter for each word
                $result = ucwords($result);
                break;
            default: //4= uppercase on first letter
                $result = ucfirst($result);
                break;
        }
        return $result;
    }


    public function ctword($x) {
        $x = abs($x);
        $number = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if ($x < 12) {
            $temp = " " . $number[$x];
        } else if ($x < 20) {
            $temp = $this->ctword($x - 10) . " belas";
        } else if ($x < 100) {
            $temp = $this->ctword($x / 10) . " puluh" . $this->ctword($x % 10);
        } else if ($x < 200) {
            $temp = " seratus" . $this->ctword($x - 100);
        } else if ($x < 1000) {
            $temp = $this->ctword($x / 100) . " ratus" . $this->ctword($x % 100);
        } else if ($x < 2000) {
            $temp = " seribu" . $this->ctword($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->ctword($x / 1000) . " ribu" . $this->ctword($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->ctword($x / 1000000) . " juta" . $this->ctword($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->ctword($x / 1000000000) . " milyar" . $this->ctword(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->ctword($x / 1000000000000) . " trilyun" . $this->ctword(fmod($x, 1000000000000));
        }
        return $temp;
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=LKRinciantagihanpasienpenunjangV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rjrinciantagihanpasien-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $id = $_REQUEST['id'];
            $modPendaftaran = LKPendaftaranT::model()->findByPk($id);
            // $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
            // $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
            
     
            $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
            if(empty($modPenunjang)){
                $criteria = new CDbCriteria;
                $criteria->with = array('pasienadmisi');
                $criteria->compare('pasienadmisi.pendaftaran_id', $id);
                $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                $modPenunjang = PasienmasukpenunjangT::model()->find($criteria);
            }
               
            $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $id,'ruangan_id'=>Yii::app()->user->getState('ruangan_id')), array('order'=>'ruangan_id'));
         
            
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $data['judulLaporan']='Data Rincian Tagihan Pasien';
            $caraPrint=$_REQUEST['caraPrint'];
            
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('laboratorium.views.rinciantagihanpasienpenunjangV.rincian', array('modPenunjang'=>$modPenunjang,'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
                //$this->render('rincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('laboratorium.views.rinciantagihanpasienpenunjangV.rincian',array('modPenunjang'=>$modPenunjang,'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $style = '<style>.control-label{float:left; text-align: right; width:140px;font-size:12px; color:black;padding-right:10px;  }</style>';
                $mpdf->WriteHTML($style, 1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('laboratorium.views.rinciantagihanpasienpenunjangV.rincian',array('modPenunjang'=>$modPenunjang,'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function actionRincianKasirLabPrint($id, $caraPrint) {
            if (!empty($id))
            {
                $format = new CustomFormat();
                $this->layout = '//layouts/frameDialog';
                $data['judulPrint'] = 'Rincian Biaya Sementara';
                $modPendaftaran = PendaftaranT::model()->findByPk($id);
                $criteria = new CDbCriteria();
                $criteria->order = 'ruangan_id';
                $criteria->addCondition('pendaftaran_id = '.$id);
                $criteria->compare('ruangan_id',Yii::app()->user->getState('ruangan_id'));
                $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
                $modRincian = RinciantagihanpasienV::model()->findAll($criteria); 
                $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                    array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id),
                    'pembatalanuangmuka_id IS NULL'
                );

                $uang_cicilan = 0;
                foreach($uangmuka as $val)
                {
                    $uang_cicilan += $val->jumlahuangmuka;
                }

                $data['uang_cicilan'] = $uang_cicilan;
                $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                $data['jenis_cetakan'] = 'kwitansi';

                if($caraPrint == 'PDF')
                {
                    $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);
                    /*
                     * cara ambil margin
                     * tinggi_header * 72 / (72/25.4)
                     *  tinggi_header = inchi
                     */
                    $header = 0.75 * 72 / (72/25.4);
                    $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                    $mpdf->WriteHTML(
                        $this->renderPartial('laboratorium.views.rinciantagihanpasienpenunjangV.rincianPdf',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modRincian'=>$modRincian, 
                                'data'=>$data, 
                                'format'=>$format,
                            ), true
                        )
                    );
                    $mpdf->Output();  
                }

            }
        }
        /**
         * actionRincianKasirSudahBayarPrint = cetak rincian yang sudah bayar / lunas
         * @param type $id
         * @param type $caraPrint
         */
        public function actionRincianKasirLabSudahBayarPrint($id, $caraPrint) {
            if (!empty($id))
            {
                $format = new CustomFormat();
                $this->layout = '//layouts/frameDialog';
                $data['judulPrint'] = 'Rincian Biaya (Sudah Bayar / Lunas)';
                $criteria = new CDbCriteria();
                $criteria->addCondition('pendaftaran_id = '.$id);
                $criteria->compare('ruangan_id',Yii::app()->user->getState('ruangan_id'));
                $criteria->addCondition('tindakansudahbayar_id IS NOT NULL'); //sudah lunas
                $criteria->order = 'ruangan_id';
                $modRincian = RinciantagihapasiensudahlunasV::model()->findAll($criteria);                 
                //$modRincian = BKRinciantagihanpasienV::model()->findAll($criteria); 
                $modPendaftaran = PendaftaranT::model()->findByPk($id);
                $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                    array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id),
                    'pembatalanuangmuka_id IS NULL'
                );
                $uang_cicilan = 0;
                foreach($uangmuka as $val)
                {
                    $uang_cicilan += $val->jumlahuangmuka;
                }

                $data['uang_cicilan'] = $uang_cicilan;
                $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                $data['jenis_cetakan'] = 'kwitansi';

                if($caraPrint == 'PDF')
                {
                    $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);
                    /*
                     * cara ambil margin
                     * tinggi_header * 72 / (72/25.4)
                     *  tinggi_header = inchi
                     */
                    $header = 0.75 * 72 / (72/25.4);
                    $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                    $mpdf->WriteHTML(
                        $this->renderPartial('laboratorium.views.rinciantagihanpasienpenunjangV.rincianPdf',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modRincian'=>$modRincian, 
                                'data'=>$data, 
                                'format'=>$format,
                            ), true
                        )
                    );
                    $mpdf->Output();  
                }

            }
        }
}
