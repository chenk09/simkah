<?php

class LaporanController extends SBaseController {
    public $pathView = 'laboratorium.views.laporan.';
    public function actionLaporanSensusHarian() {
        $model = new LKLaporansensuslabV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $jenis = CHtml::listData(JenispemeriksaanlabM::model()->findAll('jenispemeriksaanlab_aktif = true'), 'jenispemeriksaanlab_id', 'jenispemeriksaanlab_id');
        $model->jenispemeriksaanlab_id = $jenis;
        $kunjungan = Kunjungan::items();
        $model->kunjungan = $kunjungan;
        $model->pilihan = $_GET['filter'];
        if (isset($_GET['LKLaporansensuslabV'])) {
            $model->attributes = $_GET['LKLaporansensuslabV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporansensuslabV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporansensuslabV']['tglAkhir']);
        }

        $this->render('sensus/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanSensusHarian() {
        $model = new LKLaporansensuslabV('search');
        $judulLaporan = 'Laporan Sensus Harian Laboratorium';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_REQUEST['type'];
        $model->pilihan = $_GET['filter'];
        if (isset($_REQUEST['LKLaporansensuslabV'])) {
            $model->attributes = $_REQUEST['LKLaporansensuslabV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporansensuslabV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporansensuslabV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'sensus/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikSensusHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKLaporansensuslabV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $model->pilihan = $_GET['filter'];
        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['LKLaporansensuslabV'])) {
            $model->attributes = $_GET['LKLaporansensuslabV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporansensuslabV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporansensuslabV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanKunjungan() {
        $model = new LKLaporanpasienpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $model->kunjungan = Kunjungan::items();
        if (isset($_GET['LKLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['LKLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanpasienpenunjangV']['tglAkhir']);
        }

        $this->render('kunjungan/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjungan() {
        $model = new LKLaporanpasienpenunjangV('search');
        $judulLaporan = 'Laporan Kunjungan Laboratorium';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Laboratorium';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LKLaporanpasienpenunjangV'])) {
            $model->attributes = $_REQUEST['LKLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanpasienpenunjangV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjungan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjungan() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKLaporanpasienpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['LKLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['LKLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanpasienpenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporan10BesarPenyakit() {
        $model = new LKLaporan10besarpenyakit('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $model->jumlahTampil = 10;

        if (isset($_GET['LKLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['LKLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporan10besarpenyakit']['tglAkhir']);
        }

        $this->render('10Besar/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporan10BesarPenyakit() {
        $model = new LKLaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit Pasien Laboratorium';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Pasien Laboratorium';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LKLaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['LKLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '10Besar/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKLaporan10besarpenyakit('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Laboratorium';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LKLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['LKLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporan10besarpenyakit']['tglAkhir']);
        }
               
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPemakaiObatAlkes()
    {
        $model = new LKLaporanpemakaiobatalkesV;
        $model->unsetAttributes();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $jenisObat =CHtml::listData(JenisobatalkesM::model()->findAll('jenisobatalkes_aktif = true'),'jenisobatalkes_id','jenisobatalkes_id');
        $model->jenisobatalkes_id = $jenisObat;
        if(isset($_GET['LKLaporanpemakaiobatalkesV']))
        {
            $model->attributes = $_GET['LKLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        $this->render('pemakaiObatAlkes/index',array('model'=>$model));
    }

    public function actionPrintLaporanPemakaiObatAlkes() {
        $model = new LKLaporanpemakaiobatalkesV('search');
        $judulLaporan = 'Laporan Info Pemakai Obat Alkes Laboratorium';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Laboratorium';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LKLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_REQUEST['LKLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemakaiObatAlkes/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    

    public function actionFrameGrafikLaporanPemakaiObatAlkes() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKLaporanpemakaiobatalkesV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Laboratorium';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LKLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['LKLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanpemakaiobatalkesV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanJasaInstalasi() {
        $model = new LKLaporanjasainstalasi('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $tindakan = array('sudah', 'belum');
        $model->tindakansudahbayar_id = $tindakan;
        if (isset($_GET['LKLaporanjasainstalasi'])) {
            $model->attributes = $_GET['LKLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanjasainstalasi']['tglAkhir']);
        }

        $this->render('jasaInstalasi/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanJasaInstalasi() {
        $model = new LKLaporanjasainstalasi('search');
        $judulLaporan = 'Laporan Jasa Instalasi Laboratorium';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LKLaporanjasainstalasi'])) {
            $model->attributes = $_REQUEST['LKLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanjasainstalasi']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jasaInstalasi/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJasaInstalasi() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKLaporanjasainstalasi('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LKLaporanjasainstalasi'])) {
            $model->attributes = $_GET['LKLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanjasainstalasi']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBiayaPelayanan() {
        $model = new LKLaporanbiayapelayanan('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE'),'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['LKLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['LKLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanbiayapelayanan']['tglAkhir']);
        }

        $this->render('biayaPelayanan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanBiayaPelayanan() {
        $model = new LKLaporanbiayapelayanan('search');
        $judulLaporan = 'Laporan Biaya Pelayanan Laboratorium';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Laboratorium';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LKLaporanbiayapelayanan'])) {
            $model->attributes = $_REQUEST['LKLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanbiayapelayanan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'biayaPelayanan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanBiayaPelayanan() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKLaporanbiayapelayanan('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Laboratorium';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LKLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['LKLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanbiayapelayanan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPendapatanRuangan() {
        $model = new LBLaporanpendapatanpenunjangkomponenV('searchPendapatanR');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['LBLaporanpendapatanpenunjangkomponenV'])) {
            $model->attributes = $_GET['LBLaporanpendapatanpenunjangkomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpendapatanpenunjangkomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpendapatanpenunjangkomponenV']['tglAkhir']);
        }

        $this->render('pendapatanRuangan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanPendapatanRuangan() {
        $model = new LBLaporanpendapatanpenunjangkomponenV('searchPrintPendapatanR');
        $judulLaporan = 'Laporan Grafik Pendapatan Ruangan Laboratorium';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LBLaporanpendapatanpenunjangkomponenV'])) {
            $model->attributes = $_REQUEST['LBLaporanpendapatanpenunjangkomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpendapatanpenunjangkomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpendapatanpenunjangkomponenV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pendapatanRuangan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPendapatanRuangan() {
        $this->layout = '//layouts/frameDialog';
        $model = new LBLaporanpendapatanpenunjangkomponenV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LBLaporanpendapatanpenunjangkomponenV'])) {
            $model->attributes = $_GET['LBLaporanpendapatanpenunjangkomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpendapatanpenunjangkomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpendapatanpenunjangkomponenV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBukuRegister() {
        $model = new LKBukuregisterpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        if (isset($_GET['LKBukuregisterpenunjangV'])) {
            $model->attributes = $_GET['LKBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKBukuregisterpenunjangV']['tglAkhir']);
        }

        $this->render('bukuRegister/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanBukuRegister() {
        $model = new LKBukuregisterpenunjangV('search');
        $judulLaporan = 'Laporan Buku Register Pasien Laboratorium';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Laboratorium';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LKBukuregisterpenunjangV'])) {
            $model->attributes = $_REQUEST['LKBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKBukuregisterpenunjangV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuRegister/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKBukuregisterpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Laboratorium';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LKBukuregisterpenunjangV'])) {
            $model->attributes = $_GET['LKBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKBukuregisterpenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanCaraMasukPasien() {
        $model = new LKLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $asalrujukan = CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'), 'asalrujukan_id', 'asalrujukan_id');
        $model->asalrujukan_id = $asalrujukan;
        $ruanganasal = CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'),'ruangan_id','ruangan_id');
        $model->ruanganasal_id = $ruanganasal;
        if (isset($_GET['LKLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['LKLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('caraMasuk/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanCaraMasukPasien() {
        $model = new LKLaporancaramasukpenunjangV('search');
        $judulLaporan = 'Laporan Cara Masuk Pasien Laboratorium';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['LKLaporancaramasukpenunjangV'])) {
            $model->attributes = $_REQUEST['LKLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'caraMasuk/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanCaraMasukPasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LKLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['LKLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPemeriksaanPenunjang() {
        $judulLaporan = 'Laporan Jenis Pemeriksaan Laboratorium';
        $model = new LBLaporanpemeriksaanpenunjangV('searchTableLaporan');
        $modDetail = new LBLaporandetailpemeriksaanpenunjangV('searchTableLaporan');
        
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        
        $modDetail->tglAwal = date('d M Y 00:00:00');
        $modDetail->tglAkhir = date('d M Y 23:59:59');

//        if (isset($_GET['filter_tab'])){
            if($_GET['filter_tab'] == "rekap"){
                if (isset($_GET['LBLaporanpemeriksaanpenunjangV'])) {
                    $model->attributes = $_GET['LBLaporanpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
                    
                }
            } else
            if($_GET['filter_tab'] == "detail"){
                if (isset($_GET['LBLaporandetailpemeriksaanpenunjangV'])) {
                    $modDetail->attributes = $_GET['LBLaporandetailpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $modDetail->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
                    $modDetail->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
                        if($_GET['LBLaporandetailpemeriksaanpenunjangV']['pilihDokter'] == "RUJUKANLUAR"){
                            $modDetail->namapegawai = $_GET['LBLaporandetailpemeriksaanpenunjangV']['nama_perujuk'];
                            $modDetail->rujukan_id = $_GET['LBLaporandetailpemeriksaanpenunjangV']['rujukan_id'];
//                            echo $modDetail->namapegawai;
                        }else{
                            $modDetail->namapegawai = $_GET['LBLaporandetailpemeriksaanpenunjangV']['namapegawai'];
//                            echo $modDetail->namapegawai;
                        }   
                    $modDetail->penjamin_id = $_GET['LBLaporandetailpemeriksaanpenunjangV']['penjamin_id'];
                    $modDetail->carabayar_id = $_GET['LBLaporandetailpemeriksaanpenunjangV']['carabayar_id'];
                }
            }
//        }
        
        
        $this->render('pemeriksaanPenunjang/admin', array(
            'model' => $model,
            'modDetail'=>$modDetail,
            'tglAwal'=>$modDetail->tglAwal,
            'tglAkhir'=>$modDetail->tglAkhir,
            'judulLaporan' => $judulLaporan,
        ));
    }

    public function actionPrintLaporanPemeriksaanPenunjang() {
        $model = new LBLaporanpemeriksaanpenunjangV('searchPrintLaporan');
        $modDetail = new LBLaporandetailpemeriksaanpenunjangV('searchPrintLaporan');
        
        //Data Grafik
        $data['title'] = 'Grafik Laporan Jenis Pemeriksaan Laboratorium';
        $data['type'] = $_REQUEST['type'];
        
//        if (isset($_REQUEST['filter_tab'])){
            if($_REQUEST['filter_tab'] == "rekap"){
                $judulLaporan = 'Laporan Jenis Pemeriksaan Laboratorium - REKAP';
                if (isset($_REQUEST['LBLaporanpemeriksaanpenunjangV'])) {
                    $model->attributes = $_REQUEST['LBLaporanpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
                }
            } else
            if($_REQUEST['filter_tab'] == "detail"){
                $judulLaporan = 'Laporan Jenis Pemeriksaan Laboratorium - DETAIL';
//                if (isset($_REQUEST['LBLaporandetailpemeriksaanpenunjangV'])) {
                    $modDetail->attributes = $_REQUEST['LBLaporandetailpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $modDetail->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
                    $modDetail->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
                    if($_GET['LBLaporandetailpemeriksaanpenunjangV']['pilihDokter'] == "RUJUKANLUAR"){
                        $modDetail->namapegawai = $_GET['LBLaporandetailpemeriksaanpenunjangV']['nama_perujuk'];
                    }else{
                        $modDetail->namapegawai = $_GET['LBLaporandetailpemeriksaanpenunjangV']['namapegawai'];
                    }  
//                }
            }
//        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemeriksaanPenunjang/_print';
        
        $this->printFunctionPemeriksaan($model, $modDetail, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPemeriksaanPenunjang() {
        $this->layout = '//layouts/frameDialog';
        $model = new LBLaporanpemeriksaanpenunjangV('search');
        $modDetail = new LBLaporandetailpemeriksaanpenunjangV('search');
        //Data Grafik
        $data['title'] = 'Grafik Laporan Jenis Pemeriksaan Laboratorium';
        $data['type'] = $_GET['type'];
        
//        if (isset($_GET['filter_tab'])){
            if($_GET['filter_tab'] == "rekap"){
                if (isset($_GET['LBLaporanpemeriksaanpenunjangV'])) {
                    $model->attributes = $_GET['LBLaporanpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
                }
            } else
            if($_GET['filter_tab'] == "detail"){
//                if (isset($_GET['LBLaporandetailpemeriksaanpenunjangV'])) {
                    $modDetail->attributes = $_GET['LBLaporandetailpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $modDetail->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
                    $modDetail->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
//                }
            }
//        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'modDetail'=>$modDetail,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPemeriksaanRujukan() {
        $model = new LBLaporanpemeriksaanrujukanV('search');
        $modelRS = new LBLaporanpemeriksaanrujukanrsV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        if (isset($_GET['LBLaporanpemeriksaanrujukanV'])) {
            $model->attributes = $_GET['LBLaporanpemeriksaanrujukanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanrujukanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanrujukanV']['tglAkhir']);
            $model->no_pendaftaran = $_GET['LBLaporanpemeriksaanrujukanV']['no_pendaftaran'];
            
            $modelRS->attributes = $_GET['LBLaporanpemeriksaanrujukanV'];
            $format = new CustomFormat();
            $modelRS->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanrujukanV']['tglAwal']);
            $modelRS->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanrujukanV']['tglAkhir']);            
            $modelRS->no_pendaftaran = $_GET['LBLaporanpemeriksaanrujukanV']['no_pendaftaran'];            
        }

        $this->render('pemeriksaanRujukan/adminPemeriksaanRujukan', array(
            'model' => $model,
            'modelRS'=>$modelRS,
        ));
    }

    public function actionPrintLaporanPemeriksaanRujukan() {
//        $model = LBLaporanpemeriksaanrujukanV::model()->findAll();
        $model = new LBLaporanpemeriksaanrujukanV('search');
        $modelRS = new LBLaporanpemeriksaanrujukanrsV('search');

        // echo count($_REQUEST['filter_nama']);

        if($_REQUEST['filter_nama'] == "luar"){
            
            $judulLaporan = 'Laporan Pemeriksaan Rujukan Pasien Luar';
            $data['title'] = 'Grafik Laporan Pemeriksaan Rujukan Pasien Luar';
        }else if($_REQUEST['filter_nama'] == "rs"){
            
             $judulLaporan = 'Laporan Pemeriksaan Rujukan Pasien RSJK';
             $data['title'] = 'Grafik Laporan Pemeriksaan Rujukan Pasien RSJK';
        }
        $data['type'] = $_REQUEST['type'];
            if (isset($_REQUEST['LBLaporanpemeriksaanrujukanV'])) {
                $model->attributes = $_REQUEST['LBLaporanpemeriksaanrujukanV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanrujukanV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanrujukanV']['tglAkhir']);
                $model->no_pendaftaran = $_REQUEST['LBLaporanpemeriksaanrujukanV']['no_pendaftaran'];

                $modelRS->attributes = $_REQUEST['LBLaporanpemeriksaanrujukanV'];
                $format = new CustomFormat();
                $modelRS->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanrujukanV']['tglAwal']);
                $modelRS->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanrujukanV']['tglAkhir']);
                $modelRS->no_pendaftaran =$_REQUEST['LBLaporanpemeriksaanrujukanV']['no_pendaftaran'];
            }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemeriksaanRujukan/_printPemeriksaanRujukan';
        
        $this->printFunctionRujukan($model,$modelRS, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPemeriksaanRujukan() {
        $this->layout = '//layouts/frameDialog';
        $model = new LBLaporanpemeriksaanrujukanV('search');
        $modelRS = new LBLaporanpemeriksaanrujukanrsV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        //Data Grafik
        $data['title'] = 'Grafik Laporan Pemeriksaan Rujukan';
        $data['type'] = $_GET['type'];
       
        
        if (isset($_GET['LBLaporanpemeriksaanrujukanV'])) {
            $model->attributes = $_GET['LBLaporanpemeriksaanrujukanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanrujukanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanrujukanV']['tglAkhir']);
            $model->no_pendaftaran = $_GET['LBLaporanpemeriksaanrujukanV']['no_pendaftaran'];
            
            $modelRS->attributes = $_GET['LBLaporanpemeriksaanrujukanV'];
            $format = new CustomFormat();
            $modelRS->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanrujukanV']['tglAwal']);
            $modelRS->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanrujukanV']['tglAkhir']);
            $modelRS->no_pendaftaran = $_GET['LBLaporanpemeriksaanrujukanV']['no_pendaftaran'];
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'modelRS'=>$modelRS,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPemeriksaanCaraBayar() {
        $model = new LBLaporanpemeriksaangroupV('search');
        $modelPerusahaan = new LBLaporanpemeriksaanp3V('search');
        $model->tglAwal = date('d M y  00:00:00' );
        $model->tglAkhir = date('d M y 23:59:59');
        
        if (isset($_GET['LBLaporanpemeriksaangroupV'])) {
            $model->attributes = $_GET['LBLaporanpemeriksaangroupV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAkhir']);
        
            $modelPerusahaan->attributes = $_GET['LBLaporanpemeriksaangroupV'];
            $format = new CustomFormat();
            $modelPerusahaan->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAwal']);
            $modelPerusahaan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAkhir']);
        }

        $this->render('pemeriksaanCaraBayar/adminPemeriksaanCaraBayar', array(
            'model' => $model,
            'modelPerusahaan'=>$modelPerusahaan,
        ));
    }

    public function actionPrintLaporanPemeriksaanCaraBayar() {
        $model = new LBLaporanpemeriksaangroupV('search');
        $modelPerusahaan = new LBLaporanpemeriksaanp3V('search');
        $judulLaporan = 'Laporan Pemeriksaan Cara Bayar';

//        var_dump($model);
//        var_dump($modelPerusahaan);
         if($_GET['filter_tab'] == "pemeriksaan"){
            $judulLaporan = 'LAPORAN PEMERIKSAAN CARA BAYAR';
            $data['title'] = 'Grafik Laporan Pemeriksaan Cara Bayar';
      }else if($_GET['filter_tab'] == "rincian"){
            
             $judulLaporan = 'LAPORAN PEMERIKSAAN CARA BAYAR';
             $data['title'] = 'Grafik Laporan Pemeriksaan Cara Bayar';
        }
        
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LBLaporanpemeriksaangroupV'])) {
            $model->attributes = $_REQUEST['LBLaporanpemeriksaangroupV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaangroupV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaangroupV']['tglAkhir']);
            $model->carabayar_id = $_REQUEST['LBLaporanpemeriksaangroupV']['carabayar_id'];
            
            $modelPerusahaan->attributes = $_REQUEST['LBLaporanpemeriksaangroupV'];
            $format = new CustomFormat();
            $modelPerusahaan->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaangroupV']['tglAwal']);
            $modelPerusahaan->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaangroupV']['tglAkhir']);
            $modelPerusahaan->carabayar_id = $_REQUEST['LBLaporanpemeriksaangroupV']['carabayar_id'];
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemeriksaanCaraBayar/_printPemeriksaanCaraBayar';

        $this->printFunctionCaraBayar($model, $modelPerusahaan, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPemeriksaanCaraBayar() {
        $this->layout = '//layouts/frameDialog';
        $model = new LBLaporanpemeriksaangroupV('search');
        $modelPerusahaan = new LBLaporanpemeriksaanp3V('search');
        $model->tglAwal = date('Y-m-d');
        $model->tglAkhir = date('Y-m-d');
        //Data Grafik
       if($_GET['filter_tab'] == "pemeriksaan"){
            
            $data['title'] = 'Grafik Laporan Pemeriksaan Group BK';
        }else if($_GET['filter_tab'] == "rincian"){
            
             $data['title'] = 'Grafik Laporan Pemeriksaan Kontrak P3';
        }
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['LBLaporanpemeriksaangroupV'])) {
            $model->attributes = $_GET['LBLaporanpemeriksaangroupV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAkhir']);
            
            $modelPerusahaan->attributes = $_GET['LBLaporanpemeriksaangroupV'];
            $format = new CustomFormat();
            $modelPerusahaan->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAwal']);
            $modelPerusahaan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'modelPerusahaan'=>$modelPerusahaan,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPembayaranPemeriksaan() {
        $model = new LBLaporanpembayaranpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
      if (isset($_GET['LBLaporanpembayaranpenunjangV']))
        {
            $model->attributes = $_GET['LBLaporanpembayaranpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpembayaranpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpembayaranpenunjangV']['tglAkhir']);
            $model->no_pendaftaran = $_GET['LBLaporanpembayaranpenunjangV']['no_pendaftaran'];
        }

        $this->render('pembayaranPemeriksaan/adminPembayaranPemeriksaan', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanPembayaranPemeriksaan() {
        $model = new LBLaporanpembayaranpenunjangV('search');
        $judulLaporan = 'Laporan Pembayaran Pemeriksaan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pembayaran Pemeriksaan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LBLaporanpembayaranpenunjangV'])) {
            $model->attributes = $_REQUEST['LBLaporanpembayaranpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpembayaranpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpembayaranpenunjangV']['tglAkhir']);
            $model->no_pendaftaran = $_GET['LBLaporanpembayaranpenunjangV']['no_pendaftaran'];
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pembayaranPemeriksaan/_printPembayaranPemeriksaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPembayaranPemeriksaan() {
        $this->layout = '//layouts/frameDialog';
        $model = new LBLaporanpembayaranpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        //Data Grafik
        $data['title'] = 'Grafik Laporan Pembayaran Pemeriksaan';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['LBLaporanpembayaranpenunjangV'])) {
            $model->attributes = $_GET['LBLaporanpembayaranpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpembayaranpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpembayaranpenunjangV']['tglAkhir']);
            $model->no_pendaftaran = $_GET['LBLaporanpembayaranpenunjangV']['no_pendaftaran'];
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    /*
     * Laporan Pasien DBD
     */
    
    public function actionLaporanPasienDBD() {
        $model = new LKLaporanpasienpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        if (isset($_GET['LKLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['LKLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanpasienpenunjangV']['tglAkhir']);
        }

        $this->render('pasienDBD/index', array(
            'model' => $model,
            'tglAwal'=>$model->tglAwal,
            'tglAkhir'=>$model->tglAkhir,
        ));
    }

    public function actionPrintLaporanPasienDBD() {
        $model = new LKLaporanpasienpenunjangV('search');
        $judulLaporan = 'Laporan Pasien DBD';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien DBD';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LKLaporanpasienpenunjangV'])) {
            $model->attributes = $_REQUEST['LKLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanpasienpenunjangV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pasienDBD/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPasienDBD() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKLaporanpasienpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien DBD';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['LKLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['LKLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanpasienpenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    /*
     * end Laporan Pasien DBD
     */
    
    /*
     * laporan pendapatan II , 19 September 2013 - owner : Miranitha
     */
    
    public function actionLaporanPendapatan() {
        $model = new LKLaporanpendapatanruanganV('searchTableLaporan');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['LKLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['LKLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanpendapatanruanganV']['tglAkhir']);
        }

        $this->render('pendapatan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanPendapatan() {
        $model = new LKLaporanpendapatanruanganV('search');
        if($_GET['filter_tab'] == "rs"){
            $judulLaporan = 'Laporan Pendapatan Ruangan Laboratorium dari RS';
        }else if($_GET['filter_tab'] == "luar"){
            $judulLaporan = 'Laporan Pendapatan Ruangan Laboratorium dari Luar RS';
        }else{
            $judulLaporan = 'Laporan Pendapatan Ruangan Laboratorium';
        }
        
        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LKLaporanpendapatanruanganV'])) {
            $model->attributes = $_REQUEST['LKLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKLaporanpendapatanruanganV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pendapatan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPendapatan() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKLaporanpendapatanruanganV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LKLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['LKLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKLaporanpendapatanruanganV']['tglAkhir']);
        }
                
        $this->render('pendapatan/_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    /*
     * end laporan pendapatan II
     */
    
    /**
     * untuk Laporan Jasa Dokter - Laboratorium link : laboratorium/laporan/laporanJasaDokter
     * Created By : Miranitha Fasha - 17 Feb 2013 TASK : EHJ - 823
     */
        public function actionLaporanJasaDokter() {
            $model = new LBLaporanjasadokterlabradV;
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('Y-m-d 23:59:59');
			if (isset($_GET['LBLaporanjasadokterlabradV']))
			{
				$model->attributes = $_GET['LBLaporanjasadokterlabradV'];
				$format = new CustomFormat();
				$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanjasadokterlabradV']['tglAwal']);
				$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanjasadokterlabradV']['tglAkhir']);
				
				if($_GET['LBLaporanjasadokterlabradV']['pilihDokter'] == "RUJUKANLUAR")
				{
					$model->namapegawai = $_GET['LBLaporanjasadokterlabradV']['nama_perujuk'];
					$model->namaperujuk = $_GET['LBLaporanjasadokterlabradV']['namaperujuk'];
				}else{
					$model->namapegawai = $_GET['LBLaporanjasadokterlabradV']['nama_pegawai'];
				}
				
				$model->penjamin_id = $_GET['LBLaporanjasadokterlabradV']['penjamin_id'];
				$model->carabayar_id = $_GET['LBLaporanjasadokterlabradV']['carabayar_id'];
			}   

            $this->render('jasaDokter/index', array(
                'model' => $model,
            ));
        }

        public function actionPrintLaporanJasaDokter()
		{
            $model = new LBLaporanjasadokterlabradV('searchPrintLaporan');
            $judulLaporan = 'Laporan Jasa Dokter';

            //Data Grafik
            $data['title'] = 'Grafik Laporan Jasa Dokter';
            $data['type'] = $_REQUEST['type'];
			
            if (isset($_REQUEST['LBLaporanjasadokterlabradV'])) {
                $model->attributes = $_REQUEST['LBLaporanjasadokterlabradV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanjasadokterlabradV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanjasadokterlabradV']['tglAkhir']);
				
				if($_GET['LBLaporanjasadokterlabradV']['pilihDokter'] == "RUJUKANLUAR")
				{
					$model->namapegawai = $_GET['LBLaporanjasadokterlabradV']['nama_perujuk'];
					$model->namaperujuk = $_GET['LBLaporanjasadokterlabradV']['namaperujuk'];
				}else{
					$model->namapegawai = $_GET['LBLaporanjasadokterlabradV']['nama_pegawai'];
				}
				
				$model->penjamin_id = $_GET['LBLaporanjasadokterlabradV']['penjamin_id'];
				$model->carabayar_id = $_GET['LBLaporanjasadokterlabradV']['carabayar_id'];				
            }

            $caraPrint = $_REQUEST['caraPrint'];
            $target = 'jasaDokter/_print';

            $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

        public function actionFrameGrafikJasaDokter() {
            $this->layout = '//layouts/frameDialog';
            $model = new LBLaporanjasadokterlabradV('searchLaporan');
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('Y-m-d 23:59:59');
            $model->pilihan = $_GET['filter'];
            //Data Grafik
            $data['title'] = 'Grafik Laporan Sensus Harian';
            $data['type'] = $_GET['type'];

            if (isset($_GET['LBLaporanjasadokterlabradV'])) {
                $model->attributes = $_GET['LBLaporanjasadokterlabradV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanjasadokterlabradV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanjasadokterlabradV']['tglAkhir']);
            }

            $this->render('_grafik', array(
                'model' => $model,
                'data' => $data,
            ));
        }
    
    /*
     * end untuk Laporan Jasa Dokter - Laboratorium link : laboratorium/laporan/laporanJasaDokter
     */
        
    protected function printFunction($model,$data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    protected function printFunctionRujukan($model, $modelRS, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'modelRS'=>$modelRS, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'modelRS'=>$modelRS,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'modelRS'=>$modelRS,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
     protected function printFunctionCaraBayar($model, $modelPerusahaan, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model,'modelPerusahaan'=>$modelPerusahaan,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model,'modelPerusahaan'=>$modelPerusahaan,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
//            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'modelPerusahaan'=>$modelPerusahaan,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    protected function printFunctionPemeriksaan($model, $modDetail, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
         if($_GET['filter_tab'] == "rekap"){
            $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        }else if($_GET['filter_tab'] == "detail"){
            $periode = $this->parserTanggal($modDetail->tglAwal).' s/d '.$this->parserTanggal($modDetail->tglAkhir);
        } 
//        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'modDetail'=>$modDetail, 'periode'=>$periode,'periodeDetail'=>$periodeDetail, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'modDetail'=>$modDetail,'periode'=>$periode, 'periodeDetail'=>$periodeDetail, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'modDetail'=>$modDetail, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint),true));
            $mpdf->Output();
        }
    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }

}