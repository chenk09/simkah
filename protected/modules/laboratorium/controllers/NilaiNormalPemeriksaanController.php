<?php

class NilaiNormalPemeriksaanController extends SBaseController
{
        /**
	 * @return array action filters
	 */
        public $pathView = 'laboratorium.views.nilaiNormalPemeriksaan.';
        public $defaultAction = 'admin';
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
        
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{
                $dataProvider=new CActiveDataProvider('LKNilaiRujukanM');
		$this->render($this->pathView.'index',array(
			'dataProvider'=>$dataProvider,
		));
	}
        
       public function actionView($id)
	{
		$this->render($this->pathView.'view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new LKNilaiRujukanM('search');
                    $model->unsetAttributes();  // clear any default values
		if(isset($_GET['LKNilaiRujukanM']))
			$model->attributes=$_GET['LKNilaiRujukanM'];

		$this->render($this->pathView.'admin',array(
			'model'=>$model,
		));
	}
        
        public function actionUpdate($id)
        {
            $model = $this->loadModel($id);
            $modPemLab = new LKPemeriksaanLabM;
            
            if(isset($_POST['LKNilaiRujukanM'])){
                $model->Attributes = $_POST['LKNilaiRujukanM'];
                
                if($model->validate()){
                    if($model->save()){
                         Yii::app()->user->setFlash('success',"Data Berhasil disimpan"); 
                         $this->redirect(array('admin')); 
                    }else{
                         Yii::app()->user->setFlash('error',"Data Gagal disimpan"); 
                         $this->redirect(array('update','id'=>$model->pasien_id)); 
                    }
                    $this->refresh();
                }
            }
            
            $this->render($this->pathView.'update',array(
			'model'=>$model,
            ));
        }
        
        public function actionCreate()
        {
            $model = new LKNilaiRujukanM;
            $modPemLab = new LKPemeriksaanLabM;
            
            if(isset($_POST['nilaiNormal'])){
                $nilaiNormal = $_POST['nilaiNormal'];
//                echo '<pre>'.print_r($_POST,1).'</pre>';exit;
                foreach($_POST['nilaiJeniskelamin'] as $i=>$jenisKelamin) {
                    foreach ($nilaiNormal as $kelamin => $kelUmur) {
                        if($jenisKelamin == $kelamin){
                            foreach ($kelUmur as $kel=>$nilai){
                                $model = new LKNilaiRujukanM;
                                $model->kelompokumur = $kel;
                                $model->nilairujukan_jeniskelamin = $kelamin;
                                $model->nilairujukan_min = $nilai['nilaiMin'];
                                $model->nilairujukan_max = $nilai['nilaiMax'];
                                $model->nilairujukan_nama = $nilai['nilaiNama'];
                                $model->kelompokdet = $_POST['LKNilaiRujukanM']['kelompokdet'];
                                $model->namapemeriksaandet = $_POST['LKNilaiRujukanM']['namapemeriksaandet'];
                                $model->nilairujukan_satuan = $_POST['LKNilaiRujukanM']['nilairujukan_satuan'];
                                $model->nilairujukan_metode = $_POST['LKNilaiRujukanM']['nilairujukan_metode'];
                                $model->nilairujukan_keterangan = $_POST['LKNilaiRujukanM']['nilairujukan_keterangan'];
                                $model->nilairujukan_aktif = true;
                                if($model->save()){
                                    $pemDetail = new PemeriksaanlabdetM;
                                    $pemDetail->pemeriksaanlab_id = $_POST['LKPemeriksaanLabM']['pemeriksaanlab_id'];
                                    $pemDetail->nilairujukan_id = $model->nilairujukan_id;
                                    $pemDetail->pemeriksaanlabdet_nourut = 1;
                                    $pemDetail->save();
                                }
                            }
                        }
                    }
                }
                 Yii::app()->user->setFlash('success',"Data Berhasil disimpan"); 
                 $this->redirect(array('admin','modulId'=>Yii::app()->session['modulId'])); 
            }
            
            $this->render($this->pathView.'create',array(
			'model'=>$model,
                        'modPemLab'=>$modPemLab,
            ));
        }
        
        /**
        * Returns the data model based on the primary key given in the GET variable.
        * If the data model is not found, an HTTP exception will be raised.
        * @param integer the ID of the model to be loaded
        */
        public function loadModel($id)
        {
            $model=LKNilaiRujukanM::model()->findByPk($id);
            if($model===null)
                throw new CHttpException(404,'The requested page does not exist.');
            return $model;
        }

        /**
        * Performs the AJAX validation.
        * @param CModel the model to be validated
        */
        protected function performAjaxValidation($model)
        {
            if(isset($_POST['ajax']) && $_POST['ajax']==='lknilai-rujukan-m-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }

    public function actionDelete()
    {
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        if(Yii::app()->request->isPostRequest)
        {
            $id = $_POST['id'];
            $modelPemeriksaanLabDet=LKPemeriksaanlabdetM::model()->findByAttributes(array('nilairujukan_id'=>$id));
            $modelPemeriksaanLabDet->delete();
            $this->loadModel($id)->delete();
            if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                        ));
                    exit;
                }
                        
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

     /**
     *Mengubah status aktif
     * @param type $id 
     */
    public function actionRemoveTemporary()
    {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                    SAPropinsiM::model()->updateByPk($id, array('propinsi_aktif'=>false));
//                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
              
        
        $id = $_POST['id'];   
        if(isset($_POST['id']))
        {
           $update = LKNilaiRujukanM::model()->updateByPk($id,array('nilairujukan_aktif'=>false));
           if($update)
            {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
             }
        } else {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
        }

    }

        public function actionPrint()
        {
            $model= new LKNilaiRujukanM;
            $model->attributes=$_REQUEST['LKNilaiRujukanM'];
            $judulLaporan='Data Nilai Rujukan';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}