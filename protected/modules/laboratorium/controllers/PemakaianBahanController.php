<?php

class PemakaianBahanController extends SBaseController
{
    public $pathView = 'laboratorium.views.pemakaianBahan.';
    protected $successSavePemakaianBahan = true;
    
	public function actionIndex()
	{
            $modPendaftaran = new LKPendaftaranT;
            $modPasien = new LKPasienM;
            if($_GET['id'] != null){
                $modPendaftaran = LKPendaftaranT::model()->findByPk($_GET['id']);
                $modPasien = LKPasienM::model()->findByPk($modPendaftaran->pasien_id);
            }
            if(isset($_POST['pemakaianBahan'])){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modPemakainBahans = $this->savePemakaianBahan($modPendaftaran, $_POST['pemakaianBahan']);
                    if($this->successSavePemakaianBahan) {
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                        $this->redirect(array('index','id'=>$_POST['LKPendaftaranT']['pendaftaran_id']));
                    } else {
                        $transaction->rollback();
                        //Yii::app()->user->setFlash('error',"Data tidak valid ");
                        Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceObatAlkesPasien($modPemakainBahans));
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            if($_GET['id'] != null){
                $modViewBmhp = LKObatAlkesPasienT::model()->with('obatalkes')->findAllByAttributes(array('pendaftaran_id'=>$_GET['id']));
            }
            
            
            $this->render($this->pathView.'index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modViewBmhp'=>$modViewBmhp,
                                    ));
	}
        
        protected function savePemakaianBahan($modPendaftaran,$pemakaianBahan)
        {
            $valid = true;
            foreach ($pemakaianBahan as $i => $bmhp) {
                $modPakaiBahan[$i] = new LKObatAlkesPasienT;
                $modPakaiBahan[$i]->pendaftaran_id = $_POST['LKPendaftaranT']['pendaftaran_id'];
                $modPakaiBahan[$i]->penjamin_id = $_POST['LKPendaftaranT']['penjamin_id'];
                $modPakaiBahan[$i]->carabayar_id = $_POST['LKPendaftaranT']['carabayar_id'];
                $modPakaiBahan[$i]->daftartindakan_id = $bmhp['daftartindakan_id'];
                $modPakaiBahan[$i]->sumberdana_id = $bmhp['sumberdana_id'];
                $modPakaiBahan[$i]->pasien_id = $_POST['LKPendaftaranT']['pasien_id'];
                $modPakaiBahan[$i]->satuankecil_id = $bmhp['satuankecil_id'];
                $modPakaiBahan[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modPakaiBahan[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                $modPakaiBahan[$i]->obatalkes_id = $bmhp['obatalkes_id'];
                $modPakaiBahan[$i]->pegawai_id = $_POST['LKPendaftaranT']['pegawai_id'];
                $modPakaiBahan[$i]->kelaspelayanan_id = $_POST['LKPendaftaranT']['kelaspelayanan_id'];
                $modPakaiBahan[$i]->shift_id = Yii::app()->user->getState('shift_id');
                $modPakaiBahan[$i]->tglpelayanan = date('Y-m-d H:i:s');
                $modPakaiBahan[$i]->qty_oa = $bmhp['qty'];
                $modPakaiBahan[$i]->hargajual_oa = $bmhp['subtotal'];
                $modPakaiBahan[$i]->harganetto_oa = $bmhp['harganetto'];
                $modPakaiBahan[$i]->hargasatuan_oa = $bmhp['hargasatuan'];

                $valid = $modPakaiBahan[$i]->validate() && $valid;
                if($valid) {
                    $modPakaiBahan[$i]->save();
                    $updateStatusPeriksa=PendaftaranT::model()->updateByPk($_POST['LKPendaftaranT']['pendaftaran_id'],array('statusperiksa'=>Params::statusPeriksa(2)));
                    PendaftaranT::model()->updateByPk($_POST['LKPendaftaranT']['pendaftaran_id'],
                        array(
                            'pembayaranpelayanan_id'=>null
                        )
                    );
                    if($modPakaiBahan[$i]->qty_oa < StokobatalkesT::getStokBarang($modPakaiBahan[$i]->qty_oa, Yii::app()->user->getState('ruangan_id'))){
                        $this->successSavePemakaianBahan = false;
                    }else{
                        StokobatalkesT::kurangiStok($modPakaiBahan[$i]->qty_oa, $modPakaiBahan[$i]->obatalkes_id);
                        $this->successSavePemakaianBahan = true;
                    }
                } else {
                    $this->successSavePemakaianBahan = false;
                }
            }
            
            return $modPakaiBahan;
        }

        private function traceObatAlkesPasien($modObatPasiens)
        {
            foreach ($modObatPasiens as $key => $modObatPasien) {
                $echo .= "<pre>".print_r($modObatPasien->attributes,1)."</pre>";
            }
            return $echo;
        }
        
        protected function kembalikanStok($obatAlkesT)
        {
            foreach ($obatAlkesT as $i => $obatAlkes) {
                $stok = new LKStokObatAlkesT;
                $stok->obatalkes_id = $obatAlkes->obatalkes_id;
                $stok->sumberdana_id = $obatAlkes->sumberdana_id;
                $stok->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $stok->tglstok_in = date('Y-m-d H:i:s');
                $stok->tglstok_out = date('Y-m-d H:i:s');
                $stok->qtystok_in = $obatAlkes->qty_oa;
                $stok->qtystok_out = 0;
                $stok->harganetto_oa = $obatAlkes->harganetto_oa;
                $stok->hargajual_oa = $obatAlkes->hargasatuan_oa;
                $stok->discount = $obatAlkes->discount;
                $stok->satuankecil_id = $obatAlkes->satuankecil_id;
                $stok->save();
            }
        }
}