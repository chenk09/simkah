<?php

class PendaftaranPasienLuar_oldController extends SBaseController
{
        public $successSave = false;
        public $successSaveAdmisi = false; //variabel untuk validasi admisi
        public $successSaveRujukan = false; //variabel untuk validasi data opsional (rujukan)
        public $successSavePJ = false; //variabel untuk validasi data opsional (penanggung jawab)
        public $successSaveSample = false;
        public $successSaveHasilPemeriksaan = false;
        public $successSaveTindakanKomponen = false;
        public $successSaveTindakanPelayanan = true;
        public $successSaveTindakanRad = true;
        
        protected function performAjaxValidation($model)
        {
            if(isset($_POST['ajax']) && $_POST['ajax']==='pppendaftaran-mp-form')
            {
              echo CActiveForm::validate($modTindakanPelayananT,$modTindakanKomponen,$modHasilPemeriksaan,$model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienPenunjang,$modPengambilanSample);
              Yii::app()->end();
            }   
        }
        
          /**
         * action index digunakan di pendaftaran lab pasien luar
         * digunakan di :
         * 1. Laboratorium -> pendaftaran 
         * @param int $id pendaftaran_id mengambil nilai get dari index pendaftaran
         */
	public function actionIndex($id=null)
	{
            $this->pageTitle = Yii::app()->name." - Pendaftaran Pasien Laboratorium";
            $model=new LKPendaftaranMp;
            $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $model->kelaspelayanan_id = Params::KELASPELAYANAN_MP;
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->umur = "00 Thn 00 Bln 00 Hr";
            $model->pakeSample = true;
            //Inisialisasi Radiologi
            $modRadiologi=new ROPendaftaranMp;
            $modRadiologi->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
            $modRadiologi->ruangan_id = Params::RUANGAN_ID_RAD;
            $modRadiologi->tgl_pendaftaran = date('d M Y H:i:s');
            $modRadiologi->umur = "00 Thn 00 Bln 00 Hr";
            $modPeriksaRad = PemeriksaanradM::model()->findAllByAttributes(array('pemeriksaanrad_aktif'=>true));
            $modPasienPenunjangRad = new ROPasienMasukPenunjangT;
            
            $modPasien = new LKPasienM;
            $modPenanggungJawab = new LKPenanggungJawabM;
            $modRujukan = new LKRujukanT;
            $modPasienPenunjang = new LKPasienMasukPenunjangT;
            $modPengambilanSample = new LKPengambilanSampleT;
            $modPengambilanSample->no_pengambilansample = Generator::noPengambilanSample();
            $modPeriksaLab = LKPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true),array('order'=>'pemeriksaanlab_urutan'));
            $modJenisPeriksaLab = LKJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan'));
            $modHasilPemeriksaan= new LKHasilPemeriksaanLabT;
            $modTindakanPelayananT = new LKTindakanPelayananT;
            $modTindakanKomponen = new LKTindakanKomponenT;
            $modDetailHasilPemeriksaanLabT = new LKDetailHasilPemeriksaanLabT;
            $modSampleLab = new SamplelabM;
            $modRincian = new LKRinciantagihanpasienpenunjangV;
            
            $modPemeriksaanlabs = null;
            $model->pakeSample = false;
            
            $model->isRujukan = true;
            $model->isUpdatePasien = (isset($_POST['isUpdatePasien'])) ? true : false;
            $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            $model->pakeSample = (isset($_POST['pakeSample']) || $model->pakeSample) ? true : false;
            
            if (isset($id)){
              $model        = PendaftaranT::model()->findByPk($id);
              $modPasien    = PasienM::model()->findByPk($model->pasien_id);
              $modRujukan   = RujukanT::model()->findByPk($model->rujukan_id);
              $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
              $modPengambilanSample = PengambilansampleT::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$modPenunjang->pasienmasukpenunjang_id));
              $modSampleLab = SamplelabM::model()->findByPk($modPengambilanSample->samplelab_id);
              $modPengambilanSample->samplelab_nama = $modSampleLab->samplelab_nama;
              $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
             
              if (isset($model->penanggungjawab_id))
                $modPenanggungJawab = PenanggungjawabM::model()->findByPk($model->penanggungjawab_id);
            }
//              echo "<pre>";
//                echo "Lab?".$_POST['isPemeriksaanLab'];
//                echo "Rad?".$_POST['isPemeriksaanRad'];
//                print_r($_POST['LKPasienM']);
//    //            print_r($model->attributes);
//    //            print_r($modRadiologi->attributes);
                
//========================== Form General ==============================
            if(isset($_POST['isPemeriksaanLab']) || isset($_POST['isPemeriksaanRad'])){
//            if(isset($_POST['LKPasienM'])){
                
                $model->attributes = $_POST['LKPendaftaranMp'];
                $modRadiologi->attributes = $_POST['ROPendaftaranMp'];
                $modPasien->attributes = $_POST['LKPasienM'];
                $modPenanggungJawab->attributes = $_POST['LKPenanggungJawabM'];
                $modRujukan->attributes = $_POST['LKRujukanT'];
                //========== Copy Pendaftaran Lab ke Rad ========
                $modRadiologi->no_asuransi = $_POST['LKPendaftaranMp']['no_asuransi'];
                $modRadiologi->namapemilik_asuransi = $_POST['LKPendaftaranMp']['namapemilik_asuransi'];
                $modRadiologi->nopokokperusahaan = $_POST['LKPendaftaranMp']['nopokokperusahaan'];
                $modRadiologi->namaperusahaan = $_POST['LKPendaftaranMp']['namaperusahaan'];
                $modRadiologi->kelastanggungan_id = $_POST['LKPendaftaranMp']['kelastanggungan_id'];
                $modRadiologi->kunjungan = $model->kunjungan;
                $modRadiologi->statusperiksa = $model->statusperiksa;
                //========== End Copy Pendaftaran Lab ke Rad ==========
                
                //===== End Mengembalikan nilai dari form Rad =====
                $modRadiologi->ruangan_id = Params::RUANGAN_ID_RAD;
                $modRadiologi->jeniskasuspenyakit_id = $_POST['ROPendaftaranMp']['jeniskasuspenyakit_id'];
                $modRadiologi->kelaspelayanan_id = $_POST['ROPendaftaranMp']['kelaspelayanan_id'];
                $modRadiologi->pegawai_id = $_POST['ROPendaftaranMp']['pegawai_id'];
                $modRadiologi->carabayar_id = $_POST['ROPendaftaranMp']['carabayar_id'];
                $modRadiologi->penjamin_id = $_POST['ROPendaftaranMp']['penjamin_id'];
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    
                    if($_POST['LKPasienM']['namadepan'] == 'Tn.')
                    {
                        if($_POST['LKPasienM']['jeniskelamin'] != 'LAKI-LAKI')
                        {
                             $modPasien->addError('jeniskelamin', 'pilih jenis kelamin yang sesuai');
                             Yii::app()->user->setFlash('danger',"Inputan jenis kelamin kurang tepat, tolong di betulkan");
                        }
                    }
                    else
                    {
                        if($_POST['LKPasienM']['namadepan'] == 'Ny.' || $_POST['LKPasienM']['namadepan'] == 'Nn')
                        {
                            if($_POST['LKPasienM']['jeniskelamin'] != 'PEREMPUAN')
                            {
                                 $modPasien->addError('jeniskelamin', 'pilih jenis kelamin yang sesuai');
                                 Yii::app()->user->setFlash('danger',"Inputan jenis kelamin kurang tepat, tolong di betulkan");
                            }
                        }
                    }
                    
                    if($_POST['LKPasienM']['statusperkawinan'] != null){
                        if($_POST['LKPasienM']['statusperkawinan'] == 'BELUM KAWIN')
                        {
                            if($_POST['LKPasienM']['namadepan'] == 'BY. Ny.')
                            {
                                $modPasien->addError('statusperkawinan', 'pilih status perkawinan yang sesuai');
                                Yii::app()->user->setFlash('danger',"Inputan status perkawinan kurang tepat, tolong di betulkan");
                            }
                        }                        
                    }
                    
                    if($_POST['LKPasienM']['pekerjaan_id'] != null){
                        if($_POST['LKPasienM']['pekerjaan_id'] == '12')
                        {
                            if($_POST['LKPasienM']['namadepan'] != 'BY. Ny.')
                            {
                                $modPasien->addError('pendidikan_id', 'pilih pekerjaan yang sesuai');
                                Yii::app()->user->setFlash('danger',"Inputan pekerjaan kurang tepat, tolong di betulkan");
                            }
                        }                        
                    }
                    //==Penyimpanan dan Update Pasien===========================
//                     $modPemeriksaanlabs = $_POST['PemeriksaanLab'];
                    
                    if($model->isPasienLama){
                        if (!empty($_POST['noRekamMedik'])){
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = LKPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            if(!empty($modPasien) && isset($_POST['isUpdatePasien'])) {
                                 if($_POST['caraAmbilPhoto']=='file'){//Jika User Mengambil photo pegawai dengan cara upload file
                                    $photo = CUploadedFile::getInstance($modPasien, 'photopasien');
                                 }else{
                                   $photo=$_POST['LKPendaftaranMp']['tempPhoto'];
                                 }
                                 
                                $modPasien = $this->updatePasien($modPasien,$_POST['LKPasienM'],$photo,$_POST['caraAmbilPhoto']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                             $this->successSave = false;
                             $modPasien = New LKPasienM;
                        }
                         
                    }else{
                        if($_POST['caraAmbilPhoto']=='file'){//Jika User Mengambil photo pegawai dengan cara upload file
                            $photo = CUploadedFile::getInstance($modPasien, 'photopasien');
                         }else{
                           $photo=$_POST['LKPendaftaranMp']['tempPhoto'];
                         }
                         
                        $modPasien = $this->savePasien($_POST['LKPasienM'],$photo,$_POST['caraAmbilPhoto']);
                    }
                    //==Akhir Penyimpanan dan Update Pasien=====================
//                    if (count($modPasien) != 1){
//                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar.');
//                        Yii::app()->user->setFlash('warning',"Data Pasien tidak ditemukan isi Nomor Rekam Medik dengan benar");
//                    }
                    
                    //==Simpan Rujukan ==
                    $modRujukan = $this->saveRujukan($_POST['LKRujukanT']);
                    
                    //===penyimpanan Penanggung Jawab===========================                   
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['LKPenanggungJawabM']);
                    }
                    //===Akhir Penyimpanan Penanggung Jawab===================== 
                    if(empty($_POST['LKPendaftaranMp']['tgl_pendaftaran'])){
                        $model->tgl_pendaftaran = date('Y-m-d H:i:s');
                        $modRadiologi->tgl_pendaftaran = date('Y-m-d H:i:s');
                    }
                    
                    //==Simpan Transaksi Pendaftaran ==
                    if(isset($_POST['isPemeriksaanLab'])){
                        $modelNew = $this->savePendaftaranMP($model,$modPasien,$modRujukan,$modPenanggungJawab);
                    }else{
                        $modelNew = $this->savePendaftaranMP($modRadiologi,$modPasien,$modRujukan,$modPenanggungJawab);
                    }
                    $model->attributes = $modelNew->attributes;
                    $modRadiologi->attributes = $modelNew->attributes;
                    //===== End Mengembalikan nilai dari form Rad =====
                    $modRadiologi->ruangan_id = Params::RUANGAN_ID_RAD;
                    $modRadiologi->jeniskasuspenyakit_id = $_POST['ROPendaftaranMp']['jeniskasuspenyakit_id'];
                    $modRadiologi->kelaspelayanan_id = $_POST['ROPendaftaranMp']['kelaspelayanan_id'];
                    $modRadiologi->pegawai_id = $_POST['ROPendaftaranMp']['pegawai_id'];
                    $modRadiologi->carabayar_id = $_POST['ROPendaftaranMp']['carabayar_id'];
                    $modRadiologi->penjamin_id = $_POST['ROPendaftaranMp']['penjamin_id'];
                    
//========================== End Form General ==========================
           
//========================== Form Laboratorium ==========================
//            if (isset($_POST['LKPendaftaranMp'])){
                if(isset($_POST['isPemeriksaanLab'])){
                    $modPasienPenunjang = $this->savePasienPenunjang($model,$modPasien, $modPasienPenunjang);//Simpan Psien Masuk Penunjang
                    //==Awal Simpan Jika Pake Sample
                    if($_POST['pakeSample']){
                        $model->pakeSample = true;
                        $data_sample = $this->tabularSample($_POST['LKPengambilanSampleT']);
                        $modPengambilanSample = $this->savePengambilanSample($_POST['LKPengambilanSampleT'],$modPasienPenunjang);
                    }
                     
                    //==Akhir Simpan Jika Pake Sample===========================
                    $modUbahCaraBayar = new LKUbahCaraBayarR;
                    $this->saveUbahCaraBayar($model, $modUbahCaraBayar);//Simpan Ubah Cara Bayar                     

                    $this->saveHasilPemeriksaan($modPasienPenunjang,$model,$modPasien,$_POST['PemeriksaanLab']);//Simpan Hasil Pemeriksaan
                    
                    $this->saveKarcis($modPasien,$model);

                    if($this->successSave && $this->successSaveTindakanPelayanan && $this->successSaveHasilPemeriksaan
                            && $this->successSaveRujukan){   
                        $transaction->commit();  
                         Yii::app()->user->setFlash('success',"Data Pendaftaran Ke Laboratorium berhasil disimpan");
                         $model = new LKPendaftaranMp();
//                         $model->isNewRecord = true;
//                         if(empty($_POST['isPemeriksaanRad'])){ // jika ke radiologi juga dipilih
//                            $this->redirect(array('index'));
//                         }
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data Pendaftaran Ke Laboratorium Gagal disimpan");
                    }
                    
                
                }
            
//========================== End Laboratorium ==========================            
            
// ============================ Radiologi ===========================            
//            if (isset($_POST['ROPendaftaranMp']) && isset($_POST['permintaanPenunjang'])){
                if (isset($_POST['isPemeriksaanRad']) && isset($_POST['permintaanPenunjang'])){
                    
                    
                    $modPasienPenunjangRad = $this->savePasienPenunjangRad($modRadiologi,$modPasien);
                    
                    $this->saveTindakanPelayananRad($modPasien, $modRadiologi, $modPasienPenunjangRad);
                    
                    $modUbahCaraBayarRad = new ROUbahCaraBayarR;
                    $this->saveUbahCaraBayar($modRadiologi, $modUbahCaraBayarRad);//Simpan Ubah Cara Bayar                     
                    
                    $arrInputPemeriksaan = array();
                    if ($this->successSave && $this->successSaveRujukan && $this->successSaveTindakanRad){
                        $modRadiologi->isNewRecord = true;
                        $modRadiologi = new ROPendaftaranMp();
                        Yii::app()->user->setFlash('success',"Data Pendaftaran Ke Radiologi berhasil disimpan");
//                        $this->redirect(array('index'));
                    } else {
                        Yii::app()->user->setFlash('error',"Data Pendaftaran Ke Radiologi gagal disimpan ");
                        if(isset($_POST['permintaanPenunjang'])) {
                            foreach($_POST['permintaanPenunjang'] as $i=>$item) {
                                //echo $i." => ".$item['inputpemeriksaanrad']."<br/>";
                                $arrInputPemeriksaan[$i] = $item;
                            }
                        }
                    }
                    
                }
                
            }catch (Exception $exc){
                    $model->isNewRecord = true;
                    $transaction->rollback();
//                    Yii::app()->user->setFlash('error',"Data Gagal disimpan");
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            
//            
// ======================= End Radiologi ===========================
            
            
            
            $this->render('index',array(
                'model'=>$model,
                'modPasien'=>$modPasien, 
                'modPenanggungJawab'=>$modPenanggungJawab,
                'modRujukan'=>$modRujukan,
                'modPengambilanSample'=>$modPengambilanSample,
                'modPasienPenunjang'=>$modPasienPenunjang,
                'modPasienPenunjangRad'=>$modPasienPenunjangRad,
                'modPeriksaLab'=>$modPeriksaLab,
                'modJenisPeriksaLab'=>$modJenisPeriksaLab,
                'modHasilPemeriksaan'=>$modHasilPemeriksaan,
                'modTindakanKomponen'=>$modTindakanKomponen,
                'modTindakanPelayananT'=>$modTindakanPelayananT,
                'modDetailHasilPemeriksaanLabT'=>$modDetailHasilPemeriksaanLabT,
                'modSampleLab' => $modSampleLab,
                'modRincian' => $modRincian,
                 
                //Radiologi
                'modRadiologi'=>$modRadiologi,
                'modPeriksaRad'=>$modPeriksaRad,
                'arrInputPemeriksaan'=>$arrInputPemeriksaan,   
            ));
	}
        
        public function tabularSample($modSample){
            $modPengambilanSample = null;
            foreach ($modSample as $key => $value) {
                $modPengambilanSample[$key] = new PengambilansampleT();
                $modPengambilanSample[$key]->attributes = $value; 
            }
            return $modPengambilanSample;
        }


        public function savePasien($attrPasien,$photo,$caraAmbilPhoto)
        {

            $modPasien = new LKPasienM;
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modPasien->no_rekam_medik = Generator::noRekamMedikPenunjang(Params::singkatanNoPendaftaranLab());
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = 'AKTIF';
            $modPasien->create_ruangan =Yii::app()->user->getState('ruangan_id'); //Params::RUANGAN_ID_LAB
            $modPasien->ispasienluar = 1;

              if($caraAmbilPhoto=='file'){//Jika User Mengambil photo pegawai dengan cara upload file

                  $gambar=$modPasien->photopasien;
                  if(!empty($model->photopasien)){//Klo User Memasukan Logo
                         $modPasien->photopasien =$random.$modPasien->photopasien;

                         Yii::import("ext.EPhpThumb.EPhpThumb");

                         $thumb=new EPhpThumb();
                         $thumb->init(); //this is needed

                         $fullImgName =$modPasien->photopasien;   
                         $fullImgSource = Params::pathPasienDirectory().$fullImgName;
                         $fullThumbSource = Params::pathPasienTumbsDirectory().'kecil_'.$fullImgName;

                         if($modPasien->save()){
                            $gambar->saveAs($fullImgSource);
                            $thumb->create($fullImgSource)
                                  ->resize(200,200)
                                  ->save($fullThumbSource);
                         }else{
                             echo "gagal Ipload";exit;
                             }
                    }else{
                        $modPasien->save();
                    }
                    
              }else{
                 $modPasien->photopasien=$photo;
                 if($modPasien->validate())
                    {
                        $modPasien->save();
                    }
                 else 
                    {

                         unlink(Params::pathPasienDirectory().$photo);
                         unlink(Params::pathPasienTumbsDirectory().$photo);
                    }
               }
                

            return $modPasien;
        }
        
        public function updatePasien($modPasien,$attrPasien,$photo,$caraAmbilPhoto)
        {
            $modPasienupdate = $modPasien;
            $modPasienupdate->attributes = $attrPasien;
            $modPasien->ispasienluar = 1;
            $modPasienupdate->kelompokumur_id = Generator::kelompokUmur($modPasienupdate->tanggal_lahir);
            $temppPhoto=$modPasienupdate->photopasien;
            if($caraAmbilPhoto=='file')//Jika User Mengambil photo pegawai dengan cara upload file
              { 
                  $gambar=$modPasien->photopasien;

                  if(!empty($model->photopasien))//Klo User Memasukan Logo
                  { 
                         $modPasien->photopasien =$random.$modPasien->photopasien;

                         Yii::import("ext.EPhpThumb.EPhpThumb");

                         $thumb=new EPhpThumb();
                         $thumb->init(); //this is needed

                         $fullImgName =$modPasien->photopasien;   
                         $fullImgSource = Params::pathPasienDirectory().$fullImgName;
                         $fullThumbSource = Params::pathPasienTumbsDirectory().'kecil_'.$fullImgName;

                         if($modPasien->save()){
                                   if(!empty($temppPhoto)){
                                        unlink(Params::pathPasienDirectory().$photo);
                                        unlink(Params::pathPasienTumbsDirectory().$photo);
                                   } 
                                   $gambar->saveAs($fullImgSource);
                                   $thumb->create($fullImgSource)
                                         ->resize(200,200)
                                         ->save($fullThumbSource);
                              }
                    }
              }   
             else 
              {
                 $modPasien->photopasien=$photo;
                 if($modPasien->validate())
                    {
                        $modPasien->save();
                    }
                 else 
                    {
                         unlink(Params::pathPegawaiDirectory().$photo);
                         unlink(Params::pathPegawaiTumbsDirectory().$photo);
                    }
               }
            
            return $modPasienupdate;
        }
        
        public function saveRujukan($attrRujukan)
        {
            $modRujukan = new LKRujukanT;
            $modRujukan->attributes = $attrRujukan;
            $modRujukan->tanggal_rujukan = ($attrRujukan['tanggal_rujukan'] == '') ? date('Y-m-d') : $attrRujukan['tanggal_rujukan'] ;
            if($modRujukan->validate()) {
                // form inputs are valid, do something here
                $modRujukan->save();
                $this->successSaveRujukan = TRUE;
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modRujukan->tanggal_rujukan = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modRujukan->tanggal_rujukan, 'yyyy-MM-dd'),'medium',null);
                $this->successSaveRujukan = FALSE;
            }
            return $modRujukan;
        }
        
        public function savePenanggungJawab($attrPenanggungJawab)
        {
            $modPenanggungJawab = new LKPenanggungJawabM;
            $modPenanggungJawab->attributes = $attrPenanggungJawab;
            if($modPenanggungJawab->validate()) {
                // form inputs are valid, do something here
                $modPenanggungJawab->save();
                $this->successSavePJ = TRUE;
            } else {
               $this->successSavePJ = FALSE;
            }

            return $modPenanggungJawab;
        }
        
        public function savePendaftaranMP($model, $modPasien, $modRujukan, $modPenanggungJawab){
            $modelNew = new LKPendaftaranMp;
            $format = new CustomFormat();
            $modelNew->attributes = $model->attributes;
            $modelNew->tgl_pendaftaran = $format->formatDateTimeMediumForDB($modelNew->tgl_pendaftaran);
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            $modelNew->ruangan_id=$model->ruangan_id;
//            $modelNew->ruangan_id=Params::RUANGAN_ID_LAB;
            $modelNew->instalasi_id =Yii::app()->user->getState('instalasi_id');
            $modelNew->no_pendaftaran = Generator::noPendaftaran(Params::singkatanNoPendaftaranLab());
            $modelNew->no_urutantri = Generator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = Generator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
            $modelNew->statuspasien = Generator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = Generator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->statusmasuk = Params::statusMasuk('rujukan');
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            if ($modelNew->validate()){
                $modelNew->save();
                $this->successSave = true;
            }
            else{
                $modelNew->tgl_pendaftaran = date('Y-m-d H:i:s');
//                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
//                        CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'), 'medium', null);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
           
            return $modelNew;
        }
        
        
        
        public function savePengambilanSample($attrSample,$modPasienPenunjang){
            $modPengambilanSample = new LKPengambilanSampleT;
            $modPengambilanSample->tglpengambilansample = date('Y-m-d H:i:s');
            $modPengambilanSample->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
            $modPengambilanSample->attributes = $attrSample;
            if ($modPengambilanSample->validate()){
                $modPengambilanSample->Save();
                $this->successSaveSample = true;
            }else{
                $this->successSaveSample = false;
                $modPengambilanSample->tglpengambilansample = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPengambilanSample->tglpengambilansample, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPengambilanSample;
        }
        
        
        
        public function savePasienPenunjang($attrPendaftaran,$attrPasien){
            
            $modPasienPenunjang = new LKPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $attrPasien->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
//            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_LAB;
//            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang('LK');
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang(Yii::app()->user->getState('nopendaftaran_lab'));
            $modPasienPenunjang->tglmasukpenunjang = $attrPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
            
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }
        
        protected function savePasienPenunjangRad($modPendaftaran,$modPasien){ // Untuk Radiologi
            
            $modPasienPenunjang = new ROPasienMasukPenunjangT;
            
            $modPasienPenunjang->pasien_id = $modPasien->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $modPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//            $modPasienPenunjang->ruangan_id = $modPendaftaran->ruangan_id;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_RAD;
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang('RO');
            $modPasienPenunjang->tglmasukpenunjang = $modPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $modPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $modPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $modPendaftaran->ruangan_id;
            $modPasienPenunjang->create_time = $modPendaftaran->create_time;
            $modPasienPenunjang->update_time = $modPendaftaran->update_time;
            $modPasienPenunjang->create_loginpemakai_id = $modPendaftaran->create_loginpemakai_id;
            $modPasienPenunjang->update_loginpemakai_id = $modPendaftaran->update_loginpemakai_id;
            $modPasienPenunjang->create_ruangan = Params::RUANGAN_ID_RAD;
            
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            return $modPasienPenunjang;
        }
        
        public function saveUbahCaraBayar($model, $modUbahCaraBayar) 
        {
            
            $modUbahCaraBayar->pendaftaran_id = $model->pendaftaran_id;
            $modUbahCaraBayar->carabayar_id = $model->carabayar_id;
            $modUbahCaraBayar->penjamin_id = $model->penjamin_id;
            $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d');
            $modUbahCaraBayar->alasanperubahan = 'x';
            if($modUbahCaraBayar->validate())
            {
                // form inputs are valid, do something here
                $modUbahCaraBayar->save();
            }
  
        }
        
        protected function saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$pemeriksaanLab)
        {
            foreach($pemeriksaanLab as $jenisKelompok => $value) {
                if($jenisKelompok == Params::LAB_PATOLOGI){
                    //echo 'Patologi Klinik <pre>'.print_r($value,1).'</pre>';
                    $modHasilPemeriksaan = new LKHasilPemeriksaanLabT;
                    $modHasilPemeriksaan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modHasilPemeriksaan->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                    $modHasilPemeriksaan->pasien_id = $modPasien->pasien_id;
                    $modHasilPemeriksaan->nohasilperiksalab = Generator::noHasilPemeriksaanLK();
                    $modHasilPemeriksaan->tglhasilpemeriksaanlab = date('Y-m-d H:i:s');
                    $modHasilPemeriksaan->hasil_kelompokumur = Generator::kelompokUmurNama($modPasien->tanggal_lahir);
                    $modHasilPemeriksaan->hasil_jeniskelamin = $modPasien->jeniskelamin;
                    $modHasilPemeriksaan->statusperiksahasil = Params::statusPeriksaLK(1);
                    if($modHasilPemeriksaan->save()){
                        $this->successSaveHasilPemeriksaan = true;
                        
                        foreach ($value as $pemeriksaanId => $dataPeriksa) {
                            $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                            $details = PemeriksaanlabdetM::model()->pemeriksaanDetail($modHasilPemeriksaan, $pemeriksaanId);
                            foreach ($details as $k => $detail) {
                                $modDetailHasilPemeriksaan = new LKDetailHasilPemeriksaanLabT;
                                $modDetailHasilPemeriksaan->hasilpemeriksaanlab_id = $modHasilPemeriksaan->hasilpemeriksaanlab_id;
                                $modDetailHasilPemeriksaan->pemeriksaanlabdet_id = $detail->pemeriksaanlabdet_id;
                                $modDetailHasilPemeriksaan->pemeriksaanlab_id = $pemeriksaanId;
                                $modDetailHasilPemeriksaan->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                                $modDetailHasilPemeriksaan->save();
                            }
                        }
                    } else 
                        $this->successSaveHasilPemeriksaan = false;

                } else {
                    //echo 'Anatomi <pre>'.print_r($value,1).'</pre>';
                    foreach ($value as $pemeriksaanId => $dataPeriksa) {
                        $modTindakanPelayananT = $this->saveTindakanPelayanan($modPendaftaran, $modPasien, $dataPeriksa, $modPasienMasukPenunjang);
                        $modHasilPemeriksaanPA = new LKHasilPemeriksaanPAT;
                        $modHasilPemeriksaanPA->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                        $modHasilPemeriksaanPA->pemeriksaanlab_id = $pemeriksaanId;
                        $modHasilPemeriksaanPA->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modHasilPemeriksaanPA->pasien_id = $modTindakanPelayananT->pasien_id;
                        $modHasilPemeriksaanPA->pendaftaran_id = $modTindakanPelayananT->pendaftaran_id;
                        $modHasilPemeriksaanPA->nosediaanpa = Generator::noSediaanPA();
                        $modHasilPemeriksaanPA->tglperiksapa = $modTindakanPelayananT->tgl_tindakan;
                        if($modHasilPemeriksaanPA->validate()){
                            if($modHasilPemeriksaanPA->save()){
                                 $this->successSaveHasilPemeriksaan=true;
                            }else{
                                 $this->successSaveHasilPemeriksaan=false;                   
                            }  
                        }
                        
                    }
                }
            }
            
        }
        
        protected function saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad) // Untuk Radiologi
        {
            
            $valid = true;
            for($j=0;$j<$tindakan->qty_tindakan;$j++){
                $modHasil = new ROHasilPemeriksaanRadT;
                $modHasil->pendaftaran_id = $tindakan->pendaftaran_id;
                $modHasil->pasienadmisi_id = $tindakan->pasienadmisi_id;
                $modHasil->pasien_id = $tindakan->pasien_id;
                $modHasil->pasienmasukpenunjang_id = $tindakan->pasienmasukpenunjang_id;
                $modHasil->tglpemeriksaanrad = $tindakan->tgl_tindakan;
                $modHasil->pemeriksaanrad_id = $idPemeriksaanRad;
                $modHasil->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                
                $valid = $modHasil->validate() && $valid;
                
                if($valid){
                    $modHasil->save();
                    TindakanpelayananT::model()->updateByPk($tindakan->tindakanpelayanan_id, array('hasilpemeriksaanrad_id'=>$modHasil->hasilpemeriksaanrad_id));
                }
            }
            
            return $valid;
        }
        
        protected function saveTindakanPelayanan($modPendaftaran,$modPasien,$dataPemeriksaan, $modPasienMasukPenunjang)
        {
                $modTindakanPelayananT = new LKTindakanPelayananT;
                $modTindakanPelayananT->penjamin_id = $modPendaftaran->penjamin_id;
                $modTindakanPelayananT->pasien_id = $modPasien->pasien_id;
                $modTindakanPelayananT->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                $modTindakanPelayananT->instalasi_id = Params::INSTALASI_ID_LAB;
                $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                $modTindakanPelayananT->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayananT->daftartindakan_id = $dataPemeriksaan['daftartindakan_id'];
                $modTindakanPelayananT->carabayar_id = $modPendaftaran->carabayar_id;
                $modTindakanPelayananT->qty_tindakan = $dataPemeriksaan['qty_tindakan'];
                $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayananT->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayananT->tarif_satuan = $dataPemeriksaan['tarif_tindakan'];
                $modTindakanPelayananT->tarif_tindakan = $modTindakanPelayananT->tarif_satuan * $modTindakanPelayananT->qty_tindakan;
                $modTindakanPelayananT->satuantindakan = $dataPemeriksaan['satuantindakan'];
                $cyto_tindakan = $modTindakanPelayananT->cyto_tindakan = $dataPemeriksaan['cyto_tindakan'];
                $modTindakanPelayananT->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
                $modTindakanPelayananT->discount_tindakan = 0;
                $modTindakanPelayananT->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                
                $modTindakanPelayananT->subsidiasuransi_tindakan = 0;
                $modTindakanPelayananT->subsidipemerintah_tindakan = 0;
                $modTindakanPelayananT->subsisidirumahsakit_tindakan = 0;
                $modTindakanPelayananT->iurbiaya_tindakan = 0;
                $modTindakanPelayananT->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayananT->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;
                
                if($cyto_tindakan){
                    $modTindakanPelayananT->tarifcyto_tindakan = $dataPemeriksaan['tarif_cyto'];;
                }else{
                    $modTindakanPelayananT->tarifcyto_tindakan = 0;
                }
                
//                $tarifTindakan= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayananT->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayananT->kelaspelayanan_id.'');
                $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$modTindakanPelayananT->daftartindakan_id, 'kelaspelayanan_id'=>$modTindakanPelayananT->kelaspelayanan_id));
                
                
                foreach($tarifTindakan as $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayananT->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayananT->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayananT->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayananT->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;
                if($modTindakanPelayananT->save()){//Jika Tindakan Pelayanan Berhasil tersimpan
                    $idPeriksaLab = $dataPemeriksaan['pemeriksaanlab_id'];
                    $jumlahTindKomponen = COUNT($_POST['LKTindakanKomponenT'][$idPeriksaLab]);
                    for($j=0; $j<$jumlahTindKomponen; $j++):
                        $modTindakanKomponen = new LKTindakanKomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['komponentarif_id'];
                        $modTindakanKomponen->tarif_tindakankomp = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'] * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $modTindakanPelayananT->tarif_tindakan;
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                    endfor;
                    
                    $this->successSaveTindakanPelayanan = $this->successSaveTindakanPelayanan && true;
                } else {
                    $this->successSaveTindakanPelayanan = false;
                }
                
                return $modTindakanPelayananT;
        }
        
        //===== Simpan Tindakan Pelayanan untuk Radiologi ======
        protected function saveTindakanPelayananRad($modPasien,$modPendaftaran,$modPasienPenunjangRad)
        {
            
            $validTRad = true;
            foreach($_POST['permintaanPenunjang'] as $i=>$item)
            {
                if(!empty($item)){
                    if(isset($item['cbPemeriksaan'])){
                        $modTindakans[$i] = new TindakanpelayananT;
                        $modTindakans[$i]->attributes=$item;
                        $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                        $modTindakans[$i]->pasien_id = $modPasien->pasien_id;
                        $modTindakans[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                        $modTindakans[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                        $modTindakans[$i]->instalasi_id = $modPendaftaran->instalasi_id;
                        $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                        $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                        $modTindakans[$i]->pasienmasukpenunjang_id = $modPasienPenunjangRad->pasienmasukpenunjang_id;
                        $modTindakans[$i]->daftartindakan_id = $item['idDaftarTindakan'];
                        $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                        $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                        $modTindakans[$i]->tgl_tindakan = date('Y-m-d H:i:s');
                        $modTindakans[$i]->tarif_satuan = $item['inputtarifpemeriksaanrad'];
                        $modTindakans[$i]->tarif_tindakan = $item['inputtarifpemeriksaanrad'] * $item['inputqty'];
                        $modTindakans[$i]->satuantindakan = $item['satuan'];
                        $modTindakans[$i]->qty_tindakan = $item['inputqty'];
                        $modTindakans[$i]->cyto_tindakan = $item['cyto'];
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['cyto']) ? (($item['persencyto'] / 100) * $modTindakans[$i]->tarif_tindakan) : 0;
                        $modTindakans[$i]->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
                        $modTindakans[$i]->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
//                        $modTindakans[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $modTindakans[$i]->ruangan_id = Params::RUANGAN_ID_RAD;
                        $modTindakans[$i]->discount_tindakan = 0;
                        $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                        $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                        $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                        $modTindakans[$i]->iurbiaya_tindakan = 0;
                        $validTRad = $modTindakans[$i]->validate() && $validTRad;
                        $idPemeriksaanRad[$i] = $item['inputpemeriksaanrad'];
                        
                    
                    }
                    
                }
            }
            
            if($validTRad){
                
                foreach($modTindakans as $i=>$tindakan){
                    
                    $statusTindakan = $tindakan->save();
                    
                    $statusSaveKomponen = $this->saveTindakanKomponenRad($tindakan);
                    
                    $statusSaveHasilPeriksa = $this->saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad[$i]);
                    
                    if($statusTindakan && $statusSaveKomponen && $statusSaveHasilPeriksa) {
                        $this->successSaveTindakanRad = $this->successSaveTindakanRad && true;
                    } else {
                        $this->successSaveTindakanRad = false;
                    }
                }
                
            } else {
                
                $this->successSaveTindakanRad = false;
            }
           
            return $modTindakans;
        }
        
        //===== End Simpan Tindakan Pelayanan untuk Radiologi ======
        public function saveKarcis($modPasien,$model)
        {
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New LKTindakanPelayananT;
            $modTindakanPelayan->penjamin_id=$model->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
            $modTindakanPelayan->kelaspelayanan_id=$model->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id=Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id=$model->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id=$_POST['TindakanPelayananT']['idTindakan'];
            $modTindakanPelayan->carabayar_id=$model->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$model->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan=$_POST['TindakanPelayananT']['tarifSatuan'];
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$model->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
//            $modTindakanPelayan->ruangan_id = $model->ruangan_id;
            $modTindakanPelayan->ruangan_id = Params::RUANGAN_ID_LAB;
             
            if(!empty($_POST['TindakanPelayananT']['idKarcis'])){
                $modTindakanPelayan->karcis_id=$_POST['TindakanPelayananT']['idKarcis'];
                $modTindakanPelayan->tipepaket_id = $this->tipePaketKarcis($model, $_POST['TindakanPelayananT']['idKarcis'], $_POST['TindakanPelayananT']['idTindakan']);
            }

//                $tarifTindakan= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                $tarifTindakan= LKTarifTindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$modTindakanPelayanan->daftartindakan_id, 'kelaspelayanan_id'=>$modTindakanPelayanan->kelaspelayanan_id));
                foreach($tarifTindakan AS $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;
                if($modTindakanPelayan->save()){ 
                       $tindakanKomponen= LKTarifTindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                       $jumlahKomponen=COUNT($tindakanKomponen);

                       foreach ($tindakanKomponen AS $tampilKomponen):
                               $modTindakanKomponen=new LKTindakanKomponenT;
                               $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                               $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                               $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                               $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                               $modTindakanKomponen->tarifcyto_tindakankomp=0;
                               $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                               $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                               $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                               $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                               if($modTindakanKomponen->save()){
                                   $cekTindakanKomponen++;
                               }
                       endforeach;
                       if($cekTindakanKomponen!=$jumlahKomponen){
                              $this->successSaveTindakanKomponen=false;
                           }
               } 
           
        }
        
       
        protected function saveTindakanKomponenRad($tindakan) //Untuk Radiologi
        {   
            
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->compare('daftartindakan_id', $tindakan->daftartindakan_id);
            $criteria->compare('kelaspelayanan_id', $tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = new TindakankomponenT;
                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen->validate() && $valid;
                
                if($valid){
                    $modTindakanKomponen->save();
                }
            }
            
            return $valid;
        }
        
        
        
        
        public function tipePaketKarcis($modPendaftaran,$idKarcis,$idTindakan)
        {
            $criteria = new CDbCriteria;
            $criteria->with = array('tipepaket');
            $criteria->compare('daftartindakan_id', $idTindakan);
            $criteria->compare('tipepaket.carabayar_id', $modPendaftaran->carabayar_id);
            $criteria->compare('tipepaket.penjamin_id', $modPendaftaran->penjamin_id);
            $criteria->compare('tipepaket.kelaspelayanan_id', $modPendaftaran->kelaspelayanan_id);
            $paket = PaketpelayananM::model()->find($criteria)->tipepaket_id;
            if(empty($paket)) $paket = Params::TIPEPAKET_NONPAKET;
            
            return $paket;
        }
        
       public function actionPrint($id_pendaftaran,$caraPrint){
            $judulLaporan = 'Informasi Pendaftaran Laboratorium';
            $modPendaftaran = PendaftaranT::model()->findByPk($id_pendaftaran);            
            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
//            $modPendaftaran = PendaftaranT::model()->findByAttributes(array('pasien_id'=>$pasien_id));
            $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $modPendaftaran->pendaftaran_id), array('order'=>'ruangan_id'));
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modPasien'=>$modPasien,
                                            'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian));
            }
           
        }
        
}

/**
 * Whitebox Testing
 */
//echo "<font size='300' color=green> Berhasil ! </font><br><pre>";
//echo '<br>=====================<br>';
//echo print_r($modUbahCaraBayar->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modPasienPenunjangRad->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modPasienPenunjang->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modRadiologi->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modPengambilanSample->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($model->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modPenanggungJawab->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modRujukan->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modPasien->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modTindakans[0]->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($_POST['permintaanPenunjang']).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modPasienPenunjang->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modPendaftaran->attributes).'<br>';
//echo '<br>=====================<br>';
//echo print_r($modPasien->attributes).'<br>';
//echo '<br>=====================<br>';
//exit(); 

//
//if($model->isPasienLama){
//echo 'Pasien Lama - '. $_POST['isPasienLama'];
//    if(isset($_POST['noRekamMedik'])){
//        echo "no Rekam: ".$_POST['noRekamMedik'].' - ';
//        echo $_POST['isUpdatePasien'];
//        if($_POST['isUpdatePasien']){
//            echo 'Pasien Di Update<br><pre>';
//            print_r($_POST['LKPasienM']);
//        }
//    }
//}else{
//    echo 'Pasien Baru <pre>'. $_POST['isPasienLama'];
//    print_r($_POST['LKPasienM']);
//}
//exit();
