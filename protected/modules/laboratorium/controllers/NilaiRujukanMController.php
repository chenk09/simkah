
<?php

class NilaiRujukanMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new LKNilaiRujukanM;
                $modPemeriksaanLab = new LKPemeriksaanLabM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                if(isset($_POST['LKNilaiRujukanM'])){
                    
                    $transcaction=Yii::app()->db->beginTransaction();
                    try {
                            $modPemeriksaanLab = new LKPemeriksaanLabM;
                            $modPemeriksaanLab->attributes=$_POST['LKPemeriksaanLabM'];
                            $modPemeriksaanLab->pemeriksaanlab_aktif=TRUE;
                            $modPemeriksaanLab->save();

                            $jumlahJenisKelaminNilaiRujukan=COUNT($_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin']);
                           
                            for($i=0; $i<=$jumlahJenisKelaminNilaiRujukan; $i++):
                               if ($_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i]!='N'){
                                   $jumlahKelompokUmur=COUNT($_POST['LKNilaiRujukanM']['kelompokumur'][''.$_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i].'']);
                                  for($j=0; $j<=$jumlahKelompokUmur; $j++):
                                    if($_POST['LKNilaiRujukanM']['nilairujukan_nama'][$_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i]][$j]!=''){ 
                                        $jenisKelamin=$_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i];
                                        
                                        $model=new LKNilaiRujukanM;
                                        $model->attributes=$_POST['LKNilaiRujukanM'];
                                        $model->nilairujukan_jeniskelamin=$_POST['LKNilaiRujukanM']['nilairujukan_jeniskelamin'][$i];
                                        $model->pemeriksaanlab_id=$modPemeriksaanLab->pemeriksaanlab_id;
                                        $model->kelompokumur=$_POST['LKNilaiRujukanM']['kelompokumur'][$jenisKelamin][$j];
                                        $model->nilairujukan_min=$_POST['LKNilaiRujukanM']['nilairujukan_min'][$jenisKelamin][$j];
                                        $model->nilairujukan_max=$_POST['LKNilaiRujukanM']['nilairujukan_max'][$jenisKelamin][$j];
                                        $model->nilairujukan_nama=$_POST['LKNilaiRujukanM']['nilairujukan_nama'][$jenisKelamin][$j];
                                        $model->nilairujukan_aktif=TRUE;
                                        $model->save(); 
                                    }
                                 endfor;  
                                 $j=0;
                            }
                            endfor;
//                            if($model->save())
//                            {
                                $model->save();
                            $transcaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');    
//                            }

                    }
                    catch (Exception $ext){
                         $transcaction->rollback();
                         Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ext,true));  
                    }
                }    
			
		

		$this->render('create',array(
			'model'=>$model,'modPemeriksaanLab'=>$modPemeriksaanLab
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
                $modPemeriksaanDet = PemeriksaanlabdetM::model()->with('pemeriksaanlab')->findByAttributes(array('nilairujukan_id'=>$model->nilairujukan_id));
                $modPemeriksaanLab=PemeriksaanlabM::model()->findByPk($modPemeriksaanDet->pemeriksaanlab_id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LKNilaiRujukanM']))
		{
			$model->attributes=$_POST['LKNilaiRujukanM'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->nilairujukan_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,'modPemeriksaanLab'=>$modPemeriksaanLab
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('LKNilaiRujukanM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new LKNilaiRujukanM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['LKNilaiRujukanM']))
			$model->attributes=$_GET['LKNilaiRujukanM'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=LKNilaiRujukanM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sanilai-rujukan-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //LKKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new LKNilaiRujukanM;
            $model->attributes=$_REQUEST['LKNilaiRujukanM'];
            $judulLaporan='Data Nilai Rujukan';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
