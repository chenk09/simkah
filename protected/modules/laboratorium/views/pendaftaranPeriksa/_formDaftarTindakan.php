<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
    <legend class="rim">Pencarian Daftar Pemeriksaan Lab</legend>
    <table class="table table-condensed">
        <tr>
            <tr>
            <td>
                  <?php 
                        echo CHtml::dropDownList('Pilihan Pencarian','daftarTindakan',(Params::DaftarTindakan()),array('id'=>'daftarTindakan','empty'=>'-- Pilih --','style'=>'width:140px;','onChange'=>'cekOptions()'));
                  ?>
            </td>
            <td>
                
                <div style="margin-left:-50px;margin-right:90px;" id="nama">
                    
                <?php echo CHtml::hiddenField('idPemeriksaan'); ?>
                    <?php
                                $this->widget('MyJuiAutoComplete',array(
                                    'model'=>$modDaftarTindakan,
                                    'attribute'=>'daftartindakan_nama',
                                    'sourceUrl'=>  Yii::app()->createUrl('laboratorium/ActionAutoComplete/getNamaDaftarTindakan'),
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'minLength'=>2,
                                        'focus'=> 'js:function( event, ui ) {
                                                    $("#idPemeriksaan").val( ui.item.pemeriksaanlab_id );
                                                    return false;
                                                }',
                                        'select'=>'js:function( event, ui ) {
                                                   $("#idPemeriksaan").val(ui.item.pemeriksaanlab_id );
                                                    isiDataTindakan();
                                                    return false;
                                                }',
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'span2',
                                        'style'=>'float:left;',
                                        'onClick'=>'cekOptions("xx")',
                                    ),
                                    
                                    'tombolDialog'=>array('idDialog'=>'dialogAddTindakan'),
                                ));
                    
//                            $this->widget('MyJuiAutoComplete', array(
//                                    'model'=>$modDaftarTindakan,
//                                    'attribute'=>'daftartindakan_nama',
//                                    'source'=>'js: function(request, response) {
//                                                   $.ajax({
//                                                       url: "'.Yii::app()->createUrl('laboratorium/ActionAutoComplete/getNamaDaftarTindakan').'",
//                                                       dataType: "json",
//                                                       data: {
//                                                           term: request.term,
//                                                       },
//                                                       success: function (data) {
//                                                               response(data);
//                                                       }
//                                                   })
//                                                }',
//                                     'options'=>array(
//                                           'showAnim'=>'fold',
//                                           'minLength' => 2,
//                                           'focus'=> 'js:function( event, ui ) {
////                                                    $(this).val(ui.item.value);
////                                                    return false;
//                                            }',
//                                           'select'=>'js:function( event, ui ) {
////                                                isiDataTindakan(ui.item);
//                                                return false;
//                                            }',
//                                    ),
//                                        'htmlOptions'=>array('onkeypress' => "if (jQuery.trim($(this).val()) != ''){if (event.keyCode == 13){isiDataTindakan();}}else{return $(this).focusNextInputField(event)}", 'class'=>'span2','style'=>'float:left;'),
//                                        'tombolDialog'=>array('idDialog'=>'dialogAddTindakan'),
//                                )); 
                  
                            ?>
                </div>
                <div style="margin-left:-50px;margin-right:90px;" id="kode">
                    <?php 
//                        $this->widget('MyJuiAutoComplete', array(
////                                        'model'=>$modPasien,
////                                        'attribute'=>'',
//                                        'name'=>'daftarTindakan',
//                                        'source'=>'js: function(request, response) {
//                                                       $.ajax({
//                                                           url: "'.Yii::app()->createUrl('laboratorium/ActionAutoComplete/getKodeDaftarTindakan').'",
//                                                           dataType: "json",
//                                                           data: {
//                                                               term: request.term,
//                                                           },
//                                                           success: function (data) {
//                                                                   response(data);
//                                                           }
//                                                       })
//                                                    }',
//                                         'options'=>array(
//                                               'showAnim'=>'fold',
//                                               'minLength' => 2,
//                                               'focus'=> 'js:function( event, ui ) {
//                                                    $(this).val(ui.item.value);
//                                                    return false;
//                                                }',
//                                               'select'=>'js:function( event, ui ) {
//                                                    inputperiksaTindakan(ui.item);
//                                                    return false;
//                                                }',
//                                        ),
//                                        'htmlOptions'=>array(
//                                            'style'=>'width:160px;',
//                                            'class'=>'span2',
////                                            'onfocus'=>'cekOptions("xx")',
//                                        ),
//                                        'tombolDialog'=>array('idDialog'=>'dialogAddTindakan', 'jsFunction'=>'cekOptions("tombol")'),
//                                    )); 
                    ?>
                </div>
            </td>
        </tr>
    </table>