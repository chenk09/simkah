
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'masukpenunjang-Laboratorium-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php echo $form->errorSummary($modPeriksaLab); ?>
         <div class="box">
        <?php foreach($modJenisPeriksaLab as $i=>$jenisPeriksa){ 
        ?>
                <div class="boxtindakan">
                    <h6><?php echo $jenisPeriksa->jenispemeriksaanlab_nama; ?></h6>
                    <?php foreach ($modPeriksaLab as $j => $pemeriksaan) {
                          $ceklist = (!empty($arrInputPemeriksaan)) ? cekPilihan($pemeriksaan->pemeriksaanlab_id,$arrInputPemeriksaan):false;
                          if($jenisPeriksa->jenispemeriksaanlab_id == $pemeriksaan->jenispemeriksaanlab_id) {
                                 echo CHtml::checkBox("pemeriksaanLab[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanlab_id,
                                                                                          'onclick' => "inputperiksa(this)"));
                                 echo "<span>".$pemeriksaan->pemeriksaanlab_nama."</span><br/>";
                             }
                         } ?>
                </div>
        <?php } ?>
    </div>

<fieldset>
    <legend class="rim">Pencarian Daftar Pemeriksaan Lab</legend>
    
    
    <table class="table table-condensed">
        <tr>
            <tr>
               <td>Kategori Pencarian : 
                  <?php 
                        echo CHtml::dropDownList('Pilihan Pencarian','daftarTindakanNama',(Params::DaftarTindakan()),array('id'=>'daftarTindakan','empty'=>'-- Pilih --','style'=>'width:140px;','onChange'=>'cekOptions()'));
                  ?>
                </td>
            <td>
                <div style="margin-left:-50px;margin-right:400px;" id="nama">
                    
                <?php echo CHtml::hiddenField('idPemeriksaan'); ?>
                    <?php
//                            
                    $this->widget('MyJuiAutoComplete', array(
                                        'name' => 'daftartindakan_nama',
                                        'sourceUrl' => Yii::app()->createUrl('laboratorium/ActionAutoComplete/getNamaDaftarTindakan'),
                                        'options' => array(
                                            'showAnim' => 'fold',
                                            'minLength' => 2,
                                            'focus' => 'js:function( event, ui ) {
                                                                            $(this).val(ui.item.label);
                                                                            return false;
                                                                        }',
                                            'select' => 'js:function( event, ui ) {
                                                                                  $("#idPemeriksaanLab").val(ui.item.pemeriksaanlab_id);
                                                                                  return false;
                                                                        }',
                                        ),
                                        'htmlOptions' => array(
                                            'class' => 'span2',
                                            'onkeypress' =>"if (event.keyCode == 13){inputperiksaTindakan();}return $(this).focusNextInputField(event)",
                                            'onClick'=>'cekOptions()',
                                        ),
                                        'tombolDialog' => array('idDialog' => 'dialogAddTindakan'),
                                    ));
                  
                            ?>
                </div>
                <div style="margin-left:-50px;margin-right:400px;" id="kode">
                    <?php 
                        $this->widget('MyJuiAutoComplete', array(
                                        'name' => 'pemeriksaanlab_kode',
                                        'sourceUrl' => Yii::app()->createUrl('laboratorium/ActionAutoComplete/getKodeDaftarTindakan'),
                                        'options' => array(
                                            'showAnim' => 'fold',
                                            'minLength' => 2,
                                            'focus' => 'js:function( event, ui ) {
                                                                            $(this).val(ui.item.label);
                                                                            return false;
                                                                        }',
                                            'select' => 'js:function( event, ui ) {
                                                                                  $("#idPemeriksaanLab").val(ui.item.pemeriksaanlab_id);
                                                                                  return false;
                                                                        }',
                                        ),
                                        'htmlOptions' => array(
                                            'class' => 'span2',
                                            'onkeypress' =>"if (event.keyCode == 13){inputperiksaTindakan();}return $(this).focusNextInputField(event)",
                                            'onClick'=>'cekOptions()',
                                        ),
                                        'tombolDialog' => array('idDialog' => 'dialogAddTindakan'),
                                    ));
                    ?>
                </div>
            </td>
        </tr>
    </table>
<div>
    <?php 
//            echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
//                            array('class'=>'btn btn-primary','onclick'=>"$('#dialogTindakan').dialog('open');",
//                                  'id'=>'btnAddTindakan','onkeypress'=>"return $(this).focusNextInputField(event)",
//                                  'rel'=>'tooltip','title'=>'Klik Untuk Menambah Tindakan')); 
    
//            echo CHtml::link('<i class="icon-plus-sign icon-white"></i>',Yii::app()->controller->createUrl(Yii::app()->controller->id.'/daftarTindakan'),
//                    array('title'=>'Klik Untuk Tambah Tindakan','target'=>'iframe', 'onclick'=>'$("#dialogTindakan").dialog("open");', 'rel'=>'tooltip','class'=>'btn btn-primary'));
    
    ?>
    <br/>
</div>
            <table id="tblFormPemeriksaanLab" class="table table-condensed">
                <thead>
                    <tr>
                        <th>Pemeriksaan</th>
                        <th>Tarif</th>
                        <th>Qty</th>
                        <th>Satuan*</th>
                        <th>Cyto</th>
                        <th>Tarif Cyto</th>
                    </tr>
                </thead>
            </table>
            
            <div class="form-actions">
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
             <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
<?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
            </div>
        </td>
    </tr>

<?php $this->endWidget(); ?>

<?php 
//========= Dialog daftar tindakan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogTindakan',
    'options'=>array(
        'title'=>'Daftar Tindakan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>500,
        'resizable'=>false,
    ),
));
?>

<!--<iframe src="" name="iframe" width="100%" height="100%">
</iframe>-->


<?php //$this->renderPartial('_formDaftarTindakan',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,'modDaftarTindakan'=>$modDaftarTindakan)); ?>

<?
    $this->endWidget();
    //========= end pasien dialog =============================
?>
<?php

//========= Dialog daftar tindakan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogAddTindakan',
    'options'=>array(
        'title'=>'Daftar Tindakan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));


$modDataTindakan = new PemeriksaanlabM('search');
    $modDataTindakan->unsetAttributes();
    if(isset($_GET['PemeriksaanlabM'])) {
        $modDataTindakan->attributes = $_GET['PemeriksaanlabM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'pemeriksaanlab-m-grid',
            //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
            'dataProvider'=>$modDataTindakan->search(),
            'filter'=>$modDataTindakan,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPasien",
                                        "onClick" => "
                                            $(\"#dialogAddTindakan\").dialog(\"close\");
//                                           $(\"#dialogTindakan\").dialog(\"close\");
                                            $(\"#idPemeriksaanLab\").val(\"$data->pemeriksaanlab_id\");
                                            inputperiksaTindakan();
                                            clear();
                                        "))',
                    ),
                    array(
                        'header'=>'Jenis Pemeriksaan Lab',
                        'name'=>'jenispemeriksaanlab_nama',
                        'value'=>'$data->jenispemeriksaan->jenispemeriksaanlab_nama',
                    ),
                    array(
                        'header'=>'Kode Pemeriksaan',
                        'name'=>'pemeriksaanlab_kode',
                        'value'=>'$data->pemeriksaanlab_kode',
                    ),
                    array(
                        'header'=>'Nama Pemeriksaan',
                        'name'=>'pemeriksaanlab_nama',
                        'value'=>'$data->pemeriksaanlab_nama',
                    ),
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));


$this->endWidget();
//========= end pasien dialog =============================
    
?>

    
<script type="text/javascript">
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanLab = obj.value;
        var idKelasPelayanan = $('#idKelasPelayanan').val();
        var idPemeriksaanLab2 = $('#idPemeriksaanLab2').val();
        
                    $('#idPemeriksaanLab2').val(idPemeriksaanLab);
             
        if(idKelasPelayanan == ''){
             alert('Isi Terlebih Dahulu Daftar Pasien');
             
             setTimeout(function(){
                     $(obj).removeAttr('checked', 'checked');
             }, 500);
        }else{
            
            setTimeout(function(){
                 if(cekList(idPemeriksaanLab)==true){
                      jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/LoadFormPemeriksaanLabMasuk')?>',
                     'data':{idPemeriksaanlab:idPemeriksaanLab, kelasPelayan_id:idKelasPelayanan},
                     'type':'post',
                     'dataType':'json',
                     'success':function(data) {
                             if($.trim(data.form)=='')
                             {
                                $(this).removeAttr('checked');
                                alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                             } else if($.trim(data.form)=='tarif kosong') {
                                $(this).removeAttr('checked');
                                data.form = '';
                                alert ('Pemeriksaan belum memiliki tarif');
                             }
                             $('#tblFormPemeriksaanLab').append(data.form);
                             renameInput('permintaanPenunjang','inputpemeriksaanlab');
                             renameInput('permintaanPenunjang','inputtarifpemeriksaanlab');
                             renameInput('permintaanPenunjang','inputqty');
                             renameInput('permintaanPenunjang','satuan');
                             renameInput('permintaanPenunjang','cyto');
                             renameInput('permintaanPenunjang','persencyto');
                             renameInput('permintaanPenunjang','tarifcyto');
                             renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                             renameInput('permintaanPenunjang','pemeriksaanNama');
                             renameInput('permintaanPenunjang','idDaftarTindakan');
                             
                             if(obj.value == '352')
                            {
                                    batalPeriksa('563');
                                    $('#formPeriksaLab').find('input[value="563"]').attr('checked', 'checked');
                                    $('#formPeriksaLab').find('input[value="563"]').attr('disabled', 'true');

                                    batalPeriksa('564');
                                    $('#formPeriksaLab').find('input[value="564"]').attr('checked', 'checked');
                                    $('#formPeriksaLab').find('input[value="564"]').attr('disabled', 'true');
                                    hitungTotal();

                            }
                     } ,
                     'cache':false});
                }
             }, 1000);
             clear();
             
         }
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function inputperiksaTindakan()
{
        var idPemeriksaanLab = $('#idPemeriksaanLab').val();
        var idPemeriksaanLab2 = $('#idPemeriksaanLab2').val();
        var idKelasPelayanan = $('#idKelasPelayanan').val();
        
        $('#idPemeriksaanLab2').val(idPemeriksaanLab2);
//     if (cekList(idPemeriksaanLab) == true){  
         if(idKelasPelayanan == ''){
             alert('Isi Terlebih Dahulu Daftar Pasien');
             
             setTimeout(function(){
                     $("#daftartindakan_nama").val("");
                     $("#daftarTindakan").val("");
                     clear();
             }, 500);
        }else if(idPemeriksaanLab == ''){
            alert('Isi Tindakan Terlebih Dahulu');
        
        }else{
            if(cekList(idPemeriksaanLab)==true){                     
                    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/LoadFormPemeriksaanLabMasuk')?>',
                 'data':{idPemeriksaanlab:idPemeriksaanLab, kelasPelayan_id:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         if($.trim(data.form)=='')
                         {
                            $(this).removeAttr('checked');
                            alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                         } else if($.trim(data.form)=='tarif kosong') {
                            $(this).removeAttr('checked');
                            data.form = '';
                            alert ('Pemeriksaan belum memiliki tarif');
                         }
                         $('#tblFormPemeriksaanLab').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanlab');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanlab');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','idDaftarTindakan');


                 } ,
                 'cache':false}); 
             
                        clear();
            }
            
                setTimeout(function(){
                        tindakanCeklist();
                        clear();    
                }, 500);
             }
//        }
}

function cekList(id){
    x = true;
    $('.pemeriksaanlab_id').each(function(){
        if ($(this).val() == id){
            alert('Daftar Pemeriksaan Tindakan telah ada di List');
            x = false;
        }
    });
    return x;
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanLab tr').length;
    var i = -1;
    $('#tblFormPemeriksaanLab tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function batalPeriksa(idPemeriksaanlab)
{
    $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).detach();
}

function cyto_tarif(test, x){
    if(test.value == 1){
        $('.cyto_tarif_'+ x).show();
    }else{
        $('.cyto_tarif_'+ x).hide();
    }
}


function isiDataTindakan(data)
{
    
    test = $('#idPemeriksaanLab').val(data.pemeriksaanlab_id);
    alert(test);
}

function cekOptions(params){
    idOptions = $('#daftarTindakan').val();
    
    if(idOptions == ''){
        alert('Harap Memilih Kategori Pencarian Terlebih Dahulu')
    }
    
    $("#nama").show();
    $("#kode").hide();
    if(idOptions== 'nama'){
        $("#nama").show();
        $("#kode").hide();
    }else if(idOptions == 'kode'){
        $("#nama").hide();
        $("#kode").show(); 
    }  
    
}

function addId2(){
    
    idPemeriksaanLab = $('#idPemeriksaanLab').val();
    idPemeriksaanLab2 = $('#idPemeriksaanLab2').val();
    
    $('#idPemeriksaanLab2').val(idPemeriksaanLab);
    
    
}

function tindakanCeklist(){
    var idPemeriksaanLab = $('#idPemeriksaanLab').val();
    setTimeout(function(){
         $('div.boxtindakan').each(function(){
            var pemeriksaanLab = $(this).parents('tr').find('#pemeriksaanLab').val()

                if(pemeriksaanLab == idPemeriksaanLab){
                    $(this).parents('tr').find('pemeriksaanLab').each(function(){
                        $(this).attr('checked','checked');
                    });
                }
        });
    }, 500);
}

function clear(){
idOptions = $('#daftarTindakan').val();
   
    if(idOptions == 'nama'){
            $('#nama').val("");
            $('#daftarTindakan').val("");
            $('#pemeriksaanlab_kode').val("");
    }else if(idOptions == 'kode'){
            $('#pemeriksaanlab_kode').val("");
            $('#daftarTindakan').val("");
        
    }

}

</script>


<?php
function cekPilihan($pemeriksaanlab_id,$arrInputPemeriksaan)
{
    $cek = false;
    foreach ($arrInputPemeriksaan as $i => $items) {
        foreach($items as $j=>$item){
            if($pemeriksaanlab_id == $item->pemeriksaanlab_id) $cek = true;
        }
    }
    return $cek;
}
?>
<?php
$js= <<< JS
    $(document).ready(function() {
        $("#daftarTindakan").children("li").children("a").click(function() {
            $("#daftarTindakan").children("li").attr('class','');
        });
        
        $("#nama").show();
        $("#kode").hide();
    });

    
function onReset()
{
    setTimeout(
        function(){
            $.fn.yiiGridView.update('masukpenunjang-Laboratorium-form', {
                data: $("#namatindakan").serialize()
            });
            $.fn.yiiGridView.update('masukpenunjang-Laboratorium-form', {
                data: $("#kodetindakan").serialize()
            });      
        }, 2000
    );
    return false;
}   
JS;
Yii::app()->clientScript->registerScript('pencatatanriwayat',$js,CClientScript::POS_HEAD);
?>