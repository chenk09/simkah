<?php 
$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
//        array('label'=>'Jadwal Dokter', 'url'=>$this->createUrl('/rawatDarurat/jadwaldokterM')),
        
        array('label'=>'Kelompok Pemeriksaan Laboratorium', 'url'=>$this->createUrl('/laboratorium/kelompokPemeriksaanLabM/'), 'active'=>true),
        array('label'=>'Satuan Hasil Laboratorium', 'url'=>$this->createUrl('/laboratorium/satuanHasilLabM/')),
        array('label'=>'Rujukan Keluar', 'url'=>$this->createUrl('/laboratorium/rujukankeluarM')),
        array('label'=>'Status Periksa Hasil', 'url'=>$this->createUrl('/laboratorium/statusPeriksaHasilM')),
        array('label'=>'Sample Laboratorium', 'url'=>$this->createUrl('/laboratorium/samplelabM')),
        
    ),
)); ?>