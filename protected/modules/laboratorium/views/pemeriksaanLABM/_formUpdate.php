<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sanilai-rujukan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#daftartindakan_nama',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($modPemeriksaanLab); ?>
<table>
    <tr>
        <td>
            <?php //echo $form->dropDownList($modPemeriksaanLab,'daftartindakan_id',CHtml::listData($modPemeriksaanLab->DaftarTindakanItems, 'daftartindakan_id', 'daftartindakan_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'empty'=>'--'.$modPemeriksaanLab->getAttributeLabel('daftartindakan_id').'--')); ?>
            
            <div class="control-group ">
                <label class="control-label" for="bidang">Daftar Tindakan / Pemeriksaan <span class="required">*</span></label>
                <div class="controls">
                <?php echo $form->hiddenField($modPemeriksaanLab,'daftartindakan_id'); ?>
                        
                    <?php 
                            $this->widget('MyJuiAutoComplete', array(
                                            
                                            'name'=>'daftartindakan_nama',
                                            'value'=>$modPemeriksaanLab->daftartindakan->daftartindakan_nama,
                                            'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/TindakanPeriksaLab').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                             'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'focus'=> 'js:function( event, ui ) {
                                                        $(this).val( ui.item.label);
                                                        return false;
                                                    }',
                                                   'select'=>'js:function( event, ui ) { 
                                                        $("#'.CHtml::activeId($modPemeriksaanLab, 'daftartindakan_id').'").val(ui.item.daftartindakan_id);
                                                        $("#daftartindakan_nama").val(ui.item.daftartindakan_nama);
                                                        return false;
                                                    }',
                                            ),
                                            'htmlOptions'=>array(
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogTindakan'),
                                        )); 
                         ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="bidang">Jenis Pemeriksaan <span class="required">*</span></label>
                <div class="controls">
                    <?php echo $form->dropDownList($modPemeriksaanLab,'jenispemeriksaanlab_id',  CHtml::listData($modPemeriksaanLab->JenispemeriksaanLABItems, 'jenispemeriksaanlab_id', 'jenispemeriksaanlab_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'empty'=>'--'.$modPemeriksaanLab->getAttributeLabel('jenispemeriksaanlab_id').'--')); ?>
                </div>
            </div>
        </td>
    
        <td>
            <div class="control-group">
                <label class="control-label" for="bidang">Kode <span class="required">*</span>/ Urutan </label>
                <div class="controls">
                    <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_kode',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_kode'))); ?>
                    <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_urutan',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);",'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_urutan'))); ?>
                </div>
            </div>
            <?php //echo $form->textFieldRow($modPemeriksaanLab,'pemeriksaanlab_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>40,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_nama'))); ?>
            <div class="control-group">
                <?php //echo CHtml::css('ul.redactor_toolbar{z-index:10;}'); ?>
                <label class="control-label" for="bidang">Nama Pemeriksaan</label>
                <div class="controls">
                    <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_nama',array('class'=>'span3', 'onkeyup'=>"namaLain(this)", 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>40,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_nama'))); ?>
                    <?php //$this->widget('ext.redactorjs.Redactor',array('model'=>$modPemeriksaanLab,'attribute'=>'pemeriksaanlab_nama','toolbar'=>'mini','height'=>'40px')) ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="bidang">Nama Lainnya </label>
                <div class="controls">
                    <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_namalainnya',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>40,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_namalainnya'))); ?>
                    <?php //$this->widget('ext.redactorjs.Redactor',array('model'=>$modPemeriksaanLab,'attribute'=>'pemeriksaanlab_namalainnya','toolbar'=>'mini','height'=>'40px')) ?>
                </div>
            </div>
        </td>
    </tr>
</table>

<div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl('laboratorium/pemeriksaanLABM/admin'), 
                                    array('class'=>'btn btn-danger',
                                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('../tips/tipsaddedit3a',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
</div>

<?php $this->endWidget(); ?>

<?php
//========= Dialog buat cari Daftar Tindakan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogTindakan',
    'options'=>array(
        'title'=>'Daftar Tindakan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>750,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modTindakanLab = new PemeriksaanlabtindV('search');
$modTindakanLab->unsetAttributes();
if(isset($_GET['PemeriksaanlabtindV']))
    $modTindakanLab->attributes = $_GET['PemeriksaanlabtindV'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'lktindakan-m-grid',
	'dataProvider'=>$modTindakanLab->searchPeriksaLabIsNull(),
	'filter'=>$modTindakanLab,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",
                                "#",
                                array(
                                    "class"=>"btn-small", 
                                    "id" => "selectTindakan",
                                    "onClick" => "
                                    $(\"#'.CHtml::activeId($modPemeriksaanLab, 'daftartindakan_id').'\").val(\'$data->daftartindakan_id\');
                                    $(\"#daftartindakan_nama\").val(\'$data->daftartindakan_nama\');
                                    $(\'#dialogTindakan\').dialog(\'close\');return false;"))'
                ),
            'kelompoktindakan_nama',
            'kategoritindakan_nama',
            'daftartindakan_kode',
            'daftartindakan_nama',
            'harga_tariftindakan',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget();
?>

<script type="text/javascript">
    function namaLain(nama)
    {
        document.getElementById('LKPemeriksaanLabM_pemeriksaanlab_namalainnya').value = nama.value.toUpperCase();
    }
</script>