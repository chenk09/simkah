
<?php
$this->breadcrumbs=array(
	'Sapemeriksaan Labms'=>array('index'),
	'Create',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Pemeriksaan Laboratorium ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                //array_push($arrMenu,array('label'=>Yii::t('mds','List').' Pemeriksaan LAB', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pemeriksaan Laboratorium', 'icon'=>'folder-open', 'url'=>array('Admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial($this->pathView.'_form', array('model'=>$model,'modPemeriksaanLab'=>$modPemeriksaanLab)); ?>

<?php //$this->widget('TipsMasterData',array('type'=>'create'));?>

