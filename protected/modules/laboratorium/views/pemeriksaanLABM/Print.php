
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

$table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->search();
         $template = "{pager}{summary}\n{items}";
    }
    
$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>$sort,
	'dataProvider'=>$data,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'pemeriksaanlab_id',
        array(
                        'name'=>'pemeriksaanlab_id',
                        'value'=>'$data->pemeriksaanlab_id',
                        'filter'=>false,
                ),
        array(
                        'name'=>'daftartindakan_nama',
                        'value'=>'$data->daftartindakan->daftartindakan_nama',
                ),
        array(
                'name'=>'jenispemeriksaanlab_id',
                'value'=>'$data->jenispemeriksaan->jenispemeriksaanlab_nama',
        ),
        'pemeriksaanlab_kode',
        'pemeriksaanlab_urutan',

        array(
            'name'=>'pemeriksaanlab_nama',
            'type'=>'raw',
            'value'=>'$data->pemeriksaanlab_nama',
        ),
        array(
            'header'=>'<center>Status</center>',
            'value'=>'($data->pemeriksaanlab_aktif == 1 ) ? "Aktif" : "Tidak Aktif"',
            'htmlOptions'=>array('style'=>'text-align:center;'),
        ),
 
        ),
    )); 
?>