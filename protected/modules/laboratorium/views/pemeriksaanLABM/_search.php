<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sapemeriksaan-labm-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'pemeriksaanlab_id',array('class'=>'span5')); ?>
	
    <div class="control-group">
		<label class="control-label">
	        <?php echo $form->labelEx($model,'Jenis Pemeriksaan', array('class'=>'control-label')) ?>
	    </label>
	    <div class="controls">
	    	<?php echo $form->dropDownList($model,'jenispemeriksaanlab_id',CHtml::listData($model->JenisPemeriksaanItems, 'jenispemeriksaanlab_id', 'jenispemeriksaanlab_nama'),array('empty'=>'--Pilih--','class'=>'span3')); ?>
	    </div>
	</div>

	<div class="control-group">
		<label class="control-label">
	        <?php echo $form->labelEx($model,'Daftar Tindakan', array('class'=>'control-label')) ?>
	    </label>
	    <div class="controls">
	    	<?php echo $form->textField($model,'daftartindakan_nama',array('class'=>'span3','maxlength'=>10)); ?>
	    	<?php //echo $form->dropDownList($model,'daftartindakan_id',CHtml::listData($model->TindakanItems, 'daftartindakan_id', 'daftartindakan_nama'),array('empty'=>'--Pilih--','class'=>'span3')); ?>
	    </div>
	</div>

	<div class="control-group">
		<label class="control-label">
	        <?php echo $form->labelEx($model,'Kode Pemeriksaan', array('class'=>'control-label')) ?>
	    </label>
	    <div class="controls">
	    	<?php echo $form->textField($model,'pemeriksaanlab_kode',array('class'=>'span3','maxlength'=>10)); ?>
	    </div>
	</div>

	<div class="control-group">
		<label class="control-label">
	        <?php echo $form->labelEx($model,'Nama Pemeriksaan', array('class'=>'control-label')) ?>
	    </label>
	    <div class="controls">
	    	<?php echo $form->textField($model,'pemeriksaanlab_nama',array('class'=>'span3','maxlength'=>40)); ?>
	    </div>
	</div>
	<?php echo $form->checkBoxRow($model,'pemeriksaanlab_aktif', array('checked'=>'checked')); ?>




	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
