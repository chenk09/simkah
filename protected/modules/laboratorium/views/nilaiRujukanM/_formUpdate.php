
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sanilai-rujukan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
<table>
    <tr>
        <td>
            <?php echo $form->dropDownList($modPemeriksaanLab,'daftartindakan_id',CHtml::listData($modPemeriksaanLab->DaftarTindakanItems, 'daftartindakan_id', 'daftartindakan_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'empty'=>'--'.$modPemeriksaanLab->getAttributeLabel('daftartindakan_id').'--')); ?>
        </td>
        <td>
            <?php echo $form->dropDownList($modPemeriksaanLab,'jenispemeriksaanlab_id',  CHtml::listData($modPemeriksaanLab->JenispemeriksaanLABItems, 'jenispemeriksaanlab_id', 'jenispemeriksaanlab_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'empty'=>'--'.$modPemeriksaanLab->getAttributeLabel('jenispemeriksaanlab_id').'--')); ?>
        </td>
        <td>
            <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_kode',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_kode'))); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_urutan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_urutan'))); ?>
        </td>
        <td>
            <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>40,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_nama'))); ?>
        </td>
        <td>
            <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_namalainnya',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>40,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_namalainnya'))); ?>
        </td>
    </tr>
</table>
<?php echo CHtml::checkBox('sama',false,array('onclick'=>'samaIsi()'));?>        
<table class="table">
    <tr>
<?php
$jenisKelamin=JenisKelamin::items();
$jumlahJK=count($jenisKelamin);
foreach($jenisKelamin AS $dataJK):
    echo "<td width=\"".$width."%\">".$form->checkBox($model,'nilairujukan_jeniskelamin[]',array('value'=>$dataJK,'uncheckValue'=>'N'))." ".$dataJK."
            <table>
                ";
        $kelompokUmur=KelompokUmur::items();
    foreach($kelompokUmur AS $dataKelUmur):
            echo "<tr><td>".$dataKelUmur.$form->hiddenField($model,'kelompokumur['.$dataJK.'][]',array('value'=>$dataKelUmur,'readonly'=>TRUE))."</td>
                      <td>".$form->textField($model,'nilairujukan_min['.$dataJK.'][]',array('class'=>'span1','placeholder'=>$model->getAttributeLabel('nilairujukan_min'))).
                            $form->textField($model,'nilairujukan_max['.$dataJK.'][]',array('class'=>'span1','placeholder'=>$model->getAttributeLabel('nilairujukan_max')))."</td>
                      <td>".$form->textField($model,'nilairujukan_nama['.$dataJK.'][]',array('class'=>'span2','placeholder'=>$model->getAttributeLabel('nilairujukan_nama')))."</td>
                  </tr>";
    endforeach;
    echo "      
            </table>
            </td>";    
endforeach;    
 ?>
    </tr>   
</table>
<table class="table">
    <tr>
        <td>
              <?php echo $form->dropDownList($model,'nilairujukan_satuan',SatuanHasilLab::items(),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'empty'=>'--'.$model->getAttributeLabel('nilairujukan_satuan').'--')); ?>
        </td>
        <td>
              <?php echo $form->textField($model,'nilairujukan_metode',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30,'placeholder'=>$model->getAttributeLabel('nilairujukan_metode'))); ?>
        </td>
        <td>
              <?php echo $form->textArea($model,'nilairujukan_keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'placeholder'=>$model->getAttributeLabel('nilairujukan_keterangan'))); ?>
        </td>
    </tr>
</table>    
        <table class="table">
            <tr>
               <td>
                </td>
            </tr>
        </table>
            <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.nilaiRujukanM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	</div>

<?php $this->endWidget(); ?>

<?php
 $kelompokUmur=KelompokUmur::items();
 foreach ($kelompokUmur AS $data2) {
    
}
$jenisKelamin=JenisKelamin::items();
 
$js = <<< JS

//function samaIsi()
//{
//    alert('rizky');
//} 

JS;
Yii::app()->clientScript->registerScript('samaInputan',$js,CClientScript::POS_HEAD);
?>
