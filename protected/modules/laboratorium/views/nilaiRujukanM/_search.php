<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sanilai-rujukan-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'nilairujukan_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'pemeriksaanlab_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_jeniskelamin',array('class'=>'span2','maxlength'=>50,'size'=>20)); ?>

	<?php echo $form->textFieldRow($model,'kelompokumur',array('class'=>'span2','maxlength'=>20,'size'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_nama',array('class'=>'span2','maxlength'=>20,'size'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_min',array('class'=>'span1','size'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_max',array('class'=>'span1','size'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_satuan',array('class'=>'span2','maxlength'=>50,'size'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_metode',array('class'=>'span2','maxlength'=>30,'size'=>20)); ?>

	<?php// echo $form->textAreaRow($model,'nilairujukan_keterangan',array('rows'=>6, 'cols'=>15, 'class'=>'span3')); ?>

	<?php echo $form->checkBoxRow($model,'nilairujukan_aktif',array('checked'=>'checked')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
