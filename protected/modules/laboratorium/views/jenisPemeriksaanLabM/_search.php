<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sajenis-pemeriksaan-lab-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'jenispemeriksaanlab_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenispemeriksaanlab_kode',array('class'=>'span3','maxlength'=>10)); ?>

	<?php //echo $form->textFieldRow($model,'jenispemeriksaanlab_urutan',array('class'=>'span3')); ?>

	<?php echo $form->textFieldRow($model,'jenispemeriksaanlab_nama',array('class'=>'span3','maxlength'=>30)); ?>

	<?php //echo $form->textFieldRow($model,'jenispemeriksaanlab_namalainnya',array('class'=>'span3','maxlength'=>30)); ?>

	<?php echo $form->dropDownListRow($model,'jenispemeriksaanlab_kelompok',KelompokPemeriksaanLab::items(),array('empty'=>'--Pilih--','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?> 
	<?php //echo $form->textFieldRow($model,'jenispemeriksaanlab_kelompok',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->checkBoxRow($model,'jenispemeriksaanlab_aktif',array('checked'=>'checked')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
