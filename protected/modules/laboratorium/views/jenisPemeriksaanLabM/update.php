
<?php
$this->breadcrumbs=array(
	'Sajenis Pemeriksaan Lab Ms'=>array('index'),
	$model->jenispemeriksaanlab_id=>array('view','id'=>$model->jenispemeriksaanlab_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Jenis Pemeriksaan Lab ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Jenis Pemeriksaan Lab', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Jenis Pemeriksaan Lab', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jenis Pemeriksaan Lab', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->jenispemeriksaanlab_id))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jenis Pemeriksaan Lab', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial($this->pathView.'_formUpdate',array('model'=>$model)); ?>