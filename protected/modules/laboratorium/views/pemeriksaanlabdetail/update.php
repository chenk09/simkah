
<?php
$this->breadcrumbs=array(
	'Lkpemeriksaanlabdet Ms'=>array('index'),
	$model->pemeriksaanlabdet_id=>array('view','id'=>$model->pemeriksaanlabdet_id),
	'Update',
);

$arrMenu = array();
array_push($arrMenu,array('label'=>Yii::t('mds','Update').' LKPemeriksaanlabdetM #'.$model->pemeriksaanlabdet_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//array_push($arrMenu,array('label'=>Yii::t('mds','List').' LKPemeriksaanlabdetM', 'icon'=>'list', 'url'=>array('index'))) ;
//(Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' LKPemeriksaanlabdetM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//array_push($arrMenu,array('label'=>Yii::t('mds','View').' LKPemeriksaanlabdetM', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->pemeriksaanlabdet_id))) ;
(Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' LKPemeriksaanlabdetM', 'icon'=>'folder-open', 'url'=>array('admin','modulId'=>Yii::app()->session['modulId']))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model,
                                                    'modDetails'=>$modDetails,
                                                    'modPemeriksaanLab'=>$modPemeriksaanLab)); ?>