<?php
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = true;
	
	if (!isset($caraPrint)){
		$this->renderPartial('_form_header',array(
			'model'=>$model,
		));
		echo '<legend class="rim">Tabel Pendapatan Ruangan - dari Luar RS</legend>';
		$data = $model->searchPendapatanLuar();
		$template = "{pager}{summary}\n{items}";
	}else{
		if($caraPrint == "pdf") echo("<style>table td{font-size:12px;} table th{font-size:12px;}</style>");
        $data = $model->searchPendapatanLuarPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "excel"){
			$table = 'ext.bootstrap.widgets.BootExcelGridView';
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="RekapPendapatanRS-'.date("Y/m/d").'.xls"');
			header('Cache-Control: max-age=0');
		}
	}
?>
<?php
$this->widget($table,array(
	'id'=>'tablePendapatan',
	'dataProvider'=>$data,
	'template'=>$template,
	'enableSorting'=>$sort,
	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
			array(
				'header' => 'No',
				'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
				'htmlOptions'=>array('style'=>'text-align:center;'),
				'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
			),
			array(
				'header'=>'No Lab',
				'type'=>'raw',
				'value'=>'$data->no_pendaftaran',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
			),
			"no_rekam_medik",
			array(
				'header'=>'Nama',
				'type'=>'raw',
				'value'=>'$data->nama_pasien',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'header'=>'Cara Bayar / Penjamin',
				'type'=>'raw',
				'value'=>'$data->carabayar_nama." / ".$data->penjamin_nama',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'header'=>'Deposit',
				'type'=>'raw',
				'value'=>'(empty($data->jumlahuangmuka) ? "0" : $data->jumlahuangmuka)',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array('colspan'=>6,'style'=>'text-align:right;font-style:italic;'),
				'footer'=>'Jumlah Total',
			),
			array(
				'header'=>'Pend. Seharusnya',
				'type'=>'raw',
				'name'=>'pend_seharusnya',
				'value'=>'MyFunction::formatNumber($data->pend_seharusnya)',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array('style'=>'text-align:right;'),
				'footer'=>'sum(pend_seharusnya)',
			),
			array(
				'header'=>'Pend. Sebenarnya',
				'type'=>'raw',
				'name'=>'pend_sebenarnya',
				'value'=>'MyFunction::formatNumber($data->pend_sebenarnya)',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array('style'=>'text-align:right;'),
				'footer'=>'sum(pend_sebenarnya)',
			),
			array(
				'header'=>'Sisa',
				'type'=>'raw',
				'name'=>'sisa',
				'value'=>'MyFunction::formatNumber($data->sisa)',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array('style'=>'text-align:right;'),
				'footer'=>'sum(sisa)',
			),
	),
	'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>