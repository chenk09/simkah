<?php
$urlPrint = Yii::app()->createUrl('laboratorium/LaporanPendRuangan/cetak');
Yii::app()->clientScript->registerScript('search', "
$('.cetak').click(function(){
	var caraPrint = $(this).attr('jenis_cetak');
	window.open('${urlPrint}&'+ $('#searchLaporan').serialize() +'&caraPrint='+caraPrint,'','location=_new, width=900px, scrollbars=yes');
    return false;
});
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('#searchLaporan').submit(function(){
	$.fn.yiiGridView.update('tablePendapatan', {
		data: $(this).serialize()
	});
    return false;
});
");
?>
<legend class="rim2">Laporan Pendapatan Ruangan</legend>
<div class="search-form" style="">
    <?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
		'type' => 'horizontal',
		'id' => 'searchLaporan',
		'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
	)); ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
        .nav-tabs > .active a:hover{cursor:pointer;}
    </style>
<fieldset>
        <legend class="rim"><i class="icon-search"></i> Pencarian Berdasarkan : </legend>
    <table>
            <tr>
                <td>
                       <div class="control-group ">
                        <?php
                            echo CHtml::label('Tanggal Masuk Penunjang ','Tanggal Masuk Penunjang ', array('class'=>'control-label'));
                            echo $form->hiddenField($model,'filter_tab',array());
                        ?>
                           
                            <div class="controls">
                                <?php echo $form->dropDownList($model,'bulan',
                                    array(
                                        'hari'=>'Hari Ini',
                                        'bulan'=>'Bulan',
                                        'tahun'=>'Tahun',
                                    ),
                                    array(
                                        'id'=>'PeriodeName',
                                        'onChange'=>'setPeriode()',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;',
                                        'style'=>'width:120px;float:left',
                                    )
                                    );
                                ?>
                                <div style="margin-left:5px;float:left;">
                                <?php
                                    $this->widget('MyDateTimePicker', array(
                                        'model' => $model,
                                        'attribute' => 'tglAwal',
                                        'mode' => 'datetime',
                                        'options' => array(
                                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array(
                                            'readonly' => true,
                                            'class'=>'dtPicker3',
                                            'onclick'=>'checkPilihan(event)',
                                            'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        ),
                                    ));
                                ?>
                            </div>
                            <div style="margin-left:5px;float:left;">
                                <?php
                                    $this->widget('MyDateTimePicker',
                                        array(
                                            'model'=>$model,
                                            'attribute'=>'tglAkhir',
                                            'mode'=>'datetime',
                                            'options'=>array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            ),
                                            'htmlOptions'=>array(
                                                'readonly'=>true,
                                                'class'=>'dtPicker3',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'style'=>''
                                            ),
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </td> 
            </tr>
            <tr>
				<td>
					<div class="control-group">
						<label class="control-label">No. Pendaftaran</label>
						<div class="controls">
							<?php echo $form->textField($model,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
						</div>
					</div>
				</td>
            </tr>
     </table>
    </fieldset>
    <div class="form-actions">
		<div class="row">
		<div class="pull-left" style="margin-right:5px;">
			<?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), 
				array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
			?>
			<?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Reset', array('{icon}' => '<i class="icon-refresh icon-white"></i>')), 
				array('class' => 'btn btn-danger', 'type' => 'reset', 'id' => 'btn_simpan'));
			?>
		</div>
		<div class="pull-left">
			<?php $this->widget('bootstrap.widgets.BootButtonGroup', array(
				'type'=>'info',
				'buttons'=>array(
					array(
						'label'=>'Print',
						'type'=>'success',
						'icon'=>'icon-print icon-white',
						'url'=>'javascript:void(0)',
						'htmlOptions'=>array('class'=>'cetak', 'jenis_cetak'=>'print')
					),
					array('label'=>'Export','icon'=>'icon-download icon-white', 'items'=>array(
						array('label'=>'PDF', 'icon'=>'icon-pdf', 'url'=>'javascript:void(0)', 'itemOptions'=>array('class'=>'cetak', 'jenis_cetak'=>'pdf')),
						array('label'=>'Excel','icon'=>'icon-book', 'url'=>'javascript:void(0)', 'itemOptions'=>array('class'=>'cetak', 'jenis_cetak'=>'excel')),
					)),
				),
			)); ?>
		</div>
		</div>
    </div>
</div>    
<?php $this->endWidget(); ?>

<?php
	$this->widget('bootstrap.widgets.BootMenu',array(
		'type'=>'tabs',
		'stacked'=>false,
		'items'=>array(
			array(
				'label'=>'Pendapatan dari RS',
				'url'=>array("index", "view"=>"rs"),
				'active'=>($model->filter_tab == "rs" ? true : false)
			),
			array(
				'label'=>'Pendapatan dari Luar RS',
				'url'=>array("index", "view"=>"luar"),
				'active'=>($model->filter_tab == "luar" ? true : false)
			),
		),
	))
?>


<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT
function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
	$.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
		$('form input[name*="[tglAwal]"]').val(data.periodeawal);
		$('form input[name*="[tglAkhir]"]').val(data.periodeakhir);
	},'json');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
function checkPilihan(event){
	var namaPeriode = $('#PeriodeName').val();
	if(namaPeriode == '')
	{
		alert('Pilih Kategori Pencarian');
		event.preventDefault();
		$('#dtPicker3').datepicker("hide");
		return true;
		;
	}
}
</script>