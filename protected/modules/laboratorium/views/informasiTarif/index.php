<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<legend class="rim2">Infomasi Tarif Laboratorium</legend> 	
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarTindakan-grid',
	'dataProvider'=>$modTarifLabRad->searchTarifLabRad(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header'=>'No.',
                'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            ),
            'jenispemeriksaanlab_nama',
            'pemeriksaanlab_nama',
            'kelaspelayanan_nama',
//            'harga_tariftindakan',
            array(
                'name'=>'harga_tariftindakan',
                'value'=>'MyFunction::formatNumber($data->harga_tariftindakan)',
            ),
            array(
                'name'=>'persencyto_tind',
                'value'=>'$data->persencyto_tind',
            ),
//            array(
//                'name'=>'persendiskon_tind',
//                'value'=>'$data->persendiskon_tind." %"',
//            ),
//            array(
//                'name'=>'hargadiskon_tind',
//                'value'=>'"Rp. ".MyFunction::formatNumber($data->hargadiskon_tind)',
//            ),
            array(
                'header'=>'Tindakan',
                'type'=>'raw',
//                'value'=>'CHtml::checkBox("pilihPemeriksaan","", array("value"=>"$data->pemeriksaanlab_id"))',
                'value'=>'CHtml::link("<i class=\'icon-plus\'></i> ","javascript:void(0);" ,array("title"=>"Klik Untuk Menambahkan Ke Daftar Pemeriksaan Total","onclick"=>"tambahDaftar(this);", "rel"=>"tooltip"))','htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
                
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<legend class="rim">Daftar Pemeriksaan Total</legend>
<table class="table table-striped table-bordered table-condensed">
    <thead>
        <th>Jenis Pemeriksaan</th>
        <th>Nama Pemeriksaan</th>
        <th>Kelas Pelayanan</th>
        <th>Harga Tarif Tindakan (Rp.)</th>
        <th>Persen Cyto</th>
        <th colspan='2'>Tarif Cyto</th>
        <th>Qty</th>
        <th>Subtotal</th>
        <th width='50px'>Tindakan</th>
    </thead>
    <tbody id="tabelDaftarPemeriksaanTotal">
        
    </tbody>
    <tr><td colspan="8"><center><b>Total Pemeriksaan</b></center></td><td id="totalPemeriksaan"></td><td></td></tr>
</table>

<legend class="rim">Pencarian</legend>
<?php
Yii::app()->clientScript->registerScript('search', "

$('#formCari').submit(function(){
	$.fn.yiiGridView.update('daftarTindakan-grid', {
		data: $(this).serialize()
	});
	return false;
});
", CClientScript::POS_READY);
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'formCari',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#SARuanganM_instalasi_id',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>
<?php // echo $form->textFieldRow($modTarifLabRad,'jenispemeriksaanlab_nama',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
<?php echo $form->dropDownListRow($modTarifLabRad, 'jenispemeriksaanlab_nama', CHtml::listData($modTarifLabRad->getJenisPemeriksaan(), 'jenispemeriksaanlab_nama', 'jenispemeriksaanlab_nama') , array( 'empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
<?php echo $form->textFieldRow($modTarifLabRad,'pemeriksaanlab_nama',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'onclick'=>'resetPencarian();')); ?>
	 <?php 
//           $content = $this->renderPartial('../tips/informasi',array(),true);
//			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
        ?>
</div>
<?php $this->endWidget(); ?>
<script>
    $("#totalPemeriksaan").append('0');
    function resetPencarian(){
        $('#formCari').trigger("reset");
        $(".icon-search").trigger("click");
    }
    function tambahDaftar(obj){
//        alert('Dipilih !');
//        var data = $(obj).parent().parent().html(); JIKA PER BARIS
        var jenisPemeriksaan = $(obj).parent().parent().find("td").eq(1).html();
        var namaPemeriksaan = $(obj).parent().parent().find("td").eq(2).html();
        var kelasPelayanan = $(obj).parent().parent().find("td").eq(3).html();
        var tarifTindakan = $(obj).parent().parent().find("td").eq(4).html();
        var persenCyto = $(obj).parent().parent().find("td").eq(5).html();
        var appendTxt = "<tr><td>"+jenisPemeriksaan+"</td><td>"+namaPemeriksaan+"</td><td>"+kelasPelayanan+"</td><td class='tarif'>"+tarifTindakan+"</td><td class='persenCyto'>"+persenCyto+"</td><td><input title='Ceklist untuk menambahkan tarif cyto' type='checkbox' class='isCyto' onclick='hitungSubtotal(this);'></td><td class='tarifCyto'></td><td><input class='span1 qty' type='text' maxlength='3' value='1' onkeyup='inputNumber(this); hitungSubtotal(this);'></td><td class='subtotal'>"+tarifTindakan+"</td><td><center><a href='javascript:void(0);' onclick='hapusDaftar(this);'><i class='icon-minus' title='Klik untuk menghapus tindakan dari daftar ini'></i></a></center></td></tr>";
        $("#tabelDaftarPemeriksaanTotal").after(appendTxt);
        
        hitungTotalPemeriksaan();
    }
    function hapusDaftar(obj){
        if(confirm('Apakah anda akan menghapus ini dari Daftar Pemeriksaan Total?')){
            var tarifTindakan = $(obj).parent().parent().parent().find("td").eq(3).html();
            tarifTindakan = 0 - unformatNumber(tarifTindakan); //untuk menghasilkan nilai minus
            $(obj).parent().parent().parent().detach();
            
            hitungTotalPemeriksaan();
        }
    }
    function hitungSubtotal(obj){
        var tarifCyto = 0;
        var subtotal = 0;
        var tarifTindakan = 0;
        var persenCyto = 0;
        var qty = 0; 
        tarifTindakan = unformatNumber($(obj).parent().parent().find(".tarif").html());
        persenCyto = $(obj).parent().parent().find(".persenCyto").html();
        qty = $(obj).parent().parent().find(".qty").val();

        if($(obj).parent().parent().find('.isCyto').is(':checked')){
            tarifCyto = tarifTindakan * persenCyto / 100;
            $(obj).parent().parent().find(".tarifCyto").empty();
            $(obj).parent().parent().find(".tarifCyto").append(formatUang(tarifCyto));
        }else{
            $(obj).parent().parent().find(".tarifCyto").empty();
        }
        subtotal = (tarifTindakan + tarifCyto) * qty;
        $(obj).parent().parent().find(".subtotal").empty();
        $(obj).parent().parent().find(".subtotal").append(formatUang(subtotal));
        
        hitungTotalPemeriksaan();
    }
    function hitungTotalPemeriksaan(){
        var total = 0;
        $('.subtotal').each(
            function(){
                total += (unformatNumber($(this).html()));
            }
        );
        $("#totalPemeriksaan").empty();
        $("#totalPemeriksaan").append(formatUang(total));
//        alert(tarif);
    }
    function inputNumber(obj){
        var d = $(obj).attr('numeric');
        var value = $(obj).val();
        var orignalValue = value;
        value = value.replace(/[0-9]*/g, "");
        var msg = "Only Integer Values allowed.";

        if (d == 'decimal') {
        value = value.replace(/\./, "");
        msg = "Only Numeric Values allowed.";
        }

        if (value != '') {
        orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
        $(obj).val(orignalValue);
        }
    }
    
</script>
