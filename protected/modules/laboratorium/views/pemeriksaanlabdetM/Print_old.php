<style>
    #pemeriksaanlabdet-m-grid_c2
    {
        border-left:1px solid #DDDDDD;
    }
</style>
<script>
    $('document').ready(function(){
        $('#colspan').parents('th').css('border-bottom','1px solid #DDDDDD');
    });
</script>
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
    
$this->widget($table,array(
	'id'=>'pemeriksaanlabdet-m-grid',
	'dataProvider'=>$model->searchPrint(),
                'template'=>"{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                'mergeHeaders'=>array(
                    array(
                        'name'=>'<center id="colspan">Nilai Rujukan</center>',
                        'start'=>'2',
                        'end'=>'6',
                    ),
                ),
                'mergeColumns'=>array(
                    'pemeriksaanlab.pemeriksaanlab_nama',
                    'nilairujukan.kelompokdet'
                ),
	'columns'=>array(
                                array(
                                    'name'=>'pemeriksaanlab.pemeriksaanlab_nama',
                                    'value'=>'$data->pemeriksaanlab->pemeriksaanlab_nama'
                                ),
		'pemeriksaanlabdet_nourut',
		////'pemeriksaanlabdet_id',
                                array(
                                    'name'=>'nilairujukan.kelompokdet',
                                    'header'=>'Kelompok',
                                    'value'=>'$data->nilairujukan->kelompokdet',
                                ),
                                array(
                                    'header'=>'Detail pemeriksaan',
                                    'value'=>'$data->nilairujukan->namapemeriksaandet',
                                ),
                                array(
                                    'header'=>'Kelompok umur',
                                    'value'=>'$data->nilairujukan->kelompokumur',
                                ),
                                array(
                                    'header'=>'Jenis kelamin',
                                    'value'=>'$data->nilairujukan->nilairujukan_jeniskelamin',
                                ),
                                array(
                                    'header'=>'Nilai rujukan metode',
                                    'value'=>'$data->nilairujukan->nilairujukan_metode'
                                ),
//		'nilairujukan_id',
//		'pemeriksaanlab_id',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>