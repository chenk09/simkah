<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'pemeriksaanlabdet-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'pemeriksaanlabdet_id',array('class'=>'span3')); ?>
	<?php //echo $form->dropDownListRow($model,'pemeriksaanlab_id',CHtml::listData($model->getPemeriksaanlabItems(),'pemeriksaanlab_id','pemeriksaanlab_nama'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
	<?php //echo $form->dropDownListRow($model,'nilairujukan_id',CHtml::listData($model->getNilairujukanItems(),'nilairujukan_id','namapemeriksaandet'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
	<?php //echo $form->textFieldRow($model,'pemeriksaanlab_nama',array('class'=>'span3')); ?>
	<?php //echo $form->textFieldRow($model,'nilairujukan_id',array('class'=>'span3')); ?>
	<div class="control-group">
                        <?php echo CHtml::label('Pemeriksaan lab','pemeriksaanlab',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php
                                echo $form->hiddenField($model,'pemeriksaanlab_id');
                                $this->widget('MyJuiAutoComplete', array(
                                                'name'=>'pemeriksaanlab_nama',
                                                'value'=>$model,
                                                'source'=>'js: function(request, response) {
                                                               $.ajax({
                                                                   url: "'.Yii::app()->createUrl('ActionAutoComplete/Pemeriksaanlab').'",
                                                                   dataType: "json",
                                                                   data: {
                                                                       term: request.term,
                                                                   },
                                                                   success: function (data) {
                                                                           response(data);
                                                                   }
                                                               })
                                                            }',
                                                 'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'focus'=> 'js:function( event, ui ) {
                                                            $(this).val( ui.item.label);
                                                            return false;
                                                        }',
                                                       'select'=>'js:function( event, ui ) { 
                                                            $("#LKPemeriksaanlabdetM_pemeriksaanlab_id").val(ui.item.pemeriksaanlab_id);
                                                            $("#pemeriksaanlab_nama").val(ui.item.pemeriksaanlab_nama);
                                                            return false;
                                                        }',
                                                ),
                                                'htmlOptions'=>array(
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                ),
                                                'tombolDialog'=>array('idDialog'=>'dialogPemeriksaanlab'),
                                            )); 
                         ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo CHtml::label('Kelompok Nilai rujukan','nilairujukan',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php 
                                echo $form->hiddenField($model,'nilairujukan_id');
                                $this->widget('MyJuiAutoComplete', array(
                                                'name'=>'nilairujukan',
                                                'source'=>'js: function(request, response) {
                                                               $.ajax({
                                                                   url: "'.Yii::app()->createUrl('ActionAutoComplete/Nilairujukan').'",
                                                                   dataType: "json",
                                                                   data: {
                                                                       term: request.term,
                                                                   },
                                                                   success: function (data) {
                                                                           response(data);
                                                                   }
                                                               })
                                                            }',
                                                 'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'focus'=> 'js:function( event, ui ) {
                                                            $(this).val( ui.item.label);
                                                            return false;
                                                        }',
                                                       'select'=>'js:function( event, ui ) { 
                                                            $("#LKPemeriksaanlabdetM_nilairujukan_id").val(ui.item.nilairujukan_id);
                                                            $("#nilairujukan").val(ui.item.label);
                                                            return false;
                                                        }',
                                                ),
                                                'htmlOptions'=>array(
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                ),
                                                'tombolDialog'=>array('idDialog'=>'dialogNilairujukan'),
                                            )); 
                         ?>
                                         <?php 
//                                                echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
//                                                        array(
//                                                                'onclick'=>'submitPemeriksaanhasil();
//                                                                                  return false;',
//                                                                'class'=>'btn btn-primary',
//                                                                'onkeypress'=>"\$(\"#nilairujukan_id\").val();
//                                                                                          \$(\"#nilairujukan\").val();
//                                                                                            submitPemeriksaanhasil();",
//                                                                'rel'=>"tooltip",
//                                                                'id'=>'tambahPemeriksaanhasil',  
//                                                                'title'=>"Klik Untuk Hasil Pemeriksaan",
//                                                                )
//                                                        );
                                         ?>
                    </div>
                </div>


	<?php //echo $form->textFieldRow($model,'pemeriksaanlabdet_nourut',array('class'=>'span3')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
<!-- ===================================== Widget Pemeriksaan Lab =============================== -->
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'dialogPemeriksaanlab',
        'options'=>array(
            'title'=>'Pencarian Pemeriksaan Lab',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>600,
            'resizable'=>false,
        ),
    ));

    $modPemeriksaanlab = new PemeriksaanlabM;
    $modPemeriksaanlab->unsetAttributes();
    if (isset($_GET['PemeriksaanlabM'])) {
        $modPemeriksaanlab->attributes = $_GET['PemeriksaanlabM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'pemeriksaanlab-grid',
        'dataProvider'=>$modPemeriksaanlab->search(),
        'filter'=>$modPemeriksaanlab,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-striped table-condensed',
        'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                array(
                                        "class"=>"btn-small",
                                        "id" => "selectPemeriksaanlab",
                                        "onClick" => "\$(\"#LKPemeriksaanlabdetM_pemeriksaanlab_id\").val(\"$data->pemeriksaanlab_id\");
                                        						\$(\"#pemeriksaanlab_nama\").val(\"$data->pemeriksaanlab_nama\");
                                                              \$(\"#dialogPemeriksaanlab\").dialog(\"close\");"
                                ))',
            ),
            'pemeriksaanlab_kode',
            'pemeriksaanlab_nama',
            'pemeriksaanlab_namalainnya',
        ),
));
$this->endWidget();
?>
<!-- ===================================== Widget Nilairujukan==== =============================== -->
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'dialogNilairujukan',
        'options'=>array(
            'title'=>'Pencarian Nilai Rujukan',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>600,
            'resizable'=>false,
        ),
    ));

    $modNilairujukan = new NilairujukanM;
    $modNilairujukan->unsetAttributes();
    if (isset($_GET['NilairujukanM'])) {
        $modNilairujukan->attributes = $_GET['NilairujukanM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'nilairujukan-grid',
        'dataProvider'=>$modNilairujukan->search(),
        'filter'=>$modNilairujukan,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-striped table-condensed',
        'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                array(
                                        "class"=>"btn-small",
                                        "id" => "selectNilaiRujukan",
                                        "onClick" => "\$(\"#LKPemeriksaanlabdetM_nilairujukan_id\").val($data->nilairujukan_id);
                                                              \$(\"#nilairujukan\").val(\"$data->namapemeriksaandet\");
                                                              \$(\"#dialogNilairujukan\").dialog(\"close\");"
                                ))',
            ),
            'kelompokdet',
            'namapemeriksaandet',
            'kelompokumur',
            'nilairujukan_jeniskelamin',
        ),
));
$this->endWidget();
?>