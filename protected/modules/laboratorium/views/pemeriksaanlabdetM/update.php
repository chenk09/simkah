
<?php
$this->breadcrumbs=array(
	'Pemeriksaanlabdet Ms'=>array('index'),
	$model->pemeriksaanlabdet_id=>array('view','id'=>$model->pemeriksaanlabdet_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Pemeriksaan Hasil', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' PemeriksaanlabdetM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' PemeriksaanlabdetM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','View').' PemeriksaanlabdetM', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->pemeriksaanlabdet_id))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pemeriksaan Hasil', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial($this->pathView.'_formUpdateHasilpemeriksaan',array('model'=>$model,'modDetails'=>$modDetails)); ?>