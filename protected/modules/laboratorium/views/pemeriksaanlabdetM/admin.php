<style>
    #pemeriksaanlabdet-m-grid_c2
    {
        border-left:1px solid #DDDDDD;
    }
    .merge
    {
        border-right:1px solid #DDDDDD;
    }
</style>
<script>
    $('document').ready(function(){
        $('#colspan').parents('th').css('border-bottom','1px solid #DDDDDD');
    });
</script>
<?php
$this->breadcrumbs=array(
	'Pemeriksaanlabdet Ms'=>array('index'),
	'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pemeriksaan Hasil ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' PemeriksaanlabdetM', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Pemeriksaan Hasil', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
    $('#LKPemeriksaanLabM_pemeriksaanlab_nama').focus();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('pemeriksaanlabdet-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial($this->pathView.'_search',array(
	'model'=>$model,
    'modPemeriksaanLab'=>$modPemeriksaanLab,
)); ?>
</div><!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'pemeriksaanlabdet-m-grid',
	'dataProvider'=>$model->searchTable(),
	'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                // 'mergeHeaders'=>array(
                //     array(
                //         'name'=>'<center id="colspan">Nilai Rujukan</center>',
                //         'start'=>'2',
                //         'end'=>'6',
                //     ),
                // ),
//                'mergeColumns'=>array(
//                    'pemeriksaanlab.pemeriksaanlab_nama',
//                    'nilairujukan.kelompokdet'
//                ),
	'columns'=>array(
                array(
                    'name'=>'pemeriksaanlab_nama',
                    'header'=>'Pemeriksaan Lab',
                    'value'=>'$data->pemeriksaanlab->pemeriksaanlab_nama',
                    //'filter'=>'<input type="text" name="pemeriksaanlab_nama" attr-route ="'.$route.'" onblur="setFilter(this);">',
                ),
		//'pemeriksaanlabdet_nourut',
                array(
                    'name'=>'pemeriksaanlabdet_nourut',
                    'header'=>'No Urut',
                    'value'=>'$data->pemeriksaanlabdet_nourut',
                    'filter'=>false,
                ),
                array(
                    'name'=>'nilairujukan.kelompokdet',
                    'header'=>'Kelompok Nilai Rujukan',
                    'value'=>'$data->nilairujukan->kelompokdet',
                ),
                array(
                    'header'=>'Detail pemeriksaan',
                    'value'=>'$data->nilairujukan->namapemeriksaandet',
                ),
                array(
                    'header'=>'Kelompok umur',
                    'value'=>'$data->nilairujukan->kelompokumur',
                ),
                array(
                    'header'=>'Jenis kelamin',
                    'value'=>'$data->nilairujukan->nilairujukan_jeniskelamin',
                ),
                array(
                    'header'=>'Nilai rujukan',
                    'value'=>'$data->nilairujukan->nilairujukan_metode',
                    //'filter'=>'<input type="text" name="FilterForm[nilairujukan_metode]" attr-route ="'.$route.'" onblur="setFilter(this);">',
                ),
//		'nilairujukan_id',
//		'pemeriksaanlab_id',
		array(
                    'header'=>Yii::t('zii','View'),
                    'class'=>'bootstrap.widgets.BootButtonColumn',
                    'template'=>'{view}',
		),
		array(
                    'header'=>Yii::t('zii','Update'),
                    'class'=>'bootstrap.widgets.BootButtonColumn',
                    'template'=>'{update}',
                    'buttons'=>array(
                        'update' => array (
                        'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
                        ),
                     ),
		),
        array(
            'header'=>'Hapus',
            'type'=>'raw',
            'value'=>'CHtml::link("<i class=\'icon-trash\'></i> ", "javascript:deleteRecord($data->pemeriksaanlabdet_id)",array("id"=>"$data->pemeriksaanlabdet_id","rel"=>"tooltip","title"=>"Hapus"));',
            'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
        ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php 
 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp";
        $content = $this->renderPartial('laboratorium.views.tips.master2',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        $url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#pemeriksaanlabdet-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
?>
<script type="text/javascript">   
function setFilter(obj){
    url = $(obj).attr("attr-route");
    FilterForm = $(obj).val();
    FF = $(obj).val();
    $.get(url,{FilterForm:FilterForm,FF:FF},function(data){
        $('#pemeriksaanlabdet').html(data);
    });
}

function deleteRecord(id){
    var id = id;
    var url = '<?php echo $url."/delete"; ?>';
    var answer = confirm('Yakin Akan Menghapus Data ini ?');
        if (answer){
             $.post(url, {id: id},
                 function(data){
                    if(data.status == 'proses_form'){
                            $.fn.yiiGridView.update('sakabupaten-m-grid');
                        }else{
                            alert('Data gagal dihapus karena data digunakan oleh Hasil Pemeriksaan Lab.');
                        }
            },"json");
       }
}
</script>