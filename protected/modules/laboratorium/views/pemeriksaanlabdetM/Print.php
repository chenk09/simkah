<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

$table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchPrint();
         $template = "{pager}{summary}\n{items}";
    }
    
$this->widget($table,array(
    'id'=>'pemeriksaanlabdet-m-grid',
    'enableSorting'=>$sort,
    'dataProvider'=>$data,
    'template'=>$template,
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
                                array(
                                    'name'=>'pemeriksaanlab.pemeriksaanlab_nama',
                                    'value'=>'$data->pemeriksaanlab->pemeriksaanlab_nama'
                                ),
		'pemeriksaanlabdet_nourut',
		////'pemeriksaanlabdet_id',
                                array(
                                    'name'=>'nilairujukan.kelompokdet',
                                    'header'=>'Kelompok',
                                    'value'=>'$data->nilairujukan->kelompokdet',
                                ),
                                array(
                                    'header'=>'Detail pemeriksaan',
                                    'value'=>'$data->nilairujukan->namapemeriksaandet',
                                ),
                                array(
                                    'header'=>'Kelompok umur',
                                    'value'=>'$data->nilairujukan->kelompokumur',
                                ),
                                array(
                                    'header'=>'Jenis kelamin',
                                    'value'=>'$data->nilairujukan->nilairujukan_jeniskelamin',
                                ),
                                array(
                                    'header'=>'Nilai rujukan metode',
                                    'value'=>'$data->nilairujukan->nilairujukan_metode'
                                ),
//		'nilairujukan_id',
//		'pemeriksaanlab_id',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>