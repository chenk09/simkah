<?php
$this->breadcrumbs=array(
	'Pemeriksaanlabdet Ms'=>array('index'),
	$model->pemeriksaanlabdet_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Pemeriksaan Hasil', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' PemeriksaanlabdetM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' PemeriksaanlabdetM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' PemeriksaanlabdetM', 'icon'=>'pencil','url'=>array('update','id'=>$model->pemeriksaanlabdet_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' PemeriksaanlabdetM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->pemeriksaanlabdet_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pemeriksaan Hasil', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.MergeHeaderGroupGridView',array(
	'id'=>'pemeriksaanlabdet-m-grid',
	'dataProvider'=>$model->searchTable(),
	'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",
                'itemsCssClass'=>'table detail-view',
                'mergeHeaders'=>array(
                    array(
                        'name'=>'<center id="colspan">Nilai Rujukan</center>',
                        'start'=>'2',
                        'end'=>'6',
                    ),
                ),
                'mergeColumns'=>array(
                    'pemeriksaanlab.pemeriksaanlab_nama',
                    'nilairujukan.kelompokdet'
                ),
	'columns'=>array(
                                array(
                                    'name'=>'pemeriksaanlab.pemeriksaanlab_nama',
                                    'header'=>'Pemeriksaan Lab',
                                    'value'=>'$data->pemeriksaanlab->pemeriksaanlab_nama'
                                ),
		'pemeriksaanlabdet_nourut',
                                array(
                                    'name'=>'nilairujukan.kelompokdet',
                                    'header'=>'Kelompok',
                                    'value'=>'$data->nilairujukan->kelompokdet',
                                ),
                                array(
                                    'header'=>'Detail pemeriksaan',
                                    'value'=>'$data->nilairujukan->namapemeriksaandet',
                                ),
                                array(
                                    'header'=>'Kelompok umur',
                                    'value'=>'$data->nilairujukan->kelompokumur',
                                ),
                                array(
                                    'header'=>'Jenis kelamin',
                                    'value'=>'$data->nilairujukan->nilairujukan_jeniskelamin',
                                ),
                                array(
                                    'header'=>'Nilai rujukan',
                                    'value'=>'$data->nilairujukan->nilairujukan_metode'
                                ),
//		'nilairujukan_id',
//		'pemeriksaanlab_id',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>
<script>
    $('document').ready(function(){
        $('.detail-view').children('thead').children('tr').children('th').attr('style','background:#FFFFFF;');
        $('.merge').attr('style','border-right:1px solid #DDDDDD');
        $('#pemeriksaanlabdet-m-grid').removeAttr('class');
        $('.detail-view').attr('style','border-top:1px solid #DDDDDD');
    });
</script>