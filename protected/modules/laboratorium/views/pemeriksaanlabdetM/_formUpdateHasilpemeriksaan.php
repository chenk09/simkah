
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pemeriksaanlabdet-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#pemeriksaanlab',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
                <div class="control-group">
                        <?php echo CHtml::label('Pemeriksaan lab','pemeriksaanlab',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php
                                echo CHtml::hiddenField('pemeriksaanlab_id',$model->pemeriksaanlab_id,array('class'=>'span3','readonly'=>3,));
                                $this->widget('MyJuiAutoComplete', array(
                                                'name'=>'pemeriksaanlab',
                                                'value'=>$model->pemeriksaanlab->pemeriksaanlab_nama,
                                                'source'=>'js: function(request, response) {
                                                               $.ajax({
                                                                   url: "'.Yii::app()->createUrl('ActionAutoComplete/Pemeriksaanlab').'",
                                                                   dataType: "json",
                                                                   data: {
                                                                       term: request.term,
                                                                   },
                                                                   success: function (data) {
                                                                           response(data);
                                                                   }
                                                               })
                                                            }',
                                                 'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'focus'=> 'js:function( event, ui ) {
                                                            $(this).val( ui.item.label);
                                                            return false;
                                                        }',
                                                       'select'=>'js:function( event, ui ) { 
                                                            $("#pemeriksaanlab_id").val(ui.item.pemeriksaanlab_id);
                                                            $("#daftartindakan_nama").val(ui.item.daftartindakan_nama);
                                                            validasi();
                                                            return false;
                                                        }',
                                                ),
                                                'htmlOptions'=>array(
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                ),
                                                'tombolDialog'=>array('idDialog'=>'dialogPemeriksaanlab'),
                                            )); 
                         ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo CHtml::label('Nilai rujukan','nilairujukan',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php 
                                echo CHtml::hiddenField('nilairujukan_id','',array('class'=>'span3','readonly'=>3));
                                $this->widget('MyJuiAutoComplete', array(
                                                'name'=>'nilairujukan',
                                                'source'=>'js: function(request, response) {
                                                               $.ajax({
                                                                   url: "'.Yii::app()->createUrl('ActionAutoComplete/Nilairujukan').'",
                                                                   dataType: "json",
                                                                   data: {
                                                                       term: request.term,
                                                                   },
                                                                   success: function (data) {
                                                                           response(data);
                                                                   }
                                                               })
                                                            }',
                                                 'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'focus'=> 'js:function( event, ui ) {
                                                            $(this).val( ui.item.label);
                                                            return false;
                                                        }',
                                                       'select'=>'js:function( event, ui ) { 
                                                            $("#nilairujukan_id").val(ui.item.nilairujukan_id);
                                                            $("#nilairujukan").val("");
                                                            validasi();
                                                            submitPemeriksaanhasil();
                                                            return false;
                                                        }',
                                                ),
                                                'htmlOptions'=>array(
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                ),
                                                'tombolDialog'=>array('idDialog'=>'dialogNilairujukan'),
                                            )); 
                         ?>
                                         <?php 
//                                                echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
//                                                        array(
//                                                                'onclick'=>'submitPemeriksaanhasil();
//                                                                                  return false;',
//                                                                'class'=>'btn btn-primary',
//                                                                'onkeypress'=>"\$(\"#nilairujukan_id\").val();
//                                                                                          \$(\"#nilairujukan\").val();
//                                                                                            submitPemeriksaanhasil();",
//                                                                'rel'=>"tooltip",
//                                                                'id'=>'tambahPemeriksaanhasil',  
//                                                                'title'=>"Klik Untuk Hasil Pemeriksaan",
//                                                                )
//                                                        );
                                         ?>
                    </div>
                </div>
                <table class="table table-bordered table-striped table-condensed" id="tabelPemeriksaanhasil">
                    <thead>
                        <th>No Urut</th>
                        <th>Kelompok</th>
                        <th>Nama Pemeriksaan</th>
                        <th>Kelompok Umur</th>
                        <th>Jenis Kelamin</th>
                        <th>Nilai rujukan</th>
                        <th><?php if ($model->isNewRecord) {echo 'Batal';} else {echo 'Hapus';} ?></th>
                    </thead>
                    <tbody>
                <?php
                    if (COUNT($modDetails) > 0) {
                        foreach ($modDetails as $i=>$row) {
                            $modNilairujukan = NilairujukanM::model()->findByPK($row->nilairujukan_id);
                            $urlDelete = Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/DeleteFromUpdate',array('link'=>$model->pemeriksaanlabdet_id, 'id'=>$row->pemeriksaanlabdet_id));
                            $tr = '<tr>';
                                $tr .=
                                        '<td>'
                                         .CHtml::activeTextField($model,'['.$i.']pemeriksaanlabdet_nourut',array('class'=>'span1','value'=>$row->pemeriksaanlabdet_nourut))
                                         .CHtml::activeHiddenField($model,'['.$i.']nilairujukan_id',array('class'=>'span1','readonly'=>true,'value'=>$row->nilairujukan_id))
                                         .'</td>';
                                $tr .= '<td>'.$modNilairujukan->kelompokdet.'</td>';
                                $tr .= '<td>'.$modNilairujukan->namapemeriksaandet.'</td>';
                                $tr .= '<td>'.$modNilairujukan->kelompokumur.'</td>';
                                $tr .= '<td>'.$modNilairujukan->nilairujukan_jeniskelamin.'</td>';
                                $tr .= '<td>'.$modNilairujukan->nilairujukan_metode.'</td>';
                                if ($model->isNewRecord) {
                                    $tr .= '<td>'.CHtml::link('<i class="icon-remove"></i>','',array('id'=>'removebtn','style'=>'cursor:pointer;','class'=>'pemeriksaanlabdet','onclick'=>'remove(this)')).'</td>';
                                } else {
                                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('style'=>'cursor:pointer;')).'</td>';
                                }
                            $tr .= '</tr>';
                                echo $tr;
                        }
                    }
                        ?>
                    </tbody>
                </table>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl('laboratorium.pemeriksaanlabdetM.admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
                        $content = $this->renderPartial('laboratorium.views.tips.informasi',array(),true);
                        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>
	</div>

<?php $this->endWidget(); ?>
<!-- ===================================== Widget Pemeriksaan Lab =============================== -->
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'dialogPemeriksaanlab',
        'options'=>array(
            'title'=>'Pencarian Pemeriksaan Lab',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>600,
            'resizable'=>false,
        ),
    ));

    $modPemeriksaanlab = new PemeriksaanlabM;
    $modPemeriksaanlab->unsetAttributes();
    if (isset($_GET['PemeriksaanlabM'])) {
        $modPemeriksaanlab->attributes = $_GET['PemeriksaanlabM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'pemeriksaanlab-grid',
        'dataProvider'=>$modPemeriksaanlab->search(),
        'filter'=>$modPemeriksaanlab,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-striped table-condensed',
        'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                array(
                                        "class"=>"btn-small",
                                        "id" => "selectPemeriksaanlab",
                                        "onClick" => "\$(\"#pemeriksaanlab_id\").val($data->pemeriksaanlab_id);
                                                              \$(\"#pemeriksaanlab\").val(\"$data->pemeriksaanlab_nama\");
                                                              \$(\"#dialogPemeriksaanlab\").dialog(\"close\");
                                                              validasi();"
                                ))',
            ),
            'pemeriksaanlab_kode',
            'pemeriksaanlab_nama',
            'pemeriksaanlab_namalainnya',
        ),
));
$this->endWidget();
?>
<!-- ===================================== Widget Nilairujukan==== =============================== -->
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'dialogNilairujukan',
        'options'=>array(
            'title'=>'Pencarian Nilai Rujukan',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>600,
            'resizable'=>false,
        ),
    ));

    $modNilairujukan = new NilairujukanM;
    $modNilairujukan->unsetAttributes();
    if (isset($_GET['NilairujukanM'])) {
        $modNilairujukan->attributes = $_GET['NilairujukanM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'nilairujukan-grid',
        'dataProvider'=>$modNilairujukan->search(),
        'filter'=>$modNilairujukan,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-striped table-condensed',
        'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                array(
                                        "class"=>"btn-small",
                                        "id" => "selectNilaiRujukan",
                                        "onClick" => "\$(\"#nilairujukan_id\").val($data->nilairujukan_id);
                                                              \$(\"#nilairujukan\").val(\"$data->namapemeriksaandet\");
                                                              \$(\"#dialogNilairujukan\").dialog(\"close\");
                                                              submitPemeriksaanhasil();"
                                ))',
            ),
            'kelompokdet',
            'namapemeriksaandet',
            'kelompokumur',
            'nilairujukan_jeniskelamin',
        ),
));
$this->endWidget();
?>
<?php
$urlGetPemeriksaanhasil = Yii::app()->createUrl('actionAjax/Pemeriksaanhasil');
$jscript = <<< JS
    $('document').ready(function(){
        validasi();
    });
    function validasi() {
        pemeriksaanlab_id = $("#pemeriksaanlab_id").val();
        nilairujukan_id = $("#nilairujukan_id").val();
        if (pemeriksaanlab_id=="") {
            $("#pemeriksaanlab").removeAttr("readonly");
            $("#nilairujukan").attr("readonly","readonly");
        } else {
            $("#pemeriksaanlab").attr("readonly","readonly");
            $("#nilairujukan").removeAttr("readonly");
            $("#pemeriksaanlab").parents("div").children(".input-append").children(".add-on").children("a").attr("onclick","disablepemeriksaanlab()");
        }
    }

    function disablepemeriksaanlab() {
        alert("pemeriksaan lab sudah terpilih");
    }

    function submitPemeriksaanhasil() {
        pemeriksaanlab_id = $("#pemeriksaanlab_id").val();
        nilairujukan_id = $("#nilairujukan_id").val();
        
        if (nilairujukan_id=="") {
            alert("Silahkan pilih nilai rujukan terlebih dahulu");
        } else {
            $("#nilairujukan").val('');
            $.post("${urlGetPemeriksaanhasil}", {pemeriksaanlab_id:pemeriksaanlab_id, nilairujukan_id:nilairujukan_id},
            function(data) {
                $('#tabelPemeriksaanhasil tbody').append(data.tr);
                renameInput();
            }, "json");
        }
    }
    
    function renameInput(){
        $('#tabelPemeriksaanhasil').children('tbody').children('tr:last').find('[id*="REPemeriksaanlabdetM"]').each(function(){
        inputterakhir = $(this).attr('id');
        var dataterakhir = inputterakhir.split('_');
        var nourut = dataterakhir[1];
        nourutinput = 1;
        });
        $('.pemeriksaanlabdet').each(function(){
            $(this).parents('tr').find('[name*="PemeriksaanlabdetM"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('PemeriksaanlabdetM[]');
                var id = $(this).attr('id');
                var dataid = id.split('PemeriksaanlabdetM_');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','LKPemeriksaanlabdetM['+nourut+']'+data[1]);
                    $(this).attr('id','LKPemeriksaanlabdetM_'+nourut+'_'+dataid[1]);
                    $('#LKPemeriksaanlabdetM_'+nourut+'_'+dataid[1]).val(nourutinput);
                }
            });
            nourut++;
            nourutinput++;
        });
    }
JS;
Yii::app()->clientScript->registerScript('pemeriksaanhasil',$jscript, CClientScript::POS_HEAD);
?>

<script type="text/javascript">
  function hapusBaris(obj)
    {
      $(obj).parent().parent('tr').detach();
    }
</script>