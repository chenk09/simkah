<legend class='rim'>Pencarian</legend>
<?php 
    $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search',
        'type'=>'horizontal',
    ));     
?>
<table>
    <tr>
        <td>
	<!-- <div class="control-group ">
                <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php   
                            $this->widget('MyDateTimePicker',array(
                                            'model'=>$model,
                                            'attribute'=>'tglAwal',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                'maxDate' => 'd',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                    )); 
                        ?>
                </div>
            </div> -->
	<!-- <div class="control-group ">
                <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php 
                        $this->widget('MyDateTimePicker',array(
                                            'model'=>$model,
                                            'attribute'=>'tglAkhir',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                'maxDate' => 'd',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                    )); ?>    
                </div>
            </div> -->
            <?php //echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3', 'maxlength'=>20)); ?>
            
</td>
<td>

    <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3', 'maxlength'=>50)); ?>
    <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3', 'maxlength'=>50)); ?>
    <?php //echo $form->dropDownListRow($model,'statusBayar', StatusBayar::items(), array('empty'=>'-- Pilih --', 'class'=>'span3', 'maxlength'=>20)); ?>
</td>
</tr>
</table>
	<div class="form-actions">
              <?php 
                    echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                      array('class'=>'btn btn-primary', 'type'=>'submit')); 
              ?>
              <?php 
                    echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                        array('class'=>'btn btn-danger', 'type'=>'reset')); 
              ?>   
              <?php 
                    $content = $this->renderPartial('laboratorium.views.tips.informasi',array(),true);
                    $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                ?>
	</div>
<?php $this->endWidget(); ?>
