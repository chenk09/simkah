<legend class="rim2">Informasi Riwayat Pasien Laboratorium</legend>
<?php

    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('#search').submit(function(){
            $.fn.yiiGridView.update('informasiriwayatpasien-v-grid', {
                    data: $(this).serialize()
            });
            return false;
    });
    ");

$this->widget('bootstrap.widgets.BootAlert'); 
?>

<?php 
    $module  = $this->module->name; 
    $controller = $this->id;
?>
<?php $this->widget('bootstrap.widgets.BootAlert');	?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'informasiriwayatpasien-v-grid',
	'dataProvider'=>$model->searchRiwayatPemeriksaan(),
//	'filter'=>$model,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
        array(
            'header' => 'No. Urut',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
        ),
        array(
            'header'=>'No Rekam Medik',
            'type'=>'raw',
            'value'=>'$data->no_rekam_medik',
        ),
    
        array(
            'header'=>'Nama Pasien',
            'type'=>'raw',
            'value'=>'$data->nama_pasien',
        ),

        array(
            'header'=>'Tanggal Lahir',
            'type'=>'raw',
            'value'=>'$data->tanggal_lahir',
        ),
            
        array(
            'header'=>'Alamat',
            'type'=>'raw',
            'value'=>'$data->alamat_pasien',
        ),
                      
        array(
            'header'=>'Riwayat <br/> Pemeriksaan',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("informasiRiwayatPasien/rincian",array("id"=>$data->pasien_id,"frame"=>true)),
                    array("class"=>"", 
                          "target"=>"iframeRiwayatPasien",
                          "onclick"=>"$(\"#dialogRiwayat\").dialog(\"open\");",
                          "rel"=>"tooltip",
                          "title"=>"Klik untuk melihat riwayat pemeriksaan",
                    ))',  'htmlOptions'=>array('style'=>'text-align: center; width:40px')
        ),
            	
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<div class="search-form">
    <?php 
        $this->renderPartial('laboratorium.views.informasiRiwayatPasien._search',array(
            'model'=>$model,
        ));
    ?>
</div>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
        'id' => 'dialogRiwayat',
        'options' => array(
            'title' => 'Informasi Riwayat Pemeriksaan Pasien',
            'autoOpen' => false,
            'modal' => true,
            'width' => 900,
            'height' => 550,
            'resizable' => false,
        ),
    ));
?>
<iframe name='iframeRiwayatPasien' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>