<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-mp-form',
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));
//
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php 
if(empty($idPasienAdmisi))
    $this->renderPartial('/_ringkasDataPasienPendaftaran',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));
else
    $this->renderPartial('/_ringkasDataPasienPendaftaranRI',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));
?>
<table>
        <tr>
            <td style="background-color: #E5ECF9;">
                <div class="" id="formPeriksaLab">
                        <?php foreach($modJenisPeriksaLab as $i=>$jenisPeriksa){ 
                                $ceklist = false;
                        ?>
                                <div class="boxtindakan">
                                    <h6><?php echo $jenisPeriksa->jenispemeriksaanlab_nama; ?></h6>
                                    <?php foreach ($modPeriksaLab as $j => $pemeriksaan) {
                                             if($jenisPeriksa->jenispemeriksaanlab_id == $pemeriksaan->jenispemeriksaanlab_id) {
                                                 echo '<label class="checkbox inline">'.CHtml::checkBox("pemeriksaanLab[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanlab_id,
                                                                                                          'onclick' => "inputperiksa(this)"));
                                                 echo "<span>".$pemeriksaan->pemeriksaanlab_nama."</span></label><br/>";
                                             }
                                         } ?>
                                </div>
                        <?php } ?>
                </div>
            </td>
        </tr>
        <tr>
        <td>
<!--            <div class="control-group">
                <?php // echo CHtml::label('Dokter Penanggungjawab','Dokter Penanggungjawab',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
//                        echo CHtml::dropDownList('PendaftaranT[pegawai_id]',$modPendaftaran->pegawai_id,CHtml::listData(DokterV::model()->findAllByAttributes(),'pegawai_id','NamaLengkap'),array('disabled'=>'disabled','empty'=>'--Pilih--','class'=>'span3'));
                    ?>
                </div>
            </div>-->
            <div class="control-group">
                <?php echo CHtml::label('Dokter Pemeriksa','Dokter Pemeriksa',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                        $modPasienMasukPenunjang->pegawai_id = Params::DEFAULT_DOKTER_LAB;
                        echo CHtml::dropDownList('LKPasienMasukPenunjang[pegawai_id]',$modPasienMasukPenunjang->pegawai_id,CHtml::listData(DokterV::model()->findAllByAttributes(array('ruangan_id'=>Yii::app()->user->getState('ruangan_id')), array('order'=>'nama_pegawai')),'pegawai_id','NamaLengkap'),array('empty'=>'--Pilih--','class'=>'span3'));
                    ?>
                </div>
            </div>
        </td>
    </tr>
</table>

<fieldset>
    <legend class="rim">Pencarian Daftar Pemeriksaan Lab</legend>
   <table class="table table-condensed">
        <tr>
            <tr>
               <td> Kategori Pencarian : 
                  <?php 
                        echo CHtml::dropDownList('Pilihan Pencarian','daftarTindakanNama',(Params::DaftarTindakan()),array('id'=>'daftarTindakan','empty'=>'-- Pilih --','style'=>'width:140px;','onChange'=>'cekOptions()'));
                  ?>
                </td>
            <td>
                <div style="margin-left:-50px;margin-right:400px;" id="nama">
                    
                <?php echo CHtml::hiddenField('idPemeriksaan'); ?>
                    <?php
//                            
                    $this->widget('MyJuiAutoComplete', array(
                                        'name' => 'daftartindakan_nama',
                                        'sourceUrl' => Yii::app()->createUrl('laboratorium/ActionAutoComplete/getNamaDaftarTindakan'),
                                        'options' => array(
                                            'showAnim' => 'fold',
                                            'minLength' => 2,
                                            'focus' => 'js:function( event, ui ) {
                                                                            $(this).val(ui.item.label);
                                                                            return false;
                                                                        }',
                                            'select' => 'js:function( event, ui ) {
                                                                                  $("#idPemeriksaanlab").val(ui.item.pemeriksaanlab_id);
                                                                                  return false;
                                                                        }',
                                        ),
                                        'htmlOptions' => array(
                                            'class' => 'span2',
                                            'onkeypress' =>"if (event.keyCode == 13){inputperiksaTindakan();}return $(this).focusNextInputField(event)",
                                            'onClick'=>'cekOptions()',
                                        ),
                                        'tombolDialog' => array('idDialog' => 'dialogAddTindakan'),
                                    ));
                  
                            ?>
                </div>
                <div style="margin-left:-50px;margin-right:400px;" id="kode">
                    <?php 
                        $this->widget('MyJuiAutoComplete', array(
                                        'name' => 'pemeriksaanlab_kode',
                                        'sourceUrl' => Yii::app()->createUrl('laboratorium/ActionAutoComplete/getKodeDaftarTindakan'),
                                        'options' => array(
                                            'showAnim' => 'fold',
                                            'minLength' => 2,
                                            'focus' => 'js:function( event, ui ) {
                                                                            $(this).val(ui.item.label);
                                                                            return false;
                                                                        }',
                                            'select' => 'js:function( event, ui ) {
                                                                                  $("#idPemeriksaanlab").val(ui.item.pemeriksaanlab_id);
                                                                                  return false;
                                                                        }',
                                        ),
                                        'htmlOptions' => array(
                                            'class' => 'span2',
                                            'onkeypress' =>"if (event.keyCode == 13){inputperiksaTindakan();}return $(this).focusNextInputField(event)",
                                            'onClick'=>'cekOptions()',
                                        ),
                                        'tombolDialog' => array('idDialog' => 'dialogAddTindakan'),
                                    ));
                    ?>
                </div>
            </td>
        </tr>
    </table>
<?php echo CHtml::hiddenField('idPemeriksaanlab', '',array('readonly'=>true)); ?>
<?php echo CHtml::hiddenField('idPemeriksaanlab2', '',array('readonly'=>true)); ?>
<div>
    <?php 
//            echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
//                            array('class'=>'btn btn-primary','onclick'=>"$('#dialogAddTindakan').dialog('open');",
//                                  'id'=>'btnAddTindakan','onkeypress'=>"return $(this).focusNextInputField(event)",
//                                  'rel'=>'tooltip','title'=>'Klik Untuk Menambah Tindakan')); 
    ?>
</div>
<table id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>Pilih</th>
            <th>Jenis Pemeriksaan/<br/>Pemeriksaan</th>
            <th>Tarif</th>
            <th>Qty</th>
            <th>Satuan</th>
            <th>Cyto</th>
            <th>Tarif Cyto</th>
        </tr>
    </thead>
</table>
<table class="table table-bordered table-condensed">
	<tr><td width="70%" style="text-align: right;">Biaya Administrasi</td><td><?php echo CHtml::textField('biayaAdministrasi', 0,array('class'=>'span2', 'style'=>'text-align:right;', 'onkeyup'=>'hitungTotal();'));?></td></tr>
    <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan</td><td><?php echo CHtml::textField('periksaTotal', '',array('class'=>'span2', 'style'=>'text-align:right;', 'disabled'=>'disabled'));?></td></tr>
	<tr>
        <td colspan="2">
            <div class="control-group ">

                <label class="control-label required" for="PasienkirimkeunitlainT_pegawai_id">
                Dokter Perujuk :
                </label>
                <div class="controls ">
					<?php echo $form->dropDownList($modRiwayatKirimKeUnitLain,'pegawai_id', CHtml::listData($modRiwayatKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'), array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div> 
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php //echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
    //                                             array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan','onKeypress'=>'setVerifikasi();return false;','onClick'=>'setVerifikasi();return false;'));
 //echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),             array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); 

            echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
             array('class'=>'btn btn-primary', 'type'=>'submit',
             'onKeypress'=>'setVerifikasi();return false;','onClick'=>'setVerifikasi();return false;'));?>


  
    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/PencarianPasien/index&modulId=8'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
</div>    
 
<?php $this->endWidget();?>
    
    
<script type="text/javascript">
$('#formPeriksaLab').tile({widths : [ 190 ]});
    
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanlab = obj.value;
        var idPemeriksaanlab2 = $('#idPemeriksaanlab2').val();
        var kelasPelayan_id = <?php echo $modPendaftaran->kelaspelayanan_id;?>;
        
        if(kelasPelayan_id != <?php echo PARAMS::KELASPELAYANAN_ID_TANPA_KELAS; ?>){
            kelasPelayan_id = <?php echo PARAMS::KELASPELAYANAN_ID_TANPA_KELAS; ?>;
        }
//        var idPemeriksaanlab2 = $('#idPemeriksaanlab2').val();
        
//        $('#idPemeriksaanlab2').val(idPemeriksaanlab);
             
        if(kelasPelayan_id == ''){
             alert('Isi Terlebih Dahulu Daftar Pasien');
             
             setTimeout(function(){
                     $(obj).removeAttr('checked', 'checked');
             }, 500);
        }else{
            setTimeout(function(){
                 if(idPemeriksaanlab == idPemeriksaanlab2){
                  alert('Tindakan Pemeriksaan telah ada di List');
                     setTimeout(function(){
                            $(obj).removeAttr('checked', 'checked');
//                            clear();
                      }, 500);
             }else{
                jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanLabPendLab')?>',
                         'data':{idPemeriksaanlab:idPemeriksaanlab,kelasPelayan_id:kelasPelayan_id},
                         'type':'post',
                         'dataType':'json',
                         'success':function(data) {
                                 if($.trim(data.form)=='')
                                 {
                                    $(obj).removeAttr('checked');
                                    alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                                 } else if($.trim(data.form)=='tarif kosong') {
                                    $(obj).removeAttr('checked');
                                    data.form = '';
                                    alert ('Pemeriksaan belum memiliki tarif');
                                 }
                                 $('#tblFormPemeriksaanLab').append(data.form);
                         } ,
                         'cache':false});
             }
             });
             setTimeout(function(){
                    hitungTotal();
              }, 500);
             
        }
        
    }else{
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
            hitungTotal();
        }else
            $(obj).attr('checked', 'checked');
    }
}
function formSubmit(obj,evt)
    {
         evt = (evt) ? evt : event;
         var form_id = $(obj).closest('form').attr('id');
         var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
         if(charCode == 13){
            setVerifikasi();
         }
    }


function setVerifikasi()

{

   var namaPerujuk = $('#LKPasienKirimKeUnitLainT_pegawai_id').val();
    if(namaPerujuk != '')
    {
      submit();
     }
    else{
       alert('Silahkan isi Nama Perujuk terlebih dahulu');

  }
     

}



function inputperiksaTindakan()
{
        var idPemeriksaanLab = $('#idPemeriksaanlab').val();
        var idPemeriksaanLab2 = $('#idPemeriksaanlab2').val();
        var idKelasPelayanan = <?php echo $modPendaftaran->kelaspelayanan_id;?>
        
//     if (cekList(idPemeriksaanLab) == true){  
         if(idKelasPelayanan == ''){
             alert('Isi Terlebih Dahulu Daftar Pasien');
             
             setTimeout(function(){
                     $("#daftartindakan_nama").val("");
                     $("#daftarTindakan").val("");
                     clear();
             }, 500);
        }else if(idPemeriksaanLab == ''){
            alert('Isi Tindakan Terlebih Dahulu');
        
        }else{
            if(cekList(idPemeriksaanLab) == true){
                        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanLabPendLab')?>',
                     'data':{idPemeriksaanlab:idPemeriksaanLab, kelasPelayan_id:idKelasPelayanan},
                     'type':'post',
                     'dataType':'json',
                     'success':function(data) {
                             if($.trim(data.form)=='')
                             {
                                $(this).removeAttr('checked');
                                alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                             } else if($.trim(data.form)=='tarif kosong') {
                                $(this).removeAttr('checked');
                                data.form = '';
                                alert ('Pemeriksaan belum memiliki tarif');
                             }
                             $('#tblFormPemeriksaanLab').append(data.form);                            
                             
                     } ,
                     'cache':false});
                 }
                setTimeout(function(){
                        clear();
                        hitungTotal();
                }, 500);
             }
//        }
}
function batalPeriksa(idPemeriksaanlab)
{
    $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).detach();
}

function cekcyto(test, x){
    if(test.value == 1){
        $('.cyto_'+ x).show();
    }else{
        $('.cyto_'+ x).hide();
    }
}


function cekOptions(params){
    idOptions = $('#daftarTindakan').val();
    
    if(idOptions == ''){
        alert('Harap Memilih Kategori Pencarian Terlebih Dahulu')
    }
    
    $("#nama").show();
    $("#kode").hide();
    if(idOptions== 'nama'){
        $("#nama").show();
        $("#kode").hide();
    }else if(idOptions == 'kode'){
        $("#nama").hide();
        $("#kode").show(); 
    }  
    
}

function cekList(id){
    x = true;
    $('.pemeriksaanlab_id').each(function(){
        if ($(this).val() == id){
            alert('Daftar Pemeriksaan Tindakan telah ada di List');
            x = false;
        }
    });
    return x;
}
    
function clear(){
idOptions = $('#daftarTindakan').val();
   
    if(idOptions == 'nama'){
            $('#nama').val("");
            $('#daftarTindakan').val("");
            $('#pemeriksaanlab_kode').val("");
    }else if(idOptions == 'kode'){
            $('#pemeriksaanlab_kode').val("");
            $('#daftarTindakan').val("");
        
    }

}
function hitungCyto(obj){
    persen = unformatNumber($(obj).parent().parent().find('.persenCyto').val());
    tarif = unformatNumber($(obj).parent().parent().find('.tarif').val());
    qty = unformatNumber($(obj).parent().parent().find('.qty').val());
    isCyto = unformatNumber($(obj).parent().parent().find('.isCyto').val());
    cyto = (tarif * qty)*persen/100;
//        alert(cyto);
    $(obj).parents('tr').find('.cyto').val(cyto);
    if(isCyto == 0)
        $(obj).parents('tr').find('.cyto').val(0);
}
function hitungTotal(){
    var total = 0;
    $('.tarif').each(
        function(){
            qty = $(this).parent().parent().find('.qty').val();
            isCyto = $(this).parent().parent().find('.isCyto').val();
            if(isCyto == 1){
                cyto = $(this).parent().parent().find('.cyto').val();
            }else{
                cyto = 0;
            }
            
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );
    
    total += unformatNumber($('#biayaAdministrasi').val());
    $('#periksaTotal').val(formatNumber(total));    
}
</script>
    

<?php

//========= Dialog daftar tindakan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogAddTindakan',
    'options'=>array(
        'title'=>'Daftar Tindakan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));


$modDataTindakan = new PemeriksaanlabM('search');
    $modDataTindakan->unsetAttributes();
    if(isset($_GET['PemeriksaanlabM'])) {
        $modDataTindakan->attributes = $_GET['PemeriksaanlabM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'pemeriksaanlab-m-grid',
            //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
            'dataProvider'=>$modDataTindakan->search(),
            'filter'=>$modDataTindakan,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPasien",
                                        "onClick" => "
                                            $(\"#dialogAddTindakan\").dialog(\"close\");
//                                           $(\"#dialogTindakan\").dialog(\"close\");
                                            $(\"#idPemeriksaanlab\").val(\"$data->pemeriksaanlab_id\");
                                            inputperiksaTindakan();
                                            clear();
                                        "))',
                    ),
                    array(
                        'header'=>'Jenis Pemeriksaan Lab',
                        'name'=>'jenispemeriksaanlab_nama',
                        'value'=>'$data->jenispemeriksaan->jenispemeriksaanlab_nama',
                    ),
                    array(
                        'header'=>'Kode Pemeriksaan',
                        'name'=>'pemeriksaanlab_kode',
                        'value'=>'$data->pemeriksaanlab_kode',
                    ),
                    array(
                        'header'=>'Nama Pemeriksaan',
                        'name'=>'pemeriksaanlab_nama',
                        'value'=>'$data->pemeriksaanlab_nama',
                    ),
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));


$this->endWidget();
//========= end pasien dialog =============================
    
?>
<?php
$js= <<< JS
    $(document).ready(function() {
        $("#daftarTindakan").children("li").children("a").click(function() {
            $("#daftarTindakan").children("li").attr('class','');
        });
        
        $("#nama").show();
        $("#kode").hide();
    });

    
function onReset()
{
    setTimeout(
        function(){
            $.fn.yiiGridView.update('masukpenunjang-Laboratorium-form', {
                data: $("#namatindakan").serialize()
            });
            $.fn.yiiGridView.update('masukpenunjang-Laboratorium-form', {
                data: $("#kodetindakan").serialize()
            });      
        }, 2000
    );
    return false;
}   
JS;
Yii::app()->clientScript->registerScript('pencatatanriwayat',$js,CClientScript::POS_HEAD);
?>