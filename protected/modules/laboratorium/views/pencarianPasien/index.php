<?php
//$arrMenu = array();
   //             array_push($arrMenu,array('label'=>Yii::t('mds','Search Patient'), 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//$this->menu=$arrMenu;
//$this->widget('bootstrap.widgets.BootAlert');
?>
<?php 
    if($_GET['status'] > 0){ // Jika berhasil disimpan
        Yii::app()->user->setFlash('success',"Data pemeriksaan lab berhasil disimpan !");
    }
?>
<?php
//============= PRINT LABEL ==============
if(isset($_GET['caraPrint'])){
$idPendaftaran = $_GET['id'];
$idPasienpenunjang = $_GET['idPasienpenunjang'];
$urlPrint=  Yii::app()->createAbsoluteUrl('laboratorium/pendaftaranPasienLuar/print', array('id_pendaftaran'=>$idPendaftaran, 'id_pasienpenunjang'=>$idPasienpenunjang, 'labelOnly'=>1));
$js = <<< JSCRIPT
function printLabel(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
    printLabel('PRINT');
JSCRIPT;
Yii::app()->clientScript->registerScript('printLabel',$js,CClientScript::POS_HEAD);     
}
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?> 
<fieldset>
    <legend class="rim2">Informasi Pasien Kunjungan</legend>
	 <legend class="rim">Asal Instalasi</legend>
    <table class="table-condensed">
    <tr>
        <td><?php echo CHtml::radioButton('instalasi', $cekRJ, array('value'=>'RJ','onClick'=>'submitForm(this)','class'=>'ceklis')) ?> Rawat Jalan</td>
        <td><?php echo CHtml::radioButton('instalasi', $cekRI, array('value'=>'RI','onClick'=>'submitForm(this)','class'=>'ceklis')) ?> Rawat Inap</td>
        <td><?php echo CHtml::radioButton('instalasi', $cekRD, array('value'=>'RD','onClick'=>'submitForm(this)','class'=>'ceklis')) ?> Rawat Darurat</td>
    </tr>
</table>
</fieldset>
<?php 
     if($cekRJ == TRUE)
     {
       echo $this->renderPartial('_formCariRJ', array('model'=>$modRJ,'form'=>$form),true); 
     }
     elseif($cekRI == TRUE)
     {
       
       echo $this->renderPartial('_formCariRI', array('model'=>$modRI,'form'=>$form),true); 
     }
     elseif($cekRD == TRUE)
     {
       echo $this->renderPartial('_formCariRD', array('model'=>$modRD,'form'=>$form),true); 
     }
?>


      
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
			<?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>

    </div>



<?php $this->endWidget(); ?>

<?php
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);
$cetak = Yii::app()->createUrl('pendaftaranPenjadwalan/pencarianPasien/printKartu',array('id'=>''));
$js = <<< JSCRIPT
function submitForm(obj)
{
    $('#patokanInstalasi').val(obj.value);
    $(obj).closest('form').submit();
}
// ==* Fungsi Print *== //

function print(id,umur)
   {    
               window.open('${url}/printKartu/id/'+id+'/umur/'+umur,'printwin','left=100,top=100,width=355,height=255,scrollbars=0');
   }

JSCRIPT;

Yii::app()->clientScript->registerScript('jsPencarianPasien',$js, CClientScript::POS_HEAD);
?>

<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
    $urlDaftarKunjungan=Yii::app()->createAbsoluteUrl($module.'/pendaftaranPasienKunjungan/index');
$js = <<< JSCRIPT

function daftarPasienKunjungan(pendaftaran_id)
{
    $('#pendaftaran_id').val(pendaftaran_id);
    $('#form_hidden').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
<?php $forms=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hidden',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'action'=>$urlDaftarKunjungan,
        'htmlOptions'=>array('target'=>'_new'),
)); ?>
<?php echo CHtml::hiddenField('pendaftaran_id');?>
<?php $this->endWidget(); ?>

<script type="text/javascript">
{
   function batalperiksa(idPendaftaran)
   {
       var con = confirm('anda yakin akan membatalkan pemeriksaan laboratorium pasien ini? ');
       if(con){
           $.post('<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/' . 'batalPemeriksaan')?>',{idPendaftaran:idPendaftaran},
                     function(data){
                         if(data.status == 'ok'){
                             window.location = "<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/index&succes=2')?>";
                         }else{
                             if(data.status == 'exist')
                             {
                                 alert('Pasien telah melakukan pemeriksaan');
                             }

                         }
                     },'json'
                 );
       }         
   }

}

function statusPeriksa(statusperiksa,pendaftaran_id,pasienmasukpenunjang_id){
//    alert(status);
    var statusperiksa = statusperiksa;
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = '';
    
    $.post("<?php echo Yii::app()->createUrl('laboratorium/daftarPasien/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id},
        function(data){
            if(data.pasienadmisi_id == null){
                var pasienadmisi_id = '';
            }else{
                var pasienadmisi_id = data.pasienadmisi_id;
            }
            if(data.pasien_id == null){
                var pasien_id = '';
            }else{
                var pasien_id = data.pasien_id;
            }
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan pemeriksaan. ");
            }else{
                window.location.href= "<?php echo Yii::app()->createUrl('laboratorium/pencarianPasien/DaftarDariPasienRujukan',array()); ?>&idPendaftaran="+data.pendaftaran_id+"&idPasien="+pasien_id+"&idPasienAdmisi="+pasienadmisi_id,"";
            }
    },"json");
    return false; 
}
</script>
