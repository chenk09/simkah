<?php 
//if($caraPrint=='EXCEL')
//{
//    header('Content-Type: application/vnd.ms-excel');
//    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
//    header('Cache-Control: max-age=0');     
//}
//echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>7));      
//
//echo $this->renderPartial('details', array('caraPrint'=>$caraPrint,
//                                           'modDetailRencana'=>$modDetailRencana,
//   
//                                                                                   'modRencanaKebutuhan'=>$modRencanaKebutuhan));
?>
<table style="margin-left:70px">
    <tr>
        <td colspan="3">
            <table>
                <tr>
                    <td>
                        <?php echo $this->renderPartial('application.views.headerReport.headerDefaultStruk'); ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valig="middle" colspan="3">
            <b><?php echo $judulLaporan ?></b>
        </td>
    </tr>
</table>
<table style="margin-left:100px">
     <tr>
        <td align="center" valig="middle" colspan="3">
             Data Pasien
        </td>
    </tr>
    <tr>
        <td>Nama Pasien</td>
        <td>:</td>
        <td><?php echo $modPasien->namadepan.$modPasien->nama_pasien.Params::DEFAULT_BIN.$modPasien->nama_bin; ?></td>
    </tr>
    <tr>
        <td>No. Rekam Medis</td>
        <td>:</td>
        <td><?php echo $modPasien->no_rekam_medik; ?></td>
    </tr>
    <tr>
        <td>No. Pendaftaran</td>
        <td>:</td>
        <td><?php echo $modPendaftaran->no_pendaftaran; ?></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>:</td>
        <td><?php echo $modPasien->jeniskelamin; ?></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td><?php echo $modPasien->alamat_pasien; ?></td>
    </tr>
    <tr>
        <td>Tangal Lahir / Umur</td>
        <td>:</td>
        <td><?php echo $modPasien->tanggal_lahir; ?>/<?php echo $modPendaftaran->umur; ?></td>
    </tr>
    <tr>
        <td>Cara Bayar / Penjamin</td>
        <td>:</td>
        <td><?php echo $modPendaftaran->carabayar->carabayar_nama; ?>/<?php echo $modPendaftaran->penjamin->penjamin_nama; ?></td>
    </tr>
<!--    <tr>
        <td align="center" valig="middle" colspan="3">
            Instalasi <?php //echo $modPendaftaran->instalasi->instalasi_nama; ?>
        </td>
    </tr>-->
    <tr>
        <td>Poliklinik Tujuan / No. Antrian</td>
        <td>:</td>
        <td><?php echo $modPendaftaran->ruangan->ruangan_nama; ?>/<?php echo $modPendaftaran->no_urutantri; ?></td>
    </tr>
     <tr>
        <td>Karcis</td>
        <td>:</td>
        <td><?php echo "Pendaftaran Pemeriksaan Laboratorium"; ?></td>
    </tr>
    <tr>
        <td>Tanggal Pendaftaran</td>
        <td>:</td>
        <td><?php echo $modPendaftaran->tgl_pendaftaran; ?></td>
    </tr>
    <tr>
        <td>Status Pembayaran Karcis</td>
        <td>:</td>
        <td>Belum Dibayar  <!--Default dulu--></td>
    </tr>
    <tr>
        <td>Dokter Pemeriksa</td>
        <td>:</td>
        <td><?php echo $modPendaftaran->dokter->nama_pegawai; ?></td>
    </tr>
</table>
<br>
<style>
    .garis{
        border:1px solid #ddd;
    }
</style>
<table class="garis" style="margin-left:50px;">
    <thead class="garis" color="red">
        <tr>
<!--            <th>
                Keterangan
            </th>-->
            <th class="garis">
                Kategori (Dokter)<br/>Tindakan
            </th>
            <th class="garis">
                Tarif Satuan
            </th class="garis">
            <th class="garis">
                Qty
            </th class="garis">
            <th class="garis">
                Tarif Cyto
            </th>
            <th class="garis">
                Disc
            </th>
            <th class="garis">
                Sub Total
            </th> 
            <th class="garis">
                Status Bayar
            </th> 
        </tr>
    </thead>
    <tbody>
        <?php 
        $ruangan = array();
        $total = 0;
        $subsidiAsuransi = 0;
        $subsidiPemerintah = 0;
        $subsidiRumahSakit = 0;
        $iurBiaya = 0;
        foreach ($modRincian as $i=>$row){
//            $rowspan = count(RinciantagihanpasienV::model()->findAll('ruangan_id = '.$row->ruangan_id.' and pendaftaran_id = '.$row->pendaftaran_id));
//            if (!in_array($row->ruangan_id, $ruangan)){
//                $ruangan[] = $row->ruangan_id;
//                $ruanganTd = '<td rowspan="'.$rowspan.'" style="vertical-align:middle;text-align:center;">'.$row->ruangan_nama.'</td>';
//            }
//            else{
//                $ruanganTd = '';
//            }
            
            echo '<tr>
                    '.$ruanganTd.'
                    <td class=garis>'.$row->kategoritindakan_nama.' ('.$row->nama_pegawai.')<br/>'.$row->daftartindakan_nama.'
                    </td>
                    <td class=garis align=right>'.number_format($row->tarif_satuan, 0,',','.').'
                    </td>
                    <td class=garis align=center>'.$row->qty_tindakan.'
                    </td>
                    <td class=garis align=right>'.number_format($row->tarifcyto_tindakan, 0,',','.').'
                    </td>
                    <td class=garis align=center>'.$row->discount_tindakan.'
                    </td>
                    <td class=garis align=right>'.number_format($row->subTotal, 0,',','.').'
                    </td>
                    <td class=garis align=center>'.((empty($row->tindakansudahbayar_id)) ? "BELUM LUNAS" : "LUNAS").'
                    </td>
                   </tr>';
            $total += $row->subTotal;
            $subsidiAsuransi +=$row->subsidiasuransi_tindakan;
            $subsidiPemerintah += $row->subsidipemerintah_tindakan;
            $subsidiRumahSakit += $row->subsisidirumahsakit_tindakan;
            $iurBiaya += $row->iurbiaya_tindakan;
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td class="garis" colspan="5"><div class='pull-right'>Total Tagihan</div></td>
            <td class="garis" align="right"><?php echo number_format($total, 0,',','.'); ?></td>
            <td class="garis"></td>
        </tr>
        <tr>
            <td class="garis" colspan="5"><div class='pull-right'>Subsidi Asuransi</div></td>
            <td class="garis" align="right"><?php echo number_format($subsidiAsuransi, 0,',','.'); ?></td>
            <td class="garis"></td>
        </tr>
        <tr>
            <td class="garis" colspan="5"><div class='pull-right'>Subsidi Pemerintah</div></td>
            <td class="garis" align="right"><?php echo number_format($subsidiPemerintah, 0,',','.'); ?></td>
            <td class="garis"></td>
        </tr>
        <tr>
            <td class="garis" colspan="5"><div class='pull-right'>Subsidi Rumah Sakit</div></td>
            <td class="garis" align="right"><?php echo number_format($subsidiRumahSakit, 0,',','.'); ?></td>
            <td class="garis"></td>
        </tr>
        <tr>
            <td class="garis" colspan="5"><div class='pull-right'>Iur Biaya</div></td>
            <td class="garis" align="right"><?php echo number_format($iurBiaya, 0,',','.'); ?></td>
            <td class="garis"></td>
        </tr>
    </tfoot>
</table>