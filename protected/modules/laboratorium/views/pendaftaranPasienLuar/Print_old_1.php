<?php 
//if($caraPrint=='EXCEL')
//{
//    header('Content-Type: application/vnd.ms-excel');
//    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
//    header('Cache-Control: max-age=0');     
//}
//echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>7));      
//
//echo $this->renderPartial('details', array('caraPrint'=>$caraPrint,
//                                           'modDetailRencana'=>$modDetailRencana,
//   
//                                                                                   'modRencanaKebutuhan'=>$modRencanaKebutuhan));
?>
<?php
//    echo "<pre>";
//    print_r($modPasienPenunjangRad->attributes);
//    exit();
?>
<style>
    table tr .border {
        border: 1px solid;
        margin: 5px auto;
        padding: 0px auto;
        width: 98%;
    }
    .utama{
        margin:2px;
        padding: 0px;
        /*border:1px solid;*/
        width: 210mm; height: 297mm; /* ukuran A4 */
    }
    td {
        font-size: 10pt;
        vertical-align: top;
    }
    div .nama_periksa{
        font-size: 12pt;
        font-weight: bold;
        text-align: center;
    }
    
</style>

<?php 
$ruangan = array();
$total = 0;
$subsidiAsuransi = 0;
$subsidiPemerintah = 0;
$subsidiRumahSakit = 0;
$iurBiaya = 0;
foreach ($modRincian as $i=>$row){
    $total += $row->subTotal;
    $subsidiAsuransi +=$row->subsidiasuransi_tindakan;
    $subsidiPemerintah += $row->subsidipemerintah_tindakan;
    $subsidiRumahSakit += $row->subsisidirumahsakit_tindakan;
    $iurBiaya += $row->iurbiaya_tindakan;
}
?>
<?php $jmlHal = intval ((($i+1)/10)+1); //menghitung jumlah halaman
    $hal = 0;?>
<?php // echo 'Jumlah Data: '.($i+1).'Halaman :'.($jmlHal); ?>
<table class='utama'>
    <tr>
        <td width="50%" valign="top">
<!--        Contoh    
            <table class="border">
                <tr><td colspan="4">Total Pemeriksaan: Rp.23.000.000</td></tr>
                <tr><td colspan="4">Untuk Pemeriksaan Lab. dari:</td></tr>
                <tr><td>Nama</td><td colspan="3">: Tn Teng</td></tr>
                <tr><td>Alamat</td><td colspan="3">: Jln. Perintis Kemerdekaan</td></tr>
                <tr><td>No.Lab</td><td>: 20093245324</td><td>Tanggal</td><td>: 22 April 2009</td></tr>
                <tr  class="border"><td colspan="4"><table>Nama Lab: Laboratorium Klinik Utama Budi Kartini</table></td></tr>
            </table>-->
            <table class="border">
                <tr><td colspan="4">Total Pemeriksaan: Rp. <?php echo number_format($total, 0,',','.'); ?></td></tr>
                <tr><td colspan="4">Untuk Pemeriksaan Lab. dari:</td></tr>
                <tr><td>Nama</td><td colspan="3">: <?php echo $modPasien->namadepan.$modPasien->nama_pasien.Params::DEFAULT_BIN.$modPasien->nama_bin; ?></td></tr>
                <tr><td>Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?></td></tr>
                <tr><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td><td>Tanggal</td><td>: <?php echo $modPendaftaran->tgl_pendaftaran; ?></td></tr>
                <tr><td>Umur</td><td>: <?php echo $modPendaftaran->umur; ?></td><td>Sex</td><td>: <?php echo $modPasien->jeniskelamin; ?></td></tr>
                <tr  class="border"><td colspan="4"><table><b>Nama Lab: Laboratorium Klinik Utama <?php echo Params::NAMA_KLINIK; ?><b></table></td></tr>
            </table>
<!--            Mulai looping detail-->
            <?php 
            $x = 0;
            $y = 0;
            foreach ($modRincian as $i=>$row){ ?>
                <?php if($modRincian[$x]->kategoritindakan_id == Params::KATEGORI_TINDAKAN_ID_ADMINISTRASI) $x++; //skip jika ada biaya administrasi ?>
                <?php if(($x < $y+4) && (!empty($modRincian[$x]->daftartindakan_id))){?>
                    <table class="border">
                        <tr><td colspan="4">Kepada Yth : <?php // echo ($hal+1).'/'.$jmlHal.': '.$x; ?></td></tr>
                        <tr><td>Dokter</td><td colspan="3">: <?php echo $modRincian[$x]->nama_pegawai ?></td></tr>
                        <tr><td>Alamat</td><td colspan="3">: Rs. Jasa Kartini Tasikmalaya</td></tr>
                        <tr><td>Pasien</td><td colspan="3">: <?php echo $modPasien->namadepan.$modPasien->nama_pasien.Params::DEFAULT_BIN.$modPasien->nama_bin; ?></td></tr>
                        <tr><td>Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?><br>&nbsp;&nbsp;<b><?php echo $modPendaftaran->ruangan_id.' /'.$modPendaftaran->ruangan->ruangan_nama; ?></b></td></tr>
                        <tr><td>Umur</td><td>: <?php echo $modPendaftaran->umur; ?></td><td>Sex</td><td>: <?php echo $modPasien->jeniskelamin; ?></td></tr>
                        <tr><td colspan="4"><table><div class='nama_periksa'><?php echo $modRincian[$x]->daftartindakan_nama; ?></div></table></td></tr>
                        <tr class='border'><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td><td>Tanggal</td><td>: <?php echo $modPendaftaran->tgl_pendaftaran; ?></td></tr>
                    </table>
                <?php $x++; }?>
            <?php } ?>
        </td>
        <td width="50%" valign="top">
            <table class="border">
                <tr><td colspan="4">Total Pemeriksaan: Rp. <?php echo number_format($total, 0,',','.'); ?></td></tr>
                <tr><td colspan="4">Untuk Pemeriksaan Lab. dari:</td></tr>
                <tr><td border="0">Nama</td><td colspan="3">: <?php echo $modPasien->namadepan.$modPasien->nama_pasien.Params::DEFAULT_BIN.$modPasien->nama_bin; ?></td></tr>
                <tr><td border="0">Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?></td></tr>
                <tr><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td><td>Tanggal</td><td>: <?php echo $modPendaftaran->tgl_pendaftaran; ?></td></tr>
                <tr><td>Umur</td><td>: <?php echo $modPendaftaran->umur; ?></td><td>Sex</td><td>: <?php echo $modPasien->jeniskelamin; ?></td></tr>
                <tr  class="border"><td colspan="4"><table><b>Nama Lab: Laboratorium Klinik Utama <?php echo Params::NAMA_KLINIK; ?><b></table></td></tr>
            </table>
            <?php
            $x = $x;
            $y = $x;
            foreach ($modRincian as $i=>$row){ ?>
                <?php if($modRincian[$x]->kategoritindakan_id == Params::KATEGORI_TINDAKAN_ID_ADMINISTRASI) $x++; //skip jika ada biaya administrasi ?>
                <?php if(($x < $y+4) && (!empty($modRincian[$x]->daftartindakan_id))){?>
                    <table class="border">
                        <tr><td colspan="4">Kepada Yth : <?php // echo ($hal+1).'/'.$jmlHal.': '.$x; ?></td></tr>
                        <tr><td>Dokter</td><td colspan="3">: <?php echo $modRincian[$x]->nama_pegawai ?></td></tr>
                        <tr><td>Alamat</td><td colspan="3">: Rs. Jasa Kartini Tasikmalaya</td></tr>
                        <tr><td>Pasien</td><td colspan="3">: <?php echo $modPasien->namadepan.$modPasien->nama_pasien.Params::DEFAULT_BIN.$modPasien->nama_bin; ?></td></tr>
                        <tr><td>Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?><br>&nbsp;&nbsp;<b><?php echo $modPendaftaran->ruangan_id.' /'.$modPendaftaran->ruangan->ruangan_nama; ?></b></td></tr>
                        <tr><td>Umur</td><td>: <?php echo $modPendaftaran->umur; ?></td><td>Sex</td><td>: <?php echo $modPasien->jeniskelamin; ?></td></tr>
                        <tr><td colspan="4"><table><div class='nama_periksa'><?php echo $modRincian[$x]->daftartindakan_nama; ?></div></table></td></tr>
                        <tr class='border'><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td><td>Tanggal</td><td>: <?php echo $modPendaftaran->tgl_pendaftaran; ?></td></tr>
                    </table>
                <?php $x++;} ?>
            <?php } ?>
        </td>
    </tr>
    <?php $hal ++;?>
</table><br><br><br><br>
<?php if($jmlHal > 1){
    for($hal=$hal;$hal < $jmlHal;$hal++){
    ?>
    <table class='utama'>
        <tr>
            <td width="50%" valign="top">
    <!--            Mulai looping detail-->
                <?php 
                $x = $x;
                $y = $x;
                foreach ($modRincian as $i=>$row){ ?>
                    <?php if($modRincian[$x]->kategoritindakan_id == Params::KATEGORI_TINDAKAN_ID_ADMINISTRASI) $x++; //skip jika ada biaya administrasi ?>
                    <?php if(($x < $y+5) && (!empty($modRincian[$x]->daftartindakan_id))){?>
                        <table class="border">
                            <tr><td colspan="4">Kepada Yth : <?php // echo ($hal+1).'/'.$jmlHal.': '.$x; ?></td></tr>
                            <tr><td>Dokter</td><td colspan="3">: <?php echo $modRincian[$x]->nama_pegawai; ?></td></tr>
                            <tr><td>Alamat</td><td colspan="3">: Rs. Jasa Kartini Tasikmalaya</td></tr>
                            <tr><td>Pasien</td><td colspan="3">: <?php echo $modPasien->namadepan.$modPasien->nama_pasien.Params::DEFAULT_BIN.$modPasien->nama_bin; ?></td></tr>
                            <tr><td>Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?><br>&nbsp;&nbsp;<b><?php echo $modPendaftaran->ruangan_id.' /'.$modPendaftaran->ruangan->ruangan_nama; ?></b></td></tr>
                            <tr><td>Umur</td><td>: <?php echo $modPendaftaran->umur; ?></td><td>Sex</td><td>: <?php echo $modPasien->jeniskelamin; ?></td></tr>
                            <tr><td colspan="4"><table><center><b><?php echo $modRincian[$x]->daftartindakan_nama; ?><b></center></table></td></tr>
                            <tr class='border'><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td><td>Tanggal</td><td>: <?php echo $modPendaftaran->tgl_pendaftaran; ?></td></tr>
                        </table>
                    <?php $x++; }?>
                <?php } ?>
            </td>
            <td width="50%" valign="top">
                <?php
                $x = $x;
                $y = $x;
                foreach ($modRincian as $i=>$row){ ?>
                    <?php if($modRincian[$x]->kategoritindakan_id == Params::KATEGORI_TINDAKAN_ID_ADMINISTRASI) $x++; //skip jika ada biaya administrasi ?>
                    <?php if(($x < $y+5) && (!empty($modRincian[$x]->daftartindakan_id))){?>
                        <table class="border">
                            <tr><td colspan="4">Kepada Yth : <?php // echo ($hal+1).'/'.$jmlHal.': '.$x; ?></td></tr>
                            <tr><td>Dokter</td><td colspan="3">: <?php echo $modRincian[$x]->nama_pegawai.$modRincian[$x]->kategoritindakan_id; ?></td></tr>
                            <tr><td>Alamat</td><td colspan="3">: Rs. Jasa Kartini Tasikmalaya</td></tr>
                            <tr><td>Pasien</td><td colspan="3">: <?php echo $modPasien->namadepan.$modPasien->nama_pasien.Params::DEFAULT_BIN.$modPasien->nama_bin; ?></td></tr>
                            <tr><td>Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?><br>&nbsp;&nbsp;<b><?php echo $modPendaftaran->ruangan_id.' /'.$modPendaftaran->ruangan->ruangan_nama; ?></b></td></tr>
                            <tr><td>Umur</td><td>: <?php echo $modPendaftaran->umur; ?></td><td>Sex</td><td>: <?php echo $modPasien->jeniskelamin; ?></td></tr>
                            <tr><td colspan="4"><table><center><b><?php echo $modRincian[$x]->daftartindakan_nama; ?><b></center></table></td></tr>
                            <tr class='border'><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td><td>Tanggal</td><td>: <?php echo $modPendaftaran->tgl_pendaftaran; ?></td></tr>
                        </table>
                    <?php $x++;} ?>
                <?php } ?>
            </td>
        </tr>
    </table>
<?php 
    }
}?>
