<fieldset id="fieldsetBiayaAdministrasi" class="span6">
    <legend class="accord1"  style="text-align: left;">
        <?php echo CHtml::checkBox('adaBiayaAdministrasi', $model->adaBiayaAdministrasi, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'hitungTotalSeluruh();')) ?>
        Biaya Administrasi
    </legend>
    <div id="divAdministrasi" class="control-group toggle">
        <div class="control-group">
            <label class="control-label">Biaya Administrasi</label>
            <div class="controls">
                <?php echo CHtml::textField('TindakanPelayananT[tarifSatuanAdministrasi]', MyFunction::formatNumber($model->tarifAdministrasi),array('onkeyup'=>'hitungTotalSeluruh();','class'=>'span2 currency','style'=>'text-align:right;','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </div>
        </div>
    </div>
</fieldset>
<?php
$enableInputBiayaAdministrasi = ($model->adaBiayaAdministrasi) ? 1 : 0;
$biayaAdministrasi = $model->tarifAdministrasi;
$js = <<< JS
if(${enableInputBiayaAdministrasi}) {
    $('#divAdministrasi input').removeAttr('disabled');
    $('#divAdministrasi select').removeAttr('disabled');
    $('#divAdministrasi').show(500);
}
else {
    $('#divAdministrasi input').attr('disabled','true');
    $('#divAdministrasi select').attr('disabled','true');
//    $('#divAdministrasi').slideToggle(500);
    $('#divAdministrasi').hide(500);
}

$('#adaBiayaAdministrasi').change(function(){
        if ($(this).is(':checked')){
                $('#divAdministrasi input').removeAttr('disabled');
                $('#divAdministrasi select').removeAttr('disabled');
                $('#PPPengambilanSampleT_tarifAdministrasi').val('${biayaAdministrasi}')
        }else{
                $('#divAdministrasi input').attr('disabled','true');
                $('#divAdministrasi select').attr('disabled','true');
//                $('#divAdministrasi input').attr('value','0');
//                $('#divAdministrasi select').attr('value','0');
        }
        $('#divAdministrasi').slideToggle(500);
    });
JS;
Yii::app()->clientScript->registerScript('administrasi',$js,CClientScript::POS_READY);
?>