<style>
    table td{
    font-size:11px;
    }
</style>

<table width="100%" height = "55%"; border="0" cellspacing="2" cellpadding="1">
    <tr style="border-bottom:1px solid #000000">
        <td valign="" width="70" style="font-family:calibri;padding-left:40px;text-align:center;"> No RM :</td>
        <td></td>
        <td valign="top" style="font-family:calibri;font-weight: bold;font-size: 13pt;"><?php echo implode(".", str_split($modPasien->no_rekam_medik, 2));?></td>
    </tr>
</table>

<table width="100%" height = "55%"; border="0" cellspacing="2" cellpadding="1">
    <?php 

    function custom_echo($x, $length)
    {
      if(strlen($x)<=$length)
      {
        echo $x;
      }
      else
      {
        $y=substr($x,0,$length) . '...';
        echo $y;
      }
    }

     ?>
    <tr>
        <td valign="top" width="40" style="font-weight:bold;font-family:calibri;font-size:9px;" >Nama</td>
        <td valign="top">:</td>
        <td valign="top" style="font-family:calibri;font-size:11px"><?php custom_echo( $modPasien->nama_pasien , 25 ); ?>, <?php echo $modPasien->namadepan; ?>
        <?php if ($modPasien->jeniskelamin=="LAKI-LAKI") {
                echo "(L)";
            } else {
                echo "(P)";
            }
            ?>
        </td>
        
    </tr>
    <tr>
    <td style="width:40px;height:5px;"></td>
        <td valign="top" width="5" style="font-family:calibri;" ></td>
        <td valign="top"></td>
        <td valign="top" style="font-weight: bold;font-family:calibri;"></td>
    </tr>
    <tr>
        <td valign="top" style="font-weight:bold;font-family:calibri;font-size:9px;" >Tgl. Lahir</td>
        <td valign="top">:</td>
        <!--<td valign="top" style= ";font-family:times new roman;font-size:10px;"><?php echo date('d-m-Y', strtotime ($modPasien->tanggal_lahir)); ?> (<?php echo $modPendaftaran->umur; ?>)</td>--> 
        <td valign="top" style= ";font-family:calibri;font-size:10px;"><?php echo $modPasien->tanggal_lahir; ?> (<?php echo $modPendaftaran->umur; ?>)</td>
</table>
