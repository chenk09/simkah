<style>
    /*==Untuk Label Dengan DIV==*/
    .labelPrint {
        background-color: #FFFFFF;
        color: #0C0C0C;
        display: inline-block;
        margin: 0px;
        padding: 0px;
        width: 49%;
        text-align: left;
        font-size-adjust: 0.55;
        line-height:145%;
    }
    .labelPrintLab {
        background-color: #FFFFFF;
        color: #0C0C0C;
        display: inline-block;
        margin: 5px;
        padding: 0px;
        width: 49%;
        text-align: left;
        font-size-adjust: 0.60;
        line-height:110%;
        /*font-size: 40pt;*/
    }

    .labelxray{
        background-color: #FFFFFF;
        color: #0C0C0C;
        display: inline-block;
        margin: 5px;
        padding: 0px;
        width: 50%;
        text-align: left;
        font-size-adjust: 0.70;
        line-height:100%;
    }

    .printLab{
        font-family: arial;
        font-size: 8pt;
    }

    .labelPrintKecil {
        background-color: #FFFFFF;
        color: #0C0C0C;
        display: inline-block;
        margin: 0px;
        padding: 0px;
        width: 23.7%;
        text-align: center;
        font-size: 4pt;
    }
    /*=====*/
    .border {
        border: 1px solid;
        margin: 0px;
        padding: 0px;
        width: 100%;
    }
    .utama{
        padding:20px;
        text-align: left;
        font-size: 12pt;
        /*border:1px solid;*/
        /*width: 210mm; height: 297mm;  ukuran A4 */
        width: 100%; /* height: 297mm; ukuran A4 */
        vertical-align: top;
    }
    .rontgentd{
        font-family: arial;
        font-size: 12pt;
    }
    td {
        font-family: arial;
        font-size: 11pt;
        vertical-align: top;
    }
    div .nama_periksa{
        font-family: arial;
        font-size: 12pt;
        font-weight: bold;
        text-align: center;
    }
    .trrontgen {
        padding: 0px;
    }
    
</style>
<?php 
$format = new CustomFormat;
$ruangan = array();

$subsidiAsuransi = 0;
$subsidiPemerintah = 0;
$subsidiRumahSakit = 0;
$iurBiaya = 0;

$total = 0;
foreach ($modRincian as $i=>$row)
{
    //echo $row->tarif_satuan . '<br />';
    $total +=  $row->subTotal;
    $subsidiAsuransi +=$row->subsidiasuransi_tindakan;
    $subsidiPemerintah += $row->subsidipemerintah_tindakan;
    $subsidiRumahSakit += $row->subsisidirumahsakit_tindakan;
    $iurBiaya += $row->iurbiaya_tindakan;
}

$total = 0;
foreach($modTindakanPelayanan as $key=>$data){
    $total += ($data->tarif_satuan * $data->qty_tindakan);
}
//Variabel pemeriksaan lab
$urine = false;
$urine2jam = false;
$edta = false;
$kimia = false;


?>
<!--<div class="utama">-->
<br>
<?php if(!($labelOnly == 1)){ ?>
    <div class="labelPrint">
    <table class="border">
        <tr>
            <td colspan="4">
                
                    <?php
                        if($total > 0){
                            if(substr($modPendaftaran->no_pendaftaran, 0, 2) == 'LB') {
                                echo "Total Pemeriksaan : Rp. ".number_format($total, 0,',','.').",-";
                            }
                            
                        }else{
                            echo '- terlampir -';
                        }
                        
                    ?>
            </td>
        </tr>
        <tr><td colspan="3">Untuk Pemeriksaan Lab. dari: </td></tr>
        <tr><td>Nama</td><td colspan="3">: <?php echo $modPasien->namadepan ." ". $modPasien->nama_pasien . (strlen($modPasien->nama_bin) > 0  ? (Params::DEFAULT_BIN . $modPasien->nama_bin) : ""); ?></td></tr>
        <tr><td>Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?></td></tr> 
        <tr><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td></tr>
        <tr><td>Umur</td><td>: <?php echo $modPendaftaran->umur; ?></td></tr>
        <tr><td>Tanggal</td><td >: <?php $tgl = substr($modPasienMasukPenunjang->tglmasukpenunjang,0,10); $format = new CustomFormat(); echo $format->formatDateINA($tgl);?></td></tr> 
        <tr><td>Sex</td><td>: <?php echo $modPasien->jeniskelamin; ?></td></tr>
        <tr  class="border"><td colspan="3"><table><b>Laboratorium Klinik Utama <?php echo Params::NAMA_KLINIK; ?><b></table></td></tr>
    </table>
    </div>
    <div class="labelPrint">
<!-- <div style="position:absolute;margin-left: 403px;border:0px solid #000;padding: 20px;font-size: 25px;font-weight: bold;">
    KS
</div> -->
    <table class="border">
        <tr>
            <td colspan="5">
                    <?php
                        if($total > 0){
                            if(substr($modPendaftaran->no_pendaftaran, 0, 2) == 'LB') {
                                echo "Total Pemeriksaan : Rp. ".number_format($total, 0,',','.').",-";
                            }
                        }else{
                            echo '- terlampir -';
                        }
                        
                    ?>                
            </td>
        </tr>
        <tr><td colspan="4">Untuk Pemeriksaan Lab. dari: </td><td rowspan="2" width="40px"><div style="font-size: 25px;font-weight: bold;">KS</div></td></tr>
        <tr><td>Nama</td><td colspan="3">: <?php echo $modPasien->namadepan ." ". $modPasien->nama_pasien . (strlen($modPasien->nama_bin) > 0  ? (Params::DEFAULT_BIN . $modPasien->nama_bin) : ""); ?></td></tr>
        <tr><td>Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?></td></tr> 
        <tr><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td></tr>
        <tr><td>Umur</td><td>: <?php echo $modPendaftaran->umur; ?></td></tr>
        <tr><td>Tanggal</td><td >: <?php $tgl = substr($modPasienMasukPenunjang->tglmasukpenunjang,0,10); $format = new CustomFormat(); echo $format->formatDateINA($tgl);?></td></tr> 
        <tr><td>Sex</td><td >: <?php echo $modPasien->jeniskelamin; ?></td></tr>
        <tr  class="border"><td colspan="3"><table><b>Laboratorium Klinik Utama <?php echo Params::NAMA_KLINIK; ?><b></table></td></tr>
    </table>
    </div>
<?php } ?>
    <?php 
//    echo "<pre>";    print_r($modRincian[0]->attributes); exit();

        $a = null;
        $b = null;
        
    foreach ($modRincian as $i=>$row){ //Looping untuk pemeriksaan?> 
        <?php if ($modRincian[$i]->ruangan_id == Params::RUANGAN_ID_RAD){ ?>
        <?php 
        // Untuk label Rontgen, CT-Scan dan USG
        // ====================================
        if(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::PERIKSA_RONTGEN || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::PERIKSA_CTSCAN || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::PERIKSA_USG || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::PAKET_PEMERIKSAAN || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::PAKET_PEMERIKSAAN_MCU_HAJI){ ?>
            
            <div class="labelPrint">
<!--
<div style="position:absolute;margin-left: 403px;border:0px solid #000;padding: 20px;font-size: 25px;font-weight: bold;">
    KS
</div>                
-->
            <?php
                if(!empty($modPasien->nama_bin)){
                    $bin = Params::DEFAULT_BIN.$modPasien->nama_bin;
                }else{
                    $bin = "";
                }
            ?>
            <table class="border">
                <td width="2%">&nbsp;</td><td colspan="4">Kepada Yth : <?php // echo ($hal+1).'/'.$jmlHal.': '.$i; ?></td></tr>
                <tr><td width="2%">&nbsp;</td><td>Dokter</td>
                    <td colspan="3">: 
                <?php 
                    $alamatPerujuk = '';
                    if(substr($modPendaftaran->no_pendaftaran, 0, 2) == 'LB') {
                        echo $modRincian[$i]->namaPerujuk;
                        $alamatPerujuk = $modRincian[$i]->alamatPerujuk;
                    }else{
                        $modPendaftaran = PendaftaranT::model()->findByPk($modPendaftaran->pendaftaran_id);
                        if(substr($modPendaftaran->no_pendaftaran, 0, 2) != 'LB'){
                            $pasienKirimKeunitLain = PasienkirimkeunitlainT::model()->findByAttributes(array(
                                'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id,
                            ));
                            $modPegawai = PegawaiM::model()->findByPk($pasienKirimKeunitLain->pegawai_id);
                            echo  $modPegawai->nama_pegawai;// . ', ' . $modPegawai->gelarbelakang->gelarbelakang_nama;
                            //$modRincian[$i]->alamatlengkapperujuk = 'RS Jasa Kartini Tasikmalaya';
                            $alamatPerujuk = 'RS Jasa Kartini Tasikmalaya';
                        }
                    }
                ?>
                    </td></tr>
                <tr><td width="2%">&nbsp;</td><td>Alamat</td><td colspan="4">: <?php  echo  $alamatPerujuk;?></td></tr>
                <tr><td width="2%">&nbsp;</td><td>Pasien</td><td colspan="4">: <?php echo $modPasien->namadepan . ' ' .$modPasien->nama_pasien.$bin; ?></td></tr>
                <tr><td width="2%">&nbsp;</td><td>Umur</td><td colspan="4">: <?php echo $modPendaftaran->umur; ?></td></tr>
                <tr><td width="2%">&nbsp;</td><td>Sex</td><td colspan="4">: <?php echo $modPasien->jeniskelamin; ?></td></tr>
                <tr><td width="2%">&nbsp;</td><td>Alamat</td><td colspan="4">: <?php echo $modPasien->alamat_pasien; ?><br>&nbsp;&nbsp;<b><?php echo $modPendaftaran->ruangan->ruangan_nama; echo ' RM '. $modPendaftaran->pasien->no_rekam_medik; ?></b></td></tr>
                <tr><td width="2%">&nbsp;</td><td colspan="4"><table><div class='nama_periksa'><?php echo $modRincian[$i]->pemeriksaanlab_nama; ?></div></table></td></tr>
                <tr class='border'><td width="2%">&nbsp;</td><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?>
                    </td><td>Tanggal</td><td>: <?php $tgl = substr($modPasienMasukPenunjang->tglmasukpenunjang,0,10); $format = new CustomFormat(); echo $format->formatDMYtoYMD2($tgl); //echo date("d/m/y",strtotime($modPasienMasukPenunjang->tglmasukpenunjang)); //$tgl = substr($modPasienMasukPenunjang->tglmasukpenunjang,0,10); $format = new CustomFormat(); echo $format->formatDateINA($tgl);?></td></tr>
            </table>
            </div>
            
        <?php }
        }else if($modRincian[$i]->ruangan_id == Params::RUANGAN_ID_LAB){ //Jika ada pemeriksaan lab cetak Label Lab ?>
            <?php 
            // Untuk label EKG, Treadmill, Echo Cardio, Spirometri, Audiometri
            if($modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_EKG_ID 
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_TREADMILL_ID
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_ECHOCAR_A_ID
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_ECHOCAR_B_ID
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_ECHOCAR_C_ID
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_SPIROMETRI_ID
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_AUDIOMETRI_ID
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PAKET_MCU_HAJI_LAKI_LAKI
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PAKET_MCU_HAJI_PEREMPUAN
                    ){ ?>
                <div class="labelPrint">
                <table class="border">
                    <tr><td width="2%">&nbsp;</td><td colspan="4">Kepada Yth : <?php // echo ($hal+1).'/'.$jmlHal.': '.$i; ?></td></tr>
                    <!-- <tr><td width="2%">&nbsp;</td><td>Dokter</td><td colspan="4">: <?php echo $modRincian[$i]->namaPerujuk; ?></td></tr> -->
                    <tr><td width="2%">&nbsp;</td><td>Dokter</td>
                    <td colspan="3">: 
                    <?php 
                    $alamatPerujuk = '';
                    if(substr($modPendaftaran->no_pendaftaran, 0, 2) == 'LB') {
                        echo $modRincian[$i]->namaPerujuk;
                        $alamatPerujuk = $modRincian[$i]->alamatPerujuk;
                    }else{
                        $modPendaftaran = PendaftaranT::model()->findByPk($modPendaftaran->pendaftaran_id);
                        if(substr($modPendaftaran->no_pendaftaran, 0, 2) != 'LB'){
                            $pasienKirimKeunitLain = PasienkirimkeunitlainT::model()->findByAttributes(array(
                                'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id,
                            ));
                            $modPegawai = PegawaiM::model()->findByPk($pasienKirimKeunitLain->pegawai_id);
                            echo  $modPegawai->nama_pegawai;// . ', ' . $modPegawai->gelarbelakang->gelarbelakang_nama;
                            //$modRincian[$i]->alamatlengkapperujuk = 'RS Jasa Kartini Tasikmalaya';
                            $alamatPerujuk = 'RS Jasa Kartini Tasikmalaya';
                            }
                        }
                    ?>
                    </td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Alamat</td><td colspan="4">: <?php  echo  $alamatPerujuk;?></td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Pasien</td><td colspan="4">: <?php echo $modPasien->namadepan. ' ' .$modPasien->nama_pasien.Params::DEFAULT_BIN.$modPasien->nama_bin; ?></td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Umur</td><td colspan="4">: <?php echo $modPendaftaran->umur; ?></td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Sex</td><td colspan="4">: <?php echo $modPasien->jeniskelamin; ?></td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?><br>&nbsp;&nbsp;<b><?php echo $modPendaftaran->ruangan->ruangan_nama; ?></b></td></tr>
                    <tr><td width="2%">&nbsp;</td><td colspan="4"><table><div class='nama_periksa'><?php echo $modRincian[$i]->pemeriksaanlab_nama; ?></div></table></td></tr>
                    <tr class='border'><td width="2%">&nbsp;</td><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td><td>Tanggal</td><td>: 
                        <?php 
                        $tgl = substr($modPendaftaran->tgl_pendaftaran,0,11); $format = new CustomFormat(); echo $tgl;
                         ?></td></tr>
                </table>
                </div>
            <?php } ?>

             <?php 
            // Untuk label Paket Pemeriksaan 'PAKET II HDI', 'PAKET IV HDI', 'PAKET VI HDI',
            if($modRincian[$i]->pemeriksaanlab_id == Params::PAKET_II_HDI 
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PAKET_IV_HDI
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PAKET_VI_HDI
                    || $modRincian[$i]->pemeriksaanlab_id == Params::PAKET_V_HDI
                    
                    ){ ?>
                
                     <?php if ($modRincian[$i]->pemeriksaanlab_id == Params::PAKET_II_HDI
                                || $modRincian[$i]->pemeriksaanlab_id == Params::PAKET_IV_HDI
                                || $modRincian[$i]->pemeriksaanlab_id == Params::PAKET_VI_HDI
                                ){
                                $kelompokdet[$i] = 'Spirometri';
                            }
                              elseif ( $modRincian[$i]->pemeriksaanlab_id == Params::PAKET_IV_HDI
                                || $modRincian[$i]->pemeriksaanlab_id == Params::PAKET_V_HDI)
                                {
                                    $kelompokdet[$i] = 'Audiometri';
                                }
                        ?>
                        <?php// echo print_r($kelompokdet[$i]);exit; ?>
                        <?php if(($a==null AND $b==null) || ($a==null) || ($b==null)){ ?>
            <div class="labelPrint">    
                <table class="border">
                    <tr><td width="2%">&nbsp;</td><td colspan="4">Kepada Yth : <?php // echo ($hal+1).'/'.$jmlHal.': '.$i; ?></td></tr>
                    <!-- <tr><td width="2%">&nbsp;</td><td>Dokter</td><td colspan="4">: <?php echo $modRincian[$i]->namaPerujuk; ?></td></tr> -->
                    <tr><td width="2%">&nbsp;</td><td>Dokter</td>
                    <td colspan="3">: 
                    <?php 
                    $alamatPerujuk = '';
                    if(substr($modPendaftaran->no_pendaftaran, 0, 2) == 'LB') {
                        echo $modRincian[$i]->namaPerujuk;
                        $alamatPerujuk = $modRincian[$i]->alamatPerujuk;
                    }else{
                        $modPendaftaran = PendaftaranT::model()->findByPk($modPendaftaran->pendaftaran_id);
                        if(substr($modPendaftaran->no_pendaftaran, 0, 2) != 'LB'){
                            $pasienKirimKeunitLain = PasienkirimkeunitlainT::model()->findByAttributes(array(
                                'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id,
                            ));
                            $modPegawai = PegawaiM::model()->findByPk($pasienKirimKeunitLain->pegawai_id);
                            echo  $modPegawai->nama_pegawai;// . ', ' . $modPegawai->gelarbelakang->gelarbelakang_nama;
                            //$modRincian[$i]->alamatlengkapperujuk = 'RS Jasa Kartini Tasikmalaya';
                            $alamatPerujuk = 'RS Jasa Kartini Tasikmalaya';
                            }
                        }
                    ?>
                    </td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Alamat</td><td colspan="4">: <?php  echo  $alamatPerujuk;?></td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Pasien</td><td colspan="4">: <?php echo $modPasien->namadepan. ' ' .$modPasien->nama_pasien.Params::DEFAULT_BIN.$modPasien->nama_bin; ?></td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Umur</td><td colspan="4">: <?php echo $modPendaftaran->umur; ?></td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Sex</td><td colspan="4">: <?php echo $modPasien->jeniskelamin; ?></td></tr>
                    <tr><td width="2%">&nbsp;</td><td>Alamat</td><td colspan="3">: <?php echo $modPasien->alamat_pasien; ?><br>&nbsp;&nbsp;<b><?php echo $modPendaftaran->ruangan->ruangan_nama; ?></b></td></tr>
                    <tr><td width="2%">&nbsp;</td><td colspan="4"><table><div class='nama_periksa'>
                       <?php echo $kelompokdet[$i]; 
                            if($kelompokdet[$i]=='Spirometri'){
                                $a = 'Spirometri';
                            }elseif ($kelompokdet[$i]=='Audiometri') {
                                $b = 'Audiometri';
                            }
                       ?>
                    </div></table></td></tr>
                    <tr class='border'><td width="2%">&nbsp;</td><td>No.Lab</td><td>: <?php echo $modPendaftaran->no_pendaftaran; ?></td><td>Tanggal</td><td>: <?php //echo date("d/m/y",strtotime($modPendaftaran->tgl_pendaftaran));//$explTgl=explode(' ',$modPendaftaran->tgl_pendaftaran); $tgl=$explTgl[0].' '. $explTgl[1] .' '. $explTgl[2]; echo $tgl; 
                        $tgl = substr($modPendaftaran->tgl_pendaftaran,0,11); $format = new CustomFormat(); echo $tgl;
                    ?></td></tr>
                </table>
            </div>
                <?php } ?>
                
            <?php } ?>

            

        <?php } ?>
    <?php } 
     foreach ($modRincian as $i=>$row){ //Looping untuk pemeriksaan?>
    <?php 
            //Untuk Label Rontgen
            if(!empty($modPasien->nama_bin)){
                $bin = Params::DEFAULT_BIN.$modPasien->nama_bin;
            }else{
                $bin = "";
            }

            if(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::PERIKSA_RONTGEN || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::PAKET_PEMERIKSAAN_MCU_HAJI){?>
                <div class="labelPrintLab">
                    <table class="border">
                    <tr class="trrontgen"><td width="14%">&nbsp;</td><td  class="printLab">No.Lab </td><td  class="printLab">: <?php echo $modPendaftaran->no_pendaftaran; ?></td>
                                          <td class="printLab">Tanggal</td><td class="printLab">: <?php //echo date("d/m/y",strtotime($modPasienMasukPenunjang->tglmasukpenunjang)); 
                                          $tgl = substr($modPasienMasukPenunjang->tglmasukpenunjang,0,10); $format = new CustomFormat(); echo $format->formatDMYtoYMD2($tgl);?></td></tr>
                    <tr class="trrontgen">
                        <td width="5%">&nbsp;</td>
                        <td  class="printLab" >Nama</td>
                        <td  class="printLab">: <?php echo $modPasien->namadepan ." ". $modPasien->nama_pasien; ?></td>
                        
                    </tr>
                    <tr class="trrontgen">
                        <td width="5%">&nbsp;</td>
                        <td  class="printLab">Alamat</td>
                        <td  class="printLab" colspan="4">: <?php echo $modPasien->alamat_pasien; ?></td>
                    </tr>
                    <tr class="trrontgen">
                        <td width="5%">&nbsp;</td>
                        <td  class="printLab">Umur</td>
                        <td  class="printLab">: <?php echo $modPendaftaran->umur; ?></td>
                        <td  class="printLab">Sex</td>
                        <td  class="printLab">: <?php echo $modPasien->jeniskelamin; ?></td>
                    </tr>
                    <tr class="trrontgen"><td width="5%">&nbsp;</td><td  class="printLab">X-Ray</td><td colspan="4" style="font-size:13.3px;" class="printLab">: <b><?php echo $modRincian[$i]->pemeriksaanlab_nama; ?></b></td></tr>
                </table>
                </div>
            <?php } ?>
    <?php } ?>
    <?php foreach ($modRincian as $i=>$row){ //Looping untuk pemeriksaan?>
        <?php if($modRincian[$i]->ruangan_id == Params::RUANGAN_ID_LAB){ ?>
                <?php
                //Untuk label feaces
                if(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeaces(1)
                    || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeaces(2)
                    || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeaces(3)
                    || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeaces(4)
                ){
//                    echo "feaces"; 
                    $feaces = true;
                }                 
                
                //Untuk label Urine
                if(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaUrine(1)
                    || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaUrine(2)
                    || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaUrine(3)
                    || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaUrine(4)
                    || $modRincian[$i]->pemeriksaanlab_id == Params::jenisPeriksaUrine(5)
                        ){
//                    echo "URINE"; 
                    $urine = true;
                } 
                //Untuk label Urine 2Jampp
                if($modRincian[$i]->pemeriksaanlab_id == Params::jenisPeriksaUrine(6)){
//                    echo "URINE 2jamPP";
                    $urine2jam = true;
                }
                //Untuk label EDTA
                if((strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaEdta(1)
                    || strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaEdta(2))
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::jenisPeriksaUrine(5))
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::jenisPeriksaUrine(6))
                        ){
//                    echo "EDTA";
                    $edta = true;
                }
                //Untuk label Kimia (Selain Mikro dan EDTA)
                if(!(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::JENIS_PEMERIKSAAN_MIKROBIOLOGI)
                    && !(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaEdta(1))
                    && !(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaEdta(2))
                    && !(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaUrine(1))
                    && !(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaUrine(2))
                    && !(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaUrine(3))
                    && !(strtolower($modRincian[$i]->jenispemeriksaanlab_nama) == Params::jenisPeriksaUrine(4))
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::jenisPeriksaUrine(5))
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::jenisPeriksaUrine(6))
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_EKG_ID)
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_TREADMILL_ID)
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_ECHOCAR_A_ID)
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_ECHOCAR_B_ID)
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_ECHOCAR_C_ID)
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_AUDIOMETRI_ID)
                    && !($modRincian[$i]->pemeriksaanlab_id == Params::PERIKSA_SPIROMETRI_ID)
                        ){
//                    echo "Kimia";
                    $kimia = true;
                }
                ?>
        <?php } ?>
    <?php }?>
        <!--Label Kecil-->
    <?php if($urine){ ?>
            <div class="labelPrintKecil">
            <table class="border">
            <tr>
            <td class="border">
            <?php echo $modPasien->namadepan. ' ' .$modPasien->nama_pasien; ?><br>
            No. Lab: <?php echo $modPendaftaran->no_pendaftaran; ?><br>
            <b>URINE</b>
            </td>
            </tr>
            </table>
            </div>
    <?php } ?>
    <?php if($urine2jam){ ?>
            <div class="labelPrintKecil">
            <table class="border">
            <tr>
            <td class="border">
            <?php echo $modPasien->namadepan. ' ' .$modPasien->nama_pasien; ?><br>
            No. Lab: <?php echo $modPendaftaran->no_pendaftaran; ?><br>
            <b>URINE 2jamPP</b>
            </td>
            </tr>
            </table>
            </div>
    <?php } ?>
    <?php if($edta){ ?>
            <div class="labelPrintKecil">
            <table class="border">
            <tr>
            <td class="border">
            <?php echo $modPasien->namadepan. ' ' .$modPasien->nama_pasien; ?><br>
            No. Lab: <?php echo $modPendaftaran->no_pendaftaran; ?><br>
            <b>EDTA</b>
            </td>
            </tr>
            </table>
            </div>
    <?php } ?>
    <?php if($kimia){ ?>
            <div class="labelPrintKecil">
            <table class="border">
            <tr>
            <td class="border">
            <?php echo $modPasien->namadepan. ' ' .$modPasien->nama_pasien; ?><br>
            No. Lab: <?php echo $modPendaftaran->no_pendaftaran; ?><br>
            <b>Kimia</b>
            </td>
            </tr>
            </table>
            </div>
    <?php } ?>
        
    <?php if($feaces){ ?>
            <div class="labelPrintKecil">
            <table class="border">
            <tr>
            <td class="border">
            <?php echo $modPasien->namadepan. ' ' .$modPasien->nama_pasien; ?><br>
            No. Lab: <?php echo $modPendaftaran->no_pendaftaran; ?><br>
            <b>FEACES</b>
            </td>
            </tr>
            </table>
            </div>
    <?php } ?>        
    <?php // } ?>        
        
<!--</div>-->


