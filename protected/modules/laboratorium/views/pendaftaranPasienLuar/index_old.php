 <style>
    fieldset legend.accord1{
        width:100%;
    }
 </style>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-mp-form',
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));
//
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); 
 if(!empty($_GET['id'])){ ?> 
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
 <?php } ?>
    <fieldset>
        <legend class="rim2">Transaksi Pendaftaran Pasien Luar</legend>
        <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        <?php echo $form->errorSummary(array($modTindakanPelayananT,$modTindakanKomponen,$modHasilPemeriksaan,$model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienPenunjang,$modPengambilanSample)); ?>

        <table class='table-condensed'>
            <tr>
                <td width="10%">
                    <div class='control-group'>
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                            )); ?>
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>
                        </div>
                    </div>
                    </td>
            </tr>
        </table>
                    <?php echo $this->renderPartial('_formPasien', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien)); ?>
                
        <table><tr>
                <td width="50%">
                    <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan,'modPengambilanSample'=>$modPengambilanSample)); ?>
                </td>
                <td width="50%">
                    <?php echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form)); ?>
               
                    <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>
                </td>
        </tr></table>
        <table><tr>
            <td width="50%">
                <legend class="rim">
                    Pemeriksaan Laboratorium 
                    <i><?php echo CHtml::checkBox('isPemeriksaanLab', $model->isPemeriksaanLab, array('rel'=>'tooltip','title'=>'Pilih untuk pemeriksaan ke laboratorium','onchange'=>'updateInputLab(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                        <?php // echo CHtml::checkBox('isPemeriksaanLab', true, array('rel'=>'tooltip','title'=>'Pilih untuk pemeriksaan ke laboratorium','onchange'=>'updateInputLab(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></i>
                    <i class='icon-pencil'></i>
                </legend>
                <fieldset id="fieldsetLaboratorium" class="span6">
                    <?php //echo $form->textFieldRow($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData($model->getRuanganItems(), 'ruangan_id', 'ruangan_nama'), array('empty'=>'-- Pilih --', 'disabled'=>'disabled', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'ajax'=>array('type'=>'POST',
                                                                'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'PPPendaftaranMp')),
                                                                'update'=>'#PPPendaftaranMp_jeniskasuspenyakit_id'),
                                                                'onChange'=>'listDokterRuangan(this.value);'
                                                            )); ?>

                    <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems(), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>'hapusTindakanss();')); ?>
                    
                    <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems(Params::RUANGAN_ID_LAB), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'LKPendaftaranMp')), 
                                                    'update'=>'#'.CHtml::activeId($model,'penjamin_id').''  //selector to update
                                                ),
                        )); ?>

                    <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                   
                    
                    <?php echo $this->renderPartial('_formPengambilanSample', array('model'=>$model,'form'=>$form,'modPengambilanSample'=>$modPengambilanSample, 'modSampleLab'=>$modSampleLab)); ?>
                     
                     
                   <?php 
                    if(empty($_GET['id'])){
                    ?>
                    <fieldset id="fieldsetKarcis" class="">
                                    <legend class="accord1" >
                                        <?php echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                        Karcis Penunjang                                    </legend>
                                <?php echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                                </fieldset>
                   <br/>
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Pilih Pemeriksaan Laboratorium',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-success', 'type'=>'button',
                                                      "onclick"=>"cekValidasi();",
                                                      )); ?>
        
                    <table id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Pilih</th>
                                <th>Jenis Pemeriksaan/<br/>Pemeriksaan</th>
                                <th>Tarif</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                                <th>Cyto</th>
                                <th>Tarif Cyto</th>
                            </tr>
                        </thead>                        
                    </table>
                   <?php }?>
                </fieldset>
                   
            </td>
            <td width="50%">
                <legend class="rim">
                    Pemeriksaan Radiologi 
                    <i><?php echo CHtml::checkBox('isPemeriksaanRad', $model->isPemeriksaanRad, array('rel'=>'tooltip','title'=>'Pilih untuk pemeriksaan ke radiologi','onchange'=>'updateInputRad(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                        <?php // echo CHtml::checkBox('isPemeriksaanRad', false, array('rel'=>'tooltip','title'=>'Pilih untuk pemeriksaan ke radiologi','onchange'=>'updateInputRad(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></i>
                    <i class='icon-pencil'></i>
                </legend>
                <fieldset id="fieldsetRadiologi" class="span6">
                    <?php // echo $form->textFieldRow($modRadiologi,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($modRadiologi,'ruangan_id', CHtml::listData($modRadiologi->getRuanganItems(), 'ruangan_id', 'ruangan_nama'), array('disabled'=>'disabled','empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                            'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'ROPendaftaranMp')),
                                                              'update'=>'#ROPendaftaranMp_jeniskasuspenyakit_id'),'onChange'=>'listDokterRuangan(this.value);'
                                                            )); ?>

                    <?php echo $form->dropDownListRow($modRadiologi,'jeniskasuspenyakit_id', CHtml::listData($modRadiologi->getJenisKasusPenyakitItems(Params::RUANGAN_ID_RAD), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($modRadiologi,'kelaspelayanan_id', CHtml::listData($modRadiologi->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $form->dropDownListRow($modRadiologi,'pegawai_id', CHtml::listData($modRadiologi->getDokterItems(Params::RUANGAN_ID_RAD), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $form->dropDownListRow($modRadiologi,'carabayar_id', CHtml::listData($modRadiologi->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'ROPendaftaranMp')), 
                                                    'update'=>'#'.CHtml::activeId($modRadiologi,'penjamin_id').''  //selector to update
                                                ),
                        )); ?>
                    <?php
                    if(empty($_GET['id'])){
                    ?>
                        <?php echo $form->dropDownListRow($modRadiologi,'penjamin_id', CHtml::listData($modRadiologi->getPenjaminItems($modRadiologi->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Pilih Pemeriksaan Radiologi',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                  array('class'=>'btn btn-success', 'type'=>'button',
                                        "onclick"=>"cekValidasiRad();return false;",
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        )); ?>
                        <table class="table table-bordered table-condensed" id="tblFormPemeriksaanRad">
                        <thead>
                            <tr>
                                <th>Pilih</th>
                                <th>Pemeriksaan</th>
                                <th>Tarif</th>
                                <th>Qty</th>
                                <th>Satuan*</th>
                                <th>Cyto</th>
                                <th>Tarif Cyto</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            if(!empty($arrInputPemeriksaan)){
    //                            echo $this->renderPartial('_cekvalidPemeriksaanRad',array('modPeriksaRad'=>$modPeriksaRad,'arrInputPemeriksaan'=>$arrInputPemeriksaan));
                            ?>
                                <?php foreach ($arrInputPemeriksaan as $i => $item) { 
                                if(!empty($item['cbPemeriksaan'])) {
                                ?>
                                <tr id="periksarad_<?php echo $item['inputpemeriksaanrad']; ?>">
                                    <td>
                                        <?php echo CHtml::checkBox("permintaanPenunjang[$i][cbPemeriksaan]", true, array('value'=>$item['inputpemeriksaanrad'])); ?>
                                        <?php echo CHtml::hiddenField("permintaanPenunjang[$i][idDaftarTindakan]", $item['idDaftarTindakan'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                    </td>
                                    <td>
                                        <?php echo $item['jenisPemeriksaanNama']; 
                                              echo CHtml::hiddenField("permintaanPenunjang[$i][jenisPemeriksaanNama]", $item['jenisPemeriksaanNama'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                        <br/>
                                        <?php echo $item['pemeriksaanNama']; 
                                              echo CHtml::hiddenField("permintaanPenunjang[$i][pemeriksaanNama]", $item['pemeriksaanNama'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                        <?php echo CHtml::hiddenField("permintaanPenunjang[$i][inputpemeriksaanrad]", $item['inputpemeriksaanrad'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                    </td>
                                    <td>
                                        <?php //echo $tarif; ?>
                                        <?php echo CHtml::textField("permintaanPenunjang[$i][inputtarifpemeriksaanrad]", $item['inputtarifpemeriksaanrad'],array('class'=>'inputFormTabel lebar2-5 currency','readonly'=>true)); ?>
                                    </td>
                                    <td><?php echo CHtml::textField("permintaanPenunjang[$i][inputqty]", $item['inputqty'],array('class'=>'inputFormTabel lebar1')); ?></td>
                                    <td><?php echo CHtml::dropDownList("permintaanPenunjang[$i][satuan]", $item['satuan'], SatuanTindakan::items(),array('class'=>'inputFormTabel lebar3','empty'=>'-- Pilih --')); ?></td>
                                    <td>
                                        <?php echo CHtml::dropDownList("permintaanPenunjang[$i][cyto]", $item['cyto'],array('1'=>'Ya','0'=>'Tidak'),array('class'=>'inputFormTabel lebar2-5')); ?>
                                        <?php echo CHtml::hiddenField("permintaanPenunjang[$i][persencyto]", $item['persencyto'],array('class'=>'inputFormTabel lebar2','readonly'=>true)); ?>
                                    </td>
                                    <td><?php echo CHtml::textField("permintaanPenunjang[$i][tarifcyto]", $item['tarifcyto'],array('class'=>'inputFormTabel lebar2-5 currency','readonly'=>true)); ?></td>
                                </tr>
                                <?php } }?>
                            <?php } ?> 
                            <?php // else { echo $this->renderPartial('_formPemeriksaanRad',array('modPeriksaRad'=>$modPeriksaRad)); } ?>
                    <?php } ?>
                    </tbody>
                    </table>
                </fieldset>
            </td>
        <tr></table>
        <table>
            <?php
            if(!empty($_GET['id'])){
                        ?><br>
                        <fieldset id="fieldsetDetailPasien">
                            <legend class="rim">
                                Informasi Tagihan Pasien
                            </legend>
                         <table id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    Kategori /<br/>Tindakan
                                </th>
                                <th>
                                    Tarif
                                </th>
                                <th>
                                    Qty
                                </th>
                                <th>
                                    Tarif Cyto
                                </th>
                                <th>
                                    Sub Total
                                </th> 
                                <th>
                                    Status
                                </th> 
                            </tr>
                        </thead>
                        <?php
                        foreach ($modRincian as $i=>$row){
                        echo '<tr>
                                <td>'.$row->kategoritindakan_nama.' /'.$row->daftartindakan_nama.'
                                </td>
                                <td align=right>'.number_format($row->tarif_satuan, 0,',','.').'
                                </td>
                                <td align=center>'.$row->qty_tindakan.'
                                </td>
                                <td align=right>'.number_format($row->tarifcyto_tindakan, 0,',','.').'
                                </td>
                                <td align=right>'.number_format($row->subTotal, 0,',','.').'
                                </td>
                                <td align=center>'.((empty($row->tindakansudahbayar_id)) ? "BELUM LUNAS" : "LUNAS").'
                                </td>
                               </tr>';
                               $total += $row->subTotal;
                           }
                        ?>
                        <tfoot>
                            <tr>
                                <td colspan="4"><div class='pull-right'>Total Tagihan</div></td>
                                <td align="right"><?php echo number_format($total, 0,',','.'); ?></td>
                                <td></td>
                            </tr>
                        </tfoot>
                        </table>
                        </fieldset>
                    <?php
                        } ?>
        </table>
    </fieldset>

    <div class='form-actions'>
             <?php 
             if($model->isNewRecord){
                  echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button', 
                                                    'onKeypress'=>'return formSubmit(this,event)',
                                                    'id'=>'btn_simpan','onclick'=>'cekSatuan(this);',
                                                   )); 
//                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)', 'id'=>'btn_simpan','onclick'=>'cekSatuan(this);'));
             }else{ 
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
             }
//             echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
//                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
//                                                array('class'=>'btn btn-primary', 'type'=>'button', 
//                                                    'onKeypress'=>'return formSubmit(this,event)',
//                                                    'id'=>'btn_simpan','onclick'=>'cekSatuan(this);',
//                                                   )); 
                                                                     ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl(''.Yii::app()->controller->id.'/'.$this->action->Id.'',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
        <?php $id_pendaftaran = (isset($_GET['id']) ? $_GET['id'] :''); 
            if(!empty($id_pendaftaran) && !$model->isNewRecord){ 
            
//            echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false")); 
            echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info', 'onclick'=>'print(\'PRINT\')')); 
            
            
            }
        ?>
<?php  
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
	</div>
<?php $this->endWidget(); ?>





<?php
$caraPrint = 'PRINT';
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('id_pendaftaran'=>$id_pendaftaran, 'caraPrint'=>$caraPrint));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('printCaraPrint',$js,  CClientScript::POS_HEAD);
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoli',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlGetPemeriksaanLK = Yii::app()->createUrl('actionAjax/GetPemeriksaanLK');

$urlListKarcis =Yii::app()->createUrl('actionAjax/listKarcis');
$karcis = KonfigsystemK::getKonfigurasiKarcisPasien();
$karcis = ($karcis) ? true : 0;

$jscript = <<< JS

setTimeout(listKarcis, 3000); 
function listKarcis(obj)
{
     kelasPelayanan=$('#LKPendaftaranMp_kelaspelayanan_id').val();
     ruangan=$('#LKPendaftaranMp_ruangan_id').val();
//     if($('#isPasienLama').is(':checked'))
//        pasienLama = 1;
//     else
        pasienLama = 0;
            
     if(kelasPelayanan!=''){
             $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan, pasienLama:pasienLama },
                function(data){
                    $('#tblFormKarcis tbody').html(data.form);
                    if (${karcis}){
                        if (jQuery.isNumeric(data.karcis.karcis_id)){
                            tdKarcis = $('#tblFormKarcis tbody tr').find("td a[data-karcis='"+data.karcis.karcis_id+"']");
                            changeBackground(tdKarcis, data.karcis.daftartindakan_id, data.karcis.harga_tariftindakan, data.karcis.karcis_id);
                        }
                    }
             }, "json");
     }      
       
}

function changeBackground(obj,idTindakan,tarifSatuan,idKarcis)
{
        banyakRow=$('#tblFormKarcis tr').length;
        for(i=1; i<=banyakRow; i++){
        $('#tblFormKarcis tr').css("background-color", "#FFFFFF");          
        } 
             
        $(obj).parent().parent().css("backgroundColor", "#00FF00");     
        $('#TindakanPelayananT_idTindakan').val(idTindakan);  
        $('#TindakanPelayananT_tarifSatuan').val(tarifSatuan);     
        $('#TindakanPelayananT_idKarcis').val(idKarcis);     
     
}
   
function cekValidasi(){
    
    var jenisIdentitas  = $('#LKPasienM_jenisidentitas').val();
    var noIdentitas     = $('#LKPasienM_no_identitas_pasien').val();
    var namaDepan       = $('#LKPasienM_namadepan').val();
    var namaPasien      = $('#LKPasienM_nama_pasien').val();
    var jenisKelamin    = true;
    var kelasPelayanan  = $('#LKPendaftaranMp_kelaspelayanan_id').val();
    var alamatPasien    = $('#LKPasienM_alamat_pasien').val();
    var jenisPenyakit   = $('#LKPendaftaranMp_jeniskasuspenyakit_id').val();
    var dokter          = $('#LKPendaftaranMp_pegawai_id').val();
    var caraBayar       = $('#LKPendaftaranMp_carabayar_id').val(); 
    var penjamin        = $('#LKPendaftaranMp_penjamin_id').val();
    
                    
    //=====validasi jenis kelamin
    if($('input[name="LKPasienM[jeniskelamin]"]:checked').length == 0) {
        jenisKelamin = false;
    }
    
    //=======validasi sample
    var sampleLab       = $('#LKPengambilanSampleT_samplelab_id').val();
    var jumlahSample    = $('#LKPengambilanSampleT_jmlpengambilansample').val();
    if ($('input[name="pakeSample"]').is(':checked')) {
        if(sampleLab && jumlahSample){
            sample = true;
        }else{
            sample = false;
        }
    }else{
       sample = true;
    }
    
    //======validasi rujukan
    var asalRujukan     = $('#LKRujukanT_asalrujukan_id').val();
    var noRujukan       = $('#LKRujukanT_no_rujukan').val();
    var rujukanDari       = $('#LKRujukanT_rujukandari_id').val();
    
//    if ($('input[name="isRujukan"]').is(':checked')) {
//       if(asalRujukan && noRujukan){
//            rujukan = true;
//        }else{
//            rujukan = false;
//        }
//    } else {
//        rujukan = true;
//    }

//    alert(jenisIdentitas +'-'+ noIdentitas +'-'+ namaDepan +'-'+ namaPasien +'-'+ jenisKelamin +'-'+ alamatPasien +'-'+ jenisPenyakit +'-'+ dokter +'-'+ 
//        caraBayar +'-'+ penjamin +'-'+ sampleLab +'-'+ jumlahSample +'-'+ asalRujukan +'-'+ noRujukan) ;               
//                    exit();
    if (jQuery.isNumeric(kelasPelayanan) && jenisIdentitas && noIdentitas && namaDepan && namaPasien && jenisKelamin && alamatPasien && jenisPenyakit && dokter && 
        caraBayar && penjamin && asalRujukan && noRujukan && sample)
    {     
                    
          $('#dialogPemeriksaanLab').dialog('open');
    }
    else{
        if(!(jenisIdentitas && noIdentitas && namaDepan && namaPasien && jenisKelamin && alamatPasien))
            alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda * pada Form Data Pasien');
        else if (!(asalRujukan && noRujukan))
            alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda * pada Form Rujukan');
        else if(!( sample))
            alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda * pada Form Sampel');
        else if(!(jenisPenyakit && dokter && caraBayar && penjamin))
            alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda * pada Form Pemeriksaan Laboratorium');
        else
            alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda *');
    }
}

// Validasi sebelum tindakan pelayanan Radiologi
function cekValidasiRad(){
    
    var jenisIdentitas  = $('#LKPasienM_jenisidentitas').val();
    var noIdentitas     = $('#LKPasienM_no_identitas_pasien').val();
    var namaDepan       = $('#LKPasienM_namadepan').val();
    var namaPasien      = $('#LKPasienM_nama_pasien').val();
    var jenisKelamin    = true;
    var kelasPelayanan  = $('#ROPendaftaranMp_kelaspelayanan_id').val();
    var alamatPasien    = $('#LKPasienM_alamat_pasien').val();
    var jenisPenyakit   = $('#ROPendaftaranMp_jeniskasuspenyakit_id').val();
    var dokter          = $('#ROPendaftaranMp_pegawai_id').val();
    var caraBayar       = $('#ROPendaftaranMp_carabayar_id').val(); 
    var penjamin        = $('#ROPendaftaranMp_penjamin_id').val();
    
                    
    //=====validasi jenis kelamin
    if($('input[name="LKPasienM[jeniskelamin]"]:checked').length == 0) {
        jenisKelamin = false;
    }
    
    //======validasi rujukan
    var asalRujukan     = $('#LKRujukanT_asalrujukan_id').val();
    var noRujukan       = $('#LKRujukanT_no_rujukan').val();
    var rujukanDari       = $('#LKRujukanT_rujukandari_id').val();
    
    if (jQuery.isNumeric(kelasPelayanan) && jenisIdentitas && noIdentitas && namaDepan && namaPasien && jenisKelamin && alamatPasien && jenisPenyakit && dokter && 
        caraBayar && penjamin && asalRujukan && noRujukan)
    {
          $('#dialogPemeriksaanRad').dialog('open');
    }else{
        if(!(jenisIdentitas && noIdentitas && namaDepan && namaPasien && jenisKelamin && alamatPasien))
            alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda * pada Form Data Pasien');
        else if (!(asalRujukan && noRujukan))
            alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda * pada Form Rujukan');
        else if(!(jenisPenyakit && dokter && caraBayar && penjamin))
            alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda * pada Form Pemeriksaan Radiologi');
        else
            alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda *');
    }
}                    
                    
function cekSatuan(obj)
{
   var satuan = 0;
   var cekSatuanKosong = 1;
   var cekSatuanKosongRad = 1;
   if(document.getElementById('isPemeriksaanLab').checked == true){
        satuan =$('#satuan').length;
        cekSatuanKosong=1;
        if (satuan=='0'){
             alert('Anda Belum Memasukan Tindakan Pemeriksaan Laboratorium');
             return false;
         }else{

             $('#satuan').each(function() {
                 cekBox =$(this).parent().prev().prev().prev().prev().find('input:checkbox').is(':checked');   
                 isiSatuan = this.value;
                 if(cekBox==true){
                     if(isiSatuan==''){
                         alert('Anda Belum Memilih Satuan');
                         cekSatuanKosong=0;
                         return false;
                     }
                 }
             });

         }
         if (cekSatuanKosong==1){
                $('#pppendaftaran-mp-form').submit();  
         }
   }
   if(document.getElementById('isPemeriksaanRad').checked == true){
        satuan =$('.satuanRad').length;
        cekSatuanKosongRad=1;
        if (satuan=='0'){
             alert('Anda Belum Memasukan Tindakan Pemeriksaan Radiologi');
             return false;
         }else{

             $('.satuanRad').each(function() {
                 cekBox =$(this).parent().prev().prev().prev().prev().find('input:checkbox').is(':checked');   
                 isiSatuan = this.value;
                 if(cekBox==true){
                     if(isiSatuan==''){
                         alert('Anda Belum Memilih Satuan');
                         cekSatuanKosongRad=0;
                         return false;
                     }
                 }
             });

         }
         if (cekSatuanKosongRad==1){
                $('#pppendaftaran-mp-form').submit();  
         }
   }
   
 }   
 function hapusTindakanss()
    {
        banyakRow=$('#tblFormPemeriksaanLab tr').length;
        
        for(i=2; i<=banyakRow; i++){
        $('#tblFormPemeriksaanLab tr:last').remove();
        }
        
          $('.box input').removeAttr('checked');
    }
JS;
Yii::app()->clientScript->registerScript('periksa satuan',$jscript, CClientScript::POS_HEAD);

$jscript = <<< JS

//function print(idPendaftaran)
//{
//        if(document.getElementById('isPasienLama').checked == true){
//            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=280');
//        }else{
//            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');
//            window.open('${urlPrintKartuPasien}'+idPendaftaran,'printwi','left=100,top=100,width=400,height=280');
//        }
//}

function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#PPPendaftaranMp_pegawai_id').html(data.listDokter);
    }, "json");
}

function getPemeriksaanLK(obj)
{
    $.post("${urlGetPemeriksaanLK}", { idJenisPemeriksaan: obj.value },
        function(data){

            $('.boxtindakan').html(data.pemeriksaan);
    }, "json");
}

function caraAmbilPhotoJS(obj)
{
    caraAmbilPhoto=obj.value;
    
    if(caraAmbilPhoto=='webCam')
        {
          $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
            
        }
    else
        {
         $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
        }
} 
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
<script type="text/javascript">
    
$('#formPeriksaLab').tile({widths : [ 190 ]});

function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanlab = obj.value;
        var kelasPelayan_id = $('#LKPendaftaranMp_kelaspelayanan_id').val();
        
        if(kelasPelayan_id==''){
                $(obj).attr('checked', 'false');
                alert('Anda Belum Memilih Kelas Pelayanan');
                
            }
        else{
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanLabPendLab')?>',
                 'data':{idPemeriksaanlab:idPemeriksaanlab,kelasPelayan_id:kelasPelayan_id},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         if($.trim(data.form)=='')
                         {
                            $(obj).removeAttr('checked');
                            alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                         } else if($.trim(data.form)=='tarif kosong') {
                            $(obj).removeAttr('checked');
                            data.form = '';
                            alert ('Pemeriksaan belum memiliki tarif');
                         }
                         $('#tblFormPemeriksaanLab').append(data.form);
                 } ,
                 'cache':false});
        }     
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idPemeriksaanlab)
{
    $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).detach();
}

function cekcyto(obj, x){
//    console.log(x);
//    tidakan = $('.cyto_tindakan').val();
    if(obj.value == 1){
        $('.cyto_' + x).show();
    }else
        {
            $('.cyto_' + x).hide();
            
        }
}
</script>

<?php
$js = <<< JS
$('.numberOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

 <?php  $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogLaboratorium',
            'options'=>array(
                'title'=>'Pemeriksaan Laboratorium',
                'autoOpen'=>false,
                'width'=>450,
                'height'=>450,
                'modal'=>false,
                'hide'=>'explode',
                'resizelable'=>false,
            ),
        ));
        ?>
<?php echo $this->renderPartial('_formLabolatorium', array('modPeriksaLab'=>$modPeriksaLab,'modJenisPeriksaLab'=>$modJenisPeriksaLab)); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>

<script>
var selObj = document.getElementById('LKPendaftaranMp_ruangan_id');
selObj.selectedIndex = 1;
var checkBox = document.getElementById('isPemeriksaanLab');
if(!checkBox.checked){
//    $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);
    $('#fieldsetLaboratorium input').attr('disabled','true');
    $('#fieldsetLaboratorium #isUpdatePasien').removeAttr('disabled');
    $('#fieldsetLaboratorium select').attr('disabled','true');
    $('#fieldsetLaboratorium textarea').attr('disabled','true');
    $('#fieldsetLaboratorium button').attr('disabled','true');
    $('#LKPendaftaranMp_ruangan_id').attr('disabled','true');
    selObj.selectedIndex = 0;
}else{
    $('#fieldsetLaboratorium input').removeAttr('disabled');
    $('#fieldsetLaboratorium select').removeAttr('disabled');
    $('#fieldsetLaboratorium textarea').removeAttr('disabled');
    $('#fieldsetLaboratorium button').removeAttr('disabled');
    $('#fieldsetDetailPasien input').removeAttr('disabled');
    $('#fieldsetDetailPasien select').removeAttr('disabled');
    $('#LKPendaftaranMp_ruangan_id').attr('disabled','true');
//    $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);
    $('#fieldsetKarcisLab').show();
    $('#fieldsetTabelPemeriksaanLab').show();
    selObj.selectedIndex = 1;
}
//var noSampel = $('#LKPengambilanSampleT_no_pengambilansample').val();  
function updateInputLab(obj)
{
    
    selObj = document.getElementById('LKPendaftaranMp_ruangan_id');
    if(!obj.checked) {
        obj.checked = cekPilihRuangan();
        //Reset Form
        if(!obj.checked){
//            $('#fieldsetLaboratorium input').val('');
//            $('#fieldsetLaboratorium #isUpdatePasien').val('');
//            $('#fieldsetLaboratorium select').val('');
//            $('#fieldsetLaboratorium textarea').val('');
//            $('#fieldsetLaboratorium button').val('');
//            $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);

            $('#fieldsetLaboratorium input').attr('disabled','true');
            $('#fieldsetLaboratorium #isUpdatePasien').removeAttr('disabled');
            $('#fieldsetLaboratorium select').attr('disabled','true');
            $('#fieldsetLaboratorium textarea').attr('disabled','true');
            $('#fieldsetLaboratorium button').attr('disabled','true');
            $('#LKPendaftaranMp_ruangan_id').attr('disabled','true');
//            $('#fieldsetKarcisLab').hide();
//            $('#fieldsetTabelPemeriksaanLab').hide();
    //        $('#LKPendaftaranMp_ruangan_id_lab').removeAttr('disabled');
            selObj.selectedIndex = 0;
        }
    }
    else {
        $('#fieldsetLaboratorium input').removeAttr('disabled');
        $('#fieldsetLaboratorium select').removeAttr('disabled');
        $('#fieldsetLaboratorium textarea').removeAttr('disabled');
        $('#fieldsetLaboratorium button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#LKPendaftaranMp_ruangan_id').attr('disabled','true');
//        $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);
        $('#fieldsetKarcisLab').show();
        $('#fieldsetTabelPemeriksaanLab').show();
        //Jika inputLab di cek pilih Laboratorium
	selObj.selectedIndex = 1;
        
    }
}
</script>

<script>
var selObj = document.getElementById('ROPendaftaranMp_ruangan_id');
selObj.selectedIndex = 1;
var checkBox = document.getElementById('isPemeriksaanRad');
if(!checkBox.checked){
    formNonAktif();
}else{
    $('#fieldsetRadiologi input').removeAttr('disabled');
        $('#fieldsetRadiologi select').removeAttr('disabled');
        $('#fieldsetRadiologi textarea').removeAttr('disabled');
        $('#fieldsetRadiologi button').removeAttr('disabled');
        $('#fieldsetRadiologi checkbox').removeAttr('disabled');
        $('#ROPendaftaranMp_ruangan_id').attr('disabled','true');
        selObj.selectedIndex = 1;
}
function updateInputRad(obj)
{
    selObj = document.getElementById('ROPendaftaranMp_ruangan_id');
    if(!obj.checked) {
        //Reset Form
        obj.checked = cekPilihRuangan();
        if(!obj.checked)
            formNonAktif();        
    }
    else {
        $('#fieldsetRadiologi input').removeAttr('disabled');
        $('#fieldsetRadiologi select').removeAttr('disabled');
        $('#fieldsetRadiologi textarea').removeAttr('disabled');
        $('#fieldsetRadiologi button').removeAttr('disabled');
        $('#fieldsetRadiologi checkbox').removeAttr('disabled');
        
        $('#ROPendaftaranMp_ruangan_id').attr('disabled','true');
//        $('#btnPeriksaRad').show();
        selObj.selectedIndex = 1;
        
    }
}
function formNonAktif(){
//    $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);

    $('#fieldsetRadiologi input').attr('disabled','true');
    $('#fieldsetRadiologi #isUpdatePasien').removeAttr('disabled');
    $('#fieldsetRadiologi select').attr('disabled','true');
    $('#fieldsetRadiologi textarea').attr('disabled','true');
    $('#fieldsetRadiologi button').attr('disabled','true');
    $('#fieldsetRadiologi checkbox').removeAttr('disabled');
//    $('#btnPeriksaRad').hide();
    $('#ROPendaftaranMp_ruangan_id').attr('disabled','true');
    selObj.selectedIndex = 0;
}


</script>
<?php  
//========================== Dialog Pemeriksaan Lab =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
           'id'=>'dialogPemeriksaanLab',
           'options'=>array(
               'title'=>'Pemeriksaan Laboratorium',
               'autoOpen'=>false,
               'width'=>450,
               'height'=>450,
               'modal'=>false,
               'hide'=>'explode',
               'resizelable'=>false,
           ),
       ));
       ?>
<?php echo $this->renderPartial('_formLabolatorium', array('modPeriksaLab'=>$modPeriksaLab,'modJenisPeriksaLab'=>$modJenisPeriksaLab)); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>

<?php 
//========= Dialog buat Input Pemeriksaan Radiologi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPemeriksaanRad',
    'options'=>array(
        'title'=>'Pilih Pemeriksaan Radiologi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'resizable'=>false,
//        'position'=>'top',
    ),
)); ?>
    <div class="box">
        <?php 
            $jenisPeriksa = '';
            foreach($modPeriksaRad as $i=>$pemeriksaan){ 
                $ceklist = false;
                if(!empty($arrInputPemeriksaan)){
                    $ceklist = cekPilihan($pemeriksaan->pemeriksaanrad_id,$arrInputPemeriksaan);
                }
                if($jenisPeriksa != $pemeriksaan->pemeriksaanrad_jenis){
                    echo ($jenisPeriksa!='') ? "</div>" : "";
                    $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                    echo "<div class='boxtindakan'>";
                    echo "<h6>$pemeriksaan->pemeriksaanrad_jenis</h6>";
                    echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                             'onclick' => "inputPeriksaRad(this)"));
                    echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                } else {
                    $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                    echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                             'onclick' => "inputPeriksaRad(this)"));
                    echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                }
            } echo "</div>";
        ?>               
    </div>

<?php $this->endWidget();
//========= end Input Pemeriksaan Radiologi dialog =============================
?>
<script>
    function cekPilihRuangan(){
        var isLab = document.getElementById('isPemeriksaanLab').checked;
        var isRad = document.getElementById('isPemeriksaanRad').checked;
        if(isLab || isRad){
        }else{
            alert ("Anda harus memilih minimal satu ruangan !");
            return true;
        }
    }
</script>

<script>
function inputPeriksaRad(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanrad = obj.value;
        var idKelasPelayanan = $('#ROPendaftaranMp_kelaspelayanan_id').val();
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRadPendRad')?>',
                 'data':{idPemeriksaanrad:idPemeriksaanrad, idKelasPelayanan:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                     if (data.form == ''){ 
                         $(obj).removeAttr("checked");
                         alert("Tarif tindakan kosong");
                     }else{
                         $('#tblFormPemeriksaanRad').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','cbPemeriksaan');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                     }
                 } ,
                 'cache':false});
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idPemeriksaanrad)
{
    $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).detach();
}



function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanRad tr').length;
    var i = -1;
    $('#tblFormPemeriksaanRad tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}
function setKelasPelayanan(){
    if (jQuery.isNumeric($("#ROPendaftaranMp_kelaspelayanan_id").val())){
        $('#dialogPemeriksaanRad').dialog('open');
    }else{
        alert("Kelas Pelayanan harap diisi terlebih dahulu");
    }
}
$("form#ropendaftaran-mp-form").submit(function(){
    jumlah = $("#tblFormPemeriksaanRad tbody tr").length;
    if (jumlah > 0 ){return true;} else{ alert("Pemeriksaan harap diisi");return false;};
});
</script>