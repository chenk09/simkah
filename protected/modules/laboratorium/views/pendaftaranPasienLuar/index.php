<style>
    fieldset legend.accord1{
        width:100%;
    }
    table{
        width: 980px;
    }
 </style>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
    $this->widget('application.extensions.moneymask.MMask',array(
        'element'=>'.currency',
        'currency'=>'PHP',
        'config'=>array(
            'defaultZero'=>true,
            'allowZero'=>true,
            'precision'=>0,
        )
    ));
?>
<?php 
    if(isset($_GET['status'])){
        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
        // if($_GET['status'] == 2)
        //     Yii::app()->user->setFlash('warning',"Ada Data Pemeriksaan yang Gagal Disimpan karena detail pemeriksaannya tidak memiliki nilai rujukan. Silahkan Cek Di Hasil Pemeriksaan dan Nilai Rujukan !");
    }
?>
    
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-mp-form',
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'balikFormat(); return (cekValidasiFormUtama() && cekSatuan()); '), //cekSatuan(); 
));
//
?>
<?php
//Di comment karena bentrok dengan widget CMaskedTextField untuk umur
//$this->widget('application.extensions.moneymask.MMask',array(
//    'element'=>'.currency',
//    'currency'=>'PHP',
//    'config'=>array(
//        'symbol'=>'Rp. ',
////        'showSymbol'=>true,
////        'symbolStay'=>true,
//        'defaultZero'=>true,
//        'allowZero'=>true,
//        'precision'=>0,
//    )
//));
//
//$this->widget('application.extensions.moneymask.MMask',array(
//    'element'=>'.number',
//    'config'=>array(
//        'defaultZero'=>true,
//        'allowZero'=>true,
//        'precision'=>0,
//        'thousands'=>'',
//    )
//));
?>

<?php $this->widget('bootstrap.widgets.BootAlert'); ?> 
    <fieldset>
        <legend class="rim2">Transaksi Pendaftaran Pasien Luar</legend>
        <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        <?php echo $form->errorSummary(array($modTindakanPelayananT,$modTindakanKomponen,$modHasilPemeriksaan,$model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienPenunjang,$modPengambilanSample)); ?>

        <table class='table-condensed'>
            <tr>
                <td width="50%">
                    <div class="control-group" id="controlNoRekamMedik">
                        <label class="control-label">
                            <?php 
                                    $enable['readonly'] = true;
                                    $readOnly = ($model->isPasienLama) ? '' : $enable ; 
                            ?>
                           <i class="icon-user"></i> <?php echo CHtml::checkBox('isPasienLama', $model->isPasienLama, array('rel'=>'tooltip','title'=>'Pilih jika pasien lama','onclick'=>'enableInputPasien(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                            No. Rekam Medik
                        </label>
                        <div class="controls">
                            <?php 
                            $this->widget('MyJuiAutoComplete',array(
                                        'name'=>'noRekamMedik',
                                        'value'=>$model->noRekamMedik,
//                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienLamaLab'),
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienLamaLab'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#noRekamMedik").val( ui.item.value );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);
                                                $("#'.CHtml::activeId($modPasien,'jenisidentitas').'").val(ui.item.jenisidentitas);
                                                $("#'.CHtml::activeId($modPasien,'no_identitas_pasien').'").val(ui.item.no_identitas_pasien);
                                                $("#'.CHtml::activeId($modPasien,'namadepan').'").val(ui.item.namadepan);
                                                $("#'.CHtml::activeId($modPasien,'nama_pasien').'").val(ui.item.nama_pasien);
                                                $("#'.CHtml::activeId($modPasien,'nama_bin').'").val(ui.item.nama_bin);
                                                $("#'.CHtml::activeId($modPasien,'tempat_lahir').'").val(ui.item.tempat_lahir);
                                                $("#'.CHtml::activeId($modPasien,'tanggal_lahir').'").val(ui.item.tanggal_lahir);
                                                $("#'.CHtml::activeId($modPasien,'kelompokumur_id').'").val(ui.item.kelompokumur_id);
                                                $("#'.CHtml::activeId($modPasien,'jeniskelamin').'").val(ui.item.jeniskelamin);
                                                setJenisKelaminPasien(ui.item.jeniskelamin);
                                                setRhesusPasien(ui.item.rhesus);
                                                var kelurahan = String.trim(\'ui.item.kelurahan_id\');
                                                if(kelurahan.length == 0){
                                                    kelurahan = 0
                                                }
                                                loadDaerahPasien(ui.item.propinsi_id, ui.item.kabupaten_id, ui.item.kecamatan_id, kelurahan);
                                                $("#'.CHtml::activeId($modPasien,'statusperkawinan').'").val(ui.item.statusperkawinan);
                                                $("#'.CHtml::activeId($modPasien,'golongandarah').'").val(ui.item.golongandarah);
                                                $("#'.CHtml::activeId($modPasien,'rhesus').'").val(ui.item.rhesus);
                                                $("#'.CHtml::activeId($modPasien,'alamat_pasien').'").val(ui.item.alamat_pasien);
                                                $("#'.CHtml::activeId($modPasien,'rt').'").val(ui.item.rt);
                                                $("#'.CHtml::activeId($modPasien,'rw').'").val(ui.item.rw);
                                                $("#'.CHtml::activeId($modPasien,'propinsi_id').'").val(ui.item.propinsi_id);
                                                $("#'.CHtml::activeId($modPasien,'kabupaten_id').'").val(ui.item.kabupaten_id);
                                                $("#'.CHtml::activeId($modPasien,'kecamatan_id').'").val(ui.item.kecamatan_id);
                                                $("#'.CHtml::activeId($modPasien,'kelurahan_id').'").val(ui.item.kelurahan_id);
                                                $("#'.CHtml::activeId($modPasien,'no_telepon_pasien').'").val(ui.item.no_telepon_pasien);
                                                $("#'.CHtml::activeId($modPasien,'no_mobile_pasien').'").val(ui.item.no_mobile_pasien);
                                                $("#'.CHtml::activeId($modPasien,'suku_id').'").val(ui.item.suku_id);
                                                $("#'.CHtml::activeId($modPasien,'alamatemail').'").val(ui.item.alamatemail);
                                                $("#'.CHtml::activeId($modPasien,'anakke').'").val(ui.item.anakke);
                                                $("#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'").val(ui.item.jumlah_bersaudara);
                                                $("#'.CHtml::activeId($modPasien,'pendidikan_id').'").val(ui.item.pendidikan_id);
                                                $("#'.CHtml::activeId($modPasien,'pekerjaan_id').'").val(ui.item.pekerjaan_id);
                                                $("#'.CHtml::activeId($modPasien,'agama').'").val(ui.item.agama);
                                                $("#'.CHtml::activeId($modPasien,'warga_negara').'").val(ui.item.warga_negara);
                                                loadUmur(ui.item.tanggal_lahir);
                                                return false;
                                            }',

                                        ),
                                        'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                                        'htmlOptions'=>array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)"),//,'class'=>'numberOnly'
                            )); ?>



                            <?php echo $form->error($model, 'noRekamMedik'); ?>
                        </div>
                    </div>
                    
                    <fieldset id="caraBayar" class="span6">
                    <legend class="accord1">Cara Bayar</legend>
                    <?php 
                    echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItemsLab(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'LKPendaftaranMp')), 
                                                    'update'=>'#'.CHtml::activeId($model,'penjamin_id').''  //selector to update
                                                ),
                                                'onchange'=>'caraBayarChange(this)',
                        )); ?>
                    <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                    </fieldset>
                </td>
                <td width="50%">
                    <div class='control-group'>
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                            )); ?>
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>
                        </div>
                    </div>

                    <fieldset id="noLab" class="span6">
                    <legend style="margin:13% 0 0 9%;font-size:35pt;"><?php echo $model->no_pendaftaran; ?></legend>
                    </fieldset>
                </td>
            </tr>
        </table>
                    <?php echo $this->renderPartial('_formPasien', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien)); ?>
                
        <table><tr>
                <td width="50%">
                    <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan,'modPengambilanSample'=>$modPengambilanSample)); ?>
                </td>
                <td width="50%">
                    
                    
                    <?php echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form)); ?>
               
                    <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>
                </td>
        </tr></table>
        <table><tr>
                <td>
                    <legend class="rim">
                        Pemeriksaan Laboratorium 
                        <i><?php echo CHtml::checkBox('isPemeriksaanLab', $model->isPemeriksaanLab, array('rel'=>'tooltip','title'=>'Pilih untuk pemeriksaan ke laboratorium','onchange'=>'updateInputLab(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'onclick'=>'hitungTotalSeluruh();')); ?>
                        <i class='icon-pencil'></i>
                    </legend>
                </td>
                <td>
                    <legend class="rim">
                        Pemeriksaan Radiologi 
                        <i><?php echo CHtml::checkBox('isPemeriksaanRad', $model->isPemeriksaanRad, array('rel'=>'tooltip','title'=>'Pilih untuk pemeriksaan ke radiologi','onchange'=>'updateInputRad(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'onclick'=>'hitungTotalSeluruh();')); ?>
                        <i class='icon-pencil'></i>
                    </legend>
                </td>
        </tr></table>
        <table><tr>
            <td width="50%">

                <fieldset id="fieldsetLaboratorium" class="span6">
                    <?php //echo $form->textFieldRow($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php $form->dropDownListRow($model,'ruangan_id', CHtml::listData($model->getRuanganItems(), 'ruangan_id', 'ruangan_nama'), array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'ajax'=>array('type'=>'POST',
                                                                'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'PPPendaftaranMp')),
                                                                'update'=>'#PPPendaftaranMp_jeniskasuspenyakit_id'),
                                                                'onChange'=>'listDokterRuangan(this.value);'
                                                            )); ?>

                    <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems(), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>'hapusTindakanss();')); ?>
                    
                    <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems(Params::RUANGAN_ID_LAB), 'pegawai_id', 'namaLengkap') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $this->renderPartial('_formPengambilanSample', array('model'=>$model,'form'=>$form,'modPengambilanSample'=>$modPengambilanSample, 'modSampleLab'=>$modSampleLab)); ?>
                     
                     
                   
<!--                    <fieldset id="fieldsetKarcis" class="">
                        <legend class="accord1" >
                            <?php // echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                            Karcis Penunjang                                    </legend>
                    <?php // echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                    </fieldset>-->
                   <?php if(!isset($_GET['id'])){ ?>
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Pilih Pemeriksaan Laboratorium',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-success', 'type'=>'button',
                                                      "onclick"=>"cekValidasi();",
                                                      )); ?>
                   
                    <table id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Pilih</th>
                                <th>Jenis Pemeriksaan/<br/>Pemeriksaan</th>
                                <th>Tarif</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                                <th>Cyto</th>
                                <th>Tarif Cyto</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                   
                   
                        <table class="table table-bordered table-condensed">
                            <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan Laboratorium</td><td><?php echo CHtml::textField('periksaTotalLab', '0',array('class'=>'span2', 'style'=>'text-align:right;', 'readonly'=>true,'disabled'=>'disabled'));?></td></tr>
                        </table>
                        <table class="table table-bordered table-condensed" id="tblFormKarcisLab">
                            <thead>
                                <tr>
                                    <th>Pilih</th>
                                    <th>Biaya Lainnya</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                   <?php } ?>
                </fieldset>
                   
            </td>
            <td width="50%">

                <fieldset id="fieldsetRadiologi" class="span6">
                    <?php // echo $form->textFieldRow($modRadiologi,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php $form->dropDownListRow($modRadiologi,'ruangan_id', CHtml::listData($modRadiologi->getRuanganItems(), 'ruangan_id', 'ruangan_nama'), array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                            'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'ROPendaftaranMp')),
                                                              'update'=>'#ROPendaftaranMp_jeniskasuspenyakit_id'),'onChange'=>'listDokterRuangan(this.value);'
                                                            )); ?>

                    <?php echo $form->dropDownListRow($modRadiologi,'jeniskasuspenyakit_id', CHtml::listData($modRadiologi->getJenisKasusPenyakitItems(Params::RUANGAN_ID_RAD), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($modRadiologi,'kelaspelayanan_id', CHtml::listData($modRadiologi->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $form->dropDownListRow($modRadiologi,'pegawai_id', CHtml::listData($modRadiologi->getDokterItems(Params::RUANGAN_ID_RAD), 'pegawai_id', 'namaLengkap') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php
                    if(empty($_GET['id'])){
                    ?>
                        <?php // echo $form->dropDownListRow($modRadiologi,'penjamin_id', CHtml::listData($modRadiologi->getPenjaminItems($modRadiologi->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Pilih Pemeriksaan Radiologi',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                  array('class'=>'btn btn-success', 'type'=>'button',
                                        "onclick"=>"cekValidasiRad();return false;",
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        )); ?>
                        <table class="table table-bordered table-condensed" id="tblFormPemeriksaanRad">
                        <thead>
                            <tr>
                                <th>Pilih</th>
                                <th>Pemeriksaan</th>
                                <th>Tarif</th>
                                <th>Qty</th>
                                <th>Satuan*</th>
                                <th>Cyto</th>
                                <th>Tarif Cyto</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            if(!empty($arrInputPemeriksaan)){
    //                            echo $this->renderPartial('_cekvalidPemeriksaanRad',array('modPeriksaRad'=>$modPeriksaRad,'arrInputPemeriksaan'=>$arrInputPemeriksaan));
                            ?>
                                <?php foreach ($arrInputPemeriksaan as $i => $item) { 
                                if(!empty($item['cbPemeriksaan'])) {
                                ?>
                                <tr id="periksarad_<?php echo $item['inputpemeriksaanrad']; ?>">
                                    <td>
                                        <?php echo CHtml::checkBox("permintaanPenunjang[$i][cbPemeriksaan]", true, array('value'=>$item['inputpemeriksaanrad'])); ?>
                                        <?php echo CHtml::hiddenField("permintaanPenunjang[$i][idDaftarTindakan]", $item['idDaftarTindakan'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                    </td>
                                    <td>
                                        <?php echo $item['jenisPemeriksaanNama']; 
                                              echo CHtml::hiddenField("permintaanPenunjang[$i][jenisPemeriksaanNama]", $item['jenisPemeriksaanNama'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                        <br/>
                                        <?php echo $item['pemeriksaanNama']; 
                                              echo CHtml::hiddenField("permintaanPenunjang[$i][pemeriksaanNama]", $item['pemeriksaanNama'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                        <?php echo CHtml::hiddenField("permintaanPenunjang[$i][inputpemeriksaanrad]", $item['inputpemeriksaanrad'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                    </td>
                                    <td>
                                        <?php //echo $tarif; ?>
                                        <?php echo CHtml::textField("permintaanPenunjang[$i][inputtarifpemeriksaanrad]", $item['inputtarifpemeriksaanrad'],array('class'=>'inputFormTabel lebar2-5 currency tarifRad','readonly'=>true)); ?>
                                    </td>
                                    <td><?php echo CHtml::textField("permintaanPenunjang[$i][inputqty]", $item['inputqty'],array('class'=>'inputFormTabel lebar1 qtyRad')); ?></td>
                                    <td><?php echo CHtml::dropDownList("permintaanPenunjang[$i][satuan]", $item['satuan'], SatuanTindakan::items(),array('class'=>'inputFormTabel lebar3','empty'=>'-- Pilih --')); ?></td>
                                    <td>
                                        <?php echo CHtml::dropDownList("permintaanPenunjang[$i][cyto]", $item['cyto'],array('1'=>'Ya','0'=>'Tidak'),array('class'=>'inputFormTabel lebar2-5 isCytoRad')); ?>
                                        <?php echo CHtml::hiddenField("permintaanPenunjang[$i][persencyto]", $item['persencyto'],array('class'=>'inputFormTabel lebar2 persenCytoRad','readonly'=>true)); ?>
                                    </td>
                                    <td><?php echo CHtml::textField("permintaanPenunjang[$i][tarifcyto]", $item['tarifcyto'],array('class'=>'inputFormTabel lebar2-5 currency cytoRad','readonly'=>true)); ?></td>
                                </tr>
                                    <?php } }?>
                                <?php } ?> 
                                <?php // else { echo $this->renderPartial('_formPemeriksaanRad',array('modPeriksaRad'=>$modPeriksaRad)); } ?>
                        <?php } ?>
                        
                        </tbody>
                        </table>
                        <?php if(!isset($_GET['id'])){ ?>
                        <table class="table table-bordered table-condensed">
                            <tr><td width="70%" style="text-align: right; ">Total Biaya Pemeriksaan Radiologi</td><td><?php echo CHtml::textField('periksaTotalRad', '0',array('class'=>'span2', 'style'=>'text-align:right;', 'readonly'=>true, 'disabled'=>'disabled'));?></td></tr>
                        </table>
                        <table id="tblFormKarcisRad" class="table table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th>Pilih</th>
                                    <th>Biaya Lainnya</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>                       
                        <?php } ?>
                 
                </fieldset>
                
            </td>
        </tr>
        </table>
        <table>
            <tr>
                <td>
                    <br>
                    <?php
                    //FORM REKENING
                        $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                            array(
                                'form'=>$form,
                                'modRekenings'=>$modRekenings,
                            )
                        );
                    ?>
                </td>
            </tr>
        </table>
        
        
        <?php if(!isset($_GET['id'])){ ?>
            <table>
                <tr><td><center><?php // echo $this->renderPartial('_formBiayaAdministrasi', array('form'=>$form,'model'=>$model)); ?> </center></td></tr>
                <tr><td style="text-align: center; font-weight:bold; font-size: 12pt;">Total Biaya Seluruh Pemeriksaan = <?php echo CHtml::textField('periksaTotal', '0',array('class'=>'span2', 'style'=>'text-align:right;font-size:11pt;', 'disabled'=>'disabled'));?></td></tr>
            </table>
        <?php } ?>
        <table>
            <?php
            if(!empty($_GET['id'])){
                        ?><br>
                        <fieldset id="fieldsetDetailPasien">
                            <legend class="rim">
                                Informasi Tagihan Pasien
                            </legend>
                         <table id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Jenis Pemeriksaan</th>
                                <th>Nama Pemeriksaan</th>
                                <th>Tarif</th>
                                <th>Qty</th>
                                <th>Tarif Cyto</th>
                                <th>Sub Total</th> 
                                <!--<th>Status</th>--> 
                            </tr>
                        </thead>
                        <?php
                            $daftar_tindakan_id = null;
                            foreach ($modRincian as $i=>$row){
                                //if($row->daftartindakan_id == Params::PAKET_LAB)
                                //{
                                    if($daftar_tindakan_id != $row->daftartindakan_id)
                                    {
                                        $daftar_tindakan_id = $row->daftartindakan_id;
                                    }else{
                                        $row->tarif_satuan = 0;
                                    }                                    
                                //}
                                echo '<tr>
                                    <td>'.$row->jenispemeriksaanlab_nama.'</td>
                                    <td>'.$row->pemeriksaanlab_nama.'</td>
                                    <td align=right>'.number_format($row->tarif_satuan, 0,',','.').'</td>
                                    <td align=center>'.$row->qty_tindakan.'</td>
                                    <td align=right>'.number_format($row->tarifcyto_tindakan, 0,',','.').'</td>
                                    <td align=right>'.number_format($row->subTotal, 0,',','.').'</td>
                                    </tr>';
                                //<td align=center>'.((empty($row->tindakansudahbayar_id)) ? "BELUM LUNAS" : "LUNAS").'</td>
                                $total += $row->subTotal;
                                
                            }
                        ?>
                        <tfoot>
                            <tr>
                                <td colspan="5"><div class='pull-right'><b>Total Tagihan</b></div></td>
                                <td align="right"><?php echo number_format($total, 0,',','.'); ?></td>
                            </tr>
                        </tfoot>
                        </table>
                        </fieldset>
                    <?php } ?>
        </table>
    </fieldset>
    
    <div class='form-actions'>
             <?php 
             if($model->isNewRecord){
                  echo CHtml::htmlButton( Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                    array('class'=>'btn btn-primary', 'type'=>'button', 
//                                                    'onKeypress'=>'return formSubmit(this,event)',
                        'onKeypress'=>'setKonfirmasi(this)',
                        'id'=>'btn_simpan',
//                                                    'onclick'=>'cekSatuan(this);',
                        'onclick'=>'setKonfirmasi(this)',
                       )); 
             }
        ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl(''.Yii::app()->controller->id.'/'.$this->action->Id.'',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
        <?php $id_pendaftaran = (isset($_GET['id']) ? $_GET['id'] :''); 
            if(!empty($id_pendaftaran) && !$model->isNewRecord){ 
			$modPasienPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id_pendaftaran));
            echo CHtml::link(Yii::t('mds', '{icon} Print Label', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('id'=>'btnPrint','class'=>'btn btn-info', 'onclick'=>'print(\'PRINT\')')); 
            echo CHtml::link(Yii::t('mds', '{icon} Print Daftar Tindakan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('id'=>'btnPrint','class'=>'btn btn-info', 'onclick'=>'printTindakan(\'PRINT\')')); 
            // AUTOPRINT >>> echo "<script>setTimeout(function(){print(\"PRINT\");},500);</script>";
            
            }
        ?>
<?php  
//$content = $this->renderPartial('../tips/transaksi',array(),true);
//$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
	</div>
<?php $this->endWidget(); ?>





<?php
$caraPrint = 'PRINT';
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('id_pendaftaran'=>$id_pendaftaran,'id_pasienpenunjang'=>$modPasienPenunjang->pasienmasukpenunjang_id,'labelOnly'=>0)); //, 'caraPrint'=>$caraPrint
$urlPrintTindakan=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/printTindakan', array('id_pendaftaran'=>$id_pendaftaran,'id_pasienpenunjang'=>$modPasienPenunjang->pasienmasukpenunjang_id,'labelOnly'=>0)); //, 'caraPrint'=>$caraPrint
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}
function printTindakan(caraPrint)
{
    window.open("${urlPrintTindakan}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}
JSCRIPT;
    
Yii::app()->clientScript->registerScript('printLabelTindakan',$js,  CClientScript::POS_HEAD);
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoli',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlGetPemeriksaanLK = Yii::app()->createUrl('actionAjax/GetPemeriksaanLK');
$urlListKarcis =Yii::app()->createUrl('actionAjax/listKarcisRadLab');

$karcis = KonfigsystemK::getKonfigurasiKarcisPasien();
$karcis = ($karcis) ? true : 0;

$jscript = <<< JS


setTimeout(
    function(){
        listKarcisLab();
        listKarcisRad();
        setTimeout(function(){
            hitungTotal();
        },2000);
    },0);

function listKarcisLab(obj)
{
     kelasPelayanan = $('#LKPendaftaranMp_kelaspelayanan_id').val();
     ruangan = 18;
     if(kelasPelayanan != ''){
         $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan},
            function(data){
                $('#tblFormKarcisLab tbody').html(data.form);
                if(data.karcis.panjang == 1)
                {
                    tdKarcis = $('#tblFormKarcisLab tbody tr').find("td a[data-karcis='"+data.karcis.karcis_attr.karcis_id+"']");
                    $('#tblFormKarcisLab tbody').find(".currency").maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
                    changeBackground(tdKarcis, data.karcis.daftartindakan_id, data.karcis.harga_tariftindakan, data.karcis.karcis_id);
                    getDataRekening(2003,kelasPelayanan,1,'tm');
                }
         }, "json");
     }
}

function listKarcisRad(obj)
{
     kelasPelayanan = $('#ROPendaftaranMp_kelaspelayanan_id').val();
     ruangan = 19;
     if(kelasPelayanan != ''){
         $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan},
            function(data){
                $('#tblFormKarcisRad tbody').html(data.form);
                if(data.karcis.panjang == 1)
                {
                    tdKarcis = $('#tblFormKarcisRad tbody tr').find("td a[data-karcis='"+data.karcis.karcis_attr.karcis_id+"']");
                    $('#tblFormKarcisRad tbody').find(".currency").maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
                    changeBackground(tdKarcis, data.karcis.daftartindakan_id, data.karcis.harga_tariftindakan, data.karcis.karcis_id);
                    getDataRekening(2003,kelasPelayanan,1,'tm');
                }
         }, "json");
     }
}

function changeBackground(obj,idTindakan,tarifSatuan,idKarcis)
{
    banyakRow = $(obj).length;
    for(i=1; i<=banyakRow; i++){
        $(obj).css("background-color", "#FFFFFF");
    }
    $(obj).parent().parent().css("backgroundColor", "#00FF00");    
    
    if($(obj).find("#isAdminLab"))
    {
        $("#isAdminLab").attr("checked", true)
    }
    
    if($(obj).find("#isAdminRad"))
    {
        $("#isAdminRad").attr("checked", true)
    }
    hitungTotalSeluruh();
    
    /*
    $('#TindakanPelayananT_idTindakan').val(idTindakan);  
    $('#TindakanPelayananT_tarifSatuan').val(tarifSatuan);     
    $('#TindakanPelayananT_idKarcis').val(idKarcis);     
    */
}
   
function cekValidasi(){
//    var validasiUtama = cekValidasiFormUtama();
    var validasiUtama = true;
    var kelasPelayanan  = $('#LKPendaftaranMp_kelaspelayanan_id').val();
    var jenisPenyakit   = $('#LKPendaftaranMp_jeniskasuspenyakit_id').val();
    var dokter          = $('#LKPendaftaranMp_pegawai_id').val();
    
    //=======validasi sample
    var sampleLab       = $('#LKPengambilanSampleT_samplelab_id').val();
    var jumlahSample    = $('#LKPengambilanSampleT_jmlpengambilansample').val();
    if ($('input[name="pakeSample"]').is(':checked')) {
        if(sampleLab && jumlahSample){
            sample = true;
        }else{
            sample = false;
        }
    }else{
       sample = true;
    }
    if (jQuery.isNumeric(kelasPelayanan)  && validasiUtama && jenisPenyakit && dokter && sample) //&& noIdentitas && jenisIdentitas 
    {     
          $('#dialogPemeriksaanLab').dialog('open');
    }
    else{
        if(validasiUtama){
            if(!( sample))
                alert('Silahkan isi kolom yang memiliki tanda * pada Form Sampel atau Uncheck "Ambil Sample"');
            else if(!(jQuery.isNumeric(kelasPelayanan) && jenisPenyakit && dokter))
                alert('Silahkan isi kolom yang memiliki tanda * pada Form Laboratorium');
        }
    }                  
}

// Validasi sebelum tindakan pelayanan Radiologi
function cekValidasiRad(){
//    var validasiUtama = cekValidasiFormUtama();
    var validasiUtama = true;
                    
    var kelasPelayanan  = $('#ROPendaftaranMp_kelaspelayanan_id').val();
    var jenisPenyakit   = $('#ROPendaftaranMp_jeniskasuspenyakit_id').val();
    var dokter          = $('#ROPendaftaranMp_pegawai_id').val();
    
    if (jQuery.isNumeric(kelasPelayanan)  && validasiUtama && jenisPenyakit && dokter ) //&& sample&& noIdentitas && jenisIdentitas 
    {     
          $('#dialogPemeriksaanRad').dialog('open');
    }
    else{
        if(!(jQuery.isNumeric(kelasPelayanan) && jenisPenyakit && dokter ))
            alert('Silahkan isi kolom yang memiliki tanda * pada Form Radiologi');
        
    } 
}                    
function cekValidasiFormUtama(){
    var jenisIdentitas  = $('#LKPasienM_jenisidentitas').val();
    var noIdentitas     = $('#LKPasienM_no_identitas_pasien').val();
    var namaDepan       = $('#LKPasienM_namadepan').val();
    var namaPasien      = $('#LKPasienM_nama_pasien').val();
    var jenisKelamin    = true;    
    var alamatPasien    = $('#LKPasienM_alamat_pasien').val();
    var caraBayar       = $('#LKPendaftaranMp_carabayar_id').val(); 
    var penjamin        = $('#LKPendaftaranMp_penjamin_id').val();
    var kosong = '';
    var reqPasien       = $("#fieldsetPasien").find(".reqPasien[value="+kosong+"]");
    var reqPasienKosong = reqPasien.length;  
        
    //=====validasi jenis kelamin
    if($('input[name="LKPasienM[jeniskelamin]"]:checked').length == 0) {
        jenisKelamin = false;
    }
    
    //======validasi rujukan
    var asalRujukan     = $('#LKRujukanT_asalrujukan_id').val();
    var noRujukan       = $('#LKRujukanT_no_rujukan').val();
    var rujukanDari       = $('#LKRujukanT_rujukandari_id').val();
    
    if (jQuery.isNumeric(kelasPelayanan)  && namaDepan && namaPasien && jenisKelamin && alamatPasien && 
        caraBayar && penjamin && asalRujukan && noRujukan) // && sample&& noIdentitas && jenisIdentitas 
    {     
//          $('#dialogPemeriksaanLab').dialog('open');
            return true;
    }
    else{
        if(!(caraBayar && penjamin))
            alert('Silahkan isi kolom yang memiliki tanda * pada Form Cara Bayar');
        else if(!( namaDepan && namaPasien && jenisKelamin && alamatPasien && (reqPasienKosong === 0))) //&& jenisIdentitas  && noIdentitas
            alert('Silahkan isi kolom yang memiliki tanda * pada Form Data Pasien');
        else if (!(asalRujukan && noRujukan))
            alert('Silahkan isi kolom yang memiliki tanda * pada Form Rujukan');
        
        return false;
    }               
}
function cekSatuan(obj)
{
   var satuan = 0;
   var cekSatuanKosong = 1;
   var cekSatuanKosongRad = 1;
   var noRekamMedik = $('#noRekamMedik').val();
   noRekamMedik = noRekamMedik.length;
   if($('#isPasienLama').is(':checked')){
       if(noRekamMedik > 0) validRekmed = 1; else validRekmed = 1;
    }
   if(document.getElementById('isPemeriksaanLab').checked == true){
        satuan =$('#satuan').length;
        cekSatuanKosong=1;
        if (satuan=='0'){
             alert('Anda Belum Memasukan Tindakan Pemeriksaan Laboratorium');
             return false;
         }else{

             $('#satuan').each(function() {
                 cekBox =$(this).parent().prev().prev().prev().prev().find('input:checkbox').is(':checked');   
                 isiSatuan = this.value;
                 if(cekBox==true){
                     if(isiSatuan==''){
                         alert('Anda Belum Memilih Satuan');
                         cekSatuanKosong=0;
                         return false;
                     }
                 }
             });

         }
         if (cekSatuanKosong==1){
//                $('#pppendaftaran-mp-form').submit();
                    return true;
         }
   }
   if(document.getElementById('isPemeriksaanRad').checked == true){
        satuan =$('.satuanRad').length;
        cekSatuanKosongRad=1;
        if (satuan=='0'){
             alert('Anda Belum Memasukan Tindakan Pemeriksaan Radiologi');
             return false;
         }else{

             $('.satuanRad').each(function() {
                 cekBox =$(this).parent().prev().prev().prev().prev().find('input:checkbox').is(':checked');   
                 isiSatuan = this.value;
                 if(cekBox==true){
                     if(isiSatuan==''){
                         alert('Anda Belum Memilih Satuan');
                         cekSatuanKosongRad=0;
                         return false;
                     }
                 }
             });

         }
         if (cekSatuanKosongRad==1){
//                $('#pppendaftaran-mp-form').submit();  
                    return true;
         }
   }
   
 }   
 function hapusTindakanss()
    {
        banyakRow=$('#tblFormPemeriksaanLab tr').length;
        
        for(i=2; i<=banyakRow; i++){
        $('#tblFormPemeriksaanLab tr:last').remove();
        }
        
          $('.box input').removeAttr('checked');
    }
JS;
Yii::app()->clientScript->registerScript('periksa satuan',$jscript, CClientScript::POS_HEAD);

$jscript = <<< JS

//function print(idPendaftaran)
//{
//        if(document.getElementById('isPasienLama').checked == true){
//            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=280');
//        }else{
//            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');
//            window.open('${urlPrintKartuPasien}'+idPendaftaran,'printwi','left=100,top=100,width=400,height=280');
//        }
//}

function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#PPPendaftaranMp_pegawai_id').html(data.listDokter);
    }, "json");
}

function getPemeriksaanLK(obj)
{
    $.post("${urlGetPemeriksaanLK}", { idJenisPemeriksaan: obj.value },
        function(data){

            $('.boxtindakan').html(data.pemeriksaan);
    }, "json");
}

function caraAmbilPhotoJS(obj)
{
    caraAmbilPhoto=obj.value;
    
    if(caraAmbilPhoto=='webCam')
        {
          $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
            
        }
    else
        {
         $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
        }
}
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
<script type="text/javascript">
    
function autoCekTindakan(obj)
{
    var kelasPelayan_id = $('#LKPendaftaranMp_kelaspelayanan_id').val();
    if(kelasPelayan_id=='')
    {
        alert('Anda Belum Memilih Kelas Pelayanan');
    }else{
        $(obj).parent().parent().find('label').each(
            function()
            {
                $(this).find('input').attr('checked', true);            
                var idPemeriksaanlab = $(this).find('input').val();            
                jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanLabPendLab')?>',
                         'data':{idPemeriksaanlab:idPemeriksaanlab,kelasPelayan_id:kelasPelayan_id},
                         'type':'post',
                         'dataType':'json',
                         'success':function(data){
                            $('#tblFormPemeriksaanLab').find('tbody').append(data.form);
                         } ,
                         'cache':false}
                 );
            }
        );
        
        setTimeout(
            function(){
                var daftartindakan_id = null;
                var pemeriksaanlab_id = null;
                var i =0;
                $('#tblFormPemeriksaanLab > tbody').find('input[name$="[pemeriksaanlab_id]"]').each(
                    function()
                    {
                        pemeriksaanlab_id = $(this).val();
                        if($(this).parent().find('input[name$="['+ pemeriksaanlab_id +'][daftartindakan_id]"]').val() == <?php echo Params::PAKET_LAB;?>)
                        {
                            if(daftartindakan_id != $(this).parent().find('input[name$="['+ pemeriksaanlab_id +'][daftartindakan_id]"]').val())
                            {
                                daftartindakan_id = $(this).parent().find('input[name$="['+ pemeriksaanlab_id +'][daftartindakan_id]"]').val();
                            }else{
                                if(i != 0)
                                {
                                    $(this).parent().find('input[name$="['+ pemeriksaanlab_id +'][tarif_tindakan]"]').val(0);
                                }
                            }                                        
                        }
                        i++;
                    }
                );
                setTimeout(function(){hitungTotal();},700);
            },
        5000);
    }
}

function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanlab = obj.value;
        var kelasPelayan_id = $('#LKPendaftaranMp_kelaspelayanan_id').val();
        
        if(kelasPelayan_id=='')
        {
            $(obj).attr('checked', 'false');
            alert('Anda Belum Memilih Kelas Pelayanan');
        }
        else{
            jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanLabPendLab')?>',
                     'data':{idPemeriksaanlab:idPemeriksaanlab,kelasPelayan_id:kelasPelayan_id},
                     'type':'post',
                     'dataType':'json',
                     'success':function(data) {
                             if($.trim(data.form)=='')
                             {
                                $(obj).removeAttr('checked');
                                alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                             }
                             //di gunakan untuk tarif kosong tapi sekarang di by pass 23-08-2013
                             // else if($.trim(data.form)=='tarif kosong') {
                             //    $(obj).removeAttr('checked');
                             //    data.form = '';
                             //    alert ('Pemeriksaan belum memiliki tarif');
                             // }
                             $('#tblFormPemeriksaanLab').find('tbody').append(data.form);
                             //console.log(data.form);
                            var daftartindakan_id = null;
                            var pemeriksaanlab_id = null;
                            var i =0;
                            $('#tblFormPemeriksaanLab > tbody').find('input[name$="pemeriksaanlab_id"]').each(
                                function()
                                {
                                    pemeriksaanlab_id = $(this).val();
                                    if($(this).parent().find('input[name$="['+ pemeriksaanlab_id +'][daftartindakan_id]"]').val() == <?php echo Params::PAKET_LAB;?>)
                                    {
                                        if(daftartindakan_id != $(this).parent().find('input[name$="['+ pemeriksaanlab_id +'][daftartindakan_id]"]').val())
                                        {
                                            daftartindakan_id = $(this).parent().find('input[name$="['+ pemeriksaanlab_id +'][daftartindakan_id]"]').val();
                                        }else{
                                            if(i != 0)
                                            {
                                                $(this).parent().find('input[name$="['+ pemeriksaanlab_id +'][tarif_tindakan]"]').val(0);
                                            }
                                        }
                                    }
                                    i++;     
                                }
                            );
                    daftartindakan_id = data.daftartindakan_id;
                    getDataRekening(daftartindakan_id,kelasPelayan_id,1,'tm');
//                            resetTabelRek();
//                            updateTabelRek();
                            if(obj.value == '352')
                            {
                                    batalPeriksa('563');
                                    $('#formPeriksaLab').find('input[value="563"]').attr('checked', 'checked');
                                    $('#formPeriksaLab').find('input[value="563"]').attr('disabled', 'true');

                                    batalPeriksa('564');
                                    $('#formPeriksaLab').find('input[value="564"]').attr('checked', 'checked');
                                    $('#formPeriksaLab').find('input[value="564"]').attr('disabled', 'true');
                                    
                                    

                            }
                            hitungTotal();
                     } ,
                     'cache':false}
             );
        }
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
            hitungTotal();

            if(obj.value == '352')
            {
                $('#formPeriksaLab').find('input[value="563"]').removeAttr('checked');
                $('#formPeriksaLab').find('input[value="563"]').removeAttr('disabled');

                $('#formPeriksaLab').find('input[value="564"]').removeAttr('checked');
                $('#formPeriksaLab').find('input[value="564"]').removeAttr('disabled');
            }
        }else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idPemeriksaanlab)
{   
    resetTabelRek();
    $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).detach();
    updateTabelRek();
}

function cekcyto(obj, x){
//    console.log(x);
//    tidakan = $('.cyto_tindakan').val();
    if(obj.value == 1){
        $('.cyto_' + x).show();
    }else
        {
            $('.cyto_' + x).hide();
            
        }
}

function resetTabelRek(){
    $('#tblInputRekening > tbody > tr:gt(3)').detach();
}

function updateTabelRek(){
        var kelasPelayan_id = $('#LKPendaftaranMp_kelaspelayanan_id').val();
        var idKelasPelayanan = $('#ROPendaftaranMp_kelaspelayanan_id').val();


        $('#tblFormPemeriksaanLab > tbody > tr').each(function(){
            daftartindakan_id = $(this).find('input[name$="[daftartindakan_id]"]').val();
                getDataRekening(daftartindakan_id,kelasPelayan_id,1,'tm');
        });
        $('#tblFormPemeriksaanRad > tbody > tr').each(function(){
            daftartindakan_id = $(this).find('input[name$="[idDaftarTindakan]"]').val();
            getDataRekening(daftartindakan_id,idKelasPelayanan,1,'tm');
        });
}
</script>

<?php
$js = <<< JS
$('.numberOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

 

<script type="text/javascript">

selObj = document.getElementById('LKPendaftaranMp_jeniskasuspenyakit_id');
selObj.selectedIndex = 1;
selObj = document.getElementById('ROPendaftaranMp_jeniskasuspenyakit_id');
selObj.selectedIndex = 1;
//var selObj = document.getElementById('LKPendaftaranMp_ruangan_id');
//selObj.selectedIndex = 1;
var checkBox = document.getElementById('isPemeriksaanLab');
if(!checkBox.checked){
//    $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);
    $('#fieldsetLaboratorium input').attr('disabled','true');
    $('#fieldsetLaboratorium #isUpdatePasien').removeAttr('disabled');
    $('#fieldsetLaboratorium select').attr('disabled','true');
    $('#fieldsetLaboratorium textarea').attr('disabled','true');
    $('#fieldsetLaboratorium button').attr('disabled','true');
    $('#LKPendaftaranMp_ruangan_id').attr('disabled','true');
//    selObj.selectedIndex = 0;
}else{
    $('#fieldsetLaboratorium input').removeAttr('disabled');
    $('#fieldsetLaboratorium select').removeAttr('disabled');
    $('#fieldsetLaboratorium textarea').removeAttr('disabled');
    $('#fieldsetLaboratorium button').removeAttr('disabled');
    $('#fieldsetDetailPasien input').removeAttr('disabled');
    $('#fieldsetDetailPasien select').removeAttr('disabled');
    $('#LKPendaftaranMp_ruangan_id').attr('disabled','true');
//    $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);
    $('#fieldsetKarcisLab').show();
    $('#fieldsetTabelPemeriksaanLab').show();
//    selObj.selectedIndex = 1;
}
//var noSampel = $('#LKPengambilanSampleT_no_pengambilansample').val();  
function updateInputLab(obj)
{
    
//    selObj = document.getElementById('LKPendaftaranMp_ruangan_id');
    if(!obj.checked) {
        obj.checked = cekPilihRuangan();
        //Reset Form
        if(!obj.checked){
//            $('#fieldsetLaboratorium input').val('');
//            $('#fieldsetLaboratorium #isUpdatePasien').val('');
//            $('#fieldsetLaboratorium select').val('');
//            $('#fieldsetLaboratorium textarea').val('');
//            $('#fieldsetLaboratorium button').val('');
//            $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);

            $('#fieldsetLaboratorium input').attr('disabled','true');
            $('#fieldsetLaboratorium #isUpdatePasien').removeAttr('disabled');
            $('#fieldsetLaboratorium select').attr('disabled','true');
            $('#fieldsetLaboratorium textarea').attr('disabled','true');
            $('#fieldsetLaboratorium button').attr('disabled','true');
            $('#LKPendaftaranMp_ruangan_id').attr('disabled','true');
//            $('#fieldsetKarcisLab').hide();
//            $('#fieldsetTabelPemeriksaanLab').hide();
    //        $('#LKPendaftaranMp_ruangan_id_lab').removeAttr('disabled');
//            selObj.selectedIndex = 0;
        }
    }
    else {
        $('#fieldsetLaboratorium input').removeAttr('disabled');
        $('#fieldsetLaboratorium select').removeAttr('disabled');
        $('#fieldsetLaboratorium textarea').removeAttr('disabled');
        $('#fieldsetLaboratorium button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#LKPendaftaranMp_ruangan_id').attr('disabled','true');
//        $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);
        $('#fieldsetKarcisLab').show();
        $('#fieldsetTabelPemeriksaanLab').show();
        //Jika inputLab di cek pilih Laboratorium
//	selObj.selectedIndex = 1;
        
    }
}
</script>

<script>
//var selObj = document.getElementById('ROPendaftaranMp_ruangan_id');
//selObj.selectedIndex = 1;
var checkBox = document.getElementById('isPemeriksaanRad');
if(!checkBox.checked){
    formNonAktif();
}else{
    $('#fieldsetRadiologi input').removeAttr('disabled');
        $('#fieldsetRadiologi select').removeAttr('disabled');
        $('#fieldsetRadiologi textarea').removeAttr('disabled');
        $('#fieldsetRadiologi button').removeAttr('disabled');
        $('#fieldsetRadiologi checkbox').removeAttr('disabled');
        $('#ROPendaftaranMp_ruangan_id').attr('disabled','true');
//        selObj.selectedIndex = 1;
}
function updateInputRad(obj)
{
    selObj = document.getElementById('ROPendaftaranMp_ruangan_id');
    if(!obj.checked) {
        //Reset Form
        obj.checked = cekPilihRuangan();
        if(!obj.checked)
            formNonAktif();        
    }
    else {
        $('#fieldsetRadiologi input').removeAttr('disabled');
        $('#fieldsetRadiologi select').removeAttr('disabled');
        $('#fieldsetRadiologi textarea').removeAttr('disabled');
        $('#fieldsetRadiologi button').removeAttr('disabled');
        $('#fieldsetRadiologi checkbox').removeAttr('disabled');
        
        $('#ROPendaftaranMp_ruangan_id').attr('disabled','true');
//        $('#btnPeriksaRad').show();
//        selObj.selectedIndex = 1;
        
    }
}
function formNonAktif(){
//    $('#LKPengambilanSampleT_no_pengambilansample').val(noSampel);

    $('#fieldsetRadiologi input').attr('disabled','true');
    $('#fieldsetRadiologi #isUpdatePasien').removeAttr('disabled');
    $('#fieldsetRadiologi select').attr('disabled','true');
    $('#fieldsetRadiologi textarea').attr('disabled','true');
    $('#fieldsetRadiologi button').attr('disabled','true');
    $('#fieldsetRadiologi checkbox').removeAttr('disabled');
//    $('#btnPeriksaRad').hide();
    $('#ROPendaftaranMp_ruangan_id').attr('disabled','true');
//    selObj.selectedIndex = 0;
}


</script>
<?php  
//========================== Dialog Pemeriksaan Lab =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
           'id'=>'dialogPemeriksaanLab',
           'options'=>array(
               'title'=>'Pemeriksaan Laboratorium',
               'autoOpen'=>false,
               'width'=>940,
               'height'=>450,
               'modal'=>true,
               'resizelable'=>false,
           ),
       ));
       ?>
<?php // echo $this->renderPartial('_formLabolatorium', array('modPeriksaLab'=>$modPeriksaLab,'modJenisPeriksaLab'=>$modJenisPeriksaLab)); ?>

<table>
        <tr>
            <td style="background-color: #E5ECF9;">
                <div id="formPeriksaLab">
                    <?php foreach($modJenisPeriksaLab as $i=>$jenisPeriksa){ 
                            $ceklist = false;
                    ?>
                            <div class="boxtindakan">
                                <h6>
                                    <?php 
                                        echo $jenisPeriksa->jenispemeriksaanlab_nama;
                                        /*
                                        echo '&nbsp;';
                                        if($jenisPeriksa->jenispemeriksaanlab_id == Params::BLOOD_GAS)
                                        {
                                            echo CHtml::checkBox(
                                                "is_paket_list",
                                                false,
                                                array('value'=>$jenisPeriksa->jenispemeriksaanlab_id,'onclick' => "autoCekTindakan(this)")
                                            );
                                        }*/
                                    ?>
                                </h6>
                                <?php
                                    foreach ($modPeriksaLab as $j => $pemeriksaan)
                                    {
                                         if($jenisPeriksa->jenispemeriksaanlab_id == $pemeriksaan->jenispemeriksaanlab_id)
                                         {
                                             echo '<label class="checkbox inline">'.CHtml::checkBox(
                                                "pemeriksaanLab[]",
                                                 $ceklist,
                                                 array('value'=>$pemeriksaan->pemeriksaanlab_id,'onclick' => "inputperiksa(this)")
                                             );
                                             echo "<span>".$pemeriksaan->pemeriksaanlab_nama."</span></label><br/>";
                                         }
                                    }
                                ?>
                            </div>
                    <?php } ?>
                </div>
            </td>
        </tr>
</table>
    
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>

<?php 
//========= Dialog buat Input Pemeriksaan Radiologi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPemeriksaanRad',
    'options'=>array(
        'title'=>'Pilih Pemeriksaan Radiologi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>940,
        'height'=>450,
        'resizable'=>false,
//        'position'=>'top',
    ),
)); ?>
    <div class="box">
        <?php 
            $jenisPeriksa = '';
            foreach($modPeriksaRad as $i=>$pemeriksaan){ 
                $ceklist = false;
                if(!empty($arrInputPemeriksaan)){
                    $ceklist = cekPilihan($pemeriksaan->pemeriksaanrad_id,$arrInputPemeriksaan);
                }
                if($jenisPeriksa != $pemeriksaan->pemeriksaanrad_jenis){
                    echo ($jenisPeriksa!='') ? "</div>" : "";
                    $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                    echo "<div class='boxtindakan' style='width:268px;'>";
                    echo "<h6>$pemeriksaan->pemeriksaanrad_jenis</h6>";
                    echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                             'onclick' => "inputPeriksaRad(this)"));
                    echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                } else {
                    $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                    echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                             'onclick' => "inputPeriksaRad(this)"));
                    echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                }
            } echo "</div>";
        ?>               
    </div>

<?php $this->endWidget();
//========= end Input Pemeriksaan Radiologi dialog =============================
?>

<?php 
//========= Dialog buat cari data pasien =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modDataPasien = new LKPasienM('searchWithDaerahPenunjang');
$modDataPasien->unsetAttributes();
if(isset($_GET['LKPasienM'])) {
    $modDataPasien->attributes = $_GET['LKPasienM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasien-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modDataPasien->searchWithDaerahPenunjang(),
	'filter'=>$modDataPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "
                                                $(\"#dialogPasien\").dialog(\"close\");
                                                $(\"#noRekamMedik\").val(\"$data->no_rekam_medik\");
                                                $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                                setJenisKelaminPasien(\"$data->jeniskelamin\");
                                                setRhesusPasien(\"$data->rhesus\");
                                                var kelurahan = String.trim(\'$data->kelurahan_id\');
                                                if(kelurahan.length == 0){
                                                    kelurahan = 0
                                                }
                                                loadDaerahPasien($data->propinsi_id,$data->kabupaten_id,$data->kecamatan_id,kelurahan);
                                                $(\"#'.CHtml::activeId($modPasien,'pasien_id').'\").val(\"$data->pasien_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'jenisidentitas').'\").val(\"$data->jenisidentitas\");
                                                $(\"#'.CHtml::activeId($modPasien,'no_identitas_pasien').'\").val(\"$data->no_identitas_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'namadepan').'\").val(\"$data->namadepan\");
                                                $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'nama_bin').'\").val(\"$data->nama_bin\");
                                                $(\"#'.CHtml::activeId($modPasien,'tempat_lahir').'\").val(\"$data->tempat_lahir\");
                                                $(\"#'.CHtml::activeId($modPasien,'tanggal_lahir').'\").val(\"$data->tanggal_lahir\");
                                                $(\"#'.CHtml::activeId($modPasien,'kelompokumur_id').'\").val(\"$data->kelompokumur_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'jeniskelamin').'\").val(\"$data->jeniskelamin\");
                                                $(\"#'.CHtml::activeId($modPasien,'statusperkawinan').'\").val(\"$data->statusperkawinan\");
                                                $(\"#'.CHtml::activeId($modPasien,'golongandarah').'\").val(\"$data->golongandarah\");
                                                $(\"#'.CHtml::activeId($modPasien,'rhesus').'\").val(\"$data->rhesus\");
                                                $(\"#'.CHtml::activeId($modPasien,'alamat_pasien').'\").val(\"$data->alamat_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'rt').'\").val(\"$data->rt\");
                                                $(\"#'.CHtml::activeId($modPasien,'rw').'\").val(\"$data->rw\");
                                                $(\"#'.CHtml::activeId($modPasien,'propinsi_id').'\").val(\"$data->propinsi_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'kabupaten_id').'\").val(\"$data->kabupaten_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'kecamatan_id').'\").val(\"$data->kecamatan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'kelurahan_id').'\").val(\"$data->kelurahan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'no_telepon_pasien').'\").val(\"$data->no_telepon_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'no_mobile_pasien').'\").val(\"$data->no_mobile_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'suku_id').'\").val(\"$data->suku_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'alamatemail').'\").val(\"$data->alamatemail\");
                                                $(\"#'.CHtml::activeId($modPasien,'anakke').'\").val(\"$data->anakke\");
                                                $(\"#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'\").val(\"$data->jumlah_bersaudara\");
                                                $(\"#'.CHtml::activeId($modPasien,'pendidikan_id').'\").val(\"$data->pendidikan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'pekerjaan_id').'\").val(\"$data->pekerjaan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'agama').'\").val(\"$data->agama\");
                                                $(\"#'.CHtml::activeId($modPasien,'warga_negara').'\").val(\"$data->warga_negara\");
                                                loadUmur(\"$data->tanggal_lahir\");
                                            "))',
                        ),
//                'no_rekam_medik',
                array(
                        'name'=>'no_rekam_medik',
                        'header'=>'No. Rekam Medik',
                        'value'=>'$data->no_rekam_medik',
                    ),
                'nama_pasien',
//                'nama_bin',
                array(
                        'name'=>'nama_bin',
                        'header'=>'Alias',
                        'value'=>'$data->nama_bin',
                    ),
                'alamat_pasien',
                'rw',
                'rt',
                array(
                    'name'=>'propinsiNama',
                    'value'=>'$data->propinsi->propinsi_nama',
                ),
                array(
                    'name'=>'kabupatenNama',
                    'value'=>'$data->kabupaten->kabupaten_nama',
                ),
                array(
                    'name'=>'kecamatanNama',
                    'value'=>'$data->kecamatan->kecamatan_nama',
                ),
                array(
                    'name'=>'kelurahanNama',
                    'value'=>'$data->kelurahan->kelurahan_nama',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pasien dialog =============================
?>
<script>
    function cekPilihRuangan(){
        var isLab = document.getElementById('isPemeriksaanLab').checked;
        var isRad = document.getElementById('isPemeriksaanRad').checked;
        if(isLab || isRad){
        }else{
            alert ("Anda harus memilih minimal satu ruangan !");
//            setTimeout(isCheckPemeriksaan, 1000);
            return true;
        }
    }
</script>

<script>
$('#formPeriksaLab').tile({widths : [ 170 ]});
function inputPeriksaRad(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanrad = obj.value;
        var idKelasPelayanan = $('#ROPendaftaranMp_kelaspelayanan_id').val();
        var daftartindakan_id = null;
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRadPendRad')?>',
                 'data':{idPemeriksaanrad:idPemeriksaanrad, idKelasPelayanan:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                     if (data.form == ''){ 
                         $(obj).removeAttr("checked");
                         alert("Tarif tindakan kosong");
                     }else{
                         daftartindakan_id = data.daftartindakan_id;
                         $('#tblFormPemeriksaanRad').append(data.form);
                         
                         renameInput('permintaanPenunjang','inputpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','cbPemeriksaan');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                         
                         getDataRekening(daftartindakan_id,idKelasPelayanan,1,'tm');
//                        resetTabelRek();
//                        updateTabelRek();
                        hitungTotalRad();
                     }
                 } ,
                 'cache':false});
        
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksaRad(obj.value);
            hitungTotalRad();
        }else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksaRad(idPemeriksaanrad)
{   
    resetTabelRek();
    $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).detach();
    updateTabelRek();
}



function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanRad tr').length;
    var i = -1;
    $('#tblFormPemeriksaanRad tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}
function setKelasPelayanan(){
    if (jQuery.isNumeric($("#ROPendaftaranMp_kelaspelayanan_id").val())){
        $('#dialogPemeriksaanRad').dialog('open');
    }else{
        alert("Kelas Pelayanan harap diisi terlebih dahulu");
    }
}
$("form#ropendaftaran-mp-form").submit(function(){
    jumlah = $("#tblFormPemeriksaanRad tbody tr").length;
    if (jumlah > 0 ){return true;} else{ alert("Pemeriksaan harap diisi");return false;};
});

function caraBayarChange(obj){
//    var penjamin = document.getElementById('LKPendaftaranMp_penjamin_id');
    if(obj.value == 1 || obj.value == 0 ){
       setTimeout( 'var penjamin = document.getElementById(\'LKPendaftaranMp_penjamin_id\'); penjamin.selectedIndex = 1;', '500' );
       document.getElementById('pakeAsuransi').checked = false;
       $('#divAsuransi').hide(500);
    }else{
        document.getElementById('pakeAsuransi').checked = true;
        $('#divAsuransi input').removeAttr('disabled');
        $('#divAsuransi select').removeAttr('disabled');
        $('#divAsuransi').show(500);
//        $('#pakeAsuransi').click();
    }
}

function hitungCyto(obj){
    
    persen = unformatNumber($(obj).parent().parent().find('.persenCyto').val());
    tarif = unformatNumber($(obj).parent().parent().find('.tarif').val());
    qty = unformatNumber($(obj).parent().parent().find('.qty').val());
    isCyto = unformatNumber($(obj).parent().parent().find('.isCyto').val());
    cyto = (tarif * qty)*persen/100;
    $(obj).parent().parent().find('.cyto').val(cyto); 
    if(isCyto == 0)
        $(obj).parent().parent().find('.cyto').val(0);
}
function hitungCytoRad(obj){
    persen = unformatNumber($(obj).parent().parent().find('.persenCytoRad').val());
    tarif = unformatNumber($(obj).parent().parent().find('.tarifRad').val());
    qty = unformatNumber($(obj).parent().parent().find('.qtyRad').val());
    isCyto = unformatNumber($(obj).parent().parent().find('.isCytoRad').val());
    cyto = (tarif * qty)*persen/100;
//        alert(cyto);
    $(obj).parent().parent().find('.cytoRad').val(cyto);
    if(isCyto == 0)
        $(obj).parent().parent().find('.cytoRad').val(0);
}
function hitungTotal(){
    var total = 0;
    $('.tarif').each(
        function(){
            qty = unformatNumber($(this).parent().parent().find('.qty').val());
            isCyto = $(this).parent().parent().find('.isCyto').val();
            if(isCyto == 1){
                cyto = unformatNumber($(this).parent().parent().find('.cyto').val());
            }else{
                cyto = 0;
            }
            
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );

    $('#periksaTotalLab').val(formatUang(total)); 
    hitungTotalSeluruh();
}
function hitungTotalRad(){
    var total = 0;
    $('.tarifRad').each(
        function(){
            qty = unformatNumber($(this).parent().parent().find('.qtyRad').val());
            isCyto = $(this).parent().parent().find('.isCytoRad').val();
            if(isCyto == 1){
                cyto = unformatNumber($(this).parent().parent().find('.cytoRad').val());
            }else{
                cyto = 0;
            }
            
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );
 
    $('#periksaTotalRad').val(formatUang(total));
    hitungTotalSeluruh();
}

function hitungTotalSeluruh(){
    if($('#isPemeriksaanLab').is(':checked'))
    {
        totalLab = unformatNumber($('#periksaTotalLab').val());
    }else{
        totalLab = 0;
    }
    
    if($('#isPemeriksaanRad').is(':checked'))
    {
        totalRad = unformatNumber($('#periksaTotalRad').val());
    }else{
        totalRad = 0;
    }
        
//    FORM-NYA BIAYA ADMINISTRASINYA DIHAPUS ??
//    if($('#adaBiayaAdministrasi').is(':checked'))
//        totalAdm = unformatNumber($('#TindakanPelayananT_tarifSatuanAdministrasi').val());
//    else
//        totalAdm = 0;
    
    if($('#isPemeriksaanLab').is(':checked'))
    {

        if($('#isPemeriksaanRad').is(':checked'))
        {
            if($('#tblFormKarcisRad tbody tr').find("#isAdminRad"))
            {
                totalAdm = parseFloat(unformatNumber($('#tarifKarcisLab').val())) + parseFloat(unformatNumber($('#tarifKarcisRad').val()));
            }else{
                totalAdm = unformatNumber($('#tarifKarcisLab').val());
            }
        }else{
            if($('#tblFormKarcisRad tbody tr').find("#isAdminLab"))
            {
                totalAdm = unformatNumber($('#tarifKarcisLab').val());
            }
        }
    }else{
        if($('#isPemeriksaanRad').is(':checked'))
        {
            if($('#isPemeriksaanLab').is(':checked'))
            {

                if($('#tblFormKarcisLab tbody tr').find("#isAdminLab"))
                {
                    totalAdm = parseFloat(unformatNumber($('#tarifKarcisRad').val())) + parseFloat(unformatNumber($('#tarifKarcisLab').val()));
                }else{
                    totalAdm = $('#tarifKarcisRad').val();
                }
            }else{
                if($('#tblFormKarcisRad tbody tr').find("#isAdminRad"))
                {
                    totalAdm = $('#tarifKarcisRad').val();
                }
            }
        }
    }

    if($('#isPemeriksaanLab').is(':checked') || $('#isPemeriksaanRad').is(':checked')){
        cekIsPemeriksaan();
    }
    hitungTarif();

    totalSeluruh =  parseFloat(totalLab) + parseFloat(totalRad) + parseFloat(totalAdm);

    $('#periksaTotal').val(formatUang(totalSeluruh));
}

function cekIsPemeriksaan(){
    if($('#isPemeriksaanLab').is(':checked')){
        $('#tblInputRekening > tbody > tr:nth-child(1)').show();
        $('#tblInputRekening > tbody > tr:nth-child(2)').show();
        // $('#tblInputRekening > tbody > tr:nth-child(1) > td').find('input').removeAttr('disabled');
        // $('#tblInputRekening > tbody > tr:nth-child(2) > td').find('input').removeAttr('disabled');
    }else{
        $('#tblInputRekening > tbody > tr:nth-child(1)').hide();
        $('#tblInputRekening > tbody > tr:nth-child(2)').hide();
        // $('#tblInputRekening > tbody > tr:nth-child(1) > td').find('input').attr('disabled','disabled');
        // $('#tblInputRekening > tbody > tr:nth-child(2) > td').find('input').attr('disabled','disabled');
    }
    if($('#isPemeriksaanRad').is(':checked')){
        $('#tblInputRekening > tbody > tr:nth-child(3)').show();
        $('#tblInputRekening > tbody > tr:nth-child(4)').show();
        // $('#tblInputRekening > tbody > tr:nth-child(3) > td').find('input').removeAttr('disabled');
        // $('#tblInputRekening > tbody > tr:nth-child(4) > td').find('input').removeAttr('disabled');
    }else{
        $('#tblInputRekening > tbody > tr:nth-child(3)').hide();
        $('#tblInputRekening > tbody > tr:nth-child(4)').hide();
        // $('#tblInputRekening > tbody > tr:nth-child(3) > td').find('input').attr('disabled','disabled');
        // $('#tblInputRekening > tbody > tr:nth-child(4) > td').find('input').attr('disabled','disabled');
    }
}

function hitungTarif(){
    totalTarifRad = parseFloat(unformatNumber($('#tarifKarcisRad').val()));
    totalTarifLab = parseFloat(unformatNumber($('#tarifKarcisLab').val()));
    if($('#isPemeriksaanLab').is(':checked')){
        $('#tblInputRekening > tbody > tr:nth-child(1) > td:nth-child(7)').find('input').val(formatUang(totalTarifLab));
        $('#tblInputRekening > tbody > tr:nth-child(2) > td:nth-child(8)').find('input').val(formatUang(totalTarifLab));
    }else{
        $('#tblInputRekening > tbody > tr:nth-child(1) > td:nth-child(7)').find('input').val(formatUang(0));
        $('#tblInputRekening > tbody > tr:nth-child(2) > td:nth-child(8)').find('input').val(formatUang(0));
    }
    if($('#isPemeriksaanRad').is(':checked')){
        $('#tblInputRekening > tbody > tr:nth-child(3) > td:nth-child(7)').find('input').val(formatUang(totalTarifRad));
        $('#tblInputRekening > tbody > tr:nth-child(4) > td:nth-child(8)').find('input').val(formatUang(totalTarifRad));
    }else{
        $('#tblInputRekening > tbody > tr:nth-child(3) > td:nth-child(7)').find('input').val(formatUang(0));
        $('#tblInputRekening > tbody > tr:nth-child(4) > td:nth-child(8)').find('input').val(formatUang(0));
    }
}

function balikFormat(){
    tarifAdm = $('#TindakanPelayananT_tarifSatuanAdministrasi').val();
    $('#TindakanPelayananT_tarifSatuanAdministrasi').val(unformatNumber(tarifAdm));
    $('#pppendaftaran-mp-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
}
function setKonfirmasi(obj){
    if(cekValidasiFormUtama() && cekSatuan()){
        $(obj).attr('disabled',true);
        $(obj).removeAttr('onclick');
        $('#pppendaftaran-mp-form').find('.currency').each(function(){
            $(this).val(unformatNumber($(this).val()));
        });
        $('#pppendaftaran-mp-form').submit();
    }    
}
</script>