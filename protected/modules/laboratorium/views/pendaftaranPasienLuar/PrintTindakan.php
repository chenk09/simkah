<?php
?>
<style>
    fieldset{
        font-size: 12pt;
    }
    table tr .border {
        border: 1px solid;
        margin: 5px auto;
        padding: 0px auto;
        font-size: 12pt;
        /*width: 400px;*/
    }
    .utama{
        margin:2px;
        padding: 0px;
        /*border:1px solid;*/
        width: 210mm; height: 297mm; /* ukuran A4 */
    }
    .ketPasien{
        font-size:10pt;
    }
    td, rim {
        font-size: 12pt;
        vertical-align: top;
        font-weight: normal;
    }
    div .nama_periksa{
        font-size: 12pt;
        font-weight: bold;
        text-align: center;
    }
    
</style>
<h3>
    <b>DAFTAR PEMERIKSAAN LABORATORIUM - RADIOLOGI</b><br> 
</h3>
    <table>
    <tr><td class="ketPasien">No. Rekam / No. Pendaftaran</td><td class="ketPasien">: <?php echo $modRincian[0]->no_rekam_medik." / ".$modRincian[0]->no_pendaftaran." / ".$modPendaftaran->instalasi->instalasi_nama." ".$modPendaftaran->ruangan->ruangan_nama; ?></td></tr>
    <tr><td class="ketPasien">Nama Pasien</td><td class="ketPasien">: <?php echo $modRincian[0]->namadepan." ".$modRincian[0]->nama_pasien; ?></td></tr>
    <tr><td class="ketPasien">Alamat</td><td class="ketPasien">: <?php echo $modRincian[0]->alamat_pasien; ?></td></tr>
</table>
    
    <?php // echo '<pre>';print_r($modRincian[0]->attributes); exit();?>
<fieldset>
<table border="1">
    <tr  class="border">
        <th  class="border" style="width:10px;"><!--Kategori /<br/>-->No. </th>
        <th  class="border" style="width:300px;"><!--Kategori /<br/>-->Tindakan </th>
        <th  class="border" style="width:80px;"> Qty </th>
        <th  class="border" style="width:40px;"> Status </th>
        <th  class="border" style="width:100px;"> Keterangan </th>
    </tr>
    <?php
    foreach ($modRincian as $i=>$row){
    //                                <td>'.$row->kategoritindakan_nama.' /'.$row->daftartindakan_nama.'
    echo '<tr>
            <td style="text-align: right;">'.($i+1).'. </td>
            <td>'.$row->pemeriksaanlab_nama.'
            </td>
            <td align=center>'.$row->qty_tindakan.'
            </td>
            <td></td>
            <td></td>
           </tr>';
           $total += $row->subTotal;
           
       }
       $total = 0;
       foreach ($modTindakanPelayanan as $key=>$data){
           $total += ($data->tarif_satuan * $data->qty_tindakan);
       }
    ?>
    <tr>
        <td class="border" colspan="2"><center>Total Tagihan</center></td>
        <td class="border" align="right" colspan="2"><?php echo "<b>Rp. ".number_format($total, 0,',','.').",-</b>"; ?></td>
    </tr>
</table>
<!--Dokter Pemeriksa, <br><br><br><br><br>-->
<?php // echo $modRincian[0]->namaDokter; ?><br><br>
<div style="text-align: left;">
<?php echo "Dicetak oleh: ".Yii::app()->user->getState('nama_pegawai'); ?>
                            <?php echo date('Y/m/d H:i:s'); ?>
</div>
</fieldset>