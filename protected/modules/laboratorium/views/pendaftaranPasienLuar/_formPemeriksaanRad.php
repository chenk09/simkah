
<fieldset>
    <!--<legend>Pemeriksaan </legend>-->
    
    <table class="table table-bordered table-condensed" id="tblFormPemeriksaanRad">
        <thead>
            <tr>
                <th>Pilih
                    <?php 
//                    echo CHtml::link('<i class="icon-zoom-in icon-white"></i>', '#',
//                            array('onclick'=>"setKelasPelayanan();return false;",
//                                  'rel'=>"tooltip",'data-original-title'=>"Klik untuk pilih pemeriksaan",
//                                  'class'=>'btn btn-primary')) ?>
                </th>
                <th>Pemeriksaan</th>
                <th>Tarif</th>
                <th>Qty</th>
                <th>Satuan*</th>
                <th>Cyto</th>
                <th>Tarif Cyto</th>
            </tr>
        </thead>
    </table>
</fieldset>

<?php 
////========= Dialog buat Input Pemeriksaan Radiologi =========================
//$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
//    'id'=>'dialogPemeriksaanRad',
//    'options'=>array(
//        'title'=>'Pilih Pemeriksaan Radiologi',
//        'autoOpen'=>false,
//        'modal'=>true,
//        'width'=>900,
//        'resizable'=>false,
////        'position'=>'top',
//    ),
//)); ?>
<!--    <div class="box">-->
        <?php 
//            $jenisPeriksa = '';
//            foreach($modPeriksaRad as $i=>$pemeriksaan){ 
//                $ceklist = false;
//                if($jenisPeriksa != $pemeriksaan->pemeriksaanrad_jenis){
//                    echo ($jenisPeriksa!='') ? "</div>" : "";
//                    $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
//                    echo "<div class='boxtindakan'>";
//                    echo "<h6>$pemeriksaan->pemeriksaanrad_jenis</h6>";
//                    echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
//                                                                             'onclick' => "inputperiksa(this)"));
//                    echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
//                } else {
//                    $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
//                    echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
//                                                                             'onclick' => "inputperiksa(this)"));
//                    echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
//                }
//            } echo "</div>";
//        ?>               
    <!--</div>-->

<?php // $this->endWidget();
////========= end Input Pemeriksaan Radiologi dialog =============================
//?>

<script>
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanrad = obj.value;
        var idKelasPelayanan = $('#ROPendaftaranMp_kelaspelayanan_id').val();
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRadPendRad')?>',
                 'data':{idPemeriksaanrad:idPemeriksaanrad, idKelasPelayanan:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                     if (data.form == ''){ 
                         $(obj).removeAttr("checked");
                         alert("Tarif tindakan kosong");
                     }else{
                         $('#tblFormPemeriksaanRad').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','cbPemeriksaan');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                     }
                 } ,
                 'cache':false});
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idPemeriksaanrad)
{
    $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).detach();
}



function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanRad tr').length;
    var i = -1;
    $('#tblFormPemeriksaanRad tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}
function setKelasPelayanan(){
    if (jQuery.isNumeric($("#ROPendaftaranMp_kelaspelayanan_id").val())){
        $('#dialogPemeriksaanRad').dialog('open');
    }else{
        alert("Kelas Pelayanan harap diisi terlebih dahulu");
    }
}
$("form#ropendaftaran-mp-form").submit(function(){
    jumlah = $("#tblFormPemeriksaanRad tbody tr").length;
    if (jumlah > 0 ){return true;} else{ alert("Pemeriksaan harap diisi");return false;};
});
</script>