<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'id'=>'rjkasuspenyakitdiagnosa-m-search',
                 'type'=>'horizontal',
)); ?>
		<?php /* echo $form->textFieldRow($model,'jenisdiet_id'); */ ?>
		<?php // echo $form->textFieldRow($model,'jeniskasuspenyakit_nama',array('class'=>'span3','size'=>50,'maxlength'=>50)); ?>
        <?php //echo $form->DropDownListRow($model, 'jeniskasuspenyakit_id', CHtml::listData($model->getJeniskasuspenyakitItems(),'jeniskasuspenyakit_id','jeniskasuspenyakit_nama'),array('empty'=>'-- Pilih --')); ?>
        <?php //echo $form->DropDownListRow($model, 'diagnosa_id', CHtml::listData($model->getDiagnosaItems(),'diagnosa_id','diagnosa_nama'),array('empty'=>'-- Pilih --')); ?>
        <?php echo CHtml::label('Kasus Penyakit','',array('class'=>'control-label required')); ?>
            <div class="controls">
                            <?php echo $form->hiddenField($model, 'jeniskasuspenyakit_id'); ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'kasuspenyakit', 
                                                    'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/JenisKasusPenyakit').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                                    'options'=>array(
                                                               'showAnim'=>'fold',
                                                               'minLength' => 2,
                                                               'focus'=> 'js:function( event, ui )
                                                                   {
                                                                    $(this).val(ui.item.label);
                                                                    return false;
                                                                    }',
                                                               'select'=>'js:function( event, ui ) {
                                                                   $(\'#KasuspenyakitdiagnosaM_jeniskasuspenyakit_id\').val(ui.item.value);
                                                                   $(\'#kasuspenyakit\').val(ui.item.jeniskasuspenyakit_nama);
                                                                    return false;
                                                                }',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'readonly'=>false,
                                                        'placeholder'=>'Kasus Penyakit',
                                                        'size'=>13,
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                    ),
                                                    'tombolDialog'=>array('idDialog'=>'dialogkasuspenyakit'),
                                            )); ?>
            </div>
            <?php echo CHtml::label('Diagnosa','',array('class'=>'control-label required')); ?>
            <div class="controls">
                            <?php echo $form->hiddenField($model, 'diagnosa_id'); ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'diagnosa', 
                                                    'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/Diagnosa').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                                    'options'=>array(
                                                               'showAnim'=>'fold',
                                                               'minLength' => 2,
                                                               'focus'=> 'js:function( event, ui )
                                                                   {
                                                                    $(this).val(ui.item.label);
                                                                    return false;
                                                                    }',
                                                               'select'=>'js:function( event, ui ) {
                                                                   $(\'#KasuspenyakitdiagnosaM_diagnosa_id\').val(ui.item.value);
                                                                   $(\'#diagnosa\').val(ui.item.label);
                                                                    return false;
                                                                }',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'readonly'=>false,
                                                        'placeholder'=>'Diagnosa',
                                                        'size'=>13,
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                    ),
                                                    'tombolDialog'=>array('idDialog'=>'dialogkasuspenyakitdiagnosa'),
                                            )); ?>
            </div>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
<!-- ============================== Widget Dialog Jenis Kasus Penyakit =============================== -->
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'dialogkasuspenyakit',
        'options'=>array(
            'title'=>'Pencarian Kasus Penyakit',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>600,
            'resizable'=>false,
        ),
    ));
    
    $modJeniskasuspenyakit = new JeniskasuspenyakitM;
    $modJeniskasuspenyakit->unsetAttributes();
    if (isset($_GET['JeniskasuspenyakitM'])) {
        $modJeniskasuspenyakit->attributes = $_GET['JeniskasuspenyakitM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'jeniskasuspenyakit-grid',
        'dataProvider'=>$modJeniskasuspenyakit->search(),
        'filter'=>$modJeniskasuspenyakit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-striped table-condensed',
        'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                array(
                                        "class"=>"btn-small",
                                        "id" => "selectKasuspenyakit",
                                        "onClick" => "\$(\"#KasuspenyakitdiagnosaM_jeniskasuspenyakit_id\").val($data->jeniskasuspenyakit_id);
                                                              \$(\"#kasuspenyakit\").val(\"$data->jeniskasuspenyakit_nama\");
                                                              \$(\"#dialogkasuspenyakit\").dialog(\"close\");"
                                ))',
            ),
            'jeniskasuspenyakit_nama',
            'jeniskasuspenyakit_namalainnya',
            array(
                'header'=>'Urutan',
                'value'=>'$data->jeniskasuspenyakit_urutan',
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
$this->endWidget();
?>
<!-- ======================== endWidget dialogkasuspenyakit ===================================== -->

<!-- ============================== Widget Dialog Diagnosa ==================================== -->
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'dialogkasuspenyakitdiagnosa',
        'options'=>array(
            'title'=>'Pencarian Diagnosa',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>600,
            'resizable'=>false,
        ),
    ));
    
    $modDiagnosa = new DiagnosaM;
    $modDiagnosa->unsetAttributes();
    if (isset($_GET['DiagnosaM'])) {
        $modDiagnosa->attributes = $_GET['DiagnosaM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'diagnosa-grid',
        'dataProvider'=>$modDiagnosa->search(),
        'filter'=>$modDiagnosa,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-striped table-condensed',
        'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                array(
                                        "class"=>"btn-small",
                                        "id" => "selectKasuspenyakit",
                                        "onClick" => "\$(\"#KasuspenyakitdiagnosaM_diagnosa_id\").val($data->diagnosa_id);
                                                              \$(\"#diagnosa\").val(\"$data->diagnosa_nama\");
                                                              \$(\"#dialogkasuspenyakitdiagnosa\").dialog(\"close\");"
                                ))',
            ),
            'diagnosa_nama',
            'diagnosa_namalainnya',
            array(
                'header'=>'imunisasi',
                'type'=>'raw',
                'value'=>'($data->diagnosa_imunisasi==1)? Yii::t("mds","Yes") : Yii::t("mds","No")',
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
$this->endWidget();
?>
<!-- ======================== endWidget dialogkasuspenyakit ===================================== -->