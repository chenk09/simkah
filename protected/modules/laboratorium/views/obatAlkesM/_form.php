<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfobat-alkes-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
        <table>
            <tr>
                <td>
                     <?php echo $form->textFieldRow($model,'obatalkes_nama',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>200,'onkeyup'=>'generateKode(this)')); ?>
                     <?php echo $form->textFieldRow($model,'obatalkes_kode',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'onkeyup'=>'numbersOnly(this)')); ?>
<!--                    <div id="idnumberOnly">-->
                     <?php echo $form->textFieldRow($model,'harganetto',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<!--                    </div>    -->
                 <?php echo $form->textFieldRow($model,'hargajual',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                     <?php echo $form->textFieldRow($model,'discount',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                     <div class='control-group'>
                            <?php echo $form->labelEx($model,'tglkadaluarsa', array('class'=>'control-label')) ?>
                         <div class="controls">
                             <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglkadaluarsa',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                              )); ?>
                         </div>
                     </div>    
                     <?php echo $form->textFieldRow($model,'minimalstok',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                     <?php echo $form->dropDownListRow($model,'sumberdana_id',
                                               CHtml::listData($model->SumberDanaItems, 'sumberdana_id', 'sumberdana_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                     <?php echo $form->dropDownListRow($model,'satuanbesar_id',
                                               CHtml::listData($model->SatuanBesarItems, 'satuanbesar_id', 'satuanbesar_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                   
                     <?php echo $form->dropDownListRow($model,'satuankecil_id',
                                               CHtml::listData($model->SatuanKecilItems, 'satuankecil_id', 'satuankecil_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                    
                    
                    
                      <?php echo $form->dropDownListRow($model,'lokasigudang_id',
                                               CHtml::listData($model->lokasiGudangItems, 'lokasigudang_id', 'lokasigudang_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                      <?php echo $form->dropDownListRow($model,'obatalkes_kadarobat',ObatAlkesKadarObat::items(),
                                       array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                       'empty'=>'-- Pilih --',)); ?>
                </td>
                <td>
          
                     <?php echo $form->textFieldRow($model,'kemasanbesar',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <div class="control-label"> Kekuatan</div>
                    <div class="controls">
                      <?php echo $form->textField($model,'kekuatan',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                      <?php echo $form->dropDownList($model,'satuankekuatan',  SatuanKekuatan::items(),
                       array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                       'empty'=>'-- Pilih --',)); ?>
                    </div>

                     <?php echo $form->dropDownListRow($model,'formularium',  Formularium::items(),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                     <?php echo $form->dropDownListRow($model,'obatalkes_kategori',ObatAlkesKategori::items(),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                     <?php echo $form->dropDownListRow($model,'discountinue',array('1'=>'Ya','0'=>'Tidak'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                    <div class="control-label">
                        Therapi Obat
                        <?php echo $form->hiddenField($model,'therapiobat_id'); ?>
                    </div>
                    <div class="controls">
                    <?php $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'therapiobat', 
                                            'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/TherapiObat').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                            'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'focus'=> 'js:function( event, ui )
                                                           {
                                                            $(this).val(ui.item.label);
                                                            return false;
                                                            }',
                                                       'select'=>'js:function( event, ui ) {
                                                           $(\'#GFObatAlkesM_therapiobat_id\').val(ui.item.therapiobat_id);
                                                           $(\'#therapiobat\').val(ui.item.therapiobat_nama);
                                                            return false;
                                                        }',
                                            ),
                                            'htmlOptions'=>array(
                                                'readonly'=>false,
                                                'placeholder'=>'Therapi Obat',
                                                'size'=>13,
                                                'class'=>'span2',
                                                'onkeypress'=>"return $(this).focusNextInputField(event);",
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogtherapiobat'),
                                    )); ?>
                    </div>
                    
                    <?php echo $form->dropDownListRow($model,'pbf_id',
                                               CHtml::listData($model->PbfItems, 'pbf_id', 'pbf_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                    <?php echo $form->dropDownListRow($model,'generik_id',
                                               CHtml::listData($model->generikItems, 'generik_id', 'generik_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                    <div class="control-label">
                        Jenis Obat Alkes
                        <?php echo $form->hiddenField($model,'jenisobatalkes_id'); ?>
                    </div>
                    <div class="controls">
                    <?php $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'jenisobatalkes', 
                                            'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/JenisObatAlkes').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                            'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'focus'=> 'js:function( event, ui )
                                                           {
                                                            $(this).val(ui.item.label);
                                                            return false;
                                                            }',
                                                       'select'=>'js:function( event, ui ) {
                                                           $(\'#GFObatAlkesM_jenisobatalkes_id\').val(ui.item.jenisobatalkes_id);
                                                           $(\'#jenisobatalkes\').val(ui.item.jenisobatalkes_nama);
                                                            return false;
                                                        }',
                                            ),
                                            'htmlOptions'=>array(
                                                'readonly'=>false,
                                                'placeholder'=>'Jenis Obat Alkes',
                                                'size'=>13,
                                                'class'=>'span2',
                                                'onkeypress'=>"return $(this).focusNextInputField(event);",
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogjenisobatalkes'),
                                    )); ?>
                    </div>
                    <?php echo $form->dropDownListRow($model,'obatalkes_golongan',  ObatAlkesGolongan::items(),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
	
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="control-group">
                        <?php echo Chtml::label('Supplier','Supplier',array('class'=>'control-label'));?>
                        <div class="controls">
                           <?php 
                               $this->widget('application.extensions.emultiselect.EMultiSelect',
                                             array('sortable'=>true, 'searchable'=>true)
                                        );
                                echo CHtml::dropDownList(
                                'supplier_id[]',
                                '',
                                CHtml::listData(GFSupplierM::model()->findAll('supplier_aktif=TRUE ORDER BY supplier_nama'), 'supplier_id', 'supplier_nama'),
                                array('multiple'=>'multiple','key'=>'supplier_id', 'class'=>'multiselect','style'=>'width:500px;height:150px')
                                        );
                          ?>
                        </div>
                    </div>      
                </td> 
            </tr>
        </table>
        <?php echo $this->renderPartial('_ObatAlkesDetail', array('model'=>$model,'form'=>$form)); ?>
           <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.obatAlkesM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>
	</div>

<?php $this->endWidget(); ?>
        
<!-- =============================== beginWidget Therapi Obat ============================= -->        
<?php
   $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogtherapiobat',
    'options'=>array(
        'title'=>'Pencarian Therapi Obat',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>400,
        'resizable'=>false,
        ),
    ));
   
$modTherapiobat = new TherapiobatM('search');
$modTherapiobat->unsetAttributes();
if(isset($_GET['TherapiobatM'])) {
    $modTherapiobat->attributes = $_GET['TherapiobatM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'therapiobat-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modTherapiobat->search(),
	'filter'=>$modTherapiobat,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selecttherapiobat",
                                                    "onClick" => "\$(\"#GFObatAlkesM_therapiobat_id\").val($data->therapiobat_id);
                                                                          \$(\"#therapiobat\").val(\"$data->therapiobat_nama\");
                                                                          \$(\"#dialogtherapiobat\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'therapiobat_nama',
                'therapiobat_namalain',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<!-- ============================== endWidget TherapiObat ================================= -->

<!-- =============================== beginWidget Jenis Obat Alkes ============================= -->        
<?php
   $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogjenisobatalkes',
    'options'=>array(
        'title'=>'Pencarian Jenis Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>400,
        'resizable'=>false,
        ),
    ));
   
$modTherapiobat = new JenisobatalkesM('search');
$modTherapiobat->unsetAttributes();
if(isset($_GET['JenisobatalkesM'])) {
    $modTherapiobat->attributes = $_GET['JenisobatalkesM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'jenisobatalkes-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modTherapiobat->search(),
	'filter'=>$modTherapiobat,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selectjenisobatalkes",
                                                    "onClick" => "\$(\"#GFObatAlkesM_jenisobatalkes_id\").val($data->jenisobatalkes_id);
                                                                          \$(\"#jenisobatalkes\").val(\"$data->jenisobatalkes_nama\");
                                                                          \$(\"#dialogjenisobatalkes\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'jenisobatalkes_nama',
                'jenisobatalkes_namalain',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<!-- ============================== endWidget TherapiObat ================================= -->

 <?php
$urlgetKodeObatAlkes=Yii::app()->createUrl('ActionAjax/GetKodeObatAlkes');
$kodeObat = CHtml::activeId($model,'obatalkes_kode');
                       
$js = <<< JS
function generateKode(obj)
{
   namaObat =obj.value;
   if(namaObat!=''){//Jika nama Obat Tidak Kosong  
       $.post("${urlgetKodeObatAlkes}",{namaObat: namaObat},
            function(data){
                $('#${kodeObat}').val(data.kodeObatBaru);      
        },"json");
   }else{//Jika Nama Obat Kosong
                $('#${kodeObat}').val('');      
   } 
}

JS;
Yii::app()->clientScript->registerScript('sfdasdasda',$js,CClientScript::POS_HEAD);

                       
$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

