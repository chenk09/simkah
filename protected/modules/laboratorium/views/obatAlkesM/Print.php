
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

$table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchPrint();
         $template = "{pager}{summary}\n{items}";
    }
    
$this->widget($template,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>$sort,
	'dataProvider'=>$data,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'obatalkes_id',
		array(
                        'name'=>'obatalkes_id',
                        'value'=>'$data->obatalkes_id',
                        'filter'=>false,
                ),
		array(
                        'name'=>'lokasigudang_id',
                        'filter'=>  CHtml::listData($model->LokasiGudangItems, 'lokasigudang_id', 'lokasigudang_nama'),
                        'value'=>'$data->lokasigudang->lokasigudang_nama',
                ),
		array(
                        'name'=>'therapiobat_id',
                        'filter'=>  CHtml::listData($model->TherapiObatItems, 'therapiobat_id', 'therapiobat_nama'),
                        'value'=>'$data->therapiobat->therapiobat_nama',
                ),
		array(
                        'name'=>'lokasigudang_id',
                        'filter'=>  CHtml::listData($model->PbfItems, 'pbf_id', 'pbf_nama'),
                        'value'=>'$data->pbf->pbf_nama',
                ),
		array(
                        'name'=>'generik_id',
                        'filter'=>  CHtml::listData($model->GenerikItems, 'generik_id', 'generik_nama'),
                        'value'=>'$data->generik->generik_nama',
                ),
		array(
                        'name'=>'satuanbesar_id',
                        'filter'=>  CHtml::listData($model->SatuanBesarItems, 'satuanbesar_id', 'satuanbesar_nama'),
                        'value'=>'$data->satuanbesar->satuanbesar_nama',
                ),
		/*
		'sumberdana_id',
		'satuankecil_id',
		'jenisobatalkes_id',
		'obatalkes_kode',
		'obatalkes_nama',
		'obatalkes_golongan',
		'obatalkes_kategori',
		'obatalkes_kadarobat',
		'kemasanbesar',
		'kekuatan',
		'satuankekuatan',
		'harganetto',
		'hargajual',
		'discount',
		'tglkadaluarsa',
		'minimalstok',
		'formularium',
		'discountinue',
		'obatalkes_aktif',
		*/
 
        ),
    )); 
?>