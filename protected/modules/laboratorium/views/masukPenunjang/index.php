
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'masukpenunjang-Laboratorium-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'unformatFormNumber();'),
        'focus'=>'#',
)); ?>
<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<table class="table-condensed">
    <tr>
        <td style="background-color: #E5ECF9;">
                <?php
                foreach ($modRiwayatKirimKeUnitLain as $i => $riwayat) {
                    $modPermintaan = PermintaankepenunjangT::model()->with('daftartindakan','pemeriksaanlab')->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$riwayat->pasienkirimkeunitlain_id));

                        // echo count($modPermintaan);

                    foreach($modPermintaan as $j => $permintaan){
                        $modTarif = TariftindakanM::model()->findAllByAttributes(array('kelaspelayanan_id'=>$riwayat->kelaspelayanan_id,
                            'daftartindakan_id'=>$permintaan->daftartindakan_id,
                        ));
                        $tarif[$i][$j] = (!empty($modTarif->harga_tariftindakan)) ? $modTarif->harga_tariftindakan : 0 ;
                        $persencyto[$i][$j] = (!empty($modTarif->persencyto_tind)) ? $modTarif->persencyto_tind : 0 ;
                    }
                    $arrInputPemeriksaan[$i] = $modPermintaan;

                    // echo $modPermintaan."<br>";
                }
                ?>
            <div id="formPeriksaLab">
                <?php foreach($modJenisPeriksaLab as $i=>$jenisPeriksa){ 
                ?>
                        <div class="boxtindakan">
                            <h6><?php echo $jenisPeriksa->jenispemeriksaanlab_nama; ?></h6>
                            <?php foreach ($modPeriksaLab as $j => $pemeriksaan) {
                                  $ceklist = (!empty($arrInputPemeriksaan)) ? cekPilihan($pemeriksaan->pemeriksaanlab_id,$arrInputPemeriksaan):false;

                                  if($jenisPeriksa->jenispemeriksaanlab_id == $pemeriksaan->jenispemeriksaanlab_id) {
                                         echo '<label class="checkbox inline">'.CHtml::checkBox("pemeriksaanLab[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanlab_id,
                                              'onclick' => "inputperiksa(this); checkIni(this);"));
                                         echo "<span>".$pemeriksaan->pemeriksaanlab_nama."</span></label><br/>";
                                     }
                                 } ?>
                        </div>
                <?php } ?>
            </div>
        </td>
    </tr>
</table>
<div class="control-group">
    <?php echo CHtml::label('Dokter Pemeriksa','Dokter PemeriksaAA',array('class'=>'control-label')); ?>
    <div class="controls">
        <?php
            $modPasienMasukPenunjang->pegawai_id = Params::DEFAULT_DOKTER_LAB;
            echo CHtml::dropDownList('LKPasienMasukPenunjang[pegawai_id]',$modPasienMasukPenunjang->pegawai_id,CHtml::listData(DokterV::model()->findAllByAttributes(array('ruangan_id'=>Yii::app()->user->getState('ruangan_id')), array('order'=>'nama_pegawai')),'pegawai_id','NamaLengkap'),array('empty'=>'--Pilih--','class'=>'span3'));
        ?>
    </div>
</div>

<fieldset>
    <legend class="rim">Pencarian Daftar Pemeriksaan Lab</legend>
    <table class="table table-condensed">
        <tr>
            <tr>
               <td>Kategori Pencarian : 
                  <?php 
                        echo CHtml::dropDownList('Pilihan Pencarian','daftarTindakanNama',(Params::DaftarTindakan()),array('id'=>'daftarTindakan','empty'=>'-- Pilih --','style'=>'width:140px;','onChange'=>'cekOptions()'));
                  ?>
                </td>
            <td>
                <div style="margin-left:-50px;margin-right:400px;" id="nama">
                    
                <?php echo CHtml::hiddenField('idPemeriksaan'); ?>
                    <?php
//                            
                    $this->widget('MyJuiAutoComplete', array(
                                        'name' => 'daftartindakan_nama',
                                        'sourceUrl' => Yii::app()->createUrl('laboratorium/ActionAutoComplete/getNamaDaftarTindakan'),
                                        'options' => array(
                                            'showAnim' => 'fold',
                                            'minLength' => 2,
                                            'focus' => 'js:function( event, ui ) {
                                                                            $(this).val(ui.item.label);
                                                                            return false;
                                                                        }',
                                            'select' => 'js:function( event, ui ) {
                                                                                  $("#idPemeriksaanLab").val(ui.item.pemeriksaanlab_id);
                                                                                  return false;
                                                                        }',
                                        ),
                                        'htmlOptions' => array(
                                            'class' => 'span2',
                                            'onkeypress' =>"if (event.keyCode == 13){inputperiksaTindakan();}return $(this).focusNextInputField(event)",
                                            'onClick'=>'cekOptions()',
                                        ),
                                        'tombolDialog' => array('idDialog' => 'dialogAddTindakan'),
                                    ));
                  
                            ?>
                </div>
                <div style="margin-left:-50px;margin-right:400px;" id="kode">
                    <?php 
                        $this->widget('MyJuiAutoComplete', array(
                                        'name' => 'pemeriksaanlab_kode',
                                        'sourceUrl' => Yii::app()->createUrl('laboratorium/ActionAutoComplete/getKodeDaftarTindakan'),
                                        'options' => array(
                                            'showAnim' => 'fold',
                                            'minLength' => 2,
                                            'focus' => 'js:function( event, ui ) {
                                                                            $(this).val(ui.item.label);
                                                                            return false;
                                                                        }',
                                            'select' => 'js:function( event, ui ) {
                                                                                  $("#idPemeriksaanLab").val(ui.item.pemeriksaanlab_id);
                                                                                  return false;
                                                                        }',
                                        ),
                                        'htmlOptions' => array(
                                            'class' => 'span2',
                                            'onkeypress' =>"if (event.keyCode == 13){inputperiksaTindakan();}return $(this).focusNextInputField(event)",
                                            'onClick'=>'cekOptions()',
                                        ),
                                        'tombolDialog' => array('idDialog' => 'dialogAddTindakan'),
                                    ));
                    ?>
                </div>
            </td>
        </tr>
    </table>
    
 <table id="tblFormPemeriksaanLab" class="table table-condensed">
                <thead>
                    <tr>
                        <th>Pemeriksaan</th>
                        <th>Tarif</th>
                        <th>Qty</th>
                        <th>Satuan*</th>
                        <th>Cyto</th>
                        <th>Tarif Cyto</th>
                        <th>Batal/Hapus</th>
                    </tr>
                </thead>
                <?php
                $urlLoadForm = Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanLabMasuk');
                    foreach ($modPermintaan as  $itemPeriksa) {
                        $idPemeriksaanLab=$itemPeriksa['pemeriksaanlab_id'];
//                        $idKelaspelayanan=$modPendaftaran->kelaspelayanan_id;
                        $idKelaspelayanan = Params::kelasPelayanan('tanapa_kelas'); //Semua pemeriksaan lab & rad tanpa kelas
                ?>
                <script>
                
                         var idPemeriksaanlab = <?php echo $idPemeriksaanLab ?>;
                         var kelasPelayan_id = <?php echo $idKelaspelayanan ?>;

                        
                        $.post("<?php echo $urlLoadForm; ?> ", { idPemeriksaanlab: idPemeriksaanlab,kelasPelayan_id:kelasPelayan_id },
                        function(data){
                            $('#tblFormPemeriksaanLab').append(data.form);
                    }, "json");
                </script>
            <?php } ?>
            </table>
            <table class="table table-bordered table-condensed">
				<tr><td width="70%" style="text-align: right;">Biaya Administrasi</td><td><?php echo CHtml::textField('biayaAdministrasi', 0,array('class'=>'span2 currency', 'style'=>'text-align:right;', 'onkeyup'=>'hitungTotal();'));?></td></tr>
                <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan</td><td><?php echo CHtml::textField('periksaTotal', '',array('class'=>'span2', 'style'=>'text-align:right;', 'disabled'=>'disabled'));?></td></tr>
                <?php if(count($modKirimKeUnitLain) > 0) :?> 
                <tr>
                    <td colspan="2">
                        <div class="control-group ">
                            <label class="control-label required" for="PasienkirimkeunitlainT_pegawai_id">
                            Perujuk :
                            </label>
                            <div class="controls">
                            </div>
                        </div> 
                    <?php echo $form->dropDownListRow($modKirimKeUnitLain,'pegawai_id', CHtml::listData($modKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    </td>
                </tr>
                <?php endif; ?>
            </table>
            <table>
                <tr>
                    <td>
                        <br>
                        <?php
                        //FORM REKENING
                            $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                                array(
                                    'form'=>$form,
                                    'modRekenings'=>$modRekenings,
                                )
                            );
                        ?>
                    </td>
                </tr>
            </table>
            
            <div class="form-actions">
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle"></i>')), 
                                                                    Yii::app()->createUrl($this->module->id.'/RujukanPenunjang/index'), 
                                                                     array('class'=>'btn btn-danger')); ?>
		 <?php //echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
<?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
            </div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
$('#formPeriksaLab').tile({widths : [ 190 ]});
setTimeout(function(){
    hitungTotal();
}, 3000);

var a = $('#formPeriksaLab').find('input[value="352"]').attr('checked');
if(a=='checked'){
    batalPeriksa('563');
    $('#formPeriksaLab').find('input[value="563"]').attr('checked', 'checked');
    $('#formPeriksaLab').find('input[value="563"]').attr('disabled', 'true');

    batalPeriksa('564');
    $('#formPeriksaLab').find('input[value="564"]').attr('checked', 'checked');
    $('#formPeriksaLab').find('input[value="564"]').attr('disabled', 'true');
    hitungTotal();
}

function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanLab = obj.value;
        var idKelasPelayanan = <?php echo $idKelaspelayanan ?>;
             
        if(idKelasPelayanan == ''){
             alert('Isi Terlebih Dahulu Daftar Pasien');
             
             setTimeout(function(){
                     $(obj).removeAttr('checked', 'checked');
             }, 500);
        }else{
            
            setTimeout(function(){
                 if(cekList(idPemeriksaanLab)==true){
                      jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/LoadFormPemeriksaanLabMasuk')?>',
                     'data':{idPemeriksaanlab:idPemeriksaanLab, kelasPelayan_id:idKelasPelayanan},
                     'type':'post',
                     'dataType':'json',
                     'success':function(data) {
                            if($.trim(data.form)=='')
                            {
                                $(this).removeAttr('checked');
                                alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                            } else if($.trim(data.form)=='tarif kosong') {
                                $(this).removeAttr('checked');
                                data.form = '';
                                alert ('Pemeriksaan belum memiliki tarif');
                            }
                            $('#tblFormPemeriksaanLab').append(data.form);
                            renameInput('permintaanPenunjang','inputpemeriksaanlab');
                            renameInput('permintaanPenunjang','inputtarifpemeriksaanlab');
                            renameInput('permintaanPenunjang','inputqty');
                            renameInput('permintaanPenunjang','satuan');
                            renameInput('permintaanPenunjang','cyto');
                            //renameInput('permintaanPenunjang','persencyto');
                            renameInput('permintaanPenunjang','tarifcyto');
                            renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                            renameInput('permintaanPenunjang','pemeriksaanNama');
                            renameInput('permintaanPenunjang','idDaftarTindakan');
                            
                            var daftartindakanId = $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanLab).find('input[name$="[daftartindakan_id]"]').val();
                            getDataRekening(daftartindakanId,idKelasPelayanan,1,'tm');
                            hitungTotal();

                            if(obj.value == '352')
                            {
                                    batalPeriksa('563');
                                    $('#formPeriksaLab').find('input[value="563"]').attr('checked', 'checked');
                                    $('#formPeriksaLab').find('input[value="563"]').attr('disabled', 'true');

                                    batalPeriksa('564');
                                    $('#formPeriksaLab').find('input[value="564"]').attr('checked', 'checked');
                                    $('#formPeriksaLab').find('input[value="564"]').attr('disabled', 'true');
                                    hitungTotal();

                            }
                     } ,
                     'cache':false});
                hitungTotal();
                }
             }, 50);
             clear();
             
         }
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
            hitungTotal();
            if(obj.value == '352')
            {
                $('#formPeriksaLab').find('input[value="563"]').removeAttr('checked');
                $('#formPeriksaLab').find('input[value="563"]').removeAttr('disabled');

                $('#formPeriksaLab').find('input[value="564"]').removeAttr('checked');
                $('#formPeriksaLab').find('input[value="564"]').removeAttr('disabled');
            }
        }else
            $(obj).attr('checked', 'checked');
    }
}

function inputperiksaTindakan()
{
        var idPemeriksaanLab = $('#idPemeriksaanLab').val();
        var idKelasPelayanan = <?php echo $idKelaspelayanan ?>;
        
         if(idKelasPelayanan == ''){
             alert('Isi Terlebih Dahulu Daftar Pasien');
             
             setTimeout(function(){
                     $("#daftartindakan_nama").val("");
                     $("#daftarTindakan").val("");
                     clear();
             }, 500);
        }else if(idPemeriksaanLab == ''){
            alert('Isi Tindakan Terlebih Dahulu');
        
        }else{
            if(cekList(idPemeriksaanLab)==true){                     
                    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/LoadFormPemeriksaanLabMasuk')?>',
                 'data':{idPemeriksaanlab:idPemeriksaanLab, kelasPelayan_id:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         if($.trim(data.form)=='')
                         {
                            $(this).removeAttr('checked');
                            alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                         } else if($.trim(data.form)=='tarif kosong') {
                            $(this).removeAttr('checked');
                            data.form = '';
                            alert ('Pemeriksaan belum memiliki tarif');
                         }
                         $('#tblFormPemeriksaanLab').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanlab');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanlab');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         //renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                         
                         var daftartindakanId = $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanLab).find('input[name$="[daftartindakan_id]"]').val();
                         getDataRekening(daftartindakanId,idKelasPelayanan,1,'tm');
                 } ,
                 'cache':false}); 
            }
                setTimeout(function(){
                        tindakanCeklist();
                        clear();
                        hitungTotal();
                }, 500);
             }
//        }
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanLab tr').length;
    var i = -1;
    $('#tblFormPemeriksaanLab tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}
function remove(obj,idTindakan,idPemeriksaanLab){
    if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
                batalPeriksa(idPemeriksaanLab);
                $('#formPeriksaLab').find('input[value='+idPemeriksaanLab+']').removeAttr('checked');
                var daftartindakan_id = idTindakan;
                removeRekeningTindakan(daftartindakan_id);
            hitungTotal();
    }else{
        $(obj).attr('checked', 'checked');
    }
}
function batalPeriksa(idPemeriksaanlab)
{
    var daftartindakan_id = $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).find('input[name$="[daftartindakan_id]"]').val();
    removeRekeningTindakan(daftartindakan_id);
    $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).detach();
}

function cyto_tarif(test, x){
    if(test.value == 1){
        $('.cyto_tarif_'+ x).show();
    }else{
        $('.cyto_tarif_'+ x).hide();
    }
}

function cekList(id){
    x = true;
    $('.pemeriksaanlab_id').each(function(){
        if ($(this).val() == id){
            alert('Daftar Pemeriksaan Tindakan telah ada di List');
            x = false;
        }
    });
    return x;
}


function cekOptions(params){
    idOptions = $('#daftarTindakan').val();
    
    if(idOptions == ''){
        alert('Harap Memilih Kategori Pencarian Terlebih Dahulu')
    }
    
    $("#nama").show();
    $("#kode").hide();
    if(idOptions== 'nama'){
        $("#nama").show();
        $("#kode").hide();
    }else if(idOptions == 'kode'){
        $("#nama").hide();
        $("#kode").show(); 
    }  
    
}

function tindakanCeklist(){
    var idPemeriksaanLab = $('#idPemeriksaanLab').val();
    setTimeout(function(){
         $('.pemeriksaanlab_id').each(function(){
            var pemeriksaanLab = $(this).parents('tr').find('#pemeriksaanLab').val()

                if(pemeriksaanLab == idPemeriksaanLab){
                    $(this).parents('tr').find('#pemeriksaanLab').each(function(){
                        $(this).attr('checked','checked');
                    });
                }
        });
    }, 500);
}

function clear(){
idOptions = $('#daftarTindakan').val();
   
    if(idOptions == 'nama'){
            $('#nama').val("");
            $('#daftarTindakan').val("");
            $('#pemeriksaanlab_kode').val("");
    }else if(idOptions == 'kode'){
            $('#pemeriksaanlab_kode').val("");
            $('#daftarTindakan').val("");
        
    }
    
}
function hitungCyto(obj){
    persen = unformatNumber(10);
    tarif = unformatNumber($(obj).parent().parent().find('.tarif').val());
    qty = unformatNumber($(obj).parent().parent().find('.qty').val());
    isCyto = unformatNumber($(obj).parent().parent().find('.isCyto').val());
    cyto = (tarif * qty)*persen/100;
//        alert(cyto);
    $(obj).parent().parent().find('.cyto').val(cyto);
    if(isCyto == 0)
        $(obj).parent().parent().find('.cyto').val(0);
}

function hitungTotal(){
    var total = 0;
    $('.tarif').each(
        function(){
            qty = $(this).parent().parent().find('.qty').val();
            isCyto = $(this).parent().parent().find('.isCyto').val();
            if(isCyto == 1){
                cyto = $(this).parent().parent().find('.cyto').val();
            }else{
                cyto = 0;
            }
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );
	total += unformatNumber($('#biayaAdministrasi').val());
    $('#periksaTotal').val(formatNumber(total));    
}
//load rekening
function loadRekening(){
    $('#tblFormPemeriksaanLab').find('input[name$="[daftartindakan_id]"]').each(function(){
        var daftartindakanId = $(this).val();
        var kelaspelayananId = <?php echo Params::kelasPelayanan('tanapa_kelas');?>;
        getDataRekening(daftartindakanId,kelaspelayananId,1,'tm');
    });
}
setTimeout(function(){
    loadRekening(); //rekening tindakan
    var kelaspelayananId = <?php echo Params::kelasPelayanan('tanapa_kelas');?>;
    getDataRekening(2003,kelaspelayananId,1,'tm'); //rekening biaya administrasi
},1000);

function unformatFormNumber(){
    $('#masukpenunjang-Laboratorium-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
}
function formatFormNumber(){
    $('#masukpenunjang-Laboratorium-form').find('.currency').each(function(){
        $(this).val(formatNumber($(this).val()));
    });
}
setTimeout(function(){formatFormNumber()},1000);
</script>

<?php
function cekPilihan($pemeriksaanlab_id,$arrInputPemeriksaan)
{
    $cek = false;
    foreach ($arrInputPemeriksaan as $i => $items) {
        foreach($items as $j=>$item){
            if($pemeriksaanlab_id == $item->pemeriksaanlab_id) $cek = true;
        }
    }
    return $cek;
}
?>

<?php

//========= Dialog daftar tindakan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogAddTindakan',
    'options'=>array(
        'title'=>'Daftar Tindakan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));


$modDataTindakan = new PemeriksaanlabM('search');
    $modDataTindakan->unsetAttributes();
    if(isset($_GET['PemeriksaanlabM'])) {
        $modDataTindakan->attributes = $_GET['PemeriksaanlabM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'pemeriksaanlab-m-grid',
            //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
            'dataProvider'=>$modDataTindakan->search(),
            'filter'=>$modDataTindakan,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPasien",
                                        "onClick" => "
                                            $(\"#dialogAddTindakan\").dialog(\"close\");
//                                           $(\"#dialogTindakan\").dialog(\"close\");
                                            $(\"#idPemeriksaanLab\").val(\"$data->pemeriksaanlab_id\");
                                            inputperiksaTindakan();
                                            clear();
                                        "))',
                    ),
                    array(
                        'header'=>'Jenis Pemeriksaan Lab',
                        'name'=>'jenispemeriksaanlab_nama',
                        'value'=>'$data->jenispemeriksaan->jenispemeriksaanlab_nama',
                    ),
                    array(
                        'header'=>'Kode Pemeriksaan',
                        'name'=>'pemeriksaanlab_kode',
                        'value'=>'$data->pemeriksaanlab_kode',
                    ),
                    array(
                        'header'=>'Nama Pemeriksaan',
                        'name'=>'pemeriksaanlab_nama',
                        'value'=>'$data->pemeriksaanlab_nama',
                    ),
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));


$this->endWidget();
//========= end pasien dialog =============================
    
?>
<?php
$js= <<< JS
    $(document).ready(function() {
        $("#daftarTindakan").children("li").children("a").click(function() {
            $("#daftarTindakan").children("li").attr('class','');
        });
        
        $("#nama").show();
        $("#kode").hide();
    });

    
function onReset()
{
    setTimeout(
        function(){
            $.fn.yiiGridView.update('masukpenunjang-Laboratorium-form', {
                data: $("#namatindakan").serialize()
            });
            $.fn.yiiGridView.update('masukpenunjang-Laboratorium-form', {
                data: $("#kodetindakan").serialize()
            });      
        }, 2000
    );
    return false;
}   
JS;
Yii::app()->clientScript->registerScript('pencatatanriwayat',$js,CClientScript::POS_HEAD);
?>
