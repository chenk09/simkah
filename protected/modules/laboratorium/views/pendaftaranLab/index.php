 <style>
    fieldset legend.accord1{
        width:100%;
    }
 </style>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-mp-form',
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));
//
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); 
 if(!empty($_GET['id'])){ ?> 
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
 <?php } ?>
    <fieldset>
        <legend class="rim2">Transaksi Pendaftaran Pasien Luar</legend>
        <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        <?php echo $form->errorSummary(array($modTindakanPelayananT,$modTindakanKomponen,$modHasilPemeriksaan,$model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienPenunjang,$modPengambilanSample)); ?>

        <table class='table-condensed'>
            <tr>
                <td width="10%">
                    <div class='control-group'>
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                            )); ?>
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>
                        </div>
                    </div>
                    
                    <?php echo $this->renderPartial('_formPasien', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien)); ?>
                </td>
                <td width="50%">
                    
                    <?php //echo $form->textFieldRow($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData($model->getRuanganItems(), 'ruangan_id', 'ruangan_nama'), array('empty'=>'-- Pilih --', 'disabled'=>'disabled', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'ajax'=>array('type'=>'POST',
                                                                'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'PPPendaftaranMp')),
                                                                'update'=>'#PPPendaftaranMp_jeniskasuspenyakit_id'),
                                                                'onChange'=>'listDokterRuangan(this.value);'
                                                            )); ?>

                    <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems(), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>'hapusTindakanss();')); ?>
                    
                    <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems(Params::RUANGAN_ID_LAB), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'LKPendaftaranMp')), 
                                                    'update'=>'#'.CHtml::activeId($model,'penjamin_id').''  //selector to update
                                                ),
                        )); ?>

                    <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                   
                    <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan,'modPengambilanSample'=>$modPengambilanSample)); ?>
                    
                    <?php echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form)); ?>
               
                    <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>
                                               
                    <?php echo $this->renderPartial('_formPengambilanSample', array('model'=>$model,'form'=>$form,'modPengambilanSample'=>$modPengambilanSample, 'modSampleLab'=>$modSampleLab)); ?>
                     
                     
                   <?php 
                    if(!empty($_GET['id'])){
                        ?><br>
                        <fieldset id="fieldsetDetailPasien">
                            <legend class="accord1">
                                Informasi Tagihan Pasien
                            </legend>
                         <table id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    Kategori /<br/>Tindakan
                                </th>
                                <th>
                                    Tarif
                                </th>
                                <th>
                                    Qty
                                </th>
                                <th>
                                    Tarif Cyto
                                </th>
                                <th>
                                    Sub Total
                                </th> 
                                <th>
                                    Status
                                </th> 
                            </tr>
                        </thead>
                        <?php
                        foreach ($modRincian as $i=>$row){
                        echo '<tr>
                                <td>'.$row->kategoritindakan_nama.' /'.$row->daftartindakan_nama.'
                                </td>
                                <td align=right>'.number_format($row->tarif_satuan, 0,',','.').'
                                </td>
                                <td align=center>'.$row->qty_tindakan.'
                                </td>
                                <td align=right>'.number_format($row->tarifcyto_tindakan, 0,',','.').'
                                </td>
                                <td align=right>'.number_format($row->subTotal, 0,',','.').'
                                </td>
                                <td align=center>'.((empty($row->tindakansudahbayar_id)) ? "BELUM LUNAS" : "LUNAS").'
                                </td>
                               </tr>';
                               $total += $row->subTotal;
                           }
                        ?>
                        <tfoot>
                            <tr>
                                <td colspan="4"><div class='pull-right'>Total Tagihan</div></td>
                                <td align="right"><?php echo number_format($total, 0,',','.'); ?></td>
                                <td></td>
                            </tr>
                        </tfoot>
                        </table>
                        </fieldset>
                    <?php
                        } else {
                    ?>
                    <fieldset id="fieldsetKarcis" class="">
                                    <legend class="accord1" >
                                        <?php echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        Karcis Penunjang                                    </legend>
                                <?php echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                                </fieldset>
                   <br/>
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Pilih Pemeriksaan',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-success', 'type'=>'button',
                                                      "onclick"=>"cekValidasi();",
                                                      )); ?>
        
                    <table id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Pilih</th>
                                <th>Jenis Pemeriksaan/<br/>Pemeriksaan</th>
                                <th>Tarif</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                                <th>Cyto</th>
                                <th>Tarif Cyto</th>
                            </tr>
                        </thead>                        
                    </table>
                   <?php }?>
                   
                </td>
            <tr>
        </table>
    </fieldset>

    <div class='form-actions'>
             <?php 
             if($model->isNewRecord){
                  echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button', 
                                                    'onKeypress'=>'return formSubmit(this,event)',
                                                    'id'=>'btn_simpan','onclick'=>'cekSatuan(this);',
                                                   )); 
//                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)', 'id'=>'btn_simpan','onclick'=>'cekSatuan(this);'));
             }else{ 
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
             }
//             echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
//                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
//                                                array('class'=>'btn btn-primary', 'type'=>'button', 
//                                                    'onKeypress'=>'return formSubmit(this,event)',
//                                                    'id'=>'btn_simpan','onclick'=>'cekSatuan(this);',
//                                                   )); 
                                                                     ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl(''.Yii::app()->controller->id.'/'.$this->action->Id.'',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
        <?php $id_pendaftaran = (isset($_GET['id']) ? $_GET['id'] :''); 
            if(!empty($id_pendaftaran) && !$model->isNewRecord){ 
            
//            echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false")); 
            echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info', 'onclick'=>'print(\'PRINT\')')); 
            
            
            }
        ?>
<?php  
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
	</div>
<?php $this->endWidget(); ?>





<?php
$caraPrint = 'PRINT';
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('id_pendaftaran'=>$id_pendaftaran, 'caraPrint'=>$caraPrint));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=570px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('printCaraPrint',$js,  CClientScript::POS_HEAD);
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoli',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlGetPemeriksaanLK = Yii::app()->createUrl('actionAjax/GetPemeriksaanLK');

$urlListKarcis =Yii::app()->createUrl('actionAjax/listKarcis');
$karcis = KonfigsystemK::getKonfigurasiKarcisPasien();
$karcis = ($karcis) ? true : 0;

$jscript = <<< JS

setTimeout(listKarcis, 3000); 
function listKarcis(obj)
{
     kelasPelayanan=$('#LKPendaftaranMp_kelaspelayanan_id').val();
     ruangan=$('#LKPendaftaranMp_ruangan_id').val();
//     if($('#isPasienLama').is(':checked'))
//        pasienLama = 1;
//     else
        pasienLama = 0;
            
     if(kelasPelayanan!=''){
             $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan, pasienLama:pasienLama },
                function(data){
                    $('#tblFormKarcis tbody').html(data.form);
                    if (${karcis}){
                        if (jQuery.isNumeric(data.karcis.karcis_id)){
                            tdKarcis = $('#tblFormKarcis tbody tr').find("td a[data-karcis='"+data.karcis.karcis_id+"']");
                            changeBackground(tdKarcis, data.karcis.daftartindakan_id, data.karcis.harga_tariftindakan, data.karcis.karcis_id);
                        }
                    }
             }, "json");
     }      
       
}

function changeBackground(obj,idTindakan,tarifSatuan,idKarcis)
{
        banyakRow=$('#tblFormKarcis tr').length;
        for(i=1; i<=banyakRow; i++){
        $('#tblFormKarcis tr').css("background-color", "#FFFFFF");          
        } 
             
        $(obj).parent().parent().css("backgroundColor", "#00FF00");     
        $('#TindakanPelayananT_idTindakan').val(idTindakan);  
        $('#TindakanPelayananT_tarifSatuan').val(tarifSatuan);     
        $('#TindakanPelayananT_idKarcis').val(idKarcis);     
     
}
   
function cekValidasi(){
    var kelasPelayanan  = $('#LKPendaftaranMp_kelaspelayanan_id').val();
    var jenisIdentitas  = $('#LKPasienM_jenisidentitas').val();
    var noIdentitas     = $('#LKPasienM_no_identitas_pasien').val();
    var namaDepan       = $('#LKPasienM_namadepan').val();
    var namaPasien      = $('#LKPasienM_nama_pasien').val();
    var jenisKelamin    = true;
    var alamatPasien    = $('#LKPasienM_alamat_pasien').val();
    var jenisPenyakit   = $('#LKPendaftaranMp_jeniskasuspenyakit_id').val();
    var dokter          = $('#LKPendaftaranMp_pegawai_id').val();
    var caraBayar       = $('#LKPendaftaranMp_carabayar_id').val();
    var penjamin        = $('#LKPendaftaranMp_penjamin_id').val();

    //=====validasi jenis kelamin
    if($('input[name="LKPasienM[jeniskelamin]"]:checked').length == 0) {
        jenisKelamin = false;
    }
    
    //=======validasi sample
    var sampleLab       = $('#LKPengambilanSampleT_samplelab_id').val();
    var jumlahSample    = $('#LKPengambilanSampleT_jmlpengambilansample').val();
//    if ($('input[name="pakeSample"]').is(':checked')) {
//        if(sampleLab && jumlahSample){
//            sample = true;
//        }else{
//            sample = false;
//        }
//    }else{
//       sample = true;
//    }
    
    //======validasi rujukan
    var asalRujukan     = $('#LKRujukanT_asalrujukan_id').val();
    var noRujukan       = $('#LKRujukanT_no_rujukan').val();
    
//    if ($('input[name="isRujukan"]').is(':checked')) {
//       if(asalRujukan && noRujukan){
//            rujukan = true;
//        }else{
//            rujukan = false;
//        }
//    } else {
//        rujukan = true;
//    }


    if (jQuery.isNumeric(kelasPelayanan) && jenisIdentitas && noIdentitas && namaDepan && namaPasien && jenisKelamin && alamatPasien && jenisPenyakit && dokter && 
        caraBayar && penjamin && sampleLab && jumlahSample && asalRujukan && noRujukan)
    {       
          $('#dialogLaboratorium').dialog('open');
    }
    else{
        alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda *');
    }
}

function cekSatuan(obj)
{
   satuan =$('#satuan').length;
   cekSatuanKosong=true;
   if (satuan=='0'){
        alert('Anda Belum Memasukan Tindakan Yang Akan Diperiksa');
        return false;
    }else{

        $('#satuan').each(function() {
            cekBox =$(this).parent().prev().prev().prev().prev().find('input:checkbox').is(':checked');   
            isiSatuan = this.value;
            if(cekBox==true){
                if(isiSatuan==''){
                    alert('Anda Belum Memilih Satuan');
                    cekSatuanKosong==false;
                    return false;
                }
            }
        });

    }

    if (cekSatuanKosong==true){
//           do_upload();
           $('#pppendaftaran-mp-form').submit();  
//           alert('simpan');
    }
 }   
 function hapusTindakanss()
    {
        banyakRow=$('#tblFormPemeriksaanLab tr').length;
        
        for(i=2; i<=banyakRow; i++){
        $('#tblFormPemeriksaanLab tr:last').remove();
        }
        
          $('.box input').removeAttr('checked');
    }
JS;
Yii::app()->clientScript->registerScript('periksa satuan',$jscript, CClientScript::POS_HEAD);

$jscript = <<< JS

//function print(idPendaftaran)
//{
//        if(document.getElementById('isPasienLama').checked == true){
//            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=280');
//        }else{
//            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');
//            window.open('${urlPrintKartuPasien}'+idPendaftaran,'printwi','left=100,top=100,width=400,height=280');
//        }
//}

function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#PPPendaftaranMp_pegawai_id').html(data.listDokter);
    }, "json");
}

function getPemeriksaanLK(obj)
{
    $.post("${urlGetPemeriksaanLK}", { idJenisPemeriksaan: obj.value },
        function(data){

            $('.boxtindakan').html(data.pemeriksaan);
    }, "json");
}

function caraAmbilPhotoJS(obj)
{
    caraAmbilPhoto=obj.value;
    
    if(caraAmbilPhoto=='webCam')
        {
          $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
            
        }
    else
        {
         $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
        }
} 
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
<script type="text/javascript">
    
$('#formPeriksaLab').tile({widths : [ 190 ]});

function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanlab = obj.value;
        var kelasPelayan_id = $('#LKPendaftaranMp_kelaspelayanan_id').val();
        
        if(kelasPelayan_id==''){
                $(obj).attr('checked', 'false');
                alert('Anda Belum Memilih Kelas Pelayanan');
                
            }
        else{
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanLabPendLab')?>',
                 'data':{idPemeriksaanlab:idPemeriksaanlab,kelasPelayan_id:kelasPelayan_id},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         if($.trim(data.form)=='')
                         {
                            $(obj).removeAttr('checked');
                            alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                         } else if($.trim(data.form)=='tarif kosong') {
                            $(obj).removeAttr('checked');
                            data.form = '';
                            alert ('Pemeriksaan belum memiliki tarif');
                         }
                         $('#tblFormPemeriksaanLab').append(data.form);
                 } ,
                 'cache':false});
        }     
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idPemeriksaanlab)
{
    $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).detach();
}

function cekcyto(obj, x){
//    console.log(x);
//    tidakan = $('.cyto_tindakan').val();
    if(obj.value == 1){
        $('.cyto_' + x).show();
    }else
        {
            $('.cyto_' + x).hide();
            
        }
}
</script>

<?php
$js = <<< JS
$('.numberOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

 <?php  $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                                'id'=>'dialogLaboratorium',
                                'options'=>array(
                                    'title'=>'Pemeriksaan Laboratorium',
                                    'autoOpen'=>false,
                                    'width'=>450,
                                    'height'=>450,
                                    'modal'=>false,
                                    'hide'=>'explode',
                                    'resizelable'=>false,
                                ),
                            ));
                            ?>
                    <?php echo $this->renderPartial('_formLabolatorium', array('modPeriksaLab'=>$modPeriksaLab,'modJenisPeriksaLab'=>$modJenisPeriksaLab)); ?>
                    <?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>