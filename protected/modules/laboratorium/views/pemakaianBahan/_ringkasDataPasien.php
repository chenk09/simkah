<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<fieldset>
      <legend class="rim2">Pemakaian Bahan</legend>
    <legend class="rim">Data Pasien</legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'tgl_pendaftaran', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?></td>
            <td>
                <?php 
                    $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$modPasien,
                                    'attribute'=>'no_rekam_medik',
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('laboratorium/ActionAutoComplete/daftarPasien').'",
                                                       dataType: "json",
                                                       data: {
                                                           noRm: request.term,
                                                           namaPasien: "",
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val("");
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                isiDataPasien(ui.item);
                                                return false;
                                            }',
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                                    'htmlOptions'=>array('class'=>'span2', 'placeholder'=>'Ketik No. Rekam Medik','onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); 
                ?>
            </td>
            <td rowspan="4">
                <?php 
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'no_pendaftaran', array('readonly'=>true)); ?></td>
            
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label')); ?></td>
            <td>
                <?php //echo CHtml::activeTextField($modPasien, 'nama_pasien', array('readonly'=>true)); ?>
                <?php 
                    $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$modPasien,
                                    'attribute'=>'nama_pasien',
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('laboratorium/ActionAutoComplete/daftarPasien').'",
                                                       dataType: "json",
                                                       data: {
                                                           noRm: "",
                                                           namaPasien: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val("");
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                isiDataPasien(ui.item);
                                                return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array('class'=>'span3', 'placeholder'=>'Ketik Nama Pasien','onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); 
                ?>
            </td>
        </tr>
            <?php if($_GET['id'] != null){ ?>
                <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::TextField('jeniskasuspenyakit_nama',$modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama, array('readonly'=>true)); ?></td>
            <?php }else{ ?>
                <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::activeTextField($modPendaftaran, 'jeniskasuspenyakit_nama', array('readonly'=>true)); ?></td>
            <?php } ?>
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien, 'nama_bin', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <?php if($_GET['id'] != null){ ?>
                <td><?php echo CHtml::activeLabel($modPendaftaran, 'kelaspelayanan_id',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::TextField('kelaspelayanan_nama',$modPendaftaran->kelaspelayanan->kelaspelayanan_nama, array('readonly'=>true)); ?></td>
            <?php }else{ ?>
                <td><?php echo CHtml::activeLabel($modPendaftaran, 'kelaspelayanan_nama',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::activeTextField($modPendaftaran, 'kelaspelayanan_nama', array('readonly'=>true)); ?></td>
            <?php } ?>
            
            
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'umur', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <?php if($_GET['id'] != null){ ?>
                <td><?php echo CHtml::activeLabel($modPendaftaran, 'carabayar_id',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::TextField('carabayar_nama',$modPendaftaran->carabayar->carabayar_nama, array('readonly'=>true)); ?></td>
            <?php }else{ ?>
                <td><?php echo CHtml::activeLabel($modPendaftaran, 'carabayar_id',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::activeTextField($modPendaftaran, 'carabayar_nama', array('readonly'=>true)); ?></td>
            <?php } ?>
            
            
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien, 'jeniskelamin', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <?php if($_GET['id'] != null){ ?>
                <td><?php echo CHtml::activeLabel($modPendaftaran, 'penjamin_id',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::TextField('penjamin_id',$modPendaftaran->penjamin->penjamin_nama, array('readonly'=>true)); ?></td>
            <?php }else{ ?>
                <td><?php echo CHtml::activeLabel($modPendaftaran, 'penjamin_id',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::activeTextField($modPendaftaran, 'penjamin_nama', array('readonly'=>true)); ?></td>
            <?php } ?>
            
            <?php if($_GET['id'] != null){ ?>
                <td><?php echo CHtml::Label('Dokter Pemeriksa', 'Dokter Pemeriksa',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::TextField('nama_pegawai',$modPendaftaran->pegawai->nama_pegawai, array('readonly'=>true)); ?></td>
            <?php }else{ ?>
                <td><?php echo CHtml::Label('Dokter Pemeriksa', 'Dokter Pemeriksa',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::activeTextField($modPendaftaran, 'nama_pegawai', array('readonly'=>true)); ?></td>
            <?php } ?>
        </tr>
    </table>
</fieldset>
<?php echo CHtml::activeHiddenField($modPendaftaran, 'pendaftaran_id',array('readonly'=>true,'id'=>'idPendaftaran')); ?>
<?php echo CHtml::activeHiddenField($modPendaftaran, 'pasien_id',array('readonly'=>true,)); ?>
<?php echo CHtml::activeHiddenField($modPendaftaran, 'penjamin_id',array('readonly'=>true,)); ?>
<?php echo CHtml::activeHiddenField($modPendaftaran, 'kelaspelayanan_id',array('readonly'=>true,)); ?>
<?php echo CHtml::activeHiddenField($modPendaftaran, 'carabayar_id',array('readonly'=>true,)); ?>
<?php echo CHtml::activeHiddenField($modPendaftaran, 'pegawai_id',array('readonly'=>true,)); ?>
<?php echo CHtml::hiddenField('idPasienKirimKeunitLain', '',array('readonly'=>true)); ?>
<?php echo CHtml::hiddenField('idKelasPelayanan', '',array('readonly'=>true)); ?>
<hr/>

<script type="text/javascript">
function isiDataPasien(data)
{
    $('#idKelasPelayanan').val(data.kelaspelayanan_id);
    $('#idPasienKirimKeunitLain').val(data.pasienkirimkeunitlain_id);
    $('#idPendaftaran').val(data.pendaftaran_id);
    $('#<?php echo CHtml::activeId($modPasien, 'no_rekam_medik') ?>').val(data.no_rekam_medik);
    $('#<?php echo CHtml::activeId($modPasien, 'nama_pasien') ?>').val(data.nama_pasien);
    $('#<?php echo CHtml::activeId($modPasien, 'nama_bin') ?>').val(data.nama_bin);
    $('#<?php echo CHtml::activeId($modPasien, 'jeniskelamin') ?>').val(data.jeniskelamin);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'pendaftaran_id') ?>').val(data.pendaftaran_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'tgl_pendaftaran') ?>').val(data.tgl_pendaftaran);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'umur') ?>').val(data.umur);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'no_pendaftaran') ?>').val(data.no_pendaftaran);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'pasien_id') ?>').val(data.pasien_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'jeniskasuspenyakit_nama') ?>').val(data.jeniskasuspenyakit_nama);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'jeniskasuspenyakit_id') ?>').val(data.jeniskasuspenyakit_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'carabayar_id') ?>').val(data.carabayar_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'carabayar_nama') ?>').val(data.carabayar_nama);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'kelaspelayanan_id') ?>').val(data.kelaspelayanan_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'kelaspelayanan_nama') ?>').val(data.kelaspelayanan_nama);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'penjamin_id') ?>').val(data.penjamin_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'penjamin_nama') ?>').val(data.penjamin_nama);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'pegawai_id') ?>').val(data.pegawai_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'nama_pegawai') ?>').val(data.nama_pegawai);
}
</script>

<?php

$js = <<< JS
$('#cekRiwayatPasien').change(function(){
        $('#divRiwayatPasien').slideToggle(500);
});
JS;
Yii::app()->clientScript->registerScript('JSriwayatPasien',$js,CClientScript::POS_READY);
?>
<?php 
//========= Dialog buat cari data pendaftaran =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
$format = new CustomFormat();
$modDialogPasien = new LKPasienMasukPenunjangV('searchLAB');
$modDialogPasien->unsetAttributes();
$modDialogPasien->tglAwal = date('Y-m-d', strtotime('-5 days'));
$modDialogPasien->tglAkhir = date('Y-m-d')." 23:59:59";
if(isset($_GET['LKPasienMasukPenunjangV'])) {
    $modDialogPasien->attributes = $_GET['LKPasienMasukPenunjangV'];
    if(!empty($_GET['LKPasienMasukPenunjangV']['tglAwal'])){
        $modDialogPasien->tglAwal = ($_GET['LKPasienMasukPenunjangV']['tglAwal']);
        //$modDialogPasien->tglAkhir = ($_GET['LKPasienMasukPenunjangV']['tglAwal'])." 23:59:59";
    }
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pendaftaran-t-grid',
	'dataProvider'=>$modDialogPasien->searchLAB(),
	'filter'=>$modDialogPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPendaftaran",
                                    "onClick" => "
                                        $(\"#dialogPasien\").dialog(\"close\");

                                        $(\"#LKPendaftaranT_tgl_pendaftaran\").val(\"$data->tgl_pendaftaran\");
                                        $(\"#LKPendaftaranT_no_pendaftaran\").val(\"$data->no_pendaftaran\");
                                        $(\"#LKPendaftaranT_umur\").val(\"$data->umur\");
                                        $(\"#LKPendaftaranT_jeniskasuspenyakit_nama\").val(\"$data->jeniskasuspenyakit_nama\");
                                        $(\"#LKPendaftaranT_ruangan_nama\").val(\"$data->ruangan_nama\");
                                        $(\"#idPendaftaran\").val(\"$data->pendaftaran_id\");
                                        $(\"#LKPendaftaranT_carabayar_id\").val(\"$data->carabayar_id\");
                                        $(\"#LKPendaftaranT_penjamin_id\").val(\"$data->penjamin_id\");
                                        $(\"#LKPendaftaranT_kelaspelayanan_id\").val(\"$data->kelaspelayanan_id\");
                                        $(\"#idKelasPelayanan\").val(\"$data->kelaspelayanan_id\");
                                        $(\"#LKPendaftaranT_kelaspelayanan_nama\").val(\"$data->kelaspelayanan_nama\");
                                        $(\"#LKPendaftaranT_pegawai_id\").val(\"$data->pegawai_id\");
                                        $(\"#LKPendaftaranT_nama_pegawai\").val(\"$data->nama_pegawai\");
                                        $(\"#LKPendaftaranT_pasien_id\").val(\"$data->pasien_id\");

                                        $(\"#LKPasienM_jeniskelamin\").val(\"$data->jeniskelamin\");
                                        $(\"#LKPasienM_no_rekam_medik\").val(\"$data->no_rekam_medik\");
                                        $(\"#LKPasienM_nama_pasien\").val(\"$data->nama_pasien\");
                                        $(\"#LKPasienM_nama_bin\").val(\"$data->nama_bin\");
                                        $(\"#LKPendaftaranT_carabayar_nama\").val(\"$data->carabayar_nama\");
                                        $(\"#LKPendaftaranT_penjamin_nama\").val(\"$data->penjamin_nama\");



                                    "))',
                ),
                array(
                    'name'=>'tgl_pendaftaran',
                    'filter'=> 
                    CHtml::activeTextField($modDialogPasien, 'tglAwal', array('placeholder'=>'contoh: 2013-01-15')),
//                    POSISI DATE PICKER BUGS
//                    $this->widget('MyDateTimePicker',array(
//                            'model'=>$modDialogPasien,
//                            'attribute'=>'tgl_pendaftaran_cari',
//                            'mode'=>'date',
//                            'options'=> array(
//                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                            ),
//                            'htmlOptions'=>array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)"
//                            ),
//                    )),
                ),
                array(
                    'name'=>'ruangan_nama',
                    'type'=>'raw',
                ),
                array(
                    'header'=>'no_rekam_medik',
                    'name'=>'no_pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->no_pendaftaran."<br>".$data->no_rekam_medik',
                ),
                array(
                    'name'=>'nama_pasien',
                    'type'=>'raw',
                ),
                'jeniskelamin',
                array(
                    'name'=>'carabayar_nama',
                    'type'=>'raw',
                ),
                array(
                    'name'=>'statusperiksahasil',
                    'header'=>'Status Periksa',
                    'type'=>'raw',
                    'value'=>'$data->statusperiksahasil',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pendaftaran dialog =============================
?>