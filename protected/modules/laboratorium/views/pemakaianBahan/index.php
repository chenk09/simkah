<?php
    $this->widget('application.extensions.moneymask.MMask',array(
        'element'=>'.currency',
        'currency'=>'PHP',
        'config'=>array(
            'symbol'=>'Rp. ',
            'defaultZero'=>true,
            'allowZero'=>true,
            'decimal'=>',',
            'thousands'=>'.',
            'precision'=>0,
        )
    ));

    $this->widget('application.extensions.moneymask.MMask',array(
        'element'=>'.number',
        'config'=>array(
            'defaultZero'=>true,
            'allowZero'=>true,
            'decimal'=>',',
            'thousands'=>'.',
            'precision'=>0,
        )
    ));
?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'lkpemakaian-bahan-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInput();'),
)); ?>
<?php if(!empty($_GET['id'])){
?>
    <div class="mds-form-message error">
                    <?php Yii::app()->user->setFlash('success',"Data berhasil disimpan"); ?>
                </div>
<? } ?>
<?php $this->renderPartial($this->pathView.'_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien)); ?>
<?php $this->renderPartial($this->pathView.'_listObatAlkesPasien',array('modViewBmhp'=>$modViewBmhp)); ?>
<?php $this->renderPartial($this->pathView.'_formPemakaianBahan',array('modPendaftaran'=>$modPendaftaran)); ?>

    <div class="form-actions">
        <?php 
            if($_GET['id'] != null){
                echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','disabled'=>true)); 
            }else{
                echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); 
            }
                
        ?>
        
        <?php 
            $content = $this->renderPartial('rawatJalan.views.tips.tips',array(),true);
                    $this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));?>
    </div>
<?php } ?>