<fieldset id="formNonRacikan" class="table-bordered">
                <legend class="table-bordered radio">
                    Pemakaian Bahan
                </legend>
                
                <div class="control-group ">
                    <label class="control-label" for="namaObat">Nama Obat & Kesehatan</label>
                    <div class="controls">
                        <?php echo CHtml::hiddenField('url',$this->createUrl('',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),array('readonly'=>TRUE));?>
                        <?php echo CHtml::hiddenField('berubah','',array('readonly'=>TRUE));?>
                        <?php echo CHtml::hiddenField('idObat'); ?>
                        <?php echo CHtml::hiddenField('qtyStok' ,0); ?>
<!--                <div class="input-append" style='display:inline'>-->
                    <?php 
                            $this->widget('MyJuiAutoComplete', array(
                                            'name'=>'namaObatNonRacik',
                                            'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/ObatReseptur').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                                   idSumberDana: $("#idSumberDana").val(),
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                             'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'focus'=> 'js:function( event, ui ) {
                                                        $(this).val("");
                                                        return false;
                                                    }',
                                                   'select'=>'js:function( event, ui ) {
                                                        $(this).val(ui.item.value);
                                                        $("#idObat").val(ui.item.obatalkes_id); 
                                                        $("#qtyStok").val(ui.item.qtyStok); 
                                                        $("#jumlahBahan").val(1); 
                                                        
                                                        $("#namaObat").val(ui.item.obatalkes_nama);
                                                        
                                                        return false;
                                                    }',
                                            ),
                                            'htmlOptions'=>array(
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogAlkes'),
                                        )); 
                        ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <label class="control-label" for="jumlah">Qty</label>
                    <div class="controls">
                        <?php echo CHtml::textField('jumlahBahan', '', array('readonly'=>false,'onblur'=>'$("#qty").val(this.value);','onkeypress'=>"return isNumberKey(event);",'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 number numbersOnly')) ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                array('onclick'=>'validasi();return false;',
                                      'class'=>'btn btn-primary numbersOnly',
                                      'onkeypress'=>"validasi();return $(this).focusNextInputField(event)",
                                      'rel'=>"tooltip",
                                      'title'=>"Klik untuk menambahkan resep",)); ?>
                    </div>
                </div>
            </fieldset>

<fieldset>
    <legend>
        <?php //echo CHtml::dropDownList('daftartindakanPemakaianBahan', '',array()) ?>
        <?php //echo CHtml::link('<i class="icon-plus icon-white"></i>', '#', array('class'=>'btn btn-primary','onclick'=>'$("#dialogAlkes").dialog("open");return false;')); ?>
        <!--Pemakaian Bahan-->
    </legend>
    <br/>
    <table class="items table table-striped table-bordered table-condensed" id="tblInputPemakaianBahan">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Bahan Obat & Kesehatan</th>
                <!--<th>Harga</th>-->
                <th>Qty</th>
                <!--<th>Sub Total</th>-->
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <div>
<!--        <b>Total Pemakaian Bahan : </b>-->
        <b>Total Qty Pemakaian Bahan : </b>
        <?php //echo CHtml::textField("totPemakaianBahan", 0,array('readonly'=>true,'class'=>'inputFormTabel currency')); ?>
        <?php echo CHtml::textField("totQtyPemakaianBahan", 0,array('readonly'=>true,'class'=>'inputFormTabel currency')); ?>
    </div>
</fieldset>

<?php
//========= Dialog buat cari data Alat Kesehatan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogAlkes',
    'options'=>array(
        'title'=>'Alat Kesehatan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>750,
        'height'=>600,
        'resizable'=>false,
    ),
));

$moObatAlkes = new RJObatAlkesM('search');
$moObatAlkes->unsetAttributes();
if(isset($_GET['RJObatAlkesM']))
    $moObatAlkes->attributes = $_GET['RJObatAlkesM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'rjobat-alkes-m-grid',
	'dataProvider'=>$moObatAlkes->searchObatFarmasi(),
	'filter'=>$moObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectObat",
                                    "onClick" => "
                                        $(\'#idObat\').val($data->obatalkes_id);
                                        $(\'#qtyStok\').val(".StokobatalkesT::getStokBarang($data->obatalkes_id, Yii::app()->user->getState(\'ruangan_id\')).");
                                        $(\'#namaObatNonRacik\').val(\'$data->obatalkes_nama\');
                                        $(\'#jumlahBahan\').val(1);
                                        validasi();
                                            
                                        $(\'#dialogAlkes\').dialog(\'close\');
                                        return false;"
                                        ))',
                ),
                'obatalkes_kategori',
		'obatalkes_nama',
                'obatalkes_golongan',
                array(
                    'name'=>'satuankecilNama',
                    'value'=>'$data->satuankecil->satuankecil_nama',
                ),
                array(
                    'name'=>'sumberdanaNama',
                    'value'=>'$data->sumberdana->sumberdana_nama',
                ),
                array(
                    'header'=>'Qty Stok',
                    'type'=>'raw',
                    'value'=>'StokobatalkesT::getStokBarang($data->obatalkes_id, Yii::app()->user->getState("ruangan_id"))',
                ),
                //'minimalstok',
		//'hargajual',
//                array(
//                    'name'=>'hargajual',
//                    'value'=>'MyFunction::formatNumber($data->hargajual)',
//                ),
                
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget();
?>

<script type="text/javascript">
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
function inputPemakaianBahan(idObatAlkes)
{
    var idDaftartindakan = $('#daftartindakanPemakaianBahan').val();
    var idPendaftaran = $('#idPendaftaran').val();
    var jumlahBahan = $('#jumlahBahan').val();
    if (jumlahBahan.length == 0){
        $('#jumlahBahan').val(1);
    }
    
       jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/addFormPemakaianBahan')?>',
                 'data':{idObatAlkes:idObatAlkes, idDaftartindakan:idDaftartindakan, idPendaftaran:idPendaftaran},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                       if(data.idPendaftaran == ''){
                           alert('Silahkan terlebih dahulu isi Data Pasien ');
                           return false;
                       }else{
                         $('#LKPendaftaranT_pasien_id').val(data.idPasien);
                         $('#namaObatNonRacik').val("");
                         $('#namaObatNonRacik').focus();
                         $("#idObat").val(idObatAlkes); 
                         
                         $('#tblInputPemakaianBahan #trPemakaianBahan').detach();
                         $('#tblInputPemakaianBahan > tbody').append(data.form);
                         
                         $('#tblInputPemakaianBahan tr:last').find('input[name*="qty"]').val($('#jumlahBahan').val());
                         $('#jumlahBahan').attr('value',1);
                         renameInput('pemakaianBahan', 'obatalkes_id');
                         renameInput('pemakaianBahan', 'hargajual');
                         renameInput('pemakaianBahan', 'qty');
                         renameInput('pemakaianBahan', 'subtotal');
                         renameInput('pemakaianBahan', 'daftartindakan_id');
                         
                         renameInput('pemakaianBahan', 'hargasatuan');
                         renameInput('pemakaianBahan', 'harganetto');
                         renameInput('pemakaianBahan', 'sumberdana_id');
                         renameInput('pemakaianBahan', 'satuankecil_id');
                         hitungTotal();
                         $('.qty').each(function(){
                             hitungSubTotal(this);
                         });
                         $("#tblInputPemakaianBahan > tbody > tr:last .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0});
                         $('.currency').each(function(){this.value = formatNumber(this.value)});
                         $("#tblInputPemakaianBahan > tbody > tr:last .number").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0,"symbol":null});
                         $('.number').each(function(){this.value = formatNumber(this.value)});
                       }
                 } ,
                 'cache':false});  
    
    function renameInput(modelName,attributeName)
    {
        var i = -1;
        $('#tblInputPemakaianBahan tr').each(function(){
            if($(this).has('input[name$="[obatalkes_id]"]').length){
                i++;
            }
            $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
            $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
            $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
            $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        });
    }
}

function removeObat(obj)
{
    if(confirm('Apakah anda akan menghapus obat?'))
        $(obj).parent().parent().remove();
    
    renameInputAfterRemove('pemakaianBahan', 'obatalkes_id');
    renameInputAfterRemove('pemakaianBahan', 'hargajual');
    renameInputAfterRemove('pemakaianBahan', 'qty');
    renameInputAfterRemove('pemakaianBahan', 'subtotal');
    renameInputAfterRemove('pemakaianBahan', 'daftartindakan_id');

    renameInputAfterRemove('pemakaianBahan', 'hargasatuan');
    renameInputAfterRemove('pemakaianBahan', 'harganetto');
    renameInputAfterRemove('pemakaianBahan', 'sumberdana_id');
    renameInputAfterRemove('pemakaianBahan', 'satuankecil_id');
    hitungTotal();
}

function renameInputAfterRemove(modelName,attributeName)
{
    var i = -1;
    $('#tblInputPemakaianBahan tr').each(function(){
        if($(this).has('input[name$="[obatalkes_id]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}
    
function hitungSubTotal(obj)
{
    var qty = obj.value;
    var harga = unformatNumber($(obj).parents("#tblInputPemakaianBahan tr").find('input[name$="[hargajual]"]').val());
    var subtotal = qty * harga;
    $(obj).parents("#tblInputPemakaianBahan tr").find('input[name$="[subtotal]"]').val(formatNumber(subtotal));
    hitungTotal(); 
}
    
function hitungTotal()
{
    var total = 0;
    var totalQty = 0;
//    $('#tblInputPemakaianBahan').find('input[name$="[subtotal]"]').each(function(){
//        total = total + unformatNumber(this.value);
//    });
    $('#tblInputPemakaianBahan').find('input[name$="[qty]"]').each(function(){
        totalQty = totalQty + unformatNumber(this.value);
    });
//    $('#totPemakaianBahan').val(formatNumber(total));
    $('#totQtyPemakaianBahan').val(formatNumber(totalQty));
}
function validasi(){
    var idObat = $('#idObat').val();
    var jumlahObat = parseFloat($('#jumlahBahan').val());
    var qtyStok = parseFloat($('#qtyStok').val());
    if (idObat == ''){
        alert('Obat Belum Diisi');
    } else if (jumlahObat == ''){
        alert('jumlah Obat Belum Diisi');
    } else if (jumlahObat < 1){
        alert('jumlah Obat Tidak Sesuai');
    } else if (qtyStok < jumlahObat){
        alert ('Stok Tidak Mencukupi !');
    }else {
        inputPemakaianBahan(idObat);
    }
    setTimeout(function(){resetInputBahan()},500);
}
function resetInputBahan(){
    $('#namaObatNonRacik').val("");
    $('#jumlahBahan').val("");
    $('#idObat').val("");
    $('#qtyStok').val(0);
    $('#namaObatNonRacik').focus();
}
function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
</script>

<?php 
$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});

//==================================================Validasi===============================================
//*Jangan Lupa Untuk menambahkan hiddenField dengan id "berubah" di setiap form
//* hidden field dengan id "url"
//*Copas Saja hiddenfield di Line 36 dan 35
//* ubah juga id button simpannya jadi "btn_simpan"


function palidasiForm(obj)
{
    var berubah = $('#berubah').val();
    if(berubah=='Ya'){
       if(confirm('Apakah Anda Akan menyimpan Perubahan Yang Sudah Dilakukan?')){
           $('#url').val(obj);
           $('#btn_simpan').click();
       }
    }      
}

JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>  

<?php 
$js = <<< JS
//==================================================Validasi===============================================
//*Jangan Lupa Untuk menambahkan hiddenField dengan id "berubah" di setiap form
//* hidden field dengan id "url"
//*Copas Saja hiddenfield di Line 36 dan 35
//* ubah juga id button simpannya jadi "btn_simpan"


function palidasiForm(obj)
{
    var berubah = $('#berubah').val();
    if(berubah=='Ya'){
       if(confirm('Apakah Anda Akan menyimpan Perubahan Yang Sudah Dilakukan?')){
           $('#url').val(obj);
           $('#btn_simpan').click();
       }
    }      
}

JS;
Yii::app()->clientScript->registerScript('js',$js,CClientScript::POS_READY);
?>   