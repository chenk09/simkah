<legend class="rim2">Rincian Tagihan Pasien Laboratorium</legend>
<?php

    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('#search').submit(function(){
            $.fn.yiiGridView.update('rinciantagihanpasienpenunjang-v-grid', {
                    data: $(this).serialize()
            });
            return false;
    });
    ");

$this->widget('bootstrap.widgets.BootAlert'); 
?>

<?php 
    $module  = $this->module->name; 
    $controller = $this->id;
?>
<?php $this->widget('bootstrap.widgets.BootAlert');	?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'rinciantagihanpasienpenunjang-v-grid',
	'dataProvider'=>$model->searchRincianTagihan(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tgl_pendaftaran',
                array(
                    'header'=>'No Rekam Medik<br/>No Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->no_rekam_medik.\'<br/>\'.$data->no_pendaftaran',
                ),
            
                array(
                    'header'=>'Nama Pasien',
                    'type'=>'raw',
                    'value'=>'$data->nama_pasien.\'<br/>\'.$data->nama_bin',
                ),
            
                array(
                    'header'=>'Cara Bayar<br/>Penjamin',
                    'type'=>'raw',
                    'value'=>'$data->CaraBayarPenjamin',
                ),
            
                'nama_pegawai',
                'jeniskasuspenyakit_nama',
//                array(
//                    'header'=>'Status Bayar',
//                    'type'=>'raw',
//                    'value'=>'(empty($data->pembayaranpelayanan_id)) ? "Belum Lunas" : "Lunas"' ,
//                ),
            
                array(
                'header'=>'Rincian <br/> Tagihan',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("rinciantagihanpasienV/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframePembayaran",
                                  "onclick"=>"$(\"#dialogPembayaran\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk membayar tagihan pasien",
                            ))',           'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
            
                array(
                'header'=>'Bayar Tagihan Pasien',
                'type'=>'raw',
                'value'=>'(empty($data->tindakansudahbayar_id) && empty($data->pembayaranpelayanan_id) ? CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->createUrl("billingKasir/pembayaran/index",array("idPendaftaran"=>$data->pendaftaran_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframePembayaran",
                                  "onclick"=>"$(\"#dialogPembayaran\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk membayar tagihan pasien",
                            )) : "-" )',           'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
            
                array(
                    'header'=>'Status Pembayaran',
                    'type'=>'raw',
                    'value'=>'(empty($data->tindakansudahbayar_id) || empty($data->pembayaranpelayanan_id) ? "Belum Bayar" : "Sudah Bayar"."<br/>".CHtml::Link("<i class=\"icon-list-silver\"></i>",Yii::app()->controller->createUrl("kwitansiLab/view",array("idPendaftaran"=>$data->pendaftaran_id,"idPasienadmisi"=>$data->pasienadmisi_id,"idPembayaranPelayanan"=>$data->pembayaranpelayanan_id,"frame"=>true)),
                                array("class"=>"", 
                                      "target"=>"iframeKwitansi",
                                      "onclick"=>"$(\"#dialogKwitansi\").dialog(\"open\");",
                                      "rel"=>"tooltip",
                                      "title"=>"Klik untuk cetak Kwitansi",
                                )))',           'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<div class="search-form">
    <?php 
        $this->renderPartial('laboratorium.views.pasienLaboratorium._search',array(
                    'model'=>$model,
        ));
    ?>
</div>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
        'id' => 'dialogKwitansi',
        'options' => array(
            'title' => 'Kwitansi Pasien',
            'autoOpen' => false,
            'modal' => true,
            'width' => 900,
            'height' => 550,
            'resizable' => false,
        ),
    ));
?>
<iframe name='iframeKwitansi' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
        'id' => 'dialogPembayaran',
        'options' => array(
            'title' => 'Pembayaran Tagihan Pasien Laboratorium',
            'autoOpen' => false,
            'modal' => true,
            'width' => 900,
            'height' => 550,
            'resizable' => false,
        ),
    ));
?>
<iframe name='iframePembayaran' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>