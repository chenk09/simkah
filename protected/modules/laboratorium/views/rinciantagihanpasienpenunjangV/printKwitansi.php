<style>
    /*.grid{
        font-size:11px;
        font-family: tahoma;
    }*/
    th {
                border: 1px solid #000;
                background-color: transparent;
                border-collapse: collapse;
            }
            .grid td{
                border: 1px solid #000;
                border-collapse: collapse;
                padding: 5px;
                border-collapse: collapse;
            }
            .grid_td td{
                border-collapse: collapse;
                font-size: 11px;
                border-collapse: collapse;
            }   
            .grid_top td{
                border-top: 1px;
                border-left: 1px;
                border-style: solid;
                font-size: 10pt;
                border-collapse: collapse;
            }
            .right{
                border-top: 1px;
                border-right: 1px;
                border-style: solid;
                border-collapse: collapse;
            }
            .grid_top_buttom td{
                border-top: 1px;
                border-bottom: 1px;
                border-left: 1px;
                border-style: solid;  
                font-size: 10pt;
                border-collapse: collapse;
            }
            
            th{
                text-align: center;
                font-size: 14px;
                border-collapse: collapse;
            }
            table{
                /*font-family: tahoma;*/
                width: 100%;
                border-collapse: collapse;
            }
            table, tr, td{
                border-collapse: collapse;
                font-family: sans-serif;
            }

</style>
<div align="center" style="font-family: tahoma;font-size:20px;"><b>KWITANSI</b></div> <br/>
<!-- <table cellspacing=0 width="100%"  class="grid">
   

        <tr class="grid_top">
            <td width="15%"> Nama Pasien</td>
            <td width="45%" style="border-left-style: 0px;">: <?php echo $modTandaBukti->pembayaran->pendaftaran->pasien->nama_pasien; ?></td>
            <td width="10%"></td>
            <td class="right" style="border-left-style: 0px;"></td>
        </tr>
        <tr>
            <td width="15%"> Alamat</td>
            <td width="2%">:</td>
            <td width="40%"><?php echo $modPendaftaran->pasien->alamat_pasien; ?></td>
            <td width="20%"> No Lab</td>
            <td width="2%">:</td>
            <td><?php echo $modPendaftaran->no_pendaftaran; ?></td>
        </tr>
        <tr>
            <td> Umur</td>
            <td>:</td>
            <td><?php echo $modPendaftaran->umur;?></td>
            <td> Tgl Pembayaran</td>
            <td>:</td>
            <td><?php //echo $modTandaBukti->pembayaran->tglpembayaran; ;?></td>
        </tr>
   
</table> -->


<table width="100%" border="0" class="grid_td" cellpadding="0" cellspacing="0">
    <tr class="grid_top">
        <td width="12%">Nama Pasien</td>
        <td width="45%" style="border-left-style: 0px;">: <?php echo $modPendaftaran->pasien->nama_pasien; ?></td>
        <td width="10%"></td>
        <td class="right" style="border-left-style: 0px;"></td>
    </tr>
    <tr class="grid_top">
        <td style="white-space: nowrap;">Alamat</td>
        <td style="border-left-style: 0px;">: <?php echo $modPendaftaran->pasien->alamat_pasien; ?></td>
        <td width="20%">No. Lab</td>
        <td class="right" style="border-left-style: 0px;">: <?php echo $modPendaftaran->no_pendaftaran; ?></td>
    </tr>
    <tr class="grid_top_buttom">
        <td>Umur </td>
        <td style="border-left-style: 0px;">: <?php echo $modPendaftaran->umur."; ".$modPendaftaran->pasien->jeniskelamin; ?></td>
        <td>Tgl Pembayaran</td>
        <td class="right" style="border-left-style: 0px;">: </td>
    </tr>
</table>

<div>&nbsp;</div>
<table cellspacing=0 width="100%" border="0">
    <tr>
        <td width="20%"> Sudah Terima Dari</td>
        <td width="2%">:</td>
        <td align="left"><?php echo $namapasien; //echo $modTandaBukti->darinama_bkm;?></td>
    </tr>
    <tr>
        <td> Banyak Uang</td>
        <td>:</td>
        <td><?php //echo $this->terbilang($modTandaBukti->jmlpembayaran);?>
            <?php 
                $total = 0;
                foreach($modRincian as $key=>$rincian){
                    $total += $rincian->tarif_tindakan;
                }
                    echo $this->terbilang($total);                            
            ?>
        </td>
    </tr>
    <tr>
        <td> Untuk Pembayaran</td>
        <td>:</td>
        <td><?php echo "Pemeriksaan di Laboratorium Klinik ".Params::NAMA_KLINIK//$modTandaBukti->sebagaipembayaran_bkm;?></td>
    </tr>
</table>
<table cellspacing=0 width="100%" border="0">
    <tbody>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="50%" align="center">
                <div align="center">
                    <div align="center" style="border:1px solid #000; width:200px;padding:10px;float:left;margin-left:-35px;">
                        <b>Rp <?php echo number_format($total,0,'','.');?></b>
                    </div>                                
                </div>
            </td>
            <td align="center">
                <div align="center">
                    <div>Tasikmalaya, <?php echo date('d M Y');?></div>
                     <div>&nbsp;</div>
                    <div>&nbsp;</div>
<!--                    <div>&nbsp;</div>
                    <div>&nbsp;</div>
                    <div>
                        <?php $pegawai = LoginpemakaiK::pegawaiLoginPemakai(); ?>
                        <b>(<?php echo $pegawai->nama_pegawai; ?>)</b>
                    </div> -->
                </div>
            </td>
        </tr>
    </tbody>
</table>