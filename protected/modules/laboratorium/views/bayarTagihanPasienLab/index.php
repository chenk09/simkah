<?php
$this->breadcrumbs=array(
	'Pembayaran',
);?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pembayaran-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        //'action'=>$this->createUrl('/billingKasir/pembayaran/prosesBayar'),
        'focus'=>'#TandabuktibayarT_biayaadministrasi',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInputTindakan();'),
));?>
<?php
    if(!empty($_GET['id'])){
?>
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
<?php } ?>
<?php $this->renderPartial('billingKasir.views.pembayaran._ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
<fieldset>
    <legend class="rim">Pembayaran Tindakan</legend>
    <table id="tblBayarTind" class="table table-bordered table-condensed">
        <thead>
            <tr>
                <?php $checkSemua = (isset($_GET['frame']) && !empty($_GET['idPendaftaran'])) ? true : false; ?>
                <th>Pilih<br><?php 
                                echo CHtml::checkBox('checkTindakan',$checkSemua, array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                'class'=>'checkbox-column','onclick'=>'checkAllTindakan()','checked'=>'checked')) ?></th>
                <th>Tanggal</th>
                <th>Nama Tindakan</th>
                <th>Qty</th>
                <th>Tarif</th>
                <th>Tarif Cyto</th>
                <th>Discount</th>
                <th>Subsidi Asuransi</th>
                <th>Subsidi Pemerintah</th>
                <th>Subsidi Rumah Sakit</th>
                <th>Iur Biaya</th>
                <th>Sub Total</th>
            </tr>
        </thead>
        <tbody>
        <?php
          if(!empty($_GET['idPendaftaran'])){

            $totTarifTind = 0 ;
            if (count($modTindakan) > 0){
                foreach($modTindakan as $j=>$tindakan){
                    $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
                }
            }
            $totHargaSatuanObat = 0;
            if (count($modObatalkes) > 0){
                foreach($modObatalkes as $i=>$obatAlkes) { 
                    $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
                }
            }
            $totalPembagi = $totTarifTind + $totHargaSatuanObat;
        ?>
        <?php 
              $totQty = 0; $totTarif = 0; $totCyto = 0; $totSubAsuransi = 0; $totSubPemerintah = 0; $totSubRs = 0; $totIur = 0; 
              $totPembebasanTarif = 0; $totDiscount_tindakan = 0; $totDiscount_oa = 0; $totBayarTind = 0; 
              if (count($modTindakan) > 0){
                    foreach($modTindakan as $i=>$tindakan) { 
            
                          $pembebasanTarif = PembebasantarifT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id));
                          $tarifBebas = 0; 
                          foreach ($pembebasanTarif as $i => $pembebasan) {
                              $tarifBebas = $tarifBebas + $pembebasan->jmlpembebasan;
                          }
                          $totPembebasanTarif = $totPembebasanTarif + $tarifBebas;
                          $disc = ($tindakan->discount_tindakan > 0) ? $tindakan->discount_tindakan/100 : 0;
                          $discountTindakan = ($disc*$tindakan->tarif_satuan*$tindakan->qty_tindakan);
                          $totDiscount_tindakan += $discountTindakan ;
                    
                          $subsidi = $this->cekSubsidi($tindakan);
                          $tarifSatuan = $tindakan->tarif_satuan;
                          $qtyTindakan = $tindakan->qty_tindakan; $totQty = $totQty + $qtyTindakan; 
                          $tarifTindakan = $tindakan->tarif_tindakan; $totTarif = $totTarif + $tarifTindakan; 
                          $tarifCyto = $tindakan->tarifcyto_tindakan; $totCyto = $totCyto + $tarifCyto; 
                          if(!empty($subsidi['max'])){
                              $subsidiAsuransi = round($tarifTindakan/$totalPembagi * $subsidi['max']); 
                              $subsidiPemerintah = 0; 
                              $subsidiRumahSakit = 0; 
                              
                              $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi; 
                              $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                              $totSubRs = $totSubRs + $subsidiRumahSakit; 
                              $iurBiaya = round(($tarifSatuan + $tarifCyto)); 
                              $totIur = $totIur + $iurBiaya; 
                              $subTotalTind = round($iurBiaya * $qtyTindakan) - $subsidiAsuransi; 
                              $subTotalTind -= $discountTindakan;
                              $subTotalTind = ($subTotalTind > 0) ? $subTotalTind : 0; 
                              $totBayarTind = $totBayarTind + $subTotalTind;
                          } else {
                              $subsidiAsuransi = $subsidi['asuransi']; 
                              $subsidiPemerintah = $subsidi['pemerintah']; 
                              $subsidiRumahSakit = $subsidi['rumahsakit'];
                              
                              $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi; 
                              $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                              $totSubRs = $totSubRs + $subsidiRumahSakit; 
                              $iurBiaya = round(($tarifSatuan + $tarifCyto) - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit)); 
                              $totIur = $totIur + $iurBiaya; 
                              $subTotalTind = round($iurBiaya * $qtyTindakan);
                              $subTotalTind -= $discountTindakan;
                              $totBayarTind = $totBayarTind + $subTotalTind;
                          }

                    }
          
                      ?>
            <tr>
                <td>
                    <?php echo CHtml::checkBox("pembayaran[$i][tindakanpelayanan_id]", true, array('onchange'=>'hitungTotalSemuaTind();','value'=>$tindakan->tindakanpelayanan_id,'uncheckValue'=>'0', 'class'=>'pilihan')) ?>
                </td>
                <td>
                    <?php echo $tindakan->tgl_tindakan ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][tgl_tindakan]",$tindakan->tgl_tindakan, array('readonly'=>true,'class'=>'inputFormTabel span2')); ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][carabayar_id]",$tindakan->carabayar_id, array('readonly'=>true)); ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][penjamin_id]",$tindakan->penjamin_id, array('readonly'=>true)); ?> 
                    <?php echo CHtml::hiddenField("pembayaran[$i][pembebasan_tarif]",$tarifBebas, array('readonly'=>true)); ?>
                </td>
                <td>
                    <?php echo ((isset($tindakan->tipepaket->tipepaket_nama)) ? $tindakan->tipepaket->tipepaket_nama : null).' - '.  ((isset($tindakan->daftartindakan->daftartindakan_nama)) ? $tindakan->daftartindakan->daftartindakan_nama : null); ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][daftartindakan_id]", $tindakan->daftartindakan_id, array('readonly'=>true,'class'=>'inputFormTabel lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][qty_tindakan]", $tindakan->qty_tindakan, array('readonly'=>false,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel number lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][tarif_satuan]", $tindakan->tarif_satuan, array('readonly'=>false,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3')); ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][tarif_tindakan]", $tindakan->tarif_tindakan, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][tarifcyto_tindakan]", $tindakan->tarifcyto_tindakan, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][discount_tindakan]",$discountTindakan, array('readonly'=>true, 'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][subsidiasuransi_tindakan]", $subsidiAsuransi, array('readonly'=>false,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][subsidipemerintah_tindakan]", $subsidiPemerintah, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][subsisidirumahsakit_tindakan]", $subsidiRumahSakit, array('readonly'=>true,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][iurbiaya_tindakan]", $iurBiaya, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][sub_total]", $subTotalTind, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                    <?php //echo $tindakan->daftartindakan_id; ?>
                </td>
            </tr>
        <?php }               }?>
            <tr class="trfooter">
                <td colspan="3">Total <?php //echo $subsidi['max']; ?></td>
                <td>
                    <?php echo CHtml::textField("totalqtytindakan", $totQty, array('readonly'=>true,'class'=>'inputFormTabel number lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalbiayatindakan", $totTarif, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalcyto", $totCyto, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totaldiscount_tindakan", $totDiscount_tindakan, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidiasuransi", $totSubAsuransi, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidipemerintah", $totSubPemerintah, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidirs", $totSubRs, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totaliurbiaya", $totIur, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalbayartindakan", $totBayarTind, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                    <?php echo CHtml::hiddenField("totalpembebasan", $totPembebasanTarif, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?> 
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend class="rim">Pembayaran Obat Alkes</legend>
    <table id="tblBayarOA" class="table table-bordered table-condensed">
        <thead>
            <tr>
                <?php $checkSemua = (isset($_GET['frame']) && !empty($_GET['idPendaftaran'])) ? true : false; ?>
                <th>Pilih<br><?php 
                                echo CHtml::checkBox('checkAllObat',$checkSemua, array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                'class'=>'checkbox-column','onclick'=>'checkAll()','checked'=>'checked')) ?></th>
                <th>Tanggal</th>
                <th>Nama Obat Alkes</th>
                <th>Qty</th>
                <th>Harga Satuan</th>
                <th>Discount</th>
                <th>Adm/Serv/Kons</th>
                <th>Subsidi Asuransi</th>
                <th>Subsidi Pemerintah</th>
                <th>Subsidi Rumah Sakit</th>
                <th>Iur Biaya</th>
                <th>Sub Total</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $totBayarOa = 0;
            $totDiscount_oa = 0;
            $subsidiOa = 0;
            $totQtyOa = 0;
            $totHargaJualOa = 0;
            $totCytoOa = 0;
            $totSubAsuransiOa = 0;
            $totSubPemerintahOa = 0;
            $totSubRsOa = 0;
            $totIurOa = 0;
            $subTotalOa = 0;
            if (isset($modObatalkes) && (count($modObatalkes) > 0)){
                foreach($modObatalkes as $i=>$obatAlkes) { 
                    $disc = ($obatAlkes->discount > 0) ? $obatAlkes->discount/100 : 0;
                    $discount_oa = ($disc*$obatAlkes->qty_oa*$obatAlkes->hargasatuan_oa);
                    $totDiscount_oa += $discount_oa;
                    $subsidiOa = $this->cekSubsidiOa($obatAlkes);
                    $totQtyOa = $totQtyOa + $obatAlkes->qty_oa; 
                    $totHargaJualOa = $totHargaJualOa + $obatAlkes->hargasatuan_oa; 
                    $oaHargajual = $obatAlkes->hargasatuan_oa; 
                    $biayaServiceResep = $obatAlkes->biayaservice;
                    if (isset($obatAlkes->obatalkes)){
                        if($obatAlkes->obatalkes->obatalkes_kategori == "GENERIK" OR $obatAlkes->obatalkes->jenisobatalkes_id == 3){
                            $biayaServiceResep = 0;
                        }
                    }
                    $oaCyto = $obatAlkes->biayaadministrasi + $obatAlkes->biayakonseling + $biayaServiceResep; 
                    $totCytoOa += $oaCyto; 
                    if(!empty($subsidiOa['max'])){
                        $oaSubsidiasuransi = round($oaHargajual/$totalPembagi * $subsidiOa['max']); 
                        $oaSubsidipemerintah = 0; 
                        $oaSubsidirs = 0; 
                        
                        $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi; 
                        $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah; 
                        $totSubRsOa = $totSubRsOa + $oaSubsidirs; 
//                        $oaIurbiaya = round(($oaHargajual + $oaCyto)); // tarif tanpa $oaCyto
                        $oaIurbiaya = round(($oaHargajual)); 
                        $obatAlkes->iurbiaya = $oaIurbiaya; 
                        $totIurOa = $totIurOa + $oaIurbiaya; 
                        $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) - $oaSubsidiasuransi + $oaCyto - $discount_oa;
                        $subTotalOa = ($subTotalOa > 0) ? $subTotalOa : 0;
                        $totBayarOa = $totBayarOa + $subTotalOa;
                    } else {
                        $oaSubsidiasuransi = $subsidiOa['asuransi']; 
                        $oaSubsidipemerintah = $subsidiOa['pemerintah']; 
                        $oaSubsidirs = $subsidiOa['rumahsakit']; 
                        
                        $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi; 
                        $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah; 
                        $totSubRsOa = $totSubRsOa + $oaSubsidirs; 
//                        $oaIurbiaya = round(($oaHargajual + $oaCyto) - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs)); // tarif tanpa $oaCyto
                        $oaIurbiaya = round(($oaHargajual) - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs)); // tarif tanpa $oaCyto
                        $obatAlkes->iurbiaya = $oaIurbiaya; 
                        $totIurOa = $totIurOa + $oaIurbiaya; 
                        $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) + $oaCyto - $discount_oa; 
                        $totBayarOa = $totBayarOa + $subTotalOa;
                    }
        ?>
            <tr>
                <td><?php echo CHtml::checkBox("pembayaranAlkes[$i][obatalkespasien_id]", true, array('onchange'=>'hitungTotalSemuaOa();','value'=>$obatAlkes->obatalkespasien_id,'uncheckValue'=>'0', 'class'=>'pilihan')) ?></td>
                <td><?php echo $obatAlkes->tglpelayanan ?></td>
                <td>
                    <?php //echo $obatAlkes->daftartindakan->daftartindakan_nama ?>
                    <?php echo ((isset($obatAlkes->obatalkes->obatalkes_nama)) ? $obatAlkes->obatalkes->obatalkes_nama : null); ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][obatalkes_id]" ,$obatAlkes->obatalkes_id, array('readonly'=>true,'class'=>'inputFormTabel lebar2')); ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][carabayar_id]",$obatAlkes->carabayar_id, array('readonly'=>true)); ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][penjamin_id]",$obatAlkes->penjamin_id, array('readonly'=>true)); ?>
                    <?php //echo CHtml::hiddenField("pembayaranAlkes[$i][discount]",$obatAlkes->discount, array('readonly'=>true)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][qty_oa]", $obatAlkes->qty_oa, array('onblur'=>'hitungTotalSemuaOa();','readonly'=>false,'class'=>'inputFormTabel number lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][hargajual_oa]", $obatAlkes->hargajual_oa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][hargasatuan]",$obatAlkes->hargasatuan_oa, array('onblur'=>'hitungTotalSemuaOa();','readonly'=>false,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][discount]",$discount_oa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][tarifcyto]", $oaCyto, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][subsidiasuransi]", $oaSubsidiasuransi, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][subsidipemerintah]", $oaSubsidipemerintah, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][subsidirs]", $oaSubsidirs, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][iurbiaya]", $oaIurbiaya, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php //echo $obatAlkes->daftartindakan_id ?>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][sub_total]", $subTotalOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
            </tr>
        <?php } 
        
        }?>
            <tr class="trfooter">
                <td colspan="3">Total <?php //echo $subsidiOa['max']; ?></td>
                <td>
                    <?php echo CHtml::textField("totalqty_oa", $totQtyOa, array('readonly'=>true,'class'=>'inputFormTabel number lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalbiaya_oa", $totHargaJualOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totaldiscount_oa", $totDiscount_oa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalcyto_oa", $totCytoOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidiasuransi_oa", $totSubAsuransiOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidipemerintah_oa", $totSubPemerintahOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidirs_oa", $totSubRsOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totaliurbiaya_oa", $totIurOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalbayar_oa", $totBayarOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<?php
$pembulatanHarga = KonfigfarmasiK::model()->find()->pembulatanharga;
$totTagihan = round($totBayarTind + $totBayarOa);
$totDeposit = 0;
if ($modTandaBukti->isNewRecord){
    $modTandaBukti->jmlpembayaran = $totTagihan;
    $modTandaBukti->biayaadministrasi = 0;
    $modTandaBukti->biayamaterai = 0;
    $modTandaBukti->uangkembalian = 0;
    $modTandaBukti->uangditerima = $totTagihan;
    $pembulatan = ($pembulatanHarga > 0) ? $totTagihan % $pembulatanHarga : 0 ;
    if($pembulatan>0){
        $modTandaBukti->jmlpembulatan = $pembulatanHarga - $pembulatan;
        $modTandaBukti->jmlpembayaran = $modTandaBukti->jmlpembayaran + $modTandaBukti->jmlpembulatan - $totDiscount_tindakan -$totDiscount_oa - $totPembebasanTarif - $totDeposit;
        $harusDibayar = $modTandaBukti->jmlpembayaran;
    } else {
        $modTandaBukti->jmlpembulatan = 0;
    }
    $modTandaBukti->uangditerima = $modTandaBukti->jmlpembayaran;
}
?>

<fieldset>
    <legend class="rim">Data Pembayaran</legend>
    <table>
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Tagihan','harusDibayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('totTagihan',$totTagihan,array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Discount','disc', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('disc',0,array('readonly'=>false,'onblur'=>'hitungDiscount();','class'=>'inputFormTabel number lebar1', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?> %
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Discount Rupiah','discRupiah', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        Rp.<?php echo CHtml::textField('discRupiah',0,array('readonly'=>false,'onblur'=>'hitungDiscountRupiah();','class'=>'inputFormTabel number lebar3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('','discount', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('discount',$modBayar->totaldiscount,array('readonly'=>true,'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Deposit','deposit', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('deposit',$totDeposit,array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Pembebasan','totPembebasan', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('totPembebasan',$totPembebasanTarif,array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modTandaBukti,'jmlpembulatan',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'biayaadministrasi',array('onkeyup'=>'hitungJmlBayar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'biayamaterai',array('onkeyup'=>'hitungJmlBayar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'jmlpembayaran',array('onkeyup'=>'hitungKembalian();','readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'uangditerima',array('onkeyup'=>'hitungKembalian(this);','class'=>'inputFormTabel currency span3', 'onblur'=>'cekKembalian();' )); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'uangkembalian',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
            <td>
                <div class="control-group ">
                    <?php $modTandaBukti->tglbuktibayar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modTandaBukti->tglbuktibayar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo $form->labelEx($modTandaBukti,'tglbuktibayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modTandaBukti,
                                                'attribute'=>'tglbuktibayar',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($modTandaBukti,'tglbuktibayar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

                <?php echo $form->dropDownListRow($modTandaBukti,'carapembayaran',  CaraPembayaran::items(),array('onchange'=>'ubahCaraPembayaran(this)','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <div class="control-group ">
                    <?php echo CHtml::label('Menggunakan Kartu','pakeKartu', array('class'=>'control-label inline')) ?> 
                    <div class="controls">
                        <?php echo CHtml::checkBox('pakeKartu',false,array('onchange'=>"enableInputKartu();", 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?> <i class="icon-chevron-down"></i>
                    </div>
                </div>
                <div id="divDenganKartu" class="hide">
                    <?php echo $form->dropDownListRow($modTandaBukti,'dengankartu',  DenganKartu::items(),array('onchange'=>'enableInputKartu()','empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'bankkartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'nokartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'nostrukkartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </div>

                <?php echo $form->textFieldRow($modTandaBukti,'darinama_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textAreaRow($modTandaBukti,'alamat_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'sebagaipembayaran_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            </td>
        </tr>
    </table>
</fieldset>

    <?php //echo $form->textFieldRow($modTandaBukti,'closingkasir_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'shift_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'bayaruangmuka_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'pembayaranpelayanan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'nourutkasir',array('class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'nobuktibayar',array('class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'keterangan_pembayaran',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
    <div class="form-actions">
             <?php 
                if($modBayar->isNewRecord)
                echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); 
                else
                    echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','disabled'=>true)); 
                echo "&nbsp;&nbsp;";
                $reffUrl = ((isset($_GET['frame']) && !empty($_GET['idPendaftaran'])) ? array('modulId'=>Yii::app()->session['modulId'], 'frame'=>$_GET['frame'], 'idPendaftaran'=>$_GET['idPendaftaran']) : array('modulId'=>Yii::app()->session['modulId']));
                if(!$modBayar->isNewRecord)
                    echo CHtml::link(Yii::t('mds', '{icon} Print Kwitansi', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printKasir($modTandaBukti->tandabuktibayar_id);return false",'disabled'=>false)).
                    CHtml::link(Yii::t('mds', '{icon} Print Rincian', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printRincianKasir($modTandaBukti->tandabuktibayar_id);return false",'disabled'=>false));
                else
                    echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"return false",'disabled'=>true)); 
            ?>
            <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('index',$reffUrl), array('class'=>'btn btn-danger')); ?>
<?php  
$content = $this->renderPartial('billingKasir.views.tips.transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<?php 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/kwitansi/view');
        $urlPrintRincian=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/kwitansi/viewRincian');
        //http://localhost/ehospitaljk/index.php?r=billingKasir/RinciantagihanpasienV/rincian&id=389&frame=1

$js = <<< JSCRIPT
function printKasir(caraPrint)
{
    window.open("${urlPrint}/"+$('#sanapza-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
function printRincianKasir(caraPrint)
{
    window.open("${urlPrintRincian}/"+$('#sanapza-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>

<div id="testForm"></div>

<script type="text/javascript">
$('.currency').each(function(){this.value = formatUang(this.value)});

function hitungTotalTind(obj)
{
    var qty = unformatNumber(obj.value);
    var tarifSatuan = unformatNumber($(obj).parents('tr').find('input[name$="[tarif_satuan]"]').val());
    var tarifTind = unformatNumber($(obj).parents('tr').find('input[name$="[tarif_tindakan]"]').val());
    var tarifCyto = unformatNumber($(obj).parents('tr').find('input[name$="[tarifcyto_tindakan]"]').val());
    var subAsu = unformatNumber($(obj).parents('tr').find('input[name$="[subsidiasuransi_tindakan]"]').val());
    var subPem = unformatNumber($(obj).parents('tr').find('input[name$="[subsidipemerintah_tindakan]"]').val());
    var subRs = unformatNumber($(obj).parents('tr').find('input[name$="[subsisidirumahsakit_tindakan]"]').val());
    var discTindakan = unformatNumber($(obj).parents('tr').find('input[name$="[discount_tindakan]"]').val());
    var iurBiaya = tarifSatuan + tarifCyto - subAsu - subPem - subRs; // tarif tanpa tarifCyto
//    var iurBiaya = tarifSatuan - subAsu - subPem - subRs; // tarif tanpa tarifCyto
    $(obj).parents('tr').find('input[name$="[iurbiaya_tindakan]"]').val(formatUang(iurBiaya));
    $(obj).parents('tr').find('input[name$="[sub_total]"]').val(formatUang(qty*iurBiaya));
    
    var totSubTotal = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[sub_total]"]').each(function(){
            totSubTotal = totSubTotal + unformatNumber(this.value);
        });
    });
    $('#totalbayartindakan').val(formatUang(totSubTotal));
    
    var totIurBiaya = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[iurbiaya_tindakan]"]').each(function(){
            totIurBiaya = totIurBiaya + unformatNumber(this.value);
        });
    });
    $('#totaliurbiaya').val(formatUang(totIurBiaya));
    
    var totSubRs = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[subsisidirumahsakit_tindakan]"]').each(function(){
            totSubRs = totSubRs + unformatNumber(this.value);
        });
    });
    $('#totalsubsidirs').val(formatUang(totSubRs));
    
    var totSubPem = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[subsidipemerintah_tindakan]"]').each(function(){
            totSubPem = totSubPem + unformatNumber(this.value);
        });
    });
    $('#totalsubsidipemerintah').val(formatUang(totSubPem));
    
    var totSubAsu = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[subsidiasuransi_tindakan]"]').each(function(){
            totSubAsu = totSubAsu + unformatNumber(this.value);
        });
    });
    $('#totalsubsidiasuransi').val(formatUang(totSubAsu));
    
    var totCyto = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[tarifcyto_tindakan]"]').each(function(){
            totCyto = totCyto + unformatNumber(this.value);
        });
    });
    $('#totalcyto').val(formatNumber(totCyto));
    
    var totTarifTind = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[tarif_satuan]"]').each(function(){
            totTarifTind = totTarifTind + unformatNumber(this.value);
        });
    });
    $('#totalbiayatindakan').val(formatUang(totTarifTind));
    
    var totQty = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[qty_tindakan]"]').each(function(){
            totQty = totQty + unformatNumber(this.value);
        });
    });
    $('#totalqtytindakan').val(formatUang(totQty));
    
    var totDiscount = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[discount_tindakan]"]').each(function(){
            totDiscount += unformatNumber(this.value);
        });
    });
    $('#totaldiscount_tindakan').val(formatUang(totDiscount));
}

function hitungTotalSemuaTind()
{
    $('#tblBayarTind').find('input[name$="[qty_tindakan]"]').each(function(){
        hitungTotalTind(this);
    });
    hitungJmlBayar();
}

function hitungDiscount()
{
    var persenDisc = unformatNumber($('#disc').val());
    var totTagihan = unformatNumber($('#totalbayar_oa').val()) + unformatNumber($('#totalbayartindakan').val());
    var discRupiah = unformatNumber($('#discRupiah').val());
    
    discount = persenDisc / 100 * totTagihan;
    $('#discount').val(formatNumber(discount));
    if(persenDisc > 0){
        $('#discRupiah').attr('readonly','readonly');
    }else if(persenDisc <= 0){
         $('#discRupiah').removeAttr('readonly','readonly');
         $('#discRupiah').val(0);
    }
    
    hitungJmlBayar();
}


function hitungDiscountRupiah()
{
    var persenDisc = unformatNumber($('#discRupiah').val());
    var totTagihan = unformatNumber($('#totalbayar_oa').val()) + unformatNumber($('#totalbayartindakan').val());
    var discPersen = unformatNumber($('#disc').val());
    
    discount = persenDisc;
    $('#discount').val(formatNumber(discount));
    if(persenDisc > 0){
        $('#disc').attr('readonly','readonly');
    }else if(persenDisc <= 0){
         $('#disc').removeAttr('readonly','readonly');
         $('#disc').val(0);
    }
    hitungJmlBayar();
}

function hitungKembalian(obj)
{
    var jmlBayar = unformatNumber($('#TandabuktibayarT_jmlpembayaran').val());
    var uangDiterima = unformatNumber($('#TandabuktibayarT_uangditerima').val());
    var uangKembalian;
    if ($("#<?php echo CHtml::activeId($modTandaBukti,'carapembayaran'); ?>").val() == 'CICILAN'){
        if ($(obj).attr("id") == '<?php echo CHtml::activeId($modTandaBukti,'uangditerima'); ?>' ){
            if (uangDiterima < jmlBayar){
                uangDiterima = jmlBayar;
//                alert("Uang diterima tidak boleh kurang dari jumlah pembayaran");
            }
        }
        else{
            uangDiterima = jmlBayar;
        }
    }
    uangKembalian = uangDiterima - jmlBayar;
    
    $('#TandabuktibayarT_uangditerima').val(formatUang(uangDiterima));
    $('#TandabuktibayarT_uangkembalian').val(formatUang(uangKembalian));
}

function cekKembalian()
{
    var jmlBayar = unformatNumber($('#TandabuktibayarT_jmlpembayaran').val());
    var uangDiterima = unformatNumber($('#TandabuktibayarT_uangditerima').val());
    
    if($('#TandabuktibayarT_carapembayaran').val() === 'TUNAI')
    {
        if(uangDiterima < jmlBayar) {
            if(confirm('Uang diterima tidak boleh lebih kecil dari Jumlah Pembayaran')){
                uangDiterima = jmlBayar;
            } else {
                $('#TandabuktibayarT_uangditerima').addClass('error');
                $('#TandabuktibayarT_uangditerima').focus();
                $('#TandabuktibayarT_uangditerima').select();
            }
        }
    }
    uangKembalian = uangDiterima - jmlBayar;
    $('#TandabuktibayarT_uangditerima').val(formatUang(uangDiterima));
    $('#TandabuktibayarT_uangkembalian').val(formatUang(uangKembalian));
}

function enableInputKartu()
{
    if($('#pakeKartu').is(':checked'))
        $('#divDenganKartu').show();
    else 
        $('#divDenganKartu').hide();
    if($('#TandabuktibayarT_dengankartu').val() != ''){
        //alert('isi');
        $('#TandabuktibayarT_bankkartu').removeAttr('readonly');
        $('#TandabuktibayarT_nokartu').removeAttr('readonly');
        $('#TandabuktibayarT_nostrukkartu').removeAttr('readonly');
    } else {
        //alert('kosong');
        $('#TandabuktibayarT_bankkartu').attr('readonly','readonly');
        $('#TandabuktibayarT_nokartu').attr('readonly','readonly');
        $('#TandabuktibayarT_nostrukkartu').attr('readonly','readonly');
        
        $('#TandabuktibayarT_bankkartu').val('');
        $('#TandabuktibayarT_nokartu').val('');
        $('#TandabuktibayarT_nostrukkartu').val('');
    }
}

function hitungJmlBayar()
{
    var totTagihan = unformatNumber($('#totalbayar_oa').val()) + unformatNumber($('#totalbayartindakan').val());
    var biayaAdministrasi = unformatNumber($('#TandabuktibayarT_biayaadministrasi').val());
    var biayaMaterai = unformatNumber($('#TandabuktibayarT_biayamaterai').val());
    var deposit = unformatNumber($('#deposit').val());
    var totPembebasan = unformatNumber($('#totPembebasan').val());
    var totDiscountTind = unformatNumber($('#totaldiscount_tindakan').val());
    var totDiscountOa = unformatNumber($('#totaldiscount_oa').val());
    var totDiscount = totDiscountTind+totDiscountOa;
    var totBayar = 0;
    var discount = unformatNumber($('#discount').val());
    var jmlPembulatan = unformatNumber($('#TandabuktibayarT_jmlpembulatan').val());
    var pembulatan = (totTagihan - discount) % <?php echo $pembulatanHarga; ?>;
    
    jmlPembulatan = <?php echo $pembulatanHarga; ?> - pembulatan;
    if (jQuery.isNumeric(jmlPembulatan)){  
        if (jmlPembulatan >= <?php echo $pembulatanHarga; ?>){
            jmlPembulatan = 0;
        }
        $('#TandabuktibayarT_jmlpembulatan').val(formatUang(jmlPembulatan));
    }
    $('#totTagihan').val(formatUang(totTagihan));
    totBayar = totTagihan + jmlPembulatan + biayaAdministrasi + biayaMaterai - discount - totPembebasan - deposit;
    $('#TandabuktibayarT_uangditerima').val(totBayar);
    $('#TandabuktibayarT_jmlpembayaran').val(formatUang(totBayar));
    hitungKembalian();
}

function ubahCaraPembayaran(obj)
{
    if(obj.value == 'CICILAN' || obj.value == 'PIUTANG' || obj.value == 'HUTANG'){
        $('#TandabuktibayarT_jmlpembayaran').removeAttr('readonly');
    } else {
        $('#TandabuktibayarT_jmlpembayaran').attr('readonly', true);
        hitungJmlBayar();
    }
    
    if(obj.value == 'TUNAI'){
        hitungJmlBayar();
    } 
}

function printKasir(idTandaBukti)
{
    if(idTandaBukti!=''){ 
             window.open('<?php echo Yii::app()->createUrl('print/bayarKasir',array('idTandaBukti'=>'')); ?>'+idTandaBukti,'printwin','left=100,top=100,width=400,height=400,scrollbars=1');
    }     
}

function printRincianKasir(idTandaBukti)
{
    if(idTandaBukti!=''){ 
             window.open('<?php echo Yii::app()->createUrl('print/rincianKasirBaru',array('idTandaBukti'=>'')); ?>'+idTandaBukti,'printwin','left=100,top=100,width=400,height=400,scrollbars=1');
    }     
}

function cekInputTindakan()
{
    jumlahPilihan = $(".pilihan:checked").length;
    if (jumlahPilihan < 1){
        alert('Tidak ada tindakan / Obat Alkes');
        return false;
    }
    
    if($('#TandabuktibayarT_sebagaipembayaran_bkm').val() == ''){
        alert('"Sebagai Pembayaran" tidak boleh kosong!');
        $('#TandabuktibayarT_sebagaipembayaran_bkm').addClass('error');
        return false;
    }
    
    if($('#totalbayartindakan').val() <=0 && $('#totalbayar_oa').val()<=0){
        alert('Tidak ada tindakan / Obat Alkes');
        return false;
    } else {
        $('.currency').each(function(){this.value = unformatNumber(this.value)});
        $('.number').each(function(){this.value = unformatNumber(this.value)});
        return true;
    }
}

function printRincianTagihan()
{
    alert('oii');
}

function hitungTagihan(obj)
{
    var obQty = $(obj).parents('tr').find('input[name$="[qty_oa]"]');
    var obTarif = $(obj).parents('tr').find('input[name$="[hargajual_oa]"]');
    var obCyto = $(obj).parents('tr').find('input[name$="[tarifcyto]"]');
    var obSubas = $(obj).parents('tr').find('input[name$="[subsidiasuransi]"]');
    var obSubpem = $(obj).parents('tr').find('input[name$="[subsidipemerintah]"]');
    var obSubrs = $(obj).parents('tr').find('input[name$="[subsidirs]"]');
    var obIurbiaya = $(obj).parents('tr').find('input[name$="[iurbiaya]"]');
    var obSubtotal = $(obj).parents('tr').find('input[name$="[sub_total]"]');
    
    var qty = unformatNumber($(obQty).val());
    var tarif = unformatNumber($(obTarif).val());
}

function hitungTotalOa(obj)
{
    var qty = unformatNumber(obj.value);
    var hargasatuan = unformatNumber($(obj).parents('tr').find('input[name$="[hargasatuan]"]').val());
    var tarifCyto = unformatNumber($(obj).parents('tr').find('input[name$="[tarifcyto]"]').val());
    var subAsu = unformatNumber($(obj).parents('tr').find('input[name$="[subsidiasuransi]"]').val());
    var subPem = unformatNumber($(obj).parents('tr').find('input[name$="[subsidipemerintah]"]').val());
    var subRs = unformatNumber($(obj).parents('tr').find('input[name$="[subsidirs]"]').val());
    var discount = unformatNumber($(obj).parents('tr').find('input[name$="[discount]"]').val());
//    var iurBiaya = hargasatuan + tarifCyto - subAsu - subPem - subRs;// tanpa tarifCyto
    var iurBiaya = hargasatuan - subAsu - subPem - subRs;// tanpa tarifCyto
    $(obj).parents('tr').find('input[name$="[iurbiaya]"]').val(formatUang(iurBiaya));
    $(obj).parents('tr').find('input[name$="[sub_total]"]').val(formatUang((qty*iurBiaya)+tarifCyto-discount));
    $(obj).parents('tr').find('input[name$="[totalcyto_oa]"]').val(formatUang(tarifCyto));
    
    var totSubTotal = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[sub_total]"]').each(function(){
            totSubTotal = totSubTotal + unformatNumber(this.value);
        });
    });
    $('#totalbayar_oa').val(formatUang(totSubTotal));
    
    var totIurBiaya = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[iurbiaya]"]').each(function(){
            totIurBiaya = totIurBiaya + unformatNumber(this.value);
        });
    });
    $('#totaliurbiaya_oa').val(formatUang(totIurBiaya));
    
    var totSubRs = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[subsidirs]"]').each(function(){
            totSubRs = totSubRs + unformatNumber(this.value);
        });
    });
    $('#totalsubsidirs_oa').val(formatUang(totSubRs));
    
    var totSubPem = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[subsidipemerintah]"]').each(function(){
            totSubPem = totSubPem + unformatNumber(this.value);
        });
    });
    $('#totalsubsidipemerintah_oa').val(formatUang(totSubPem));
    
    var totSubAsu = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[subsidiasuransi]"]').each(function(){
            totSubAsu = totSubAsu + unformatNumber(this.value);
        });
    });
    $('#totalsubsidiasuransi_oa').val(formatUang(totSubAsu));
    
    var totCyto = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[tarifcyto]"]').each(function(){
            totCyto = totCyto + unformatNumber(this.value);
        });
    });
    $('#totalcyto_oa').val(formatNumber(totCyto));
    
    var totTarifOa = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[hargasatuan]"]').each(function(){
            totTarifOa = totTarifOa + unformatNumber(this.value);
        });
    });
    $('#totalbiaya_oa').val(formatUang(totTarifOa));
    
    var totQty = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[qty_oa]"]').each(function(){
            totQty = totQty + unformatNumber(this.value);
        });
    });
    $('#totalqty_oa').val(formatUang(totQty));
    
    var totDiscount = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[discount]"]').each(function(){
            totDiscount += unformatNumber(this.value);
        });
    });
    $('#totaldiscount_oa').val(formatUang(totDiscount));
}

function hitungTotalSemuaOa()
{
    $('#tblBayarOA').find('input[name$="[qty_oa]"]').each(function(){
        hitungTotalOa(this);
    });
    hitungJmlBayar();
}

function checkAll() {
    if ($("#checkAllObat").is(":checked")) {
        $('#tblBayarOA input[name*="obatalkespasien_id"]').each(function(){
           $(this).attr('checked',true);
        })
//        alert('Checked');
    } else {
       $('#tblBayarOA input[name*="obatalkespasien_id"]').each(function(){
           $(this).removeAttr('checked');
        })
    }
    hitungTotalSemuaOa();
}

function checkAllTindakan() {
    if ($("#checkTindakan").is(":checked")) {
        $('#tblBayarTind input[name*="tindakanpelayanan_id"]').each(function(){
           $(this).attr('checked',true);
        })
//        alert('Checked');
    } else {
       $('#tblBayarTind input[name*="tindakanpelayanan_id"]').each(function(){
           $(this).removeAttr('checked');
        })
    }
    hitungTotalSemuaTind();
}

function cekParentValue(value,obj){
    if ($(obj).is(":checked")){
        $('#tblBayarOA input[name*="obatalkespasien_id"][parent-value="'+value+'"]').each(function(){
           $(this).attr('checked',true);
        });
    }else{
        $('#tblBayarOA input[name*="obatalkespasien_id"][parent-value="'+value+'"]').each(function(){
           $(this).removeAttr('checked');
        });
    }
    hitungTotalSemuaOa();
}
</script>

<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogRincian',
    'options' => array(
        'title' => 'Rincian Tagihan Pasien',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 550,
        'resizable' => false,
    ),
));
?>
<iframe name='frameRincian' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>

<?php
//if($successSave){
//Yii::app()->clientScript->registerScript('tutupDialog',"
//window.parent.setTimeout(\"$('#dialogBayarKarcis').dialog('close')\",1500);
//window.parent.$.fn.yiiGridView.update('pencarianpasien-grid', {
//		data: $('#caripasien-form').serialize()
//});
//",  CClientScript::POS_READY);
//}
?>