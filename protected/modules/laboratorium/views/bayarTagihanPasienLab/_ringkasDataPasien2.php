<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
    <legend class="rim">Data Pasien</legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[tgl_pendaftaran]', $modPendaftaran->tgl_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td>
                <?php //echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?>
                 <label class="control-label no_rek">No Rekam Medik</label>
            </td>
            <td>
                <?php //echo CHtml::textField('BKPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true)); ?>
                <?php 
                    $this->widget('MyJuiAutoComplete', array(
                                    'name'=>'BKPasienM[no_rekam_medik]',
                                    'value'=>$modPasien->no_rekam_medik,
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienRetur').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                isiDataPasien(ui.item);
                                                isiTandaBuktiKeluar(ui.item);
//                                                $("#BKPendaftaranT_pendaftaran_id").val();
//                                                $("#BKPendaftaranT_pasienadmisi_id").val();
                                                //loadPembayaran(ui.item.pembayaranpelayanan_id);
                                                cekHakRetur();
                                                return false;
                                            }',
                                    ),
                                )); 
                ?>
            </td>
            <td rowspan="5">
                <?php 
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[no_pendaftaran]', $modPendaftaran->no_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label no_rek')); ?></td>
            <td><?php //echo CHtml::textField('BKPasienM[nama_pasien]', $modPasien->nama_pasien, array('readonly'=>true)); 
                    $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'BKPasienM[nama_pasien]',
                                           'value'=>$modPasien->nama_pasien,
                                           'source'=>'js: function(request, response) {
                                                          $.ajax({
                                                              url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienberdasarkanNama').'",
                                                              dataType: "json",
                                                              data: {
                                                                  retur:true,
                                                                  term: request.term,
                                                              },
                                                              success: function (data) {
                                                                      response(data);
                                                              }
                                                          })
                                                       }',
                                            'options'=>array(
                                                  'showAnim'=>'fold',
                                                  'minLength' => 2,
                                                  'focus'=> 'js:function( event, ui ) {
                                                       $(this).val(ui.item.value);
                                                       return false;
                                                   }',
                                                  'select'=>'js:function( event, ui ) {
                                                       isiDataPasien(ui.item);
                                                       loadPembayaran(ui.item.pendaftaran_id);
                                                       return false;
                                                   }',
                                           ),
                                       )); 
            ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[umur]', $modPendaftaran->umur, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPasienM[jeniskelamin]', $modPasien->jeniskelamin, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[jeniskasuspenyakit_nama]',  ((isset($modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama)) ? $modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama : null), array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPasienM[nama_bin]', $modPasien->nama_bin, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('BKPendaftaranT[instalasi_nama]',  ((isset($modPendaftaran->instalasi->instalasi_nama)) ? $modPendaftaran->instalasi->instalasi_nama : null), array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pendaftaran_id]',$modPendaftaran->pendaftaran_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pasien_id]',$modPendaftaran->pasien_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pasienadmisi_id]',$modPendaftaran->pasienadmisi_id, array('readonly'=>true)); ?>
            </td>
            
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'ruangan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[ruangan_nama]', ((isset($modPendaftaran->ruangan->ruangan_nama)) ? $modPendaftaran->ruangan->ruangan_nama : null), array('readonly'=>true)); ?></td>
        </tr>
    </table>
</fieldset> 

<script type="text/javascript">
function isiDataPasien(data)
{
    $('#BKPendaftaranT_tgl_pendaftaran').val(data.tglpendaftaran);
    $('#BKPendaftaranT_no_pendaftaran').val(data.nopendaftaran);
    $('#BKPendaftaranT_umur').val(data.umur);
    $('#BKPendaftaranT_jeniskasuspenyakit_nama').val(data.jeniskasuspenyakit);
    $('#BKPendaftaranT_instalasi_nama').val(data.namainstalasi);
    $('#BKPendaftaranT_ruangan_nama').val(data.namaruangan);
    $('#BKPendaftaranT_pendaftaran_id').val(data.pendaftaran_id);
    $('#BKPendaftaranT_pasien_id').val(data.pasien_id);
    $('#BKPendaftaranT_pasienadmisi_id').val(data.pasienadmisi_id);
    if (typeof data.norekammedik !=  'undefined'){
        $('#BKPasienM_no_rekam_medik').val(data.norekammedik);
    }
    $('#BKPasienM_jeniskelamin').val(data.jeniskelamin);
    $('#BKPasienM_nama_pasien').val(data.namapasien);
    $('#BKPasienM_nama_bin').val(data.namabin);
    
    $('#BKReturbayarpelayananT_tandabuktibayar_id').val(data.tandabuktibayar_id);
    $('#BKReturbayarpelayananT_totaloaretur').val(formatNumber(data.totalbiayaoa));
    $('#BKReturbayarpelayananT_totaltindakanretur').val(formatNumber(data.totalbiayatindakan));
    $('#BKReturbayarpelayananT_totalbiayaretur').val(formatNumber(data.totalbiayapelayanan));
    
    $('#BKReturbayarpelayananT_noreturbayar').focus();
}

function isiTandaBuktiKeluar(data)
{
    $('#BKTandabuktikeluarT_namapenerima').val(data.namapasien);
    $('#BKTandabuktikeluarT_alamatpenerima').val(data.alamatpasien);
}

function loadPembayaran(idPembayaran)
{
    $.post('<?php echo Yii::app()->createUrl('billingKasir/ActionAjax/loadPembayaranRetur');?>', {idPembayaran:idPembayaran}, function(data){
        $('#tblBayarTind tbody').html(data.formBayarTindakan);
        $('#tblBayarOA tbody').html(data.formBayarOa);
        $('#TandabuktibayarT_jmlpembayaran').val(formatNumber(data.jmlpembayaran));
        $('#totTagihan').val(formatNumber(data.tottagihan));
        
        $('#BKTandabuktibayarUangMukaT_jmlpembulatan').val(formatNumber(data.jmlpembulatan));
        $('#BKTandabuktibayarUangMukaT_uangditerima').val(formatNumber(data.uangditerima));
        $('#BKTandabuktibayarUangMukaT_uangkembalian').val(formatNumber(data.uangkembalian));
        $('#BKTandabuktibayarUangMukaT_biayamaterai').val(formatNumber(data.biayamaterai));
        $('#BKTandabuktibayarUangMukaT_biayaadministrasi').val(formatNumber(data.biayaadministrasi));
        $('#BKTandabuktibayarUangMukaT_darinama_bkm').val(data.namapasien);
        $('#BKTandabuktibayarUangMukaT_alamat_bkm').val(data.alamatpasien);
    }, 'json');
}
</script>
