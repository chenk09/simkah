<?php if($status == true){ 
    echo "<script>
                alert('Pasien Sudah Membayar Tindakan');
                window.top.location.href='".Yii::app()->createUrl('laboratorium/DaftarPasien/index')."';
            </script>";
    exit;
}
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0
    )
));
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'masukpenunjang-Laboratorium-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'unformatFormNumber();'),
        'focus'=>'#',
)); ?>
<legend class="rim2">Ubah Pemeriksaan</legend>
<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,'modPasienMasukPenunjang'=>$modPasienMasukPenunjang));?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<table class="table-condensed">
    <tr>
        <td style="background-color: #E5ECF9;">
            <div class="" id="formPeriksaLab">
                <?php foreach($modJenisPeriksaLab as $i=>$jenisPeriksa){ 
                ?>
                        <div class="boxtindakan">
                            <h6><?php echo $jenisPeriksa->jenispemeriksaanlab_nama; ?></h6>
                            <?php foreach ($modPeriksaLab as $j => $pemeriksaan) {
                                  if($jenisPeriksa->jenispemeriksaanlab_id == $pemeriksaan->jenispemeriksaanlab_id) {
                                      $cekAktif = PemeriksaanlabM::model()->findByPk($pemeriksaan->pemeriksaanlab_id);
                                      if($cekAktif->pemeriksaanlab_aktif){
										if($cekAktif->pemeriksaanlab_id == 352 && $pemeriksaan->isChecked == true) $cekVal=1;
										if($cekVal == 1 && ($cekAktif->pemeriksaanlab_id == 563 || $cekAktif->pemeriksaanlab_id == 564)){
											$pemeriksaan->isChecked=true;
										}
											echo '<label class="checkbox inline">'.CHtml::checkBox("pemeriksaanLab[]", $pemeriksaan->isChecked, array('value'=>$pemeriksaan->pemeriksaanlab_id, 'onclick' => "inputperiksa(this);",'readonly'=>$pemeriksaan->isChecked));
											echo "<span>".$pemeriksaan->pemeriksaanlab_nama."</span></label><br/>";
                                      }
                                     }
                                 } ?>
                        </div>
                <?php } ?>
            </div>
        </td>
    </tr>
</table>

<table id="tblFormPemeriksaanLab" class="table table-condensed">
    <thead>
        <tr>
            <th>Pemeriksaan</th>
            <th>Tarif</th>
            <th>Qty</th>
            <th>Satuan*</th>
            <th>Cyto</th>
            <th>Tarif Cyto</th>
            <th>Batal / Hapus </th>
        </tr>
    </thead>
    <?php
				$modHasilPeriksaDetails1 = DetailhasilpemeriksaanlabT::model()->findByAttributes(array('hasilpemeriksaanlab_id'=>$modHasilPeriksa->hasilpemeriksaanlab_id, 'pemeriksaanlab_id'=>'352'));
				$hdl=0;
				if(count($modHasilPeriksaDetails1)>0) $hdl=1;
        $urlLoadForm = Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanLabMasukBaru');
        foreach ($modPeriksaLab as  $itemPeriksa) {
            if($itemPeriksa->isChecked){
                $idPendaftaran = $modPendaftaran->pendaftaran_id;
                $idPemeriksaanLab=$itemPeriksa->pemeriksaanlab_id;
//                $idKelaspelayanan=$modPendaftaran->kelaspelayanan_id;
                $idKelaspelayanan = Params::kelasPelayanan('tanapa_kelas');//Semua tindakan lab & rad itu tanpa kelas
				if($hdl==1 && $idPemeriksaanLab=='563'){
				} else if($hdl==1 && $idPemeriksaanLab == '564'){
				} else {
?>
						<script>
						var idPemeriksaanlab = <?php echo $idPemeriksaanLab ?>;
						var kelasPelayan_id = <?php echo $idKelaspelayanan ?>;
						var idPendaftaran = <?php echo $idPendaftaran ?>;
						$.post("<?php echo $urlLoadForm; ?> ", { idPemeriksaanlab: idPemeriksaanlab,kelasPelayan_id:kelasPelayan_id,idPendaftaran:idPendaftaran},
						function(data){
							$('#tblFormPemeriksaanLab').append(data.form);
							renameInput('permintaanPenunjang','inputpemeriksaanlab');
							renameInput('permintaanPenunjang','inputtarifpemeriksaanlab');
							renameInput('permintaanPenunjang','inputqty');
							renameInput('permintaanPenunjang','satuan');
							renameInput('permintaanPenunjang','cyto');
							renameInput('permintaanPenunjang','persencyto');
							renameInput('permintaanPenunjang','tarifcyto');
							renameInput('permintaanPenunjang','jenisPemeriksaanNama');
							renameInput('permintaanPenunjang','pemeriksaanNama');
							renameInput('permintaanPenunjang','idDaftarTindakan');
							hitungTotal();
						}, "json");
						</script>
<?php
				}
            }
        } ?>
</table>
<table class="table table-bordered table-condensed">
	<tr><td width="70%" style="text-align: right;">Biaya Administrasi</td><td><?php echo CHtml::textField('biayaAdministrasi', $modTindakanPelayan->tarif_satuan,array('class'=>'span2 currency', 'style'=>'text-align:right;', 'onkeyup'=>'hitungTotal();'));?></td></tr>
    <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan</td><td><?php echo CHtml::textField('periksaTotal', '',array('class'=>'span2', 'style'=>'text-align:right;', 'disabled'=>'disabled'));?></td></tr>
    <?php if(count($modKirimKeUnitLain) > 0) {?> 
    <tr>
        <td colspan="2">
            <div class="control-group ">
                <label class="control-label required" for="PasienkirimkeunitlainT_pegawai_id">
                Perujuk :
                </label>
                <div class="controls"></div>
            </div> 
        <?php
            echo $form->dropDownListRow(
                $modKirimKeUnitLain,
                'pegawai_id',
                CHtml::listData($modKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                array(
                    'empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);"
                )
            );
        ?>
        </td>
    </tr>
    <?php }else{ ?>
    <tr>
        <td colspan="2">
            <div class="control-group ">
                <label class="control-label required" for="PasienkirimkeunitlainT_pegawai_id">
                Perujuk
                </label>
                <div class="controls">
                <?php
                    echo $form->dropDownList(
                        $rujukanM,
                        'nama_perujuk',
                        CHtml::listData($rujukanM->getDokterLuarItems(), 'namaperujuk', 'namaperujuk'),
                        array(
                            'empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    );
                ?>
                </div>
            </div>
        </td>
    </tr>    
    <?php }?>
    
</table>
<table>
    <tr>
        <td>
            <br>
                <?php
                //FORM REKENING
                    $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                        array(
                            'form'=>$form,
                            'modRekenings'=>$modRekenings,
                        )
                    );
                ?>
        </td>
    </tr>
</table>
<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php 
            $id_pendaftaran = (isset($_GET['idPendaftaran']) ? $_GET['idPendaftaran'] :''); 
            $id_pasienpenunjang = (isset($_GET['idPasienMasukPenunjang']) ? $_GET['idPasienMasukPenunjang'] :''); 
//            if(!empty($id_pendaftaran) && !$model->isNewRecord){ 
            echo CHtml::link(Yii::t('mds', '{icon} Print Label', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('id'=>'btnPrint','class'=>'btn btn-info', 'onclick'=>'print(\'PRINT\')')); 
            echo CHtml::link(Yii::t('mds', '{icon} Print Daftar Tindakan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('id'=>'btnPrint','class'=>'btn btn-info', 'onclick'=>'printTindakan(\'PRINT\')')); 
            // AUTOPRINT >>> echo "<script>setTimeout(function(){print(\"PRINT\");},500);</script>";
//            }
        ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('daftarPasien/index',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
</div>
        
<?php $this->endWidget(); ?>
<?php 
$caraPrint = 'PRINT';
$labelOnly = 1;
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/pendaftaranPasienLuar/print', array('id_pendaftaran'=>$id_pendaftaran,'id_pasienpenunjang'=>$id_pasienpenunjang, 'labelOnly'=>$labelOnly)); 
$urlPrintTindakan=  Yii::app()->createAbsoluteUrl($this->module->id.'/pendaftaranPasienLuar/printTindakan', array('id_pendaftaran'=>$id_pendaftaran,'id_pasienpenunjang'=>$id_pasienpenunjang, 'labelOnly'=>$labelOnly)); 
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}
function printTindakan(caraPrint)
{
    window.open("${urlPrintTindakan}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}
JSCRIPT;
    
Yii::app()->clientScript->registerScript('printLabelTindakan',$js,  CClientScript::POS_HEAD);
?>

<script type="text/javascript">
    
$('#formPeriksaLab').tile({widths : [ 190 ]});
   
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanLab = obj.value;
        var idKelasPelayanan = <?php echo $modPendaftaran->kelaspelayanan_id; ?>;
        /* if(idPemeriksaanLab == 352){
            $('input[value="563"]').attr("checked",true);
            $('input[value="564"]').attr("checked",true); 
            inputperiksaPaket(563,idKelasPelayanan);
            inputperiksaPaket(564,idKelasPelayanan);
            inputperiksaPaket(353,idKelasPelayanan);
        }
        if(idPemeriksaanLab == 369){ 
            inputperiksaPaket(370,idKelasPelayanan);
            inputperiksaPaket(371,idKelasPelayanan);
        }
        if(idPemeriksaanLab == 373){
            inputperiksaPaket(374,idKelasPelayanan);
            inputperiksaPaket(375,idKelasPelayanan);
        } */
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/LoadFormPemeriksaanLabMasuk')?>',
                 'data':{idPemeriksaanlab:idPemeriksaanLab, kelasPelayan_id:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         if($.trim(data.form)=='')
                         {
                            $(obj).removeAttr('checked');
                            alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                         } else if($.trim(data.form)=='tarif kosong') {
                            $(obj).removeAttr('checked');
                            data.form = '';
                            alert ('Pemeriksaan belum memiliki tarif');
                         }
                         $('#tblFormPemeriksaanLab').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanlab');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanlab');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                         
                         var daftartindakanId = $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanLab).find('input[name$="[daftartindakan_id]"]').val();
                         getDataRekening(daftartindakanId,idKelasPelayanan,1,'tm');
                         hitungTotal();
						 
                        if(obj.value == '352')
                        {
                            batalPeriksa('563');
                            $('#formPeriksaLab').find('input[value="563"]').attr('checked', 'checked');
                            $('#formPeriksaLab').find('input[value="563"]').attr('disabled', 'true');

                            batalPeriksa('564');
                            $('#formPeriksaLab').find('input[value="564"]').attr('checked', 'checked');
                            $('#formPeriksaLab').find('input[value="564"]').attr('disabled', 'true');
                            hitungTotal();

                        }
                 } ,
                 'cache':false});
    } else {
        if($(obj).is(':checkbox[readonly=readonly]')){
             $(obj).attr('checked', 'checked');
        }else{
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
            var idPemeriksaanLab = obj.value;
			if(obj.value == '352')
            {
                $('#formPeriksaLab').find('input[value="563"]').removeAttr('checked');
                $('#formPeriksaLab').find('input[value="563"]').removeAttr('disabled');

                $('#formPeriksaLab').find('input[value="564"]').removeAttr('checked');
                $('#formPeriksaLab').find('input[value="564"]').removeAttr('disabled');
            }
            /* if(idPemeriksaanLab == 352){
                $('input[value="563"]').attr("checked",false);
                $('input[value="564"]').attr("checked",false); 
                batalPeriksa(563);
                batalPeriksa(564);
                batalPeriksa(353);
            }
            if(idPemeriksaanLab == 369){ 
                batalPeriksa(370);
                batalPeriksa(371);
            }
            if(idPemeriksaanLab == 373){
                batalPeriksa(374);
                batalPeriksa(375);
            } */
            hitungTotal();
        }else
            $(obj).attr('checked', 'checked');
        }
    }
}
function inputperiksaPaket(idLab,idKelas)
{
    var idPemeriksaanLab = idLab;
    var idKelasPelayanan = idKelas;
    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/LoadFormPemeriksaanLabMasuk')?>',
             'data':{idPemeriksaanlab:idPemeriksaanLab, kelasPelayan_id:idKelasPelayanan},
             'type':'post',
             'dataType':'json',
             'success':function(data) {
                     $('#tblFormPemeriksaanLab').append(data.form);
                     renameInput('permintaanPenunjang','inputpemeriksaanlab');
                     renameInput('permintaanPenunjang','inputtarifpemeriksaanlab');
                     renameInput('permintaanPenunjang','inputqty');
                     renameInput('permintaanPenunjang','satuan');
                     renameInput('permintaanPenunjang','cyto');
                     renameInput('permintaanPenunjang','persencyto');
                     renameInput('permintaanPenunjang','tarifcyto');
                     renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                     renameInput('permintaanPenunjang','pemeriksaanNama');
                     renameInput('permintaanPenunjang','idDaftarTindakan');
                     hitungTotal();
             } ,
             'cache':false});
}
function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanLab tr').length;
    var i = -1;
    $('#tblFormPemeriksaanLab tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function batalPeriksa(idPemeriksaanlab)
{
    var daftartindakan_id = $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).find('input[name$="[daftartindakan_id]"]').val();
    removeRekeningTindakan(daftartindakan_id);
    $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).detach();
}

function cyto_tarif(test, x){
    if(test.value == 1){
        $('.cyto_tarif_'+ x).show();
    }else{
        $('.cyto_tarif_'+ x).hide();
    }
}
function hitungCyto(obj){
    persen = unformatNumber($(obj).parent().parent().find('.persenCyto').val());
    tarif = unformatNumber($(obj).parent().parent().find('.tarif').val());
    qty = unformatNumber($(obj).parent().parent().find('.qty').val());
    isCyto = unformatNumber($(obj).parent().parent().find('.isCyto').val());
    cyto = (tarif * qty)*persen/100;
//        alert(cyto);
    $(obj).parent().parent().find('.cyto').val(cyto);
    if(isCyto == 0)
        $(obj).parent().parent().find('.cyto').val(0);
}

function hitungTotal(){
    var total = 0;
    $('.tarif').each(
        function(){
            qty = $(this).parent().parent().find('.qty').val();
            isCyto = $(this).parent().parent().find('.isCyto').val();
            if(isCyto == 1){
                cyto = $(this).parent().parent().find('.cyto').val();
            }else{
                cyto = 0;
            }
            
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );
	total += unformatNumber($('#biayaAdministrasi').val());
    $('#periksaTotal').val(formatNumber(total));    
}

function remove(obj,idTindakan,idPemeriksaanLab){
    if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
                batalPeriksa(idPemeriksaanLab);
                $('#formPeriksaLab').find('input[value='+idPemeriksaanLab+']').removeAttr('checked');
                var daftartindakan_id = idTindakan;
                removeRekeningTindakan(daftartindakan_id);
            hitungTotal();
    }else{
        $(obj).attr('checked', 'checked');
    }
}

function deleteTindakan(obj,idTindakan,idPemeriksaanLab){
    var idTindakan = idTindakan;
    var idPemeriksaanLab = idPemeriksaanLab;
    var idPendaftaran = $('#LKPendaftaranT_pendaftaran_id').val();
    var idPenunjang = $('#LKPasienMasukPenunjangT_pasienmasukpenunjang_id').val();
//    alert(idTindakan+'-'+idPendaftaran+'-'+idPenunjang);
    if(confirm('Apakah anda yakin akan menghapus tindakan dari database?')){
        $.post('<?php echo $this->createUrl('ajaxDeleteTindakan') ?>', {idTindakan: idTindakan, idPendaftaran:idPendaftaran, idPenunjang:idPenunjang, idPemeriksaanLab:idPemeriksaanLab}, function(data){
            if(data.success)
            {
                $(obj).parent().parent().detach();
                alert('Data berhasil dihapus !!');
                hitungTotal();
                window.location.reload(false); 
            } else if(data.status != '' && data.success != true){
                alert(data.status);
            }else{
                alert('Data Gagal dihapus');
            }
        }, 'json');
    }
}
function unformatFormNumber(){
    $('#masukpenunjang-Laboratorium-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
}
function formatFormNumber(){
    $('#masukpenunjang-Laboratorium-form').find('.currency').each(function(){
        $(this).val(formatNumber($(this).val()));
    });
}
setTimeout(function(){formatFormNumber()},1000);
</script>


<?php
function cekPilihan($pemeriksaanlab_id,$arrInputPemeriksaan)
{
    $cek = false;
    foreach ($arrInputPemeriksaan as $i => $items) {
        foreach($items as $j=>$item){
            if($pemeriksaanlab_id == $item->pemeriksaanlab_id) $cek = true;
        }
    }
    return $cek;
}
?>
