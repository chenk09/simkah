<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'masukpenunjang-Laboratorium-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<h3>Pilih Pemeriksaan</h3>
<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<table class="table-condensed">
    <tr>
        <td style="background-color: #E5ECF9;">
            <div class="" id="formPeriksaLab">
                <?php foreach($modJenisPeriksaLab as $i=>$jenisPeriksa){ 
                ?>
                        <div class="boxtindakan">
                            <h6><?php echo $jenisPeriksa->jenispemeriksaanlab_nama; ?></h6>
                            <?php foreach ($modPeriksaLab as $j => $pemeriksaan) {
                                  $ceklist = false;
                                  if($jenisPeriksa->jenispemeriksaanlab_id == $pemeriksaan->jenispemeriksaanlab_id) {
                                         echo '<label class="checkbox inline">'.CHtml::checkBox("pemeriksaanLab[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanlab_id,
                                                                                                  'onclick' => "inputperiksa(this);"));
                                         echo "<span>".$pemeriksaan->pemeriksaanlab_nama."</span></label><br/>";
                                     }
                                 } ?>
                        </div>
                <?php } ?>
            </div>
        </td>
    </tr>
</table>
<table id="tblFormPemeriksaanLab" class="table table-condensed">
    <thead>
        <tr>
            <th>Pemeriksaan</th>
            <th>Tarif</th>
            <th>Qty</th>
            <th>Satuan*</th>
            <th>Cyto</th>
            <th>Tarif Cyto</th>
        </tr>
    </thead>
</table>
<table class="table table-bordered table-condensed">
    <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan</td><td><?php echo CHtml::textField('periksaTotal', '',array('class'=>'span2', 'style'=>'text-align:right;', 'disabled'=>'disabled'));?></td></tr>
</table>
<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
</div>
        
<?php $this->endWidget(); ?>

<script type="text/javascript">
    
$('#formPeriksaLab').tile({widths : [ 190 ]});
    
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanLab = obj.value;
        var idKelasPelayanan = <?php echo $modPendaftaran->kelaspelayanan_id; ?>;
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/LoadFormPemeriksaanLabMasuk')?>',
                 'data':{idPemeriksaanlab:idPemeriksaanLab, kelasPelayan_id:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         if($.trim(data.form)=='')
                         {
                            $(obj).removeAttr('checked');
                            alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                         } else if($.trim(data.form)=='tarif kosong') {
                            $(obj).removeAttr('checked');
                            data.form = '';
                            alert ('Pemeriksaan belum memiliki tarif');
                         }
                         $('#tblFormPemeriksaanLab').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanlab');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanlab');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                 } ,
                 'cache':false});
      setTimeout(function(){hitungTotal()},500);
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
            hitungTotal();
        }else
            $(obj).attr('checked', 'checked');
    }
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanLab tr').length;
    var i = -1;
    $('#tblFormPemeriksaanLab tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function batalPeriksa(idPemeriksaanlab)
{
    $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).detach();
}

function cyto_tarif(test, x){
    if(test.value == 1){
        $('.cyto_tarif_'+ x).show();
    }else{
        $('.cyto_tarif_'+ x).hide();
    }
}
function hitungCyto(obj){
    persen = unformatNumber($(obj).parent().parent().find('.persenCyto').val());
    tarif = unformatNumber($(obj).parent().parent().find('.tarif').val());
    qty = unformatNumber($(obj).parent().parent().find('.qty').val());
    isCyto = unformatNumber($(obj).parent().parent().find('.isCyto').val());
    cyto = (tarif * qty)*persen/100;
//        alert(cyto);
    $(obj).parent().parent().find('.cyto').val(cyto);
    if(isCyto == 0)
        $(obj).parent().parent().find('.cyto').val(0);
}

function hitungTotal(){
    var total = 0;
    $('.tarif').each(
        function(){
            qty = $(this).parent().parent().find('.qty').val();
            isCyto = $(this).parent().parent().find('.isCyto').val();
            if(isCyto == 1){
                cyto = $(this).parent().parent().find('.cyto').val();
            }else{
                cyto = 0;
            }
            
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );
 
    $('#periksaTotal').val(formatNumber(total));    
}
</script>


<?php
function cekPilihan($pemeriksaanlab_id,$arrInputPemeriksaan)
{
    $cek = false;
    foreach ($arrInputPemeriksaan as $i => $items) {
        foreach($items as $j=>$item){
            if($pemeriksaanlab_id == $item->pemeriksaanlab_id) $cek = true;
        }
    }
    return $cek;
}
?>
