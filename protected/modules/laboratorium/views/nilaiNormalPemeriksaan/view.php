<?php
$this->breadcrumbs=array(
	'Sanilai rujukan Ms'=>array('index'),
	$model->nilairujukan_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Referensi Hasil Lab ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Jenis Pemeriksaan Lab', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Jenis Pemeriksaan Lab', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Jenis Pemeriksaan Lab', 'icon'=>'pencil','url'=>array('update','id'=>$model->jenispemeriksaanlab_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Jenis Pemeriksaan Lab','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->jenispemeriksaanlab_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Referensi Hasil Lab ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nilairujukan_id',
		'kelompokdet',
		'nilairujukan_jeniskelamin',
		'kelompokumur',
		'nilairujukan_nama',
		'nilairujukan_min',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>

