<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'nilairujukan-m-search',
        'type'=>'horizontal',
)); ?>

    <?php //echo $form->textFieldRow($model,'nilairujukan_id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'kelompokumur',array('class'=>'span3','maxlength'=>50)); ?>
    <?php echo $form->DropDownListRow($model, 'kelompokumur', CHtml::listData(KelompokumurM::model()->findAllByAttributes(array('kelompokumur_aktif'=>true)),'kelompokumur_nama','kelompokumur_nama'),array('empty'=>'-- Pilih --')); ?>

    <?php echo $form->textFieldRow($model,'kelompokdet',array('class'=>'span3','maxlength'=>50)); ?>

    <?php echo $form->textFieldRow($model,'namapemeriksaandet',array('class'=>'span3','maxlength'=>200)); ?>

    <?php //echo $form->textFieldRow($model,'nilairujukan_jeniskelamin',array('class'=>'span3','maxlength'=>50)); ?>
    <?php echo $form->DropDownListRow($model, 'nilairujukan_jeniskelamin', JenisKelamin::items(), array('empty'=>'-- Pilih --')); ?>

    <?php echo $form->textFieldRow($model,'nilairujukan_nama',array('class'=>'span3','maxlength'=>100)); ?>

    <?php echo $form->textFieldRow($model,'nilairujukan_min',array('class'=>'span3')); ?>

    <?php echo $form->textFieldRow($model,'nilairujukan_max',array('class'=>'span3')); ?>

    <?php echo $form->textFieldRow($model,'nilairujukan_satuan',array('class'=>'span3','maxlength'=>50)); ?>

    <?php echo $form->textFieldRow($model,'nilairujukan_metode',array('class'=>'span3','maxlength'=>30)); ?>

    <?php echo $form->textAreaRow($model,'nilairujukan_keterangan',array('rows'=>3, 'cols'=>50, 'class'=>'span3')); ?>

    <?php echo $form->checkBoxRow($model,'nilairujukan_aktif', array('checked'=>'checked')); ?>

    <div class="form-actions">
                        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
    </div>

<?php $this->endWidget(); ?>