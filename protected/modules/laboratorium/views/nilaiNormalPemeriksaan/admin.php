<?php
$this->breadcrumbs=array(
    'Nilairujukan Ms'=>array('index'),
    'Manage',
);

$arrMenu = array();
(Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Referensi Hasil Lab ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//array_push($arrMenu,array('label'=>Yii::t('mds','List').' NilairujukanM', 'icon'=>'list', 'url'=>array('index'))) ;
(Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Referensi Hasil Lab ', 'icon'=>'file', 'url'=>array('create','modulId'=>Yii::app()->session['modulId']))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    $('#LKNilaiRujukanM_kelompokumur').focus();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('nilairujukan-m-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('laboratorium.views.nilaiNormalPemeriksaan._search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->
     
<?php $this->widget('ext.bootstrap.widgets.BootGroupGridView',array(
    'id'=>'nilairujukan-m-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    //'extraRowColumns' => array('kelompokdet','kelompokumur'),
    'mergeColumns' => array('kelompokdet','namapemeriksaandet','kelompokumur','nilairujukan_jeniskelamin'),
    'columns'=>array(
        ////'nilairujukan_id',
        array(
            'name'=>'nilairujukan_id',
            'value'=>'$data->nilairujukan_id',
            'filter'=>false,
        ),
//        'kelompokumur',
        array(
            'name'=>'kelompokumur',
            'filter'=> CHtml::listData(KelompokumurM::model()->findAllByAttributes(array('kelompokumur_aktif'=>true)), 'kelompokumur_nama', 'kelompokumur_nama'),
        ),
        'kelompokdet',
        'namapemeriksaandet',
        //'nilairujukan_jeniskelamin',
        array(
            'name'=>'nilairujukan_jeniskelamin',
            'filter'=> JenisKelamin::items(),
        ),
        'nilairujukan_nama',
        'nilairujukan_min',
        'nilairujukan_max',
        //'nilairujukan_satuan',
        array(
            'name'=>'nilairujukan_satuan',
            //'filter'=>SatuanHasilLab::items(),
        ),
        'nilairujukan_metode',
        'nilairujukan_keterangan',
        /*
        'nilairujukan_aktif',
        */
        array(
            'header'=>'<center>Status</center>',
            'value'=>'($data->nilairujukan_aktif == 1 ) ? "Aktif" : "Tidak Aktif"',
            'htmlOptions'=>array('style'=>'text-align:center;'),
        ),
//        array(
//                'header'=>'Aktif',
//                'class'=>'CCheckBoxColumn',     
//                'selectableRows'=>0,
//                'id'=>'rows',
//                'checked'=>'$data->nilairujukan_aktif',
//        ),
        array(
            'header'=>Yii::t('zii','View'),
            'class'=>'bootstrap.widgets.BootButtonColumn',
            'template'=>'{view}',
        ),
        array(
            'header'=>Yii::t('zii','Update'),
            'class'=>'bootstrap.widgets.BootButtonColumn',
            'template'=>'{update}',
            'buttons'=>array(
                'update' => array (
                              'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
                            ),
            ),
        ),
        array(
            'header'=>'Hapus',
            'type'=>'raw',
            'value'=>'($data->nilairujukan_aktif)?CHtml::link("<i class=\'icon-remove\'></i> ","javascript:removeTemporary($data->nilairujukan_id)",array("id"=>"$data->nilairujukan_id","rel"=>"tooltip","title"=>"Menonaktifkan"))." ".CHtml::link("<i class=\'icon-trash\'></i> ", "javascript:deleteRecord($data->nilairujukan_id)",array("id"=>"$data->nilairujukan_id","rel"=>"tooltip","title"=>"Hapus")):CHtml::link("<i class=\'icon-trash\'></i> ", "javascript:deleteRecord($data->nilairujukan_id)",array("id"=>"$data->nilairujukan_id","rel"=>"tooltip","title"=>"Hapus"));',
            'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<div class="form-actions">
<?php 
 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
        $content = $this->renderPartial('laboratorium.views.tips.master',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        $url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#nilairujukan-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?> 
</div>
<script type="text/javascript">
    function removeTemporary(id){
        var url = '<?php echo $url."/removeTemporary"; ?>';
        var answer = confirm('Yakin akan menonaktifkan data ini untuk sementara?');
            if (answer){
                 $.post(url, {id: id},
                     function(data){
                        if(data.status == 'proses_form'){
                                $.fn.yiiGridView.update('nilairujukan-m-grid');
                            }else{
                                alert('Data Gagal di Nonaktifkan')
                            }
                },"json");
           }
    }
    
    function deleteRecord(id){
        var id = id;
        var url = '<?php echo $url."/delete"; ?>';
        var answer = confirm('Yakin Akan Menghapus Data ini ?');
            if (answer){
                 $.post(url, {id: id},
                     function(data){
                        if(data.status == 'proses_form'){
                                $.fn.yiiGridView.update('nilairujukan-m-grid');
                            }else{
                                alert('Data Gagal di Hapus')
                            }
                },"json");
           }
    }
</script>