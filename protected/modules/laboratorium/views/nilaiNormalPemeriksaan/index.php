<?php
$this->breadcrumbs=array(
	'Nilai Normal Pemeriksaan',
);

$arrMenu = array();
array_push($arrMenu,array('label'=>Yii::t('mds','List').' Nilai Normal Pemeriksaan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//(Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Nilai Normal Pemeriksaan', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
(Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Nilai Normal Pemeriksaan', 'icon'=>'folder-open', 'url'=>array('admin','modulId'=>Yii::app()->session['modulId']))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'laboratorium.views.nilaiNormalPemeriksaan._view',// ini yang benernya yang mana
//        'itemView'=>'_view',
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'list'));?>
