<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'lknilai-rujukan-m-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#pemeriksaanlab_nama',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($model); ?>
    <div class="control-group ">
        <label class="control-label" for="bidang">Pemeriksaan Laboratorium</label>
        <div class="controls">
        <?php echo $form->hiddenField($modPemLab,'pemeriksaanlab_id'); ?>

            <?php 
            
           $this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => '.',
        'thousands' => '',
        'precision' =>0,
        'allowDecimal'=>true,
    )

));
                    $this->widget('MyJuiAutoComplete', array(

                                    'name'=>'pemeriksaanlab_nama',
                                    'value'=>$modPemLab->pemeriksaanlab_nama,
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/searchPeriksaLabIsNotNull').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.pemeriksaanlab_nama);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) { 
                                                $("#'.CHtml::activeId($modPemLab, 'pemeriksaanlab_id').'").val(ui.item.pemeriksaanlab_id);
                                                $(this).val(ui.item.pemeriksaanlab_nama);
                                                return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array(
                                            'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            'readonly'=>false,
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogPemeriksaanLab'),
                                )); 
                 ?>
        </div>
    </div>
    
            <?php echo $form->textFieldRow($model,'kelompokdet',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'namapemeriksaandet',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>200)); ?>
            <?php //echo $form->textFieldRow($model,'nilairujukan_jeniskelamin',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php //echo $form->textFieldRow($model,'kelompokumur',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php //echo $form->textFieldRow($model,'nilairujukan_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php //echo $form->textFieldRow($model,'nilairujukan_min',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'nilairujukan_max',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'nilairujukan_satuan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php //echo $form->textFieldRow($model,'nilairujukan_metode',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
            <?php //echo $form->textAreaRow($model,'nilairujukan_keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span5', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->checkBoxRow($model,'nilairujukan_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    
    <table>
        <tr>
            <?php
            $colspan = 0;
            $jenisKelamin=JenisKelamin::items();
            $kelompokUmur=KelompokUmur::items();
            foreach($jenisKelamin as $col=>$kelamin){
            $colspan++;
                echo '<td>';
                echo '<table style="margin:0;">';
                echo '<tr><td>';
                echo '<label class="checkbox inline">';
                echo CHtml::checkBox('nilaiJeniskelamin['.($colspan-1).']',true,array('value'=>$kelamin, 'onkeypress'=>"return $(this).focusNextInputField(event);"));
                echo $kelamin;
                echo '</label>';
                echo '</td></tr>';
                echo '<tr><td>';
                
                echo '<table class="table table-condensed table-bordered" style="margin:0;">';
                echo '<thead><tr><th>Kelompok Umur</th><th>Min</th><th>Max</th><th>Nilai Normal</th></tr></thead>';
                foreach($kelompokUmur as $i=>$kelUmur){
                        echo '<tr><td>'.$kelUmur.CHtml::hiddenField("nilaiNormal[$kelamin][$kelUmur][nilaiKelUmur]", $kelUmur, array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>true)).'</td>';
                        echo '<td>'.CHtml::textField("nilaiNormal[$kelamin][$kelUmur][nilaiMin]", '', array('class'=>'inputFormTabel lebar2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);",)).'</td>';
                        echo '<td>'.CHtml::textField("nilaiNormal[$kelamin][$kelUmur][nilaiMax]", '', array('class'=>'inputFormTabel lebar2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);",)).'</td>';
                        echo '<td>'.CHtml::textField("nilaiNormal[$kelamin][$kelUmur][nilaiNama]", '-', array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);",)).'</td>';
                        echo '</tr>';
                }
                echo '</table>';
                
                echo '</td></tr>';
                echo '</table>';
                echo '</td>';
            }        
            ?>
        </tr>
        <tr>
            <td colspan="<?php echo $colspan; ?>">
                <table class="table table-bordered">
                    <thead><tr><th>Satuan</th><th>Metode</th><th>Keterangan</th></tr></thead>
                    <tr>
                        <td>
                              <?php echo $form->dropDownList($model,'nilairujukan_satuan',SatuanHasilLab::items(),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'empty'=>'--'.$model->getAttributeLabel('nilairujukan_satuan').'--')); ?>
                        </td>
                        <td>
                              <?php echo $form->textField($model,'nilairujukan_metode',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30,'placeholder'=>$model->getAttributeLabel('nilairujukan_metode'))); ?>
                        </td>
                        <td>
                              <?php echo $form->textArea($model,'nilairujukan_keterangan',array('class'=>'span6', 'onkeypress'=>"return $(this).focusNextInputField(event);",'placeholder'=>$model->getAttributeLabel('nilairujukan_keterangan'))); ?>
                        </td>
                    </tr>
                </table> 
            </td>
        </tr>
    </table>

<div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl('/'.laboratorium.'/'.NilaiNormalPemeriksaan.'/'.admin), 
                                    array('class'=>'btn btn-danger',
                                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('laboratorium.views.tips.tipsaddedit3a',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
</div>

<?php $this->endWidget(); ?>

<?php
//========= Dialog buat cari Pemeriksaan Laboratorium =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPemeriksaanLab',
    'options'=>array(
        'title'=>'Pemeriksaan Laboratorium',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>750,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modTindakanLab = new PemeriksaanlabtindV('search');
$modTindakanLab->unsetAttributes();
if(isset($_GET['PemeriksaanlabtindV']))
    $modTindakanLab->attributes = $_GET['PemeriksaanlabtindV'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'sainstalasi-m-grid',
	'dataProvider'=>$modTindakanLab->searchPeriksaLabIsNotNull(),
	'filter'=>$modTindakanLab,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            'jenispemeriksaanlab_nama',
            'pemeriksaanlab_nama',
            'daftartindakan_nama',
            'kategoritindakan_nama',
            'kelompoktindakan_nama',
            //'daftartindakan_kode',
            'harga_tariftindakan',            
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",
                            "#",
                            array(
                                "class"=>"btn-small", 
                                "id" => "selectTindakan",
                                "onClick" => "
                                $(\"#'.CHtml::activeId($modPemLab, 'pemeriksaanlab_id').'\").val(\'$data->pemeriksaanlab_id\');
                                $(\"#pemeriksaanlab_nama\").val(\'$data->pemeriksaanlab_nama\');
                                $(\'#dialogPemeriksaanLab\').dialog(\'close\');return false;"))'
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget();


?>
