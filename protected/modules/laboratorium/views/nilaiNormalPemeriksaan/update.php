
<?php
$this->breadcrumbs=array(
    'Lknilai Rujukan Ms'=>array('index'),
    $model->nilairujukan_id=>array('view','id'=>$model->nilairujukan_id),
    'Update',
);

$arrMenu = array();
array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Referensi Hasil Lab '/*.$model->nilairujukan_id*/, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//array_push($arrMenu,array('label'=>Yii::t('mds','List').' LKNilaiRujukanM', 'icon'=>'list', 'url'=>array('index'))) ;
//(Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' LKNilaiRujukanM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//array_push($arrMenu,array('label'=>Yii::t('mds','View').' LKNilaiRujukanM', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->nilairujukan_id))) ;
(Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Referensi Hasil Lab ', 'icon'=>'folder-open', 'url'=>array('admin','modulId'=>Yii::app()->session['modulId']))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('laboratorium.views.nilaiNormalPemeriksaan._formUpdate',array('model'=>$model)); ?>
