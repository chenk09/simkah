<div class="view">
    
    <b><?php echo CHtml::encode($model->getAttributeLabel('nilairujukan_id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($model->nilairujukan_id),array('view','id'=>$model->nilairujukan_id)); ?>
    <br />

    <b><?php echo CHtml::encode($model->getAttributeLabel('kelompokdet')); ?>:</b>
    <?php echo CHtml::encode($model->kelompokdet); ?>
    <br />


    <b><?php echo CHtml::encode($model->getAttributeLabel('nilairujukan_jeniskelamin')); ?>:</b>
    <?php echo CHtml::encode($model->nilairujukan_jeniskelamin); ?>
    <br />

    <b><?php echo CHtml::encode($model->getAttributeLabel('kelompokumur')); ?>:</b>
    <?php echo CHtml::encode($model->kelompokumur); ?>
    <br />

    <b><?php echo CHtml::encode($model->getAttributeLabel('nilairujukan_nama')); ?>:</b>
    <?php echo CHtml::encode($model->nilairujukan_nama); ?>
    <br />

    <b><?php echo CHtml::encode($model->getAttributeLabel('nilairujukan_min')); ?>:</b>
    <?php echo CHtml::encode($model->nilairujukan_min); ?>
    <br />

    
</div>