<?php
    $url = Yii::app()->createUrl('laboratorium/laporan/frameGrafikPemeriksaanRujukan&id=1');
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('#searchLaporan').submit(function(){
        $('#Grafik').attr('src','').css('height','0px');
        $.fn.yiiGridView.update('tableRujukanLuar', {
                data: $(this).serialize()
        });
        $.fn.yiiGridView.update('tableRujukanRS', {
                data: $(this).serialize()
        });
        return false;
    });
    ");
?>

<legend class="rim2">Laporan Pemeriksaan Rujukan</legend>
<div class="search-form">
        <?php $this->renderPartial('laboratorium.views.laporan.pemeriksaanRujukan/_searchPemeriksaanRujukan',
                array('model'=>$model,'modelRS'=>$modelRS)); ?>
</div>
<div class="tab">
    <?php
        $this->widget('bootstrap.widgets.BootMenu',array(
            'type'=>'tabs',
            'stacked'=>false,
            'htmlOptions'=>array('id'=>'filter_tab'),
            'items'=>array(
                array('label'=>'Rujukan dari Luar','url'=>'javascript:tab(0);','active'=>true),
                array('label'=>'Rujukan dari RS','url'=>'javascript:tab(1);', 'itemOptions'=>array("index"=>1)),
            ),
        ))
    ?>
    
    <div>
         <?php $this->renderPartial('laboratorium.views.laporan.pemeriksaanRujukan/_tablePemeriksaanRujukan', array('model'=>$model,'modelRS'=>$modelRS)); ?>
    </div>            
</div>
<br/>
<!-- <fieldset> 
    <?php $this->renderPartial('laboratorium.views.laporan._tab'); ?>
   
    <iframe src="" id="Grafik" width="100%" height='0' onload="javascript:resizeIframe(this);">
    </iframe>       
</fieldset> -->
<?php   
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanPemeriksaanRujukan');
    $this->renderPartial('laboratorium.views.laporan._footerNonGrafik', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>
<?php
$js= <<< JS
    $(document).ready(function() {
        $("#filter_tab").children("li").children("a").click(function() {
            $("#filter_tab").children("li").attr('class','');
            $(this).parents("li").attr('class','active');
            $(".icon-pencil").remove();
            $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
        });
        
        $("#div_rujukanLuar").show();
        $("#div_rujukanRS").hide();
    });

    function tab(index){
        $(this).hide();
        if (index==0){
            $("#filter_tab").val('luar');
            $("#filter_nama").val('luar');
            $("#div_rujukanLuar").show();
            $("#div_rujukanRS").hide();  
        } else if (index==1){
            $("#filter_tab").val('rs');
            $("#filter_nama").val('rs');
            $("#div_rujukanLuar").hide();
            $("#div_rujukanRS").show();
        }
   }
function onReset()
{
    setTimeout(
        function(){
            $.fn.yiiGridView.update('tableRujukanLuar', {
                data: $("#searchLaporan").serialize()
            });
            $.fn.yiiGridView.update('tableRujukanRS', {
                data: $("#searchLaporan").serialize()
            });       
        }, 2000
    );
    return false;
}   
JS;
Yii::app()->clientScript->registerScript('pemeriksaanRujukan',$js,CClientScript::POS_HEAD);
?>

