<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrintLaporan();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchLaporan();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
?>
<?php $this->widget($table,array(
	'id'=>'tableLaporan',
	'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:center;'),
                'footerHtmlOptions'=>array('colspan'=>8,'style'=>'font-weight:bold;text-align:right;'),
                'footer'=>'Total',
            ),
            array(
                'header'=>'No Lab',
                'type'=>'raw',
                'value'=>'$data->no_masukpenunjang',
                'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
                'header'=>'No Pendaftaran',
                'type'=>'raw',
                'value'=>'$data->no_pendaftaran',
                'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
                'header'=>'Nama Pasien',
                'value'=>'$data->nama_pasien',
                'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
            ),
            array(
                'header'=>'Alamat',
                'type'=>'raw',
                'value'=>'$data->alamat_pasien',
                'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
            ),
            array(
                'header'=>'Cara Bayar / Penjamin',
                'type'=>'raw',
                'value'=>'$data->carabayar_nama." / ".$data->penjamin_nama',
                'headerHtmlOptions'=>array('style'=>'text-align:center'),
                ),
            array(
                'header'=>'<center>Dokter</center>',
                'type'=>'raw',
                'value'=>'$data->nama_pegawai',
            ),
            array(
                'header'=>'<center>Perujuk</center>',
                'type'=>'raw',
                'value'=>'$data->namaperujuk',
            ),
            array(
                'header'=>'Tarif Tindakan',
                'name'=>'tarif_tindakan',
                'type'=>'raw',
                'value'=>'MyFunction::formatNumber($data->tarif_tindakan)',
                'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:right;'),
                'footer'=>'sum(tarif_tindakan)',
            ),
            array(
                'header'=>'Deposit',
                'name'=>'jumlahuangmuka',
                'type'=>'raw',
                'value'=>'isset($data->jumlahuangmuka) ? $data->jumlahuangmuka : 0',
                'headerHtmlOptions'=>array('style'=>'text-align:center'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:right;'),
                'footer'=>'sum(jumlahuangmuka)',
            ),
            array(
                'header'=>'Jasa Dokter',
                'name'=>'jasadokter',
                'type'=>'raw',
                'value'=>'MyFunction::formatNumber($data->jasadokter)',
                'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:right;'),
                'footer'=>'sum(jasadokter)',
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>