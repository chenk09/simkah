<legend class="rim2">Laporan Jasa Dokter</legend>

<?php
$url = Yii::app()->createUrl('laboratorium/laporan/frameGrafikJasaDokter&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
	$('#tableLaporan').addClass('srbacLoading');
	$('#Grafik').attr('src','').css('height','0px');
	$.fn.yiiGridView.update('tableLaporan', {
		data: $(this).serialize()
	});
    return false;
});
");
?>
<div class="search-form">
<?php $this->renderPartial('jasaDokter/_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form --> 
<fieldset> 
    <legend class="rim">Tabel Jasa Dokter</legend>
    <?php $this->renderPartial('jasaDokter/_table', array('model'=>$model)); ?>
    <?php //$this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);"></iframe>        
</fieldset>
<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanJasaDokter');
    $this->renderPartial('_footerNonGrafik', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>
<script> 
function pilihDokter(){
    var pilih = $("#LBLaporanjasadokterlabradV_pilihDokter");
    if(pilih.val() == "RUJUKANLUAR"){
       $("#formRujukan").show();
        $("#formDokter").find('input').each(function(){
            $(this).val("");
        });
        $("#formDokter").hide();
    }else{
        $("#formDokter").show();
        $("#formRujukan").find('input').each(function(){
            $(this).val("");
        });
        $("#formRujukan").hide();
    }
    bersihTabelDetail();
    bersihFormPembayaran();
}
pilihDokter(); //default

function bersihTabelDetail(){
    $('#tabelDetail tbody').html("");
}

function bersihFormPembayaran(){
    $('#formPembayaran .currency').each(function(){
        $(this).val(0);
    });
}

</script>



