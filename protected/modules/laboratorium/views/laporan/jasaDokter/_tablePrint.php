<?php 
    $rim = 'width:600px;overflow-x:none;';
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $data = $model->searchLaporan();
    $template = "{pager}{summary}\n{items}";
    $sort = true;
    if (isset($caraPrint)){
      $sort = false;
      $data = $model->searchPrintLaporan();
      $rim = '';
      $template = "{items}";
      if ($caraPrint == "EXCEL")
          $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
?>

<div>
    <?php 
        $criteriaSearch = new CDbCriteria();
        $format = new CustomFormat();
        if (isset($_GET['LBLaporanjasadokterlabradV']['tglAwal'])){
            $tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanjasadokterlabradV']['tglAwal']);
        }
        if (isset($_GET['LBLaporanjasadokterlabradV']['tglAkhir'])){
            $tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanjasadokterlabradV']['tglAkhir']);
        }
        
        if($_GET['LBLaporanjasadokterlabradV']['pilihDokter'] == "RUJUKANLUAR"){
            $namapegawai = $_GET['LBLaporanjasadokterlabradV']['nama_perujuk'];
            $idPegawai = $_GET['LBLaporanjasadokterlabradV']['rujukan_id'];
        }else{
            $namapegawai = $_GET['LBLaporanjasadokterlabradV']['nama_pegawai'];
        }
		
		$tglAw = date('d M Y',strtotime($tglAwal));
		$tglAk = date('d M Y',strtotime($tglAkhir));
		$tglPerAwal = explode(" ",$tglAw);
		$tglPerAkhir = explode(" ",$tglAk);
		$tanggalAw = $tglPerAwal[0];
		$bulanAw = $tglPerAwal[1];
		$tahunAw = $tglPerAwal[2];
		
		$tanggalAk = $tglPerAkhir[0];
		$bulanAk = $tglPerAkhir[1];
		$tahunAk = $tglPerAkhir[2];
            
		$tglPeriode = $tanggalAw." ".$bulanAw." ".$tahunAw." - ".$tanggalAk." ".$bulanAk." ".$tahunAk;
		$tgls = $tglPeriode;
        $criteriaSearch->select ='nama_pegawai';
        $criteriaSearch->group ='nama_pegawai';
        $criteriaSearch->order='nama_pegawai asc';
		
        $criteriaSearch->compare('LOWER(nama_pegawai)',strtolower($namapegawai),true);
		$criteriaSearch->addBetweenCondition('DATE(tglmasukpenunjang)', $tglAwal, $tglAkhir);
        $models = LBLaporanjasadokterlabradV::model()->findAll($criteriaSearch);
		
        $pegawai_temp = "";
        foreach($models as $i=>$data){
            if($data->nama_pegawai){
                $pegawai = $data->nama_pegawai;
            } else{
                $pegawai = '';
            }           
            if($pegawai_temp != $pegawai)
            {
                echo "<table >
                        <tr>
                            <td>Nama Dokter</td>
                            <td>:</td>
                            <td>".$pegawai."</td>
                        </tr>
                        <tr>
                            <td>Periode</td>
                            <td>:</td>
                            <td>".$tgls."</td>
                        </tr>";
                echo "</table>";
            
            echo "<table width='100%' border='1'>
                            <tr style='font-weight:bold;'>
                                <td align='center'>No</td>
                                <td align='center'>No Pendaftaran</td>
                                <td align='center'>No Lab</td>
                                <td align='center'>Nama Pasien</td>
                                <td align='center'>Alamat</td>
                                
                                <td align='center'>Tarif Tindakan</td>
                                <td align='center'>Deposit</td>
                                <td align='center'>Jasa Dokter</td>
                            </tr>";
               
            $criteria = new CDbCriteria;
            $format = new CustomFormat();
            if (isset($_GET['LBLaporanjasadokterlabradV']['tglAwal'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanjasadokterlabradV']['tglAwal']);
            }
            if (isset($_GET['LBLaporanjasadokterlabradV']['tglAkhir'])){
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanjasadokterlabradV']['tglAkhir']);
            }
            $term = $pegawai;
            $criteria->select ='nama_pegawai,no_masukpenunjang,nama_pasien,alamat_pasien,sum(tarif_tindakan) as tarif_tindakan,sum(jasadokter) as jasadokter, sum(jumlahuangmuka) as jumlahuangmuka,nama_pegawai,no_pendaftaran';
            $criteria->group ='nama_pegawai,nama_pegawai,nama_pasien,no_masukpenunjang,alamat_pasien,no_pendaftaran';
            $criteria->order = 'nama_pasien asc';
            $condition  = "nama_pegawai = '".$term."'";
            $criteria->compare('LOWER(nama_pegawai)',strtolower($term),true);
//            $criteria->addCondition($condition);
            $criteria->addBetweenCondition('tglmasukpenunjang', $tglAwal, $tglAkhir);
            $criteria->limit = -1;
            
            $totHarga = 0;
            $totBayar = 0;
            $detail = LBLaporanjasadokterlabradV::model()->findAll($criteria);
            foreach($detail as $key=>$details){
                    $totHarga += $details->tarif_tindakan;
                    $totBayar += $details->jasadokter;
                    $totjumlahuangmuka += $details->jumlahuangmuka;
                    
                    echo "<tr>
                              <td width='3%;' style='text-align:center'>".($key+1)."</td>
                              <td width='10%;'><center>".$details->no_pendaftaran."</center></td>
                              <td width='10%;'><center>".$details->no_masukpenunjang."</center></td>
                              <td width='25%;'>".$details->nama_pasien."</td>
                              <td width='25%;'>".$details->alamat_pasien."</td>
                              
                              <td width='10%;' style='text-align:right'>".MyFunction::formatNumber($details->tarif_tindakan)."</td>
                              <td width='10%;' style='text-align:right'>".MyFunction::formatNumber($details->jumlahuangmuka)."</td>
                              <td width='10%;' style='text-align:right'>".MyFunction::formatNumber($details->jasadokter)."</td>
                          </tr>";
            }
            
                    echo "<tr>
                              <td colspan=5 style='text-align:right'><b>Total : </b></td>
                              <td width='10%;' style='text-align:right;font-weight:bold;'>".MyFunction::formatNumber($totHarga)."</td>
                              <td width='10%;' style='text-align:right;font-weight:bold;'>".MyFunction::formatNumber($totjumlahuangmuka)."</td>
                              <td width='10%;' style='text-align:right;font-weight:bold;'>".MyFunction::formatNumber($totBayar)."</td>
                          </tr>";
            echo "</table><br/><br/>";
            
            }
            $pegawai_temp = $pegawai;
        }
    ?> 
</div>