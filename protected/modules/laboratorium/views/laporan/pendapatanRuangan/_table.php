<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrintPendapatanR();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchPendapatanR();
         $template = "{pager}{summary}\n{items}";
    }
?>

<?php $this->widget($table,array(
	'id'=>'tableLaporan',
	'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Tarif</center>',
                'start'=>7, //indeks kolom 7
                'end'=>12, //indeks kolom 8
            ),
        ),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header' => 'No',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                ),
                array(
                    'header'=>'No RM',
                    'name'=>'no_rekam_medik',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'Nama<br/>Pasien',
                    'type'=>'raw',
                    'name'=>'nama_pasien',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'No</br>Pendaftaran',
                    'type'=>'raw',
                    'name'=>'no_pendaftaran',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'Nama Dokter',
                    'name'=>'nama_pegawai',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'Nama Kelas <br/> Pelayanan',
                    'type'=>'raw',
                    'name'=>'kelaspelayanan_nama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    'footerHtmlOptions'=>array('colspan'=>7,'style'=>'text-align:right;font-style:italic;'),
                    'footer'=>'Jumlah Total',
                ),
                array(
                    'header'=>'Cara Bayar /<br/> Penjamin',
                    'type'=>'raw',
                    'name'=>'carabayarPenjamin',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),                    
//                array(
//                    'header'=>'Total Tarif',
////                    'name'=>'tarif',
//                    'value'=>'MyFunction::formatNumber($data->getTarif("total"))',
//                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                    'htmlOptions'=>array('style'=>'text-align:right;'),
//                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
//                    'footer'=>  MyFunction::formatNumber($model->getTarif('total',true))
//                ),
//                array(
//                    'header'=>'Jasa Perujuk',
////                    'name'=>'tarif',
//                    'value'=>'MyFunction::formatNumber($data->getTarif("perujuk"))',
//                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                    'htmlOptions'=>array('style'=>'text-align:right;'),
//                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
//                    'footer'=>  MyFunction::formatNumber($model->getTarif('perujuk',true))
//                ),
//                array(
//                    'header'=>'Jasa Pembaca',
////                    'name'=>'tarif',
//                    'value'=>'MyFunction::formatNumber($data->getTarif("pembaca"))',
//                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                    'htmlOptions'=>array('style'=>'text-align:right;'),
//                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
//                    'footer'=>  MyFunction::formatNumber($model->getTarif('pembaca',true))
//                ),
//                array(
//                    'header'=>'Jasa Prod. <br/> Analis & Rad.',
////                    'name'=>'tarif',
//                    'value'=>'MyFunction::formatNumber($data->getTarif("analisrad"))',
//                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                    'htmlOptions'=>array('style'=>'text-align:right;'),
//                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
//                    'footer'=>  MyFunction::formatNumber($model->getTarif('analisrad',true))
//                ),
//                array(
//                    'header'=>'Insentif',
////                    'name'=>'tarif',
//                    'value'=>'MyFunction::formatNumber($data->getTarif("insentif"))',
//                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                    'htmlOptions'=>array('style'=>'text-align:right;'),
//                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
//                    'footer'=>  MyFunction::formatNumber($model->getTarif('insentif',true))
//                ),
//                array(
//                    'header'=>'Jasa Dokter <br/> Lab',
////                    'name'=>'tarif',
//                    'value'=>'MyFunction::formatNumber($data->getTarif("dokterlab"))',
//                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                    'htmlOptions'=>array('style'=>'text-align:right;'),
//                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
//                    'footer'=>  MyFunction::formatNumber($model->getTarif('dokterlab',true))
//                ),
////                array(
////                    'header'=>'Total',
////                    'name'=>'totalTarifLab',
////                    'value'=>'"Rp. ".MyFunction::formatNumber($data->totalTarifLab)',
////                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
////                    'htmlOptions'=>array('style'=>'text-align:right;'),
////                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
////                    'footer'=>'sum(totalTarifLab)',
////                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>