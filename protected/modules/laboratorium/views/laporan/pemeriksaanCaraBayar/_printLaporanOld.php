<?php 
    $rim = 'width:600px;overflow-x:none;';
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $data = $model->searchTableLaporan();
    $dataPerusahaan = $modelPerusahaan->searchTableLaporan();
    $template = "{pager}{summary}\n{items}";
    $sort = true;
    if (isset($caraPrint)){
      $sort = false;
      $data = $model->searchPrintLaporan();
      $dataPerusahaan = $modelPerusahaan->searchPrintLaporan();
      $rim = '';
      $template = "{items}";
      if ($caraPrint == "EXCEL")
          $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
?>

<?php if(isset($_GET['filter_tab']) == "pemeriksaan"){ ?>
<div id="div_group">
    <?php 
        $format2 = new CustomFormat();
        $criteria2 = new CDbCriteria;
        $criteria2->addBetweenCondition('tgl_tindakan',$format2->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAwal']),$format2->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAkhir']));
        $model = LBLaporanpemeriksaangroupV::model()->findAll();
        
        $penjamin_temp = "";
        foreach($model as $i=>$data){
            if($data->penjamin_id){
                $penjamin = $data->penjamin_nama;
            } else{
                $penjamin = '';
            }           
            if($penjamin_temp != $penjamin)
            {
            echo "<table width='100%' border='2'>
                    <tr>
                        <td colspan=7><b> Nama Group: $penjamin </b></td>
                    </tr>
                    <tr><td>";
            
            echo "<table width='100%' border='1'>
                            <tr style='font-weight:bold;background-color:#C0C0C0'>
                                <td align='center'>No</td>
                                <td align='center'>No Pendaftaran Lab</td>
                                <td align='center'>Nama Pasien</td>
                                <td align='center'>Alamat</td>
                                <td align='center'>Total</td>
                                <td align='center'>Bayar</td>
                                <td align='center'>Sisa</td>
                            </tr>";
            $format = new CustomFormat();
            $criteria = new CDbCriteria;
            $term = $penjamin;
            $condition  = "penjamin_nama ILIKE '%".$term."%' OR penjamin_nama ILIKE '%".$penjamin."%'";
            $criteria->select = 't.pendaftaran_id as id,
                    pendaftaran_t.no_pendaftaran,
                    pasien_m.nama_pasien,
                    pasien_m.alamat_pasien,
                    carabayar_m.carabayar_nama,
                    SUM(t.qty_tindakan * t.tarif_satuan) AS total_biaya,
                    SUM(tindakansudahbayar_t.jmlbayar_tindakan) AS bayartindakan,
                    SUM(tindakansudahbayar_t.jmlsisabayar_tindakan) AS sisatindakan';
            $criteria->group = 't.pendaftaran_id,pendaftaran_t.no_pendaftaran,pasien_m.nama_pasien,pasien_m.alamat_pasien, carabayar_m.carabayar_nama';
            $criteria->order = 't.pendaftaran_id, pendaftaran_t.no_pendaftaran';
            $criteria->addBetweenCondition('tgl_tindakan',$format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAwal']),$format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAkhir']));
            $criteria->addCondition($condition);
            $criteria->compare('t.carabayar_id',$_GET['LBLaporanpemeriksaangroupV']['carabayar_id']);
            $criteria->compare('t.ruangan_id',18);
            $criteria->join = 'JOIN pasienmasukpenunjang_t ON 
                                    t.pasienmasukpenunjang_id = pasienmasukpenunjang_t.pasienmasukpenunjang_id
                             LEFT JOIN tindakansudahbayar_t ON 
                                    t.tindakansudahbayar_id = tindakansudahbayar_t.tindakansudahbayar_id
                             JOIN pendaftaran_t ON
                                    pasienmasukpenunjang_t.pendaftaran_id = pendaftaran_t.pendaftaran_id
                             JOIN pasien_m ON 
                                    pendaftaran_t.pasien_id = pasien_m.pasien_id
                             JOIN penjaminpasien_m ON
                                    pendaftaran_t.penjamin_id = penjaminpasien_m.penjamin_id
                             JOIN carabayar_m ON 
                                    pendaftaran_t.carabayar_id = carabayar_m.carabayar_id';
            $criteria->limit = -1;
            
            $totHarga = 0;
            $totBayar = 0;
            $totSisa = 0;
            $detail = TindakanpelayananT::model()->findAll($criteria);
            foreach($detail as $key=>$details){
//                    $selisih += ($details->tarif_satuan * $details->qty_tindakan);
                    $selisih += $details->total_biaya;
//                    $harga = $details->tarif_satuan * $details->qty_tindakan;
                    $harga = $details->total_biaya;
                    $totHarga += $harga;
                    
                    if($details->bayartindakan > 0){
                        $bayartagihan = $details->bayartindakan;
                        $sisatagihan = $details->sisatindakan;
                        $totBayar +=$details->bayartindakan;
                    }else if($details->bayartindakan <= 0 ){
                        $bayartagihan = $details->bayartindakan;
                        $sisatagihan = $harga - $details->bayartindakan;
                        $totBayar +=$details->bayartindakan;
                    }
                    
                    if($details->sisatindakan <= 0){
                        $totSisa += $sisatagihan;
                    }else{
                        $totSisa += $details->sisatindakan;
                    }
                    
                    echo "<tr>
                              <td width='30px;' style='text-align:center'>".($key+1)."</td>
                              <td width='150px;'>".$details->no_pendaftaran."</td>
                              <td width='280px;'>".$details->nama_pasien."</td>
                              <td width='220px;'>".$details->alamat_pasien."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($harga)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($bayartagihan)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($sisatagihan)."</td>
                          </tr>";
            }
            
                    echo "<tfoot style='background-color:#E8E8E8;'>
                              <td colspan=4 style='text-align:right'>Total : </td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totHarga)."</td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totBayar)."</td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totSisa)."</td>
                          </tfoot>";
                     echo "</table><br/>";
                     
            
            }
            $penjamin_temp = $penjamin;
        }
         echo "<table align='right' style='margin-right:200px;'>";
         echo "<tr style='font-size:50px;margin-left:-30px;'>
                                  <td colspan=6 style='text-align:right;font-weight:bold;'>Selisih ( Jumlah Tagihan ) </td>
                                  <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($selisih)."</td>
                              </tr></table>";
         
            
        echo "</td></tr></table>";
    ?> <br/><br/>
</div>

<?php }else if($_GET['filter_tab'] == "rincian"){ ?>
<div id="div_perusahaan">
       <?php 
        $modelPerusahaan = LBLaporanpemeriksaanp3V::model()->findAll();
        
        $penjamin_temp = "";
        foreach($modelPerusahaan as $i=>$data){
            if($data->penjamin_id){
                $penjamin = $data->penjamin_nama;
            } else{
                $penjamin = '';
            }           
            if($penjamin_temp != $penjamin)
            {
             echo "<table width='100%' border='2'>
                    <tr>
                        <td colspan=7><b> Nama Group: $penjamin </b></td>
                    </tr>
                    <tr><td>";
            echo "<table width='100%' border='1'>
                            <tr style='font-weight:bold;background-color:#C0C0C0'>
                                <td align='center'>No</td>
                                <td align='center'>No Pendaftaran Lab</td>
                                <td align='center'>Nama Pasien</td>
                                <td align='center'>Alamat</td>
                                <td align='center'>Total</td>
                                <td align='center'>Bayar</td>
                                <td align='center'>Sisa</td>
                            </tr>";
               
            $format = new CustomFormat();
            $criteria = new CDbCriteria;
            $term = $penjamin;
            $criteria->select = ' t.pendaftaran_id as id,
                    pasienmasukpenunjang_t.pasienmasukpenunjang_id,
                    pasienmasukpenunjang_t.no_masukpenunjang,
                    t.pendaftaran_id,
                    pendaftaran_t.no_pendaftaran,
                    pasien_m.nama_pasien,
                    daftartindakan_m.daftartindakan_nama,
                    carabayar_m.carabayar_nama,
                    pasienmasukpenunjang_t.statusperiksa,
                    SUM(tindakansudahbayar_t.jmlbayar_tindakan) AS bayartindakan,
                    SUM(tindakansudahbayar_t.jmlsisabayar_tindakan) AS sisatindakan,
                    SUM(t.qty_tindakan * t.tarif_satuan) AS total_biaya';
            $criteria->group = 't.pendaftaran_id,pasienmasukpenunjang_t.pasienmasukpenunjang_id,
                          pasienmasukpenunjang_t.no_masukpenunjang,pendaftaran_t.no_pendaftaran,
                          carabayar_m.carabayar_nama,pasien_m.nama_pasien,pasienmasukpenunjang_t.statusperiksa,daftartindakan_m.daftartindakan_nama';
            $criteria->order = 'tindakanpelayanan_t.pendaftaran_id, pasienmasukpenunjang_t.statusperiksa';
            $condition  = "penjamin_nama ILIKE '%".$term."%' OR penjamin_nama ILIKE '%".$penjamin."%'";
            $criteria->addBetweenCondition('tgl_tindakan',$format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAwal']),$format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaangroupV']['tglAkhir']));
            $criteria->addCondition($condition);
            $criteria->compare('t.ruangan_id',18);
            $criteria->compare('t.carabayar_id',$_GET['LBLaporanpemeriksaangroupV']['carabayar_id']);
            $criteria->join = 'JOIN pasienmasukpenunjang_t ON 
                                        t.pasienmasukpenunjang_id = pasienmasukpenunjang_t.pasienmasukpenunjang_id
                                 LEFT JOIN tindakansudahbayar_t ON 
                                        t.tindakansudahbayar_id = tindakansudahbayar_t.tindakansudahbayar_id
                                 JOIN daftartindakan_m ON 
                                        t.daftartindakan_id = daftartindakan_m.daftartindakan_id
                                 JOIN pendaftaran_t ON
                                        pasienmasukpenunjang_t.pendaftaran_id = pendaftaran_t.pendaftaran_id
                                 JOIN pasien_m ON 
                                        pendaftaran_t.pasien_id = pasien_m.pasien_id
                                 JOIN penjaminpasien_m ON
                                        pendaftaran_t.penjamin_id = penjaminpasien_m.penjamin_id
                                 JOIN carabayar_m ON 
                                        pendaftaran_t.carabayar_id = carabayar_m.carabayar_id';
            $criteria->limit = -1;
            
            $totHarga = 0;
            $totBayar = 0;
            $totSisa = 0;
            $details = TindakanpelayananT::model()->findAll($criteria);
            foreach($details as $key=>$detail){
//                    $selisih += ($detail->tarif_satuan * $detail->qty_tindakan); 
                    $selisih += $detail->total_biaya; 
//                    $harga = $detail->tarif_satuan * $detail->qty_tindakan;
                    $harga = $detail->total_biaya;
                    $totHarga += $harga;
                    
                    if($detail->bayartindakan > 0){
                        $bayartagihan = $detail->bayartindakan;
                        $sisatagihan = $detail->sisatindakan;
                        $totBayar +=$detail->bayartindakan;
                    }else if($detail->bayartindakan <= 0 ){
                        $bayartagihan = $detail->bayartindakan;
                        $sisatagihan = $harga - $detail->bayartindakan;
                        $totBayar +=$detail->bayartindakan;
                    }
                    
                    if($detail->sisatindakan <= 0){
                        $totSisa += $sisatagihan;
                    }else{
                        $totSisa += $detail->sisatindakan;
                    }
                    
                    
                   echo "<tr>
                              <td width='30px;' style='text-align:center'>".($key+1)."</td>
                              <td width='150px;'>".$detail->no_pendaftaran."</td>
                              <td width='280px;'>".$detail->nama_pasien."</td>
                              <td width='220px;'>".$detail->alamat_pasien."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($harga)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($bayartagihan)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($sisatagihan)."</td>
                          </tr>";
            }
            
                    echo "<tfoot style='background-color:#E8E8E8;' >
                            <tr>
                              <td colspan=4 style='text-align:right'>Total : </td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totHarga)."</td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totBayar)."</td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totSisa)."</td>
                             </tr>
                          </tfoot>";
            echo "</table><br/>";
            
            }
            $penjamin_temp = $penjamin;
        }
        echo "<table align='right' style='margin-right:200px;'>";
        echo "<tr style='font-size:50px;margin-left:-30px;'>
                                  <td colspan=6 style='text-align:right;font-weight:bold;'>Selisih ( Jumlah Tagihan ) </td>
                                  <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($selisih)."</td>
                              </tr></table>";
        
        echo "</td></tr></table>";
    ?><br/><br/>
</div>
<?php } ?>