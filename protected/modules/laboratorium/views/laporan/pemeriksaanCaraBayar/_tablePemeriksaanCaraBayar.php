<?php
    $data = $model->searchLaporanCarabayar();
    if(isset($caraPrint)){
        $data = $model->searchPrintLaporanCarabayar();
    }
        
    $this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',
        array(
            'id'=>'tableGroupPemeriksaanCaraBayar',
            'dataProvider'=>$data,
            'template'=>"{pager}{summary}\n{items}",
            'enableSorting'=>true,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                array(
                    'header' => '<center>No</center>',
                    'type'=>'raw',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    'htmlOptions'=>array(
                        'style'=>'text-align:center'
                    ),
                    'footerHtmlOptions'=>array(
                        'colspan'=>7,
                        'style'=>'text-align:right;font-style:italic;'
                    ),
                    'footer'=>'Total',
                ),
                array(
                    'header' => '<center>No Pendaftaran</center>',
                    'type'=>'raw',
                    // 'name' => 'no_pendaftaran',
                    'value'=>'$data->no_pendaftaran',
                ),
                array(
                    'header' => '<center>Nama Pasien</center>',
                    'type'=>'raw',
                    // 'name' => 'nama_pasien',
                    'value'=>'$data->nama_pasien',
                ),
                array(
                    'header' => '<center>Tanggal Masuk Penunjang</center>',
                    'type'=>'raw',
//                    'name' => 'tglmasukpenunjang',
                    'value'=>'$data->tglmasukpenunjang',
                ),
                array(
                    'header' => '<center>Alamat Pasien</center>',
                    'type'=>'raw',
                    // 'name' => 'alamat_pasien',
                    'value'=>'$data->alamat_pasien',
                ),
                array(
                    'header' => '<center>Cara Bayar</center>',
                    'type'=>'raw',
                    // 'name' => 'carabayar_nama',
                    'value'=>'$data->carabayar_nama',
                ),
                array(
                    'header' => '<center>Penjamin</center>',
                    'type'=>'raw',
                    // 'name' => 'penjamin_nama',
                    'value'=>'$data->penjamin_nama',
                ),
                array(
                   'header' => '<center>Status</center>',
                    'type'=>'raw',
                     'name' => 'statusperiksa',
                //   'value'=>'$data->statusbayar',
                ),
                array(
                    'header' => '<center>Total Biaya</center>',
                    'type'=>'raw',
                    'name' => 'total_biaya',
                    'value'=>'MyFunction::formatNumber($data->total_biaya)',
                    'htmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),
                       'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(total_biaya)',
                ),
                 array(
                    'header' => '<center>Deposit</center>',
                   'type'=>'raw',
                     'name' => 'uangmuka',
                    'value'=>'MyFunction::formatNumber($data->uangmuka)',
                      'htmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(uangmuka)',
                ),
                array(
                    'header' => '<center>Bayar</center>',
                    'type'=>'raw',
                    'name' => 'bayartindakan',
                    'value'=>'MyFunction::formatNumber($data->bayartindakan)',
                    'htmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(bayartindakan)',
                ),
                array(
                    'header' => '<center>Sisa</center>',
                    'type'=>'raw',
                    'name' => 'sisatindakan',
                    'value'=>'MyFunction::formatNumber($data->sisatindakan)',
                    'htmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(sisatindakan)',
                ),
            ),
            'afterAjaxUpdate'=>'function(id, data){
                jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            }',                    
        )
    );
?>



