<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = true;
    if (isset($caraPrint)){
        ?>
<style>
    thead th, .footer{
        border: 1px solid #000;
    }
    .tablePrint{
        width: 100%;
    }
    
</style>
        <?php
        $data = $model->searchPrintLaporan();
        $template = "{items}";
        $sort = false;
        $css = 'tablePrint';
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchTableLaporan();
         $template = "{pager}{summary}\n{items}";
         $css = 'table table-striped table-bordered table-condensed';
    }
?>
<?php
    if(isset($caraPrint)){
        if($_GET['filter_tab'] == "rekap"){
            $this->renderPartial('laboratorium.views.laporan.pemeriksaanPenunjang/_printRekap', array('model'=>$model,'modDetail'=>$modDetail));
        }
        
        if($_GET['filter_tab'] == "detail"){
            $this->renderPartial('laboratorium.views.laporan.pemeriksaanPenunjang/_printDetail', array('model'=>$model,'modDetail'=>$modDetail));
        }
    }else{
?>
<?php $this->widget($table,array(
    'id'=>'tableRekapPemeriksaan',
    'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>$css,
    'columns'=>array(
        array(
            'header' => '<center>No</center>',
            'type'=>'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            'htmlOptions'=>array('style'=>'text-align:center'),
        ),
        array(
            'header' => '<center>Kode</center>',
            'type'=>'raw',
            'value' => '$data->daftartindakan_kode',
        ),
        array(
            'header'=>'<center>Nama Jenis Periksa</center>',
            'type'=>'raw',
            'value'=>'$data->daftartindakan_nama',
            'footerHtmlOptions'=>array('colspan'=>3,'style'=>'text-align:right;font-style:italic;'),
            'footer'=>'Total',
        ),
        array(
            'header'=>'<center>Harga</center>',
            'name'=>'tarif_satuan',
            'type'=>'raw',
            'value'=>'MyFunction::formatNumber($data->tarif_satuan)',
            'htmlOptions'=>array('style'=>'text-align:right'),
            'footerHtmlOptions'=>array('style'=>'text-align:right;'),
            'footer'=>'sum(tarif_satuan)',
        ),
        array(
            'header'=>'<center>Jumlah</center>',
            'name'=>'qty_tindakan',
            'type'=>'raw',
            'value'=>'MyFunction::formatNumber($data->qty_tindakan)',
            'htmlOptions'=>array('style'=>'text-align:center'),
            'footerHtmlOptions'=>array('style'=>'text-align:center;'),
            'footer'=>'sum(qty_tindakan)',
        ),
        array(
            'header'=>'<center>Total</center>',
            'name'=>'Total',
            'type'=>'raw',
            'value'=>'MyFunction::formatNumber($data->Total)',
            'htmlOptions'=>array('style'=>'text-align:right'),
            'footerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:right;'),
            'footer'=>'sum(Total)',
        ),
        
    ),
)); ?> 

<?php } ?>