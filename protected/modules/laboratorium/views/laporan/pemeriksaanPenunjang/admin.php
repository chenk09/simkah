<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
$url = Yii::app()->createUrl('laboratorium/laporan/frameGrafikPemeriksaanPenunjang&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('#searchLaporan').submit(function(){
        $('#tableRekapPemeriksaan').addClass('srbacLoading');
        $('#tableDetailPemeriksaan').addClass('srbacLoading');
        if($('#filter_tab').val() == 'rekap')
        {
            $.fn.yiiGridView.update('tableRekapPemeriksaan', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
            $.fn.yiiGridView.update('tableDetailPemeriksaan', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
        }else{            
            $.fn.yiiGridView.update('tableDetailPemeriksaan', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
            $.fn.yiiGridView.update('tableRekapPemeriksaan', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
        }
        $('#Grafik').attr('src','').css('height','0px');
        return false;
});
");
?>

<legend class="rim2"><?php echo $judulLaporan?></legend>
<div class="search-form">
    <?php $this->renderPartial('laboratorium.views.laporan.pemeriksaanPenunjang/_searchRekap',
            array('model'=>$model,'modDetail'=>$modDetail,'form'=>$form)); ?>
</div>    
<fieldset> 
    <div class="tab">
        <?php
            $this->widget('bootstrap.widgets.BootMenu',array(
                'type'=>'tabs',
                'stacked'=>false,
                'htmlOptions'=>array('id'=>'tabmenu'),
                'items'=>array(
                    array('label'=>'Rekap Pemeriksaan','url'=>'javascript:tab(0);','active'=>true),
                    array('label'=>'Detail Pemeriksaan','url'=>'javascript:tab(1);', 'itemOptions'=>array("index"=>1)),
                ),
            ))
        ?>

        <div>
            <div id="div_rekap">
                <legend class="rim">Tabel Pemeriksaan Lab - Rekap</legend>
                <?php $this->renderPartial('laboratorium.views.laporan.pemeriksaanPenunjang/_tableRekap', array('model'=>$model,'modDetail'=>$modDetail)); ?>
            </div>
            <div id="div_detail">
                <legend class="rim">Tabel Pemeriksaan Lab - Detail</legend>
                <?php $this->renderPartial('laboratorium.views.laporan.pemeriksaanPenunjang/_tableDetail', array('model'=>$model,'modDetail'=>$modDetail)); ?>
            </div>
        </div>            
    </div>
    <iframe src="" id="Grafik" width="100%" height='0' onload="javascript:resizeIframe(this);">
    </iframe>       
</fieldset>
<?php   
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanPemeriksaanPenunjang');
    $this->renderPartial('laboratorium.views.laporan._footerNonGrafik', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>
<?php
$js= <<< JS
    $(document).ready(function() {
        $("#tabmenu").children("li").children("a").click(function() {
            $("#tabmenu").children("li").attr('class','');
            $(this).parents("li").attr('class','active');
            $(".icon-pencil").remove();
            $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
        });
        
        $("#div_rekap").show();
        $("#search_rekap").show();
        $("#tgl_rekap").show();
        $("#div_detail").hide();
        $("#search_detail").hide();
        $("#tgl_detail").hide();
    });

    function tab(index){
        $(this).hide();
        if (index==0){
            $("#filter_tab").val('rekap');
            $("#div_rekap").show();
            $("#search_rekap").show();
            $("#tgl_rekap").show();
            $("#div_detail").hide();
            $("#search_detail").hide();
            $("#tgl_detail").hide();
            $.fn.yiiGridView.update('tableRekapPemeriksaan', {
                    data: $("#searchLaporan").serialize()
                }
            );            
        } else if (index==1){
            $("#filter_tab").val('detail');
            $("#div_rekap").hide();
            $("#search_rekap").hide();
            $("#tgl_rekap").hide();
            $("#div_detail").show();
            $("#search_detail").show();
            $("#tgl_detail").show();
            $.fn.yiiGridView.update('tableDetailPemeriksaan', {
                    data: $("#searchLaporan").serialize()
                }
            );
        }
   }
function onReset()
{
   setTimeout(
        function()
        {
            if($("#filter_tab").val() == 'rekap')
            {
                $('#tableRekapPemeriksaan').addClass('srbacLoading');
                $.fn.yiiGridView.update('tableRekapPemeriksaan', {
                        data: $("#searchLaporan").serialize()
                    }
                );
                $.fn.yiiGridView.update('tableDetailPemeriksaan', {
                        data: $("#searchLaporan").serialize()
                    }
                );
            }else{
                $('#tableDetailPemeriksaan').addClass('srbacLoading');
                $.fn.yiiGridView.update('tableDetailPemeriksaan', {
                        data: $("#searchLaporan").serialize()
                    }
                );
                $.fn.yiiGridView.update('tableRekapPemeriksaan', {
                        data: $("#searchLaporan").serialize()
                    }
                );
            }
        }, 500
    );
    return false;
}   
JS;
Yii::app()->clientScript->registerScript('pemeriksaanLab',$js,CClientScript::POS_HEAD);
?>
<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT

function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
        $.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
                $('#LBLaporanpemeriksaanpenunjangV_tglAwal').val(data.periodeawal);
                $('#LBLaporanpemeriksaanpenunjangV_tglAkhir').val(data.periodeakhir);
        },'json');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
</script>
<script>
function pilihDokter(){
    var pilih = $("#LBLaporandetailpemeriksaanpenunjangV_pilihDokter");
    if(pilih.val() == "RUJUKANLUAR"){
       $("#formRujukan").show();
        $("#formTglPenunjang").show();
        $("#formDokter").find('input').each(function(){
            $(this).val("");
        });
        $("#formTglPendaftaran").find('input').each(function(){
            $(this).val("");
        });
        $("#formDokter").hide();
        $("#formTglPendaftaran").hide();
    }else{
        $("#formDokter").show();
        $("#formTglPendaftaran").show();
        $("#formRujukan").find('input').each(function(){
            $(this).val("");
        });
        $("#formTglPenunjang").find('input').each(function(){
            $(this).val("");
        });
        $("#formRujukan").hide();
        $("#formTglPenunjang").hide();
    }
    bersihTabelDetail();
    bersihFormPembayaran();
}
pilihDokter(); //default
function addDetail(){
    var rujukandari_id = $('#BKPembayaranjasaT_rujukandari_id').val();
    var pegawai_id = $('#BKPembayaranjasaT_pegawai_id').val();
    var tglAwalPenunjang = $('#BKPembayaranjasaT_tglAwalPenunjang').val();
    var tglAkhirPenunjang = $('#BKPembayaranjasaT_tglAkhirPenunjang').val();
    var tglAwalPendaftaran = $('#BKPembayaranjasaT_tglAwalPendaftaran').val();
    var tglAkhirPendaftaran = $('#BKPembayaranjasaT_tglAkhirPendaftaran').val();
    var komponentarifIds = {};
    var i = 0;
    $('#checkBoxList').find('input').each(function(){
        if($(this).is(':checked')){
            komponentarifIds[i] = $(this).val();
            i ++;
        }
    });
    if(tglAwalPenunjang.length > 0 && tglAkhirPenunjang.length > 0 && i > 0){
        var tglAwal = tglAwalPenunjang;
        var tglAkhir = tglAkhirPenunjang;
    }else if(tglAwalPendaftaran.length > 0 && tglAkhirPendaftaran.length > 0){
        var tglAwal = tglAwalPendaftaran;
        var tglAkhir = tglAkhirPendaftaran;
    }else{
        alert ("Silahkan isi form dengan benar ! Rujukan / Dokter, Tanggal dan Komponen Tarif wajib diisi !");
        return false;
    }
    bersihTabelDetail();
    bersihFormPembayaran();
    $('#tabelDetail').addClass('srbacLoading');
    $.post("<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasa'); ?>", {
        rujukandari_id:rujukandari_id, 
        pegawai_id: pegawai_id, 
        tgl_awal:tglAwal, 
        tgl_akhir:tglAkhir, 
        komponentarifId:komponentarifIds},
        function(data){
            if (data.tr == ""){
                alert('Data tidak ditemukan !');
                $('#tabelDetail').removeClass('srbacLoading');
                return false;
            }else{
                $('#tabelDetail tbody').append(data.tr);
                $("#tabelDetail tbody tr .currency").each(function(){
                    $(this).maskMoney({"defaultZero":true,"allowZero":true,"decimal":"","thousands":",","precision":0,"symbol":null});
                });    
                formatNumbers();
                hitungSemua();
                $('#tabelDetail').removeClass('srbacLoading');
            }
        }, "json");
    return false;
}
function formatNumbers(){
    $('.currency').each(function(){this.value = formatNumber(this.value)});
}
formatNumbers();
function unformatNumbers(){
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
}
function bersihTabelDetail(){
    $('#tabelDetail tbody').html("");
}
function bersihFormPembayaran(){
    $('#formPembayaran .currency').each(function(){
        $(this).val(0);
    });
}
function hitungSemua(){
    unformatNumbers();
    var totTarif = 0;
    var totJasa = 0;
    var totBayar = 0;
    var totSisa = 0;
    $('#tabelDetail tbody tr').each(function(){
        if($(this).find('input[name$="[pilihDetail]"]').is(":checked")){ //hitung yang dicheck aja
            var jmltarif = parseFloat($(this).find('input[name$="[jumahtarif]"]').val());
            var jmljasa = parseFloat($(this).find('input[name$="[jumlahjasa]"]').val());
            var jmlbayar = parseFloat($(this).find('input[name$="[jumlahbayar]"]').val());
            var jmlsisa = parseFloat(jmljasa - jmlbayar);
            $(this).find('input[name$="[sisajasa]"]').val(jmlsisa);
            totTarif += jmltarif;
            totJasa += jmljasa;
            totBayar += jmlbayar;
            totSisa += jmlsisa;
        }
    });
    $("#BKPembayaranjasaT_totaltarif").val(totTarif);
    $("#BKPembayaranjasaT_totaljasa").val(totJasa);
    $("#BKPembayaranjasaT_totalbayarjasa").val(totBayar);
    $("#BKPembayaranjasaT_totalsisajasa").val(totSisa);
    formatNumbers();
}
function checkAll(obj){
    if($(obj).is(':checked')){
        $('#tabelDetail tbody tr').each(function(){
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).attr('checked',true)});
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).val(1)});
        });
    }else{
        $('#tabelDetail tbody tr').each(function(){
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).removeAttr('checked')});
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).val("")});
        });
    }
    hitungSemua();
}
function checkIni(obj){
    if($(obj).is(':checked')){
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).attr('checked',true)});
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).val(1)});
    }else{
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).removeAttr('checked')});
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).val("")});
        $('#pilihSemua').removeAttr('checked');
    }
    hitungSemua();
}
</script>