<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = true;
    if (isset($caraPrint)){
        ?>
<style>
    thead th, .footer{
        border: 1px solid #000;
    }
    .tablePrint{
        width: 100%;
    }
    
</style>
        <?php
        $data = $modDetail->searchPrintLaporan();
        $template = "{items}";
        $sort = false;
        $css = 'tablePrint';
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $modDetail->searchTableLaporan();
         $template = "{pager}{summary}\n{items}";
         $css = 'table table-striped table-bordered table-condensed';
    }
?>
<?php $this->widget($table,array(
    'id'=>'tableDetailPemeriksaan',
    'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>$css,
    'columns'=>array(
        array(
            'header' => '<center>No</center>',
            'type'=>'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            'htmlOptions'=>array('style'=>'text-align:center'),
        ),
        array(
            'header' => '<center>No Lab</center>',
            'type'=>'raw',
            'value' => '$data->no_masukpenunjang',
        ),
        array(
            'header'=>'<center>Tanggal</center>',
            'type'=>'raw',
            'value'=>'$data->tglmasukpenunjang',
        ),
        array(
            'header'=>'<center>Kode</center>',
            'type'=>'raw',
            'value'=>'$data->daftartindakan_kode',
        ),
        array(
            'header'=>'<center>Nama Jenis Periksa</center>',
            'type'=>'raw',
            'value'=>'$data->daftartindakan_nama',
        ),
        array(
            'header'=>'<center>Dokter</center>',
            'type'=>'raw',
            'value'=>'$data->nama_pegawai',
        ),
        array(
            'header'=>'<center>Cara Bayar</center>',
            'type'=>'raw',
            'value'=>'$data->carabayar_nama',
            'footerHtmlOptions'=>array('colspan'=>7,'style'=>'text-align:right;font-style:italic;'),
            'footer'=>'Total',
        ),
        array(
            'header'=>'<center>Harga</center>',
            'name'=>'total',
            'type'=>'raw',
            'value'=>'MyFunction::formatNumber($data->total)',
            'htmlOptions'=>array('style'=>'text-align:right'),
            'footerHtmlOptions'=>array('style'=>'text-align:right;'),
            'footer'=>'sum(total)', 
        ),
        
    ),
)); ?> 