<?php 
    echo "<table width='100%' border='1' class='table' id='tableLaporan'>
            <thead>
                    <tr style='font-weight:bold;'>
                        <td style='text-align:center;'>No</td>
                        <td style='text-align:center;'>No Lab</td>
                        <td style='text-align:center;'>Tanggal</td>
                        <td style='text-align:center;'>Kode</td>
                        <td style='text-align:center;'>Nama Jenis Periksa</td>
                        <td style='text-align:center;'>Harga</td>
                    </tr>";
    $criteriaSearch = new CDbCriteria();
    $format = new CustomFormat();
    if (isset($_GET['LBLaporanpemeriksaanpenunjangV']['tglAwal'])){
        $tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
    }
    if (isset($_GET['LBLaporanpemeriksaanpenunjangV']['tglAkhir'])){
        $tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
    }
    if($_GET['LBLaporandetailpemeriksaanpenunjangV']['pilihDokter'] == "RUJUKANLUAR"){
        $namapegawai = $_GET['LBLaporandetailpemeriksaanpenunjangV']['nama_perujuk'];
    }else{
        $namapegawai = $_GET['LBLaporandetailpemeriksaanpenunjangV']['namapegawai'];
    } 
    $criteriaSearch->addBetweenCondition('tglmasukpenunjang', $tglAwal, $tglAkhir);
    $criteriaSearch->compare('LOWER(nama_pegawai)',strtolower($namapegawai),true);
    $models = LBLaporandetailpemeriksaanpenunjangV::model()->findAll($criteriaSearch);
    $row = array();
    foreach($models as $i=>$data){
                $rujukan_id = $data->rujukan_id;
                $row[$rujukan_id]['nama'] = $data->nama_pegawai;
                $row[$rujukan_id]['ruangan_id'] = $data->ruangan_id;
                $row[$rujukan_id]['nama_ruangan'] = $data->ruangan_nama;
                $row[$rujukan_id]['kategori'][$i]['no_masukpenunjang'] = $data->no_masukpenunjang;
                    $format = new CustomFormat();
                    $tgl = date('Y-m-d',strtotime($format->formatDateTimeMediumForDB($val->tgl_tindakan)));
                    $tglTindakan = $format->formatDateINAtimeNew($tgl); 
                $row[$rujukan_id]['kategori'][$i]['tgl_tindakan'] = $tglTindakan;
                $row[$rujukan_id]['kategori'][$i]['daftartindakan_kode'] = $data->daftartindakan_kode;
                $row[$rujukan_id]['kategori'][$i]['daftartindakan_nama'] = $data->daftartindakan_nama;
                $row[$rujukan_id]['kategori'][$i]['harga'] = $data->tarif_satuan;
                $row[$rujukan_id]['kategori'][$i]['qty'] = $data->qty_tindakan;
                $row[$rujukan_id]['kategori'][$i]['total'] = ($row[$rujukan_id]['kategori'][$i]['harga'] * $row[$rujukan_id]['kategori'][$i]['qty']);
    }
?> 

<?php
    $cols = '';
    $total_biaya = 0;
     if(count($models) > 0){
    //     echo "masuk";exit;
    foreach($row as $key=>$values)
    {
            $cols .= '<tr>';
            $cols .= '<td colspan=6>';
            $cols .= '</td>';
            $cols .= '</tr>';
            $cols .= '<tr>';
            $cols .= '<td colspan=6><b>Perujuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.strtoupper($values['nama']).'</b></td>';
            $cols .= '</tr>';
            $cols .= '<td colspan=6><b>Kedatangan &nbsp;'.strtoupper($values['nama_ruangan']) .'</b></td>';
            $cols .= '</tr>';
        $col = '';
        $total = 0;
        foreach($values['kategori'] as $x => $val)
        {
                    $col .= '<tr style="page-break-inside: avoid;">';
                    $col .= '<td width="20" style="text-align:center">'.($x+1).'</td>';
                    $col .= '<td width="30" style="text-align:center;">'.$val['no_masukpenunjang'].'</td>';
                    $col .= '<td width="40" style="text-align:center;">'. $val['tgl_tindakan'] .'</td>';
                    $col .= '<td width="20" style="text-align:left;">'.$val['daftartindakan_kode'].'</td>'; //merge tarif dokter '. number_format($values['kategori'][$x]['harga'] + $values['kategori'][$x]['harga_dokter'],0,'','.') .'
                    $col .= '<td width="100" style="text-align:left;">'.$val['daftartindakan_nama'].'</td>'; //$values['kategori'][$x]['qty']
                    $col .= '<td width="40" style="text-align:right;" class="uang">'. number_format($values['kategori'][$x]['total'],2,',','.') .'</td>'; //  + $values['kategori'][$x]['total_dokter']
                    $col .= '</tr>';
                    $total += ($val['qty'] * $val['harga']);
        }
        
        $total = $total;
        $col .= '<tr>';
        $col .= '<td colspan=5 class="total" style="text-align:right;font-weight:bold">Total :</td>';
        $col .= '<td class="total" style="text-align:right;"><b>'. number_format($total,2,',','.') .'</b></td>';
        $col .= '</tr>';
        $cols .= $col;
    }
 }else{
        $col .= '<tr>';
        $col .= '<td colspan=6 class="total" style="text-align:left;font-weight:bold">Data tidak ditemukan</td>';        
        $col .= '</tr>';
        $cols .= $col;
}
echo($cols);
?>