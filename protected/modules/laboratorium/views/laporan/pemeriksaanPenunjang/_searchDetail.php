    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
        .nav-tabs > .active a:hover{cursor:pointer;}
    </style>
   <fieldset>
        <legend class="rim"><i class="icon-search"></i> Pencarian Berdasarkan : </legend>
    <table>
            <tr>
                <td>
                       <div class="control-group ">
                        <?php
                            echo CHtml::label('Tanggal Periode ','Tanggal Periode ', array('class'=>'control-label'));
                            echo CHtml::hiddenField('page',0, array('class'=>'number_page'));
                        ?>
                           
                            <div class="controls">
                                <?php echo $form->dropDownList($modDetail,'bulan',
                                    array(
                                        'hari'=>'Hari Ini',
                                        'bulan'=>'Bulan',
                                        'tahun'=>'Tahun',
                                    ),
                                    array(
                                        'id'=>'PeriodeName',
                                        'onChange'=>'setPeriode()',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;',
                                        'style'=>'width:120px;float:left',
                                    )
                                    );
                                ?>
                                <div style="margin-left:5px;float:left;">
                                <?php
                                    $this->widget('MyDateTimePicker', array(
                                        'model' => $modDetail,
                                        'attribute' => 'tglAwal',
                                        'mode' => 'datetime',
                                        'options' => array(
                                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array(
                                            'readonly' => true,
                                            'class'=>'dtPicker3',
                                            'onclick'=>'checkPilihan(event)',
                                            'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        ),
                                    ));
                                ?>
                            </div>
                            <div style="margin-left:5px;float:left;">
                                <?php
                                    $this->widget('MyDateTimePicker',
                                        array(
                                            'model'=>$modDetail,
                                            'attribute'=>'tglAkhir',
                                            'mode'=>'datetime',
                                            'options'=>array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            ),
                                            'htmlOptions'=>array(
                                                'readonly'=>true,
                                                'class'=>'dtPicker3',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'style'=>''
                                            ),
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </td> 
            </tr>
            <tr>
                <td>
                      <div class="control-group">
                          <label class="control-label">Perujuk</label>
                            <div class="controls">
                                <?php echo $form->textField($modDetail,'nama_pegawai',array('class'=>'span3','maxlength'=>50)); ?>
                            </div>
                        </div>    
                </td>
            </tr>
            <tr>
                <td>
                      <div class="control-group">
                          <label class="control-label">Kedatangan</label>
                            <div class="controls">
                                <?php echo $form->textField($modDetail,'ruangan_nama',array('class'=>'span3','maxlength'=>50)); ?>
                            </div>
                        </div>    
                </td>
            </tr>
     </table>
    </fieldset>