<div class="search-form" style="">
    <?php
        $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
            'type' => 'horizontal',
            'id' => 'searchLaporan',
            'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
        ));
    ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
        .nav-tabs > .active a:hover{cursor:pointer;}
    </style>
   <fieldset>
        <legend class="rim"><i class="icon-search"></i> Pencarian Berdasarkan : </legend>
        <table>
                <tr>
                    <td>
                                   <div class="control-group ">
                                    <?php
                                        echo CHtml::label('Tanggal Masuk Penunjang ','Tanggal Masuk Penunjang ', array('class'=>'control-label'));                                            
                                        echo CHtml::hiddenField('page',0, array('class'=>'number_page'));
                                        echo CHtml::hiddenField('filter_tab','rekap', array('class'=>'number_page'));
                                    ?>
                                   <div class="controls">
                                    <?php echo $form->dropDownList($model,'bulan',
                                        array(
                                            'hari'=>'Hari Ini',
                                            'bulan'=>'Bulan',
                                            'tahun'=>'Tahun',
                                        ),
                                        array(
                                            'id'=>'PeriodeName',
                                            'onChange'=>'setPeriode()',
                                            'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;',
                                            'style'=>'width:120px;float:left',
                                        )
                                        );
                                    ?>
                                    <div style="margin-left:5px;float:left;">
                                    <?php
                                        $this->widget('MyDateTimePicker', array(
                                            'model' => $model,
                                            'attribute' => 'tglAwal',
                                            'mode' => 'datetime',
                                            'options' => array(
                                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                            ),
                                            'htmlOptions'=>array(
                                                'readonly' => true,
                                                'class'=>'dtPicker3',
                                                'onclick'=>'checkPilihan(event)',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            ),
                                        ));
                                    ?>
                                </div>
                                <div style="margin-left:5px;float:left;">
                                    <?php
                                        $this->widget('MyDateTimePicker',
                                            array(
                                                'model'=>$model,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=>array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array(
                                                    'readonly'=>true,
                                                    'class'=>'dtPicker3',
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                    'style'=>''
                                                ),
                                            )
                                        );
                                    ?>
                                </div>
                            </div>
                           </div>
                    </td> 
                </tr>
         </table>
        <div id="search_rekap">
        </div>
        <div id="search_detail">
            <table>
            <tr>
                <td width="51" style="padding-left:55px;"> 
                    <fieldset>
                        <?php echo '<table id="penjamin_tbl">
                            <tr>
                                <td width=86px><label>Cara&nbsp;Bayar</label></td>
                                <td>'.$form->dropDownList($modDetail, 'carabayar_id', CHtml::listData(CarabayarM::model()->findAll('carabayar_aktif = true'), 'carabayar_id', 'carabayar_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                    'ajax' => array('type' => 'POST',
                                        'url' => Yii::app()->createUrl('ActionDynamic/GetPenjaminPasienForCheckBox', array('encode' => false, 'namaModel' => ''.$modDetail->getNamaModel().'')),
                                        'update' => '#penjamin',  //selector to update
                                    ),
                                )) . '
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Penjamin</label></td>
                                <td>
                                    <div id="penjamin">
                                        <label> Data Tidak Ditemukan </label>
                                    </div>
                                </td>
                            </tr>
                         </table>'; ?>
                    </fieldset>
            </tr>
            <tr>
                <td>
                    <div class="control-label" style="margin-left:122px;width:400px;">
                        <?php echo $form->dropDownList($modDetail,'pilihDokter', 
                                array('RUJUKANLUAR'=>'Jasa Dokter Luar', 'RUJUKANRS'=>'Jasa Dokter RS'), 
                                array('onchange'=>'pilihDokter();', 'class'=>'span3')); ?>
                    </div>
                </td>
                <td>


                    <div class="control-group" id="formRujukan">
                <?php echo $form->labelEx($modDetail,'rujukan_id', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->hiddenField($modDetail,'rujukan_id',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php $this->widget('MyJuiAutoComplete', array(
                               'model'=>$modDetail,
                               'attribute'=>'nama_perujuk',
                               'source'=>'js: function(request, response) {
                                              $.ajax({
                                                  url: "'.Yii::app()->createUrl('ActionAutoComplete/RujukanDari').'",
                                                  dataType: "json",
                                                  data: {
                                                      term: request.term,
                                                  },
                                                  success: function (data) {
                                                          response(data);
                                                  }
                                              })
                                           }',
                                'options'=>array(
                                      'showAnim'=>'fold',
                                      'minLength' => 2,
                                      'focus'=> 'js:function( event, ui ) {
                                           $(this).val("");
                                           $("#LBLaporandetailpemeriksaanpenunjangV_rujukan_id").val(ui.item.rujukan_id);
                                           $("#LBLaporandetailpemeriksaanpenunjangV_nama_perujuk").val(ui.item.namaperujuk);
                                           return false;
                                       }',
                                      'select'=>'js:function( event, ui ) {
                                           $(this).val(ui.item.value);
                                           $("#LBLaporandetailpemeriksaanpenunjangV_rujukan_id").val(ui.item.rujukan_id);
                                           $("#LBLaporandetailpemeriksaanpenunjangV_nama_perujuk").val(ui.item.namaperujuk);
                                           return false;
                                       }',
                               ),
                               'tombolDialog'=>array('idDialog'=>'dialogPerujuk','idTombol'=>'tombolPerujukDialog'),
                               'htmlOptions'=>array('placeholder'=>'Ketik Nama Perujuk', 'onkeypress'=>"return $(this).focusNextInputField(event);"),
                           )); 
                    ?>
                </div>
            </div>
                      <div class="control-group"  id="formDokter">
                <?php echo $form->labelEx($modDetail,'nama_pegawai', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->hiddenField($modDetail,'namapegawai',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php $this->widget('MyJuiAutoComplete', array(
                               'model'=>$modDetail,
                               'attribute'=>'nama_pegawai',
                               'source'=>'js: function(request, response) {
                                              $.ajax({
                                                  url: "'.Yii::app()->createUrl('ActionAutoComplete/GetDokter').'",
                                                  dataType: "json",
                                                  data: {
                                                      term: request.term,
                                                  },
                                                  success: function (data) {
                                                          response(data);
                                                  }
                                              })
                                           }',
                                'options'=>array(
                                      'showAnim'=>'fold',
                                      'minLength' => 2,
                                      'focus'=> 'js:function( event, ui ) {
                                           $(this).val("");
                                           return false;
                                       }',
                                      'select'=>'js:function( event, ui ) {
                                           $(this).val(ui.item.NamaLengkap);
                                           $("#LBLaporandetailpemeriksaanpenunjangV_nama_pegawai").val(ui.item.nama_pegawai);
                                           $("#LBLaporandetailpemeriksaanpenunjangV_namapegawai").val(ui.item.nama_pegawai);
                                           return false;
                                       }',
                               ),
                               'tombolDialog'=>array('idDialog'=>'dialogDokter','idTombol'=>'tombolDokterDialog'),
                               'htmlOptions'=>array('placeholder'=>'Ketik Nama Dokter RS', 'onkeypress'=>"return $(this).focusNextInputField(event);"),
                           )); 
                    ?>
                </div>
            </div>
                </td>
            </tr>
         </table>
        </div>
    
    </fieldset>
    
  <div class="form-actions">
    <?php
            echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), 
                    array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
    ?>
    <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                $this->createUrl('laporan/LaporanPemeriksaanPenunjang'), array('class'=>'btn btn-danger','onKeypress'=>'return formSubmit(this,event)')); ?>
  </div>  
</div>    
<?php $this->endWidget(); ?>
<script>
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
</script>

<?php 
//========= Dialog buat cari data dokter =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogDokter',
    'options'=>array(
        'title'=>'Pencarian Data Dokter',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
    $pegawai = new DokterpegawaiV('searchByDokter');
    if (isset($_GET['DokterpegawaiV'])){
        $pegawai->attributes = $_GET['DokterpegawaiV'];
    }

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'dokter-t-grid',
            'dataProvider'=>$pegawai->searchByDokter(),
            'filter'=>$pegawai,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPendaftaran",
                                        "onClick" => "
                                            $(\"#dialogDokter\").dialog(\"close\");
                                            // $(\"#LBLaporandetailpemeriksaanpenunjangV_pegawai_id\").val(\"$data->pegawai_id\");
                                            $(\"#LBLaporandetailpemeriksaanpenunjangV_nama_pegawai\").val(\"$data->gelardepan"." "."$data->nama_pegawai".", "."$data->gelarbelakang_nama\");
                                            $(\"#LBLaporandetailpemeriksaanpenunjangV_namapegawai\").val(\"$data->nama_pegawai\");

                                        "))',
                    ),
                    'gelardepan',
                    array(
                        'name'=>'nama_pegawai',
                        'header'=>'Nama Dokter',
                    ),
                    'gelarbelakang_nama',
                    'jeniskelamin',
                    'agama',
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));

$this->endWidget();
////======= end pendaftaran dialog =============
?>

<?php 
//========= Dialog buat cari data dokter =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPerujuk',
    'options'=>array(
        'title'=>'Pencarian Data Perujuk',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
    $perujuk = new RujukandariM('search');
    if (isset($_GET['RujukandariM'])){
        $perujuk->attributes = $_GET['RujukandariM'];
    }

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'perujuk-t-grid',
            'dataProvider'=>$perujuk->search(),
            'filter'=>$perujuk,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPendaftaran",
                                        "onClick" => "
                                            $(\"#dialogPerujuk\").dialog(\"close\");
                                            $(\"#LBLaporandetailpemeriksaanpenunjangV_rujukan_id\").val(\"$data->rujukandari_id\");
                                            $(\"#LBLaporandetailpemeriksaanpenunjangV_nama_perujuk\").val(\"$data->namaperujuk\");

                                        "))',
                    ),
                    'namaperujuk',
                    'spesialis',
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));

$this->endWidget();
////======= end pendaftaran dialog =============
?>