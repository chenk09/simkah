<?php 
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');     
    }
    if($_REQUEST['filter_tab'] == "rekap"){
        echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array(
        'judulLaporan'=>$judulLaporan, 'periode'=>$periode, 'colspan'=>10));  
    }else if($_REQUEST['filter_tab'] == "detail"){
        echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array(
        'judulLaporan'=>$judulLaporan, 'periode'=>$periodeDetail, 'colspan'=>10));  
    }

    if ($caraPrint != 'GRAFIK')
    $this->renderPartial('laboratorium.views.laporan.pemeriksaanPenunjang/_tableRekap', array('model'=>$model,'modDetail'=>$modDetail, 'caraPrint'=>$caraPrint)); 

    if ($caraPrint == 'GRAFIK')
    echo $this->renderPartial('laboratorium.views.laporan._grafik', array('model'=>$model,'data'=>$data, 'caraPrint'=>$caraPrint), true); 
    
?>