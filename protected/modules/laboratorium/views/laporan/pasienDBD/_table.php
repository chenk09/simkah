<div id="div_rekap">
    <legend class="rim">Pasien DBD - Rekap</legend>
        <?php
            $this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',
                array(
                    'id'=>'tableRekapPasienDBD',
                    'dataProvider'=>$model->searchPasienDBD(),
                    'template'=>"{pager}{summary}\n{items}",
                    'enableSorting'=>true,
                    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                    'mergeHeaders'=>array(
                        array(
                            'name'=>'<center>HASIL(&#x2714;) </center>',
                            'start'=>5, //indeks kolom 3
                            'end'=>6, //indeks kolom 4
                        ),
                    ),
                    'columns'=>array(
                        array(
                            'header' => '<center>NO</center>',
                            'type'=>'raw',
                            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                            'htmlOptions'=>array('style'=>'text-align:center'),
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>NAMA</center>',
                           'type'=>'raw',
                           'value'=>'$data->nama_pasien',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>ALAMAT</center>',
                           'type'=>'raw',
                           'value'=>'($data->rt || $data->rw) ? $data->alamat_pasien." RT ".$data->rt." / ".$data->rw : $data->alamat_pasien." "',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>UMUR / <br/> SEX</center>',
                           'type'=>'raw',
                           'value'=>'$data->umur." /<br/>".$data->jeniskelamin',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>TGL MASUK</center>',
                           'type'=>'raw',
                           'value'=>'$data->tgl_pendaftaran',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>IGG</center>',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("pasienDBD/_igg",array(pasienmasukpenunjang_id=>$data->pasienmasukpenunjang_id,pendaftaran_id=>$data->pendaftaran_id),true)',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>IGM</center>',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("pasienDBD/_igm",array(pasienmasukpenunjang_id=>$data->pasienmasukpenunjang_id,pendaftaran_id=>$data->pendaftaran_id),true)',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>KETERANGAN</center>',
                           'type'=>'raw',
                           'value'=>'""',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                    ),
                    'afterAjaxUpdate'=>'function(id, data){
                        jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                        $(".currency").each(function(){
                            $(this).text(
                                formatUang($(this).text())
                            );
                        });
                    }',                    
                )
            );
        ?>
</div>

<div id="div_detail">
    <legend class="rim">Pasien DBD - Detail</legend>
        <?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',array(
            'id'=>'rincianPasienDBD',
            'dataProvider'=>$model->searchPasienDBD(),
            'template'=>"{pager}{summary}\n{items}",
            'enableSorting'=>true,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'mergeHeaders'=>array(
                array(
                    'name'=>'<center>HASIL(&#x2714;) </center>',
                    'start'=>9, //indeks kolom 3
                    'end'=>10, //indeks kolom 4
                ),
            ),
            'columns'=>array(
                array(
                    'header' => '<center>NO</center>',
                    'type'=>'raw',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    'htmlOptions'=>array('style'=>'text-align:center'),
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
               array(
                   'header'=>'<center>NO RM</center>',
                   'type'=>'raw',
                   'value'=>'$data->no_rekam_medik',
                   'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
               array(
                   'header'=>'<center>NO LAB</center>',
                   'type'=>'raw',
                   'value'=>'$data->no_masukpenunjang',
                   'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
               array(
                   'header'=>'<center>NAMA</center>',
                   'type'=>'raw',
                   'value'=>'$data->nama_pasien',
                   'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
               array(
                   'header'=>'<center>ALAMAT</center>',
                   'type'=>'raw',
                   'value'=>'($data->rt || $data->rw) ? $data->alamat_pasien." RT ".$data->rt." / ".$data->rw : $data->alamat_pasien." "',
                   'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
               array(
                   'header'=>'<center>KOTA</center>',
                   'type'=>'raw',
                   'value'=>'$data->kabupaten_nama',
                   'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
               array(
                   'header'=>'<center>PROPINSI</center>',
                   'type'=>'raw',
                   'value'=>'$data->propinsi_nama',
                   'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
               array(
                   'header'=>'<center>UMUR / <br/> SEX</center>',
                   'type'=>'raw',
                   'value'=>'$data->umur." /<br/>".$data->jeniskelamin',
                   'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
               array(
                   'header'=>'<center>TGL MASUK</center>',
                   'type'=>'raw',
                   'value'=>'$data->tgl_pendaftaran',
                   'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
              array(
                   'header'=>'<center>IGG</center>',
                   'type'=>'raw',
                   'value'=>'$this->grid->owner->renderPartial("pasienDBD/_igg",array(pasienmasukpenunjang_id=>$data->pasienmasukpenunjang_id,pendaftaran_id=>$data->pendaftaran_id),true)',
                  'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
              ),
              array(
                   'header'=>'<center>IGM</center>',
                   'type'=>'raw',
                   'value'=>'$this->grid->owner->renderPartial("pasienDBD/_igm",array(pasienmasukpenunjang_id=>$data->pasienmasukpenunjang_id,pendaftaran_id=>$data->pendaftaran_id),true)',
                  'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
              ),
              array(
                   'header'=>'<center>KETERANGAN</center>',
                   'type'=>'raw',
                   'value'=>'""',
                  'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
               ),
            ),
        )); ?> 
</div>
