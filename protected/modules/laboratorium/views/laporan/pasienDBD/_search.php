<div class="search-form" style="">
    <?php
        $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
            'type' => 'horizontal',
            'id' => 'searchLaporan',
            'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
        ));
    ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
        .nav-tabs > .active a:hover{cursor:pointer;}
        .checkbox{
            width:150px;
            float:left;
        }
        
    </style>
    
    <fieldset>
        <legend class="rim"><i class="icon-search"></i> Pencarian Berdasarkan : </legend>
        <table border="1" cellpadding="0" celspacing="0">
            <tr>
                <td>
                   <div class="control-group ">
                        <?php 
                           echo CHtml::label('Periode Masuk Penunjang ','Periode Masuk Penunjang ', array('class'=>'control-label'));
                            echo CHtml::hiddenField('filter_tab', 'pemeriksaan', array('readonly'=>true));
                            echo CHtml::hiddenField('type', '');
                        ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model,'bulan',
                                array(
                                    'hari'=>'Hari Ini',
                                    'bulan'=>'Bulan',
                                    'tahun'=>'Tahun',
                                ),
                                array(
                                    'id'=>'PeriodeName',
                                    'onChange'=>'setPeriode()',
                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                    'style'=>'width:120px;float:left',
                                )
                                );
                            ?>
                            <div style="margin-left:5px;float:left;">
                                <?php
                                    $this->widget('MyDateTimePicker', array(
                                        'model' => $model,
                                        'attribute' => 'tglAwal',
                                        'mode' => 'datetime',
                                        'options' => array(
                                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array(
                                            'readonly' => true,
                                            'class'=>'dtPicker3',
                                            'onclick'=>'checkPilihan(event)',
                                            'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        ),
                                    ));
                                ?>
                            </div>
                            <div style="margin-left:5px;float:left;">
                                <?php
                                    $this->widget('MyDateTimePicker',
                                        array(
                                            'model'=>$model,
                                            'attribute'=>'tglAkhir',
                                            'mode'=>'datetime',
                                            'options'=>array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            ),
                                            'htmlOptions'=>array(
                                                'readonly'=>true,
                                                'class'=>'dtPicker3',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'style'=>''
                                            ),
                                        )
                                    );
                                ?>
                            </div>
                    </div>
                </td>
            </tr>
        </table>
    </fieldset>
    
    <div class="form-actions">
        <?php
                echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), 
                        array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                $this->createUrl('laporan/LaporanPasienDBD'), array('class'=>'btn btn-danger','onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>  
</div>    
<?php $this->endWidget(); ?>
<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT

function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
        $.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
            $('#LKLaporanpasienpenunjangV_tglAwal').val(data.periodeawal);
            $('#LKLaporanpasienpenunjangV_tglAkhir').val(data.periodeakhir);
        },'json');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
    function cek_all_penjamin(obj){
        if($(obj).is(':checked')){
            $("#penjamin").find("input[type=\'checkbox\']").attr("checked", "checked");
        }else{
            $("#penjamin").find("input[type=\'checkbox\']").attr("checked", false);
        }
    }    
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
</script>