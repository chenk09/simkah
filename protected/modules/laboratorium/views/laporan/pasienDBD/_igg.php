<?php
$format = new CustomFormat();
    if(!empty($pendaftaran_id) && !empty($pasienmasukpenunjang_id)){
        $criteria = new CDbCriteria();
        
        $criteria->compare('pendaftaran_id',$pendaftaran_id);
        $criteria->compare('pasienmasukpenunjang_id',$pasienmasukpenunjang_id);
    }else{
         $criteria->compare('ruangna_id',$this->ruangan_id);
    }
    
    $tindakanPelayanan = TindakanpelayananT::model()->findAll($criteria);
    
    if(count($tindakanPelayanan)>0)
    {   
        foreach($tindakanPelayanan as $i=>$tindakan)
        {
            $criteria2 = new CDbCriteria();
            
            $criteria2->compare('tindakanpelayanan_id',$tindakan->tindakanpelayanan_id);
            $criteria2->addCondition('pemeriksaanlab_id = 418');
            
//            if(count($criteria2->addCondition("hasilpemeriksaan ILIKE '%Reaktif%'"))>0){
//                $data = strtolower('Reaktif');
//            }else if(count($criteria2->addCondition("hasilpemeriksaan ILIKE '%Non Reaktif%'"))>0){
//                $data = strtolower('Non Reaktif');
//            }            
            
            $DetailHasil = DetailhasilpemeriksaanlabT::model()->findAll($criteria2);
            
            if(count($DetailHasil) > 0){
                foreach($DetailHasil as $key=>$detail){
                    $hasilpemeriksaan = strtolower($detail->hasilpemeriksaan);
                    if($hasilpemeriksaan == strtolower('Non Reaktif')){
                        $status = false;
                        $hasilpemeriksaan = $hasilpemeriksaan;
                    }else if($hasilpemeriksaan == strtolower('Reaktif')){
                        $status = true;
                        $hasilpemeriksaan = $hasilpemeriksaan;
                    }else{
                        $status = false;
                        $hasilpemeriksaan = $hasilpemeriksaan;
                    }
                }
            }
        }
        
        if($status == false){
            echo "<center>&#45;</center>";
        }else{
//            echo "<center><i class=icon-ok icon-black></i></center>";
			echo "<center>&#x2714;</center>"; // pakai Unicode karakter checklist agar di print pdf bisa terbaca
        }
    }
    else
    {
            echo "<center>&#45;</center>";
    }    
?>