<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrintLaporan();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchTableLaporan();
         $template = "{pager}{summary}\n{items}";
    }
?>


<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari cari', "
$('#searchLaporan').submit(function(){
        $('#tablePendapatanRS').addClass('srbacLoading');
        $('#tablePendapatanLuar').addClass('srbacLoading');
    $.fn.yiiGridView.update('tablePendapatanRS', {
        data: $(this).serialize()
    });
    $.fn.yiiGridView.update('tablePendapatanLuar', {
        data: $(this).serialize()
    });
    return false;
});
");

?>

<div id="div_rs">
    <?php if(isset($caraPrint)){ 
        
    }else{ ?>
            <legend class="rim">Tabel Pendapatan Ruangan - dari RS</legend>
    <?php } ?>
    <?php $this->widget($table,array(
	'id'=>'tablePendapatanRS',
	'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header' => 'No',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:center;'),
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                ),
                array(
                    'header'=>'No Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->no_pendaftaran',
                    'headerHtmlOptions'=>array('style'=>'text-align:center'),
                ),
                array(
                    'header'=>'No Rekam Medik',
                    'type'=>'raw',
                    'value'=>'$data->no_rekam_medik',
                    'headerHtmlOptions'=>array('style'=>'text-align:center'),
                ),
                array(
                    'header'=>'Nama',
                    'type'=>'raw',
                    'value'=>'$data->nama_pasien',
                    'headerHtmlOptions'=>array('style'=>'text-align:center'),
                ),
                array(
                    'header'=>'Cara Bayar / Penjamin',
                    'type'=>'raw',
                    'value'=>'$data->carabayar_nama." / ".$data->penjamin_nama',
                    'headerHtmlOptions'=>array('style'=>'text-align:center'),
                ),
                array(
                    'header'=>'Deposit',
                    'type'=>'raw',
                    'value'=>'(empty($data->jumlahuangmuka) ? "0" : $data->jumlahuangmuka)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array('colspan'=>6,'style'=>'text-align:right;font-style:italic;'),
                    'footer'=>'Jumlah Total',
                ),
                array(
                    'header'=>'Pend. Seharusnya',
                    'type'=>'raw',
                    'name'=>'pend_seharusnya',
                    'value'=>'MyFunction::formatNumber($data->pend_seharusnya)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                    'footer'=>'sum(pend_seharusnya)',
                ),
                array(
                    'header'=>'Pend. Sebenarnya',
                    'type'=>'raw',
                    'name'=>'pend_sebenarnya',
                    'value'=>'MyFunction::formatNumber($data->pend_sebenarnya)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                    'footer'=>'sum(pend_sebenarnya)',
                ),
                array(
                    'header'=>'Sisa',
                    'type'=>'raw',
                    'name'=>'sisa',
                    'value'=>'MyFunction::formatNumber($data->sisa)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                    'footer'=>'sum(sisa)',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
</div>

<div id="div_luar">
    <?php if(isset($caraPrint)){ 
            $dataLuar = $model->searchPrintPendapatanLuar();
    }else{ 
        $dataLuar = $model->searchPendapatanLuar();
   ?>
            <legend class="rim">Tabel Pendapatan Ruangan - dari Luar RS</legend>
    <?php } ?>
    <?php $this->widget($table,array(
            'id'=>'tablePendapatanLuar',
            'dataProvider'=>$dataLuar,
            'template'=>$template,
            'enableSorting'=>$sort,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header' => 'No',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                    ),
                    array(
                        'header'=>'No Lab',
                        'type'=>'raw',
                        'value'=>'$data->no_pendaftaran',
                        'headerHtmlOptions'=>array('style'=>'text-align:center'),
                    ),
                    array(
                        'header'=>'Nama',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien',
                        'headerHtmlOptions'=>array('style'=>'text-align:center'),
                    ),
                    array(
                        'header'=>'Cara Bayar / Penjamin',
                        'type'=>'raw',
                        'value'=>'$data->carabayar_nama." / ".$data->penjamin_nama',
                        'headerHtmlOptions'=>array('style'=>'text-align:center'),
                    ),
                    array(
                        'header'=>'Deposit',
                        'type'=>'raw',
                        'value'=>'(empty($data->jumlahuangmuka) ? "0" : $data->jumlahuangmuka)',
                        'headerHtmlOptions'=>array('style'=>'text-align:center'),
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('colspan'=>5,'style'=>'text-align:right;font-style:italic;'),
                        'footer'=>'Jumlah Total',
                    ),
                    array(
                        'header'=>'Pend. Seharusnya',
                        'type'=>'raw',
                        'name'=>'pend_seharusnya',
                        'value'=>'MyFunction::formatNumber($data->pend_seharusnya)',
                        'headerHtmlOptions'=>array('style'=>'text-align:center'),
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                        'footer'=>'sum(pend_seharusnya)',
                    ),
                    array(
                        'header'=>'Pend. Sebenarnya',
                        'type'=>'raw',
                        'name'=>'pend_sebenarnya',
                        'value'=>'MyFunction::formatNumber($data->pend_sebenarnya)',
                        'headerHtmlOptions'=>array('style'=>'text-align:center'),
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                        'footer'=>'sum(pend_sebenarnya)',
                    ),
                    array(
                        'header'=>'Sisa',
                        'type'=>'raw',
                        'name'=>'sisa',
                        'value'=>'MyFunction::formatNumber($data->sisa)',
                        'headerHtmlOptions'=>array('style'=>'text-align:center'),
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                        'footer'=>'sum(sisa)',
                    ),
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); ?>
</div>
