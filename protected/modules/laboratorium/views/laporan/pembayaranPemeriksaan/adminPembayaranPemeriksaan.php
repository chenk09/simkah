<?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js');
    $url = Yii::app()->createUrl('laboratorium/laporan/frameGrafikPembayaranPemeriksaan&id=1');
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('#searchLaporan').submit(function(){
        $('#Grafik').attr('src','').css('height','0px');
        $.fn.yiiGridView.update('tableLaporanPembayaranPemeriksaan', {
                data: $(this).serialize()
        });
        return false;
    });
    ");
?>

<legend class="rim2">Laporan Pembayaran Pemeriksaan</legend>
<div class="search-form">
    <?php $this->renderPartial('laboratorium.views.laporan.pembayaranPemeriksaan/_searchPembayaranPemeriksaan',
            array('model'=>$model)); ?>
</div>
<fieldset> 
    <?php $this->renderPartial('laboratorium.views.laporan.pembayaranPemeriksaan/_tablePembayaranPemeriksaan', array('model'=>$model)); ?>
    <?php //$this->renderPartial('laboratorium.views.laporan._tab'); ?>
   
    <iframe src="" id="Grafik" width="100%" height='0' onload="javascript:resizeIframe(this);">
    </iframe>       
</fieldset>
<?php   
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanPembayaranPemeriksaan');
    $this->renderPartial('laboratorium.views.laporan._footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>

