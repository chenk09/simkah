<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = true;
    $pagination = 10;
    if (isset($caraPrint)){
        $data = $model->searchPrintLaporanPembayaranPemeriksaan();
        $template = "{items}";
        $sort = false;
        $pagination = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchLaporanPembayaranPemeriksaan();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php
$this->widget($table,
    array(
        'id'=>'tableLaporanPembayaranPemeriksaan',
        'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
      'columns'=>array(
        array(
            'header' => 'No',
            'type'=>'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            'htmlOptions'=>array('style'=>'text-align:center'),
            'footerHtmlOptions'=>array('colspan'=>5,'style'=>'text-align:right;font-style:italic;'),
            'footer'=>'Total',
        ),
        array(
            'header' => 'No Pendaftaran',
            'name'=>'no_pendaftaran',
        ),
        array(
            'header'=>'Tgl Pendaftaran',
            'name'=>'tgl_pendaftaran',
            'value'=>'($data[tgl_pendaftaran]!=="None") ? date_format(new DateTime($data["tgl_pendaftaran"]), "Y-m-d") : "None"',

            //  'value'=>'$format->formatDateMediumForDB($data["tgl_pendaftaran"])',



        ),
        array(
            'header'=>'Nama Pasien',
            'name'=>'nama_pasien',
        ),
        array(
            'header'=>'Alamat',
            'name'=>'alamat_pasien',
        ),
        array(
            'header'=>'Harga',
            'type'=>'raw',
            'name'=>'total',
            'value'=>'MyFunction::formatNumber($data->total)',
            'htmlOptions'=>array(
                    'style'=>'text-align:right',
                    'class'=>'currency'
                ),
            'footerHtmlOptions'=>array('style'=>'text-align:right;font-style:italic;','class'=>'currency'),
            'footer'=>'sum(total)',
        ),
        array(
            'header'=>'Adm',
            'type'=>'raw',
            'name'=>'administrasi',
            'value'=>'MyFunction::formatNumber($data->administrasi)',
            'htmlOptions'=>array(
                    'style'=>'text-align:right',
                    'class'=>'currency'
                ),
            'footerHtmlOptions'=>array('style'=>'text-align:right;font-style:italic;','class'=>'currency'),
            'footer'=>'sum(administrasi)',
        ),
        array(
            'header'=>'Disc',
            'type'=>'raw',
            'name'=>'totaldiscount',
            'value'=>'MyFunction::formatNumber($data->totaldiscount)',
            'htmlOptions'=>array(
                    'style'=>'text-align:right',
                    'class'=>'currency'
                ),
            'footerHtmlOptions'=>array('style'=>'text-align:right;font-style:italic;','class'=>'currency'),
            'footer'=>'sum(totaldiscount)',
        ),
        array(
            'header'=>'Deposit',
            'type'=>'raw',
            'name'=>'jumlahuangmuka',
            'value'=>'MyFunction::formatNumber($data->jumlahuangmuka)',
            'htmlOptions'=>array(
                    'style'=>'text-align:right',
                    'class'=>'currency'
                ),
            'footerHtmlOptions'=>array('style'=>'text-align:right;font-style:italic;','class'=>'currency'),
            'footer'=>'sum(jumlahuangmuka)',
        ),

        array(
            'header'=>'Bayar',
            'type'=>'raw',
            'name'=>'totalbayartindakan',
            'value'=>'MyFunction::formatNumber($data->totalbayartindakan)',
            'htmlOptions'=>array(
                    'style'=>'text-align:right',
                    'class'=>'currency'
                ),
            'footerHtmlOptions'=>array('style'=>'text-align:right;font-style:italic;','class=>currency'),
            'footer'=>'sum(totalbayartindakan)',
        ),
        array(
            'header'=>'Sisa',
            'type'=>'raw',
            'name'=>'totalsisatagihan',
            'value'=>'MyFunction::formatNumber($data->totalsisatagihan)',
            'htmlOptions'=>array(
                    'style'=>'text-align:right',
                    'class'=>'currency'
                ),
            'footerHtmlOptions'=>array('style'=>'text-align:right;font-style:italic;','class=>currency'),
            'footer'=>'sum(totalsisatagihan)',
        ),
        array(
            'header'=>'Kasir',
            'type'=>'raw',
            'name'=>'nama_pegawai',
            'footerHtmlOptions'=>array('style'=>'text-align:right;font-style:italic;color:white;'),
            'footer'=>'-',
        ),
       
    ),
        'afterAjaxUpdate'=>'function(id, data)
        {
            var paging = $("#tableLaporanPembayaranPemeriksaan table").find("input[name=\'paging\']").val();
            if(typeof paging == \'undefined\')
            {
                paging = 0;
            }
            paging = parseInt(paging) + 1;
            $(".number_page").val(paging);
            
            $("#tableLaporanPembayaranPemeriksaan").parent().find("li").each(
                function()
                {
                    if($(this).attr("class") == "active")
                    {
                        setType(this);
                    }
                }
            );
            
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            $(".currency").each(function(){
                $(this).text(
                    formatUang($(this).text())
                );
            });
        }',
    )
);
?> 