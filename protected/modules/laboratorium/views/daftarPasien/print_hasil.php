<?php
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');     
    }else if($caraPrint=='PRINT')
    {
?>
        <style>
            th {
                border: 1px solid;        
                background-color: transparent;
                padding: 5px;
            }
            .grid td{
                border: 1px solid;
                background-color: transparent;
                padding: 5px;
            }
            th{
                text-align: center;
                font-size: 14px;
            }
            table{
                width: 100%;
            }
        </style>
        <div style="height:3cm;">&nbsp;</div>
<?php
    }else{
?>
        <style>
            th {
                border: 1px solid #000;
                background-color: transparent;
                border-collapse: collapse;
            }
            .grid td{
                border: 1px solid #000;
                border-collapse: collapse;
                padding: 5px;
                border-collapse: collapse;
            }
            .grid_td td{
                border-collapse: collapse;
                font-size: 11px;
                border-collapse: collapse;
            }   
            .grid_top td{
                border-top: 1px;
                border-left: 1px;
                border-style: solid;
                font-size: 10pt;
                border-collapse: collapse;
            }
            .right{
                border-top: 1px;
                border-right: 1px;
                border-style: solid;
                border-collapse: collapse;
            }
            .grid_top_buttom td{
                border-top: 1px;
                border-bottom: 1px;
                border-left: 1px;
                border-style: solid;  
                font-size: 10pt;
                border-collapse: collapse;
            }
            
            th{
                text-align: center;
                font-size: 14px;
                border-collapse: collapse;
            }
            table{
                /*font-family: tahoma;*/
                width: 100%;
                border-collapse: collapse;
            }
            table, tr, td{
                border-collapse: collapse;
                font-family: sans-serif;
            }
        </style>        
<?php
    }
?>
<table width="100%" border="0" class="grid_td" cellpadding="0" cellspacing="0">
    <tr class="grid_top"><?php $format=new CustomFormat(); $tgl = explode(" ",$modHasilPeriksa->update_time); $tgl_periksa = $format->formatDateMediumForDB($tgl[0]." ".$tgl[1]." ".$tgl[2]);?>
        <td width="50%" style="border:none;"><center><?php echo "Tasikmalaya, ".$format->formatDateINA(date($tgl_periksa)); ?></center></td>
        <td width="15%" style="border:none;">Penanggungjawab</td>
        <td width="35%" style="border:none;">: <?php echo $pemeriksa->gelardepan." ".$pemeriksa->nama_pegawai.", ".$pemeriksa->gelarbelakang->gelarbelakang_nama; ?></td>
    </tr>
    <tr class="grid_top"> 
        <td style="border:none;"></td>
        <td style="border:none;">Izin</td>
        <!--<td style="border:none;">: YM.01.05/8/455/IV.46/DKK/2008</td>-->
        <?php 
            /**
             * modul laboratorium/daftarPasien/Details
             * @author  : Miranitha Fasha
             * date     : 07-May-2014 modify 12 May 2014
             * issue    : EHJ-1801 / EHJ-1844
             * desc     : Rubah Izin Praktek Dokter Laboratorium
             */
        ?>
        <td style="border:none;">: 440/00157/LAB-KLINIK/BPPT/I/2014</td>
    </tr>
</table>
<br>
<table width="100%" border="0" class="grid_td" cellpadding="0" cellspacing="0">
    <tr class="grid_top">
        <td width="12%">No. Lab</td>
        <td width="45%" style="border-left-style: 0px;">: <?php echo $masukpenunjang->no_pendaftaran; ?></td>
        <td width="14%">No. Pemeriksaan</td>
        <td class="right" style="border-left-style: 0px;">: <?php echo $masukpenunjang->no_masukpenunjang; ?></td>
    </tr>
    <tr class="grid_top">
        <td style="white-space: nowrap;">Nama</td>
        <td style="border-left-style: 0px;">: <?php echo $modHasilPeriksa->namadepan." ".$modHasilPeriksa->nama_pasien; ?></td>
        <td width="10%">Dokter</td>
        <td class="right" style="border-left-style: 0px;">: <?php echo $masukpenunjang->namaperujuk; ?></td>
    </tr>
    <tr class="grid_top">
        <td>Umur </td>
        <td style="border-left-style: 0px;">: <?php echo $modHasilPeriksa->umur."; ".$modHasilPeriksa->jeniskelamin; ?></td>
        <td>Alamat</td>
        <td class="right" style="border-left-style: 0px;">: <?php echo $masukpenunjang->alamatlengkapperujuk; ?></td>
    </tr>
    <tr class="grid_top_buttom">
        <td>Alamat</td>
        <td style="border-left-style: 0px;">: <?php echo $modHasilPeriksa->alamat_pasien ?></td>
        <td>No. Telp</td>
        <td class="right" style="border-left-style: 0px;">: <?php echo $masukpenunjang->notelpperujuk; ?></td>
    </tr>
</table>
<div style="font-family:arial;font-size:11pt;">
    <b>
    <?php
        $modRuangan = RuanganM::model()->findByPk($masukpenunjang->ruanganasal_id);
        $modKamarruangan = KamarruanganM::model()->findByPk($modRuangan->ruangan_id);
        $modPendaftaran = PendaftaranT::model()->findByPk($masukpenunjang->pendaftaran_id);
        $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
        if(count($modPasienAdmisi) > 0){
            $modKelasPelayanan = KelaspelayananM::model()->findByPk($modPasienAdmisi->kelaspelayanan_id);
            $masukpenunjang->kelaspelayanan_nama = $modKelasPelayanan->kelaspelayanan_nama;
        }
        if($modRuangan->instalasi->instalasi_id == 4){
            $modInfoPasien = InfopasienmasukkamarV::model()->findByAttributes(array(
                'pendaftaran_id'=>$masukpenunjang->pendaftaran_id,
            ));
            echo $masukpenunjang->no_rekam_medik . '/' . $modInfoPasien->instalasi_nama . '/'. $modInfoPasien->ruangan_nama . '/' . $modInfoPasien->kamarruangan_nokamar . '/' .$modInfoPasien->kelaspelayanan_nama;
        }else{
            echo $masukpenunjang->no_rekam_medik . '/' . $masukpenunjang->ruanganasal_nama . '/' . $masukpenunjang->kelaspelayanan_nama;
        }
    ?>
    </b>
</div>
<br>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <div style="font-family:arial;font-size:16pt;text-align: center;">
                <b>
                    HASIL PEMERIKSAAN LABORATORIUM
                </b>
            </div>
        </td>
    </tr>
</table>
<br>
<div style="clear:both;border:none;">
    <?php
        $menu = array(
            '4'=>'URIN LENGKAP',
            '64'=>'TEST KEHAMILAN',
            '65'=>'URIN KHUSUS',
            '6'=>'FEACES LENGKAP',
            '66'=>'FEACES KHUSUS',
            '24'=>'Hematologi',
            '53'=>'KARBOHIDRAT',
            '55'=>'LEMAK JANTUNG',
            '57'=>'IMUNO-SEROLOGI',
            '1'=>'Serologi',
            '69'=>'ANALISA SPERMA',
//            '70'=>'ANALISA BATU GINJAL',
            '51'=>'HUMADRUG',
            '52'=>'LAIN - LAIN',
            '5'=>'Mikrobiologi',
        );
        foreach($data as $jenisperiksa => $kelompok)
        {
            if(array_key_exists($jenisperiksa, $menu))
            {
                $this->renderPartial(
                    'template/__hasilPemeriksaan_' . $jenisperiksa,
                    array(
                        'params'=>$kelompok['grid'],
                        'jenisperiksa'=>$kelompok['tittle']
                    )
                );
            }else{
                $this->renderPartial(
                    'template/__hasilPemeriksaan',
                    array(
                        'params'=>$kelompok['grid'],
                        'jenisperiksa'=>$kelompok['tittle']
                    )
                );
            }
        }
    ?>
</div>
<br/>
<?php
    if(count($data_rad))
    {
        echo('
            <table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td colspan="3" align="center">
                            <div style="text-align: center;font-size:14pt;">
                                <b>RONTGEN DIAGNOSTIK</b>
                            </div>                
                        </td>
                    </tr>
                    <tr>
                        <th width="3%">No.</th>
                        <th width="25%">Pemeriksaan</th>
                        <th>Hasil</th>
                    </tr>
                </thead>
                <tbody>
        ');
        $i = 0;
        foreach($data_rad as $val)
        {
            echo('<tr>');
            echo('<td valign="top">'. ($i+1) .'</td>');
            echo('<td valign="top">'. $val['pemeriksaan'] .'</td>');
            echo('<td valign="top">'. $val['hasil'] .'</td>');
            echo('</tr>');
            $i++;
        }
        echo('</tbody></table><br>');
    }
?>
<div>
<?php 
$this->renderPartial(
    'template/_tandaTangan',
    array(
        'masukpenunjang'=>$masukpenunjang,
    )
);
?>
</div>