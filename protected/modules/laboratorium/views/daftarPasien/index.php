<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php 
    if($_GET['status'] > 0){ // Jika berhasil disimpan
        Yii::app()->user->setFlash('success',"Data pemeriksaan lab berhasil disimpan !");
    }
?>
<?php
//============= PRINT LABEL sebelumnya ==============
// if(isset($_GET['caraPrint'])){
// $idPendaftaran = $_GET['id'];
// $urlPrint=  Yii::app()->createAbsoluteUrl('laboratorium/pendaftaranPasienLuar/print', array('id_pendaftaran'=>$idPendaftaran));
// $js = <<< JSCRIPT
// function printLabel(caraPrint)
// {
//     window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
// }
//     printLabel('PRINT');
// JSCRIPT;
// Yii::app()->clientScript->registerScript('printLabel',$js,CClientScript::POS_HEAD);     
// }
?>

<?php
//============= PRINT LABEL DAN TINDAKAN ==============
if(isset($_GET['caraPrint'])){
$idPendaftaran = $_GET['id'];
$id_pasienpenunjang = $_GET['idPasienPenunjang'];
$labelOnly = 1;
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/pendaftaranPasienLuar/print', array('id_pendaftaran'=>$idPendaftaran,'id_pasienpenunjang'=>$id_pasienpenunjang, 'labelOnly'=>$labelOnly)); 
$urlPrintTindakan=  Yii::app()->createAbsoluteUrl($this->module->id.'/pendaftaranPasienLuar/printTindakan', array('id_pendaftaran'=>$idPendaftaran,'id_pasienpenunjang'=>$id_pasienpenunjang, 'labelOnly'=>$labelOnly)); 
$urlPrintPrint_old_2=  Yii::app()->createAbsoluteUrl($this->module->id.'/pendaftaranPasienLuar/print', array('id_pendaftaran'=>$idPendaftaran,'id_pasienpenunjang'=>$id_pasienpenunjang, 'labelOnly'=>$labelOnly));
$urlUpdateTindakan= Yii::app()->createAbsoluteUrl($this->module->id.'/inputPemeriksaan/update');
$js = <<< JSCRIPT
function printLabel(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=980px');
    window.open("${urlPrintTindakan}&caraPrint="+caraPrint,"",'location=_new, width=980px');
  window.open("${$urlPrintPrint_old_2}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}
    printLabel('PRINT');
JSCRIPT;
    
Yii::app()->clientScript->registerScript('printLabel',$js,  CClientScript::POS_HEAD);
}
?>


<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari cari', "
$('#daftarPasien-form').submit(function(){
        $('#daftarPasien-grid').addClass('srbacLoading');
  $.fn.yiiGridView.update('daftarPasien-grid', {
    data: $(this).serialize()
  });
  return false;
});
");
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?> 
<fieldset>
    <legend class="rim2">Informasi Daftar Pasien</legend>
     
     
<?php $this->widget('bootstrap.widgets.BootAlert');
 $this->widget('ext.bootstrap.widgets.BootGridView',array(
  'id'=>'daftarPasien-grid',
  'dataProvider'=>$modPasienMasukPenunjang->searchLAB(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
  'columns'=>array(
//            'tgl_pendaftaran',
            array(
                'header'=>'Tgl. Masuk Penunjang',
                'name'=>'tglmasukpenunjang',
            ),
            array(
                'header'=>'Tgl. Pendaftaran',
                'name'=>'tgl_pendaftaran',
                // 'value'=>'$data->pendaftaran->tgl_pendaftaran'
            ),
            'ruanganasal_nama',
//            'nama_dokterasal', 
            array(
                'header'=>'Perujuk RS',
                'value'=>'$this->grid->owner->renderPartial("_dokterPerujuk",array(pasienmasukpenunjang_id=>$data->pasienmasukpenunjang_id),true)',
            ),
            array(
                'header'=>'Perujuk Luar',
                'value'=>'$data->nama_perujuk',
            ),
//            'ruangan_nama',
            // array(
            //     // 'name'=>'ruangan_nama',
            //     'header'=>'Ruangan Asal',
            //     'value'=>'$data->ruangan->ruangan_nama'
            // ),
//            'no_pendaftaran',
//            'no_rekam_medik',
            array(
                'name'=>'no_rekam_medik',
                'type'=>'raw',
                'header'=>'No. Pendaftaran /<br>No. RM /<br>No. Pemeriksaan Lab',
                'value'=>'(($data->statusperiksahasil == "BELUM" || $data->statusperiksahasil == "SEDANG") ? CHtml::link("<i class=\"icon-pencil-blue\"></i>$data->no_pendaftaran","",array("onclick"=>"statusPeriksa(\"$data->statusperiksa\",$data->pendaftaran_id,$data->pasienmasukpenunjang_id);return false;","href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Pemeriksaan")) : $data->no_pendaftaran). "<br/>" . $data->no_rekam_medik. "<br/>" . $data->no_masukpenunjang',
//                'value'=>'(($data->statusperiksahasil == "BELUM" || $data->statusperiksahasil == "SEDANG") ? CHtml::link("<i class=\"icon-pencil-blue\"></i>$data->no_pendaftaran",Yii::app()->controller->createUrl("/'.$module.'/inputPemeriksaan/update",array("idPendaftaran"=>$data->pendaftaran_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik Untuk Mengubah Pemeriksaan")) : $data->no_pendaftaran). "\n" . $data->no_rekam_medik',
            ),
            array(
                'header'=>'Nama Pasien / Alias',
                'type'=>'raw',
//                'value'=> '((substr($data->no_rekam_medik,0,-6)) == "LB" || (substr($data->no_rekam_medik,0,-6)) == "RD" ? CHtml::link("<i class=\"icon-pencil\"></i>", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/ubahPasien",array("id"=>"$data->pasien_id")), array("rel"=>"tooltip","title"=>"Klik untuk mengubah data pasien"))." ".CHtml::link($data->nama_pasien.\' / \'.$data->nama_bin, Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/ubahPasien",array("id"=>"$data->pasien_id")), array("rel"=>"tooltip","title"=>"Klik untuk mengubah data pasien")) : $data->nama_pasien.\' / \'.$data->nama_bin )',
                'value'=> '((substr($data->no_rekam_medik,0,-6)) == "LB" || (substr($data->no_rekam_medik,0,-6)) == "RD" ? CHtml::link("<i class=\"icon-pencil-blue\"></i> ".$data->NamaPasienNamaBin, Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/ubahPasien",array("id"=>"$data->pasien_id","idpendaftaran"=>"$data->pendaftaran_id","modulId"=>"'.Yii::app()->session['modulId'].'")), array("rel"=>"tooltip","title"=>"Klik untuk mengubah data pasien")) : $data->NamaPasienNamaBin )',
                ),
//            array(
//                'header'=>'Nama Pasien / Alias',
//                'type'=>'raw',
//                'value'=>'"$data->nama_pasien"." / "."$data->nama_bin"',
//            ),
//            'nama_pasien',
//            'jeniskasuspenyakit_nama',
            array(
                'header'=>'Jenis Kelamin',
                'type'=>'raw',
                'value'=>'$data->jeniskelamin',
            ),
            'umur',
            'golonganumur_nama',
            'alamat_pasien',
           
            array(
                'header'=>'Cara Bayar',
                'name'=>'CaraBayarPenjamin',
                'type'=>'raw',
                'value'=>'$data->caraBayarPenjamin',    
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ), 
            array(
                'header'=>'Dokter Periksa',
                'type'=>'raw',
                'value'=>'($data->statusperiksahasil == "SEDANG") ? CHtml::link("<i class=\"icon-pencil-blue\"></i>". $data->getNamaLengkapDokter($data->pegawai_id),Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/ApprovePemeriksaan",array("idPendaftaran"=>$data->pendaftaran_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik untuk approve pemeriksaan", "onclick"=>"return confirm(\"Apakah anda akan meng-approve pemeriksaan ini?\");")) : $data->getNamaLengkapDokter($data->pegawai_id)',
            ),
            'statusperiksa',
//            array(
//                'header'=>'Status Pemeriksaan',
//                'type'=>'raw',
//                'value'=>'($data->statusperiksahasil == "SEDANG") ? $data->statusperiksahasil." :<br>".CHtml::link("<i class=\"icon-pencil-blue\"></i>". $data->getNamaLengkapDokter($data->pegawai_id),Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/approvePemeriksaan",array("idPendaftaran"=>$data->pendaftaran_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik untuk approve pemeriksaan", "onclick"=>"return confirm(\"Apakah anda akan meng-approve pemeriksaan ini?\");")) : $data->statusperiksahasil." : <br>".$data->getNamaLengkapDokter($data->pegawai_id) ',
//            ),
//            array(
//                'header'=>'Status Print',
//                'type'=>'raw',
//                'value'=>'($data->printhasillab == true) ? "SUDAH" : "BELUM"',
//            ),
             array(
                'name'=>'ambilSample',
                'type'=>'raw',
                'value'=>'($data->statusperiksahasil != "SUDAH") ? CHtml::link("<i class=\"icon-pencil-blue\"></i>",Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/updateSample",array("idPendaftaran"=>$data->pendaftaran_id,"idPengambilanSample"=>$data->pengambilansample_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik Untuk Mengubah Ambil Sample")) : ""',    
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            array(
                'header'=>'Status Periksa Hasil',
                'type'=>'raw',
                // 'value'=>'(empty($data->pasienbatalperiksa_id)) ? $data->statusperiksahasil :  "DIBATALKAN" ',
                'value'=>'($data->statusperiksahasil == "SUDAH") ? CHtml::link("<i class=\"icon-pencil-blue\"></i>". $data->statusperiksahasil,Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/CancelPemeriksaan",array("idPendaftaran"=>$data->pendaftaran_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik untuk membatalkan pemeriksaan", "onclick"=>"return confirm(\"Apakah anda akan membatalkan pemeriksaan ini?\");")) : ((empty($data->pasienbatalperiksa_id)) ? $data->statusperiksahasil : "DIBATALKAN")',
            ),             
             array(
                'name'=>'masukanHasil',
                'type'=>'raw',
                'value'=>'(($data->statusperiksahasil == "SEDANG" || $data->statusperiksahasil == "BELUM") ? CHtml::link("<i class=\"icon-pencil-brown\"></i>",Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/hasilPemeriksaan",array("idPendaftaran"=>$data->pendaftaran_id,"idPasien"=>$data->pasien_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik Untuk Masukan Hasil Pemeriksaan")) : "")',    
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            array(
                'header'=>'Lihat Hasil',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-file-silver\"></i>",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/Details",array("idPendaftaran"=>$data->pendaftaran_id,"idPasien"=>$data->pasien_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id, "popup"=>"true")),
                            array("class"=>"", 
                                  "target"=>"iframeLihatHasil",
                                  "onclick"=>"$(\"#dialogLihatHasil\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat hasil pemeriksaan", 
                            ))','htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            array(
                'header'=>'Print Label',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("pendaftaranPasienLuar/print",array("id_pendaftaran"=>$data->pendaftaran_id,"id_pasienpenunjang"=>$data->pasienmasukpenunjang_id,"labelOnly"=>1,"caraPrint"=>"PRINT")),
                            array("class"=>"", 
                                  "target"=>"iframePrintLabel",
                                  "onclick"=>"$(\"#dialogDetailPrintLabel\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat hasil pemeriksaan", 
                            ))','htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
        array(
                'header'=>'Print Label RM',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("pendaftaranPasienLuar/Print_old_2",array("id_pendaftaran"=>$data->pendaftaran_id,"id_pasienpenunjang"=>$data->pasienmasukpenunjang_id,"labelOnly"=>1,"caraPrint"=>"PRINT")),
                            array("class"=>"", 
                                  "target"=>"iframePrintLabel",
                                  "onclick"=>"$(\"#dialogDetailPrintLabel\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat hasil pemeriksaan", 
                            ))','htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
      
            array(
               'header'=>'Batal Periksa',
               'type'=>'raw',
               'value'=>'($data->statusperiksahasil != "SUDAH") ? CHtml::link("<i class=\'icon-remove\'></i>", "javascript:batalperiksa($data->pasienmasukpenunjang_id,$data->pendaftaran_id)",array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk membatalkan Pemeriksaan")) : null',
               'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
            
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>


<?php 
// Dialog buat lihat penjualan resep =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetailPrintLabel',
    'options'=>array(
        'title'=>'Print Label Pasien Laboratorium',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>false,
    ),
));
?>
<iframe src="" name="iframePrintLabel" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
//========= end lihat penjualan resep dialog =============================
?>


<?php 
// Dialog untuk Lihat Hasil =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogLihatHasil',
    'options'=>array(
        'title'=>'Hasil Pemeriksaan Laboratorium',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>450,
        'resizable'=>true,
    ),
));
?>

<iframe src="" name="iframeLihatHasil" width="100%" height="500">
</iframe>

<?php
$this->endWidget();
//========= end Lihat Hasil =============================
?>
    
<?php
 //CHtml::link($text, $url, $htmlOptions)
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
  'action'=>Yii::app()->createUrl($this->route),
  'method'=>'get',
        'id'=>'daftarPasien-form',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <table  class="table-condensed">
        <tr>
            <td>
              <?php echo $form->labelEx($modPasienMasukPenunjang,'tglmasukpenunjang', array('class'=>'control-label')) ?>
                  <div class="controls">  
                   
                     <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPasienMasukPenunjang,
                                         'attribute'=>'tglAwal',
                                         'mode'=>'datetime',
//                                          'maxDate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                      
                   </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' s/d', array('class'=>'control-label')) ?>

                   <div class="controls">  
                    <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPasienMasukPenunjang,
                                         'attribute'=>'tglAkhir',
                                         'mode'=>'datetime',
//                                         'maxdate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                   </div> 
                <div class="control-group">
                    <label class="control-label">Status Permeriksaan</label>
                    <div class="controls">
                        <?php // echo $form->textField($modPasienMasukPenunjang,'statusperiksahasil',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                        <?php echo $form->dropDownList($modPasienMasukPenunjang,'statusperiksahasil',  CHtml::listData(LookupM::model()->findAllByAttributes(array('lookup_type'=>'statusperiksahasil', 'lookup_aktif'=>true)), 'lookup_value', 'lookup_name'),array('empty'=>'TAMPILKAN SEMUA','class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                    </div>
                </div> 
                <div class="control-group">
                    <label class="control-label">No. Rekam Medik</label>
                    <div class="controls">
                        <?php echo $form->textField($modPasienMasukPenunjang,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                    </div>
                </div>                 
                                
            </td>
            <td>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->hiddenField($modPasienMasukPenunjang,'pendaftaran_id',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->hiddenField($modPasienMasukPenunjang,'pasienmasukpenunjang_id',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
            </td>
            
        </tr>
    </table>
    <div class="form-actions">
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
                   ?>
                       <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
  <?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>

    </div>
</fieldset>  
<?php $this->endWidget();?>
</fieldset>
<script type="text/javascript">

function batalperiksa(idPenunjang,idPendaftaran)
{
   
   var con = confirm('anda yakin akan membatalkan pemeriksaan laboratorium pasien ini? ');
   if(con){
       $.post('<?php echo Yii::app()->createUrl('laboratorium/daftarPasien/BatalPeriksaPasienLuar')?>',{idPendaftaran:idPendaftaran,idPenunjang:idPenunjang},
                 function(data){
                     if(data.status == 'ok' && data.pesan != 'exist'){
                         $('#dialogKonfirm div.divForForm').html(data.keterangan);
                          $('#dialogKonfirm').dialog('open');
                          $.fn.yiiGridView.update('daftarPasien-grid', {
                                data: $(this).serialize()
                        });
                     }else{
                         if(data.pesan == 'exist' && data.status == 'not')
                         {
                             $('#dialogKonfirm div.divForForm').html(data.keterangan);
                             $('#dialogKonfirm').dialog('open');
//                                 alert(data.keterangan);
                         }
                     }
                 },'json'
             );
   }else{
//       alert('tidak');
   }
//    if(alasan==''){
//        alert('Anda Belum Mengisi Alasan Pembatalan');
//    }else{
//        $.post('<?php //echo Yii::app()->createUrl('rawatInap/pasienRawatInap/BatalRawatInap');?>', $('#formAlasan').serialize(), function(data){
////            if(data.error != '')
////                alert(data.error);
////            $('#'+data.cssError).addClass('error');
//            if(data.status=='success'){
//                batal();
//                alert('Data Berhasil Disimpan');
//                location.reload();
//            }else{
//                alert(data.status);
//            }
//        }, 'json');
//   }     
}

function statusPeriksa(statusperiksa,pendaftaran_id,pasienmasukpenunjang_id){
//    alert(status);
    var statusperiksa = statusperiksa;
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = pasienmasukpenunjang_id;
    
    $.post("<?php echo Yii::app()->createUrl('laboratorium/daftarPasien/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id},
        function(data){
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa menambahkan tindakan transaksi. ");
            }else{
                window.location.href= "<?php echo Yii::app()->controller->createUrl('inputPemeriksaan/update',array()); ?>&idPendaftaran="+data.pendaftaran_id+"&idPasienMasukPenunjang="+data.pasienmasukpenunjang_id,"";
            }
    },"json");
    return false; 
}
</script>
<?php 
// Dialog untuk masukkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogKonfirm',
    'options'=>array(
        'title'=>'',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>500,
        'minHeight'=>200,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end masukkamar_t dialog =============================
?>
