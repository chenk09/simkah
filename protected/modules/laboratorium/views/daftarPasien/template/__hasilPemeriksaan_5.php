


<style type="text/css">

/*---------------------------------------*/

.grid_td_nomor {
    width :3.7%;
}

.grid_td_pemeriksaan {
    width :50%;
}

.grid_td_hasil {
    width :16.3%;
}

.grid_td_normal {
    width :15%;
}

</style>

<table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">
    <thead>
        <tr>
            <td colspan="6" align="center" style="border-width: 0px;">
                <div style="text-align: center;font-size:13pt;">
                    <b><?=$jenisperiksa?></b>
                </div>
            </td>
        </tr>       
        <tr style="font-family: arial;font-size: 11pt;">
            <th width="3%">No.</th>
            <th width="25%">Pemeriksaan</th>
            <th width="40%">Hasil</th>
            <th width="15%" colspan="3">Metode</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $idx = 1;
            $kelompok = "";
            $rows = '';
            foreach($params as $val)
            {
                $rows = '';
                if(count($val['pemeriksaan']) > 1)
                {
                    $namapemeriksaan = '';
                    $hasil = '';
                    $satuan = '';
                    $normal = '';
                    $metode = '';
                    
                    //$rows = '';
                        if((round(count($params)/2)) == $idx && $idx > 5){
                            $rows .= '</table><table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">';
                         }

                    foreach($val['pemeriksaan'] as $temp)
                    {

                        if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
                        
                        $namapemeriksaan .= '<div>' . $temp['namapemeriksaan'] .'</div>';
                        $hasil .= '<div>'. $temp['hasil'] .'&nbsp;</div>';
                        $satuan .= '<div>'. $temp['satuan'] .'</div>';
                        $normal .= '<div>'. $temp['normal'] .'</div>';
                        $metode .= '<div>'. $temp['metode'] .'</div>';
                    }
                    $rows .= '<tr>';
                    $rows .= '<td valign="top" colspan="6" style="text-align:left;"><b>'. $val['kelompok'] . '</b></td>';
                    $rows .= '</tr>';
                    $rows .= '<tr>';
                    $rows .= '<td valign="top">'. $idx .'</td>';
                    $rows .= '<td valign="top">'. $namapemeriksaan .'</td>';

                    // PERUBAHAN UNTUK KONDISI TAMPILAN SENSITIF RESISTENSI I DAN II
                    // BY RAFA

                        if($temp['kelompok'] == 'Sensitif Resistensi Test I' || $temp['kelompok'] == 'Sensitif Resistensi Test II'  ){
                            $rows .= '<td colspan="4" valign="top" style="">'. $hasil .'</td>';
                        }else{
                            $rows .= '<td valign="top" style="">'. $hasil .'</td>';
                            $rows .= '<td valign="top" colspan="3">'. $metode .'</td>';
                    //$rows .= '<td valign="top" style="white-space: nowrap; text-align: center;">'. $satuan .'</td>';
                    //$rows .= '<td valign="top" style="white-space: nowrap; text-align: center;">'. $normal .'</td>';
                        }
                    $rows .= '</tr>';                    
                }else{
                    // $row = '';
                    // if((round(count($params)/2)) == $idx && $idx > 4){
                    //     $row .= '</table><table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">';
                    //  }

                    foreach($val['pemeriksaan'] as $temp)
                    {

                        if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
                        
                        $row .= '<tr>';
                        $row .= '<td class="grid_td_nomor" valign="top">'. $idx .'</td>';
                        $row .= '<td class="grid_td_pemeriksaan" valign="top"><div>'. $temp['namapemeriksaan'] .'</div></td>';
                        $row .= '<td class="grid_td_hasil" valign="top" style="">'. $temp['hasil'] .'</td>';
//                        $row .= '<td valign="top" style="white-space: nowrap; text-align: center;">'. $temp['satuan'] .'</td>';
//                        $row .= '<td valign="top"  style="white-space: nowrap; text-align: center;">'. $temp['normal'] .'</td>';
                        $row .= '<td class="grid_td_metode" valign="top">'. $temp['metode'] .'</td>';
                        $row .= '</tr>';
                    }
                    $rows = $row;
                }
                echo($rows);
                $kelompok = $val['kelompok'];
                $idx++;
            }

        ?>
   </tbody>
</table><br>