<style>
    body{
        font-family: tahoma;
        font-size: 11.5px;
    }
    .td{
        font-size: 11.5px;
    }
    table td {
        vertical-align: top;
    }
    .grid td{
        border-top: 0.5px solid;
        border-collapse: collapse;
        /*border:1px solid #000;*/
        font-size: 11.5px;
         padding: 5px;
    }
    .grid th{
        border-collapse: collapse;
        font-size: 12px;
        font-family: tahoma;
        border-collapse: collapse;
        /*border-top: 0.5px solid;*/
    }
    .grid td,th{
        padding: 5px;
    } 
    table{
        width:100%;
    }
</style>
<table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">
    <thead>
        <tr>
            <td colspan="6" align="center" style="border-width: 0px;">
                <div style="text-align: center;font-size:13pt;">
                    <b><?=$jenisperiksa?></b>
                </div>
            </td>
        </tr>
        <tr style="font-family: arial;font-size: 11pt;">
            <th width="3%">No.</th>
            <th width="45%">Pemeriksaan</th>
            <th width="15%">Hasil</th>
            <th width="15%">Satuan</th>
            <th width="15%">Normal</th>
            <th width="15%">Metode</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //ok
            $idx = 1;
            $kelompok_temp = "";
            $rows = '';
            foreach($params as $val)
            {
                $kelompok = $val['kelompok'];
                $rows = '';
                if(count($val['pemeriksaan']) > 1)
                {
                    $namapemeriksaan = '';
                    $hasil = '';
                    $satuan = '';
                    $normal = '';
                    $metode = '';
                     // echo (count($params));
                    
                    foreach($val['pemeriksaan'] as $temp)
                    {
                        if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
                        
                        $namapemeriksaan .= '<div>' . $temp['namapemeriksaan'] .'</div>';
                        $hasil .= '<div>'. $temp['hasil'] .'</div>';
                        $satuan .= '<div>'. $temp['satuan'] .'</div>';
                        $normal .= '<div>'. $temp['normal'] .'</div>';
                        $metode .= '<div>'. $temp['metode'] .'</div>';
                    }
                    
                    if($kelompok == $val['kelompok']){
                        $rows .= '</table><table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">';
                        $rows .= '<tr>';
                        $rows .= '<td valign="top" colspan="6" style="text-align:left;"><b>'. $val['kelompok'] . '</b></td>';
                        $rows .= '</tr>';
                        $rows .= '<tr>';
                        $rows .= '<td valign="top" width="4%">'. $idx .'</td>';
                        $rows .= '<td valign="top" width="45%">'. $namapemeriksaan .'</td>';
                        $rows .= '<td valign="top" width="15%">'. $hasil .'</td>';
                        $rows .= '<td valign="top" width="15%">'. $satuan .'</td>';
                        $rows .= '<td valign="top"  style="white-space: nowrap;" width="15%">'. $normal .'</td>';
                        $rows .= '<td valign="top" width="15%">'. $metode .'</td>';
                        $rows .= '</tr>'; 
                    }
                    else{
                        $rows .= '</table><table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">';
                        $rows .= '<tr>';
                        $rows .= '<td valign="top" width="4%">'. $idx .'</td>';
                        $rows .= '<td valign="top" width="45%">'. $namapemeriksaan .'</td>';
                        $rows .= '<td valign="top" width="15%">'. $hasil .'</td>';
                        $rows .= '<td valign="top" width="15%">'. $satuan .'</td>';
                        $rows .= '<td valign="top"  style="white-space: nowrap;" width="15%">'. $normal .'</td>';
                        $rows .= '<td valign="top" width="15%">'. $metode .'</td>';
                        $rows .= '</tr>'; 
                    }
//                    $rows .= '<tr>';
//                    $rows .= '<td valign="top" colspan="6" style="text-align:left;"><b>'. $val['kelompok'] . '</b></td>';
//                    $rows .= '</tr>';
//                    $rows .= '<tr>';
//                    $rows .= '<td valign="top">'. $idx .'</td>';
//                    $rows .= '<td valign="top">'. $namapemeriksaan .'</td>';
//                    $rows .= '<td valign="top">'. $hasil .'</td>';
//                    $rows .= '<td valign="top">'. $satuan .'</td>';
//                    $rows .= '<td valign="top"  style="white-space: nowrap;">'. $normal .'</td>';
//                    $rows .= '<td valign="top">'. $metode .'</td>';
//                    $rows .= '</tr>'; 
                    // exit();                   
                }else{
                    $row = '';
                    // if((round(count($params)/2)) == $idx){
                    //     $row .= '</table><table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">';
                    //  }
                    foreach($val['pemeriksaan'] as $temp)
                    {
                        if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
                        
//                        $rows .= '<table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">';
                        $row .= '<tr>';
                        $row .= '<td valign="top" width="4%">'. $idx .'</td>';
                        $row .= '<td valign="top" width="45%"><div>'. $temp['namapemeriksaan'].'</div></td>';
                        $row .= '<td valign="top" width="15%">'. $temp['hasil'] .'</td>';
                        $row .= '<td valign="top" width="15%">'. $temp['satuan'] .'</td>';
                        $row .= '<td valign="top" width="15%">'. $temp['normal'] .'</td>';
                        $row .= '<td valign="top" width="15%">'. $temp['metode'] .'</td>';
                        $row .= '</tr>';
                    }
                    $rows = $row;
                }
                echo($rows);
                $kelompok_temp = $kelompok;
                $idx++;
            }
//            exit;
        ?>
   </tbody>
</table><br>