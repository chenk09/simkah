
<style type="text/css">

/*---------------------------------------*/

.grid_td_nomor {
    width :3.7%;
}

.grid_td_pemeriksaan {
    width :22%;
}

.grid_td_hasil {
    width :16.3%;
}

.grid_td_satuan {
    width :15%;
}

.grid_td_normal {
    width :15%;
}

.grid_td_metode {
    width :15%;
}

/*---------------------------------------*/
/*.grid_td_nomor2 {
    width :3.9%;
}

.grid_td_pemeriksaan2 {
    width :23%;
}

.grid_td_n_pemeriksaan2 {
    width :10%;
}

.grid_td_hasil2 {
    width :16%;
}

.grid_td_satuan2 {
    width :15%;
}

.grid_td_normal2 {
    width :15%;
}

.grid_td_metode2 {
    width : 14%;
}*/

/*---------------------------------------*/

</style>


<table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">
    <thead>
        <tr>
            <td colspan="7" align="center" style="border-width: 0px;">
                <div style="text-align: center;font-size:13pt;">
                    <b><?=$jenisperiksa?></b>
                </div>
            </td>
        </tr>
        <tr style="font-family: arial;font-size: 11pt;">
            <th width="3%">No.</th>
            <th width="25%" colspan="2">Pemeriksaan</th>
            <th width="15%">Hasil</th>
            <th width="15%">Satuan</th>
            <th width="15%" style="text-align: center;" >Normal</th>
            <th width="15%">Metode</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $idx = 1;
            $kelompok = "";
            $rows = '';

            foreach($params as $val)
            {
                $rows = '';       

                if(count($val['pemeriksaan']) > 1)
                {
                    $namapemeriksaan = '';
                    $hasil = '';
                    $satuan = '';
                    $normal = '';
                    $metode = '';
                    $val_pem = true;


                    foreach($val['pemeriksaan'] as $temp)
                    {
                        // if ( (round(count($params)/2)) == $idx){ 

                           // echo (round(count($params)/2));

                        if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
						if($temp['kelompok'] == 'Morfologi'){
							$rows .= '<tr>';
							if($val_pem)
							{
								$rows .= '<td valsign="top" rowspan="'. count($val['pemeriksaan']) .'">'. $idx .'</td>';
								$rows .= '<td valign="top" width="10%" rowspan="'. count($val['pemeriksaan']) .'"><b>'. $val['kelompok'] . '</b></td>';
								$val_pem = false;
							}
							$rows .= '<td valign="top" width="10%">'. $temp['namapemeriksaan'] . '</td>';
							$rows .= '<td valign="top" colspan="4">'. $temp['hasil'] .'</td>';
							$rows .= '</tr>';
						} else {
                             
							$rows .= '<tr>';
							if($val_pem)
							{
								$rows .= '<td class="grid_td_nomor2" valign="top" rowspan="'. count($val['pemeriksaan']) .'">'. $idx .'</td>';
								$rows .= '<td class="grid_td_pemeriksaan2" valign="top" rowspan="'. count($val['pemeriksaan']) .'"><b>'. $val['kelompok'] . '</b></td>';
								$val_pem = false;
							}
							$rows .= '<td valign="top">'. $temp['namapemeriksaan'] . '</td>';
							$rows .= '<td valign="top" style="text-align: center;">'. $temp['hasil'] .'</td>';
							$rows .= '<td valign="top" style="white-space: nowrap; text-align: center;">'. $temp['satuan'] .'</td>';
							$rows .= '<td valign="top"  style="white-space: nowrap; text-align: center;">'. $temp['normal'] .'</td>';
							$rows .= '<td valign="top">'. $temp['metode'] .'</td>';
							$rows .= '</tr>';
						}
                        // }

                    }
                }else{
                    $row = '';
                     if((round(count($params)/2)) == $idx && $idx > 15){
                        $row .= '</table><table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">';
                     }
                    
                    foreach($val['pemeriksaan'] as $temp)
                    {
                         
                       
                    if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
                        
                       
                        $row .= '<tr>';
                        $row .= '<td class="grid_td_nomor" valign="top">'. $idx .'</td>';
                        $row .= '<td class"grid_td_pemeriksaan" valign="top" colspan="2"><div>'. $temp['kelompok'] .'</div></td>';
                        $row .= '<td class="grid_td_hasil" valign="top" style="text-align: center;">'. $temp['hasil'] .'</td>';
                        $row .= '<td class="grid_td_satuan" valign="top" style="white-space: nowrap; text-align: center;">'. $temp['satuan'] .'</td>';
                        $row .= '<td class="grid_td_normal" valign="top" style="white-space: nowrap; text-align: center;">'. $temp['normal'] .'</td>';
                        $row .= '<td class="grid_td_metode" valign="top">'. $temp['metode'] .'</td>';
                        $row .= '</tr>';

                }

                // $row .= '</table>';
                    $rows = $row;
                   
                }
                

                echo($rows);
            
                $kelompok = $val['kelompok'];
                $idx++;
        
    }

        ?>
   </tbody>
</table><br>