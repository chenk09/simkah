<table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">
    <thead>
        <tr>
            <td colspan="3" align="center" style="border-width: 0px;">
                <div style="text-align:right;font-size:13pt;">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                 
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                  
                 <b><?=$jenisperiksa?></b>
                </div>
            </td>
        </tr>
        <tr>
            <th width="4%">No.</th>
            <th width="28%">Pemeriksaan</th>
            <th width="17%">Hasil</th>
            <th width="17%">Normal</th>
            <th width="17%">Satuan</th>
            <th>Metode</th>
<!--            <th>Keterangan</th>-->
        </tr>
    </thead>
    <tbody>
        <?php
            $idx = 1;
            $kelompok = "";
            $rows = '';
            foreach($params as $val)
            {
                $rows = '';
                if(count($val['pemeriksaan']) > 1)
                {
                    $namapemeriksaan = '';
                    $hasil = '';
                    $satuan = '';
                    $normal = '';
                    $metode = '';
//                    $ket = '';

                    foreach($val['pemeriksaan'] as $temp)
                    {
                        if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
                        
                        $namapemeriksaan .= '<div>' . $temp['namapemeriksaan'] .'</div>';
                        $hasil .= '<div style="text-align: center;">'. $temp['hasil'] .'</div>';
                        $normal .= '<div style="white-space: nowrap; text-align: center;">'. $temp['normal'] .'</div>';
                        $satuan .= '<div style="white-space: nowrap; text-align: center;">'. $temp['satuan'] .'</div>';
                        $metode .= '<div>'. $temp['metode'] .'</div>';
//                        $ket .= '<div>'. $temp['keterangan'] .'</div>';
                    }
                    $rows .= '<tr>';
                    $rows .= '<td valign="top">'. $idx .'</td>';
                    $rows .= '<td valign="top">'. $namapemeriksaan .'</td>';
                    $rows .= '<td valign="top" style="white-space: nowrap; text-align: center;">'. $hasil .'</td>';
                    $rows .= '<td valign="top"><div>'. $satuan .'</div></td>';
                    $rows .= '<td valign="top">'. $normal .'</td>';
                    $rows .= '<td valign="top"><div>'. $metode .'</div></td>';
//                    $rows .= '<td valign="top"><div>'. $ket .'</div></td>';
                    $rows .= '</tr>';                    
                }else{
                    $row = '';
                    foreach($val['pemeriksaan'] as $temp)
                    {
                        if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
                        
                        $row .= '<tr>';
                        $row .= '<td valign="top">'. $idx .'</td>';
                        $row .= '<td valign="top"><div>'. $temp['namapemeriksaan'] .'</div></td>';
                        $row .= '<td valign="top">'. $temp['hasil'] .'</td>';
                        $row .= '<td valign="top"><div>'. $temp['satuan'] .'</div></td>';
                        $row .= '<td valign="top">'. $temp['normal'] .'</td>';
                        $row .= '<td valign="top"><div>'. $temp['metode'] .'</div></td>';
//                        $row .= '<td valign="top"><div>'. $temp['keterangan'] .'</div></td>';
                        $row .= '</tr>';
                    }
                    $rows = $row;
                }
                echo($rows);
                $kelompok = $val['kelompok'];
                $idx++;
            }

        ?>
   </tbody>
</table><br>