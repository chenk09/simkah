<table  width="100%" border="0" class="grid_td" cellpadding="0" cellspacing="0" style="page-break-inside: avoid; font-size: 20px;">
    <tr>
        <td align="left" width="50%" style="font-size: 14px;">Catatan : </td>
        <td align="center" style="font-size: 14px;">PEMERIKSA</td>
    </tr>
    <tr>
        <td style="height: 70px; vertical-align: top; padding-left: 20px; font-size: 11pt;">
            <p style="text-align: justify">
            <?php
                $modHasilPemeriksaanLab = HasilpemeriksaanlabT::model()->findByAttributes(array(
                    'pasienmasukpenunjang_id'=>$masukpenunjang->pasienmasukpenunjang_id,
                    'pasien_id'=>$masukpenunjang->pasien_id,
                ));
                echo $modHasilPemeriksaanLab->catatanlabklinik;
            ?>
            </p>
        </td>
    </tr>
    <tr>
        <td align="left" style="font-size: 12px;">
            
           <!-- Printed By : <?=$masukpenunjang->getNamaPegawai(Yii::app()->user->getState('pegawai_id'))?> <?=date('d/m/Y H:i:s')?> -->
            <!-- Waktu Pengambilan Spesimen : <?php echo $masukpenunjang->tglpengambilansample; ?>  -->
        </td>
         <td align="center" style="font-size: 14px;">
             --------------        
            <!--<?=$masukpenunjang->getNamaLengkapDokter($masukpenunjang->pegawai_id)?>-->
        </td>
        <td align="center" style="font-size: 14px;">
                     
           <!-- <?=$masukpenunjang->getNamaLengkapDokter($masukpenunjang->pegawai_id)?> -->
        </td>
    </tr>
         <tr>
            <td align="left" style="font-size: 12px;">
            <?php
            if ( $modHasilPemeriksaanLab->tglhasilpemeriksaanlab != '' ) {
                $tgl_awal = $modHasilPemeriksaanLab->tglhasilpemeriksaanlab;
            } else {
                $tgl_awal = '0000-00-00 00:00:00';
            }

            echo "Waktu Datang Sample  : " . date('d-m-Y H:i:s' , strtotime( $tgl_awal ) );
            ?>
            
            </td>   
        </tr>
        
        <tr>
            <td align="left" style="font-size: 12px;">
            <?php

                if ( $modHasilPemeriksaanLab->tglpengambilanhasil != NULL ) {
                    $tgl_ambil_hasil = $modHasilPemeriksaanLab->tglpengambilanhasil;
                } else {
                    $tgl_ambil_hasil = '0000-00-00 00:00:00';
                }
                echo "Waktu Keluar Hasil Sample : " . date('d-m-Y H:i:s' , strtotime( $tgl_ambil_hasil ) );
            ?>
            
            </td>   
        </tr>
     <tr>
        <td align="left" style="font-size: 12px;">
            <?php
                $begin = new DateTime($tgl_awal);
                $end = new DateTime($tgl_ambil_hasil);
                $diff = $begin->diff($end);

                if ( $modHasilPemeriksaanLab->tglhasilpemeriksaanlab == '01-01-1970 07:00:00' || $modHasilPemeriksaanLab->tglpengambilanhasil == NULL ) {
                    echo "TAT : DATA TIDAK VALID";  
                } else {
                    echo "TAT : " . $diff->format("%d hari %h jam %i menit");   
                }
                
                
            ?>
        </td>
    </tr>
    
</table>