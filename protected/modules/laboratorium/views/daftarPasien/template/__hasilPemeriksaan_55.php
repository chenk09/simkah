<table border="0" width="100%" class="grid" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">
    <thead>
        <tr>
            <td colspan="6" align="center" style="border-width: 0px;">
                <div style="text-align: center;font-size:13pt;">
                    <b><?=$jenisperiksa?></b>
                </div>
            </td>
        </tr>        
        <tr>
            <th width="3%">No.</th>
            <th width="25%">Pemeriksaan</th>
            <th width="15%">Hasil</th>
            <th width="15%">Satuan</th>
            <th width="15%">Normal</th>
            <th width="15%">Metode</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $idx = 1;
            $kelompok = "";
            $rows = '';
            
            $value_cons = array(
                '426'=>'Hasil EKG Test Terlampir',
                '427'=>'Hasil Treadmil Test Terlampir',
                '428'=>'Hasil Echo Cardiography A (lengkap) Terlampir',
                '429'=>'Hasil Terlampir',
                '430'=>'Hasil Terlampir',
            );
            
            foreach($params as $val)
            {
                $rows = '';
                if(count($val['pemeriksaan']) > 1)
                {
                    $namapemeriksaan = '';
                    $hasil = '';
                    $satuan = '';
                    $normal = '';
                    $metode = '';
                    
                    foreach($val['pemeriksaan'] as $temp)
                    {
                        if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
                        
                        $namapemeriksaan .= '<div>' . $temp['namapemeriksaan'] .'</div>';
                        $hasil .= '<div>'. $temp['hasil'] .'</div>';
                        $satuan .= '<div>'. $temp['satuan'] .'</div>';
                        $normal .= '<div>'. $temp['normal'] .'</div>';
                        $metode .= '<div>'. $temp['metode'] .'</div>';
                    }
                    $rows .= '<tr>';
                    $rows .= '<td valign="top">'. $idx .'</td>';
                    $rows .= '<td valign="top"><b>'. $val['kelompok'] . '</b>'. $namapemeriksaan .'</td>';
                    $rows .= '<td valign="top" style="text-align: center;">'. $hasil .'</td>';
                    $rows .= '<td valign="top" style="white-space: nowrap; text-align: center;">'. $satuan .'</td>';
                    $rows .= '<td valign="top"  style="text-align: center;">'. $normal .'</td>';
                    $rows .= '<td valign="top">'. $metode .'</td>';
                    $rows .= '</tr>';                    
                }else{
                    $row = '';
                    foreach($val['pemeriksaan'] as $temp)
                    {
                        if(empty($temp['hasil'])) if($temp['hasil'] == "0") $temp['hasil'] = '0'; else $temp['hasil'] = '-';
                        
                        $row .= '<tr>';
                        $row .= '<td valign="top">'. $idx .'</td>';
                        $row .= '<td valign="top"><div>'. $temp['namapemeriksaan'] .'</div></td>';
                        
                        if(array_key_exists($temp['id_pemeriksaan'], $value_cons))
                        {
                            $row .= '<td valign="top" colspan="4">'. $value_cons[$temp['id_pemeriksaan']] .'</td>';
                        }else{
                            $row .= '<td valign="top" style="white-space: nowrap; text-align: center;">'. $temp['hasil'] .'</td>';
                            $row .= '<td valign="top" style="white-space: nowrap; text-align: center;">'. $temp['satuan'] .'</td>';
                            $row .= '<td valign="top"  style="white-space: nowrap; text-align: center;">'. $temp['normal'] .'</td>';
                            $row .= '<td valign="top">'. $temp['metode'] .'</td>';
                        }
                        $row .= '</tr>';
                    }
                    $rows = $row;
                }
                echo($rows);
                $kelompok = $val['kelompok'];
                $idx++;
            }

        ?>
   </tbody>
</table><br>