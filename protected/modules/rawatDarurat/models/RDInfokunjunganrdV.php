<?php

/**
 * This is the model class for table "infokunjunganrd_v".
 *
 * The followings are the available columns in table 'infokunjunganrd_v':
 * @property integer $pendaftaran_id
 * @property string $tgl_pendaftaran
 * @property string $no_pendaftaran
 * @property string $statusperiksa
 * @property string $statusmasuk
 * @property string $no_rekam_medik
 * @property string $nama_pasien
 * @property string $alamat_pasien
 * @property integer $propinsi_id
 * @property string $propinsi_nama
 * @property integer $kabupaten_id
 * @property string $kabupaten_nama
 * @property integer $kecamatan_id
 * @property string $kecamatan_nama
 * @property integer $kelurahan_id
 * @property string $kelurahan_nama
 * @property integer $instalasi_id
 * @property string $ruangan_nama
 * @property integer $carabayar_id
 * @property string $carabayar_nama
 * @property integer $penjamin_id
 * @property string $penjamin_nama
 * @property integer $rujukan_id
 */
class RDInfokunjunganrdV extends InfokunjunganrdV
{
         public $ceklis = false;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InfokunjunganrdV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function searchRD()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
		$criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
		$criteria->compare('LOWER(carakeluar)',strtolower($this->carakeluar ),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
        if($this->ceklis)
        {
            $criteria->addBetweenCondition('tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
        }
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
		$criteria->compare('rujukan_id',$this->rujukan_id);
        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
//        $criteria->addCondition("pasienpulang_id IS NULL");
//		$criteria->addCondition("statusperiksa NOT IN ('SEDANG DIRAWAT INAP', 'BATAL PERIKSA')");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                        }
            }
            return true;
        }


        function getNamaPasienNamaBin()
        {
            return $this->nama_pasien.' bin '.$this->nama_bin;
        }


        public function getInsatalasiRuangan()
        {

            return $this->instalasi_nama.' / '.$this->ruangan_nama;
        }

        public function getStatus($status,$id){
            if($status == "ANTRIAN"){
                $status = '<button id="red" class="btn btn-primary" name="yt1">'.$status.'</button>';

            }else if($status == "SEDANG PERIKSA"){
                $status = '<button id="green" class="btn btn-danger" name="yt1" onclick="setStatus(this,\''.$status.'\','.$id.')">'.$status.'</button>';
            }else if($status == "SUDAH PULANG"){
                $status = '<button id="blue" class="btn btn-danger-yellow" name="yt1" onclick="setStatus(this,\''.$status.'\','.$id.')">'.$status.'</button>';
            }else if($status == "SUDAH DI PERIKSA"){
                $status = '<button id="red" class="btn btn-danger-red" name="yt1" onclick="setStatus(this,\''.$status.'\','.$id.')">'.$status.'</button>';
            }else{
//                $status = '<button id="orange" class="btn btn-danger-blue"  name="yt1">'.$status.'</button>';
				$status = '<center>-</center>';
            }
            return $status;
        }

    public function getPeriksaPasien($status,$id,$pembayaran,$nopen,$alih){
        if($pembayaran != null){
            $status = '<center><a id='.$nopen.' href="#"  onclick="cekStatus(\''.$status.'\')" rel="tooltip"
                            data-original-title="Klik untuk Pemeriksaan Pasien"><i class="icon-list-alt"></i></a></center>';
        }else{
            if($status == "ANTRIAN"){
                $status = "<center><a id=".$nopen." href=\"index.php?r=rawatDarurat/anamnesaTRD&idPendaftaran=".$id."\" rel=\"tooltip\" data-original-title=\"Klik untuk Pemeriksaan Pasien\">
                            <i class=\"icon-list-alt\"></i>
                            </a></center>";

            }else if($status == "SEDANG PERIKSA"){
                $status = "<center><a id=".$nopen." href=\"index.php?r=rawatDarurat/anamnesaTRD&idPendaftaran=".$id."\" rel=\"tooltip\" data-original-title=\"Klik untuk Pemeriksaan Pasien\">
                            <i class=\"icon-list-alt\"></i>
                            </a></center>";
            }else if($status == "BATAL PERIKSA" || $status =="DIBATALKAN" ||     $status == "SEDANG DIRAWAT INAP" || $alih == true || $status == "SUDAH PULANG"){
                $status = '<center><a id='.$nopen.' href="#"  onclick="cekStatus(\''.$status.'\')" rel="tooltip"
                                data-original-title="Klik untuk Pemeriksaan Pasien"><i class="icon-list-alt"></i></a></center>';
            }else{
                $status = "<center><a id=".$nopen." href=\"index.php?r=rawatDarurat/anamnesaTRD&idPendaftaran=".$id."\" rel=\"tooltip\" data-original-title=\"Klik untuk Pemeriksaan Pasien\">
                            <i class=\"icon-list-alt\"></i>
                            </a></center>";
            }
        }

        return $status;
    }

    public function getTindakLanjut($status,$id,$pembayaran,$nopen,$pasienpulang,$carakeluar,$alih,$kondisipulang){
            if($status == "ANTRIAN" || $status == "BATAL PERIKSA" || $status == "DIBATALKAN" || $statusperiksa == "SEDANG DIRAWAT INAP"){
                $status = '<center><a href="#"  onclick="cekStatus(\''.$status.'\')"
                                     rel="tooltip" data-original-title="Klik Untuk Menindak Lanjuti Pasien"><i class="icon-pencil"></i></a></center>';
            }else if($status == "SEDANG PERIKSA" || $status == "SUDAH PULANG" && $carakeluar != "MENINGGAL"){
                 $status = '<center><a href="index.php?r=rawatDarurat/daftarPasien/PasienPulang&idPendaftaran='.$id.'&dialog=1"
                                 onclick="$(\'#dialogPasienPulang\').dialog(\'open\');" target="iframePasienPulang"
                                     rel="tooltip" data-original-title="Klik Untuk Menindak Lanjuti Pasien"><i class="icon-pencil"></i></a></center>';
            }else if(!empty($pasienpulang)&& ($carakeluar == "DIRAWAT INAP") OR ($carakeluar == "DIPULANGKAN") OR ($carakeluar == "DIRUJUK")){
                $status = '<center>'.$carakeluar.'<br>
                            <a href="index.php?r=rawatDarurat/daftarPasien/BatalRawatInap&idPendaftaran='.$id.'" rel=\"tooltip"
                                onclick="$(\'#dialogBatalRawatInap\').dialog(\'open\');" target="iframeBatalRawatInap"
                                    data-original-title="Klik Untuk Batal '.$carakeluar.'"><i class="icon-remove"></i></a></center>';
            }else if($carakeluar == "MENINGGAL") {
                $status = '<center>'.$kondisipulang.'</center>';
            }else{
                  $status = '<center><a href="index.php?r=rawatDarurat/daftarPasien/PasienPulang&idPendaftaran='.$id.'&dialog=1"
                                 onclick="$(\'#dialogPasienPulang\').dialog(\'open\');" target="iframePasienPulang"
                                     rel="tooltip" data-original-title="Klik Untuk Menindak Lanjuti Pasien"><i class="icon-pencil"></i></a></center>';
            }
        return $status;
    }

    public function getPemulanganPasien(){
        $status = CHtml::link("<i class='icon-edit'></i>", array("daftarPasien/PasienPulang", "idPendaftaran"=>$this->pendaftaran_id, "dialog"=>1), array(
            "onclick"=>"$('#dialogPasienPulang').dialog('open');",
            "target"=>"iframePasienPulang",
            "rel"=>"tooltip",
            "data-original-title"=>"Klik Untuk Menindak Lanjuti Pasien"
        ));
        return $status;
    }

    public function getKirimRanap(){
        $status = CHtml::link("<i class='icon-user'></i>", array("daftarPasien/PasienPulang", "idPendaftaran"=>$this->pendaftaran_id, "dialog"=>1, "is_ranap"=>1), array(
            "onclick"=>"$('#dialogPasienPulang').dialog('open');",
            "target"=>"iframePasienPulang",
            "rel"=>"tooltip",
            "data-original-title"=>"Klik Untuk Menindak Lanjuti Pasien"
        ));
        return $status;
    }
	
    public function searchPasienRanap()
	{
		$format = new CustomFormat();
        $this->tglAwal  = $format->formatDateTimeMediumForDB($this->tglAwal);
        $this->tglAkhir = $format->formatDateTimeMediumForDB($this->tglAkhir);

		$criteria = new CDbCriteria;
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
		$criteria->addCondition("statusperiksa IN ('SEDANG DIRAWAT INAP') AND pasienadmisi_id IS NULL");
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->addBetweenCondition("DATE(tgl_pendaftaran)", $this->tglAwal, $this->tglAkhir);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	

}

?>
