<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PPRuanganM
 *
 * @author sujana
 */
class RDRuanganM extends RuanganM{
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function relations()
   {
       return array(
           'tindakanruangan'=>array(self::HAS_MANY,'PPTindakanruanganM','ruangan_id'),
           'daftartindakan'=>array(self::HAS_MANY,'PPDaftartindakanM',array('daftartindakan_id'=>'daftartindakan_id'),'through'=>'tindakanruangan'),
           'tariftindakan'=>array(self::HAS_MANY,'PPTariftindakan',array('daftartindakan_id'=>'daftar_tindakan_id'),'through'=>'daftartindakan'),
           'instalasi' => array(self::BELONGS_TO, 'InstalasiM', 'instalasi_id'),
       );
   }
   
   public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('ruangan_id',$this->ruangan_id);
		//$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(ruangan_namalainnya)',strtolower($this->ruangan_namalainnya),true);
		$criteria->compare('LOWER(ruangan_lokasi)',strtolower($this->ruangan_lokasi),true);
//		$criteria->compare('ruangan_aktif',$this->ruangan_aktif);
       		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);

                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 
                $criteria->order='ruangan_nama';
                $criteria->with=array('instalasi','tindakanruangan','daftartindakan','tariftindakan');
                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}

?>
