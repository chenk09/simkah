<?php

class RDPendaftaranT extends PendaftaranT
{    
    public $noRm, $namaInstalasi, $namaRuangan, $namaCarabayar, $tgl_pendaftaran_cari; //untuk filter grid / dialog box
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PasienM the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function relations()
	{ 
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                        'pasien'=>array(self::BELONGS_TO,'PasienM','pasien_id'),
                        'penanggungJawab'=>array(self::BELONGS_TO,'PenanggungjawabM','penanggungjawab_id'),
                        'ruangan'=>array(self::BELONGS_TO,'RuanganM','ruangan_id'),
                        'kasuspenyakit'=>array(self::BELONGS_TO,'JeniskasuspenyakitM','jeniskasuspenyakit_id'),
                        'dokter'=>array(self::BELONGS_TO,'PegawaiM','pegawai_id'),
                        'penjamin'=>array(self::BELONGS_TO, 'PenjaminpasienM', 'penjamin_id'),
                        'instalasi'=>array(self::BELONGS_TO, 'InstalasiM', 'instalasi_id'),
                        'carabayar'=>array(self::BELONGS_TO,  'CarabayarM', 'carabayar_id'),
                        'kirimkeunitlain'=>array(self::HAS_MANY, 'PasienkirimkeunitlainT', 'pendaftaran_id'),
                        'anamnesa'=>array(self::HAS_ONE, 'AnamnesaT', 'pendaftaran_id'),
                        'pemeriksaanfisik'=>array(self::HAS_ONE, 'PemeriksaanfisikT', 'pendaftaran_id'),
                        'pasienmasukpenunjang'=>array(self::HAS_ONE, 'PasienmasukpenunjangT', 'pendaftaran_id'),
                        'diagnosa'=>array(self::HAS_MANY, 'PasienmorbiditasT', 'pendaftaran_id'),
                        'pegawai'=>array(self::BELONGS_TO, 'PegawaiM', 'pegawai_id'),
                        'hasilpemeriksaanlab'=>array(self::HAS_ONE, 'HasilpemeriksaanlabT', 'pendaftaran_id'),
                        'pasienpulang'=>array(self::HAS_ONE, 'PasienpulangT', 'pasienpulang_id'),
                        'tindakanpelayanan'=>array(self::HAS_MANY, 'TindakanpelayananT', 'pendaftaran_id'),
                    
		);
	}
        
    public function searchRiwayat($data)
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('caramasuk_id',$this->caramasuk_id);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->compare('shift_id',$this->shift_id);
            $criteria->compare('golonganumur_id',$this->golonganumur_id);
            $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
            $criteria->compare('rujukan_id',$this->rujukan_id);
            $criteria->compare('penanggungjawab_id',$this->penanggungjawab_id);
            $criteria->compare('ruangan_id',$this->ruangan_id);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
            $criteria->condition = "pasien_id = ".$data;
//            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
//            $criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
//            $criteria->compare('no_urutantri',$this->no_urutantri);
//            $criteria->compare('LOWER(transportasi)',strtolower($this->transportasi),true);
//            $criteria->compare('LOWER(keadaanmasuk)',strtolower($this->keadaanmasuk),true);
//            $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
//            $criteria->compare('LOWER(statuspasien)',strtolower($this->statuspasien),true);
//            $criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
//            $criteria->compare('alihstatus',$this->alihstatus);
//            $criteria->compare('byphone',$this->byphone);
//            $criteria->compare('kunjunganrumah',$this->kunjunganrumah);
//            $criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
//            $criteria->compare('LOWER(umur)',strtolower($this->umur),true);
            //$criteria->condition = 'pasienpulang.pendaftaran_id = t.pendaftaran_id';
            //$criteria->order = 'no_urutantri';

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    
        /**
        * searchPasienRD menampilkan pasien rawat darurat untuk dialog box di:
         * - transaksi - pasien pulang
        * @return \CActiveDataProvider
        */
        public function searchPasienRD(){
           $format = new CustomFormat;
           $criteria = new CDbCriteria();
           $criteria->with = array('pasien','instalasi','ruangan', 'carabayar');
           $criteria->compare('LOWER(t.no_pendaftaran)', strtolower($this->no_pendaftaran), true);
           if(!empty($this->tgl_pendaftaran_cari)){
               $this->tgl_pendaftaran_cari = $format->formatDateMediumForDB($this->tgl_pendaftaran_cari);
               $criteria->addBetweenCondition('t.tgl_pendaftaran', $this->tgl_pendaftaran_cari." 00:00:00", $this->tgl_pendaftaran_cari." 23:59:59");
           }
           $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->noRm), true);
           $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->namaPasien), true);
           $criteria->compare('LOWER(instalasi.instalasi_nama)', strtolower($this->namaInstalasi), true);
           $criteria->compare('LOWER(ruangan.ruangan_nama)', strtolower($this->namaRuangan), true);
           $criteria->compare('LOWER(carabayar.carabayar_nama)', strtolower($this->namaCarabayar), true);
           $criteria->compare('LOWER(statusperiksa)', strtolower($this->statusperiksa), true);
           $criteria->addCondition("pasienbatalperiksa_id is null and pendaftaran_id is not null");
           $criteria->addCondition('t.ruangan_id = '.Yii::app()->user->getState('ruangan_id'));
           $criteria->addNotInCondition("statusperiksa", array("SUDAH PULANG"));
           $criteria->addInCondition('ispasienluar',array(false));
           $criteria->limit = 50;
           $criteria->order = 'tgl_pendaftaran DESC';
           //kembalikan format
           $this->tgl_pendaftaran_cari = empty($this->tgl_pendaftaran_cari) ? null : date('d M Y',strtotime($this->tgl_pendaftaran_cari));
           return new CActiveDataProvider($this, array(
                       'criteria'=>$criteria,
               ));
        }
        //=== Start functions untuk dialogpendaftaran ===
        public function getJenisKasusPenyakitNama(){
            return $this->kasuspenyakit->jeniskasuspenyakit_nama;
        }
        public function getInstalasiNama(){
            return $this->instalasi->instalasi_nama;
        }
        public function getRuanganNama(){
            return $this->ruangan->ruangan_nama;
        }
        public function getPasienNama(){
            return $this->pasien->nama_pasien;
        }
        public function getPasienJenisKelamin(){
            return $this->pasien->jeniskelamin;
        }
        public function getPasienNoRm(){
            return $this->pasien->no_rekam_medik;
        }
        public function getPasienAlias(){
            return $this->pasien->nama_bin;
        }
        public function getCarabayarNama(){
            return $this->carabayar->carabayar_nama;
        }
        public function getPenjaminNama(){
            return $this->penjamin->penjamin_nama;
        }
        public function getPegawaiNama(){
            return $this->pegawai->NamaLengkap;
        }
        
}
?>
