<?php

/**
 * This is the model class for table "jadwaldokter_m".
 *
 * The followings are the available columns in table 'jadwaldokter_m':
 * @property integer $jadwaldokter_id
 * @property integer $instalasi_id
 * @property integer $ruangan_id
 * @property integer $pegawai_id
 * @property string $jadwaldokter_hari
 * @property string $jadwaldokter_buka
 * @property string $jadwaldokter_mulai
 * @property string $jadwaldokter_tutup
 * @property integer $maximumantrian
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class RDJadwaldokterM extends JadwaldokterM
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return JadwaldokterM the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function searchJadwalIGD()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria(array(
                    'distinct' => true,
                    'select' => array('pegawai_id')
                    ));

            $criteria->compare('t.instalasi_id',Params::INSTALASI_ID_RD);
            $criteria->compare('ruangan_id',$this->ruangan_id);
            $criteria->compare('pegawai_id',$this->pegawai_id);
            $criteria->compare('jadwaldokter_tutup',strtolower($this->jadwaldokter_tutup));
            $criteria->compare('jadwaldokter_mulai',strtolower($this->jadwaldokter_mulai));
            $criteria->compare('maximumantrian',$this->maximumantrian);


            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }

    public function searchJadwalRJ()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria(array(
                    'distinct' => true,
                    'select' => array('pegawai_id'),
                    ));

//            $criteria->compare('t.instalasi_id',Params::INSTALASI_ID_RJ);
            $criteria->compare('ruangan_id',$this->ruangan_id);
            $criteria->compare('pegawai_id',$this->pegawai_id);
            $criteria->compare('jadwaldokter_tutup',strtolower($this->jadwaldokter_tutup));
            $criteria->compare('jadwaldokter_mulai',strtolower($this->jadwaldokter_mulai));
            $criteria->compare('maximumantrian',$this->maximumantrian);


            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
}
?>
