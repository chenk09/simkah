<?php

class RDPenjualanresepT extends PenjualanresepT {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchDetailTerapi($data)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array('obatalkes');
		$criteria->compare('penjualanresep_id',$this->penjualanresep_id);
		$criteria->compare('reseptur_id',$this->reseptur_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('t.pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('t.ruangan_id',$this->ruangan_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('pasien_id',$this->pasien_id);
                $criteria->condition = 't.pendaftaran_id = '.$data;
//		$criteria->compare('LOWER(tglpenjualan)',strtolower($this->tglpenjualan),true);
//		$criteria->compare('LOWER(jenispenjualan)',strtolower($this->jenispenjualan),true);
//		$criteria->compare('LOWER(tglresep)',strtolower($this->tglresep),true);
//		$criteria->compare('LOWER(noresep)',strtolower($this->noresep),true);
//		$criteria->compare('totharganetto',$this->totharganetto);
//		$criteria->compare('totalhargajual',$this->totalhargajual);
//		$criteria->compare('totaltarifservice',$this->totaltarifservice);
//		$criteria->compare('biayaadministrasi',$this->biayaadministrasi);
//		$criteria->compare('biayakonseling',$this->biayakonseling);
//		$criteria->compare('pembulatanharga',$this->pembulatanharga);
//		$criteria->compare('jasadokterresep',$this->jasadokterresep);
//		$criteria->compare('LOWER(instalasiasal_nama)',strtolower($this->instalasiasal_nama),true);
//		$criteria->compare('LOWER(ruanganasal_nama)',strtolower($this->ruanganasal_nama),true);
//		$criteria->compare('discount',$this->discount);
//		$criteria->compare('subsidiasuransi',$this->subsidiasuransi);
//		$criteria->compare('subsidipemerintah',$this->subsidipemerintah);
//		$criteria->compare('subsidirs',$this->subsidirs);
//		$criteria->compare('iurbiaya',$this->iurbiaya);
//		$criteria->compare('lamapelayanan',$this->lamapelayanan);
//		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
//		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
//		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
//		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
//		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        /**
         * getDetailObatTerapi untuk menampilkan detail obat terapi (obatalkespasien_t)
         * @author Ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         * @param type $idPenjualanResep 
         */
        public function getObatTerapi($idPenjualanResep){
            $modObatTerapi = ObatalkespasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$idPenjualanResep));
            return $modObatTerapi;
        }

}