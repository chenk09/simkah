<?php

class LaporanController extends SBaseController {

    public function actionLaporanSensusHarian() {
        $model = new RDLaporansensusharian('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['RDLaporansensusharian'])) {
            $model->attributes = $_GET['RDLaporansensusharian'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporansensusharian']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporansensusharian']['tglAkhir']);
        }

        $this->render('sensus/adminSensus', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanSensusHarian() {
        $model = new RDLaporansensusharian('search');
        $judulLaporan = 'Laporan Sensus Harian Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporansensusharian'])) {
            $model->attributes = $_REQUEST['RDLaporansensusharian'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporansensusharian']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporansensusharian']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'sensus/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikSensusHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporansensusharian('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['RDLaporansensusharian'])) {
            $model->attributes = $_GET['RDLaporansensusharian'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporansensusharian']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporansensusharian']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanKunjungan() {
        $model = new RDLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RDLaporankunjunganrdV'])) {
            $model->attributes = $_GET['RDLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporankunjunganrdV']['tglAkhir']);
        }


        $this->render('kunjungan/adminKunjungan', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjungan() {
        $model = new RDLaporankunjunganrdV('search');
        $judulLaporan = 'Laporan Info Kunjungan Pasien Rawat Darurat';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Info Kunjungan';
        $data['type'] = $_REQUEST['type'];

        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_REQUEST['RDLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['RDLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporankunjunganrdV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjungan/_printKunjungan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjungan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Info Kunjungan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporankunjunganrdV'])) {
            $model->attributes = $_GET['RDLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporankunjunganrdV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBukuRegister() {
        $model = new RDBukuregisterpasien('search');
        $model->tglAwal = date('d M Y  00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RDBukuregisterpasien'])) {
            $model->attributes = $_GET['RDBukuregisterpasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDBukuregisterpasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDBukuregisterpasien']['tglAkhir']);
        }

        $this->render('bukuRegister/adminBukuRegister', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanBukuRegister() {
        $model = new RDBukuregisterpasien('search');
        $judulLaporan = 'Laporan Buku Register Pasien Rawat Darurat';

        //Data Grafik   
        $data['title'] = 'Grafik Laporan Buku Register Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDBukuregisterpasien'])) {
            $model->attributes = $_REQUEST['RDBukuregisterpasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDBukuregisterpasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDBukuregisterpasien']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuRegister/_printBukuRegister';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDBukuregisterpasien('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Rawat Darurat';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDBukuregisterpasien'])) {
            $model->attributes = $_GET['RDBukuregisterpasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDBukuregisterpasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDBukuregisterpasien']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporan10BesarPenyakit() {
        $model = new RDLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->jumlahTampil = 10;

        if (isset($_GET['RDLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['RDLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporan10besarpenyakit']['tglAkhir']);
        }

        $this->render('10Besar/admin10BesarPenyakit', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporan10BesarPenyakit() {
        $model = new RDLaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['RDLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '10Besar/_print10Besar';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['RDLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporan10besarpenyakit']['tglAkhir']);
        }
               
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanCaraMasukPasien() {
        $model = new RDLaporancaramasukpasienrd('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $asalrujukan = CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'),'asalrujukan_id','asalrujukan_id');
        $model->asalrujukan_id = $asalrujukan;
        if (isset($_GET['RDLaporancaramasukpasienrd'])) {
            $model->attributes = $_GET['RDLaporancaramasukpasienrd'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporancaramasukpasienrd']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporancaramasukpasienrd']['tglAkhir']);
        }

        $this->render('caraMasuk/adminCaraMasukPasien', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanCaraMasukPasien() {
        $model = new RDLaporancaramasukpasienrd('search');
        $judulLaporan = 'Laporan Cara Masuk Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporancaramasukpasienrd'])) {
            $model->attributes = $_REQUEST['RDLaporancaramasukpasienrd'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporancaramasukpasienrd']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporancaramasukpasienrd']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'caraMasuk/_printCaraMasuk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanCaraMasukPasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporancaramasukpasienrd('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporancaramasukpasienrd'])) {
            $model->attributes = $_GET['RDLaporancaramasukpasienrd'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporancaramasukpasienrd']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporancaramasukpasienrd']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanTindakLanjut() {
        $model = new RDLaporantindaklanjutrd('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $temp = array();
        foreach (CaraKeluar::items() as $i=>$data){
            $temp[] = strtoupper($data);
        }
        $model->carakeluar = $temp;
        
        if (isset($_GET['RDLaporantindaklanjutrd'])) {
            $model->attributes = $_GET['RDLaporantindaklanjutrd'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporantindaklanjutrd']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporantindaklanjutrd']['tglAkhir']);
        }

        $this->render('tindakLanjut/adminTindakLanjut', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanTindakLanjut() {
        $model = new RDLaporantindaklanjutrd('search');
        $judulLaporan = 'Laporan Tindak Lanjut Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Tindak Lanjut Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporantindaklanjutrd'])) {
            $model->attributes = $_REQUEST['RDLaporantindaklanjutrd'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporantindaklanjutrd']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporantindaklanjutrd']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'tindakLanjut/_printTindakLanjut';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanTindakLanjut() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporantindaklanjutrd('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik 
        $data['title'] = 'Grafik Laporan Tindak Lanjut Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporantindaklanjutrd'])) {
            $model->attributes = $_GET['RDLaporantindaklanjutrd'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporantindaklanjutrd']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporantindaklanjutrd']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanKonsulAntarPoli() {
        $model = new RDLaporankonsulantarpoli('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $ruanganrawatjalan = CHtml::listData(RuanganrawatjalanV::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_id');
        $model->ruangantujuan_id = $ruanganrawatjalan;
        if (isset($_GET['RDLaporankonsulantarpoli'])) {
            $model->attributes = $_GET['RDLaporankonsulantarpoli'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporankonsulantarpoli']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporankonsulantarpoli']['tglAkhir']);
        }

        $this->render('konsulPoli/adminKonsulAntarPoli', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKonsulAntarPoli() {
        $model = new RDLaporankonsulantarpoli('search');
        $judulLaporan = 'Laporan Konsul Antar Poli Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Konsul Antar Poli';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporankonsulantarpoli'])) {
            $model->attributes = $_REQUEST['RDLaporankonsulantarpoli'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporankonsulantarpoli']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporankonsulantarpoli']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'konsulPoli/_printKonsulPoli';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanKonsulAntarPoli() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporankonsulantarpoli('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Konsul Antar Poli';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporankonsulantarpoli'])) {
            $model->attributes = $_GET['RDLaporankonsulantarpoli'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporankonsulantarpoli']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporankonsulantarpoli']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanKepenunjang() {
        $model = new RDLaporankepenunjangrd('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');        
        $kepenunjang = CHtml::listData(RuanganpenunjangV::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_id');
        $model->ruanganpenunj_id = $kepenunjang;
        if (isset($_GET['RDLaporankepenunjangrd'])) {
            $model->attributes = $_GET['RDLaporankepenunjangrd'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporankepenunjangrd']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporankepenunjangrd']['tglAkhir']);
        }

        $this->render('kepenunjang/adminKepenunjang', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKepenunjang() {
        $model = new RDLaporankepenunjangrd('search');
        $judulLaporan = 'Laporan Kepenunjang Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kepenunjang';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporankepenunjangrd'])) {
            $model->attributes = $_REQUEST['RDLaporankepenunjangrd'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporankepenunjangrd']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporankepenunjangrd']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kepenunjang/_printKepenunjang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanKepenunjang() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporankepenunjangrd('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kepenunjang';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporankepenunjangrd'])) {
            $model->attributes = $_GET['RDLaporankepenunjangrd'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporankepenunjangrd']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporankepenunjangrd']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBiayaPelayanan() {
        $model = new RDLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE'),'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['RDLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['RDLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanbiayapelayanan']['tglAkhir']);
        }

        $this->render('biayaPelayanan/adminBiayaPelayanan', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanBiayaPelayanan() {
        $model = new RDLaporanbiayapelayanan('search');
        $judulLaporan = 'Laporan Biaya Pelayanan Rawat Darurat';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Biaya Pelayanan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporanbiayapelayanan'])) {
            $model->attributes = $_REQUEST['RDLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanbiayapelayanan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'biayaPelayanan/_printBiayaPelayanan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanBiayaPelayanan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Biaya Pelayanan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['RDLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanbiayapelayanan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPendapatanRuangan() {
        $model = new RDLaporanpendapatanruangan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['RDLaporanpendapatanruangan'])) {
            $model->attributes = $_GET['RDLaporanpendapatanruangan'];
            $model->nama_pegawai = $_GET['RDLaporanpendapatanruangan_nama_pegawai'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpendapatanruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpendapatanruangan']['tglAkhir']);
        }

        $this->render('pendapatanRuangan/adminPendapatanRuangan', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanPendapatanRuangan() {
        $model = new RDLaporanpendapatanruangan('search');
        $judulLaporan = 'Laporan Grafik Pendapatan Ruangan Rawat Darurat';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporanpendapatanruangan'])) {
            $model->attributes = $_REQUEST['RDLaporanpendapatanruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpendapatanruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpendapatanruangan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pendapatanRuangan/_printPendapatanRuangan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPendapatanRuangan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporanpendapatanruangan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporanpendapatanruangan'])) {
            $model->attributes = $_GET['RDLaporanpendapatanruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpendapatanruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpendapatanruangan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanJasaInstalasi() {
        $model = new RDLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $model->tindakansudahbayar_id = Params::statusBayar();
        if (isset($_GET['RDLaporanjasainstalasi'])) {
            $model->attributes = $_GET['RDLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanjasainstalasi']['tglAkhir']);
        }

        $this->render('jasaInstalasi/adminJasaInstalasi', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanJasaInstalasi() {
        $model = new RDLaporanjasainstalasi('search');
        $judulLaporan = 'Laporan Jasa Instalasi Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporanjasainstalasi'])) {
            $model->attributes = $_REQUEST['RDLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanjasainstalasi']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jasaInstalasi/_printJasaInstalasi';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJasaInstalasi() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporanjasainstalasi'])) {
            $model->attributes = $_GET['RDLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanjasainstalasi']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanPemakaiObatAlkes()
    {
        $model = new RDLaporanpemakaiobatalkesV;
        $model->unsetAttributes();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $jenisObat =CHtml::listData($model->getJenisobatalkesItems(),'jenisobatalkes_id','jenisobatalkes_id');
        $model->jenisobatalkes_id = $jenisObat;
        if(isset($_GET['RDLaporanpemakaiobatalkesV']))
        {
            $model->attributes = $_GET['RDLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        $this->render('pemakaiObatAlkes/adminPemakaiObatAlkes',array('model'=>$model));
    }

    public function actionPrintLaporanPemakaiObatAlkes() {
        $model = new RDLaporanpemakaiobatalkesV('search');
        $judulLaporan = 'Laporan Info Pemakai Obat Alkes Rawat Darurat';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_REQUEST['RDLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemakaiObatAlkes/_printPemakaiObatAlkes';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    

    public function actionFrameGrafikLaporanPemakaiObatAlkes() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporanpemakaiobatalkesV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RDLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['RDLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpemakaiobatalkesV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanPasienMeninggal() {
        $model = new RDLaporanpasienmeninggalV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        //$caramasuk = CHtml::listData(CaramasukM::model()->findAll('caramasuk_aktif = true'), 'caramasuk_id', 'caramasuk_id');
        //$model->caramasuk_id = RDLaporanpasienmeninggalV;
        if (isset($_GET['RDLaporanpasienmeninggalV'])) {
            $model->attributes = $_GET['RDLaporanpasienmeninggalV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasienmeninggalV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasienmeninggalV']['tglAkhir']);
        }

        $this->render('pasienMeninggal/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanPasienMeninggal() {
        $model = new RDLaporanpasienmeninggalV('search');
        $judulLaporan = 'Laporan Pasien Meninggal';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Meninggal';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporanpasienmeninggalV'])) {
            $model->attributes = $_REQUEST['RDLaporanpasienmeninggalV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpasienmeninggalV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpasienmeninggalV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pasienMeninggal/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPasienMeninggal() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporanpasienmeninggalV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Meninggal';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['RDLaporanpasienmeninggalV'])) {
            $model->attributes = $_GET['RDLaporanpasienmeninggalV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasienmeninggalV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasienmeninggalV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

   public function actionLaporanTriasePasien() {
        $model = new RDLaporantriasepasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        //$caramasuk = CHtml::listData(CaramasukM::model()->findAll('caramasuk_aktif = true'), 'caramasuk_id', 'caramasuk_id');
        //$model->caramasuk_id = RDLaporanpasienmeninggalV;
        $triase = CHtml::listData(Triase::model()->findAll(), 'triase_id', 'triase_id');
        $model->triase_id = $triase;
        if (isset($_GET['RDLaporantriasepasienV'])) {
            $model->attributes = $_GET['RDLaporantriasepasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporantriasepasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporantriasepasienV']['tglAkhir']);
        }
        if (Yii::app()->request->isAjaxRequest) {
                    echo $this->renderPartial('rawatDarurat.views.laporan.triase._table', array('model'=>$model),true);
                }else{
                   $this->render('triase/index', array(
                    'model' => $model,
                    ));
            }

        
    }

    public function actionPrintLaporanTriasePasien() {
        $model = new RDLaporantriasepasienV('search');
        $judulLaporan = 'Laporan Pasien Meninggal';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Meninggal';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporantriasepasienV'])) {
            $model->attributes = $_REQUEST['RDLaporantriasepasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporantriasepasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporantriasepasienV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'triase/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikTriasePasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporantriasepasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Meninggal';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['RDLaporantriasepasienV'])) {
            $model->attributes = $_GET['RDLaporantriasepasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporantriasepasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporantriasepasienV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }


    public function actionLaporanPasienDirujuk() {
        $model = new RDLaporanpasiendirujukV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        //$caramasuk = CHtml::listData(CaramasukM::model()->findAll('caramasuk_aktif = true'), 'caramasuk_id', 'caramasuk_id');
        $rujuk = CHtml::listData(RujukankeluarM::model()->findAll(), 'rujukankeluar_id', 'rujukankeluar_id');
        $model->rujukankeluar_id = $rujuk;
        if (isset($_GET['RDLaporanpasiendirujukV'])) {
            $model->attributes = $_GET['RDLaporanpasiendirujukV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasiendirujukV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasiendirujukV']['tglAkhir']);
        }

        $this->render('pasienDirujuk/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanPasienDirujuk() {
        $model = new RDLaporanpasiendirujukV('search');
        $judulLaporan = 'Laporan Pasien Meninggal';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Meninggal';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporanpasiendirujukV'])) {
            $model->attributes = $_REQUEST['RDLaporanpasiendirujukV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpasiendirujukV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpasiendirujukV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pasienDirujuk/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPasienDirujuk() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporanpasiendirujukV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Dirujuk';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['RDLaporanpasiendirujukV'])) {
            $model->attributes = $_GET['RDLaporanpasiendirujukV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasiendirujukV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasiendirujukV']['tglAkhir']);
        }
        
        $this->render('_grafik', array( 
            'model' => $model,
            'data' => $data,
        ));
    }

    /**
     * printFunction digunakan oleh hampir semua action jadi jangan edit sembarangan
     * @param type $model
     * @param type $data
     * @param type $caraPrint
     * @param type $judulLaporan
     * @param type $target
     */
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){ //PARAMETER $modDetail, UNTUK APA??
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
//        echo $caraPrint;
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('modDetail'=>$modDetail,'rincianUang'=>$rincianUang,'model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint , 'modRincian'=>$modRincian));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('modDetail'=>$modDetail,'rincianUang'=>$rincianUang,'model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint, 'modRincian'=>$modRincian));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('modDetail'=>$modDetail,'rincianUang'=>$rincianUang,'model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint, 'modRincian'=>$modRincian), true));
            $mpdf->Output();
        }
    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }
    
}
