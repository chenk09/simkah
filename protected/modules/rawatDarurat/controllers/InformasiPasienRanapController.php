<?php

class InformasiPasienRanapController extends SBaseController
{
	public function actionIndex()
	{
        $modPasienYangPulang = new RDInfokunjunganrdV;
        $modPasienYangPulang->tglAwal    = date('Y-m-d H:i:s');
    	$modPasienYangPulang->tglAwal    = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPasienYangPulang->tglAwal, 'yyyy-MM-dd hh:mm:ss','medium',null));

		$modPasienYangPulang->tglAkhir   = date('Y-m-d H:i:s');
		$modPasienYangPulang->tglAkhir	 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPasienYangPulang->tglAkhir, 'yyyy-MM-dd hh:mm:ss','medium',null));

        if(isset($_GET["RDInfokunjunganrdV"])){
			$modPasienYangPulang->attributes	= $_GET["RDInfokunjunganrdV"];
			$modPasienYangPulang->tglAwal		= $_GET['RDInfokunjunganrdV']['tglAwal'];
			$modPasienYangPulang->tglAkhir		= $_GET['RDInfokunjunganrdV']['tglAkhir'];
		}

        $this->render('index',array('modPasienYangPulang'=>$modPasienYangPulang));
	}

    public function actionBatalPulang()
    {
        $return = array();
        $return["status"] = "NO";
        $return["pesan"]  = "Pembatalkan tidak bisa dilakukan";

        if(Yii::app()->request->isAjaxRequest)
        {
            if(!empty($_POST["pendaftaran_id"])){
                $idPendaftaran = $_POST["pendaftaran_id"];
                $pendaftaran = RDPendaftaranT::model()->updateByPk($idPendaftaran, array(
                    'pasienpulang_id'=>null,
                    'statusperiksa'=>Params::statusPeriksa(2)
                ), "pasienadmisi_id IS NULL");

                if ($pendaftaran){
                    $return["status"] = "OK";
                    $return["pesan"]  = "Pembatalkan berhasil dilakukan";
                }
            }
        }
        echo CJSON::encode($return);
    }
}
