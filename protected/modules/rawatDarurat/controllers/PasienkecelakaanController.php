<?php

class PasienkecelakaanController extends SBaseController {

    public function actionPasienKecelakaan() {
        $model = new RDLaporanpasienkecelakaanV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
//       $jenis = CHtml::listData(JeniskecelakaanM::model()->findAll('jeniskecelakaan_aktif = true'), 'jeniskecelakaan_id', 'jeniskecelakaan_nama');
//        $model->jeniskecelakaan_id = RDLaporanpasienkecelakaanV;
        if (isset($_GET['RDLaporanpasienkecelakaanV'])) {
            $model->attributes = $_GET['RDLaporanpasienkecelakaanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasienkecelakaanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasienkecelakaanV']['tglAkhir']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanPasienKecelakaan() {
        $model = new RDLaporanpasienkecelakaanV('search');
        $judulLaporan = 'Laporan Pasien Kecelakaan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Kecelakaan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RDLaporanpasienkecelakaanV'])) {
            $model->attributes = $_REQUEST['RDLaporanpasienkecelakaanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpasienkecelakaanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDLaporanpasienkecelakaanV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPasienKecelakaan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RDLaporanpasienkecelakaanV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Kecelakaan';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['RDLaporanpasienkecelakaanV'])) {
            $model->attributes = $_GET['RDLaporanpasienkecelakaanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasienkecelakaanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RDLaporanpasienkecelakaanV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
   protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }

}
?>
