<?php
Yii::import('application.controllers.ResepturController');
class ResepturRawatDaruratController extends ResepturController
{
	public function actionIndex($idPendaftaran, $idAdmisi = null)
	{
	    if(!empty($idAdmisi))
        {
            $modAdmisi = PasienadmisiT::model()->findByAttributes(array(
                'pendaftaran_id'=>$idPendaftaran,
                'pasienadmisi_id'=>$idAdmisi
            ));
        }else{
            $modAdmisi = new PasienadmisiT();
        }

        $modPendaftaran = RDPendaftaranT::model()->findByPk($idPendaftaran);
        $modPasien = RDPasienM::model()->findByPk($modPendaftaran->pasien_id);
        $modDiagnosa = new RDDiagnosaM;

        $modReseptur = new RDResepturT;
        $instalasi_id = Yii::app()->user->getState('instalasi_id');
        $modReseptur->noresep = MyGenerator::noReseptur($instalasi_id);
        $modReseptur->pegawai_id = $modPendaftaran->pegawai_id;
        $modReseptur->ruanganreseptur_id = Yii::app()->user->getState('ruangan_id');
        $modReseptur->pasien_id = $modPendaftaran->pasien_id;
        $modReseptur->pendaftaran_id = $modPendaftaran->pendaftaran_id;
        $modReseptur->pasienadmisi_id = $idAdmisi;
        $modReseptur->ruangan_id = 20;

        $modMorbiditas = new RDPasienMorbiditasT();
        $golUmur = Logic::cekGolonganUmur($modPendaftaran->golonganumur_id);
        $modMorbiditas->pendaftaran_id = $modPendaftaran->pendaftaran_id;
        $modMorbiditas->pasien_id = $modPendaftaran->pasien_id;
        $modMorbiditas->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $modMorbiditas->kelompokumur_id = $modPasien->kelompokumur_id;
        $modMorbiditas->golonganumur_id = $modPendaftaran->golonganumur_id;
        $modMorbiditas->$golUmur = 1;
        $modMorbiditas->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
        $modMorbiditas->pegawai_id = $modPendaftaran->pegawai_id;
        $modMorbiditas->infeksinosokomial = '0';

        if(Yii::app()->request->isPostRequest && isset($_POST['RDResepturT']))
        {
            $this->layout = false;
            $vRest = $this->postingReseptur($modReseptur, $modMorbiditas, $_POST);
            echo json_encode($vRest);
            Yii::app()->end();
        }

        $this->render('index',array(
            'modMorbiditas'=>$modMorbiditas,
            'modPendaftaran'=>$modPendaftaran,
            'modPasien'=>$modPasien,
            'modDiagnosa'=>$modDiagnosa,
            'modReseptur'=>$modReseptur,
            'modAdmisi'=>$modAdmisi
        ));
	}

}