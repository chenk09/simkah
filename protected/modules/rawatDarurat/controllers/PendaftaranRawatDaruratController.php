<?php
//Yii::import('pendaftaranPenjadwalan.controllers.PendaftaranController');
class PendaftaranRawatDaruratController extends PendaftaranController
{
//    
//        public $successSave = false;
//        public $successSaveAdmisi = false; //variabel untuk validasi admisi
//        public $successSaveRujukan = true; //variabel untuk validasi data opsional (rujukan)
//        public $successSavePJ = true; //variabel untuk validasi data opsional (penanggung jawab)
//        
//        /**
//	 * @return array action filters
//	 */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}
//
//	/**
//	 * Specifies the access control rules.
//	 * This method is used by the 'accessControl' filter.
//	 * @return array access control rules
//	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
//        
//	public function actionIndex()
//	{
//            $this->pageTitle = Yii::app()->name." - Pendaftaran Rawat Darurat";
//            $model=new RDPendaftaran;
//            $model->kelaspelayanan_id = Params::KELASPELAYANAN_RD; // menset default kelas pelayanan rawat darurat
//            $model->tgl_pendaftaran = date('d M Y H:i:s');
//            $model->umur = '00 Thn 00 Bln 00 Hr';
//            $modPasien = new RDPasienM;
//            $modPasien->tanggal_lahir = date('d M Y');
//            $modPenanggungJawab = new RDPenanggungJawabM;
//            $modRujukan = new RDRujukanT;
//
//            if(isset($_POST['RDPendaftaran']))
//            {
//                $model->attributes = $_POST['RDPendaftaran'];
//                $modPasien->attributes = $_POST['RDPasienM'];
//                $modPenanggungJawab->attributes = $_POST['RDPenanggungJawabM'];
//                $modRujukan->attributes = $_POST['RDRujukanT'];
//                
//                $transaction = Yii::app()->db->beginTransaction();
//                try {
//                    if(!isset($_POST['isPasienLama'])) {
//                        $modPasien = $this->savePasien($_POST['RDPasienM']);
//                    } else {
//                        $model->isPasienLama = true;
//                        if(!empty($_POST['noRekamMedik'])) {
//                            $model->noRekamMedik = $_POST['noRekamMedik'];
//                            $modPasien = RDPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
//                            if(!empty($modPasien) && isset($_POST['isUpdatePasien'])) {
//                                $modPasien = $this->updatePasien($modPasien, $_POST['RDPasienM']);
//                            }
//                        }else{
//                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
//                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
//                        }
//                    }
//                    
//                    if(isset($_POST['isRujukan'])) {
//                        $model->isRujukan = true;
//                        $model->statusmasuk = 'RUJUKAN';
//                        $modRujukan = $this->saveRujukan($_POST['RDRujukanT']);
//                    } else {
//                        $model->statusmasuk = 'NON RUJUKAN';
//                    }
//                    
//                    if(isset($_POST['adaPenanggungJawab'])) {
//                        $model->adaPenanggungJawab = true;
//                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['RDPenanggungJawabM']);
//                    }
//                    
//                    if(count($modPasien)>0)
//                        $model = $this->savePendaftaranRD($model,$modPasien,$modRujukan,$modPenanggungJawab);
//                    else {
//                        $this->successSave = false;
//                        $modPasien = new RDPasienM;
//                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
//                        Yii::app()->user->setFlash('danger',"Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar!");
//                    }
//                    $this->saveUbahCaraBayar($model);
//                    
//                    if($this->successSave && $this->successSaveRujukan && $this->successSavePJ) {
//                        $transaction->commit();
//                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
//                    } else {
//                        $transaction->rollback();
//                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
//                        $model->isNewRecord = TRUE;
//                    }
//                    
//                } catch (Exception $exc) {
//                    $transaction->rollback();
//                    $model->isNewRecord = TRUE;
//                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
//                }
//            }
//            $model->isRujukan = (isset($_POST['isRujukan'])) ? true : false;
//            $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
//            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
//            $model->adaPenanggungJawab = true;
//            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
//            
//            $this->render('index',array('model'=>$model,
//                                       'modPasien'=>$modPasien,
//                                       'modPenanggungJawab'=>$modPenanggungJawab,
//                                       'modRujukan'=>$modRujukan));
//	}
//        
//        public function savePasien($attrPasien)
//        {
//            $modPasien = new RDPasienM;
//            $modPasien->attributes = $attrPasien;
//            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
//            $modPasien->no_rekam_medik = Generator::noRekamMedik();
//            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
//            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
//            $modPasien->statusrekammedis = 'AKTIF';
//            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');
//            
//            if($modPasien->validate()) {
//                // form inputs are valid, do something here
//                $modPasien->save();
//            } else {
//                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
//                $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
//                                                                CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
//            }
//            return $modPasien;
//        }
//        
//        public function updatePasien($modPasien,$attrPasien)
//        {
//            $modPasienupdate = $modPasien;
//            $modPasienupdate->attributes = $attrPasien;
//            $modPasienupdate->kelompokumur_id = Generator::kelompokUmur($modPasienupdate->tanggal_lahir);
//            
//            if($modPasienupdate->validate()) {
//                // form inputs are valid, do something here
//                $modPasienupdate->save();
//            } else {
//                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
//                $modPasienupdate->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
//                                                                CDateTimeParser::parse($modPasienupdate->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
//            }
//            
//            return $modPasienupdate;
//        }
//        
//        public function savePenanggungJawab($attrPenanggungJawab)
//        {
//            $modPenanggungJawab = new RDPenanggungJawabM;
//            $modPenanggungJawab->attributes = $attrPenanggungJawab;
//            if($modPenanggungJawab->validate()) {
//                // form inputs are valid, do something here
//                $modPenanggungJawab->save();
//                $this->successSavePJ = TRUE;
//            } else {
//                // mengembalikan format tanggal contoh 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
//                //if($modPenanggungJawab->tgllahir_pj != null)
//                //$modPenanggungJawab->tgllahir_pj = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenanggungJawab->tgllahir_pj, 'yyyy-MM-dd'),'medium',null);
//                $this->successSavePJ = FALSE;
//            }
//
//            return $modPenanggungJawab;
//        }
//        
//        public function saveRujukan($attrRujukan)
//        {
//            $modRujukan = new RDRujukanT;
//            $modRujukan->attributes = $attrRujukan;
//            $modRujukan->tanggal_rujukan = ($attrRujukan['tanggal_rujukan'] == '') ? date('Y-m-d') : $attrRujukan['tanggal_rujukan'] ;
//            if($modRujukan->validate()) {
//                // form inputs are valid, do something here
//                $modRujukan->save();
//                $this->successSaveRujukan = TRUE;
//            } else {
//                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
//                $modRujukan->tanggal_rujukan = Yii::app()->dateFormatter->formatDateTime(
//                                                                CDateTimeParser::parse($modRujukan->tanggal_rujukan, 'yyyy-MM-dd'),'medium',null);
//                $this->successSaveRujukan = FALSE;
//            }
//            return $modRujukan;
//        }
//        
//        public function saveUbahCaraBayar($model) 
//        {
//            $modUbahCaraBayar = new RDUbahCaraBayarR;
//            $modUbahCaraBayar->pendaftaran_id = $model->pendaftaran_id;
//            $modUbahCaraBayar->carabayar_id = $model->carabayar_id;
//            $modUbahCaraBayar->penjamin_id = $model->penjamin_id;
//            $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d');
//            $modUbahCaraBayar->alasanperubahan = 'x';
//            if($modUbahCaraBayar->validate())
//            {
//                // form inputs are valid, do something here
//                $modUbahCaraBayar->save();
//            }
//            
//        }
//        
//        public function savePendaftaranRD($model,$modPasien,$modRujukan,$modPenanggungJawab)
//        {
//            $modelNew = new RDPendaftaran;
//            $modelNew->attributes = $model->attributes;
//            $modelNew->pasien_id = $modPasien->pasien_id;
//            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
//            $modelNew->rujukan_id = $modRujukan->rujukan_id;
//            $modelNew->no_pendaftaran = Generator::noPendaftaran('RD');
//            $modelNew->instalasi_id = Generator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
//            $modelNew->no_urutantri = Generator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
//            $modelNew->golonganumur_id = Generator::golonganUmur($modPasien->tanggal_lahir);
//            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
//            $modelNew->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
//            $modelNew->statuspasien = Generator::getStatusPasien($modPasien);
//            $modelNew->statusperiksa = Params::statusPeriksa(1);
//            $modelNew->kunjungan = Generator::getKunjungan($modPasien, $modelNew->ruangan_id);
//            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
//            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
//            
//            if($modelNew->validate()) {
//                // form inputs are valid, do something here
//                $modelNew->save();
//                $this->successSave = true;
//            } else {
//                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
//                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
//                                                                CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'),'medium',null);
//            }
//            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
//                    
//            return $modelNew;
//        }
//        

}