<?php

class InformasiJadwalDokterController extends SBaseController
{
        /**
	 * @return array action filters
	 */
        public $_lastHari = null;
        
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{
            $this->pageTitle = Yii::app()->name." - Informasi Jadwal Dokter";
            $model=new RDJadwaldokterM;
            $listHari = array( 'Senin'=> 'Senin',
                                   'Selasa'=> 'Selasa',
                                   'Rabu'=> 'Rabu',
                                   'Kamis'=> 'Kamis',
                                   'Jumat'=> 'Jumat',
                                   'Sabtu'=> 'Sabtu',
                                   'Minggu'=> 'Minggu',
                                );
            if(isset($_REQUEST['RDJadwaldokterM'])){
                $model->attributes = $_REQUEST['RDJadwaldokterM'];

            }
            $this->render('index',
                    array('model'=>$model,'listHari'=>$listHari)
                    );
	}
        
        protected function gridHari($data,$row)
        {
           if($this->_lastHari != $data->jadwaldokter_hari)
           {
               return $data->jadwaldokter_hari;
           }
           else{
               return '';
           }
        }
        
        protected function gridDokter($data,$row)
        {
           $this->_lastHari = $data->jadwaldokter_hari;
           return $data->pegawai->nama_pegawai;  
        }

	
}