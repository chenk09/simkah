<?php

class DaftarPasienController extends SBaseController
{
        public $validRujukan = false;
        public $validPulang = false;
        /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','batalRawatInap', 'pasienPulang'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionIndex()
    {
        $format = new CustomFormat();
        $this->pageTitle = Yii::app()->name." - Daftar Pasien Rawat Darurat";
        $model = new RDInfokunjunganrdV;

        $model->tglAwal = date("d M Y").' 00:00:00';
        $model->tglAkhir = date('d M Y').' 23:59:59';

        $model->ceklis = true;
        $model->pasienpulang_id = null;

        if(isset ($_REQUEST['RDInfokunjunganrdV'])){
            $model->attributes=$_REQUEST['RDInfokunjunganrdV'];
            $model->ceklis = $_REQUEST['RDInfokunjunganrdV']['ceklis'];

            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RDInfokunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RDInfokunjunganrdV']['tglAkhir']);
        }
        $this->render('index',array('model'=>$model));
    }

    public function actionBatalRawatInap($idPendaftaran){
        $this->layout='//layouts/frameDialog';
        $modPendaftaran = PendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
        $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);

        $modPasienBatalPulang = new PasienbatalpulangT;
        $tersimpan='tidak';

            if(!empty($_POST['PasienbatalpulangT'])){
                $pasienPulangId = $_POST['pasienpulang_id'];
                $idPendaftaran = $_POST['pendaftaran_id'];
                $format = new CustomFormat();
                $modPasienBatalPulang->attributes = $_POST['PasienbatalpulangT'];
                $modPasienBatalPulang->create_time = date('Y-m-d H:i:s');
                $modPasienBatalPulang->update_time = date('Y-m-d H:i:s');
                $modPasienBatalPulang->tglpembatalan = $format->formatDateTimeMediumForDB($modPasienBatalPulang->tglpembatalan);
                $modPasienBatalPulang->namauser_otorisasi = Yii::app()->user->name;
                $modPasienBatalPulang->iduser_otorisasi = Yii::app()->user->id;
                $modPasienBatalPulang->create_loginpemakai_id = Yii::app()->user->id;
                $modPasienBatalPulang->update_loginpemakai_id = Yii::app()->user->id;
                $modPasienBatalPulang->create_ruangan = Yii::app()->user->getState('ruangan_id');
                $modPasienBatalPulang->pasienpulang_id = $pasienPulangId;
                if($modPasienBatalPulang->validate()){
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        if($modPasienBatalPulang->save()){
                            $pulang =  PasienpulangT::model()->updateByPk($pasienPulangId,array('pasienbatalpulang_id'=>$modPasienBatalPulang->pasienbatalpulang_id));
                            $pendaftaran =  PendaftaranT::model()->updateByPk($idPendaftaran,array('pasienpulang_id'=>null,'statusperiksa'=>'SEDANG PERIKSA'));
                            if ($pulang && $pendaftaran){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                                $tersimpan='Ya';
    //
                            }
                            else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan");
                            }
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpanx");
                        }
                    } catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan", MyExceptionMessage::getMessage($exc,false));
                    }
                } else{
                     Yii::app()->user->setFlash('error',"Data gagal disimpan");
                }

            }
            $this->render('formBatalRawatInap',array('modPasien'=>$modPasien,'modPendaftaran'=>$modPendaftaran, 'modPasienBatalPulang'=>$modPasienBatalPulang, 'tersimpan'=>$tersimpan));
        }

        /**
         * actionPasienPulang = transaksi - pasien pulang
         */
        public function actionPasienPulang($idPendaftaran = null, $dialog = false, $is_ranap = false)
        {
            if($dialog) $this->layout='//layouts/frameDialog';
            $tersimpan  = false;
            $status     = false;

            if(!empty($idPendaftaran)){
                $modPendaftaran = RDPendaftaranT::model()->findByPk($idPendaftaran);
                if(!$modPendaftaran) Yii::app()->user->setFlash('error','Pendaftaran Tidak Ditemukan !');
                else $modPasien = RDPasienM::model()->findByPk($modPendaftaran->pasien_id);

                if(!empty($modPendaftaran->pasienpulang_id)){
                    if($status == false){
                        echo "Pasien Telah Di Tindak Lanjut Dari Rawat Darurat !";
                        exit;
                    }
                }

                $ruangan = array();
                if($modPendaftaran->statusfarmasi == false){
                    $ruangan[] = "+ Ruangan : APOTEK FARMASI > Status pasien belum dikonfirmasi";
                }
                $modPasienKirimUnit = PasienkirimkeunitlainT::model()->findAllByAttributes(array(
                    'pendaftaran_id'=>$idPendaftaran,
                    'pasienmasukpenunjang_id'=>null
                ));
                if(count($modPasienKirimUnit) > 0){
                    foreach($modPasienKirimUnit as $key=>$kirimPasien){
                        if($kirimPasien->ruangan_id != Params::RUANGAN_ID_GIZI){
                            $ruangan[] = "+ Ruangan : ". $kirimPasien->ruangan->ruangan_nama . " Tgl Rujuk Pasien : ".$kirimPasien->tgl_kirimpasien;
                        }
                    }
                }

                if(count($ruangan) > 0 && $is_ranap == false){
                    $status = true;
                    Yii::app()->user->setFlash('error',"Pasien Belum Ditindak Lanjut Di : <br> " . implode("<br>", $ruangan));
                }

            }else{
                $modPendaftaran = new RDPendaftaranT;
                $modPasien = new RDPasienM;
            }

            $modelPulang = new PasienpulangT;
            $modRujukanKeluar = new PasiendirujukkeluarT;

            $modelPulang->tglpasienpulang = date('d M Y H:i:s');
            $modelPulang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modelPulang->pasien_id = $modPasien->pasien_id;

            $modRujukanKeluar->pegawai_id = PendaftaranT::model()->findByPk($idPendaftaran)->pegawai_id;
            $modRujukanKeluar->ruanganasal_id = Yii::app()->user->getState('ruangan_id'); //ruangan asal itu diasumsikan ruangan terakhir dia dari mana
            $modRujukanKeluar->tgldirujuk = date('d M Y H:i:s');

            $format  = new CustomFormat();
            $date1   = $format->formatDateTimeMediumForDB($modPendaftaran->tgl_pendaftaran);
            $date2   = date('Y-m-d H:i:s');
            $diff    = abs(strtotime($date2) - strtotime($date1));
            $hours   = floor(($diff)/3600);
            $modelPulang->lamarawat = $hours;

            if(isset($_POST['PasienpulangT']))
            {
                if(!empty($_POST['RDPendaftaranT']['pendaftaran_id'])) $modPendaftaran = $modPendaftaran->findByPk($_POST['RDPendaftaranT']['pendaftaran_id']);
                if(!empty($_POST['RDPasienM']['pasien_id'])) $modPasien = $modPasien->findByPk($_POST['RDPasienM']['pasien_id']);

                $transaction = Yii::app()->db->beginTransaction();
                try{
                    $modelPulang = $this->savePasienPulang($modelPulang,$_POST['PasienpulangT']);
                    if(isset($_POST['pakeRujukan'])){
                        $modelPulang->pakeRujukan = true;
                        $modRujukanKeluar = $this->saveRujukanKeluar($modRujukanKeluar,$modelPulang,$_POST['PasiendirujukkeluarT']);
                    }
                    $this->validRujukan = true;

                    if(isset($_POST['isDead'])){
                        $modPasien = PasienM::model()->findByPk($_POST['PasienpulangT']['pasien_id']);
                        $modPasien->tgl_meninggal = $_POST['PasienpulangT']['tgl_meninggal'];
                        $modPasien->save();
                    }

                    if($this->validPulang && $this->validRujukan){
                        $statusperiksa = $_POST['PasienpulangT']['carakeluar'];
                        $status = "SUDAH PULANG";
                        if($statusperiksa == 'DIRAWAT INAP'){
                            $status = "SEDANG DIRAWAT INAP";
                        }

                        PendaftaranT::model()->updateByPk($modelPulang->pendaftaran_id, array(
                            'tglselesaiperiksa'=>date( 'Y-m-d H:i:s'),
                            'pasienpulang_id'=>$modelPulang->pasienpulang_id,
                            'statusperiksa'=>$status,
                            'kirim_farmasi'=>'f'
                        ));
/*
                        $_model = new LogKirimFarmasi;
                        $_model->pendaftaran_id = $modelPulang->pendaftaran_id;
                        $_model->komentar = 'KIRIM KE FARMASI';
                        $_model->save();
*/
                        $transaction->commit();
                        Yii::app()->user->setFlash('success','Data berhasil disimpan !');

                        if($dialog){
                            $tersimpan = true;
						}else $this->redirect(Yii::app()->createUrl($this->route)); //refresh dgn menghilangkan $_get
                    }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }

            if($is_ranap){
                $modelPulang->carakeluar = "DIRAWAT INAP";
                $modelPulang->kondisipulang = "BELUM SEMBUH";
				
                $this->render('formKirimRanap', array(
                    'modPendaftaran'=>$modPendaftaran,
                    'modPasien'=>$modPasien,
                    'modelPulang'=>$modelPulang,
                    'modRujukanKeluar'=>$modRujukanKeluar,
                    'tersimpan'=>$tersimpan,
                    'status'=>$status
                ));
            }else{
                $this->render('formPasienPulang', array(
                    'modPendaftaran'=>$modPendaftaran,
                    'modPasien'=>$modPasien,
                    'modelPulang'=>$modelPulang,
                    'modRujukanKeluar'=>$modRujukanKeluar,
                    'tersimpan'=>$tersimpan,
                    'status'=>$status
                ));
            }
        }

        protected function savePasienPulang($modPasienPulang,$attrPasienPulang,$idPasienAdmisi='')
        {
            $modelPulangNew = new PasienpulangT;
            $modelPulangNew->attributes = $attrPasienPulang;
            $modelPulangNew->satuanlamarawat = (Yii::app()->user->getState('instalasi_id') == Params::INSTALASI_ID_RD) ? Params::SATUANLAMARAWAT_RD : Params::SATUANLAMARAWAT_RI;
            $modelPulangNew->ruanganakhir_id = Yii::app()->user->getState('ruangan_id');
            $modelPulangNew->create_time = date( 'Y-m-d H:i:s');
            $modelPulangNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelPulangNew->create_loginpemakai_id = Yii::app()->user->id;
            $modelPulangNew->pasienadmisi_id = (Yii::app()->user->getState('instalasi_id') == Params::INSTALASI_ID_RD) ? null : $idPasienAdmisi;

            if($modelPulangNew->save())
            {
                $this->validPulang = true;
            }

            return $modelPulangNew;
        }
        protected function saveRujukanKeluar($modRujukanKeluar,$modelPulang,$attrRujukanKeluar)
        {
            $modRujukanKeluarNew = new PasiendirujukkeluarT;
            $modRujukanKeluarNew->attributes = $attrRujukanKeluar;
            $modRujukanKeluarNew->pendaftaran_id = $modelPulang->pendaftaran_id;
            $modRujukanKeluarNew->pasien_id = $modelPulang->pasien_id;
            $modRujukanKeluarNew->create_time = date( 'Y-m-d H:i:s');
            $modRujukanKeluarNew->create_loginpemakai_id = Yii::app()->user->id;
            if($modRujukanKeluarNew->save()){
                $this->validRujukan = true;
            }
            else{
                $this->validRujukan = false;
            }
            return $modRujukanKeluarNew;
        }
}

?>
