<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ActionAjaxController extends SBaseController
{
        
         public function actionCekHakAkses()
        {
            if(!Yii::app()->user->checkAccess('Administrator')){
                //throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));
                $data['cekAkses'] = false;
            } else {
                $pendaftaran = PendaftaranT::model()->findByPk($_POST['idPendaftaran']);
                $data['pendaftaran'] = $pendaftaran->attributes;
                //echo 'punya hak akses';
                $pasienAdmisi = PasienadmisiT::model()->findByAttributes(array('pendaftaran_id'=>$pendaftaran->pendaftaran_id));
                $ruangan_m = RuanganM::model()->findByPk($pasienAdmisi->ruangan_id);
//                $kamar_m = KamarruanganM::model()->findByPk($pasienAdmisi->kamarruangan_id);
                $data['ruanganPasien'] =  $ruangan_m->ruangan_nama;
//                $data['nokamarPasien'] = $kamar_m->kamarruangan_nokamar;
//                $data['nobedPasien'] = $kamar_m->kamarruangan_nobed;
                $data['cekAkses'] = true;
                $data['userid'] = Yii::app()->user->id;
                $data['username'] = Yii::app()->user->name;
            }
            
            echo CJSON::encode($data);
            Yii::app()->end();
        }
        
        public function actiondataPasien()
        {
            $idPasien=$_POST['idPasien'];
            $idPendaftaran=$_POST['idPendaftaran'];
            $modPasien = RIPasienM::model()->findByPk($idPasien);
            $modPendaftaran = RIPendaftaranT::model()->findByPk($idPendaftaran);
            $form=$this->renderPartial('/_ringkasDataPasien', array('modPasien'=>$modPasien,
                                                                           'modPendaftaran'=>$modPendaftaran,
                                                                               ), true);
            
            $data['form']=$form;
                       echo CJSON::encode($data);

        }  
}

?>
