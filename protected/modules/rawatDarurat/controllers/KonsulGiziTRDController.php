<?php
Yii::import('rawatJalan.controllers.KonsulGiziController');
Yii::import('rawatJalan.models.*');
class KonsulGiziTRDController extends KonsulGiziController
{
        
}
//class KonsulGiziController extends SBaseController
//{
//    protected $statusSaveKirimkeUnitLain = false;
//    protected $statusSavePermintaanPenunjang = false;
//    
//	public function actionIndex($idPendaftaran)
//	{
//            $modPendaftaran = RDPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
//            $modPasien = RDPasienM::model()->findByPk($modPendaftaran->pasien_id);
//            $modKirimKeUnitLain = new RDPasienKirimKeUnitLainT;
//            $modKirimKeUnitLain->tgl_kirimpasien = date('Y-m-d H:i:s');
//            $modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
//            
//            if(isset($_POST['RDPasienKirimKeUnitLainT'])) {
//                $transaction = Yii::app()->db->beginTransaction();
//                try {
//                    $modKirimKeUnitLain = $this->savePasienKirimKeUnitLain($modPendaftaran);
//                    if(isset($_POST['permintaanPenunjang'])){
//                        $this->savePermintaanPenunjang($_POST['permintaanPenunjang'],$modKirimKeUnitLain);
//                    } else {
//                        $this->statusSavePermintaanPenunjang = true;
//                    }
//                    
//                    if($this->statusSaveKirimkeUnitLain && $this->statusSavePermintaanPenunjang){
//                        $transaction->commit();
//                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
//                    } else {
//                        $transaction->rollback();
//                        Yii::app()->user->setFlash('error',"Data tidak valid ");
//                    }
//                } catch (Exception $exc) {
//                    $transaction->rollback();
//                    Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
//                }
//                
//            }
//		
//            $modRiwayatKirimKeUnitLain = RDPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
//                                                                                                      'instalasi_id'=>Params::INSTALASI_ID_GIZI),
//                                                                                                'pasienmasukpenunjang_id IS NULL');
//            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
//                                        'modPasien'=>$modPasien,
//                                        'modKirimKeUnitLain'=>$modKirimKeUnitLain,
//                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,));
//	}
//
//        protected function savePasienKirimKeUnitLain($modPendaftaran)
//        {
//            $modKirimKeUnitLain = new RDPasienKirimKeUnitLainT;
//            $modKirimKeUnitLain->attributes = $_POST['RDPasienKirimKeUnitLainT'];
//            $modKirimKeUnitLain->pasien_id = $modPendaftaran->pasien_id;
//            $modKirimKeUnitLain->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//            $modKirimKeUnitLain->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//            $modKirimKeUnitLain->instalasi_id = Params::INSTALASI_ID_GIZI;
//            $modKirimKeUnitLain->nourut = Generator::noUrutPasienKirimKeUnitLain($modKirimKeUnitLain->ruangan_id);
//            if($modKirimKeUnitLain->validate()){
//                $modKirimKeUnitLain->save();
//                $this->statusSaveKirimkeUnitLain = true;
//            }
//            
//            return $modKirimKeUnitLain;
//        }
//        
//        protected function savePermintaanPenunjang($permintaan,$modKirimKeUnitLain)
//        {
//            foreach ($permintaan as $i=>$item) {
//                if(isset($item['cbTindakan'])){
//                    $modPermintaan = new RDPermintaanPenunjangT;
//                    $modPermintaan->daftartindakan_id = $item['idDaftarTindakan'];     //$permintaan['idDaftarTindakan'][$i];
//                    $modPermintaan->pemeriksaanlab_id = '';
//                    $modPermintaan->pemeriksaanrad_id = '';
//                    $modPermintaan->pasienkirimkeunitlain_id = $modKirimKeUnitLain->pasienkirimkeunitlain_id;
//                    $modPermintaan->noperminatanpenujang = Generator::noPermintaanPenunjang('PG');
//                    $modPermintaan->qtypermintaan = 1;
//                    $modPermintaan->tglpermintaankepenunjang = $modKirimKeUnitLain->tgl_kirimpasien; //date('Y-m-d H:i:s');
//                    if($modPermintaan->validate()){
//                        $modPermintaan->save();
//                        $this->statusSavePermintaanPenunjang = true;
//                    }
//                }
//            }
//        }
//        
//        public function actionAjaxBatalKirim()
//        {
//            if(Yii::app()->request->isAjaxRequest) {
//            $idPasienKirimKeUnitLain = $_POST['idPasienKirimKeUnitLain'];
//            $idPendaftaran = $_POST['idPendaftaran'];
//            
//            PermintaankepenunjangT::model()->deleteAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
//            PasienkirimkeunitlainT::model()->deleteByPk($idPasienKirimKeUnitLain);
//            $modRiwayatKirimKeUnitLain = RDPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
//                                                                                                      'instalasi_id'=>Params::INSTALASI_ID_GIZI),
//                                                                                                'pasienmasukpenunjang_id IS NULL');
//            
//            $data['result'] = $this->renderPartial('_listKirimKeUnitLain', array('modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain), true);
//
//            echo json_encode($data);
//             Yii::app()->end();
//            }
//        }
//
//	// Uncomment the following methods and override them if needed
//	/*
//	public function filters()
//	{
//		// return the filter configuration for this controller, e.g.:
//		return array(
//			'inlineFilterName',
//			array(
//				'class'=>'path.to.FilterClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//
//	public function actions()
//	{
//		// return external action classes, e.g.:
//		return array(
//			'action1'=>'path.to.ActionClass',
//			'action2'=>array(
//				'class'=>'path.to.AnotherActionClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//	*/
//}