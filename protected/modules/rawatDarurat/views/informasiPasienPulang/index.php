<legend class="rim2">Informasi Pasien Pulang</legend>
<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasienPulang-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasienPulang-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
//echo Yii::app()->user->getState('ruangan_id');
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarPasienPulang-grid',
	'dataProvider'=>$modPasienYangPulang->searchRD(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
//                'tglpasienpulang',
                array(
                    'header'=>'Tgl Pasien Pulang',
                    'value'=>'$data->tglpasienpulang',
                ),
                array(
                    'header'=>'Cara/ Kondisi Pulang',
                    'type'=>'raw',
                    'value'=>'$data->CaradanKondisiPulang'
                ),
               
                array(
                    'header'=>'Tgl Pendaftaran',
                    'value'=>'$data->tgl_pendaftaran',
                ),
//                'tgladmisi',
                array(
                    'header'=>'No Rekam Medik / <BR> No Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->NoRMdanNoPendaftaran'
                ),
            
                array(
                    'header'=>'Nama / Alias',
                    'type'=>'raw',
                    'value'=>'$data->NamadanNamaBIN'
                ),    
//                'umur',
//                 array(
//                       'header'=>'Cara Bayar/ Penjamin',
//                        'type'=>'raw',
//                        'value'=>'$data->CaraBayardanPenjamin'
//                    ),
                array(
                    'header'=>'Kelas Pelayanan',
                    'type'=>'raw',
                    'value'=>'$data->KelasPelayanan'
                ),   
                array(
                    'header'=>'Nama Jenis Kasus Penyakit',
                    'value'=>'$data->jeniskasuspenyakit_nama',
                ),
//                'jeniskasuspenyakit_nama',

//                array(
//                       'header'=>'Batal Pulang',
//                       'type'=>'raw',
//                       'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ","javascript:cekHakAkses($data->pasienpulang_id,$data->pasienadmisi_id,$data->pasien_id,$data->pendaftaran_id)" ,array("title"=>"Klik Untuk Membatalkan Kepulangan"))',
//                    ),
                array(
                       'header'=>'Batal Pulang',
                       'type'=>'raw',
                       'value'=>'CHtml::link("<i class=\'icon-remove\'></i>", 
                           Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/batalPulang",array("idPendaftaran"=>$data->pendaftaran_id)),
                               array("title"=>"Klik untuk Batal Pulang", "target"=>"iframeBatalPulang", "onclick"=>"$(\"#dialogBatalPulang\").dialog(\"open\");", "rel"=>"tooltip"))',
                       'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
              
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
echo CHtml::hiddenField('idPasien','',array('readonly'=>TRUE));
echo CHtml::hiddenField('idPendaftaran','',array('readonly'=>TRUE));
?>

<?php echo $this->renderPartial('_formPencarian', array('modPasienYangPulang'=>$modPasienYangPulang)); ?>
<?php 
// Dialog untuk batal Rawat Darurat =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogBatalPulang',
    'options'=>array(
        'title'=>'Pembatalan Pulang Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>500,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeBatalPulang" width="100%" height="550">
</iframe>
<?php $this->endWidget(); ?>
<script type="text/javascript">
  
</script>