<?php
$this->breadcrumbs=array(
	'Rehab Medis',
);

$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));

$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'Anamnesis', 'url'=>$this->createUrl('/rawatDarurat/anamnesa',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Periksa Fisik', 'url'=>$this->createUrl('/rawatDarurat/pemeriksaanFisik',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Laboratorium', 'url'=>$this->createUrl('/rawatDarurat/laboratorium',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Radiologi', 'url'=>$this->createUrl('/rawatDarurat/radiologi',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rehab Medis', 'url'=>'', 'active'=>true),
        array('label'=>'Konsultasi Gizi', 'url'=>$this->createUrl('/rawatDarurat/konsulGizi',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Konsul Poliklinik', 'url'=>$this->createUrl('/rawatDarurat/konsulPoli',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Tindakan', 'url'=>$this->createUrl('/rawatDarurat/tindakan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
        array('label'=>'Diagnosis', 'url'=>$this->createUrl('/rawatDarurat/diagnosa',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Bedah Sentral', 'url'=>$this->createUrl('/rawatDarurat/bedahSentral',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rujukan Ke Luar', 'url'=>$this->createUrl('/rawatDarurat/rujukanKeluar',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Reseptur', 'url'=>$this->createUrl('/rawatDarurat/reseptur',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Pemakaian Bahan', 'url'=>$this->createUrl('/rawatDarurat/pemakaianBahan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
    ),
));
?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjpasien-rehabMedis-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInput()'),
)); ?>

    <?php $this->renderPartial('_listKirimKeUnitLain',array('modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain)) ?>

<div class="formInputTab">
    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($modKirimKeUnitLain, $modPasienMasukPenunjang); ?>
    
    <div class="box">
        <?php foreach($modJenisTindakanRm as $i=>$jenisPeriksa){ 
                $ceklist = false;
        ?>
                <div class="boxtindakan">
                    <h6><?php echo $jenisPeriksa->jenistindakanrm_nama; ?></h6>
                    <?php foreach ($modTindakanRm as $j => $pemeriksaan) {
                             if($jenisPeriksa->jenistindakanrm_id == $pemeriksaan->jenistindakanrm_id) {
                                 echo CHtml::checkBox("tindakanRM[]", $ceklist, array('value'=>$pemeriksaan->tindakanrm_id,
                                                                                          'onclick' => "inputperiksa(this);"));
                                 echo "<span>".$pemeriksaan->tindakanrm_nama."</span><br/>";
                             }
                         } ?>
                </div>
        <?php } ?>
    </div>
                
    <table class="table-condensed">
        <tr>
            <td width="50%">
                <div class="control-group ">
                    <?php echo $form->labelEx($modKirimKeUnitLain,'tgl_kirimpasien', array('class'=>'control-label')) ?>
                    <?php $modKirimKeUnitLain->tgl_kirimpasien = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modKirimKeUnitLain->tgl_kirimpasien, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modKirimKeUnitLain,
                                                    'attribute'=>'tgl_kirimpasien',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($modKirimKeUnitLain,'pegawai_id', CHtml::listData($modKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modKirimKeUnitLain,'catatandokterpengirim',array('onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
            </td>
            <td width="50%">
                <table id="tblFormPermintaanRehab" class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>Jenis Tindakan</th>
                            <th>Tindakan</th>
                            <th>Tarif</th>
                            <th>Qty</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    
    
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>

</div>
<?php $this->endWidget(); ?>

<script>
$('#formPermintaanRehab').tile({widths : [ 190 ]});
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idTindakanRM = obj.value;
        var idKelasPelayanan = <?php echo $modPendaftaran->kelaspelayanan_id ?>;
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPermintaanRehabMedis')?>',
                 'data':{idTindakanRM:idTindakanRM, idKelasPelayanan:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         if($.trim(data.form)=='')
                         {
                            $(obj).removeAttr('checked');
                            alert ('Permintaan belum memiliki tarif');
                         } 
                         $('#tblFormPermintaanRehab #trPeriksaLabKosong').detach();
                         $('#tblFormPermintaanRehab > tbody').append(data.form);
                         $("#tblFormPermintaanRehab > tbody > tr:last .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0});
                         $('.currency').each(function(){this.value = formatNumber(this.value)});
                         $("#tblFormPermintaanRehab > tbody > tr:last .number").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0,"symbol":null});
                         $('.number').each(function(){this.value = formatNumber(this.value)});
                 } ,
                 'cache':false});
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idTindakanRm)
{
    $('#tblFormPermintaanRehab #tindakanrm_'+idTindakanRm).detach();
//     if($('#tblFormPermintaanRehab tr').length == 1)
//        $('#tblFormPermintaanRehab').append('<tr id="trPeriksaLabKosong"><td colspan="4"></td></tr>');
}

function batalKirim(idPasienKirimKeUnitLain,idPendaftaran)
{
    if(confirm('Apakah anda akan membatalkan kirim pasien ke Rehab Medis?')){
        $.post('<?php echo $this->createUrl('ajaxBatalKirim') ?>', {idPasienKirimKeUnitLain: idPasienKirimKeUnitLain, idPendaftaran:idPendaftaran}, function(data){
            $('#tblListPermintaanRehab').html(data.result);
        }, 'json');
    }
}

function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
</script>