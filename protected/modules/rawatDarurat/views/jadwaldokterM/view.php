

<?php
$this->breadcrumbs=array(
	'Rdjadwaldokter Ms'=>array('index'),
	$model->jadwaldokter_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jadwal Dokter', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jadwal Dokter', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'jadwaldokter_id',
		'instalasi.instalasi_nama',
		'ruangan.ruangan_nama',
		'pegawai.nama_pegawai',
		'jadwaldokter_hari',
		'jadwaldokter_buka',
		'jadwaldokter_mulai',
		'jadwaldokter_tutup',
		'maximumantrian',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>