<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'rdjadwaldokter-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'jadwaldokter_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'instalasi_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'pegawai_id',array('class'=>'span3')); ?>
	<?php echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData(RDPendaftaran::model()->getRuanganItems(), 'ruangan_id', 'ruangan_nama') ,
                  array('empty'=>'-- Pilih --',
                        'onchange'=>"listDokterRuangan(this.value)",
                        'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData(RDPendaftaran::model()->getDokterItems(), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

	<?php echo $form->textFieldRow($model,'jadwaldokter_hari',array('class'=>'span3','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'jadwaldokter_buka',array('class'=>'span3','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'jadwaldokter_mulai',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'jadwaldokter_tutup',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'maximumantrian',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span5')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
