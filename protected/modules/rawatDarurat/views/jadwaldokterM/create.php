
<?php
$this->breadcrumbs=array(
	'Rdjadwaldokter Ms'=>array('index'),
	'Create',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Jadwal Dokter ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
            
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jadwal Dokter', 'icon'=>'folder-open', 'url'=>array('Admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('rawatDarurat.views.jadwaldokterM._form', array('model'=>$model,'listHari'=>$listHari)); ?>
<?php //$this->widget('TipsMasterData',array('type'=>'create'));?>