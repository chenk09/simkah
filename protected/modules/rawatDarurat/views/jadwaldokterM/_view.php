<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('jadwaldokter_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->jadwaldokter_id),array('view','id'=>$data->jadwaldokter_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('instalasi_id')); ?>:</b>
	<?php echo CHtml::encode($data->instalasi->instalasi_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruangan_id')); ?>:</b>
	<?php echo CHtml::encode($data->ruangan->ruangan_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pegawai_id')); ?>:</b>
	<?php echo CHtml::encode($data->pegawai->nama_pegawai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jadwaldokter_hari')); ?>:</b>
	<?php echo CHtml::encode($data->jadwaldokter_hari); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jadwaldokter_buka')); ?>:</b>
	<?php echo CHtml::encode($data->jadwaldokter_buka); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jadwaldokter_mulai')); ?>:</b>
	<?php echo CHtml::encode($data->jadwaldokter_mulai); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jadwaldokter_tutup')); ?>:</b>
	<?php echo CHtml::encode($data->jadwaldokter_tutup); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maximumantrian')); ?>:</b>
	<?php echo CHtml::encode($data->maximumantrian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
	<?php echo CHtml::encode($data->create_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_loginpemakai_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_loginpemakai_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_loginpemakai_id')); ?>:</b>
	<?php echo CHtml::encode($data->update_loginpemakai_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_ruangan')); ?>:</b>
	<?php echo CHtml::encode($data->create_ruangan); ?>
	<br />

	*/ ?>

</div>