

<?php
$this->breadcrumbs=array(
	'Rdjadwaldokter Ms'=>array('index'),
	$model->jadwaldokter_id=>array('view','id'=>$model->jadwaldokter_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Jadwal Dokter', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jadwal Dokter', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('rawatDarurat.views.jadwaldokterM._formUpdate',array('model'=>$model,'listHari'=>$listHari)); ?>
