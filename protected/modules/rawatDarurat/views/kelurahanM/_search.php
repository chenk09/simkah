<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sakelurahan-m-search',
                 'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'kelurahan_id',array('class'=>'span5')); ?>

	<?php echo $form->dropDownListRow($model,'kecamatan_id',  CHtml::listData($model->KecamatanItems, 'kecamatan_id', 'kecamatan_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --')); ?>

	<?php echo $form->textFieldRow($model,'kelurahan_nama',array('class'=>'span3','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'kode_pos',array('class'=>'span1','maxlength'=>6)); ?>

	<?php echo $form->checkBoxRow($model,'kelurahan_aktif',array('checked'=>'checked')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
