
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan));      

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'dtd_id',
		array(
                        'header'=>'ID',
                        'value'=>'$data->dtd_id',
                ),
		//'diagnosa_id',
		//'dtd_no',
		'dtd_noterperinci',
		'dtd_nama',
		'dtd_namalainnya',
                 array
                (
                        'name'=>'dtd_menular',
                        'type'=>'raw',
                        'value'=>'($data->dtd_menular==1)? Yii::t("mds","Yes") : Yii::t("mds","No")',
                ),
             array
                (
                        'name'=>'dtd_aktif',
                        'type'=>'raw',
                        'value'=>'($data->dtd_aktif==1)? Yii::t("mds","Yes") : Yii::t("mds","No")',
                ),
		/*
		'dtd_katakunci',
		'dtd_nourut',
		'dtd_menular',
		'dtd_aktif',
		*/
 
        ),
    )); 
?>