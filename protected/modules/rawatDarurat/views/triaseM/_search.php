<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'rdtriase-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'triase_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'triase_nama',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'triase_namalainnya',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'warna_triase',array('class'=>'span3','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'kode_warnatriase',array('class'=>'span3','maxlength'=>12)); ?>

	<?php echo $form->textAreaRow($model,'keterangan_triase',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->checkBoxRow($model,'triase_aktif',array('checked'=>true)); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
