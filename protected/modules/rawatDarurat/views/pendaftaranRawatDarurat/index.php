<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-rd-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset>  
        <legend class="rim2">Pendaftaran Pasien Rawat Darurat</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        
	<?php echo $form->errorSummary(array($model,$modPasien,$modPenanggungJawab,$modRujukan)); ?>
        <table class="table-condensed">
            <tr>
                <td width="50%">
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')); ?>
                        <div class="controls">
                          <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                            )); ?>
                        <?php echo $form->error($model, 'tgl_pendaftaran'); ?>                        </div>
                    </div>
                    
                    <?php echo $this->renderPartial('_formPasien', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien)); ?>                </td>
                    
                <td width="50%"><?php echo $form->textFieldRow($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?><?php echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData($model->getRuanganItems(Params::INSTALASI_ID_RD), 'ruangan_id', 'ruangan_nama') ,
                                                      array('empty'=>'-- Pilih --',
                                                            'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'RDPendaftaran')),
                                                              'update'=>'#RDPendaftaran_jeniskasuspenyakit_id'),
                                                            'onchange'=>"listDokterRuangan(this.value)",
                                                            'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems($model->ruangan_id), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,
                                                      array('empty'=>'-- Pilih --',
                                                            'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php //echo $form->dropDownListRow($model,'statusmasuk', StatusMasuk::items(), array('onchange'=>'enableRujukan(this);','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    
                    <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'RDPendaftaran')), 
                                                    'update'=>'#'.CHtml::activeId($model,'penjamin_id').''  //selector to update
                                                ),
                        )); ?>

                    <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>						
							
                    <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan)); ?>

                    <?php echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form)); ?>

                    	<?php echo $form->dropDownListRow($model,'caramasuk_id', CHtml::listData($model->getCaraMasukItems(), 'caramasuk_id', 'caramasuk_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'keadaanmasuk', KeadaanMasuk::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                
                    <?php echo $form->dropDownListRow($model,'transportasi', Transportasi::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>
					
                    
<!--                    <div class="control-group ">
                        <div class="controls">
                            <div class="checkbox inline">
                                <?php // echo $form->checkBox($model,'kunjunganrumah', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                <?php // echo CHtml::activeLabel($model, 'kunjunganrumah'); ?>
                            </div>
                            <div class="checkbox inline">
                                <?php // echo $form->checkBox($model,'byphone', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                <?php echo CHtml::activeLabel($model, 'byphone'); ?>                            </div>
                        </div>
                    </div>-->
                    
                    <fieldset id="fieldsetKarcis" class="">
                        <legend class="accord1">
                            <?php echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                            Karcis Rawat Darurat                        </legend>
                            <?php echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                    </fieldset>                </td>
            </tr>
        </table>
    </fieldset>

	<div class="form-actions">
		<?php 
                    if($model->isNewRecord)
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                    else 
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('pendaftaranRawatDarurat/index'), array('class'=>'btn btn-danger')); ?>
                <?php if(!$model->isNewRecord) echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false")); ?>
				<?php $content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'create','content'=>$content)); ?>
	</div>

<?php $this->endWidget(); ?>



<?php Yii::app()->clientScript->registerScript('status_masuk',"
    $(document).ready(function(){               
        $(':input').keypress(function(e) {
            if(e.keyCode == 13) {
                $(this).focusNextInputField(); 
            } 
        });
    });
    "); ?>


<?php
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoli',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$jscript = <<< JS
function print(idPendaftaran)
{
        if(document.getElementById('isPasienLama').checked == true){
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=280');
        }else{
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');
            window.open('${urlPrintKartuPasien}'+idPendaftaran,'printwi','left=100,top=100,width=400,height=280');
        }
}

function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#RDPendaftaran_pegawai_id').html(data.listDokter);
    }, "json");
}
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>

