<?php
$modKarcisTindakan = DaftartindakanM::model()->findAllByAttributes(array('daftartindakan_karcis'=>true,'daftartindakan_aktif'=>true));

?>

    <div id="daftarKarcis" class="<?php echo ($model->adaKarcis) ? '':'hide'; ?>">
        <div class="controls">
		
            <?php echo CHtml::radioButtonList('karcisTindakan', '', CHtml::listData($modKarcisTindakan, 'daftartindakan_id', 'daftartindakan_nama'), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        </div>
         <table id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Karcis</th>
                        <th>Harga</th>                        
                    </tr>
                </thead>
            </table>
    </div>
    
<?php
$enableInputKarcis = ($model->adaKarcis) ? 1 : 0;
$js = <<< JS
if(${enableInputKarcis}) {
    $('#daftarKarcis input').removeAttr('disabled');
    $('#daftarKarcis select').removeAttr('disabled');
}
else {
    $('#daftarKarcis input').attr('disabled','true');
    $('#daftarKarcis select').attr('disabled','true');
}

$('#karcisTindakan').change(function(){
        if ($(this).is(':checked')){
                $('#daftarKarcis input').removeAttr('disabled');
                $('#daftarKarcis select').removeAttr('disabled');
        }else{
                $('#daftarKarcis input').attr('disabled','true');
                $('#daftarKarcis select').attr('disabled','true');
                $('#daftarKarcis input').attr('value','');
                $('#daftarKarcis select').attr('value','');
        }
        $('#daftarKarcis').slideToggle(500);
    });
JS;
Yii::app()->clientScript->registerScript('karcis',$js,CClientScript::POS_READY);
?>
