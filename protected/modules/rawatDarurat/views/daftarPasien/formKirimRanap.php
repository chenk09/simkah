<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pasienpulang-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
));
$this->widget('bootstrap.widgets.BootAlert');
?>

<?php if($status == true){ ?>

<?php }else{ ?>
<?php echo $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
    <legend class="rim">Tindak Lanjut</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary(array($modelPulang,$modRujukanKeluar)); ?>
        <table>
            <tr>
                <td width="50%">
                    <?php //echo $form->textFieldRow($modelPulang,'pasienadmisi_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <div class="control-group ">
                        <?php //echo $form->labelEx($modelPulang,'tglpasienpulang', array('class'=>'control-label')) ?>
                        <?php echo CHtml::label('Tgl Pasien Keluar', 'tglpasienpulang', array('class'=>'control-label'))?>
                        <div class="controls">
                            <?php
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modelPulang,
                                                    'attribute'=>'tglpasienpulang',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                            )); ?>
                            <?php echo $form->error($modelPulang, 'tglpasienpulang'); ?>
                        </div>
                    </div>
                    <?php echo $form->hiddenfield($modelPulang,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>true)); ?>
                    <?php echo $form->hiddenfield($modelPulang,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>true)); ?>
                    <?php echo $form->hiddenfield($modelPulang,'carakeluar',array()); ?>
					<div class="control-group ">
						<?php echo CHtml::label('Kondisi Keluar <font color=red>*</font>', 'kondisipulang', array('class'=>'control-label'))?>
						<div class="controls">
							<?php echo $form->dropDownList($modelPulang,'kondisipulang', KondisiPulang::items(), array('empty'=>'-- Pilih --', 'onchange'=>'cekKondisiKeluar(this);','onkeypress'=>"return $(this).focusNextInputField(event)"));?>
							<?php echo $form->error($modelPulang,'kondisipulang',array('errorCssClass'=>'error')); ?>
						</div>
					</div>
                    <?php echo $form->textFieldRow($modelPulang,'penerimapasien',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>

                    <?php if(Yii::app()->user->getState('instalasi_id') == Params::INSTALASI_ID_RI) { ?>
                        <?php //echo $form->textFieldRow($modMasukKamar,'tglmasukkamar',array('readonly'=>true)) ?>
                        <div class="control-group ">
                            <?php //echo $form->labelEx($modMasukKamar,'lamadirawat_kamar', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php //echo $form->textField($modMasukKamar,'lamadirawat_kamar',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> Hari
                                <?php echo $form->hiddenField($modelPulang,'lamarawat',array('class'=>'span1','value'=>$modMasukKamar->lamadirawat_kamar, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            </div>
                        </div>
                    <?php } else{ ?>
                        <div class="control-group ">
                            <?php echo $form->labelEx($modelPulang,'lamarawat', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php echo $form->textField($modelPulang,'lamarawat',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> Jam
                            </div>
                        </div>
                        <?php echo $form->error($modelPulang, 'lamarawat'); } ?>

                     <?php //echo $form->textFieldRow($modelPulang,'satuanlamarawat',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                </td>
                <td width="50%"></td>
            </tr>
        </table>
	<div class="form-actions">
        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : Yii::t('mds','{icon} Save', array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
            array(
                'class'=>'btn btn-primary',
                'type'=>'submit',
                'onKeypress'=>'return formSubmit(this,event)'
            )
        ); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel', array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
            array(
                'class'=>'btn btn-danger',
                'onclick'=>'konfirmasi()',
                'onKeypress'=>'return formSubmit(this,event)'
            )
        ); ?>
	</div>
<?php } ?>
<?php $this->endWidget(); ?>

<script>
    function cekCaraKeluar(obj){
        if(obj.value == "<?php echo Params::KODE_RUJUKAN ?>"){
            $('#pakeRujukan').attr('checked',true);
            $('#divRujukan input').removeAttr('disabled');
            $('#divRujukan select').removeAttr('disabled');
            $('#divRujukan textarea').removeAttr('disabled');
            $('#divRujukan').show(500);
        }else{
            $('#pakeRujukan').removeAttr('checked');
            $('#divRujukan input').attr('disabled','true');
            $('#divRujukan select').attr('disabled','true');
            $('#divRujukan textarea').attr('disabled','true');
            $('#divRujukan').hide(500);
        }
    }
    function cekKondisiKeluar(obj){
        if(obj.value == "<?php echo Params::KODE_MENINGGAL_1 ?>" || obj.value == "<?php echo Params::KODE_MENINGGAL_2 ?>")
        {
             $('#isDead').attr('checked',true);
             $('#PasienpulangT_tgl_meninggal').removeAttr('disabled');
        }
        else
        {
            $('#isDead').removeAttr('checked');
            $('#PasienpulangT_tgl_meninggal').attr('disabled','true');
        }
    }
    function konfirmasi()
    {
        if(confirm('<?php echo Yii::t('mds','Do You want to cancel?') ?>')){
            window.location.href = window.location;
        }
        else
        {
            $('#PasienpulangT_carakeluar').focus();
            return false;
        }
    }
</script>
<?php if($tersimpan == true) {?>
<script>
    parent.location.reload();
</script>
<?php } ?>
