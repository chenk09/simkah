<style>
    .row-red td{
        background-color: indianred !important;
        color: #ffffff;
    }
</style>

<legend class="rim2">Daftar pasien Rawat Darurat</legend>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    Yii::app()->clientScript->registerScript('cari wew', "
        $('#daftarPasien-form').submit(function(){
        	$.fn.yiiGridView.update('daftarPasien-grid', {
        		data: $(this).serialize()
        	});
        	return false;
        });");
?>
<?php $this->widget('bootstrap.widgets.BootAlert');	?>
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
    	'id'=>'daftarPasien-grid',
    	'dataProvider'=>$model->searchRD(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'rowCssClassExpression'=>'($data->statusfarmasi == true ? "row-red" : "row-white" )',
    	'columns'=>array(
            array(
                'name'=>'tgl_pendaftaran',
                'type'=>'raw',
                'value'=>'$data->tgl_pendaftaran'
            ),
//                    array(
//                        'header'=>'Instalasi / Poliklinik',
//                        'value'=>'$data->insatalasiRuangan'
//                    ),
                    array(
                       'name'=>'no_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->no_pendaftaran',
                    ),
                    array(
                       'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->no_rekam_medik',
                    ),
                    array(
                        'header'=>'Nama Pasien / Bin',
                        'value'=>'$data->namaNamaBin'
                    ),
                    array(
                        'header'=>'Cara Bayar / Penjamin',
                        'value'=>'$data->caraBayarPenjamin',
                    ),
                    array(
                       'name'=>'Dokter',
                        'type'=>'raw',
                        'value'=>'$data->nama_pegawai',
                    ),
                    array(
                       'name'=>'Transportasi',
                        'type'=>'raw',
                        'value'=>'(!empty($data->transportasi))? $data->transportasi : "-"',
                    ),
                    array(
                       'name'=>'Cara Masuk',
                        'type'=>'raw',
                        'value'=>'(!empty($data->caramasuk_nama))? $data->caramasuk_nama : "-"',
                    ),
                    array(
                       'name'=>'Rujukan',
                        'type'=>'raw',
                        'value'=>'(!empty($data->asalrujukan_nama))? $data->asalrujukan_nama : "-"',
                    ),
//                    array(
//                       'name'=>'kelaspelayanan_nama',
//                        'type'=>'raw',
//                        'value'=>'$data->kelaspelayanan_nama',
//                    ),
                    array(
                        'header'=>'Kasus Penyakit / <br> Kelas Pelayanan',
                        'type'=>'raw',
                        'value'=>'"$data->jeniskasuspenyakit_nama"."<br/>"."$data->kelaspelayanan_nama"',
                    ),
//                   array(
//                      'name'=>'pembayaranpelayanan_id',
//                       'type'=>'raw',
//                       'value'=>'$data->pendaftaran->pembayaranpelayanan_id',
//                   ),
                    array(
                       'name'=>'alamat_pasien',
                        'type'=>'raw',
                        'value'=>'$data->alamat_pasien',
                    ),
                    array(
                        'header'=>'Status Periksa',
                        'type'=>'raw',
//                        'value'=>'$data->statusperiksa.CHtml::link("<i class=icon-pencil></i>","",array("href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Status Periksa","onclick"=>"{buatSessionUbahStatus($data->pendaftaran_id); ubahStatusPeriksa(); $(\'#dialogUbahStatus\').dialog(\'open\');}return false;"))',
                        'value'=>'RDInfokunjunganrdV::getStatus($data->statusperiksa,$data->pendaftaran_id)',
                    ),
                    array(
                        'name'=>'Pemeriksaan Pasien',
                        'type'=>'raw',
/*                        'value'=>'(!empty($data->pasienpulang_id))? "-" : CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/rawatDarurat/anamnesaTRD",array("idPendaftaran"=>$data->pendaftaran_id)),array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien"))','htmlOptions'=>array('style'=>'text-align: center; width:40px')*/
//                        'value'=>'($data->pendaftaran->pembayaranpelayanan_id != null OR $data->statusperiksa == "SUDAH PULANG")? "PASIEN ".$data->statusperiksa:
//                                    CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/rawatDarurat/anamnesaTRD",array("idPendaftaran"=>$data->pendaftaran_id)),array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien"))','htmlOptions'=>array('style'=>'text-align: center; width:40px')
                          'value'=>'RDInfokunjunganrdV::getPeriksaPasien($data->statusperiksa,$data->pendaftaran_id,$data->pendaftaran->pembayaranpelayanan_id,$data->no_pendaftaran,$data->pendaftaran->alihstatus)',
                    ),
                    /*
                    array(
                        'header'=>'Tindak Lanjut',
                        'type'=>'raw',
                        'value'=>'RDInfokunjunganrdV::getTindakLanjut($data->statusperiksa,$data->pendaftaran_id,$data->pendaftaran->pembayaranpelayanan_id,$data->no_pendaftaran,$data->pasienpulang_id,$data->carakeluar,$data->pendaftaran->alihstatus,$data->kondisipulang)',
                        'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
                    */
                    array(
                        'name'=>'pemulanganPasien',
                        'type'=>'raw',
                        'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
                    ),
                    array(
                        'name'=>'kirimRanap',
                        'type'=>'raw',
                        'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
                    ),
                    array(
                        'name'=>'statusfarmasi',
                        'type'=>'raw',
                        'value'=>'($data->statusfarmasi == true ? "SUDAH" : "BELUM")',
                        'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
                    ),
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));


?>
<hr/>
<?php
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'id'=>'daftarPasien-form',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>
<fieldset>
    <legend class="rim">Pencarian</legend>
    <table class="table-condensed">
        <tr>
            <td>
                 <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($model,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
            </td>
            <td>
                <div class="control-group ">
                    <label for="namaPasien" class="control-label">
                        <?php echo CHtml::activecheckBox($model, 'ceklis', array('onclick'=>'cekTgl();','onkeypress'=>"return $(this).focusNextInputField(event)", 'uncheckValue'=>0,'rel'=>'tooltip' ,'data-original-title'=>'Cek untuk pencarian berdasarkan tanggal')); ?>
                        Tgl Masuk
                    </label>
                    <div class="controls">
                        <?php   $format = new CustomFormat;
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                        ));  ?> </div></div>
						<div class="control-group ">
                    <label for="namaPasien" class="control-label">
                       Sampai dengan
                      </label>
                    <div class="controls">
                            <?php    $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                        )); ?>
                    </div>
                </div>
                     <?php echo $form->dropDownListRow($model,'statusperiksa', StatusPeriksa::items(),array('empty'=>'-- Pilih --')); ?>

            </td>
        </tr>
    </table>

<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
echo CHtml::hiddenField('pendaftaran_id');
echo CHtml::hiddenField('pasien_id');
?>
<?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                        Yii::app()->createUrl($this->module->id.'/'.daftarPasien.'/index'),
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
 <?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
?>

<?php $this->endWidget();?>
</fieldset>

<?php
// Dialog untuk ubah status periksa =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogUbahStatus',
    'options'=>array(
        'title'=>'Ubah Status Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>500,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end ubah status periksa dialog =============================
?>


<?php
// Dialog untuk pasienpulang_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPasienPulang',
    'options'=>array(
        'title'=>'Tindak Lanjut Pasien Rawat Darurat',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>600,
        'resizable'=>false,
    ),
));?>
<iframe src="" name="iframePasienPulang" width="100%" height="900">
</iframe>
<?php

$this->endWidget();
//========= end pasienpulang_t dialog =============================

// Dialog untuk Batal Rawat Inap =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogBatalRawatInap',
    'options'=>array(
        'title'=>'Pembatalan Rawat Inap/ Pulang Pasien Rawat Darurat',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>500,
        'resizable'=>false,
    ),
));
?>
<iframe src="" name="iframeBatalRawatInap" width="100%" height="900">
</iframe>
<?php

$this->endWidget();
//========= end ubah status periksa dialog =============================
?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'loginDialog',
    'options'=>array(
        'title'=>'Login',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>250,
        'resizable'=>false,
    ),
));?>
<div class="alert alert-block alert-error" id="alertDiv" style="display : none;">
    Kesalahan dalam Pengisian Usename atau Password
</div>
<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formLogin')); ?>
    <div class="control-group ">
        <?php echo CHtml::label('Login Pemakai','username', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::textField('username', '', array()); ?>
        </div>
    </div>

    <div class="control-group ">
        <?php echo CHtml::label('Password','password', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::passwordField('password', '', array()); ?>
        </div>
    </div>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Login',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'cekLogin();return false;')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>'batal();return false;')); ?>
    </div>
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>


<!--dialog untuk menampilkan alaasan pembatalan pasien rawat inap-->
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogAlasan',
    'options'=>array(
        'title'=>'Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>900,
        'resizable'=>false,
    ),
));
?>
<div id="divFormDataPasien"></div>


<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formAlasan')); ?>
<table>
    <tr>
        <td><?php echo CHtml::label('Alasan','Alasan', array('class'=>'')) ?></td>
        <td>
            <?php echo CHtml::textArea('Alasan', '', array()); ?>
            <?php echo CHtml::hiddenField('idOtoritas', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('namaOtoritas', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('idPasienPulang', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('idPendaftaran', '', array('readonly'=>TRUE)); ?>

        </td>
    </tr>
</table>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'simpanAlasan();return false;')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>'batal();return false;')); ?>    </div>
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>
<!--akhir dari dialog alasan pasien dibatalkan rewat inap-->

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'konfirmasiDialog',
    'options'=>array(
        'title'=>'Konfirmasi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>190,
        'resizable'=>false,
    ),
));?>
<div align="center">
    User Tidak Memiliki Akses Untuk Proses Ini,<br/>
    Yakin Akan Melakukan Ke Proses Selanjutnya ?
</div>
<div class="form-actions" align="center">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Yes',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'button', 'onclick'=>"$('#loginDialog').dialog('open');$('#konfirmasiDialog').dialog('close');")); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} No',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>"$('#konfirmasiDialog').dialog('close');")); ?>    </div>

<?php $this->endWidget();?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'konfirmasiAdmisi',
    'options'=>array(
        'title'=>'Konfirmasi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>420,
        'height'=>200,
        'resizable'=>false,
    ),
));?>
<div align="center">
    Pasien sudah di rawat di ruangan <div id="ruanganPasien"></div>
    Anda tidak bisa melakukan pembatalan disini,<br/>
    Silahkan hubungi petugas Rawat Inap yang bersangkutan ?
</div>
<div id=""></div>
<div class="form-actions" align="center">
       <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Yes',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'button', 'onclick'=>"$('#konfirmasiAdmisi').dialog('close');")); ?>  </div>

<?php $this->endWidget();?>
<script type="text/javascript">

function batal(){
    $('#loginDialog').dialog('close');
    $('#loginDialog #username').val('');
    $('#loginDialog #password').val('');
    $('#alertDiv').hide();
    $('#idPasien').val('');
    $('#idPendaftaran').val('');

    $('#dialogAlasan').dialog('close');
    $('#dialogAlasan #idOtoritas').val('');
    $('#dialogAlasan #namaOtoritas').val('');
    $('#dialogAlasan #idPasienPulang').val('');
    $('#dialogAlasan #idPendaftaran').val('');

    $.fn.yiiGridView.update('daftarPasien-grid', {
        data: $('#daftarPasienPulang-form').serialize()
    });
}
function cekHakAkses(idPendaftaran)
{
//       $('#dialogAlasan #idPasienPulang').val(idPasienPulang);
//       $('#dialogAlasan #idPendaftaran').val(idPendaftaran);
//       $('#idPasien').val(idPasien);
//       $('#idPendaftaran').val(idPendaftaran);

//    $('#konfirmasiDialog').dialog('open');

    $.post('<?php echo Yii::app()->createUrl('rawatDarurat/ActionAjax/CekHakAkses');?>',
    {idPendaftaran:idPendaftaran, idUser:'<?php echo Yii::app()->user->id; ?>',useName:'<?php echo Yii::app()->user->name; ?>'}, function(data){
//        console.log(data);
     var cekAdmisi = data.pendaftaran.pasienadmisi_id;

     if(cekAdmisi){
         $('#konfirmasiAdmisi').dialog('open');
          $('#konfirmasiAdmisi #ruanganPasien').html(data.ruanganPasien);
     }else{
        $('#konfirmasiDialog').dialog('open');
        if(data.cekAkses==true){
            $('#dialogAlasan').dialog('open');
            $('#dialogAlasan #idOtoritas').val(data.userid);
            $('#dialogAlasan #namaOtoritas').val(data.username);
        } else {
            $('#konfirmasiDialog').dialog('open');
        }
     }
       $('#dialogAlasan #idPasienPulang').val(data.pendaftaran.pasienpulang_id);
       $('#dialogAlasan #idPendaftaran').val(data.pendaftaran.pendaftaran_id);
       $('#idPasien').val(data.pendaftaran.pasien_id);
       $('#idPendaftaran').val(data.pendaftaran.pendaftaran_id);
    }, 'json');
}

function cekLogin()
{
    idPasien = $('#idPasien').val();
    idPendaftaran = $('#idPendaftaran').val();
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/CekLoginPembatalRawatInap');?>', $('#formLogin').serialize(), function(data){
        if(data.error != '')
        $('#'+data.cssError).addClass('error');
        if(data.status=='success'){
              $.post('<?php echo Yii::app()->createUrl('rawatDarurat/ActionAjax/dataPasien');?>', {idPasien:idPasien ,idPendaftaran:idPendaftaran}, function(dataPasien){

              $('#divFormDataPasien').html(dataPasien.form);

             }, 'json');

            $('#dialogAlasan').dialog('open');
            $('#dialogAlasan #idOtoritas').val(data.userid);
            $('#dialogAlasan #namaOtoritas').val(data.username);
            $('#loginDialog').dialog('close');
        }else{
    $('#alertDiv').show();
        }
    }, 'json');
}

function simpanAlasan()
{
    alasan =$('#dialogAlasan #Alasan').val();
    if(alasan==''){
        alert('Anda Belum Mengisi Alasan Pembatalan');
    }else{
        $.post('<?php echo Yii::app()->createUrl('rawatDarurat/daftarPasien/BatalRawatInap');?>', $('#formAlasan').serialize(), function(data){
//            if(data.error != '')
//                alert(data.error);
//            $('#'+data.cssError).addClass('error');
            if(data.status=='success'){
                batal();
                alert('Data Berhasil Disimpan');
            }else{
                alert(data.status);
            }
        }, 'json');
   }
}




</script>

<script>
function addPasienPulang(idPendaftaran,idPasien)
{
    $('#pendaftaran_id').val(idPendaftaran);
    $('#pasien_id').val(idPasien);

    <?php
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjaxRIRD/addPasienPulang'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogPasienPulang div.divForForm').html(data.div);
                    $('#dialogPasienPulang div.divForForm form').submit(addPasienPulang);

                    jQuery('.dtPicker2-5').datetimepicker(jQuery.extend({showMonthAfterYear:false},
                    jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','maxDate'  : 'd','timeText':'Waktu','hourText':'Jam',
                         'minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih   Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));


                }
                else
                {
                    $('#dialogPasienPulang div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    setTimeout(\"$('#dialogPasienPulang').dialog('close') \",1000);
                }

            } ",
    ))
?>;
    return false;
}

</script>

<script>
        function ubahStatusPeriksa()
{
    <?php
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjaxRIRD/ubahStatusPeriksaRD'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogUbahStatus div.divForForm').html(data.div);
                    $('#dialogUbahStatus div.divForForm form').submit(ubahStatusPeriksa);

                    jQuery('.dtPicker3').datetimepicker(jQuery.extend({showMonthAfterYear:false},
                    jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','maxDate'  : 'd','timeText':'Waktu','hourText':'Jam',
                         'minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih   Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));

                }
                else
                {
                    $('#dialogUbahStatus div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    setTimeout(\"$('#dialogUbahStatus').dialog('close') \",1000);
                }

            } ",
    ))
?>;
    return false;
}




</script>
<script type="text/javascript">
        $('document').ready(function(){
            $('#daftarPasien-grid button').each(function(){
                $('#orange').removeAttr('class');
                $('#red').removeAttr('class');
                $('#green').removeAttr('class');
                $('#blue').removeAttr('class');

                $('#orange').attr('class','btn btn-danger-blue');
                $('#red').attr('class','btn btn-danger-red');
                $('#green').attr('class','btn btn-danger');
                $('#blue').attr('class','btn btn-danger-yellow');
            });

        });
        function setStatus(obj,status,idpendaftaran){
            var status = status;
            var idpendaftaran = idpendaftaran;
            var answer = confirm('Yakin Akan Merubah Status Periksa Pasien?');
            if (answer){
    //            alert(status);
    //            alert(idpendaftaran);
                  $.post('<?php echo Yii::app()->createUrl('ActionAjaxRIRD/UbahStatusPeriksaPasien');?>', {status:status ,idpendaftaran:idpendaftaran}, function(data){
                    if(data.status == 'proses_form'){

                            $('#dialogUbahStatusPasien div.divForForm').html(data.div);
                            $.fn.yiiGridView.update('daftarPasien-grid');
                            setTimeout("$('#dialogUbahStatus').dialog('close')",1000);
                    }else{
                        $('#alertDiv').show();
                    }
                }, 'json');
            }else{
                preventDefault();
            }
        }

        function cekTgl()
        {
            if($("#RDInfokunjunganrdV_ceklis").is(":checked"))
            {
                $("#RDInfokunjunganrdV_tglAwal").removeAttr('disabled');
                $("#RDInfokunjunganrdV_tglAwal_date").removeAttr('style');
                $("#RDInfokunjunganrdV_tglAkhir").removeAttr('disabled');
                $("#RDInfokunjunganrdV_tglAkhir_date").removeAttr('style');

                // alert("test");
            }else{
                $("#RDInfokunjunganrdV_tglAwal").attr('disabled',true);
                $("#RDInfokunjunganrdV_tglAwal_date").attr('style','display:none');
                $("#RDInfokunjunganrdV_tglAkhir").attr('disabled',true);
                $("#RDInfokunjunganrdV_tglAkhir_date").attr('style','display:none');

            }
        }
        cekTgl();

        function cekStatus(status){
            var status = status;
            alert("Pasien "+status+" Tidak bisa melanjutkan pemeriksaan atau tindak lanjut");
        }

</script>

<?php
$urlSession = Yii::app()->createUrl('ActionAjaxRIRD/buatSessionPendaftaranPasien');
$urlSessionUbahStatus = Yii::app()->createUrl('ActionAjaxRIRD/buatSessionUbahStatus ');
$jscript = <<< JS
function buatSession(idPendaftaran,idPasien)
{
    $.post("${urlSession}", { idPendaftaran: idPendaftaran,idPasien: idPasien },
        function(data){
            'sukses';
    }, "json");
}

function buatSessionUbahStatus(idPendaftaran)
{
    var answer = confirm('Yakin Akan Merubah Status Periksa Pasien?');
        if (answer){
            $.post("${urlSessionUbahStatus}", {idPendaftaran: idPendaftaran },
                function(data){
                    'sukses';
            }, "json");
        }
        else{
            preventDefault();
        }
}
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
