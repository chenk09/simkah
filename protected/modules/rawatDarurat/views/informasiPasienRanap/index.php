<legend class="rim2">Informasi Pasien Dirawat Inap</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'daftarPasienPulang-grid',
    'dataProvider'=>$modPasienYangPulang->searchPasienRanap(),
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
        array(
            'header'=>'Tgl Pendaftaran',
            'value'=>'$data->tgl_pendaftaran',
        ),
        array(
            'header'=>'No Rekam Medik / <BR> No Pendaftaran',
            'type'=>'raw',
			'value'=>'$data->no_rekam_medik . "/<br>" . $data->no_pendaftaran'
        ),
        array(
            'header'=>'Nama',
            'type'=>'raw',
			'value'=>'$data->nama_pasien'
        ),
        array(
            'header'=>'Kelas Pelayanan',
            'type'=>'raw',
            'value'=>'$data->kelaspelayanan_nama'
        ),
        array(
            'header'=>'Nama Jenis Kasus Penyakit',
            'value'=>'$data->jeniskasuspenyakit_nama',
        ),
        array(
               'header'=>'Batal Dirawat',
               'type'=>'raw',
               'value'=>'CHtml::link("<i class=\'icon-remove\'></i>", "#", array(
                   "title"=>"Klik untuk Batal Dirawat",
                   "onclick"=>"prosesPembatalanRanap($data->pendaftaran_id);return false",
                   "rel"=>"tooltip"
               ))',
               'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
        ),
	),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
?>
<?php echo $this->renderPartial('_formPencarian', array('modPasienYangPulang'=>$modPasienYangPulang)); ?>

<?php
$urlBatalPulang = Yii::app()->createUrl('rawatDarurat/informasiPasienRanap/batalPulang');
Yii::app()->clientScript->registerScript('search', "
$('#daftarPasienPulang-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasienPulang-grid', {
		data: $(this).serialize()
	});
	return false;
});
function prosesPembatalanRanap(id){
	var answer = confirm('Yakin Pasien Akan melakukan pembatalan?');
	if (answer){
        var pendaftaran_id = id;
    	$.post('$urlBatalPulang',{pendaftaran_id:pendaftaran_id}, function(data){
            if(data.status == 'OK'){
                $.fn.yiiGridView.update('daftarPasienPulang-grid', {
            		data: $('#daftarPasienPulang-form').serialize()
            	});
            }
            alert(data.pesan);
    	}, 'json');
    	return false;
	}
}",  CClientScript::POS_END);
?>
