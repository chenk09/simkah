<div class="search-form" style="">
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
 <style>

        #penjamin label.checkbox{
            width: 100px;
            display:inline-block;
        }

    </style><legend class="rim">Berdasarkan Kunjungan</legend>
       <table width="200" style="margin-top:10px;">
  <tr>
    <td>
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <?php //echo CHtml::hiddenField('src', ''); ?>
                    <div class = 'control-label'>Tanggal Pelayanan</div>
                    <div class="controls">  
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAwal',
                            'mode' => 'datetime',
//                                          'maxDate'=>'d',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div> 
      </td>
    <td style="padding:0px 130px 0 0px;"> <?php echo CHtml::label('Sampai dengan', ' s/d', array('class' => 'control-label')) ?>
                    <div class="controls">  
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAkhir',
                            'mode' => 'datetime',
//                                         'maxdate'=>'d',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div> </td>
  </tr>
</table>  
          <table width="216" border="0">
  <tr>
    <td width="51" style="padding-left:10px;"> <fieldset>
					<legend class="rim">Berdasarkan Kelas Pelayanan </legend>
                        <?php echo'<table>
                                                <tr>
                                                    <td>
                                                        <div>'.CHtml::checkBox("checkAllR",true, array("onkeypress"=>"return $(this).focusNextInputField(event)",
                                                            "class"=>"checkbox-column","onclick"=>"checkAllRuangan()","checked"=>"checked")).' Pilih Semua </div><br/>
                                                        <div id="Ruangan">'.
                                                        $form->checkBoxList($model, 'kelaspelayanan_id', CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_nama'), array('value'=>'pengunjung', 'inline'=>true, 'empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)")).'
                                                        </div>
                                                    </td>
                                                 </tr>
                                                 </table>'; ?>
      </fieldset>
      <fieldset>
            <legend class="rim">Berdasarkan Dokter </legend>
            <?php //echo $form->textFieldRow($model,'nama_pegawai',array()); ?>
            <div class="control-group ">
                    <?php echo $form->labelEx($model, 'Nama Dokter', array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php 
               //echo CHtml::activeHiddenField($model, 'pegawai_id');
                        $this->widget('MyJuiAutoComplete', array(
                                   'name'=>CHtml::activeId($model, 'nama_pegawai'),
                                    'value'=>$model,
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/Pegawai').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($model, 'nama_pegawai').'").val(ui.item.nama_pegawai);
                                                return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array(
                                        'readonly'=>false,
                                        'placeholder'=>'Nama Dokter',
                                        'class'=>'span3',
                                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogPegawai'),
                            )); ?>
                    </div>
                </div>
      </fieldset>
    </td>
    <td width="155" style="padding-right:30px;">   <div id='searching'>
                    <fieldset>
					<legend class="rim">Berdasarkan Cara Bayar </legend>
                        <?php echo '<table>
                                                    <tr>
                                                        <td>'.CHtml::hiddenField('filter', 'carabayar', array('disabled'=>'disabled')).'<label>Cara&nbsp;Bayar</label></td>
                                                        <td>'.$form->dropDownList($model, 'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                                            'ajax' => array('type' => 'POST',
                                                                'url' => Yii::app()->createUrl('ActionDynamic/GetPenjaminPasienForCheckBox', array('encode' => false, 'namaModel' => ''.$model->getNamaModel().'')),
                                                                'update' => '#penjamin',  //selector to update
                                                            ),
                                                        )).'
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Penjamin</label>
                                                        </td>
                                                        <td>
                                                            <div id="penjamin">'.
                                                                //$form->checkBoxList($model, 'penjamin_id', CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_nama'), array('value'=>'pengunjung', 'empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)")).'
                                                            '</div>
                                                        </td>
                                                    </tr>
                                                 </table>'; ?>
                    </fieldset>
      </div></td>
  </tr>
</table>
    <div class="form-actions">
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
		<?php
 echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                                                                        array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));?> 
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ', array('idPendaftaran' => ''));
?>

<?php Yii::app()->clientScript->registerScript('cekAll','
  $("#big").find("input").attr("checked", "checked");
  $("#kelasPelayanan").find("input").attr("checked", "checked");
',  CClientScript::POS_READY);
?>
<?php 
//$urlGetPenjamin = Yii::app()->createUrl('ActionDynamic/GetPenjaminPasienForCheckBox', array('encode' => false, 'namaModel' => ''.$model->getNamaModel().''));
//Yii::app()->clientScript->registerScript('ajax','
//    $("#'.CHtml::activeId($model, 'carabayar_id').'").change(function(){
//        id = $(this).val();
//        $.post("'.$urlGetPenjamin.'", {id:id},function(data){
//            
//        });
//    });
//',CClientScript::POS_READY); ?>

<?php //Yii::app()->clientScript->registerScript('onclickButton','
//  var tampilGrafik = "<div class=\"tampilGrafik\" style=\"display:inline-block\"> <i class=\"icon-arrow-right icon-white\"></i> Grafik</div>";
//  $(".accordion-heading a.accordion-toggle").click(function(){
//            $(this).parents(".accordion").find("div.tampilGrafik").remove();
//            $(this).parents(".accordion-group").has(".accordion-body.in").length ? "" : $(this).append(tampilGrafik);
//            
//            
//  });
//',  CClientScript::POS_READY);
?>
<?php 
    // Dialog buat nambah data pegawai =========================
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
        'id'=>'dialogPegawai',
        'options'=>array(
            'title'=>'Pencarian Dokter',
            'autoOpen'=>false,
            'modal'=>true,
            'minWidth'=>800,
            'minHeight'=>500,
            'resizable'=>false,
        ),
));

    $modPegawai =new PegawaiM();
    $modPegawai->unsetAttributes();
    if(isset($_GET['PegawaiM'])) {
        $modPegawai->attributes = $_GET['PegawaiM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array( 
        'id'=>'sapegawai-m-grid', 
        'dataProvider'=>$modPegawai->search(), 
        'filter'=>$modPegawai, 
        'template'=>"{pager}{summary}\n{items}", 
        'itemsCssClass'=>'table table-striped table-bordered table-condensed', 
        'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=\"icon-check\"></i>","#", array("id" => "selectPegawai",
                                              "onClick"=>"
                                                $(\"#'.CHtml::activeId($model, 'nama_pegawai').'\").val(\"$data->nama_pegawai\");
                                                $(\"#dialogPegawai\").dialog(\"close\");    
                                                "
                                         ))',
            ),
            'nomorindukpegawai',
            'nama_pegawai',
            'jeniskelamin',
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
        )); ?>

        <?php $this->endWidget(); ?>
<script>
        function checkAll() {
            if ($("#checkAllCaraBayar").is(":checked")) {
                $('#penjamin input[name*="RDLaporanpendapatanruangan"]').each(function(){
                   $(this).attr('checked',true);
                })
            } else {
               $('#penjamin input[name*="RDLaporanpendapatanruangan"]').each(function(){
                   $(this).removeAttr('checked');
                })
            }
            //setAll();
        }

        function checkAllRuangan() {
            if ($("#checkAllR").is(":checked")) {
                $('#Ruangan input[name*="RDLaporanpendapatanruangan"]').each(function(){
                   $(this).attr('checked',true);
                })
            } else {
               $('#Ruangan input[name*="RDLaporanpendapatanruangan"]').each(function(){
                   $(this).removeAttr('checked');
                })
            }
            //setAll();
        }
        
        function setAll(obj){
            $('.cekList').each(function(){
               if ($(this).is(':checked')){

                    $(this).parents('tr').find('.cekList').val(1);
                    }else{
                        $(this).parents('tr').find('.cekList').val(0);
                    }
            });
        }
</script>