<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
    <style>
        #rujukankeluar tr td label.checkbox{
            width: 100px;
            display:inline-block;
        }
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
        .nav-tabs > .active a:hover{cursor:pointer;}
    </style>
        <fieldset>
        <legend class="rim2">Berdasarkan Kunjungan</legend>
        <table>
            <tr>
                <td width="50%">
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <div class='control-group'>
                        <div class = 'control-label'>Tanggal Pemeriksaan</div>
                        <div class="controls">  
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'tglAwal',
                                'mode' => 'datetime',
                                //                                          'maxDate'=>'d',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                ),
                                'htmlOptions' => array('readonly' => true,
                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                            ));
                            ?>
                        </div>
                    </div>
                </td>
                <td> 
                    <div class="control-group ">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAkhir',
                            'mode' => 'datetime',
                            //                                         'maxdate'=>'d',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <legend class="rim">
            Berdasarkan Tujuan Rujukan &nbsp; <?php echo CHtml::checkBox('cek_all', true, array('value'=>'cek', 'onchange'=>'cek_all_tindakan(this)'));?>
        </legend>
                        <?php echo '<table id="rujukankeluar">
                                                    <tr>
                                                        <td>'.
                                                           
                                                            $form->checkBoxList($model, 'rujukankeluar_id', CHtml::listData(RujukankeluarM::model()->findAll(), 'rujukankeluar_id', 'rumahsakitrujukan'),
                                                                    array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",'checked'=>'checked',)).'
                                                            
                                                        </td>
                                                    </tr>
                                                 </table>'; ?>
    </fieldset>

    <div class="form-actions">
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
		<?php
 echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                                                                        array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));?> 
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ', array('idPendaftaran' => ''));
?>

<script type="text/javascript">
    function cek_all_tindakan(obj){
        if($(obj).is(':checked')){
            $("#rujukankeluar").find("input[type=\'checkbox\']").attr("checked", "checked");
        }else{
            $("#rujukankeluar").find("input[type=\'checkbox\']").attr("checked", false);
        }
    }
</script>

