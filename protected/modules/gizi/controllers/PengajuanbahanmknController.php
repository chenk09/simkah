<?php

class PengajuanbahanmknController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'informasi', 'detailPengajuan'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZPengajuanbahanmkn;
                $modDetailPengajuan = new PengajuanbahandetailT;
                
                $model->alamatpengiriman = ProfilrumahsakitM::model()->find()->alamatlokasi_rumahsakit;
                $model->nopengajuan = Generator::noPengajuanBahan();
                $model->tglpengajuanbahan = date('d M Y H:i:s');
                $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $pegawai_id = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai_id;
                $model->idpegawai_mengajukan = $pegawai_id;
                
                if (isset($_POST['GZPengajuanbahanmkn'])) {
                        var_dump($_POST['GZPengajuanbahanmkn']);
                     $model->attributes = $_POST['GZPengajuanbahanmkn'];
                     $modDetailPengajuan->attributes = $_POST['PengajuanbahandetailT'];
                if (count($_POST['PengajuanbahandetailT']) > 0) {
                    $modDetails = $this->validasiTabular($model, $_POST['PengajuanbahandetailT']);
                
                }
//                        echo '<pre>';
//                        echo print_r($_POST['PengajuanbahandetailT']);
//                        echo '</pre>';
//                        echo '<pre>';
//                        echo print_r($_POST['GZPengajuanbahanmkn']);
//                        echo '</pre>';
//                        exit();
//                
                    if ($model->validate()) {
                        $transaction = Yii::app()->db->beginTransaction();
                        try {
                            $success = true;
                            if ($model->save()) {
//                                    echo '<pre>';
//                                    echo print_r($_POST['PengajuanbahandetailT']);
//                                    echo '</pre>';
//                                    exit();
                                    
                                    $modDetails = $this->validasiTabular($model, $_POST['PengajuanbahandetailT']);
                                    print_r($_POST['PengajuanbahandetailT']);
                                    
                                    
                                    foreach ($modDetails as $i => $data) {
//                                        echo '<pre>';
//                                      echo print_r($data->bahanmakanan_id);
//                                       echo exit();
                                        if ($data->bahanmakanan_id > 0) {
                                            if ($data->save()) {
                                                
                                            } else {
                                                $success = false;
                                            }
//                                            echo '<pre>';
//                                            echo print_r($data->getErrors());
//                                            echo '</pre>';
                                        }
                                        
//                                          exit();
                                    }
                            }
                            if ($success == true) {
                                $transaction->commit();
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                $this->redirect(array('index', 'id' => $model->pengajuanbahanmkn_id));
                            } else {
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                            }
                        } catch (Exception $ex) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan ".$ex->getMessage());
                        }
                } else {
                    Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
                }
            }
		$this->render('index',array(
			'model'=>$model, 'modDetails'=>$modDetails,'modDetailPengajuan'=>$modDetailPengajuan
		));
	}

        protected function validasiTabular($model,$data){
            foreach ($data as $i=>$row){
                $modDetails[$i] = new PengajuanbahandetailT();
                $modDetails[$i]->attributes = $row;
                $modDetails[$i]->pengajuanbahanmkn_id = $model->pengajuanbahanmkn_id;
                $modDetails[$i]->golbahanmakanan_id = $row['golbahanmakanan_id'];
                $modDetails[$i]->bahanmakanan_id = $row['bahanmakanan_id'];
                $modDetails[$i]->nourutbahan = 1;
                $modDetails[$i]->ukuranbahan =$row['ukuranbahan'];
                $modDetails[$i]->merkbahan = $row['merkbahan'];
                $modDetails[$i]->jmlkemasan = $row['jmlkemasan'];
                if (!is_numeric($modDetails[$i]->jmlkemasan)){
                   $modDetails[$i]->jmlkemasan = 0;
                }
                if (!is_numeric($modDetails[$i]->qty_pengajuan)) {
                    $modDetails[$i]->qty_pengajuan = 0;
                }
                $modDetails[$i]->qty_pengajuan = $row['qtypengajuan'];
                $modDetails[$i]->satuanbahan = $row['satuanbahan'];
                $modDetails[$i]->harganettobhn = $row['harganettobhn'];
                $modDetails[$i]->validate();
//                echo '<pre>';
//                echo print_r($modDetails[$i]->getErrors());
//                echo '</pre>';
            }

            return $modDetails;
        }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZPengajuanbahanmkn']))
		{
			$model->attributes=$_POST['GZPengajuanbahanmkn'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pengajuanbahanmkn_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
//	public function actionIndex()
//	{
//		$dataProvider=new CActiveDataProvider('GZPengajuanbahanmkn');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
//	}

	/**
	 * Manages all models.
	 */
	public function actionInformasi()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZPengajuanbahanmkn('search');
//		$model->unsetAttributes();  // clear any default values
                $model->tglAwal = date('d M Y H:i:s');
                $model->tglAkhir = date('d M Y H:i:s');
		if(isset($_GET['GZPengajuanbahanmkn'])){
                    $model->attributes=$_GET['GZPengajuanbahanmkn'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                }

		$this->render('informasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GZPengajuanbahanmkn::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gzpengajuanbahanmkn-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new GZPengajuanbahanmkn;
            $model->attributes=$_REQUEST['GZPengajuanbahanmkn'];
            $judulLaporan='Data GZPengajuanbahanmkn';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function actionDetailPengajuan($id){
            $this->layout ='frameDialog';
            $modPengajuan = PengajuanbahanmknT::model()->findByPk($id);
            $modDetailPengajuan = PengajuanbahandetailT::model()->with('bahanmakanan', 'golbahanmakanan')->findAllByAttributes(array('pengajuanbahanmkn_id'=>$modPengajuan->pengajuanbahanmkn_id), array('order'=>'nourutbahan'));
            $this->render('detailInformasi', array(
                'modPengajuan'=>$modPengajuan,
                'modDetailPengajuan'=>$modDetailPengajuan,
            ));
        }
}
