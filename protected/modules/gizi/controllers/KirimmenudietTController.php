<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class KirimmenudietTController extends SBaseController
{
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $successSaveTindakan = true;
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'indexPegawai', 'informasi', 'detailKirimMenuDiet'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex($idPesan = null , $id = null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZKirimmenudietT;
                $model->jenispesanmenu = Params::JENISPESANMENU_PASIEN;
                if (isset($idPesan)){
                    $modPesan = PesanmenudietT::model()->find('pesanmenudiet_id = '.$idPesan.' and kirimmenudiet_id is null');
                    if (count($modPesan) == 1){
                        $model->bahandiet_id = $modPesan->bahandiet_id;
                        $model->jenisdiet_id = $modPesan->jenisdiet_id;
                        $model->pesanmenudiet_id = $idPesan;
                        $model->jenispesanmenu = $modPesan->jenispesanmenu;
                        
                        if($modPesan->jenispesanmenu == Params::JENISPESANMENU_PASIEN){
//                            $modDetailPesan = PesanmenudetailT::model()->with('menudiet')->findAllByAttributes(array('pesanmenudiet_id'=>$idPesan));
                            $criteria = new CDbCriteria();
                            $criteria->select = 'pasienadmisi_id, pendaftaran_id, pasien_id,  pesanmenudiet_id, jml_pesan_porsi, satuanjml_urt,menudiet_id';
                            $criteria->group = 'pasienadmisi_id, pendaftaran_id, pasien_id, pesanmenudiet_id, jml_pesan_porsi, satuanjml_urt,menudiet_id';
                            $criteria->compare('pesanmenudiet_id', $idPesan);
                            $modDetailPesan = PesanmenudetailT::model()->findAll($criteria);
                        }
                    }
                    $modDetail = PesanmenudetailT::model()->find('pesanmenudiet_id = '.$idPesan.'');
                    $modPendaftaran = PendaftaranT::model()->findByPk($modDetail->pendaftaran_id);
                    if(!empty($modPendaftaran->pasienadmisi_id)){
                        $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                        if(!empty($modPasienAdmisi->pasienpulang_id)){
                            $status = "SUDAH PULANG";
                        }
                    }else{
                        $status = $modPendaftaran->statusperiksa;
                    }

                    if($status == "SUDAH PULANG"){
                        echo "<script>
                                alert('Maaf, status pasien SUDAH PULANG tidak bisa melakukan transaksi Pengiriman Menu Diet Pasien. ');
                                window.top.location.href='".Yii::app()->createUrl('gizi/PesanmenudietT/informasi')."';
                            </script>";
                    }
                }
                
                if(isset($id)){
                    $model = GZKirimmenudietT::model()->findByPk($id);
                }
                
                $model->nokirimmenu = Generator::noKirimMenuDiet();
                $model->tglkirimmenu = date('d M Y H:i:s');
                $modJurnalRekening = new JurnalrekeningT;
                $modRekenings = array();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

            
		if(isset($_POST['GZKirimmenudietT']))
		{
                    $model->attributes=$_POST['GZKirimmenudietT'];
                    $model->ruangan_id = $_POST['GZKirimmenudietT']['ruangan_id'];
                    $model->instalasi_id = $_POST['GZKirimmenudietT']['instalasi_id'];
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = true;
                        if($model->save()){
                            if (!empty($model->pesanmenudiet_id)){PesanmenudietT::model()->updateByPk($model->pesanmenudiet_id, array('kirimmenudiet_id'=>$model->kirimmenudiet_id));}
                            if (count($_POST['KirimmenupasienT']) > 0){
                                foreach($_POST['KirimmenupasienT'] as $i=>$v){
                                    if ($v['checkList'] == 1){
                                        foreach($v['menudiet_id'] as $j=>$x){
                                            if (!empty($x)){
                                                $modDetail = new GZKirimmenupasienT();
                                                //$modDetail->attributes = $v;
                                                $modDetail->ruangan_id = $v['ruangan_id'];
                                                $modDetail->kirimmenudiet_id = $model->kirimmenudiet_id;
                                                $modDetail->pasien_id = $v['pasien_id'];
                                                $modDetail->pasienadmisi_id = $v['pasienadmisi_id'];
                                                $modDetail->menudiet_id = $x;
                                                $modDetail->pendaftaran_id = $v['pendaftaran_id'];
                                                $modDetail->pesanmenudetail_id = $v['pesanmenudetail_id'][$j];
                                                $modDetail->jeniswaktu_id = $j;
                                                $modDetail->jml_kirim = $v['jml_kirim'];
                                                $modDetail->satuanjml_urt = $v['satuanjml_urt'];
                                                $modDetail->kelaspelayanan_id = $v['kelaspelayanan_id'][$j];
                                                $modDetail->hargasatuan = $v['satuanTarif'][$j];
                                                //$modDetail->status_menu = $v['status_menu'];
                                                if(!isset($idPesan)){
                                                    $modDetail->ruangan_id = $model->ruangan_id;
                                                }
                                                    $criteria = new CDbCriteria();
                                                    $criteria->compare('pendaftaran_id', $v['pendaftaran_id'][$j]);
                                                    $modPendaftaran = PendaftaranT::model()->find($criteria);
                                                    //set null pembayaran supaya muncul di informasi belum bayar
                                                    PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id, 
                                                        array('pembayaranpelayanan_id'=>null)
                                                    );
                                                    $criteria2 = new CDbCriteria();
                                                    $criteria2->compare('pendaftaran_id', $v['pendaftaran_id'][$j]);
                                                    $criteria2->compare('ruangan_id',PARAMS::RUANGAN_ID_GIZI);
                                                    $modPasienPenunjang = PasienmasukpenunjangT::model()->find($criteria2);
                                                if ($modDetail->save()){
                                                    if(!empty($modDetail->pesanmenudetail_id)){PesanmenudetailT::model()->updateByPk($modDetail->pesanmenudetail_id, array('kirimmenupasien_id'=>$modDetail->kirimmenupasien_id));}
                                                    if (KonfigsystemK::getKonfigKurangiStokGizi() == true){
                                                        if (StokbahanmakananT::validasiStokMenu($modDetail->jml_kirim, $modDetail->menudiet_id)){
                                                            StokbahanmakananT::kurangiStokMenu($modDetail->jml_kirim, $modDetail->menudiet_id);
                                                        }else{
                                                            $success = true;
                                                        }
                                                    }
                                                } else{
                                                    $success=false;
                                                }
                                                echo '<pre>';
                                                echo print_r($modDetail->Errors,1);
                                            }
                                        }
                                    }
                                }
                                $this->saveTindakanPelayanan($_POST['KirimmenupasienT'],$_POST['RekeningakuntansiV']);
                            } else{
                                $success = false;
                            }
                        } else{
                            $success = false;
                        }
                        if ($success == TRUE && $this->successSaveTindakan){
//                            exit();
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('index','id'=>$model->kirimmenudiet_id));
                            $this->refresh();
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan");
                        }
                    }
                    catch(Exception $ex){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                    }
		}

		$this->render('index',array(
			'model'=>$model, 'modPesan'=>$modPesan, 'modDetailPesan'=>$modDetailPesan,
                        'modPendaftaran'=>$modPendaftaran,
                        'modPasienPenunjang'=>$modPasienPenunjang,
                        'modRekenings'=>$modRekenings
		));
	}
        
	public function actionIndexPegawai($idPesan = null, $id = null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZKirimmenudietT;
                if (isset($idPesan)){
                    $modPesan = PesanmenudietT::model()->find('pesanmenudiet_id = '.$idPesan.' and kirimmenudiet_id is null');
//                    print_r($modPesan->attributes);exit;
                      
                    if (count($modPesan) == 1){
                        $model->bahandiet_id = $modPesan->bahandiet_id;
                        $model->jenisdiet_id = $modPesan->jenisdiet_id;
                        $model->pesanmenudiet_id = $idPesan;
                        $model->jenispesanmenu = $modPesan->jenispesanmenu;
                        if($modPesan->jenispesanmenu != Params::JENISPESANMENU_PASIEN){
                            $criteria = new CDbCriteria();
                            $criteria->select = 'pegawai_id, pesanmenudiet_id, jml_pesan_porsi, satuanjml_urt,menudiet_id';
                            $criteria->group = 'pegawai_id, pesanmenudiet_id, jml_pesan_porsi, satuanjml_urt,menudiet_id';
                            $criteria->compare('pesanmenudiet_id', $idPesan);
                            $modDetailPesan = PesanmenupegawaiT::model()->findAll($criteria);
                        }
                    }
                }
                $model->nokirimmenu = Generator::noKirimMenuDiet();
                $model->tglkirimmenu = date('d M Y H:i:s');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZKirimmenudietT']))
		{
                    $model->attributes=$_POST['GZKirimmenudietT'];
                    $model->jenispesanmenu = $modPesan->jenispesanmenu;
                    if(!isset($idPesan)){
                        $model->jenispesanmenu = $_POST['jenisPesan'];
                    }
//                    echo '<pre>';
//                    echo print_r($modPesan->jenispesanmenu);
//                    exit();
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = true;
                        if($model->save()){
                            if (!empty($model->pesanmenudiet_id)){PesanmenudietT::model()->updateByPk($model->pesanmenudiet_id, array('kirimmenudiet_id'=>$model->kirimmenudiet_id));}
                            if (count($_POST['KirimmenupegawaiT']) > 0){
                                foreach($_POST['KirimmenupegawaiT'] as $i=>$v){
                                    if ($v['checkList'] == 1){
                                        foreach($v['menudiet_id'] as $j=>$x){
                                            if (!empty($x)){
                                                $modDetail = new GZKirimmenupegawaiT();
                                                $modDetail->attributes = $v;
                                                $modDetail->kirimmenudiet_id = $model->kirimmenudiet_id;
                                                $modDetail->pesanmenupegawai_id = $v['pesanmenupegawai_id'][$j];
                                                $modDetail->jeniswaktu_id = $j;
                                                $modDetail->menudiet_id = $x;
                                                if ($modDetail->save()){
                                                    if(!empty($modDetail->pesanmenupegawai_id)){PesanmenupegawaiT::model()->updateByPk($modDetail->pesanmenupegawai_id, array('kirimmenupegawai_id'=>$modDetail->kirimmenupegawai_id));}
                                                    if (KonfigsystemK::getKonfigKurangiStokGizi() == TRUE){
                                                        if (StokbahanmakananT::validasiStokMenu($modDetail->jml_kirim, $modDetail->menudiet_id)){
                                                            StokbahanmakananT::kurangiStokMenu($modDetail->jml_kirim, $modDetail->menudiet_id);                                                            
                                                        }else{
                                                            $success = true;
                                                        }
                                                    }
                                                } else{
                                                    $success=false;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                $success = false;
                            }
                        } else{
                            $success = false;
                        }
                        if ($success == TRUE){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('indexPegawai','id'=>$model->kirimmenudiet_id));
                            $this->refresh();
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    }
                    catch(Exception $ex){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                    }
		}

		$this->render('indexPegawai',array(
			'model'=>$model, 'modPesan'=>$modPesan, 'modDetail'=>$modDetail, 'modDetailPesan'=>$modDetailPesan
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZKirimmenudietT']))
		{
			$model->attributes=$_POST['GZKirimmenudietT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->kirimmenudiet_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZKirimmenudietT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GZKirimmenudietT']))
			$model->attributes=$_GET['GZKirimmenudietT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gzkirimmenudiet-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionPrint()
        {
            $model= new GZKirimmenudietT;
            $model->attributes=$_REQUEST['GZKirimmenudietT'];
            $judulLaporan='Data GZKirimmenudietT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function actionInformasi()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZKirimmenudietT('search');
//		$model->unsetAttributes();  // clear any default values
                $model->tglAwal = date('d M Y H:i:s');
                $model->tglAkhir = date('d M Y H:i:s');
		if(isset($_GET['GZKirimmenudietT'])){
                    $model->attributes=$_GET['GZKirimmenudietT'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                }

		$this->render('informasi',array(
			'model'=>$model,
		));
	}
        
        public function actionDetailKirimMenuDiet($id){
            $this->layout ='/layouts/frameDialog';
            $modKirim = KirimmenudietT::model()->findByPk($id);
            if ($modKirim->jenispesanmenu == Params::JENISPESANMENU_PASIEN){
                $criteria = new CDbCriteria();
                $criteria->select = 'pasienadmisi_id, pendaftaran_id, pasien_id, menudiet_id, kirimmenudiet_id, jml_kirim, satuanjml_urt,ruangan_id';
                $criteria->group = 'pasienadmisi_id, pendaftaran_id, pasien_id, menudiet_id,kirimmenudiet_id, jml_kirim, satuanjml_urt,ruangan_id';
                $criteria->compare('kirimmenudiet_id', $id);
                $modDetailKirim = KirimmenupasienT::model()->findAll($criteria);
            }
            else{
                $criteria = new CDbCriteria();
                $criteria->select = 'pegawai_id, menudiet_id, kirimmenudiet_id, jml_kirim, satuanjml_urt, ruangan_id';
                $criteria->group = 'pegawai_id, menudiet_id, kirimmenudiet_id, jml_kirim, satuanjml_urt, ruangan_id';
                $criteria->compare('kirimmenudiet_id', $id);
                $modDetailKirim = KirimmenupegawaiT::model()->findAll($criteria);
            }
            $this->render('detailInformasi', array(
                'modKirim'=>$modKirim,
                'modDetailKirim'=>$modDetailKirim,
            ));
        }
        
        protected function saveTindakanPelayanan($modDetail,$postRekenings = array())
        {
            $valid=true;
            if (count($_POST['KirimmenupasienT']) > 0){
//                echo '<pre>';
//                print_r($_POST['KirimmenupasienT']);
//                exit;
            foreach($_POST['KirimmenupasienT'] as $i=>$v){
//                echo '<pre>';
//                print_r($v);
//                exit;
                if ($v['checkList'] == 1){
                    foreach($v['menudiet_id'] as $j=>$x){
//                        echo "<pre>";
//                        echo print_r($v['jeniswaktu_id']);
//                        echo "</pre>";
//                        exit;
                        
                        if (!empty($x)){
                            
//                            "echo a";
                            $modPendaftaran = PendaftaranT::model()->findAllByAttributes(array('pendaftaran_id'=>$v['pendaftaran_id']));

                            $daftartindakan_id = $v['daftartindakan_id'];
                            $kelaspelayanan_id = $v['kelaspelayanan_id'];
                            
//                            echo "<pre>";
//                            echo count($_POST['KirimmenupasienT']);
//                            exit;
                            $idDaftarTindakan =  (int) $data;
                            $modTindakans = new TindakanpelayananT;
                            $modTindakans->attributes=$v;
                            
                            $modTindakans->penjamin_id = (int) $v['penjamin_id'];
                            $modTindakans->pasien_id = $v['pasien_id'];
                            $modTindakans->kelaspelayanan_id = (int) $v['kelaspelayanan_id'][$j];
                            $modTindakans->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                            $modTindakans->instalasi_id = Params::INSTALASI_ID_GIZI; //$modPendaftaran->instalasi_id;
                            $modTindakans->pendaftaran_id = $v['pendaftaran_id'];
                            $modTindakans->shift_id = Yii::app()->user->getState('shift_id');
                            $modTindakans->pasienmasukpenunjang_id = null;
                            $modTindakans->daftartindakan_id = $v['daftartindakan_id'][$j];
                            $modTindakans->carabayar_id = (int) $v['carabayar_id'][$j];
                            $modTindakans->jeniskasuspenyakit_id = (int) $v['jeniskasuspenyakit_id'];
                            $modTindakans->tgl_tindakan = date('Y-m-d H:i:s');
                            $modTindakans->tarif_satuan = $v['satuanTarif'][$j];
                            $modTindakans->tarif_tindakan = $v['satuanTarif'][$j] * $v['jml_kirim'];
                            $modTindakans->satuantindakan = "HARI";
                            $modTindakans->qty_tindakan = $v['jml_kirim'];
    //                        $modTindakans[$i]->cyto_tindakan = $item['cyto'];
                            $modTindakans->cyto_tindakan = 0;

    //                        $modTindakans[$i]->tarifcyto_tindakan = ($item['cyto']) ? (($item['cyto'] / 100) * $modTindakans[$i]->tarif_tindakan) : 0;
                            $modTindakans->tarifcyto_tindakan =((0 / 100) * $modTindakans->tarif_tindakan);
                            $modTindakans->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
                            $modTindakans->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;

                            $modTindakans->discount_tindakan = 0;
                            $modTindakans->subsidiasuransi_tindakan = 0;
                            $modTindakans->subsidipemerintah_tindakan = 0;
                            $modTindakans->subsisidirumahsakit_tindakan = 0;
                            $modTindakans->iurbiaya_tindakan = 0;
                            $modTindakans->ruangan_id = Yii::app()->user->getState('ruangan_id');
                            
                            $valid = $modTindakans->validate() && $valid;
//                             
//                            echo "<pre>";
//                            echo print_r($modTindakans->getErrors());
//                            exit;
                            
                            if($valid){
                                if($modTindakans->save()){
                                    if(isset($postRekenings)){
                                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                                        //update jurnalrekening_id
                                        $modTindakans->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                                        $modTindakans->save();
                                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakans, 'tm');
                                    }
                                      $statusSaveKomponen = $this->saveTindakanKomponen($modTindakans);
//                                      KirimmenupasienT::model()->updateByPk($modDetail->kirimmenupasien_id,array('tindakanpelayanan_id'=>$modTindakans->tindakanpelayanan_id));
                                }
                                
                                if($statusSaveKomponen) {
                                    $this->successSaveTindakan = true;
                                } else {
                                    $this->successSaveTindakan = false;
                                }
                            } else {
                                $this->successSaveTindakan = false;
                            }
                        }
                    }
                }
            }
        }
        
            return $modTindakans;
        }
        
        
        protected function saveTindakanKomponen($tindakan)
        {   
            $valid = true;
            $criteria = new CDbCriteria();
//            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->compare('daftartindakan_id', $tindakan->daftartindakan_id);
            $criteria->compare('kelaspelayanan_id', $tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen[$i] = new TindakankomponenT;
                $modTindakanKomponen[$i]->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen[$i]->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen[$i]->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen[$i]->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen[$i]->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen[$i]->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen[$i]->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen[$i]->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen[$i]->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen[$i]->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen[$i]->validate() && $valid;
//                echo "<pre>";
//                        echo print_r($modTarifs);
//                        exit;
                if($valid)
                    $modTindakanKomponen[$i]->save();
            }
            
            return $valid;
        }        
}
?>
