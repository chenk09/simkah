<?php

class SupplierMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $successSaveSupplier=false;
        
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZSupplierM;
                $modObatSupplier = new GZObatSupplierM;
                $model->supplier_jenis = Params::GIZI_JENIS_SUPPLIER;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZSupplierM']))
		{
           $cekObatAlkes=0; 
           $transaction = Yii::app()->db->beginTransaction();
           try {
				$model->attributes=$_POST['GZSupplierM'];
				if($model->validate()){//Jika Data Untuk Model Supplier Valid
                        if($model->save()){//Jika Model Supplier Sudah Disimpan
                             $this->successSaveSupplier=true;   
                             $jumlahObatAlkes=COUNT($_POST['obatalkes_id']);
                             $idSupplier=$model->supplier_id;
                            for($i=0; $i<=$jumlahObatAlkes; $i++):
                              $modObatSupplier = new GZObatSupplierM;
                              $modObatSupplier->supplier_id=$idSupplier;
                              $modObatSupplier->obatalkes_id=$_POST['obatalkes_id'][$i];
                              if($modObatSupplier->validate()){//Jika Data ObatSupplierM Valid
                                 if($modObatSupplier->save()){//Jika ObatSupplierM Sudah Berhasil Disimpan
                                    $cekObatAlkes++; 
                                 }else{
                                     Yii::app()->user->setFlash('error', "Data Obat Supplier Gagal Disimpan");
                                 }   
                              }
                            endfor;
                           }else{//JIka Model Supplier Gagal Disimpan
                                Yii::app()->user->setFlash('error', "Data Supplier Gagal Disimpan");
                           }  
                        }else{//Jika Data Supplier Tidak Valid
                           Yii::app()->user->setFlash('error', "Data Supplier Tidak Valid");
                        }
	            if($this->successSaveSupplier && ($cekObatAlkes == $jumlahObatAlkes)){
	               $transaction->commit();
	               Yii::app()->user->setFlash('success', "Data Suplier dan Obat Supplier Berhasil Disimpan");
	               $this->redirect(array('admin'));
	            }else{
	              Yii::app()->user->setFlash('error', "Data Suplier dan Obat Supplier Gagal Disimpan");
	            }     

           }catch(Exception $exc){
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
           }
		}

		$this->render('create',array(
			'model'=>$model,'modObatSupplier'=>$modObatSupplier
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
                $modObatSupplier=GZObatSupplierM::model()->findAll('supplier_id='.$id.'');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZSupplierM']))
		{
			if(isset($_POST['GZSupplierM']))
                        {
                           $cekObatAlkes=0; 
                           $transaction = Yii::app()->db->beginTransaction();
                           try {
                                $model->attributes=$_POST['GZSupplierM'];
                                if($model->validate()){//Jika Data Untuk Model Supplier Valid
                                   if($model->save()){//Jika Model Supplier Sudah Disimpan
                                    $idSupplier=$model->supplier_id;  
                                    $hapusObatSupplier=GZObatSupplierM::model()->deleteAll('supplier_id='.$idSupplier.''); 
                                    $this->successSaveSupplier=true;   
                                    $jumlahObatAlkes=COUNT($_POST['obatalkes_id']);
                                    for($i=0; $i<=$jumlahObatAlkes; $i++):
                                      $modObatSupplier = new GZObatSupplierM;
                                      $modObatSupplier->supplier_id=$idSupplier;
                                      $modObatSupplier->obatalkes_id=$_POST['obatalkes_id'][$i];
                                      if($modObatSupplier->validate()){//Jika Data ObatSupplierM Valid
                                         if($modObatSupplier->save()){//Jika ObatSupplierM Sudah Berhasil Disimpan
                                            $cekObatAlkes++; 
                                         }else{
                                             Yii::app()->user->setFlash('error', "Data Obat Supplier Gagal Disimpan");
                                         }   
                                      }
                                    endfor;
                                   }else{//JIka Model Supplier Gagal Disimpan
                                        Yii::app()->user->setFlash('error', "Data Supplier Gagal Disimpan");
                                   }  
                                }else{//Jika Data Supplier Tidak Valid
                                   Yii::app()->user->setFlash('error', "Data Supplier Tidak Valid");
                                }

                               if($this->successSaveSupplier && ($cekObatAlkes==$jumlahObatAlkes)){
                                   $transaction->commit();
                                   Yii::app()->user->setFlash('success', "Data Suplier dan Obat Supplier Berhasil Disimpan");
                                   $this->redirect(array('admin'));
                               }else{
                                  Yii::app()->user->setFlash('error', "Data Suplier dan Obat Supplier Gagal Disimpan");
                               }     

                           }catch(Exception $exc){
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                           }
                        }
		}

		$this->render('update',array(
			'model'=>$model,'modObatSupplier'=>$modObatSupplier
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('GZSupplierM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZSupplierM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GZSupplierM']))
			$model->attributes=$_GET['GZSupplierM'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GZSupplierM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gzsupplier-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		//if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		if(Yii::app()->request->isPostRequest)
		{
			//$hapusObatSupplier=GZObatSupplierM::model()->deleteAll('supplier_id='.$id.'');
			$id = $_POST['id'];
            $this->loadModel($id)->delete();
            if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                        ));
                    exit;
                }
	                    
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

     /**
     *Mengubah status aktif
     * @param type $id 
     */
    public function actionRemoveTemporary()
    {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                    SAPropinsiM::model()->updateByPk($id, array('propinsi_aktif'=>false));
//                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
              
        
        $id = $_POST['id'];   
        if(isset($_POST['id']))
        {
           $update = GZSupplierM::model()->updateByPk($id,array('supplier_aktif'=>false));
           if($update)
            {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
             }
        } else {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
        }

    }
        
        public function actionPrint()
        {
            $model= new GZSupplierM;
            $model->attributes=$_REQUEST['GZSupplierM'];
            $judulLaporan='Data Supplier';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
