<?php

class PendaftaranKonsultasiGiziController extends SBaseController
{
        public $successSave = false;
        public $successSaveAdmisi = false; //variabel untuk validasi admisi
        public $successSaveRujukan = true; //variabel untuk validasi data opsional (rujukan)
        public $successSavePJ = false; //variabel untuk validasi data opsional (penanggung jawab)
        public $successSaveTindakanKomponen = true;
        public $successSaveTindakanPelayanan = true;
        
        protected function performAjaxValidation($model)
        {
            if(isset($_POST['ajax']) && $_POST['ajax']==='pppendaftaran-mp-form')
            {
              echo CActiveForm::validate($modTindakanPelayananT,$modTindakanKomponen,$model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienPenunjang);
              Yii::app()->end();
            }   
        }
        
          /**
         * action index digunakan di pendaftaran Gizi pasien luar
         * digunakan di :
         * 1. Gizi -> pendaftaran Konsultasi Gizi
         * @param int $id pendaftaran_id mengambil nilai get dari index pendaftaran
         */
	public function actionIndex($id = null)
	{
                $this->pageTitle = Yii::app()->name." - Pendaftaran Pasien Laboratorium";
                $model=new LKPendaftaranMp;
                $format = new CustomFormat();
                $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $model->kelaspelayanan_id = Params::kelasPelayanan('rawat_jalan');
                $model->tgl_pendaftaran = date('d M Y H:i:s');
                $model->tglrenkontrol = null;
                $model->umur = "00 Thn 00 Bln 00 Hr";
                $model->pakeSample = true;
                
            if(!empty($id)){
                  $model        = LKPendaftaranMp::model()->findByPk($id);
                  $modPasien    = PasienM::model()->findByPk($model->pasien_id);
                  $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
                  $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
             
                  if (!empty($model->penanggungjawab_id)){
                        $modPenanggungJawab = PenanggungjawabM::model()->findByPk($model->penanggungjawab_id);  
                  }
                  if(!empty($model->rujukan_id)){
                       $modRujukan   = RujukanT::model()->findByPk($model->rujukan_id);
                  }
                
            }
                $modPasien = new LKPasienM;
                $modPenanggungJawab = new LKPenanggungJawabM;
                $modRujukan = new LKRujukanT;
                $modPasienPenunjang = new LKPasienMasukPenunjangT;
                $modTindakanPelayananT = new LKTindakanPelayananT;
                $modTindakanKomponen = new LKTindakanKomponenT;

                $modRincian = new LKRinciantagihanpasienpenunjangV;
                
                $model->isRujukan = (isset($_POST['isRujukan'])) ? true : false;
                $model->isUpdatePasien = (isset($_POST['isUpdatePasien'])) ? true : false;
                $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
                $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
                $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
                $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            
//            if (isset($id)){
//              $model        = PendaftaranT::model()->findByPk($id);
//              $modPasien    = PasienM::model()->findByPk($model->pasien_id);
//              $modRujukan   = RujukanT::model()->findByPk($model->rujukan_id);
//              $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
//              $modRincian = LKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
//             
//              if (isset($model->penanggungjawab_id))
//                $modPenanggungJawab = PenanggungjawabM::model()->findByPk($model->penanggungjawab_id);
//           }
//========================== Form General ==============================
            if(isset($_POST['LKPendaftaranMp'])){
//                 echo "<pre>";
//                print_r($model->getAttributes());
//                echo "</pre>";
//                echo "<pre>";
//                print_r($modPasien->getAttributes());
//                echo "</pre>";
//                echo "<pre>";
//                print_r($modPenanggungJawab->getAttributes());
//                echo "</pre>";
//                echo "<pre>";
//                print_r($modRujukan->getAttributes());
//                echo "</pre>";
//                echo "<pre>";
//                print_r($modPasienPenunjang->getAttributes());
//                echo "</pre>";
//                echo "<pre>";
//                print_r($modTindakanPelayananT->getAttributes());
//                echo "</pre>";
//                echo "<pre>";
//                print_r($modTindakanKomponen->getAttributes());
//                echo "</pre>";
//                exit;
                $model->attributes = $_POST['LKPendaftaranMp'];
                $modPasien->attributes = $_POST['LKPasienM'];
                $modPenanggungJawab->attributes = $_POST['LKPenanggungJawabM'];
                $modRujukan->attributes = $_POST['LKRujukanT'];
                
                $model->tgl_pendaftaran = $format->formatDateTimeMediumForDB($_POST['LKPendaftaranMp']['tgl_pendaftaran']);
               
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    //==Penyimpanan dan Update Pasien===========================
                    
                    if(isset($_POST['LKPasienM'])){
                        $modPasien = $this->savePasien($_POST['LKPasienM'],$photo,$_POST['caraAmbilPhoto']);
                    }
                    
                    if(isset($_POST['LKRujukanT'])) {
                        $modRujukan = $this->saveRujukan($_POST['LKRujukanT']);
                    }
                    
                    //===penyimpanan Penanggung Jawab===========================                   
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $modPenanggungJawab->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['LKPenanggungJawabM']);
                    }
                    //===Akhir Penyimpanan Penanggung Jawab===================== 
                    if(empty($_POST['LKPendaftaranMp']['tgl_pendaftaran'])){
                        $model->tgl_pendaftaran = date('Y-m-d H:i:s');
                    }
                    
                    //==Simpan Transaksi Pendaftaran ==
                    if(isset($_POST['LKPendaftaranMp'])){
                        $modelNew = $this->savePendaftaranMP($model,$modPasien,$modRujukan,$modPenanggungJawab);
                    }else{
                        $modelNew = $this->savePendaftaranMP($modRadiologi,$modPasien,$modRujukan,$modPenanggungJawab);
                    }
                    
                    
                    $model->attributes = $modelNew->attributes;
                    $this->savePasienPenunjang($model,$modPasien);
                    
                    if(isset($_POST['TindakanPelayananT']) && (!empty($_POST['TindakanPelayananT']['idTindakan'])) && (!empty($_POST['tarifKarcis'])))
                    {
                        $this->saveTindakanPelayanan($modPasien,$model);
                    }else{
                        $this->successSaveTindakanPelayanan = true;
                    }
                    
                    if($this->successSave && $this->successSaveRujukan && $this->successSaveTindakanKomponen){   
                        $transaction->commit();  
                        $model->isNewRecord = false;
                        Yii::app()->user->setFlash('success',"Transaksi berhasil disimpan");
                        $this->redirect(array('index','id'=>$model->pendaftaran_id));
                    } else {
                        $model->isNewRecord = true;
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Transaksi Gagal disimpan");
                    }
//========================== End Form General ==========================
           
            }catch (Exception $exc){
                    $model->isNewRecord = true;
                    $transaction->rollback();
//                    Yii::app()->user->setFlash('error',"Data Gagal disimpan");
                    Yii::app()->user->setFlash('error',"Transaksi gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('index',array(
                'model'=>$model,
                'modPasien'=>$modPasien, 
                'modPenanggungJawab'=>$modPenanggungJawab,
                'modRujukan'=>$modRujukan,
                'modPasienPenunjang'=>$modPasienPenunjang,
                'modTindakanKomponen'=>$modTindakanKomponen,
                'modTindakanPelayananT'=>$modTindakanPelayananT,
                'modRincian' => $modRincian,
            ));
	}
        
        public function savePasien($attrPasien,$photo,$caraAmbilPhoto)
        {

            $modPasien = new LKPasienM;
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modPasien->no_rekam_medik = Generator::noRekamMedikGizi(Params::singkatanNoPendaftaranGZ());
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = 'AKTIF';
            $modPasien->create_ruangan =Yii::app()->user->getState('ruangan_id'); 
            $modPasien->ispasienluar = 1;

              if($caraAmbilPhoto=='file'){//Jika User Mengambil photo pegawai dengan cara upload file

                  $gambar=$modPasien->photopasien;
                  if(!empty($model->photopasien)){//Klo User Memasukan Logo
                         $modPasien->photopasien =$random.$modPasien->photopasien;

                         Yii::import("ext.EPhpThumb.EPhpThumb");

                         $thumb=new EPhpThumb();
                         $thumb->init(); //this is needed

                         $fullImgName =$modPasien->photopasien;   
                         $fullImgSource = Params::pathPasienDirectory().$fullImgName;
                         $fullThumbSource = Params::pathPasienTumbsDirectory().'kecil_'.$fullImgName;

                         if($modPasien->save()){
                            $gambar->saveAs($fullImgSource);
                            $thumb->create($fullImgSource)
                                  ->resize(200,200)
                                  ->save($fullThumbSource);
                         }else{
                             echo "gagal Ipload";exit;
                             }
                    }else{
                        $modPasien->save();
                    }
                    
              }else{
                 $modPasien->photopasien=$photo;
                 if($modPasien->validate())
                    {
//                        echo "<pre>";
//                        echo print_r($modPasien->getErrors());
//                        echo print_r($modPasien->getAttributes());
//                        echo "</pre>";
//                        exit;
                        $modPasien->save();
                    }
                 else 
                    {

                         unlink(Params::pathPasienDirectory().$photo);
                         unlink(Params::pathPasienTumbsDirectory().$photo);
                    }
               }
                

            return $modPasien;
        }
        
        public function updatePasien($modPasien,$attrPasien,$photo,$caraAmbilPhoto)
        {
            $modPasienupdate = $modPasien;
            $modPasienupdate->attributes = $attrPasien;
            $modPasien->ispasienluar = 1;
            $modPasienupdate->kelompokumur_id = Generator::kelompokUmur($modPasienupdate->tanggal_lahir);
            $temppPhoto=$modPasienupdate->photopasien;
            if($caraAmbilPhoto=='file')//Jika User Mengambil photo pegawai dengan cara upload file
              { 
                  $gambar=$modPasien->photopasien;

                  if(!empty($model->photopasien))//Klo User Memasukan Logo
                  { 
                         $modPasien->photopasien =$random.$modPasien->photopasien;

                         Yii::import("ext.EPhpThumb.EPhpThumb");

                         $thumb=new EPhpThumb();
                         $thumb->init(); //this is needed

                         $fullImgName =$modPasien->photopasien;   
                         $fullImgSource = Params::pathPasienDirectory().$fullImgName;
                         $fullThumbSource = Params::pathPasienTumbsDirectory().'kecil_'.$fullImgName;

                         if($modPasien->save()){
                                   if(!empty($temppPhoto)){
                                        unlink(Params::pathPasienDirectory().$photo);
                                        unlink(Params::pathPasienTumbsDirectory().$photo);
                                   } 
                                   $gambar->saveAs($fullImgSource);
                                   $thumb->create($fullImgSource)
                                         ->resize(200,200)
                                         ->save($fullThumbSource);
                              }
                    }
              }   
             else 
              {
                 $modPasien->photopasien=$photo;
                 if($modPasien->validate())
                    {
                        $modPasien->save();
                    }
                 else 
                    {
                         unlink(Params::pathPegawaiDirectory().$photo);
                         unlink(Params::pathPegawaiTumbsDirectory().$photo);
                    }
               }
            
            return $modPasienupdate;
        }
        
        public function saveRujukan($attrRujukan)
        {
            $modRujukan = new LKRujukanT;
            $format = new CustomFormat();
            $modRujukan->attributes = $attrRujukan;
            $modRujukan->tanggal_rujukan = ($attrRujukan['tanggal_rujukan'] == '') ? date('Y-m-d H:i:s') : $format->formatDateTimeMediumForDB($attrRujukan['tanggal_rujukan']) ;
            $modRujukan->asalrujukan_id = ($attrRujukan['asalrujukan_id'] == '') ? null: $attrRujukan['asalrujukan_id'] ;
            $modRujukan->rujukandari_id = ($attrRujukan['rujukandari_id'] == '') ? null: $attrRujukan['rujukandari_id'] ;
            if(!empty($modRujukan->rujukandari_id)){
                $modCariRujukanDari = RujukandariM::model()->findByPk($modRujukan->rujukandari_id);
                $modRujukan->nama_perujuk = $modCariRujukanDari->namaperujuk;
            }
            if($modRujukan->validate()) {
                // form inputs are valid, do something here
                $modRujukan->save();
                $this->successSaveRujukan = TRUE;
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modRujukan->tanggal_rujukan = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modRujukan->tanggal_rujukan, 'yyyy-MM-dd'),'medium',null);
                $this->successSaveRujukan = FALSE;
            }
            return $modRujukan;
        }
        
        public function savePenanggungJawab($attrPenanggungJawab)
        {
            $modPenanggungJawab = new LKPenanggungJawabM;
            $modPenanggungJawab->attributes = $attrPenanggungJawab;
            if($modPenanggungJawab->validate()) {
                // form inputs are valid, do something here
//                 echo "<pre>";
//                echo print_r($modPenanggungJawab->getErrors());
//                echo print_r($modPenanggungJawab->getAttributes());
//                echo "</pre>";
//                exit;
                $modPenanggungJawab->save();
                $this->successSavePJ = TRUE;
            } else {
               $this->successSavePJ = FALSE;
            }

            return $modPenanggungJawab;
        }
        
        public function savePendaftaranMP($model, $modPasien, $modRujukan, $modPenanggungJawab){
            $modelNew = new LKPendaftaranMp;
            $format = new CustomFormat();
            $modelNew->attributes = $model->attributes;
            $modelNew->tgl_pendaftaran = $model->tgl_pendaftaran;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            $modelNew->ruangan_id=Yii::app()->user->getState('ruangan_id');
            $modelNew->instalasi_id =Yii::app()->user->getState('instalasi_id');
            $modelNew->no_pendaftaran = Generator::noPendaftaran(Params::singkatanNoPendaftaranGZ());
            $modelNew->no_urutantri = Generator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = Generator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
            $modelNew->statuspasien = Generator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = "KUNJUNGAN BARU";
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->statusmasuk = Params::statusMasuk('rujukan');
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modelNew->tgl_konfirmasi = null;
            $modelNew->tglrenkontrol = null;
            if ($modelNew->validate()){
//                echo "<pre>";
//                echo print_r($modelNew->getErrors());
//                echo print_r($modelNew->getAttributes());
//                echo "</pre>";
//                exit;
                $modelNew->save();
                $this->successSave = true;
            }
            else{
//                $modelNew->tgl_pendaftaran = date('Y-m-d H:i:s');
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'), 'medium', null);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
           
            return $modelNew;
        }
        
        public function savePasienPenunjang($attrPendaftaran,$attrPasien){
            
            $modPasienPenunjang = new LKPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $attrPasien->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjangGZ(PARAMS::singkatanNoPendaftaranGZ());
            $modPasienPenunjang->tglmasukpenunjang = $attrPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = "ANTRIAN";
            $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
            
            
            if ($modPasienPenunjang->validate()){
//                echo "<pre>";
//                echo print_r($modPasienPenunjang->getErrors());
//                echo print_r($modPasienPenunjang->getAttributes());
//                echo "</pre>";
//                exit;
                $modPasienPenunjang->Save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }
        
        public function saveTindakanPelayanan($modPasien,$model)
        {
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New GZTindakanPelayananT;
            $modTindakanPelayan->penjamin_id = $model->penjamin_id;
            $modTindakanPelayan->pasien_id = $modPasien->pasien_id;
            $modTindakanPelayan->kelaspelayanan_id=$model->kelaspelayanan_id;
            $modTindakanPelayan->tarif_satuan= (isset($_POST['TindakanPelayananT']['tarifSatuan']) ? $_POST['TindakanPelayananT']['tarifSatuan'] : 0);
            $modTindakanPelayan->instalasi_id=Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id=$model->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id=$_POST['TindakanPelayananT']['idTindakan'];
            $modTindakanPelayan->carabayar_id=$model->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$model->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
//            $modTindakanPelayan->tarif_satuan=$_POST['TindakanPelayananT']['tarifSatuan'];
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$model->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            $modTindakanPelayan->ruangan_id = Yii::app()->user->getState('ruangan_id');
            
            
            if(!empty($_POST['TindakanPelayananT']['idKarcis'])){
                $modTindakanPelayan->karcis_id=$_POST['TindakanPelayananT']['idKarcis'];
                $modTindakanPelayan->tipepaket_id = $this->tipePaketKarcis($model, $_POST['TindakanPelayananT']['idKarcis'], $_POST['TindakanPelayananT']['idTindakan']);
            }

                $tarifTindakan= GZTariftindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                foreach($tarifTindakan AS $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;
             if($modTindakanPelayan->save()){
//                    echo print_r($modTindakanPelayanan->getAttributes());
                    $tindakanKomponen= GZTariftindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                    $jumlahKomponen=COUNT($tindakanKomponen);

                    foreach ($tindakanKomponen AS $tampilKomponen):
                            $modTindakanKomponen=new GZTindakanKomponenT;
                            $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                            $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                            $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                            $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                            $modTindakanKomponen->tarifcyto_tindakankomp=0;
                            $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                            $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                            $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                            $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                            if($modTindakanKomponen->save()){
                                $cekTindakanKomponen++;
                            }
                    endforeach;
                    if($cekTindakanKomponen!=$jumlahKomponen){
                           $this->successSaveTindakanKomponen=false;
                    }
                    
//                    exit;
            } 
            else{
                throw new Exception("Karcis Tindakan gagal disimpan");
            }
        }
        
        public function tipePaketKarcis($modPendaftaran,$idKarcis,$idTindakan)
        {
            $criteria = new CDbCriteria;
            $criteria->with = array('tipepaket');
            $criteria->compare('daftartindakan_id', $idTindakan);
            $criteria->compare('tipepaket.carabayar_id', $modPendaftaran->carabayar_id);
            $criteria->compare('tipepaket.penjamin_id', $modPendaftaran->penjamin_id);
            $criteria->compare('tipepaket.kelaspelayanan_id', $modPendaftaran->kelaspelayanan_id);
            $paket = PaketpelayananM::model()->find($criteria);
            $result = Params::TIPEPAKET_NONPAKET;
            if(isset($paket)) $result = $paket->tipepaket_id;
            
            return $result;
        }
}

?>