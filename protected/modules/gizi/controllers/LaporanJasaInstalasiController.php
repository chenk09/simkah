<?php
Yii::import('rawatJalan.controllers.LaporanController');
Yii::import('rawatJalan.models.*');
class LaporanJasaInstalasiController extends LaporanController {
    
    public $pathView = 'rawatJalan.views.laporan.';
    public $pathViewLap = 'rawatJalan.views.laporan.';
    
    public function actionLaporanJasaInstalasi() {
            $model = new RJLaporanjasainstalasi('search');
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('d M Y H:i:s');
            $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
            $model->penjamin_id = $penjamin;
            $model->tindakansudahbayar_id = Params::statusBayar();
            if (isset($_GET['RJLaporanjasainstalasi'])) {
                $model->attributes = $_GET['RJLaporanjasainstalasi'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanjasainstalasi']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanjasainstalasi']['tglAkhir']);
            }

            $this->render($this->pathView.'jasaInstalasi/adminJasaInstalasi', array(
                'model' => $model, 'filter'=>$filter
            ));
        }

        public function actionPrintLaporanJasaInstalasi() {
            $model = new RJLaporanjasainstalasi('search');
            $judulLaporan = 'Laporan Jasa Instalasi Rawat Jalan';

            //Data Grafik
            $data['title'] = 'Grafik Laporan Jasa Instalasi';
            $data['type'] = $_REQUEST['type'];

            if (isset($_REQUEST['RJLaporanjasainstalasi'])) {
                $model->attributes = $_REQUEST['RJLaporanjasainstalasi'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanjasainstalasi']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanjasainstalasi']['tglAkhir']);
            }

            $caraPrint = $_REQUEST['caraPrint'];
            $target = $this->pathView.'jasaInstalasi/_printJasaInstalasi';

            $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

        public function actionFrameGrafikLaporanJasaInstalasi() {
            $this->layout = '//layouts/frameDialog';
            $model = new RJLaporanjasainstalasi('search');
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('d M Y H:i:s');

            //Data Grafik
            $data['title'] = 'Grafik Laporan Jasa Instalasi';
            $data['type'] = $_GET['type'];
            if (isset($_GET['RJLaporanjasainstalasi'])) {
                $model->attributes = $_GET['RJLaporanjasainstalasi'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanjasainstalasi']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanjasainstalasi']['tglAkhir']);
            }

            $this->render($this->pathViewLap.'_grafik', array(
                'model' => $model,
                'data' => $data,
            ));
        }
}
?>
