<?php

class DaftarPasienController extends SBaseController
{
        public $successPengambilanSample = false;
        public $successKirimSample = false;
	public function actionIndex()
	{
                $modPasienMasukPenunjang = new GZPasienMasukPenunjangV;
                $format = new CustomFormat();
                $modPasienMasukPenunjang->tglAwal=date('d M Y').' 00:00:00';
                $modPasienMasukPenunjang->tglAkhir=date('d M Y H:i:s');
                if(isset ($_REQUEST['GZPasienMasukPenunjangV'])){
                    $modPasienMasukPenunjang->attributes=$_REQUEST['GZPasienMasukPenunjangV'];
                    $modPasienMasukPenunjang->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZPasienMasukPenunjangV']['tglAwal']);
                    $modPasienMasukPenunjang->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZPasienMasukPenunjangV']['tglAkhir']);
                }
		$this->render('index',array('modPasienMasukPenunjang'=>$modPasienMasukPenunjang));
	}
        
	public function actionDetails($idPendaftaran,$idPasien,$idPasienMasukPenunjang)
	{
            $this->layout = '//layouts/printKartu';
            
            $masukpenunjang = PasienmasukpenunjangT::model()->findByPk($idPasienMasukPenunjang);  
            $pemeriksa = PegawaiM::model()->findByPk($masukpenunjang->pegawai_id);
            
            $modHasilPeriksa = HasilpemeriksaanlabV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $detailHasil = DetailhasilpemeriksaanlabT::model()->findAllByAttributes(array('hasilpemeriksaanlab_id'=>$modHasilPeriksa->hasilpemeriksaanlab_id),array('order'=>'detailhasilpemeriksaanlab_id'));
            $hasil = array();
            foreach ($detailHasil as $i => $detail) {
                $jenisPeriksa = $detail->pemeriksaanlab->jenispemeriksaan->jenispemeriksaanlab_nama;
                $kelompokDet = $detail->pemeriksaandetail->nilairujukan->kelompokdet;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['namapemeriksaan'] = $detail->pemeriksaandetail->nilairujukan->namapemeriksaandet;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['hasil'] = $detail->hasilpemeriksaan;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['nilairujukan'] = $detail->nilairujukan;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['satuan'] = $detail->hasilpemeriksaan_satuan;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['keterangan'] = $detail->pemeriksaandetail->nilairujukan->nilairujukan_keterangan;
            }
            
            $this->render('details',array('modHasilPeriksa'=>$modHasilPeriksa,
                                               'detailHasil'=>$detailHasil,
                                               'hasil'=>$hasil, 
                                               'masukpenunjang'=>$masukpenunjang,
                                               'pemeriksa'=>$pemeriksa));
	}
        
        public function actionPrint($idPasienMasukPenunjang,$caraPrint){
            $judulLaporan = 'Laporan Detail Permintaan Penawaran';
            $masukpenunjang = PasienmasukpenunjangT::model()->findByPk($idPasienMasukPenunjang);  
            $pemeriksa = PegawaiM::model()->findByPk($masukpenunjang->pegawai_id);            
            $modHasilPeriksa = HasilpemeriksaanlabV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $detailHasil = DetailhasilpemeriksaanlabT::model()->findAllByAttributes(array('hasilpemeriksaanlab_id'=>$modHasilPeriksa->hasilpemeriksaanlab_id),array('order'=>'detailhasilpemeriksaanlab_id'));
            $hasil = array();
            foreach ($detailHasil as $i => $detail) {
                $jenisPeriksa = $detail->pemeriksaanlab->jenispemeriksaan->jenispemeriksaanlab_nama;
                $kelompokDet = $detail->pemeriksaandetail->nilairujukan->kelompokdet;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['namapemeriksaan'] = $detail->pemeriksaandetail->nilairujukan->namapemeriksaandet;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['hasil'] = $detail->hasilpemeriksaan;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['nilairujukan'] = $detail->nilairujukan;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['satuan'] = $detail->hasilpemeriksaan_satuan;
                $hasil[$jenisPeriksa][$kelompokDet][$i]['keterangan'] = $detail->pemeriksaandetail->nilairujukan->nilairujukan_keterangan;
            }
            
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
//                $this->render('Print',array('judulLaporan'=>$judulLaporan,
//                                            'caraPrint'=>$caraPrint,
//                                            'modPermintaanPenawaran'=>$modPermintaanPenawaran,
//                                            'modPermintaanPenawaranDetails'=>$modPermintaanPenawaranDetails));
                $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                                'modHasilPeriksa'=>$modHasilPeriksa,
                                               'detailHasil'=>$detailHasil,
                                               'hasil'=>$hasil, 
                                               'masukpenunjang'=>$masukpenunjang,
                                               'pemeriksa'=>$pemeriksa));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                                'modHasilPeriksa'=>$modHasilPeriksa,
                                               'detailHasil'=>$detailHasil,
                                               'hasil'=>$hasil, 
                                               'masukpenunjang'=>$masukpenunjang,
                                               'pemeriksa'=>$pemeriksa));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                                'modHasilPeriksa'=>$modHasilPeriksa,
                                               'detailHasil'=>$detailHasil,
                                               'hasil'=>$hasil, 
                                               'masukpenunjang'=>$masukpenunjang,
                                               'pemeriksa'=>$pemeriksa),true));
                
                $mpdf->Output();
            }   
        }
        
        public function actionBatalPeriksaPasienLuar()
        {            
            $ajax = Yii::app()->request->isAjaxRequest;
            if($ajax){
                $idpendaftaran  = $_POST['idpendaftaran'];
                $pendaftaran    = PendaftaranT::model()->findByPk($idpendaftaran);
                $pasien         = PasienM::model()->findByPk($pendaftaran->pasien_id);
                $pasienMasukPenunjang   = PasienmasukpenunjangT::model()->findByAttributes(array('pasien_id'=>$pendaftaran->pasien_id));
                $hasilPemeriksaanLab    = HasilpemeriksaanlabT::model()->findByAttributes(array('pendaftaran_id'=>$idpendaftaran));
                
                $pasienKirimKeUnitLain  = PasienkirimkeunitlainT::model()->findByAttributes(array('pendaftaran_id'=>$idpendaftaran, 'instalasi_id'=>5));
                $detailHasilPemeriksaanLab = DetailhasilpemeriksaanlabT::model()->findAllByAttributes(array('hasilpemeriksaanlab_id'=>$hasilPemeriksaanLab->hasilpemeriksaanlab_id));
                
                $cekPasien = substr($pendaftaran->no_pendaftaran, 0,2);
//                jika pasien berasal dari pendaftaran pasien luar
                if($cekPasien=='LB'){
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
    //                   update rujuakan_id = null terlebih dahulu di pendaftaran_t
                        $updateRujuakan             = PendaftaranT::model()->updateByPk($idpendaftaran, array('rujukan_id'=>null));
    //                    delete tabel rujukan sesuai dengan id_rujuan yang berada di pendaftaran_t
                        $deletePasienRujukan        = RujukanT::model()->deleteByPk($pendaftaran->rujukan_id); 
    //                    delete penagambilan sample berdasarkan pasienmasukpenunjang_id di pasienmasukpenunjang_t
                        $deletePengambilanSample    = PengambilansampleT::model()->deleteAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));                    
    //                    delete detailhasilpemeriksaanlab_t berdasarkan dengan hasilpemeriksaanlab_id
                        $deleteDetailPemerriksaanLab= DetailhasilpemeriksaanlabT::model()->deleteAllByAttributes(array('hasilpemeriksaanlab_id'=>$hasilPemeriksaanLab->hasilpemeriksaanlab_id));
    //                    delete hasilpemeriksaanlab_t berdasarkan pendaftaran_id
                        $deleteHasilPemeriksaanLab  = HasilpemeriksaanlabT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$idpendaftaran));
    //                    delete uabahcarabayar_t berdasarkan pendaftaran_id
                        $deleteUbahCarabayar        = UbahcarabayarR::model()->deleteAll('pendaftaran_id = '.$idpendaftaran);
    //                    delete tindakanpelayanan_t berdasarkan pendaftaran_id
                        $deleteTindakanPelayanan    = TindakanpelayananT::model()->deleteAll('pendaftaran_id = '.$idpendaftaran);
    //                    delete pasienmasuk penunjang berdasarkan pendaftaran_t
                        $deletePasienMasukPenunjang = PasienmasukpenunjangT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$idpendaftaran));   
    //                    delete pendaftaran berdasarkan id_pendaftaran
                        $deletePendaftaran          = PendaftaranT::model()->deleteByPk($idpendaftaran);
    //                    delete pasie_m berdasarkan pasien_id
                        $deletePasien               = PasienM::model()->deleteByPk($pasien->pasien_id);
    //                    
    //                    $delete = $deletePasienRujukan && $deletePengambilanSample && $deleteUbahCarabayar && $deleteHasilPemeriksaanLab && $deleteTindakanPelayanan && $deletePasien && $deletePendaftaran;

                        if($deletePasien && $pendaftaran){
                            $data['status'] = 'success';
                            $transaction->commit();
                        }else{
                            $data['status'] = 'gagal';
                            $transaction->rollback();
                            throw new Exception("Pasien tidak bisa dibatalkan");
                        }

                    }catch(Exception $ex){
                         $transaction->rollback();
                         $data['status'] = 'gagal';
                         $data['info'] = $ex;
                     }      
                    
                }else{
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
//                        echo "pasienkirimke unit lain->".$pasienKirimKeUnitLain->pasienmasukpenunjang_id;
//                        echo "----pasien masuk penunjang ->".$pasienKirimKeUnitLain->pasienmasukpenunjang_id;
                        $updatePasienKirimKeUnitLain = PasienkirimkeunitlainT::model()->updateByPk($pasienKirimKeUnitLain->pasienkirimkeunitlain_id, array('pasienmasukpenunjang_id'=>null));
                       
                        $deleteDetailPemerriksaanLab    = DetailhasilpemeriksaanlabT::model()->deleteAllByAttributes(array('hasilpemeriksaanlab_id'=>$hasilPemeriksaanLab->hasilpemeriksaanlab_id));
                            
                            foreach ($detailHasilPemeriksaanLab as $key => $deleteDetailHasil) {
                                $deleteTindakanPelayanan        = TindakanpelayananT::model()->deleteByPk($deleteDetailHasil->tindakanpelayanan_id);
                            }
                        
                        $deleteHasilPemeriksaanLab  = HasilpemeriksaanlabT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$idpendaftaran));
//                        $deletePasienMasukPenunjang = PasienmasukpenunjangT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$idpendaftaran)); 
                        $deletePasienMasukPenunjang = PasienmasukpenunjangT::model()->deleteByPk($pasienKirimKeUnitLain->pasienmasukpenunjang_id); 
                        if($deletePasienMasukPenunjang){
                            $data['status'] = 'success';
                                $transaction->commit();
                        }else{
                            $data['status'] = 'gagal';
                            $transaction->rollback();
                            throw new Exception("Pasien tidak bisa dibatalkan");
                        }

                    }catch(Exception $ex){
                         $transaction->rollback();
                         $data['status'] = 'gagal';
                         $data['info'] = $ex;
                     }              
                    
                }
                 
                echo json_encode($data);
                Yii::app()->end();
            }
        }
        
        public function actionGetRiwayatPasien($id){
            $this->layout='//layouts/frameDialog';
            $criteria = new CDbCriteria(array(
                    'condition' => 't.pasien_id = '.$id,
                //.'
                  //      and t.ruangan_id ='.Yii::app()->user->getState('ruangan_id'),
                    'order'=>'tgl_pendaftaran DESC',
                ));

            $pages = new CPagination(GZPendaftaranT::model()->count($criteria));
            $pages->pageSize = Params::JUMLAH_PERHALAMAN; //Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);
            
            $modKunjungan = GZPendaftaranT::model()->with('hasilpemeriksaanlab','anamnesa','pemeriksaanfisik','pasienmasukpenunjang','diagnosa')->
                    findAll($criteria);
            
           
            $this->render('/_periksaDataPasien/_riwayatPasien', array(
                    'pages'=>$pages,
                    'modKunjungan'=>$modKunjungan,
            ));
        }
        
        public function actionDetailKonsulGizi($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = GZPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);

            // var_dump($id);exit();

            $modTindakan = GZTindakanPelayananT::model()->with('daftartindakan')->findAllByAttributes(array('pendaftaran_id'=>$id));
            $format = new CustomFormat;
            $modTindakanSearch = new GZTindakanPelayananT('search');
            $modPasien = GZPasienM::model()->findByPK($modPendaftaran->pasien_id);
            $this->render('/_periksaDataPasien/_konsultasiGizi', 
                    array('modPendaftaran'=>$modPendaftaran, 
                        'modTindakan'=>$modTindakan,
                        'modTindakanSearch'=>$modTindakanSearch,
                        'modPasien'=>$modPasien));
        }
        
        public function actionDetailAnamnesaDiet($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = GZPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);

            // var_dump($id);exit();

            $modTindakan = GZAnamnesaDietT::model()->with('bahanmakanan','menudiet')->findAllByAttributes(array('pendaftaran_id'=>$id));
            $format = new CustomFormat;
            $modTindakanSearch = new GZAnamnesaDietT('search');
            $modPasien = GZPasienM::model()->findByPK($modPendaftaran->pasien_id);
            $this->render('/_periksaDataPasien/_anamnesaDiet', 
                    array('modPendaftaran'=>$modPendaftaran, 
                        'modTindakan'=>$modTindakan,
                        'modTindakanSearch'=>$modTindakanSearch,
                        'modPasien'=>$modPasien));
        }


         public function actionBatalPeriksa()
        {
            if(Yii::app()->request->isAjaxRequest)
            {
                $transaction = Yii::app()->db->beginTransaction();
                $pesan = 'success';
                $status = 'ok';

                try{
                    $idPenunjang = $_POST['idPenunjang'];
                    if($idPenunjang){
                        $pasienMasukPenunjang = PasienmasukpenunjangT::model()->findByPk($idPenunjang);
                        $modPendaftaran = PendaftaranT::model()->findByPk($pasienMasukPenunjang->pendaftaran_id);
                        if($modPendaftaran->pembayaranpelayanan_id){ // sudah lunas semua
                            $status = 'not';
                            $pesan = 'exist';
                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pasienMasukPenunjang->pendaftaran->pasien->nama_pasien." 
                                                </b> sudah melakukan pembayaran pemeriksaan </div>";
                        }else{
                            $criteria = new CdbCriteria;
                            $criteria->addCondition('pasienmasukpenunjang_id = '.$pasienMasukPenunjang->pasienmasukpenunjang_id);
                            $criteria->addCondition('tindakansudahbayar_id > 0');
                            $tindakan = TindakanpelayananT::model()->findAll($criteria);
                            if(count($tindakan) > 0){
                                $status = 'not';
                                $pesan = 'exist';
                                $keterangan = "<div class='flash-success'>Pasien <b> ".$pasienMasukPenunjang->pendaftaran->pasien->nama_pasien." 
                                                    </b> sudah melakukan pembayaran pemeriksaan </div>";
                            }else{
                                $model = new PasienbatalperiksaR();
                                $model->pendaftaran_id = null;
                                $model->pasien_id = $pasienMasukPenunjang->pasien_id;
                                $model->pasienmasukpenunjang_id = $pasienMasukPenunjang->pasienmasukpenunjang_id;
                                $model->pasienkirimkeunitlain_id = $pasienMasukPenunjang->pasienkirimkeunitlain_id;
                                $model->tglbatal = date('Y-m-d');
                                $model->keterangan_batal = "Batal Bedah Sentral";
                                $model->create_time = date('Y-m-d H:i:s');
                                $model->update_time = null;
                                $model->create_loginpemakai_id = Yii::app()->user->id;
                                $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
                                if($model->save()){
                                    $status = 'ok';
                                    $pesan = 'exist';
                                    $keterangan = "<div class='flash-success'>Pemeriksaan Berhasil dibatalkan ! </div>";
                                }
                            }
                        }
                    }

                    /*
                     * kondisi_commit
                     */
                    if($status == 'ok')
                    {
                        $transaction->commit();
                    }else{
                        $transaction->rollback();
                    }   
                }catch(Exception $ex){
                    print_r($ex);
                    $status = 'not';
                    $transaction->rollback();
                }

                $data['pesan'] = $pesan;
                $data['status'] = $status;
                $data['keterangan'] = $keterangan;

                echo json_encode($data);
                Yii::app()->end();
            }
        }
        
}