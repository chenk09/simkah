<?php

class PendaftaranKonsultasiGiziController extends SBaseController
{
	 public $successSave = false;
         public $successSavePJ = true; //variabel untuk validasi data opsional (penanggung jawab)
        
        /**
	 * @return array action filters
	 */ 
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','getOperasi'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{
		
            $this->pageTitle = Yii::app()->name." - Pendaftaran Konsultasi Gizi";
            $model = new GZPendaftaranT;
            $modPasienAdmisi = new PasienadmisiT; //hanya untuk mengambil kamarruangan_m
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->umur = "00 Thn 00 Bln 00 Hr";
            $model->ruangan_id = Params::RUANGAN_ID_KONSULTASI_GIZI;
            $model->kelaspelayanan_id = Params::KELASPELAYANAN_RJ;
            $modPasien = new GZPasienM;
            $modPasien->tanggal_lahir = date('d M Y');
            $modPenanggungJawab = new GZPenanggungJawabM;
            $modRujukan = new GZRujukanT;
            $modPasienPenunjang = new GZPasienMasukPenunjangV;
            
            if (isset($_POST['GZPendaftaranT'])){
                
                $model->attributes = $_POST['GZPendaftaranT'];
                $modPasien->attributes = $_POST['GZPasienM'];
                $modPenanggungJawab->attributes = $_POST['GZPenanggungJawabM'];
                $modRujukan->attributes = $_POST['GZRujukanT'];
                
                
//                if(isset($_POST['tindakanrm_id']))
//                {
//                    
//                    $attrTindakan = $_POST['tindakanrm_id'];
//                    $attrCeklis = $_POST['ceklis'];
//                }
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    
//                    if($_POST['caraAmbilPhoto']=='file')//Jika User Mengambil photo pegawai dengan cara upload file
//                              { 
//                                  $model->pegawai_aktif=true;
//                                  $model->photopegawai = CUploadedFile::getInstance($model, 'photopegawai');
//                                  $gambar=$model->photopegawai;
//
//                                  if(!empty($model->photopegawai))//Klo User Memasukan Logo
//                                  { 
//
//                                        $model->photopegawai =$random.$model->photopegawai;
//
//                                        Yii::import("ext.EPhpThumb.EPhpThumb");
//
//                                         $thumb=new EPhpThumb();
//                                         $thumb->init(); //this is needed
//
//                                         $fullImgName =$model->photopegawai;   
//                                         $fullImgSource = Params::pathPegawaiDirectory().$fullImgName;
//                                         $fullThumbSource = Params::pathPegawaiTumbsDirectory().'kecil_'.$fullImgName;
//
//                                         if($model->save())
//                                              {
//                                                   $gambar->saveAs($fullImgSource);
//                                                   $thumb->create($fullImgSource)
//                                                         ->resize(200,200)
//                                                         ->save($fullThumbSource);
//                                              }
//                                          else
//                                              {
//                                                   Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
//                                              }
//                                    }
//                              }   
//                             else 
//                              {
//                                 $model->photopegawai=$_POST['SAPegawaiM']['tempPhoto'];
//                                 if($model->validate())
//                                    {
//                                        $model->save();
//                                    }
//                                 else 
//                                    {
//                                         unlink(Params::pathPegawaiDirectory().$_POST['SAPegawaiM']['tempPhoto']);
//                                         unlink(Params::pathPegawaiTumbsDirectory().$_POST['SAPegawaiM']['tempPhoto']);
//                                    }
//                               }
                    
                    //==Penyimpanan dan Update Pasien===========================                    
                    if(!isset($_POST['isPasienLama'])){
                        $modPasien = $this->savePasien($_POST['GZPasienM']);
                    }
                    else{
                        $model->isPasienLama = true;
                        if (!empty($_POST['noRekamMedik'])){
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = GZPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            if(!empty($modPasien) && isset($_POST['isUpdatePasien'])) {
                                $modPasien = $this->updatePasien($modPasien, $_POST['GZPasienM']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }
                    
                    //==Akhir Penyimpanan dan Update Pasien=====================
                    
                    
                    //===penyimpanan Penanggung Jawab===========================                   
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['GZPenanggungJawabM']);
                    }
                    //===Akhir Penyimpanan Penanggung Jawab=====================                    
                    
                    if(isset ($_POST['pendaftaranAda'])) // kondisi dimana no pendaftaran sudah ada, jadi tidak usah simpan ke pendaftaran_t
                    {
                        $model = $this->loadPendaftaran($_POST['id_pendaftaran']);
                        
                        //==AwalSimpan Psien Masuk Penunjang========================
                        $modPasienPenunjang = $this->savePasienPenunjangJadwal($model,$_POST['JadwalKunjungan']);
                        //Akhir Pasien MAsuk Penunjang
                       
                    }
                    else
                    {
                       $modRujukan = $this->saveRujukan($_POST['GZRujukanT']); //Save Rujukan
                        
                       //==Awal Simpan Pendaftaran=============================
                       $model = $this->savePendaftaranMP($model,$modPasien,$modRujukan,$modPenanggungJawab);
                       //==Akhir Simpan Pendaftaran============================ 
                       
                       //==Awal Simpan Ubah Cara Bayar=============================
                       $this->saveUbahCaraBayar($model);
                       //==Akhir Simpan Ubah Cara Bayar
                       
                       //==AwalSimpan Psien Masuk Penunjang========================
                       $modPasienPenunjang = $this->savePasienPenunjang($model);
                       //Akhir Pasien MAsuk Penunjang
                       
                       //==AwalSimpan Hasil Pemeriksaan========================  
//                           if(!empty($_POST['tindakanrm_id']))
//                           {
//                              $modHasilPemeriksaan = $this->saveHasilPemeriksaan($model,$modPasienPenunjang,$attrTindakan,$attrCeklis,$_POST['GZTindakanPelayananT']);
//                           }
//                           else
//                           {
//                              $modHasilPemeriksaan->validate();
//                              $this->successSave = false;
//                           }
                       //Akhir Hasil Pemeriksaan
                    }
   
                    if (count($modPasien) != 1){ //Jika Pasien Tidak Ada
                        $modPasien = New GZPasienM;
                        $this->successSave = false;
                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
                        Yii::app()->user->setFlash('warning',"Data Pasien tidak ditemukan isi Nomor Rekam Medik dengan benar");
                    }
                   
                    if ($this->successSave && $this->successSavePJ = true){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    }
                    else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $model->isRujukan = true;
            $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            
            $this->render('index',array(
                'model'=>$model, 
                'modPasien'=>$modPasien, 
                'modPenanggungJawab'=>$modPenanggungJawab,
                'modRujukan'=>$modRujukan,
                'modPasienPenunjang'=>$modPasienPenunjang,
                'modPasienAdmisi'=>$modPasienAdmisi,
                'modTindakanKomponen'=>$modTindakanKomponen,
                'modTindakanPelayanan'=>$modTindakanPelayanan,
                'modJenisTindakan'=>$modJenisTindakan,
                'modTindakan'=>$modTindakan,
                
            ));
	}
        
        public function savePasien($attrPasien)
        {
            $modPasien = new GZPasienM;
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modPasien->no_rekam_medik = $modPasien->no_rekam_medik;
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = 'AKTIF';
            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');

            if($modPasien->validate()) {
                // form inputs are valid, do something here
                $modPasien->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            return $modPasien;
        }
        
        public function updatePasien($modPasien,$attrPasien)
        {
            $modPasienupdate = $modPasien;
            $modPasienupdate->attributes = $attrPasien;
            $modPasienupdate->kelompokumur_id = Generator::kelompokUmur($modPasienupdate->tanggal_lahir);
            
            if($modPasienupdate->validate()) {
                // form inputs are valid, do something here
                $modPasienupdate->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasienupdate->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasienupdate->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            
            return $modPasienupdate;
        }
        
        public function saveRujukan($attrRujukan)
        {
            $modRujukan = new GZRujukanT;
            $modRujukan->attributes = $attrRujukan;
            $modRujukan->tanggal_rujukan = ($attrRujukan['tanggal_rujukan'] == '') ? date('Y-m-d') : $attrRujukan['tanggal_rujukan'] ;
            if($modRujukan->validate()) {
                // form inputs are valid, do something here
                $modRujukan->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modRujukan->tanggal_rujukan = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modRujukan->tanggal_rujukan, 'yyyy-MM-dd'),'medium',null);
                
            }
            return $modRujukan;
        }
        
        public function savePenanggungJawab($attrPenanggungJawab)
        {
            $modPenanggungJawab = new GZPenanggungJawabM;
            $modPenanggungJawab->attributes = $attrPenanggungJawab;
            if($modPenanggungJawab->validate()) {
                // form inputs are valid, do something here
                $modPenanggungJawab->save();
                $this->successSavePJ = TRUE;
            } else {
                // mengembalikan format tanggal contoh 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                //if($modPenanggungJawab->tgllahir_pj != null)
                //$modPenanggungJawab->tgllahir_pj = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenanggungJawab->tgllahir_pj, 'yyyy-MM-dd'),'medium',null);
                $this->successSavePJ = FALSE;
            }

            return $modPenanggungJawab;
        }
        
         public function savePendaftaranMP($model, $modPasien, $modRujukan, $modPenanggungJawab){
            $modelNew = new GZPendaftaranT;
            $modelNew->attributes = $model->attributes;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            if (isset($modelNew->ruangan_id)){
                  $inisial_ruangan = 'RE';
            }
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modelNew->instalasi_id = Generator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
            $modelNew->no_pendaftaran = Generator::noPendaftaran(Params::singkatanNoPendaftaranRehabMedis());
            $modelNew->no_urutantri = Generator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = Generator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
            $modelNew->statuspasien = Generator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = Generator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->statusmasuk = Params::statusMasuk('rujukan');
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
           
            if ($modelNew->validate()){
                
                $modelNew->Save();
                $this->successSave = true;
            }else{
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'), 'medium', null);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
            
            return $modelNew;
        }
        
        public function savePasienPenunjang($attrPendaftaran){
           
                    $modPasienPenunjang = new GZPasienMasukPenunjangT;
                    $modPasienPenunjang->pasien_id = $attrPendaftaran->pasien_id;
                    $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                    $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
                    $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
                    $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
                    $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
                    switch ($modPasienPenunjang->ruangan_id) {
                        case Params::RUANGAN_ID_LAB:$inisial_ruangan = 'LK';break;
                        case Params::RUANGAN_ID_RAD:$inisial_ruangan = 'RO';break;
                        case Params::RUANGAN_ID_REHAB:$inisial_ruangan = 'RE';break;
                        case Params::RUANGAN_ID_IRM:$inisial_ruangan = 'RM';break;
                        default:break;
                    }
                    $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang($inisial_ruangan);
                    $modPasienPenunjang->tglmasukpenunjang = $attrPendaftaran->tgl_pendaftaran;
                    $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
                    $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
                    $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
                    $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;


                    if ($modPasienPenunjang->validate()){
                        $modPasienPenunjang->Save();
                        $this->successSave = true;
                    } else {
                        $this->successSave = false;
                        $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
                    }
                
            
            return $modPasienPenunjang;
        }
        
        public function savePasienPenunjangJadwal($attrPendaftaran,$attrJadwal){
            
            for ($i = 0; $i < count($attrJadwal['ceklis']); $i++) {
                    $patokan = $attrJadwal['ceklis'][$i];
                    $modPasienPenunjang = new GZPasienMasukPenunjangT;
                    $modPasienPenunjang->pasien_id = $attrPendaftaran->pasien_id;
                    $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                    $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
                    $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
                    $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
                    $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
                    switch ($modPasienPenunjang->ruangan_id) {
                        case Params::RUANGAN_ID_LAB:$inisial_ruangan = 'LK';break;
                        case Params::RUANGAN_ID_RAD:$inisial_ruangan = 'RO';break;
                        case Params::RUANGAN_ID_REHAB:$inisial_ruangan = 'RE';break;
                        case Params::RUANGAN_ID_IRM:$inisial_ruangan = 'RM';break;
                        default:break;
                    }
                    $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang($inisial_ruangan);
                    $modPasienPenunjang->tglmasukpenunjang = date('Y-m-d h:i:s');
                    $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
                    $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
                    $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
                    $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
                    
                    
                    if ($modPasienPenunjang->validate()){
                        $modPasienPenunjang->Save();
                        $this->successSave = true;
                        $this->updateJadwalKunjungan($attrJadwal,$attrPendaftaran,$modPasienPenunjang,$patokan);
                    } else {
                        $this->successSave = false;
                        $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
                    }
                
                
            }//ENDING FOR
            
            return $modPasienPenunjang;
        }
        
//        public function saveUbahCaraBayar($model) 
//        {
//            $modUbahCaraBayar = new RMUbahCaraBayarR;
//            $modUbahCaraBayar->pendaftaran_id = $model->pendaftaran_id;
//            $modUbahCaraBayar->carabayar_id = $model->carabayar_id;
//            $modUbahCaraBayar->penjamin_id = $model->penjamin_id;
//            $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d');
//            $modUbahCaraBayar->alasanperubahan = 'x';
//            if($modUbahCaraBayar->validate())
//            {
//                // form inputs are valid, do something here
//                
//                $modUbahCaraBayar->save();
//            }
//            
//        }
        
       
        
        public function saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$attrHasil,$attrTindakanPelayanan,$attrTindakan)
        {
            $validTindakanPelayanan = 'true';
            $arrSave = array();
            
            $daftar_tindakan = $attrTindakanPelayanan['daftartindakan_id'][$attrTindakan];
            
            $kelaspelayanan_id = $attrTindakanPelayanan['kelaspelayanan_id'][$attrTindakan];
            
                $tarifTotal = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                
                $tarifRS = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_RS));
                
                $tarifBHP = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_BHP));
                
                $tarifParamedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_PARAMEDIS));
                
                $tarifMedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_MEDIS));

                $modTindakanPelayanan = new GZTindakanPelayananT;
                $modTindakanPelayanan->penjamin_id = $attrPendaftaran->penjamin_id;
                $modTindakanPelayanan->pasien_id = $attrPenunjang->pasien_id;
                $modTindakanPelayanan->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayanan->kelaspelayanan_id = $attrTindakanPelayanan['kelaspelayanan_id'][$attrTindakan];
                $modTindakanPelayanan->hasilpemeriksaanrm_id = $attrHasil->hasilpemeriksaanrm_id ;
                $modTindakanPelayanan->tipepaket_id = 1;
                $modTindakanPelayanan->instalasi_id = Params::INSTALASI_ID_REHAB;
                $modTindakanPelayanan->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                $modTindakanPelayanan->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayanan->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                $modTindakanPelayanan->daftartindakan_id = $attrTindakanPelayanan['daftartindakan_id'][$attrTindakan];
                $modTindakanPelayanan->carabayar_id = $attrPendaftaran->carabayar_id;
                $modTindakanPelayanan->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayanan->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayanan->tarif_tindakan = $attrTindakanPelayanan['tarif_tindakan'][$attrTindakan];
                $modTindakanPelayanan->tarif_satuan = $modTindakanPelayanan->tarif_tindakan;
                $modTindakanPelayanan->tarif_rsakomodasi = (!empty($tarifRS)) ? $tarifRS->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_medis = (!empty($tarifMedis)) ? $tarifMedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_paramedis = (!empty($tarifParamedis)) ? $tarifParamedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_bhp = (!empty($tarifBHP)) ? $tarifBHP->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->satuantindakan = $attrTindakanPelayanan['satuantindakan'][$attrTindakan];
                $modTindakanPelayanan->qty_tindakan = $attrTindakanPelayanan['qty_tindakan'][$attrTindakan];
                $modTindakanPelayanan->cyto_tindakan = $attrTindakanPelayanan['cyto_tindakan'][$attrTindakan];
                if($modTindakanPelayanan->cyto_tindakan){
                    $modTindakanPelayanan->tarifcyto_tindakan = $modTindakanPelayanan->tarif_tindakan * ($attrTindakanPelayanan['persencyto_tind'][$attrTindakan]/100);
                } else {
                    $modTindakanPelayanan->tarifcyto_tindakan = 0;
                }
                $modTindakanPelayanan->discount_tindakan = 0;
                $modTindakanPelayanan->subsidiasuransi_tindakan=0;
                $modTindakanPelayanan->subsidipemerintah_tindakan=0;
                $modTindakanPelayanan->subsisidirumahsakit_tindakan=0;
                $modTindakanPelayanan->iurbiaya_tindakan=0;
                
                if ($modTindakanPelayanan->validate()){
                    $arrSave[$i] = $modTindakanPelayanan; // menyimpan objek RMRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                    $validTindakanPelayanan = 'true';

                }else
                {   
                    $validTindakanPelayanan = $validTindakanPelayanan.'false';
                }
//            } //ENDING FOR
            if($validTindakanPelayanan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $x => $simpan) {
                    $simpan->save();
                    $this->saveTindakanKomponenT($simpan);
                    $this->upadateHasilTindakan($simpan);
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }

            return $modTindakanPelayanan;
        }
        
        public function saveTindakanKomponenT($attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakanKomponen = 'true';
            $daftarTindakan_id = $attrTindakanPelayanan->daftartindakan_id;
            $kelaspelayanan_id = $attrTindakanPelayanan->kelaspelayanan_id;
            $arrTarifTindakan = "select * from tariftindakan_m where daftartindakan_id = ".$daftarTindakan_id." and kelaspelayanan_id = ".$kelaspelayanan_id." and komponentarif_id <> ".Params::KOMPONENTARIF_ID_TOTAL." ";
            $query = Yii::app()->db->createCommand($arrTarifTindakan)->queryAll();
            foreach ($query as $i => $tarifKomponen) {
                $modTarifKomponen = new GZTindakanKomponenT;
                $modTarifKomponen->tindakanpelayanan_id = $attrTindakanPelayanan->tindakanpelayanan_id;
                $modTarifKomponen->komponentarif_id = $tarifKomponen['komponentarif_id'];
                $modTarifKomponen->tarif_tindakankomp = $tarifKomponen['harga_tariftindakan'];
                if($attrTindakanPelayanan->cyto_tindakan){
                    $modTarifKomponen->tarifcyto_tindakankomp = $tarifKomponen['harga_tariftindakan'] * ($tarifKomponen['persencyto_tind']/100);
                } else {
                    $modTarifKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTarifKomponen->subsidiasuransikomp = 0;
                $modTarifKomponen->subsidipemerintahkomp = 0;
                $modTarifKomponen->subsidirumahsakitkomp = 0;
                $modTarifKomponen->iurbiayakomp = 0;
                $modTarifKomponen->tarif_kompsatuan = $modTarifKomponen->tarif_tindakankomp;
                if ($modTarifKomponen->validate()){
                    $arrSave[$i] = $modTarifKomponen; // menyimpan objek tarif komponen ke dalam sebuah array dan siap untuk disave
                    $validTindakanKomponen = 'true';

                }else
                {
                    $validTindakanKomponen = $validTindakanKomponen.'false';
                }
            } // ending foreach
            if($validTindakanKomponen == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $f => $simpan) {
                    $simpan->save();
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }
            return $modTarifKomponen;
        }
        
        public function saveTindakanPelayanJadwal($attrPendaftaran,$attrPenunjang,$attrHasil,$index,$attrPOST)
        {
            $validTindakanPelayanan = 'true';
            $arrSave = array();
                $daftar_tindakan = Tindakanrm::model()->findByPk($attrHasil->tindakanrm_id)->daftartindakan_id;
                
                $tarifTotal = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                
                $tarifRS = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_RS));
                
                $tarifBHP = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_BHP));
                
                $tarifParamedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_PARAMEDIS));
                
                $tarifMedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_MEDIS));
            
                $modTindakanPelayanan = new GZTindakanPelayananT;
                $modTindakanPelayanan->penjamin_id = $attrPendaftaran->penjamin_id;
                $modTindakanPelayanan->pasien_id = $attrPenunjang->pasien_id;
                $modTindakanPelayanan->kelaspelayanan_id = $tarifTotal->kelaspelayanan_id;
                $modTindakanPelayanan->hasilpemeriksaanrm_id = $attrHasil->hasilpemeriksaanrm_id;
                $modTindakanPelayanan->tipepaket_id = 1;
                $modTindakanPelayanan->instalasi_id = Params::INSTALASI_ID_REHAB;
                $modTindakanPelayanan->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                $modTindakanPelayanan->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayanan->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                $modTindakanPelayanan->daftartindakan_id = $daftar_tindakan;
                $modTindakanPelayanan->carabayar_id = $attrPendaftaran->carabayar_id;
                $modTindakanPelayanan->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayanan->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayanan->tarif_tindakan = $tarifTotal->harga_tariftindakan;
                $modTindakanPelayanan->tarif_satuan = $modTindakanPelayanan->tarif_tindakan;
                $modTindakanPelayanan->tarif_rsakomodasi = (!empty($tarifRS)) ? $tarifRS->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_medis = (!empty($tarifMedis)) ? $tarifMedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_paramedis = (!empty($tarifParamedis)) ? $tarifParamedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_bhp = (!empty($tarifBHP)) ? $tarifBHP->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->satuantindakan = 'KALI';
                $modTindakanPelayanan->qty_tindakan = 1;
                $modTindakanPelayanan->cyto_tindakan = 0;
                $modTindakanPelayanan->dokterpemeriksa1_id = $attrHasil->pegawai_id;
                
                if($modTindakanPelayanan->cyto_tindakan){
                    $modTindakanPelayanan->tarifcyto_tindakan = $modTindakanPelayanan->tarif_tindakan * ($attrTindakanPelayananTotal->persencyto_tind/100);
                } else {
                    $modTindakanPelayanan->tarifcyto_tindakan = 0;
                }
                
                $arrTindakan[$i]=array(
                                            'tindakanrm_id'=> $attrHasil->tindakanrm_id
                                        );
                
                $modTindakanPelayanan->discount_tindakan = 0;
                $modTindakanPelayanan->subsidiasuransi_tindakan=0;
                $modTindakanPelayanan->subsidipemerintah_tindakan=0;
                $modTindakanPelayanan->subsisidirumahsakit_tindakan=0;
                $modTindakanPelayanan->iurbiaya_tindakan=0;
                
                if ($modTindakanPelayanan->validate()){
                    $arrSave[$i] = $modTindakanPelayanan; // menyimpan objek RMRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                    $validTindakanPelayanan = 'true';

                }else
                {   
                    $validTindakanPelayanan = $validTindakanPelayanan.'false';
                }
            
            if($validTindakanPelayanan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrTindakan as $y => $hasilTindakan) {
                        $tindakanNya[$y] = $hasilTindakan['tindakanrm_id'];
                }
                
                foreach ($arrSave as $x => $simpan) {
                    $simpan->save();
                    $this->saveTindakanKomponenJadwal($simpan);
                    $this->upadateHasilTindakanAll($simpan,$attrHasil,$tindakanNya[$x]);
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }

            return $modTindakanPelayanan;
        }
        
        public function saveTindakanKomponenJadwal($attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakanKomponen = 'true';
            $daftarTindakan_id = $attrTindakanPelayanan->daftartindakan_id;
            $kelaspelayanan_id = $attrTindakanPelayanan->kelaspelayanan_id;
            $arrTarifTindakan = "select * from tariftindakan_m where daftartindakan_id = ".$daftarTindakan_id." and kelaspelayanan_id = ".$kelaspelayanan_id." and komponentarif_id <> ".Params::KOMPONENTARIF_ID_TOTAL." ";
            
            $query = Yii::app()->db->createCommand($arrTarifTindakan)->queryAll();
            foreach ($query as $i => $tarifKomponen) {
                $modTarifKomponen = new RMTindakanKomponenT;
                $modTarifKomponen->tindakanpelayanan_id = $attrTindakanPelayanan->tindakanpelayanan_id;
                $modTarifKomponen->komponentarif_id = $tarifKomponen['komponentarif_id'];
                $modTarifKomponen->tarif_tindakankomp = $tarifKomponen['harga_tariftindakan'];
                if($attrTindakanPelayanan->cyto_tindakan){
                    $modTarifKomponen->tarifcyto_tindakankomp = $tarifKomponen['harga_tariftindakan'] * ($tarifKomponen['persencyto_tind']/100);
                } else {
                    $modTarifKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTarifKomponen->subsidiasuransikomp = 0;
                $modTarifKomponen->subsidipemerintahkomp = 0;
                $modTarifKomponen->subsidirumahsakitkomp = 0;
                $modTarifKomponen->iurbiayakomp = 0;
                $modTarifKomponen->tarif_kompsatuan = $modTarifKomponen->tarif_tindakankomp;
                if ($modTarifKomponen->validate()){
                    
                    $arrSave[$i] = $modTarifKomponen; // menyimpan objek tarif komponen ke dalam sebuah array dan siap untuk disave
                    $validTindakanKomponen = 'true';

                }else
                {
                    $validTindakanKomponen = $validTindakanKomponen.'false';
                }
            } // ending foreach
            if($validTindakanKomponen == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $f => $simpan) {
                    $simpan->save();
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }
            return $modTarifKomponen;
        }
        
        public function loadPendaftaran($id)
        {       $model= PendaftaranT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dari pendaftaran_t dengan method findByPk yang 
         * @param type $id
         * @return type 
         */
        public function loadJadwalKunjungan($id)
        {       $model= JadwalkunjunganrmT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dengan method findByAttributes yang nanti digunakan untuk mendeskripsikan hasilpemeriksanrm_t
         * @param type $id
         * @return type 
         */
        protected function loadHasilPemeriksaan($id)
        {
                $model= HasilpemeriksaanrmT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        
        
}