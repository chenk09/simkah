<?php

class TerimabahanmakanController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'Informasi', 'detailPenerimaan'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex($idPengajuan = null,$id=null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                
		$model=new GZTerimabahanmakan;
                $model->nopenerimaanbahan = Generator::noPenerimaanBahan();
                $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $model->tglterimabahan = date('d M Y H:i:s');
                $model->totaldiscount = 0;
                $model->biayapajak = 0;
                $model->biayapengiriman = 0;
                $model->biayatransportasi = 0;
                if (isset($idPengajuan)){
                    $modPengajuan = PengajuanbahanmknT::model()->find('pengajuanbahanmkn_id = '.$idPengajuan.' and terimabahanmakan_id is null');
                    if (count($modPengajuan) == 1){
                        $model->supplier_id = $modPengajuan->supplier_id;
                        $model->sumberdanabhn = $modPengajuan->sumberdanabhn;
                        $model->totalharganetto = $modPengajuan->totalharganetto;
                        $model->pengajuanbahanmkn_id = $modPengajuan->pengajuanbahanmkn_id;
                        $modDetailPengajuan = PengajuanbahandetailT::model()->with('golbahanmakanan')->findAllByAttributes(array('pengajuanbahanmkn_id'=>$idPengajuan));
                    }
//                  $model->totaldiscount = $modPengajuan->totalharganetto;
                }
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZTerimabahanmakan']))
		{
                    $model->attributes=$_POST['GZTerimabahanmakan'];
                    $transaction = Yii::app()->db->beginTransaction();
                    
                    try{    
                        $success = true;
                        if($model->save()) {
                            if (isset($modPengajuan->pengajuanbahanmkn_id)){
                                PengajuanbahanmknT::model()->updateByPk($model->pengajuanbahanmkn_id, array('terimabahanmakan_id'=>$model->terimabahanmakan_id));
                            }
                            $jumlah = count($_POST['TerimabahandetailT']['bahanmakanan_id']);
                            for ($i = 0; $i < $jumlah; $i++) {
                                if ($_POST['checkList'][$i] == 1) {
                                    $modDetail = new GZTerimabahandetailT();
                                    $modDetail->terimabahanmakan_id = $model->terimabahanmakan_id;
                                    $modDetail->golbahanmakanan_id = $_POST['TerimabahandetailT']['golbahanmakanan_id'][$i];
                                    $modDetail->bahanmakanan_id = $_POST['TerimabahandetailT']['bahanmakanan_id'][$i];
                                    $modDetail->nourutbahan = $_POST['noUrut'][$i];
                                    $modDetail->ukuran_bahanterima = $_POST['TerimabahandetailT']['ukuran_bahanterima'][$i];
                                    $modDetail->merk_bahanterima = $_POST['TerimabahandetailT']['merk_bahanterima'][$i];
                                    $modDetail->jmlkemasan = $_POST['TerimabahandetailT']['jmlkemasan'][$i];
                                    if (isset($modPengajuan->pengajuanbahanmkn_id)){
                                        $modDetail->pengajuanbahandetail_id = $_POST['TerimabahandetailT']['pengajuanbahandetail_id'][$i];
                                    }
                                    if (!is_numeric($modDetail->jmlkemasan)){
                                        $modDetail->jmlkemasan = 0;
                                    }
                                    if (!is_numeric($_POST['TerimabahandetailT']['qty_terima'][$i])) {
                                        $_POST['TerimabahandetailT']['qty_terima'][$i] = 0;
                                    }
                                    $modDetail->qty_terima = $_POST['TerimabahandetailT']['qty_terima'][$i];
                                    $modDetail->satuanbahan = $_POST['TerimabahandetailT']['satuanbahan'][$i];
                                    $modDetail->hargajualbhn = $_POST['TerimabahandetailT']['hargajualbhn'][$i];
                                    $modDetail->harganettobhn = $_POST['TerimabahandetailT']['harganettobhn'][$i];
//                                    echo '<pre>';
//                                    echo print_r($modDetail->attributes);
//                                    echo '<pre>';
                                    if ($modDetail->save()){
                                        $this->tambahStokBahanMakanan($modDetail);
                                        if (isset($modPengajuan->pengajuanbahanmkn_id)){
                                            PengajuanbahandetailT::model()->updateByPk($modDetail->pengajuanbahandetail_id, array('terimabahandetail_id'=>$modDetail->terimabahandetail_id));
                                        }
                                    }else{
                                        $success = false;
                                    }
                                }
                            }
            //			
                        }
                        else{
                            $success = false;
                        }
                        if ($success == TRUE){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                             $this->redirect(array('index', 'id' =>$model->terimabahanmakan_id));
                            $this->refresh();
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    }
                    catch (Exception $exc) {
                        $transaction->rollback();
                        $successSave = false;
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
		}

		$this->render('index',array(
			'model'=>$model, 'modDetailPengajuan'=>$modDetailPengajuan, 'modPengajuan'=>$modPengajuan,
		));
	}
        
        protected function tambahStokBahanMakanan($detail){
            $modStokBahan = new GZStokbahanmakananT();
            $modStokBahan->terimabahandetail_id = $detail->terimabahandetail_id;
            $modStokBahan->bahanmakanan_id = $detail->bahanmakanan_id;
            $modStokBahan->tgltransaksi = date('Y-m-d');
            $modStokBahan->qty_masuk = $detail->qty_terima;
            $modStokBahan->qty_current = $detail->qty_terima;
            $modStokBahan->qty_keluar = 0;
            if ($modStokBahan->save()){

            }
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZTerimabahanmakan']))
		{
			$model->attributes=$_POST['GZTerimabahanmakan'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->terimabahanmakan_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
//	public function actionIndex()
//	{
//		$dataProvider=new CActiveDataProvider('GZTerimabahanmakan');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
//	}

	/**
	 * Manages all models.
	 */
	public function actionInformasi()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZTerimabahanmakan('search');
                $model->tglAwal = date('d M Y 00:00:00');
                $model->tglAkhir = date('d M Y H:i:s');
//		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GZTerimabahanmakan'])){
                    $model->attributes=$_GET['GZTerimabahanmakan'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                }

		$this->render('informasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GZTerimabahanmakan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gzterimabahanmakan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new GZTerimabahanmakan;
            $model->attributes=$_REQUEST['GZTerimabahanmakan'];
            $judulLaporan='Data GZTerimabahanmakan';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function actionDetailPenerimaan($id){
            
            $this->layout = '//layouts/frameDialog';
            
            $modTerima = TerimabahanmakanT::model()->findByPk($id);
            $modDetailTerima = TerimabahandetailT::model()->with('bahanmakanan', 'golbahanmakanan')->findAllByAttributes(array('terimabahanmakan_id'=>$modTerima->terimabahanmakan_id), array('order'=>'nourutbahan ASC'));
            $this->render('detailInformasi', array(
                'modTerima'=>$modTerima,
                'modDetailTerima'=>$modDetailTerima,
            ));
        }
}
