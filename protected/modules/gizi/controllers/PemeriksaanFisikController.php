<?php
    class PemeriksaanFisikController extends SBaseController
    {
            protected $pathView = 'gizi.views.pemeriksaanFisik.';
            public $layout='//layouts/column1';
            public $defaultAction = 'index';

            
            public function actionIndex($idPendaftaran)
            {   
    //            $result = $this->xmlParser();

                $format = new CustomFormat();
                $modPendaftaran = GZPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
                $modPasien = GZPasienM::model()->findByPk($modPendaftaran->pasien_id);
                $cekPemeriksaanFisik=GZPemeriksaanFisikT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));

                    if(COUNT($cekPemeriksaanFisik)>0)
                        {  //Jika Pasien Sudah Melakukan Pemeriksaan Fisik  Sebelumnya
                            $modPemeriksaanFisik=$cekPemeriksaanFisik;
                        }
                    else
                        {  //Jika Pasien Belum Pernah melakukan Pemeriksaan Fisik
                            $modPemeriksaanFisik=new GZPemeriksaanFisikT;
                            $modPemeriksaanFisik->pegawai_id=$modPendaftaran->pegawai_id;
                            $modPemeriksaanFisik->pendaftaran_id=$modPendaftaran->pendaftaran_id;
                            $modPemeriksaanFisik->pasien_id=$modPasien->pasien_id;
                            $modPemeriksaanFisik->tglperiksafisik=date('Y-m-d H:i:s');

                            $modPemeriksaanFisik->Lila = 0;
                            $modPemeriksaanFisik->LingkarPinggang = 0;
                            $modPemeriksaanFisik->LingkarPinggul = 0;
                            $modPemeriksaanFisik->TebalLemak = 0;
                            $modPemeriksaanFisik->TinggiLutut = 0;
                        }
    //            $modPemeriksaanFisik->td_diastolic = $result[2];
    //            $modPemeriksaanFisik->td_systolic = $result[1];
    //            $modPemeriksaanFisik->detaknadi = $result[3];
    //            
    //            $modPemeriksaanFisik->tekanandarah = $this->panjangText($modPemeriksaanFisik->td_diastolic, $modPemeriksaanFisik->td_systolic);;
    //            echo $modPemeriksaanFisik->tekanandarah;exit();
                        if(isset($_POST['GZPemeriksaanFisikT']))
                        {
                            $transaction = Yii::app()->db->beginTransaction();
                            try {
                                    $modPemeriksaanFisik->attributes=$_POST['GZPemeriksaanFisikT'];  
                                    $modPemeriksaanFisik->keadaanumum = (count($_POST['GZPemeriksaanFisikT']['keadaanumum'])>0) ? implode(', ', $_POST['GZPemeriksaanFisikT']['keadaanumum']) : ''; 
                                    $modPemeriksaanFisik->tglperiksafisik=$format->formatDateTimeMediumForDB($_POST['GZPemeriksaanFisikT']['tglperiksafisik']);

                                    if($modPemeriksaanFisik->validate()){
                                       if($modPemeriksaanFisik->save()){
                                          $updateStatusPeriksa=PendaftaranT::model()->updateByPk($idPendaftaran,array('statusperiksa'=>Params::statusPeriksa(2)));
                                          $transaction->commit();
                                       }else{
                                           echo "gagal Simpan";exit;
                                       } 

                                    }  
                                    Yii::app()->user->setFlash('success',"Data Pemeriksaan Fisik berhasil disimpan");
                                    $this->redirect($_POST['url']); 
                                 }
                            catch (Exception $exc) 
                                {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error',"Data Pemeriksaan Fisik gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                                }
                        } 
                      $modPemeriksaanFisik->tglperiksafisik = Yii::app()->dateFormatter->formatDateTime(
                                            CDateTimeParser::parse($modPemeriksaanFisik->tglperiksafisik, 'yyyy-MM-dd hh:mm:ss'));       

                  $this->render($this->pathView.'index',
                        array(
                            'modPasien'=>$modPasien,
                            'modPemeriksaanFisik'=>$modPemeriksaanFisik,
                            'modPendaftaran'=>$modPendaftaran,
                        ));

            }

    }
?>
