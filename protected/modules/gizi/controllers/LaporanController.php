<?php

class LaporanController extends SBaseController {

    public $tglAwal = "d M Y 00:00:00";
    public $tglAkhir = "d M Y 23:59:59";
    
/*
 * gizi->Laporan->LaporanKonsultasiGizi
 * by Miranitha Fasha
 */   
    public function actionLaporanKonsulGizi() {
        $model = new GZLaporankonsultasigiziV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GZLaporankonsultasigiziV'])) {
            $model->attributes = $_GET['GZLaporankonsultasigiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigiziV']['tglAkhir']);
        }
        $this->render('konsulGizi/admin', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanKonsulGizi() {
        $model = new GZLaporankonsultasigiziV('search');
        $judulLaporan = 'LAPORAN KONSULTASI GIZI <BR/> INSTALASI GIZI BULAN';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Konsultasi Gizi';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['GZLaporankonsultasigiziV'])) {
            $model->attributes = $_REQUEST['GZLaporankonsultasigiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporankonsultasigiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporankonsultasigiziV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target ='konsulGizi/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanKonsulGizi() {
        $this->layout = '//layouts/frameDialog';
        $model = new GZLaporankonsultasigiziV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Konsultasi Gizi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['GZLaporankonsultasigiziV'])) {
            $model->attributes = $_GET['GZLaporankonsultasigiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigiziV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
/* end
 * gizi->Laporan->LaporanKonsultasiGizi
 * by Miranitha Fasha
 */ 

 /*
 * gizi->Laporan->LaporanKonsultasiGiziBerdasarkanRuangan
 */   
    public function actionLaporanKonsulGiziRekap() {
        $model = new GZLaporankonsultasigizirekapV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GZLaporankonsultasigizirekapV'])) {
            $model->attributes = $_GET['GZLaporankonsultasigizirekapV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigizirekapV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigizirekapV']['tglAkhir']);
        }
        $models = $model->findAll($model->searchTable());
        if (Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('gizi.views.laporan.konsulGiziRekap/_table',
                        array(
                            'model'=>$model,
                            'models'=>$models,
                             ), true
                    );
        }else{
        $this->render('konsulGiziRekap/admin', array(
            'model' => $model,
            'models' => $models,
            'filter'=>$filter
        ));
        }
    }

    // public function actionPrintLaporanKonsulGiziRekap2() {
    //     $model = new GZLaporankonsultasigizirekapV('search');
    //     $judulLaporan = 'LAPORAN KONSULTASI GIZI <BR/> INSTALASI GIZI BULAN';

    //     //Data Grafik
    //     $data['title'] = 'Laporan Konsultasi Gizi';
    //     $data['type'] = $_REQUEST['type'];

    //     if (isset($_REQUEST['GZLaporankonsultasigizirekapV'])) {
    //         $model->attributes = $_REQUEST['GZLaporankonsultasigizirekapV'];
    //         $format = new CustomFormat();
    //         $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporankonsultasigizirekapV']['tglAwal']);
    //         $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporankonsultasigizirekapV']['tglAkhir']);
    //     }

    //     $caraPrint = $_REQUEST['caraPrint'];
    //     $target ='konsulGiziRekap/_print';

    //     $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    // }

   public function actionPrintLaporanKonsulGiziRekap(){
        $this->layout='//layouts/printWindows';
        $model = new GZLaporankonsultasigizirekapV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GZLaporankonsultasigizirekapV'])) {
            $model->attributes = $_GET['GZLaporankonsultasigizirekapV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigizirekapV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigizirekapV']['tglAkhir']);
        }
        $models = $model->findAll($model->searchTable());
        $data = array();
        $data['judulLaporan'] = 'Laporan Pasien Konsultasi Gizi Berdasarkan Ruangan';
        $data['periode'] = 'Periode : ' . date("d-m-Y", strtotime($model->tglAwal)) . ' s/d ' . date("d-m-Y", strtotime($model->tglAkhir));
        if($_REQUEST['caraPrint'] == 'PDF'){
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);  
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            
            $mpdf->WriteHTML(
                    $this->render('gizi.views.laporan.konsulGiziRekap/_print',
                        array(
                            'model'=>$model,
                            'models'=>$models,
                            'data'=>$data,
                            'caraPrint'=>$_REQUEST['caraPrint']
                        ),true)
                );  
            $mpdf->Output();
        }else{
            $this->render('gizi.views.laporan.konsulGiziRekap/_print', array(
                'model' => $model,
                'models'=>$models,
                'caraPrint'=>$_REQUEST['caraPrint'],
                'data'=>$data
            )); 

        }
    }    

    public function actionFrameGrafikLaporanKonsulGiziRekap() {
        $this->layout = '//layouts/frameDialog';
        $model = new GZLaporankonsultasigizirekapV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Konsultasi Gizi Berdasarkan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['GZLaporankonsultasigizirekapV'])) {
            $model->attributes = $_GET['GZLaporankonsultasigizirekapV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigizirekapV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporankonsultasigizirekapV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
/* end
 * gizi->Laporan->LaporanKonsultasiGiziBerdasarkanRuangan
 */         
    
/*
 * gizi->Laporan->LaporanJasaKonsultasiGizi
 * by Miranitha Fasha
 */   
    public function actionLaporanJasaKonsulGizi() {
        $model = new GZLaporanjasakomponengiziV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GZLaporanjasakomponengiziV'])) {
            $model->attributes = $_GET['GZLaporanjasakomponengiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanjasakomponengiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanjasakomponengiziV']['tglAkhir']);
        }

        $this->render('jasaKonsulGizi/admin', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanJasaKonsulGizi() {
        $model = new GZLaporanjasakomponengiziV('search');
        $judulLaporan = 'Laporan Jasa Konsultasi Gizi';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Konsultasi Gizi';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['GZLaporanjasakomponengiziV'])) {
            $model->attributes = $_REQUEST['GZLaporanjasakomponengiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanjasakomponengiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanjasakomponengiziV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target ='jasaKonsulGizi/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJasaKonsulGizi() {
        $this->layout = '//layouts/frameDialog';
        $model = new GZLaporanjasakomponengiziV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Konsultasi Gizi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['GZLaporanjasakomponengiziV'])) {
            $model->attributes = $_GET['GZLaporanjasakomponengiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanjasakomponengiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanjasakomponengiziV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
/* end
 * gizi->Laporan->LaporanJasaKonsultasiGizi
 * by Miranitha Fasha
 */
    
/*
 * gizi->Laporan->LaporanMakananHarian
 * by Miranitha Fasha
 */   
    public function actionLaporanMakananHarian() {
        $model = new GZLaporanmakanangiziV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GZLaporanmakanangiziV'])) {
            $model->attributes = $_GET['GZLaporanmakanangiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanmakanangiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanmakanangiziV']['tglAkhir']);
        }

        $this->render('makananHarian/admin', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanMakananHarian() {
        $model = new GZLaporanmakanangiziV('search');
        $judulLaporan = 'Laporan Makanan Harian';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Makanan Harian';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['GZLaporanmakanangiziV'])) {
            $model->attributes = $_REQUEST['GZLaporanmakanangiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanmakanangiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanmakanangiziV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target ='makananHarian/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanMakananHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new GZLaporanmakanangiziV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Makanan Harian';
        $data['type'] = $_GET['type'];
        if (isset($_GET['GZLaporanmakanangiziV'])) {
            $model->attributes = $_GET['GZLaporanmakanangiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanmakanangiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanmakanangiziV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
/* end
 * gizi->Laporan->LaporanMakananHarian
 * by Miranitha Fasha
 */    
    
    public function actionLaporanJumlahPasienHarian()
    {
        $model = new GZLaporanjmlpasienhariangiziV('searchLaporan');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GZLaporanjmlpasienhariangiziV'])) {
            $model->attributes = $_GET['GZLaporanjmlpasienhariangiziV'];
            $model->pilihanTab = $_GET['GZLaporanjmlpasienhariangiziV']['pilihan_tab'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlpasienhariangiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlpasienhariangiziV']['tglAkhir']);
        }
        $models = $model->findAll($model->searchLaporan());
        $modRekaps = $model->findAll($model->searchRekap());
        if (Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('gizi.views.laporan.jumlahPasienHarian/_tables',
                        array(
                            'model'=>$model,
                            'models'=>$models,
                            'modRekaps'=>$modRekaps,
                            'pilihanTab'=>$_GET['GZLaporanjmlpasienhariangiziV']['pilihan_tab'],
                        ), true
                    );
        }else{
            $this->render('jumlahPasienHarian/adminJmlPasienHarian', array(
                'model' => $model,
                'models' => $models,
                'modRekaps' => $modRekaps,
            ));    
        }

    }
    
    public function actionPrintLaporanJumlahPasienHarian(){
        $this->layout='//layouts/printWindows';
        $model = new GZLaporanjmlpasienhariangiziV('searchLaporan');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GZLaporanjmlpasienhariangiziV'])) {
            $model->attributes = $_GET['GZLaporanjmlpasienhariangiziV'];
            $model->pilihanTab = $_GET['GZLaporanjmlpasienhariangiziV']['pilihan_tab'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlpasienhariangiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlpasienhariangiziV']['tglAkhir']);
        }
        $models = $model->findAll($model->searchLaporan());
        $modRekaps = $model->findAll($model->searchRekap());
        $data = array();
        if($_GET['GZLaporanjmlpasienhariangiziV']['pilihan_tab'] == 'rekap'){
            $data['judulLaporan'] = 'Laporan Rekap Jumlah';
        }else{
            $data['judulLaporan'] = 'Laporan Jumlah Harian';
        }
        $data['periode'] = 'Periode : ' . date("d-m-Y", strtotime($model->tglAwal)) . ' s/d ' . date("d-m-Y", strtotime($model->tglAkhir));
        if($_REQUEST['caraPrint'] == 'PDF'){
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);  
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            
            $mpdf->WriteHTML(
                    $this->render('gizi.views.laporan.jumlahPasienHarian/print',
                        array(
                            'model'=>$model,
                            'models'=>$models,
                            'modRekaps'=>$modRekaps,
                            'pilihanTab'=>$_GET['GZLaporanjmlpasienhariangiziV']['pilihan_tab'],
                            'caraPrint'=>$_REQUEST['caraPrint'],
                            'data'=>$data
                        ),true)
                );  
            $mpdf->Output();
        }else{
            $this->render('gizi.views.laporan.jumlahPasienHarian/print', array(
                'model' => $model,
                'models'=>$models,
                'modRekaps'=>$modRekaps,
                'pilihanTab'=>$_GET['GZLaporanjmlpasienhariangiziV']['pilihan_tab'],
                'caraPrint'=>$_REQUEST['caraPrint'],
                'data'=>$data
            )); 

        }
    }

/*
 * gizi->Laporan->LaporanExtraFooding
 * by Miranitha Fasha
 */   
    public function actionLaporanExtraFooding() {
        $model = new GZLaporanextrafoodinggiziV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GZLaporanextrafoodinggiziV'])) {
            $model->attributes = $_GET['GZLaporanextrafoodinggiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanextrafoodinggiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanextrafoodinggiziV']['tglAkhir']);
        }

        $this->render('extraFooding/admin', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanExtraFooding() {
        $model = new GZLaporanextrafoodinggiziV('search');
        $judulLaporan = 'Laporan Extra Fooding';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Extra Fooding';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['GZLaporanextrafoodinggiziV'])) {
            $model->attributes = $_REQUEST['GZLaporanextrafoodinggiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanextrafoodinggiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanextrafoodinggiziV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target ='extraFooding/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanExtraFooding() {
        $this->layout = '//layouts/frameDialog';
        $model = new GZLaporanextrafoodinggiziV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Extra Fooding';
        $data['type'] = $_GET['type'];
        if (isset($_GET['GZLaporanextrafoodinggiziV'])) {
            $model->attributes = $_GET['GZLaporanextrafoodinggiziV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanextrafoodinggiziV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanextrafoodinggiziV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
   /*
 * gizi->Laporan->LaporanJumlahPorsiKelas
 * by Miranitha Fasha
 */   
    public function actionLaporanJumlahPorsiKelas() {
        $model = new GZLaporanjmlporsikelasruanganV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GZLaporanjmlporsikelasruanganV'])) {
            $model->attributes = $_GET['GZLaporanjmlporsikelasruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlporsikelasruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlporsikelasruanganV']['tglAkhir']);
        }

        $this->render('jmlPorsiKelas/admin', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanJumlahPorsiKelas() {
        $model = new GZLaporanjmlporsikelasruanganV('search');
        $judulLaporan = 'Laporan Jumlah Porsi per Kelas';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jumlah Porsi per Kelas';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['GZLaporanjmlporsikelasruanganV'])) {
            $model->attributes = $_REQUEST['GZLaporanjmlporsikelasruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanjmlporsikelasruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanjmlporsikelasruanganV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target ='jmlPorsiKelas/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJumlahPorsiKelas() {
        $this->layout = '//layouts/frameDialog';
        $model = new GZLaporanjmlporsikelasruanganV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jumlah Porsi per Kelas';
        $data['type'] = $_GET['type'];
        if (isset($_GET['GZLaporanjmlporsikelasruanganV'])) {
            $model->attributes = $_GET['GZLaporanjmlporsikelasruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlporsikelasruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlporsikelasruanganV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
/* end
 * gizi->Laporan->LaporanJumlahPorsiKelas
 * by Miranitha Fasha
 */


public function actionLaporanJumlahPorsiGizi() {
        $model = new GZLaporanJumlahPorsiV  ('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        
        if (isset($_GET['GZLaporanJumlahPorsiV'])) {
            $model->attributes = $_GET['GZLaporanJumlahPorsiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('gizi.views.laporan.jumlahPorsi/_tableJumlahPorsi',
                        array(
                            'model'=>$model,
                        ), true
                    );
        }else{
             $this->render('jumlahPorsi/adminJumlahPorsi', array(
            'model' => $model, 'filter'=>$filter
        ));
        }

       
    }

    public function actionPrintLaporanJumlahPorsiGizi() {
        $model = new GZLaporanJumlahPorsiV('');
        $judulLaporan = 'Laporan Jumlah Porsi Berdasarkan Ruangan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jumlah Porsi Berdasarkan Ruangan';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['GZLaporanJumlahPorsiV'])) {
            $model->attributes = $_REQUEST['GZLaporanJumlahPorsiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanJumlahPorsiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZLaporanJumlahPorsiV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target ='jumlahPorsi/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJumlahPorsiGizi() {
        $this->layout = '//layouts/frameDialog';
        $model = new GZLaporanJumlahPorsiV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jumlah Porsi Berdasarkan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['GZLaporanJumlahPorsiV'])) {
            $model->attributes = $_GET['GZLaporanJumlahPorsiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
             $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
        foreach ($tgl as $row){
            if (!empty($row)){
                $result[] = $row;
            }
        }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }
}