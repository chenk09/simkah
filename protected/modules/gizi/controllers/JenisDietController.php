<?php
class JenisDietController extends SBaseController
{
    protected $pathView = 'gizi.views.jenisDiet.';
    public $layout='//layouts/column1';
    public $defaultAction = 'index';
	
    public function actionIndex($idPendaftaran){   
//            $result = $this->xmlParser();
            
   	    $format = new CustomFormat();
            $modPendaftaran = GZPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = GZPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modJenisDiet = new GZJenisdietM;
            $dataPendaftaran = GZPendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$modPasien->pasien_id), array('order'=>'tgl_pendaftaran DESC'));
            
            $i = 1;
                if (count($dataPendaftaran) > 1){
                    foreach ($dataPendaftaran as $row){
                        if ($i == 2){
                            $lastPendaftaran = $row->pendaftaran_id;
                        }
                        $i++;
                    }
                }else{
                    $lastPendaftaran = $idPendaftaran;
                }

            $cekDietPasien=GZDietPasienT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));

            if(COUNT($cekDietPasien)>0) {  //Jika Pasien Sudah Melakukan Diet Pasien Sebelumnya
                $modDietPasien=$cekDietPasien;
            } else {  
                ////Jika Pasien Belum Pernah melakukan Diet Pasein
                $modDietPasien=new GZDietPasienT;
                
            }
            
            if(isset($_POST['GZDietPasienT']))
            {
                $modDietPasien->attributes=$_POST['GZDietPasienT'];
                $modDetails = $this->validasiTabular($modPendaftaran, $_POST['GZDietPasienT']);
//                                    echo '<pre>';
//                                    echo print_r($_POST['GZDietPasienT']);
//                                    exit();
                $transaction = Yii::app()->db->beginTransaction();
                try {
                        $jumlah = 0;

                            $modDetails = $this->validasiTabular($modPendaftaran, $_POST['GZDietPasienT']);
                            foreach ($modDetails as $i=>$row)
                            {
                                if ($row->save()) {
                                    $jumlah++;
                                }
                            }
                        if ($jumlah == count($modDetails)) {
                            $transaction->commit();
                            Yii::app()->user->setFlash('success','<strong>Sukses</strong> Data berhasil disimpan');
                            $this->redirect(array('index', 'idPendaftaran' =>$idPendaftaran));
                        } else {
                            Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan '.'<pre>'.print_r($row->getErrors(),1).'</pre>');

//                            Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan');
                            $transaction->rollback();
                        }
                }
                catch (Exception $ex) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan '.$ex->getMessage());
                }
            }
            
                
            $modDietPasein->tgljenisdiet = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modDietPasien->tgljenisdiet, 'yyyy-MM-dd hh:mm:ss'));         
		
              $this->render($this->pathView.'index',
                                array(
                                    'modPasien'=>$modPasien,
                                    'modDietPasien'=>$modDietPasien,
                                    'modPendaftaran'=>$modPendaftaran,
                                    'modJenisDiet'=>$modJenisDiet,
                                    'modDetails'=>$modDetails,
             ));

    }
    
    protected function validasiTabular($modPendaftaran,$data) {
        foreach ($data as $i => $row) {
            
                $format = new CustomFormat();
                $modDetails[$i] = new GZDietPasienT();
                $modDetails[$i]->attributes = $row;                
                $modDetails[$i]->pasien_id = $modPendaftaran->pasien_id;
                $modDetails[$i]->pasienadmisi_id = $modPendaftaran->pasienadmisi_id;
                $modDetails[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                
                $modDetails[$i]->tipediet_id = $row['tipediet_id'];
                $modDetails[$i]->jenisdiet_id = $row['jenisdiet_id'];
                
                $modDetails[$i]->energikalori = $row['energikalori'];
                $modDetails[$i]->protein = $row['protein'];
                $modDetails[$i]->lemak = $row['lemak'];
                $modDetails[$i]->hidratarang = $row['hidratarang'];
                $modDetails[$i]->diet_kandungan = $row['diet_kandungan'];
                $modDetails[$i]->keterangan = $row['keterangan'];
                $modDetails[$i]->alergidengan = $row['alergidengan'];
                
                $modDetails[$i]->pegawai_id = $_POST['pegawai_id'];
                $modDetails[$i]->tgljenisdiet = $format->formatDateTimeMediumForDB($_POST['tglJenisDiet']);
                
                $modDetails[$i]->create_time = date('Y-m-d H:i:s');
                $modDetails[$i]->update_time = date('Y-m-d H:i:s');
                $modDetails[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modDetails[$i]->create_loginpemakai_id = Yii::app()->user->id;
                $modDetails[$i]->update_loginpemakai_id =  Yii::app()->user->id;
                $modDetails[$i]->create_ruangan = Yii::app()->user->getState('ruangan_id');
                $modDetails[$i]->ahligizi = $_POST['ahligizi'];
                
//                    echo '<pre>';
//                    echo print_r($modDetails[$i]->attributes);
//                    echo '</pre>';
                $modDetails[$i]->validate();
                   
        }
        return $modDetails;
    }
        
}
?>