<?php

class InformasiRiwayatPasienGiziController extends SBaseController
{
    public function actionIndex()
    {
        $modPasienMasukPenunjang = new GZPasienMasukPenunjangV;
        $format = new CustomFormat();
        $modPasienMasukPenunjang->tglAwal=date('d M Y').' 00:00:00';
        $modPasienMasukPenunjang->tglAkhir=date('d M Y H:i:s');
        if(isset ($_REQUEST['GZPasienMasukPenunjangV'])){
            $modPasienMasukPenunjang->attributes=$_REQUEST['GZPasienMasukPenunjangV'];
            $modPasienMasukPenunjang->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZPasienMasukPenunjangV']['tglAwal']);
            $modPasienMasukPenunjang->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZPasienMasukPenunjangV']['tglAkhir']);
        }
        $this->render('index',array('modPasienMasukPenunjang'=>$modPasienMasukPenunjang));
    }
    public function actionRiwayatPasien($id,$idPasien)
    {
        $criteria = new CDbCriteria(array(
                    'condition' => 't.pasien_id = '.$idPasien,
                //.'
                  //      and t.ruangan_id ='.Yii::app()->user->getState('ruangan_id'),
                    'order'=>'tgl_pendaftaran DESC',
                ));

            $pages = new CPagination(GZPendaftaranT::model()->count($criteria));
            $pages->pageSize = Params::JUMLAH_PERHALAMAN; //Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);
            
            $modKunjungan = GZPendaftaranT::model()->with('hasilpemeriksaanlab','anamnesa','pemeriksaanfisik','pasienmasukpenunjang','diagnosa')->
                    findAll($criteria);
            $modPasienMasukPenunjang = GZPasienMasukPenunjangT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
            $modPendaftaran = GZPendaftaranT::model()->findByPk($id);
            $modPasien = GZPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modAnamnesa = GZAnamnesaDietT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
        
        $this->render('_riwayatPasien',array(
                            'pages'=>$pages,
                            'modKunjungan'=>$modKunjungan,
                            'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                            'modPendaftaran'=>$modPendaftaran, 
                            'modPasien'=>$modPasien,
                            'modAnamnesa'=>$modAnamnesa,
        ));
    }

}