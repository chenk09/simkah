<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class ActionAjaxGZController extends SBaseController
{
    
     public function actionGetKomposisiMakanan(){
        if(Yii::app()->request->isAjaxRequest) { 
            $jeniswaktu_id = $_POST['jeniswaktu_id'];
            $menudiet_id = $_POST['menudiet_id'];
            $bahanmakanan_id = $_POST['bahanmakanan_id'];
            
            $modJeniswaktu = JeniswaktuM::model()->findByPk($jeniswaktu_id);
            $modMenuDiet = MenuDietM::model()->findByPk($menudiet_id);
            $modBahanMakanan = BahanmakananM::model()->findByPk($bahanmakanan_id);
            
            $zatGiziBahanEner = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>1));
            $zatGiziBahanPro = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>3));
            $zatGiziBahanLemak = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>4));
            $zatGiziBDD = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>32));
            $zatGiziHidratArang = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>5));
            
            $zatEnergi = $zatGiziBahanEner->kandunganbahan;
            $zatProtein = $zatGiziBahanPro->kandunganbahan;
            $zatLemak = $zatGiziBahanLemak->kandunganbahan;
            $zatBDD = $zatGiziBDD->kandunganbahan;
            $zatHidratArang = $zatGiziHidratArang->kandunganbahan;
//            
            $modAnamnesa = new GZAnamnesaDietT;
            $nourut = 1;
                $tr="<tr>
                        <td>".CHtml::TextField('noUrut',$nourut,array('class'=>'span1 noUrut','readonly'=>TRUE,'onClick'=>'setAll(this)')).                              
                              CHtml::activeHiddenField($modAnamnesa,'['.$jeniswaktu_id.']menudiet_id',array('value'=>$menudiet_id, 'class'=>'menudiet_id' )).
                              CHtml::activeHiddenField($modAnamnesa,'['.$jeniswaktu_id.']bahanmakanan_id',array('value'=>$bahanmakanan_id, 'class'=>'bahanmakanan_id')).
                              CHtml::activeHiddenField($modAnamnesa,'['.$jeniswaktu_id.']jeniswaktu_id',array('value'=>$jeniswaktu_id, 'class'=>'jeniswaktu_id')).
                       "</td>
                        <td>".$modBahanMakanan->namabahanmakanan."</td>
                        <td>".$modBahanMakanan->satuanbahan."</td>
                        <td>".CHtml::activetextField($modAnamnesa,'['.$jeniswaktu_id.']beratbahan',array('onkeyup'=>'setBeratBahan(this);','value'=>0,'class'=>'span1  beratbahan numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activetextField($modAnamnesa,'['.$jeniswaktu_id.']energikalori',array('value'=>(empty($zatEnergi) ?  "0" : $zatEnergi),'class'=>'span1 energikalori numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;')).
                              CHtml::activeHiddenField($modAnamnesa,'['.$jeniswaktu_id.']energikalori2',array('value'=>(empty($zatEnergi) ?  "0" : $zatEnergi),'class'=>'span1 energikalori2 numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;')).
                        "</td>
                        <td>".CHtml::activetextField($modAnamnesa,'['.$jeniswaktu_id.']protein',array('value'=>(empty($zatProtein) ?  "0" : $zatProtein),'class'=>'span1 protein numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activetextField($modAnamnesa,'['.$jeniswaktu_id.']lemak',array('value'=>(empty($zatLemak) ?  "0" : $zatLemak),'class'=>'span1 lemak numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activetextField($modAnamnesa,'['.$jeniswaktu_id.']hidratarang',array('value'=>(empty($zatHidratArang) ?  "0" : $zatHidratArang),'class'=>'span1 hidratarang numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;')).
                              CHtml::activeHiddenField($modAnamnesa,'['.$jeniswaktu_id.']bdd',array('value'=>(empty($zatBDD) ?  "0" : $zatBDD),'class'=>'span1 bdd numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;')).
                       "</td>
                        <td>".CHtml::activeDropDownList($modAnamnesa,'['.$jeniswaktu_id.']katpekerjaan', KatPekerjaan::items(),array('empty'=>'--Pilih--','class'=>'span1 katpekerjaan','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activeTextField($modAnamnesa,'['.$jeniswaktu_id.']urt',array('class'=>'span1 urt','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activeTextArea($modAnamnesa,'['.$jeniswaktu_id.']keterangan',array('class'=>'span1 keterangan','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;'))."</td>
                      </tr>   
                    ";
           
           $data['tr']=$tr;
           $data['sat']=$modBahanMakanan->satuanbahan;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
     public function actionGetDietPasien(){
        if(Yii::app()->request->isAjaxRequest) { 
            $idJenisDiet = $_POST['idJenisDiet'];
            
            $modMenuDiet = MenuDietM::model()->findAllByAttributes(array('jenisdiet_id'=>$idJenisDiet));
            $modBahanMakanan = BahanmakananM::model()->findByPk($bahanmakanan_id);
            $modJenisDiet = GZJenisdietM::model()->findByPk($idJenisDiet);
            
            $zatGiziBahanEner = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>1));
            $zatGiziBahanPro = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>3));
            $zatGiziBahanLemak = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>4));
            $zatGiziBDD = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>32));
            $zatGiziHidratArang = ZatBahanMakananM::model()->findByAttributes(array('bahanmakanan_id'=>$bahanmakanan_id,'zatgizi_id'=>5));
            
            $zatEnergi = $zatGiziBahanEner->kandunganbahan;
            $zatProtein = $zatGiziBahanPro->kandunganbahan;
            $zatLemak = $zatGiziBahanLemak->kandunganbahan;
            $zatBDD = $zatGiziBDD->kandunganbahan;
            $zatHidratArang = $zatGiziHidratArang->kandunganbahan;
//            
            $modDietPasien = new GZDietPasienT;
            $nourut = 1;
                $tr="<tr>
                        <td>".CHtml::TextField('noUrut',$nourut,array('class'=>'span1 noUrut','readonly'=>TRUE,'onClick'=>'setAll(this)')).                              
                              CHtml::activeHiddenField($modDietPasien,'['.$idJenisDiet.']jenisdiet_id',array('value'=>$menudiet_id, 'class'=>'menudiet_id' )).
                       "</td>
                        <td>".CHtml::activeDropDownList($modDietPasien,'['.$idJenisDiet.']tipediet_id', CHtml::listData(TipeDietM::model()->findAll(), 'tipediet_id', 'tipediet_nama'),array('empty'=>'--Pilih--','class'=>'span1 katpekerjaan','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                         
                        <td>".$modJenisDiet->jenisdiet_nama."</td>   
                        <td>".CHtml::activetextField($modDietPasien,'['.$idJenisDiet.']energikalori',array('onkeyup'=>'setEnergiKalori(this)','value'=>0,'class'=>'span1 energikalori numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activetextField($modDietPasien,'['.$idJenisDiet.']protein',array('onkeyup'=>'setProtein(this)','value'=>0,'class'=>'span1 protein numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activetextField($modDietPasien,'['.$idJenisDiet.']lemak',array('onkeyup'=>'setLemak(this)','value'=>0,'class'=>'span1 lemak numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activetextField($modDietPasien,'['.$idJenisDiet.']hidratarang',array('onkeyup'=>'setHidratArang(this)','value'=>0,'class'=>'span1 hidratarang numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activetextField($modDietPasien,'['.$idJenisDiet.']diet_kandungan',array('onkeyup'=>'setDietKandungan(this)','value'=>0,'class'=>'span1 dietkandungan numbersOnly','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activeTextArea($modDietPasien,'['.$idJenisDiet.']alergidengan',array('class'=>'span1 alergidengan','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                        <td>".CHtml::activeTextArea($modDietPasien,'['.$idJenisDiet.']keterangan',array('class'=>'span1 keterangan','readonly'=>FALSE, 'style'=>'width:80px;'))."</td>
                     
                      </tr>   
                    ";
           
           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetJenisKonsul()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $kelasPelayan_id = $_POST['kelasPelayan_id'];
            $kelaspelayanan = (isset($_POST['kelasPelayan_id']) ? $_POST['kelasPelayan_id'] : null);
            $criteria = new CDbCriteria;
            $criteria->select = 'count(daftartindakan_m.daftartindakan_id),daftartindakan_m.daftartindakan_id,daftartindakan_m.daftartindakan_nama,
                                t.kelaspelayanan_id, sum(t.harga_tariftindakan) as harga_tariftindakan,(t.persendiskon_tind) as persendiskon_tind,
                                (t.hargadiskon_tind) as hargadiskon_tind, (t.persencyto_tind) as persencyto_tind';
            $criteria->group = 'daftartindakan_m.daftartindakan_id,daftartindakan_m.daftartindakan_nama,t.kelaspelayanan_id,
                                daftartindakan_m.daftartindakan_id,t.persendiskon_tind, t.persencyto_tind, t.hargadiskon_tind';
            $criteria->compare('daftartindakan_m.daftartindakan_konsul',true);
            $criteria->compare('t.kelaspelayanan_id',$kelaspelayanan);
            $criteria->join = 'JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = t.daftartindakan_id';

            $modTindakan = TariftindakanM::model()->findAll($criteria);
            
            echo CJSON::encode(array(
                'status'=>'create_form', 
                'form'=>$this->renderPartial('_formTarifKonsul', array(
                                      'modTindakan'=>$modTindakan,
                                      'kelaspelayanan'=>$kelaspelayanan), true)));
            exit;               
        }
    }
    
public function actionBatalMenuDiet(){
    if (Yii::app()->request->isAjaxRequest)
    {
     $idPesanDiet = $_POST['idPesanDiet'];
     $modelPesan = new PesanmenudietT;
     $model = PesanmenudietT::model()->findByPk($idPesanDiet);  
     $modDetail = PesanmenudetailT::model()->findAllByAttributes(array('pesanmenudiet_id'=>$model->pesanmenudiet_id));
     $modPegawai = PesanmenupegawaiT::model()->findAllByAttributes(array('pesanmenudiet_id'=>$model->pesanmenudiet_id));
     
     $totDet = count($modDetail);
     $totPeg = count($modPegawai);

      if(isset($_POST['PesanmenupegawaiT']) || isset($_POST['PesanmenudetailT'])){
        if(count($modDetail) > 0 || count($modPegawai) > 0){        
                if(count($modPegawai) > 0){
                    // Untuk Menghapus Pesan Menu Diet untuk Pegawai
                 if (count($_POST['PesanmenupegawaiT']) > 0){
                    foreach($_POST['PesanmenupegawaiT'] as $i=>$v){
                    if (isset($v['checkList'])){
                            if(empty($v['kirimmenupegawai_id'])){
                                $detail = false;
                            }else{
                                $detail = true;
                                $updatePesanPegawai = PesanmenupegawaiT::model()->updateByPk($v['pesanmenupegawai_id'],array('kirimmenupegawai_id'=>null));
                                $updateKirimPegawai = KirimmenupegawaiT::model()->updateByPk($v['kirimmenupegawai_id'],array('pesanmenupegawai_id'=>null));
                                if(count($modPegawai) <= 1){
                                    $updatePesanDiet = KirimmenudietT::model()->updateByPk($V['kirimmenudiet_id'],array('pesanmenudiet_id'=>null));
                                    $updateKirimDiet = PesanmenudietT::model()->updateByPk($idPesanDiet,array('kirimmenudiet_id'=>null));
                                }
                            }

                        if($detail == true){
                            $deletePegawai = PesanmenupegawaiT::model()->deleteByPk($v['pesanmenupegawai_id']);                                                                          
                            if($updatePesanPegawai && $updatePesanDiet && $updateKirimPegawai && $updateKirimDiet){                                            
                               $delete = true;
                            }else{
                                $delete = false; 
                            }
                        }else{
                            $deletePegawai = PesanmenupegawaiT::model()->deleteByPk($v['pesanmenupegawai_id']);
                            if($deletePegawai){
                                $delete = true;
                            }else{
                                $delete = false;
                            }
                        }
                    $totPeg = count(PesanmenupegawaiT::model()->findAllByAttributes(array('pesanmenudiet_id'=>$idPesanDiet)));
                }                
               }
               if($delete == true){
                    if($totPeg < 1){
                       PesanmenudietT::model()->deleteByPk($idPesanDiet);
                    }
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'pesan'=>'Berhasil',
                        'keterangan'=>'',
                        'total'=>$totPeg,
                        'div'=>"<div class='flash-success'>Pemesanan Menu Diet <b></b> berhasil dibatalkan </div>",
                        ));
                    exit;  
                }else{
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'pesan'=>'Gagal',
                        'keterangan'=>'',
                        'total'=>$totPeg,
                        'div'=>"<div class='flash-success'>Pemesanan Menu Diet <b></b> gagal dibatalkan </div>",
                        ));
                    exit;  
                }
             }
            }else{
                if(count($_POST['PesanmenudetailT']) > 0){
                    $jml = 0;
                    foreach($_POST['PesanmenudetailT'] as $i=>$v){
                        if(isset($v['checkList'])){
//                            foreach($modDetail as $i=>$detail){
                                if(empty($v['kirimmenupasien_id'])){
                                    $details = false;
                                }else{
                                    $details = true;
                                    $updatePesanPasien = PesanmenudetailT::model()->updateByPk($v['pesanmenudetail_id'],array('kirimmenupasien_id'=>null));                                            
                                    $updateKirimPasien = KirimmenupasienT::model()->updateByPk($v['kirimmenupasien_id'],array('pesanmenudetail_id'=>null));

                                    if(count($modDetail) <= 1){
                                         $updatePesanDiet = KirimmenudietT::model()->updateByPk($v['kirimenudiet_id'],array('pesanmenudiet_id'=>null));
                                         $updateKirimDiet = PesanmenudietT::model()->updateByPk($idPesanDiet,array('kirimmenudiet_id'=>null));
                                    }
                                }
//                            }
                    // Untuk Menghapus menu Gizi dari PesanmenudetailT                    
                    if($details == true){
                        $deleteDetail = PesanmenudetailT::model()->deleteByPk($v['pesanmenudetail_id']);
                        if($updatePesanPasien && $updatePesanDiet && $updateKirimPasien && $updateKirimDiet && $deleteDetail){
                            $tindakan = true;
                        }else{
                          $tindakan = false;
                        }
                    }else{
                        $deleteDetail = PesanmenudetailT::model()->deleteByPk($v['pesanmenudetail_id']);
                        if($deleteDetail){
                            $tindakan = true;
                        }else{
                          $tindakan = false;
                        }
                    } 
                        $jml++;
                        $totDet = count(PesanmenudetailT::model()->findAllByAttributes(array('pesanmenudiet_id'=>$idPesanDiet)));
                  }
                }
                if($tindakan == true){
                    if($totDet < 1){
                        PesanmenudietT::model()->deleteByPk($idPesanDiet);
                    }
                    // Untuk Menghapus Data Kirim Menu Diet dari PesanmenudietT
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'pesan'=>'Berhasil',
                        'keterangan'=>'',
                        'total'=>$totDet,
                        'div'=>"<div class='flash-success'>Pemesanan Menu Diet <b></b> berhasil dibatalkan </div>",
                        ));
                    exit;
                }else{
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'pesan'=>'Gagal',
                        'keterangan'=>'',
                        'total'=>$totDet,
                        'div'=>"<div class='flash-success'>Pemesanan Menu Diet <b></b> gagal dibatalkan </div>",
                        ));
                    exit;
                }
//                echo $jml;
            }
          }
        }
    }
   /* 
    * Tidak dipake .. ini untuk bypass penghapusan ke PesanmenudietT 
    else{
        if(count($model) > 0){
            $deletePesan = PesanmenudietT::model()->deleteByPk($idPesanDiet);
            if($deletePesan){
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'pesan'=>'Berhasil',
                        'keterangan'=>'',
                        'total'=>$totDet,
                        'div'=>"<div class='flash-success'>Pengiriman Menu Diet <b></b> berhasil dibatalkan </div>",
                        ));
                    exit;               
                }
            }else{
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'pesan'=>'Gagal',
                        'keterangan'=>'',
                        'total'=>$totDet,
                        'div'=>"<div class='flash-success'>Pengiriman Menu Diet <b></b> gagal dibatalkan </div>",
                        ));
                    exit;               
                }
            }
        }
    }
    /*
     * 
     */
        echo CJSON::encode(array(
            'status'=>'create_form', 
            'idPesan'=>$idPesanDiet,
            'total'=>$totDet,
            'div'=>$this->renderPartial('_formBatalPesanDiet', array('modelPesan'=>$modelPesan,'modDetail'=>$modDetail,'modPegawai'=>$modPegawai,'model'=>$model), true)));             
        exit;
  }
}
    
public function actionBatalKirimMenuDiet(){
if (Yii::app()->request->isAjaxRequest)
{
        $idKirimDiet = $_POST['idKirimDiet'];
        $modelKirim = new KirimmenudietT;
        $model = KirimmenudietT::model()->findByPk($idKirimDiet);              
        $modDetail = KirimmenupasienT::model()->findAllByAttributes(array('kirimmenudiet_id'=>$idKirimDiet));
        $modPegawai = KirimmenupegawaiT::model()->findAllByAttributes(array('kirimmenudiet_id'=>$idKirimDiet));
        
        $totDet = count($modDetail);
        $totPeg = count($modPegawai);
        
        if(count($modPegawai) > 0){
            $total = $totPeg;
        }else{
            $total = $totDet;
        }

    if(isset($_POST['KirimmenupegawaiT']) || isset($_POST['KirimmenupasienT'])){
        if(count($modDetail) > 0 || count($modPegawai) > 0){
            if(count($modPegawai) > 0){
                // Untuk Menghapus Kirim Menu Diet untuk Pegawai
                if (count($_POST['KirimmenupegawaiT']) > 0){
                    foreach($_POST['KirimmenupegawaiT'] as $i=>$v){
                        if (isset($v['checkList'])){
                                if(empty($v['pesanmenupegawai_id'])){
                                    $detail = false;
                                }else{
                                    $detail = true;
                                    $updatePesanPegawai = PesanmenupegawaiT::model()->updateByPk($v['pesanmenupegawai_id'],array('kirimmenupegawai_id'=>null));
                                    $updateKirimPegawai = KirimmenupegawaiT::model()->updateByPk($v['kirimmenupegawai_id'],array('pesanmenupegawai_id'=>null));
                                }

                            if($detail == true){
                                $deletePegawai = KirimmenupegawaiT::model()->deleteByPk($v['kirimmenupegawai_id']);                                                                      
                                if($updatePesanPegawai  && $updateKirimPegawai && $deletePegawai){   
                                    $delete = true;           
                                }else{
                                    $delete = false;
                                }
                            }else{
                                $deletePegawai = KirimmenupegawaiT::model()->deleteByPk($v['kirimmenupegawai_id']);
                                if($deletePegawai){
                                    $delete = true;  
                                }else{
                                    $delete = false;
                                }
                            }
                            $totPeg = count(KirimmenupegawaiT::model()->findAllByAttributes(array('kirimmenudiet_id'=>$idKirimDiet)));
                            if($totPeg < 1){                                
                                $updatePasienDiet = PesanmenudietT::model()->updateAll(array('kirimmenudiet_id'=>null),'kirimmenudiet_id = '.$idKirimDiet.'');
                                $updateKirimDiet  = KirimmenudietT::model()->updateByPk($idKirimDiet,array('pesanmenudiet_id'=>null));
                                $deletePesanDiet  = PesanmenudietT::model()->deleteByPk($v['pesanmenudiet_id']);
                                if($updatePasienDiet && $updateKirimDiet){
                                    $updateAll = true;
                                }else{
                                    $updateAll = false;
                                }
                            }
                        }
                    }
                    if($delete == true || $updateAll == true){
                        if($totPeg < 1){
                            KirimmenudietT::model()->deleteByPk($idKirimDiet);
                        } 
                        echo CJSON::encode(array(
                            'status'=>'proses_form', 
                            'pesan'=>'Berhasil',
                            'keterangan'=>'',
                            'total'=>$totPeg,
                            'div'=>"<div class='flash-success'>Pemesanan Menu Diet <b></b> berhasil dibatalkan </div>",
                            ));
                        exit;  
                    }else{
                        echo CJSON::encode(array(
                            'status'=>'proses_form', 
                            'pesan'=>'Gagal',
                            'keterangan'=>'',
                            'total'=>$totPeg,
                            'div'=>"<div class='flash-success'>Pemesanan Menu Diet <b></b> gagal dibatalkan </div>",
                            ));
                        exit;  
                    }
                }
            }else{
                // Untuk Menghapus Kirim Menu Diet untuk Pasien
                    if (count($_POST['KirimmenupasienT']) > 0){
                        $jml = 1;
                        foreach($_POST['KirimmenupasienT'] as $i=>$v){
                        if (isset($v['checkList'])){
                            if(empty($v['pesanmenudetail_id'])){
                                $details = false;
                            }else{
                                $details = true;
                                $updatePesanPasien = PesanmenudetailT::model()->updateByPk($v['pesanmenudetail_id'],array('kirimmenupasien_id'=>null));                                            
                                $updateKirimPasien = KirimmenupasienT::model()->updateByPk($V['kirimmenupasien_id'],array('pesanmenudetail_id'=>null));                                                                                                
                            }
                            
                            $criteria = new CDbCriteria();
                            $criteria->compare('menudiet_id',$v['menudiet_id']);
                            $menudiet = MenuDietM::model()->findAll($criteria);  
                            // Untuk Menghapus menu gizi dari TindakanPelayananT
                            foreach($menudiet as $d=>$diet){
                                $criteria2 = new CDbCriteria();                                        
                                $criteria2->compare('pendaftaran_id',$v['pendaftaran_id']);                        
                                $criteria2->compare('ruangan_id',Params::RUANGAN_ID_GIZI);
                                $criteria2->compare('daftartindakan_id',$diet->daftartindakan_id);
                                $criteria2->addCondition('tindakansudahbayar_id is null');
                                $tindakan = TindakanpelayananT::model()->findAll($criteria2);
                                if(count($tindakan) > 0){
                                    foreach($tindakan as $key=>$datas){
                                        $sukses_pembalik = TindakanController::jurnalPembalikTindakan($datas->tindakanpelayanan_id);
                                        $tindakanpelayanan_id = $datas->tindakanpelayanan_id;
                                        $daftartindakan_id = $diet->daftartindakan_id;
                                    }
                                }else{
                                        echo CJSON::encode(array(
                                            'status'=>'proses_form', 
                                            'pesan'=>'Gagal',
                                            'keterangan'=>'Maaf, tindakan tidak dapat dibatalkan , karena sudah dibayarkan !',
                                            'total'=>$totDet,
                                            'div'=>"<div class='flash-success'>Maaf, tindakan tidak dapat dibatalkan , karena sudah dibayarkan ! </div>",
                                            )); 
                                }
                            }
                        // Untuk Menghapus menu Gizi dari KirimmenupasienT                    
                            if($details == true){
                                $deleteDetail = KirimmenupasienT::model()->deleteByPk($v['kirimmenupasien_id']);                                
                                if($deleteDetail){  
                                    $sukses_delPel = TindakanpelayananT::model()->deleteByPk($tindakanpelayanan_id);
                                    $sukses_delKom = TindakankomponenT::model()->deleteAllByAttributes(array('tindakanpelayanan_id'=>$tindakanpelayanan_id));                                                                           
                                    $konfigSys = KonfigsystemK::model()->find();
                                    //DINONAKTIFKAN KARENA PENGHAPUSAN DI ATUR DI RELASI TABEL : CASCADE ON DELETE
//                                      if($konfigSys->isdeljurnaltransaksi == true){
//                                        $updateTindakan = TindakanpelayananT::model()->findByPk($tindakanpelayanan_id);
//                                        if(isset($updateTindakan->jurnalrekening_id)){
//                                                $jurnalrekId = $updateTindakan->jurnalrekening_id;
//                                                $updateTindakan->jurnalrekening_id = null;
//                                                $updateTindakan->save();
//                                                // hapus jurnaldetail_t dan jurnalrekening_t
//                                                if($jurnalrekId){
//                                                    JurnaldetailT::model()->deleteAllByAttributes(array('jurnalrekening_id'=>$jurnalrekId));
//                                                    JurnalrekeningT::model()->deleteByPk($jurnalrekId);
//                                                }
//                                            }
//                                        }
                                    if($sukses_delKom&&$sukses_delPel){
                                        $tindakan = true;
                                    }else{
                                        $tindakan = false;
                                    }

                                }else{
                                  $tindakan = false;
                                } 
                            }else{
                                $deleteDetail = KirimmenupasienT::model()->deleteByPk($v['kirimmenupasien_id']);                                
                                if($deleteDetail){
                                    $sukses_delPel = TindakanpelayananT::model()->deleteByPk($tindakanpelayanan_id);
                                    $sukses_delKom = TindakankomponenT::model()->deleteAllByAttributes(array('tindakanpelayanan_id'=>$tindakanpelayanan_id));                                    
                                    $konfigSys = KonfigsystemK::model()->find();
                                    //DINONAKTIFKAN KARENA PENGHAPUSAN DI ATUR DI RELASI TABEL : CASCADE ON DELETE
//                                      if($konfigSys->isdeljurnaltransaksi == true){
//                                        $updateTindakan = TindakanpelayananT::model()->findByPk($tindakanpelayanan_id);
//                                        if(isset($updateTindakan->jurnalrekening_id)){
//                                                $jurnalrekId = $updateTindakan->jurnalrekening_id;
//                                                $updateTindakan->jurnalrekening_id = null;
//                                                $updateTindakan->save();
//                                                // hapus jurnaldetail_t dan jurnalrekening_t
//                                                if($jurnalrekId){
//                                                    JurnaldetailT::model()->deleteAllByAttributes(array('jurnalrekening_id'=>$jurnalrekId));
//                                                    JurnalrekeningT::model()->deleteByPk($jurnalrekId);
//                                                }
//                                            }
//                                        }

                                    if($sukses_delKom&&$sukses_delPel){
                                        $tindakan = true;
                                    }else{
                                        $tindakan = false;
                                    }
                                    
                                }else{
                                  $tindakan = false;
                                }
                            }
                        }
                        $totDet = count(KirimmenupasienT::model()->findAllByAttributes(array('kirimmenudiet_id'=>$idKirimDiet)));
                        if($totDet < 1){
                            $updatePasienDiet = PesanmenudietT::model()->updateAll(array('kirimmenudiet_id'=>null),'kirimmenudiet_id = '.$idKirimDiet.'');
                            $updateKirimDiet = KirimmenudietT::model()->updateByPk($idKirimDiet,array('pesanmenudiet_id'=>null));
                            $deletePesanDiet = PesanmenudietT::model()->deleteByPk($v['pesanmenudiet_id']);
                            if($updatePasienDiet && $updateKirimDiet){
                                $updateAll = true;
                            }else{
                                $updateAll = false;
                            }
                        }
                        $jml++;
                    } 
                  }
                }
                
                if($tindakan == true || $updateAll == true){
                    // Untuk Menghapus Data Kirim Menu Diet dari KirimmenudietT
                   if($totDet < 1){
                            KirimmenudietT::model()->deleteByPk($idKirimDiet);
                   }
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'pesan'=>'Berhasil',
                        'keterangan'=>'',
                        'total'=>$totDet,
                        'div'=>"<div class='flash-success'>Pengiriman Menu Diet <b></b> berhasil dibatalkan </div>",
                        ));
                    exit;
                 }else{
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'pesan'=>'Gagal',
                        'keterangan'=>'',
                        'total'=>$totDet,
                        'div'=>"<div class='flash-success'>Pengiriman Menu Diet <b></b> gagal dibatalkan </div>",
                        ));
                    exit;   
                 }
            }
        }

        echo CJSON::encode(array(
            'status'=>'create_form', 
            'idKirim'=>$idKirimDiet,
            'total'=>$total,
            'div'=>$this->renderPartial('_formBatalKirimDiet', array('modelKirim'=>$modelKirim,'modDetail'=>$modDetail,'modPegawai'=>$modPegawai,'model'=>$model), true)));             
        exit;
    }
    
}
    
}
?>
