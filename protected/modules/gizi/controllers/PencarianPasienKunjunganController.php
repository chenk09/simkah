<?php

class PencarianPasienKunjunganController extends SBaseController
{
	
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';
        public $successSavepasienKirimKeUnitLain=false;
        public $successSaveHasilPemeriksaan=false;
        public $successSavePasienMasukPenunjang=false;
        public $errorMessages = 'error<br/>';


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','DaftarDariPasienRujukan'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionIndex()
        {
                $format = new CustomFormat();
                $modRJ = new GZInfoKunjunganRJV;
                $modRI = new GZInfoKunjunganRIV;
                $modRD = new GZInfoKunjunganRDV;
                $cekRJ = true;
                $cekRI = false;
                $cekRD = false;
                $modRJ->tglAwal = date("d M Y").' 00:00:00';
                $modRJ->tglAkhir =date('d M Y H:i:s');
                $modRI->tglAwal = date("d M Y").' 00:00:00';
                $modRI->tglAkhir = date('d M Y H:i:s');
                $modRD->tglAwal = date("d M Y").' 00:00:00';
                $modRD->tglAkhir =date('d M Y H:i:s');
       
                
                if(isset ($_POST['instalasi']))
                {
                    switch ($_POST['instalasi']) {
                        case 'RJ':
                            $cekRJ = true;
                            $cekRI = false;
                            $cekRD = false;
                            $modRJ->attributes = $_POST['GZInfoKunjunganRJV'];
                            if(!empty($_POST['GZInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZInfoKunjunganRJV']['tglAwal']);
                            }
                            if(!empty($_POST['GZInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZInfoKunjunganRJV']['tglAkhir']);
                            }
                            
                            break;
                            
                        case 'RI':
                            $cekRI = true;
                            $cekRJ = false;
                            $cekRD = false;
                            $modRI->attributes = $_POST['GZInfoKunjunganRIV'];
                            if(!empty($_POST['GZInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZInfoKunjunganRIV']['tglAwal']);
                            }
                            if(!empty($_POST['GZInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZInfoKunjunganRIV']['tglAkhir']);
                            }
                            break;
                            
                        case 'RD':
                            $cekRD = true;
                            $cekRI = false;
                            $cekRJ = false;
                            $modRD->attributes = $_POST['GZInfoKunjunganRDV'];
                            if(!empty($_POST['GZInfoKunjunganRDV']['tglAwal']))
                            {
                                $modRD->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GZInfoKunjunganRDV']['tglAwal']);
                            }
                            if(!empty($_POST['GZInfoKunjunganRDV']['tglAwal']))
                            {
                                $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GZInfoKunjunganRDV']['tglAkhir']);
                            }
                           
                            break;
                    }
                }
                
                $this->render('index',array(
                                 'modRJ'=>$modRJ,
                                 'modRI'=>$modRI,
                                 'modRD'=>$modRD,
                                 'cekRJ'=>$cekRJ,
                                 'cekRI'=>$cekRI,
                                 'cekRD'=>$cekRD,
                ));
        }
        
        public function actionDaftarDariPasienRujukan($idPendaftaran,$idPasien,$idPasienAdmisi)
        {
            if(empty($idPasienAdmisi)){//Jika Pasien Bukan dari RI
                $modPendaftaran=GZPendaftaranT::model()->findByPk($idPendaftaran);
            }else{
                $modPendaftaran=GZPasienAdmisiT::model()->with('pendaftaran')->findByPk($idPasienAdmisi);
            }
            $modPasien=GZPasienM::model()->findByPk($idPasien);
//            $modPeriksaLab = GZPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true),array('order'=>'pemeriksaanlab_urutan'));
//            $modJenisPeriksaLab = GZJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan'));
         

            if(!empty($_POST['PemeriksaanLab'])){
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    $this->savePasienMasukPenunjang($modPasien,$modPendaftaran);
                    
                    if ($this->successSavepasienKirimKeUnitLain && $this->successSaveHasilPemeriksaan
                        && $this->successSavePasienMasukPenunjang){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Berhasil Disimpan");
                            $this->redirect(array('index'));
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan");
                            Yii::app()->user->setFlash('info',$this->errorMessages);
                        }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
          
            $this->render('daftarDariPasienRujukan',array(
                                 'modPeriksaLab'=>$modPeriksaLab,
                                 'modJenisPeriksaLab'=>$modJenisPeriksaLab,
                                 'modPendaftaran'=>$modPendaftaran, 
                                 'modPasien'=>$modPasien,
                                 'idPasienAdmisi'=>$idPasienAdmisi,
                ));
        }
        
        protected function savePasienMasukPenunjang($modPasien,$modPendaftaran)
        {
            $modPasienMasukPenunjang = new GZPasienMasukPenunjangT;
            $modPasienMasukPenunjang->pasien_id = $modPasien->pasien_id;
            if(!empty($modPendaftaran->pasienadmisi_id)){ ////Jika Pasien Berasal dari Rawat Inap
                $modPasienMasukPenunjang->pasienadmisi_id=$modPendaftaran->pasienadmisi_id;
                $modPasienMasukPenunjang->pendaftaran_id=$modPendaftaran->pendaftaran->pendaftaran_id;
                $modPasienMasukPenunjang->jeniskasuspenyakit_id=$modPendaftaran->pendaftaran->jeniskasuspenyakit_id;
                $no_pendaftaran = $modPendaftaran->pendaftaran->no_pendaftaran;
                $modPasienMasukPenunjang->statusperiksa=$modPendaftaran->pendaftaran->statusperiksa;
            }else{ ////Jika Pasien berasal dari RJ ataw RD
                $modPasienMasukPenunjang->pendaftaran_id=$modPendaftaran->pendaftaran_id;
                $modPasienMasukPenunjang->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
                $no_pendaftaran = $modPendaftaran->no_pendaftaran;
                $modPasienMasukPenunjang->statusperiksa=$modPendaftaran->statusperiksa;
            }
            $modPasienMasukPenunjang->pegawai_id=$modPendaftaran->pegawai_id;
            $modPasienMasukPenunjang->kelaspelayanan_id=$modPendaftaran->kelaspelayanan_id;
            $modPasienMasukPenunjang->ruangan_id = Params::RUANGAN_ID_LAB;
            $modPasienMasukPenunjang->no_masukpenunjang=Generator::noMasukPenunjang('LK');
            $modPasienMasukPenunjang->tglmasukpenunjang=date('Y-m-d H:i:s');
            $modPasienMasukPenunjang->no_urutperiksa=Generator::noAntrianPenunjang($no_pendaftaran, $modPasienMasukPenunjang->ruangan_id);
            $modPasienMasukPenunjang->kunjungan=$modPendaftaran->kunjungan;
            $modPasienMasukPenunjang->ruanganasal_id=$modPendaftaran->ruangan_id;
            if($modPasienMasukPenunjang->validate()){//Jika proses Palidasi Pasien Masuk Penunjang Berhasil
                if($modPasienMasukPenunjang->save()){//Jika $modPasienMasukPenunjang Berhasil Disimpan
                     $this->successSavePasienMasukPenunjang=true;
                     //Yii::app()->user->setFlash('success',"Data Pasien Masuk Penunjang berhasil Disimpan.");
                     $this->savePasienKirimKeUnitLain($modPasien,$modPendaftaran,$modPasienMasukPenunjang);
                     $this->saveHasilPemeriksaan($modPasienMasukPenunjang,$modPendaftaran,$modPasien,$_POST['PemeriksaanLab']);//Simpan Hasil Pemeriksaan
                }else{//Jika $modPasienMasukPenunjang Gagal Disimpan
                     $this->successSavePasienMasukPenunjang=false;
                     //Yii::app()->user->setFlash('error',"Data Pasien Masuk Pennunjang Gagal Disimpan.");
                     $this->errorMessages .= "Data Pasien Masuk Penunjang Gagal Disimpan.<br/>";
                }
            }else{//Jika proses Palidasi Pasien Masuk Penunjang Berhasil Gagal
               $this->successSavePasienMasukPenunjang=false;
               //Yii::app()->user->setFlash('error',"Data Pasien Masuk Pennunjang Tidak Tervalidasi.");
               $this->errorMessages .= "Data Pasien Masuk Penunjang Tidak Tervalidasi.</br>";
            }
        }
        
        protected function savePasienKirimKeUnitLain($modPasien,$modPendaftaran,$modPasienMasukPenunjang)
        {
            $modPasienKirimKeUnitLain = new GZPasienKirimKeUnitLainT;
            $modPasienKirimKeUnitLain->kelaspelayanan_id=$modPendaftaran->kelaspelayanan_id;
            $modPasienKirimKeUnitLain->instalasi_id=Params::INSTALASI_ID_LAB;
            $modPasienKirimKeUnitLain->pasien_id=$modPasien->pasien_id;
            $modPasienKirimKeUnitLain->pasienmasukpenunjang_id=$modPasienMasukPenunjang->pasienmasukpenunjang_id;
            $modPasienKirimKeUnitLain->ruangan_id= Params::RUANGAN_ID_LAB;
            $modPasienKirimKeUnitLain->pegawai_id=$modPendaftaran->pegawai_id;
            $modPasienKirimKeUnitLain->pendaftaran_id= $modPendaftaran->pendaftaran_id;
            $modPasienKirimKeUnitLain->nourut='001';
            $modPasienKirimKeUnitLain->tgl_kirimpasien= date('Y-m-d H:i:s');
            if($modPasienKirimKeUnitLain->validate()){
                if($modPasienKirimKeUnitLain->save()){
                       $this->successSavepasienKirimKeUnitLain=true;
                       //Yii::app()->user->setFlash('success',"Data Pasien Masuk Penunjang berhasil Disimpan.");
                }else{
                     $this->successSavepasienKirimKeUnitLain=false;
                     //Yii::app()->user->setFlash('error',"Data Pasien Kirim Ke Unit Lain Gagal Disimpan.");
                     $this->errorMessages .= "Data Pasien Kirim Ke Unit Lain Gagal Disimpan. <br/>";
                }
            }else{
                     $this->successSavepasienKirimKeUnitLain=false;
                     //Yii::app()->user->setFlash('error',"Data Pasien Kirim Ke Unit Lain Tidak Valid");
                     $this->errorMessages .= "Data Pasien Kirim Ke Unit Lain Tidak Valid. <br/>";
            }
            return $modPasienKirimKeUnitLain;
           
        }
        
        protected function saveTindakanPelayanan($modPendaftaran,$modPasien,$dataPemeriksaan, $modPasienMasukPenunjang)
        {
                $persenRujukanIn = Yii::app()->user->getState('persentasirujin');
                if($persenRujukanIn==0){
                    if(!empty($modPasienMasukPenunjang->pasienadmisi_id))
                        $persenRujukanIn = KelaspelayananM::model()->findByPk($modPendaftaran->kelaspelayanan_id)->persentasirujin;
                }
                $tarifRujukanIn = ($dataPemeriksaan['tarif_tindakan'] * $persenRujukanIn / 100);
                $modTindakanPelayananT = new GZTindakanPelayananT;
                $modTindakanPelayananT->penjamin_id = $modPendaftaran->penjamin_id;
                $modTindakanPelayananT->pasien_id = $modPasien->pasien_id;
                $modTindakanPelayananT->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                $modTindakanPelayananT->instalasi_id = Params::INSTALASI_ID_LAB;
                $modTindakanPelayananT->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayananT->daftartindakan_id = $dataPemeriksaan['daftartindakan_id'];
                $modTindakanPelayananT->carabayar_id = $modPendaftaran->carabayar_id;
                $modTindakanPelayananT->qty_tindakan = $dataPemeriksaan['qty_tindakan'];
                if(!empty($modPendaftaran->pasienadmisi_id)){ ////Jika Paien Berasal dari Rawat Inap
                    $modTindakanPelayananT->pasienadmisi_id = $modPendaftaran->pasienadmisi_id;
                    $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran->pendaftaran_id;
                    $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->pendaftaran->jeniskasuspenyakit_id;
                }else{ ////Jika Pasien berasal dari RJ ataw RD
                    $modTindakanPelayananT->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modTindakanPelayananT->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                }
                
                $modTindakanPelayananT->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayananT->tarif_satuan = $dataPemeriksaan['tarif_tindakan'] + $tarifRujukanIn;
                $modTindakanPelayananT->tarif_tindakan = $modTindakanPelayananT->tarif_satuan * $modTindakanPelayananT->qty_tindakan;
                $modTindakanPelayananT->satuantindakan = $dataPemeriksaan['satuantindakan'];
                $modTindakanPelayananT->cyto_tindakan = $dataPemeriksaan['cyto_tindakan'];
                $modTindakanPelayananT->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
                $modTindakanPelayananT->discount_tindakan = 0;
                $modTindakanPelayananT->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                $modTindakanPelayananT->tarifcyto_tindakan = 0;
                $modTindakanPelayananT->subsidiasuransi_tindakan = 0;
                $modTindakanPelayananT->subsidipemerintah_tindakan = 0;
                $modTindakanPelayananT->subsisidirumahsakit_tindakan = 0;
                $modTindakanPelayananT->iurbiaya_tindakan = 0;
                $modTindakanPelayananT->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayananT->pasienmasukpenunjang_id = $modPasienMasukPenunjang->pasienmasukpenunjang_id;

                $tarifTindakan= GZTarifTindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayananT->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayananT->kelaspelayanan_id.'');
                foreach($tarifTindakan as $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayananT->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayananT->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayananT->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayananT->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;

                if($modTindakanPelayananT->save()){//Jika Tindakan Pelayanan Berhasil tersimpan
                    $idPeriksaLab = $dataPemeriksaan['pemeriksaanlab_id'];
                    $jumlahTindKomponen = COUNT($_POST['LKTindakanKomponenT'][$idPeriksaLab]);
                    for($j=0; $j<$jumlahTindKomponen; $j++):
                        $modTindakanKomponen = new GZTindakanKomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['komponentarif_id'];
                        $modTindakanKomponen->tarif_tindakankomp = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'] * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $_POST['LKTindakanKomponenT'][$idPeriksaLab][$j]['tarif_tindakankomp'];
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                    endfor;
                        // -- menyimpan komponen jasa rujukan penunjang
                        $modTindakanKomponen = new GZTindakanKomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayananT->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = Params::KOMPONENTARIF_ID_JASA_RUJPEN;
                        $modTindakanKomponen->tarif_tindakankomp = $tarifRujukanIn * $modTindakanPelayananT->qty_tindakan;
                        $modTindakanKomponen->tarif_kompsatuan = $tarifRujukanIn;
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = 0;
                        $modTindakanKomponen->subsidipemerintahkomp = 0;
                        $modTindakanKomponen->subsidirumahsakitkomp = 0;
                        $modTindakanKomponen->iurbiayakomp = 0;
                        $modTindakanKomponen->save();
                }
                
                return $modTindakanPelayananT;
        }    
        
}
