
<?php

class AnamnesisDietController extends SBaseController
{
	public $layout='//layouts/column1';
        public $defaultAction = 'index';
        public $pathView = 'gizi.views.anamnesisDiet.';

	
	public function actionIndex($idPendaftaran)
	{
            $modPendaftaran=GZPendaftaranT::model()->findByPk($idPendaftaran);
            $modPasien = GZPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $dataPendaftaran = GZPendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$modPasien->pasien_id), array('order'=>'tgl_pendaftaran DESC'));
            //print_r($lastPendaftaran);
            //echo $modPasien->pasien_id;exit();
            
            if(!empty($modPendaftaran->pasienadmisi_id)){
                $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                if(!empty($modPasienAdmisi->pasienpulang_id)){
                    $status = "SUDAH PULANG";
                }
            }else{
                $status = $modPendaftaran->statusperiksa;
            }
            
            if($status == "SUDAH PULANG"){
                echo "<script>
                        alert('Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan Pemeriksaan. ');
                        window.top.location.href='".Yii::app()->createUrl('gizi/DaftarPasien/index')."';
                    </script>";
            }
            
            $i = 1;
            if (count($dataPendaftaran) > 1){
                foreach ($dataPendaftaran as $row){
                    if ($i == 2){
                        $lastPendaftaran = $row->pendaftaran_id;
                    }
                    $i++;
                }
            }else{
                $lastPendaftaran = $idPendaftaran;
            }

            $cekAnamnesa=GZAnamnesaDietT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));

            if(COUNT($cekAnamnesa)>0) {  //Jika Pasien Sudah Melakukan Anamnesa Sebelumnya
                $modAnamnesa=$cekAnamnesa;
            } else {  
                ////Jika Pasien Belum Pernah melakukan Anamnesa
                $modAnamnesa=new GZAnamnesaDietT;
//                $modAnamnesa->pegawai_id=$modPendaftaran->pegawai_id;
//                $modAnamnesa->tglanamesadiet=date('Y-m-d H:i:s');
                
            }
            
            if(isset($_POST['GZAnamnesaDietT']))
            {
                $modAnamnesa->attributes=$_POST['GZAnamnesaDietT'];
                $modDetails = $this->validasiTabular($modPendaftaran, $_POST['GZAnamnesaDietT']);
//                                    echo '<pre>';
//                                    echo print_r($_POST['GZAnamnesaDietT']);
//                                    exit();
                $transaction = Yii::app()->db->beginTransaction();
                try {
                        $jumlah = 0;

//                        if($modAnamnesa->save()){
                            $modDetails = $this->validasiTabular($modPendaftaran, $_POST['GZAnamnesaDietT']);
                            foreach ($modDetails as $i=>$row)
                            {
//                                                $modRencanaprodet = new RencanaprodetM;
//                                                $modRencanaprodet->rencanaproduksi_id = $model->rencanaproduksi_id;
//                                                
//                                                $modRencanaprodet->attributes = $row;
                                if ($row->save()) {
                                    $jumlah++;
                                }
                            }
//                        }
                        if ($jumlah == count($modDetails)) {
                            $transaction->commit();
                            Yii::app()->user->setFlash('success','<strong>Sukses</strong> Data berhasil disimpan');
                            $this->redirect(array('index', 'idPendaftaran' =>$idPendaftaran));
                        } else {
                            Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan '.'<pre>'.
                                    print_r($row->getErrors(),1).'</pre>');

//                            Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan');
                            $transaction->rollback();
                        }
                }
                catch (Exception $ex) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan '.$ex->getMessage());
                }
            }
            
                
            $modAnamnesa->tglanamesadiet = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modAnamnesa->tglanamesadiet, 'yyyy-MM-dd hh:mm:ss'));    
            
            $this->render($this->pathView.'index',array(
                            'modPendaftaran'=>$modPendaftaran,
                            'modPasien'=>$modPasien,
                            'modAnamnesa'=>$modAnamnesa, 
                            'modDetails'=>$modDetails,
            ));
    }
    
        
    protected function validasiTabular($modPendaftaran,$data) {
        foreach ($data as $i => $row) {
            
//            if(is_integer($row)){
                $format = new CustomFormat();
                $modDetails[$i] = new GZAnamnesaDietT();
                $modDetails[$i]->attributes = $row;                
                $modDetails[$i]->pasien_id = $modPendaftaran->pasien_id;
                $modDetails[$i]->pasienadmisi_id = $modPendaftaran->pasienadmisi_id;
                $modDetails[$i]->jeniswaktu_id = $modDetails[$i]['jeniswaktu_id'];
                $modDetails[$i]->bahanmakanan_id = $row['bahanmakanan_id'];
                $modDetails[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                $modDetails[$i]->pekerjaan_id = $modPendaftaran->pekerjaan_id;
                $modDetails[$i]->menudiet_id = $row['menudiet_id'];
                $modDetails[$i]->katpekerjaan = $row['katpekerjaan'];
                $modDetails[$i]->beratbahan = $row['beratbahan'];
                $modDetails[$i]->energikalori = $row['energikalori'];
                $modDetails[$i]->protein = $row['protein'];
                $modDetails[$i]->lemak = $row['lemak'];
                $modDetails[$i]->hidratarang = $row['hidratarang'];
                $modDetails[$i]->urt = $row['urt'];
                $modDetails[$i]->tglanamesadiet = $format->formatDateTimeMediumForDB($_POST['tglAnamnesadiet']);
                $modDetails[$i]->pegawai_id = $_POST['pegawai_id'];
                $modDetails[$i]->keterangan = $row['keterangan'];
                $modDetails[$i]->create_time = date('Y-m-d H:i:s');
                $modDetails[$i]->update_time = date('Y-m-d H:i:s');
                $modDetails[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modDetails[$i]->create_loginpemakai_id = Yii::app()->user->id;
                $modDetails[$i]->update_loginpemakai_id =  Yii::app()->user->id;
                $modDetails[$i]->create_ruangan = Yii::app()->user->getState('ruangan_id');
                $modDetails[$i]->ahligizi = $_POST['ahligizi'];
//                    echo '<pre>';
//                    echo print_r($modDetails[$i]->attributes);
//                    echo '</pre>';
                $modDetails[$i]->validate();
                    
//            }    
                   
        }
        return $modDetails;
    }
        
}
?>
