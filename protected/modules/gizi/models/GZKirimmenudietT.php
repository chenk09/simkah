<?php

class GZKirimmenudietT extends KirimmenudietT {

    public $instalasi_id;
    public $ruangan_id;
    public $tglAwal;
    public $tglAkhir;
//    public $jenispesanmenu;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getNamaModel() {
        return __CLASS__;
    }

    public function searchInformasi() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        $criteria->addBetweenCondition('DATE(tglkirimmenu)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('kirimmenudiet_id', $this->kirimmenudiet_id);
        $criteria->compare('bahandiet_id', $this->bahandiet_id);
        $criteria->compare('jenisdiet_id', $this->jenisdiet_id);
        $criteria->compare('pesanmenudiet_id', $this->pesanmenudiet_id);
        $criteria->compare('LOWER(nokirimmenu)', strtolower($this->nokirimmenu), true);
        $criteria->compare('LOWER(jenispesanmenu)',strtolower($this->jenispesanmenu),true);
//        $criteria->compare('LOWER(tglkirimmenu)', strtolower($this->tglkirimmenu), true);
        $criteria->compare('LOWER(keterangan_kirim)', strtolower($this->keterangan_kirim), true);
        $criteria->compare('LOWER(create_time)', strtolower($this->create_time), true);
        $criteria->compare('LOWER(update_time)', strtolower($this->update_time), true);
        $criteria->compare('LOWER(create_loginpemakai_id)', strtolower($this->create_loginpemakai_id), true);
        $criteria->compare('LOWER(update_loginpemakai_id)', strtolower($this->update_loginpemakai_id), true);
        $criteria->compare('LOWER(create_ruangan)', strtolower($this->create_ruangan), true);
        // $criteria->with yg dibawah untuk menghasilkan relasi dari tabel master transaksi ke detail transaksinya .
//        $criteria->with = array('kirimmenupasien'=>array('select'=>'pasien_id,ruangan_id','together'=>false),'kirimmenupegawai'=>array('select'=>'pegawai_id,ruangan_id','together'=>false));
        $criteria->order='tglkirimmenu DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}