<?php

class GZLaporanJumlahPorsiV extends LaporanjmlporsigiziruanganV {
    
    public $tglAwal;
    public $tglAkhir;  
     
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

   public function searchTable() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->select='jenisdiet_nama, jenisdiet_id';
        $criteria->group='jenisdiet_nama, jenisdiet_id';
        $criteria->addBetweenCondition('tglkirimmenu', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('LOWER(tglkirimmenu)',strtolower($this->tglkirimmenu),true);
        $criteria->compare('jenisdiet_id',$this->jenisdiet_id);
        $criteria->compare('LOWER(jenisdiet_nama)',strtolower($this->jenisdiet_nama),true);
        $criteria->compare('LOWER(jml_kirim)',strtolower($this->jml_kirim),true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
        $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
        $criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
        $criteria->compare('kirimmenudiet_id',$this->kirimmenudiet_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function searchPrint()
        {
                 // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->select='jenisdiet_nama, jenisdiet_id';
        $criteria->group='jenisdiet_nama, jenisdiet_id';
        $criteria->addBetweenCondition('tglkirimmenu', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('LOWER(tglkirimmenu)',strtolower($this->tglkirimmenu),true);
        $criteria->compare('jenisdiet_id',$this->jenisdiet_id);
        $criteria->compare('LOWER(jenisdiet_nama)',strtolower($this->jenisdiet_nama),true);
        $criteria->compare('LOWER(jml_kirim)',strtolower($this->jml_kirim),true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
        $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
        $criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
        $criteria->compare('kirimmenudiet_id',$this->kirimmenudiet_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
        }
    
    // public function searchPrint() {
    //     // Warning: Please modify the following code to remove attributes that
    //     // should not be searched.

    //     $criteria = new CDbCriteria;
    //     $criteria = $this->functionCriteria();

    //     return new CActiveDataProvider($this, array(
    //                 'criteria' => $criteria,
    //                 // 'pagination'=>false,
    //             ));
    // }
    
     public function searchGrafik() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    protected function functionCriteria(){
        $criteria=new CDbCriteria;

        $criteria->addBetweenCondition('tglkirimmenu', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('LOWER(tglkirimmenu)',strtolower($this->tglkirimmenu),true);
        $criteria->compare('jenisdiet_id',$this->jenisdiet_id);
        $criteria->compare('LOWER(jenisdiet_nama)',strtolower($this->jenisdiet_nama),true);
        $criteria->compare('LOWER(jml_kirim)',strtolower($this->jml_kirim),true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
        $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
        $criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
        $criteria->compare('kirimmenudiet_id',$this->kirimmenudiet_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }   

    public function getSumKelas($nama_kolom = null, $idjenisdiet, $nama_ruangan = null){
            if(isset($_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'])){
                $komponentarif_id = $_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'];
            }
            // var_dump($idjenisdiet);
            $format = new CustomFormat();
            $criteria=new CDbCriteria();
            $criteria->group = 'ruangan_id, kelaspelayanan_id, jenisdiet_id';
            // foreach($groups AS $i => $group){
            //     if($group == 'jenisdiet_id'){
            //         $criteria->group .= ', jenisdiet_id';
            //         $criteria->compare('jenisdiet_id',$this->jenisdiet_id);
            //     }
            // }
            if($nama_kolom == 'Vvip'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_VVIP);
            }else if($nama_kolom == 'Vip A'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_VIP_A);
            }else if($nama_kolom == 'Vip B'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_VIP_B);
            }else if($nama_kolom == 'Utama'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_UTAMA);
            }else if($nama_kolom == 'Madya A'){
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_MADYA_A);
                $criteria->compare('jenisdiet_id',$idjenisdiet);
            }else if($nama_kolom == 'Madya B'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_MADYA_B);
            }else if($nama_kolom == 'Pratama A'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_PRATAMA_A);
            }else if($nama_kolom == 'Pratama B'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_PRATAMA_B);
            }else if($nama_kolom == 'Pratama C'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_PRATAMA_C);
            }

            if($nama_ruangan == '2A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2A);
            }
            if($nama_ruangan == '2B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2B);
            }
            if($nama_ruangan == '2C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2C);
            }
            if($nama_ruangan == '3A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3A);
            }
            if($nama_ruangan == '3B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3B);
            }
            if($nama_ruangan == '3C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3C);
            }
            if($nama_ruangan == '4A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4A);
            }
            if($nama_ruangan == '4B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4B);
            }
            if($nama_ruangan == '4C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4C);
            }
            if($nama_ruangan == 'ICU'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_ICU);
            }
            if($nama_ruangan == 'OBSERVASI'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_OBSERVASI);
            }
            if($nama_ruangan == 4042){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_BANGSAL);
            }
            

            if(isset($_GET['GZLaporanJumlahPorsiV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
                $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
            }
            $criteria->select = $criteria->group.', sum(jml_kirim) AS jml_kirim';
            $modTarif = LaporanjmlporsigiziruanganV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jml_kirim;
            }
             return $totTarif;
        }

    public function getSumRuangan($idjenisdiet, $nama_ruangan=null){
        if (isset($_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'])){
            $komponentarif_id = $_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'];
            
        }
        $format = new CustomFormat();
        $criteria = new CDbCriteria();
        $criteria->group = 'ruangan_id, jenisdiet_id';

            if($nama_ruangan == 'ICU'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_ICU);
            }
            if($nama_ruangan == 'OBSERVASI'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_OBSERVASI);
            }
            if($nama_ruangan == 4042){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_BANGSAL);
            }




            if(isset($_GET['GZLaporanJumlahPorsiV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
                $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
            }
            $criteria->select = $criteria->group.', sum(jml_kirim) AS jml_kirim';
            $modTarif = LaporanjmlporsigiziruanganV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jml_kirim;
            }
             return $totTarif;

    }        

    public function getSumJml($idjenisdiet, $nama_ruangan=null){
        if (isset($_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'])){
            $komponentarif_id = $_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'];
            
        }
        $format = new CustomFormat();
        $criteria = new CDbCriteria();
        $criteria->group = 'ruangan_id, jenisdiet_id';
        $criteria->compare('jenisdiet_id',$idjenisdiet);


            if($nama_ruangan == '2A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2A);
            }
            if($nama_ruangan == '2B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2B);
            }
            if($nama_ruangan == '2C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2C);
            }
            if($nama_ruangan == '3A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3A);
            }
            if($nama_ruangan == '3B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3B);
            }
            if($nama_ruangan == '3C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3C);
            }
            if($nama_ruangan == '4A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4A);
            }
            if($nama_ruangan == '4B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4B);
            }
            if($nama_ruangan == '4C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4C);
            }
            if($nama_ruangan == 'ICU'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_ICU);
            }
            if($nama_ruangan == 'OBSERVASI'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_OBSERVASI);
            }
            if($nama_ruangan == 4042){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_BANGSAL);
            }


            if(isset($_GET['GZLaporanJumlahPorsiV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
                $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
            }
            $criteria->select = $criteria->group.', sum(jml_kirim) AS jml_kirim';
            $modTarif = LaporanjmlporsigiziruanganV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jml_kirim;
            }
             return $totTarif;

    }        

    public function getSumJmlT($nama_ruangan=null){
        // if (isset($_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'])){
        //     $komponentarif_id = $_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'];
            
        // }
        // $format = new CustomFormat();
        // $criteria = new CDbCriteria();
        // $criteria->group = 'ruangan_id';
        // $criteria->compare('jenisdiet_id',$idjenisdiet);


            $format = new CustomFormat();
            $criteria = new CDbCriteria();
            $criteria->group = 'kelaspelayanan_id,ruangan_id,jenisdiet_nama';
                foreach($groups AS $i => $group){
                if($group == 'jenisdiet'){
                    $criteria->group .= ', kelaspelayanan_id,ruangan_id';
                    $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
                    $criteria->compare('ruangan_id',$this->ruangan_id);
                // $criteria->compare('ruangan_id',$this->ruangan_id);
            } }

            if($nama_ruangan == '2A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2A);
            }
            if($nama_ruangan == '2B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2B);
            }
            if($nama_ruangan == '2C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2C);
            }
            if($nama_ruangan == '3A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3A);
            }
            if($nama_ruangan == '3B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3B);
            }
            if($nama_ruangan == '3C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3C);
            }
            if($nama_ruangan == '4A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4A);
            }
            if($nama_ruangan == '4B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4B);
            }
            if($nama_ruangan == '4C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4C);
            }
            if($nama_ruangan == 'ICU'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_ICU);
            }
            if($nama_ruangan == 'OBSERVASI'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_OBSERVASI);
            }
            if($nama_ruangan == 4042){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_BANGSAL);
            }


            if(isset($_GET['GZLaporanJumlahPorsiV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
                $jenisdiet_nama =$_GET['GZLaporanJumlahPorsiV']['jenisdiet_nama'];
                $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
                $criteria->compare('LOWER(jenisdiet_nama)',strtolower($jenisdiet_nama));
                $criteria->addInCondition('jenisdiet_id',$this->jenisdiet_id);
            }
            $criteria->select = $criteria->group.',jenisdiet_nama, sum(jml_kirim) AS jml_kirim';
            $modTarif = LaporanjmlporsigiziruanganV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jml_kirim;
            }
             return $totTarif;

            // if(isset($_GET['GZLaporanJumlahPorsiV'])){
            //     $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
            //     $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
            //     $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
            // }
            // $criteria->select = $criteria->group.', sum(jml_kirim) AS jml_kirim';
            // $modTarif = LaporanjmlporsigiziruanganV::model()->findAll($criteria);
            // $totTarif = 0;
            // foreach($modTarif as $key=>$tarif){
            //     $totTarif += $tarif->jml_kirim;
            // }
            //  return $totTarif;

    }        

     public function getSumTotal($idjenisdiet){

         if (isset($_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'])){
            $komponentarif_id = $_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'];
            
        }
        $format = new CustomFormat();
        $criteria = new CDbCriteria();
        $criteria->group = 'jenisdiet_id';
        $criteria->compare('jenisdiet_id',$idjenisdiet);




            if(isset($_GET['GZLaporanJumlahPorsiV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
                $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
            }
            $criteria->select = $criteria->group.', sum(jml_kirim) AS jml_kirim';
            $modTarif = LaporanjmlporsigiziruanganV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jml_kirim;
            }
             return $totTarif;
     }

     public function getSumTotalT($idjenisdiet){

         $format = new CustomFormat();
         $criteria = new CDbCriteria();
         $criteria->group = 'kelaspelayanan_id,ruangan_id,jenisdiet_nama';
            foreach($groups AS $i => $group){
            if($group == 'jenisdiet'){
                $criteria->group .= ', kelaspelayanan_id,ruangan_id';
                $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
                $criteria->compare('ruangan_id',$this->ruangan_id);
                // $criteria->compare('ruangan_id',$this->ruangan_id);
            }
        }

        if(isset($_GET['GZLaporanJumlahPorsiV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
                $jenisdiet_nama =$_GET['GZLaporanJumlahPorsiV']['jenisdiet_nama'];
                $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
                $criteria->compare('LOWER(jenisdiet_nama)',strtolower($jenisdiet_nama));
                $criteria->addInCondition('jenisdiet_id',$this->jenisdiet_id);
            }
            $criteria->select = $criteria->group.',jenisdiet_nama, sum(jml_kirim) AS jml_kirim';
            $modTarif = LaporanjmlporsigiziruanganV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jml_kirim;
            }
             return $totTarif;
     }

 public function getSumKelasP($groups=array(),$nama_kolom = null, $nama_ruangan = null){

        //  if (isset($_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'])){
        //     $komponentarif_id = $_GET['GZLaporanJumlahPorsiV']['kelaspelayanan_id'];
            
        // }
        $format = new CustomFormat();
        $criteria = new CDbCriteria();
        $criteria->group = 'kelaspelayanan_id,ruangan_id,jenisdiet_nama';
            foreach($groups AS $i => $group){
            if($group == 'jenisdiet'){
                $criteria->group .= ', kelaspelayanan_id,ruangan_id';
                $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
                $criteria->compare('ruangan_id',$this->ruangan_id);
                // $criteria->compare('ruangan_id',$this->ruangan_id);
            }
        }
        //$criteria->compare('jenisdiet_id',$idjenisdiet);

            if($nama_kolom == 'Vvip'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_VVIP);
            }else if($nama_kolom == 'Vip A'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_VIP_A);
            }else if($nama_kolom == 'Vip B'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_VIP_B);
            }else if($nama_kolom == 'Utama'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_UTAMA);
            }else if($nama_kolom == 'Madya A'){
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_MADYA_A);
                $criteria->compare('jenisdiet_id',$idjenisdiet);
            }else if($nama_kolom == 'Madya B'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_MADYA_B);
            }else if($nama_kolom == 'Pratama A'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_PRATAMA_A);
            }else if($nama_kolom == 'Pratama B'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_PRATAMA_B);
            }else if($nama_kolom == 'Pratama C'){
                $criteria->compare('jenisdiet_id',$idjenisdiet);
                $criteria->addCondition('kelaspelayanan_id = '.PARAMS::KELASPELAYANAN_ID_PRATAMA_C);
            }


            if($nama_ruangan == '2A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2A);
            }
            if($nama_ruangan == '2B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2B);
            }
            if($nama_ruangan == '2C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_2C);
            }
            if($nama_ruangan == '3A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3A);
            }
            if($nama_ruangan == '3B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3B);
            }
            if($nama_ruangan == '3C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_3C);
            }
            if($nama_ruangan == '4A'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4A);
            }
            if($nama_ruangan == '4B'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4B);
            }
            if($nama_ruangan == '4C'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_4C);
            }
            if($nama_ruangan == 'ICU'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_ICU);
            }
            if($nama_ruangan == 'OBSERVASI'){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_OBSERVASI);
            }
            if($nama_ruangan == 4042){
                $criteria->addCondition('ruangan_id = '.PARAMS::RUANGAN_ID_BANGSAL);
            }

            if(isset($_GET['GZLaporanJumlahPorsiV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
                $jenisdiet_nama =$_GET['GZLaporanJumlahPorsiV']['jenisdiet_nama'];
                $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
                $criteria->compare('LOWER(jenisdiet_nama)',strtolower($jenisdiet_nama));
                $criteria->addInCondition('jenisdiet_id',$this->jenisdiet_id);
            }
            $criteria->select = $criteria->group.',jenisdiet_nama, sum(jml_kirim) AS jml_kirim';
            $modTarif = LaporanjmlporsigiziruanganV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jml_kirim;
            }
             return $totTarif;
     }
    // if(isset($_GET['GZLaporanjmlporsikelasruanganV'])){
    // $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlporsikelasruanganV']['tglAwal']);
    // $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanjmlporsikelasruanganV']['tglAkhir']);
    // $jenisdiet_nama =$_GET['GZLaporanjmlporsikelasruanganV']['jenisdiet_nama'];
    // $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
    // $criteria->compare('LOWER(jenisdiet_nama)',strtolower($this->jenisdiet_nama));
    // }
    // $criteria->select = $criteria->group.', sum(jml_kirim) AS jml_kirim';
    // $modKirim = LaporanjmlporsikelasruanganV::model()->findAll($criteria);
    // $totKirim = 0;




    
    public function getNamaModel(){
        return __CLASS__;
    }

}