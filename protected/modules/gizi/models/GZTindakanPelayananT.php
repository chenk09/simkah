<?php

class GZTindakanPelayananT extends TindakanpelayananT {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
     public function searchDetailKonsul($pendaftaran_id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
//		$criteria->compare('tipepaket_id',$this->tipepaket_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
                // $criteria->condition = 'pendaftaran_id = '.$pendaftaran_id;
                // $criteria->condition = 'tipepaket_id = '.$this->tipepaket_id;
                $criteria->addCondition('pendaftaran_id = '.$pendaftaran_id);
//		$criteria->compare('carabayar_id',$this->carabayar_id);
//		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
//		$criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
//		$criteria->compare('tarif_tindakan',$this->tarif_tindakan);
//		$criteria->compare('LOWER(satuantindakan)',strtolower($this->satuantindakan),true);
//		$criteria->compare('LOWER(qty_tindakan)',strtolower($this->qty_tindakan),true);
//		$criteria->compare('cyto_tindakan',$this->cyto_tindakan);
//		$criteria->compare('tarifcyto_tindakan',$this->tarifcyto_tindakan);
//		$criteria->compare('LOWER(dokterpemeriksa1_id)',strtolower($this->dokterpemeriksa1_id),true);
//		$criteria->compare('LOWER(dokterpemeriksa2_id)',strtolower($this->dokterpemeriksa2_id),true);
//		$criteria->compare('LOWER(dokterpendamping_id)',strtolower($this->dokterpendamping_id),true);
//		$criteria->compare('LOWER(dokteranastesi_id)',strtolower($this->dokteranastesi_id),true);
//		$criteria->compare('LOWER(dokterdelegasi_id)',strtolower($this->dokterdelegasi_id),true);
//		$criteria->compare('LOWER(bidan_id)',strtolower($this->bidan_id),true);
//		$criteria->compare('LOWER(suster_id)',strtolower($this->suster_id),true);
//		$criteria->compare('LOWER(perawat_id)',strtolower($this->perawat_id),true);
//		$criteria->compare('kelastanggungan_id',$this->kelastanggungan_id);
//		$criteria->compare('discount_tindakan',$this->discount_tindakan);
//		$criteria->compare('subsidiasuransi_tindakan',$this->subsidiasuransi_tindakan);
//		$criteria->compare('subsidipemerintah_tindakan',$this->subsidipemerintah_tindakan);
//		$criteria->compare('subsisidirumahsakit_tindakan',$this->subsisidirumahsakit_tindakan);
//		$criteria->compare('iurbiaya_tindakan',$this->iurbiaya_tindakan);
//		$criteria->compare('LOWER(tm)',strtolower($this->tm),true);
//		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
//		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
//		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
//		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
//		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->addInCondition('ruangan_id', array(Params::RUANGAN_ID_GIZI));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    
}
