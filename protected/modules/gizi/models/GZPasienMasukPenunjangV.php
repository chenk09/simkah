<?php

class GZPasienMasukPenunjangV extends PasienmasukpenunjangV {

    public $pembayaranpelayanan_id;
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchKonsulGizi()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->select = 't.*,pendaftaran_t.*';
        $criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
        $criteria->compare('LOWER(t.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
        $criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
        $criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
        $criteria->compare('LOWER(t.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
        $criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);


        if(Yii::app()->user->getState('ruangan_id')==PARAMS::RUANGAN_ID_KASIR_SENTRAL){
            $criteria->compare('t.ruangan_id',PARAMS::RUANGAN_ID_GIZI);
        }else{
            $criteria->compare('t.ruangan_id',Yii::app()->user->getState('ruangan_id'));    
        }
        
        $criteria->addBetweenCondition('t.tglmasukpenunjang', $this->tglAwal, $this->tglAkhir);
//        $criteria->addCondition('pendaftaran_t.pembayaranpelayanan_id is null');
        $criteria->join = 'LEFT JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id';
        $criteria->order='t.tglmasukpenunjang DESC';

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
    
}
