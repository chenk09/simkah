<?php
class GZPengajuanbahanmkn extends PengajuanbahanmknT {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->addBetweenCondition('DATE(tglpengajuanbahan)', $this->tglAwal, $this->tglAkhir);
                $criteria->addCondition('terimabahanmakan_id is null');
		$criteria->compare('pengajuanbahanmkn_id',$this->pengajuanbahanmkn_id);
		$criteria->compare('terimabahanmakan_id',$this->terimabahanmakan_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('LOWER(nopengajuan)',strtolower($this->nopengajuan),true);
//		$criteria->compare('LOWER(tglpengajuanbahan)',strtolower($this->tglpengajuanbahan),true);
		$criteria->compare('LOWER(sumberdanabhn)',strtolower($this->sumberdanabhn),true);
		$criteria->compare('LOWER(alamatpengiriman)',strtolower($this->alamatpengiriman),true);
		$criteria->compare('idpegawai_mengetahui',$this->idpegawai_mengetahui);
		$criteria->compare('idpegawai_mengajukan',$this->idpegawai_mengajukan);
		$criteria->compare('LOWER(keterangan_bahan)',strtolower($this->keterangan_bahan),true);
		$criteria->compare('totalharganetto',$this->totalharganetto);
		$criteria->compare('LOWER(tglmintadikirim)',strtolower($this->tglmintadikirim),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->order='tglpengajuanbahan DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}