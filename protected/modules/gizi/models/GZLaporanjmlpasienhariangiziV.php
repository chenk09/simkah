<?php

class GZLaporanjmlpasienhariangiziV extends LaporanjmlpasienhariangiziV
{
        public $tglAwal, $tglAkhir, $pilihanTab, $jml_perhari;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanjmlpasienhariangiziV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
        
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->addBetweenCondition('tglkirimmenu',$this->tglAwal,$this->tglAkhir);
                if(is_array($this->jenisdiet_id)){
                    $criteria->addInCondition('jenisdiet_id', $this->jenisdiet_id);
                }else{
                    $criteria->compare('jenisdiet_id',$this->jenisdiet_id);
                }
                if(is_array($this->jeniswaktu_id)){
                    $criteria->addInCondition('jeniswaktu_id', $this->jeniswaktu_id);
                }else{
                    $criteria->compare('jeniswaktu_id',$this->jeniswaktu_id);
                }             
		return $criteria;
	}
        
	public function searchLaporan()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->criteriaSearch();
                $criteria->group = "jenisdiet_id, jenisdiet_nama, jeniswaktu_id, jeniswaktu_nama";
                $criteria->select = $criteria->group.", MIN(tglkirimmenu) AS tglkirimmenu, SUM(jml_kirim) AS jml_kirim";
                $criteria->order = "tglkirimmenu, jeniswaktu_id";
		return $criteria;
	}
        
	public function searchRekap()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->criteriaSearch();
                $criteria->group = "jenisdiet_id, jenisdiet_nama";
                $criteria->select = $criteria->group.", MIN(tglkirimmenu) AS tglkirimmenu, SUM(jml_kirim) AS jml_kirim";
		return $criteria;
	}
        
        public function getNamaModel(){
            return __CLASS__;
        }
        
        
}