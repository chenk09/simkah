<?php

class GZPendaftaranT extends PendaftaranT {

        public $adaKarcisKonsul = false;
        public $adaTindakanKonsul = false;
        public $isUpdatePasien = false;
        public $isPasienLama = false;
        
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchDialog()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
                $criteria->with=array('pasien','ruangan');
                $criteria->compare('LOWER(pasien.no_rekam_medik)',  strtolower($this->noRekamMedik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)',  strtolower($this->namaPasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)',  strtolower($this->namaBin),true);
                $criteria->compare('LOWER(pasien.alamat_pasien)',  strtolower($this->alamat),true);
                $criteria->compare('pasien.propinsi_id',  strtolower($this->propinsi));
                $criteria->compare('pasien.kabupaten_id',  strtolower($this->kabupaten));
                $criteria->compare('pasien.kecamatan_id',  strtolower($this->kecamatan));
                $criteria->compare('pasien.kelurahan_id',  strtolower($this->kelurahan));
                $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
                $criteria->compare('penjamin_id',$this->penjamin_id);
                $criteria->compare('caramasuk_id',$this->caramasuk_id);
                $criteria->compare('carabayar_id',$this->carabayar_id);
                $criteria->compare('pasien_id',$this->pasien_id);
                $criteria->compare('shift_id',$this->shift_id);
                $criteria->compare('golonganumur_id',$this->golonganumur_id);
                $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
                $criteria->compare('rujukan_id',$this->rujukan_id);
                $criteria->compare('penanggungjawab_id',$this->penanggungjawab_id);
                $criteria->compare('t.ruangan_id',$this->ruangan_id);
                $criteria->compare('instalasi_id',$this->instalasi_id);
                $criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
                $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
                $criteria->compare('no_urutantri',$this->no_urutantri);
                $criteria->compare('LOWER(transportasi)',strtolower($this->transportasi),true);
                $criteria->compare('LOWER(keadaanmasuk)',strtolower($this->keadaanmasuk),true);
                $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
                $criteria->compare('LOWER(statuspasien)',strtolower($this->statuspasien),true);
                $criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
                $criteria->compare('alihstatus',$this->alihstatus);
                $criteria->compare('byphone',$this->byphone);
                $criteria->compare('kunjunganrumah',$this->kunjunganrumah);
                $criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
                $criteria->compare('LOWER(umur)',strtolower($this->umur),true);
                $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
                $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
                $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
                $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
                $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->compare('nopendaftaran_aktif',$this->nopendaftaran_aktif);
                
                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
        
        

}
