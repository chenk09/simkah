<?php
class GZPesanmenudietT extends PesanmenudietT{
    public $instalasi_id;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function getNamaModel(){
        return __CLASS__;
    }
    
    public static function jenisPesan(){
        $result = array();
        foreach (Jenispesanmenu::items() as $i=>$value){
            if ($i != Params::JENISPESANMENU_PASIEN){
                $result[$i]=$value;
            }
        }
        return $result;
    }
    
    public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->addBetweenCondition('DATE(tglpesanmenu)', $this->tglAwal, $this->tglAkhir);
                $criteria->addCondition('kirimmenudiet_id is null');
		$criteria->compare('pesanmenudiet_id',$this->pesanmenudiet_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('kirimmenudiet_id',$this->kirimmenudiet_id);
		$criteria->compare('jenisdiet_id',$this->jenisdiet_id);
		$criteria->compare('bahandiet_id',$this->bahandiet_id);
		$criteria->compare('LOWER(jenispesanmenu)',strtolower($this->jenispesanmenu),true);
		$criteria->compare('LOWER(nopesanmenu)',strtolower($this->nopesanmenu),true);
		$criteria->compare('LOWER(tglpesanmenu)',strtolower($this->tglpesanmenu),true);
		$criteria->compare('LOWER(adaalergimakanan)',strtolower($this->adaalergimakanan),true);
		$criteria->compare('LOWER(keterangan_pesan)',strtolower($this->keterangan_pesan),true);
		$criteria->compare('LOWER(nama_pemesan)',strtolower($this->nama_pemesan),true);
		$criteria->compare('totalpesan_org',$this->totalpesan_org);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->order='tglpesanmenu DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}