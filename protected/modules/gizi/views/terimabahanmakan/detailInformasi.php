<table class='table'>
    <tr>
        <td>
             <b><?php echo CHtml::encode($modTerima->getAttributeLabel('nopenerimaanbahan')); ?>:</b>
            <?php echo CHtml::encode($modTerima->nopenerimaanbahan); ?>
            <br />
             <b><?php echo CHtml::encode($modTerima->getAttributeLabel('tglterimabahan')); ?>:</b>
            <?php echo CHtml::encode($modTerima->tglterimabahan); ?>

             
        </td>
        <td>
             <b><?php echo CHtml::encode($modTerima->getAttributeLabel('ruangan_id')); ?>:</b>
            <?php echo CHtml::encode($modTerima->ruangan->ruangan_nama); ?>
            <br />
             <b><?php echo CHtml::encode($modTerima->getAttributeLabel('create_time')); ?>:</b>
            <?php echo CHtml::encode($modTerima->create_time); ?>

        </td>
    </tr>   
</table>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
        <th>No.Urut</th>
        <th>Golongan</th>
        <th>Jenis</th>
        <th>Kelompok</th>
        <th>Nama</th>
        <th>Jumlah Persediaan</th>
        <th>Satuan</th>
        <th>Harga Netto</th>
        <th>Harga Jual</th>
        <th>Tgl Kadaluarsa</th>
        <th>Qty</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $no=1;
        foreach($modDetailTerima AS $tampilData):
            $subTotal = $tampilData->qty_terima*$tampilData->harganettobhn;
    echo "<tr>
            <td>".$tampilData->nourutbahan."</td>
            <td>".$tampilData->golbahanmakanan->golbahanmakanan_nama."</td>  
            <td>".$tampilData->bahanmakanan->jenisbahanmakanan."</td>   
            <td>".$tampilData->bahanmakanan->kelbahanmakanan."</td>   
            <td>".$tampilData->bahanmakanan->namabahanmakanan."</td>   
            <td>".$tampilData->bahanmakanan->jmlpersediaan."</td>   
            <td>".$tampilData->satuanbahan."</td>   
            <td>".$tampilData->harganettobhn."</td>   
            <td>".$tampilData->bahanmakanan->hargajualbahan."</td>   
            <td>".$tampilData->bahanmakanan->tglkadaluarsabahan."</td>   
            <td>".$tampilData->qty_terima."</td>   
            <td>".$subTotal."</td>     
            
                      
         </tr>";  
        $no++;
       
        $totalSubTotal=$totalSubTotal+$subTotal;
        
        endforeach;
     
    ?>
    </tbody>
    <tfoot>
        <?php
        echo "<tr>
            <td colspan='11' style='text-align:right;'> Total Harga Netto</td>
           
            <td>".$totalSubTotal."</td>
         </tr>";   
        ?>
    </tfoot>
</table>