<?php
    $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'search',
            'type'=>'horizontal',
            'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

    )); 
?>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <table  class="table-condensed">
        <tr>
            <td>
                 <?php echo $form->textFieldRow($modKomposisiBahanMakanan,'namabahanmakanan',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php //echo $form->textFieldRow($modKomposisiBahanMakanan,'zatgizi_nama',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
               
            </td>
            
        </tr>
    </table>
    
    
<div class="form-actions">
    <?php 
            echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                        array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));

            echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); 
    ?>
    <?php 
            $content = $this->renderPartial('../tips/informasi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  
    ?>

</div>
</fieldset>  
<?php $this->endWidget();?>
</fieldset>
