<fieldset>
    <legend class="rim">Retur Menu Diet Gizi</legend>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
    <?php
        $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
            'id' => 'returmenudiet-form',
            'enableAjaxValidation' => false,
            'type' => 'horizontal',
            'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
            'focus' => '#',
        ));
    ?>
<?php
    if(!empty($_GET['id'])){
?>
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
<?php }else{ } ?>
    <p class="help-block"><?php echo Yii::t('mds', 'Fields with <span class="required">*</span> are required.') ?></p>
    <?php echo $form->errorSummary($modKirim); ?>
    
    <div>
        <table>
            <tr>
                <td>
                    <div class="control-group ">
                        <?php echo CHtml::activeLabel($modKirim, 'jenispesanmenu', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php
                            echo CHtml::activeTextField($modKirim, 'jenispesanmenu', array('readonly'=>true))
                            ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo CHtml::activeLabel($modKirim, 'nokirimmenu', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php
                            echo CHtml::activeTextField($modKirim, 'nokirimmenu', array('readonly'=>true))
                            ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo CHtml::activeLabel($modKirim, 'tglkirimmenu', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php
                            echo CHtml::activeTextField($modKirim, 'tglkirimmenu', array('readonly'=>true))
                            ?>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="control-group ">
                        <?php if($modKirim->jenispesanmenu == 'Pasien'){ ?>
                        <?php echo CHtml::Label('Nama Pasien','', array('class'=>'control-label')) ?>
                        <?php }else{ ?>
                        <?php echo CHtml::Label('Nama Tamu / Pegawai','', array('class'=>'control-label')) ?>
                        <?php } ?>
                        <div class="controls">
                            <?php if($modKirim->jenispesanmenu == 'Pasien'){ 
                                    $nama = $modKirim->kirimmenupasien->pasien->nama_pasien;                                
                                  }else{
                                      $nama = $modKirim->kirimmenupegawai->pegawai->nama_pegawai;
                                  }
                            ?>
                            <?php
                                echo CHtml::TextField('nama',$nama, array('readonly'=>true))
                            ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo CHtml::Label('Ruangan', '', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php if($modKirim->jenispesanmenu == 'Pasien'){ 
                                    $ruangan = $modKirim->kirimmenupasien->ruangan->ruangan_nama;                                 
                                  }else{
                                     $ruangan = $modKirim->kirimmenupegawai->ruangan->ruangan_nama;  
                                  }
                            ?>
                            <?php
                            echo CHtml::TextField('ruangan', $ruangan, array('readonly'=>true))
                            ?>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    
    <div>
        <legend class="rim">Daftar Menu Diet</legend>
        <?php $this->renderPartial('_tblReturMenu',array('modDetailKirim'=>$modDetailKirim,'modKirim'=>$modKirim)); ?>
    </div>
    <div class="form-actions">
        <?php
                if(!empty($_GET['id'])){
                    echo CHtml::htmlButton(Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 
                                'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)','disabled'=>true));
        
                }else{
                 echo CHtml::htmlButton(Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 
                                'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)'));
                }
       ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                    array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
	<?php 
                $content = $this->renderPartial('../tips/transaksi',array(),true);
                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
    </div>


<?php $this->endWidget(); ?>
</fieldset>
