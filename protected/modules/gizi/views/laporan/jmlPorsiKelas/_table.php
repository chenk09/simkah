<?php 
    $table = 'ext.bootstrap.widgets.MergeHeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchTable();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php 
        $this->widget($table,array(
            'id'=>'tableLaporan',
            'dataProvider'=>$data,
            'template'=>$template,
            'enableSorting'=>$sort,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header' => 'NO',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    ),
                    array(
                        'header' => 'JENIS DIIT',
                        'value' => '$data->jenisdiet_nama',
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('colspan'=>2,'style'=>'text-align:right;color:black;font-weight:bold'),
                        'footer'=>'JUMLAH',
                    ),
                    array(
                        'header' => 'BANGSAL',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"BANGSAL"))',
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"BANGSAL")),
                    ),
                    array(
                        'header' => 'OBS',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"OBS"))',
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"OBS")),
                    ),
                    array(
                        'header' => 'ICU',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"ICU"))',
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"ICU")),
                    ),
                    array(
                        'header' => 'VVIP',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"VVIP"))',
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"VVIP")),
                    ),
                    array(
                        'header' => 'VIP A',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"VIPA"))',
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"VIPA")),
                    ),
                    array(
                        'header' => 'VIP B',
                        'name'=>'hargasatuan',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"VIPB"))',
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"VIPB")),
                    ),
                    array(
                        'header' => 'UTAMA',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"UTAMA"))',
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"UTAMA")),
                    ),
                    array(
                        'header' => 'MADYA',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"MADYA"))',
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"MADYA")),
                    ),
                    array(
                        'header' => 'I',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"I"))',
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"I")),
                    ),
                    array(
                        'header' => 'II',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"II"))',
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"II")),
                    ),
                    array(
                        'header' => 'III',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"III"))',
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("jenisdiet"),"III")),
                    ),
                    array(
                        'header' => 'JML',
                        'value'=>'MyFunction::formatNumber($data->getSumJmlPorsi(array("jenisdiet"),"JML"))',
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>MyFunction::formatNumber($model->getSumTotalPorsi(array("kelas"),"TOTAL")),
                    ),
                    array(
                        'header' => '%',
                        'value' => '"-"',
                        'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                        'htmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),                    
                        'footerHtmlOptions'=>array('style'=>'text-align:center;color:black;font-weight:bold'),
                        'footer'=>'-',
                    ),
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); 
?>