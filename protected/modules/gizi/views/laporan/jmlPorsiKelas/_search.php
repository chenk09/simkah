<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
<style>

    #penjamin label.checkbox{
        width: 100px;
        display:inline-block;
    }
    label.checkbox, label.radio{
        width:150px;
        display:inline-block;
    }

</style>
<legend class="rim">Berdasarkan Pencarian : </legend> 
	<table>
            <tr>
            <td>
                <div class="control-group">
                    <div class="control-label"> Tanggal Masuk Penunjang </div>
                    <div class="controls">
                        <?php echo $form->dropDownList($model,'bulan',
                            array(
                                'hari'=>'Hari Ini',
                                'bulan'=>'Bulan',
                                'tahun'=>'Tahun',
                            ),
                            array(
                                'empty'=>'--Pilih--',
                                'id'=>'PeriodeName',
                                'onChange'=>'setPeriode()',
                                'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;',
                            )
                            );
                        ?>
                    </div>
                </div>
            </td>
            <td width="250px">
                <?php echo CHtml::hiddenField('type', ''); ?>
                <?php
                $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglAwal',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </td>
            <td width="50px">s/d</td>
            <td>
                <?php
                    $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglAkhir',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3','onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php /**
                Diubah oleh : Jembar Hardian
                tgl         : 17 April 2014
                tujuan      : Mengubah text field menjadi combobox berdasarkan abel jenisdiet_m dimana status jenisdiet_aktif = 'true'
                */
                ?>
               <!--  <div class="control-group">
                    <div class="control-label">
                        Jenis Diet
                    </div>
                    <div class="controls"> -->
                        <?php//echo $form->textField($model,'jenisdiet_nama',array('style'=>'width:140px;'));?>
<?php echo $form->DropDownListRow($model, 'jenisdiet_nama', CHtml::listData($model->getJenisdietItems(),'jenisdiet_id','jenisdiet_nama'),array('empty'=>'-- Pilih --')); ?> 

  <!--                   </div>                    
                </div> -->
            </td>
        </tr>
  </table> 
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', 
                array('{icon}' => '<i class="icon-ok icon-white"></i>')),array('class' => 'btn btn-primary', 
                    'type' => 'submit', 'id' => 'btn_simpan'));?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',
                    array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger',
                        'onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));?> 
    </div>
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
?>

<?php Yii::app()->clientScript->registerScript('cekAll','
  $("#big").find("input").attr("checked", "checked");
  $("#kelasPelayanan").find("input").attr("checked", "checked");
',  CClientScript::POS_READY);
?>
<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT

function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
    
        $.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
            $('#GZLaporanjmlporsikelasruanganV_tglAwal').val(data.periodeawal);
            $('#GZLaporanjmlporsikelasruanganV_tglAkhir').val(data.periodeakhir);
        },'json');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
    function checkAllKelas(){
        if($('#pilihSemua').is(':checked')){
            $('#checkBoxList').each(function(){
                $(this).find('input').attr('checked',true);
            });
        }else{
            $('#checkBoxList').each(function(){
                $(this).find('input').removeAttr('checked');
            });
        }
    }
    checkAllKelas();
</script>