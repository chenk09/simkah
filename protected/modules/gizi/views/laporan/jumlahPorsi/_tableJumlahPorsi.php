<?php 
//     $rim = 'max-width:1300px;overflow-x:scroll;';
// $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
//     $sort = true;
//     if (isset($caraPrint)){
//         $data = $model->searchPrint();
//         $template = "{items}";
//         $sort = false;
//         if ($caraPrint == "EXCEL")
//             $table = 'ext.bootstrap.widgets.BootExcelGridView';
            
//     } else{
//         $data = $model->searchTable();
//          $template = "{pager}{summary}\n{items}";
//     }
?>

<?php
$rim = 'max-width:1300px;overflow-x:scroll;';
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$data = $model->search();
$template = "{pager}{summary}\n{items}";
$sort = true;
    if (isset($caraPrint)){
        $sort = false;
        $data = $model->searchPrint();
        $rim = '';
        $template = "{items}";
    if ($caraPrint == "EXCEL")
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
            $data = $model->searchTable();
            $template = "{pager}{summary}\n{items}";
        }
?>

<!-- <div id="div_rekap"> -->
<div style="<?php echo $rim; ?>">

<!-- <div style='max-width:1320px;overflow-x:scroll;'> -->
<?php $this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>2A</center>',
                'start'=>2, 
                'end'=>11, 
            ),
            array(
                'name'=>'<center>2B</center>',
                'start'=>12, 
                'end'=>21, 
            ),
             array(
                'name'=>'<center>2C</center>',
                'start'=>22, 
                'end'=>31, 
            ),
             array(
                'name'=>'<center>3A</center>',
                'start'=>32, 
                'end'=>41, 
            ),
             array(
                'name'=>'<center>3B</center>',
                'start'=>42, 
                'end'=>51, 
            ),
             array(
                'name'=>'<center>3C</center>',
                'start'=>52, 
                'end'=>61, 
            ),
             array(
                'name'=>'<center>4A</center>',
                'start'=>62, 
                'end'=>71, 
            ),
             array(
                'name'=>'<center>4B</center>',
                'start'=>72, 
                'end'=>81, 
            ),
             array(
                'name'=>'<center>4C</center>',
                'start'=>82, 
                'end'=>91, 
            ),

        ),
        //'mergeColumns'=>array('nama_pasien','no_rekam_medik'),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(

                //RUANGAN 2A
                array(
                    'header' => 'No',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                ),
                array(
                    'header'=>'Jenis DIIT',
                    'value'=> '$data->jenisdiet_nama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footerHtmlOptions'=>array('colspan'=>2,'style'=>'text-align:right;'),
                    'footer'=>'JUMLAH',
                ),
                array(
                    'header'=>'VVIP',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vvip",$data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    // 'footer'=>MyFunction::formatNumber($data->getSumKelasP("Vvip","2A")),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","2A")),
                ),
                array(
                    'header'=>'VIP A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip A",$data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip A","2A")),
                ),
                array(
                    'header'=>'VIP B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip B",$data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip B","2A")),
                ),
                array(
                    'header'=>'Utama',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Utama",$data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Utama","2A")),

                ),
                array(
                    'header'=>'Madya A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya A",$data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya A","2A")),
                ),
                array(
                    'header'=>'Madya B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya B",$data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya B","2A")),
                ),
                array(
                    'header'=>'Pratama A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama A",$data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama A","2A")),
                ),
                array(
                    'header'=>'Pratama B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama B",$data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama B","2A")),
                ),
                array(
                    'header'=>'Pratama C',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama C",$data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama C","2A")),
                ),
                array(
                    'header'=>'JML',
                    'value'=> 'MyFunction::formatNumber($data->getSumJml($data->jenisdiet_id,"2A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumJmlT("2A")),
                ),


                //RUANGAN 2B

                array(
                    'header'=>'VVIP',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vvip",$data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","2B")),
                ),
                array(
                    'header'=>'VIP A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip A",$data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip A","2B")),
                ),
                array(
                    'header'=>'VIP B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip B",$data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip B","2B")),
                ),
                array(
                    'header'=>'Utama',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Utama",$data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Utama","2B")),
                ),
                array(
                    'header'=>'Madya A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya A",$data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya A","2B")),
                ),
                array(
                    'header'=>'Madya B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya B",$data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya B","2B")),
                ),
                array(
                    'header'=>'Pratama A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama A",$data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama A","2B")),
                ),
                array(
                    'header'=>'Pratama B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama B",$data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama B","2B")),
                ),
                array(
                    'header'=>'Pratama C',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama C",$data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama C","2B")),
                ),
                array(
                    'header'=>'JML',
                    'value'=> 'MyFunction::formatNumber($data->getSumJml($data->jenisdiet_id,"2B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumJmlT("2B")),
                ),


                //RUANGAN 2C 

                array(
                    'header'=>'VVIP',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vvip",$data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","2C")),
                ),
                array(
                    'header'=>'VIP A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip A",$data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip A","2C")),
                ),
                array(
                    'header'=>'VIP B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip B",$data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip B","2C")),
                ),
                array(
                    'header'=>'Utama',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Utama",$data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Utama","2C")),
                ),
                array(
                    'header'=>'Madya A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya A",$data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya A","2C")),
                ),
                array(
                    'header'=>'Madya B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya B",$data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya B","2C")),
                ),
                array(
                    'header'=>'Pratama A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama A",$data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","2C")),
                ),
                array(
                    'header'=>'Pratama B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama B",$data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama B","2C")),
                ),
                array(
                    'header'=>'Pratama C',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama C",$data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama C","2C")),
                ),
                array(
                    'header'=>'JML',
                    'value'=> 'MyFunction::formatNumber($data->getSumJml($data->jenisdiet_id,"2C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumJmlT("2C")),
                ),


                //RUANGAN 3A

                array(
                    'header'=>'VVIP',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vvip",$data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","3A")),
                ),
                array(
                    'header'=>'VIP A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip A",$data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip A","3A")),
                ),
                array(
                    'header'=>'VIP B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip B",$data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip B","3A")),
                ),
                array(
                    'header'=>'Utama',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Utama",$data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Utama","3A")),
                ),
                array(
                    'header'=>'Madya A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya A",$data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya A","3A")),
                ),
                array(
                    'header'=>'Madya B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya B",$data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya B","3A")),
                ),
                array(
                    'header'=>'Pratama A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama A",$data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama A","3A")),
                ),
                array(
                    'header'=>'Pratama B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama B",$data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama B","3A")),
                ),
                array(
                    'header'=>'Pratama C',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama C",$data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama C","3A")),
                ),
                array(
                    'header'=>'JML',
                    'value'=> 'MyFunction::formatNumber($data->getSumJml($data->jenisdiet_id,"3A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumJmlT("3A")),
                ),


                //RUANGAN 3B

                array(
                    'header'=>'VVIP',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vvip",$data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","3B")),
                ),
                array(
                    'header'=>'VIP A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip A",$data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","3B")),
                ),
                array(
                    'header'=>'VIP B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip B",$data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip B","3B")),
                ),
                array(
                    'header'=>'Utama',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Utama",$data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Utama","3B")),
                ),
                array(
                    'header'=>'Madya A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya A",$data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya A","3B")),
                ),
                array(
                    'header'=>'Madya B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya B",$data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya B","3B")),
                ),
                array(
                    'header'=>'Pratama A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama A",$data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","3B")),
                ),
                array(
                    'header'=>'Pratama B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama B",$data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama B","3B")),
                ),
                array(
                    'header'=>'Pratama C',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama C",$data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama C","3B")),
                ),
                array(
                    'header'=>'JML',
                    'value'=> 'MyFunction::formatNumber($data->getSumJml($data->jenisdiet_id,"3B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumJmlT("3B")),
                ),


                //RUANGAN 3C

                array(
                    'header'=>'VVIP',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vvip",$data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","3C")),
                ),
                array(
                    'header'=>'VIP A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip A",$data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip A","3C")),
                ),
                array(
                    'header'=>'VIP B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip B",$data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip B","3C")),
                ),
                array(
                    'header'=>'Utama',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Utama",$data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Utama","3C")),
                ),
                array(
                    'header'=>'Madya A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya A",$data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya A","3C")),
                ),
                array(
                    'header'=>'Madya B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya B",$data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya B","3C")),
                ),
                array(
                    'header'=>'Pratama A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama A",$data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama A","3C")),
                ),
                array(
                    'header'=>'Pratama B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama B",$data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama B","3C")),
                ),
                array(
                    'header'=>'Pratama C',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama C",$data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama C","3C")),
                ),
                array(
                    'header'=>'JML',
                    'value'=> 'MyFunction::formatNumber($data->getSumJml($data->jenisdiet_id,"3C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumJmlT("3C")),
                ),


                //RUANGAN 4A

                array(
                    'header'=>'VVIP',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vvip",$data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","4A")),
                ),
                array(
                    'header'=>'VIP A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip A",$data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","4A")),
                ),
                array(
                    'header'=>'VIP B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip B",$data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip B","4A")),
                ),
                array(
                    'header'=>'Utama',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Utama",$data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Utama","4A")),
                ),
                array(
                    'header'=>'Madya A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya A",$data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya A","4A")),
                ),
                array(
                    'header'=>'Madya B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya B",$data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","4A")),
                ),
                array(
                    'header'=>'Pratama A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama A",$data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya B","4A")),
                ),
                array(
                    'header'=>'Pratama B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama B",$data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","4A")),
                ),
                array(
                    'header'=>'Pratama C',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama C",$data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama C","4A")),
                ),
                array(
                    'header'=>'JML',
                    'value'=> 'MyFunction::formatNumber($data->getSumJml($data->jenisdiet_id,"4A"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumJmlT("4A")),
                ),


                //RUANGAN 4B

                array(
                    'header'=>'VVIP',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vvip",$data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","4B")),
                ),
                array(
                    'header'=>'VIP A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip A",$data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip A","4B")),
                ),
                array(
                    'header'=>'VIP B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip B",$data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip B","4B")),
                ),
                array(
                    'header'=>'Utama',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Utama",$data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Utama","4B")),
                ),
                array(
                    'header'=>'Madya A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya A",$data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya A","4B")),
                ),
                array(
                    'header'=>'Madya B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya B",$data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya B","4B")),
                ),
                array(
                    'header'=>'Pratama A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama A",$data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama A","4B")),
                ),
                array(
                    'header'=>'Pratama B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama B",$data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama B","4B")),
                ),
                array(
                    'header'=>'Pratama C',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama C",$data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama C","4B")),
                ),
                array(
                    'header'=>'JML',
                    'value'=> 'MyFunction::formatNumber($data->getSumJml($data->jenisdiet_id,"4B"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumJmlT("4B")),
                ),


                //RUANGAN 4C

                array(
                    'header'=>'VVIP',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vvip",$data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vvip","4C")),
                ),
                array(
                    'header'=>'VIP A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip A",$data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip A","4C")),
                ),
                array(
                    'header'=>'VIP B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Vip B",$data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Vip B","4C")),
                ),
                array(
                    'header'=>'Utama',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Utama",$data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Utama","4C")),
                ),
                array(
                    'header'=>'Madya A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya A",$data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya A","4C")),
                ),
                array(
                    'header'=>'Madya B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Madya B",$data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Madya B","4C")),
                ),
                array(
                    'header'=>'Pratama A',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama A",$data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama A","4C")),
                ),
                array(
                    'header'=>'Pratama B',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama B",$data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama B","4C")),
                ),
                array(
                    'header'=>'Pratama C',
                    'value'=> 'MyFunction::formatNumber($data->getSumKelas("Pratama C",$data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"Pratama C","4C")),
                ),
                
                array(
                    'header'=>'JML',
                    'value'=> 'MyFunction::formatNumber($data->getSumJml($data->jenisdiet_id,"4C"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumJmlT("4C")),
                ),




                //RUANGAN ICU

                 array(
                    'header'=>'ICU',
                    'value'=> 'MyFunction::formatNumber($data->getSumRuangan($data->jenisdiet_id,"ICU"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"","ICU")),
                ),


                 //RUANGAN OBSERVASI

                 array(
                    'header'=>'OBS',
                    'value'=> 'MyFunction::formatNumber($data->getSumRuangan($data->jenisdiet_id,"OBSERVASI"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"","OBSERVASI")),
                ),


                 //RUANGAN BANGSAL

                 array(
                    'header'=>'BANGSAL',
                    'value'=> 'MyFunction::formatNumber($data->getSumRuangan($data->jenisdiet_id,"4042"))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumKelasP(array("jenisdiet"),"","4042")),
                ),
                 array(
                    'header'=>'JML TOTAL',
                    'value'=> 'MyFunction::formatNumber($data->getSumTotal($data->jenisdiet_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                    'footer'=>MyFunction::formatNumber($model->getSumTotalT()),
                    ),

                
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
</div>