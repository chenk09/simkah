<?php
//Yii::app()->clientScript->registerScript('search', "
//$('.search-form form').submit(function(){
//    $.fn.yiiGridView.update('tableLaporanJmlPasienHarian', {
//        data: $(this).serialize()
//    });
//    $.fn.yiiGridView.update('tableRekapJmlPasienHarian', {
//        data: $(this).serialize()
//    });
//    return false;
//});
//");
?>
<legend class="rim2">Laporan Jumlah Pasien Harian</legend>
<div class="search-form">
    <?php
        $this->renderPartial('gizi.views.laporan.jumlahPasienHarian/_searchJmlPasienHarian',
            array(
                'model'=>$model,
            )
        ); 
    ?>
</div>
<div class="tab">
    <?php
        $this->widget('bootstrap.widgets.BootMenu',array(
            'type'=>'tabs',
            'stacked'=>false,
            'htmlOptions'=>array('id'=>'tabmenu'),
            'items'=>array(
                array('label'=>'Laporan Jumlah Harian','url'=>'javascript:tab(0);', 'itemOptions'=>array("index"=>1),'active'=>true),
                array('label'=>'Rekap Jumlah','url'=>'javascript:tab(1);', 'itemOptions'=>array("index"=>1)),
            ),
        ))
    ?>
    <div id="tables">
                <?php
                    $this->renderPartial('gizi.views.laporan.jumlahPasienHarian/_tables',
                        array(
                            'model'=>$model,
                            'models'=>$models,
                            'modRekaps'=>$modRekaps,
                        )
                    );
                ?>
    </div>
</div>

<div style="float:left;margin-right:6px;">
    <?php
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanKeseluruhan');

        $this->widget('bootstrap.widgets.BootButtonGroup', array(
            'type'=>'primary',
            'buttons'=>array(
                array('label'=>'Print', 'icon'=>'icon-print icon-white', 'url'=>$urlPrint, 'htmlOptions'=>array('onclick'=>'print(\'PRINT\');return false;')),
                array('label'=>'',
                    'items'=>array(
                        array('label'=>'PDF', 'icon'=>'icon-book', 'url'=>$urlPrint, 'itemOptions'=>array('onclick'=>'print(\'PDF\');return false;')),
                        array('label'=>'Excel','icon'=>'icon-pdf', 'url'=>$urlPrint, 'itemOptions'=>array('onclick'=>'print(\'EXCEL\');return false;')),
                    )
                ),
            ),
        ));              
     ?>
</div>      

<script type="text/javascript">
    function checkAll()
    {
        if($("#checkAllRuangan").is(':checked')){
            $("#ruangan").find("input[type=\'checkbox\']").attr("checked", "checked");
        }else{
            $("#ruangan").find("input[type=\'checkbox\']").attr("checked", false);
        }        
        
    }
</script>
<?php

$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintLaporanJumlahPasienHarian');

$js = <<< JSCRIPT
$(document).ready(function() {
    $("#tabmenu").children("li").children("a").click(function() {
        $("#tabmenu").children("li").attr('class','');
        $(this).parents("li").attr('class','active');
        $(".icon-pencil").remove();
        $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
    });

    $("#div_reportJmlPasienHarian").show();
    $("#div_rekapJmlPasienHarian").hide();
});

function tab(index){
    $(this).hide();
    if (index==0){
        $("#GZLaporanjmlpasienhariangiziV_pilihan_tab").val("report");
        $("#div_reportJmlPasienHarian").show();
        $("#div_rekapJmlPasienHarian").hide();
    }else if(index==1){
        $("#GZLaporanjmlpasienhariangiziV_pilihan_tab").val("rekap");
        $("#div_reportJmlPasienHarian").hide();
        $("#div_rekapJmlPasienHarian").show();
    }
}
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#searchLaporan').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
?>