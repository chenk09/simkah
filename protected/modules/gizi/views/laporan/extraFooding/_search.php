<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
<style>

    #penjamin label.checkbox{
        width: 100px;
        display:inline-block;
    }
    label.checkbox, label.radio{
        width:150px;
        display:inline-block;
    }

</style>
<legend class="rim">Berdasarkan Pencarian : </legend> 
	<table>
            <tr>
            <td>
                <div class="control-group">
                    <div class="control-label"> Tanggal Masuk Penunjang </div>
                    <div class="controls">
                        <?php echo $form->dropDownList($model,'bulan',
                            array(
                                'hari'=>'Hari Ini',
                                'bulan'=>'Bulan',
                                'tahun'=>'Tahun',
                            ),
                            array(
                                'empty'=>'--Pilih--',
                                'id'=>'PeriodeName',
                                'onChange'=>'setPeriode()',
                                'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;',
                            )
                            );
                        ?>
                    </div>
                </div>
            </td>
            <td width="250px">
                <?php echo CHtml::hiddenField('type', ''); ?>
                <?php
                $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglAwal',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </td>
            <td width="50px">s/d</td>
            <td>
                <?php
                    $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglAkhir',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3','onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </td>
        </tr>
  </table> 
  <div class="control-group">
        <?php echo CHtml::label('Kelas Pelayanan', 'kelasPelayanan', array('class'=>'control-label')); ?> 
        <div class="controls">
            <?php echo CHtml::checkBox('pilihSemua', true, array('onclick'=>'checkAllKelas();')) ?> <label><b>Pilih Semua</b></label>
            <div id="checkBoxList">
                <?php echo $form->checkBoxList($model,'kelaspelayanan_id', CHtml::listData(KelaspelayananM::model()->items, 'kelaspelayanan_id', 'kelaspelayanan_nama'), array('class'=>'kelasPelayanan')); ?><br>
            </div>                
        </div>
 </div>
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', 
                array('{icon}' => '<i class="icon-ok icon-white"></i>')),array('class' => 'btn btn-primary', 
                    'type' => 'submit', 'id' => 'btn_simpan','onclick'=>'CekCaraBayar();return false;'));?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',
                    array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger',
                        'onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));?> 
    </div>
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
?>

<?php Yii::app()->clientScript->registerScript('cekAll','
  $("#big").find("input").attr("checked", "checked");
  $("#kelasPelayanan").find("input").attr("checked", "checked");
',  CClientScript::POS_READY);
?>
<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT

function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
    
        $.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
            $('#GZLaporanextrafoodinggiziV_tglAwal').val(data.periodeawal);
            $('#GZLaporanextrafoodinggiziV_tglAkhir').val(data.periodeakhir);
        },'json');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
    function checkAllKelas(){
        if($('#pilihSemua').is(':checked')){
            $('#checkBoxList').each(function(){
                $(this).find('input').attr('checked',true);
            });
        }else{
            $('#checkBoxList').each(function(){
                $(this).find('input').removeAttr('checked');
            });
        }
    }
    checkAllKelas();
</script>