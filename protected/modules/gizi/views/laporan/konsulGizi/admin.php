<legend class="rim2">Laporan Konsultasi Gizi</legend> 
<?php
$url = Yii::app()->createUrl('gizi/laporan/frameGrafikLaporanKonsulGizi&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#Grafik').attr('src','').css('height','0px');
    $.fn.yiiGridView.update('tableLaporan', {
            data: $(this).serialize()
    });
    return false;
});
");
?>
<div class="search-form">
<?php 
    $this->renderPartial('konsulGizi/_search',array(
        'model'=>$model,
    )); ?>
</div>
<legend class="rim">Tabel Konsultasi Gizi</legend> 
<fieldset> 
    <?php $this->renderPartial('konsulGizi/_table', array('model'=>$model)); ?>
    <?php //$this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
    </iframe>        
</fieldset>
<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanKonsulGizi');
    $this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>