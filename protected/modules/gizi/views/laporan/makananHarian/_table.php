<?php 
    $table = 'ext.bootstrap.widgets.MergeHeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchTable();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php $this->widget($table,array(
	'id'=>'tableLaporan',
	'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header' => 'No',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                ),
                array(
                    'header' => 'No. RM',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->no_rekam_medik',
                ),
                array(
                    'header' => 'Nama Lengkap',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->nama_pasien',
                ),
                array(
                    'header' => 'No. Pendaftaran',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->no_pendaftaran',
                ),
                array(
                    'header' => 'Jenis',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->jenisdiet_nama'
                ),
                array(
                    'header' => 'Jenis Diet',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->menudiet_nama'
                ),
//                array(
//                    'header' => 'No. Gizi',
//                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
//                    'value' => '$data->no_masukpenunjang',
//                ),
                array(
                    'header' => 'Jml',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->jml_kirim',
                    'footerHtmlOptions'=>array('colspan'=>7,'style'=>'text-align:right;font-weight:bold'),
                    'footer'=>'Total',
                ),
                array(
                    'header' => 'Harga',
                    'name'=>'hargasatuan',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->hargasatuan',
                    'footerHtmlOptions'=>array('style'=>'text-align:right;font-weight:bold'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footer'=>'sum(hargasatuan)',
                ),
                array(
                    'header' => 'Ruangan',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->ruangan_nama',
                    'footerHtmlOptions'=>array('style'=>'text-align:right;color:white'),
                    'footer'=>'-',
                ),
                array(
                    'header' => 'Kelas',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->kelaspelayanan_nama',
                    'footerHtmlOptions'=>array('style'=>'text-align:right;color:white'),
                    'footer'=>'-',
                ),
                array(
                    'header' => 'Tgl. Transaksi',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->tglkirimmenu',
                    'footerHtmlOptions'=>array('style'=>'text-align:right;color:white'),
                    'footer'=>'-',
                ),
                array(
                    'header' => 'Tgl. Pemberian',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'value' => '$data->tglkirimmenu',
                    'footerHtmlOptions'=>array('style'=>'text-align:right;color:white'),
                    'footer'=>'-',
                ),
                array(
                    'header' => 'Jam Pemberian',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'htmlOptions'=>array('style'=>'text-align:center;'),
                    'value' => '$data->jeniswaktu_jam',
                    'footerHtmlOptions'=>array('style'=>'text-align:right;color:white'),
                    'footer'=>'-',
                ),
//                array(
//                    'header' => 'Hari',
//                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
//                    'value' => 'date("l,strtotime($data->tglkirimmenu")',
//                ),
                array(
                    'header' => 'Waktu',
                    'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                    'htmlOptions'=>array('style'=>'text-align:center;'),
                    'value' => '$data->jeniswaktu_nama',
                    'footerHtmlOptions'=>array('style'=>'text-align:right;color:white'),
                    'footer'=>'-',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>