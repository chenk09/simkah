<?php 
    $table = 'ext.bootstrap.widgets.MergeHeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchTable();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php $this->widget($table,array(
	'id'=>'tableLaporan',
	'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header' => 'NO',
                'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                'htmlOptions'=>array('style'=>'text-align:center'),
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
            array(
                'header' => 'NAMA',
                'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                'value' => '$data->nama_pasien',
            ),
            array(
                'header' => 'KELAS',
                'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                'htmlOptions'=>array('style'=>'text-align:center'),
                'value' => '$data->kelaspelayanan_nama',
                'footerHtmlOptions'=>array('colspan'=>4,'style'=>'text-align:right;font-style:italic;'),
                'footer'=>'Total',
            ),
            array(
                'header' => 'TARIF',
                'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                'htmlOptions'=>array('style'=>'text-align:right'),
                'value'=>'MyFunction::formatNumber($data->getSumTotal(array("pasien","kelaspelayanan"),"total"))',
            ),
            array(
                'header' => 'JASA AG',
                'name'=>'tarif_tindakankomp',
                'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                'htmlOptions'=>array('style'=>'text-align:right'),
                'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pasien","kelaspelayanan"),"ag"))',
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>MyFunction::formatNumber($model->getSumTotalKomponen(array("pasien","kelaspelayanan"),"ag")),
            ),
            array(
                'header' => 'INSENTIF',
                'name'=>'tarif_tindakankomp',
                'headerHtmlOptions'=>array('style'=>'text-align: center;vertical-align:middle;'),
                'htmlOptions'=>array('style'=>'text-align:right'),
                'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pendaftaran","kelaspelayanan"),"insentif"))',
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>MyFunction::formatNumber($model->getSumTotalKomponen(array("pasien","kelaspelayanan"),"insentif")),
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>