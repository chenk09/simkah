

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
    'id'=>'diet-m-search',
	'method'=>'get',
                'type'=>'horizontal',
)); ?>
                            <?php echo $form->dropDownListRow($model,'tipediet_id',
                            CHtml::listData($model->TipeDietItems, 'tipediet_id', 'tipediet_nama'),
                            array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>
                            <?php echo $form->dropDownListRow($model,'jenisdiet_id',
                            CHtml::listData($model->JenisdietItems, 'jenisdiet_id', 'jenisdiet_nama'),
                            array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                            'empty'=>'-- Pilih --',)); ?>
                    <?php echo $form->textFieldRow($model,'diet_kandungan'); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>