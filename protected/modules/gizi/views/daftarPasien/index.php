<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari cari', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
if(!empty($_GET['succes'])){?>
<div class="alert alert-block alert-success">
    <a class="close" data-dismiss="alert">×</a>
        <?php if($_GET['succes']==2){ ?> Pemeriksaan Pasien berhasil di batalkan<?php } if($_GET['succes']==1){?>Pasein Berhasil Di Rujuk<?php }?>
</div>
<?php } ?>
<fieldset>
    <legend class="rim2">Informasi Daftar Pasien Gizi</legend>
     
     
<?php $this->widget('bootstrap.widgets.BootAlert');
 $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$modPasienMasukPenunjang->searchKonsulGizi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            'tgl_pendaftaran',
            'tglmasukpenunjang',
            'ruanganasal_nama',
            'no_pendaftaran',
            'no_rekam_medik',
            array(
                'header'=>'Nama Pasien / Alias',
                'type'=>'raw',
                'value'=>'"$data->nama_pasien"." / "."$data->nama_bin"',
            ),
           'jeniskasuspenyakit_nama',
           'umur',
           array(
                'header'=>'Jenis Kelamin',
                'type'=>'raw',
                'value'=>'$data->jeniskelamin',
           ),
           'alamat_pasien',
           
           array(
                'name'=>'CaraBayarPenjamin',
                'type'=>'raw',
                'value'=>'$data->caraBayarPenjamin',    
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
           ),
           array(
                'header'=>'Status Periksa',
                'type'=>'raw',
                'value'=>'$data->statusperiksa',
           ),
           array(
                'header'=>'Konsultasi Gizi',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=\"icon-user\"></i>","",array("onclick"=>"statusPeriksa($data->pendaftaran_id);return false;","href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Rencana Pemeriksaan"))',
                'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
//                'value'=>'CHtml::link("<i class=\"icon-user\"></i>",Yii::app()->controller->createUrl("/gizi/anamnesisDiet&idPendaftaran",array("idPendaftaran"=>$data->pendaftaran_id,"idPasien"=>$data->pasien_id,"idPasienAdmisi"=>"")), array("rel"=>"tooltip","title"=>"Klik untuk Rencana Pemeriksaan"))', 'htmlOptions'=>array('style'=>'text-align: center; width:40px')
           ),
           array(
               'header'=>'Batal Periksa',
               'type'=>'raw',
               'value'=>'CHtml::link("<i class=\'icon-remove\'></i>", "javascript:batalPeriksa($data->pasienmasukpenunjang_id)",array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk membatalkan Pemeriksaan"))',
               'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
   


<?php 
// Dialog untuk Lihat Hasil =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogLihatHasil',
    'options'=>array(
        'title'=>'Hasil Pemeriksaan Laboratorium',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>950,
        'minHeight'=>450,
        'resizable'=>true,
    ),
));
?>

<iframe src="" name="iframeLihatHasil" width="100%" height="500">
</iframe>

<?php
$this->endWidget();
//========= end Lihat Hasil =============================
?>
    
<?php
 //CHtml::link($text, $url, $htmlOptions)
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'id'=>'daftarPasien-form',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <table  class="table-condensed">
        <tr>
            <td>
              <?php echo $form->labelEx($modPasienMasukPenunjang,'tglmasukpenunjang', array('class'=>'control-label')) ?>
                  <div class="controls">  
                   
                     <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPasienMasukPenunjang,
                                         'attribute'=>'tglAwal',
                                         'mode'=>'datetime',
//                                          'maxDate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                      
                   </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' s/d', array('class'=>'control-label')) ?>

                   <div class="controls">  
                    <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPasienMasukPenunjang,
                                         'attribute'=>'tglAkhir',
                                         'mode'=>'datetime',
//                                         'maxdate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                   </div> 
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'noRM',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 
            </td>
            <td>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
            </td>
            
        </tr>
    </table>
    <div class="form-actions">
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
							     ?>
								       <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
  <?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>

    </div>
</fieldset>  
<?php $this->endWidget();?>
</fieldset>
<script type="text/javascript">
function batalPeriksa(idPenunjang){
   var con = confirm('Apakah anda yakin akan membatalkan pemeriksaan Operasi Bedah pasien ini? ');
   if(con){
       $.post('<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id.'/BatalPeriksa')?>',{idPenunjang:idPenunjang},
                 function(data){
                     if(data.status == 'ok' && data.pesan != 'exist'){
                         window.location = "<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id.'/index&succes=2')?>";
                     }else{
                         if(data.pesan == 'exist' && data.status == 'ok')
                         {
                            $('#dialogKonfirm div.divForForm').html(data.keterangan);
                            $('#dialogKonfirm').dialog('open');
                            $('#daftarPasien-grid').addClass('srbacLoading');
                            $.fn.yiiGridView.update('daftarPasien-grid', {
                                    data: $(this).serialize()
                            });
                         }
                     }
                 },'json'
             );
        
   }
}

function statusPeriksa(pendaftaran_id){
//    alert(status);
    var statusperiksa = '';
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = '';
    
    $.post("<?php echo Yii::app()->createUrl('gizi/pesanmenudietT/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id},
        function(data){
            if(data.pasienadmisi_id == null){
                var pasienadmisi_id = '';
            }else{
                var pasienadmisi_id = data.pasienadmisi_id;
            }
            if(data.pasien_id == null){
                var pasien_id = '';
            }else{
                var pasien_id = data.pasien_id;
            }
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan Konsultasi Gizi. ");
//                location.reload();
            }else{
                window.location.href= "<?php echo Yii::app()->createUrl('gizi/anamnesisDiet',array()); ?>&idPendaftaran="+data.pendaftaran_id+"&idPasien="+pasien_id+"&idPasienAdmisi="+pasienadmisi_id,"";
            }
    },"json");
    return false; 
}    
</script>
<?php 
// Dialog untuk masukkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogKonfirm',
    'options'=>array(
        'title'=>'',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>500,
        'minHeight'=>200,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end masukkamar_t dialog =============================
?>