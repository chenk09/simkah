
<?php $modPendaftaran = new GZPendaftaranT; ?>
       <?php $this->widget('bootstrap.widgets.BootPager', array(
                'pages' => $pages,    
                'header'=>'<div class="pagination" id="pagin">',
                'footer'=>'</div>',
       )); ?>      
       <table class="items table table-striped table-bordered table-condensed" >
        <thead>
            <tr>
                <th rowspan="2">Tgl Kunjungan/<br/>No.Pendaftaran</th>
                <th rowspan ="2"><center>Anamnesis Diet</center></th>  
                <th colspan ="9"><center>Pemeriksaan Fisik</center></th>  
                <th rowspan ="2"><center>Konsultasi Gizi</center></th>  
            </tr>
            <tr>
                <th><center>TD</center></th>  
                <th><center>DN</center></th>  
                <th><center>ST</center></th>  
                <th><center>TB/BB</center></th>  
                <th><center>Lila <br/> (Untuk Pasien Hamil)</center></th>  
                <th><center>Lingkar Pinggang <br/> (Untuk Pasien <br/> Obgyn)</center></th>  
                <th><center>LIngkar Pinggul <br/> (Untuk Pasien <br/> Obesitas)</center></th>  
                <th><center>Tebal Lema <br/> (Untuk Pasien <br/> Obesitas)</center></th>  
                <th><center>Tinggi Lutut <br/> (Untuk Pasien <br/> Usia Lanjut / Bongkok)</center></th>  
            </tr>
            
        </thead>
        <tbody>
            <?php foreach($modKunjungan as $modKunjungan) { ?>
            <tr>
                <td><?php echo $modKunjungan->no_pendaftaran; ?><br/><?php echo $modKunjungan->tgl_pendaftaran; ?></td>
                <td><?php //if (count($modKunjungan->tindakanpelayanan->daftartindakan_id) != 0){
                    echo CHtml::link("<i class='icon-list-alt'></i> ",  Yii::app()->controller->createUrl("daftarPasien/DetailAnamnesaDiet",
                            array("id"=>$modKunjungan->pendaftaran_id)),array("id"=>"$modKunjungan->no_pendaftaran","target"=>"detailDialogAnamnesa","rel"=>"tooltip","title"=>"Klik untuk Detail Anamnesa Diet", "onclick"=>"var text = $(this).attr('dialog-text'); window.parent.$('#ui-dialog-title-dialogDetailAnamnesa').text(text);window.parent.$('#dialogDetailAnamnesa').dialog('open');", "dialog-text"=>"Detail Anamnesa Diet")); 
                    
                //}?>
                </td>
                <td><?php echo $modKunjungan->pemeriksaanfisik->tekanandarah; ?></td>
                <td><?php echo $modKunjungan->pemeriksaanfisik->detaknadi; ?></td>
                <td><?php echo $modKunjungan->pemeriksaanfisik->suhutubuh; ?></td>
                <td>
                <?php 
                    echo $modKunjungan->pemeriksaanfisik->tinggibadan_cm; 
                ?>
                    <?php if((empty($modKunjungan->pemeriksaanfisik->tinggibadan_cm))&&(empty($modKunjungan->pemeriksaanfisik->beratbadan_kg))){
                        
                    } else { ;?>/
                    <?php } ?><br/>
                <?php 
                    echo $modKunjungan->pemeriksaanfisik->beratbadan_kg; 
                ?></td>
                <td><?php echo (!empty($modKunjungan->pemeriksaanfisik->Lila) ? $modKunjungan->pemeriksaanfisik->Lila." cm" : ""); ?></td>
                <td><?php echo (!empty($modKunjungan->pemeriksaanfisik->LingkarPinggang) ? $modKunjungan->pemeriksaanfisik->LingkarPinggang." cm" : ""); ?></td>
                <td><?php echo (!empty($modKunjungan->pemeriksaanfisik->LingkarPinggul) ? $modKunjungan->pemeriksaanfisik->LingkarPinggul." cm" : ""); ?></td>
                <td><?php echo (!empty($modKunjungan->pemeriksaanfisik->TebalLemak) ? $modKunjungan->pemeriksaanfisik->TebalLemak." cm" : ""); ?></td>
                <td><?php echo (!empty($modKunjungan->pemeriksaanfisik->TinggiLutut) ? $modKunjungan->pemeriksaanfisik->TinggiLutut." cm" : ""); ?></td>
                <td><?php //if (count($modKunjungan->tindakanpelayanan->daftartindakan_id) != 0){
                    echo CHtml::link("<i class='icon-list-alt'></i> ",  Yii::app()->controller->createUrl("daftarPasien/detailKonsulGizi",
                            array("id"=>$modKunjungan->pendaftaran_id)),array("id"=>"$modKunjungan->no_pendaftaran","target"=>"detailDialog","rel"=>"tooltip","title"=>"Klik untuk Detail Konsultasi Gizi", "onclick"=>"var text = $(this).attr('dialog-text'); window.parent.$('#ui-dialog-title-dialogDetailData').text(text);window.parent.$('#dialogDetailData').dialog('open');", "dialog-text"=>"Detail Pelayanan Konsultasi Gizi")); 
                    
                //}?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
        <tfoot><tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr></tfoot>
    </table>

   
