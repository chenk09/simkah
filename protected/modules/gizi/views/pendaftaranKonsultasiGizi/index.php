 <style>
    fieldset legend.accord1{
        width:100%;
    }
    table{
        width : 980px;
    }
 </style>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-mp-form',
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));
?>

<?php $this->widget('bootstrap.widgets.BootAlert'); 
 if(!empty($_GET['id'])){ ?> 
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
 <?php } ?>
    <fieldset>
        <legend class="rim2">Transaksi Konsultasi Gizi Pasien Luar</legend>
        <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        <?php echo $form->errorSummary(array($modTindakanPelayananT,$modTindakanKomponen,$model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienPenunjang)); ?>

        <table class='table-condensed'>
            <tr>
                <td width="10%">
                    <div class='control-group'>
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                            )); ?>
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>
                        </div>
                    </div>
                    
                    <?php echo $this->renderPartial('_formPasien', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien)); ?>
                </td>
                <td width="50%">
                    
                    <?php //echo $form->textFieldRow($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData($model->getRuanganItems(), 'ruangan_id', 'ruangan_nama'), array('class'=>'reqPasien','empty'=>'-- Pilih --', 'disabled'=>'disabled', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'ajax'=>array('type'=>'POST',
                                                                'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'PPPendaftaranMp')),
                                                                'update'=>'#PPPendaftaranMp_jeniskasuspenyakit_id'),
                                                                'onChange'=>'listDokterRuangan(this.value);'
                                                            )); ?>

                    <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems(), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'reqPasien')); ?>

                    <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('class'=>'reqPasien','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>'getJenisKonsul();',)); ?>
                    
                    <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems(Params::RUANGAN_ID_LAB), 'pegawai_id', 'nama_pegawai') ,array('class'=>'reqPasien','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('class'=>'reqPasien','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'LKPendaftaranMp')), 
                                                    'update'=>'#LKPendaftaranMp_penjamin_id'  
                                                ),'onchange'=>'caraBayarChange(this)',
                        )); ?>

                    <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('class'=>'reqPasien','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                   
                    <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan,'modPengambilanSample'=>$modPengambilanSample)); ?>
                    
                    <?php echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form)); ?>
               
                    <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>
                                               
                    <?php //echo $this->renderPartial('_formPengambilanSample', array('model'=>$model,'form'=>$form,'modPengambilanSample'=>$modPengambilanSample, 'modSampleLab'=>$modSampleLab)); ?>
                     
                     
                    
                    <fieldset id="fieldsetKarcis" class="span6">
                                    <legend class="accord1" >
                                        <?php echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        Karcis Penunjang                                    </legend>
                                <?php echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                                </fieldset>
                   <br/>
                   <fieldset class="span6">
                        <legend class="rim"> Daftar Konsultasi </legend>
                        <fieldset class="span6">
                                <table id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th>Pilih</th>
                                            <th>Jenis Konsultasi / <br/> Pemeriksaan</th>
                                            <th>Tarif</th>
                                            <th>Qty</th>
                                            <th>Satuan</th>
                                            <th>Cyto</th>
                                            <th>Tarif Cyto</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                        </fieldset>
                   </fieldset>
                </td>
            <tr>
        </table>
    </fieldset>

    <div class='form-actions'>
             <?php 
             if($model->isNewRecord){
                  echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button', 
                                                    'onKeypress'=>'setVerifikasi();return false;','onClick'=>'setVerifikasi();return false;',
                                                    'id'=>'btn_simpan',
                                                   )); 
//                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)', 'id'=>'btn_simpan','onclick'=>'cekSatuan(this);'));
             }else{ 
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
             }
//             echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
//                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
//                                                array('class'=>'btn btn-primary', 'type'=>'button', 
//                                                    'onKeypress'=>'return formSubmit(this,event)',
//                                                    'id'=>'btn_simpan','onclick'=>'cekSatuan(this);',
//                                                   )); 
                                                                     ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl(''.Yii::app()->controller->id.'/'.$this->action->Id.'',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
        <?php $id_pendaftaran = $_GET['id']; 
            if(!empty($id_pendaftaran) && !$model->isNewRecord){ 
            
//            echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false")); 
           // echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info', 'onclick'=>'print(\'PRINT\')')); 
            
            
            }
        ?>
<?php  
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
	</div>
<?php $this->endWidget(); ?>


<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
        'id'=>'dialogVerifikasi',
        'options'=>array(
            'title'=>'Verifikasi Data Pasien',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>850,
            'height'=>550,
            'resizable'=>false,
        ),
    ));
?>
<div id="dataPasien">
    <legend class=rim>Data Pasien</legend>
    <table class="table table-bordered  table-condensed">
        <tbody>

        </tbody>
        
    </table>
</div>
<div id ="dataKunjungan">
    <legend class=rim>Data Kunjungan</legend>
    <table class="table table-bordered  table-condensed">
        <tbody>
            
        </tbody>
    </table>
    <div class="form-actions">
        <?php 
            echo CHtml::link(Yii::t('mds', '{icon} Teruskan', array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 
                '#', array('class'=>'btn btn-primary','onclick'=>'setKonfirmasi(this);','disabled'=>FALSE  )); 
        ?>
        <?php 
            echo CHtml::link(Yii::t('mds', '{icon} Kembali', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                '#', array('class'=>'btn btn-blue','onclick'=>'kembaliForm();','disabled'=>FALSE)); 
        ?>
    </div>
</div>
<?php
    $this->endWidget();
?>


<?php

$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('id_pendaftaran'=>$id_pendaftaran, 'caraPrint'=>$caraPrint));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=570px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('printCaraPrint',$js,  CClientScript::POS_HEAD);
//$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoli',array('idPendaftaran'=>''));
//$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlGetPemeriksaanLK = Yii::app()->createUrl('actionAjax/GetPemeriksaanLK');

$urlListKarcis =Yii::app()->createUrl('actionAjax/listKarcis');
$karcis = KonfigsystemK::getKonfigurasiKarcisPasien();
$karcis = ($karcis) ? true : 0;

$jscript = <<< JS

setTimeout(listKarcis, 3000); 
function listKarcis(obj)
{
     kelasPelayanan=$('#LKPendaftaranMp_kelaspelayanan_id').val();
     ruangan=$('#LKPendaftaranMp_ruangan_id').val();

        pasienLama = 0;
            
     if(kelasPelayanan!=''){
             $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan, pasienLama:pasienLama },
                function(data){
                    $('#tblFormKarcis tbody').html(data.form);
                    if (${karcis}){
                        if (jQuery.isNumeric(data.karcis.karcis_id)){
                            tdKarcis = $('#tblFormKarcis tbody tr').find("td a[data-karcis='"+data.karcis.karcis_id+"']");
                            changeBackground(tdKarcis, data.karcis.daftartindakan_id, data.karcis.harga_tariftindakan, data.karcis.karcis_id);
                        }
                    }
             }, "json");
     }      
       
}

function changeBackground(obj,idTindakan,tarifSatuan,idKarcis)
{
        banyakRow=$('#tblFormKarcis tr').length;
        for(i=1; i<=banyakRow; i++){
        $('#tblFormKarcis tr').css("background-color", "#FFFFFF");          
        } 
             
        $(obj).parent().parent().css("backgroundColor", "#00FF00");     
        $('#TindakanPelayananT_idTindakan').val(idTindakan);  
        $('#TindakanPelayananT_tarifSatuan').val(tarifSatuan);     
        $('#TindakanPelayananT_idKarcis').val(idKarcis);     
     
}
   
function cekValidasi(){
    var kelasPelayanan  = $('#LKPendaftaranMp_kelaspelayanan_id').val();
    var jenisIdentitas  = $('#LKPasienM_jenisidentitas').val();
    var noIdentitas     = $('#LKPasienM_no_identitas_pasien').val();
    var namaDepan       = $('#LKPasienM_namadepan').val();
    var namaPasien      = $('#LKPasienM_nama_pasien').val();
    var jenisKelamin    = true;
    var alamatPasien    = $('#LKPasienM_alamat_pasien').val();
    var jenisPenyakit   = $('#LKPendaftaranMp_jeniskasuspenyakit_id').val();
    var dokter          = $('#LKPendaftaranMp_pegawai_id').val();
    var caraBayar       = $('#LKPendaftaranMp_carabayar_id').val();
    var penjamin        = $('#LKPendaftaranMp_penjamin_id').val();

    //=====validasi jenis kelamin
    if($('input[name="LKPasienM[jeniskelamin]"]:checked').length == 0) {
        jenisKelamin = false;
    }
    
    //=======validasi sample
    var sampleLab       = $('#LKPengambilanSampleT_samplelab_id').val();
    var jumlahSample    = $('#LKPengambilanSampleT_jmlpengambilansample').val();
//    if ($('input[name="pakeSample"]').is(':checked')) {
//        if(sampleLab && jumlahSample){
//            sample = true;
//        }else{
//            sample = false;
//        }
//    }else{
//       sample = true;
//    }
    
    //======validasi rujukan
    var asalRujukan     = $('#LKRujukanT_asalrujukan_id').val();
    var noRujukan       = $('#LKRujukanT_no_rujukan').val();
    
//    if ($('input[name="isRujukan"]').is(':checked')) {
//       if(asalRujukan && noRujukan){
//            rujukan = true;
//        }else{
//            rujukan = false;
//        }
//    } else {
//        rujukan = true;
//    }


    if (jQuery.isNumeric(kelasPelayanan) && jenisIdentitas && noIdentitas && namaDepan && namaPasien && jenisKelamin && alamatPasien && jenisPenyakit && dokter && 
        caraBayar && penjamin && sampleLab && jumlahSample && asalRujukan && noRujukan)
    {       
          $('#dialogLaboratorium').dialog('open');
    }
    else{
        alert('Silahkan isi terlebih dahulu kolom yang memiliki tanda *');
    }
}

function cekSatuan(obj)
{
   satuan =$('#satuan').length;
   cekSatuanKosong=true;
//   if (satuan=='0'){
//        alert('Anda Belum Memasukan Tindakan Yang Akan Diperiksa');
//        return false;
//    }else{

        $('#satuan').each(function() {
            cekBox =$(this).parent().prev().prev().prev().prev().find('input:checkbox').is(':checked');   
            isiSatuan = this.value;
            if(cekBox==true){
                if(isiSatuan==''){
                    alert('Anda Belum Memilih Satuan');
                    cekSatuanKosong==false;
                    return false;
                }
            }
        });

//}

    if (cekSatuanKosong==true){
//           do_upload();
           $('#pppendaftaran-mp-form').submit();  
//           alert('simpan');
    }
 }   
 function hapusTindakanss()
    {
        banyakRow=$('#tblFormPemeriksaanLab tr').length;
        
        for(i=2; i<=banyakRow; i++){
        $('#tblFormPemeriksaanLab tr:last').remove();
        }
        
          $('.box input').removeAttr('checked');
    }
JS;
Yii::app()->clientScript->registerScript('periksa satuan',$jscript, CClientScript::POS_HEAD);

$jscript = <<< JS

function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#PPPendaftaranMp_pegawai_id').html(data.listDokter);
    }, "json");
}

function getPemeriksaanLK(obj)
{
    $.post("${urlGetPemeriksaanLK}", { idJenisPemeriksaan: obj.value },
        function(data){

            $('.boxtindakan').html(data.pemeriksaan);
    }, "json");
}

function caraAmbilPhotoJS(obj)
{
    caraAmbilPhoto=obj.value;
    
    if(caraAmbilPhoto=='webCam')
        {
          $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
            
        }
    else
        {
         $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
        }
} 
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
<script type="text/javascript">
    
$('#formPeriksaLab').tile({widths : [ 190 ]});

function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanlab = obj.value;
        var kelasPelayan_id = $('#LKPendaftaranMp_kelaspelayanan_id').val();
        
        if(kelasPelayan_id==''){
                $(obj).attr('checked', 'false');
                alert('Anda Belum Memilih Kelas Pelayanan');
                
            }
        else{
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanLabPendLab')?>',
                 'data':{idPemeriksaanlab:idPemeriksaanlab,kelasPelayan_id:kelasPelayan_id},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         if($.trim(data.form)=='')
                         {
                            $(obj).removeAttr('checked');
                            alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                         } else if($.trim(data.form)=='tarif kosong') {
                            $(obj).removeAttr('checked');
                            data.form = '';
                            alert ('Pemeriksaan belum memiliki tarif');
                         }
                         $('#tblFormPemeriksaanLab').append(data.form);
                 } ,
                 'cache':false});
        }     
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idPemeriksaanlab)
{
    $('#tblFormPemeriksaanLab #periksalab_'+idPemeriksaanlab).detach();
}

function cekcyto(obj, x){
//    console.log(x);
//    tidakan = $('.cyto_tindakan').val();
    if(obj.value == 1){
        $('.cyto_' + x).show();
    }else
        {
            $('.cyto_' + x).hide();
            
        }
}

function getJenisKonsul(obj)
{
        var kelasPelayan_id = $('#LKPendaftaranMp_kelaspelayanan_id').val();
        
        if(kelasPelayan_id==''){
                $(obj).attr('checked', 'false');
                alert('Anda Belum Memilih Kelas Pelayanan');
                
            }
        else{
             $.post("<?php echo Yii::app()->createUrl('gizi/ActionAjaxGZ/getJenisKonsul'); ?>", {kelasPelayan_id:kelasPelayan_id},
                function(data){
                    if($.trim(data.form)=='')
                         {
                            $(obj).removeAttr('checked');
                            alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                         } else if($.trim(data.form)=='tarif kosong') {
                            $(obj).removeAttr('checked');
                            data.form = '';
                            alert ('Tindakan belum memiliki tarif');
                         }
                         $('#tblFormPemeriksaanLab').append(data.form);
            },"json");
//        jQuery.ajax({'url':'<?php // echo Yii::app()->createUrl('gizi/ActionAjaxGZ/getJenisKonsul')?>',
//                 'data':{kelasPelayan_id:kelasPelayan_id},
//                 'type':'post',
//                 'dataType':'json',
//                 'success':function(data) {
//                         if($.trim(data.form)=='')
//                         {
//                            $(obj).removeAttr('checked');
//                            alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
//                         } else if($.trim(data.form)=='tarif kosong') {
//                            $(obj).removeAttr('checked');
//                            data.form = '';
//                            alert ('Tindakan belum memiliki tarif');
//                         }
//                         $('#tblFormPemeriksaanLab').append(data.form);
//                 } ,
//                 'cache':false});
        }     
        listKarcis();

}

function setVerifikasi()
    {
        var namaPasien = $('#LKPasienM_nama_pasien').val();             
        var namaDepan = $('#LKPasienM_namadepan').val();
        var noIdentitas = $('#LKPasienM_no_identitas_pasien').val();    
        var jenisIdentitas = $('#LKPasienM_jenisidentitas').val();
        var alias = $('#LKPasienM_nama_bin').val();
        var tempatLahir = $('#LKPasienM_tempat_lahir').val();           
        var tglLahir = $('#LKPasienM_tanggal_lahir').val();
        var umur = $('#LKPendaftaranMp_umur').val();
        
        var jenisKelamin = $('#fieldsetPasien').find('input:radio[name$="[jeniskelamin]"][checked="checked"]').val();
        if(jenisKelamin == '')
        {
            var jenisKelamin = "-";
        }else{
            var jenisKelamin = $('#fieldsetPasien').find('input:radio[name$="[jeniskelamin]"][checked="checked"]').val();
        }
        
        var alamatPasien = $('#LKPasienM_alamat_pasien').val();         
        var rt = $('#LKPasienM_rt').val();  var rw = $('#LKPasienM_rw').val();
        var noTelp = $('#LKPasienM_no_telepon_pasien').val();           
        var noMobile = $('#LKPasienM_no_mobile_pasien').val();
        
        var propinsi = $('#LKPasienM_propinsi_id').val();
            if(propinsi != ''){
                 var propinsi = $("#LKPasienM_propinsi_id option:selected").text();
            }else{
                var propinsi = "-";
            }
        var kabupaten = $('#LKPasienM_kabupaten_id').val();
            if(kabupaten != ''){
                var kabupaten = $("#LKPasienM_kabupaten_id option:selected").text();
            }else{
                var kabupaten = "-";
            }
        var kecamatan = $('#LKPasienM_kecamatan_id').val();
            if(kecamatan != ''){
                var kecamatan = $("#LKPasienM_kecamatan_id option:selected").text();
            }else{
                var kecamatan = "-";
            }
        var kelurahan = $('#LKPasienM_kelurahan_id').val();
            if(kelurahan != ''){
                var kelurahan = $("#LKPasienM_kelurahan_id option:selected").text();
            }else{
                var kelurahan = "-";
            }
        var agama = $('#LKPasienM_agama').val();
            if(agama != ''){
                var agama = $('#LKPasienM_agama').val();
            }else{
                var agama = "-";
            }
        var warganegara = $('#LKPasienM_warga_negara').val();
            if(warganegara != ''){
                var warganegara = $('#LKPasienM_warga_negara').val();
            }else{
                var warganegara = "-";
            }
        var namaAyah = $('#LKPasienM_nama_ayah').val();     
        var namaIbu = $('#LKPasienM_nama_ibu').val();
        
        var poliklinikTujuan = $('#LKPendaftaranMp_ruangan_id').val();
            if(poliklinikTujuan != ''){
                var poliklinikTujuan = $("#LKPendaftaranMp_ruangan_id option:selected").text();      
            }else{
                var poliklinikTujuan = "-";
            }
        var jenisKasusPenyakit = $('#LKPendaftaranMp_jeniskasuspenyakit_id').val();
            if(jenisKasusPenyakit != ''){
                var jenisKasusPenyakit = $("#LKPendaftaranMp_jeniskasuspenyakit_id option:selected").text();    
            }else{
                var jenisKasusPenyakit = "-";
            }
        var kelasPelayanan = $('#LKPendaftaranMp_kelaspelayanan_id').val();
            if(kelasPelayanan != ''){
                var kelasPelayanan = $("#LKPendaftaranMp_kelaspelayanan_id option:selected").text();
            }else{
                var kelasPelayanan = "-";
            }
        var dokter = $('#LKPendaftaranMp_pegawai_id').val();
            if(dokter != ''){
                var dokter = $("#LKPendaftaranMp_pegawai_id option:selected").text();
            }else{
                var dokter = "-";
            }
        var caraBayar = $('#LKPendaftaranMp_carabayar_id').val();
            if(caraBayar != ''){
                var caraBayar = $("#LKPendaftaranMp_carabayar_id option:selected").text();
            }else{
                var caraBayar = "-";
            }
        var penjamin = $('#LKPendaftaranMp_penjamin_id').val();
            if(penjamin !=''){
                var penjamin = $("#LKPendaftaranMp_penjamin_id option:selected").text();
            }else{
                var penjamin = "-";
            }

        var kosong = '';
        var reqPasien       = $("#fieldsetPasien").find(".reqPasien[value="+kosong+"]");
        var pasienKosong = reqPasien.length;
        
        var reqKunjungan = $('#fieldsetKunjungan').find(".reqKunjungan[value="+kosong+"]");
        var kunjunganKosong = reqKunjungan.length;
        
        var pjKosong = 0;
        if($('#adaPenanggungJawab').is(':checked')){ //jika ada penanggung jawab data * harus diisi
            var reqPj = $('#fieldsetPenanggungjawab').find(".reqPj[value="+kosong+"]");
            pjKosong = reqPj.length;
        }
        
        if($("#isUpdatePasien"))
        {
            
            if (!(namaDepan && namaPasien && jenisKelamin && alamatPasien && (pasienKosong === 0)))
            {
                if(pasienKosong != 0)
                {
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Pasien');
                }
               return false;
            }else{
                if (!((kunjunganKosong === 0) && (pjKosong === 0)))
                {
                    if(kunjunganKosong != 0)
                        alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Kunjungan');
                    else if(pjKosong != 0)
                        alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Penanggung Jawab');
                   return false;
                }
                else
                {
                    var confirm = $('#dialogVerifikasi').dialog('open');
                    if(confirm){
                        $('#dataPasien').find('tbody').empty();
                        $('#dataPasien').find('tbody').append('<tr>\n\
                                <td>No Identitas </td><td>'+ jenisIdentitas + " - " + noIdentitas + '</td>\n\
                                <td>No. Mobile</td><td>'+ noMobile + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Nama Pasien</td><td>'+ namaDepan + " - " + namaPasien + '</td>\n\
                                <td>Nama Ibu</td><td>' + namaIbu + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Tempat Lahir</td><td>'+ tempatLahir + '</td>\n\
                                <td>Nama Ayah</td><td>'+ namaAyah + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Tanggal Lahir</td><td>'+ tglLahir + '</td>\n\
                                <td>Propinsi</td><td>' + propinsi + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Umur</td><td>' + umur + '</td>\n\
                                <td>Kota / Kabupaten</td><td>' + kabupaten + '</td>\n\
                                \n\
                            </tr>\n\
                            <tr>\n\
                                <td>Jenis Kelamin</td><td>' + jenisKelamin + '</td>\n\
                                <td>Kecamatan</td><td>' + kecamatan + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Alamat Pasien</td><td>'+ alamatPasien +'</td>\n\
                                <td>Kelurahan</td><td>' + kelurahan + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>RT / RW </td><td>' + rt + " / " + rw + '</td>\n\
                                <td>Agama</td><td>' + agama + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>No Telp</td><td>' + noTelp +'</td>\n\
                                <td>Warga Negara</td><td>'+ warganegara +'</td>\n\
                            </tr>'
                        );
                        $('#dataKunjungan').find('tbody').empty();
                        $('#dataKunjungan').find('tbody').append('<tr>\n\
                                <td>Poliklinik Tujuan </td><td>'+ poliklinikTujuan + '</td>\n\
                                <td>Dokter</td><td>'+ dokter + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Jenis Kasus Penyakit</td><td>'+ jenisKasusPenyakit + '</td>\n\
                                <td>Cara Bayar</td><td>' + caraBayar + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Kelas Pelayanan</td><td>'+ kelasPelayanan + '</td>\n\
                                <td>Penjamin</td><td>' + penjamin + '</td>\n\
                            </tr>'
                        );
                    }
                   return false;
                }                
            }
        }else{
            if (!((kunjunganKosong === 0) && (pjKosong === 0)))
            {
                if(kunjunganKosong != 0)
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Kunjungan');
                else if(pjKosong != 0)
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Penanggung Jawab');

               return false;
            }
            else
            {
                var confirm = $('#dialogVerifikasi').dialog('open');
                if(confirm){
                    $('#dataPasien').find('tbody').empty();
                    $('#dataPasien').find('tbody').append('<tr>\n\
                            <td>No Identitas </td><td>'+ jenisIdentitas + " - " + noIdentitas + '</td>\n\
                            <td>No. Mobile</td><td>'+ noMobile + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Nama Pasien</td><td>'+ namaDepan + " - " + namaPasien + '</td>\n\
                            <td>Nama Ibu</td><td>' + namaIbu + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Tempat Lahir</td><td>'+ tempatLahir + '</td>\n\
                            <td>Nama Ayah</td><td>'+ namaAyah + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Tanggal Lahir</td><td>'+ tglLahir + '</td>\n\
                            <td>Propinsi</td><td>' + propinsi + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Umur</td><td>' + umur + '</td>\n\
                            <td>Kota / Kabupaten</td><td>' + kabupaten + '</td>\n\
                            \n\
                        </tr>\n\
                        <tr>\n\
                            <td>Jenis Kelamin</td><td>' + jenisKelamin + '</td>\n\
                            <td>Kecamatan</td><td>' + kecamatan + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Alamat Pasien</td><td>'+ alamatPasien +'</td>\n\
                            <td>Kelurahan</td><td>' + kelurahan + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>RT / RW </td><td>' + rt + " / " + rw + '</td>\n\
                            <td>Agama</td><td>' + agama + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>No Telp</td><td>' + noTelp +'</td>\n\
                            <td>Warga Negara</td><td>'+ warganegara +'</td>\n\
                        </tr>'
                    );
                    $('#dataKunjungan').find('tbody').empty();
                    $('#dataKunjungan').find('tbody').append('<tr>\n\
                            <td>Poliklinik Tujuan </td><td>'+ poliklinikTujuan + '</td>\n\
                            <td>Dokter</td><td>'+ dokter + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Jenis Kasus Penyakit</td><td>'+ jenisKasusPenyakit + '</td>\n\
                            <td>Cara Bayar</td><td>' + caraBayar + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Kelas Pelayanan</td><td>'+ kelasPelayanan + '</td>\n\
                            <td>Penjamin</td><td>' + penjamin + '</td>\n\
                        </tr>'
                    );
                }
               return false;
            }
        }
        return false;
    }
    
     function kembaliForm(){
        
       $('#dialogVerifikasi').dialog('close');
    }
    
//      function setKonfirmasi(){
//        $('#pppendaftaran-mp-form').submit();
//        }
    
     function caraBayarChange(obj){
        if(obj.value == 1 || obj.value == 0 ){
           setTimeout( 'var penjamin = document.getElementById(\'LKPendaftaran_penjamin_id\'); penjamin.selectedIndex = 1;', '500' );
           document.getElementById('pakeAsuransi').checked = false;
           $('#divAsuransi').hide(500);
        }else{
            document.getElementById('pakeAsuransi').checked = true;
            $('#divAsuransi input').removeAttr('disabled');
            $('#divAsuransi select').removeAttr('disabled');
            $('#divAsuransi').show(500);
        }
    }
    
    function setKonfirmasi(obj){
        $(obj).attr('disabled',true);
        $(obj).removeAttr('onclick');
        $('#pppendaftaran-mp-form').find('.currency').each(function(){
            $(this).val(unformatNumber($(this).val()));
        });
        $('#pppendaftaran-mp-form').submit();
    }
</script>

<?php
$js = <<< JS
$('.numberOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

 <?php  $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                                'id'=>'dialogLaboratorium',
                                'options'=>array(
                                    'title'=>'Pemeriksaan Laboratorium',
                                    'autoOpen'=>false,
                                    'width'=>450,
                                    'height'=>450,
                                    'modal'=>false,
                                    'hide'=>'explode',
                                    'resizelable'=>false,
                                ),
                            ));
                            ?>
<?php //echo $this->renderPartial('_formLabolatorium', array('modPeriksaLab'=>$modPeriksaLab,'modJenisPeriksaLab'=>$modJenisPeriksaLab)); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>