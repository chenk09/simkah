
<legend class="rim2">Infomasi Tarif Gizi</legend> 		
<?php 
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarTindakan-grid',
	'dataProvider'=>$modTarifTindakanRuanganV->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'kelompoktindakan_nama',
                'kategoritindakan_nama',
                'daftartindakan_nama',
                'kelaspelayanan_nama',

                 array(
                    'name'=>'tarifTotal',
                    'value'=>'$this->grid->getOwner()->renderPartial(\'_tarifTotal\',array(\'kelaspelayanan_id\'=>$data[kelaspelayanan_id],\'daftartindakan_id\'=>$data[daftartindakan_id]),true)',
                 ),
                'persencyto_tind',
                'persendiskon_tind',
                array(
                            'name'=>'Komponen Tarif',
                            'type'=>'raw',
                            'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/detailsTarif",array("idKelasPelayanan"=>$data->kelaspelayanan_id,"idDaftarTindakan"=>$data->daftartindakan_id, "idKategoriTindakan"=>$data->kategoritindakan_id)) ,array("title"=>"Klik Untuk Melihat Detail Tarif","target"=>"iframe", "onclick"=>"$(\"#dialogDetailsTarif\").dialog(\"open\");", "rel"=>"tooltip"))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
                
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); 
?>
<?php
    $this->renderPartial('_search',array('modTarifTindakanRuanganV'=>$modTarifTindakanRuanganV));
?>

<?php
    // ===========================Dialog Details Tarif=========================================
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                        'id'=>'dialogDetailsTarif',
                            // additional javascript options for the dialog plugin
                            'options'=>array(
                            'title'=>'Komponen Tarif',
                            'autoOpen'=>false,
                            'width'=>250,
                            'height'=>300,
                            'resizable'=>false,
                            'scroll'=>false    
                             ),
                    ));
?>
<iframe src="" name="iframe" width="100%" height="100%">
</iframe>
<?php    
    $this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details Tarif================================
?>