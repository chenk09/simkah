<?php 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gzpesanmenudiet-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
    <legend class="rim2">Informasi Pesan Menu Diet</legend>


<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'gzpesanmenudiet-t-grid',
	'dataProvider'=>$model->searchInformasi(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'nopesanmenu',
                'jenispesanmenu',
                'nama_pemesan',
                'ruangan.instalasi.instalasi_nama',
                'ruangan.ruangan_nama',                
                'tglpesanmenu',
                'bahandiet.bahandiet_nama',
                'jenisdiet.jenisdiet_nama',
                'adaalergimakanan',
                'keterangan_pesan',
                array(
                    'header'=>'Detail',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",  Yii::app()->controller->createUrl("/gizi/PesanmenudietT/detailPesanMenuDiet",array("id"=>$data->pesanmenudiet_id)),array("id"=>"$data->pesanmenudiet_id","target"=>"frameDetail","rel"=>"tooltip","title"=>"Klik untuk Detail Pemesanan Menu Diet", "onclick"=>"window.parent.$(\'#dialogDetail\').dialog(\'open\')"));','htmlOptions'=>array('style'=>'text-align: center')
                ),
                array(
                    'header'=>'Kirim Menu Diet',
                    'type'=>'raw',
                    'value'=>'(($data->jenispesanmenu == '.Params::JENISPESANMENU_PASIEN.') ? CHtml::link(\'<i class="icon-list-alt"></i>\',"",array("onclick"=>"statusPeriksa($data->pesanmenudiet_id);return false;","href"=>"","rel"=>"tooltip","title"=>"Klik untuk Melanjutkan ke Pengiriman")) : CHtml::link(\'<i class="icon-list-alt"></i>\', "",array("onclick"=>"statusPeriksa($data->pesanmenudiet_id);return false;","href"=>"","rel"=>"tooltip","title"=>"Klik untuk Melanjutkan ke Pengiriman")))','htmlOptions'=>array('style'=>'text-align: center')
                ),
                array(
                    'header'=>'Batal <br/> Pesan',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=icon-remove></i>","javascript:batalPesan(\'$data->pesanmenudiet_id\')",array("idPesanDiet"=>$data->pesanmenudiet_id,"href"=>"#","rel"=>"tooltip","title"=>"Klik Untuk Batal Pesan Menu Diet",))',
                ),
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
    <div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
</fieldset>

<?php 
$js = <<< JSCRIPT
function openDialog(id){
    $('#dialogDetail').dialog('open');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('head',$js,CClientScript::POS_HEAD);                        
?>

<?php
//========= Dialog untuk Melihat detail Pengajuan Bahan Makanan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDetail',
    'options' => array(
        'title' => 'Detail Pemesanan Menu Diet',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

echo '<iframe src="" name="frameDetail" width="100%" height="500">
</iframe>';

$this->endWidget();
?>
<?php 
// Dialog untuk Batal Pesan Menu Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogBatalPesan',
    'options'=>array(
        'title'=>'Batal Pesan Menu Diet',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>600,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>'; 


$this->endWidget();
//========= Dialog untuk Batal Pesan Menu Diet =============================
?>

<script>
function batalPesan(idPesanDiet){
    var idPesanDiet = idPesanDiet;
//    var answer = confirm('Yakin Akan Membatalkan Pemesanan Diet ?');
//    if (answer){
          $.post('<?php echo Yii::app()->createUrl('gizi/ActionAjaxGZ/batalMenuDiet');?>', 
          {idPesanDiet:idPesanDiet}, function(data){
            if (data.status == 'create_form')
            {
                setTimeout("$('#dialogBatalPesan').dialog('open') ",1000);
                $('#dialogBatalPesan div.divForForm').html(data.div);
                $('#dialogBatalPesan div.divForForm form #idPesanDiet').val(data.idPesan);
                $('#dialogBatalPesan div.divForForm form').submit(konfirmBatal);            
            }
            else
            {
               $('#dialogBatalPesan div.divForForm').html(data.div);
               $.fn.yiiGridView.update('gzpesanmenudiet-t-grid');
               setTimeout("$('#dialogBatalPesan').dialog('close') ",1000);

            }
                      
        }, 'json');
//    }
}

function konfirmBatal()
{
    
    <?php 
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('gizi/ActionAjaxGZ/batalMenuDiet'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogBatalPesan div.divForForm').html(data.div);
                    $('#dialogBatalPesan div.divForForm form').submit(konfirmBatal);
                }
                else
                {
                    $('#dialogBatalPesan div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('gzpesanmenudiet-t-grid');
                    setTimeout(\"$('#dialogBatalPesan').dialog('close') \",3000);
                }
 
            } ",
    ))
?>;
    return false; 
}

function statusPeriksa(pesanmenudiet_id){
//    alert(status);
    var statusperiksa = '';
    var pendaftaran_id = '';
    var pasienmasukpenunjang_id = '';
    var pesanmenudiet_id = pesanmenudiet_id;
    
    $.post("<?php echo Yii::app()->createUrl('gizi/pesanmenudietT/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id,pesanmenudiet_id:pesanmenudiet_id},
        function(data){
            if(data.pasienadmisi_id == null){
                var pasienadmisi_id = '';
            }else{
                var pasienadmisi_id = data.pasienadmisi_id;
            }
            if(data.pasien_id == null){
                var pasien_id = '';
            }else{
                var pasien_id = data.pasien_id;
            }
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan transaksi Pengiriman Menu Diet. ");
                location.reload();
            }else{
                window.location.href= "<?php echo Yii::app()->controller->createUrl('KirimmenudietT/index',array()); ?>&idPesan="+data.pesanmenudiet_id,"";
            }
    },"json");
    return false; 
}    
</script>