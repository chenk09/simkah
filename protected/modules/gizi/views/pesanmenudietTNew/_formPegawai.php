<fieldset>
    <legend class="rim2">Transaksi Pemesanan Menu Diet Pegawai &amp; Tamu</legend>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'id' => 'gzpesanmenudiet-t-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
        'focus' => '#',
            ));
    ?>
    <?php
    if(!empty($_GET['id'])){
?>
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
<?php } ?>

    <?php echo $this->renderPartial('gizi.views.pesanmenudietT._dataForm', array('form' => $form, 'model' => $model)); ?>

    <fieldset id="fieldsetMenuDiet">
        <legend class="rim">Detail Menu Diet</legend>
<table width="200" border="0">
  <tr>
    <td width="80"> 
        <?php echo CHtml::hiddenField('jenisPesan'); ?>
        <?php echo $form->dropDownListRow($model, 'jenispesanmenu', GZPesanmenudietT::jenisPesan(), array('inline' => true, 'empty' => '-- Pilih --', 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'onchange' => 'setJenisPesan();', 'maxlength' => 50)); ?>
        <div class="control-group" id="groupRuangan">
            <label class='control-label'><?php echo CHtml::checkBox('cekRuangan', true, array('onclick' => 'setRuangan();', 'onkeypress' => "return $(this).focusNextInputField(event);",)) . ' '; ?><?php echo CHtml::encode($model->getAttributeLabel('ruangan_id')); ?><span class="required">*</span></label>
            <div class="controls">
                <?php echo CHtml::hiddenField('instalasi_id'); ?>
                <?php echo CHtml::hiddenField('ruangan_id'); ?>
                <?php
                echo $form->dropDownList($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50,
                    'ajax' => array('type' => 'POST',
                        'url' => Yii::app()->createUrl('ActionDynamic/ruanganDariInstalasi', array('encode' => false, 'namaModel' => '' . $model->getNamaModel() . '')),
                        'update' => '#' . CHtml::activeId($model, 'ruangan_id') . ''),));
                ?>
                <?php echo $form->dropDownList($model, 'ruangan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50, 'onchange' => 'clearAll()')); ?>
                <?php echo $form->error($model, 'ruangan_id'); ?>
            </div>
        </div>
        <div class="control-group" id="pegawaiGroup" style='display:none'>
            <label class='control-label'>Nama Pegawai</label>
            <div class="controls">
                <?php echo CHtml::hiddenField('idPegawai'); ?>

                <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'namaPegawai',
                    'source' => 'js: function(request, response) {
                                                                   $.ajax({
                                                                       url: "' . Yii::app()->createUrl('ActionAutoComplete/pegawaiUntukMenuDiet') . '",
                                                                       dataType: "json",
                                                                       data: {
                                                                           term: request.term,
                                                                           idRuangan:$("#' . CHtml::activeId($model, 'ruangan_id') . '").val(),
                                                                       },
                                                                       success: function (data) {
                                                                               response(data);
                                                                       }
                                                                   })
                                                                }',
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ) {
                                                                $(this).val( ui.item.label);
                                                                return false;
                                                            }',
                        'select' => 'js:function( event, ui ) {
                                                                $("#idPegawai").val(ui.item.pegawai_id); 
                                                                $("#ruangan_id").val(ui.item.ruangan_id);
                                                                $("#instalasi_id").val(ui.item.instalasi_id);
                                                                $("#namaPegawai").val(ui.item.label); 
                                                                return false;
                                                            }',
                    ),
                    'htmlOptions' => array(
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogPegawai', 'jsFunction'=>'dialogMenuPegawai()'),
                ));
                ?>
            </div>
        </div>
        <div class="control-group ">
            <label class='control-label'>Menu Diet</label>
            <div class="controls">
                <?php echo CHtml::hiddenField('idMenuDiet'); ?>
                <!--                <div class="input-append" style='display:inline'>-->
                <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'menuDiet',
                    'source' => 'js: function(request, response) {
                                                                   $.ajax({
                                                                       url: "' . Yii::app()->createUrl('ActionAutoComplete/menuDiet') . '",
                                                                       dataType: "json",
                                                                       data: {
                                                                           term: request.term,
                                                                       },
                                                                       success: function (data) {
                                                                               response(data);
                                                                       }
                                                                   })
                                                                }',
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ) {
                                                                $(this).val( ui.item.label);
                                                                return false;
                                                            }',
                        'select' => 'js:function( event, ui ) {
                                                                $("#idMenuDiet").val(ui.item.menudiet_id); 
                                                                $("#URT").val(ui.item.ukuranrumahtangga); 
                                                                return false;
                                                            }',
                    ),
                    'htmlOptions' => array(
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                        'class' => 'span2',
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogMenuDiet'),
                ));
                ?>

            </div>
        </div></td>
    <td width="104" style="padding-right:180px;">
        <div class="control-group ">
            <label class='control-label'>Jenis Waktu</label>
            <div>
                <?php                 
                $modJenisWaktu = JeniswaktuM::getJenisWaktu();
                $myData = CHtml::encodeArray(CHtml::listData($modJenisWaktu, 'jeniswaktu_id', 'jeniswaktu_id'));
                $myData = empty($myData) ? categories : $myData;
                ?>
                <fieldset>
                        <?php echo '<table>
                                                <tr>
                                                    <td>
                                                        '.Chtml::checkBoxList('jeniswaktu', false, CHtml::listData($modJenisWaktu, 'jeniswaktu_id', 'jeniswaktu_nama'), array('template'=>'<label class="checkbox inline">{input}{label}</label>', 'separator'=>'', 'style'=>'margin-left:2px;max-width:10px;','class'=>'span2 jeniswaktu', 'onkeypress' => "return $(this).focusNextInputField(event)")).'
                                                    </td>
                                                </tr>
                                                </table>'; ?>
           </fieldset>
            </div>
            
        </div>
        <div class="control-group ">
            <label class='control-label'>Jumlah</label>
            <div class="controls">
                <?php echo Chtml::textField('jumlah', 1, array('class' => 'span1 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>                
                <?php echo Chtml::dropDownList('URT', '', Ukuranrumahtangga::items(), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>                
                <?php
                echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>', array('onclick' => 'inputMenuDiet();return false;',
                    'class' => 'btn btn-primary',
                    'onkeypress' => "inputMenuDiet();return $(this).focusNextInputField(event)",
                    'rel' => "tooltip",
                    'title' => "Klik untuk menambahkan Menu Diet",));
                ?>
            </div>
        </div>
	</td>
  </tr>
</table>
        <style>
            .table thead tr th{
                vertical-align:middle;
            }
        </style>
        <table class="table table-bordered table-condensed" id="tableMenuDiet">
            <thead>
                <tr>
                    <th rowspan="2"><center><input type="checkbox" id="checkListUtama" name="checkListUtama" value="1" checked="checked" onclick="checkAll('cekList',this);hitungSemua();"></center></th>
                    <th rowspan="2"><center>Instalasi/<br/>Ruangan</center></th>
                    <th rowspan="2"><center>Nama Pegawai/Tamu</center></th>
                    <th rowspan="2"><center>Jenis Kelamin</center></th>
                    <th colspan="<?php echo count($modJenisWaktu); ?>"><center>Menu Diet</center></th>
                    <th rowspan="2"><center>Jumlah</center></th>
                    <th rowspan="2"><center>Satuan/URT</center></th>
                </tr>
                <tr>
                    <?php foreach($modJenisWaktu as $row){
                    echo '<th><center>'.$row->jeniswaktu_nama.'</center></th>';
                     } ?>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </fieldset>
    <div class="form-actions">
        <?php
                echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')) :
                        Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)'));
        ?>
        <?php 
             echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                    Yii::app()->createUrl($this->module->id.'/'.PesanmenudietT.'/indexPegawai'), 
                    array('class'=>'btn btn-danger',
                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));  
        ?>
	<?php 
$content = $this->renderPartial('gizi.views./tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
    </div>

    <?php $this->endWidget(); ?>
</legend>

<?php $this->renderPartial('gizi.views.pesanmenudietT._dialog', array('model' => $model)); ?>

<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPegawai',
    'options' => array(
        'title' => 'Daftar Pegawai',
        'autoOpen' => false,
        'modal' => true,
        'width' => 780,
        'height' => 600,
        'resizable' => false,
    ),
));

$modPegawai = new GZPegawairuanganV('search');
$modPegawai->unsetAttributes();
//$modPegawai->ruangan_id = 0;
if (isset($_GET['GZPegawairuanganV']))
    $modPegawai->attributes = $_GET['GZPegawairuanganV'];

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'gzpegawairuangan-v-grid',
    'dataProvider' => $modPegawai->searchDialog(),
    'filter' => $modPegawai,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'name' => 'ruangan_nama',
            'value' => '$data->ruangan_nama',
        ),
        'nama_pegawai',
        array(
            'name' => 'jeniskelamin',
            'filter' => JenisKelamin::items(),
            'value' => '$data->jeniskelamin',
        ),
        'alamat_pegawai',
         array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                            "id" => "selectPegawai",
                            "onClick" => "
                                $(\"#idPegawai\").val($data->pegawai_id);
                                $(\"#namaPegawai\").val(\'$data->nama_pegawai\');
                                $(\"#dialogPegawai\").dialog(\"close\");
//                                dialogMenuPegawai($data->pegawai_id);
                            "))',
       ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
?>


<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>
<?php
$totalPesan = CHtml::activeId($model, 'totalpesan_org');
$idInstalasi = CHtml::activeId($model, 'instalasi_id');
$idRuangan = CHtml::activeId($model, 'ruangan_id');
$jenisPesan = CHtml::activeId($model, 'jenispesanmenu');
$idBahanDiet = CHtml::activeId($model, 'bahandiet_id');
$namaPemesan = CHtml::activeId($model, 'nama_pemesan');
$pesanPegawai = Params::JENISPESANMENU_PEGAWAI;
$url = Yii::app()->createUrl('actionAjax/getMenuDietPegawai');
$jsx = <<< JS
    function inputMenuDiet(){
        var idPegawai = $('#idPegawai').val();
        var idMenuDiet = $('#idMenuDiet').val();
        var jumlah = $('#jumlah').val();
        var urt = $('#URT').val();
        var jeniswaktu = new Array();
        var idRuangan = $('#${idRuangan}').val();
        var idInstalasi = $('#${idInstalasi}').val();
        var instalasi = $('#instalasi_id').val();
        var ruangan = $('#ruangan_id').val();
        var jenisPesan = $('#${jenisPesan}').val();
        var pegawaiId = new Array();
        if (jenisPesan == ''){
            alert('Isi Jenis Pesan Menu');
            return false;
        }
        
        i = 0;
        $('.jeniswaktu').each(function(){
            value = $(this).val();
            if ($(this).is(':checked')){
                jeniswaktu[i]=value;
                i++;
            }
        });
        i = 0;
        $('.pegawaiId').each(function(){
            value = $(this).val();
            if ($(this).is(':checked')){
                pegawaiId[i]=value;
                i++;
            }
        });
        
        if (!jQuery.isNumeric(idInstalasi)){
            idInstalasi = instalasi;
        }
        if (!jQuery.isNumeric(idRuangan)){
            if (!jQuery.isNumeric(idPegawai) && (jenisPesan != "${pesanPegawai}")){
                alert('Ruangan untuk Tamu harus diisi');
                return false;
            }
            idRuangan = ruangan;
        }
        
        if ($('#jenisPesan').val() == '${pesanPegawai}'){
            if ((!jQuery.isNumeric(idPegawai))&&(pegawaiId.length < 1)){
                alert('Nama Pegawai Harus Diisi !');
                return false;
            }
        }
        
        if (!jQuery.isNumeric(idMenuDiet)){
            alert('Isi Makanan Diet yang dipilih !');
            return false;
        }
        else if (jeniswaktu.length < 1){
            alert('Isi Jenis Waktu !');
            return false;
        }
        else{
            $.post('${url}', {pegawaiId:pegawaiId, jeniswaktu:jeniswaktu, idPegawai:idPegawai, idMenuDiet:idMenuDiet, jumlah:jumlah, urt:urt, idRuangan:idRuangan, idInstalasi:idInstalasi}, function(data){
                $('#tableMenuDiet tbody').append(data);
                $("#tableMenuDiet tbody tr:last .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
                hitungSemua();
            }, 'json');
        }
        $('#${jenisPesan}').attr('disabled','disabled');
        clearAll(1);
    }
    
    function clearAll(code){
        var tempRuangan = $('#${idRuangan}').val();
        var tempInstalasi = $('#${idInstalasi}').val();
        var tempJenisPesan = $('#${jenisPesan}').val();
        
        $('#fieldsetMenuDiet div').find('input,select').each(function(){
           if ($(this).attr('type') == 'checkbox'){
                
            }
            else{
                $(this).val('');
            }
        });
        if (!jQuery.isNumeric(code)){
            $('#fieldsetMenuDiet #tableMenuDiet tbody').find('tr').each(function(){
                $(this).remove();
            });
        }
        if (jQuery.isNumeric(tempRuangan)){
            if ($('#cekRuangan').is(':checked')){
                 $.fn.yiiGridView.update('gzpegawairuangan-v-grid', {
                    data: "GZPegawairuanganV[ruangan_id]="+tempRuangan
                });
            }
        }
        
        $('#jumlah').val(1);
        $('#${idRuangan}').val(tempRuangan);
        $('#${idInstalasi}').val(tempInstalasi);
        $('#${jenisPesan}').val(tempJenisPesan);
        $('#jenisPesan').val(tempJenisPesan);
    }
        
    
    function hitungSemua(){
        var sekian = 1;
        noUrut = 1;
        jumlah = 0;         

        
        $('.cekList').each(function(){
            var value = $(this).parents('tr').find('.nama').val();
            $(this).parents('tr').find('[name*="PesanmenupegawaiT"]').each(function(){
                var nama = $(this).attr('name');
                data = nama.split('PesanmenupegawaiT[]');
                if (typeof data[1] === "undefined"){}else{
                    $(this).attr('name','PesanmenupegawaiT['+(noUrut-1)+']'+data[1]);
                }
            });
        
            if (value == ''){
                $(this).parents('tr').find('.nama').val('Tamu '+sekian);
                sekian++;
            }
            
            $(this).parents('tr').find('#checkList').attr('name','checkList['+(noUrut-1)+']');
            
            if($(this).is(':checked')){
                jumlah++;
            }
            noUrut++;
        });
        $('#${totalPesan}').val(jumlah);
    }
    
    function setRuangan(){
        if ($('#cekRuangan').is(':checked')){
            $('#groupRuangan').find('select').each(function(){
                $(this).removeAttr('disabled','disabled');
            });
        }
        else{
            $('#groupRuangan').find('select').each(function(){
                $(this).attr('disabled','disabled');
            });
        }
        clearAll();
    }
    
    function setJenisPesan(){
        var value = $('#${jenisPesan}').val();
        if (value == '${pesanPegawai}'){
            $("#pegawaiGroup").slideDown('slow');
        }
        else{
            $("#pegawaiGroup").slideUp('slow');
        }
    }
    
    function dialogMenuPegawai(){
        ruangan = $('#${idRuangan}').val();
        if(!jQuery.isNumeric(ruangan)){
            $.fn.yiiGridView.update('gzpegawairuangan-v-grid', {
                    data: "GZPegawairuanganV[ruangan_id]=0"
            });
        }
        else{
            $.fn.yiiGridView.update('gzpegawairuangan-v-grid', {
                    data: "GZPegawairuanganV[ruangan_id]="+ruangan
            });
        }
        if(!jQuery.isNumeric(ruangan)){
            alert('Isi ruangan terlebih dahulu');
            return false;
        }else{
            $('#dialogPegawai').dialog('open');
        }
    }
JS;
Yii::app()->clientScript->registerScript('head', $jsx, CClientScript::POS_HEAD);
?>

<?php Yii::app()->clientScript->registerScript('submit', '
    $.fn.yiiGridView.update(\'gzpegawairuangan-v-grid\', {
                    data: "GZPegawairuanganV[ruangan_id]=0"
            });
    $("form").submit(function(){
        jumlah = 0;
        $(".cekList").each(function(){
            if ($(this).is(":checked")){
                jumlah++;
            }
        });
        
        if (!jQuery.isNumeric($("#'.$idBahanDiet.'").val())){
            alert("'.CHtml::encode($model->getAttributeLabel('bahandiet_id')).' harus diisi !");
            return false;
        }
        else if ($("#'.$namaPemesan.'").val() == ""){
            alert("'.CHtml::encode($model->getAttributeLabel('nama_pemesan')).' harus diisi !");
            return false;
        }
        else if ($("#'.$jenisPesan.'").val() == ""){
            alert("'.CHtml::encode($model->getAttributeLabel('jenispesanmenu')).' harus diisi !");
            return false;
        }
        else if (jumlah < 1){
            alert("Pilih Menu Diet Pasien yang akan dipesan !");
            return false;
        }
    });
    
    setJenisPesan();
    clearAll();
', CClientScript::POS_READY); ?>
