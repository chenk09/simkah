<?php $this->renderPartial('_tabMenu',array()); ?>
<?php
$this->breadcrumbs=array(
	'gzbahanmakanan Ms'=>array('index'),
	'Manage',
);
$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Bahan Makanan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Bahan Makanan', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE))?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Bahan Makanan', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
    $('#BahanmakananM_golbahanmakanan_id').focus();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('bahan-makanan-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'bahan-makanan-m-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>"{summary}{pager}\n{items}",
	'columns'=>array(
		array(
                        'header'=>'ID',
                        'value'=>'$data->bahanmakanan_id',
                    ),
		array(
                        'name'=>'golbahanmakanan_id',
                        'filter'=>CHtml::listData($model->getGolBahanMakananItems(), 'golbahanmakanan_id','golbahanmakanan_nama'),
                        'value'=>'$data->golbahanmakanan->golbahanmakanan_nama',
                    ),
        array(
                        'name'=>'sumberdanabhn',
                        'filter'=>CHtml::listData($model->SumberDanaItems, 'lookup_name', 'lookup_value'),
                        'value'=>'$data->sumberdanabhn',
                    ),
        array(
                        'name'=>'jenisbahanmakanan',
                        'filter'=>CHtml::listData($model->JenisBahanMakananItems, 'lookup_name', 'lookup_value'),
                        'value'=>'$data->jenisbahanmakanan',
                    ),
        array(
                        'name'=>'kelbahanmakanan',
                        'filter'=>CHtml::listData($model->KelBahanMakananItems, 'lookup_name', 'lookup_value'),
                        'value'=>'$data->kelbahanmakanan',
                    ),
		'namabahanmakanan',
		/*
        'sumberdanabhn',
        'jenisbahanmakanan',
        'kelbahanmakanan',
		'jmlpersediaan',
		'satuanbahan',
		'harganettobahan',
		'hargajualbahan',
		'discount',
		'tglkadaluarsabahan',
		'jmlminimal',
		*/
                array(
                    'header'=>Yii::t('mds','View'),
                    'class'=>'bootstrap.widgets.BootButtonColumn',
                    'template'=>'{view}',
                ),
                array(
                    'header'=>Yii::t('mds','Update'),
                    'class'=>'bootstrap.widgets.BootButtonColumn',
                    'template'=>'{update}',
                ),
                array(
                    'header'=>'Hapus',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-trash\'></i> ", "javascript:deleteRecord($data->bahanmakanan_id)",array("id"=>"$data->bahanmakanan_id","rel"=>"tooltip","title"=>"Hapus"));',
                    'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php 
 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
            $content = $this->renderPartial('../tips/master2',array(),true);
            $this->widget('TipsMasterData',array('type'=>'admin','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        $url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#gzbahanmakanan-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
<script type="text/javascript">
    function deleteRecord(id){
        var id = id;
        var url = '<?php echo $url."/delete"; ?>';
        var answer = confirm('Yakin Akan Menghapus Data ini ?');
            if (answer){
                 $.post(url, {id: id},
                     function(data){
                        if(data.status == 'proses_form'){
                                $.fn.yiiGridView.update('bahan-makanan-m-grid');
                            }else{
                                alert('Data gagal dihapus karena data digunakan oleh Master Bahan Menu Diet atau Master Zat Bahan Makanan atau Menu Anamesa Diet.');
                            }
                },"json");
           }
    }
</script>