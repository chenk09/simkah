
<?php
$this->breadcrumbs=array(
	'Gzjenisdiet Ms'=>array('index'),
	$model->jenisdiet_id=>array('view','id'=>$model->jenisdiet_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Jenis Diet ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Jenis Diet', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Jenis Diet', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jenis Diet', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->jenisdiet_id))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jenis Diet', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gzjenisdiet-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#JenisdietM_jenisdiet_nama',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldRow($model,'jenisdiet_nama',array('class'=>'span3', 'onkeyup'=>"namaLain(this)", 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>25)); ?>
            <?php echo $form->textFieldRow($model,'jenisdiet_namalainnya',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>25)); ?>
            <?php echo $form->textareaRow($model,'jenisdiet_keterangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
            <?php /* echo $form->textareaRow($model,'jenisdiet_catatan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",)); */ ?>
                                <div>
                                <table>
                                 <tr>
                                 <td width="140px" style="text-align:right;">
                                    <?php echo Yii::t('mds','Catatan'); ?>
                                 </td>
                                 <td>
                                     <div style="width:215px;">
                                     <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$model,'attribute'=>'jenisdiet_catatan','name'=>'jenisdiet_catatan','toolbar'=>'mini','width'=>'50px','height'=>'100px')) ?>
                                     </div>
                                 </td>
                                 </tr>
                                </table>
                                </div>
            <?php echo $form->checkBoxRow($model,'jenisdiet_aktif', array('onkeypress'=>"return nextFocus(this,event,'btn_simpan','GZJenisdietM_jenisdiet_namalainnya')")); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                     array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan')); ?>
                                                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle"></i>')), 
                                                                     Yii::app()->createUrl($this->module->id.'/'.JenisdietM.'/admin'), 
                                                                     array('class'=>'btn btn-danger',
                                                                     'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    function namaLain(nama)
    {
        document.getElementById('JenisdietM_jenisdiet_namalainnya').value = nama.value.toUpperCase();
    }
</script>