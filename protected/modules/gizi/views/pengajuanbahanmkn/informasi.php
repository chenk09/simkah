<?php 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gzpengajuanbahanmkn-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
    <legend class="rim2">Informasi Pengajuan Bahan Makanan</legend>


<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'gzpengajuanbahanmkn-grid',
	'dataProvider'=>$model->searchInformasi(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'pengajuanbahanmkn_id',
//		array(
//                        'name'=>'pengajuanbahanmkn_id',
//                        'value'=>'$data->pengajuanbahanmkn_id',
//                        'filter'=>false,
//                ),
//		'terimabahanmakan_id',
                'tglpengajuanbahan',
                'nopengajuan',
                'sumberdanabhn',
                'alamatpengiriman',
                array(
                    'name'=>'idpegawai_mengajukan',
                    'value'=>'$data->getNamaPegawai($data->idpegawai_mengajukan)',
                ),
                array(
                    'name'=>'idpegawai_mengajukan',
                    'value'=>'$data->getNamaPegawai($data->idpegawai_mengetahui)',
                ),                
                'totalharganetto',
                'tglmintadikirim',
                'keterangan_bahan',
                array(
                    'header'=>'Detail',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",  Yii::app()->controller->createUrl("/gizi/Pengajuanbahanmkn/detailPengajuan",array("id"=>$data->pengajuanbahanmkn_id)),array("id"=>"$data->pengajuanbahanmkn_id","target"=>"frameDetail","rel"=>"tooltip","title"=>"Klik untuk Detail Pengajuan Barang", "onclick"=>"window.parent.$(\'#dialogDetail\').dialog(\'open\')"));','htmlOptions'=>array('style'=>'text-align: center')
                ),
                array(
                    'header'=>'Terima Bahan',
                    'type'=>'raw',
                    'value'=>'(\'empty($data->terimabahanmakan_id)\')? CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/gizi/Terimabahanmakan/index",array("idPengajuan"=>$data->pengajuanbahanmkn_id)),array("rel"=>"tooltip","title"=>"Klik untuk Melanjutkan ke Penerimaan")) : "Telah Diterima"','htmlOptions'=>array('style'=>'text-align: center')
                ),
//		'ruangan_id',
//		'supplier_id',	
/*        	'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		*/
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
    <div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
</fieldset>

<?php 
$js = <<< JSCRIPT
function openDialog(id){
    $('#dialogDetail').dialog('open');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('head',$js,CClientScript::POS_HEAD);                        
?>

<?php
//========= Dialog untuk Melihat detail Pengajuan Bahan Makanan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDetail',
    'options' => array(
        'title' => 'Detail Pengajuan Bahan Makanan',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

echo '<iframe src="" name="frameDetail" width="100%" height="500">
</iframe>';

$this->endWidget();
?>