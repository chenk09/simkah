<?php
    if(!empty($_GET['id'])){
?>
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
<?php } ?>
<fieldset>
    <legend class="rim2">
        Transaksi Pengajuan Bahan Makanan
    </legend>
</fieldset>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/accounting.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'id' => 'gzpengajuanbahanmkn-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
        'focus' => '#',
            ));
    ?>

    <p class="help-block"><?php echo Yii::t('mds', 'Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($model, $modDetails, $modDetailPengajuan); ?>
    <table>
        <tr>
            <td width="50%">
                <?php //echo $form->textFieldRow($model,'terimabahanmakan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->dropDownListRow($model, 'supplier_id', CHtml::listData(SupplierM::model()->findAll('supplier_aktif = true'), 'supplier_id', 'supplier_nama'), array('empty' => '-- Pilih --', 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model, 'nopengajuan', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
                <?php //echo $form->textFieldRow($model,'tglpengajuanbahan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglpengajuanbahan', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglpengajuanbahan',
                            'mode' => 'datetime',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                'maxDate' => 'd',
                            ),
                            'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                        ));
                        ?>
                        <?php echo $form->error($model, 'tglpengajuanbahan'); ?>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($model, 'sumberdanabhn', SumberdanabahanM::items(), array('empty' => '-- Pilih --', 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
                <?php echo $form->textAreaRow($model, 'alamatpengiriman', array('rows' => 6, 'cols' => 50, 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            </td>
            <td>
                <?php echo $form->dropDownListRow($model, 'idpegawai_mengetahui', CHtml::listData(PegawaiM::model()->findAll('pegawai_aktif = true'), 'pegawai_id', 'nama_pegawai'), array('empty' => '-- Pilih --', 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->dropDownListRow($model, 'idpegawai_mengajukan', CHtml::listData(PegawaiM::model()->findAll('pegawai_aktif = true'), 'pegawai_id', 'nama_pegawai'), array('empty' => '-- Pilih --', 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($model, 'keterangan_bahan', array('rows' => 6, 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>

                <?php //echo $form->textFieldRow($model,'tglmintadikirim',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);"));  ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglmintadikirim', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglmintadikirim',
                            'mode' => 'datetime',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                // S'maxDate' => 'd',
                            ),
                            'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                        ));
                        ?>
                        <?php echo $form->error($model, 'tglmintadikirim'); ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>


        <div class="control-group ">
            <label class="control-label">Nama Bhn. Makanan <font color="red"> * </font></label>
            <div class="controls">
                <?php echo CHtml::hiddenField('idBahan'); ?>
                <!--                <div class="input-append" style='display:inline'>-->
                <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'namaBahan',
                    'source' => 'js: function(request, response) {
                                                           $.ajax({
                                                               url: "' . Yii::app()->createUrl('ActionAutoComplete/BahanMakanan') . '",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                                   idSumberDana: $("#idSumberDana").val(),
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ) {
                                                        $(this).val( ui.item.label);
                                                        return false;
                                                    }',
                        'select' => 'js:function( event, ui ) {
                                                        $("#idBahan").val(ui.item.bahanmakanan_id); 
                                                        $("#qty").val(1); 
                                                        $("#satuanbahan").val(ui.item.satuanbahan);
                                                        return false;
                                                    }',
                    ),
                    'htmlOptions' => array(
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogBahanMakanan'),
                ));
                ?>
            </div>
        </div>

		

        <div class="control-group ">
            <label class="control-label">Qty</label>
            <div class="controls">
                <?php echo CHtml::textField('qty', '', array('class' => 'span1 numbersOnly number', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>
                <?php echo CHtml::dropDownList('satuanbahan', '', Satuanbahan::items(), array('empty' => '-- Pilih --', 'class' => 'span1')); ?>
                <?php echo CHtml::textField('ukuran', '', array('class' => 'span2', 'placeholder' => 'Ukuran')); ?>
                <?php echo CHtml::textField('merk', '', array('class' => 'span2', 'placeholder' => 'Merk')); ?>
                <?php
                echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>', array('onclick' => 'inputBahanMakanan();return false;',
                    'class' => 'btn btn-primary numbersOnly',
                    'onkeypress' => "inputBahanMakanan();return $(this).focusNextInputField(event)",
                    'rel' => "tooltip",
                    'title' => "Klik untuk menambahkan Bahan Makanan",));
                ?>
            </div>
        </div>
        <table class="table table-bordered table-condensed" id="tableBahanMakanan">
            <thead>
                <tr>
                    <th><input type="checkbox" id="checkListUtama" name="checkListUtama" value="1" checked="checked" onclick="checkAll('cekList',this);hitungSemua();"></th>
                    <th>No.Urut</th>
                    <th>Golongan</th>
                    <th>Jenis</th>
                    <th>Kelompok</th>
                    <th>Nama</th>
                    <th>Jumlah Persediaan</th>
                    <th>Satuan</th>
                    <th>Harga Netto</th>
                    <th>Harga Jual</th>
                    <th>Diskon</th>
                    <th>Tgl Kadaluarsa</th>
                    <th>Qty</th>
                    <th>Sub Total</th>
                    <th>Batal</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
            <tfoot>
                <tr>
                    <td colspan='13'><div class='pull-right'>Total Harga</div></td>
                    <td><?php echo $form->textField($model, 'totalharganetto', array('readonly' => true, 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?></td>

                </tr>
            </tfoot>
        </table>
    
    <?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);"));  ?>
    <div class="form-actions">
        <?php
        echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds', '{icon} Simpan', array('{icon}' => '<i class="icon-ok icon-white"></i>')) :
                        Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)'));
        ?>
			 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
	<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	

    </div>

    <?php $this->endWidget(); ?>


<?php
$totalHarga = CHtml::activeId($model, 'totalharganetto');
$urlBahan = Yii::app()->createUrl('actionAjax/getBahanMakanan');
$js = <<<JS
    function inputBahanMakanan(){
        var id = $('#idBahan').val();
        var qty= $('#qty').val();
        var ukuran = $('#ukuran').val();
        var merk = $('#merk').val();
        var satuanbahan = $('#satuanbahan').val();

        if (jQuery.isNumeric(id)){
        if(cekList(id)==true){    
              $.post("${urlBahan}", {id:id, qty:qty, ukuran:ukuran, merk:merk, stuanbahan:satuanbahan},
                function(data){

                    $('#tableBahanMakanan tbody').append(data.tr);
                    hitungSemua();
                    $("#tableBahanMakanan tbody tr:last .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
                    $("#tableBahanMakanan tbody tr:last .satuanbahan").val(satuanbahan);

              }, "json");
              clear();
        }   
        }
        else{
            alert('Isi Data dengan Benar');
        }
    }
    
    function hitungSemua(){
        noUrut = 1;
        value = 0;
        $('.noUrut').each(function(){
            $(this).val(noUrut);
            noUrut++;
            if ($(this).parents('tr').find('#checkList').is(':checked')){
                val = parseFloat($(this).parents('tr').find('.subNetto').val());
                value += val;
            }
            
            $('.cekList').each(function(){
               if ($(this).is(':checked')){

                     $(this).parents('tr').find('.cek').val(1);
                }else{
                    $(this).parents('tr').find('.cek').val(0);
                }
            });
        });
        
        $('#${totalHarga}').val(value);
    }
    
    function hitung(obj){
        var netto = $('#PengajuanbahandetailT_harganettobhn').val();
        var jml = $(obj).val();
        $(obj).parents('tr').find('.subNetto').val(netto*jml);
        hitungSemua();
    }
    function remove(obj) {
        $(obj).parents('tr').remove();
        hitungSemua();
    }
    
    function cekList(id){
        x = true;
        $('.bahanmakanan_id').each(function(){
            if ($(this).val() == id){
                alert('Daftar Bahan Makanan telah ada di List');
                clear();
                x = false;
            }
        });
        return x;   
    }   
    function clear(){
        $('#namaBahan').val('');
        $('#qty').val('');
        $('#satuanbahan').val('');
        $('#ukuran').val('');
        $('#merk').val('');
    }
    
JS;
Yii::app()->clientScript->registerScript('fungsi', $js, CClientScript::POS_HEAD);
?>

<?php
//========= Dialog buat cari Bahan Makanan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogBahanMakanan',
    'options' => array(
        'title' => 'Bahan Makanan',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

$modBahanMakanan = new GZBahanmakananM('search');
$modBahanMakanan->unsetAttributes();
if (isset($_GET['GZBahanmakananM']))
    $modBahanMakanan->attributes = $_GET['GZBahanmakananM'];

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'gzbahanmakanan-m-grid',
    'dataProvider' => $modBahanMakanan->search(),
    'filter' => $modBahanMakanan,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        ////'bahanmakanan_id',
//        array(
//                        'name'=>'bahanmakanan_id',
//                        'value'=>'$data->bahanmakanan_id',
//                        'filter'=>false,
//                ),
        array(
            'name' => 'golbahanmakanan_id',
            'filter' => CHtml::listData(GolbahanmakananM::model()->findAll('golbahanmakanan_aktif = true'), 'golbahanmakanan_id', 'golbahanmakanan_nama'),
            'value' => '$data->golbahanmakanan->golbahanmakanan_nama',
        ),
//        'golbahanmakanan.golbahanmakanan_nama',
//        'sumberdanabhn',
        'jenisbahanmakanan',
        'kelbahanmakanan',
        'namabahanmakanan',
        'jmlpersediaan',
        'satuanbahan',
        'harganettobahan',
        'hargajualbahan',
        'discount',
        'tglkadaluarsabahan',
//        'jmlminimal',
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectBahan",
                                    "onClick" => "$(\'#idBahan\').val($data->bahanmakanan_id);
                                    $(\'#satuanbahan\').val(\'$data->satuanbahan\');
                                    $(\'#qty\').val(1); 
                                    $(\'#namaBahan\').val(\'$data->jenisbahanmakanan - $data->namabahanmakanan - $data->jmlpersediaan\');
                                    $(\'#dialogBahanMakanan\').dialog(\'close\');return false;"))',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));


$this->endWidget();
?>
<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>

<?php Yii::app()->clientScript->registerScript('submit', '
    $("form").submit(function(){
        sumberDana = $("#'.CHtml::activeId($model, 'sumberdanabhn').'").val();
        jumlah = 0;
        
        if (sumberDana == ""){
            alert("'.CHtml::encode($model->getAttributeLabel('sumberdanabhn')).' harus diisi !");
            return false;
        }

        $(".cekList").each(function(){
            if ($(this).is(":checked")){
                jumlah++;
            }
        });
        
        
        if (jumlah < 1){
            alert("Pilih bahan makanan yang akan diajukan !");
            return false;
        }
    });
', CClientScript::POS_READY); ?>