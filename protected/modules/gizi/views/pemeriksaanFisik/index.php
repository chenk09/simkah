<?php
    $this->breadcrumbs=array(
            'Anamnesa',
    );
    $this->widget('bootstrap.widgets.BootAlert');
?>

<?php 
if(empty($idPasienAdmisi))
    $this->renderPartial('/_ringkasDataPasienPendaftaran',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));
else
    $this->renderPartial('/_ringkasDataPasienPendaftaranRI',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));
?>
<?php $this->renderPartial('/_tabulasi', array('modPendaftaran'=>$modPendaftaran)); ?>

<?php
    $this->widget('application.extensions.moneymask.MMask',array(
        'element'=>'.number',
        'config'=>array(
            'defaultZero'=>true,
            'allowZero'=>true,
            'decimal'=>',',
            'thousands'=>'.',
            'precision'=>0,
        )
    ));

    $this->widget('application.extensions.moneymask.MMask',array(
        'element'=>'.numbers',
        'config'=>array(
            'defaultZero'=>true,
            'allowZero'=>true,
            'allowDecimal'=>true,
            'decimal'=>'.',
            'thousands'=>',',
            'precision'=>0,
        )
    ));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rjpemeriksaan-fisik-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',),
        'focus'=>'#',
)); ?>
<style>
    .groupUkurans{
        display:inline;
    }
    </style>
    <?php  

    if(!$modPemeriksaanFisik->isNewRecord){
?>
    <div class="mds-form-message error">
                    <?php Yii::app()->user->setFlash('success',"Data berhasil disimpan"); ?>
                </div>
<? } ?>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($modPemeriksaanFisik); ?>
        <table class="table-condensed">
            <tr>
                <td>
                    <?php echo CHtml::hiddenField('url',$this->createUrl('',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),array('readonly'=>TRUE));?>
                    <?php echo CHtml::hiddenField('berubah','',array('readonly'=>TRUE));?>
                    <?php echo $form->labelEx($modPemeriksaanFisik,'tglperiksafisik', array('class'=>'control-label')) ?>
                    <div class="controls">  
                    <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPemeriksaanFisik,
                                         'attribute'=>'tglperiksafisik',
                                         'mode'=>'datetime',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                         'maxDate'=>'d',   
                                             ),
                                         'htmlOptions'=>array('readonly'=>true, 'class'=>'dtPicker3',
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                      </div> 
                </td>
                <td>
                      <?php echo $form->dropDownListRow($modPemeriksaanFisik,'pegawai_id',CHtml::listData($modPemeriksaanFisik->DokterItemsKonsul, 'pegawai_id', 'nama_pegawai'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'empty'=>'--Pilih--'));?>
                      
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <?php echo $form->dropDownListRow($modPemeriksaanFisik,'ahligizi', CHtml::listData($modPemeriksaanFisik->getAhliGiziItems(), 'pegawai_id', 'nama_pegawai'),
                                                            array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </td>
            </tr>
        </table>
        
        <style>
            .hoveringIcon:hover{
                background-color: #FFA0A2;
                cursor: pointer;
                -webkit-border-radius:1px;
                -moz-border-radius:1px;
                -o-border-radius:1px;
                -border-radius:1px;
            }
            </style>
        <table class="table-condensed">
            <tr>
                <td>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'detaknadi',array('label'=>'<i class="icon-facetime-video hoveringIcon" onclick="getfromDevice();"></i> Detak Nadi','class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'detaknadi',array('class'=>'span2 number numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                         /Menit
                        </div>
                    </div>

                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'tekanandarah',array('class'=>'control-label'));?>
                        <div class="controls">
                            <?php
                                $this->widget('CMaskedTextField', array(
                                'model' => $modPemeriksaanFisik,
                                'attribute' => 'tekanandarah',
                                'mask' => '999 / 999',
                                'placeholder'=>'0',
                                'htmlOptions' => array('onfocus'=>'change(this);', 'onblur'=>'change(this);', 'class'=>'span2', 'style'=>'width:60px;','onkeypress'=>"return $(this).focusNextInputField(event)",'onkeyup'=>'getTekananDarah(this);')
                                ));
                                ?>
                             <?php //echo $form->textField($modPemeriksaanFisik,'tekanandarah',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                         /MmHg <?php echo "    ";?>
                         <?php
                                $this->widget('CMaskedTextField', array(
                                'model' => $modPemeriksaanFisik,
                                'attribute' => 'td_systolic',
                                'mask' => '999',
                                'placeholder'=>'0',
                                'htmlOptions' => array('onfocus'=>'change(this);', 'onblur'=>'change(this);getText();', 'class'=>'span1 number systolic', 'onkeypress'=>"return $(this).focusNextInputField(event)",'onkeyup'=>'getTekananDarah(this);')
                                ));
                                ?>Mm
                                <?php echo $form->textField($modPemeriksaanFisik,'td_diastolic',array('onblur'=>'','class'=>'span1 number numbersOnly diastolic', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>3, 'onkeyup'=>'returnValue(this)'));?>
                         <?php
//                                $this->widget('CMaskedTextField', array(
//                                'model' => $modPemeriksaanFisik,
//                                'attribute' => 'td_diastolic',
//                                'mask' => '999',
//                                'placeholder'=>'0',
//                                'htmlOptions' => array('onfocus'=>'change(this);', 'onblur'=>'change(this);getText();', 'class'=>'span1 diastolic', 'onkeypress'=>"return $(this).focusNextInputField(event)",'onkeyup'=>'getTekananDarah(this);')
//                                ));
                                ?>Hg
                         <?php //echo $form->textField($modPemeriksaanFisik,'td_systolic',array('class'=>'span1 numbersOnly systolic', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>3, 'onkeyup'=>'returnValue(this)'));?>
                         
                        </div>
                    </div>
                    <div class="control-group ">
                        
                        <div class="controls">
                             <?php echo CHtml::textField('tekananDarah','', array('class'=>'span3 ', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);"));?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'meanarteripressure',array('class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'meanarteripressure',array('readonly'=>true, 'class'=>'span2 number numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'suhutubuh',array('class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'suhutubuh',array('class'=>'span2 numbers', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                         &#176 Celcius 
                        </div>
                    </div>
                   
                </td>
                <td>
                     <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'tinggibadan_cm',array('class'=>'control-label','style'=>'width:75px'));?>
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'beratbadan_kg',array('class'=>'control-label','style'=>'width:80px'));?>
                        <div class="controls">
                            <div class="groupUkurans">
                                <?php echo $form->textField($modPemeriksaanFisik,'tinggibadan_cm',array('class'=>'span1 number numbersOnly tinggibadan', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10,'size'=>3));?>
                                <?php echo $form->hiddenField($modPemeriksaanFisik,'tinggibadan_cm',array('class'=>'span1 number numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10,'size'=>3));?>
                                <?php echo CHtml::dropDownList('meter', '100', array('100'=>'Cm', '0.01'=>'M'), array('style'=>'width:50px;','class'=>'span1', 'onchange'=>'gantiJumlah(this)')); ?>
                            </div>
                            <div class="groupUkurans">
                             <?php echo $form->textField($modPemeriksaanFisik,'beratbadan_kg',array('class'=>'span1 number numbersOnly beratbadan', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10,'size'=>3));?>
                             <?php echo $form->hiddenField($modPemeriksaanFisik,'beratbadan_kg',array('class'=>'span1 number numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10,'size'=>3));?>
                             <?php echo CHtml::dropDownList('gram', '0.001', array('1000'=>'Gr', '0.001'=>'Kg'), array('class'=>'span1', 'onchange'=>'gantiJumlah(this)')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'bb_ideal',array('class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'bb_ideal',array('class'=>'span2 number numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10, 'readonly'=>true)).' ';?>Kg
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class='control-label'>Index Masa Tubuh</label>
                        <div class="controls">
                             <?php echo CHtml::textField('imtValue', '', array('readonly'=>true, 'class'=>'span1'));?>
                             <?php echo CHtml::textField('imt', '', array('readonly'=>true, 'class'=>'span2'));?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'pernapasan',array('class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'pernapasan',array('class'=>'span2 number numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                         /Menit
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'Lila',array('class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'Lila',array('class'=>'span2 numbers ', 'style'=>'width:50px;','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                         cm (Untuk Pasien Hamil)
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'LingkarPinggan',array('class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'LingkarPinggang',array('class'=>'span2 numbers ', 'style'=>'width:50px;','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                         cm (Untuk Pasien Obgyn)
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'LingkarPinggul',array('class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'LingkarPinggul',array('class'=>'span2 numbers ', 'style'=>'width:50px;','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                         cm (Untuk Pasien Obesitas)
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'TebalLemak',array('class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'TebalLemak',array('class'=>'span2 numbers ', 'style'=>'width:50px;','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                         cm (Untuk Pasien Obesitas) 
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->LabelEx($modPemeriksaanFisik,'TinggiLutut',array('class'=>'control-label'));?>
                        <div class="controls">
                             <?php echo $form->textField($modPemeriksaanFisik,'TinggiLutut',array('class'=>'span2 numbers ', 'style'=>'width:50px;','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10));?>
                         cm (Untuk Pasien Usia Lanjut / Bongkok)
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        
        <fieldset class="">
<!--            <legend class="accord1" style="width:460px;">-->
<!--                             <?php //echo CHtml::checkBox('pemeriksaanFisik',false, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?> Bagian Yang Diperiksa-->
                  
<!--            </legend>-->
                <div id="divBagianYAngDiperiksa" class="toggle control-group" style="display: none">

                <table class="table-condensed">
                    <tr>
                        <td>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'kulit',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'mata',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'hidung',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'tenggorokan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'jantung',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </td>
                        <td>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'kepala',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'telinga',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'leher',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'payudara',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->textAreaRow($modPemeriksaanFisik,'abdomen',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </td>
                    </tr>    
                </table>
                </div>       
        </fieldset>
        
                <div class="form-actions">
                                        <?php echo CHtml::htmlButton($modPemeriksaanFisik->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','id'=>'btn_simpan')); ?>
              <?php 
           $content = $this->renderPartial('gizi.views.tips.tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
        ?>
			    </div>

<?php $this->endWidget(); ?>

<?php
$diastolic = CHtml::activeId($modPemeriksaanFisik, 'td_diastolic');
$js = <<< JS

    if ($('#${diastolic}').val().length == 2){
        $('#${diastolic}').val('0'+$('#${diastolic}').val());
    };

    $('#${diastolic}').blur(function(){
        var jumlahPanjang = $(this).val().length;
        var tambah = '';
        for (i=jumlahPanjang; i<3;i++){
            tambah = tambah+'0';
        }
        $(this).val(tambah+$(this).val());
        change($(this));
    });

       $('#namaGCS').attr('value','Hasil Metode GCS');
    $('#pemeriksaanFisik').change(function(){
            if ($(this).is(':checked')){
                    $('#divBagianYAngDiperiksa input').removeAttr('disabled');
                    $('#divBagianYAngDiperiksa select').removeAttr('disabled');
            }else{
                    $('#divBagianYAngDiperiksa input').attr('disabled','true');
                    $('#divBagianYAngDiperiksa select').attr('disabled','true');
                    $('#divBagianYAngDiperiksa input').attr('value','');
                    $('#divBagianYAngDiperiksa select').attr('value','');
            }
            $('#divBagianYAngDiperiksa').slideToggle(500);
        });
    //===============Awal untu Mengecek Form Sudah DiUbah Atw Belum====================    
        $(":input").keyup(function(event){
                $('#berubah').val('Ya');
             });
        $(":input").change(function(event){
                $('#berubah').val('Ya');
             });  
        $(":input").click(function(event){
                $('#berubah').val('Ya');
             });  
    //================Akhir Untuk Mengecek  Form Sudah DiUbah Atw Belum===================         
    $('.groupUkurans').find('input').keyup(function(){
        gantiHidden();
        getBeratBadanIdeal();
        getBMI();
    });

    getBMI();
    getText();
JS;
Yii::app()->clientScript->registerScript('cekform',$js,CClientScript::POS_READY);
?>

<?php
$urlgetMetodeGCS=Yii::app()->createUrl('ActionAjax/GetMetodeGCS');
$idTekananDarah = CHtml::activeId($modPemeriksaanFisik, 'tekanandarah');
$systolic = CHtml::activeId($modPemeriksaanFisik, 'td_systolic');
$diastolic = CHtml::activeId($modPemeriksaanFisik, 'td_diastolic');
$idDetakNadi = CHtml::activeId($modPemeriksaanFisik, 'detaknadi');
$getTextTekananDarah = Yii::app()->createUrl('ActionAjax/GetTextTekananDarah');
$arteriPressure = CHtml::activeId($modPemeriksaanFisik, 'meanarteripressure');
$beratBadan = CHtml::activeId($modPemeriksaanFisik, 'beratbadan_kg');
$tinggiBadan = CHtml::activeId($modPemeriksaanFisik, 'tinggibadan_cm');
$jenisKelamin = CHtml::activeId($modPasien, 'jenis_kelamin');
$jenisKelaminPerempuan = Params::JENIS_KELAMIN_PEREMPUAN;
$beratBadanIdeal = CHtml::activeId($modPemeriksaanFisik, 'bb_ideal');
$getBMIText = Yii::app()->createUrl('actionAjax/getBMIText');
$getfromDevice = Yii::app()->createUrl('actionAjax/getfromDevice');
$js = <<< JS

//==================================================Validasi===============================================
//*Jangan Lupa Untuk menambahkan hiddenField dengan id "berubah" di setiap form
//* hidden field dengan id "url"
//*Copas Saja hiddenfield di Line 36 dan 35
//* ubah juga id button simpannya jadi "btn_simpan"


function palidasiForm(obj)
{
    var berubah = $('#berubah').val();
    if(berubah=='Ya'){
       if(confirm('Apakah Anda Akan menyimpan Perubahan Yang Sudah Dilakukan?')){
           $('#url').val(obj);
           $('#btn_simpan').click();
       }
    }      
}

function ubahWarna(obj)   
{ 
    $(obj).attr("class","btn btn-success");
}


function kembaliWarna(obj)
{
   $(obj).attr("class","btn");
}

function SetNilai(obj)
{
    idTombol=obj.id;
    i=0;
    if(idTombol=='E'){
        $('#RJPemeriksaanFisikT_gcs_eye').val(obj.value);
    }else if(idTombol=='M'){
        $('#RJPemeriksaanFisikT_gcs_motorik').val(obj.value);
    }else if(idTombol=='V'){
        $('#RJPemeriksaanFisikT_gcs_verbal').val(obj.value);
    } 
    
    $('#divTombol #'+idTombol).each(function() {
        $(this).attr("class","btn"); 
    });

//    jumlah=$('#divTombol #'+idTombol).length;

$(obj).attr("class","btn btn-success"); 
    $(obj).removeAttr('onmouseout');
    $(obj).removeAttr('onmouseover');

    hitungCGS();
}

function hitungCGS()
{
    gcs_eye =  $('#RJPemeriksaanFisikT_gcs_eye').val();
    gcs_motorik =  $('#RJPemeriksaanFisikT_gcs_motorik').val();
    gcs_verbal =  $('#RJPemeriksaanFisikT_gcs_verbal').val();    
    if((gcs_eye!='') && (gcs_motorik!='') &&(gcs_verbal!='')){
        $.post("${urlgetMetodeGCS}",{gcs_eye: gcs_eye,gcs_motorik:gcs_motorik,gcs_verbal:gcs_verbal},
        function(data){
               if(data.pesan==null){
                 $('#RJPemeriksaanFisikT_gcs_id').val(data.idGCS);
                 $('#namaGCS').val(data.namaGCS);
               }else{
                    alert(data.pesan);
               }    
        },"json");
    }
}    

function getTekananDarah(obj){
    var hasil = $(obj).val();
    var data = hasil.split(' / ');

    data[0] = data[0].replace(/_/gi, "0");
    data[1] = data[1].replace(/_/gi, "0");
    $('#${systolic}').val(data[0]);
    $('#${diastolic}').val(data[1]);
}

function returnValue(obj){
    var value = $(obj).val();
    var attrID = $(obj).attr('id');
    var td = $('#${idTekananDarah}').val();
    var splitTD = td.split(' / ');
    
    if (attrID == '${diastolic}'){
        splitTD[0] = splitTD[0].replace(/_/gi, "0");
        $('#${idTekananDarah}').val(splitTD[0]+' / '+value);
    }
    else if (attrID == '${systolic}'){
        splitTD[1] = splitTD[1].replace(/_/gi, "0");
        $('#${idTekananDarah}').val(value+' / '+splitTD[1]);
    }
}

function change(obj){
    var value = $(obj).val();
    var hasil = value.replace(/_/gi, "0");
    
    if (value == ''){
        $(obj).val('000 / 00')
    }else{
        $(obj).val(hasil);
        returnValue(obj);
    }
    
}

function getText(){
    var dias = parseFloat($('#${diastolic}').val());
    var sys = parseFloat($('#${systolic}').val());
    var arteri = ((sys+(2*dias))/3);
    
    if (jQuery.isNumeric(dias)){
        if (jQuery.isNumeric(sys)){
            $.post('${getTextTekananDarah}', {diastolic:dias, systolic:sys}, function(data){
                if (data.text == null){
                    $('#tekananDarah').val('Tekanan Darah Tidak Ditemukan');
                } else {
                    $('#tekananDarah').val(data.text);
                }
            },'json');
            $('#${arteriPressure}').val(arteri);
        }
    }
}

function gantiJumlah(obj){
    var value = parseFloat($(obj).val());
    var teman = $(obj).parent('.groupUkurans').find('input[type="text"]');
    var valueTeman = parseFloat(teman.val());
    var hasil;

    hasil = valueTeman*value;
    teman.val(hasil);
}

function gantiHidden(){
    var defaultBB = parseFloat(0.001);
    var defaultTB = parseFloat(100);
    var valueBB = parseFloat($('#${beratBadan}').val());
    var valueTB = parseFloat($('#${tinggiBadan}').val());

    if ($('#gram').val() != defaultBB){
        $('#${beratBadan}').parent('.groupUkurans').find('input[type="hidden"]').val(valueBB*defaultBB);
    }
    else{
        $('#${beratBadan}').parent('.groupUkurans').find('input[type="hidden"]').val(valueBB);
    }
    
    if ($('#meter').val() != defaultTB){
        $('#${tinggiBadan}').parent('.groupUkurans').find('input[type="hidden"]').val(valueTB*defaultTB);
    }
    else{
        $('#${tinggiBadan}').parent('.groupUkurans').find('input[type="hidden"]').val(valueTB);
    }
}            
            
function getBeratBadanIdeal(){
    var beratBadan = parseFloat($('#${beratBadan}').val());
    var tinggiBadan = parseFloat($('#${tinggiBadan}').parent('.groupUkurans').find('input[type="hidden"]').val());
    var jenisKelamin = $('#${jenisKelamin}').val();
    var hasil;
    if (jenisKelamin == "${jenisKelaminPerempuan}"){
        hasil = (tinggiBadan - 100) - ((15/100)*(tinggiBadan-100));
        if (hasil < 0){
            hasil = 0;
        }
        $('#${beratBadanIdeal}').val(hasil);
    }
    else{
        hasil = (tinggiBadan - 100) - ((10/100)*(tinggiBadan-100));
        if (hasil < 0){
            hasil = 0;
        }
        $('#${beratBadanIdeal}').val(hasil);
    }
}

function getBMI(){
    var beratBadan = parseFloat($('#${beratBadan}').parent('.groupUkurans').find('input[type="hidden"]').val());
    var tinggiBadan = parseFloat($('#${tinggiBadan}').parent('.groupUkurans').find('input[type="hidden"]').val());
    var hasil;
    
    hasil = (beratBadan/((tinggiBadan*tinggiBadan)/10000));
    if (jQuery.isNumeric(hasil)){
        $.post('${getBMIText}', {bmi:hasil}, function(data){
            $('#imt').val(data.text);
            $('#imtValue').val(Math.floor(hasil));
        },'json');
    }
}

function getfromDevice(){
    $.post('${getfromDevice}',{},function(dataz){
        $('#${idDetakNadi}').val(dataz.detaknadi);
        $('#${idTekananDarah}').val(dataz.tekanandarah);
        $('#${systolic}').val(dataz.sys);
        $('#${diastolic}').val(dataz.dias);
            getText();
    }, 'json');
    
    
}

function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    $('.numbers').each(function(){this.value = unformatNumber(this.value)});
    $('.numbersOnly').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
JS;
Yii::app()->clientScript->registerScript('validasi',$js,CClientScript::POS_HEAD);

$js = <<< JS
    $('.numbersOnly').keyup(function() {
        var d = $(this).attr('numeric');
        var value = $(this).val();
        var orignalValue = value;
        value = value.replace(/[0-9]*/g, "");
        var msg = "Only Integer Values allowed.";

        if (d == 'decimal') {
        value = value.replace(/\./, "");
        msg = "Only Numeric Values allowed.";
        }

        if (value != '') {
        orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
        $(this).val(orignalValue);
        }
    });
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>   
<?php } ?>