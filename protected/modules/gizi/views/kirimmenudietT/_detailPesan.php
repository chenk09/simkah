<?php echo CHtml::css('input[type="checkbox"].span2{width:13px;}'); ?>
<fieldset id="fieldsetMenuDiet">
    <?php if (!isset($modDetailPesan)){ ?>
        <legend class="rim">Detail Menu Diet</legend>
		<table width="200" border="0">
  <tr>
    <td width="80">
        <div class="control-group ">
            <label class='control-label'><?php echo CHtml::encode($model->getAttributeLabel('ruangan_id')); ?><span class="required">*</span></label>
            <div class="controls">
                <?php
                echo $form->dropDownList($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50,
                    'ajax' => array('type' => 'POST',
                        'url' => Yii::app()->createUrl('ActionDynamic/ruanganDariInstalasi', array('encode' => false, 'namaModel' => '' . $model->getNamaModel() . '')),
                        'update' => '#' . CHtml::activeId($model, 'ruangan_id') . ''),));
                ?>
                <?php echo $form->dropDownList($model, 'ruangan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50, 'onchange'=>'clearAll()')); ?>
                <?php echo $form->error($model, 'ruangan_id'); ?>
            </div>
        </div>
        <div class="control-group ">
            <label class='control-label'>Nama Pasien</label>
            <div class="controls">
                <?php echo CHtml::hiddenField('idPasien'); ?>
                <?php echo CHtml::hiddenField('idKelasPelayanan'); ?>
                <?php echo CHtml::hiddenField('idPendaftaran'); ?>
                <?php echo CHtml::hiddenField('idPasienAdmisi'); ?>
                <?php echo CHtml::hiddenField('idDaftarTindakan'); ?>
                <?php echo CHtml::hiddenField('satuanTarif'); ?>
                <!--                <div class="input-append" style='display:inline'>-->
                <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'namaPasien',
                    'source' => 'js: function(request, response) {
                                                                   $.ajax({
                                                                       url: "' . Yii::app()->createUrl('ActionAutoComplete/pasienUntukMenuDiet') . '",
                                                                       dataType: "json",
                                                                       data: {
                                                                           term: request.term,
                                                                           idRuangan:$("#'.CHtml::activeId($model, 'ruangan_id').'").val(),
                                                                       },
                                                                       success: function (data) {
                                                                               response(data);
                                                                       }
                                                                   })
                                                                }',
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ) {
                                                                $(this).val( ui.item.label);
                                                                return false;
                                                            }',
                        'select' => 'js:function( event, ui ) {
                                statusPeriksa(ui.item.pendaftaran_id);
                                $("#idPasien").val(ui.item.pasien_id); 
                                $("#idPendaftaran").val(ui.item.pendaftaran_id); 
                                $("#idPasienAdmisi").val(ui.item.pasienadmisi_id); 
                                return false;
                            }',
                    ),
                    'htmlOptions' => array(
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogPasien'),
                ));
                ?>
            </div>
        </div>
		
        <div class="control-group ">
            <label class='control-label'>Menu Diet</label>
            <div class="controls">
                <?php echo CHtml::hiddenField('idMenuDiet'); ?>
                <!--                <div class="input-append" style='display:inline'>-->
                <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'menuDiet',
                    'source' => 'js: function(request, response) {
                                                                   $.ajax({
                                                                       url: "' . Yii::app()->createUrl('ActionAutoComplete/menuDiet') . '",
                                                                       dataType: "json",
                                                                       data: {
                                                                           term: request.term,
                                                                           kelaspelayananId: $("#idKelasPelayanan").val(),
                                                                       },
                                                                       success: function (data) {
                                                                               response(data);
                                                                       }
                                                                   })
                                                                }',
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ) {
                                                                $(this).val( ui.item.label);
                                                                return false;
                                                            }',
                        'select' => 'js:function( event, ui ) {
                                                                $("#idMenuDiet").val(ui.item.menudiet_id);
                                                                $("#idDaftarTindakan").val(ui.item.daftartindakan_id);
                                                                $("#URT").val(ui.item.ukuranrumahtangga); 
                                                                return false;
                                                            }',
                    ),
                    'htmlOptions' => array(
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                        'class'=>'span2',
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogMenuDiet'),
                ));
                ?>
                
            </div>
        </div>
		</td>
    <td width="104" style="padding-right:180px;">
        <div class="control-group ">
            <label class='control-label'>Jenis Waktu</label>
            <div class="controls">
                <?php                 
                $modJenisWaktu = JeniswaktuM::model()->findAll('jeniswaktu_aktif = true');
                $myData = CHtml::encodeArray(CHtml::listData($modJenisWaktu, 'jeniswaktu_id', 'jeniswaktu_id'));
                $myData = empty($myData) ? categories : $myData;
                ?>
                <?php echo Chtml::checkBoxList('jeniswaktu', false, CHtml::listData($modJenisWaktu, 'jeniswaktu_id', 'jeniswaktu_nama'), array('template'=>'<label class="checkbox inline">{input}{label}</label>', 'separator'=>'', 'class'=>'span2 jeniswaktu', 'onkeypress' => "return $(this).focusNextInputField(event)")); ?>                
            </div>
        </div>
        <div class="control-group ">
            <label class='control-label'>Jumlah</label>
            <div class="controls">
                <?php echo Chtml::textField('jumlah', 1, array('class'=>'span1 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>                
                <?php echo Chtml::dropDownList('URT', '', Ukuranrumahtangga::items(), array('empty'=>'-- Pilih --', 'class'=>'span2', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>                
                <?php
                echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>', array('onclick' => 'inputMenuDiet();return false;',
                    'class' => 'btn btn-primary',
                    'onkeypress' => "inputMenuDiet();return $(this).focusNextInputField(event)",
                    'rel' => "tooltip",
                    'title' => "Klik untuk menambahkan Menu Diet",));
                ?>
            </div>
        </div>
				</td>
  </tr>
</table>
        <?php } ?>
        <div style="max-width:1380px;overflow:auto;">
<table class="table table-bordered table-condensed" id="tableMenuDiet" >
    <thead>
        <tr>
            <th rowspan="2"><center><input type="checkbox" id="checkListUtama" name="checkListUtama" value="1" checked="checked" onclick="checkAll('cekList',this);hitungSemua();"></center></th>
            <th rowspan="2"><center>Instalasi/<br/>Ruangan</center></th>
            <th rowspan="2"><center>No Pendaftaran/<br/>No Rekam Medik</center></th>
            <th rowspan="2"><center>Nama Pasien</center></th>
            <th rowspan="2"><center>Umur</center></th>
            <th rowspan="2"><center>Jenis Kelamin</center></th>
            <th colspan="<?php echo count(JeniswaktuM::getJenisWaktu()); ?>"><center>Menu Diet</center></th>
            <th rowspan="2"><center>Jumlah</center></th>
            <th rowspan="2"><center>Satuan/URT</center></th>
            <th rowspan="2"><center>Jenis Makanan</center></th>
        </tr>
        <tr>
            <?php
            foreach (JeniswaktuM::getJenisWaktu() as $row) {
                echo '<th><center>' . $row->jeniswaktu_nama . '</center></th>';
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php if ($modDetailPesan > 0){
            $no = 1;
            foreach ($modDetailPesan AS $i=>$tampilData):
                echo "<tr>
                <td>" 
                .CHtml::checkBox('KirimmenupasienT['.$i.'][checkList]',true,array('class'=>'cekList','onclick'=>'hitungSemua()')) 
                .CHtml::hiddenField('KirimmenupasienT['.$i.'][ruangan_id]',$modPesan->ruangan_id) 
                .CHtml::hiddenField('KirimmenupasienT['.$i.'][pendaftaran_id]',$tampilData->pendaftaran_id) 
                .CHtml::hiddenField('KirimmenupasienT['.$i.'][pasienadmisi_id]',$tampilData->pasienadmisi_id) 
                .CHtml::hiddenField('KirimmenupasienT['.$i.'][pasien_id]',$tampilData->pasien_id) 
                .CHtml::hiddenField('KirimmenupasienT['.$i.'][satuanjml_urt]',$tampilData->satuanjml_urt) 
                .CHtml::hiddenField('KirimmenupasienT['.$i.'][penjamin_id]', $tampilData->pendaftaran->penjamin_id)
                .CHtml::hiddenField('KirimmenupasienT['.$i.'][jeniskasuspenyakit_id]', $tampilData->pendaftaran->jeniskasuspenyakit_id)
                . "</td>
                <td>" .$modPesan->ruangan->instalasi->instalasi_nama . "/<br/>" .$modPesan->ruangan->ruangan_nama . "</td>
                <td>" .$tampilData->pendaftaran->no_pendaftaran . "/<br/>" .$tampilData->pasien->no_rekam_medik . "</td>   
                <td>" .$tampilData->pasien->nama_pasien . "</td>   
                <td>" .$tampilData->pendaftaran->umur . "</td>   
                <td>" .$tampilData->pasien->jeniskelamin . "</td>";

                foreach (JeniswaktuM::getJenisWaktu() as $row) {
                    $detail = PesanmenudetailT::model()->with('menudiet')->findByAttributes(array('pendaftaran_id' => $tampilData->pendaftaran_id, 'pasienadmisi_id' => $tampilData->pasienadmisi_id, 'pesanmenudiet_id' => $tampilData->pesanmenudiet_id, 'jeniswaktu_id' => $row->jeniswaktu_id,'menudiet_id'=>$tampilData->menudiet_id));
                    if(empty($detail->pasienadmisi_id)){
                        $kelaspelayanan_id = $detail->pendaftaran->kelaspelayanan_id;
                    }else{
                        $kelaspelayanan_id = $detail->pasienadmisi->kelaspelayanan_id;
                    }
                    $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$detail->menudiet->daftartindakan_id,'kelaspelayanan_id'=>$kelaspelayanan_id,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL),array('order'=>'tariftindakan_id asc','limit'=>1));
//                    $tarifTindakan = TariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$idDaftarTindakan,'kelaspelayanan_id'=>$idKelasPelayanan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                   if(count($modTarif) <= 0){
                          $modTarif = TariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$detail->menudiet->daftartindakan_id,'kelaspelayanan_id'=>Params::KELASPELAYANAN_ID_TANPA_KELAS,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL),array('order'=>'tariftindakan_id asc','limit'=>1));
                   }
                   
        //echo '--->' . count($modTarif);
                    if (empty($detail->menudiet_id)) {
                        echo "<td><center>-</center></td>";
                    } else {
                        echo "<td>" .CHtml::hiddenField('KirimmenupasienT['.$i.'][jeniswaktu_id]['.$row->jeniswaktu_id.']',$row->jeniswaktu_id)
                                    .CHtml::hiddenField('KirimmenupasienT['.$i.'][pesanmenudetail_id]['.$row->jeniswaktu_id.']',$detail->pesanmenudetail_id)
                                    .CHtml::hiddenField('KirimmenupasienT['.$i.'][menudiet_id]['.$row->jeniswaktu_id.']',$detail->menudiet_id) . $detail->menudiet->menudiet_nama
                                    .CHtml::hiddenField('KirimmenupasienT['.$i.'][daftartindakan_id]['.$row->jeniswaktu_id.']', $detail->menudiet->daftartindakan_id)                                
                                    .CHtml::hiddenField('KirimmenupasienT['.$i.'][carabayar_id]['.$row->jeniswaktu_id.']', $detail->pendaftaran->carabayar_id)
                                    .CHtml::hiddenField('KirimmenupasienT['.$i.'][kelaspelayanan_id]['.$row->jeniswaktu_id.']', $detail->pendaftaran->kelaspelayanan_id)
                                    .'<br/>'.CHtml::textField('KirimmenupasienT['.$i.'][satuanTarif]['.$row->jeniswaktu_id.']', (!empty($modTarif->harga_tariftindakan)) ? $modTarif->harga_tariftindakan : 0,array('class'=>'span2')).
                                "</td>";
                    }
                };

                echo "<td>" .CHtml::textField('KirimmenupasienT['.$i.'][jml_kirim]',$tampilData->jml_pesan_porsi, array('class'=>'span1 numbersOnly')) . "</td>
                <td>" . $tampilData->satuanjml_urt . "</td>";
                echo "<td>".CHtml::dropDownList('KirimmenupasienT['.$i.'][status_menu]', '', StatusMakanan::items(),array('class'=>'inputFormTabel span2','empty'=>'--Pilih--'))."</td>";
                "</tr>";
                $no++;

            endforeach;
        }?>
    </tbody>
</table>
        <br>
        <?php
        //FORM REKENING
            $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                array(
                    'form'=>$form,
                    'modRekenings'=>$modRekenings,
                )
            );
        ?>
</div>
        


