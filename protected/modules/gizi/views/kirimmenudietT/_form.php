<fieldset>
    <legend class="rim2">Transaksi Pengiriman Menu Diet Pasien</legend>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'id' => 'gzkirimmenudiet-t-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
        'focus' => '#',
            ));
    ?>
<?php
    if(!empty($_GET['id'])){
?>
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
<?php } ?>
    <p class="help-block"><?php echo Yii::t('mds', 'Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php
    if (isset($modPesan)) {
        $this->renderPartial('_dataPesan', array('modPesan' => $modPesan, 'modDetailPesan' => $modDetailPesan, 'model'=>$model, 'form'=>$form));
    }
    ?>
    <?php $this->renderPartial('_dataForm', array('model' => $model, 'form' => $form, 'modPesan' => $modPesan)); ?>
    <?php echo Chtml::css('.table thead tr th{vertical-align:middle;}'); ?>
    <?php $this->renderPartial('_detailPesan', array('modPesan' => $modPesan, 'modDetailPesan' => $modDetailPesan, 'model' => $model, 'form' => $form)); ?>
    <div class="form-actions">
        <?php
                if(!empty($_GET['id'])){
                    echo CHtml::htmlButton(Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 
                                 'type' => 'submit','onKeypress' => 'return formSubmit(this,event)','disabled'=>true));
        
                }else{
                 echo CHtml::htmlButton(Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 
                                 'onKeypress' => 'return formSubmit(this,event)','onclick'=>'setKonfirmasi(this);'));
                }
       ?>
        
        <?php 
                if((!empty($_GET['id'])) AND ((KonfigsystemK::model()->find()->printkartulsng==TRUE) OR (KonfigsystemK::model()->find()->printkartulsng==TRUE)))
                {  
        ?>
                <script>
                    print(<?php echo $model->kirimmenudiet_id?>);
                </script>
        <?php 
                    echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', 
                            array('class'=>'btn btn-info','onclick'=>"print('$model->kirimmenudiet_id');return false",'disabled'=>FALSE  )); 
                }else{
                     echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', 
                             array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                } 
        ?>
                
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                    array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
	<?php 
                $content = $this->renderPartial('../tips/transaksi',array(),true);
                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
    </div>


<?php $this->endWidget(); ?>
</fieldset>
<?php $this->renderPartial('_dialog', array('model' => $model, 'form' => $form)); ?>
<?php
    //========= Dialog buat cari Bahan Diet =========================
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
        'id' => 'dialogPasien',
        'options' => array(
            'title' => 'Daftar Pasien',
            'autoOpen' => false,
            'modal' => true,
            'width' => 750,
            'height' => 600,
            'resizable' => false,
        ),
    ));

    $modKunjungan = new InfopasienmasukkamarV('search');
    $modKunjungan->unsetAttributes();
    if (isset($_GET['InfopasienmasukkamarV']))
        $modKunjungan->attributes = $_GET['InfopasienmasukkamarV'];

    $this->widget('ext.bootstrap.widgets.BootGridView', array(
        'id'=>'gzinfokunjunganri-v-grid', 
        'dataProvider' => $modKunjungan->search(),
        'filter'=>$modKunjungan,
        'template' => "{pager}{summary}\n{items}",
        'itemsCssClass' => 'table table-striped table-bordered table-condensed',
        'columns' => array(
            'no_pendaftaran',
            'no_rekam_medik',
            'nama_pasien',
            'umur',
            array(
                'header'=>'Jenis Kelamin',
                'name'=>'jeniskelamin',
                'filter'=>  JenisKelamin::items(),
                'value'=>'$data->jeniskelamin'
            ),
            array(
                'header'=>'kelas Pelayanan',
                'name'=>'kelaspelayanan_nama',
                'value'=>'$data->kelaspelayanan_nama'
            ),
            array(
                'header'=>'Ruangan',
                'name'=>'ruangan_id',
                'filter'=>  CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'),
                'value'=>'$data->ruangan_nama'
            ),
            'kamarruangan_nokamar',
            'kamarruangan_nobed',
            array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                "id" => "selectPasien",
                "onClick" => "
                    statusPeriksa($data->pendaftaran_id);
                    $(\"#idPasien\").val($data->pasien_id);
                    $(\"#idKelasPelayanan\").val($data->kelaspelayanan_id);
                    $(\"#idPendaftaran\").val($data->pendaftaran_id);
                    $(\"#idPasienAdmisi\").val($data->pasienadmisi_id);
                    $(\"#idKelasPelayanan\").val($data->kelaspelayanan_id);
                    $(\'#namaPasien\').val(\'$data->nama_pasien\');
                    $(\"#dialogPasien\").dialog(\"close\");
//                                dialogMenuPasien($data->pendaftaran_id);
                "))',
//            array(
//                'header'=>'Pilih',
//                'type'=>'raw',
//                'value'=>'CHtml::checkBox("data", "", array("value"=>$data->pendaftaran_id, "class"=>"pendaftaranId", "admisi"=>$data->pasienadmisi_id))',
           ),
        ),
        'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
    ));

$this->endWidget();
?>
<?php
$idInstalasi = CHtml::activeId($model, 'instalasi_id');
$idRuangan = CHtml::activeId($model, 'ruangan_id');
$totalPesan = CHtml::activeId($model, 'totalpesan_org');
$idBahanDiet = CHtml::activeId($model, 'bahandiet_id');
$namaPemesan = CHtml::activeId($model, 'nama_pemesan');
//$url = Yii::app()->createUrl('actionAjax/getMenuDietDetailDariKirim');

$urlPrintKuponGizi = Yii::app()->createUrl('print/KuponGizi',array('idKirimMenuDiet'=>''));

$url = Yii::app()->createUrl('actionAjax/GetMenuDietDetailKirim');
$urlCekStok = Yii::app()->createUrl('actionAjax/getStokBahanMakanan');
$urlCekInput = Yii::app()->createUrl('actionAjax/getStokBahanMakananInput');
$js = <<< JS

function print(idKirimMenuDiet)
{
     window.open('${urlPrintKuponGizi}'+idKirimMenuDiet,'printwin','left=100,top=100,width=940,height=400');
 
}
                    
function hitungSemua(){
        noUrut = 1;
        jumlah = 0;
        $('.cekList').each(function(){
            $(this).parents('tr').find('[name*="PesanmenudetailT"]').each(function(){
                var nama = $(this).attr('name');
                data = nama.split('PesanmenudetailT[]');
                if (typeof data[1] === "undefined"){}else{
                    $(this).attr('name','KirimmenupasienT['+(noUrut-1)+']'+data[1]);
                }
            });
            $(this).parents('tr').find('[name*="KirimmenupegawaiT"]').each(function(){
                var nama = $(this).attr('name');
                data = nama.split('KirimmenupegawaiT[]');
                if (typeof data[1] === "undefined"){}else{
                    $(this).attr('name','KirimmenupegawaiT['+(noUrut-1)+']'+data[1]);
                }
            });
            noUrut++;
            if($(this).is(':checked')){
                jumlah++;
            }
        });
    }
    
function inputMenuDiet(){
        var idPasien = $('#idPasien').val();
        var idPendaftaran = $('#idPendaftaran').val();
        var idPasienAdmisi = $('#idPasienAdmisi').val();
        var idMenuDiet = $('#idMenuDiet').val();
        var idDaftarTindakan = parseFloat($('#idDaftarTindakan').val());
        var idKelasPelayanan = parseFloat($('#idKelasPelayanan').val());
        var satuanTarif = $('#satuanTarif').val();
        var jumlah = $('#jumlah').val();
        var jeniswaktu = new Array();
        var pendaftaranId = new Array();
        var pasienAdmisi = new Array();
        var urt = $('#URT').val();
        var idRuangan = $('#${idRuangan}').val();
        var idInstalasi = $('#${idInstalasi}').val();
        var butuh = new Array();
        var total = new Array();
        i=0;
        $('.menudiet').each(function(){
            var jml_kirim = parseFloat($(this).parents('tr').find('.jmlKirim').val());
            var values = $(this).val();
            if(jQuery.isNumeric(values)){
                if (jQuery.inArray(values, butuh) == -1){
                    butuh[i] = values;
                    total[i] = jml_kirim;
                    i++;
                }
                else{
                    total[jQuery.inArray(values, butuh)] = total[jQuery.inArray(values, butuh)]+jml_kirim;
                }
            }
        });
        i = 0;
        $('.jeniswaktu').each(function(){
            value = $(this).val();
            if ($(this).is(':checked')){
                jeniswaktu[i]=value;
                i++;
            }
        });
        i = 0;
        $('.pendaftaranId').each(function(){
            value = $(this).val();
            valueAdmisi = $(this).attr('admisi');
            if ($(this).is(':checked')){
                pasienAdmisi[i]=valueAdmisi;
                pendaftaranId[i]=value;
                i++;
            }
            
        });
        
        if (!jQuery.isNumeric(idRuangan)){
            alert('Pilih Ruangan !');
            return false;
        }
        else if ((!jQuery.isNumeric(idPendaftaran))&&(pendaftaranId.length < 1)){
            alert('Isi Nama Pasien !');
            return false;
        }
        else if (!jQuery.isNumeric(idMenuDiet)){
            alert('Isi Makanan Diet yang dipilih !');
            return false;
        }
        else if (jeniswaktu.length < 1){
            alert('Isi Jenis Waktu yang dipilih !');
            return false;
        }
        else{
            $.post('${url}', {idPasien:idPasien, butuh:butuh, total:total, pasienAdmisi:pasienAdmisi, idPasienAdmisi:idPasienAdmisi, pendaftaranId:pendaftaranId, jeniswaktu:jeniswaktu, idPendaftaran:idPendaftaran, idMenuDiet:idMenuDiet, jumlah:jumlah, urt:urt, idRuangan:idRuangan, idInstalasi:idInstalasi, idDaftarTindakan:idDaftarTindakan, idKelasPelayanan:idKelasPelayanan}, function(data){
            
                if (data == null){
                    alert('Stok Bahan Menu Diet habis');
                }else{
                    $('#tableMenuDiet tbody').append(data);
                    $("#tableMenuDiet tbody tr:last .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
                    getDataRekening(idDaftarTindakan,idKelasPelayanan,jumlah,"tm");
                    hitungSemua();
                }
            }, 'json');
        }
        clearAll(1);
    }
    
function clearAll(code){
        var tempRuangan = $('#${idRuangan}').val();
        var tempInstalasi = $('#${idInstalasi}').val();
//        $('#fieldsetMenuDiet div').find('input,select').each(function(){
//            if ($(this).attr('type') == 'checkbox'){
//                
//            }
//            else{
//                $(this).val('');
//            }
//        }); 
//        if (!jQuery.isNumeric(code)){
//            $('#fieldsetMenuDiet #tableMenuDiet tbody').find('tr').each(function(){
//                $(this).remove();
//            });
//        }
        if(jQuery.isNumeric(tempRuangan)){
            $.fn.yiiGridView.update('gzinfokunjunganri-v-grid', {
                    data: "InfopasienmasukkamarV[ruangan_id]="+tempRuangan
            });
        }
        $('#jumlah').val(1);
        $('#namaPasien').val('');
        $('#menuDiet').val('');
        $('#${idRuangan}').val(tempRuangan);
        $('#${idInstalasi}').val(tempInstalasi);
    }
    
function dialogMenuPasien(){
        ruangan = $('#${idRuangan}').val();
        if(!jQuery.isNumeric(ruangan)){
            $.fn.yiiGridView.update('gzinfokunjunganri-v-grid', {
                    data: "InfopasienmasukkamarV[ruangan_id]=0"
            });
        }
        else{
            $.fn.yiiGridView.update('gzinfokunjunganri-v-grid', {
                    data: "InfopasienmasukkamarV[ruangan_id]="+ruangan
            });
        }
        if(!jQuery.isNumeric(ruangan)){
            alert('Isi ruangan terlebih dahulu');
            return false;
        }else{
            $('#dialogPasien').dialog('open');
        }
    }
    function cekStokMenu(obj){
        var value = $(obj).val();
        var total = parseFloat(0);
        $('.menudiet').each(function(){
            var jml_kirim = parseFloat($(this).parents('tr').find('.jmlKirim').val());
            if ($(this).val() == value){
                total = total+jml_kirim;
            }
        });
        $.post('${urlCekStok}', {total:total, value:value}, function(data){
            if (data == 1){
                
            }else{
                $(obj).val('');
                alert('Stok bahan makanan habis');
            }
        }, 'json');
    }
    function cekStokMenuInput(obj){
        var butuh = new Array();
        var total = new Array();
        i=0;
        $('.menudiet').each(function(){
            var jml_kirim = parseFloat($(this).parents('tr').find('.jmlKirim').val());
            var values = $(this).val();
            if(jQuery.isNumeric(values)){
                if (jQuery.inArray(values, butuh) == -1){
                    butuh[i] = values;
                    total[i] = jml_kirim;
                    i++;
                }
                else{
                    total[jQuery.inArray(values, butuh)] = parseFloat(total[jQuery.inArray(values, butuh)]+jml_kirim);
                }
            }
        });
        $.post('${urlCekInput}', {butuh:butuh, total:total}, function(data){
            if (data == 1){
                
            }else{
                $(obj).val(0);
                alert('Stok bahan makanan habis');
            }
        }, 'json');
    }
JS;
Yii::app()->clientScript->registerScript('onhead',$js,  CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('ready','
    hitungSemua();
    
    $("form").submit(function(){
        var idBahanDiet =$("#'.$idBahanDiet.'").val();
        jumlah = 0;
        $(".cekList").each(function(){
            if ($(this).is(":checked")){
                jumlah++;
            }
        });
        
        
        if (!jQuery.isNumeric($("#'.$idBahanDiet.'").val())){
            alert("'.CHtml::encode($model->getAttributeLabel('bahandiet_id')).' harus diisi !");
            return false;
        }
        else if (!jQuery.isNumeric($("#'.CHtml::activeId($model, 'jenisdiet_id').'").val())){
            alert("'.CHtml::encode($model->getAttributeLabel('jenisdiet_id')).' harus diisi !");
            return false;
//        }
//        else if (!jQuery.isNumeric($("#'.$idRuangan.'").val())){
//            alert("'.CHtml::encode($model->getAttributeLabel('ruangan_id')).' harus diisi !");
//            return false;
        }
        else if (jumlah < 1){
            alert("Pilih Menu Diet Pasien yang akan dipesan !");
            return false;
        }
        
    });
    
',  CClientScript::POS_READY);
?>
<script>
    function cekJenisDiet(idJenisDiet,idJenisDietNama){
        var idJenisBaru = idJenisDiet;
        var idJenisLama= $('#idJenisDiet').val();
        var namaJenisBaru = idJenisDietNama;
        var namaJenisLama = $('#jenisdiet').val();
        
        if(idJenisLama == ''){
            idJenisLama = $('#GZKirimmenudietT_jenisdiet_id').val();
        }
        
        if(idJenisBaru != idJenisLama){
            alert('Maaf, Jenis Diet tidak dapat dipilih lebih dari satu');
            $('#GZKirimmenudietT_jenisdiet_id').val(idJenisLama);
            $('#idJenisDiet').val(idJenisLama);
            $('#idJenisDiet2').val(idJenisLama);
            $('#jenisdiet').val(namaJenisLama);
            return false;
        }else{
            $('#idJenisDiet').val(idJenisBaru);
            $('#idJenisDiet2').val(idJenisBaru);
            $('#jenisdiet').val(namaJenisBaru);
        }
        refreshDialogMenu();
        
    }
    function refreshDialogMenu(){
        var idJenisDiet = $("#GZKirimmenudietT_jenisdiet_id").val();
        $.fn.yiiGridView.update('gzmenudiet-m-grid', {
            data: {
                "GZJenisdietM[jenisdiet_id]":idJenisDiet
            }
        });
    }
    
    function statusPeriksa(pendaftaran_id){
//    alert(status);
    var statusperiksa = '';
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = '';
    
    $.post("<?php echo Yii::app()->createUrl('gizi/pesanmenudietT/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id},
        function(data){
            if(data.pasienadmisi_id == null){
                var pasienadmisi_id = '';
            }else{
                var pasienadmisi_id = data.pasienadmisi_id;
            }
            if(data.pasien_id == null){
                var pasien_id = '';
            }else{
                var pasien_id = data.pasien_id;
            }
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan transaksi Pengiriman Menu Diet. ");
                $('#namaPasien').val('');
                $('#idPasien').val('');
                $('#idPendaftaran').val('');
                $('#idPasienAdmisi').val('');
                $('#idKelasPelayanan').val('');
//                location.reload();
            }
    },"json");
    return false; 
}  

function setKonfirmasi(obj){
    $(obj).attr('disabled',true);
    $(obj).removeAttr('onclick');
    $('#gzkirimmenudiet-t-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
    $('#gzkirimmenudiet-t-form').submit();
}
</script>