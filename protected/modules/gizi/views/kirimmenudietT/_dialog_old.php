<?php
//========= Dialog buat cari Jenis Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogJenisDiet',
    'options' => array(
        'title' => 'Jenis Diet',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

$modJenisDiet = new GZJenisdietM('search');
$modJenisDiet->unsetAttributes();
if (isset($_GET['GZJenisdietM']))
    $modJenisDiet->attributes = $_GET['GZJenisdietM'];

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'gzjenisdiet-m-grid',
    'dataProvider' => $modJenisDiet->search(),
    'filter' => $modJenisDiet,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        'jenisdiet_nama',
        'jenisdiet_namalainnya',
        'jenisdiet_keterangan',
        'jenisdiet_catatan',
//        'jenisdiet_aktif',
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectBahan",
                                    "onClick" => "$(\'#'.Chtml::activeId($model,'jenisdiet_id').'\').val($data->jenisdiet_id);
                                    $(\'#jenisdiet\').val(\'$data->jenisdiet_nama\');
                                    $(\'#dialogJenisDiet\').dialog(\'close\');return false;"))',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
?>

<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogBahanDiet',
    'options' => array(
        'title' => 'Bahan Diet',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

$modBahanDiet = new GZBahandietM('search');
$modBahanDiet->unsetAttributes();
if (isset($_GET['GZBahandietM']))
    $modBahanDiet->attributes = $_GET['GZBahandietM'];

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'gzbahandiet-m-grid',
    'dataProvider' => $modBahanDiet->search(),
    'filter' => $modBahanDiet,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        'bahandiet_nama',
        'bahandiet_namalain',
//        'bahandiet_aktif',
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectBahan",
                                    "onClick" => "$(\'#'.Chtml::activeId($model,'bahandiet_id').'\').val($data->bahandiet_id);
                                    $(\'#bahandiet\').val(\'$data->bahandiet_nama\');
                                    $(\'#dialogBahanDiet\').dialog(\'close\');return false;"))',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
?>

<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogMenuDiet',
    'options' => array(
        'title' => 'Daftar Menu Diet',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

$modMenuDiet = new GZMenuDietM('searchz');
$modMenuDiet->unsetAttributes();
if (isset($_GET['GZMenuDietM']))
    $modMenuDiet->attributes = $_GET['GZMenuDietM'];

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'gzmenudiet-m-grid',
    'dataProvider' => $modMenuDiet->searchMenuDiet(),
    'filter' => $modMenuDiet,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
                        'name'=>'menudiet_id',
                        'value'=>'$data->menudiet_id',
                        'filter'=>false,
                ),
        'jenisdiet.jenisdiet_nama',
        'menudiet_nama',
        'menudiet_namalain',
        'jml_porsi',
        'ukuranrumahtangga',
        array(
            'header'=>'Kelas Pelayanan',
            'name'=>'kelaspelayanan_nama',
            'value'=>'$data->kelaspelayanan_nama',
        ),
        array(
          'header'=>'Tarif Diet',
          'name'=>'harga_tariftindakan',
          'value'=>'$data->harga_tariftindakan',
        ),
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectBahan",
                                    "onClick" => "$(\'#idMenuDiet\').val($data->menudiet_id);
                                    $(\'#menuDiet\').val(\'$data->menudiet_nama\');
                                    $(\'#URT\').val(\'$data->ukuranrumahtangga\');
                                    $(\'#idDaftarTindakan\').val($data->daftartindakan_id);
                                    $(\'#satuanTarif\').val(\'$data->harga_tariftindakan\');
                                    $(\'#dialogMenuDiet\').dialog(\'close\');return false;"))',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
?>
<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>

