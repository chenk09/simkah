<?php 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gzkirimmenudiet-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
    <legend class="rim2">Informasi Pengiriman Menu Diet</legend>


<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'gzkirimmenudiet-t-grid',
    'dataProvider'=>$model->searchInformasi(),
//    'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(       
                array(
                    'header'=>'No Kirim Menu',
                    'type'=>'raw',
                    'value'=>'$data->nokirimmenu',
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center;')
                ),
                array(
                    'header'=>'Jenis Pesan Menu',
                    'type'=>'raw',
                    'value'=>'$data->jenispesanmenu',
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center;')
                ),
                array(
                    'header'=>'Tgl. Kirim Menu',
                    'type'=>'raw',
                    'value'=>'$data->tglkirimmenu',
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center;')
                ),
                array(
                    'header'=>'Nama Pasien',
                    'type'=>'raw',
                    'value'=>'($data->jenispesanmenu == "Pasien") ? $data->kirimmenupasien->pasien->nama_pasien : $data->kirimmenupegawai->pegawai->nama_pegawai',
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center;')
                ),
                array(
                    'header'=>'Ruangan',
                    'type'=>'raw',
                    'value'=>'($data->jenispesanmenu == "Pasien") ? $data->kirimmenupasien->ruangan->ruangan_nama : $data->kirimmenupegawai->ruangan->ruangan_nama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center;')
                ),
                array(
                    'header'=>'Nama Bahan Diet',
                    'type'=>'raw',
                    'value'=>'$data->bahandiet->bahandiet_nama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center;')
                ),
                array(
                    'header'=>'Nama Jenis',
                    'type'=>'raw',
                    'value'=>'$data->jenisdiet->jenisdiet_nama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center;')
                ),
                array(
                    'header'=>'NO Pesan Menu',
                    'type'=>'raw',
                    'value'=>'$data->pesanmenudiet->nopesanmenu',
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center;')
                ),
                array(
                    'header'=>'Ket. <br/> Kirim',
                    'type'=>'raw',
                    'value'=>'$data->keterangan_kirim',
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center;')
                ),
                array(
                    'header'=>'Detail',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",  Yii::app()->controller->createUrl("/gizi/KirimmenudietT/detailKirimMenuDiet",array("id"=>$data->kirimmenudiet_id)),array("id"=>"$data->kirimmenudiet_id","target"=>"frameDetail","rel"=>"tooltip","title"=>"Klik untuk Detail Pengiriman Menu Diet", "onclick"=>"window.parent.$(\'#dialogDetail\').dialog(\'open\')"));',
                    'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    'headerHtmlOptions'=>array('style'=>'vertical-align: middle;text-align:center; width:40px')
                ),
                array(
                    'header'=>'Retur /<br/> Ubah Menu Diet',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("/gizi/InformasiMenuDiet/indexKirim",array("idKirimmenudiet"=>$data->kirimmenudiet_id)),array("idKirimmenudiet"=>"$data->kirimmenudiet_id","target"=>"frameUbahMenu","rel"=>"tooltip","title"=>"Klik untuk Retur  / Ubah Menu Diet", "onclick"=>"statusPeriksa($data->kirimmenudiet_id);window.parent.$(\'#dialogUbahMenu\').dialog(\'open\')"));',
//                    'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/gizi/InformasiMenuDiet/indexKirim",array("idKirimmenudiet"=>$data->kirimmenudiet_id)),array("idKirimmenudiet"=>"$data->kirimmenudiet_id","target"=>"frameUbahMenu","rel"=>"tooltip","onclick"=>"statusPeriksa($data->kirimmenudiet_id,$data->pesanmenudiet_id);return false;","title"=>"Klik untuk Retur  / Ubah Menu Diet", "onclick"=>"window.parent.$(\'#dialogUbahMenu\').dialog(\'open\')"));',
                    'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
                array(
                    'header'=>'Batal <br/> Kirim',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=icon-remove></i>","javascript:batalKirim(\'$data->kirimmenudiet_id\')",array("idKirimDiet"=>$data->kirimmenudiet_id,"href"=>"#","rel"=>"tooltip","title"=>"Klik Untuk Batal Pesan Menu Diet",))',
                ),
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
    <div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
</fieldset>

<?php 
$js = <<< JSCRIPT
function openDialog(id){
    $('#dialogDetail').dialog('open');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('head',$js,CClientScript::POS_HEAD);                        
?>

<?php
//========= Dialog untuk Melihat detail Pengajuan Bahan Makanan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDetail',
    'options' => array(
        'title' => 'Detail Pemesanan Menu Diet',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

echo '<iframe src="" name="frameDetail" width="100%" height="500"></iframe>';

$this->endWidget();
?>

<?php
//========= Dialog untuk Melihat detail Pengajuan Bahan Makanan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogUbahMenu',
    'options' => array(
        'title' => 'Retur / Ubah Menu Diet',
        'autoOpen' => false,
        'modal' => true,
        'width' => 1200,
        'height' => 600,
        'resizable' => false,
    ),
));

echo '<iframe src="" name="frameUbahMenu" width="100%" height="500"></iframe>';

$this->endWidget();
?>

<?php 
// Dialog untuk Batal Kirim Menu Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogBatalKirim',
    'options'=>array(
        'title'=>'Batal Kirim Menu Diet',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>600,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>'; 


$this->endWidget();
//========= Dialog untuk Batal Kirim Menu Diet =============================
?>

<script>
function batalKirim(idKirimDiet){
    var idKirimDiet = idKirimDiet;
//    var answer = confirm('Yakin Akan Membatalkan Pengiriman Diet ?');
//    if (answer){
          $.post('<?php echo Yii::app()->createUrl('gizi/ActionAjaxGZ/batalKirimMenuDiet');?>', 
          {idKirimDiet:idKirimDiet}, function(data){
            if (data.status == 'create_form')
            {
                setTimeout("$('#dialogBatalKirim').dialog('open') ",1000);
                $('#dialogBatalKirim div.divForForm').html(data.div);
                $('#dialogBatalKirim div.divForForm form #idKirimDiet').val(data.idKirim);
                $('#dialogBatalKirim div.divForForm form').submit(konfirmBatal);            
            }
            else
            {
                $.fn.yiiGridView.update('gzkirimmenudiet-t-grid');
                $('#dialogBatalKirim div.divForForm').html(data.div);
                setTimeout("$('#dialogBatalKirim').dialog('close') ",1000);

            }
                      
        }, 'json');
//    }
}
function konfirmBatal()
{
    
    <?php 
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('gizi/ActionAjaxGZ/batalKirimMenuDiet'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogBatalKirim div.divForForm').html(data.div);
                    $('#dialogBatalKirim div.divForForm form').submit(konfirmBatal);
                }
                else
                {
                    $('#dialogBatalKirim div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('gzkirimmenudiet-t-grid');
                    setTimeout(\"$('#dialogBatalKirim').dialog('close') \",3000);
                }
 
            } ",
    ))
?>;
    return false; 
}

function statusPeriksa(kirimmenudiet_id){
//    alert(status);
    var statusperiksa = '';
    var pendaftaran_id = '';
    var pasienmasukpenunjang_id = '';
    var pesanmenudiet_id = '';
    var kirimmenudiet_id = kirimmenudiet_id;
    
    $.post("<?php echo Yii::app()->createUrl('gizi/pesanmenudietT/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id,pesanmenudiet_id:pesanmenudiet_id,kirimmenudiet_id:kirimmenudiet_id},
        function(data){
            if(data.pasienadmisi_id == null){
                var pasienadmisi_id = '';
            }else{
                var pasienadmisi_id = data.pasienadmisi_id;
            }
            if(data.pasien_id == null){
                var pasien_id = '';
            }else{
                var pasien_id = data.pasien_id;
            }
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan perubahan Menu Diet. ");
                $('#dialogUbahMenu').dialog('close');
                location.reload();
            }
//            else{
//                window.location.href= "<?php // echo Yii::app()->controller->createUrl('InformasiMenuDiet/indexKirim',array()); ?>&idKirimmenudiet="+data.kirimmenudiet_id,"target=frameUbahMenu";
//            }
    },"json");
    return false; 
}

</script>