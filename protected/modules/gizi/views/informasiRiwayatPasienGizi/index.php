<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari cari', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
if(!empty($_GET['succes'])){?>
<div class="alert alert-block alert-success">
    <a class="close" data-dismiss="alert">×</a>
        <?php if($_GET['succes']==2){ ?> Pemeriksaan Pasien berhasil di batalkan<?php } if($_GET['succes']==1){?>Pasein Berhasil Di Rujuk<?php }?>
</div>
<?php } ?>
<fieldset>
    <legend class="rim2">Informasi Riwayat Pasien</legend>
     
     
<?php $this->widget('bootstrap.widgets.BootAlert');
 $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$modPasienMasukPenunjang->searchKonsulGizi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            'tgl_pendaftaran',
            'tglmasukpenunjang',
            'ruanganasal_nama',
            'no_pendaftaran',
            'no_rekam_medik',
            array(
                'header'=>'Nama Pasien / Alias',
                'type'=>'raw',
                'value'=>'"$data->nama_pasien"." / "."$data->nama_bin"',
            ),
           'jeniskasuspenyakit_nama',
           'umur',
           array(
                'header'=>'Jenis Kelamin',
                'type'=>'raw',
                'value'=>'$data->jeniskelamin',
           ),
           'alamat_pasien',
           
           array(
                'name'=>'CaraBayarPenjamin',
                'type'=>'raw',
                'value'=>'$data->caraBayarPenjamin',    
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
           ),
           array(
                'header'=>'Status Periksa',
                'type'=>'raw',
                'value'=>'$data->statusperiksa',
           ),
           array(
                'header'=>'Riwayat Pasien',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=\"icon-user\"></i>",Yii::app()->createUrl("/gizi/InformasiRiwayatPasienGizi/RiwayatPasien&id",array("id"=>$data->pendaftaran_id,"idPasien"=>$data->pasien_id)), array("rel"=>"tooltip","title"=>"Klik untuk Rencana Pemeriksaan"))', 'htmlOptions'=>array('style'=>'text-align: center; width:40px')
           ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
    
<?php
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'id'=>'daftarPasien-form',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <table  class="table-condensed">
        <tr>
            <td>
              <?php echo $form->labelEx($modPasienMasukPenunjang,'tglmasukpenunjang', array('class'=>'control-label')) ?>
                  <div class="controls">  
                   
                     <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPasienMasukPenunjang,
                                         'attribute'=>'tglAwal',
                                         'mode'=>'datetime',
//                                          'maxDate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                      
                   </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' s/d', array('class'=>'control-label')) ?>

                   <div class="controls">  
                    <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPasienMasukPenunjang,
                                         'attribute'=>'tglAkhir',
                                         'mode'=>'datetime',
//                                         'maxdate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                   </div> 
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'noRM',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 
            </td>
            <td>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
            </td>
            
        </tr>
    </table>
    <div class="form-actions">
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
							     ?>
								       <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
  <?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>

    </div>
</fieldset>  
<?php $this->endWidget();?>
</fieldset>
<script type="text/javascript">

function batalperiksa(idpendaftaran)
{
   
   var con = confirm('anda yakin akan membatalkan pemeriksaan laboratorium pasien ini? ');
   if(con){
       $.post('<?php echo Yii::app()->createUrl('laboratorium/daftarPasien/BatalPeriksaPasienLuar')?>',{idpendaftaran:idpendaftaran},
                 function(data){
                     if(data.status=='success'){
                          
                         window.location = "<?php echo Yii::app()->createUrl('laboratorium/daftarPasien/index&succes=2')?>";
//                         $('#pesan').show();
                        
                     }
                 },'json'
             );
   }else{
       alert('tidak');
   }
//    if(alasan==''){
//        alert('Anda Belum Mengisi Alasan Pembatalan');
//    }else{
//        $.post('<?php //echo Yii::app()->createUrl('rawatInap/pasienRawatInap/BatalRawatInap');?>', $('#formAlasan').serialize(), function(data){
////            if(data.error != '')
////                alert(data.error);
////            $('#'+data.cssError).addClass('error');
//            if(data.status=='success'){
//                batal();
//                alert('Data Berhasil Disimpan');
//                location.reload();
//            }else{
//                alert(data.status);
//            }
//        }, 'json');
//   }     
}
</script>

<?php
//========= Dialog Detail Anamnesa Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDetailAnamnesa',
    'options' => array(
        'title' => 'Data Anamnesa Diet',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 600,
        'resizable' => false,
    ),
));
?>
<iframe src="" name="detailDialogAnamnesa" width="100%" height="500">
</iframe>
<?php
$this->endWidget();
//=======================================================================
?>

<?php
//========= Dialog Detail Konsultasi Gizi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDetailData',
    'options' => array(
        'title' => 'Detail Data',
        'autoOpen' => false,
        'modal' => true,
        'width' => 500,
        'height' => 600,
        'resizable' => false,
    ),
));
?>
<iframe src="" name="detailDialog" width="100%" height="500">
</iframe>
<?php
$this->endWidget();
