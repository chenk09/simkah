    <legend  class="rim2">Transaksi Pemesanan Menu Diet Pasien</legend>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'id' => 'gzpesanmenudiet-t-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
        'focus' => '#',
            ));
    ?>
<?php
    if(!empty($_GET['id'])){
?>
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
<?php } ?>
    
    
    <?php $this->renderPartial('gizi.views.pesanmenudietT._dataForm', array('form'=>$form, 'model'=>$model)); ?>
    <fieldset id="fieldsetMenuDiet">
        <legend class="rim">Detail Menu Diet</legend>
		
		<table width="200" border="0">
  <tr>
    <td width="80"> <div class="control-group ">
            <?php //echo CHtml::hiddenField('jenisPesan'); ?>
        <?php //echo $form->dropDownListRow($model, 'jenispesanmenu', GZPesanmenudietT::jenisPesan(), array('inline' => true, 'empty' => '-- Pilih --', 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'onchange' => 'setJenisPesan();', 'maxlength' => 50)); ?>
            <label class='control-label'><?php echo CHtml::encode($model->getAttributeLabel('ruangan_id')); ?><span class="required">*</span></label>
            <div class="controls">
                 <?php //echo CHtml::hiddenField('instalasi_id'); ?>
                <?php //echo CHtml::hiddenField('ruangan_id'); ?>
                <?php
                echo $form->dropDownList($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50,
                    'ajax' => array('type' => 'POST',
                        'url' => Yii::app()->createUrl('ActionDynamic/ruanganDariInstalasi', array('encode' => false, 'namaModel' => '' . $model->getNamaModel() . '')),
                        'update' => '#' . CHtml::activeId($model, 'ruangan_id') . ''),));
                ?>
                <?php echo $form->dropDownList($model, 'ruangan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50, 'onchange'=>'clearAll()')); ?>
                <?php echo $form->error($model, 'ruangan_id'); ?>
            </div>
        </div>
        <div class="control-group ">
            <label class='control-label'>Nama Pasien</label>
            <div class="controls">
                <?php echo CHtml::hiddenField('idPasien'); ?>
                <?php echo CHtml::hiddenField('idKelasPelayanan'); ?>
                <?php echo CHtml::hiddenField('idPendaftaran'); ?>
                <?php echo CHtml::hiddenField('idPasienAdmisi'); ?>
                <!--                <div class="input-append" style='display:inline'>-->
                <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'namaPasien',
                    'source' => 'js: function(request, response) {
                               $.ajax({
                                   url: "' . Yii::app()->createUrl('ActionAutoComplete/pasienUntukMenuDiet') . '",
                                   dataType: "json",
                                   data: {
                                       term: request.term,
                                       idRuangan:$("#'.CHtml::activeId($model, 'ruangan_id').'").val(),
                                   },
                                   success: function (data) {
                                           response(data);
                                   }
                               })
                            }',
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ) {
                                                                $(this).val( ui.item.label);
                                                                return false;
                                                            }',
                        'select' => 'js:function( event, ui ) {
                            statusPeriksa(ui.item.pendaftaran_id);
                            $("#idPasien").val(ui.item.pasien_id); 
                            $("#idPendaftaran").val(ui.item.pendaftaran_id); 
                            $("#idPasienAdmisi").val(ui.item.pasienadmisi_id); 
                            return false;
                        }',
                    ),
                    'htmlOptions' => array(
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogPasien', 'jsFunction'=>'dialogMenuPasien()'),
                ));
                ?>
            </div>
        </div>
        <div class="control-group ">
            <label class='control-label'>Menu Diet</label>
            <div class="controls">
                <?php echo CHtml::hiddenField('idMenuDiet'); ?>
                <!--                <div class="input-append" style='display:inline'>-->
                <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name' => 'menuDiet',
                    'source' => 'js: function(request, response) {
                               $.ajax({
                                   url: "' . Yii::app()->createUrl('ActionAutoComplete/menuDiet') . '",
                                   dataType: "json",
                                   data: {
                                       term: request.term,
                                       kelaspelayananId: $("#idKelasPelayanan").val(),
                                   },
                                   success: function (data) {
                                           response(data);
                                   }
                               })
                            }',
                    'options' => array(
                        'showAnim' => 'fold',
                        'minLength' => 2,
                        'focus' => 'js:function( event, ui ) {
                                                                $(this).val( ui.item.label);
                                                                return false;
                                                            }',
                        'select' => 'js:function( event, ui ) {
                                                                $("#idMenuDiet").val(ui.item.menudiet_id); 
                                                                $("#URT").val(ui.item.ukuranrumahtangga); 
                                                                return false;
                                                            }',
                    ),
                    'htmlOptions' => array(
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                        'class'=>'span2',
                    ),
                    'tombolDialog' => array('idDialog' => 'dialogMenuDiet'),
                ));
                ?>
                
            </div>
    </div></td>
    <td width="104" style="padding-right:180px;">
        <div class="control-group ">
            <label class='control-label'>Jenis Waktu</label>
            <div>
                <?php                 
                $modJenisWaktu = JeniswaktuM::getJenisWaktu();
                $myData = CHtml::encodeArray(CHtml::listData($modJenisWaktu, 'jeniswaktu_id', 'jeniswaktu_id'));
                $myData = empty($myData) ? categories : $myData;
                ?>
                <fieldset>
                        <?php echo '<table>
                                        <tr>
                                            <td>
                                                '.Chtml::checkBoxList('jeniswaktu', false, CHtml::listData($modJenisWaktu, 'jeniswaktu_id', 'jeniswaktu_nama'), array('template'=>'<label class="checkbox inline">{input}{label}</label>', 'separator'=>'', 'style'=>'margin-left:2px;max-width:10px;','class'=>'span2 jeniswaktu', 'onkeypress' => "return $(this).focusNextInputField(event)")).'
                                            </td>
                                        </tr>
                                        </table>'; 
                        ?>
                </fieldset>
            </div>   
        </div>
        <div class="control-group ">
            <label class='control-label'>Jumlah</label>
            <div class="controls">
                <?php echo Chtml::textField('jumlah', 1, array('class'=>'span1 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>                
                <?php echo Chtml::dropDownList('URT', '', Ukuranrumahtangga::items(), array('empty'=>'-- Pilih --', 'class'=>'span2', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>                
                <?php
                echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>', array('onclick' => 'inputMenuDiet();return false;',
                    'class' => 'btn btn-primary',
                    'onkeypress' => "inputMenuDiet();return $(this).focusNextInputField(event)",
                    'rel' => "tooltip",
                    'title' => "Klik untuk menambahkan Menu Diet",));
                ?>
            </div>
    </div></td>
  </tr>
</table>

       
        
        <style>
            .table thead tr th{
                vertical-align:middle;
            }
        </style>
        <table class="table table-bordered table-condensed" id="tableMenuDiet">
            <thead>
                <tr>
                    <th rowspan="2"><center><input type="checkbox" id="checkListUtama" name="checkListUtama" value="1" checked="checked" onclick="checkAll('cekList',this);hitungSemua();"></center></th>
                    <th rowspan="2"><center>Instalasi/<br/>Ruangan</center></th>
                    <th rowspan="2"><center>No Pendaftaran/<br/>No Rekam Medik</center></th>
                    <th rowspan="2"><center>Nama Pasien</center></th>
                    <th rowspan="2"><center>Umur</center></th>
                    <th rowspan="2"><center>Jenis Kelamin</center></th>
                    <th colspan="<?php echo count($modJenisWaktu); ?>"><center>Menu Diet</center></th>
                    <th rowspan="2"><center>Jumlah</center></th>
                    <th rowspan="2"><center>Satuan/URT</center></th>
                </tr>
                <tr>      
                    <?php foreach($modJenisWaktu as $row){
                    echo '<th><center>'.$row->jeniswaktu_nama.'</center></th>';
                     } ?>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </fieldset>
    <div class="form-actions">
        <?php
             if(!empty($_GET['id'])){
                    echo CHtml::htmlButton(Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 
                                'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)','disabled'=>true));
        
                }else{
                 echo CHtml::htmlButton(Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 
                                'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)'));
                }
        ?>
        <?php 
                echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                    Yii::app()->createUrl($this->module->id.'/'.PesanmenudietT.'/index'), 
                    array('class'=>'btn btn-danger',
                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));  
        ?>
	<?php 
$content = $this->renderPartial('gizi.views./tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
    </div>

    <?php $this->endWidget(); ?>
</legend>

<?php $this->renderPartial('gizi.views.pesanmenudietT._dialog', array('model'=>$model)); ?>
<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPasien',
    'options' => array(
        'title' => 'Daftar Pasien',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

$modKunjungan = new GZInfokunjunganriV('search');
$modKunjungan->unsetAttributes();
if (isset($_GET['GZInfokunjunganriV']))
    $modKunjungan->attributes = $_GET['GZInfokunjunganriV'];

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id'=>'gzinfokunjunganri-v-grid', 
    'dataProvider' => $modKunjungan->searchDialog(),
    'filter' => $modKunjungan,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        'no_pendaftaran',
        'no_rekam_medik',
        'nama_pasien',
        'umur',
        array(
            'header'=>'Jenis Kelamin',
            'name'=>'jeniskelamin',
            'filter'=>  JenisKelamin::items(),
            'value'=>'$data->jeniskelamin'
        ),
        array(
            'header'=>'Ruangan',
            'name'=>'ruangan_id',
            'filter'=>  CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'),
            'value'=>'$data->ruangan_nama'
        ),
        'kamarruangan_nokamar',
        'kamarruangan_nobed',
          array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                            "id" => "selectPasien",
                            "onClick" => "
                                statusPeriksa($data->pendaftaran_id);
                                $(\"#idPasien\").val($data->pasien_id);
                                $(\"#idKelasPelayanan\").val($data->kelaspelayanan_id);
                                $(\"#idPendaftaran\").val($data->pendaftaran_id);
                                $(\"#idPasienAdmisi\").val($data->pasienadmisi_id);
                                $(\'#namaPasien\').val(\'$data->nama_pasien\');
                                $(\"#dialogPasien\").dialog(\"close\");
//                                dialogMenuPasien($data->pendaftaran_id);
                            "))',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
?>
<?php
$idInstalasi = CHtml::activeId($model, 'instalasi_id');
$idRuangan = CHtml::activeId($model, 'ruangan_id');
$totalPesan = CHtml::activeId($model, 'totalpesan_org');
$idBahanDiet = CHtml::activeId($model, 'bahandiet_id');
$namaPemesan = CHtml::activeId($model, 'nama_pemesan');
$url = Yii::app()->createUrl('actionAjax/getMenuDietDetail');
$jsx = <<< JS
    function inputMenuDiet(){
        var idPasien = $('#idPasien').val();
        var idPendaftaran = $('#idPendaftaran').val();
        var idPasienAdmisi = $('#idPasienAdmisi').val();
        var idMenuDiet = $('#idMenuDiet').val();
        var jumlah = $('#jumlah').val();
        var jeniswaktu = new Array();
        var pendaftaranId = new Array();
        var pasienAdmisi = new Array();
        var urt = $('#URT').val();
        var idRuangan = $('#${idRuangan}').val();
        var idInstalasi = $('#${idInstalasi}').val();
        i = 0;
        $('.jeniswaktu').each(function(){
            value = $(this).val();
            if ($(this).is(':checked')){
                jeniswaktu[i]=value;
                i++;
            }
        });
        i = 0;
        $('.pendaftaranId').each(function(){
            value = $(this).val();
            valueAdmisi = $(this).attr('admisi');
            if ($(this).is(':checked')){
                pasienAdmisi[i]=valueAdmisi;
                pendaftaranId[i]=value;
                i++;
            }
            
        });
        
        if (!jQuery.isNumeric(idRuangan)){
            alert('Pilih Ruangan !');
            return false;
        }
        else if ((!jQuery.isNumeric(idPendaftaran))&&(pendaftaranId.length < 1)){
            alert('Isi Nama Pasien !');
            return false;
        }
        else if (!jQuery.isNumeric(idMenuDiet)){
            alert('Isi Makanan Diet yang dipilih !');
            return false;
        }
        else if (jeniswaktu.length < 1){
            alert('Isi Jenis Waktu yang dipilih !');
            return false;
        }
        else{
            $.post('${url}', {idPasien:idPasien, pasienAdmisi:pasienAdmisi, idPasienAdmisi:idPasienAdmisi, pendaftaranId:pendaftaranId, jeniswaktu:jeniswaktu, idPendaftaran:idPendaftaran, idMenuDiet:idMenuDiet, jumlah:jumlah, urt:urt, idRuangan:idRuangan, idInstalasi:idInstalasi}, function(data){
                $('#tableMenuDiet tbody').append(data);
                $("#tableMenuDiet tbody tr:last .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
                hitungSemua();
            }, 'json');
        }
        clearAll(1);
    }
            
    function hitungSemua(){
        noUrut = 1;
        jumlah = 0;
        $('.cekList').each(function(){
            $(this).parents('tr').find('[name*="PesanmenudetailT"]').each(function(){
                var nama = $(this).attr('name');
                data = nama.split('PesanmenudetailT[]');
                if (typeof data[1] === "undefined"){}else{
                    $(this).attr('name','PesanmenudetailT['+(noUrut-1)+']'+data[1]);
                }
            });
            noUrut++;
            if($(this).is(':checked')){
                jumlah++;
            }
        });
        $('#${totalPesan}').val(jumlah);
    }
    
    function clearAll(code){
        var tempRuangan = $('#${idRuangan}').val();
        var tempInstalasi = $('#${idInstalasi}').val();
        $('#fieldsetMenuDiet div').find('input,select').each(function(){
            if ($(this).attr('type') == 'checkbox'){
                
            }
            else{
                $(this).val('');
            }
        });
        if (!jQuery.isNumeric(code)){
            $('#fieldsetMenuDiet #tableMenuDiet tbody').find('tr').each(function(){
                $(this).remove();
            });
        }
        if(jQuery.isNumeric(tempRuangan)){
            $.fn.yiiGridView.update('gzinfokunjunganri-v-grid', {
                    data: "GZInfokunjunganriV[ruangan_id]="+tempRuangan
            });
        }
        $('#jumlah').val(1);
        $('#${idRuangan}').val(tempRuangan);
        $('#${idInstalasi}').val(tempInstalasi);
    }
    
    function dialogMenuPasien(){
        ruangan = $('#${idRuangan}').val();
        if(!jQuery.isNumeric(ruangan)){
            $.fn.yiiGridView.update('gzinfokunjunganri-v-grid', {
                    data: "GZInfokunjunganriV[ruangan_id]=0"
            });
        }
        else{
            $.fn.yiiGridView.update('gzinfokunjunganri-v-grid', {
                    data: "GZInfokunjunganriV[ruangan_id]="+ruangan
            });
        }
        if(!jQuery.isNumeric(ruangan)){
            alert('Isi ruangan terlebih dahulu');
            return false;
        }else{
            $('#dialogPasien').dialog('open');
        }
    }
JS;
Yii::app()->clientScript->registerScript('head', $jsx, CClientScript::POS_HEAD);
?>

<?php Yii::app()->clientScript->registerScript('submit', '
    $.fn.yiiGridView.update("gzinfokunjunganri-v-grid", {
		data: "GZInfokunjunganriV[ruangan_id]=0"
	});
    $("form").submit(function(){
        var idBahanDiet =$("#'.$idBahanDiet.'").val();
        jumlah = 0;
        $(".cekList").each(function(){
            if ($(this).is(":checked")){
                jumlah++;
            }
        });
        
        
        if (!jQuery.isNumeric($("#'.$idBahanDiet.'").val())){
            alert("'.CHtml::encode($model->getAttributeLabel('bahandiet_id')).' harus diisi !");
            return false;
        }
        else if ($("#'.$namaPemesan.'").val() == ""){
            alert("'.CHtml::encode($model->getAttributeLabel('nama_pemesan')).' harus diisi !");
            return false;
        }
        else if (jumlah < 1){
            alert("Pilih Menu Diet Pasien yang akan dipesan !");
            return false;
        }
        
    });
    
', CClientScript::POS_READY); ?>
<script>
function statusPeriksa(pendaftaran_id){
//    alert(status);
    var statusperiksa = '';
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = '';
    
    $.post("<?php echo Yii::app()->createUrl('gizi/pesanmenudietT/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id},
        function(data){
            if(data.pasienadmisi_id == null){
                var pasienadmisi_id = '';
            }else{
                var pasienadmisi_id = data.pasienadmisi_id;
            }
            if(data.pasien_id == null){
                var pasien_id = '';
            }else{
                var pasien_id = data.pasien_id;
            }
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan transaksi Pemesanan Menu Diet. ");
                $('#namaPasien').val('');
                $('#idPasien').val('');
                $('#idPendaftaran').val('');
                $('#idPasienAdmisi').val('');
                $('#idKelasPelayanan').val('');
//                location.reload();
            }
    },"json");
    return false; 
}        
</script>