<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
                'id'=>'zatBahanMakanan-m-search',
)); ?>

                                <?php echo $form->dropDownListRow($model,'zatgizi_id',
                                CHtml::listData($model->ZatgiziItems, 'zatgizi_id', 'zatgizi_nama'),
                                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>

                                <?php echo $form->dropDownListRow($model,'bahanmakanan_id',
                                CHtml::listData($model->BahanMakananItems, 'bahanmakanan_id', 'namabahanmakanan'),
                                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>

		<?php echo $form->textFieldRow($model,'kandunganbahan',array('class'=>'span2')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>