<?php
    $criteria = new CDbCriteria;
    $criteria->select = 'daftartindakan_m.daftartindakan_id, daftartindakan_m.daftartindakan_nama, t.daftartindakan_id, daftartindakan_m.daftartindakan_konsul,
                        daftartindakan_m.kelompoktindakan_id,daftartindakan_m.kategoritindakan_id,daftartindakan_m.kategoritindakan_id, t.tariftindakan_id,
                        t.kelaspelayanan_id, t.komponentarif_id, t.jenistarif_id, t.perdatarif_id, t.harga_tariftindakan, t.persendiskon_tind, t.hargadiskon_tind, t.persencyto_tind';
    
    $criteria->compare('daftartindakan_m.daftartindakan_konsul',true);
    $criteria->join = 'LEFT JOIN daftartindakan_m ON t.daftartindakan_id = daftartindakan_m.daftartindakan_id';
    
    $tarifTindakan = TariftindakanM::model()->findAll($criteria);
 
    
    foreach($tarifTindakan as $i=>$tindakan){
?>
<tr id="periksalab_<?php echo $idPemeriksaanLab; ?>">
    <td><?php echo CHtml::checkBox('LKTindakanPelayananT[cek][]',true);?></td>    
    <td>
        <?php echo $data->daftartindakan_nama; ?>
        <br/>
        <?php echo CHtml::hiddenField("periksalab_id[]", $idPemeriksaanLab,array('class'=>'inputFormTabel','readonly'=>true)); ?>
    </td>
    <td>
        <?php echo CHtml::hiddenField("PemeriksaanLab[$jenisPemeriksaanLabKelompok][$idPemeriksaanLab][pemeriksaanlab_id]", $idPemeriksaanLab,array('class'=>'inputFormTabel lebar1','readonly'=>true)); ?>
        <?php echo CHtml::hiddenField("PemeriksaanLab[$jenisPemeriksaanLabKelompok][$idPemeriksaanLab][jenispemeriksaanlab_kelompok]", $jenisPemeriksaanLabKelompok,array('class'=>'inputFormTabel lebar1','readonly'=>true)); ?>
        <?php echo CHtml::hiddenField("PemeriksaanLab[$jenisPemeriksaanLabKelompok][$idPemeriksaanLab][daftartindakan_id]", $idDaftarTindakan,array('class'=>'inputFormTabel','readonly'=>true)); ?>
        <?php echo CHtml::hiddenField("PemeriksaanLab[$jenisPemeriksaanLabKelompok][$idPemeriksaanLab][kelaspelayanan_id]", $idKelasPelayan,array('class'=>'inputFormTabel','readonly'=>true)); ?>
        <?php echo CHtml::textField("PemeriksaanLab[$jenisPemeriksaanLabKelompok][$idPemeriksaanLab][tarif_tindakan]", $tarif,array('class'=>'inputFormTabel lebar2','readonly'=>true)); ?>
    </td>
    <td><?php echo CHtml::textField("PemeriksaanLab[$jenisPemeriksaanLabKelompok][$idPemeriksaanLab][qty_tindakan]", '1',array('class'=>'inputFormTabel lebar1')); ?></td>
    <td><?php echo CHtml::dropDownList("PemeriksaanLab[$jenisPemeriksaanLabKelompok][$idPemeriksaanLab][satuantindakan]",'',SatuanTindakan::items(),array('style'=>'width:70px;','id'=>'satuan')) ?></td>
    <td><?php echo CHtml::dropDownList("PemeriksaanLab[$jenisPemeriksaanLabKelompok][$idPemeriksaanLab][cyto_tindakan]",0,array(1=>'Ya',0=>'Tidak'),array('style'=>'width:70px', 'class'=>'cyto_tindakan', 'onClick'=>'cekcyto(this, '. $idPemeriksaanLab .')')) ?></td>
    <td><?php echo CHtml::textField("PemeriksaanLab[$jenisPemeriksaanLabKelompok][$idPemeriksaanLab][tarif_cyto]", $totCyto,array('class'=>'inputFormTabel lebar2 cyto_'.$idPemeriksaanLab, 'style' => 'display:none')); ?></td>
</tr>
<?php } ?>