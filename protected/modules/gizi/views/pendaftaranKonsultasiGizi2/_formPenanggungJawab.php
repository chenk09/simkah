<fieldset>
    <legend class="accord1">
        <?php echo CHtml::checkBox('adaPenanggungJawab', $model->adaPenanggungJawab, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Penanggungjawab Pasien
    </legend>
    <div id="divPenanggungJawab" class="control-group toggle <?php echo ($model->adaPenanggungJawab) ? '':'hide'; ?>">
        <?php //echo $form->textFieldRow($model,'penanggungjawab_id', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

        <?php echo $form->dropDownListRow($modPenanggungJawab,'pengantar', Pengantar::items(), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modPenanggungJawab,'nama_pj', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Nama Penanggung Jawab')); ?>
        <?php echo $form->radioButtonListInlineRow($modPenanggungJawab,'jeniskelamin', JenisKelamin::items(), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php //echo $form->dropDownListRow($modPenanggungJawab,'jenisidentitas', JenisIdentitas::items(), array('empty'=>'-- Pilih --','class'=>'span1','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <div class="control-group ">
            <?php echo $form->labelEx($modPenanggungJawab,'jenisidentitas', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php echo $form->dropDownList($modPenanggungJawab,'jenisidentitas', JenisIdentitas::items(), array('empty'=>'-- Pilih --','class'=>'span2','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textField($modPenanggungJawab,'no_identitas', array('placeholder'=>$modPenanggungJawab->getAttributeLabel('no_identitas'),'class'=>'span2','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->error($modPenanggungJawab, 'no_identitas'); ?>
            </div>
        </div>
        <?php //echo $form->textFieldRow($modPenanggungJawab,'no_identitas', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->dropDownListRow($modPenanggungJawab,'hubungankeluarga', HubunganKeluarga::items(), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modPenanggungJawab,'tempatlahir_pj', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Tempat Lahir Penanggung Jawab')); ?>
        <?php //echo $form->textFieldRow($modPenanggungJawab,'tgllahir_pj', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <div class="control-group ">
            <?php echo $form->labelEx($modPenanggungJawab,'tgllahir_pj', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPenanggungJawab,
                                        'attribute'=>'tgllahir_pj',
                                        'mode'=>'date',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                )); ?>
                <?php echo $form->error($modPenanggungJawab, 'tgllahir_pj'); ?>
            </div>
        </div>
        <?php echo $form->textAreaRow($modPenanggungJawab,'alamat_pj', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Alamat Penanggung Jawab')); ?>
        <?php echo $form->textFieldRow($modPenanggungJawab,'no_teleponpj', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No Telephone Yang Dapat Dihubungi')); ?>
        <?php echo $form->textFieldRow($modPenanggungJawab,'no_mobilepj', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No Hp Yang Dapat Dihubungi')); ?>
        <?php //echo $form->checkBoxRow($modPenanggungJawab,'penanggungjawab_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    </div>
</fieldset>

<?php
$enableInputPJ = ($model->adaPenanggungJawab) ? 1 : 0;
$js = <<< JS
if(${enableInputPJ}) {
    $('#divPenanggungJawab input').removeAttr('disabled');
    $('#divPenanggungJawab select').removeAttr('disabled');
}
else {
    $('#divPenanggungJawab input').attr('disabled','true');
    $('#divPenanggungJawab select').attr('disabled','true');
}

$('#adaPenanggungJawab').change(function(){
    if ($(this).is(':checked')){
            $('#divPenanggungJawab input').removeAttr('disabled');
            $('#divPenanggungJawab select').removeAttr('disabled');
    }else{
            $('#divPenanggungJawab input').attr('disabled','true');
            $('#divPenanggungJawab select').attr('disabled','true');
    }
    $('#divPenanggungJawab').slideToggle(500);
});
JS;
Yii::app()->clientScript->registerScript('penanggungJawab',$js,CClientScript::POS_READY);
?>