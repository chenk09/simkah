<?php
$modKarcisTindakan = DaftartindakanM::model()->findAllByAttributes(array('daftartindakan_karcis'=>true,'daftartindakan_aktif'=>true));

?>
<!--
<fieldset id="fieldsetKarcis" class="toggle">
    <legend class="accord1">
        <?php //echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Karcis Tindakan
    </legend>
    <div id="daftarKarcis" class="control-group toggle">
        <div class="controls">
            <div class="radio inline">
                <div class="form-inline">
                <?php //echo CHtml::radioButtonList('karcisTindakan', '', CHtml::listData($modKarcisTindakan, 'daftartindakan_id', 'daftartindakan_nama'), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </div>
        <?php //echo CHtml::checkBox('bayarKarcisLangsung', '', array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Bayar Langsung
            </div>
        </div>
    </div>
</fieldset>-->

<?php
$enableInputKarcis = ($model->adaKarcis) ? 1 : 0;
$js = <<< JS
if(${enableInputKarcis}) {
    $('#daftarKarcis input').removeAttr('disabled');
    $('#daftarKarcis select').removeAttr('disabled');
}
else {
    $('#daftarKarcis input').attr('disabled','true');
    $('#daftarKarcis select').attr('disabled','true');
}

$('#karcisTindakan').change(function(){
        if ($(this).is(':checked')){
                $('#daftarKarcis input').removeAttr('disabled');
                $('#daftarKarcis select').removeAttr('disabled');
        }else{
                $('#daftarKarcis input').attr('disabled','true');
                $('#daftarKarcis select').attr('disabled','true');
                $('#daftarKarcis input').attr('value','');
                $('#daftarKarcis select').attr('value','');
        }
        $('#daftarKarcis').slideToggle(500);
    });
JS;
Yii::app()->clientScript->registerScript('karcis',$js,CClientScript::POS_READY);
?>
