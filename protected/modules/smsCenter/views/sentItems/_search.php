<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'sentitems-search',
        'type'=>'horizontal',
)); ?>

    <?php echo $form->textFieldRow($model,'updatedindb',array('class'=>'span5','maxlength'=>0)); ?>

    <?php echo $form->textFieldRow($model,'insertintodb',array('class'=>'span5','maxlength'=>0)); ?>

    <?php echo $form->textFieldRow($model,'sendingdatetime',array('class'=>'span5','maxlength'=>0)); ?>

    <?php echo $form->textFieldRow($model,'deliverydatetime',array('class'=>'span5','maxlength'=>0)); ?>

    <?php echo $form->textFieldRow($model,'text',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'destinationnumber',array('class'=>'span5','maxlength'=>20)); ?>

    <?php echo $form->textFieldRow($model,'coding',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textFieldRow($model,'udh',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'smscnumber',array('class'=>'span5','maxlength'=>20)); ?>

    <?php //echo $form->textFieldRow($model,'class',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'textdecoded',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'senderid',array('class'=>'span5','maxlength'=>255)); ?>

    <?php //echo $form->textFieldRow($model,'sequenceposition',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'status',array('class'=>'span5','maxlength'=>255)); ?>

    <?php echo $form->textFieldRow($model,'statuserror',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'tpmr',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'relativevalidity',array('class'=>'span5')); ?>

    <?php //echo $form->textAreaRow($model,'creatorid',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

    <div class="form-actions">
                        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
    </div>

<?php $this->endWidget(); ?>