<?php
$this->breadcrumbs=array(
	'Send Message',
);

$this->widget('bootstrap.widgets.BootMenu', array(
    'type' => 'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked' => false, // whether this is a stacked menu
    'items' => array(
        array('label' => 'Pasien', 'url' => $this->createUrl('/smsCenter/SendMessage', array('modulId' => Yii::app()->session['modulId']))),
        array('label' => 'Pegawai', 'url' => $this->createUrl('/smsCenter/SendMessagePegawai', array('modulId' => Yii::app()->session['modulId']))),
        array('label' => 'Umum', 'active' => true),
    ),
));

$this->renderPartial('_formUmum',array('model'=>$model));
?>