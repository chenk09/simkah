<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'inbox-search',
        'type'=>'horizontal',
)); ?>

    <?php //echo $form->textFieldRow($model,'UpdatedInDB',array('class'=>'span5','maxlength'=>0)); ?>

    <?php echo $form->textFieldRow($model,'receivingdatetime',array('class'=>'span5','maxlength'=>0)); ?>

    <?php echo $form->textFieldRow($model,'text',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'sendernumber',array('class'=>'span5','maxlength'=>20)); ?>

    <?php //echo $form->textFieldRow($model,'coding',array('class'=>'span5','maxlength'=>255)); ?>

    <?php //echo $form->textAreaRow($model,'udh',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

    <?php echo $form->textFieldRow($model,'smscnumber',array('class'=>'span5','maxlength'=>20)); ?>

    <?php //echo $form->textFieldRow($model,'class',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'textdecoded',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'ID',array('class'=>'span5')); ?>

    <?php echo $form->textFieldRow($model,'recipientid',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

    <?php //echo $form->checkBoxRow($model,'Processed'); ?>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
    </div>

<?php $this->endWidget(); ?>