<?php
$this->breadcrumbs=array(
	'Send Message',
);

$this->widget('bootstrap.widgets.BootMenu', array(
    'type' => 'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked' => false, // whether this is a stacked menu
    'items' => array(
        array('label' => 'Pasien', 'url' => '', 'active' => true),
        array('label' => 'Pegawai', 'url' => $this->createUrl('/smsCenter/SendMessagePegawai', array('modulId' => Yii::app()->session['modulId']))),
        array('label' => 'Umum', 'url' => $this->createUrl('/smsCenter/SendMessageUmum', array('modulId' => Yii::app()->session['modulId']))),
    ),
));

$this->renderPartial('_formPasien',array('model'=>$model,'modPasien'=>$modPasien));

//$this->widget('bootstrap.widgets.BootTabbable', array(
//    'type'=>'tabs', // 'tabs' or 'pills'
////    'placement'=>'left', // 'above', 'right', 'below' or 'left'
//    'tabs'=>array(
//        array('label'=>'Pasien', 'content'=>$this->renderPartial('_formPasien',array('model'=>$model,'modPasien'=>$modPasien),true)),
//        array('label'=>'Pegawai', 'content'=>$this->renderPartial('_formPegawai',array('model'=>$model),true)),
//        array('label'=>'Umum', 'content'=>$this->renderPartial('_formUmum',array('model'=>$model),true)),
//    ),
//)); 
?>