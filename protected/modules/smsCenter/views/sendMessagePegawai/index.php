<?php
$this->breadcrumbs=array(
	'Send Message',
);

$this->widget('bootstrap.widgets.BootMenu', array(
    'type' => 'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked' => false, // whether this is a stacked menu
    'items' => array(
        array('label' => 'Pasien', 'url' => $this->createUrl('/smsCenter/SendMessage', array('modulId' => Yii::app()->session['modulId']))),
        array('label' => 'Pegawai', 'active' => true),
        array('label' => 'Umum', 'url' => $this->createUrl('/smsCenter/SendMessageUmum', array('modulId' => Yii::app()->session['modulId']))),
    ),
));

$this->renderPartial('_formPegawai',array('model'=>$model,'modPegawai'=>$modPegawai));
?>