<?php

class MonitorController extends SBaseController
{
	public function actionAjaxMonitorInbox()
        {
            $criteria = new CDbCriteria;
            //$criteria->compare('DATE(receivingdatetime)',date('Y-m-d'));
            $criteria->addCondition('processed = FALSE');
            
            $inboxs = Inbox::model()->findAll($criteria);
            foreach ($inboxs as $i => $inbox) {
                $formatPesan = strtoupper($inbox->textdecoded);
                $text = explode('#',$formatPesan);
                switch (trim(strtoupper($text[0]))) {
                    case 'BJ':  // format pesan : BJ#NORM#ID_RUANGAN#ID_DOKTER#TGL_JANJI
                                $buatJanji = new BuatjanjipoliT;
                                $pasienId = PasienM::model()->findByAttributes(array('no_rekam_medik'=>trim($text[1])))->pasien_id;
                                $buatJanji->pasien_id = $pasienId;
                                $buatJanji->no_rekam_medik = trim($text[1]);
                                $buatJanji->ruangan_id = trim($text[2]);
                                $buatJanji->pegawai_id = trim($text[3]);
                                $buatJanji->tgljadwal = trim($text[4]).' 00:00:00';
                                $buatJanji->tglbuatjanji = date('Y-m-d H:i:s');
                                $buatJanji->harijadwal = $this->getHari($buatJanji->tgljadwal);
                                $buatJanji->byphone = true;
                                if($buatJanji->validate()) {
                                    if($buatJanji->save()) {
                                        $this->balasInbox($inbox, 'Permintaan Buat Janji Poli berhasil. Terimakasih. #innova-ehospital');
                                    }
                                } else echo '<pre>'.  print_r($buatJanji->getErrors(),1).'</pre>';

                        break;
                    case 'BK': // format pesan BK#NORM#ID_RUANGAN#NO_KAMAR#ID_KELASPELAYANAN#TGL_BOOKING
                                $booking = new BookingkamarT;
                                $pasienId = PasienM::model()->findByAttributes(array('no_rekam_medik'=>trim($text[1])))->pasien_id;
                                $booking->pasien_id = $pasienId;
                                $booking->ruangan_id = trim($text[2]);
                                $booking->kamarruangan_id = trim($text[3]);
                                $booking->kelaspelayanan_id = trim($text[4]);
                                $booking->tglbookingkamar = trim($text[5]).' '.date('H:i:s');
                                $booking->bookingkamar_no = Generator::noBookingKamar();
                                $booking->tgltransaksibooking = date('Y-m-d H:i:s');
                                $booking->statusbooking = 'NON ANTRI';
                                if($booking->validate()) {
                                    if($booking->save()) {
                                        $this->balasInbox($inbox, 'Permintaan Booking Kamar berhasil. Terimakasih. #innova-ehospital');
                                    }
                                } else echo '<pre>'.  print_r($booking->getErrors(),1).'</pre>';

                        break;

                    default:
                        break;
                };
            }
        }
        
        public function getHari($tanggalWaktu)
        {
                $format = new CustomFormat();

                $tanggal=trim(substr($tanggalWaktu,0,-8)); //Menampilkan Tanggal Tanpa Jam
                $tanggalDB = $format->formatDateMediumForDB($tanggal);//Mengubah Tanggal inputan ke tanggal database
                $hari=date('l', strtotime($tanggalDB)); //Mendapatkan nilai hari dari tanggal yang dipilih

                if(strtolower($hari)=='sunday') {
                        $hari='Minggu';
                } else if(strtolower($hari)=='monday') {
                        $hari='Senin';
                } else if(strtolower($hari)=='tuesday') {
                        $hari='Selasa';
                } else if(strtolower($hari)=='wednesday') {
                        $hari='Rabu';
                } else if(strtolower($hari)=='thursday') {
                        $hari='Kamis';
                } else if(strtolower($hari)=='friday') {
                        $hari='Jumat';
                } else if(strtolower($hari)=='saturday') {
                        $hari='Sabtu';
                }    

                return $hari;
        }

        
        protected function balasInbox($inbox,$textBalasan)
        {
            $outbox= new Outbox;
            $outbox->destinationnumber = $inbox->sendernumber;
            $outbox->textdecoded = $textBalasan;//'Permintaan buat janji poli Anda segera diproses. Terimakasih.';
            $outbox->creatorid = 'innova';
            if($outbox->save()) {
                $inbox->processed = true;
                $inbox->update();
            }
        }
        
        protected function buatJanji()
        {
            
        }
        
        protected function bookingKamar(){
            
        }

        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}