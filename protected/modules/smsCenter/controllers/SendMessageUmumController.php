<?php

class SendMessageUmumController extends SBaseController
{
	public function actionIndex()
	{
            $model = new Outbox;
            $model->creatorid = Yii::app()->user->name;
            
            $modPasien = new PasienM('searchNoMobile');
            if(isset($_GET['PasienM'])) {
                $modPasien->unsetAttributes();
                $modPasien->attributes = $_GET['PasienM'];
            }
            
            if(isset($_POST['Outbox'])) {
                $noTujuan = explode(',', $_POST['Outbox']['destinationnumber']);
                for($i=0;$i<count($noTujuan);$i++){
                    $model = new Outbox;
                    $model->attributes = $_POST['Outbox'];
                    $model->destinationnumber = trim($noTujuan[$i]);
                    if($model->validate()){
                        $model->save();
                    }
                }
                
                $this->redirect(array('sentItems/admin','modulId'=>Yii::app()->session['modulId']));
            }
		
            $this->render('index',array('model'=>$model));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}