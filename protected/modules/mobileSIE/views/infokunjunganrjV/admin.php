<?php $this->renderPartial('mobileSIE.views._search', array('model' => $model, 'id' => 'garis')); ?>
<table>
    <tr>
        <td width="50%">
            <div id="garis" style='width: 100%;'>
                <?php $this->renderPartial('mobileSIE.views._grafik', array('dataProvider' => $value['dataProvider'], 'id' => 'garis')); ?>
            </div>
        </td>
        <td width="50%">
            <div id="spedo" style='width: 100%;'>
                <?php $this->renderPartial('mobileSIE.views._spedo', array('dataProvider'=>$value['dataProviderSpedo'],'title'=>$value['title'])); ?>
            </div>
        </td>
    </tr>
    <tr>
        <td width="50%">
            <div id="batang" style='width: 100%;z-index:2;'>
                <?php $this->renderPartial('mobileSIE.views._grafik', array('dataProvider' => $value['dataProviderBatang'], 'id' => 'batang')); ?>
            </div>
        </td>
        <td width="50%">
            <div id="pie" style='width: 100%;z-index:2;'>
                <?php $this->renderPartial('mobileSIE.views._grafik', array('dataProvider' => $value['dataProviderPie'], 'id' => 'pie')); ?>
            </div>
        </td>
    </tr>
</table>

<?php
$url = Yii::app()->createUrl($this->route);
$js = <<< JS
$('#garis').bind('jqplotDblClick', 
    function (ev, seriesIndex, pointIndex, data,a) {
        kirim = $('#searchLaporan').serialize();
        $.post('index.php?'+kirim,{test:'test',ruangan_nama:a.options.axes.xaxis.ticks[(data.data[0])-1]}, function(hasil){
            plot_pie.destroy();
            plot_pie.series[0].data = hasil.pie;
            plot_pie.axes.xaxis.ticks = hasil.pie;
            plot_pie.redraw();
            setValue_spedo(hasil.spedo);
        },'json');
    });
        
    setInterval(function(){
        kirim = $('#searchLaporan').serialize();
        $.post('index.php?'+kirim,{data:1},function(hasil){
            plot_pie.destroy();
            plot_pie.series[0].data = hasil.pie.result;
            plot_pie.axes.xaxis.ticks = hasil.pie.index;
            plot_pie.replot();

            plot_garis.destroy();
            plot_garis.series[0].data = hasil.garis.result;
            plot_garis.axes.xaxis.ticks = hasil.garis.index;
            plot_garis.axes.xaxis.tickOptions = (hasil.garis.index.length > 4 ) ? {angle:-30} : {angle:-0};
            plot_garis.replot({resetAxes:['yaxis'],axes:{yaxis:{min:-1, pad:5}}});
            
            plot_batang.destroy();
            plot_batang.series[0].data = hasil.batang.result;
            plot_batang.axes.xaxis.ticks = hasil.batang.index;
            plot_batang.axes.xaxis.tickOptions = (hasil.batang.index.length > 4 ) ? {angle:-30} : {angle:-0};
            plot_batang.replot({resetAxes:['yaxis'],axes:{yaxis:{min:-1, pad:5}}});
            setValue_spedo(hasil.spedo.jumlah);
        },'json');
    },3000);
JS;

?>