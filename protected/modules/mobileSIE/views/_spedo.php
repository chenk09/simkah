<?php
    $this->widget('ext.jQPlot.widget.gaugeMeterChart', array(
                'dataProvider' => $dataProvider,
//                                'autoUpdate'=>array(  //you can use this property to set this widget autoupdate 
//                                    'bind'=>array(
//                                        'form'=>'#searchLaporan',
//                                    ),
//                                    'url'=>Yii::app()->createUrl($this->route),
//                                ),
                'rendererOptions'=>array(
                    'min'=>0,
                    'max'=>array_pop(Params::intervalSpedometerGrafik()),
                    'intervals'=>Params::intervalSpedometerGrafik(), // interval must be an array
                    'label'=> $title,
                    'labelPosition'=>'bottom',
                    'labelHeightAdjust'=> -5,
                    'intervalOuterRadius'=> 85,
                ),
                'id' => 'spedo',
            )
    );
?>