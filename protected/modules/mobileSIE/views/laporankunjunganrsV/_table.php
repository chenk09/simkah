<?php
    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $sort = true;
    if (isset($caraPrint)) {
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else {
        $data = $model->searchTable();
        $template = "{summary}\n{items}{pager}";
    }
?>
<?php
    $this->widget($table, array(
        'id' => 'tableLaporan',
        'dataProvider' => $data,
        'template' => $template,
        'enableSorting' => $sort,
        'itemsCssClass' => 'table table-striped table-bordered table-condensed',
        'mergeColumns' => array('instalasi_nama', 'ruangan_nama'),
        'columns' => array(
            'instalasi_nama',
            'ruangan_nama',
            'no_rekam_medik',
            'nama_pasien',
            'jeniskasuspenyakit_nama',
        ),
    ));
?> 