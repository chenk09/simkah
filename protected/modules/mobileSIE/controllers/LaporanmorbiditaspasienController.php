
<?php

class LaporanmorbiditaspasienController extends SBaseController
{
	public $layout='//layouts/columnmobile';
    public $defaultAction = 'admin';

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin'),
				'users'=>array('@'),
			),
		);
	}

	public function actionAdmin()
	{
            $model = new SELaporanmorbiditasV('search');
            $model->tglAwal = date('Y-m-d').' 00:00:00';
            $model->tglAkhir = date('Y-m-d 23:59:59');
            if (isset($_GET['SELaporanmorbiditasV'])) {
                $model->attributes = $_GET['SELaporanmorbiditasV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['SELaporanmorbiditasV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['SELaporanmorbiditasV']['tglAkhir']);
            }
            
            $value['dataProvider'] = $model->searchDashboard();
            $value['dataProviderSpedo'] = $model->searchTable();
            $value['dataProviderPie'] = $model->searchDashboardPie();
            $value['title'] = 'Jumlah Pasien';
            if (isset($_POST['test'])){
                $model->instalasi_id = InstalasiM::model()->findByAttributes(array('instalasi_nama'=>$_POST['ruangan_nama']))->instalasi_id;
                $model->ruangan_id = CHtml::listData(RuanganM::model()->findAllByAttributes(array('instalasi_id'=>$model->instalasi_id)), 'ruangan_id','ruangan_id');
                
                $hasil = $value['dataProvider']->getData(); 
                $result = array();
                $jumlah = 0;
                foreach ($hasil as $i=>$v){
                    $jumlah += (int)$v['jumlah'];
                    $result[] = array($v['data'],(int)$v['jumlah']);
                }
                $data['pie'] = $result;
                $data['spedo'] = $jumlah;
                echo json_encode($data);
                exit();

            }
            if (isset($_POST['data'])){
                $hasil = $value['dataProvider']->getData(); 
                $hasil2 = $value['dataProviderPie']->getData(); 
                $jumlah = $value['dataProviderSpedo']->getTotalItemCount();
                $result = array();
                $result2 = array();
                $index = array();
                $SeriesTick = array();
                $SeriesData = array();
                foreach ($hasil as $i=>$v){
                    if (!in_array($v['tick'], $SeriesTick)){
                        $SeriesTick[] = $v['tick'];
                    }
                    if (!in_array($v['data'],$SeriesData)){
                        $SeriesData[] = $v['data'];
                    }
                }
                
                foreach ($hasil2 as $i=>$v){
                    $result[] = array($v['data'],(int)$v['jumlah']);
                }
                        
                foreach ($SeriesData as $i => $v){
                    $var1 = array();
                    foreach ($SeriesTick as $j => $val){
                        $var2 = 0;
                        foreach ($hasil as $x => $value){
                            if ($value['tick'] == $val && $value['data'] == $v){
                                $var2 += $value['jumlah'];
                            }
                        }
                        if ($var2 > 0){
                            $var1[] = array($j+1,$var2);
                        }
                    }
                    $result2[] = $var1;
                }
                $index = $SeriesTick;
                $return['pie']['result'] = $result;
                $return['pie']['index'] = $index;
                $return['garis']['result'] = $result2;
                $return['garis']['index'] = $index;
                $return['batang']['result'] = $result2;
                $return['batang']['index'] = $index;
                $return['spedo']['jumlah'] = $jumlah;
                echo json_encode($return);
                exit();
            }

            $this->render('admin', array('model'=>$model, 'value'=>$value));
	}

	public function loadModel($id)
	{
		$model=SELaporankunjunganrsV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='selaporankunjunganrs-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}