
<?php

class InfokunjunganriVController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/columnmobile';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	// public function filters()
	// {
	// 	return array(
	// 		'accessControl', // perform access control for CRUD operations
	// 	);
	// }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin'),
				'users'=>array('@'),
			),
			// array('allow', // allow authenticated user to perform 'create' and 'update' actions
			// 	'actions'=>array('create','update','print'),
			// 	'users'=>array('@'),
			// ),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('admin','delete','RemoveTemporary'),
			// 	'users'=>array('@'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
            $model = new SEInfokunjunganriV('search');
            $model->tglAwal = date('Y-m-d').' 00:00:00';
            $model->tglAkhir = date('Y-m-d 23:59:59');
            if (isset($_GET['SEInfokunjunganriV'])) {
                $model->attributes = $_GET['SEInfokunjunganriV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['SEInfokunjunganriV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['SEInfokunjunganriV']['tglAkhir']);
            }
            
            $value['dataProvider'] = $model->searchDashboard();
            $value['dataProviderBatang'] = $model->searchDashboardBatang();
            $value['dataProviderPie'] = $model->searchDashboardPie();
            $value['dataProviderSpedo'] = $model->searchRI();
//            $value['dataProviderBatang'] = $model->searchGrafikBatang();
//            $value['dataProviderPie'] = $model->searchGrafikPie();
            $value['title'] = 'Jumlah Kunjungan Rawat Inap';

            if (isset($_POST['test'])){
                $model->instalasi_id = InstalasiM::model()->findByAttributes(array('instalasi_nama'=>$_POST['ruangan_nama']))->instalasi_id;
                $model->ruangan_id = CHtml::listData(RuanganM::model()->findAllByAttributes(array('instalasi_id'=>$model->instalasi_id)), 'ruangan_id','ruangan_id');
                
                $hasil = $value['dataProvider']->getData(); 
                $result = array();
                $jumlah = 0;
                foreach ($hasil as $i=>$v){
                    $jumlah += (int)$v['jumlah'];
                    $result[] = array($v['data'],(int)$v['jumlah']);
                }
                $data['pie'] = $result;
                $data['spedo'] = $jumlah;
                echo json_encode($data);
                exit();

            }
            if (isset($_POST['data'])){
                $hasil = $value['dataProvider']->getData(); 
                $hasil2 = $value['dataProviderBatang']->getData(); 
                $hasil3 = $value['dataProviderPie']->getData(); 
                $jumlah = $value['dataProviderSpedo']->getTotalItemCount();
                $result = array();
                $index = array();
                foreach ($hasil as $i=>$v){
                    $index[] = $v['data'];
                    $result[] = array($i+1,(int)$v['jumlah']);
                }
                $result2 = array();
                $index2 = array();
                foreach ($hasil2 as $i=>$v){
                    $index2[] = $v['data'];
                    $result2[] = array($i+1,(int)$v['jumlah']);
                }
                $result3 = array();
                $index3 = array();
                foreach ($hasil3 as $i=>$v){
                    $index3[] = $v['tick'];
                    $result3[] = array($v['tick'],(int)$v['jumlah']);
                }
                $return['garis']['result'] = $result;
                $return['garis']['index'] = $index;
                $return['pie']['result'] = $result3;
                $return['pie']['index'] = $index3;
                $return['batang']['result'] = $result2;
                $return['batang']['index'] = $index2;
                $return['spedo']['jumlah'] = $jumlah;
                echo json_encode($return);
                exit();
            }
            
            $this->render('admin', array('model'=>$model, 'value'=>$value));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SEInfokunjunganriV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seinfokunjunganri-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
