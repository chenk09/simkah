
<?php

class LaporankunjunganrsVController extends SBaseController
{
	public $layout='//layouts/columnmobile';
    public $defaultAction = 'admin';

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin'),
				'users'=>array('@'),
			),
		);
	}

	public function actionAdmin()
	{
            $model = new SELaporankunjunganrsV('search');
            $model->tglAwal = date('Y-m-d').' 00:00:00';
            $model->tglAkhir = date('Y-m-d 23:59:59');
            
            if (isset($_GET['SELaporankunjunganrsV'])) {
                $model->attributes = $_GET['SELaporankunjunganrsV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['SELaporankunjunganrsV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['SELaporankunjunganrsV']['tglAkhir']);
            }
            
            $value['dataProvider'] = $model->searchDashboard();
            $value['dataProviderSpedo'] = $model->searchTable();
            $value['title'] = 'Jumlah Kunjungan Rumah Sakit';
            if (isset($_POST['test'])){
                $model->instalasi_id = InstalasiM::model()->findByAttributes(array('instalasi_nama'=>$_POST['ruangan_nama']))->instalasi_id;
                $model->ruangan_id = CHtml::listData(RuanganM::model()->findAllByAttributes(array('instalasi_id'=>$model->instalasi_id)), 'ruangan_id','ruangan_id');
                
                $hasil = $value['dataProvider']->getData(); 
                $result = array();
                $jumlah = 0;
                foreach ($hasil as $i=>$v){
                    $jumlah += (int)$v['jumlah'];
                    $result[] = array($v['data'],(int)$v['jumlah']);
                }
                $data['pie'] = $result;
                $data['spedo'] = $jumlah;
                echo json_encode($data);
                exit();

            }
            if (isset($_POST['data'])){
                $hasil = $value['dataProvider']->getData(); 
                $jumlah = $value['dataProviderSpedo']->getTotalItemCount();
                $result = array();
                $result2 = array();
                $index = array();
                $index2 = array();
                foreach ($hasil as $i=>$v){
                    $index[] = $v['data'];
                    $result[] = array($v['data'],(int)$v['jumlah']);
                    $result2[] = array($i+1,(int)$v['jumlah']);
                }
                $return['pie']['result'] = $result;
                $return['pie']['index'] = $index;
                $return['garis']['result'] = $result2;
                $return['garis']['index'] = $index;
                $return['spedo']['jumlah'] = $jumlah;
                echo json_encode($return);
                exit();
            }

            $this->render('admin', array('model'=>$model, 'value'=>$value));
	}

	public function loadModel($id)
	{
		$model=SELaporankunjunganrsV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='selaporankunjunganrs-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}