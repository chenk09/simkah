<?php
class SELaporanmortalitaspasienV extends LaporanmortalitaspasienV
{
        public $kondisipulang1, $kondisipulang2, $jumlahkunjungan;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
         * method untuk criteria
         * @return CActiveDataProvider 
         */
        protected function functionCriteria()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->select = "golonganumur_nama, tglmorbiditas, diagnosa_nama,umur_0_28hr,umur_28hr_1thn, umur_1_4thn,umur_5_14thn,umur_15_24thn,umur_25_44thn,umur_45_64thn, umur_65, kondisipulang, pendaftaran_id , CASE WHEN kondisipulang = '".Params::KODE_MENINGGAL_1."' THEN 1 else 0 END AS kondisipulang1, CASE WHEN kondisipulang = '".Params::KODE_MENINGGAL_1."' THEN 0 else 1 END AS kondisipulang2";
                $criteria->group = "golonganumur_nama, tglmorbiditas, diagnosa_nama,umur_0_28hr,umur_28hr_1thn, umur_1_4thn,umur_5_14thn,umur_15_24thn,umur_25_44thn,umur_45_64thn, umur_65, kondisipulang, pendaftaran_id";
                    
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
//		$criteria->compare('LOWER(tglmorbiditas)',strtolower($this->tglmorbiditas),true);
                $criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
		$criteria->compare('umur_0_28hr',$this->umur_0_28hr);
		$criteria->compare('umur_28hr_1thn',$this->umur_28hr_1thn);
		$criteria->compare('umur_1_4thn',$this->umur_1_4thn);
		$criteria->compare('umur_5_14thn',$this->umur_5_14thn);
		$criteria->compare('umur_15_24thn',$this->umur_15_24thn);
		$criteria->compare('umur_25_44thn',$this->umur_25_44thn);
		$criteria->compare('umur_45_64thn',$this->umur_45_64thn);
		$criteria->compare('umur_65',$this->umur_65);
		$criteria->compare('diagnosa_id',$this->diagnosa_id);
		$criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
		$criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
		$criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
		$criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
		$criteria->compare('golonganumur_id',$this->golonganumur_id);
		$criteria->compare('LOWER(kondisipulang)',strtolower($this->kondisipulang),true);
		$criteria->compare('LOWER(carakeluar)',strtolower($this->carakeluar),true);

		return $criteria;
	}
        /**
         * method data provider laporan
         * @return CActiveDataProvider 
         */
        public function searchPrint(){
            $criteria = $this->functionCriteria();
            $crit = new MyCriteria();
            $crit->select = 'diagnosa_nama, sum(umur_0_28hr) as umur_0_28hr,sum(umur_28hr_1thn) as umur_28hr_1thn, sum(umur_1_4thn) as umur_1_4thn, sum(umur_5_14thn) as umur_5_14thn, sum(umur_15_24thn) as umur_15_24thn,sum(umur_25_44thn) as umur_25_44thn,sum(umur_45_64thn) as umur_45_64thn, sum(umur_65) as umur_65, count(pendaftaran_id) as jumlah, sum(kondisipulang1) as kondisipulang1 , sum(kondisipulang2) as kondisipulang2, count(pendaftaran_id) as jumlahkunjungan';
            $crit->group = 'diagnosa_nama';
            $crit->mergeWith($criteria);
            return new CActiveDataProvider($this, array(
			'criteria'=>$crit,
                        'pagination'=>false,
		));
        }
        /**
         * method untuk data provider table
         * @return CActiveDataProvider 
         */
        public function searchTable(){
            $criteria = $this->functionCriteria();
            $crit = new MyCriteria();
            $crit->select = 'diagnosa_nama, sum(umur_0_28hr) as umur_0_28hr,sum(umur_28hr_1thn) as umur_28hr_1thn, sum(umur_1_4thn) as umur_1_4thn, sum(umur_5_14thn) as umur_5_14thn, sum(umur_15_24thn) as umur_15_24thn,sum(umur_25_44thn) as umur_25_44thn,sum(umur_45_64thn) as umur_45_64thn, sum(umur_65) as umur_65, count(pendaftaran_id) as jumlah, sum(kondisipulang1) as kondisipulang1 , sum(kondisipulang2) as kondisipulang2, count(pendaftaran_id) as jumlahkunjungan';
            $crit->group = 'diagnosa_nama';
            $crit->mergeWith($criteria);
            return new CActiveDataProvider($this, array(
			'criteria'=>$crit,
		));
        }
        /**
         * method untuk data provider Grafik
         * @return CActiveDataProvider 
         */
        public function searchDashboard(){
            $criteria = $this->functionCriteria();
            $crit = new MyCriteria();
            $crit->select = 'golonganumur_nama as data, kondisipulang as tick, count(pendaftaran_id) as jumlah';
            $crit->group = 'golonganumur_nama,kondisipulang';
            $crit->mergeWith($criteria);
            return new CActiveDataProvider($this, array(
			'criteria'=>$crit,
		));
        }
        
        public function primaryKey() {
            return 'pendaftaran_id';
        }
        
        public function searchDashboardPie()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->select = 'kondisipulang as data, count(pendaftaran_id) as jumlah';
                $criteria->group = 'kondisipulang';
                $criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(tglmorbiditas)',strtolower($this->tglmorbiditas),true);
		$criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
		$criteria->compare('umur_0_28hr',$this->umur_0_28hr);
		$criteria->compare('umur_28hr_1thn',$this->umur_28hr_1thn);
		$criteria->compare('umur_1_4thn',$this->umur_1_4thn);
		$criteria->compare('umur_5_14thn',$this->umur_5_14thn);
		$criteria->compare('umur_15_24thn',$this->umur_15_24thn);
		$criteria->compare('umur_25_44thn',$this->umur_25_44thn);
		$criteria->compare('umur_45_64thn',$this->umur_45_64thn);
		$criteria->compare('umur_65',$this->umur_65);
		$criteria->compare('diagnosa_id',$this->diagnosa_id);
		$criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
		$criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
		$criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
		$criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
		$criteria->compare('golonganumur_id',$this->golonganumur_id);
		$criteria->compare('LOWER(kondisipulang)',strtolower($this->kondisipulang),true);
		$criteria->compare('LOWER(carakeluar)',strtolower($this->carakeluar),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}