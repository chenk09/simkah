<?php
class SELaporanmorbiditasV extends LaporanmorbiditasV
{
        public $lakilaki,$perempuan,$jumlahkunjungan;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        /**
         *  method untuk criteria
         * @return CDbCriteria 
         */
        protected function functionCriteria()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->select = "golonganumur_nama, tglmorbiditas, diagnosa_nama,umur_0_28hr,umur_28hr_1thn, umur_1_4thn,umur_5_14thn,umur_15_24thn,umur_25_44thn,umur_45_64thn, umur_65, jeniskelamin, pendaftaran_id , CASE WHEN jeniskelamin = '".Params::JENIS_KELAMIN_PEREMPUAN."' THEN 0 else 1 END AS lakilaki, CASE WHEN jeniskelamin = '".Params::JENIS_KELAMIN_PEREMPUAN."' THEN 1 else 0 END AS perempuan";
                $criteria->group = "golonganumur_nama, tglmorbiditas, diagnosa_nama,umur_0_28hr,umur_28hr_1thn, umur_1_4thn,umur_5_14thn,umur_15_24thn,umur_25_44thn,umur_45_64thn, umur_65, jeniskelamin, pendaftaran_id";
//               
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
		$criteria->compare('umur_0_28hr',$this->umur_0_28hr);
		$criteria->compare('umur_28hr_1thn',$this->umur_28hr_1thn);
		$criteria->compare('umur_1_4thn',$this->umur_1_4thn);
		$criteria->compare('umur_5_14thn',$this->umur_5_14thn);
		$criteria->compare('umur_15_24thn',$this->umur_15_24thn);
		$criteria->compare('umur_25_44thn',$this->umur_25_44thn);
		$criteria->compare('umur_45_64thn',$this->umur_45_64thn);
		$criteria->compare('umur_65',$this->umur_65);
		$criteria->compare('diagnosa_id',$this->diagnosa_id);
		$criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
		$criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
		$criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
		$criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
		$criteria->compare('golonganumur_id',$this->golonganumur_id);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);

		return $criteria;
	}
        /**
         * method untuk data provider grafik pada sistemInformasiEksekutif/Laporanmorbiditaspasien/Admin
         * @return CActiveDataProvider 
         */
        public function searchTable(){
                $criteria=new CDbCriteria;
            	
                $criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(tglmorbiditas)',strtolower($this->tglmorbiditas),true);
		$criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
		$criteria->compare('umur_0_28hr',$this->umur_0_28hr);
		$criteria->compare('umur_28hr_1thn',$this->umur_28hr_1thn);
		$criteria->compare('umur_1_4thn',$this->umur_1_4thn);
		$criteria->compare('umur_5_14thn',$this->umur_5_14thn);
		$criteria->compare('umur_15_24thn',$this->umur_15_24thn);
		$criteria->compare('umur_25_44thn',$this->umur_25_44thn);
		$criteria->compare('umur_45_64thn',$this->umur_45_64thn);
		$criteria->compare('umur_65',$this->umur_65);
		$criteria->compare('diagnosa_id',$this->diagnosa_id);
		$criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
		$criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
		$criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
		$criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
		$criteria->compare('golonganumur_id',$this->golonganumur_id);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);

            return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        /**
         * method untuk data provider grafik pada sistemInformasiEksekutif/Laporanmorbiditaspasien/Admin
         * @return CActiveDataProvider 
         */
        public function searchDashboard(){
            $criteria = $this->functionCriteria();
            $crit = new MyCriteria();
            $crit->select = 'golonganumur_nama as tick, jeniskelamin as data, count(pendaftaran_id) as jumlah';
            $crit->group = 'golonganumur_nama,jeniskelamin';
            $crit->mergeWith($criteria);
            return new CActiveDataProvider($this, array(
			'criteria'=>$crit,
		));
        }
        /**
         * method untuk data provider grafik pada sistemInformasiEksekutif/Laporanmorbiditaspasien/Admin
         * @return CActiveDataProvider 
         */
        public function searchDashboardPie(){
            $criteria=new CDbCriteria;
            	
            $criteria->select = 'jeniskelamin as data, count(pendaftaran_id) as jumlah';
            $criteria->group = 'jeniskelamin';
            $criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
            $criteria->compare('LOWER(tglmorbiditas)',strtolower($this->tglmorbiditas),true);
            $criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
            $criteria->compare('umur_0_28hr',$this->umur_0_28hr);
            $criteria->compare('umur_28hr_1thn',$this->umur_28hr_1thn);
            $criteria->compare('umur_1_4thn',$this->umur_1_4thn);
            $criteria->compare('umur_5_14thn',$this->umur_5_14thn);
            $criteria->compare('umur_15_24thn',$this->umur_15_24thn);
            $criteria->compare('umur_25_44thn',$this->umur_25_44thn);
            $criteria->compare('umur_45_64thn',$this->umur_45_64thn);
            $criteria->compare('umur_65',$this->umur_65);
            $criteria->compare('diagnosa_id',$this->diagnosa_id);
            $criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
            $criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
            $criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
            $criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
            $criteria->compare('golonganumur_id',$this->golonganumur_id);
            $criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);

            return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

}