
<?php

class PemeriksaanFisikController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
        protected $pathView = 'rawatJalan.views.pemeriksaanFisik.';
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	
	

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	
	public function actionIndex($idPendaftaran)
	{   
//            $result = $this->xmlParser();
            
   	    $format = new CustomFormat();
            $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
            //$modRJMetodeGSCM = RJMetodeGCSM::model()->findAll('metodegcs_aktif=TRUE ORDER BY metodegcs_singkatan,metodegcs_nilai DESC');
            $modRJMetodeGSCM = RJMetodeGCSM::model()->findAll('metodegcs_aktif=TRUE ORDER BY metodegcs_id');
            $cekPemeriksaanFisik=RJPemeriksaanFisikT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
                if(COUNT($cekPemeriksaanFisik)>0)
                    {  //Jika Pasien Sudah Melakukan Pemeriksaan Fisik  Sebelumnya
                        $modPemeriksaanFisik=$cekPemeriksaanFisik;
                    }
                else
                    {  //Jika Pasien Belum Pernah melakukan Pemeriksaan Fisik
                        $modPemeriksaanFisik=new RJPemeriksaanFisikT;
                        $modPemeriksaanFisik->pegawai_id=$modPendaftaran->pegawai_id;
                        $modPemeriksaanFisik->pendaftaran_id=$modPendaftaran->pendaftaran_id;
                        $modPemeriksaanFisik->pasien_id=$modPasien->pasien_id;
                        $modPemeriksaanFisik->tglperiksafisik=date('Y-m-d H:i:s');
                    }
//            $modPemeriksaanFisik->td_diastolic = $result[2];
//            $modPemeriksaanFisik->td_systolic = $result[1];
//            $modPemeriksaanFisik->detaknadi = $result[3];
//            
//            $modPemeriksaanFisik->tekanandarah = $this->panjangText($modPemeriksaanFisik->td_diastolic, $modPemeriksaanFisik->td_systolic);;
//            echo $modPemeriksaanFisik->tekanandarah;exit();
                    if(isset($_POST['RJPemeriksaanFisikT']))
                    {
                        $transaction = Yii::app()->db->beginTransaction();
                        try {
                                $modPemeriksaanFisik->attributes=$_POST['RJPemeriksaanFisikT'];  
                                $modPemeriksaanFisik->keadaanumum = (count($_POST['RJPemeriksaanFisikT']['keadaanumum'])>0) ? implode(', ', $_POST['RJPemeriksaanFisikT']['keadaanumum']) : ''; 
                                $modPemeriksaanFisik->tglperiksafisik=$format->formatDateTimeMediumForDB($_POST['RJPemeriksaanFisikT']['tglperiksafisik']);

                                if($modPemeriksaanFisik->validate()){
                                   if($modPemeriksaanFisik->save())
								   {
										/*
										$updateStatusPeriksa = PendaftaranT::model()->updateByPk($idPendaftaran,array(
											'statusperiksa'=>Params::statusPeriksa(2)
										));
										*/
										$transaction->commit();
                                   }else{
                                       echo "gagal Simpan";exit;
                                   } 

                                }  
                                Yii::app()->user->setFlash('success',"Data Pemeriksaan Fisik berhasil disimpan");
                                $this->redirect($_POST['url']); 
                             }
                        catch (Exception $exc) 
                            {
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data Pemeriksaan Fisik gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                            }
                    } 
                  $modPemeriksaanFisik->tglperiksafisik = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modPemeriksaanFisik->tglperiksafisik, 'yyyy-MM-dd hh:mm:ss'));       
		
                  $this->render($this->pathView.'index',array('modPasien'=>$modPasien,
                        'modPemeriksaanFisik'=>$modPemeriksaanFisik,
                        'modPendaftaran'=>$modPendaftaran,
                        'modRJMetodeGSCM'=>$modRJMetodeGSCM
		));

	}
        
//        protected function xmlParser(){            
//            $file = dirname('c:/').'/data/xml/ostar.xml';
//            echo $file;
//                    ////'http://www.php.net/feed.atom';
//            $data = simplexml_load_file($file);
////            print_r($data);
//            //echo count($data);
//            
//            $result = array($data->BPMRecord[0]['Date_Time'], $data->BPMRecord[0]['H'], $data->BPMRecord[0]['L'], $data->BPMRecord[0]['P']);
//            return $result;
//        }
//        
//        protected function panjangText($a,$b){
//            $tambah = '';
//            if (strlen($a) < 3){
//                for($i = strlen($a); $i < 3; $i++){
//                    $tambah = $tambah.'0';
//                }
//                $a = $tambah.$a;
//            }
//            $tambah = '';
//            if (strlen($b) < 3){
//                for($i = strlen($b); $i < 3; $i++){
//                    $tambah = $tambah.'0';
//                }
//                $b = $tambah.$b;
//            }
//            
//            return $b.' / '.$a;
//        }

	
}
