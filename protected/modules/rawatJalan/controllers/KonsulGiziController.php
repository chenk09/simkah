<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class KonsulGiziController extends SBaseController
{
    public $successSave = false;
    public $successSaveTindakan = true;
    protected $statusSaveKirimkeUnitLain = false;
    protected $statusSavePermintaanPenunjang = false;
    protected $pathView = 'rawatJalan.views.konsulGizi.';
    
	public function actionIndex($idPendaftaran)
	{
            $insert_notifikasi = new MyFunction();
            $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modKirimKeUnitLain = new RJPasienKirimKeUnitLainT;
            $modKirimKeUnitLain->tgl_kirimpasien = date('Y-m-d H:i:s');
            $modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
            $modKirimKeUnitLain->ruangan_id = Params::RUANGAN_ID_GIZI;
            


            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();


            if(isset($_POST['RJPasienKirimKeUnitLainT'])) {
            
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modKirimKeUnitLain = $this->savePasienKirimKeUnitLain($modPendaftaran);
                    if(isset($_POST['permintaanPenunjang'])){
                        $this->savePermintaanPenunjang($_POST['permintaanPenunjang'],$modKirimKeUnitLain);
                        
                        PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id,
                            array(
                                'pembayaranpelayanan_id'=>null
                            )
                        );
                        
                        $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                        $params['create_time'] = date( 'Y-m-d H:i:s');
                        $params['create_loginpemakai_id'] = Yii::app()->user->id;
                        $params['instalasi_id'] = Params::INSTALASI_ID_GIZI;
                        $params['modul_id'] = 15;
                        $ruangan = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'));
                        $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $modPendaftaran->no_pendaftaran . '-' . $modPasien->nama_pasien . '-' . $ruangan->ruangan_nama;
                        $params['create_ruangan'] = Params::RUANGAN_ID_GIZI;
                        $params['judulnotifikasi'] = 'Rujukan Rawat Jalan';                        
                        $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                        
                    } else {
                        $this->statusSavePermintaanPenunjang = true;
                    }
                    $this->savePasienPenunjang($modPendaftaran,$modKirimKeUnitLain,$_POST['RekeningakuntansiV']);
                    if($this->statusSaveKirimkeUnitLain && $this->statusSavePermintaanPenunjang && $this->successSave && $this->successSaveTindakan){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                        $this->refresh();
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data tidak valid ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
                }
                
            }
		
            $modRiwayatKirimKeUnitLain = RJPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                        'instalasi_id'=>Params::INSTALASI_ID_GIZI));
            
            $this->render($this->pathView.'index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modKirimKeUnitLain'=>$modKirimKeUnitLain,
                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
                                        'modRekenings'=>$modRekenings));
	}

        protected function savePasienKirimKeUnitLain($modPendaftaran)
        {
            $modKirimKeUnitLain = new RJPasienKirimKeUnitLainT;
            $modKirimKeUnitLain->attributes = $_POST['RJPasienKirimKeUnitLainT'];
            $modKirimKeUnitLain->pasien_id = $modPendaftaran->pasien_id;
            $modKirimKeUnitLain->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modKirimKeUnitLain->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
            $modKirimKeUnitLain->instalasi_id = Params::INSTALASI_ID_GIZI;
            $modKirimKeUnitLain->nourut = Generator::noUrutPasienKirimKeUnitLain($modKirimKeUnitLain->ruangan_id);
            if($modKirimKeUnitLain->validate()){
                $modKirimKeUnitLain->save();
                $this->statusSaveKirimkeUnitLain = true;
                $updateStatusPeriksa=PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id,array('statusperiksa'=>Params::statusPeriksa(2)));
            }
            
            return $modKirimKeUnitLain;
        }
        
        protected function savePermintaanPenunjang($permintaan,$modKirimKeUnitLain)
        {
            foreach ($permintaan as $i=>$item) {
                if($item['cbTindakan'] == 1){
                    $modPermintaan = new RJPermintaanPenunjangT;
                    $modPermintaan->daftartindakan_id = $item['idDaftarTindakan'];     //$permintaan['idDaftarTindakan'][$i];
                    $modPermintaan->pemeriksaanlab_id = '';
                    $modPermintaan->pemeriksaanrad_id = '';
                    $modPermintaan->pasienkirimkeunitlain_id = $modKirimKeUnitLain->pasienkirimkeunitlain_id;
                    $modPermintaan->noperminatanpenujang = Generator::noPermintaanPenunjang('PG');
                    $modPermintaan->qtypermintaan = 1;
                    $modPermintaan->tglpermintaankepenunjang = $modKirimKeUnitLain->tgl_kirimpasien; //date('Y-m-d H:i:s');
                    if($modPermintaan->validate()){
                        $modPermintaan->save();
                        $this->statusSavePermintaanPenunjang = true;
                    }
                }
            }
        }
        
         protected function savePasienPenunjang($modPendaftaran,$modKirimKeUnitLain,$postRekenings = array()){
            
            $modPasienPenunjang = new RJPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $modPendaftaran->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $modPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_GIZI;   //$modPendaftaran->ruangan_id;
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang('RJ');
            $modPasienPenunjang->tglmasukpenunjang = date('Y-m-d H:i:s');    //$modPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $modPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $modPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $modPendaftaran->ruangan_id;
            $modPasienPenunjang->pasienkirimkeunitlain_id = $modKirimKeUnitLain->pasienkirimkeunitlain_id;
            $modPasienPenunjang->pasienadmisi_id = $modPendaftaran->pasienadmisi_id;
            if(empty($modPendaftaran->pasienadmisi_id)){
                $modPasienPenunjang->pasienadmisi_id = null;
            }else{
                $modPasienPenunjang->pasienadmisi_id = $modPendaftaran->pasienadmisi_id;
            }
            
            
//                        echo "<pre>";
//                        echo print_r($modPasienPenunjang->getAttributes());
//                        echo "</pre>";
                        
            if ($modPasienPenunjang->validate()){
//                echo "<pre>";
//                print_r($modPasienPenunjang->getAttributes());
//                exit;
                $modPasienPenunjang->Save();
                $this->successSave = true;
                $this->saveTindakanPelayanan($modPendaftaran, $modPasienPenunjang,$postRekenings);
                $this->updatePasienKirimKeUnitLain($modPasienPenunjang);
            } else {
                $this->successSave = false;
            }
            
            return $modPasienPenunjang;
        }
        
        protected function saveTindakanPelayanan($modPendaftaran,$modPasienPenunjang,$postRekenings = array())
        {
            $valid=true;
            foreach($_POST['permintaanPenunjang'] as $i=>$item)
            {
                if(!empty($item)){
                    if($item['cbTindakan'] == 1){
                        $modTindakans[$i] = new TindakanpelayananT;
                        $modTindakans[$i]->attributes=$item;
                        $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                        $modTindakans[$i]->pasien_id = $modPendaftaran->pasien_id;
                        $modTindakans[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                        $modTindakans[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                        $modTindakans[$i]->instalasi_id = Params::INSTALASI_ID_GIZI; //$modPendaftaran->instalasi_id;
                        $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                        $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                        $modTindakans[$i]->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
                        $modTindakans[$i]->daftartindakan_id = $item['idDaftarTindakan'];
                        $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                        $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                        $modTindakans[$i]->tgl_tindakan = date('Y-m-d H:i:s');
                        $modTindakans[$i]->tarif_satuan = $item['tarifPelayanan'];
                        $modTindakans[$i]->tarif_tindakan = $item['tarifPelayanan'] * 1;
                        $modTindakans[$i]->satuantindakan = "HARI";
                        $modTindakans[$i]->qty_tindakan = 1;
                        $modTindakans[$i]->cyto_tindakan = $item['cyto'];
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['cyto']) ? (($item['cyto'] / 100) * $modTindakans[$i]->tarif_tindakan) : 0;
                        $modTindakans[$i]->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
                        $modTindakans[$i]->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;

                        $modTindakans[$i]->discount_tindakan = 0;
                        $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                        $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                        $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                        $modTindakans[$i]->iurbiaya_tindakan = 0;
//                        $modTindakans[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $modTindakans[$i]->ruangan_id = PARAMS::RUANGAN_ID_GIZI;
                        $valid = $modTindakans[$i]->validate() && $valid;
                        
                        $idPemeriksaanRad[$i] = $item['inputpemeriksaanrad'];
                    }
                }
            }
            
            if($valid){
                foreach($modTindakans as $i=>$tindakan){
                    $tindakan->save();
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening($tindakan->ruangan_id);
                        //update jurnalrekening_id
                        $tindakan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $tindakan->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $tindakan, 'tm');
                          
                    }
                    $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
                }
                if($statusSaveKomponen) {
                    $this->successSaveTindakan = true;
                } else {
                    $this->successSaveTindakan = false;
                }
            } else {
                $this->successSaveTindakan = false;
            }
            
            return $modTindakans;
        }
        
        
        protected function saveTindakanKomponen($tindakan)
        {   
            $valid = true;
            $criteria = new CDbCriteria();
//            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->compare('daftartindakan_id', $tindakan->daftartindakan_id);
            $criteria->compare('kelaspelayanan_id', $tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = new TindakankomponenT;
                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen->validate() && $valid;
                if($valid)
                    $modTindakanKomponen->save();
            }
            
            return $valid;
        }
        
        protected function updatePasienKirimKeUnitLain($modPasienPenunjang) {
            RJPasienKirimKeUnitLainT::model()->updateByPk($modPasienPenunjang->pasienkirimkeunitlain_id, 
                    array('pasienmasukpenunjang_id'=>$modPasienPenunjang->pasienmasukpenunjang_id));
        }
        
        public function actionAjaxBatalKirim()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idPasienKirimKeUnitLain = $_POST['idPasienKirimKeUnitLain'];
            $idPendaftaran = $_POST['idPendaftaran'];
            
            $pasienpenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));

            $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienpenunjang->pasienmasukpenunjang_id));
            
            PermintaankepenunjangT::model()->deleteAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            PasienmasukpenunjangT::model()->deleteAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            // PasienkirimkeunitlainT::model()->deleteByPk($idPasienKirimKeUnitLain);
           
            TindakanpelayananT::model()->deleteAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienpenunjang->pasienmasukpenunjang_id));
            
            if(count($modTindakanPelayanan) > 0){
                foreach($modTindakanPelayanan as $i=>$items){
                    TindakankomponenT::model()->deleteAllByAttributes(array('tindakanpelayanan_id'=>$items['tindakanpelayanan_id']));
                }
            }
//            $modRiwayatKirimKeUnitLain = RJPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
//                                                                                                      'instalasi_id'=>Params::INSTALASI_ID_GIZI),
//                                                                                                'pasienmasukpenunjang_id IS NULL');
            $modRiwayatKirimKeUnitLain = RJPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                      'instalasi_id'=>Params::INSTALASI_ID_GIZI));
            
            $data['result'] = $this->renderPartial($this->pathView.'_listKirimKeUnitLain', array('modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain), true);

            echo json_encode($data);
             Yii::app()->end();
            }
        }
        
        public function actionPrint()
        {
             $idPendaftaran = $_GET['id'];
             $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
             $modKirimKeUnitLain = RJPasienKirimKeUnitLainT::model()->findAll('pendaftaran_id='.$idPendaftaran);
             $modRiwayatKirimKeUnitLain = RJPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                      'instalasi_id'=>Params::INSTALASI_ID_GIZI),
                                                                                                'pasienmasukpenunjang_id IS NULL');
            
            $judulLaporan='Konsultasi Gizi';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('modKirimKeUnitLain'=> $modKirimKeUnitLain,'modPendaftaran'=>$modPendaftaran,'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('modKirimKeUnitLain'=> $modKirimKeUnitLain,'modPendaftaran'=>$modPendaftaran,'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('modKirimKeUnitLain'=> $modKirimKeUnitLain,'modPendaftaran'=>$modPendaftaran,'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }

        public function actionAjaxDeleteTindakanPelayanan()
        {
            if(Yii::app()->request->isAjaxRequest) {
               
                $idPasienMasukPenunjang = $_POST['idPasienMasukPenunjang'];
                $sukses_pembalik = true;


                $transaction = Yii::app()->db->beginTransaction();

                try {
                     
                    $modTin = TindakanpelayananT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
                    $modKirKeULain = RJPasienKirimKeUnitLainT::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));

                    PasienmasukpenunjangT::model()->updateByPk($idPasienMasukPenunjang,
                            array('pasienkirimkeunitlain_id' => NULL)
                        );
                    RJPasienKirimKeUnitLainT::model()->updateByPk($modKirKeULain->pasienkirimkeunitlain_id,
                            array('pasienmasukpenunjang_id' => NULL)
                        );
                    foreach($modTin as $i=>$items){
                        TindakanpelayananT::model()->updateByPk($items['tindakanpelayanan_id'],
                            array('pasienmasukpenunjang_id' => NULL)
                        );
                    }

                    $deletePasienMasukPenunjang=PasienmasukpenunjangT::model()->deleteByPk($idPasienMasukPenunjang);
                    $deletePermintaanKePenunjang=PermintaankepenunjangT::model()->deleteAllByAttributes(array('pasienkirimkeunitlain_id'=>$modKirKeULain->pasienkirimkeunitlain_id));
                    $deleteKirimKeunitLain=RJPasienKirimKeUnitLainT::model()->deleteByPk($modKirKeULain->pasienkirimkeunitlain_id);

                    if($deletePasienMasukPenunjang&&$deletePermintaanKePenunjang&&$deleteKirimKeunitLain){
                        foreach($modTin as $i=>$items){
                            $sukses_pembalik =  TindakanController::jurnalPembalikTindakan($items['tindakanpelayanan_id']);
                            $deleteKomponen=TindakankomponenT::model()->deleteAllByAttributes(array('tindakanpelayanan_id'=>$items['tindakanpelayanan_id']));
                            $TindakankomponenT = RJTindakanPelayananT::model()->deleteByPk($items['tindakanpelayanan_id']);
                            if ($TindakankomponenT && $deleteKomponen && $sukses_pembalik){
                                $data['success'] = true;
                            }else{
                                $data['success'] = false;
                            }
                        }
                    }else{      
                          $data['success'] = false;
                    }

                    if($data['success']){
                        $transaction->commit();
                    }else{
                        $transaction->rollback();
                    }

                } catch (Exception $exc) {
                    $transaction->rollback();
                    echo MyExceptionMessage::getMessage($exc,true);
                    $data['success'] = false;
                }

                echo json_encode($data);
                 Yii::app()->end();
            }
        }

      

}