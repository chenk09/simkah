<?php
//untuk actionDetailHasilLab
Yii::import('application.modules.laboratorium.models.LKPasienMasukPenunjangV');
//untuk actionDetailHasilRab
Yii::import('application.modules.radiologi.models.ROPasienMasukPenunjangV');
class DaftarPasienController extends SBaseController
{
        public $defaultAction = 'index';
        public $pathView = "rawatJalan.views.";
	public function actionIndex()
	{
            $model = new RJInfokunjunganrjV('searchDaftarPasien');
            //$model = new RJPendaftaranT('searchDaftarPasien');
            $model->unsetAttributes();
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('d M Y H:i:s');
            $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
            if(isset($_GET['RJInfokunjunganrjV'])){
                $model->attributes = $_GET['RJInfokunjunganrjV'];
                $format = new CustomFormat();
                $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAkhir']);
                $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
            }
            
            $this->render('index',array('model'=>$model));
	}
        /**
        * actionDetailHasilLab = mnampilkan hasil lab sesuai dengan yang dilab
        * @param type $idPendaftaran
        * @param type $idPasien
        * @param type $idPasienMasukPenunjang
        */
       public function actionDetailHasilLab($idPendaftaran, $idPasien, $idPasienMasukPenunjang)
       {
           $this->layout = '//layouts/frameDialog';

           $cek_penunjang = LKPasienMasukPenunjangV::model()->findAllByAttributes(
               array('pendaftaran_id'=>$idPendaftaran)
           );

           $data_rad = array();
           if(count($cek_penunjang) > 1)
           {
               $masukpenunjangRad = LKPasienMasukPenunjangV::model()->findByAttributes(
                   array(
                       'pendaftaran_id'=>$idPendaftaran,                        
                       'ruangan_id'=>Params::RUANGAN_ID_RAD
                   )
               );

               $modHasilPeriksaRad = HasilpemeriksaanradV::model()->findAllByAttributes(
                   array(
                       'pasienmasukpenunjang_id'=>$masukpenunjangRad->pasienmasukpenunjang_id
                   ),
                   array(
                       'order'=>'pemeriksaanrad_urutan'
                   )
               );

               foreach($modHasilPeriksaRad as $i=>$val)
               {
                   $data_rad[] = array(
                       'pemeriksaan'=>$val['pemeriksaanrad_nama'],
   //                        'hasil'=>'Hasil Pemeriksaan ' . $val['pemeriksaanrad_nama'] . ' terlampir',
                       'hasil'=>'Hasil terlampir'
                   );
               }

           }

           $masukpenunjang = LKPasienMasukPenunjangV::model()->findByAttributes(
               array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang)
           );

           $pemeriksa = PegawaiM::model()->findByPk($masukpenunjang->pegawai_id);

           $modHasilPeriksa = HasilpemeriksaanlabV::model()->findByAttributes(
               array(
                   'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang
               )
           );
           $query = "
               SELECT * FROM detailhasilpemeriksaanlab_t 
               JOIN pemeriksaanlab_m ON detailhasilpemeriksaanlab_t.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id 
               JOIN pemeriksaanlabdet_m ON detailhasilpemeriksaanlab_t.pemeriksaanlabdet_id = pemeriksaanlabdet_m.pemeriksaanlabdet_id 
               JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
               JOIN nilairujukan_m ON nilairujukan_m.nilairujukan_id = pemeriksaanlabdet_m.nilairujukan_id
               WHERE detailhasilpemeriksaanlab_t.hasilpemeriksaanlab_id = '". $modHasilPeriksa->hasilpemeriksaanlab_id ."'
               ORDER BY jenispemeriksaanlab_m.jenispemeriksaanlab_urutan, pemeriksaanlab_urutan, pemeriksaanlabdet_nourut
           ";
           $detailHasil = Yii::app()->db->createCommand($query)->queryAll();

           $data = array();
           $kelompokDet = null;
           $idx = 0;
           $temp = '';

           foreach ($detailHasil as $i => $detail)
           {
               $id_jenisPeriksa = $detail['jenispemeriksaanlab_id'];
               $jenisPeriksa = $detail['jenispemeriksaanlab_nama'];
               $kelompokDet = $detail['kelompokdet'];
               if($id_jenisPeriksa == '72')
               {
                   $query = "
                       SELECT jenispemeriksaanlab_m.* FROM pemeriksaanlabdet_m
                       JOIN pemeriksaanlab_m ON pemeriksaanlabdet_m.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id
                       JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
                       WHERE nilairujukan_id = ". $detail['nilairujukan_id'] ." AND pemeriksaanlab_m.jenispemeriksaanlab_id <> ". $id_jenisPeriksa ."
                   ";
                   $rec = Yii::app()->db->createCommand($query)->queryRow();
                   $id_jenisPeriksa = $rec['jenispemeriksaanlab_id'];
                   $jenisPeriksa = $rec['jenispemeriksaanlab_nama'];
               }

               if($temp != $kelompokDet)
               {
                   $idx = 0;
               }

               $data[$id_jenisPeriksa]['tittle'] = $jenisPeriksa;
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['id'] = $id_jenisPeriksa;
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['nama'] = $jenisPeriksa;
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['kelompok'] = $kelompokDet;                
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['kelompok'] = $kelompokDet;
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan_det'] = $detail['pemeriksaanlab_nama'];
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan'] = $detail['namapemeriksaandet'];
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['id_pemeriksaan'] = $detail['nilairujukan_id'];
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['normal'] = $detail['nilairujukan_nama'];
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['metode'] = $detail['nilairujukan_metode'];
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['hasil'] = $detail['hasilpemeriksaan'];
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['nilairujukan'] = $detail['nilairujukan'];
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['satuan'] = $detail['hasilpemeriksaan_satuan'];
               $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['keterangan'] = $detail['nilairujukan_keterangan'];
               $temp = $kelompokDet;
               $idx++;
           }

           $this->render('rawatInap.views.riwayatPasien.detailHasilLab',
               array(
                  'modHasilPeriksa'=>$modHasilPeriksa,
                  'masukpenunjang'=>$masukpenunjang,
                  'pemeriksa'=>$pemeriksa,
                  'data'=>$data,
                  'data_rad'=>$data_rad
               )
           );
       }

       /**
        * actionDetailHasilRad = menampilkan hasil radiologi sesuai dengan rad
        * @param type $idPendaftaran
        * @param type $idPasien
        * @param type $idPasienMasukPenunjang
        * @param type $caraPrint
        */
       public function actionDetailHasilRad($idPendaftaran,$idPasien,$idPasienMasukPenunjang,$caraPrint='')
       {   
           $this->layout = '//layouts/frameDialog';
           $modPasienMasukPenunjang = ROPasienMasukPenunjangV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
           $pemeriksa = PegawaiM::model()->findByAttributes(array('pegawai_id'=>$modPasienMasukPenunjang->pegawai_id));
           $detailHasil = HasilpemeriksaanradT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));

           $this->render('rawatInap.views.riwayatPasien.detailHasilRad',array('detailHasil'=>$detailHasil,
                                              'masukpenunjang'=>$modPasienMasukPenunjang,
                                              'pemeriksa'=>$pemeriksa,
                                              'caraPrint'=>$caraPrint,
                                               ));
       }
        
        public function actionDetailTindakan($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = RJPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);

            // var_dump($id);exit();

            $modTindakan = RJTindakanPelayananT::model()->with('daftartindakan')->findAllByAttributes(array('pendaftaran_id'=>$id));
            $format = new CustomFormat;
            $modTindakanSearch = new RJTindakanPelayananT('search');
            $modPasien = RJPasienM::model()->findByPK($modPendaftaran->pasien_id);
            $this->render($this->pathView.'/_periksaDataPasien/_tindakan', 
                    array('modPendaftaran'=>$modPendaftaran, 
                        'modTindakan'=>$modTindakan,
                        'modTindakanSearch'=>$modTindakanSearch,
                        'modPasien'=>$modPasien));
        }
        
        public function actionDetailTerapi($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = RJPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
            $modTerapi = RJPenjualanresepT::model()->with('reseptur')->findAllByAttributes(array('pendaftaran_id'=>$id));
            $format = new CustomFormat;
            $modDetailTerapi = new RJPenjualanresepT();
            $modPasien = RJPasienM::model()->findByPK($modPendaftaran->pasien_id);
            $this->render($this->pathView.'/_periksaDataPasien/_terapi', 
                    array('modPendaftaran'=>$modPendaftaran, 
                        'modTerapi'=>$modTerapi,
                        'modDetailTerapi'=>$modDetailTerapi,
                        'modPasien'=>$modPasien));
        }
        
        public function actionDetailPemakaianBahan($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = RJPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
            $modBahan = RJObatalkesPasienT::model()->with('obatalkes')->findAllByAttributes(array('pendaftaran_id'=>$id));
            $format = new CustomFormat;
            $modPemakaianBahan = new RJObatalkesPasienT;
            $modPasien = RJPasienM::model()->findByPK($modPendaftaran->pasien_id);
            $this->render($this->pathView.'/_periksaDataPasien/_pemakaianBahan', 
                    array('modPendaftaran'=>$modPendaftaran, 
                        'modBahan'=>$modBahan,
                        'modPemakaianBahan'=>$modPemakaianBahan,
                        'modPasien'=>$modPasien));
        }
        
        public function actionGetRiwayatPasien($id){
            $this->layout='//layouts/frameDialog';
            $criteria = new CDbCriteria;
            $criteria->addCondition("t.pasien_id = ".$id);
            $criteria->order = 'tgl_pendaftaran DESC';
            // $criteria = new CDbCriteria(array(
            //         'condition' => 't.pasien_id = '.$id,
            //     //.'
            //       //      and t.ruangan_id ='.Yii::app()->user->getState('ruangan_id'),
            //         'order'=>'tgl_pendaftaran DESC',
            //     ));

            $pages = new CPagination(RJPendaftaranT::model()->count($criteria));
            $pages->pageSize = Params::JUMLAH_PERHALAMAN; //Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);
            
            // Banyak data yang tidak muncul->$modKunjungan = RJPendaftaranT::model()->with('hasilpemeriksaanlab','anamnesa','pemeriksaanfisik','pasienmasukpenunjang','diagnosa')->
            //         findAll($criteria);
            $modKunjungan = RJPendaftaranT::model()->findAll($criteria);
            // foreach ($modKunjungan as $Kunjungan) {
            //   echo "<pre>";
            //   print_r($Kunjungan->attributes);

            // }
            // exit();
           
            $this->render($this->pathView.'/_periksaDataPasien/_riwayatPasien', array(
                    'pages'=>$pages,
                    'modKunjungan'=>$modKunjungan,
            ));
        }
        
        public function actionPrint($id = null)
         {
            //$this->layout='//layouts/frameDialog';
             //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                                 
            $modPendaftaran = RJPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
            $modHasilLab = RJHasilpemeriksaanlabT::model()->findByAttributes(array('pendaftaran_id'=>$id));
            $modDetailHasilLab = RJDetailhasilpemeriksaanlabT::model()->with('pemeriksaanlab')->findAllByAttributes(array('hasilpemeriksaanlab_id'=>$modHasilLab->hasilpemeriksaanlab_id));
            $modDetailHasil = new RJDetailhasilpemeriksaanlabT();
            $format = new CustomFormat;
            $modHasilLab->tglhasilpemeriksaanlab = $format->formatDateINAtime($modHasilLab->tglhasilpemeriksaanlab);
           
            $modPasien = RJPasienM::model()->findByPK($modPendaftaran->pasien_id);
      
             $judulLaporan='Data Hasil Pemeriksaan Lab';
             $caraPrint=$_REQUEST['caraPrint'];
             
            if($caraPrint=='PRINT')
                {
                    $this->layout='//layouts/printWindows';
                    $this->render('/_periksaDataPasien/detailHasilLab', array('modPendaftaran'=>$modPendaftaran, 
                        'modHasilLab'=>$modHasilLab, 
                        'modDetailHasilLab'=>$modDetailHasilLab,
                        'modDetailHasil'=>$modDetailHasil,
                        'modPasien'=>$modPasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
            else if($caraPrint=='EXCEL')    
                {
                    $this->layout='//layouts/printExcel';
                    $this->render('/_periksaDataPasien/detailHasilLab', array('modPendaftaran'=>$modPendaftaran, 
                        'modHasilLab'=>$modHasilLab, 
                        'modDetailHasilLab'=>$modDetailHasilLab,
                        'modDetailHasil'=>$modDetailHasil,
                        'modPasien'=>$modPasien,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
            else if($_REQUEST['caraPrint']=='PDF')
                {
                   
                    $ukuranKertasPDF=Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                    $posisi=Yii::app()->user->getState('posisi_kertas');                            //Posisi L->Landscape,P->Portait
                    $mpdf=new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);  
                    $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                    $mpdf->WriteHTML($this->renderPartial('/_periksaDataPasien/detailHasilLab', array('modPendaftaran'=>$modPendaftaran, 
                        'modHasilLab'=>$modHasilLab, 
                        'modDetailHasilLab'=>$modDetailHasilLab,
                        'modDetailHasil'=>$modDetailHasil,
                        'modPasien'=>$modPasien,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                    $mpdf->Output();
                }                       
         }
         
         public function actionGetRiwayat(){
            if (Yii::app()->request->isAjaxRequest)
            {
                $idPasien = $_GET['idPasien'];
                $page = $_GET['page'];
                if (empty($page)){
                    $page = 1;
                }
                //$modPendaftaran=RJPendaftaranT::model()->findByPk($idPendaftaran);
            
                $modPasien = RJPasienM::model()->findByPk($idPasien);
                echo CJSON::encode(array(
                    'status'=>'create_form', 
                    'div'=>$this->renderPartial($this->pathView.'/_periksaDataPasien/_riwayatPasien', array('modPasien' => $modPasien, 'page'=>$page), true)));
                exit;               
            }
         }
         
         public function actionBatalRawatInap()
        {
             if(Yii::app()->request->isAjaxRequest) {
                $idOtoritas = $_POST['idOtoritas'];
                $namaOtoritas = $_POST['namaOtoritas'];
                $idPasienPulang=$_POST['idPasienPulang'];
                $alasanPembatalan=$_POST['Alasan'];
                $idPendaftaran = $_POST['idPendaftaran'];
                
                
                $modPasienBatalPulang = new PasienbatalpulangT;    
                $modPasienBatalPulang->namauser_otorisasi=$namaOtoritas;
                $modPasienBatalPulang->iduser_otorisasi=$idOtoritas;
                $modPasienBatalPulang->pasienpulang_id=$idPasienPulang;
                $modPasienBatalPulang->tglpembatalan=date('Y-m-d H:i:s');
                $modPasienBatalPulang->alasanpembatalan=$alasanPembatalan;
                 $transaction = Yii::app()->db->beginTransaction();
                 try{
                    if($modPasienBatalPulang->save()){
                        $pulang =  PasienpulangT::model()->updateByPk($idPasienPulang,array('pasienbatalpulang_id'=>$modPasienBatalPulang->pasienbatalpulang_id));
                        $pendaftaran =  PendaftaranT::model()->updateByPk($idPendaftaran,array('pasienpulang_id'=>null,'pasienadmisi_id'=>null,'statusperiksa'=>'SEDANG PERIKSA'));   
                        if ($pendaftaran){
                            $data['status'] = 'success';
                            $transaction->commit();
                        }
                        else{
                            $data['status'] = 'Gagal';
                            throw new Exception("Update Data Gagal");
                        }
                    }
                    else{
                        $data['status'] = 'Gagal';
                        Throw new Exception("Pasien Batal Rawat Inap Gagal Disimpan");
                    }
                 }catch(Exception $ex){
                     $transaction->rollback();
                     $data['status'] = $ex;
                 }

                echo json_encode($data);
                Yii::app()->end();
                }
        }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
         public function actionUbahStatusPeriksaRJ($id){
            $this->layout='//layouts/frameDialog';
            
            $format = new CustomFormat();
            $model = PendaftaranT::model()->findByPk($id);
            $model->tglselesaiperiksa = date('Y-m-d H:i:s');            
            if(isset($_POST['PendaftaranT'])){
                $update = PendaftaranT::model()->updateByPk($id,array('statusperiksa'=>$_POST['PendaftaranT']['statusperiksa'],'tglselesaiperiksa'=>$format->formatDateTimeMediumForDB($_POST['PendaftaranT']['tglselesaiperiksa'])));
                    if($update){
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->refresh();
//                        echo CHtml::script("window.parent.$('#dialogUbahStatus').dialog('close');window.parent.$('#dialogUbahStatus').attr('src',,'');window.parent.$.fn.yiiGridView.update('{$_GET['id']}');");
//                        $this->redirect(array('index',array('id'=>$id)));
                    }else{
                        Yii::app()->user->setFlash('error',"Data Gagal Disimpan");
                    }
            }
            $this->render('_ubahStatusPeriksa', array(
                    'model'=>$model,
            ));
        }
        
        // -- Detail Hasil Diagnosa -- //
        
            public function actionDetailHasilDiagnosa($id){
             
            $this->layout='//layouts/frameDialog';
            
            $modPasienMasukPenunjang = RJPasienMasukPenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
            $modPendaftaran = PendaftaranT::model()->findByPk($id);
            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
            $detailHasil = PasienmorbiditasT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
            
            $this->render($this->pathView.'/_periksaDataPasien/detailHasilDiagnosa',array('detailHasil'=>$detailHasil,
                                               'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                                               'modPendaftaran'=>$modPendaftaran,
                                               'modPasien'=>$modPasien,
                                                ));
        }
        // -- End Detail Hasil Diagnosa -- //
        
        // -- Detail Hasil Anamnesa -- //
        
            public function actionDetailHasilAnamnesa($id){
             
            $this->layout='//layouts/frameDialog';
            
            $modPasienMasukPenunjang = RJPasienMasukPenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
            $modPendaftaran = PendaftaranT::model()->findByPk($id);
            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
            $detailHasil = AnamnesaT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
            
            $this->render($this->pathView.'/_periksaDataPasien/detailHasilAnamnesa',array('detailHasil'=>$detailHasil,
                                               'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                                               'modPendaftaran'=>$modPendaftaran,
                                               'modPasien'=>$modPasien,
                                                ));
        }
        // -- End Detail Hasil Anamnesas -- //
        
        /**
        * Menampilkan riwayat hasil operasi
        * @param type $id
        */
        public function actionDetailHasilOperasi($id){
            $this->layout='//layouts/frameDialog';
            
            $modPasienMasukPenunjang = RJPasienMasukPenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
            $modPendaftaran = PendaftaranT::model()->findByPk($id);
            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
            $detailHasil = new RJTindakanPelayananT('searchDetailTindakan');
            $detailHasil->unsetAttributes();
            $detailHasil->pendaftaran_id = $id;
            $detailHasil->ruangan_id = Params::RUANGAN_ID_IBS;
            
            $this->render($this->pathView.'_periksaDataPasien.detailHasilOperasi',array('detailHasil'=>$detailHasil,
                                               'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                                               'modPendaftaran'=>$modPendaftaran,
                                               'modPasien'=>$modPasien,
                                                ));
        }
        // -- End Detail Hasil Operasi -- //
        
        public function actionRencanaKontrolPasienRJ($idPendaftaran)
	{
             $this->layout='//layouts/frameDialog';
             $insert_notifikasi = new MyFunction();
             $format = new CustomFormat;
             $model = new PendaftaranT;
             $model->tglrenkontrol = date('Y-m-d H:i:s');
             $tersimpan = 'Tidak';
             
             $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
             $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
             
             if(isset($_POST['PendaftaranT'])){
                    $renKontrol = $format->formatDateTimeMediumForDB($_POST['PendaftaranT']['tglrenkontrol']);
                    $idPasien = $_POST['PendaftaranT']['pendaftaran_id'];
                    $transaction = Yii::app()->db->beginTransaction();
                  try {
                        $update = PendaftaranT::model()->updateByPk($idPasien,array('tglrenkontrol'=>$renKontrol));

                        if($update){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                            $tersimpan='Ya';   
                        }else{
                           $transaction->rollback();
                           Yii::app()->user->setFlash('error',"Data gagal disimpan");   
                        }
                        
                        $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                        $params['create_time'] = date( 'Y-m-d H:i:s');
                        $params['create_loginpemakai_id'] = Yii::app()->user->id;
                        $params['instalasi_id'] = Yii::app()->user->getState('instalasi_id');
                        $params['modul_id'] = Yii::app()->id->module_id;
                        $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $modPendaftaran->no_pendaftaran . '-' . $modPasien->nama_pasien;
                        $params['create_ruangan'] = $modPendaftaran->ruangan_id;
                        $params['judulnotifikasi'] = ($modPendaftaran->tglrenkontrol != null ? 'Rencana Kontrol Pasien' : 'Rencana Kontrol Pasien' );
                        $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                    
                   } catch (Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan", MyExceptionMessage::getMessage($exc,false));
                   }
             }
             
             $model->tglrenkontrol = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($model->tglrenkontrol, 'yyyy-MM-dd hh:mm:ss'));
             
             $this->render('formRencanaKontrol',array(
                                'modPasien'=>$modPasien,
                                'modPendaftaran'=>$modPendaftaran,
                                'model'=>$model,
                                'tersimpan'=>$tersimpan,
                          ));
	}
        /**
         * menampilkan riwayat konsul gizi
         * disesuaikan dengan gizi/controllers/DaftarPasienController
         * @param type $id
         */
        public function actionDetailKonsulGizi($id){
            Yii::import('gizi.models.GZPendaftaranT');
            Yii::import('gizi.models.GZTindakanPelayananT');
            Yii::import('gizi.models.GZTindakanPelayananT');
            Yii::import('gizi.models.GZPasienM');
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = GZPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
            $modTindakan = GZTindakanPelayananT::model()->with('daftartindakan')->findAllByAttributes(array('pendaftaran_id'=>$id));
            $format = new CustomFormat;
            $modTindakanSearch = new GZTindakanPelayananT('search');
            $modPasien = GZPasienM::model()->findByPK($modPendaftaran->pasien_id);
            $this->render('gizi.views/_periksaDataPasien/_konsultasiGizi', 
                    array('modPendaftaran'=>$modPendaftaran, 
                        'modTindakan'=>$modTindakan,
                        'modTindakanSearch'=>$modTindakanSearch,
                        'modPasien'=>$modPasien));
        }

        public function actionPasienPulang($idPendaftaran = null, $dialog = false)
        {
            if($dialog) $this->layout='//layouts/frameDialog';
			
			$kirimunit = PasienkirimkeunitlainT::model()->findAllByAttributes(array(
				'pendaftaran_id'=>$idPendaftaran, 'pasienmasukpenunjang_id'=>null
			));
			$ruangan = array();
            if(count($kirimunit)>0){
				foreach($kirimunit as $key=>$kirimPasien){
					$ruangan[] = "+ Ruangan : ". $kirimPasien->ruangan->ruangan_nama . " Tgl Rujuk Pasien : ".$kirimPasien->tgl_kirimpasien;
				}
				Yii::app()->user->setFlash('error',"Pasien Belum Ditindak Lanjut Di : <br> " . implode("<br>", $ruangan));
            }
			
			$criteria = new CDbCriteria;
			$criteria->compare("pendaftaran_id", $idPendaftaran);
			$pendaftaranT = PendaftaranT::model()->find($criteria);
			$pasienM = PasienM::model()->findByPk($pendaftaranT->pasien_id);

			if(is_null($pendaftaranT->pasienpulang_id))
			{
				$modelPulang = new PasienpulangT;
				$modelPulang->pendaftaran_id = $pendaftaranT->pendaftaran_id;
				$modelPulang->pasien_id = $pendaftaranT->pasien_id;
				$modelPulang->carakeluar = "DIPULANGKAN";
				$modelPulang->kondisipulang = "SEMBUH";
				$modelPulang->ruanganakhir_id = $pendaftaranT->ruangan_id;
			}else{
				$pasienpulangT = PasienpulangT::model()->findByPK($pendaftaranT->pasienpulang_id);
				$modelPulang = $pasienpulangT;
				Yii::app()->user->setFlash('danger', "Pasien sudah dipulangkan");
			}
			
			$format = new CustomFormat();
			$date1 = $format->formatDateTimeMediumForDB($pendaftaranT->tgl_pendaftaran);
			$date2 = date('Y-m-d H:i:s');
			$diff = abs(strtotime($date2) - strtotime($date1));
			$hours   = floor(($diff)/3600);
			$modelPulang->lamarawat = $hours;
			
			$modelPulang->satuanlamarawat = "Jam";
			$modelPulang->keterangankeluar = "SEMBUH";
			$modelPulang->tglpasienpulang = date('d M Y H:i:s');
			
			if(isset($_POST["PasienpulangT"]))
			{
				$pesan = array();
                $transaction = Yii::app()->db->beginTransaction();
                try{
					$modelPulang->attributes = $_POST["PasienpulangT"];
					if(!$modelPulang->save())
					{
						foreach($modelPulang->getErrors() as $key=>$val)
						{
							$pesan[] = $key . " : " . implode(", ", $val);
						}
					}
					
					$pendaftaran = PendaftaranT::model()->findByPK($pendaftaranT->pendaftaran_id);
					$pendaftaran->pasienpulang_id = $modelPulang->pasienpulang_id;
					$pendaftaran->statusperiksa = "SUDAH PULANG";
					if(!$pendaftaran->update())
					{
						foreach($pendaftaran->getErrors() as $key=>$val)
						{
							$pesan[] = $key . " : " . implode(", ", $val);
						}
					}
					
					if(count($pesan) > 0)
					{
						throw new Exception(implode("<br>", $pesan));
					}
					
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Proses pemulangan berhasil");
					$this->redirect(array('pasienPulang','idPendaftaran'=>$idPendaftaran, 'dialog'=>true));
					
				}catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }				
			}
			
			
			
            $this->render('formPasienPulang', array(
				"ruangan"=>$ruangan,
				"modelPulang"=>$modelPulang,
				"pasienM"=>$pasienM,
				"pendaftaranT"=>$pendaftaranT
			));
        }

		public function actionBatalPulang($pendaftaran_id = null)
		{	
			$this->layout='//layouts/frameDialog';
			if(is_null($pendaftaran_id))
			{
				echo("<h4>Alamat pemohon tidak valid</h4>");
				Yii::app()->end();
			}
			
			$pasienbatalpulangT = new PasienbatalpulangT;
			$pasienbatalpulangT->tglpembatalan = date('d M Y H:i:s');
			$pendaftaran = PendaftaranT::model()->findByPK($pendaftaran_id);
			$pasienpulangT = PasienpulangT::model()->findByPK($pendaftaran->pasienpulang_id);
			$pasienM = PasienM::model()->findByPk($pendaftaran->pasien_id);
			
			if(isset($_POST["PasienbatalpulangT"]))
			{
				$pesan = array();
                $transaction = Yii::app()->db->beginTransaction();
                try{
					$pasienbatalpulangT->attributes = $_POST["PasienbatalpulangT"];
					$loginpemakai = LoginpemakaiK::model()->findByAttributes(
						array(
							'nama_pemakai' => $pasienbatalpulangT->namauser_otorisasi,
							'loginpemakai_aktif' =>TRUE,
							'katakunci_pemakai' => LoginpemakaiK::encrypt($pasienbatalpulangT->password)
						)
					);
					
					if(!$loginpemakai)
					{
						$pesan[] = "Previliges : Otorisasi gagal";
					}
					
					$pasienbatalpulangT->iduser_otorisasi = $loginpemakai->pegawai_id;
					if(!$pasienbatalpulangT->save())
					{
						foreach($pasienbatalpulangT->getErrors() as $key=>$val)
						{
							$pesan[] = $key . " : " . implode(", ", $val);
						}
					}
					
					$pasienpulangT->pasienbatalpulang_id = $pasienbatalpulangT->pasienbatalpulang_id;
					if(!$pasienpulangT->save())
					{
						foreach($pasienpulangT->getErrors() as $key=>$val)
						{
							$pesan[] = $key . " : " . implode(", ", $val);
						}
					}
					
					$pendaftaran->pasienpulang_id = null;
					$pendaftaran->statusperiksa = "ANTRIAN";
					
					if(!$pendaftaran->update())
					{
						foreach($pendaftaran->getErrors() as $key=>$val)
						{
							$pesan[] = $key . " : " . implode(", ", $val);
						}
					}
					
					if(count($pesan) > 0)
					{
						throw new Exception(implode("<br>", $pesan));
					}					
					
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Proses pembatalan berhasil");
					$this->redirect(array('batalPulang','pendaftaran_id'=>$pendaftaran_id));
					
				}catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ". MyExceptionMessage::getWarning($exc,true));
                }
			}
			
            $this->render('formBatalPulang', array(
				"pasienM"=>$pasienM,
				"pendaftaran"=>$pendaftaran,
				"pasienbatalpulangT"=>$pasienbatalpulangT
			));
		}
		
		
}