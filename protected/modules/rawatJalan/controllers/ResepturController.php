<?php

class ResepturController extends SBaseController
{
    protected $successSave = false;
    protected $pathView = 'rawatJalan.views.reseptur.';
    
    public function actionIndex($idPendaftaran)
	{
            $insert_notifikasi = new MyFunction();
            
            $modPendaftaran=RJPendaftaranT::model()->findByPk($idPendaftaran);
            $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
            
            $modReseptur = new RJResepturT;
            $modReseptur->noresep = Generator::noResep($instalasi_id);
            $modReseptur->pegawai_id = $modPendaftaran->pegawai_id;
            $modReseptur->ruangan_id = Params::RUANGAN_ID_APOTEK_RJ;
            $modReseptur->ruanganreseptur_id = Yii::app()->user->getState('ruangan_id');
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            
            if(isset($_POST['RJResepturT'])){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $this->saveReseptur($_POST, $modPendaftaran, $konfigFarmasi);
                    
                    $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                    $params['create_time'] = date( 'Y-m-d H:i:s');
                    $params['create_loginpemakai_id'] = Yii::app()->user->id;
                    $params['instalasi_id'] = 9;
                    $params['modul_id'] = 16;
                    $ruangan = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'));
                    $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $modPendaftaran->no_pendaftaran . '-' . $modPasien->nama_pasien . '-' . $ruangan->ruangan_nama;
                    $params['create_ruangan'] = $_POST['RJResepturT']['ruangan_id'];
                    $params['judulnotifikasi'] = 'Reseptur Rawat Jalan';                        
                    $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                    
                    if($this->successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Resep berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    //echo '<pre>'.print_r($_POST,1).'</pre>';
                }
            }
		
            $this->render($this->pathView.'index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modReseptur'=>$modReseptur,
                                        'konfigFarmasi'=>$konfigFarmasi,
                                        ));
	}
        
        protected function saveReseptur($post,$modPendaftaran)
        {
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $reseptur = new RJResepturT;
            $reseptur->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $reseptur->tglreseptur = $post['RJResepturT']['tglreseptur'];
            $reseptur->noresep = $post['RJResepturT']['noresep'];
            $reseptur->pegawai_id = $post['RJResepturT']['pegawai_id'];
            $reseptur->ruangan_id = $post['RJResepturT']['ruangan_id'];
            $reseptur->ruanganreseptur_id = Yii::app()->user->getState('ruangan_id');
            $reseptur->pasien_id = $modPendaftaran->pasien_id;
            if($reseptur->validate()){
                $reseptur->save();
                $updateStatusPeriksa=PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id,array('statusperiksa'=>Params::statusPeriksa(2)));
                
                PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id,
                    array(
                        'pembayaranpelayanan_id'=>null
                    )
                );
                
                $this->saveDetailReseptur($post, $reseptur,$konfigFarmasi);
            } else {
                $this->successSave = false;
            }
        }
        
        protected function saveDetailReseptur($post,$reseptur)
        {
            $valid = true;
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            for ($i = 0; $i < count($post['obat']); $i++) {
                $detail = new RJResepturDetailT;
                $detail->reseptur_id = $reseptur->reseptur_id;
                $detail->obatalkes_id = $post['obat'][$i];
                $detail->sumberdana_id = $post['sumberdana'][$i];
                $detail->satuankecil_id = $post['satuankecil'][$i];
                $detail->racikan_id = ($post['isRacikan'][$i] == 0) ? Params::DEFAULT_NON_RACIKAN_ID : Params::DEFAULT_RACIKAN_ID;
                $detail->r = 'R/';
                $detail->rke = $post['Rke'][$i];
                $detail->qty_reseptur = $post['qty'][$i];
                $detail->signa_reseptur = $post['signa'][$i];
                $detail->etiket = $post['etiket'][$i];
                $detail->kekuatan_reseptur = $post['kekuatan'][$i];
                $detail->satuankekuatan = $post['satuankekuatan'][$i];
                
                $detail->hargasatuan_reseptur = $post['hargasatuan'][$i];
                $detail->harganetto_reseptur = $post['harganetto'][$i];
                $detail->hargajual_reseptur = $post['hargajual'][$i] * $post['qty'][$i];
                
                $detail->permintaan_reseptur = $post['jmlpermintaan'][$i];
                $detail->jmlkemasan_reseptur = $post['jmlkemasan'][$i];
                $valid = $detail->validate() && $valid;
                if($valid){
                    $detail->save();
                }
            }
            
            $this->successSave = ($valid) ? true : false;
        }
        
         public function actionPrint()
        {
             $modKonsul = new RJKonsulPoliT;
             $idPendaftaran = $_GET['id'];
             $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
             $reseptur = new RJResepturT;
             $modReseptur = RJResepturT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
             $detail = new RJResepturDetailT;
//             $karcisTindakan = DaftartindakanM::model()->findAllByAttributes(array('daftartindakan_karcis'=>true));
            
            $judulLaporan='Reseptur Pasien';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('detail'=>$detail,'modPendaftaran'=>$modPendaftaran,'reseptur'=>$reseptur,'modReseptur'=>$modReseptur,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('detail'=>$detail,'modPendaftaran'=>$modPendaftaran,'reseptur'=>$reseptur,'modReseptur'=>$modReseptur,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('detail'=>$detail,'modPendaftaran'=>$modPendaftaran,'reseptur'=>$reseptur,'modReseptur'=>$modReseptur,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }

        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}