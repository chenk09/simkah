<?php

class KasuspenyakitruanganMController extends SBaseController
{
    
                public $layout = '//layouts/column1';
                public $defaultAction = 'admin';
                
                public function filters()
                {
                    return array(
                            'accessControl',
                        );
                }
                
                public function accessRules()
                {
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
                }
                        
	public function actionIndex()
	{
		$this->render('index');
	}
        
	public function actionAdmin()
	{
                                $model = new KasuspenyakitruanganM('search');
                                $model->unsetAttributes();
                                if (isset($_GET['KasuspenyakitruanganM']))
                                    $model->attributes = $_GET['KasuspenyakitruanganM'];
		$this->render('admin',array('model'=>$model));
	}
        
                public function actionCreate()
                {
                                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE))
                                //    {throw new CHttpException(401,Yii::t('mds','You are probihited to access this page. Contact Super Administrator'));}
                                $model = new RJKasuspenyakitruanganM;
                                $ruangansession = Yii::app()->user->ruangan_id;
                                if(isset($_POST['jeniskasuspenyakit_id']))
                                {
                                    $modDetails = $this->validasiTabular($_POST['jeniskasuspenyakit_id']);
                                    $transaction = Yii::app()->db->beginTransaction();
                                    try{
                                        $jumlah = 0;
//                                        $success = true;
                                        for($i=0;$i<COUNT($_POST['jeniskasuspenyakit_id']);$i++)
                                        {
                                            $model = new RJKasuspenyakitruanganM;
                                            $model->ruangan_id = $_POST['ruanganid'];
                                            $model->jeniskasuspenyakit_id = $_POST['jeniskasuspenyakit_id'][$i];
                                            if ($model->save()){
                                                $jumlah++;
                                            }
                                        }
                                        
                                        if ($jumlah == count($_POST['jeniskasuspenyakit_id'])){
                                            $transaction->commit();
                                            Yii::app()->user->setFlash('success','<strong>Berhasil</strong>Data Berhasil disimpan');
                                            $this->redirect(array('admin'));
                                        }
                                        else{
                                            $transaction->rollback();
//                                            Yii::app()->user->setFlash('error', '<strong>Gagal</strong>Data Gagal disimpan');
                                        }
                                    }
                                    catch(Exception $ex){
                                        $transaction->rollback();
                                        Yii::app()->user->setFlash('error', '<strong>Gagal</strong>Data Gagal disimpan'.MyExceptionMessage::getMessage($ex));
                                    }
//                                        Yii::app()->user->setFlash('success','<strong>Berhasil</strong>Data Berhasil disimpan');
//                                        
                                }
                                
                                $this->render('create',array('model'=>$model, 'modDetails'=>$modDetails));
                }
                
                protected function validasiTabular($data){
                    foreach ($data as $i=>$row){
                        $modDetails[$i] = new RJKasuspenyakitruanganM();
                        $modDetails[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $modDetails[$i]->jeniskasuspenyakit_id = $row;
                        $modDetails[$i]->validate();
                    }
                    
                    return $modDetails;
                }


                public function actionUpdate()
                {
                                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE))
                                //    {throw new CHttpException(401,Yii::t('mds','You are probihited to access this page. Contact Super Administrator'));}
                                $model = new RJKasuspenyakitruanganM;
                                $ruangansession = Yii::app()->user->ruangan_id;
                                if(isset($_POST['jeniskasuspenyakit_id']))
                                {
                                    $modDetails = $this->validasiTabular($_POST['jeniskasuspenyakit_id']);
                                    $transaction = Yii::app()->db->beginTransaction();
                                    try{
                                        $jumlah = 0;
//                                        $success = true;
                                        for($i=0;$i<COUNT($_POST['jeniskasuspenyakit_id']);$i++)
                                        {
                                            $model = new RJKasuspenyakitruanganM;
                                            $model->ruangan_id = $_POST['ruanganid'];
                                            $model->jeniskasuspenyakit_id = $_POST['jeniskasuspenyakit_id'][$i];
                                            if ($model->save()){
                                                $jumlah++;
                                            }
                                        }
                                        
                                        if ($jumlah == count($_POST['jeniskasuspenyakit_id'])){
                                            $transaction->commit();
                                            Yii::app()->user->setFlash('success','<strong>Berhasil</strong>Data Berhasil disimpan');
                                            $this->redirect(array('admin'));
                                        }
                                        else{
                                            $transaction->rollback();
//                                            Yii::app()->user->setFlash('error', '<strong>Gagal</strong>Data Gagal disimpan');
                                        }
                                    }
                                    catch(Exception $ex){
                                        $transaction->rollback();
                                        Yii::app()->user->setFlash('error', '<strong>Gagal</strong>Data Gagal disimpan'.MyExceptionMessage::getMessage($ex));
                                    }
//                                        Yii::app()->user->setFlash('success','<strong>Berhasil</strong>Data Berhasil disimpan');
//                                        
                                }
                                
                                $this->render('update',array('model'=>$model, 'modDetails'=>$modDetails));
                }
                
                public function actionDelete($ruangan_id, $jeniskasuspenyakit_id)
                {
                        $this->loadModel($ruangan_id,$jeniskasuspenyakit_id)->delete();
                        if(!isset($_GET['ajax']))
                                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                }
                
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
	public function loadModel($id, $jeniskasus = null)
	{
                                if (!empty($jeniskasus)){
                                    $model=RJKasuspenyakitruanganM::model()->findByAttributes(array('ruangan_id'=>$id, 'jeniskasuspenyakit_id'=>$jeniskasus));
                                }
                                else{
                                    $model=RJKasuspenyakitruanganM::model()->findByAttributes(array('ruangan_id'=>$id));
                                }
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
                
                public function actionPrint()
                {
                    $model= new KasuspenyakitruanganM;
                    $model->attributes=$_REQUEST['KasuspenyakitruanganM'];
                    $judulLaporan='Data Kasus Penyakit Ruangan';
                    $caraPrint=$_REQUEST['caraPrint'];
                    if($caraPrint=='PRINT') {
                        $this->layout='//layouts/printWindows';
                        $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                    }
                    else if($caraPrint=='EXCEL') {
                        $this->layout='//layouts/printExcel';
                        $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                    }
                    else if($_REQUEST['caraPrint']=='PDF') {
                        $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                        $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('',$ukuranKertasPDF); 
                        $mpdf->useOddEven = 2;  
                        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet,1);  
                        $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                        $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                        $mpdf->Output();
                    }
                }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}