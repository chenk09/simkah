<?php

class PeriksaGigiController extends Controller
{
        public function actions()
        {
                return array(
                        'myOdontogram'=>array(
                            'class'=>'MyOdontogramAction',
                        ),
                );
        }
        
	public function actionIndex($id = null)
    {       

                $kunjunganPasien = new RJInfokunjunganrjV('searchKunjunganPasien');
                $kunjunganPasien->unsetAttributes();
                $kunjunganPasien->tglAwal = date('Y-m-d 00:00:00');
                $kunjunganPasien->tglAkhir = date('Y-m-d 23:59:59');
                $kunjunganPasien->ruangan_id = Yii::app()->user->getState('ruangan_id');
                if(isset($_GET['RJInfokunjunganrjV'])){
                    $kunjunganPasien->attributes = $_GET['RJInfokunjunganrjV'];
                    $format = new CustomFormat();
                     $kunjunganPasien->tgl_pendaftaran  = $format->formatDateMediumForDB($_REQUEST['RJInfokunjunganrjV']['tgl_pendaftaran']);
                    $kunjunganPasien->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAwal']);
                    $kunjunganPasien->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAkhir']);
                    //$kunjunganPasien->ruangan_id = Yii::app()->user->getState('ruangan_id');
                }
                // if(!empty($id)){
                //     $modOdontogramDetail = OdontogramdetailT::model()->findByPk($id);
                //     $modOdontogramDetail->isNewRecord = false;
                // }else{
                    $gigi = array(); //OdontogrampasienR::model()->polaGigi(2);
                    $odontogramPasien = array();
                    $modOdontogramDetail = new OdontogramdetailT;
                    $modOdontogramDetail->tglperiksa = date('d M Y H:i:s');
                    $modOdontogramDetail->ruangan_id = Yii::app()->user->getState('ruangan_id');
                    $modOdontogramDetail->pendaftaran_id = 12012012;
                    $modOdontogramDetail->pasien_id = 12012012;
                // }
                
                
                if(isset($_GET['OdontogramdetailT']))
                {
                    $modOdontogramDetail->pendaftaran_id = $_GET['OdontogramdetailT']['pendaftaran_id'];
                    $modOdontogramDetail->pasien_id = $_GET['OdontogramdetailT']['pasien_id'];
                }
                
                if(isset($_POST['codeOdon']) && isset($_POST['OdontogramdetailT'])){
                    $modOdontogramDetail = $this->setOdontogram($modOdontogramDetail, $_POST['codeOdon']);
                    $modOdontogramDetail->attributes = $_POST['OdontogramdetailT'];
                    if(!empty($modOdontogramDetail->pasien_id))
                        $odontogramPasien = OdontogrampasienR::model()->findByAttributes(array('pasien_id'=>$modOdontogramDetail->pasien_id));
                    if(!empty($odontogramPasien)){
                        $modOdontogramDetail->odontogrampasien_id = $odontogramPasien->odontogrampasien_id;
                        $odontogramPasien = $this->setOdontogram($odontogramPasien, $_POST['codeOdon']);
                        $odontogramPasien->update();
                    } else {
                        $odontogramPasien = $this->createOdontogramPasien($modOdontogramDetail->pasien_id, $_POST['codeOdon']);
                        $modOdontogramDetail->odontogrampasien_id = $odontogramPasien->odontogrampasien_id;
                    }
                    
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        if($modOdontogramDetail->validate()){
                            $modOdontogramDetail->save();
                            $transaction->commit();
                            // $modOdontogramDetail->isNewRecord = false;
                            Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                            $this->redirect(array('index','id'=>$modOdontogramDetail->odontogramdetail_id));
                            
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    } catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                    
                    $gigi = $_POST['codeOdon'];
                }
                
        $this->render('index',array('kunjunganPasien'=>$kunjunganPasien,
                                            'gigi'=>$gigi,
                                            'modOdontogramDetail'=>$modOdontogramDetail,
                                            'odontogramPasien'=>$odontogramPasien));
    }
        
        protected function setOdontogram($odontogram,$codeOdon)
        {
                // dewasa ==============================
                for($i=18;$i>10;$i--){
                    $urutan = no_.$i;
                    $odontogram->$urutan = $codeOdon[$i];
                }
                for($j=21;$j<29;$j++){
                    $urutan = no_.$j;
                    $odontogram->$urutan = $codeOdon[$j];
                }
                for($i=48;$i>40;$i--){
                    $urutan = no_.$i;
                    $odontogram->$urutan = $codeOdon[$i];
                }
                for($j=31;$j<39;$j++){
                    $urutan = no_.$j;
                    $odontogram->$urutan = $codeOdon[$j];
                }
                // =====================================
                
                
                // anak ================================
                for($i=55;$i>50;$i--){
                    $urutan = no_.$i;
                    $odontogram->$urutan = $codeOdon[$i];
                }
                for($j=61;$j<66;$j++){
                    $urutan = no_.$j;
                    $odontogram->$urutan = $codeOdon[$j];
                }
                for($i=85;$i>80;$i--){
                    $urutan = no_.$i;
                    $odontogram->$urutan = $codeOdon[$i];
                }
                for($j=71;$j<76;$j++){
                    $urutan = no_.$j;
                    $odontogram->$urutan = $codeOdon[$j];
                }
                // =====================================
                
                return $odontogram;
        }
        
        protected function createOdontogramPasien($idPasien,$codeOdon)
        {
            $odontogram = new OdontogrampasienR;
            $odontogram = $this->setOdontogram($odontogram, $codeOdon);
            $odontogram->pasien_id = $idPasien;
            if($odontogram->validate())
                $odontogram->save();
            
            return $odontogram;
        }
        
        public function actionAjaxOdontogram()
        {
            if(Yii::app()->request->isAjaxRequest) {
                $idPasien = $_POST['idPasien'];
                $gigiPasien = OdontogrampasienR::model()->findByAttributes(array('pasien_id'=>$idPasien));
                $gigi = OdontogrampasienR::model()->polaGigi($gigiPasien->odontogrampasien_id);
                
                echo json_encode($gigi);
                Yii::app()->end();
                //$this->widget('Gigi',array('gigis'=>$gigi));
            }
        }
        
        public function actionCetakOdontogram($idPasien, $idPendaftaran)
        {
            $this->layout = '//layouts/frameDialog';
            
            if(!empty($idPasien) && !empty($idPendaftaran)){
                $gigiPasien = OdontogrampasienR::model()->findByAttributes(array('pasien_id'=>$idPasien));
                $gigi = OdontogrampasienR::model()->polaGigi($gigiPasien->odontogrampasien_id);
                $pasien = PasienM::model()->findByPk($idPasien);
                $pendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            } else {
                $gigi = array();
                $pasien = new PasienM;
                $pendaftaran = new PendaftaranT;
            }
            
            $this->render('odontogram',array('gigi'=>$gigi,
                                             'pasien'=>$pasien,
                                             'pendaftaran'=>$pendaftaran));
        }

        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}