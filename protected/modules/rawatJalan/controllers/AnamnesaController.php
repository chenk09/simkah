
<?php

class AnamnesaController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';
        public $pathView = 'rawatJalan.views.anamnesa.';

	public function actionIndex($idPendaftaran)
	{
            $modPendaftaran=RJPendaftaranT::model()->findByPk($idPendaftaran);
            
            $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
            
            $dataPendaftaran = RJPendaftaranT::model()->findByAttributes(array('pasien_id'=>$modPasien->pasien_id), array('order'=>'tgl_pendaftaran DESC'));
            //print_r($lastPendaftaran);
            //echo $modPasien->pasien_id;exit();
            //EHJ-2969
//            $i = 1;
//            if (count($dataPendaftaran) > 1){
//                foreach ($dataPendaftaran as $row){
//                    if ($i == 2){
//                        $lastPendaftaran = $row->pendaftaran_id;
//                    }
//                    $i++;
//                }
//            }else{
//                $lastPendaftaran = $idPendaftaran;
//            }

            
            $cekAnamnesa=RJAnamnesaT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            $modDiagnosa = new RJDiagnosaM;
           
            if(COUNT($cekAnamnesa)>0) {  //Jika Pasien Sudah Melakukan Anamnesa Sebelumnya
                $modAnamnesa=new RJAnamnesaT;
                $detTriase = (isset($_POST['Triase']) ? $_POST['Triase'] : null);
                if(isset($detTriase)){
                    if(count($detTriase) > 0){
                        foreach($detTriase as $i=>$triase){
                            $modAnamnesa->triase_id = $triase['triase_id'];
                        }
                    }
                }
                 $modAnamnesa->pegawai_id=$modPendaftaran->pegawai_id;
                 $modAnamnesa=$cekAnamnesa;
//                $modAnamnesa->riwayatimunisasi = $modPendaftaran->statuspasien;
            } else {  
                ////Jika Pasien Belum Pernah melakukan Anamnesa
                $modAnamnesa=new RJAnamnesaT;
                $modAnamnesa->pegawai_id=$modPendaftaran->pegawai_id;
                $modAnamnesa->paramedis_nama = "Rina Trianasari, AMd. AK";
                $modAnamnesa->pendaftaran_id=$modPendaftaran->pendaftaran_id;
                $modAnamnesa->pasien_id=$modPendaftaran->pasien_id;
                $modAnamnesa->tglanamnesis=date('Y-m-d H:i:s');
                //$isPasien = RJPendaftaranT::model()->findByPk($idPendaftaran)->statuspasien;
//                $sql = "SELECT c(diagnosa_id) FROM pasienimunisasi_t WHERE pendaftaran_id = $idPendaftaran";
//                $stoks = Yii::app()->db->createCommand($sql)->queryAll();
                
            }
            
            if ($modPendaftaran->statuspasien == "PENGUNJUNG LAMA"){
                $modDiagnosaTerdahulu = RJPasienMorbiditasT::model()->with('diagnosa')->findAllByAttributes(array('pasien_id'=>$modPasien->pasien_id, 'pendaftaran_id'=>$idPendaftaran));
                
                $hasilImunisasi = array();
                $hasilDiagnosaDahulu = array();
                foreach($modDiagnosaTerdahulu as $row){
                    if ($row->diagnosa->diagnosa_imunisasi == true)
                        $hasilImunisasi[] = $row->diagnosa->diagnosa_nama;
                    else
                        $hasilDiagnosaDahulu[] = $row->diagnosa->diagnosa_nama;
                }
                if (empty($modAnamnesa->riwayatimunisais)){
                    $modAnamnesa->riwayatimunisasi = implode(', ',$hasilImunisasi);
                }
                if (empty($modAnamnesa->riwayatpenyakitterdahulu)){
                    $modAnamnesa->riwayatpenyakitterdahulu = implode(', ',$hasilDiagnosaDahulu);
                }
            }
            
            //echo $modAnamnesa->riwayatpenyakitterdahulu;exit();
            if(isset($_POST['RJAnamnesaT'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $detTriase = (isset($_POST['Triase']) ? $_POST['Triase'] : null);
                    $modAnamnesa->attributes=$_POST['RJAnamnesaT'];
                    if(isset($detTriase)){
                        if(count($detTriase) > 0){
                            foreach($detTriase as $i=>$triase){
                                $modAnamnesa->triase_id = $triase['triase_id'];
                            }
                        }
                    }
                    $modAnamnesa->keluhanutama = (isset($_POST['RIAnamnesaT']['keluhanutama'])>0) ? implode(', ', $_POST['RIAnamnesaT']['keluhanutama']) : '';
                    $modAnamnesa->keluhantambahan = (isset($_POST['RIAnamnesaT']['keluhantambahan'])> 0) ? implode(', ', $_POST['RIAnamnesaT']['keluhantambahan']) : '';
                    $modAnamnesa->save();
					
					/*
					$updateStatusPeriksa = PendaftaranT::model()->updateByPk($idPendaftaran,array(
						'statusperiksa'=>Params::statusPeriksa(2)
					));
					*/
					
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data Anamnesa berhasil disimpan");
                    $this->redirect($_POST['url']);       
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
                
            $modAnamnesa->tglanamnesis = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modAnamnesa->tglanamnesis, 'yyyy-MM-dd hh:mm:ss'));    
            
            $modDataDiagnosa = new RJDiagnosaM('search');
            $modDataDiagnosa->unsetAttributes();
            if(isset($_GET['RJDiagnosaM']))
                $modDataDiagnosa->attributes = $_GET['RJDiagnosaM'];
            
            $this->render($this->pathView.'index',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,
                        'modAnamnesa'=>$modAnamnesa, 'modDiagnosa'=>$modDiagnosa, 'modDataDiagnosa'=>$modDataDiagnosa,
		));
	}
        
        public function actionHapusTriase(){ 
            if(Yii::app()->request->isPostRequest)
            {
                    $idAnamesa = $_POST['idAnamesa'];
                    $idTriase = $_POST['idTriase'];
                    $modAnamnesa = AnamnesaT::model()->findByPk($idAnamesa);
                    if(!empty($modAnamnesa->triase_id)){
                        $update = AnamnesaT::model()->updateByPk($idAnamesa,array('triase_id'=>null));
                    }

                    if($update){
                        if (Yii::app()->request->isAjaxRequest)
                        {
                            echo CJSON::encode(array(
                                'status'=>'proses_form', 
                                'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                                ));
                            exit;               
                        }
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
            else
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        
}
