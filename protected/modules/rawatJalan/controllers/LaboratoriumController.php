<?php

class LaboratoriumController extends SBaseController
{
    protected $statusSaveKirimkeUnitLain = false;
    protected $statusSavePermintaanPenunjang = false;
    protected $pathView = 'rawatJalan.views.laboratorium.';

    /**
     * method untuk mengirimkan pasien ke unit lain
     * digunakan di :
     * 1. rawatJalan/laboratorium/index
     * @param int $idPendaftaran pendaftaran_id
     */
    public function actionIndex($idPendaftaran)
	{
            $insert_notifikasi = new MyFunction();
            $params = array();
            $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modKirimKeUnitLain = new RJPasienKirimKeUnitLainT;
            $modKirimKeUnitLain->tgl_kirimpasien = date('Y-m-d H:i:s');

            // $kirimUnit = PasienkirimkeunitlainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            
            // if(empty($kirimUnit[0]->pegawai_id)){
            //     $modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
            //     }else{
            //         $modKirimKeUnitLain->pegawai_id = $kirimUnit[0]->pegawai_id;
            //     }

            // $modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
            $modJenisPeriksaLab = RJJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan')); 
            $modPeriksaLab = RJPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true),array('order'=>'pemeriksaanlab_id, pemeriksaanlab_urutan'));
                        
            if(isset($_POST['RJPasienKirimKeUnitLainT'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modKirimKeUnitLain = $this->savePasienKirimKeUnitLain($modPendaftaran);
                    if(isset($_POST['permintaanPenunjang'])){
                        $this->savePermintaanPenunjang($_POST['permintaanPenunjang'],$modKirimKeUnitLain);
                        $updateStatusPeriksa=PendaftaranT::model()->updateByPk($idPendaftaran,array('statusperiksa'=>Params::statusPeriksa(2)));
                        
                        PendaftaranT::model()->updateByPk($idPendaftaran,
                            array(
                                'pembayaranpelayanan_id'=>null
                            )
                        );
                        
                        $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                        $params['create_time'] = date( 'Y-m-d H:i:s');
                        $params['create_loginpemakai_id'] = Yii::app()->user->id;
                        $params['instalasi_id'] = 5;
                        $params['modul_id'] = 8;
                        $ruangan = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'));
                        $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $modPendaftaran->no_pendaftaran . '-' . $modPasien->nama_pasien . '-' . $ruangan->ruangan_nama;
                        $params['create_ruangan'] = 18;
                        $params['judulnotifikasi'] = 'Rujukan Rawat Jalan';                        
                        $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                    } else {
                        $this->statusSavePermintaanPenunjang = true;
                    }
                    
                    if($this->statusSaveKirimkeUnitLain && $this->statusSavePermintaanPenunjang){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                        $this->refresh();
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data tidak valid ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $modRiwayatKirimKeUnitLain = RJPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                      'ruangan_id'=>Params::RUANGAN_ID_LAB),
                                                                                                'pasienmasukpenunjang_id IS NULL');
		
            $this->render($this->pathView.'index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modKirimKeUnitLain'=>$modKirimKeUnitLain,
                                        'modJenisPeriksaLab'=>$modJenisPeriksaLab,
                                        'modPeriksaLab'=>$modPeriksaLab,
                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
                                        ));
	}

        /**
         * method untuk menyimpan data pasien ke unit lain RJPasienKirimkeUnitLainT
         * digunakan di :
         * 1. rawatJalan/laboratorium/index
         * @param object $modPendaftaran model PendaftaranT
         * @return \RJPasienKirimKeUnitLainT 
         */
        protected function savePasienKirimKeUnitLain($modPendaftaran)
        {
            $modKirimKeUnitLain = new RJPasienKirimKeUnitLainT;
            $modKirimKeUnitLain->attributes = $_POST['RJPasienKirimKeUnitLainT'];
            $modKirimKeUnitLain->pasien_id = $modPendaftaran->pasien_id;
            $modKirimKeUnitLain->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//            $modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;

            // $kirimUnit = PasienkirimkeunitlainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            
            // if(empty($kirimUnit[0]->pegawai_id)){
            //     $modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
            //     }else{
            //         $modKirimKeUnitLain->pegawai_id = $kirimUnit[0]->pegawai_id;
            //     }
            
            $modKirimKeUnitLain->pegawai_id = $_POST['RJPasienKirimKeUnitLainT']['pegawai_id'];
            $modKirimKeUnitLain->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
             $modKirimKeUnitLain->instalasi_id = Params::INSTALASI_ID_LAB;
            $modKirimKeUnitLain->ruangan_id = Params::RUANGAN_ID_LAB;
            $modKirimKeUnitLain->nourut = Generator::noUrutPasienKirimKeUnitLain($modKirimKeUnitLain->ruangan_id);
            if($modKirimKeUnitLain->validate()){
                if ($modKirimKeUnitLain->save())
                    $this->statusSaveKirimkeUnitLain = true;
            }
            
            return $modKirimKeUnitLain;
        }
        
        /**
         * method untuk menyimpan dan validasi permintaan penunjang
         * digunakan di :
         * 1. rawatJalan/laboratorium/index
         * @param array $permintaan berupa post request berisi data permintaan penunjang
         * @param object $modKirimKeUnitLain model PasienkirimkeunitlainT
         */
        protected function savePermintaanPenunjang($permintaan,$modKirimKeUnitLain)
        {
            foreach ($permintaan['inputpemeriksaanlab'] as $i => $value) {
                $modPermintaan = new RJPermintaanPenunjangT;
                $modPermintaan->daftartindakan_id = "";//$permintaan['idDaftarTindakan'][$i];
                $modPermintaan->pemeriksaanlab_id = $permintaan['inputpemeriksaanlab'][$i];
                $modPermintaan->pemeriksaanrad_id = '';
                $modPermintaan->pasienkirimkeunitlain_id = $modKirimKeUnitLain->pasienkirimkeunitlain_id;
                $modPermintaan->noperminatanpenujang = Generator::noPermintaanPenunjang('PL');
                $modPermintaan->qtypermintaan = $permintaan['inputqty'][$i];
                $modPermintaan->tglpermintaankepenunjang = $modKirimKeUnitLain->tgl_kirimpasien; //date('Y-m-d H:i:s');
                if($modPermintaan->validate()){
                    if ($modPermintaan->save())
                        $this->statusSavePermintaanPenunjang = true;
                }
            }
        }
        
        public function actionAjaxBatalKirim()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idPasienKirimKeUnitLain = $_POST['idPasienKirimKeUnitLain'];
            $idPendaftaran = $_POST['idPendaftaran'];
            
            PermintaankepenunjangT::model()->deleteAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            PasienkirimkeunitlainT::model()->deleteByPk($idPasienKirimKeUnitLain);
            $modRiwayatKirimKeUnitLain = RJPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                      'ruangan_id'=>Params::RUANGAN_ID_LAB),
                                                                                                'pasienmasukpenunjang_id IS NULL');
            
            $data['result'] = $this->renderPartial($this->pathView.'_listKirimKeUnitLain', array('modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain), true);

            echo json_encode($data);
             Yii::app()->end();
            }
        }
        
        public function actionPrint()
        {
             $idPendaftaran = $_GET['id'];
             $modPendaftaran= PendaftaranT::model()->findByPk($idPendaftaran);
             $modKirimKeUnitLain = RJPasienKirimKeUnitLainT::model()->findAll('pendaftaran_id='.$idPendaftaran);
             $modRiwayatKirimKeUnitLain = RJPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                      'ruangan_id'=>Params::RUANGAN_ID_LAB),
                                                                                                'pasienmasukpenunjang_id IS NULL');
            
            $judulLaporan='Permintaan Pasien Ke Laboratorium';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('modKirimKeUnitLain'=> $modKirimKeUnitLain,'modPendaftaran'=>$modPendaftaran,'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('modKirimKeUnitLain'=> $modKirimKeUnitLain,'modPendaftaran'=>$modPendaftaran,'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('modKirimKeUnitLain'=> $modKirimKeUnitLain,'modPendaftaran'=>$modPendaftaran,'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }


        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}