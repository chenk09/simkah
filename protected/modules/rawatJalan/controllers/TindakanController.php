<?php
class TindakanController extends SBaseController
{
    public $succesSave = false;
    public $pesan = "";
    protected $successSaveBmhp = true;
    protected $successSavePemakaianBahan = true;
    protected $pathView = 'rawatJalan.views.tindakan.';

    /**
	 * @return array action filters
	 */
//	FILTER USER SUDAH MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','AjaxDeleteTindakanPelayanan','UpdateStok'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
        
	public function actionIndex($idPendaftaran)
	{
//$this->layout = false;
            //$modAdmisi = (!empty($idAdmisi)) ? PasienadmisiT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran,'pasienadmisi_id'=>$idAdmisi)) : array();
            $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modViewTindakans = RJTindakanPelayananT::model()
                                ->with('daftartindakan','dokter1','dokter2','dokterPendamping','dokterAnastesi',
                                       'dokterDelegasi','bidan','suster','perawat','tipePaket')
                                ->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            $modTindakans = null;
            $modTindakan = new RJTindakanPelayananT;
            $modTindakan->tarifcyto_tindakan = 0;
            $modTindakan->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
            $modTindakan->dokterpemeriksa1Nama = $modPendaftaran->pegawai->NamaLengkap;
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            
            if(isset($_POST['RJTindakanPelayananT']) || isset($_POST['TindakanpelayananT'])){
                
            
//                $post = (isset($_POST['TindakanpelayananT'])) ? $_POST['TindakanpelayananT'] : $_POST['RJTindakanPelayananT'];
//                echo '<pre>'.print_r($post,1).'</pre>'; exit;
                $modTindakans = $this->saveTindakan($modPasien, $modPendaftaran, $_POST['RekeningakuntansiV']);

                if($this->succesSave)
                    $this->redirect(array($this->id.'/','idPendaftaran'=>$idPendaftaran));
            }
            
            if (isset($_GET['FilterForm'])){
                $_GET['test'] = true;
                $_GET['term'] = $_GET['FilterForm'];
            }
            if (isset($_GET['FilterForm2'])){
                $_GET['test'] = true;
                $_GET['term2'] = $_GET['FilterForm2'];
            }
            if (isset($_GET['FilterForm3'])){
                $_GET['test'] = true;
                $_GET['term3'] = $_GET['FilterForm3'];
            }
            if (isset($_GET['FilterForm4'])){
                $_GET['test'] = true;
                $_GET['term4'] = $_GET['FilterForm4'];
            }

            // var_dump($_GET['FilterForm']);
                // exit();
            

            if (isset($_GET['test'])){
                $_GET['term'] = (isset($_GET['term'])) ? $_GET['term'] : null;
                $_GET['term2'] = (isset($_GET['term2'])) ? $_GET['term2'] : null;
                $_GET['term3'] = (isset($_GET['term3'])) ? $_GET['term3'] : null;
                $_GET['term4'] = (isset($_GET['term4'])) ? $_GET['term4'] : null;
                if(Yii::app()->request->isAjaxRequest) {
                if($_GET['idTipePaket']==Params::TIPEPAKET_LUARPAKET){
                    $criteria = new CDbCriteria();
                    $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['term']), true);
                    $criteria->compare('LOWER(daftartindakan_kode)', strtolower($_GET['term2']), true);
                    $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['term3']), true);
                    if(Yii::app()->user->getState('tindakanruangan'))
                        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                    if(Yii::app()->user->getState('tindakankelas'))
                        $criteria->compare('kelaspelayanan_id', $_GET['idKelasPelayanan']);
                    $criteria->compare('tipepaket_id', Params::TIPEPAKET_LUARPAKET);
                    $criteria->order = 'daftartindakan_nama';
                    $models = 'PaketpelayananV';
                } else if($_GET['idTipePaket']==Params::TIPEPAKET_NONPAKET) {
                    $criteria = new CDbCriteria();
                    $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['term']), true);
                    $criteria->compare('LOWER(daftartindakan_kode)', strtolower($_GET['term2']), true);
                    $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['term3']), true);
                    $criteria->order = "daftartindakan_kode ASC, daftartindakan_nama ASC, substring(daftartindakan_kode from '.....$') ASC";
                    if(Yii::app()->user->getState('tindakankelas'))
                        $criteria->compare('kelaspelayanan_id', $_GET['idKelasPelayanan']);
                    if(Yii::app()->user->getState('tindakanruangan')){
                        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                        $models = 'TariftindakanperdaruanganV';
                    } else {
                        $models = 'TariftindakanperdaV';
                    }
                } else {
                    $criteria = new CDbCriteria();
                    $criteria->compare('LOWER(daftartindakan_kode)', strtolower($_GET['daftartindakan_kode']), true);
                    $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['daftartindakan_nama']), true);
                    $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['kategoritindakan_nama']), true);
                    if(Yii::app()->user->getState('tindakanruangan'))
                        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                    if(Yii::app()->user->getState('tindakankelas'))
                        $criteria->compare('kelaspelayanan_id', $_GET['idKelasPelayanan']);
                    $criteria->compare('tipepaket_id', $_GET['idTipePaket']);
                    $criteria->order = 'daftartindakan_nama';
                    
                    $models = 'PaketpelayananV';
                }

                $dataProvider = new CActiveDataProvider($models, array(
			'criteria'=>$criteria,
		));
                    $route = Yii::app()->createUrl($this->route, array('idPendaftaran'=>$idPendaftaran, 'idKelasPelayanan'=>$_GET['idKelasPelayanan'], 'idTipePaket'=>$_GET['idTipePaket']));
                    $route2 = Yii::app()->createUrl($this->route, array('idPendaftaran'=>$idPendaftaran, 'idKelasPelayanan'=>$_GET['idKelasPelayanan'], 'idTipePaket'=>$_GET['idTipePaket']));
                    $route3 = Yii::app()->createUrl($this->route, array('idPendaftaran'=>$idPendaftaran, 'idKelasPelayanan'=>$_GET['idKelasPelayanan'], 'idTipePaket'=>$_GET['idTipePaket']));
                    $route4 = Yii::app()->createUrl($this->route, array('idPendaftaran'=>$idPendaftaran, 'idKelasPelayanan'=>$_GET['idKelasPelayanan'], 'idTipePaket'=>$_GET['idTipePaket']));
                    $this->renderPartial($this->pathView.'_daftarTindakanPaket', array('dataProvider'=>$dataProvider, 'models'=>$models, 'route'=>$route,'route2'=>$route2,'route3'=>$route3,'route4'=>$route4));
                    Yii::app()->end();
                }
            }
            
            $modViewBmhp = RJObatalkesPasienT::model()->with('obatalkes')->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));

            $this->render($this->pathView.'index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modTindakans'=>$modTindakans,
                                        'modTindakan'=>$modTindakan,
                                        'modViewTindakans'=>$modViewTindakans,
                                        'modViewBmhp'=>$modViewBmhp,
                                        'modRekenings'=>$modRekenings,
                ));

	}
        
        public function saveTindakan($modPasien,$modPendaftaran, $postRekenings = array())
        {
            $post = (isset($_POST['TindakanpelayananT'])) ? $_POST['TindakanpelayananT'] : $_POST['RJTindakanPelayananT'];
            $valid=true; //echo $_POST['RJTindakanPelayananT'][0]['tipepaket_id'];exit;
            foreach($post as $i=>$item)
            {
                if(!empty($item) && (!empty($item['daftartindakan_id']))){
                    $modTindakans[$i] = new RJTindakanPelayananT;
                    $modTindakans[$i]->attributes=$item;
                    $modTindakans[$i]->tipepaket_id = $_POST['RJTindakanPelayananT'][0]['tipepaket_id'];
                    $modTindakans[$i]->pasien_id = $modPasien->pasien_id;
                    $modTindakans[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                    $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                    $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                    $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                    $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//                    $modTindakans[$i]->tgl_tindakan = $item['tgl_tindakan'];
                    $modTindakans[$i]->tgl_tindakan = $modTindakans[0]->tgl_tindakan;
                    $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                    $modTindakans[$i]->tarif_tindakan = $modTindakans[$i]->tarif_satuan * $modTindakans[$i]->qty_tindakan;
                    if($item['cyto_tindakan'])
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['persenCyto'] / 100) * $modTindakans[$i]->tarif_tindakan;
                    else
                        $modTindakans[$i]->tarifcyto_tindakan = 0;
                    $modTindakans[$i]->discount_tindakan = 0;
                    $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                    $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                    $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                    $modTindakans[$i]->iurbiaya_tindakan = 0;
                    $modTindakans[$i]->instalasi_id = $modPendaftaran->instalasi_id;
                    $modTindakans[$i]->ruangan_id =  Yii::app()->user->getState('ruangan_id');
                    $modTindakans[$i]->alatmedis_id = $this->cekAlatmedis($modTindakans[$i]->daftartindakan_id);
                                        
                    $tarifTindakan= TariftindakanM::model()->findAll('daftartindakan_id='.$item['daftartindakan_id'].' AND kelaspelayanan_id='.$modTindakans[$i]->kelaspelayanan_id.'');
                    foreach($tarifTindakan AS $dataTarif):
                        if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakans[$i]->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakans[$i]->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakans[$i]->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakans[$i]->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                    endforeach;
                    $valid = $modTindakans[$i]->validate() && $valid;
                    
                    
                }
            }

            $transaction = Yii::app()->db->beginTransaction();
            try {
                if($valid && (count($modTindakans) > 0)){
                    foreach($modTindakans as $i=>$tindakan){
                        $tindakan->save();
                        if(isset($postRekenings)){
                            $modJurnalRekening = TindakanController::saveJurnalRekening();
                            //update jurnalrekening_id
                            $tindakan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                            $tindakan->save();
                            $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $tindakan, 'tm');
                        }
                        $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
                        if(isset($_POST['paketBmhp'])){
                            $modObatPasiens = $this->savePaketBmhp($modPendaftaran, $_POST['paketBmhp'],$tindakan,$postRekenings);
                        }
                        if(isset($_POST['pemakaianBahan'])){
                            $modPemakainBahans = $this->savePemakaianBahan($modPendaftaran, $_POST['pemakaianBahan'],$tindakan,$postRekenings);
                        }
                    }
                    if($statusSaveKomponen && $this->successSaveBmhp && $this->successSavePemakaianBahan) {
                        $updateStatusPeriksa=PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id,array(
                                    'pembayaranpelayanan_id'=>null,
                                    'statusperiksa'=>Params::statusPeriksa(2),
                                ));
                        
                        $transaction->commit();
                        $this->succesSave = true;
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                        //Yii::app()->user->setFlash('error',"Data valid ".$this->traceObatAlkesPasien($modPemakainBahans));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data tidak valid 1");
                        //Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceObatAlkesPasien($modPemakainBahans));
                    }
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data tidak valid 2");
                    //Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceTindakan($modTindakans));
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
            }
            
            return $modTindakans;
        }
        
        protected function cekAlatmedis($idDaftartindakan)
        {
            $idAlatmedis = null;
            if(!empty($_POST['pemakaianAlat'])){
                foreach($_POST['pemakaianAlat'] as $k=>$item){
                    if($item['daftartindakan_id']==$idDaftartindakan){
                        $idAlatmedis = $item['alatmedis_id'];
                    }
                }
            }
            
            return $idAlatmedis;
        }

        public function saveTindakanKomponen($tindakan)
        {   
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->addCondition('daftartindakan_id ='.$tindakan->daftartindakan_id);
            $criteria->addCondition('kelaspelayanan_id ='.$tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = new TindakankomponenT;
                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen->validate() && $valid;
                if($valid)
                    $modTindakanKomponen->save();
            }
            
            return $valid;
        }

        private function traceTindakan($modTindakans)
        {
            foreach ($modTindakans as $key => $modTindakan) {
                $echo .= "<pre>".print_r($modTindakan->attributes,1)."</pre>";
            }
            return $echo;
        }
        
        public function actionAjaxDeleteTindakanPelayanan()
        {
            if(Yii::app()->request->isAjaxRequest) {
                $konfigSys = KonfigsystemK::model()->find();
                $tindakanpelayanan_id = $_POST['idTindakanpelayanan'];
                $sukses_pembalik = true;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    //DINONAKTIFKAN KARENA PENGHAPUSAN DI ATUR DI RELASI TABEL : CASCADE ON DELETE
//                    if($konfigSys->isdeljurnaltransaksi == true){
//                        $updateTindakan = RJTindakanPelayananT::model()->findByPk($tindakanpelayanan_id);
//                        if(isset($updateTindakan->jurnalrekening_id)){
//                            $jurnalrekId = $updateTindakan->jurnalrekening_id;
//                            $updateTindakan->jurnalrekening_id = null;
//                            $updateTindakan->save();
//                            // hapus jurnaldetail_t dan jurnalrekening_t
//                            if($jurnalrekId){
//                                JurnaldetailT::model()->deleteAllByAttributes(array('jurnalrekening_id'=>$jurnalrekId));
//                                JurnalrekeningT::model()->deleteByPk($jurnalrekId);
//                            }
//                        }
//                    }
                    
                    $sukses_pembalik = self::jurnalPembalikTindakan($tindakanpelayanan_id);

                    $obatAlkesT = RJObatalkesPasienT::model()->findAllByAttributes(
                        array(
                            'tindakanpelayanan_id'=>$tindakanpelayanan_id
                        )
                    );
                    $data['success'] = true;
                    if(count($obatAlkesT) > 0){
                        $this->kembalikanStok($obatAlkesT);
                        //DINONAKTIFKAN KARENA PENGHAPUSAN DI ATUR DI RELASI TABEL : CASCADE ON DELETE
//                        if($konfigSys->isdeljurnaltransaksi == true){//hapus jurnalrekening_t
//                            $updateObatPasiens = RJObatalkesPasienT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakanpelayanan_id));
//                            if(count($updateObatPasiens)>0){
//                                foreach($updateObatPasiens AS $i => $modObat){
//                                    if(isset($modObat->jurnalrekening_id)){
//                                        $jurnalrekId = $modObat->jurnalrekening_id;
//                                        $modObat->jurnalrekening_id = null;
//                                        $modObat->save();
//                                        // hapus jurnaldetail_t dan jurnalrekening_t
//                                        JurnaldetailT::model()->deleteAllByAttributes(array('jurnalrekening_id'=>$jurnalrekId));
//                                        JurnalrekeningT::model()->deleteByPk($jurnalrekId);
//                                    }
//                                }
//                            }
//                        }
                        $modObats = ObatalkesPasienT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakanpelayanan_id));
                        if(isset($modObats)){
                            if(count($modObats)>0){  
                                foreach($modObats AS $i => $obat){
                                    $sukses_pembalik = self::jurnalPembalikObatalkes($obat->obatalkespasien_id);
                                    $sukses_pembalik &= $sukses_pembalik;
                                }
                            }
                        }
                        
                        $deleteObatPasien = RJObatalkesPasienT::model()->deleteAllByAttributes(
                            array(
                                'tindakanpelayanan_id'=>$tindakanpelayanan_id
                            )
                        );
                        if(!$deleteObatPasien)
                        {
                            $data['success'] = false;
                        }
                    }

                    $deleteTindakan = RJTindakanPelayananT::model()->deleteByPk($tindakanpelayanan_id);

                    if ($deleteTindakan && $data['success'] && $sukses_pembalik){
                        $data['success'] = true;
                        $transaction->commit();
                    }else{
                        $data['success'] = false;
                        $transaction->rollback();
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    echo MyExceptionMessage::getMessage($exc,true);
                    $data['success'] = false;
                }



                echo json_encode($data);
                 Yii::app()->end();
            }
        }
        
        
        protected function savePaketBmhp($modPendaftaran,$paketBmhp,$tindakan, $postRekenings = array())
        {
            $valid = true; $totalBmhp = 0;
            foreach ($paketBmhp as $i => $bmhp) {
                if($tindakan->daftartindakan_id == $bmhp['daftartindakan_id']){
                    $modObatPasien[$i] = new RJObatalkesPasienT;
                    $modObatPasien[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modObatPasien[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                    $modObatPasien[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                    $modObatPasien[$i]->daftartindakan_id = $bmhp['daftartindakan_id'];
                    $modObatPasien[$i]->sumberdana_id = $bmhp['sumberdana_id'];
                    $modObatPasien[$i]->pasien_id = $modPendaftaran->pasien_id;
                    $modObatPasien[$i]->satuankecil_id = $bmhp['satuankecil_id'];
                    $modObatPasien[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                    $modObatPasien[$i]->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                    $modObatPasien[$i]->tipepaket_id = Params::TIPEPAKET_BMHP; //$tindakan->tipepaket_id;
                    $modObatPasien[$i]->obatalkes_id = $bmhp['obatalkes_id'];
                    $modObatPasien[$i]->pegawai_id = $modPendaftaran->pegawai_id;
                    $modObatPasien[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                    $modObatPasien[$i]->shift_id = Yii::app()->user->getState('shift_id');
                    $modObatPasien[$i]->tglpelayanan = date('Y-m-d H:i:s');
                    $modObatPasien[$i]->qty_oa = $bmhp['qtypemakaian'];
                    $modObatPasien[$i]->hargajual_oa = $bmhp['hargapemakaian'];
                    $modObatPasien[$i]->harganetto_oa = $bmhp['harganetto'];
                    $modObatPasien[$i]->hargasatuan_oa = $bmhp['hargapemakaian']; //$bmhp['hargasatuan'];
                    $totalBmhp = $totalBmhp + $bmhp['hargapemakaian'];

                    $valid = $modObatPasien[$i]->validate() && $valid;
                    if($valid) {
                        $modObatPasien[$i]->save();
                        StokobatalkesT::kurangiStok($modObatPasien[$i]->qty_oa, $modObatPasien[$i]->obatalkes_id);
                        if(isset($postRekenings)){
                            $modJurnalRekening = TindakanController::saveJurnalRekening();
                            //update jurnalrekening_id
                            $modObatPasien[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                            $modObatPasien[$i]->save();
                            $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modObatPasien[$i], 'oa');
                        }
                        $this->successSaveBmhp = true;
                    } else {
                        $this->successSaveBmhp = false;
                    }
                }
            }
            
            $totalBmhp = $totalBmhp + $tindakan->tarif_bhp;
            $tindakan->tarif_bhp = $totalBmhp;
            $tindakan->update();
            
            return $modObatPasien;
        }
        
        protected function savePemakaianBahan($modPendaftaran,$pemakaianBahan,$tindakan, $postRekenings = array())
        {
            $valid = true;
            foreach ($pemakaianBahan as $i => $bmhp) {
                if($tindakan->daftartindakan_id == $bmhp['daftartindakan_id']){
                    $modPakaiBahan[$i] = new RJObatalkesPasienT;
                    $modPakaiBahan[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modPakaiBahan[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                    $modPakaiBahan[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                    $modPakaiBahan[$i]->daftartindakan_id = $bmhp['daftartindakan_id'];
                    $modPakaiBahan[$i]->sumberdana_id = $bmhp['sumberdana_id'];
                    $modPakaiBahan[$i]->pasien_id = $modPendaftaran->pasien_id;
                    $modPakaiBahan[$i]->satuankecil_id = $bmhp['satuankecil_id'];
                    $modPakaiBahan[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                    $modPakaiBahan[$i]->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                    $modPakaiBahan[$i]->tipepaket_id = $tindakan->tipepaket_id;
                    $modPakaiBahan[$i]->obatalkes_id = $bmhp['obatalkes_id'];
                    $modPakaiBahan[$i]->pegawai_id = $modPendaftaran->pegawai_id;
                    $modPakaiBahan[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                    $modPakaiBahan[$i]->shift_id = Yii::app()->user->getState('shift_id');
                    $modPakaiBahan[$i]->tglpelayanan = date('Y-m-d H:i:s');
                    $modPakaiBahan[$i]->qty_oa = $bmhp['qty'];
                    $modPakaiBahan[$i]->hargajual_oa = $bmhp['subtotal'];
                    $modPakaiBahan[$i]->harganetto_oa = $bmhp['harganetto'];
                    $modPakaiBahan[$i]->hargasatuan_oa = $bmhp['hargasatuan'];

                    $valid = $modPakaiBahan[$i]->validate() && $valid;
                    if($valid) {
                        $modPakaiBahan[$i]->save();
                        StokobatalkesT::kurangiStok($modPakaiBahan[$i]->qty_oa, $modPakaiBahan[$i]->obatalkes_id);
                        if(isset($postRekenings)){
                            $modJurnalRekening = TindakanController::saveJurnalRekening();
                            //update jurnalrekening_id
                            $modPakaiBahan[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                            $modPakaiBahan[$i]->save();
                            $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modPakaiBahan[$i], 'oa');
                        }
                        $this->successSavePemakaianBahan = true;
                    } else {
                        $this->successSavePemakaianBahan = false;
                    }
                }
            }
            
            return $modPakaiBahan;
        }

        private function traceObatAlkesPasien($modObatPasiens)
        {
            foreach ($modObatPasiens as $key => $modObatPasien) {
                $echo .= "<pre>".print_r($modObatPasien->attributes,1)."</pre>";
            }
            return $echo;
        }

        /**
         * 
         * @param ObatalkespasienT $modObatPasien 
         */
        protected function saveObatAlkesKomponen($modObatPasien)
        {
            $modObatPasien = new ObatalkespasienT;
            $obat = ObatalkesM::model()->findByPk($modObatPasien->obatalkes_id);
            $obat = new ObatalkesM;
            $modObatPasienKomponen = new ObatalkeskomponenT;
            $modObatPasienKomponen->obatalkespasien_id = $modObatPasien->obatalkespasien_id;
            $modObatPasienKomponen->hargajualkomponen = $obat->hargajual;
            $modObatPasienKomponen->harganettokomponen = $obat->harganetto;
            $modObatPasienKomponen->hargasatuankomponen = $obat->hargajual;
            $modObatPasienKomponen->iurbiaya = 0;
            $modObatPasienKomponen->komponentarif_id = null;
            
        }
        
//        GUNAKAN PENGURANGAN STOK DI StokobatalkesT
//        protected function kurangiStok($qty,$idobatAlkes)
//        {
//            $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $idobatAlkes ORDER BY tglstok_in";
//            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
//            $selesai = false;
////            while(!$selesai){
//                foreach ($stoks as $i => $stok) {
//                    if($qty <= $stok['qtystok_current']) {
//                        $stok_current = $stok['qtystok_current'] - $qty;
//                        $stok_out = $stok['qtystok_out'] + $qty;
//                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_current'=>$stok_current,'qtystok_out'=>$stok_out));
//                        $selesai = true;
//                        break;
//                    } else {
//                        $qty = $qty - $stok['qtystok_current'];
//                        $stok_current = 0;
//                        $stok_out = $stok['qtystok_out'] + $stok['qtystok_current'];
//                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('stok_current'=>$stok_current,'qtystok_out'=>$stok_out));
//                    }
//                }
////            }
//        }
        
        protected function kembalikanStok($obatAlkesT)
        {
            foreach ($obatAlkesT as $i => $obatAlkes) {
                $stok = new RJStokObatalkesT;
                $stok->obatalkes_id = $obatAlkes->obatalkes_id;
                $stok->sumberdana_id = $obatAlkes->sumberdana_id;
                $stok->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $stok->tglstok_in = date('Y-m-d H:i:s');
                $stok->tglstok_out = date('Y-m-d H:i:s');
                $stok->qtystok_in = $obatAlkes->qty_oa;
                $stok->qtystok_out = 0;
                $stok->qtystok_current = $obatAlkes->qty_oa;
                $stok->harganetto_oa = $obatAlkes->harganetto_oa;
                $stok->hargajual_oa = $obatAlkes->hargasatuan_oa;
                $stok->discount = $obatAlkes->discount;
                $stok->satuankecil_id = $obatAlkes->satuankecil_id;
                $stok->save();
            }
        }
        
        private function cekKomponenTarifTindakan($idKomponenTarifTindakan)
        {
            switch ($idTarifTindakan) {
                case Params::KOMPONENTARIF_ID_RS:return 'tarif_rsakomodasi';
                case Params::KOMPONENTARIF_ID_MEDIS:return 'tarif_medis';
                case Params::KOMPONENTARIF_ID_PARAMEDIS:return 'tarif_paramedis';
                case Params::KOMPONENTARIF_ID_BHP:return 'tarif_bhp';

                default:return null;
                    break;
            }
        }
        /**
         * simpan jurnalrekening_t
         * @return \JurnalrekeningT
         * @params $ruangan_id = jika ruangan bukan diambil dari getState("ruangan_id")
         */
        public function saveJurnalRekening($ruangan_id = null)
        {
            $modJurnalRekening = new JurnalrekeningT;
            $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');
            $modJurnalRekening->nobuktijurnal = KeyGenerator::noBuktiJurnalRek();
            $modJurnalRekening->kodejurnal = KeyGenerator::kodeJurnalRek();
            $modJurnalRekening->noreferensi = 0;
            $modJurnalRekening->tglreferensi = date('Y-m-d H:i:s');
            $modJurnalRekening->nobku = "";
            $modJurnalRekening->urianjurnal = "";
            $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENERIMAAN_KAS;
            $periodeID = Yii::app()->session['periodeID'];
            $modJurnalRekening->rekperiod_id = $periodeID[0];
            if (empty($modJurnalRekening->rekperiod_id)){
                $periodeID = Yii::app()->user->getState('periodeID');
            }
            $modJurnalRekening->rekperiod_id = $periodeID[0];
            $modJurnalRekening->create_time = date('Y-m-d H:i:s');
            $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
            $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modJurnalRekening->ruangan_id = empty($ruangan_id) ? Yii::app()->user->getState('ruangan_id') : $ruangan_id;
            
            
            if($modJurnalRekening->validate()){
                $modJurnalRekening->save();
            } else {
                $modJurnalRekening['errorMsg'] = $modJurnalRekening->getErrors();
            }
            return $modJurnalRekening;
        }
        /**
         * insert detail dari post rekening digunakan di:
         * - rawatJalan/TindakanController
         * @param type $modJurnalRekening
         * @param type $postRekenings
         * @param type $jenisPelayanan = 'tm' / 'oa' / 'ap'
         * @param type $jenisSimpan
         * @return boolean
         */
        public function saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanObat, $jenisPelayanan, $jenisSimpan = null){
            $valid = true;
            $modJurnalPosting = null;
            if($jenisSimpan == 'posting')
            {
                $modJurnalPosting = new JurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                }
            }
            
            foreach($postRekenings AS $i => $rekening){
                //insert berdasarkan daftartindakan_id atau obatalkes_id dgn melihat jnspelayanan
                $idOaTindakanSama = false;
                if(isset($modTindakanObat->daftartindakan_id)){
                    if(trim($rekening['daftartindakan_id'] == $modTindakanObat->daftartindakan_id))
                        $idOaTindakanSama = true;
                    $nama_tindakan = $modTindakanObat->daftartindakan->daftartindakan_nama;
                }else if(isset($modTindakanObat->obatalkes_id)){
                    if(trim($rekening['obatalkes_id'] == $modTindakanObat->obatalkes_id))
                        $idOaTindakanSama = true;
                    $nama_tindakan = $modTindakanObat->obatalkes->obatalkes_kode." - ".$modTindakanObat->obatalkes->obatalkes_nama;
                }
                if(strtolower(trim($rekening['jnspelayanan'])) == strtolower(trim($jenisPelayanan)) 
                    && $idOaTindakanSama){
                    
                    if($rekening['saldodebit'] == 0 && $rekening['saldokredit'] == 0)
                    {
                        
                    }
                    else{
                        $model[$i] = new JurnaldetailT();
                        $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
                        $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
                        $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $model[$i]->uraiantransaksi = $nama_tindakan;
                        $model[$i]->saldodebit = $rekening['saldodebit'];
                        $model[$i]->saldokredit = $rekening['saldokredit'];
                        $model[$i]->nourut = $i+1;
                        $model[$i]->rekening1_id = $rekening['struktur_id'];
                        $model[$i]->rekening2_id = $rekening['kelompok_id'];
                        $model[$i]->rekening3_id = $rekening['jenis_id'];
                        $model[$i]->rekening4_id = $rekening['obyek_id'];
                        $model[$i]->rekening5_id = $rekening['rincianobyek_id'];
                        $model[$i]->catatan = "";
                        if($model[$i]->validate())
                        {
                            $model[$i]->save();
                        }else{
    //                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $model[$i]->getErrors();
                            $valid = false;
                            break;
                        }
                    }
                    
                }
            }
            return $valid;        
        }
    
        /**
         * Jurnal Pembalik (jurnaldetail_t) untuk tindakanpelayanan_t | EHJ-2020
         * @param type $tindakanpelayanan_id
         * @return boolean
         */
        public function jurnalPembalikTindakan($tindakanpelayanan_id){
            $sukses = true;
            $modTindakan = TindakanpelayananT::model()->findByPk($tindakanpelayanan_id);
            if(isset($modTindakan->jurnalrekening_id)){
                $modJurnalRekening = JurnalrekeningT::model()->findByPk($modTindakan->jurnalrekening_id);
                if(isset($modJurnalRekening)){
                    $modJurnalDetail = JurnaldetailT::model()->findAllByAttributes(array('jurnalrekening_id'=>$modJurnalRekening->jurnalrekening_id));
                    if(isset($modJurnalDetail)){
                        if(count($modJurnalDetail) > 0){
                            foreach($modJurnalDetail AS $i => $jurnal){
                                $jurnalBaru = new JurnaldetailT;
                                $jurnalBaru->attributes = $jurnal->attributes;
                                $jurnalBaru->jurnaldetail_id = null; //dikosongkan agar record baru
                                $jurnalBaru->saldokredit = $jurnal->saldodebit;
                                $jurnalBaru->saldodebit = $jurnal->saldokredit;
                                $jurnalBaru->catatan = "TINDAKAN DIBATALKAN";
                                if($jurnalBaru->save())
                                    $sukses_pembalik &= true;
                                else
                                    $sukses_pembalik = false;
                            }
                        }
                    }
                }
            }
            return $sukses;
        }
        
        /**
         * Jurnal Pembalik (jurnaldetail_t) untuk obatalkespasien_t | EHJ-2020
         * @param type $obatalkespasien_id
         * @return boolean
         */
        public function jurnalPembalikObatalkes($obatalkespasien_id){
            $sukses = true;
            $modObat = ObatalkespasienT::model()->findByPk($obatalkespasien_id);
            if(isset($modObat->jurnalrekening_id)){
                $modJurnalRekening = JurnalrekeningT::model()->findByPk($modObat->jurnalrekening_id);
                if(isset($modJurnalRekening)){
                    $modJurnalDetail = JurnaldetailT::model()->findAllByAttributes(array('jurnalrekening_id'=>$modJurnalRekening->jurnalrekening_id));
                    if(isset($modJurnalDetail)){
                        if(count($modJurnalDetail) > 0){
                            foreach($modJurnalDetail AS $i => $jurnal){
                                $jurnalBaru = new JurnaldetailT;
                                $jurnalBaru->attributes = $jurnal->attributes;
                                $jurnalBaru->jurnaldetail_id = null; //dikosongkan agar record baru
                                $jurnalBaru->saldokredit = $jurnal->saldodebit;
                                $jurnalBaru->saldodebit = $jurnal->saldokredit;
                                $jurnalBaru->catatan = "OBAT DIBATALKAN";
                                if($jurnalBaru->save())
                                    $sukses_pembalik &= true;
                                else
                                    $sukses_pembalik = false;
                            }
                        }
                    }
                }
            }
            return $sukses;
        }
        
        
        
//        public function actionUpdateStok()
//        {
//            $qty = $_POST['qty'];
//            $idobatAlkes = $_POST['idObatAlkes'];
//            $sql = "select stokobatalkes_id,qtystok_in,qtystok_out from stokobatalkes_t order by tglstok_in";
//            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
//            $selesai = false;
//            while(!$selesai){
//                foreach ($stoks as $i => $stok) {
//                    if($qty <= $stok['qtystok_in']) {
//                        $stok_in = $stok['qtystok_in'] - $qty;
//                        $stok_out = $stok['qtystok_out'] + $qty;
//                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_in'=>$stok_in,'qtystok_out'=>$stok_out));
//                        $selesai = true;
//                        break;
//                    } else {
//                        $qty = $qty - $stok['qtystok_in'];
//                        $stok_in = 0;
//                        $stok_out = $stok['qtystok_out'] + $stok['qtystok_in'];
//                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_in'=>$stok_in,'qtystok_out'=>$stok_out));
//                    }
//                }
//            }
//            $data['input'] = 'qty: '.$qty.' | ID Obat: '.$idobatAlkes;
//            echo CJSON::encode($data);
//            Yii::app()->end();
//        }


        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
