<?php
class DaftarPasienRajalController extends SBaseController{
	public $defaultAction = 'index';
	public $pathView = "rawatJalan.views.";

	public function actionIndex()
	{
		$model = new RJInfokunjunganrjV();
		$model->unsetAttributes();
		$model->tglAwal = date('d M Y 00:00:00');
		$model->tglAkhir = date('d M Y H:i:s');
		$model->ruangan_id = Yii::app()->user->getState('ruangan_id');

		if(isset($_GET['RJInfokunjunganrjV'])){
			$model->attributes = $_GET['RJInfokunjunganrjV'];
			$format = new CustomFormat();
			$model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAwal']);
			$model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAkhir']);
		}
		$this->render('index',array('model'=>$model));
	}

	public function actionKirimFarmasi(){
		$this->layout='//layouts/frameDialog';
		$pendaftaran_id = $_GET["pendaftaran_id"];
		$pendaftaran = PendaftaranT::model()->findByPK($pendaftaran_id);
		$pasienpulangT = PasienpulangT::model()->findByPK($pendaftaran->pasienpulang_id);
		$pasienM = PasienM::model()->findByPk($pendaftaran->pasien_id);

		$model = new LogKirimFarmasi;
		$model->pendaftaran_id = $pendaftaran_id;

		if(isset($_POST["LogKirimFarmasi"])){
			$pesan = array();
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$model->attributes = $_POST["LogKirimFarmasi"];

				$pendaftaran->kirim_farmasi = true;
				$pendaftaran->update();

				if(!$model->validate()){
					foreach($model->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
					if(count($pesan) > 0)
					{
						throw new Exception(implode("<br>", $pesan));
					}
				}else{
					$model->save();
					$transaction->commit();
					Yii::app()->user->setFlash('success', "Proses pengiriman berhasil");
				}

			}catch(Exception $exc){
				$transaction->rollback();
				Yii::app()->user->setFlash('error',"Data gagal disimpan ". MyExceptionMessage::getWarning($exc,true));
			}
			$this->redirect(array('kirimFarmasi','pendaftaran_id'=>$pendaftaran_id));
		}

		$this->render('kirim_farmasi',array(
			'model'=>$model,
			'pendaftaran'=>$pendaftaran,
			'pasienpulangT'=>$pasienpulangT,
			'pasienM'=>$pasienM
		));
	}

	public function actionPasienPulang($idPendaftaran = null, $dialog = false)
	{
		if($dialog) $this->layout='//layouts/frameDialog';

		$criteria = new CDbCriteria;
		$criteria->compare("pendaftaran_id", $idPendaftaran);
		$pendaftaranT = PendaftaranT::model()->find($criteria);
		$pasienM = PasienM::model()->findByPk($pendaftaranT->pasien_id);

		$ruangan = array();
		if($pendaftaranT->statusfarmasi == false){
			$ruangan[] = "+ Ruangan : APOTEK FARMASI > Status pasien belum dikonfirmasi";
		}
		$kirimunit = PasienkirimkeunitlainT::model()->findAllByAttributes(array(
			'pendaftaran_id'=>$idPendaftaran, 'pasienmasukpenunjang_id'=>null
		));
		if(count($kirimunit)>0){
			foreach($kirimunit as $key=>$kirimPasien){
				$ruangan[] = "+ Ruangan : ". $kirimPasien->ruangan->ruangan_nama . " Tgl Rujuk Pasien : ".$kirimPasien->tgl_kirimpasien;
			}
		}

		if(count($ruangan) > 0){
			Yii::app()->user->setFlash('error',"Pasien Belum Ditindak Lanjut Di : <br> " . implode("<br>", $ruangan));
		}

		if(is_null($pendaftaranT->pasienpulang_id))
		{
			$modelPulang = new PasienpulangT;
			$modelPulang->pendaftaran_id = $pendaftaranT->pendaftaran_id;
			$modelPulang->pasien_id = $pendaftaranT->pasien_id;
			$modelPulang->carakeluar = "DIPULANGKAN";
			$modelPulang->kondisipulang = "SEMBUH";
			$modelPulang->ruanganakhir_id = $pendaftaranT->ruangan_id;
		}else{
			$pasienpulangT = PasienpulangT::model()->findByPK($pendaftaranT->pasienpulang_id);
			$modelPulang = $pasienpulangT;
			Yii::app()->user->setFlash('danger', "Pasien sudah dipulangkan");
		}

		$format = new CustomFormat();
		$date1 = $format->formatDateTimeMediumForDB($pendaftaranT->tgl_pendaftaran);
		$date2 = date('Y-m-d H:i:s');
		$diff = abs(strtotime($date2) - strtotime($date1));
		$hours   = floor(($diff)/3600);
		$modelPulang->lamarawat = $hours;

		$modelPulang->satuanlamarawat = "Jam";
		$modelPulang->keterangankeluar = "SEMBUH";
		$modelPulang->tglpasienpulang = date('d M Y H:i:s');

		if(isset($_POST["PasienpulangT"]))
		{
			$pesan = array();
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$modelPulang->attributes = $_POST["PasienpulangT"];
				if(!$modelPulang->save())
				{
					foreach($modelPulang->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}

				$_pendaftaran = PendaftaranT::model()->updateByPk($pendaftaranT->pendaftaran_id, array(
					"pasienpulang_id" => $modelPulang->pasienpulang_id,
					"statusperiksa" => "SUDAH PULANG"
				));
				if(!$_pendaftaran)
				{
					foreach($_pendaftaran->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}

/*
				$pendaftaran = PendaftaranT::model()->findByPK($pendaftaranT->pendaftaran_id);
				$pendaftaran->pasienpulang_id = $modelPulang->pasienpulang_id;
				$pendaftaran->statusperiksa = "SUDAH PULANG";
				if(!$pendaftaran->save())
				{
					foreach($pendaftaran->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}
*/
				if(count($pesan) > 0)
				{
					throw new Exception(implode("<br>", $pesan));
				}

				$transaction->commit();
				Yii::app()->user->setFlash('success', "Proses pemulangan berhasil");
				$this->redirect(array('pasienPulang','idPendaftaran'=>$idPendaftaran, 'dialog'=>true));

			}catch(Exception $exc){
				$transaction->rollback();
				Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
			}
		}



		$this->render('formPasienPulang', array(
			"ruangan"=>$ruangan,
			"modelPulang"=>$modelPulang,
			"pasienM"=>$pasienM,
			"pendaftaranT"=>$pendaftaranT
		));
	}

	public function actionBatalPulang($pendaftaran_id = null)
	{
		$this->layout='//layouts/frameDialog';
		if(is_null($pendaftaran_id))
		{
			echo("<h4>Alamat pemohon tidak valid</h4>");
			Yii::app()->end();
		}

		$pasienbatalpulangT = new PasienbatalpulangT;
		$pasienbatalpulangT->tglpembatalan = date('d M Y H:i:s');
		$pendaftaran = PendaftaranT::model()->findByPK($pendaftaran_id);
		$pasienpulangT = PasienpulangT::model()->findByPK($pendaftaran->pasienpulang_id);
		$pasienM = PasienM::model()->findByPk($pendaftaran->pasien_id);

		if(isset($_POST["PasienbatalpulangT"]))
		{
			$pesan = array();
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$pasienbatalpulangT->attributes = $_POST["PasienbatalpulangT"];
				$loginpemakai = LoginpemakaiK::model()->findByAttributes(
					array(
						'nama_pemakai' => $pasienbatalpulangT->namauser_otorisasi,
						'loginpemakai_aktif' =>TRUE,
						'katakunci_pemakai' => LoginpemakaiK::encrypt($pasienbatalpulangT->password)
					)
				);

				if(!$loginpemakai)
				{
					$pesan[] = "Previliges : Otorisasi gagal";
				}

				$pasienbatalpulangT->iduser_otorisasi = $loginpemakai->pegawai_id;
				if(!$pasienbatalpulangT->save())
				{
					foreach($pasienbatalpulangT->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}

				$pasienpulangT->pasienbatalpulang_id = $pasienbatalpulangT->pasienbatalpulang_id;
				if(!$pasienpulangT->save())
				{
					foreach($pasienpulangT->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}
				
				$_pendaftaran = PendaftaranT::model()->updateByPk($pendaftaran->pendaftaran_id, array(
					"statusfarmasi" => false,
					"pasienpulang_id" => null,
					"statusperiksa" => "ANTRIAN"
				));
				if(!$_pendaftaran)
				{
					foreach($_pendaftaran->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}
				
				/*
				$pendaftaran->pasienpulang_id = null;
				$pendaftaran->statusperiksa = "ANTRIAN";

				if(!$pendaftaran->update())
				{
					foreach($pendaftaran->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}
				*/

				if(count($pesan) > 0)
				{
					throw new Exception(implode("<br>", $pesan));
				}

				$transaction->commit();
				Yii::app()->user->setFlash('success', "Proses pembatalan berhasil");
				$this->redirect(array('batalPulang','pendaftaran_id'=>$pendaftaran_id));

			}catch(Exception $exc){
				$transaction->rollback();
				Yii::app()->user->setFlash('error',"Data gagal disimpan ". MyExceptionMessage::getWarning($exc,true));
			}
		}

		$this->render('formBatalPulang', array(
			"pasienM"=>$pasienM,
			"pendaftaran"=>$pendaftaran,
			"pasienbatalpulangT"=>$pasienbatalpulangT
		));
	}
	
	public function actionBatalRanap(){
        $return = array();
        $return["status"] = "NO";
        $return["pesan"]  = "Pembatalkan tidak bisa dilakukan";

        if(Yii::app()->request->isAjaxRequest)
        {
            if(!empty($_POST["pendaftaran_id"])){
                $idPendaftaran = $_POST["pendaftaran_id"];
                $pendaftaran = PendaftaranT::model()->updateByPk($idPendaftaran, array(
                    'pasienpulang_id'=>null,
                    'statusperiksa'=>Params::statusPeriksa(2)
                ), "pasienadmisi_id IS NULL");

                if ($pendaftaran){
                    $return["status"] = "OK";
                    $return["pesan"]  = "Pembatalkan berhasil dilakukan";
                }
            }
        }
        echo CJSON::encode($return);
    }
	
}
