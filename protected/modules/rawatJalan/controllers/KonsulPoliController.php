<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class KonsulPoliController extends SBaseController
{
        protected $pathView = 'rawatJalan.views.konsulPoli.';
	public function actionIndex($idPendaftaran)
	{
            $insert_notifikasi = new MyFunction();
            $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $karcisTindakan = DaftartindakanM::model()->findAllByAttributes(array('daftartindakan_karcis'=>true));
            
            $modKonsul = new RJKonsulPoliT;
            $modelPendaftaran = new RJPendaftaranT;
            $modKonsul->pasien_id = $modPendaftaran->pasien_id;
            $modKonsul->pendaftaran_id = $idPendaftaran;
            $modKonsul->pegawai_id = $modPendaftaran->pegawai_id;
            $modKonsul->statusperiksa = Params::statusPeriksa(1);
            $modKonsul->asalpoliklinikkonsul_id = Yii::app()->user->getState('ruangan_id');
            
            if(isset($_POST['RJKonsulPoliT'])) {
                $modKonsul->attributes = $_POST['RJKonsulPoliT'];
                $modelPendaftaran->pasienpulang_id = $modPendaftaran->pasienpulang_id;
                $modelPendaftaran->pasienbatalperiksa_id = $modPendaftaran->pasienbatalperiksa_id;
                if(empty($modelPendaftaran->penanggungjawab_id)){
                   $penanggungjawab = 1;
                }else{
                    $penanggungjawab = $modPendaftaran->penanggungjawab_id;
                }
//                $modelPendaftaran->penanggungjawab_id = $penanggungjawab;
//                $modelPendaftaran->penjamin_id = $modPendaftaran->penjamin_id;
//                $modelPendaftaran->shift_id = $modPendaftaran->shift_id;                
//                $modelPendaftaran->pasien_id = $modPendaftaran->pasien_id;                
//                $modelPendaftaran->persalinan_id = $modPendaftaran->persalinan_id;                
//                $modelPendaftaran->pegawai_id = $_POST['RJKonsulPoliT']['pegawai_id'];
//                $modelPendaftaran->instalasi_id = Yii::app()->user->getState('instalasi_id');                
//                $modelPendaftaran->caramasuk_id = $modPendaftaran->caramasuk_id;                
//                $modelPendaftaran->pengirimanrm_id = $modPendaftaran->pengirimanrm_id;
//                $modelPendaftaran->peminjamanrm_id = $modPendaftaran->peminjamanrm_id;                
//                $modelPendaftaran->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;                
//                $modelPendaftaran->pembayaranpelayanan_id = $modPendaftaran->pembayaranpelayanan_id;                
//                $modelPendaftaran->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;                
//                $modelPendaftaran->carabayar_id = $modPendaftaran->carabayar_id;                
//                $modelPendaftaran->pasienadmisi_id = $modPendaftaran->pasienadmisi_id;
//                $modelPendaftaran->kelompokumur_id = $modPendaftaran->kelompokumur_id;                               
//                $modelPendaftaran->golonganumur_id = $modPendaftaran->golonganumur_id;
//                $modelPendaftaran->rujukan_id = $modPendaftaran->rujukan_id;              
//                $modelPendaftaran->antrian_id = $modPendaftaran->antrian_id;                
//                $modelPendaftaran->karcis_id = $modPendaftaran->karcis_id;
//                $modelPendaftaran->ruangan_id = $_POST['RJKonsulPoliT']['ruangan_id'];                
//                $modelPendaftaran->no_pendaftaran = $modPendaftaran->no_pendaftaran;
//                $modelPendaftaran->statusperiksa = Params::statusPeriksa(1);              
//                $modelPendaftaran->tgl_pendaftaran = $modPendaftaran->tgl_pendaftaran;                
//                $modelPendaftaran->no_urutantri = $modPendaftaran->no_urutantri;                
//                $modelPendaftaran->transportasi = $modPendaftaran->transportasi;                
//                $modelPendaftaran->keadaanmasuk = $modPendaftaran->keadaanmasuk;
//                $modelPendaftaran->statusperiksa = $modPendaftaran->statusperiksa;
//                $modelPendaftaran->statuspasien = $modPendaftaran->statuspasien;                
//                $modelPendaftaran->kunjungan = $modPendaftaran->kunjungan;                
//                $modelPendaftaran->alihstatus = $modPendaftaran->alihstatus;                
//                $modelPendaftaran->byphone = $modPendaftaran->byphone;
//                $modelPendaftaran->kunjunganrumah = $modPendaftaran->kunjunganrumah;                
//                $modelPendaftaran->statusmasuk = $modPendaftaran->statusmasuk;                
//                $modelPendaftaran->umur = $modPendaftaran->umur;                
//                $modelPendaftaran->no_asuransi = $modPendaftaran->no_asuransi;                
//                $modelPendaftaran->namapemilik_asuransi = $modPendaftaran->namapemilik_asuransi;                
//                $modelPendaftaran->nopokokperusahaan = $modPendaftaran->nopokokperusahaan;                
//                $modelPendaftaran->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
//                $modelPendaftaran->namaperusahaan = $modPendaftaran->namaperusahaan;                
//                $modelPendaftaran->tglselesaiperiksa = $modPendaftaran->tglselesaiperiksa;                
//                $modelPendaftaran->create_time = $modPendaftaran->create_time;
//                $modelPendaftaran->update_time = $modPendaftaran->update_time;                
//                $modelPendaftaran->create_loginpemakai_id = $modPendaftaran->create_loginpemakai_id;
//                $modelPendaftaran->update_loginpemakai_id = $modPendaftaran->update_loginpemakai_id;
//                $modelPendaftaran->create_ruangan = $modPendaftaran->create_ruangan;
//                $modelPendaftaran->nopendaftaran_aktif = $modPendaftaran->nopendaftaran_aktif; 
//                $modelPendaftaran->keterangan_reg = $modPendaftaran->keterangan_reg;
                if($modKonsul->validate()){
                    if($modKonsul->save()){
                        $updateStatusPeriksa=PendaftaranT::model()->updateByPk($idPendaftaran,array('statusperiksa'=>Params::statusPeriksa(2)));
                        
                        PendaftaranT::model()->updateByPk($idPendaftaran,
                            array(
                                'pembayaranpelayanan_id'=>null
                            )
                        );
                        
                        $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                        $params['create_time'] = date( 'Y-m-d H:i:s');
                        $params['create_loginpemakai_id'] = Yii::app()->user->id;
                        $params['instalasi_id'] = Yii::app()->user->getState('instalasi_id');
                        $params['modul_id'] = 5;
                        $ruangan = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'));
                        $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $modPendaftaran->no_pendaftaran . '-' . $modPasien->nama_pasien . '-' . $ruangan->ruangan_nama;
                        $params['create_ruangan'] = $modKonsul->ruangan_id;
                        $params['judulnotifikasi'] = 'Rujukan Rawat Jalan';                        
                        $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                        
//                        echo '<pre>';
//                        echo print_r($modelPendaftaran->attributes);
//                        echo print_r($modelPendaftaran->getErrors());
//                        exit();
//                        $modelPendaftaran->save();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->refresh();
                    }
                }
            }
            
            $modRiwayatKonsul = RJKonsulPoliT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
		
            $this->render($this->pathView.'index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modKonsul'=>$modKonsul,
                                        'karcisTindakan'=>$karcisTindakan,
                                        'modRiwayatKonsul'=>$modRiwayatKonsul,
                                        'modelPendaftaran'=>$modelPendaftaran,));
	}
        
        public function actionAjaxDetailKonsul()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idKonsulAntarPoli = $_POST['idKonsulAntarPoli'];
            $modKonsulPoli = RJKonsulPoliT::model()->findByPk($idKonsulAntarPoli);
            $data['result'] = $this->renderPartial($this->pathView.'_viewKonsulPoli', array('modKonsul'=>$modKonsulPoli), true);

            echo json_encode($data);
             Yii::app()->end();
            }
        }
        
        public function actionAjaxBatalKonsul()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idKonsulAntarPoli = $_POST['idKonsulAntarPoli'];
            $idPendaftaran = $_POST['idPendaftaran'];
            
            RJKonsulPoliT::model()->deleteByPk($idKonsulAntarPoli);
            $modRiwayatKonsul = RJKonsulPoliT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            
            $data['result'] = $this->renderPartial($this->pathView.'_listKonsulPoli', array('modRiwayatKonsul'=>$modRiwayatKonsul), true);

            echo json_encode($data);
             Yii::app()->end();
            }
        }
        
        public function actionPrint()
        {
            $modKonsul = new RJKonsulPoliT;
             $idPendaftaran = $_GET['id'];
             $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
             $modKonsulPoli = RJKonsulPoliT::model()->findByPk($idKonsulAntarPoli);
             $modRiwayatKonsul = RJKonsulPoliT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
//             $karcisTindakan = DaftartindakanM::model()->findAllByAttributes(array('daftartindakan_karcis'=>true));
            
            $judulLaporan='Konsultasi Gizi';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('modPendaftaran'=>$modPendaftaran,'modRiwayatKonsul'=>$modRiwayatKonsul,'modKonsulPoli'=>$modKonsulPoli,'modKonsul'=>$modKonsul,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('modPendaftaran'=>$modPendaftaran,'modRiwayatKonsul'=>$modRiwayatKonsul,'modKonsulPoli'=>$modKonsulPoli,'modKonsul'=>$modKonsul,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('modPendaftaran'=>$modPendaftaran,'modRiwayatKonsul'=>$modRiwayatKonsul,'modKonsulPoli'=>$modKonsulPoli,'modKonsul'=>$modKonsul,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}