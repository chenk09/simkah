<?php
class InformasiRJController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        protected $pathView = 'rawatJalan.views.informasi.';

        public function actionInformasiBatalPeriksa()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new RJInfopasienbatalperiksaV('search');
                $model->tglAwal = date('d M Y 00:00:00');
                $model->tglAkhir = date('d M Y 23:59:59');
		 if (isset($_GET['RJInfopasienbatalperiksaV'])) {
                    $format = new CustomFormat;
                    $model->attributes = $_GET['RJInfopasienbatalperiksaV'];
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJInfopasienbatalperiksaV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJInfopasienbatalperiksaV']['tglAkhir']);
                }
		$this->render($this->pathView.'batalPeriksaPasien/index',array(
			'model'=>$model,
		));
	}

}
