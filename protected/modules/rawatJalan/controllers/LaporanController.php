<?php

class LaporanController extends SBaseController {

    public $pathView = 'rawatJalan.views.laporan.';
    public function actionLaporanSensusHarian() {
        $model = new RJLaporansensusharian('search');
        $model->tglAwal = date('d M Y 00:00:00'                                                                           );
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['RJLaporansensusharian'])) {
            $model->attributes = $_GET['RJLaporansensusharian'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporansensusharian']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporansensusharian']['tglAkhir']);
        }

        $this->render('sensus/adminSensus', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanSensusHarian() {
        $model = new RJLaporansensusharian('search');
        $judulLaporan = 'Laporan Sensus Harian Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RJLaporansensusharian'])) {
            $model->attributes = $_REQUEST['RJLaporansensusharian'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporansensusharian']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporansensusharian']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'sensus/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikSensusHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporansensusharian('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['RJLaporansensusharian'])) {
            $model->attributes = $_GET['RJLaporansensusharian'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporansensusharian']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporansensusharian']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanKunjungan() {
        $model = new RJInfokunjunganrjV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RJInfokunjunganrjV'])) {
            $model->attributes = $_GET['RJInfokunjunganrjV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJInfokunjunganrjV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJInfokunjunganrjV']['tglAkhir']);
        }
        $this->render('kunjungan/adminKunjungan', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjungan() {
        $model = new RJInfokunjunganrjV('search');
        $judulLaporan = 'Laporan Info Kunjungan Pasien Rawat Jalan';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Info Kunjungan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RJInfokunjunganrjV'])) {
            $model->attributes = $_REQUEST['RJInfokunjunganrjV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjungan/_printKunjungan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjungan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJInfokunjunganrjV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Info Kunjungan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJInfokunjunganrjV'])) {
            $model->attributes = $_GET['RJInfokunjunganrjV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJInfokunjunganrjV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJInfokunjunganrjV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBukuRegister() {
        $model = new RJBukuregisterpasien('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RJBukuregisterpasien'])) {
            $model->attributes = $_GET['RJBukuregisterpasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJBukuregisterpasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJBukuregisterpasien']['tglAkhir']);
        }

        $this->render('bukuRegister/adminBukuRegister', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanBukuRegister() {
        $model = new RJBukuregisterpasien('search');
        $judulLaporan = 'Laporan Buku Register Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RJBukuregisterpasien'])) {
            $model->attributes = $_REQUEST['RJBukuregisterpasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJBukuregisterpasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJBukuregisterpasien']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuRegister/_printBukuRegister';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJBukuregisterpasien('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Rawat Jalan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJBukuregisterpasien'])) {
            $model->attributes = $_GET['RJBukuregisterpasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJBukuregisterpasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJBukuregisterpasien']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporan10BesarPenyakit() {
        $model = new RJLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->jumlahTampil = 10;

        if (isset($_GET['RJLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['RJLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporan10besarpenyakit']['tglAkhir']);
        }

        $this->render('10Besar/admin10BesarPenyakit', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporan10BesarPenyakit() {
        $model = new RJLaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RJLaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['RJLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '10Besar/_print10Besar';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['RJLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporan10besarpenyakit']['tglAkhir']);
        }
               
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanCaraMasukPasien() {
        $model = new RJLaporancaramasukpasienrj('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $asalrujukan = CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'),'asalrujukan_id','asalrujukan_id');
        $model->asalrujukan_id = $asalrujukan;
        $model->is_rujukan = 'non_rujukan';
        if (isset($_GET['RJLaporancaramasukpasienrj']))
        {
            $model->attributes = $_GET['RJLaporancaramasukpasienrj'];
            $model->is_rujukan = $_GET['RJLaporancaramasukpasienrj']['is_rujukan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporancaramasukpasienrj']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporancaramasukpasienrj']['tglAkhir']);
        }

        $this->render('caraMasuk/adminCaraMasukPasien', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanCaraMasukPasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporancaramasukpasienrj('search');
        $judulLaporan = 'Laporan Cara Masuk Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_REQUEST['type'];
        $asalrujukan = CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'),'asalrujukan_id','asalrujukan_id');
        $model->asalrujukan_id = $asalrujukan;
        $model->is_rujukan = 'non_rujukan';
        if (isset($_REQUEST['RJLaporancaramasukpasienrj'])) {
            $model->attributes = $_REQUEST['RJLaporancaramasukpasienrj'];
            $model->is_rujukan = $_GET['RJLaporancaramasukpasienrj']['is_rujukan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporancaramasukpasienrj']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporancaramasukpasienrj']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'caraMasuk/_printCaraMasuk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanCaraMasukPasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporancaramasukpasienrj('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJLaporancaramasukpasienrj'])) {
            $model->attributes = $_GET['RJLaporancaramasukpasienrj'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporancaramasukpasienrj']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporancaramasukpasienrj']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanTindakLanjut() {
        $model = new RJLaporantindaklanjutrj('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $temp = array();
        foreach (CaraKeluar::items() as $i=>$data){
            $temp[] = strtoupper($data);
        }
        $model->carakeluar = $temp;
        
        if (isset($_GET['RJLaporantindaklanjutrj'])) {
            $model->attributes = $_GET['RJLaporantindaklanjutrj'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporantindaklanjutrj']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporantindaklanjutrj']['tglAkhir']);
        }

        $this->render('tindakLanjut/adminTindakLanjut', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanTindakLanjut() {
        $model = new RJLaporantindaklanjutrj('search');
        $judulLaporan = 'Laporan Tindak Lanjut Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Tindak Lanjut Pasien';
        $data['type'] = $_REQUEST['type'];
        
         if (isset($_GET['RJLaporantindaklanjutrj'])) {
            $model->attributes = $_GET['RJLaporantindaklanjutrj'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporantindaklanjutrj']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporantindaklanjutrj']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'tindakLanjut/_printTindakLanjut';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanTindakLanjut() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporantindaklanjutrj('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik 
        $data['title'] = 'Grafik Laporan Tindak Lanjut Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJLaporantindaklanjutrj'])) {
            $model->attributes = $_GET['RJLaporantindaklanjutrj'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporantindaklanjutrj']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporantindaklanjutrj']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanKonsulAntarPoli() {
        $model = new RJLaporankonsulantarpoli('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $ruanganrawatjalan = CHtml::listData(RuanganrawatjalanV::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_id');
        $model->ruangantujuan_id = $ruanganrawatjalan;
        if (isset($_GET['RJLaporankonsulantarpoli'])) {
            $model->attributes = $_GET['RJLaporankonsulantarpoli'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporankonsulantarpoli']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporankonsulantarpoli']['tglAkhir']);
        }

        $this->render('konsulPoli/adminKonsulAntarPoli', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKonsulAntarPoli() {
        $model = new RJLaporankonsulantarpoli('search');
        $judulLaporan = 'Laporan Konsul Antar Poli Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Konsul Antar Poli';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RJLaporankonsulantarpoli'])) {
            $model->attributes = $_REQUEST['RJLaporankonsulantarpoli'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporankonsulantarpoli']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporankonsulantarpoli']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'konsulPoli/_printKonsulPoli';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanKonsulAntarPoli() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporankonsulantarpoli('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Konsul Antar Poli';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJLaporankonsulantarpoli'])) {
            $model->attributes = $_GET['RJLaporankonsulantarpoli'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporankonsulantarpoli']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporankonsulantarpoli']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanKepenunjang() {
        $model = new RJLaporankepenunjangrj('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');        
        $kepenunjang = CHtml::listData(RuanganpenunjangV::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_id');
        $model->ruanganpenunj_id = $kepenunjang;
        if (isset($_GET['RJLaporankepenunjangrj'])) {
            $model->attributes = $_GET['RJLaporankepenunjangrj'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporankepenunjangrj']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporankepenunjangrj']['tglAkhir']);
        }

        $this->render('kepenunjang/adminKepenunjang', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKepenunjang() {
        $model = new RJLaporankepenunjangrj('search');
        $judulLaporan = 'Laporan Kepenunjang Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kepenunjang';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RJLaporankepenunjangrj'])) {
            $model->attributes = $_REQUEST['RJLaporankepenunjangrj'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporankepenunjangrj']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporankepenunjangrj']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kepenunjang/_printKepenunjang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanKepenunjang() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporankepenunjangrj('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kepenunjang';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJLaporankepenunjangrj'])) {
            $model->attributes = $_GET['RJLaporankepenunjangrj'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporankepenunjangrj']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporankepenunjangrj']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBiayaPelayanan() {
        $model = new RJLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE'),'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['RJLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['RJLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanbiayapelayanan']['tglAkhir']);
        }

        $this->render('biayaPelayanan/adminBiayaPelayanan', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanBiayaPelayanan() {
        $model = new RJLaporanbiayapelayanan('search');
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE'),'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        $judulLaporan = 'Laporan Biaya Pelayanan Rawat Jalan';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Biaya Pelayanan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RJLaporanbiayapelayanan'])) {
            $model->attributes = $_REQUEST['RJLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanbiayapelayanan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'biayaPelayanan/_printBiayaPelayanan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanBiayaPelayanan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Biaya Pelayanan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['RJLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanbiayapelayanan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionLaporanPendapatanRuangan() {
        $model = new RJLaporanpendapatanruangan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['RJLaporanpendapatanruangan'])) {
            $model->attributes = $_GET['RJLaporanpendapatanruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanpendapatanruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanpendapatanruangan']['tglAkhir']);
        }

        $this->render($this->pathView.'pendapatanRuangan/adminPendapatanRuangan', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanPendapatanRuangan() {
        $model = new RJLaporanpendapatanruangan('search');
        $judulLaporan = 'Laporan Grafik Pendapatan Ruangan Rawat Jalan';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RJLaporanpendapatanruangan'])) {
            $model->attributes = $_REQUEST['RJLaporanpendapatanruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanpendapatanruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanpendapatanruangan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathView.'pendapatanRuangan/_printPendapatanRuangan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPendapatanRuangan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporanpendapatanruangan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJLaporanpendapatanruangan'])) {
            $model->attributes = $_GET['RJLaporanpendapatanruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanpendapatanruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanpendapatanruangan']['tglAkhir']);
        }
                
        $this->render($this->pathView.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionLaporanJasaInstalasi() {
        $model = new RJLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $model->tindakansudahbayar_id = Params::statusBayar();
        if (isset($_GET['RJLaporanjasainstalasi'])) {
            $model->attributes = $_GET['RJLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanjasainstalasi']['tglAkhir']);
        }

        $this->render($this->pathView.'jasaInstalasi/adminJasaInstalasi', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanJasaInstalasi() {
        $model = new RJLaporanjasainstalasi('search');
        $judulLaporan = 'Laporan Jasa Instalasi Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['RJLaporanjasainstalasi'])) {
            $model->attributes = $_REQUEST['RJLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanjasainstalasi']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathView.'jasaInstalasi/_printJasaInstalasi';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJasaInstalasi() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJLaporanjasainstalasi'])) {
            $model->attributes = $_GET['RJLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanjasainstalasi']['tglAkhir']);
        }
                
        $this->render($this->pathView.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
             $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    public function actionLaporanPemakaiObatAlkes()
    {
        $model = new RJLaporanpemakaiobatalkesV;
        $model->unsetAttributes();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $jenisObat =CHtml::listData($model->getJenisobatalkesItems(),'jenisobatalkes_id','jenisobatalkes_id');
        $model->jenisobatalkes_id = $jenisObat;
        if(isset($_GET['RJLaporanpemakaiobatalkesV']))
        {
            $model->attributes = $_GET['RJLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        $this->render('pemakaiObatAlkes/adminPemakaiObatAlkes',array('model'=>$model));
    }

    public function actionPrintLaporanPemakaiObatAlkes() {
        $model = new RJLaporanpemakaiobatalkesV('search');
        $jenisObat =CHtml::listData($model->getJenisobatalkesItems(),'jenisobatalkes_id','jenisobatalkes_id');
        $model->jenisobatalkes_id = $jenisObat;
        $judulLaporan = 'Laporan Info Pemakai Obat Alkes Rawat Jalan';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RJLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_REQUEST['RJLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemakaiObatAlkes/_printPemakaiObatAlkes';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    

    public function actionFrameGrafikLaporanPemakaiObatAlkes() {
        $this->layout = '//layouts/frameDialog';
        $model = new RJLaporanpemakaiobatalkesV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RJLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['RJLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RJLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RJLaporanpemakaiobatalkesV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function parserTanggal($tgl){
        $tgl = explode(' ', $tgl);
        $result = array();
        foreach ($tgl as $row){
            if (!empty($row)){
                $result[] = $row;
            }
        }
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }

}