<?php
Yii::import('application.controllers.ResepturController');
class ResepturRalanController extends ResepturController
{
	public function actionIndex($idPendaftaran)
	{
        $modPendaftaran = RJPendaftaranT::model()->findByPk($idPendaftaran);
        $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
        $v_ruangan_id = Yii::app()->user->getState('ruangan_id');

        $vRuangans = RuanganM::model()->findAllByAttributes(array(
            'instalasi_id'=>9
        ), 'ruangan_aktif = true');
        $vDataRuangan = CHtml::listData($vRuangans, 'ruangan_id', 'ruangan_nama');

        $vMapRuanganFarmasi = MapRuanganFarmasi::model()->findAllByAttributes(array(
            'ruangan_asal_id'=>$v_ruangan_id
        ), 'endda >= CURRENT_DATE');
        $vMapRuangan = CHtml::listData($vMapRuanganFarmasi, 'ruangan_target_id', 'ruangan_target_id');

        foreach ($vDataRuangan as $index => $item)
        {
            if(!isset($vMapRuangan[$index]))
            {
                unset($vDataRuangan[$index]);
            }
        }
        ksort($vDataRuangan);

        $modDiagnosa = new RJDiagnosaM;
        $modReseptur = new RJResepturT;
        $instalasi_id = Yii::app()->user->getState('instalasi_id');
        $modReseptur->noresep = MyGenerator::noReseptur($instalasi_id);
        $modReseptur->pegawai_id = $modPendaftaran->pegawai_id;
        $modReseptur->ruanganreseptur_id = Yii::app()->user->getState('ruangan_id');
        $modReseptur->pasien_id = $modPendaftaran->pasien_id;
        $modReseptur->pendaftaran_id = $modPendaftaran->pendaftaran_id;

        $modMorbiditas = new RJPasienMorbiditasT();
        $golUmur = Logic::cekGolonganUmur($modPendaftaran->golonganumur_id);
        $modMorbiditas->pendaftaran_id = $modPendaftaran->pendaftaran_id;
        $modMorbiditas->pasien_id = $modPendaftaran->pasien_id;
        $modMorbiditas->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $modMorbiditas->kelompokumur_id = $modPasien->kelompokumur_id;
        $modMorbiditas->golonganumur_id = $modPendaftaran->golonganumur_id;
        $modMorbiditas->$golUmur = 1;
        $modMorbiditas->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
        $modMorbiditas->pegawai_id = $modPendaftaran->pegawai_id;
        $modMorbiditas->infeksinosokomial = '0';

        if(Yii::app()->request->isPostRequest && isset($_POST['RJResepturT']))
        {
            $this->layout = false;
            $vRest = $this->postingReseptur($modReseptur, $modMorbiditas, $_POST);
            echo json_encode($vRest);
            Yii::app()->end();
        }

        $this->render('index',array(
            'dataRuangan'=>$vDataRuangan,
            'modMorbiditas'=>$modMorbiditas,
            'modPendaftaran'=>$modPendaftaran,
            'modPasien'=>$modPasien,
            'modDiagnosa'=>$modDiagnosa,
            'modReseptur'=>$modReseptur
        ));
	}
}