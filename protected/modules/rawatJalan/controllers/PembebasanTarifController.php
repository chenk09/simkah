<?php

class PembebasanTarifController extends SBaseController
{
        public $successSavePembebasan = true;
        
	public function actionIndex()
	{
            $modPasien = new RJPasienM;
            $modPendaftaran = new RJPendaftaranT;
            $model = new RJPembebasantarifT;
            $model->tglpembebasan = date('d M Y H:i:s');
            
            if(isset($_POST[get_class($model)])){
                $model->attributes = $_POST[get_class($model)];
                $modPendaftaran = RJPendaftaranT::model()->findByPk($_POST[get_class($modPendaftaran)]['pendaftaran_id']);
                $modPasien = RJPasienM::model()->findByPk($_POST[get_class($modPendaftaran)]['pasien_id']);
                
                if(isset($_POST['pembebasan'])){
                    foreach($_POST['pembebasan'] as $tindkomponen_id=>$dataPembebasan){
                        if($tindkomponen_id==$dataPembebasan['tindkomponen_id']) {
                            //echo '<pre>'.print_r($data,1).'</pre>';
                            $transaction = Yii::app()->db->beginTransaction();
                            try {
                                $model = $this->savePembebasan($model, $dataPembebasan);
                                if($this->successSavePembebasan){
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', "Data Berhasil Disimpan ");
                                } else {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error', "Data Gagal Disimpan ");
                                }
                            } catch (Exception $exc) {
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                            }

                            
                        }
                    }
                }
            }
		
            $this->render('index', array('model'=>$model,
                                         'modPasien'=>$modPasien,
                                         'modPendaftaran'=>$modPendaftaran));
	}
        
        protected function savePembebasan($model,$dataPembebasan)
        {
            $format = new CustomFormat;
            $modPembebasan = new RJPembebasantarifT;
            $modPembebasan->attributes = $model->attributes;
            $modPembebasan->tglpembebasan = $format->formatDateTimeMediumForDB(trim($modPembebasan->tglpembebasan));
            $modPembebasan->tindakanpelayanan_id = $dataPembebasan['tindakanpelayanan_id'];
            $modPembebasan->komponentarif_id = $dataPembebasan['komponentarif_id'];
            $modPembebasan->jmlpembebasan = $dataPembebasan['tarif_tindakankomp'];
            $modPembebasan->loginpemakai_id = Yii::app()->user->id;
            if($modPembebasan->validate()){
                $modPembebasan->save();
                $tindakan = $this->saveTindakanPelayanan($dataPembebasan);
                if($tindakan){
                    $this->successSavePembebasan = $this->successSavePembebasan && true;
                }else{
                    $this->successSavePembebasan = false;
                }
            } else 
                $this->successSavePembebasan = false;
            
            return $modPembebasan;
        }
        
        protected function saveTindakanPelayanan($dataPembebasan)
        {
            $is_simpan = false;
            
            $attributes = array();
            if($dataPembebasan['komponentarif_id'] == Params::KOMPONENTARIF_ID_RS){
                $attributes['tarif_rsakomodasi'] = $dataPembebasan['tarif_tindakankomp'];
            }
            
            if($dataPembebasan['komponentarif_id'] == Params::KOMPONENTARIF_ID_MEDIS){
                $attributes['tarif_medis'] = $dataPembebasan['tarif_tindakankomp'];
            }

            if($dataPembebasan['komponentarif_id'] == Params::KOMPONENTARIF_ID_PARAMEDIS){
                $attributes['tarif_paramedis'] = $dataPembebasan['tarif_tindakankomp'];
            }
            
            if(count($attributes) > 0)
            {
                $total = 0;
                foreach($attributes as $key=>$val){
                    $total += $val;
                }
                
                $attributes['tarif_satuan'] = $total;
                $tindakan = RJTindakanPelayananT::model()->updateByPk($dataPembebasan['tindakanpelayanan_id'], $attributes);
                $attributes_komp = array(
                    'tarif_tindakankomp' => $dataPembebasan['tarif_tindakankomp']
                );
                TindakankomponenT::model()->updateByPk($dataPembebasan['tindkomponen_id'], $attributes_komp);
                if($tindakan)
                {
                    $is_simpan = true;   
                }
            }
            
            return $is_simpan;
//            RJTindakanPelayananT->updateByPk($pk, $attributes);
            /*
            tindakanpelayanan_t
            tindakankomponen_t*/
        }
}