<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ActionAjaxController extends SBaseController
{
        public function actionLoadTindakanKomponenPasien()
        {
            if(Yii::app()->request->isAjaxRequest) {
                $idPendaftaran = $_POST['idPendaftaran'];
                
                $tindakans = TindakanpelayananT::model()->with('daftartindakan')->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,));
                foreach($tindakans as $i=>$tindakan){
                    $returnVal[$tindakan->tindakanpelayanan_id]['daftartindakan_id'] = $tindakan->daftartindakan_id;
                    $returnVal[$tindakan->tindakanpelayanan_id]['daftartindakan_nama'] = $tindakan->daftartindakan->daftartindakan_nama;
                    $komponens = TindakankomponenT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id));
                    foreach($komponens as $j=>$komponen){
                        $tindKomponenId = $komponen->tindakankomponen_id;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['tindakankomponen_id'] = $tindKomponenId;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['komponentarif_id'] = $komponen->komponentarif_id;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['komponentarif_nama'] = $komponen->komponentarif->komponentarif_nama;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['tarif_kompsatuan'] = $komponen->tarif_kompsatuan;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['tarif_tindakankomp'] = $komponen->tarif_tindakankomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['tarifcyto_tindakankomp'] = $komponen->tarifcyto_tindakankomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['subsidiasuransikomp'] = $komponen->subsidiasuransikomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['subsidipemerintahkomp'] = $komponen->subsidipemerintahkomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['subsidirumahsakitkomp'] = $komponen->subsidirumahsakitkomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['iurbiayakomp'] = $komponen->iurbiayakomp;
                    }
                }
                
                $form = $this->renderPartial('_formPembebasanTarif', array('data'=>$returnVal), true);
                $returnVal['tabelPembebasanTarif'] = $form;
                
                echo CJSON::encode($returnVal);
            }
        }
        
         public function actionCekHakAkses()
        {
            if(!Yii::app()->user->checkAccess('Administrator')){
                //throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));
                $data['cekAkses'] = false;
            } else {
                $pendaftaran = PendaftaranT::model()->findByPk($_POST['idPendaftaran']);
                $data['pendaftaran'] = $pendaftaran->attributes;
                //echo 'punya hak akses';
                $pasienAdmisi = PasienadmisiT::model()->findByAttributes(array('pendaftaran_id'=>$pendaftaran->pendaftaran_id));
                $ruangan_m = RuanganM::model()->findByPk($pasienAdmisi->ruangan_id);
                $data['ruanganPasien'] =  $ruangan_m->ruangan_nama;
                $data['cekAkses'] = false;
                $data['userid'] = Yii::app()->user->id;
                $data['username'] = Yii::app()->user->name;
            }
            
            echo CJSON::encode($data);
            Yii::app()->end();
        }
        
        public function actiondataPasien()
        {
            $idPasien=$_POST['idPasien'];
            $idPendaftaran=$_POST['idPendaftaran'];
            $modPasien = RIPasienM::model()->findByPk($idPasien);
            $modPendaftaran = RIPendaftaranT::model()->findByPk($idPendaftaran);
            $form=$this->renderPartial('/_ringkasDataPasien', array('modPasien'=>$modPasien,
                                                                           'modPendaftaran'=>$modPendaftaran,
                                                                               ), true);
            
            $data['form']=$form;
                       echo CJSON::encode($data);

        }  

    public function actionLoadFormDiagnosis()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idDiagnosa = $_POST['idDiagnosa'];
            $idKelDiagnosa = $_POST['idKelDiagnosa'];
            $tglDiagnosa = $_POST['tglDiagnosa'];
            
            $modDiagnosaicdixM = DiagnosaicdixM::model()->findAll();
            $modSebabDiagnosa = SebabdiagnosaM::model()->findAll();
            $modDiagnosa = DiagnosaM::model()->findByPk($idDiagnosa);
            
            echo CJSON::encode(array(
                'status'=>'create_form', 
                'form'=>$this->renderPartial('/diagnosa/_formLoadDiagnosis', array('modDiagnosa'=>$modDiagnosa,
                'idKelDiagnosa'=>$idKelDiagnosa,
                'modDiagnosaicdixM'=>$modDiagnosaicdixM,
                'modSebabDiagnosa'=>$modSebabDiagnosa,
               'tglDiagnosa'=>$tglDiagnosa), true)));
            exit;               
        }
    }

    public function actionSaveDiagnosis()
    {    
        if (Yii::app()->request->isAjaxRequest)
        {
            $NoPendaftaran = $_POST['NoPendaftaran'];
            $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByAttributes(array('no_pendaftaran'=>$NoPendaftaran));
            
            $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $morbiditas = new RJPasienMorbiditasT;
            $morbiditas->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $morbiditas->pasien_id = $modPendaftaran->pasien_id;
            $morbiditas->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $morbiditas->kelompokumur_id = $modPasien->kelompokumur_id;
            $morbiditas->golonganumur_id = $modPendaftaran->golonganumur_id;
            $morbiditas->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $morbiditas->pegawai_id = $modPendaftaran->pegawai_id;
            $morbiditas->diagnosa_id = $_POST['idDiagnosa'];
            $morbiditas->kelompokdiagnosa_id = $_POST['kelompokDiagnosa'];
            $morbiditas->infeksinosokomial = '0';
            $morbiditas->tglmorbiditas = $_POST['tglDiagnosa'];

            $modMorbiditas = PasienmorbiditasT::model()->findByAttributes(array('pasien_id'=>$modPendaftaran->pasien_id,'diagnosa_id'=>$morbiditas->diagnosa_id));
            if(!empty($modMorbiditas))
                $morbiditas->kasusdiagnosa = 'KASUS LAMA';
            else 
                $morbiditas->kasusdiagnosa = 'KASUS BARU';

            $valid = $morbiditas->validate();
            if($valid){
                $morbiditas->save();
            }
            
        }
    }

    public function actionHapusDiagnosis()
    {    
        if (Yii::app()->request->isAjaxRequest)
        {
            $NoPendaftaran = $_POST['NoPendaftaran'];
            $idDiagnosa    = $_POST['idDiagnosa'];

            $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByAttributes(array('no_pendaftaran'=>$NoPendaftaran));

            PasienmorbiditasT::model()->deleteAllByAttributes(array('diagnosa_id'=>$idDiagnosa,'pendaftaran_id'=>$modPendaftaran->pendaftaran_id));
        }
    }
}

?>
