<?php

class RujukanKeluarController extends SBaseController
{
        protected $pathView = 'rawatJalan.views.rujukanKeluar.';
	public function actionIndex($idPendaftaran)
	{
            $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = RJPasienM::model()->findByPk($modPendaftaran->pasien_id);
            
            $modRujukanKeluar = new RJPasienDirujukKeluarT;
            $modRujukanKeluar->pendaftaran_id = $idPendaftaran;
            $modRujukanKeluar->pasien_id = $modPendaftaran->pasien_id;
            // $modRujukanKeluar->pegawai_id = $modPendaftaran->pegawai_id;
            $modRujukanKeluar->ruanganasal_id = Yii::app()->user->getState('ruangan_id');
//            $modRujukanKeluar->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modRujukanKeluar->tgldirujuk = date('d M Y H:i:s');
            $modRujukanKeluar->diagnosasementara_ruj = $modRujukanKeluar->getDiagnosaSementara($idPendaftaran);
            if(isset($_POST['RJPasienDirujukKeluarT'])) {
                $modRujukanKeluar->attributes = $_POST['RJPasienDirujukKeluarT'];
                if($modRujukanKeluar->validate())
				{
					$modRujukanKeluar->save();
					/*
					$updateStatusPeriksa = PendaftaranT::model()->updateByPk($idPendaftaran,array(
						'statusperiksa'=>Params::statusPeriksa(2)
					));
					*/
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    $this->refresh();
                }
            }
		
            $modRiwayatRujukanKeluar = RJPasienDirujukKeluarT::model()->with('rujukankeluar','pendaftaran')->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran),array('order'=>'t.create_time DESC'));
            
            $this->render($this->pathView.'index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modRujukanKeluar'=>$modRujukanKeluar,
                                        'modRiwayatRujukanKeluar'=>$modRiwayatRujukanKeluar));
	}
        
        public function actionAjaxDetailRujukanKeluar()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idPasienDirujuk = $_POST['idPasienDirujuk'];
            $modRujukanKeluar = RJPasienDirujukKeluarT::model()->findByPk($idPasienDirujuk);
            $data['result'] = $this->renderPartial($this->pathView.'_viewRujukanKeluar', array('modRujukanKeluar'=>$modRujukanKeluar), true);

            echo json_encode($data);
             Yii::app()->end();
            }
        }
        
        public function actionPrint()
        {
//            $modKonsul = new RJKonsulPoliT;
             $idPendaftaran = $_GET['id'];
             $modPendaftaran = RJPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
             $modRujukanKeluar = new RJPasienDirujukKeluarT;
             $modRiwayatRujukanKeluar = RJPasienDirujukKeluarT::model()->with('rujukankeluar','pendaftaran')->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran),array('order'=>'t.create_time DESC'));
//             $karcisTindakan = DaftartindakanM::model()->findAllByAttributes(array('daftartindakan_karcis'=>true));
            
            $judulLaporan='Rujukan Keluar';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('modPendaftaran'=>$modPendaftaran,'modRiwayatRujukanKeluar'=>$modRiwayatRujukanKeluar,'modRujukanKeluar'=>$modRujukanKeluar,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('modPendaftaran'=>$modPendaftaran,'modRiwayatRujukanKeluar'=>$modRiwayatRujukanKeluar,'modRujukanKeluar'=>$modRujukanKeluar,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('modPendaftaran'=>$modPendaftaran,'modRiwayatRujukanKeluar'=>$modRiwayatRujukanKeluar,'modRujukanKeluar'=>$modRujukanKeluar,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}