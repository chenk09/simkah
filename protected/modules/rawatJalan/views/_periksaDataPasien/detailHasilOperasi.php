<?php $this->widget('ext.bootstrap.widgets.BootGridView',array( 
    'id'=>'tindakan-operasi-t-grid', 
    'dataProvider'=>$detailHasil->searchDetailTindakan($modPendaftaran->pendaftaran_id), 
    //'filter'=>$model, 
        'template'=>"{pager}{summary}\n{items}", 
        'itemsCssClass'=>'table table-striped table-bordered table-condensed', 
    'columns'=>array( 
        array(
            'header'=>'No. Rencana Operasi',
            'type'=>'raw',
            'value'=>'empty($data->rencanaoperasi_id) ? "<center>-</center>" : $data->rencanaoperasi->norencanaoperasi',
        ),
        array(
            'header'=>'Tgl. Tindakan',
            'type'=>'raw',
            'value'=>'$data->tgl_tindakan',
        ),
        array(
            'header'=>'Tindakan',
            'type'=>'raw',
            'value'=>'$data->daftartindakan->daftartindakan_kode."-".$data->daftartindakan->daftartindakan_nama',
        ),
        array(
            'header'=>'Dokter Pemeriksa',
            'type'=>'raw',
            'value'=>'PegawaiM::model()->findByPk($data->dokterpemeriksa1_id)->NamaLengkap',
        ),
        array(
            'header'=>'Dokter Pelaksana',
            'type'=>'raw',
            'value'=>'empty($data->rencanaoperasi_id) ? "<center>-</center>" : PegawaiM::model()->findByPk($data->rencanaoperasi->dokterpelaksana1_id)->NamaLengkap',
        ),
        array(
            'header'=>'Status Operasi',
            'type'=>'raw',
            'value'=>'empty($data->rencanaoperasi_id) ? "<center>-</center>" : $data->rencanaoperasi->statusoperasi',
        ),
    ), 
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}', 
)); ?> 