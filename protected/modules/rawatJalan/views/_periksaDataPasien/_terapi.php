<?php
if (isset($caraPrint)){
    if($caraPrint=='EXCEL')
        {
            header('Content-Type: application/vnd.ms-excel');
              header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
              header('Cache-Control: max-age=0');     
        }
    echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan));     
}
?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array( 
    'id'=>'rjpenjualanresep-t-grid', 
    'dataProvider'=>$modDetailTerapi->searchDetailTerapi($modPendaftaran->pendaftaran_id), 
//    'filter'=>$modDetailTerapi, 
    'template'=>"{pager}{summary}\n{items}", 
    'itemsCssClass'=>'table table-striped table-bordered table-condensed', 
    'columns'=>array(
        array(
            'header'=>'Ruangan',
            'value'=>'$data->ruangan->ruangan_nama'
        ),
        'tglpenjualan',
        array(
            'header'=>'Obat Alkes',
            'type'=>'raw',
//            'value'=>'$this->grid->getOwner()->renderPartial(\'/_periksaDataPasien/_terapi_obatalkes_column\',array(\'data\'=>$data),true)',
            'value'=>'$this->grid->getOwner()->renderPartial(\'/_periksaDataPasien/_terapi_obatalkes_column\',array(\'data\'=>$data->getObatTerapi($data->penjualanresep_id)),true)',
        ),
    ), 
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}', 
)); ?> 