<?php
$this->breadcrumbs=array(
	'Konsul Gizi',
);

$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('rawatJalan.views._ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));

$this->renderPartial('/_tabulasi', array('modPendaftaran'=>$modPendaftaran));

?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjpasien-konsulGizi-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onSubmit'=>'return cekInput();'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($modKirimKeUnitLain); ?>

    <table class="table-condensed">
        <tr>
            <td width="50%">
                <div class="control-group ">
                    <?php echo CHtml::hiddenField('url',$this->createUrl('',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),array('readonly'=>TRUE));?>
                    <?php echo CHtml::hiddenField('berubah','',array('readonly'=>TRUE));?>
                    <?php echo $form->labelEx($modKirimKeUnitLain,'tgl_kirimpasien', array('class'=>'control-label')) ?>
                    <?php $modKirimKeUnitLain->tgl_kirimpasien = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modKirimKeUnitLain->tgl_kirimpasien, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modKirimKeUnitLain,
                                                    'attribute'=>'tgl_kirimpasien',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($modKirimKeUnitLain,'pegawai_id', CHtml::listData($modKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->dropDownListRow($modKirimKeUnitLain,'ahligizi', CHtml::listData($modKirimKeUnitLain->getAhliGiziItems(), 'pegawai_id', 'nama_pegawai'),
                                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->dropDownListRow($modKirimKeUnitLain,'ruangan_id', CHtml::listData($modKirimKeUnitLain->getRuanganGiziItems(), 'ruangan_id', 'ruangan_nama'),
                                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                                      'onchange'=>'loadFormTindakanGizi(this.value)')); ?>
                
            </td>
            <td width="50%">
                <?php echo $form->textAreaRow($modKirimKeUnitLain,'catatandokterpengirim',array('onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td width="50%">
                    <table id="tblFormPermintaanGizi" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Pilih</th>
                                <th>Pelayanan</th>
                                <!-- <th>Tarif</th> -->
                            </tr>
                        </thead>
                        <tbody id="bodyTblFormPermintaanGizi"></tbody>
                    </table> 
            </td>
            <td width="50%">
                <?php $this->renderPartial($this->pathView.'_listKirimKeUnitLain',array('modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain)) ?>
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
            <td></td>
        </tr>
    </table>
    
    <?php
                //FORM REKENING
                    $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                        array(
                            'form'=>$form,
                            'modRekenings'=>$modRekenings,
                        )
                    );
                ?>
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','id'=>'btn_simpan')); ?>
									<?php
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; ?>
									<?php 
           $content = $this->renderPartial('rawatJalan.views.tips.tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
        $urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print&id='.$modPendaftaran->pendaftaran_id);
        $ruanganGizi = Params::RUANGAN_ID_GIZI;
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}

JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
    </div>

<?php $this->endWidget(); ?>

    <?php 
$js = <<< JS
//==================================================Validasi===============================================
//*Jangan Lupa Untuk menambahkan hiddenField dengan id "berubah" di setiap form
//* hidden field dengan id "url"
//*Copas Saja hiddenfield di Line 36 dan 35
//* ubah juga id button simpannya jadi "btn_simpan"


function palidasiForm(obj)
{
    var berubah = $('#berubah').val();
    if(berubah=='Ya'){
       if(confirm('Apakah Anda Akan menyimpan Perubahan Yang Sudah Dilakukan?')){
           $('#url').val(obj);
           $('#btn_simpan').click();
       }
    }      
}
loadFormTindakanGizi(${ruanganGizi});
JS;
Yii::app()->clientScript->registerScript('js',$js,CClientScript::POS_READY);
?>   
    
<script type="text/javascript">
function loadFormTindakanGizi(idRuangan)
{
    var idKelasPelayanan = <?php echo $modPendaftaran->kelaspelayanan_id; ?>;
    $.post("<?php echo Yii::app()->createUrl('ActionAjax/loadFormTindakanGizi')?>", {idRuangan:idRuangan, idKelasPelayanan:idKelasPelayanan}, function(data){
        $('#bodyTblFormPermintaanGizi').html(data.form);
    }, "json");
}

function batalKirim(idPasienKirimKeUnitLain,idPendaftaran)
{
    if(confirm('Apakah anda akan membatalkan Konsultasi Gizi pasien?')){
        $.post('<?php echo $this->createUrl('ajaxBatalKirim') ?>', {idPasienKirimKeUnitLain: idPasienKirimKeUnitLain, idPendaftaran:idPendaftaran}, function(data){
            $('#tblListPemeriksaanRad').html(data.result);
        }, 'json');
    }
}
function cekInputan(){
    $('.inputFormTabel').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
function ceklist(obj){
    var idKelas = <?php echo $modPendaftaran->kelaspelayanan_id; ?>;
    if(idKelas ==''){
        idKelas = <?php echo Params::KELASPELAYANAN_ID_TANPA_KELAS; ?>;
    }


    var checked = [];
    $("#bodyTblFormPermintaanGizi input:checked").each(function ()
    {
        checked.push(parseInt($(this).parents('tr').find('input[name$="[idDaftarTindakan]"]').val()));
    });

    $("#tblInputRekening tbody").html('');

    
    for (i = 0; i< checked.length; ++i) {
        isiTabelRekening(checked[i],idKelas,obj);
    }

    $('.ceklis').each(function(){
        if ($(this).is(':checked')){
            $(this).val(1);
        }else{
            $(this).val(0);
        }
    });
}

function isiTabelRekening(idTindakan,idKelas,obj){
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/loadFormTindakanGiziKelas') ?>', {idTindakan: idTindakan, idKelas:idKelas}, function(data){
        // $('.ceklis').each(function(){
        //     if ($(this).is(':checked')){
                if(data.status != 'ada'){
                    $(obj).removeAttr('checked');
                    alert ('Tindakan Bukan Termasuk kelas Pelayanan Yang Digunakan');
                }else{
                    // $(this).val(1);
                    cekRekening(idTindakan,idKelas);
                }
        //     }else{
        //         $(this).val(0);
        //     }
        // });
    }, 'json'); 
}
function cekRekening(idTindakan,idKelas){
    getDataRekening(idTindakan,idKelas,1,"tm");  
}

function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}

function deleteTindakan(obj,idPasienMasukPenunjang)
{
    if(confirm('Apakah anda yakin akan menghapus tindakan?')){
        $.post('<?php echo $this->createUrl('ajaxDeleteTindakanPelayanan') ?>', {idPasienMasukPenunjang: idPasienMasukPenunjang}, function(data){
            if(data.success)
            {
                $(obj).parent().parent().detach();
                alert('Data berhasil dihapus !!');
            } else {
                alert('Data Gagal dihapus');
            }
        }, 'json');
    }
}
</script>