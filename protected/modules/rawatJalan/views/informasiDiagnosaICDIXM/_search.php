<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sadiagnosa-icdixm-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'diagnosaicdix_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'diagnosaicdix_kode',array('class'=>'span3','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'diagnosaicdix_nama',array('class'=>'span3','maxlength'=>50)); ?>

	<?php //cho $form->textFieldRow($model,'diagnosaicdix_namalainnya',array('class'=>'span5','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'diagnosatindakan_katakunci',array('class'=>'span5','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'diagnosaicdix_nourut',array('class'=>'span5')); ?>

	<?php //echo $form->checkBoxRow($model,'diagnosaicdix_aktif',array('checked'=>'diagnosaicdix_aktif')); ?>

	<div class="form-actions">
             <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                    array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
            ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.informasiDiagnosaICDIXM.'/index'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); 
             ?>
          <?php 
                $content = $this->renderPartial('../tips/informasi',array(),true);
                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
	</div>

<?php $this->endWidget(); ?>
