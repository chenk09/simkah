<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
 <style>

        #penjamin label.checkbox{
            width: 150px;
            display:inline-block;
        }

        .checkbox.inline{
            width: 63px;
            margin-left: 10px;
        }

    </style><legend class="rim">Berdasarkan Tanggal Pelayanaan</legend>
       <table width="200" style="margin-top:10px;">
  <tr>
    <td>
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <?php //echo CHtml::hiddenField('src', ''); ?>
                    <div class = 'control-label'>Tanggal Pelayanan</div>
                    <div class="controls">  
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAwal',
                            'mode' => 'datetime',
//                                          'maxDate'=>'d',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div> 
      </td>
    <td style="padding:0px 130px 0 0px;"> <?php echo CHtml::label('Sampai dengan', ' s/d', array('class' => 'control-label')) ?>
                    <div class="controls">  
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAkhir',
                            'mode' => 'datetime',
//                                         'maxdate'=>'d',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div> </td>
  </tr>
</table>  
          <table width="216" border="0">
  <tr>
    <td width="51" style="padding-left:10px;"> <fieldset>
					<legend class="rim">Berdasarkan Kelas Pelayanan </legend>
                                        <div style='margin-left:0px;'>
                                            <?php echo CHtml::checkBox('checkAllKelas',true, array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'class'=>'checkbox-column','onclick'=>'checkAll()','checked'=>'checked'))." Pilih Semua"; ?>
                                       </div>
                        <?php echo'<table>
                                                <tr>
                                                    <td>
                                                        <div id="kelasPelayanan">'.
                                                        $form->checkBoxList($model, 'kelaspelayanan_id', CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_nama'), array('value'=>'pengunjung', 'inline'=>true, 'empty' => '-- Pilih --')).'
                                                        </div>
                                                    </td>
                                                 </tr>
                                                 </table>'; ?>
      </fieldset>
      <fieldset>
            <legend class="rim">Berdasarkan Dokter </legend>
            <?php //echo $form->textFieldRow($model,'nama_pegawai',array()); ?>
          <div class="control-group">  
            <label class="control-label">Nama Dokter</label>
            <div class="controls">
              <?php $this->widget('MyJuiAutoComplete',array(
                'name'=>'RJLaporanpendapatanruangan_nama_pegawai',
                'value'=>$model->nama_pegawai,
                'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/GetDokter'),
                'options'=>array(
                   'showAnim'=>'fold',
                   'minLength' => 4,
                   'focus'=> 'js:function( event, ui ) {
                        // $("#noRekamMedik").val( ui.item.value );
                        return false;
                    }',
                   'select'=>'js:function( event, ui ) {
            
                        $("#'.CHtml::activeId($model,'nama_pegawai').'").val(ui.item.nama_pegawai);
                        return false;
                    }',

                ),
              )); ?>
            </div>
          </div>
      </fieldset>

    </td>
    <td width="155" style="padding-right:30px;">   <div id='searching'>
                    <fieldset>
					<legend class="rim">Berdasarkan Cara Bayar </legend>
                        <?php echo '<table>
                                                    <tr>
                                                        <td>'.CHtml::hiddenField('filter', 'carabayar', array('disabled'=>'disabled')).'<label>Cara&nbsp;Bayar</label></td>
                                                        <td>'.$form->dropDownList($model, 'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                                            'ajax' => array('type' => 'POST',
                                                                'url' => Yii::app()->createUrl('ActionDynamic/GetPenjaminPasienForCheckBox', array('encode' => false, 'namaModel' => ''.$model->getNamaModel().'')),
                                                                'update' => '#penjamin',  //selector to update
                                                            ),
                                                        )).'
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Penjamin</label>
                                                        </td>
                                                        <td>
                                                            <div id="penjamin">
                                                                   <label> Data belum ditemukan </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                 </table>'; ?>
                    </fieldset>
      </div></td>
  </tr>
</table>
    <div class="form-actions">
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary',
                'type' => 'submit', 'id' => 'btn_simpan','onclick'=>'CekCaraBayar(); return false;'));
        ?>
		<?php
 echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                                                                        array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));?> 
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ', array('idPendaftaran' => ''));
?>

<?php Yii::app()->clientScript->registerScript('cekAll','
  $("#big").find("input").attr("checked", "checked");
  $("#kelasPelayanan").find("input").attr("checked", "checked");
',  CClientScript::POS_READY);
?>
<script>
    function CekCaraBayar(){
       var carabayar = $('#RJLaporanpendapatanruangan_carabayar_id').val();
       if(!jQuery.isNumeric(carabayar)){
           alert('Isi terlebih dahulu Cara Bayar');
           return false;
       }else{
           $('#searchLaporan').submit();
       }
    }
</script>
<script>
function checkAll() {
    if ($("#checkAllKelas").is(":checked")) {
        $('#kelasPelayanan input[name*="kelaspelayanan_id"]').each(function(){
           $(this).attr('checked',true);
        })
//        alert('Checked');
    } else {
       $('#kelasPelayanan input[name*="kelaspelayanan_id"]').each(function(){
           $(this).removeAttr('checked');
        })
    }
    
    if ($("#checkAllCaraBayar").is(":checked")) {
        $('#penjamin input[name*="penjamin_id"]').each(function(){
           $(this).attr('checked',true);
        })
//        alert('Checked');
    } else {
       $('#penjamin input[name*="penjamin_id"]').each(function(){
           $(this).removeAttr('checked');
        })
    }
}   
</script>
<?php 
//$urlGetPenjamin = Yii::app()->createUrl('ActionDynamic/GetPenjaminPasienForCheckBox', array('encode' => false, 'namaModel' => ''.$model->getNamaModel().''));
//Yii::app()->clientScript->registerScript('ajax','
//    $("#'.CHtml::activeId($model, 'carabayar_id').'").change(function(){
//        id = $(this).val();
//        $.post("'.$urlGetPenjamin.'", {id:id},function(data){
//            
//        });
//    });
//',CClientScript::POS_READY); ?>

<?php //Yii::app()->clientScript->registerScript('onclickButton','
//  var tampilGrafik = "<div class=\"tampilGrafik\" style=\"display:inline-block\"> <i class=\"icon-arrow-right icon-white\"></i> Grafik</div>";
//  $(".accordion-heading a.accordion-toggle").click(function(){
//            $(this).parents(".accordion").find("div.tampilGrafik").remove();
//            $(this).parents(".accordion-group").has(".accordion-body.in").length ? "" : $(this).append(tampilGrafik);
//            
//            
//  });
//',  CClientScript::POS_READY);
?>
