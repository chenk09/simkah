<?php
$this->breadcrumbs=array(
	'Reseptur',
);

$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));

$this->renderPartial('/_tabulasi', array('modPendaftaran'=>$modPendaftaran));
?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>1,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number0',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
        'allowDecimal'=>true
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rjreseptur-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#RJResepturT_noresep',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInput();'),
)); ?>

<?php $this->renderPartial($this->pathView.'_formInputObat',array('modPendaftaran'=>$modPendaftaran,'form'=>$form,'modReseptur'=>$modReseptur,'konfigFarmasi'=>$konfigFarmasi)); ?>

<table id="tblDaftarResep" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>Recipe</th>
            <th>R ke</th>
            <th>Nama Obat</th>
            <th>Sumber Dana</th>
            <th>Satuan Kecil</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Sub Total</th>
            <th>Signa</th>
            <th>Etiket</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
        <tr>
            
            <td colspan="7" style="text-align: right;"><b>Total Harga</b></td>
            <td><input type="text" readonly name="totalHargaReseptur" id="totalHargaReseptur" class="inputFormTabel lebar2 currency" /></td>
            <td colspan="3">&nbsp;</td>
        </tr>
    </tfoot>
</table>

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)', 'id'=>'btn_simpan')); ?>
        <?php
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; ?>
				      <?php 
           $content = $this->renderPartial('rawatJalan.views.tips.tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
           $urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print&id='.$modPendaftaran->pendaftaran_id);

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
			
    </div>

<?php $this->endWidget(); ?>
<?php 
$js = <<< JS
//==================================================Validasi===============================================
//*Jangan Lupa Untuk menambahkan hiddenField dengan id "berubah" di setiap form
//* hidden field dengan id "url"
//*Copas Saja hiddenfield di Line 36 dan 35
//* ubah juga id button simpannya jadi "btn_simpan"


function palidasiForm(obj)
{
    var berubah = $('#berubah').val();
    if(berubah=='Ya'){
       if(confirm('Apakah Anda Akan menyimpan Perubahan Yang Sudah Dilakukan?')){
           $('#url').val(obj);
           $('#btn_simpan').click();
       }
    }      
}

JS;
Yii::app()->clientScript->registerScript('js',$js,CClientScript::POS_READY);
?>   