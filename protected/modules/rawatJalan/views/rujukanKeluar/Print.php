
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10)); 


 $style = 'margin-left:auto; margin-right:auto;';
    if (isset($caraPrint)){
        if ($caraPrint == "EXCEL")
            $style = "cellpadding='10',cellspasing='6', width='100%'";
//            $td = "width='100%'";
    } else{
        $style = "style='margin-left:auto; margin-right:auto;'";
//        $td ='';
    }
    
?>

<table width="100%" <?php echo $style; ?>>
    <tr>
        <td <?php echo $td; ?>>
            <label class='control-label'><?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('nama_pasien')); ?>:</label>
            <?php echo CHtml::encode($modPendaftaran->pasien->nama_pasien); ?>
        </td>
        <td>
            <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('tgl_pendaftaran')); ?>:</label>
            <?php echo CHtml::encode($modPendaftaran->tgl_pendaftaran); ?>
        </td>
    </tr><br/>
    <tr>
        <td>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('jeniskelamin')); ?>:</label>
                <?php echo CHtml::encode($modPendaftaran->pasien->jeniskelamin); ?>
        </td>
        <td>
            <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('no_pendaftaran')); ?>:</label>
                <?php echo CHtml::encode($modPendaftaran->no_pendaftaran); ?>
        </td>
    </tr><br/>
    <tr>
        <td>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('umur')); ?>:</label>
                <?php echo CHtml::encode($modPendaftaran->umur); ?>
        </td>
        <td>
            <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('Kelas Pelayanan')); ?>:</label>
            <?php echo CHtml::encode($modPendaftaran->kelaspelayanan->kelaspelayanan_nama); ?>
        </td>
    </tr><br/>
    <tr>
        <td>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('Cara Bayar / Penjamin')); ?>:</label>
                <?php echo CHtml::encode($modPendaftaran->carabayar->carabayar_nama); ?> /  <?php echo CHtml::encode($modPendaftaran->penjamin->penjamin_nama); ?>
        </td>
        <td>
            <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('Nama Dokter')); ?>:</label>
            <?php echo CHtml::encode($modPendaftaran->pegawai->nama_pegawai); ?>
        </td>
    </tr>
       
</table>
<br/>
<table align="CENTER" id="tblDaftarResep" class='table-bordered table-condensed' border="2">
    <thead>
        <tr>
            <th>DETAIL RUJUKAN KELUAR</th>
        </tr>
    </thead>
    <tr>
        <td width=150px >
             <?php foreach ($modRiwayatRujukanKeluar as $i => $rujukan){ ?>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('tgldirujuk')); ?>:</label>
            <br/>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('pegawai_id')); ?>:</label>
            <br/>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('rujukankeluar_id')); ?>:</label>
            <br/>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('nosuratrujukan')); ?>:</label>
            <br/>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('ythdokter')); ?>:</label>
            <br/>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('dirujukkebagian')); ?>:</label>
            <br/>
            
            
        </td>
        <?php } ?>
        <td width=150px >
            <?php echo CHtml::encode($rujukan->tgldirujuk); ?>
            <br/>
            <?php echo CHtml::encode($rujukan->pegawai->nama_pegawai); ?>
            <br/>
            <?php echo CHtml::encode($rujukan->rujukankeluar->rumahsakitrujukan); ?>
            <br/>
            <?php echo CHtml::encode($rujukan->nosuratrujukan); ?>
            <br/>
             <?php echo CHtml::encode($rujukan->ythdokter); ?>
            <br/>
            <?php echo CHtml::encode($rujukan->dirujukkebagian); ?>
            <br/>
            
</td>

        <td width=150px>
             <?php foreach ($modRiwayatRujukanKeluar as $i => $rujukan){ ?>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('catatandokterperujuk')); ?>:</label>
            <br/>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('alasandirujuk')); ?>:</label>
            <br/>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('hasilpemeriksaan_ruj')); ?>:</label>
            <br/>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('diagnosasementara_ruj')); ?>:</label>
            <br/>
            <label class='control-label'><?php echo CHtml::encode($rujukan->getAttributeLabel('lainlain_ruj')); ?>:</label>
            <br/>
                        
        </td>
                <?php } ?>
        <td width=150px>
            <?php echo CHtml::encode($rujukan->catatandokterperujuk); ?>
            <br/>
            <?php echo CHtml::encode($rujukan->alasandirujuk); ?>
            <br/>
            <?php echo CHtml::encode($rujukan->hasilpemeriksaan_ruj); ?>
            <br/>
            <?php echo CHtml::encode($rujukan->diagnosasementara_ruj); ?>
            <br/>
            <?php echo CHtml::encode($rujukan->pengobatan_ruj); ?>
            <br/>
            <?php echo CHtml::encode($rujukan->lainlain_ruj); ?>
            <br/>
             
</td>

    </tr>
    
</table>

<br/>
<table style='margin-left:auto; margin-right:auto;'>
    <tr>
        <td width="400px">
            Catatan Dokter : <?php echo CHtml::encode($riwayat->catatandokterpengirim); ?>
        </td>
        <td width="40px">&nbsp;</td>
        <td>
             Dokter Pemeriksa
            <br/><br/><br/><br/>
           ( <?php echo CHtml::encode($modPendaftaran->pegawai->nama_pegawai); ?> )
        </td>
    </tr>
</table>
