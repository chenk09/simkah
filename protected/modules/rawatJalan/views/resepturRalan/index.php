<style type="text/css">
    .info-msg{
        font-weight: bold;
        color: white;
    }
</style>

<?php
$this->breadcrumbs=array(
	'Reseptur Ranap',
);
$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('/_ringkasDataPasien', array('modPendaftaran' => $modPendaftaran, 'modPasien' => $modPasien, 'modAdmisi'=>$modAdmisi));
$this->renderPartial('/_tabulasi',array('modPendaftaran'=>$modPendaftaran, 'modAdmisi'=>$modAdmisi));
$vMaxNominal = Yii::app()->user->getState('MAX_NOMINAL_DIAG_OBT_BPJS');
echo '<div class="alert alert-block alert-danger" style="text-align: center">Silakan lengkapi form yang sudah disediakan dan jangan lupa pilih <b>apotek tujuan</b> terlebih dahulu<br>karena sangat berpengaruh untuk <b>perhitungan stok obat</b><br>(Maksimal entry nomilan hanya <b>Rp '. number_format($vMaxNominal) .'</b> Jika lebih dari maksimal yang ditentukan proses tidak bisa dilanjutkan.)</div>';
echo '<legend class="rim">RESEPTUR</legend>';
?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency'
));
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => '.',
        'thousands' => '',
        'precision' =>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'ri-reseptur-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'focus'=>'#RJResepturT_noresep',
    'htmlOptions'=>array(
        'onKeyPress'=>'return disableKeyPress(event)',
    ),
)); ?>
<?php $this->renderPartial('_formInputObat',array(
    'dataRuangan'=>$dataRuangan,
    'form'=>$form,
    'modReseptur'=>$modReseptur,
    'modMorbiditas'=>$modMorbiditas
)); ?>
<table id="tblDaftarResep" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>Nama Obat</th>
            <th style="width: 15px">Sumber Dana</th>
            <th style="width: 15px">Satuan Kecil</th>
            <th style="width: 15px">Qty</th>
            <th style="width: 15px">Harga</th>
            <th style="width: 15px">Sub Total</th>
            <th style="width: 15px">Etiket</th>
            <th style="width: 15px">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <tr class="no_record">
            <td colspan="7">Data tidak ditemukan</td>
        </tr>
    </tbody>
    <tfoot>
    <tr class="footer_total">
        <td colspan="5" style="text-align: right;"><b>Total Harga</b></td>
        <td><input style="width: auto;" type="text" readonly name="totalHargaReseptur" id="totalHargaReseptur" class="inputFormTabel currency" /></td>
        <td colspan="3" class="info-msg"></td>
    </tr>
    </tfoot>
</table>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
            array(
                'id'=>'btn_submit',
                'class'=>'btn btn-primary',
                'type'=>'submit',
                'onKeypress'=>'return formSubmit(this,event)'
            )
        ); ?>
        <?php
            $content = $this->renderPartial('../tips/tips',array(),true);
            $this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
        ?>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    function konversiUnFormat()
    {
        $("#tblDaftarResep > tbody > tr .currency").maskMoney('unmasked');
        $('.currency').each(function(){
            this.value = unformatNumber(this.value)
        });
        $("#tblDaftarResep > tbody > tr .number").maskMoney('unmasked');
        $('.number').each(function(){
            this.value = unformatNumber(this.value)
        });
    }

    $(document).on('submit', "#ri-reseptur-form", function (event){
        var thatForm = this;
        var vUri = $(this).attr('action');
        swal.queue([{
            confirmButtonText: 'Lanjutkan',
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            showCancelButton: true,
            cancelButtonText: 'Batal',
            text:'Konfirmasi proses dokumen',
            preConfirm: function (){
                return new Promise(function (resolve){
                    konversiUnFormat();

                    var formdata = new FormData(thatForm);
                    setTimeout(function (){
                        $.ajax({
                            type: "POST",
                            data:formdata,
                            cache: false,
                            contentType: false,
                            processData: false,
                            dataType: 'json',
                            url: vUri,
                            success: function(response, statusText, xhr, $form)
                            {
                                konversiFormat()
                                swal.close();
                                if(response.status == 'not')
                                {
                                    swal({
                                        html: response.msg,
                                        type: 'error'
                                    });
                                }else{
                                    location.reload();
                                }
                            },
                            error: function(response, statusText, xhr, $form)
                            {
                                swal.close();
                            }
                        });
                    }, 500)
                })
            }
        }])
        event.preventDefault();
    });
</script>
