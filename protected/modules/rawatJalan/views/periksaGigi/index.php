
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'odontogramdetail-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#', 
)); ?>
<?php 

$this->widget('bootstrap.widgets.BootAlert');
echo $form->errorSummary(array($modOdontogramDetail)); 

?>
<table>
    <tr>
        <td width="50%">
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Pendaftaran','tglpendaftaran', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="tglpendaftaran"></span>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('No Pendaftaran','nopendaftaran', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="nopendaftaran"></span>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Lahir / Umur','tgllahirumur', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="tgllahirumur"></span>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Kasus Penyakit','kasuspenyakit', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="kasuspenyakit"></span>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Golongan Darah','goldarah', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="goldarah"></span>
                </div>
            </div>
            
            <div class="control-group ">
                <fieldset id="fieldsetKunjunagn" class="">
                    <legend class="accord1">
                        <?php echo CHtml::checkBox('cex_kunjunganpasien', false, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                        Data Kunjungan
                    </legend>
                    <div id="detail_kunjungna_pasien">
                        <?php echo $this->renderPartial('_tabelKunjungan', array('model'=>$modOdontogramDetail)); ?>
                    </div>
                </fieldset>
            </div>
        </td>
        <td width="50%">
            <div class="control-group ">
                <?php echo CHtml::label('No Rekam Medik','noRekamMedik', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php $this->widget('MyJuiAutoComplete',array(
                        'name'=>'noRekamMedik',
                        'value'=>'',
                        //'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienRJ'),
                        'options'=>array(
                           'showAnim'=>'fold',
                           'minLength' => 2,
                           'focus'=> 'js:function( event, ui ) {
                                $("#noRekamMedik").val( ui.item.value );
                                return false;
                            }',
                           'select'=>'js:function( event, ui ) {
                                ambilOdontogram(ui.item.pasien_id,ui.item.pendaftaran_id);
                                $("#tglpendaftaran").text(ui.item.tgl_pendaftaran);
                                $("#nopendaftaran").text(ui.item.no_pendaftaran);
                                $("#tgllahirumur").text(ui.item.tanggal_lahir+" / "+ui.item.umur);
                                $("#kasuspenyakit").text(ui.item.jeniskasuspenyakit_nama);
                                $("#goldarah").text(ui.item.golongandarah);
                                $("#namapegawai").text(ui.item.nama_pegawai);
                                $("#namapasien").text(ui.item.nama_pasien);
                                $("#binbinti").text(ui.item.nama_bin);
                                $("#jeniskelamin").text(ui.item.jeniskelamin);
                                $("#alamat").text(ui.item.alamat_pasien);
                                
                                return false;
                            }',

                        ),
                        'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly'),
                        'tombolDialog'=>array('idDialog'=>'dialogDaftarPasien','idTombol'=>'tombolPasienDialog'),
                    )); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Nama Pasien','namapasien', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="namapasien"></span>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Bin / Binti','binbinti', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="binbinti"></span>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Jenis Kelamin','jeniskelamin', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="jeniskelamin"></span>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Alamat','alamat', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="alamat"></span>
                </div>
            </div>
        </td>
    </tr>
</table>
<?php 
$this->widget('Gigi',array('gigis'=>$gigi)); 
?>
<br/>
<table style="margin:0;">
    <tr>
        <td>
            <div class="control-group ">
                <?php echo $form->labelEx($modOdontogramDetail,'tglperiksa', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php   
                            $this->widget('MyDateTimePicker',array(
                                            'model'=>$modOdontogramDetail,
                                            'attribute'=>'tglperiksa',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                'maxDate' => 'd',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                    )); 
                             ?>
                </div>
            </div>
            <?php //echo $form->textFieldRow($modOdontogramDetail,'tglperiksa',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->hiddenField($modOdontogramDetail,'pasien_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->hiddenField($modOdontogramDetail,'pendaftaran_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->dropDownListRow($modOdontogramDetail,'pegawai_id',  CHtml::listData(DokterV::model()->findAllByAttributes(array('ruangan_id'=>Yii::app()->user->getState('ruangan_id'))), 'pegawai_id', 'nama_pegawai'),array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->hiddenField($modOdontogramDetail,'pegawai_id', 
                array('class'=>'span3', 
                    'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo CHtml::label('Dokter ','namapegawai', array('class'=>'control-label')) ?>
                <div class="controls">
                    : <span id="namapegawai"></span>
                </div>
            </div>  
        </td>
        <td>
            <?php //echo $form->textFieldRow($modOdontogramDetail,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($modOdontogramDetail,'odontogrampasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textAreaRow($modOdontogramDetail,'catatan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        </td>
    </tr>
</table>

 


<center class="form-actions">
    <table style="width:615px;margin:0;">
        <tr>
            <td><div id="icon-odontogram" class="belum-erupsi">UE</div><?php echo CHtml::button('Belum Erupsi', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("E");')); ?></td>
            <td><div id="icon-odontogram" class="tambalan-logam"></div><?php echo CHtml::button('Tambalan Logam', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("r");')); ?></td>
            <td><div id="icon-odontogram" class="sisa-akar"></div><?php echo CHtml::button('Sisa Akar', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("A");')); ?></td>
            <td><?php echo CHtml::button('Sembunyikan Gigi Hilang', array('class'=>'btn btn-primary span3')); ?></td>
        </tr>
        <tr>
            <td><div id="icon-odontogram" class="erupsi-sebagian">PE</div><?php echo CHtml::button('Erupsi Sebagian', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("S");')); ?></td>
            <td><div id="icon-odontogram" class="tambalan-nonlogam"></div><?php echo CHtml::button('Tambalan Non Logam', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("b");')); ?></td>
            <td><div id="icon-odontogram" class="gigi-hilang">X</div><?php echo CHtml::button('Gigi Hilang', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("H");')); ?></td>
            <td><?php echo CHtml::button('Tampilkan Gigi Hilang', array('class'=>'btn btn-primary span3')); ?></td>
        </tr>
        <tr>
            <td><div id="icon-odontogram" class="anomali-bentuk">A</div><?php echo CHtml::button('Anomali Bentuk', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("B");')); ?></td>
            <td><div id="icon-odontogram" class="mahkota-logam"></div><?php echo CHtml::button('Mahkota Logam', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("g");')); ?></td>
            <td><div id="icon-odontogram" class="jembatan"></div><?php echo CHtml::button('Jembatan', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("J");')); ?></td>
            <td><?php// echo CHtml::submitButton('Simpan', array('class'=>'btn btn-primary span3')); ?></td>
        </tr>
        <tr>
            <td><div id="icon-odontogram" class="karies"></div><?php echo CHtml::button('Karies', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("K");')); ?></td>
            <td><div id="icon-odontogram" class="mahkota-nonlogam"></div><?php echo CHtml::button('Mahkota Non Logam', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("n");')); ?></td>
            <td><div id="icon-odontogram" class="gigi-tiruanlepas"></div><?php echo CHtml::button('Gigi Tiruan Lepas', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("L");')); ?></td>
            <td><?php// echo CHtml::button('Cetak', array('class'=>'btn btn-primary span3','onclick'=>'cetakOdontogram()')); ?></td>
        </tr>
        <tr>
            <td><div id="icon-odontogram" class="non-vital"></div><?php echo CHtml::button('Non Vital', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("V");')); ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</center>
<div class="form-actions">
    <?php
    // if(!empty($_GET['id'])){
    //     echo CHtml::htmlButton(Yii::t('mds', '{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary','type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','disabled'=>true));
    // }else{
            echo CHtml::htmlButton(Yii::t('mds', '{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary','type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)'));
    // }
    ?>
    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),Yii::app()->createUrl($this->module->id.'/'.periksaGigi.'/index'),array('class'=>'btn btn-danger'));  ?>
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Cetak',array('{icon}'=>'<i class="icon-print icon-white"></i>')), array('class'=>'btn btn-primary','onclick'=>'cetakOdontogram()')); ?>
    <?php
    $content = $this->renderPartial('../tips/transaksi',array(),true);
    $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
    ?>
</div>

<?php $this->endWidget(); ?>
    
    
<script type="text/javascript">

function ambilOdontogram(idPasien,idPendaftaran)
{
    $.post('<?php echo $this->createUrl('ajaxOdontogram'); ?>', {idPasien:idPasien}, function(data){
        $('#<?php echo CHtml::activeId($modOdontogramDetail, 'pasien_id') ?>').val(idPasien);
        $('#<?php echo CHtml::activeId($modOdontogramDetail, 'pendaftaran_id') ?>').val(idPendaftaran);
        $('#dialogDaftarPasien').dialog('close');
        for(n in data) {
            url = '<?php echo $this->createUrl('myOdontogram'); ?>&code='+data[n];
            $('#gram_'+n).find('input[name^=\"codeOdon\"]').val(data[n]);
            $('#gram_'+n).css('background-image','url('+url+')');
        }
    }, 'json');
    
    setTimeout(function(){
        $.fn.yiiGridView.update('tableKunjungan', {
                data: $("#odontogramdetail-t-form").serialize()
        });            
    }, 1000);
}

function cetakOdontogram()
{
    var idPasien = $('#<?php echo CHtml::activeId($modOdontogramDetail,'pasien_id'); ?>').val();
    var idPendaftaran = $('#<?php echo CHtml::activeId($modOdontogramDetail,'pendaftaran_id'); ?>').val();
    var src = '<?php echo $this->createUrl('cetakOdontogram'); ?>&idPasien='+idPasien+'&idPendaftaran='+idPendaftaran;
    $('#iframeCetakOdontogram').attr('src', src);
    $('#dialogCetakOdontogram').dialog('open');
}
</script>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogDaftarPasien',
            'options'=>array(
                'title'=>'Daftar Pasien',
                'autoOpen'=>false,
                'resizable'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>600,
            ),
        ));

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'daftarpasien-v-grid',
    'dataProvider'=>$kunjunganPasien->searchKunjunganPasien(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'filter'=>$kunjunganPasien,
    'columns'=>array(   
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-check\'></i> ", "javascript:void(0)",array("onclick"=>"ambilOdontogram($data->pasien_id,$data->pendaftaran_id);
                                                                                                                     \$(\"#tglpendaftaran\").text(\"$data->tgl_pendaftaran\");
                                                                                                                     \$(\"#nopendaftaran\").text(\"$data->no_pendaftaran\");
                                                                                                                     \$(\"#tgllahirumur\").text(\"$data->tanggal_lahir / $data->umur\");
                                                                                                                     \$(\"#kasuspenyakit\").text(\"$data->jeniskasuspenyakit_nama\");
                                                                                                                     \$(\"#goldarah\").text(\"$data->golongandarah\");
                                                                                                                     \$(\"#namapegawai\").text(\"$data->nama_pegawai\");
                                                                                                                     \$(\"#namapasien\").text(\"$data->nama_pasien\");
                                                                                                                     \$(\"#binbinti\").text(\"$data->nama_bin\");
                                                                                                                     \$(\"#jeniskelamin\").text(\"$data->jeniskelamin\");
                                                                                                                    \$(\"#OdontogramdetailT_pegawai_id\").val(\"$data->pegawai_id\");
                                                                                                                     \$(\"#alamat\").text(\"$data->alamat_pasien\");",
                                                                                                                    
                                                                                                         "rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien"))',
                  'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                ),
                    
                'no_rekam_medik',   
                //tgl_pendaftaran',
                array(
                    'name'=>'tgl_pendaftaran',
                    'value'=>'$data->tgl_pendaftaran',
                    'filter'=>$this->widget('MyDateTimePicker',array(
                    'model'=>$kunjunganPasien,
                    'attribute'=>'tgl_pendaftaran',
                    'mode'=>'date',
                    'options'=> array(
                        'dateFormat'=>Params::DATE_TIME_FORMAT
                        ),
                        'htmlOptions'=>array('readonly'=>false, 'class'=>'dtPicker3 tgl'),
                        ),true
                        ),
                        'htmlOptions'=>array('width'=>'80','style'=>'text-align:center'),
                ),
                'no_pendaftaran',
                'nama_pasien', 
                'alamat_pasien',
                'penjamin_nama',
                'nama_pegawai',
                'jeniskasuspenyakit_nama',
                array(
                    'name'=>'statusperiksa',
                    'type'=>'raw',
                    'filter' => CHtml::listData(RJInfokunjunganrjV::model()->findAll(),'statusperiksa', 'statusperiksa'),
                ),
        
                // array(
                //     'header'=>'No Pendaftaran',
                //     'value'=>'$data->no_pendaftaran',
                                    
                //     ),
                
                //'nama_bin',
                // array(
                //     'header'=>'Nama Pasien'.' /<br/>'.'Alias',
                //     'type'=>'raw',
                //     'value'=>'"$data->nama_pasien"."<br/>"."$data->nama_bin"',
                // ),
              
                // array(
                //     'header'=>'Alamat'.' /<br/>'.'RT RW',
                //     'type'=>'raw',
                //     'value'=>'"$data->alamat_pasien"."<br/>"."$data->RTRW"',
                // ),
                // array(
                //     'header'=>'Penjamin'.' /<br/>'.'Cara Bayar',
                //     'type'=>'raw',
                //     'value'=>'"$data->penjamin_nama"."<br/>"."$data->carabayar_nama"',
                // ),
                
                //'ruangan_nama',
        
                
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogCetakOdontogram',
            'options'=>array(
                'title'=>'Odontogram Pasien',
                'autoOpen'=>false,
                'resizable'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>600,
            ),
        ));
echo '<iframe src="" id="iframeCetakOdontogram" name="iframeCetakOdontogram" width="100%" height="550" ></iframe>';
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>