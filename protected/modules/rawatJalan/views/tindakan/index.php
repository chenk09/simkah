<?php
$this->breadcrumbs=array(
	'Tindakan',
);

$this->renderPartial('rawatJalan.views._ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));
$this->renderPartial('/_tabulasi', array('modPendaftaran'=>$modPendaftaran));

?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
        'allowDecimal'=>true
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjtindakan-pelayanan-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'htmlOptions'=>array(
            'onKeyPress'=>'return disableKeyPress(event)',
            'onSubmit'=>'return cekInput();'
        ),
)); ?>

    
    <?php
        if(!empty($modViewTindakans)) {

            $this->renderPartial($this->pathView.'_listTindakanPasien',array('modTindakans'=>$modViewTindakans,
                                                             'modViewBmhp'=>$modViewBmhp,
                                                             'removeButton'=>true));
        }

    ?>
<div class="formInputTab">
    <p class="help-block">
        <?php echo CHtml::hiddenField('url',$this->createUrl('',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),array('readonly'=>TRUE));?>
        <?php echo CHtml::hiddenField('berubah','',array('readonly'=>TRUE));?>
        <?php //echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
    
    <?php
        echo $form->dropDownListRow(
            $modTindakan,'[0]tipepaket_id',
            Chtml::listData($modTindakan->getTipePaketItems($modPendaftaran->carabayar_id), 'tipepaket_id', 'tipepaket_nama'),
            array(
                'class'=>'span3',
                'onkeypress'=>"return $(this).focusNextInputField(event);",
                'onchange'=>'loadTindakanPaket(this.value,"'.$modPendaftaran->kelaspelayanan_id.'","'.$modPendaftaran->kelompokumur_id.'")'
            )
        );
    ?>
        
    <table class="items table table-striped table-bordered table-condensed" id="tblInputTindakan">
        <thead>
            <tr>
                <th>Kategori Tindakan</th>
                <th rowspan="2">Nama Tindakan</th>
                <!-- <th rowspan="2">Tarif Satuan AA</th> -->
                <th rowspan="2">Qty</th>
                <!--<th rowspan="2">Tarif Satuan</th>-->
                <!--<th rowspan="2">Qty Tindakan</th>-->
                <th rowspan="2">Satuan<br/>Tindakan</th>
                <th rowspan="2">Cyto </th>
                <th rowspan="2">Tarif Cyto</th>
                <!-- <th rowspan="2">Jml Tarif</th> -->
                <th rowspan="2">&nbsp;</th>
            </tr>
            <tr>
                <th>Tgl Tindakan</th>
            </tr>
        </thead>
        <?php 

            $trTindakan = $this->renderPartial($this->pathView.'_rowTindakanPasien',array('modTindakan'=>$modTindakan,'modTindakans'=>$modTindakans),true); 
            echo $trTindakan;

        ?>
    </table>
    <?php echo $form->errorSummary($modTindakan); ?>
    
    <table>
        <tr>
            <td width="60%">
                <?php $this->renderPartial($this->pathView.'_formPemakaianBahan',array()); ?>
                <br>
                <?php
                //FORM REKENING
                    $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                        array(
                            'form'=>$form,
                            'modRekenings'=>$modRekenings,
                        )
                    );

                ?>
            </td>
            <td>
                <?php $this->renderPartial($this->pathView.'_formPaketBmhp',array('modViewBmhp'=>$modViewBmhp, 'modTindakan'=>$modTindakan)); ?>
            </td>
        </tr>
    </table>
    <table>
        <tr>
        <td width="100%">
            
        </td>
        </tr>
    </table>
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan')); ?>
									      <?php 
           $content = $this->renderPartial('rawatJalan.views.tips.tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
        ?>
			
            <?php //echo CHtml::link('Test Update Stok', '#', array('onclick'=>'testUpdateStok(80,4);return false;','class'=>'btn')); ?>
    </div>
    
</div>

<?php $this->endWidget(); ?>

<?php $this->renderPartial($this->pathView.'_dialogPemeriksaLengkap',array('modTindakan'=>$modTindakan)); ?> 
<?php $this->renderPartial($this->pathView.'_dialogPemeriksa',array('modTindakan'=>$modTindakan));?> 

<script type="text/javascript">
 
// the subviews rendered with placeholders
var trTindakan = new String(<?php echo CJSON::encode($this->renderPartial($this->pathView.'_rowTindakanPasien',array('modTindakan'=>$modTindakan,'removeButton'=>true),true));?>);
var trTindakanFirst = new String(<?php echo CJSON::encode($this->renderPartial($this->pathView.'_rowTindakanPasien',array('modTindakan'=>$modTindakan,'removeButton'=>false),true));?>);

// function addRowTindakan(obj)
// {
//     $(obj).parents('table').children('tbody').append(trTindakan.replace());
//     <?php 
//         $attributes = $modTindakan->attributeNames(); 
//         foreach($attributes as $i=>$attribute){
//             echo "renameInput('RJTindakanPelayananT','$attribute');";
//         }
//     ?>
//     renameInput('RJTindakanPelayananT','daftartindakanNama');
//     renameInput('RJTindakanPelayananT','kategoriTindakanNama');
//     renameInput('RJTindakanPelayananT','persenCyto');
//     renameInput('RJTindakanPelayananT','jumlahTarif');
    
//     jQuery('<?php echo Params::TOOLTIP_SELECTOR; ?>').tooltip({"placement":"<?php echo Params::TOOLTIP_PLACEMENT;?>"});
//     jQuery('input[name$="[daftartindakanNama]"]').autocomplete(
//         {
//             'showAnim':'fold',
//             'minLength':2,
//             'focus':function(event, ui )
//             {
//                 $(this).val( ui.item.label);
//                 return false;
//             },
//             'select':function( event, ui )
//             {
//                 setTindakan(this, ui.item);
//                 return false;
//             },
//             'source':function(request, response)
//             {
//                 $.ajax({
//                     url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/DaftarTindakan');?>",
//                     dataType: "json",
//                     data:{
//                         term: request.term,
//                         idTipePaket: $("#RJTindakanPelayananT_0_tipepaket_id").val(),
//                         idKelasPelayanan: $("#RJPendaftaranT_kelaspelayanan_id").val(),
//                     },
//                     success: function (data) {
//                         response(data);
//                     }
//                 })
//             }
//         }
//     );
    
//     jQuery('#tblInputTindakan tr:last .tanggal').datetimepicker(
//         jQuery.extend(
//             {
//                 showMonthAfterYear:false
//             }, 
//             jQuery.datepicker.regional['id'],
//             {
//                 'dateFormat':'dd M yy',
//                 'maxDate':'d',
//                 'timeText':'Waktu',
//                 'hourText':'Jam',
//                 'minuteText':'Menit',
//                 'secondText':'Detik',
//                 'showSecond':true,
//                 'timeOnlyTitle':'Pilih Waktu',
//                 'timeFormat':'hh:mm:ss',
//                 'changeYear':true,
//                 'changeMonth':true,
//                 'showAnim':'fold',
//                 'yearRange':'-80y:+20y'
//             }
//         )
//     );
// }

function addRowTindakan(obj)
{
    $(obj).parents('table').children('tbody').append(trTindakan.replace());
    <?php 
        $attributes = $modTindakan->attributeNames(); 
        foreach($attributes as $i=>$attribute){
            echo "renameInput('RJTindakanPelayananT','$attribute');";
        }
    ?>
    renameInput('RJTindakanPelayananT','daftartindakanNama');
    renameInput('RJTindakanPelayananT','kategoriTindakanNama');
    renameInput('RJTindakanPelayananT','persenCyto');
    renameInput('RJTindakanPelayananT','jumlahTarif');
    renameInput('RJTindakanPelayananT','tgl_tindakan');
    jQuery('<?php echo Params::TOOLTIP_SELECTOR; ?>').tooltip({"placement":"<?php echo Params::TOOLTIP_PLACEMENT;?>"});
    jQuery('input[name$="[daftartindakanNama]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                    $(this).val( ui.item.label);
                                                                                    return false;
                                                                                },'select':function( event, ui ) {
                                                                                    setTindakan(this, ui.item);
                                                                                    return false;
                                                                                },'source':function(request, response) {
                                                                                                $.ajax({
                                                                                                    url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/DaftarTindakan');?>",
                                                                                                    dataType: "json",
                                                                                                    data: {
                                                                                                        term: request.term,
                                                                                                        idTipePaket: $("#RJTindakanPelayananT_0_tipepaket_id").val(),
                                                                                                        idKelasPelayanan: $("#PasienadmisiT_kelaspelayanan_id").val(),
                                                                                                    },
                                                                                                    success: function (data) {
                                                                                                        response(data);
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                });   
    jQuery('input[name$="[tgl_tindakan]"]').datetimepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','maxDate':'d','timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold','yearRange':'-80y:+20y'}));
}

function batalTindakan(obj)
{
    if(confirm('Apakah anda yakin akan membatalkan tindakan?')){
        $(obj).parents('tr').next('tr').detach();
        $(obj).parents('tr').detach();
        
        <?php 
            foreach($attributes as $i=>$attribute){
                echo "renameInput('RJTindakanPelayananT','$attribute');";
            }
        ?>
        renameInput('RJTindakanPelayananT','daftartindakanNama');
        renameInput('RJTindakanPelayananT','kategoriTindakanNama');
        renameInput('RJTindakanPelayananT','persenCyto');
        renameInput('RJTindakanPelayananT','jumlahTarif');
        var daftartindakan_id = $(obj).parents('tr').find('input[name$="[daftartindakan_id]"]').val();
        removeRekeningTindakan(daftartindakan_id);
        
    }
}
 
function deleteTindakan(obj,idTindakanpelayanan)
{
    if(confirm('Apakah anda yakin akan menghapus tindakan?')){
        $.post('<?php echo $this->createUrl('ajaxDeleteTindakanPelayanan') ?>', {idTindakanpelayanan: idTindakanpelayanan}, function(data){
            if(data.success)
            {
                $(obj).parent().parent().detach();
                alert('Data berhasil dihapus !!');
            } else {
                alert('Data Gagal dihapus');
            }
        }, 'json');
    }
}

function renameListTindakan(modelName,attributeName)
{
    var trLength = $('#tblInputTindakan tr').length;
    var i = -1;
    $('#tblInputTindakan tr').each(function(){
        if($(this).has('input[name$="[tarif_satuan]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('input[name^="daftartindakanNama["]').attr('name','daftartindakanNama['+i+']');
        $(this).find('input[name^="daftartindakanNama["]').attr('id','daftartindakanNama_'+i+'');
        $(this).find('a[id^="btnAddDokter_"]').attr('id','btnAddDokter_'+i+'');
    });
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblInputTindakan tr').length;
    var i = -1;
    $('#tblInputTindakan tr').each(function(){
        if($(this).has('input[name$="[daftartindakan_id]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('input[name^="daftartindakanNama["]').attr('name','daftartindakanNama['+i+']');
        $(this).find('input[name^="daftartindakanNama["]').attr('id','daftartindakanNama_'+i+'');
        $(this).find('a[id^="btnAddDokter_"]').attr('id','btnAddDokter_'+i+'');
        $(this).find('div[id^="tampilanDokterPemeriksa_"]').attr('id','tampilanDokterPemeriksa_'+i+'');
        $(this).find('input[id="row"]').attr('value',i);
        $(this).find('input[id="row"]').val(i);
        
        jQuery('#tblInputTindakan tr:last .tanggal').datetimepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','maxDate':'d','timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold','yearRange':'-80y:+20y'}));
    });
}
 
function addDokter1(obj)
{
    $('#dialogPemeriksa').dialog('open');
    $('#dialogPemeriksa #rowTindakan').val($(obj).get(0).id); //$(obj).attr('id')
}

function addDokter2(obj)
{
    $('#dialogPemeriksaLengkap').dialog('open');
    $('#dialogPemeriksaLengkap #rowTindakan').val($(obj).parent().find('input[id="row"]').val());
}

function setParamedis()
{
    var idBtnAddDokter = $('#dialogPemeriksa #rowTindakan').val();
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokterpemeriksa1_id]"]').val($('#dialogPemeriksa #dokterpemeriksa1').val());
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokterpemeriksa2_id]"]').val($('#dialogPemeriksa #dokterpemeriksa2').val());
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokterpendamping_id]"]').val($('#dialogPemeriksa #dokterpendamping').val());
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokteranastesi_id]"]').val($('#dialogPemeriksa #dokterpemeriksa1').val());
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokterdelegasi_id]"]').val($('#dialogPemeriksa #dokterdelegasi').val());
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[bidan_id]"]').val($('#dialogPemeriksa #bidan').val());
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[suster_id]"]').val($('#dialogPemeriksa #suster').val());
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[perawat_id]"]').val($('#dialogPemeriksa #perawat').val());
}

function setDokterPemeriksa1(item)
{
    var idBtnAddDokter = $('#dialogPemeriksa #rowTindakan').val();
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokterpemeriksa1_id]"]').val(item.pegawai_id);
    $('#'+idBtnAddDokter).parents('td').find('#tampilanDokterPemeriksa').html("Dokter Pemeriksa : "+item.nama_pegawai);
    //document.getElementById('a').innerHTML = "Dokter Pemeriksa : "+item.nama_pegawai;
}

// function setDefaultDokterPemeriksa1(){
//     var dokterId = '<?php echo (empty($modTindakan->dokterpemeriksa1_id)) ? " " : $modTindakan->dokterpemeriksa1_id; ?>';
//     var dokterNama = "<?php echo (empty($modTindakan->dokterpemeriksa1_id)) ? "" : $modTindakan->dokterpemeriksa1Nama ?>";
//     if(dokterId != ""){
//         $('#dialogPemeriksaLengkap #dokterpemeriksa1_id').val(dokterNama);
//     }
// }

// setDefaultDokterPemeriksa1();
// function setDokterPemeriksa1(item)
// {
//     var row = $('#dialogPemeriksa #rowTindakan').val();
//     $('#RJTindakanPelayananT_'+row+'_dokterpemeriksa1_id').val(item.pegawai_id);
//     $('#tampilanDokterPemeriksa_'+row).html("Dokter Pemeriksa : "+item.nama_pegawai);
// }

// function setDokterPemeriksa2(item)
// {
//     var idBtnAddDokter = $('#dialogPemeriksa #rowTindakan').val();
//     $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokterpemeriksa2_id]"]').val(item.pegawai_id);
// }

// function setDokterPendamping(item)
// {
//     var idBtnAddDokter = $('#dialogPemeriksa #rowTindakan').val();
//     $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokterpendamping_id]"]').val(item.pegawai_id);
// }

// function setDokterAnastesi(item)
// {
//     var idBtnAddDokter = $('#dialogPemeriksa #rowTindakan').val();
//     $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokteranastesi_id]"]').val(item.pegawai_id);
// }

function setDokterDelegasi(item)
{
    var idBtnAddDokter = $('#dialogPemeriksa #rowTindakan').val();
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[dokterdelegasi_id]"]').val(item.pegawai_id);
}

function setBidan(item)
{
    var idBtnAddDokter = $('#dialogPemeriksa #rowTindakan').val();
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[bidan_id]"]').val(item.pegawai_id);
}

function setSuster(item)
{
    var idBtnAddDokter = $('#dialogPemeriksa #rowTindakan').val();
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[suster_id]"]').val(item.pegawai_id);
}

function setPerawat(item)
{
    var idBtnAddDokter = $('#dialogPemeriksa #rowTindakan').val();
    $('#'+idBtnAddDokter).parents('td').find('input[name$="[perawat_id]"]').val(item.pegawai_id);
} 

function setTindakan(obj,item)
{
    var hargaTindakan = unformatNumber(item.harga_tariftindakan);
    var subsidiAsuransi = unformatNumber(item.subsidiasuransi);
    var subsidiPemerintah = unformatNumber(item.subsidipemerintah);
    var subsidiRumahsakit = unformatNumber(item.subsidirumahsakit);
    if(isNaN(subsidiAsuransi))subsidiAsuransi=0;
    if(isNaN(subsidiPemerintah))subsidiPemerintah=0;
    if(isNaN(subsidiRumahsakit))subsidiRumahsakit=0;
    $(obj).parents('tr').find('input[name$="[kategoriTindakanNama]"]').val(item.kategoritindakan_nama);
    $(obj).parents('tr').find('input[name$="[daftartindakan_id]"]').val(item.daftartindakan_id);
    $(obj).parents('tr').find('input[name$="[tarif_satuan]"]').val(formatNumber(item.harga_tariftindakan));
    $(obj).parents('tr').find('input[name$="[qty_tindakan]"]').val('1');
    $(obj).parents('tr').find('input[name$="[persenCyto]"]').val(formatNumber(item.persencyto_tind));
    $(obj).parents('tr').find('input[name$="[jumlahTarif]"]').val(formatNumber(item.harga_tariftindakan));
    $(obj).parents('tr').find('input[name$="[subsidiasuransi_tindakan]"]').val(formatNumber(item.subsidiasuransi));
    $(obj).parents('tr').find('input[name$="[subsidipemerintah_tindakan]"]').val(formatNumber(item.subsidipemerintah));
    $(obj).parents('tr').find('input[name$="[subsisidirumahsakit_tindakan]"]').val(formatNumber(item.subsidirumahsakit));
    $(obj).parents('tr').find('input[name$="[iurbiaya_tindakan]"]').val(formatNumber(hargaTindakan - (subsidiAsuransi + subsidiPemerintah +subsidiRumahsakit)));
    //$(obj).parents('tr').find('input[name$="[iurbiaya_tindakan]"]').val(item.iurbiaya);
    tambahTindakanPemakaianBahan(item.daftartindakan_id,item.label);
    var kelaspelayananId = $("#RJPendaftaranT_kelaspelayanan_id").val();
    var hargaSatuan = unformatNumber($(obj).parents('tr').find('input[name$="[tarif_satuan]"]').val());
    var qty = $(obj).parents('tr').find('input[name$="[qty_tindakan]"]').val();
    getDataRekening(item.daftartindakan_id,kelaspelayananId,1,"tm");
    setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya
        updateRekeningTindakan(item.daftartindakan_id, formatDesimal(hargaSatuan*qty))
    },1500);

    // var tombolAddDokter = $(obj).parents('tr').next().find('a');
    // addDokter(tombolAddDokter);
}

function tambahTindakanPemakaianBahan(value,label)
{
    $('#daftartindakanPemakaianBahan').append('<option value="'+value+'">'+label+'</option>');
}

function loadTindakanPaket(idTipePaket,idKelasPelayanan,idKelompokUmur)
{
//    alert(idTipePaket);
    //var idNonPaket = <?php //echo Params::TIPEPAKET_NONPAKET; ?>; 
    var idCarabayar = $('#RJPendaftaranT_carabayar_id').val();
    
    /*    
    if(idTipePaket == <?php echo Params::TIPEPAKET_NONPAKET; ?>)
    {
        $('#tblInputTindakan > tbody').html(trTindakanNonPaket.replace());
    }else if(idTipePaket == <?php echo Params::TIPEPAKET_LUARPAKET; ?>)
    {
        $('#tblInputTindakan > tbody').html(trTindakanPaketLuar.replace());
    }else{
        
    }
    */
   
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/loadFormTindakanPaket') ?>',
        {
            idTipePaket: idTipePaket,
            idKelasPelayanan:idKelasPelayanan, 
            idKelompokUmur:idKelompokUmur, 
            idCarabayar:idCarabayar
        },
        function(data)
        {
            if(data.form == '')
                $('#tblInputTindakan > tbody').html(trTindakanFirst.replace());
            else
                $('#tblInputTindakan > tbody').html(data.form); 

            $("#tblInputTindakan > tbody .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0});
            $('.currency').each(function(){this.value = formatNumber(this.value)});
            $("#tblInputTindakan > tbody .number").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0,"symbol":null});
            $('.number').each(function(){this.value = formatNumber(this.value)});

            $('#tblInputPaketBhp > tbody').html(data.formPaketBmhp);
            $('#totHargaBmhp').val(formatNumber(data.totHargaBmhp));
            $('#tblInputPemakaianBahan > tbody').html('');
            $('#daftartindakanPemakaianBahan').html(data.optionDaftarttindakan);

            jQuery('<?php echo Params::TOOLTIP_SELECTOR; ?>').tooltip({"placement":"<?php echo Params::TOOLTIP_PLACEMENT;?>"});
            jQuery('input[name$="[daftartindakanNama]"]').autocomplete(
                {
                    'showAnim':'fold',
                    'minLength':2,
                    'focus':function( event, ui )
                    {
                        $(this).val( ui.item.label);
                        return false;
                    },
                    'select':function( event, ui )
                    {
                        setTindakan(this, ui.item);
                        return false;
                    },
                    'source':function(request, response)
                    {
                        $.ajax({
                            url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/DaftarTindakan');?>",
                            dataType: "json",
                            data: {
                                term: request.term,
                                idTipePaket: $("#RJTindakanPelayananT_0_tipepaket_id").val(),
                                idKelasPelayanan: $("#RJPendaftaranT_kelaspelayanan_id").val(),
                            },
                            success: function (data) {
                                response(data);
                            }
                        })
                    }
                }
            ); 
            jQuery('#tblInputTindakan tbody .tanggal').datetimepicker(
                jQuery.extend(
                    {showMonthAfterYear:false},
                    jQuery.datepicker.regional['id'],
                    {
                        'dateFormat':'dd M yy',
                        'maxDate':'d',
                        'timeText':'Waktu',
                        'hourText':'Jam',
                        'minuteText':'Menit',
                        'secondText':'Detik',
                        'showSecond':true,
                        'timeOnlyTitle':'Pilih Waktu',
                        'timeFormat':'hh:mm:ss',
                        'changeYear':true,
                        'changeMonth':true,
                        'showAnim':'fold',
                        'yearRange':'-80y:+20y'
                    }
                )
            );
        },
        'json'
    );
}

function hitungCyto(obj)
{
    var tarifSatuan = unformatNumber($(obj).parents("#tblInputTindakan tr").find('input[name$="[tarif_satuan]"]').val());
    var qty = unformatNumber($(obj).parents("#tblInputTindakan tr").find('input[name$="[qty_tindakan]"]').val());
    var persenCyto = unformatNumber($(obj).parents("#tblInputTindakan tr").find('input[name$="[persenCyto]"]').val());
    var cyto = unformatNumber($(obj).parents("#tblInputTindakan tr").find('select[name$="[cyto_tindakan]"]').val());
    if(cyto == '0')
        persenCyto = 0;
    var tarifCyto = qty * tarifSatuan * persenCyto / 100;
    var subTotal = tarifSatuan * qty + tarifCyto;
    $(obj).parents("#tblInputTindakan tr").find('input[name$="[tarifcyto_tindakan]"]').val(formatNumber(tarifCyto));
    $(obj).parents("#tblInputTindakan tr").find('input[name$="[jumlahTarif]"]').val(formatNumber(subTotal));
    hitungTotal(); 
}

function hitungSubtotal(obj)
{
    var tarifSatuan = unformatNumber($(obj).parents("#tblInputTindakan tr").find('input[name$="[tarif_satuan]"]').val());
    var qty = unformatNumber($(obj).parents("#tblInputTindakan tr").find('input[name$="[qty_tindakan]"]').val());
    var persenCyto = unformatNumber($(obj).parents("#tblInputTindakan tr").find('input[name$="[persenCyto]"]').val());
    var cyto = unformatNumber($(obj).parents("#tblInputTindakan tr").find('select[name$="[cyto_tindakan]"]').val());
    if(cyto == '0')
        persenCyto = 0;
    var tarifCyto = qty * tarifSatuan * persenCyto / 100;
    var subTotal = tarifSatuan * qty + tarifCyto;
    $(obj).parents("#tblInputTindakan tr").find('input[name$="[tarifcyto_tindakan]"]').val(formatNumber(tarifCyto));
    $(obj).parents("#tblInputTindakan tr").find('input[name$="[jumlahTarif]"]').val(formatNumber(subTotal));
    var hargaSatuan = unformatNumber($(obj).parents("#tblInputTindakan tr").find('input[name$="[tarif_satuan]"]').val());   
    var daftartindakan_id = $(obj).parents("#tblInputTindakan tr").find('input[name$="[daftartindakan_id]"]').val();
    var qty = unformatNumber($(obj).parents("#tblInputTindakan tr").find('input[name$="[qty_tindakan]"]').val());
    saldo = hargaSatuan * qty;
//    $('#tblInputRekening #row').each(function(){
//        var daftartindakan = $("#tblInputRekening tr").find('input[name$="[daftartindakan_id]"]').val();
//        var debit = unformatNumber($(this).find('input[name$="[saldodebit]"]').val());
//        var kredit = unformatNumber($(this).find('input[name$="[saldokredit]"]').val());
//        alert(debit);
//        alert(kredit);
//        if(daftartindakan_id == daftartindakan){
//            if(debit == 0){
//                saldo = kredit * qty;    
//            }else{
//                saldo = debit * qty
//            }
//            
//        }        
//    });
    var obatalkesId = $(obj).parents("#tblInputPemakaianBahan tr").find('input[name$="[obatalkes_id]"]').val();
    var qty = $(obj).parents("#tblInputPemakaianBahan tr").find('input[name$="[qty]"]').val();
    var saldo = $(obj).parents("#tblInputPemakaianBahan tr").find('input[name$="[harganetto]"]').val();
    var saldo2 = $(obj).parents("#tblInputPemakaianBahan tr").find('input[name$="[harganetto]"]').val();
    saldoTindakan = (saldo2*qty);
    updateRekeningTindakan(daftartindakan_id, formatDesimal(saldo));
    updateRekeningObatApotek(obatalkesId, formatDesimal(saldo), formatDesimal(saldoTindakan));
    hitungTotal(); 
    
    $('.currency').each(function(){this.value = formatNumber(this.value)});
//    $('.number').each(function(){this.value = formatNumber(this.value)});
}

function testUpdateStok(qty,idObatAlkes)
{
    $.post('<?php echo $this->createUrl('updateStok') ?>', {qty:qty, idObatAlkes:idObatAlkes}, function(data){
            alert(data.input);
        }, 'json');
}

function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
function setDialog(obj){
    $("#giladiagnosa-m-grid").find("tr").removeClass("yellow_background");
    idTipePaket = $("#<?php echo CHtml::activeId($modTindakan,'[0]tipepaket_id'); ?>").val();
    idKelasPelayanan = $("#<?php echo CHtml::activeId($modPendaftaran,'kelaspelayanan_id'); ?>").val();
    $.get('<?php echo Yii::app()->createUrl($this->route, array('idPendaftaran'=>$modPendaftaran->pendaftaran_id));?>',{test:'aing',idTipePaket: idTipePaket, idKelasPelayanan:idKelasPelayanan},function(data){
        $("#tableDaftarTindakanPaket").html(data);
    });
    parent = $(obj).parents(".input-append").find("input").attr("id");
    dialog = "#dialogDaftarTindakanPaket";
    $(dialog).attr("parent-dialog",parent);
    $(dialog).dialog("open");
}
function setTindakanAuto(idKelasPelayanan, idDaftarTindakan){
    idTipePaket = $("#<?php echo CHtml::activeId($modTindakan,'[0]tipepaket_id'); ?>").val();
    dialog = "#dialogDaftarTindakanPaket";
    /*
    if(idDlg != null)
    {
        dialog = idDlg;
    }
    */
    parent = $(dialog).attr("parent-dialog");
    obj = $("#"+parent);
    $.get('<?php echo Yii::app()->createUrl('ActionAutoComplete/daftarTindakan'); ?>',{idTipePaket: idTipePaket, idKelasPelayanan:idKelasPelayanan, idDaftarTindakan:idDaftarTindakan},function(data){
        $(obj).val(data[0].kategoritindakan_nama);
        $(obj).val(data[0].daftartindakan_nama);
        setTindakan(obj,data[0]);
    },"json");
    $(dialog).dialog("close");
    
}

</script>

<?php 
//DINONAKTIFKAN : RND-7536 / EHJ-3488
//========= Dialog buat daftar tindakan  =========================
//$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
//    'id'=>'dialogDaftarTindakan',
//    'options'=>array(
//        'title'=>'Daftar Tindakan',
//        'autoOpen'=>false,
//        'modal'=>true,
//        'width'=>800,
//        'height'=>400,
//        'resizable'=>false,
//    ),
//));
//    //echo $modPendaftaran->kelaspelayanan_id;
//    $this->renderPartial($this->pathView.'_daftarTindakan');
//
//$this->endWidget('zii.widgets.jui.CJuiDialog');
//========= end daftar tindakan =============================
?> 
<?php 
//========= Dialog buat daftar tindakan  =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogDaftarTindakanPaket',
    'options'=>array(
        'title'=>'Daftar Tindakan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>500,
        'resizable'=>false,
    ),
));

echo '<div id="tableDaftarTindakanPaket"></div>';
    //echo $modPendaftaran->kelaspelayanan_id;
    //$this->renderPartial($this->pathView.'_daftarTindakanPaket');

$this->endWidget('zii.widgets.jui.CJuiDialog');
//========= end daftar tindakan =============================
?> 
<?php 
$js = <<< JS
//==================================================Validasi===============================================
//*Jangan Lupa Untuk menambahkan hiddenField dengan id "berubah" di setiap form
//* hidden field dengan id "url"
//*Copas Saja hiddenfield di Line 36 dan 35
//* ubah juga id button simpannya jadi "btn_simpan"


function palidasiForm(obj)
{
    var berubah = $('#berubah').val();
    if(berubah=='Ya'){
       if(confirm('Apakah Anda Akan menyimpan Perubahan Yang Sudah Dilakukan?')){
           $('#url').val(obj);
           $('#btn_simpan').click();
       }
    }      
}

JS;
Yii::app()->clientScript->registerScript('js',$js,CClientScript::POS_READY);
?>   
<div style='display:none;'>
<?php
    $this->widget('MyDateTimePicker', array(
        'name'=>'testingkktest',
        'mode' => 'datetime',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
            'maxDate' => 'd',
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)", 'id'=>'RJTindakanPelayananT_0_tgl_tindakan'),
    ));
?>
</div>

<?php
/* DINONAKTIFKAN : RND-7536 / EHJ-3488
 * PAKET LUAR */
//$this->beginWidget('zii.widgets.jui.CJuiDialog',
//    array(
//        'id'=>'dialogTindakanPaketLuar',
//        'options'=>array(
//            'title'=>'Daftar Tindakan',
//            'autoOpen'=>false,
//            'modal'=>true,
//            'width'=>800,
//            'height'=>500,
//            'resizable'=>false,
//        ),
//    )
//);
//
//$tindakanPaketLuar = new PaketpelayananV;
//if(Yii::app()->user->getState('tindakanruangan'))
//    $tindakanPaketLuar->ruangan_id = Yii::app()->user->getState('ruangan_id');
//
//if(Yii::app()->user->getState('tindakankelas'))
//    //$tindakanPaketLuar->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//    $tindakanPaketLuar->kelaspelayanan_id = 2;
//
//$tindakanPaketLuar->tipepaket_id = Params::TIPEPAKET_LUARPAKET;
//
//if (isset($_GET['PaketpelayananV']))
//{
//    $tindakanPaketLuar->attributes = $_GET['PaketpelayananV'];
//}
//
//$this->widget('ext.bootstrap.widgets.BootGridView',
//    array(
//        'id'=>'tindakanLuarPaket',
//        'dataProvider'=>$tindakanPaketLuar->search(),
//        'filter'=>$tindakanPaketLuar,
//        'template'=>"{pager}{summary}\n{items}",
//        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//        'columns'=>array(
//            array(
//                'header'=>'Pilih',
//                'type'=>'raw',
//                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small","id" => "selectObat","onClick" => "setTindakanAuto($data->kelaspelayanan_id,$data->daftartindakan_id);return false;"))',
//            ),
//            'kategoritindakan_nama',
//            array(
//                'header'=>'Nama Tindakan',
//                'name'=>'daftartindakan_nama',
//            ),
//        ),
//        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
//    )
//);
//
//$this->endWidget('zii.widgets.jui.CJuiDialog');
?>




<?php
/* DINONAKTIFKAN : RND-7536 / EHJ-3488
 * NON PAKET */
//$this->beginWidget('zii.widgets.jui.CJuiDialog',
//    array(
//        'id'=>'dialogTindakanNonPaket',
//        'options'=>array(
//            'title'=>'Daftar Tindakan',
//            'autoOpen'=>false,
//            'modal'=>true,
//            'width'=>800,
//            'height'=>500,
//            'resizable'=>false,
//        ),
//    )
//);
//
//if(Yii::app()->user->getState('tindakanruangan'))
//{
//    $tindakanPaketLuar = new TariftindakanperdaruanganV;
//    $tindakanPaketLuar->ruangan_id = Yii::app()->user->getState('ruangan_id');
//} else {
//    $tindakanPaketLuar = new TariftindakanperdaV;
//}
//
//if(Yii::app()->user->getState('tindakankelas'))
//    $tindakanPaketLuar->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//
//if (isset($_GET['TariftindakanperdaruanganV']))
//{
//    $tindakanPaketLuar->attributes = $_GET['TariftindakanperdaruanganV'];
//}
//
//$this->widget('ext.bootstrap.widgets.BootGridView',
//    array(
//        'id'=>'tindakanLuarPaket',
//        'dataProvider'=>$tindakanPaketLuar->search(),
//        'filter'=>$tindakanPaketLuar,
//        'template'=>"{pager}{summary}\n{items}",
//        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//        'columns'=>array(
//            array(
//                'header'=>'Pilih',
//                'type'=>'raw',
//                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small","id" => "selectObat","onClick" => "setTindakanAuto($data->kelaspelayanan_id,$data->daftartindakan_id);return false;"))',
//            ),
//            'kategoritindakan_nama',
//            array(
//                'header'=>'Nama Tindakan',
//                'name'=>'daftartindakan_nama',
//            ),
//        ),
//        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
//    )
//);
//
//$this->endWidget('zii.widgets.jui.CJuiDialog');

?>
