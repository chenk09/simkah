<?php
/**
 * KHUSUS UNTUK MENYIMPAN FUNGSI-FUNGSI JAVASCRIPT
 */

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'.',
        'thousands'=>',',
        'precision'=>2,
    )
));
?>
<?php 
    $modNewRekenings = array();
    $modNewRekenings[0] = new RekeningakuntansiV;
?>
<script type="text/javascript">
    var trRekening=new String(<?php echo CJSON::encode($this->renderPartial('rawatJalan.views.tindakan.rekening._rowRekening',array('modRekenings'=>$modNewRekenings,'removeButton'=>true),true));?>);
    function setDialogRekening(obj){
        var row = $(obj).parents('tr').find('#row').val();
        $('#dialogRekDebitKredit #row').val(row);
        $('#dialogRekDebitKredit').dialog('open');
    }
    function editDataRekeningFromGrid(params, row){
        $("#RekeningakuntansiV_"+row+"_struktur_id").val(params.struktur_id);
        $("#RekeningakuntansiV_"+row+"_kelompok_id").val(params.kelompok_id);
        $("#RekeningakuntansiV_"+row+"_jenis_id").val(params.jenis_id);
        $("#RekeningakuntansiV_"+row+"_obyek_id").val(params.obyek_id);
        $("#RekeningakuntansiV_"+row+"_rincianobyek_id").val(params.rincianobyek_id);
        
        $("#RekeningakuntansiV_"+row+"_kdstruktur").val(params.kdstruktur);
        $("#RekeningakuntansiV_"+row+"_kdkelompok").val(params.kdkelompok);
        $("#RekeningakuntansiV_"+row+"_kdjenis").val(params.kdjenis);
        $("#RekeningakuntansiV_"+row+"_kdobyek").val(params.kdobyek);
        $("#RekeningakuntansiV_"+row+"_kdrincianobyek").val(params.kdrincianobyek);
        $("#RekeningakuntansiV_"+row+"_rekDebitKredit").val(params.nmrincianobyek);
        $("#RekeningakuntansiV_"+row+"_nama_rekening").val(params.nmrincianobyek);
    }
    
    function addDataRekening(obj){
        $("#tblInputRekening > tbody").append(trRekening.replace());
        $("#tblInputRekening > tbody > tr:last").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                            $(this).val("");
                                                                            return false;
                                                                        },'select':function( event, ui ) {
                                                                            $(this).val(ui.item.value);
                                                                            var data = {
                                                                                rincianobyek_id:ui.item.rincianobyek_id,
                                                                                obyek_id:ui.item.obyek_id,
                                                                                jenis_id:ui.item.jenis_id,
                                                                                kelompok_id:ui.item.kelompok_id,
                                                                                struktur_id:ui.item.struktur_id,
                                                                                nmrincianobyek:ui.item.nmrincianobyek,
                                                                                kdstruktur:ui.item.kdstruktur,
                                                                                kdkelompok:ui.item.kdkelompok,
                                                                                kdjenis:ui.item.kdjenis,
                                                                                kdobyek:ui.item.kdobyek,
                                                                                kdrincianobyek:ui.item.kdrincianobyek,
                                                                                saldodebit:ui.item.saldodebit,
                                                                                saldokredit:ui.item.saldokredit,
                                                                                status:"debit"
                                                                            };
                                                                            var row = $(this).parents("tr").find("#row").val();
                                                                            editDataRekeningFromGrid(data, row);
                                                                            return false;
                                                                        },'source':function(request, response) {
                                                                                        $.ajax({
                                                                                            url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                                                                            dataType: "json",
                                                                                            data: {
                                                                                                term: request.term,
                                                                                            },
                                                                                            success: function (data) {
                                                                                                response(data);
                                                                                            }
                                                                                        })
                                                                                    }
                                                                        });  
        $("#tblInputRekening > tbody > tr:last").find('.uncurrency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
        //uncurrency agar tidak duakali maskMoney krn bisa error input
        $("#tblInputRekening > tbody").find('.uncurrency').addClass('currency');
        $("#tblInputRekening > tbody").find('.currency').removeClass('uncurrency');
        //copy daftartindakan_id, obatalkes_id dan jnspelayanan
        var jnspelayanan = $(obj).parents('tr').find("input[name$='[jnspelayanan]']").val();
        var daftartindakanId = $(obj).parents('tr').find("input[name$='[daftartindakan_id]']").val();
        var obatalkesId = $(obj).parents('tr').find("input[name$='[obatalkes_id]']").val();
        $("#tblInputRekening > tbody > tr:last").find("input[name$='[jnspelayanan]']").val(jnspelayanan);
        $("#tblInputRekening > tbody > tr:last").find("input[name$='[daftartindakan_id]']").val(daftartindakanId);
//      DIGABUNG KE daftartindakan_id >>>  $("#tblInputRekening > tbody > tr:last").find("input[name$='[obatalkes_id]']").val(obatalkesId);
        renameRowRekening();
//        setTimeout(function(){renameRowRekening()},500);
    }
    
    //getDataRekening untuk load data rekening berdasarkan pelayanan yang ditambahkan
    function getDataRekening(daftartindakan_id, kelaspelayanan_id, qty, jnspelayanan){
        //Mas Ichan di comment dulu ya soalnya datanya gak muncul2 :) by Mitha
//        $("#formJurnalRekening").addClass("srbacLoading");
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/GetDataRekeningPelayanan');?>', {daftartindakan_id:daftartindakan_id, kelaspelayanan_id:kelaspelayanan_id, qty:qty, jnspelayanan:jnspelayanan},
            function(data){
                if(data == null){
//                    alert("Tidak memiliki rekening !");
                }else{
                    $("#tblInputRekening > tbody").append(data.replace());
                    $("#tblInputRekening > tbody").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                        $(this).val("");
                                                                                        return false;
                                                                                    },'select':function( event, ui ) {
                                                                                        $(this).val(ui.item.value);
                                                                                        var data = {
                                                                                            rincianobyek_id:ui.item.rincianobyek_id,
                                                                                            obyek_id:ui.item.obyek_id,
                                                                                            jenis_id:ui.item.jenis_id,
                                                                                            kelompok_id:ui.item.kelompok_id,
                                                                                            struktur_id:ui.item.struktur_id,
                                                                                            nmrincianobyek:ui.item.nmrincianobyek,
                                                                                            kdstruktur:ui.item.kdstruktur,
                                                                                            kdkelompok:ui.item.kdkelompok,
                                                                                            kdjenis:ui.item.kdjenis,
                                                                                            kdobyek:ui.item.kdobyek,
                                                                                            kdrincianobyek:ui.item.kdrincianobyek,
                                                                                            saldodebit:ui.item.saldodebit,
                                                                                            saldokredit:ui.item.saldokredit,
                                                                                            status:"debit"
                                                                                        };
                                                                                        var row = $(this).parents("tr").find("#row").val();
                                                                                        editDataRekeningFromGrid(data, row);
                                                                                        return false;
                                                                                    },'source':function(request, response) {
                                                                                                    $.ajax({
                                                                                                        url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                                                                                        dataType: "json",
                                                                                                        data: {
                                                                                                            term: request.term,
                                                                                                        },
                                                                                                        success: function (data) {
                                                                                                            response(data);
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                    });  
                    $("#tblInputRekening > tbody").find('.uncurrency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
                    //uncurrency agar tidak duakali maskMoney krn bisa error input
                    $("#tblInputRekening > tbody").find('.uncurrency').addClass('currency');
                    $("#tblInputRekening > tbody").find('.currency').removeClass('uncurrency');
//                    setTimeout(function(){renameRowRekening()},1000);
                    renameRowRekening();
                }
              $("#formJurnalRekening").removeClass("srbacLoading");  
            }, "json");
            return false;
    }

    function renameRowRekening(){
        var i = 0;
        var namaModel = "RekeningakuntansiV";
        $("#tblInputRekening > tbody").find('tr').each(
            function()
            {
                $(this).find('#noUrut').val(i+1);
                $(this).find('#row').val(i);
                $(this).find('.currency').each(function(){this.value = formatDesimal(this.value)});

                $(this).find('input[name$="[kdstruktur]"]').attr('name',namaModel+'['+i+'][kdstruktur]')
                        .attr('id',namaModel+'_'+i+'_kdstruktur');
                $(this).find('input[name$="[struktur_id]"]').attr('name',namaModel+'['+i+'][struktur_id]')
                        .attr('id',namaModel+'_'+i+'_struktur_id');
                $(this).find('input[name$="[kdkelompok]"]').attr('name',namaModel+'['+i+'][kdkelompok]')
                        .attr('id',namaModel+'_'+i+'_kdkelompok');
                $(this).find('input[name$="[kelompok_id]"]').attr('name',namaModel+'['+i+'][kelompok_id]')
                        .attr('id',namaModel+'_'+i+'_kelompok_id');
                $(this).find('input[name$="[kdjenis]"]').attr('name',namaModel+'['+i+'][kdjenis]')
                        .attr('id',namaModel+'_'+i+'_kdjenis');
                $(this).find('input[name$="[jenis_id]"]').attr('name',namaModel+'['+i+'][jenis_id]')
                        .attr('id',namaModel+'_'+i+'_jenis_id');
                $(this).find('input[name$="[kdobyek]"]').attr('name',namaModel+'['+i+'][kdobyek]')
                        .attr('id',namaModel+'_'+i+'_kdobyek');
                $(this).find('input[name$="[obyek_id]"]').attr('name',namaModel+'['+i+'][obyek_id]')
                        .attr('id',namaModel+'_'+i+'_obyek_id');
                $(this).find('input[name$="[kdrincianobyek]"]').attr('name',namaModel+'['+i+'][kdrincianobyek]')
                        .attr('id',namaModel+'_'+i+'_kdrincianobyek');
                $(this).find('input[name$="[rincianobyek_id]"]').attr('name',namaModel+'['+i+'][rincianobyek_id]')
                        .attr('id',namaModel+'_'+i+'_rincianobyek_id');
                $(this).find('input[name$="[nama_rekening]"]').attr('name',namaModel+'['+i+'][nama_rekening]')
                        .attr('id',namaModel+'_'+i+'_nama_rekening');
                $(this).find('input[name$="[daftartindakan_id]"]').attr('name',namaModel+'['+i+'][daftartindakan_id]')
                        .attr('id',namaModel+'_'+i+'_daftartindakan_id');
                $(this).find('input[name$="[obatalkes_id]"]').attr('name',namaModel+'['+i+'][obatalkes_id]')
                        .attr('id',namaModel+'_'+i+'_obatalkes_id');
                $(this).find('input[name$="[jnspelayanan]"]').attr('name',namaModel+'['+i+'][jnspelayanan]')
                        .attr('id',namaModel+'_'+i+'_jnspelayanan');
                $(this).find('input[name$="[rekDebitKredit]"]').attr('name',namaModel+'['+i+'][rekDebitKredit]')
                        .attr('id',namaModel+'_'+i+'_rekDebitKredit');
                $(this).find('input[name$="[saldodebit]"]').attr('name',namaModel+'['+i+'][saldodebit]')
                        .attr('id',namaModel+'_'+i+'_saldodebit');
                $(this).find('input[name$="[saldokredit]"]').attr('name',namaModel+'['+i+'][saldokredit]')
                        .attr('id',namaModel+'_'+i+'_saldokredit');
                                
                i++;

                

            }
        );
    }
    //rekening dihapus perbaris
    function removeDataRekening(obj)    {
        var namaRekening = $(obj).parents("tr").find("input[name$='[nama_rekening]']").val();
        if(confirm("Apakah anda akan menghapus rekening '"+namaRekening+"' ?") == true){
            $(obj).parent().parent('tr').detach();
        }
        setTimeout(function(){renameRowRekening()},500);
    }
    //rekening dihapus jika tindakan dihapus
    function removeRekeningTindakan(daftartindakan_id){
        $("#tblInputRekening > tbody > tr").each(function(){
            if($(this).find('input[name$="[daftartindakan_id]"]').val() == daftartindakan_id){
                $(this).detach();
            }
        });
    }
    //rekening dihapus jika obat dihapus
    function removeRekeningObat(obatalkes_id){
        $("#tblInputRekening > tbody > tr").each(function(){
            if($(this).find('input[name$="[obatalkes_id]"]').val() == obatalkes_id){
                $(this).detach();
            }
        });
    }

    //ubah saldo rekening tindakan
    function updateRekeningTindakan(daftartindakan_id, saldo){
        // bugs EHJ-1904
       /* $("#tblInputRekening > tbody > tr").each(function(){
            if($(this).find('input[name$="[daftartindakan_id]"]').val() == daftartindakan_id){
                if($(this).find('input[name$="[saldonormal]"]').val() == "D"){
                    $(this).find('input[name$="[saldodebit]"]').val(saldo);
                }else{
                    $(this).find('input[name$="[saldokredit]"]').val(saldo);
                }
            }
        });*/
    }
    //ubah saldo rekening obat
    function updateRekeningObat(obatalkes_id, saldo){
        $("#tblInputRekening > tbody > tr").each(function(){
            if($(this).find('input[name$="[obatalkes_id]"]').val() == obatalkes_id){
                if($(this).find('input[name$="[saldonormal]"]').val() == "D"){
                    $(this).find('input[name$="[saldodebit]"]').val(saldo);
                }else{
                    $(this).find('input[name$="[saldokredit]"]').val(saldo);
                }
            }
        });
    }
</script>
<?php 
Yii::app()->clientScript->registerScript('renameOnload', "renameRowRekening();", CClientScript::POS_READY);
?>