<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
	if(!is_null($pendaftaran->pembayaranpelayanan_id))
	{
		echo '<div class="alert alert-block alert-error">Pasien sudah melakukan pembayaran</div>';
	}else{
?>
	<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'pasienpulang-t-form',
		'enableAjaxValidation'=>false,
		'type'=>'horizontal',
		'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
		'focus'=>'#',
	)); ?>
	<?php echo $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$pendaftaran,'modPasien'=>$pasienM));?> 
	<legend class="rim">Kirim ke farmasi</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary(array($model)); ?>
	<table width="100%">
		<tr>
			<td>
				<?php echo $form->textAreaRow($model,'komentar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
			</td>
		</tr>
	</table>
	<div class="form-actions">
		<?php
			echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
			echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));
		?>
	</div>
	<?php $this->endWidget(); ?>
<?php
	}
?>