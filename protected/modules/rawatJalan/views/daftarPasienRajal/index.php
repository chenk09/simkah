<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/socket.io.js'); ?>
<style>
    .row-red td{
        background-color: indianred !important;
        color: #ffffff;
    }
</style>

<fieldset>
    <legend class="rim2">Informasi Pasien Rawat Jalan</legend>
</fieldset>
<style>
    .icon{
        cursor : pointer;
    }
</style>

<?php
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlTindakLanjut = Yii::app()->createUrl('actionAjax/pasienRujukRI');
$urlTindakLanjutPulang = Yii::app()->createUrl('actionAjax/pasienRujukRI');
$urlBatalPulang = Yii::app()->createUrl('rawatJalan/daftarPasien/batalPulang');

Yii::app()->clientScript->registerScript('search', "
$(document).ready(function(){
	$('#daftarpasien-v-search-form').submit(function(){
		$.fn.yiiGridView.update('daftarpasien-v-grid', {
			data: $(this).serialize()
		});
		return false;
	});
});

function tindaklanjutrawatjalan(id){
	idPendaftaran = id;
	var answer = confirm('Yakin Pasien Akan Ditindak Lanjut ke Rawat Inap?');
	if (answer){
		$.post('${urlTindakLanjut}',{idPendaftaran:idPendaftaran}, function(data){
			$.fn.yiiGridView.update('daftarpasien-v-grid');
		});
	}
}

function tindaklanjutpulang(id){
	idPendaftaran = id;
	var answer = confirm('Yakin pasien akan dipulangkan?');
	if (answer){
		$(\"#dialogPasienPulang\").dialog(\"open\");
	}
}

function ubahstatusperiksa(id){
	idPendaftaran = id;
	var answer = confirm('Yakin Pasien Akan Dirubah Status Periksa?');
	if (answer){
		$(\"#dialogUbahStatus\").dialog(\"open\");
	}
}

function setBatalPulang(obj)
{
	var pendaftaran_id = $(obj).attr('pendaftaran_id');
	$.post('$urlBatalPulang',{pendaftaran_id:pendaftaran_id}, function(data){
	}, 'json');
	return false;
}

",  CClientScript::POS_HEAD);
?>
<?php
$js = <<< JS
$('#cekRiwayatPasien').change(function(){
        $('#divRiwayatPasien').slideToggle(500);
});
JS;
Yii::app()->clientScript->registerScript('JSriwayatPasien',$js,CClientScript::POS_READY);
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarpasien-v-grid',
	'dataProvider'=>$model->searchPasienByKonfirmasi(),
	'template'=>"{pager}{summary}\n{items}",
	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'rowCssClassExpression'=>'($data->statusfarmasi == "Sudah" ? "row-red" : "row-white" )',
	'columns'=>array(
//		'no_urutantri',
                array(
			'name'=>'no_urutantri',
			'type'=>'raw',
			'value'=>'"$data->no_urutantri"."<br/>".CHtml::htmlButton(Yii::t("mds","{icon}",array("{icon}"=>"<i class=\'icon-volume-up icon-white\'></i>")),array("class"=>"btn btn-primary antrian","onclick"=>"panggilAntrian(\"$data->pendaftaran_id\");","rel"=>"tooltip","title"=>"Klik untuk memanggil<br>pasien antrian ini"))',
                        'htmlOptions'=>array('style'=>'text-align:center;')
		),
		'tgl_pendaftaran',
		array(
			'name'=>'No_pendaftaran'.'/<br/>'.'No_rekam_medik',
			'type'=>'raw',
			'value'=>'"$data->no_pendaftaran"."<br/>"."$data->no_rekam_medik"',
		),
		array(
			'name'=>'nama_pasien'.'/<br/>'.'Alias',
			'type'=>'raw',
			'value'=>'"$data->nama_pasien"."<br/>"."$data->nama_bin"',
		),
		array(
			'name'=>'alamat_pasien'.'/<br/>'.'RT RW',
			'type'=>'raw',
			'value'=>'"$data->alamat_pasien"."<br/>"."$data->RTRW"',
		),
		array(
			'name'=>'Penjamin'.'/<br/>'.'Cara Bayar',
			'type'=>'raw',
			'value'=>'"$data->penjamin_nama"."<br/>"."$data->carabayar_nama"',
		),
		 array(
			'header'=>'Dokter / <br> Kelas Pelayanan',
			'type'=>'raw',
			'value'=>'"<div style=\'width:100px;\'>" . CHtml::link("<i class=icon-pencil-brown></i> ". $data->gelardepan." ".$data->nama_pegawai." ".$data->gelarbelakang_nama," ",array("onclick"=>"ubahDokterPeriksa(\'$data->pendaftaran_id\');$(\'#editDokterPeriksa\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Data Dokter Periksa")) . "</div>"."<br/>"."$data->kelaspelayanan_nama"',
			'htmlOptions'=>array(
			   'style'=>'text-align:center;',
			   'class'=>'rajal'
		   )
		),
		array(
			'header'=>'Kasus Penyakit ',
			'type'=>'raw',
			'value'=>'CHtml::hiddenField("RJInfokunjunganrV[$data->pendaftaran_id][idPendaftaran]", $data->pendaftaran_id, array("id"=>"idPendaftaran","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span3"))."".$data->jeniskasuspenyakit_nama',
		),
		array(
			'header'=>'Status Periksa',
			'type'=>'raw',
			'value'=>'RJInfokunjunganrjV::getStatus($data->statusperiksa,$data->pendaftaran_id)',
			'htmlOptions'=>array(
				'style'=>'text-align: center',
				'class'=>'status'
			)
		),
		array(
			'name'=>'Periksa Pasien',
			'type'=>'raw',
			'value'=>'RJInfokunjunganrjV::getPeriksaPasien($data->statusperiksa,$data->pendaftaran_id,$data->pendaftaran->pembayaranpelayanan_id,$data->no_pendaftaran,$data->pendaftaran->alihstatus)',
			'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
		),
		array(
			'name'=>'Rawat Inap',
			'type'=>'raw',
			'value'=>'RJInfokunjunganrjV::getTindakLanjut($data->statusperiksa,$data->pendaftaran_id,$data->pendaftaran->pembayaranpelayanan_id,
						$data->no_pendaftaran,$data->pendaftaran->pasienpulang_id,$data->pendaftaran->alihstatus)',
			'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
		),
		// array(
		// 	'name'=>'Kirim Ke Farmasi',
		// 	'type'=>'raw',
		// 	'value'=>'RJInfokunjunganrjV::getKirimFarmasi($data)',
		// 	'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
		// ),
        array(
            'name'=>'Pemulangan Pasien',
            'type'=>'raw',
            'value'=>'RJInfokunjunganrjV::getTindakPulangNew($data)',
            'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
        ),
		array(
		   'header'=>'Rencana Kontrol',
		   'type'=>'raw',
		   'value'=>'((!empty($data->tglrenkontrol)) ? $data->tglrenkontrol.CHtml::link("<i class=\'icon-time\'></i> ",
					Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/RencanaKontrolPasienRJ",array("idPendaftaran"=>$data->pendaftaran_id)) ,
					array("title"=>"Klik Untuk Rencana Kontrol Pasien","target"=>"iframeRencanaKontrol", "onclick"=>"cekRenControl(event)", "rel"=>"tooltip")) : CHtml::link("<i class=\'icon-time\'></i> ",
					Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/RencanaKontrolPasienRJ",array("idPendaftaran"=>$data->pendaftaran_id)) ,
					array("title"=>"Klik Untuk Rencana Kontrol Pasien","target"=>"iframeRencanaKontrol", "onclick"=>"$(\"#dialogRencanaKontrol\").dialog(\"open\");", "rel"=>"tooltip")))',
		   'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
		),
        "statusFarmasi"
	),
	'afterAjaxUpdate'=>'function(id, data){
		jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
		disableLink();
	}',
)); ?>
<?php $this->renderPartial('_search',array('model'=>$model)); ?>

<?php
// Dialog untuk rencana kontrol =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogRencanaKontrol',
    'options'=>array(
        'title'=>'Rencana Kontrol',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>300,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRencanaKontrol" width="100%" height="300"></iframe>
<?php
$this->endWidget();
//========= end rencana kontrol dialog =============================
?>

<?php
// Dialog untuk ubah status periksa =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogUbahStatus',
    'options'=>array(
        'title'=>'Ubah Status Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>500,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end ubah status periksa dialog =============================
?>


<?php
    //======================= Edit Dokter Periksa =======================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editDokterPeriksa',
            'options'=>array(
                'title'=>'Ganti Dokter Periksa',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_idPendaftaranDP','',array('readonly'=>true));
    echo '<div class="divForFormEditDokterPeriksa"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>



<?php
// Dialog untuk ubah status periksa =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogUbahStatusPasien',
    'options'=>array(
        'title'=>'Ubah Status Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>500,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end ubah status periksa dialog =============================
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'loginDialog',
    'options'=>array(
        'title'=>'Login',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>250,
        'resizable'=>false,
    ),
));?>
<div class="alert alert-block alert-error" id="alertDiv" style="display : none;">
    Kesalahan dalam Pengisian Usename atau Password
</div>
<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formLogin')); ?>
    <div class="control-group ">
        <?php echo CHtml::label('Login Pemakai','username', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::textField('username', '', array()); ?>
        </div>
    </div>

    <div class="control-group ">
        <?php echo CHtml::label('Password','password', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::passwordField('password', '', array()); ?>
        </div>
    </div>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Login',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'cekLogin();return false;')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>'batal();return false;')); ?>
    </div>
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>


<script type="text/javascript">


function disableLink()
{
    var status = null;
    $("#daftarpasien-v-grid tbody").find('tr > td[class="rajal"]').each(
        function()
        {
            status = $(this).parent().find('td[class="status"]');
            var xxx = $(this).find('a');
            if(status.text() == 'SUDAH PULANG')
            {
               $(this).text($.trim(xxx.text()));
               $(this).find('a').remove();
            }
        }
    );
}
disableLink();
</script>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogAlasan',
    'options'=>array(
        'title'=>'Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>500,
        'height'=>200,
        'resizable'=>false,
    ),
));
?>
<div id="divFormDataPasien"></div>


<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formAlasan')); ?>
<table>
    <tr>
        <td><?php echo CHtml::label('Alasan','Alasan', array('class'=>'')) ?></td>
        <td>
            <?php echo CHtml::textArea('Alasan', '', array()); ?>
            <?php echo CHtml::hiddenField('idOtoritas', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('namaOtoritas', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('idPasienPulang', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('idPendaftaran', '', array('readonly'=>TRUE)); ?>

        </td>
    </tr>
</table>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'simpanAlasan();return false;')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>'batal();return false;')); ?>    </div>
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'konfirmasiDialog',
    'options'=>array(
        'title'=>'Konfirmasi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>190,
        'resizable'=>false,
    ),
));?>
<div align="center">
    User Tidak Memiliki Akses Untuk Proses Ini,<br/>
    Yakin Akan Melakukan Ke Proses Selanjutnya ?
</div>
<div class="form-actions" align="center">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Yes',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'button', 'onclick'=>"$('#loginDialog').dialog('open');$('#konfirmasiDialog').dialog('close');")); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} No',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>"$('#konfirmasiDialog').dialog('close');")); ?>    </div>

<?php $this->endWidget();?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'konfirmasiAdmisi',
    'options'=>array(
        'title'=>'Konfirmasi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>420,
        'height'=>200,
        'resizable'=>false,
    ),
));?>
<div align="center">
    Pasien sudah di rawat di ruangan <div id="ruanganPasien"></div>
    Anda tidak bisa melakukan pembatalan disini,<br/>
    Silahkan hubungi petugas Rawat Inap yang bersangkutan ?
</div>
<div id=""></div>
<div class="form-actions" align="center">
       <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Yes',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'button', 'onclick'=>"$('#konfirmasiAdmisi').dialog('close');")); ?>  </div>

<?php $this->endWidget();?>
<script type="text/javascript">
function cekRenControl(event){
    var answer = confirm('Yakin Akan Merubah Tgl Rencana Kontrol?');
        if (answer){
           $("#dialogRencanaKontrol").dialog("open");
        }
        else
        {
            event.preventDefault();
        }
}
function batal(){
    $('#loginDialog').dialog('close');
    $('#loginDialog #username').val('');
    $('#loginDialog #password').val('');
    $('#alertDiv').hide();
    $('#idPasien').val('');
    $('#idPendaftaran').val('');

    $('#dialogAlasan').dialog('close');
    $('#dialogAlasan #idOtoritas').val('');
    $('#dialogAlasan #namaOtoritas').val('');
    $('#dialogAlasan #idPasienPulang').val('');
    $('#dialogAlasan #idPendaftaran').val('');

    $.fn.yiiGridView.update('daftarpasien-v-grid', {
        data: $('#daftarPasienPulang-form').serialize()
    });
}

function cekHakAkses(idPendaftaran)
{
//       $('#dialogAlasan #idPasienPulang').val(idPasienPulang);
//       $('#dialogAlasan #idPendaftaran').val(idPendaftaran);
//       $('#idPasien').val(idPasien);
//       $('#idPendaftaran').val(idPendaftaran);

//    $('#konfirmasiDialog').dialog('open');

    $.post('<?php echo Yii::app()->createUrl('rawatJalan/ActionAjax/CekHakAkses');?>',
    {idPendaftaran:idPendaftaran, idUser:'<?php echo Yii::app()->user->id; ?>',useName:'<?php echo Yii::app()->user->name; ?>'}, function(data){
//        console.log(data);
     var cekAdmisi = data.pendaftaran.pasienadmisi_id;

     if(cekAdmisi){
         $('#konfirmasiAdmisi').dialog('open');
          $('#konfirmasiAdmisi #ruanganPasien').html(data.ruanganPasien);
     }else{
        // $('#konfirmasiDialog').dialog('open');
        if(data.cekAkses==true){
            $('#dialogAlasan').dialog('open');
            $('#dialogAlasan #idOtoritas').val(data.userid);
            $('#dialogAlasan #namaOtoritas').val(data.username);
        } else {
            $('#konfirmasiDialog').dialog('open');
        }
     }
       $('#dialogAlasan #idPasienPulang').val(data.pendaftaran.pasienpulang_id);
       $('#dialogAlasan #idPendaftaran').val(data.pendaftaran.pendaftaran_id);
       $('#idPasien').val(data.pendaftaran.pasien_id);
       $('#idPendaftaran').val(data.pendaftaran.pendaftaran_id);
    }, 'json');
}

function cekLogin()
{
    idPasien = $('#idPasien').val();
    idPendaftaran = $('#idPendaftaran').val();
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/CekLoginPembatalRawatInap');?>', $('#formLogin').serialize(), function(data){
        if(data.error != '')
        $('#'+data.cssError).addClass('error');
        if(data.status=='success'){
              $.post('<?php echo Yii::app()->createUrl('rawatJalan/ActionAjax/dataPasien');?>', {idPasien:idPasien ,idPendaftaran:idPendaftaran}, function(dataPasien){

              $('#divFormDataPasien').html(dataPasien.form);

             }, 'json');

            $('#dialogAlasan').dialog('open');
            $('#dialogAlasan #idOtoritas').val(data.userid);
            $('#dialogAlasan #namaOtoritas').val(data.username);
            $('#loginDialog').dialog('close');
        }else{
    $('#alertDiv').show();
        }
    }, 'json');
}


function ubahDokterPeriksa(idPendaftaran)
{
    $('#temp_idPendaftaranDP').val(idPendaftaran);
    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahDokterPeriksa')?>',
        'data':$(this).serialize(),
        'type':'post',
        'dataType':'json',
        'success':function(data){
            if (data.status == 'create_form') {
                $('#editDokterPeriksa div.divForFormEditDokterPeriksa').html(data.div);
                $('#editDokterPeriksa div.divForFormEditDokterPeriksa form').submit(ubahDokterPeriksa);
            }else{
                $('#editDokterPeriksa div.divForFormEditDokterPeriksa').html(data.div);
                $.fn.yiiGridView.update('daftarpasien-v-grid', {
                        data: $(this).serialize()
                });
                setTimeout("$('#editDokterPeriksa').dialog('close') ",500);
            }
        },
        'cache':false
    });
    return false;
}

function simpanAlasan()
{
    alasan =$('#dialogAlasan #Alasan').val();
    if(alasan==''){
        alert('Anda Belum Mengisi Alasan Pembatalan');
    }else{
        $.post('<?php echo Yii::app()->createUrl('rawatJalan/daftarPasien/BatalRawatInap');?>', $('#formAlasan').serialize(), function(data){
//            if(data.error != '')
//                alert(data.error);
//            $('#'+data.cssError).addClass('error');
            // if(data.status=='success'){
                alert('Data Berhasil Disimpan');

                $('#dialogAlasan').dialog('close');
                $.fn.yiiGridView.update('daftarpasien-v-grid', {
                    data: $('#daftarPasienPulang-form').serialize()
                });
            // }else{
            //     alert(data.status);
            // }
        }, 'json');
   }
}

</script>
<script>
        function ubahStatusPeriksa()
{
    <?php
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjaxRIRD/ubahStatusPeriksaRJ'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogUbahStatus div.divForForm').html(data.div);
                    $('#dialogUbahStatus div.divForForm form').submit(ubahStatusPeriksa);

                    jQuery('.dtPicker3').datetimepicker(jQuery.extend({showMonthAfterYear:false},
                    jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','maxDate'  : 'd','timeText':'Waktu','hourText':'Jam',
                         'minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih   Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));

                }
                else
                {
                    $('#dialogUbahStatus div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('daftarpasien-v-grid');
                    setTimeout(\"$('#dialogUbahStatus').dialog('close') \",1000);
                }

            } ",
    ))
?>;
    return false;
}




</script>
<script type="text/javascript">
        $('document').ready(function(){
			$('#dialogPasienPulang').on('dialogclose', function( event, ui ){
				$.fn.yiiGridView.update('daftarpasien-v-grid', {
					data: $('#daftarpasien-v-search-form').serialize()
				});
			});

			$('#dialogPasienBatalPulang').on('dialogclose', function( event, ui ){
				$.fn.yiiGridView.update('daftarpasien-v-grid', {
					data: $('#daftarpasien-v-search-form').serialize()
				});
			});

            $('#daftarpasien-v-grid button').each(function(){
                $('#orange').removeAttr('class');
                $('#red').removeAttr('class');
                $('#green').removeAttr('class');
                $('#blue').removeAttr('class');

                $('#orange').attr('class','btn btn-danger-blue');
                $('#red').attr('class','btn btn-primary');
                $('#green').attr('class','btn btn-danger');
                $('#blue').attr('class','btn btn-danger-yellow');
            });

        });
        function setStatus(obj,status,idpendaftaran){
            var status = status;
            var idpendaftaran = idpendaftaran;
            var answer = confirm('Yakin Akan Merubah Status Periksa Pasien?');
            if (answer){
    //            alert(status);
    //            alert(idpendaftaran);
                  $.post('<?php echo Yii::app()->createUrl('ActionAjaxRIRD/UbahStatusPeriksaPasien');?>', {status:status ,idpendaftaran:idpendaftaran}, function(data){
                    if(data.status == 'proses_form'){

                            $('#dialogUbahStatusPasien div.divForForm').html(data.div);
                            $.fn.yiiGridView.update('daftarpasien-v-grid');
                            setTimeout("$('#dialogUbahStatus').dialog('close')",1000);
                    }else{
                        $('#alertDiv').show();
                    }
                }, 'json');
            }else{
                preventDefault();
            }
        }

        function cekStatus(status){
            var status = status;
            alert("Pasien "+status+" Tidak bisa melanjutkan pemeriksaan atau tindak lanjut");
        }
        
    /**
    * memanggil antrian ke poliklinik
    * @param {type} pendaftaran_id
    * @returns {undefined} */
    function panggilAntrian(pendaftaran_id){
        var chatServer='<?php echo Yii::app()->user->getState("nodejs_host") ?>';
        if (chatServer == ''){
         chatServer='http://localhost';
        }
        var chatPort='<?php echo Yii::app()->user->getState("nodejs_port") ?>';
        socket = io.connect(chatServer+':'+chatPort);
        
        $('.antrian').attr("disabled",true);
        $.ajax({
           type:'POST',
           url:'<?php echo Yii::app()->createUrl('pendaftaranPenjadwalan/TampilAntrian/Panggil'); ?>',
           data: {pendaftaran_id:pendaftaran_id},
           dataType: "json",
           success:function(data){
               if(data.pesan == "GAGAL"){
                   alert("Gagal panggil pasien");
                   return false;
               } 
               if(data.pesan == "OK"){
                    socket.emit('send',{conversationID:'antrianPoli',pendaftaran_id:pendaftaran_id});
                    setTimeout(function(){
                        $('.antrian').removeAttr("disabled");
                    },5000); //5 detik tombol baru aktif
               }
               if(data.pesan == "SUDAH"){
                    if(confirm("Antrian sudah dipaggil. Apakah akan mengulang ?")){
                        socket.emit('send',{conversationID:'antrianPoli',pendaftaran_id:pendaftaran_id});
                        setTimeout(function(){
                            $('.antrian').removeAttr("disabled");
                        },5000); //5 detik tombol baru aktif
                    }else{
                        $('.antrian').removeAttr("disabled");
                        return false;
                    }
                }
              
           },
           error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
       });
    }

</script>
<?php
$urlSessionUbahStatus = Yii::app()->createUrl('ActionAjaxRIRD/buatSessionUbahStatus ');
$urlBatalPulang = Yii::app()->createUrl('rawatJalan/daftarPasienRajal/batalRanap');
$jscript = <<< JS
function buatSessionUbahStatus(idPendaftaran)
{
    var answer = confirm('Yakin Akan Merubah Status Periksa Pasien?');
        if (answer){
            $.post("${urlSessionUbahStatus}", {idPendaftaran: idPendaftaran },
                function(data){
                    'sukses';
            }, "json");
        }
        else
            {
            }
}
function prosesPembatalanRanap(id){
	var answer = confirm('Yakin Pasien Akan melakukan pembatalan?');
	if (answer){
        var pendaftaran_id = id;
    	$.post('$urlBatalPulang',{pendaftaran_id:pendaftaran_id}, function(data){
            if(data.status == 'OK'){
                $.fn.yiiGridView.update('daftarpasien-v-grid', {
            		data: $('#daftarpasien-v-search-form').serialize()
            	});
            }
            alert(data.pesan);
    	}, 'json');
    	return false;
	}
}
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>



<script>
function addPasienPulang(idPendaftaran,idPasien)
{
    $('#pendaftaran_id').val(idPendaftaran);
    $('#pasien_id').val(idPasien);

    <?php
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjaxRIRD/addPasienPulang'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogPasienPulang div.divForForm').html(data.div);
                    $('#dialogPasienPulang div.divForForm form').submit(addPasienPulang);

                    jQuery('.dtPicker2-5').datetimepicker(jQuery.extend({showMonthAfterYear:false},
                    jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','maxDate'  : 'd','timeText':'Waktu','hourText':'Jam',
                         'minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih   Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));


                }
                else
                {
                    $('#dialogPasienPulang div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    setTimeout(\"$('#dialogPasienPulang').dialog('close') \",1000);
                }

            } ",
    ))
?>;
    return false;
}

</script>


<!--Dialog untuk pasienpulang_t ========================= -->
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPasienPulang',
    'options'=>array(
        'title'=>'Kirim Ke Farmasi',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>600,
        'resizable'=>false,
		'close'=>'js:function( event, ui ) {}'
    ),
));?>
<iframe src="" name="iframePasienPulang" width="100%" height="900"></iframe>
<?php $this->endWidget(); ?>
 <!-- end pasienpulang_t dialog ========================= -->


 <!--Dialog untuk batal pulang ========================= -->
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPasienBatalPulang',
    'options'=>array(
        'title'=>'Batal Pemulangan Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>600,
        'resizable'=>false,
		'close'=>'js:function( event, ui ) {}'
    ),
));?>
<iframe src="" name="iframePasienBatalPulang" width="100%" height="600"></iframe>
<?php $this->endWidget(); ?>
 <!-- end pasienpulang_t dialog ========================= -->
