<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
    <legend class="rim2">Transaksi Pembebasan Tarif Pasien </legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('RJPendaftaranT[tgl_pendaftaran]', $modPendaftaran->tgl_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td>
            <label class="no_rek" style="padding-left:40px;">No Rekam Medik</label>
            </td>
            <td>
                <?php 
                    $this->widget('MyJuiAutoComplete', array(
                                    'name'=>'RJPasienM[no_rekam_medik]',
                                    'value'=>$modPasien->no_rekam_medik,
                                    'source'=>'js: function(request, response) {
                                       $.ajax({
                                           url: "'.Yii::app()->createUrl('rawatJalan/ActionAutoComplete/daftarPasienTindakanRuangan').'",
                                           dataType: "json",
                                           data: {
                                               term: request.term,
                                           },
                                           success: function (data) {
                                                   response(data);
                                           }
                                       })
                                    }',
                                    'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                isiDataPasien(ui.item);
                                                return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2'),
                                    'tombolDialog'=>array('idDialog'=>'dialogRekamedik','idTombol'=>'tombolDialogRekamedik'),
                                )); 
                ?>
            </td>
            <td rowspan="5">
                <?php 
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('RJPendaftaranT[no_pendaftaran]', $modPendaftaran->no_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('RJPasienM[jeniskelamin]', $modPasien->jeniskelamin, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('RJPendaftaranT[umur]', $modPendaftaran->umur, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('RJPasienM[nama_pasien]', $modPasien->nama_pasien, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('RJPendaftaranT[jeniskasuspenyakit_nama]',$modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('RJPasienM[nama_bin]', $modPasien->nama_bin, array('readonly'=>true)); ?></td>
        </tr>
    
                <?php echo CHtml::hiddenField('RJPendaftaranT[pendaftaran_id]',$modPendaftaran->pendaftaran_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('RJPendaftaranT[pasien_id]',$modPendaftaran->pasien_id, array('readonly'=>true)); ?>
          
        </tr>
    </table>
</fieldset> 

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogRekamedik',
    'options'=>array(
        'title'=>'No. Rekamedik',
        'autoOpen'=>false,
        'resizable'=>false,
        'width'=>870,
        'height'=>560,
        'modal'=>true,
    ),
));

$criteria = new CDbCriteria();
$criteria->compare('LOWER(no_rekam_medik)', strtolower($_GET['term']), true);
$criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
$criteria->order = 'tgl_pendaftaran DESC';
$models = RJInfokunjunganrjV::model()->findAll($criteria);
$dataProvider = new CActiveDataProvider('InfokunjunganrjV',array(
    'criteria'=>$criteria,
));

$modDataPasien = new RJInfokunjunganrjV('searchKunjunganPasien');
$modDataPasien->statusperiksa = "SEDANG PERIKSA";
if(isset($_GET['RJInfokunjunganrjV'])){
    $modDataPasien->attributes = $_GET['RJInfokunjunganrjV'];
    $format = new CustomFormat();
    $modDataPasien->tgl_pendaftaran  = $format->formatDateMediumForDB($_REQUEST['RJInfokunjunganrjV']['tgl_pendaftaran']);
    $modDataPasien->statusperiksa  = $_REQUEST['RJInfokunjunganrjV']['statusperiksa'];
    $modDataPasien->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAwal']);
    $modDataPasien->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RJInfokunjunganrjV']['tglAkhir']);
}

$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'rjrekamedik-alkes-m-grid',
    'dataProvider'=>$modDataPasien->searchRJ(),
    'filter'=>$modDataPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
            array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPasien",
                                    "onClick" => "
                                        isiDataPasien_fungsi(\"$data->no_rekam_medik\", \"$data->pendaftaran_id\");
                                        $(\"#dialogRekamedik\").dialog(\"close\");
                                        return false;
                                    "))',
            ),

            'no_rekam_medik',   
                //'tgl_pendaftaran',
                array(
                    'name'=>'tgl_pendaftaran',
                    'value'=>'$data->tgl_pendaftaran',
                    'filter'=>$this->widget('MyDateTimePicker',array(
                    'model'=>$modDataPasien,
                    'attribute'=>'tgl_pendaftaran',
                    'mode'=>'date',
                    'options'=> array(
                        'dateFormat'=>Params::DATE_TIME_FORMAT
                    ),
                        'htmlOptions'=>array('readonly'=>false, 'class'=>'dtPicker3','onclick'=>'showDateTime();'),
                    ),true
                    ),
                    'htmlOptions'=>array('width'=>'80','style'=>'text-align:center'),
                ),
                 'no_pendaftaran',
                 'nama_pasien', 
                 'alamat_pasien',
                 'penjamin_nama',
                 'nama_pegawai',
                 'jeniskasuspenyakit_nama',

                array(
                    'name'=>'statusperiksa',
                    'type'=>'raw',
                    'value'=>'$data->statusperiksa',
                    'filter' =>CHtml::activeDropDownList($modDataPasien,'statusperiksa',
                        Statusperiksa::items(),array('options' => array('SEDANG PERIKSA'=>array('selected'=>true)))),
                ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});

            jQuery(\'#RJInfokunjunganrjV_tgl_pendaftaran\').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional[\'id\'], {\'dateFormat\':\'dd M yy\',\'maxDate\':\'d\',\'timeText\':\'Waktu\',\'hourText\':\'Jam\',\'minuteText\':\'Menit\',
                \'secondText\':\'Detik\',\'showSecond\':true,\'timeOnlyTitle\':\'Pilih Waktu\',\'timeFormat\':\'hh:mms\',
                \'changeYear\':true,\'changeMonth\':true,\'showAnim\':\'fold\',\'yearRange\':\'-80y:+20y\'})); 
        }',
));

$this->endWidget('ext.bootstrap.widgets.BootGridView');
?>

<script type="text/javascript">
function isiDataPasien(data)
{
    $('#RJPendaftaranT_tgl_pendaftaran').val(data.tgl_pendaftaran);
    $('#RJPendaftaranT_no_pendaftaran').val(data.no_pendaftaran);
    $('#RJPendaftaranT_umur').val(data.umur);
    $('#RJPendaftaranT_jeniskasuspenyakit_nama').val(data.jeniskasuspenyakit_nama);
    $('#RJPendaftaranT_instalasi_nama').val(data.instalasi_nama);
    $('#RJPendaftaranT_ruangan_nama').val(data.ruangan_nama);
    $('#RJPendaftaranT_pendaftaran_id').val(data.pendaftaran_id);
    $('#RJPendaftaranT_pasien_id').val(data.pasien_id);
    
    $('#RJPasienM_jeniskelamin').val(data.jeniskelamin);
    $('#RJPasienM_nama_pasien').val(data.nama_pasien);
    $('#RJPasienM_nama_bin').val(data.nama_bin);
    
    $.post('<?php echo Yii::app()->createUrl('rawatJalan/ActionAjax/loadTindakanKomponenPasien');?>', {idPendaftaran:data.pendaftaran_id}, function(data){
        //$('#tblTindakanPasien tbody').html(data.formTindakanKomponen);
        $('#divTarifPasien').html(data.tabelPembebasanTarif);        
        $("#tblTindakanPasien .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
        $("#tblTindakanPasien .currency").each(function(){this.value = formatNumber(this.value)});
    }, 'json');
    
}

function isiDataPasien_fungsi(params, pendaftaran_id)
{
    $.post("<?php echo Yii::app()->createUrl('rawatJalan/ActionAutoComplete/loadDataPasien');?>", {no_rekam_medik:params },
        function(data){
            $('#RJPendaftaranT_tgl_pendaftaran').val(data.tgl_pendaftaran);
            $('#RJPendaftaranT_no_pendaftaran').val(data.no_pendaftaran);
            $('#RJPendaftaranT_umur').val(data.umur);
            $('#RJPendaftaranT_jeniskasuspenyakit_nama').val(data.jeniskasuspenyakit_nama);
            $('#RJPendaftaranT_instalasi_nama').val(data.instalasi_nama);
            $('#RJPendaftaranT_ruangan_nama').val(data.ruangan_nama);
            $('#RJPendaftaranT_pendaftaran_id').val(data.pendaftaran_id);
            $('#RJPendaftaranT_pasien_id').val(data.pasien_id);
            
            $('#RJPasienM_jeniskelamin').val(data.jeniskelamin);
            $('#RJPasienM_nama_pasien').val(data.nama_pasien);
            $('#RJPasienM_nama_bin').val(data.nama_bin);             
            $('#RJPasienM_no_rekam_medik').val(params);
        }, "json");

    $.post('<?php echo Yii::app()->createUrl('rawatJalan/ActionAjax/loadTindakanKomponenPasien');?>', {idPendaftaran:pendaftaran_id}, function(data){
        $('#divTarifPasien').html(data.tabelPembebasanTarif);        
        $("#tblTindakanPasien .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
        $("#tblTindakanPasien .currency").each(function(){this.value = formatNumber(this.value)});
    }, 'json');
}

function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
</script>