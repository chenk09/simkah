         
<table id="tblFormRencanaOperasi" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>Jenis Operasi</th>
            <th>Operasi</th>
            <th>Tarif</th>
            <th>Qty</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<script>
function inputOperasi(obj)
{
    if($(obj).is(':checked')) {
        var idOperasi = obj.value;
        var kelasPelayan_id = <?php echo $modPendaftaran->kelaspelayanan_id; ?>;
        
        if(kelasPelayan_id==''){
                $(obj).attr('checked', 'false');
                alert('Anda Belum Memilih Kelas Pelayanan');
                
            }
        else{
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPermintaanOperasi')?>',
                 'data':{idOperasi:idOperasi,kelasPelayan_id:kelasPelayan_id},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         $('#tblFormRencanaOperasi > tbody').append(data.form);
                         $("#tblFormRencanaOperasi > tbody > tr:last .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0});
                         $('.currency').each(function(){this.value = formatNumber(this.value)});
                         $("#tblFormRencanaOperasi > tbody > tr:last .number").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0,"symbol":null});
                         $('.number').each(function(){this.value = formatNumber(this.value)});
                 } ,
                 'cache':false});
        }     
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idOperasi)
{
    $('#tblFormRencanaOperasi #operasi_'+idOperasi).detach();
}

function getOperasi(item)
{
    $('#operasi').val(item[0]);    
    $('#BSRencanaOperasiT_operasi_id').val(item[1]);    
}

function hitungCyto(id,obj)
{
    if(obj == 1)
    {
        var persen_cytotind = $('#BSTindakanPelayananT_persencyto_tind_'+id+'').val(); 
        var harga_tarif = $('#BSTindakanPelayananT_tarif_tindakan_'+id+'').val(); 
        var tarif_cyto = harga_tarif * (persen_cytotind/100);

        $('#BSTindakanPelayananT_tarif_cyto_'+id+'').val(tarif_cyto);
    }
    else
    {
        $('#BSTindakanPelayananT_tarif_cyto_'+id+'').val(0);
    }
    
}
</script>
