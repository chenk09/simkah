<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
    <legend   class="rim">Data Pasien</legend>
    <table>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran,'tgl_pendaftaran', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo CHtml::activeHiddenField($modPendaftaran, 'pendaftaran_id',  array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
            
            <td>
               <div class="control-label">   <label class="no_rek" for="noRekamMedik">No Rekam Medik</label></div>
            </td>
            <td>
                <?php if (!empty($modPasien->no_rekam_medik)) { 
                    echo CHtml::activeTextField($modPasien,'no_rekam_medik', array('readonly'=>true));
                    }else{
                        echo CHtml::activeHiddenField($modPasien, 'pasien_id',  array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                        $this->widget('MyJuiAutoComplete', array(
                            'model'=>$modPasien,
                            'attribute'=>'no_rekam_medik',
                            'source'=>'js: function(request, response) {
                                           $.ajax({
                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/NoRmPasienRawatDarurat').'",
                                               dataType: "json",
                                               data: {
                                                   term: request.term,
                                                   
                                               },
                                               success: function (data) {
                                                       response(data);
                                               }
                                           })
                                        }',
                             'options'=>array(
                                   'showAnim'=>'fold',
                                   'minLength' => 2,
                                   'focus'=> 'js:function( event, ui ) {
                                        $(this).val(ui.item.value);
                                        
                                        return false;
                                    }',
                                   'select'=>'js:function( event, ui ) {
                                        isiDataPasien(ui.item);
                                        $("#PasienpulangT_lamarawat").val(ui.item.lama_rawat);
                                        return false;
                                    }',
                            ),
                            'tombolDialog'=>array('idDialog'=>'dialogPendaftaran','idTombol'=>'tombolPasienDialog'),
                            'htmlOptions'=>array('class'=>'span2', 'placeholder'=>'Ketik No. Rekam Medik','onkeypress'=>"return $(this).focusNextInputField(event)"),
                        )); 
                    }
                ?>
            </td>
            
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran,'no_pendaftaran', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            
            <td>
               <div class="control-label">   <label class="no_rek" for="namaPasien">Nama Pasien</label></div>
            </td>
            <td>
                <?php 
                    if (!empty($modPasien->pasien_id)) { 
                        echo CHtml::activeTextField($modPasien, 'nama_pasien', array('readonly'=>true));
                    }else{
                    $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$modPasien,
                                    'attribute'=>'nama_pasien',
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/NamaPasienRawatDarurat').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                           
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                isiDataPasien(ui.item);
                                                $("#RDPasienM_no_rekam_medik").val(ui.item.no_rekam_medik);
                                                $("#PasienpulangT_lamarawat").val(ui.item.lama_rawat);
                                                return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array('class'=>'span3', 'placeholder'=>'Ketik Nama Pasien','style'=>'width:200px;','onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); 
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'umur', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien,'jeniskelamin', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('jeniskasuspenyakit_nama', empty($modPendaftaran->jeniskasuspenyakit_id) ? "-":$modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama,array('readonly'=>true)); ?>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien, 'nama_bin', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('instalasi_nama', empty($modPendaftaran->instalasi_id) ? "-" : $modPendaftaran->instalasi->instalasi_nama,array('readonly'=>true)); ?>
                <?php echo CHtml::activeHiddenField($modPendaftaran, 'instalasi_id',  array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
            <td>
                <?php echo CHtml::activeLabel($modPendaftaran, 'carabayar_id',array('class'=>'control-label')) ?>
            </td>
            <td>
                <?php echo CHtml::textField('carabayar_nama', empty($modPendaftaran->carabayar_id) ? "-" : $modPendaftaran->carabayar->carabayar_nama, array('readonly'=>true));   ?>
                <?php echo CHtml::activeHiddenField($modPendaftaran, 'carabayar_id', array('readonly'=>true));   ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'ruangan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('ruangan_nama', ((empty($modPendaftaran->ruangan->ruangan_nama)) ? "-" : $modPendaftaran->ruangan->ruangan_nama), array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo CHtml::activeHiddenField($modPendaftaran, 'ruangan_id',array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            <td>
                <?php echo CHtml::activeLabel($modPendaftaran, 'penjamin_id',array('class'=>'control-label')) ?>
            </td>
            <td>
                <?php echo CHtml::textField('penjamin_nama', (empty($modPendaftaran->penjamin->penjamin_nama) ? "-" : $modPendaftaran->penjamin->penjamin_nama), array('readonly'=>true));   ?>
                <?php echo CHtml::activeHiddenField($modPendaftaran, 'penjamin_id', array('readonly'=>true));   ?>
                <?php echo CHtml::activeHiddenField($modPendaftaran, 'kelaspelayanan_id', array('readonly'=>true)); ?>
            </td>
            
        </tr>
    </table>
</fieldset>