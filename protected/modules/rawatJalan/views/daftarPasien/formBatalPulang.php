<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
	if(!is_null($pendaftaranT->pembayaranpelayanan_id))
	{
		echo '<div class="alert alert-block alert-error">Pasien sudah melakukan pembayaran</div>';
	}else{
?>
	<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'t-pasienbatalpulang-form',
		'enableAjaxValidation'=>false,
			'type'=>'horizontal',
			'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
			'focus'=>'#',
	)); ?>
	<?php echo $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$pendaftaran,'modPasien'=>$pasienM));?> 
	
	<legend class="rim">Tindak Lanjut</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary(array($pasienbatalpulangT)); ?>
	<div class="control-group ">
		<?php echo CHtml::label('Tgl Pembatalan', 'tglpembatalan', array('class'=>'control-label'))?>
		<div class="controls">
			<?php   
				$this->widget('MyDateTimePicker',array(
					'model'=>$pasienbatalpulangT,
					'attribute'=>'tglpembatalan',
					'mode'=>'datetime',
					'options'=> array(
					'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
					'maxDate' => 'd',
				),
				'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
			)); ?>
			<?php echo $form->error($pasienbatalpulangT, 'tglpembatalan'); ?> 
		</div>
	</div>
	<?php echo $form->textAreaRow($pasienbatalpulangT,'alasanpembatalan',array('onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
	<?php echo $form->textFieldRow($pasienbatalpulangT,'namauser_otorisasi',array('onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
	<?php echo $form->passwordFieldRow($pasienbatalpulangT,'password',array('onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
	<div class="form-actions">
		<?php
			echo CHtml::htmlButton($pasienbatalpulangT->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
			echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));
		?>
	</div>	
	<?php $this->endWidget(); ?>
<?php
	}
?>