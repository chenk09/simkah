<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
	if(!is_null($pendaftaranT->pembayaranpelayanan_id))
	{
		echo '<div class="alert alert-block alert-error">Pasien sudah melakukan pembayaran</div>';
	}else{
?>
	<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'pasienpulang-t-form',
		'enableAjaxValidation'=>false,
			'type'=>'horizontal',
			'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
			'focus'=>'#',
	)); ?>
	<?php echo $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$pendaftaranT,'modPasien'=>$pasienM));?> 
	<legend class="rim">Tindak Lanjut</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary(array($modelPulang)); ?>
	<table width="100%">
		<tr>
			<td>
				<div class="control-group ">
					<?php echo CHtml::label('Tgl Pasien Pulang', 'tglpasienpulang', array('class'=>'control-label'))?>
					<div class="controls">
						<?php   
							$this->widget('MyDateTimePicker',array(
								'model'=>$modelPulang,
								'attribute'=>'tglpasienpulang',
								'mode'=>'datetime',
								'options'=> array(
								'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
								'maxDate' => 'd',
							),
							'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
						)); ?>
						<?php echo $form->error($modelPulang, 'tglpasienpulang'); ?> 
					</div>
				</div>
				<?php echo $form->textFieldRow($modelPulang,'penerimapasien',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
			</td>
		</tr>
	</table>
	<div class="form-actions">
		<?php
			if(count($ruangan) == 0 && $modelPulang->isNewRecord == true)
			{
				echo CHtml::htmlButton($modelPulang->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
				echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));
			}
		?>
	</div>
	<?php $this->endWidget(); ?>
<?php
	}
?>