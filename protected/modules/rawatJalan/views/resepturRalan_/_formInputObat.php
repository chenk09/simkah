<table>
    <tr>
        <td width="50%">
            <div class="control-group ">
                <?php $modReseptur->tglreseptur = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modReseptur->tglreseptur, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                <?php echo $form->labelEx($modReseptur,'tglreseptur', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker',array(
                        'model'=>$modReseptur,
                        'attribute'=>'tglreseptur',
                        'mode'=>'datetime',
                        'options'=> array(
                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                            'maxDate' => 'd',
                            'yearRange'=> "-60:+0",
                        ),
                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                        ),
                    )); ?>
                    <?php echo $form->error($modReseptur, 'tglreseptur'); ?>
                </div>
            </div>
            <?php echo $form->textFieldRow($modReseptur,'noresep', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->hiddenField($modReseptur,'pasien_id', array()); ?>
            <?php echo $form->hiddenField($modReseptur,'pendaftaran_id', array()); ?>
        </td>
        <td width="50%">
            <?php echo $form->dropDownListRow($modReseptur,'pegawai_id',CHtml::listData($modReseptur->DokterItems, 'pegawai_id', 'nama_pegawai'),
                array(
                    'onkeypress'=>"return $(this).focusNextInputField(event)"
                )
            );?>
            <?php echo $form->dropDownListRow($modReseptur,'ruangan_id',CHtml::listData($modReseptur->ApotekRawatJalan, 'ruangan_id', 'ruangan_nama'),
                array(
                    'onchange'=>"return clearForm();",
                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                )
            );?>
        </td>
    </tr>
    <tr>
        <td colspan="2"><hr></td>
    </tr>
    <tr>
        <td>
            <table class="table-bordered">
                <tr>
                    <td>
                        <div style="margin-bottom: 15px;"><b>Form Entry Diagnosa / Obat</b></div>
                        <div class="control-group">
                            <label class="control-label" for="namaObat">Nama Diagnosa</label>
                            <div class="controls">
                                <div class="input-append" style='display:inline'>
                                    <?php $this->widget('MyJuiAutoComplete', array(
                                        'name'=>'namaDiagnosa',
                                        'source'=>'js: function(request, response) {
                                           $.ajax({
                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/ObatResepturByDiagnosa').'",
                                               dataType: "json",
                                               data:{
                                                   term: request.term,
                                                   id_kelas_pelayanan: 29,
                                                   ruangan_id: $("#RJResepturT_ruangan_id").val(),
                                               },
                                               success: function (data){
                                                    response(data);
                                               }
                                           })
                                        }',
                                        'options'=>array(
                                            'showAnim'=>'fold',
                                            'minLength' => 2,
                                            'focus'=> 'js:function( event, ui ) {
                                                $(this).val( ui.item.label);
                                                return false;
                                            }',
                                            'select'=>'js:function( event, ui ){
                                                addRowByDiagnosa(ui.item); 
                                                return false;
                                            }',
                                        ),
                                    )); ?>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="namaObat">Nama Obat</label>
                            <div class="controls">
                                <div class="input-append" style='display:inline'>
                                    <?php $this->widget('MyJuiAutoComplete', array(
                                        'name'=>'namaObatNonRacik',
                                        'source'=>'js: function(request, response) {
                                           $.ajax({
                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/ObatReseptur').'",
                                               dataType: "json",
                                               data:{
                                                   term: request.term,
                                                   idSumberDana: $("#idSumberDana").val(),
                                                   ruangan_id: $("#RJResepturT_ruangan_id").val(),
                                               },
                                               success: function (data){
                                                    response(data);
                                               }
                                           })
                                        }',
                                        'options'=>array(
                                            'showAnim'=>'fold',
                                            'minLength' => 2,
                                            'focus'=> 'js:function( event, ui ) {
                                                $(this).val( ui.item.label);
                                                return false;
                                            }',
                                            'select'=>'js:function( event, ui ){
                                                addRowByObat(ui.item); 
                                                return false;
                                            }',
                                        ),
                                    )); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">
    var i = 0;
    var entryObat = {};
    var maxNominal = "<?=Yii::app()->user->getState('MAX_NOMINAL_DIAG_OBT_BPJS')?>";

    function addRowByObat(item)
    {
        $('#tblDaftarResep > tbody').find('.no_record').remove();
        addObat(item);
        $('#namaObatNonRacik').val('');
        $('#namaDiagnosa').val('');
        hitungTotal();
    }

    function addRowByDiagnosa(item){
        $('#tblDaftarResep > tbody').find('.no_record').remove();
        var row = '<tr id="tr_x_'+i+'">'+
            '<td colspan="9" style="font-weight: bold;">'+ item.diagnosa_nama +'</td>'+
            '</tr>';
        $('#tblDaftarResep > tbody').append(row);

        $.each(item.obat, function( index, value ){
            addObat(value);
        });

        $('#namaObatNonRacik').val('');
        $('#namaDiagnosa').val('');
        hitungTotal();
    }

    function addObat(item)
    {
        var inputObat = '<?php echo CHtml::hiddenField('obat[]', '', array('readonly'=>true,'class'=>'inputFormTable span1')) ?>';
        var inputHargaSatuan = '<?php echo CHtml::hiddenField('hargasatuan[]', '', array('readonly'=>true,'class'=>'inputFormTable span1')) ?>';
        var inputHargaNetto = '<?php echo CHtml::hiddenField('harganetto[]', '', array('readonly'=>true,'class'=>'inputFormTable span1')) ?>';
        var inputKekuatan = '<?php echo CHtml::hiddenField('kekuatan[]', '', array('readonly'=>true,'class'=>'inputFormTable span1')) ?>';
        var inputJmlPermintaan = '<?php echo CHtml::hiddenField('jmlpermintaan[]', '', array('readonly'=>true,'class'=>'inputFormTable span1')) ?>';
        var inputJmlKemasan = '<?php echo CHtml::hiddenField('jmlkemasan[]', '', array('readonly'=>true,'class'=>'inputFormTable span1')) ?>';
        var inputSatuanKekuatan = '<?php echo CHtml::hiddenField('satuankekuatan[]', '', array('readonly'=>true,'class'=>'inputFormTable span1')) ?>';
        var inputIsRacikan = '<?php echo CHtml::hiddenField('isRacikan[]', '', array()) ?>';
        var inputJmlStock = '<?php echo CHtml::hiddenField('jmlstock[]', '', array()) ?>';

        var inputSatuan = <?php echo json_encode(CHtml::dropDownList('satuankecil[]', '', CHtml::listData(SatuankecilM::model()->findAll(), 'satuankecil_id', 'satuankecil_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3'))); ?>;
        var inputSumberDana = <?php echo json_encode(CHtml::dropDownList('sumberdana[]', '', CHtml::listData(SumberdanaM::model()->findAll(), 'sumberdana_id', 'sumberdana_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3-5'))); ?>;
        var inputSigna = '<?php echo CHtml::textField('signa[]', '', array('placeholder'=>'-- Aturan Pakai --','class'=>'span2')) ?>';
        var inputHargaJual = '<?php echo CHtml::textField('hargajual[]', '', array(
            'readonly'=>true,
            'style'=>'width: auto;',
            'class'=>'inputFormTabel lebar2 currency'
        )) ?>';
        var inputEtiket = <?php echo json_encode(CHtml::dropDownList('etiket[]', '', Etiket::items(),array('class'=>'inputFormTabel span3'))); ?>;
        var inputQty = '<?php echo CHtml::textField('qty[]', '', array(
            'readonly'=>false,
            'class'=>'inputFormTabel lebar2 number',
            'onblur'=>'hitungSubTotal(this);'
        )) ?>';
        var inputSubTotal = '<?php echo CHtml::textField('subTotal[]', '', array(
            'readonly'=>true,
            'style'=>'width: auto;',
            'class'=>'vSubTotal inputFormTabel lebar2 currency'
        )) ?>';
        var iconRemove = '<a onclick="removeObat(this);return false;" rel="tooltip" href="javascript:void(0);" data-original-title="Klik untuk menghapus Obat"><i class="icon-remove"></i></a>';

        var classDuplicat = '';
        if(entryObat[item.obatalkes_id] !== undefined)
        {
            classDuplicat = 'label-warning';
        }

        if(item.qtyStok == 0)
        {
            classDuplicat = 'label-important';
            inputQty = '<?php echo CHtml::textField('qty[]', '', array(
                'readonly'=>true,
                'class'=>'inputFormTabel lebar2 number',
            )) ?>';
        }

        i = i + 1;
        var row_x = '<tr class="'+ classDuplicat +'" id="tr_'+i+'">'+
            '<td style="padding-left: 20px;">'+ item.obatalkes_kode + ' - ' + item.obatalkes_nama + ' / Stock Obat : ' + formatNumber(item.qtyStok) + inputObat +
            inputHargaSatuan +
            inputHargaNetto +
            inputKekuatan +
            inputJmlPermintaan +
            inputJmlKemasan +
            inputJmlStock +
            inputSatuanKekuatan +
            inputIsRacikan +'</td>'+
            '<td>'+ inputSumberDana +'</td>'+
            '<td>'+ inputSatuan +'</td>'+
            '<td>'+ inputQty +'</td>'+
            '<td>'+ inputHargaJual +'</td>'+
            '<td>'+ inputSubTotal +'</td>'+
            '<td>'+ inputSigna +'</td>'+
            '<td>'+ inputEtiket +'</td>'+
            '<td>'+ iconRemove +'</td>'+
            '</tr>';
        $('#tblDaftarResep > tbody').append(row_x);

        var idSatuanKecil = item.satuankecil_id;
        var qty = 0;
        if(item.qtyStok > 0)
        {
            qty = 1;
        }
        var idSumberDana = item.sumberdana_id;
        var idObat = item.obatalkes_id;
        var hargaSatuan = item.hargajual;
        var hargaNetto = item.harganetto;
        var hargaJual = item.hargajual;
        var subTotal = (qty * item.hargajual);
        var kekuatan = item.kekuatan;
        var satuanKekuatan = item.satuankekuatan;
        var jmlPermintaan = 0;
        var jmlKemasan = item.kemasanbesar;
        var isRacikan = 0;
        var jmlStock = item.qtyStok;

        $('#tr_'+i).find('select[name="satuankecil[]"]').attr('value', idSatuanKecil);
        $('#tr_'+i).find('input[name="qty[]"]').attr('value', qty);
        $('#tr_'+i).find('select[name="sumberdana[]"]').attr('value', idSumberDana);
        $('#tr_'+i).find('input[name="obat[]"]').attr('value', idObat);
        $('#tr_'+i).find('input[name="hargasatuan[]"]').attr('value', hargaSatuan);
        $('#tr_'+i).find('input[name="harganetto[]"]').attr('value', hargaNetto);
        $('#tr_'+i).find('input[name="hargajual[]"]').attr('value', hargaJual);
        $('#tr_'+i).find('input[name="subTotal[]"]').attr('value', subTotal);
        $('#tr_'+i).find('input[name="kekuatan[]"]').attr('value', kekuatan);
        $('#tr_'+i).find('input[name="satuankekuatan[]"]').attr('value', satuanKekuatan);
        $('#tr_'+i).find('input[name="jmlpermintaan[]"]').attr('value', jmlPermintaan);
        $('#tr_'+i).find('input[name="jmlkemasan[]"]').attr('value', jmlKemasan);
        $('#tr_'+i).find('input[name="isRacikan[]"]').attr('value', isRacikan);
        $('#tr_'+i).find('input[name="jmlstock[]"]').attr('value', jmlStock);

        entryObat[idObat] = 'yes';
    }

    function hitungSubTotal(obj)
    {
        var qty = unformatNumber($(obj).parents('tr').find('input[name="qty[]"]').val());
        var harga = unformatNumber($(obj).parents('tr').find('input[name="hargajual[]"]').val());
        var subTotal = (qty * harga);
        $(obj).parents('tr').find('input[name="subTotal[]"]').val(subTotal);
        hitungTotal();
    }

    function hitungTotal()
    {
        var vTotal = 0;
        $("#tblDaftarResep > tbody").find('.vSubTotal').each(function(){
            vTotal = (vTotal + parseFloat(unformatNumber(this.value)));
        });
        $('#totalHargaReseptur').val(vTotal);

        $('#btn_submit').removeAttr('disabled');
        $('.footer_total').removeClass('label-important');
        $('.info-msg').html('');
        if(vTotal > maxNominal)
        {
            $('#btn_submit').attr('disabled', 'disabled');
            $('.footer_total').addClass('label-important');
            $('.info-msg').html('Total nominal melebihi maksimum entry, silakan di revisi ulang!!');
        }
        konversiFormat()
    }

    function konversiFormat()
    {
        $("#tblDaftarResep > tbody > tr:last .currency").unmaskMoney();
        $("#tblDaftarResep > tbody > tr:last .currency").maskMoney({
            "symbol":"Rp. ",
            "defaultZero":true,
            "allowZero":true,
            "decimal":".",
            "thousands":",",
            "precision":0
        });
        $('.currency').each(function(){
            this.value = formatUang(this.value)
        });
        $("#tblDaftarResep > tbody > tr:last .number").unmaskMoney();
        $("#tblDaftarResep > tbody > tr:last .number").maskMoney({
            "defaultZero":true,
            "allowZero":true,
            "decimal":".",
            "thousands":",",
            "precision":0,
            "symbol":null
        });
        $('.number').each(function(){
            this.value = formatNumber(this.value)
        });
    }

    function removeObat(obj)
    {
        if(confirm('Apakah anda akan menghapus obat?'))
        {
            $(obj).parent().parent().remove();
            var idObat = $(obj).closest('tr').find('input[name="obat[]"]').val();
            delete entryObat[idObat];
            hitungTotal();
        }
    }

    function clearForm(obj)
    {
        $('#tblDaftarResep > tbody').empty();
        $('#tblDaftarResep > tbody').find('.no_record').remove();
        var row = '<tr class="no_record">'+
            '<td colspan="9">Data tidak ditemukan</td>'+
            '</tr>';
        $('#tblDaftarResep > tbody').append(row);
        entryObat = {};
    }

</script>