<?php
$this->breadcrumbs=array(
	'Konsul Poli',
);

$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));

$this->renderPartial('/_tabulasi', array('modPendaftaran'=>$modPendaftaran));
?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjkonsul-poli-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary(array($modKonsul,$modelPendaftaran)); ?>
    <table>
        <tr>
            <td width="50%">
                <div class="control-group ">
                    <?php echo CHtml::hiddenField('url',$this->createUrl('',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),array('readonly'=>TRUE));?>
                    <?php echo CHtml::hiddenField('berubah','',array('readonly'=>TRUE));?>
                    <?php echo $form->labelEx($modKonsul,'tglkonsulpoli', array('class'=>'control-label')) ?>
                    <?php $modKonsul->tglkonsulpoli = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modKonsul->tglkonsulpoli, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modKonsul,
                                                    'attribute'=>'tglkonsulpoli',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($modKonsul,'ruangan_id', CHtml::listData($modKonsul->getRuanganInstalasiItems(Params::INSTALASI_ID_RJ,true), 'ruangan_id', 'ruangan_nama'),
                                                    array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->dropDownListRow($modKonsul,'pegawai_id', CHtml::listData($modKonsul->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                                    array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
            <td width="50%">
                <?php //echo $form->dropDownListRow($modKonsul,'asalpoliklinikkonsul_id', CHtml::listData($modKonsul->getRuanganInstalasiItems(''), 'ruangan_id', 'ruangan_nama'),
                                                //array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modKonsul,'catatan_dokter_konsul',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
<!--        <tr>
            <td colspan="2">
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th colspan="2">Karcis Tindakan</th>
                        </tr>
                    </thead>
                    <?php //foreach ($karcisTindakan as $i => $karcis) { ?>
                    <tr>
                        <td width="15px;">
                            <?php //echo CHtml::checkBox('karcis[]', false, array()); ?>
                        </td>
                        <td>
                            <?php //echo $karcis->daftartindakan_nama; ?>
                        </td>
                    </tr>
                    <?php //} ?>
                </table>
            </td>
        </tr>-->
    </table>
    
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','id'=>'btn_simpan')); ?>
	    <?php
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; ?>								
            <?php 
           $content = $this->renderPartial('rawatJalan.views.tips.tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print&id='.$modPendaftaran->pendaftaran_id);

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}

JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
    </div>

<?php $this->endWidget(); ?>

<?php $this->renderPartial($this->pathView.'_listKonsulPoli',array('modRiwayatKonsul'=>$modRiwayatKonsul)); ?>

    <?php 
$js = <<< JS
//==================================================Validasi===============================================
//*Jangan Lupa Untuk menambahkan hiddenField dengan id "berubah" di setiap form
//* hidden field dengan id "url"
//*Copas Saja hiddenfield di Line 36 dan 35
//* ubah juga id button simpannya jadi "btn_simpan"


function palidasiForm(obj)
{
    var berubah = $('#berubah').val();
    if(berubah=='Ya'){
       if(confirm('Apakah Anda Akan menyimpan Perubahan Yang Sudah Dilakukan?')){
           $('#url').val(obj);
           $('#btn_simpan').click();
       }
    }      
}

JS;
Yii::app()->clientScript->registerScript('js',$js,CClientScript::POS_READY);
?>   
    
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogDetailKonsul',
    'options'=>array(
        'title'=>'Detail Konsul',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'resizable'=>false,
        'position'=>'top',
    ),
));

    echo '<div id="contentDetailKonsul">dialog content here</div>';

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<script type="text/javascript">
function viewDetailKonsul(idKonsulAntarPoli)
{
    $.post('<?php echo $this->createUrl('ajaxDetailKonsul') ?>', {idKonsulAntarPoli: idKonsulAntarPoli}, function(data){
        $('#contentDetailKonsul').html(data.result);
    }, 'json');
    $('#dialogDetailKonsul').dialog('open');
}
function batalKonsul(idKonsulAntarPoli,idPendaftaran)
{
    if(confirm('Apakah anda akan membatalkan konsul ini?')){
        $.post('<?php echo $this->createUrl('ajaxBatalKonsul') ?>', {idKonsulAntarPoli: idKonsulAntarPoli, idPendaftaran:idPendaftaran}, function(data){
            $('#tblListKonsul').html(data.result);
        }, 'json');
    }
}
</script>