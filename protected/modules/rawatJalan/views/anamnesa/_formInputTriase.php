<style>
    div.colorPicker-swatch {
height: 30px;
width: 73px;
}
div.colorPicker-picker {
height: 30px;
width: 83px;
}
div.colorPicker-palette{
width:79px;
}
.colorPicker_palette-1{display:none;}
</style>
<fieldset id="formTriase" class="table-bordered">
    <legend class="table-bordered">Triase</legend>
<table>
    <tr>
        <td colspan=2 width="50%">                
                <div class="control-group ">
                    <label class="control-label" for="namaTriase">Triase Pasien</label>
                    <div class="controls">
                        <div class="input-append" style='display:inline'>
                        <?php 
                            $this->widget('MyJuiAutoComplete', array(
                                            'name'=>'namaTriase',
                                            'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/DaftarTriase').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                             'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'focus'=> 'js:function( event, ui ) {
                                                        $(this).val( ui.item.label);
                                                        return false;
                                                    }',
                                                   'select'=>'js:function( event, ui ) {
                                                        $("#idTriase").val(ui.item.triase_id); 
                                                        return false;
                                                    }',
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogTriase'),
                                        )); 
                        ?>
                </div>      
                    </div>
                </div>            
            <?php echo CHtml::hiddenField('idTriase', '', array('readonly'=>true)) ?>
        </td>
    </tr>
    <tr>
        <td>
            <table id="tblDataTriase" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>Warna Triase</th>
            <th>Nama Triase</th>
            <th>Keterangan</th>
            <th>Batal / Hapus</th>
        </tr>
    </thead>
    <tbody>
        <?php
            
            $anamnesa=AnamnesaT::model()->find('pendaftaran_id = '.$_GET['idPendaftaran']);
//            echo $anamnesa->triase_id;
            if(count($anamnesa)>0 && !empty($anamnesa->triase_id)){
                $modDetail = new Triase;
                $triase = Triase::model()->findByPk($anamnesa->triase_id);
        ?>
        <tr>
            <td><?php echo CHtml::activeHiddenField($modDetail,'['.$triase->triase_id.']triase_id',array('value'=>$triase->triase_id, 'class'=>'idTriase'))."".$this->renderPartial($this->pathView.'_warnaTriase', array('triase_id'=>$triase->triase_id), true); ?></td>
            <td><?php echo $triase->triase_nama; ?></td>
            <td><?php echo $triase->keterangan_triase; ?></td>
            <td><?php echo CHtml::link("<span class='icon-trash'>&nbsp;</span>",'',
                                   array('href'=>'#','onClick'=>'hapusTriase('.$anamnesa->anamesa_id.','.$anamnesa->triase_id.');return false;','style'=>'text-decoration:none;')); ?></td>
        </tr>
            <?php } ?>
    </tbody>
</table>
        </td>
    </tr>
</table>
</fieldset>
<?php
//========= Dialog buat cari data Alat Kesehatan (RACIKAN)  =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogTriase',
    'options'=>array(
        'title'=>'Daftar Triase',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>750,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modTriase = new Triase('search');
$modTriase->unsetAttributes();
if(isset($_GET['Triase']))
    $modTriase->attributes = $_GET['Triase'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'triase-m-grid',
	'dataProvider'=>$modTriase->search(),
	'filter'=>$modTriase,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Nama Triase',
                    'name'=>'triase_nama',
                    'value'=>'$data->triase_nama',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'triase_namalainnya',
                    'value'=>'$data->triase_namalainnya',
                ),
                array(
                    'header'=>'Warna Triase',
                    'name'=>'warna_triase',
                    'value'=>'$data->warna_triase',
                ),
                array(
                    'header'=>'Keterangan',
                    'name'=>'keterangan_triase',
                    'value'=>'$data->keterangan_triase',
                ),
                
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectObat",
                                    "onClick" => "$(\"#idTriase\").val(\"$data->triase_id\");  
                                                  $(\"#namaTriase\").val(\"$data->triase_nama\");
                                                  submitTriase();
                                                $(\'#dialogTriase\').dialog(\'close\');return false;"))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>

<?php
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        
$urlGetTriasePasien = Yii::app()->createUrl('actionAjax/GetTriasePasien');//MAsukan Dengan memilih Triase
$url=Yii::app()->createAbsoluteUrl($module.'/'.$controller);
$mds = Yii::t('mds','Anda yakin akan membatalkan triase pasien?');

$jscript = <<< JS
        
function submitTriase()
{
    idTriase = $('#idTriase').val();
        triase = $("#tblDataTriase tbody").find(".idTriase");
        jumlah =  triase.length;
        if (jumlah != 1){
            $.post("${urlGetTriasePasien}", { idTriase: idTriase},
            function(data){
                $('#tblDataTriase').append(data.tr);
                $('#namaTriase').val('');
            hitungUrutan();
            }, "json");
        }else{
            $('#tblDataTriase tbody tr').parent().remove();
            $.post("${urlGetTriasePasien}", { idTriase: idTriase},
            function(data){
                $('#tblDataTriase').append(data.tr);
                $('#namaTriase').val('');
            hitungUrutan();
            }, "json");
        }
}    
   
function batal(){
    if(confirm("${mds}"))
    {
        $('#tblDataTriase tbody tr').parent().remove();
        hitungUrutan();
    }
}
    
function hapus(){
    if(confirm("${mds}"))
    {
        $('#tblDataTriase tbody tr').parent().remove();
        hitungUrutan();
    }
}    
    
function hitungUrutan(){
    noUrut = 1;
    $("#tblDataTriase tbody tr").find(".noUrut").each(function(){
        $(this).val(noUrut);
        noUrut++;
    });
}            
JS;
Yii::app()->clientScript->registerScript('masukantriase',$jscript, CClientScript::POS_HEAD);
?>
<script>
function hapusTriase(idAnamesa, idTriase){
    var idAnamesa = idAnamesa;
    var idTriase = idTriase;
    var url = '<?php echo $url."/hapusTriase"; ?>';
    var answer = confirm('Yakin Akan Menghapus Data Triase Pasien ?');
        if (answer){
             $.post(url, {idAnamesa: idAnamesa, idTriase:idTriase},
                 function(data){
                    if(data.status == 'proses_form'){
                            location.reload();
                            $('#tblDataTriase tbody tr').parent().remove();
                        }else{
                            alert('Data Gagal di Hapus')
                        }
            },"json");
       }
}    
</script>

