<fieldset>
    <legend class="rim2">Informasi Batal Periksa Pasien</legend>
</fieldset>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#search').submit(function(){
	$.fn.yiiGridView.update('daftarpasien-v-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarpasien-v-grid',
	'dataProvider'=>$model->searchInformasiBatalPeriksaPasien(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(	
                array(
                    'name'=>'Tanggal Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->tgl_pendaftaran',
                ),
                array(
                    'name'=>'No Pendaftaran'.'/<br/>'.'No Rekam Medik',
                    'type'=>'raw',
                    'value'=>'"$data->no_pendaftaran"."<br/>"."$data->no_rekam_medik"',
                ),
                array(
                    'name'=>'Nama Pasien'.'/<br/>'.'Alias',
                    'type'=>'raw',
                    'value'=>'"$data->nama_pasien"."<br/>"."$data->nama_bin"',
                ),
                array(
                    'name'=>'Cara Bayar'.'/<br/>'.'Penjamin',
                    'type'=>'raw',
                    'value'=>'"$data->carabayar_nama"."<br/>"."$data->penjamin_nama"',
                ),
                array(
                    'name'=>'Dokter',
                    'type'=>'raw',
                    'value'=>'$data->nama_pegawai',
                ),
                array(
                    'name'=>'Kasus Penyakit',
                    'type'=>'raw',
                    'value'=>'$data->jeniskasuspenyakit_nama',
                ),
                array(
                    'name'=>'Tanggal Pembatalan',
                    'type'=>'raw',
                    'value'=>'$data->tglbatal',
                ),
                array(
                    'name'=>'Keterangan Batal',
                    'type'=>'raw',
                    'value'=>'$data->keterangan_batal',
                ),
		array(
                    'name'=>'Dibatalkan Oleh',
                    'type'=>'raw',
                    'value'=>'$data->nama_pemakai',
                ),
               
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php $this->renderPartial('rawatJalan.views.informasi.batalPeriksaPasien._search',array('model'=>$model)); ?>


