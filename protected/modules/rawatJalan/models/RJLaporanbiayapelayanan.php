<?php

class RJLaporanbiayapelayanan extends LaporanbiayapelayananV {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchTable() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function searchGrafik() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        
        $table1 = 'select kelaspelayanan_nama, pendaftaran_id, ruangan_id, penjamin_nama'; 
            
        $table2 = ' from laporanbiayapelayanan_v where ruangan_id = '.Yii::app()->user->getState('ruangan_id');
            
        $group = ' group by pendaftaran_id, kelaspelayanan_nama, ruangan_id, penjamin_nama';
        
        if (is_array($this->penjamin_id)){
            $table2 .=' and penjamin_id in ('.implode(',', $this->penjamin_id).')';
            $group2 .= ', penjamin_id';
        }else{
            $table2 .= ' and penjamin_id is null';
            $group2 .= ', penjamin_id';
        }
        if (is_array($this->kelaspelayanan_id)){
            $table2 .= ' and kelaspelayanan_id in ('.implode(',',$this->kelaspelayanan_id).')';
            $group2 .= ', kelaspelayanan_id';
        }else{
            $table2 .= ' and kelaspelayanan_id is null';
            $group2 .= ', kelaspelayanan_id';
        }
        
        $table = $table1.$group2.$table2.$group.$group2;
        $sql = 'select count(*) as jumlah, kelaspelayanan_nama as data, penjamin_nama as tick from ('.$table.') x group by kelaspelayanan_nama, penjamin_nama';
        
        return new CSqlDataProvider($sql);

    }
    public function searchPrint() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination'=>false,
                ));
    }
    
    protected function functionCriteria(){
        $criteria = new CDbCriteria();
        
        $criteria->addBetweenCondition('tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
        $criteria->select = 'pendaftaran_id, ruangan_id, tgl_pendaftaran, no_rekam_medik, nama_pasien, nama_bin, jeniskelamin, umur, no_pendaftaran, jeniskasuspenyakit_nama, kelaspelayanan_nama, kelaspelayanan_id, carabayar_nama, penjamin_nama, penjamin_id, carabayar_id, sum(tarif_tindakan) as total, sum(iurbiaya_tindakan) as iurbiaya';
        $criteria->group = 'pendaftaran_id, ruangan_id, tgl_pendaftaran, no_rekam_medik, nama_pasien, nama_bin, jeniskelamin, umur, no_pendaftaran, jeniskasuspenyakit_nama, kelaspelayanan_nama, kelaspelayanan_id, carabayar_nama, penjamin_nama, penjamin_id, carabayar_id';
////        $criteria->compare('profilrs_id', $this->profilrs_id);
////        $criteria->compare('pasien_id', $this->pasien_id);
////        $criteria->compare('LOWER(no_rekam_medik)', strtolower($this->no_rekam_medik), true);
////        $criteria->compare('LOWER(tgl_rekam_medik)', strtolower($this->tgl_rekam_medik), true);
////        $criteria->compare('LOWER(jenisidentitas)', strtolower($this->jenisidentitas), true);
////        $criteria->compare('LOWER(no_identitas_pasien)', strtolower($this->no_identitas_pasien), true);
////        $criteria->compare('LOWER(namadepan)', strtolower($this->namadepan), true);
////        $criteria->compare('LOWER(nama_pasien)', strtolower($this->nama_pasien), true);
////        $criteria->compare('LOWER(nama_bin)', strtolower($this->nama_bin), true);
////        $criteria->compare('LOWER(jeniskelamin)', strtolower($this->jeniskelamin), true);
////        $criteria->compare('LOWER(tempat_lahir)', strtolower($this->tempat_lahir), true);
////        $criteria->compare('LOWER(tanggal_lahir)', strtolower($this->tanggal_lahir), true);
////        $criteria->compare('LOWER(alamat_pasien)', strtolower($this->alamat_pasien), true);
////        $criteria->compare('rt', $this->rt);
////        $criteria->compare('rw', $this->rw);
////        $criteria->compare('LOWER(statusperkawinan)', strtolower($this->statusperkawinan), true);
////        $criteria->compare('LOWER(agama)', strtolower($this->agama), true);
////        $criteria->compare('LOWER(golongandarah)', strtolower($this->golongandarah), true);
////        $criteria->compare('LOWER(rhesus)', strtolower($this->rhesus), true);
////        $criteria->compare('anakke', $this->anakke);
////        $criteria->compare('jumlah_bersaudara', $this->jumlah_bersaudara);
////        $criteria->compare('LOWER(no_telepon_pasien)', strtolower($this->no_telepon_pasien), true);
////        $criteria->compare('LOWER(no_mobile_pasien)', strtolower($this->no_mobile_pasien), true);
////        $criteria->compare('LOWER(warga_negara)', strtolower($this->warga_negara), true);
////        $criteria->compare('LOWER(photopasien)', strtolower($this->photopasien), true);
////        $criteria->compare('LOWER(alamatemail)', strtolower($this->alamatemail), true);
////        $criteria->compare('pendaftaran_id', $this->pendaftaran_id);
////        $criteria->compare('LOWER(no_pendaftaran)', strtolower($this->no_pendaftaran), true);
////        $criteria->compare('LOWER(tgl_pendaftaran)', strtolower($this->tgl_pendaftaran), true);
////        $criteria->compare('LOWER(umur)', strtolower($this->umur), true);
////        $criteria->compare('LOWER(no_asuransi)', strtolower($this->no_asuransi), true);
////        $criteria->compare('LOWER(namapemilik_asuransi)', strtolower($this->namapemilik_asuransi), true);
////        $criteria->compare('LOWER(nopokokperusahaan)', strtolower($this->nopokokperusahaan), true);
////        $criteria->compare('LOWER(namaperusahaan)', strtolower($this->namaperusahaan), true);
////        $criteria->compare('LOWER(tglselesaiperiksa)', strtolower($this->tglselesaiperiksa), true);
////        $criteria->compare('tindakanpelayanan_id', $this->tindakanpelayanan_id);
        if (is_array($this->penjamin_id)){
            $criteria->compare('penjamin_id', $this->penjamin_id);
        }else{
            $criteria->addCondition('penjamin_id is null');
        }
////        $criteria->compare('LOWER(penjamin_nama)', strtolower($this->penjamin_nama), true);
////        $criteria->compare('carabayar_id', $this->carabayar_id);
////        $criteria->compare('LOWER(carabayar_nama)', strtolower($this->carabayar_nama), true);
        if (is_array($this->kelaspelayanan_id)){
            $criteria->compare('kelaspelayanan_id', $this->kelaspelayanan_id);
        }else{
            $criteria->addCondition('kelaspelayanan_id is null');
        }
////        $criteria->compare('LOWER(kelaspelayanan_nama)', strtolower($this->kelaspelayanan_nama), true);
////        $criteria->compare('instalasi_id', $this->instalasi_id);
////        $criteria->compare('LOWER(instalasi_nama)', strtolower($this->instalasi_nama), true);
//        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));

        
        return $criteria;
    }

    public function getNamaModel() {
        return __CLASS__;
    }

}