<?php

class RJInfokunjunganrjV extends InfokunjunganrjV {

    public $tglAwal;
    public $tglAkhir;
    public $data;
    public $jumlah;
    public $tick;
    public $jml_kunjungan;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PasienM the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchDaftarPasien() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->addBetweenCondition('t.tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('LOWER(t.no_pendaftaran)', strtolower($this->no_pendaftaran), true);
        $criteria->compare('LOWER(t.no_rekam_medik)', strtolower($this->no_rekam_medik), true);
        $criteria->compare('LOWER(t.nama_pasien)', strtolower($this->nama_pasien), true);
        $criteria->compare('LOWER(t.statusperiksa)', strtolower($this->statusperiksa), true);

        //agar yang statusperiksa = batal periksa tidak ditampilkan
        /*$condition = "t.statusperiksa <> 'BATAL PERIKSA'";
        $criteria->addCondition($condition);
         *
         */


        $criteria->compare('t.ruangan_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->with = array('pendaftaran');
        $criteria->order = 't.tgl_pendaftaran DESC';
        //$criteria->condition = 't.statusperiksa <> $batal';





        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchKunjunganPasien() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        //$criteria->addBetweenCondition('t.tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('LOWER(t.no_pendaftaran)', strtolower($this->no_pendaftaran), true);
        $criteria->compare('LOWER(t.no_rekam_medik)', strtolower($this->no_rekam_medik), true);
        $criteria->compare('LOWER(t.nama_pasien)', strtolower($this->nama_pasien), true);
        $criteria->compare('LOWER(t.statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('LOWER(t.ruangan_nama)', strtolower($this->ruangan_nama), true);
        $criteria->compare('LOWER(t.nama_pegawai)', strtolower($this->nama_pegawai), true);
        $criteria->compare('date(t.tgl_pendaftaran)',$this->tgl_pendaftaran);
        $criteria->compare('LOWER(t.jeniskasuspenyakit_nama)', strtolower($this->jeniskasuspenyakit_nama), true);
        $criteria->compare('LOWER(t.statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('t.ruangan_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->with = array('pendaftaran');
        //$criteria->condition = 'pasienpulang.pendaftaran_id = t.pendaftaran_id';
        $criteria->order = 't.tgl_pendaftaran DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            //'pasienpulang'=> array(self::HAS_ONE, 'PasienpulangT', 'pendaftaran_id'),
            'pendaftaran' => array(self::BELONGS_TO, 'RJPendaftaranT', 'pendaftaran_id'),
                //'operasi'=>array(self::BELONGS_TO, 'OperasiM', 'operasi_id'),
        );
    }

    public function primaryKey() {
        return 'pendaftaran_id';
    }

    public static function berdasarkanStatus() {
        $status = array('pengunjung' => 'Berdasarkan Pengunjung',
            'kunjungan' => 'Berdasarkan Kunjungan',
            'rujukan' => 'Berdasarkan Rujukan'
        );
        return $status;
    }

    public function getNamaModel() {
        return __CLASS__;
    }

    public function searchTable() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('pasien_id', $this->pasien_id);
        $criteria->addBetweenCondition('tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('LOWER(jenisidentitas)', strtolower($this->jenisidentitas), true);
        $criteria->compare('LOWER(no_identitas_pasien)', strtolower($this->no_identitas_pasien), true);
        $criteria->compare('LOWER(namadepan)', strtolower($this->namadepan), true);
        $criteria->compare('LOWER(nama_pasien)', strtolower($this->nama_pasien), true);
        $criteria->compare('LOWER(nama_bin)', strtolower($this->nama_bin), true);
        $criteria->compare('LOWER(jeniskelamin)', strtolower($this->jeniskelamin), true);
        $criteria->compare('LOWER(tempat_lahir)', strtolower($this->tempat_lahir), true);
        $criteria->compare('LOWER(tanggal_lahir)', strtolower($this->tanggal_lahir), true);
        $criteria->compare('LOWER(alamat_pasien)', strtolower($this->alamat_pasien), true);
        $criteria->compare('rt', $this->rt);
        $criteria->compare('rw', $this->rw);
        $criteria->compare('LOWER(agama)', strtolower($this->agama), true);
        $criteria->compare('LOWER(golongandarah)', strtolower($this->golongandarah), true);
        $criteria->compare('LOWER(photopasien)', strtolower($this->photopasien), true);
        $criteria->compare('LOWER(alamatemail)', strtolower($this->alamatemail), true);
        $criteria->compare('LOWER(statusrekammedis)', strtolower($this->statusrekammedis), true);
        $criteria->compare('LOWER(statusperkawinan)', strtolower($this->statusperkawinan), true);
        $criteria->compare('LOWER(no_rekam_medik)', strtolower($this->no_rekam_medik), true);
        $criteria->compare('LOWER(tgl_rekam_medik)', strtolower($this->tgl_rekam_medik), true);
        $criteria->compare('propinsi_id', $this->propinsi_id);
        $criteria->compare('LOWER(propinsi_nama)', strtolower($this->propinsi_nama), true);
        $criteria->compare('kabupaten_id', $this->kabupaten_id);
        $criteria->compare('LOWER(kabupaten_nama)', strtolower($this->kabupaten_nama), true);
        $criteria->compare('kelurahan_id', $this->kelurahan_id);
        $criteria->compare('LOWER(kelurahan_nama)', strtolower($this->kelurahan_nama), true);
        $criteria->compare('kecamatan_id', $this->kecamatan_id);
        $criteria->compare('LOWER(kecamatan_nama)', strtolower($this->kecamatan_nama), true);
        $criteria->compare('pendaftaran_id', $this->pendaftaran_id);
        $criteria->compare('pekerjaan_id', $this->pekerjaan_id);
        $criteria->compare('LOWER(pekerjaan_nama)', strtolower($this->pekerjaan_nama), true);
        $criteria->compare('LOWER(no_pendaftaran)', strtolower($this->no_pendaftaran), true);
        $criteria->compare('LOWER(tgl_pendaftaran)', strtolower($this->tgl_pendaftaran), true);
        $criteria->compare('LOWER(no_urutantri)', strtolower($this->no_urutantri), true);
        $criteria->compare('LOWER(transportasi)', strtolower($this->transportasi), true);
        $criteria->compare('LOWER(keadaanmasuk)', strtolower($this->keadaanmasuk), true);
        $criteria->compare('LOWER(statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('LOWER(statuspasien)', strtolower($this->statuspasien), true);
        $criteria->compare('LOWER(kunjungan)', strtolower($this->kunjungan), true);
        $criteria->compare('alihstatus', $this->alihstatus);
        $criteria->compare('byphone', $this->byphone);
        $criteria->compare('kunjunganrumah', $this->kunjunganrumah);
        $criteria->compare('LOWER(statusmasuk)', strtolower($this->statusmasuk), true);
        $criteria->compare('LOWER(umur)', strtolower($this->umur), true);
        $criteria->compare('LOWER(no_asuransi)', strtolower($this->no_asuransi), true);
        $criteria->compare('LOWER(namapemilik_asuransi)', strtolower($this->namapemilik_asuransi), true);
        $criteria->compare('LOWER(nopokokperusahaan)', strtolower($this->nopokokperusahaan), true);
        $criteria->compare('LOWER(create_time)', strtolower($this->create_time), true);
        $criteria->compare('LOWER(create_loginpemakai_id)', strtolower($this->create_loginpemakai_id), true);
        $criteria->compare('LOWER(create_ruangan)', strtolower($this->create_ruangan), true);
        $criteria->compare('LOWER(gelardepan)', strtolower($this->gelardepan), true);
        $criteria->compare('LOWER(nama_pegawai)', strtolower($this->nama_pegawai), true);
        $criteria->compare('LOWER(gelarbelakang_nama)', strtolower($this->gelarbelakang_nama), true);
        $criteria->compare('carabayar_id', $this->carabayar_id);
        $criteria->compare('LOWER(carabayar_nama)', strtolower($this->carabayar_nama), true);
        $criteria->compare('penjamin_id', $this->penjamin_id);
        $criteria->compare('LOWER(penjamin_nama)', strtolower($this->penjamin_nama), true);
        $criteria->compare('caramasuk_id', $this->caramasuk_id);
        $criteria->compare('LOWER(caramasuk_nama)', strtolower($this->caramasuk_nama), true);
        $criteria->compare('shift_id', $this->shift_id);
        $criteria->compare('golonganumur_id', $this->golonganumur_id);
        $criteria->compare('LOWER(golonganumur_nama)', strtolower($this->golonganumur_nama), true);
        $criteria->compare('LOWER(no_rujukan)', strtolower($this->no_rujukan), true);
        $criteria->compare('LOWER(nama_perujuk)', strtolower($this->nama_perujuk), true);
        $criteria->compare('LOWER(tanggal_rujukan)', strtolower($this->tanggal_rujukan), true);
        $criteria->compare('LOWER(diagnosa_rujukan)', strtolower($this->diagnosa_rujukan), true);
        $criteria->compare('asalrujukan_id', $this->asalrujukan_id);
        $criteria->compare('LOWER(asalrujukan_nama)', strtolower($this->asalrujukan_nama), true);
        $criteria->compare('penanggungjawab_id', $this->penanggungjawab_id);
        $criteria->compare('LOWER(pengantar)', strtolower($this->pengantar), true);
        $criteria->compare('LOWER(hubungankeluarga)', strtolower($this->hubungankeluarga), true);
        $criteria->compare('LOWER(nama_pj)', strtolower($this->nama_pj), true);
        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->compare('LOWER(ruangan_nama)', strtolower($this->ruangan_nama), true);
        $criteria->compare('instalasi_id', $this->instalasi_id);
        $criteria->compare('LOWER(instalasi_nama)', strtolower($this->instalasi_nama), true);
        $criteria->compare('jeniskasuspenyakit_id', $this->jeniskasuspenyakit_id);
        $criteria->compare('LOWER(jeniskasuspenyakit_nama)', strtolower($this->jeniskasuspenyakit_nama), true);
        $criteria->compare('kelaspelayanan_id', $this->kelaspelayanan_id);
        $criteria->compare('LOWER(kelaspelayanan_nama)', strtolower($this->kelaspelayanan_nama), true);
        $criteria->order = 'tgl_pendaftaran DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchGrafik() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.



        $criteria = MyFunction::criteriaGrafik1($this, 'tick');
        if (!empty($criteria->group) &&(!empty($this->pilihanx))){
            $criteria->group .=',';
        }
        if ($this->pilihanx == 'pengunjung') {
            $criteria->select .= ', statuspasien as data';
            $criteria->group .= ' statuspasien';
        } else if ($this->pilihanx == 'kunjungan') {
            $criteria->select .= ', kunjungan as data';
            $criteria->group .= ' kunjungan';
        }
        else if ($this->pilihanx == 'rujukan'){
            $criteria->select .= ', statusmasuk as data';
            $criteria->group .= ' statusmasuk';
        }

        if (empty($this->pilihanx)){
            $criteria = MyFunction::criteriaGrafik1($this, 'data');
        }

        $criteria->addBetweenCondition('DATE(tgl_pendaftaran)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('propinsi_id', $this->propinsi_id);
        $criteria->compare('LOWER(propinsi_nama)', strtolower($this->propinsi_nama), true);
        $criteria->compare('kabupaten_id', $this->kabupaten_id);
        $criteria->compare('LOWER(kabupaten_nama)', strtolower($this->kabupaten_nama), true);
        $criteria->compare('kelurahan_id', $this->kelurahan_id);
        $criteria->compare('LOWER(kelurahan_nama)', strtolower($this->kelurahan_nama), true);
        $criteria->compare('kecamatan_id', $this->kecamatan_id);
        $criteria->compare('LOWER(kecamatan_nama)', strtolower($this->kecamatan_nama), true);
        $criteria->compare('carabayar_id', $this->carabayar_id);
        $criteria->compare('LOWER(carabayar_nama)', strtolower($this->carabayar_nama), true);
        $criteria->compare('penjamin_id', $this->penjamin_id);
        $criteria->compare('LOWER(penjamin_nama)', strtolower($this->penjamin_nama), true);
        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->compare('LOWER(ruangan_nama)', strtolower($this->ruangan_nama), true);
//        $criteria->order = 'tgl_pendaftaran DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchPrint() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->addCondition('tgl_pendaftaran BETWEEN \'' . $this->tglAwal . '\' AND \'' . $this->tglAkhir . '\'');
        $criteria->compare('pasien_id', $this->pasien_id);
        $criteria->compare('LOWER(jenisidentitas)', strtolower($this->jenisidentitas), true);
        $criteria->compare('LOWER(no_identitas_pasien)', strtolower($this->no_identitas_pasien), true);
        $criteria->compare('LOWER(namadepan)', strtolower($this->namadepan), true);
        $criteria->compare('LOWER(nama_pasien)', strtolower($this->nama_pasien), true);
        $criteria->compare('LOWER(nama_bin)', strtolower($this->nama_bin), true);
        $criteria->compare('LOWER(jeniskelamin)', strtolower($this->jeniskelamin), true);
        $criteria->compare('LOWER(tempat_lahir)', strtolower($this->tempat_lahir), true);
        $criteria->compare('LOWER(tanggal_lahir)', strtolower($this->tanggal_lahir), true);
        $criteria->compare('LOWER(alamat_pasien)', strtolower($this->alamat_pasien), true);
        $criteria->compare('rt', $this->rt);
        $criteria->compare('rw', $this->rw);
        $criteria->compare('LOWER(agama)', strtolower($this->agama), true);
        $criteria->compare('LOWER(golongandarah)', strtolower($this->golongandarah), true);
        $criteria->compare('LOWER(photopasien)', strtolower($this->photopasien), true);
        $criteria->compare('LOWER(alamatemail)', strtolower($this->alamatemail), true);
        $criteria->compare('LOWER(statusrekammedis)', strtolower($this->statusrekammedis), true);
        $criteria->compare('LOWER(statusperkawinan)', strtolower($this->statusperkawinan), true);
        $criteria->compare('LOWER(no_rekam_medik)', strtolower($this->no_rekam_medik), true);
        $criteria->compare('LOWER(tgl_rekam_medik)', strtolower($this->tgl_rekam_medik), true);
        $criteria->compare('propinsi_id', $this->propinsi_id);
        $criteria->compare('LOWER(propinsi_nama)', strtolower($this->propinsi_nama), true);
        $criteria->compare('kabupaten_id', $this->kabupaten_id);
        $criteria->compare('LOWER(kabupaten_nama)', strtolower($this->kabupaten_nama), true);
        $criteria->compare('kelurahan_id', $this->kelurahan_id);
        $criteria->compare('LOWER(kelurahan_nama)', strtolower($this->kelurahan_nama), true);
        $criteria->compare('kecamatan_id', $this->kecamatan_id);
        $criteria->compare('LOWER(kecamatan_nama)', strtolower($this->kecamatan_nama), true);
        $criteria->compare('pendaftaran_id', $this->pendaftaran_id);
        $criteria->compare('pekerjaan_id', $this->pekerjaan_id);
        $criteria->compare('LOWER(pekerjaan_nama)', strtolower($this->pekerjaan_nama), true);
        $criteria->compare('LOWER(no_pendaftaran)', strtolower($this->no_pendaftaran), true);
        $criteria->compare('LOWER(tgl_pendaftaran)', strtolower($this->tgl_pendaftaran), true);
        $criteria->compare('LOWER(no_urutantri)', strtolower($this->no_urutantri), true);
        $criteria->compare('LOWER(transportasi)', strtolower($this->transportasi), true);
        $criteria->compare('LOWER(keadaanmasuk)', strtolower($this->keadaanmasuk), true);
        $criteria->compare('LOWER(statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('LOWER(statuspasien)', strtolower($this->statuspasien), true);
        $criteria->compare('LOWER(kunjungan)', strtolower($this->kunjungan), true);
        $criteria->compare('alihstatus', $this->alihstatus);
        $criteria->compare('byphone', $this->byphone);
        $criteria->compare('kunjunganrumah', $this->kunjunganrumah);
        $criteria->compare('LOWER(statusmasuk)', strtolower($this->statusmasuk), true);
        $criteria->compare('LOWER(umur)', strtolower($this->umur), true);
        $criteria->compare('LOWER(no_asuransi)', strtolower($this->no_asuransi), true);
        $criteria->compare('LOWER(namapemilik_asuransi)', strtolower($this->namapemilik_asuransi), true);
        $criteria->compare('LOWER(nopokokperusahaan)', strtolower($this->nopokokperusahaan), true);
        $criteria->compare('LOWER(create_time)', strtolower($this->create_time), true);
        $criteria->compare('LOWER(create_loginpemakai_id)', strtolower($this->create_loginpemakai_id), true);
        $criteria->compare('LOWER(create_ruangan)', strtolower($this->create_ruangan), true);
        $criteria->compare('LOWER(gelardepan)', strtolower($this->gelardepan), true);
        $criteria->compare('LOWER(nama_pegawai)', strtolower($this->nama_pegawai), true);
        $criteria->compare('LOWER(gelarbelakang_nama)', strtolower($this->gelarbelakang_nama), true);
        $criteria->compare('carabayar_id', $this->carabayar_id);
        $criteria->compare('LOWER(carabayar_nama)', strtolower($this->carabayar_nama), true);
        $criteria->compare('penjamin_id', $this->penjamin_id);
        $criteria->compare('LOWER(penjamin_nama)', strtolower($this->penjamin_nama), true);
        $criteria->compare('caramasuk_id', $this->caramasuk_id);
        $criteria->compare('LOWER(caramasuk_nama)', strtolower($this->caramasuk_nama), true);
        $criteria->compare('shift_id', $this->shift_id);
        $criteria->compare('golonganumur_id', $this->golonganumur_id);
        $criteria->compare('LOWER(golonganumur_nama)', strtolower($this->golonganumur_nama), true);
        $criteria->compare('LOWER(no_rujukan)', strtolower($this->no_rujukan), true);
        $criteria->compare('LOWER(nama_perujuk)', strtolower($this->nama_perujuk), true);
        $criteria->compare('LOWER(tanggal_rujukan)', strtolower($this->tanggal_rujukan), true);
        $criteria->compare('LOWER(diagnosa_rujukan)', strtolower($this->diagnosa_rujukan), true);
        $criteria->compare('asalrujukan_id', $this->asalrujukan_id);
        $criteria->compare('LOWER(asalrujukan_nama)', strtolower($this->asalrujukan_nama), true);
        $criteria->compare('penanggungjawab_id', $this->penanggungjawab_id);
        $criteria->compare('LOWER(pengantar)', strtolower($this->pengantar), true);
        $criteria->compare('LOWER(hubungankeluarga)', strtolower($this->hubungankeluarga), true);
        $criteria->compare('LOWER(nama_pj)', strtolower($this->nama_pj), true);
        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->compare('LOWER(ruangan_nama)', strtolower($this->ruangan_nama), true);
        $criteria->compare('instalasi_id', $this->instalasi_id);
        $criteria->compare('LOWER(instalasi_nama)', strtolower($this->instalasi_nama), true);
        $criteria->compare('jeniskasuspenyakit_id', $this->jeniskasuspenyakit_id);
        $criteria->compare('LOWER(jeniskasuspenyakit_nama)', strtolower($this->jeniskasuspenyakit_nama), true);
        $criteria->compare('kelaspelayanan_id', $this->kelaspelayanan_id);
        $criteria->compare('LOWER(kelaspelayanan_nama)', strtolower($this->kelaspelayanan_nama), true);
        $criteria->order = 'tgl_pendaftaran DESC';
        // Klo limit lebih kecil dari nol itu berarti ga ada limit
        $criteria->limit = -1;

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => false,
                ));
    }

    public function getTotaltagihan(){
        $criteria = new CDbCriteria();
        $criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
        $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
        $criteria->select = 'SUM((tarif_satuan * qty_tindakan) + tarifcyto_tindakan + biayaservice + biayaadministrasi + biayakonseling) AS tarif_tindakan';
        $jumlah = RinciantagihanpasienV::model()->find($criteria)->tarif_tindakan;
        if (empty($jumlah)){
            $jumlah = 0;
        }
        return $jumlah;
    }

    public function getTotals(){
        $criteria = new CDbCriteria();
        $criteria->select = 'sum(tarif_tindakan) as tarif_tindakan';
        $criteria->compare('pendaftaran_id', $this->pendaftaran_id);
        $jumlah = RinciantagihanpasienV::model()->find($criteria)->tarif_tindakan;
        if (empty($jumlah)){
            $jumlah = 0;
        }
        return $jumlah;
    }

    public function searchDaftarPasienRincian() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->addBetweenCondition('t.tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('LOWER(t.no_pendaftaran)', strtolower($this->no_pendaftaran), true);
        $criteria->compare('LOWER(t.no_rekam_medik)', strtolower($this->no_rekam_medik), true);
        $criteria->compare('LOWER(t.nama_pasien)', strtolower($this->nama_pasien), true);
        $criteria->compare('LOWER(t.statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('t.ruangan_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->order = 't.tgl_pendaftaran DESC';
        if ($this->statusBayar == 'LUNAS'){

            $criteria->addCondition('pendaftaran.pembayaranpelayanan_id is not null');
        }else if ($this->statusBayar == 'BELUM LUNAS'){
            $criteria->addCondition('pendaftaran.pembayaranpelayanan_id is null');
        }

        echo $this->statusBayar;
        $criteria->with = array('pendaftaran');
        //$criteria->condition = 'pasienpulang.pendaftaran_id = t.pendaftaran_id';
        //$criteria->order = 'no_urutantri';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

     public function getStatus($status,$id){
        if($status == "ANTRIAN"){
            $status = '<button id="red" class="btn btn-primary" name="yt1">'.$status.'</button>';

        }else if($status == "SEDANG PERIKSA"){
            $status = '<button id="green" class="btn btn-danger" name="yt1" onclick="setStatus(this,\''.$status.'\','.$id.')">'.$status.'</button>';
        }else if($status == "SUDAH PULANG"){
            $status = '<button id="blue" class="btn btn-danger-yellow" name="yt1" onclick="setStatus(this,\''.$status.'\','.$id.')">'.$status.'</button>';
        }else{
            $status = '<button id="orange" class="btn btn-danger-blue"  name="yt1">'.$status.'</button>';
        }
        return $status;
    }
    public function getPeriksaPasien($status,$id,$pembayaran,$nopen,$alih){
        if($pembayaran != null){
            $status = '<center><a id='.$id.' href="#"  onclick="cekStatus(\''.$status.'\')" rel="tooltip" data-original-title="Klik untuk Pemeriksaan Pasien">
                            <i class="icon-list-alt"></i>
                            </a></center>';
        }else{
            if($status == "ANTRIAN"){
                $status = "<center><a id=".$nopen." href=\"index.php?r=rawatJalan/anamnesa&idPendaftaran=".$id."\" rel=\"tooltip\" data-original-title=\"Klik untuk Pemeriksaan Pasien\">
                            <i class=\"icon-list-alt\"></i>
                            </a></center>";

            }else if($status == "SEDANG PERIKSA"){
                $status = "<center><a id=".$nopen." href=\"index.php?r=rawatJalan/anamnesa&idPendaftaran=".$id."\" rel=\"tooltip\" data-original-title=\"Klik untuk Pemeriksaan Pasien\">
                            <i class=\"icon-list-alt\"></i>
                            </a></center>";
            }else if($status == "BATAL PERIKSA" || $status =="DIBATALKAN" || $status == "SEDANG DIRAWAT INAP" || $alih == true || $status == "SUDAH PULANG"){
                 $status = '<center><a id='.$id.' href="#"  onclick="cekStatus(\''.$status.'\')" rel="tooltip" data-original-title="Klik untuk Pemeriksaan Pasien">
                            <i class="icon-list-alt"></i>
                            </a></center>';
            }else{
                $status = "<center><a id=".$nopen." href=\"index.php?r=rawatJalan/anamnesa&idPendaftaran=".$id."\" rel=\"tooltip\" data-original-title=\"Klik untuk Pemeriksaan Pasien\">
                            <i class=\"icon-list-alt\"></i>
                            </a></center>";
            }
        }

        return $status;
    }

	 public function getTindakLanjut($status, $id, $pembayaran, $nopen, $pasienpulang, $alih)
	 {
		/*
		if($status == "ANTRIAN" || $status == "BATAL PERIKSA" || $status == "DIBATALKAN"){
			$status = '<center><a id='.$id.' href="#" onclick="cekStatus(\''. $status .'\')" rel="tooltip" data-original-title="Klik untuk Proses Tindak Lanjut Pasien"><i class="icon-user"></i></a></center>';
		}else if($status == "SEDANG PERIKSA" || $status == "SUDAH PULANG"){
			 $status = "<center><a id=".$id." href=\"javascript:tindaklanjutrawatjalan('$id')\" rel=\"tooltip\" data-original-title=\"Klik untuk Proses Tindak Lanjut Pasien\"><i class=\"icon-user\"></i></a></center>";
		}else if(!empty($pasienpulang) || ($status==Params::statusPeriksa(6)) && $alih = true){
			$cek = PasienadmisiT::model()->findbyAttributes(array('pendaftaran_id'=>$id));
			if(count($cek)>0){
				$status = "<center>Pasien di Rawat Inap</center>";
			}else{
				$status = "<center>Pasien di Rawat Inap <a id=".$id." href=\"javascript:cekHakAkses('$id')\" rel=\"tooltip\" data-original-title=\"Klik untuk Batal Rawat Inap\"><i class=\"icon-remove\"></i></a></center>";
			}
		}else{
			 $status = "<center><a id=".$id." href=\"javascript:tindaklanjutrawatjalan('$id')\" rel=\"tooltip\" data-original-title=\"Klik untuk Proses Tindak Lanjut Pasien\"><i class=\"icon-user\"></i></a></center>";
		}
        //return $status;
		*/

		$result = $status;
		if($status == "SUDAH PULANG")
		{
			$cek = PasienadmisiT::model()->findbyAttributes(array('pendaftaran_id'=>$id));
			if(count($cek)>0){
				$result = "<center>Pasien di Rawat Inap <a id=".$id." href=\"javascript:cekHakAkses('$id')\" rel=\"tooltip\" data-original-title=\"Klik untuk Batal Rawat Inap\"><i class=\"icon-remove\"></i></a></center>";
			}
		}

		if($status == "SEDANG PERIKSA")
		{
			$result = "<center><a id=".$id." href=\"javascript:tindaklanjutrawatjalan('$id')\" rel=\"tooltip\" data-original-title=\"Klik untuk Proses Tindak Lanjut Pasien\"><i class=\"icon-user\"></i></a></center>";
		}
		
        if($status == "SEDANG DIRAWAT INAP"){
            $cek_admisi = PasienadmisiT::model()->findbyAttributes(array('pendaftaran_id'=>$id));
            if(empty($cek_admisi) && is_null($cek_admisi)) {
                $result = CHtml::link('<i class="icon-remove"></i>', "#", array(
                    "rel"=>"tooltip",
                    "data-original-title"=>"Klik untuk proses pembatalan ranap",
                    "onclick"=>"prosesPembatalanRanap($id);return false;"
                ));
            }
        }
        return $result;
    }

	public function getTindakPulang($data)
	{
		if($data->statusperiksa == "SUDAH PULANG")
		{
			$result = '<a href="index.php?r=rawatJalan/daftarPasien/BatalPulang&pendaftaran_id='. $data->pendaftaran_id .'&dialog=1" onClick="$(\'#dialogPasienBatalPulang\').dialog(\'open\');" target="iframePasienBatalPulang" rel="tooltip" data-original-title="Klik untuk batal pemulangan pasien"><i class="icon-remove"></i></a>';
		}else{
			$result = '<a href="index.php?r=rawatJalan/daftarPasien/PasienPulang&idPendaftaran='. $data->pendaftaran_id .'&dialog=1" onClick="$(\'#dialogPasienPulang\').dialog(\'open\');" target="iframePasienPulang" rel="tooltip" data-original-title="Klik untuk pemulangan pasien"><i class="icon-edit"></i></a>';
		}
        return $result;
    }

    public function getTindakPulangNew($data)
	{
		if($data->statusperiksa == "SUDAH PULANG")
		{
			$result = '<a href="index.php?r=rawatJalan/daftarPasienRajal/BatalPulang&pendaftaran_id='. $data->pendaftaran_id .'&dialog=1" onClick="$(\'#dialogPasienBatalPulang\').dialog(\'open\');" target="iframePasienBatalPulang" rel="tooltip" data-original-title="Klik untuk batal pemulangan pasien"><i class="icon-remove"></i></a>';
		}else{
			$result = '<a href="index.php?r=rawatJalan/daftarPasienRajal/PasienPulang&idPendaftaran='. $data->pendaftaran_id .'&dialog=1" onClick="$(\'#dialogPasienPulang\').dialog(\'open\');" target="iframePasienPulang" rel="tooltip" data-original-title="Klik untuk pemulangan pasien"><i class="icon-edit"></i></a>';
		}
        return $result;
    }

	public function getKirimFarmasi($data)
	{
		$criteria = new CDbCriteria;
		$criteria->select = "MAX(pendaftaran_id) AS pendaftaran_id, komentar, status_kirim";
		$criteria->compare("pendaftaran_id", $data->pendaftaran_id);
		$criteria->compare("status_kirim", "A");
		$criteria->group = "komentar, status_kirim";
		$logKirimFarmasi = LogKirimFarmasi::model()->find($criteria);

		$result = " - ";
		if($logKirimFarmasi && $logKirimFarmasi->status_kirim == 'A'){
			$result = '<a href="index.php?r=rawatJalan/daftarPasienRajal/batalKirimFarmasi&pendaftaran_id='. $data->pendaftaran_id .'&dialog=1" onClick="$(\'#dialogPasienBatalPulang\').dialog(\'open\');" target="iframePasienBatalPulang" rel="tooltip" data-original-title="Klik untuk batal pemulangan pasien"><i class="icon-remove"></i></a>';
		}else{
			$result = '<a href="index.php?r=rawatJalan/daftarPasienRajal/kirimFarmasi&pendaftaran_id='. $data->pendaftaran_id .'&dialog=1" onClick="$(\'#dialogPasienPulang\').dialog(\'open\');" target="iframePasienPulang" rel="tooltip" data-original-title="Klik untuk pemulangan pasien"><i class="icon-edit"></i></a>';
		}
        return $result;
    }

    public function getStatusFarmasi()
	{
        $criteria = new CDbCriteria;
		$criteria->compare("pendaftaran_id", $this->pendaftaran_id);
		$pendaftaranT = PendaftaranT::model()->find($criteria);
		$ruangan = array();
		if($pendaftaranT->statusfarmasi == false){
			$ruangan[] = "+ Ruangan : APOTEK FARMASI > Status pasien belum dikonfirmasi";
		}
        return (count($ruangan) > 0 ? "Belum" : "Sudah");
    }

    public function searchPasienByKonfirmasi()
	{
        $criteria = new CDbCriteria;
		
        $criteria->compare('LOWER(t.no_pendaftaran)', strtolower($this->no_pendaftaran), true);
        $criteria->compare('LOWER(t.no_rekam_medik)', strtolower($this->no_rekam_medik), true);
        $criteria->compare('LOWER(t.nama_pasien)', strtolower($this->nama_pasien), true);
        $criteria->compare('LOWER(t.statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('LOWER(t.ruangan_nama)', strtolower($this->ruangan_nama), true);
        $criteria->compare('LOWER(t.nama_pegawai)', strtolower($this->nama_pegawai), true);
        $criteria->compare('date(t.tgl_pendaftaran)',$this->tgl_pendaftaran);
        $criteria->compare('LOWER(t.jeniskasuspenyakit_nama)', strtolower($this->jeniskasuspenyakit_nama), true);
        $criteria->compare('LOWER(t.statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('t.ruangan_id', $this->ruangan_id);
		
		$criteria->addBetweenCondition('t.tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
		$criteria->join = "LEFT JOIN pasienadmisi_t ON pasienadmisi_t.pendaftaran_id = t.pendaftaran_id";
		$criteria->addCondition("pasienadmisi_t.pendaftaran_id IS NULL AND t.pembayaranpelayanan_id IS NULL");
		
		/*
        $criteria->with = array('pendaftaran');
        $criteria->addCondition("t.pendaftaran_id NOT IN (
			SELECT log_kirim_farmasi.pendaftaran_id FROM log_kirim_farmasi WHERE status_kirim = 'A'
		) AND pendaftaran.kirim_farmasi = false");
		*/
        $criteria->order = 't.tgl_pendaftaran DESC';
        return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
    }

}

?>
