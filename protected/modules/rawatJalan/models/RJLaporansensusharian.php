<?php

class RJLaporansensusharian extends LaporansensuharianrjV {

    public $data;
    public $jumlah;
    public $tick;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchTable() {
        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchGrafik() {
        
        $select = ' select nama_pasien, nama_bin, no_rekam_medik, umur, jeniskelamin, alamat_pasien, kunjungan, statuspasien, diagnosa_nama, carabayar_nama, carabayar_id, penjamin_nama, penjamin_id, pendaftaran_id, ruangan_id, propinsi_id, kecamatan_id, kelurahan_id, kabupaten_id, propinsi_nama, kecamatan_nama, kelurahan_nama, kabupaten_nama';
        $select2 = 'select count(pendaftaran_id) as jumlah ';
        $group2 = '';
        $sql = ' from laporansensuharianrj_v as t where (date(tgl_pendaftaran) between \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\') and ruangan_id = '.Yii::app()->user->getState('ruangan_id');
        $where = '';
        
        if (!empty($this->pilihanx)){
            $type = 'tick';
        }else{
            $type = 'data';
        }
        
        if ($_GET['filter'] == 'carabayar') {
            if (!empty($this->penjamin_id)) {
                $select2 .= ', penjamin_nama as '.$type;
                $group2 .= ' penjamin_nama';
                $where .= ' and penjamin_id = '.$this->penjamin_id;
            } else if (!empty($this->carabayar_id)) {
                $select2 .= ', penjamin_nama as '.$type;
                $group2 .= ' penjamin_nama';
                $where .= ' and carabayar_id = '.$this->carabayar_id;
            }
            else{
                $select2 .= ', carabayar_nama as '.$type;
                $group2 .= ' carabayar_nama';
            }
        } else if ($_GET['filter'] == 'wilayah') {
            
            if (!empty($this->kelurahan_id)) {
                $select2 .= ', kelurahan_nama as '.$type;
                $group2 .= ' kelurahan_nama';
                $where .= ' and kelurahan_id = '.$this->kelurahan_id;
            } else if (!empty($this->kecamatan_id)) {
                $select2 .= ', kecamatan_nama as tick, kelurahan_nama as data';
                $group2 .= ' kelurahan_nama,kecamatan_nama';
                $where .= ' and kecamatan_id = '.$this->kecamatan_id;
            } else if (!empty($this->kabupaten_id)) {
                $select2 .= ', kabupaten_nama as tick, kecamatan_nama as data';
                $group2 .= ' kecamatan_nama,kabupaten_nama';
                $where .= ' and kabupaten_id = '.$this->kabupaten_id;
            } else if (!empty($this->propinsi_id)) {
                $select2 .= ', propinsi_nama as tick, kabupaten_nama as data';
                $group2 .= ' kabupaten_nama,propinsi_nama';
                $where .= ' and propinsi_id = '.$this->propinsi_id;
            } else {
                $select2 .= ', propinsi_nama as tick, propinsi_nama as data';
                $group2 .= ' propinsi_nama';
            }
        }
        
        $group = ' group by nama_pasien, nama_bin, no_rekam_medik, umur, jeniskelamin, alamat_pasien, diagnosa_nama, carabayar_nama, carabayar_id, penjamin_nama, penjamin_id, pendaftaran_id, ruangan_id, propinsi_id, kecamatan_id, kelurahan_id, kabupaten_id, propinsi_nama, kecamatan_nama, kelurahan_nama, kabupaten_nama,statuspasien, kunjungan ';

        if (!empty($group2) && (!empty($this->pilihanx))) {
            $group2 .=',';
        }
        if ($this->pilihanx == 'pengunjung') {
            $select2 .= ', statuspasien as data';
            $group2 .= ' statuspasien';
        } else if ($this->pilihanx == 'kunjungan') {
            $select2 .= ', kunjungan as data';
            $group2 .= ' kunjungan';
        }
        
        $sql1 = $select.$sql.$where.$group;
        $sql2 = $select2.' from ('.$sql1.') x group by '.$group2;

        return new CSqlDataProvider($sql2);
        
    }

    public function searchPrint() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => false,
                ));
    }

    protected function functionCriteria() {
        $criteria = new CDbCriteria();
        
        $criteria->select = 'nama_pasien, nama_bin, no_rekam_medik, umur, jeniskelamin, alamat_pasien, kunjungan, statuspasien, diagnosa_nama, carabayar_nama, carabayar_id, penjamin_nama, penjamin_id, pendaftaran_id, ruangan_id, diagnosa_id, diagnosa_nama';
        $criteria->group = 'nama_pasien, nama_bin, no_rekam_medik, umur, jeniskelamin, alamat_pasien, kunjungan, statuspasien, diagnosa_nama, carabayar_nama, carabayar_id, penjamin_nama, penjamin_id, pendaftaran_id, ruangan_id, diagnosa_id, diagnosa_nama';
        
        $criteria->addBetweenCondition('tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('propinsi_id', $this->propinsi_id);
        $criteria->compare('LOWER(propinsi_nama)', strtolower($this->propinsi_nama), true);
        $criteria->compare('kabupaten_id', $this->kabupaten_id);
        $criteria->compare('LOWER(kabupaten_nama)', strtolower($this->kabupaten_nama), true);
        $criteria->compare('kelurahan_id', $this->kelurahan_id);
        $criteria->compare('LOWER(kelurahan_nama)', strtolower($this->kelurahan_nama), true);
        $criteria->compare('kecamatan_id', $this->kecamatan_id);
        $criteria->compare('LOWER(kecamatan_nama)', strtolower($this->kecamatan_nama), true);
        $criteria->compare('pendaftaran_id', $this->pendaftaran_id);
        $criteria->compare('carabayar_id', $this->carabayar_id);
        $criteria->compare('LOWER(carabayar_nama)', strtolower($this->carabayar_nama), true);
        $criteria->compare('penjamin_id', $this->penjamin_id);
        $criteria->compare('LOWER(penjamin_nama)', strtolower($this->penjamin_nama), true);
        
        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->compare('LOWER(ruangan_nama)', strtolower($this->ruangan_nama), true);
        
        return $criteria;
    }

    public function getNamaModel() {
        return __CLASS__;
    }

    public static function statusPasien() {
        $status = array('baru' => 'PENGUNJUNG BARU',
            'lama' => 'PENGUNJUNG LAMA');
        return $status;
    }

    public static function statusKunjungan() {
        $status = array('baru' => 'KUNJUNGAN BARU',
            'lama' => 'KUNJUNGAN LAMA');
        return $status;
    }

    public static function berdasarkanStatus() {
        $status = array('pengunjung' => 'Berdasarkan Pengunjung',
            'kunjungan' => 'Berdasarkan Kunjungan');
        return $status;
    }

}