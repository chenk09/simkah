<?php

class RJLaporanpendapatanruangan extends LaporanpendapatanruanganV {

    public $jumlah;
    public $data;
    public $tick;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchTable() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchGrafik() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        $criteria = $this->functionCriteria();

        $criteria->select = 'count(t.pendaftaran_id) as jumlah, t.kelaspelayanan_nama as data';
        $criteria->group = 't.kelaspelayanan_nama';
        if (!empty($this->carabayar_id)) {
            $criteria->select .= ', t.penjamin_nama as tick';
            $criteria->group .= ', t.penjamin_nama';
        } else {
            $criteria->select .= ', t.carabayar_nama as tick';
            $criteria->group .= ', t.carabayar_nama';
        }

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchPrint() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => false,
                ));
    }
    
    public function functionCriteria(){
        $criteria = new CDbCriteria();
        
        if (!is_array($this->kelaspelayanan_id)){
            $this->kelaspelayanan_id = 0;
        }
        
        $criteria->addBetweenCondition('t.tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('t.penjamin_id', $this->penjamin_id);
        $criteria->compare('LOWER(t.penjamin_nama)', strtolower($this->penjamin_nama), true);
        $criteria->compare('t.carabayar_id', $this->carabayar_id);
        $criteria->compare('LOWER(t.carabayar_nama)', strtolower($this->carabayar_nama), true);
        $criteria->compare('t.kelaspelayanan_id', $this->kelaspelayanan_id);
        $criteria->compare('LOWER(t.kelaspelayanan_nama)', strtolower($this->kelaspelayanan_nama), true);
        $criteria->compare('t.instalasi_id', $this->instalasi_id);
        $criteria->compare('LOWER(t.instalasi_nama)', strtolower($this->instalasi_nama), true);
        $criteria->compare('t.ruangan_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->compare('LOWER(t.ruangan_nama)', strtolower($this->ruangan_nama), true);
        $criteria->compare('LOWER(t.tgl_tindakan)', strtolower($this->tgl_tindakan), true);
        $criteria->compare('t.tipepaket_id', $this->tipepaket_id);
        $criteria->compare('LOWER(t.tipepaket_nama)', strtolower($this->tipepaket_nama), true);
        $criteria->compare('LOWER(t.nama_pegawai)', strtolower($this->nama_pegawai), true);
        
        return $criteria;
    }

    public function getNamaModel() {
        return __CLASS__;
    }

}