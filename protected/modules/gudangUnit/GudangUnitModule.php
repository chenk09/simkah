<?php

class GudangUnitModule extends CWebModule
{
    public $defaultController = 'pemesananBarang/index';
    public $kelompokMenu = array();
    public $menu = array();
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'gudangUnit.models.*',
			'gudangUnit.components.*',
		));
        if(!empty($_REQUEST['modulId']))
        Yii::app()->session['modulId'] = $_REQUEST['modulId'];
        Yii::app()->session['app_key'] = '25215';
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
            $this->kelompokMenu = KelompokmenuK::model()->findAllAktif();
            $this->menu = MenumodulK::model()->findAllAktif(array(
                'modulk.modul_id'=>Yii::app()->session['modulId']
            ));
            return true;
		}else return false;
	}
}
