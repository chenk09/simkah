<?php

class ApprovedPemesananController extends SBaseController
{
	public function actionVerifikasi()
	{
        $pesan_barang = new GBPesanbarangT;
        $pesan_barang->tglpesanbarang = date('d M Y H:i:s');
        $pesan_barang->tglmintadikirim = date('d M Y H:i:s');
		$instalasi_id = Yii::app()->user->getState('instalasi_id');
        $pesan_barang->nopemesanan = KeyGenerator::noPemesananBarang($instalasi_id);
        $pesan_barang_det = new GBPesanbarangdetailT;
		
		if(isset($_POST['GBPesanbarangT'])){
			$transaction  = Yii::app()->db->beginTransaction();
			$result['status'] = 'OK';
			$result['pesan'] = 'Simpan berhasil';
			try{
				$pesan_barang->attributes = $_POST['GBPesanbarangT'];
				if($pesan_barang->status_pemesanan != 'D'){
					$is_cek = GBPesanbarangT::model()->findByAttributes(array(
						'ruanganpemesan_id'=>$pesan_barang->ruanganpemesan_id,
						'status_pemesanan'=>'P'
					));
					if($is_cek){
						throw new Exception('Pengajuan telah terdaftar dan sedang diproses. silahkan simpan sebagai Draft terlebih dahulu', 500);
					}
				}
				$pesan_barang->pegpemesan_id = Yii::app()->user->id;
				if(!$pesan_barang->save()){
					$err_pesan = array();
					foreach($pesan_barang->getErrors() as $key=>$val){
						$err_pesan[$key] = implode($val, ',');
					}
					throw new Exception(implode($err_pesan, '<br>'), 500);
				}
				
				$items = array();
				foreach($_POST['GBPesanbarangdetailT'] as $key => $value){
					$_barang_det = new GBPesanbarangdetailT;
					$_barang_det->attributes = $value;
					$_barang_det->pesanbarang_id = $pesan_barang->pesanbarang_id;
					$items[] = $value;
					if(!$_barang_det->save()){
						$err_pesan = array();
						foreach($_barang_det->getErrors() as $a=>$b){
							$err_pesan[$a] = implode($b, ',');
						}
						throw new Exception(implode($err_pesan, '<br>'), 500);
					}
				};
				
				$attributes = $pesan_barang->attributes;
				$attributes['items'] = $items;
				$inbox = new TaskInbox;
				$_inbox = $inbox->create(array(
					'id_transaksi'=>1,
					'judul_request'=>'Pemesanan barang oleh user 12345 dengan banyak barang 10. No. Pemesanan = 00004/JUL/2017/PMSNBRG/RSUJK',
					'response'=>json_encode($attributes)
				),Yii::app()->user->id);
				if($_inbox['code'] != 200){
					throw new Exception($_inbox['message'], 500);
				}else $transaction->commit();
				
			} catch (Exception $e){
				$result['pesan'] = 'Simpan gagal';
				$result['message'] = $e->getMessage();
				$result['status'] = 'NOT';
				$transaction->rollback();
			}
			echo json_encode($result);
			Yii::app()->end();
		}
		$this->render('index', array(
            'pesan_barang'=>$pesan_barang,
            'pesan_barang_det'=>$pesan_barang_det
        ));
	}
	
}