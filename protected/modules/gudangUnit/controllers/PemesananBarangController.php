<?php

class PemesananBarangController extends SBaseController
{
	public function actionTambahPemesanan()
	{
        $pesan_barang = new GBPesanbarangT;
        $pesan_barang->tglpesanbarang = date('d M Y H:i:s');
        $pesan_barang->tglmintadikirim = date('d M Y H:i:s');
		$instalasi_id = Yii::app()->user->getState('instalasi_id');
		$ruangan_id = Yii::app()->user->getState('ruangan_id');

        $pesan_barang->nopemesanan = KeyGenerator::noPemesananBarang($instalasi_id);
        $pesan_barang->ruanganpemesan_id = $ruangan_id;

        $pesan_barang_det = new GBPesanbarangdetailT;
		if(isset($_POST['GBPesanbarangT'])){
			$transaction  = Yii::app()->db->beginTransaction();
			$result['status'] = 'OK';
			$result['pesan'] = 'Simpan berhasil';
			try{
				$pesan_barang->attributes = $_POST['GBPesanbarangT'];
				if($pesan_barang->status_pemesanan != 'D'){
					$is_cek = GBPesanbarangT::model()->findByAttributes(array(
						'ruanganpemesan_id'=>$pesan_barang->ruanganpemesan_id,
						'status_pemesanan'=>'P'
					));
					if($is_cek){
						throw new Exception('Pengajuan telah terdaftar dan sedang diproses. silahkan simpan sebagai Draft terlebih dahulu', 500);
					}
				}
				$pesan_barang->pegpemesan_id = Yii::app()->user->id;
				if(!$pesan_barang->save()){
					$err_pesan = array();
					foreach($pesan_barang->getErrors() as $key=>$val){
						$err_pesan[$key] = implode($val, ',');
					}
					throw new Exception(implode($err_pesan, '<br>'), 500);
				}

				$items = array();
				foreach($_POST['GBPesanbarangdetailT'] as $key => $value){
					$_barang_det = new GBPesanbarangdetailT;
					$_barang_det->attributes = $value;
					$_barang_det->pesanbarang_id = $pesan_barang->pesanbarang_id;
					$items[] = $value;
					if(!$_barang_det->save()){
						$err_pesan = array();
						foreach($_barang_det->getErrors() as $a=>$b){
							$err_pesan[$a] = implode($b, ',');
						}
						throw new Exception(implode($err_pesan, '<br>'), 500);
					}
				};
				$approvals = Logic::setApprovals(array(
					'app_id' => Yii::app()->session['app_key'],
					'id_transaksi' => $pesan_barang->pesanbarang_id
				));
				$transaction->commit();
			} catch (Exception $e){
				$result['pesan'] = 'Simpan gagal';
				$result['message'] = $e->getMessage();
				$result['status'] = 'NOT';
				$transaction->rollback();
			}
			echo json_encode($result);
			Yii::app()->end();
		}
		$this->render('index', array(
            'pesan_barang'=>$pesan_barang,
            'pesan_barang_det'=>$pesan_barang_det
        ));
	}

	public function actionIndex()
	{
		$pesan_barang = new GBPesanbarangT;
		$pesan_barang->create_loginpemakai_id = Yii::app()->user->id;
		$this->render('informasi', array(
            'pesan_barang'=>$pesan_barang
        ));
	}

	public function actionDetailBarang()
	{
		$this->layout = false;
		$id = Yii::app()->request->getParam("id");
		$pesan_barang_det = GBPesanbarangdetailT::model()->findAllByAttributes(array(
			'pesanbarang_id'=>$id
		));

		$this->renderPartial('detail_barang', array(
            'pesan_barang_det'=>$pesan_barang_det
        ));
	}

	public function actionEditPemesanan($id)
	{
		$pesan_barang = GBPesanbarangT::model()->findByPk($id);
		$pesan_barang_det = new GBPesanbarangdetailT;
		$list_barang = GBPesanbarangdetailT::model()->findAllByAttributes(array(
			'pesanbarang_id'=>$id
		));

		if(isset($_POST['GBPesanbarangT'])){
			$transaction  = Yii::app()->db->beginTransaction();
			$result['status'] = 'OK';
			$result['pesan'] = 'Simpan berhasil';
			try{
				$pesan_barang->attributes = $_POST['GBPesanbarangT'];
				if($pesan_barang->status_pemesanan != 'D'){
					$is_cek = GBPesanbarangT::model()->findByAttributes(array(
						'ruanganpemesan_id'=>$pesan_barang->ruanganpemesan_id,
						'status_pemesanan'=>'P'
					));
					if($is_cek){
						throw new Exception('Pengajuan telah terdaftar dan sedang diproses. silahkan simpan sebagai Draft terlebih dahulu', 500);
					}
				}
				$pesan_barang->pegpemesan_id = Yii::app()->user->id;
				if(!$pesan_barang->save()){
					$err_pesan = array();
					foreach($pesan_barang->getErrors() as $key=>$val){
						$err_pesan[$key] = implode($val, ',');
					}
					throw new Exception(implode($err_pesan, '<br>'), 500);
				}
				$delete = GBPesanbarangdetailT::model()->deleteAll('pesanbarang_id = :pesanbarang_id',array(
					':pesanbarang_id'=>$pesan_barang->pesanbarang_id
				));
				$items = array();
				foreach($_POST['GBPesanbarangdetailT'] as $key => $value){
					$_barang_det = new GBPesanbarangdetailT;
					$_barang_det->attributes = $value;
					$_barang_det->pesanbarang_id = $pesan_barang->pesanbarang_id;
					$items[] = $value;
					if(!$_barang_det->save()){
						$err_pesan = array();
						foreach($_barang_det->getErrors() as $a=>$b){
							$err_pesan[$a] = implode($b, ',');
						}
						throw new Exception(implode($err_pesan, '<br>'), 500);
					}
				};
				$transaction->commit();
			} catch (Exception $e){
				$result['pesan'] = 'Simpan gagal';
				$result['message'] = $e->getMessage();
				$result['status'] = 'NOT';
				$transaction->rollback();
			}
			echo json_encode($result);
			Yii::app()->end();
		}

		$this->render('edit_pemesanan', array(
            'pesan_barang'=>$pesan_barang,
			'pesan_barang_det'=>$pesan_barang_det,
			'list_barang'=>$list_barang
        ));
	}

	public function actionVerifikasiPemesanan($id){

	}
	public function actionApprovedPemesanan($id){
		$trxApproval = TrxApproval::model()->findByAttributes(array(
			'id_transaksi'=>$id,
			'id_pegawai'=>Yii::app()->user->id,
			'app_id'=>Yii::app()->session['app_key']
		));
		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->isPostRequest){
			$transaction  = Yii::app()->db->beginTransaction();
			$result['status'] = 'OK';
			$result['pesan'] = 'Simpan berhasil';
			try{
				$trxApproval->attributes = $_POST['TrxApproval'];
				if(!$trxApproval->validate()){
					$err_pesan = array();
					foreach($idem->getErrors() as $a=>$b){
						$err_pesan[$a] = implode($b, ',');
					}
					throw new Exception(implode($err_pesan, '<br>'), 500);
				}
				$_POST['TrxApproval']['updated'] = Yii::app()->user->id;
				$_POST['TrxApproval']['update_time'] = new CDbExpression('NOW()');
				$_trxApproval = TrxApproval::model()->updateByPk($trxApproval->id_approval, $_POST['TrxApproval']);
				Logic::setApprovals(array(
					'app_id' => Yii::app()->session['app_key'],
					'id_transaksi' => $id
				));
				$jumApproval = TrxApproval::model()->countByAttributes(array(
					'id_transaksi'=>$id,
					'status'=>'O'
				));
				if($jumApproval == 0){
					$pesanbarangT = GBPesanbarangT::model()->updateByPk($id, array(
						'status_pemesanan'=>'V'
					));
				}
				$transaction->commit();
			} catch (Exception $e){
				$result['pesan'] = 'Simpan gagal';
				$result['message'] = $e->getMessage();
				$result['status'] = 'NOT';
				$transaction->rollback();
			}
			echo json_encode($result);
			Yii::app()->end();
		}

        $this->renderPartial('approved_pemesanan',array(
            'trxApproval'=>$trxApproval
        ));
	}

}