<?php

class VerifikasiPemesananController extends SBaseController
{
	public function actionDaftarVerifikasi()
	{
        $pesan_barang = new GBPesanbarangT;
        $pesan_barang->status_pemesanan = 'V';
		$this->render('daftar_verifikasi', array(
            'pesan_barang'=>$pesan_barang
        ));
	}
	
	public function actionVerifikasiBarang()
	{
		$id = Yii::app()->request->getParam("id");
        $pesan_barang = GBPesanbarangT::model()->findByPk($id);
		$pesan_barang_det = GBPesanbarangdetailT::model()->findAllByAttributes(array(
			'pesanbarang_id'=>$id
		));
		$this->render('verifikasi_barang', array(
            'pesan_barang'=>$pesan_barang,
            'pesan_barang_det'=>$pesan_barang_det
        ));
	}
}