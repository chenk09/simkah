<?php

class PermintaanPembelianController extends SBaseController
{
	public function actionTambahPermintaan()
	{
		$instalasi_id = Yii::app()->user->getState('instalasi_id');
		$ruangan_id = Yii::app()->user->getState('ruangan_id');

		$format = new CustomFormat();
		$id = Yii::app()->request->getParam("pesanbarang_id");
        $permintaanpembelianT = new GBPermintaanBeliBarangT;
		$permintaanpembelianT->no_permintaan_beli = KeyGenerator::noPermintaan();
		$permintaanpembelianT->tanggal_pengajuan = $format->formatDateTimeMediumForDB(date('Y-m-d H:i:s'));

		$permintaanbarangdetail = new GBPermintaanBeliBarangDetT;
		$barang_det = array();
		if(isset($id) && strlen($id) > 0){
			$pesanbarangdetailT = GBPesanbarangdetailT::model()->findAllByAttributes(array(
				'pesanbarang_id'=>$id
			));
			foreach($pesanbarangdetailT as $key => $value){
				$barang_det[] = $value;
			}
		}
		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->isPostRequest && isset($_POST['GBPermintaanBeliBarangT'])){
			$transaction  = Yii::app()->db->beginTransaction();
			$result['status'] = 'OK';
			$result['pesan'] = 'Simpan berhasil';
			try{
				$permintaanpembelianT->attributes = $_POST['GBPermintaanBeliBarangT'];
				$permintaanpembelianT->ruangan_id = $ruangan_id;
				$permintaanpembelianT->created = Yii::app()->user->id;
				$permintaanpembelianT->instalasi_id = $instalasi_id;
				if(!$permintaanpembelianT->save()){
					$err_pesan = array();
					foreach($permintaanpembelianT->getErrors() as $key=>$val){
						$err_pesan[$key] = implode($val, ',');
					}
					throw new Exception(implode($err_pesan, '<br>'), 500);
				}
				if(count($_POST['GBPermintaanBeliBarangDetT']) > 0){
					foreach ($_POST['GBPermintaanBeliBarangDetT'] as $a => $b){
						$_permintaanbarangdetail = new GBPermintaanBeliBarangDetT;
						$_permintaanbarangdetail->attributes = $b;
						$_permintaanbarangdetail->permintaanbelibarang_id = $permintaanpembelianT->permintaanbelibarang_id;
						if(!$_permintaanbarangdetail->save()){
							$err_pesan = array();
							foreach($_permintaanbarangdetail->getErrors() as $key=>$val){
								$err_pesan[$key] = implode($val, ',');
							}
							throw new Exception(implode($err_pesan, '<br>'), 500);
						}
					}
				}else{
					throw new Exception('Input data barang', 500);
				}
				$transaction->commit();
			} catch (Exception $e){
				$result['pesan'] = 'Simpan gagal';
				$result['message'] = $e->getMessage();
				$result['status'] = 'NOT';
				$transaction->rollback();
			}
			echo json_encode($result);
			Yii::app()->end();
		}
		$this->render('tambah_permintaan', array(
            'permintaanpembelianT'=>$permintaanpembelianT,
            'permintaanbarangdetail'=>$permintaanbarangdetail,
            'barang_det'=>$barang_det
        ));
	}

	public function actionInformasiPermintaan()
	{
		$instalasi_id = Yii::app()->user->getState('instalasi_id');
		$ruangan_id = Yii::app()->user->getState('ruangan_id');
		$permintaanpembelian = new GBPermintaanBeliBarangT;
		$this->render('informasi_permintaan', array(
			'permintaanpembelian'=>$permintaanpembelian
		));
	}

	public function actionDetailPermintaan()
	{
		$this->layout = false;
		$id = Yii::app()->request->getParam("id");
		$minta_barang_det = GBPermintaanBeliBarangDetT::model()->findAllByAttributes(array(
			'permintaanbelibarang_id'=>$id
		));
		$this->renderPartial('detail_permintaan', array(
            'minta_barang_det'=>$minta_barang_det
        ));
	}

}