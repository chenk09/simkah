<?php

class PembelianBarangController extends SBaseController
{
	public function actionTambahPembelian()
	{
		$instalasi_id = Yii::app()->user->getState('instalasi_id');
		$modLogin = LoginpemakaiK::model()->findByAttributes(array('loginpemakai_id' => Yii::app()->user->id));
		$id = Yii::app()->request->getParam("id");

		$model = new GBPembelianbarangT;
		$model->tglpembelian = date('Y-m-d H:i:s');
		$model->nopembelian = KeyGenerator::noPembelianBarang($instalasi_id);
		$model->peg_pemesanan_id = $modLogin->pegawai_id;
		$model->peg_pemesan_nama = $modLogin->pegawai->nama_pegawai;
		$detail = $model->getPermintaanBarangBeli(array(
			'id'=>$id
		));
		$belibrgdetail = new GBBelibrgdetailT;
		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->isPostRequest && isset($_POST['GBPembelianbarangT'])){
			$transaction  = Yii::app()->db->beginTransaction();
			$result['status'] = 'OK';
			$result['pesan'] = 'Simpan berhasil';
			try{
				$model->attributes = $_POST['GBPembelianbarangT'];
				if(!$model->save()){
					$err_pesan = array();
					foreach($model->getErrors() as $key=>$val){
						$err_pesan[$key] = implode($val, ',');
					}
					throw new Exception(implode($err_pesan, "\n"), 500);
				}
				if(count($_POST['GBBelibrgdetailT']) > 0){
					foreach ($_POST['GBBelibrgdetailT'] as $a => $b){
						$_belibrgdetail = new GBBelibrgdetailT;
						$_belibrgdetail->attributes = $b;
						$_belibrgdetail->pembelianbarang_id = $model->pembelianbarang_id;
						if(!$_belibrgdetail->save()){
							$err_pesan = array();
							foreach($_belibrgdetail->getErrors() as $key=>$val){
								$err_pesan[$key] = implode($val, ',');
							}
							throw new Exception(implode($err_pesan, "\n"), 500);
						}
					}
				}else{
					throw new Exception('Input data barang', 500);
				}
				GBPermintaanBeliBarangT::model()->updateByPk($id, array(
					'status_permintaan'=>'P',
					'update_time'=>new CDbExpression('NOW()'),
					'updated'=>Yii::app()->user->id
				));
				$transaction->commit();
			}catch(Exception $e){
				$result['pesan'] = 'Simpan gagal';
				$result['message'] = $e->getMessage();
				$result['status'] = 'NOT';
				$transaction->rollback();
			}
			echo json_encode($result);
			Yii::app()->end();
		}
		$this->render('tambah_pembelian', array(
			'model'=>$model,
			'detail'=>$detail,
			'belibrgdetail'=>$belibrgdetail
		));
	}

	public function actionDaftarPembelian()
	{
		$model = new GBPembelianbarangT;
		$this->render('daftar_pembelian', array(
			'model'=>$model
		));
	}

	public function actionDetailBarang()
	{
		$this->layout = false;
		$id = Yii::app()->request->getParam("id");
		$barang_det = GBBelibrgdetailT::model()->findAllByAttributes(array(
			'pembelianbarang_id'=>$id
		));
		$this->renderPartial('detail_barang', array(
            'barang_det'=>$barang_det
        ));
	}

}