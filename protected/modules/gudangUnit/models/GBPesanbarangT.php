<?php
class GBPesanbarangT extends PesanbarangT{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchByVerifikasi()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('status_pemesanan', 'V');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function searchByRole()
	{
        $role = AssignmentsK::model()->findAllByAttributes(array(
            'userid'=>Yii::app()->user->id
        ));
        $roles = array();
        foreach ($role as $key => $value){
            $roles[] = $value->itemname;
        }

		$criteria = new CDbCriteria;
        $criteria->join = "INNER JOIN " . TrxApproval::model()->tableName() . " app ON app.id_transaksi = t.pesanbarang_id AND id_approval = (
            SELECT MIN(id_approval) AS id_approval FROM ". TrxApproval::model()->tableName() ." app_child
            WHERE t.pesanbarang_id = app_child.id_transaksi AND app_child.status = 'O'
        )";
        $criteria->addCondition('
            app.app_id = :app_id AND
            app.status = :status AND
            app.id_pegawai = :id_pegawai AND
            status_pemesanan = :status_pemesanan OR create_loginpemakai_id = :id_pemohon
        ');
        $criteria->params[':app_id'] = Yii::app()->session['app_key'];
        $criteria->params[':status'] = 'O';
        $criteria->params[':id_pegawai'] = Yii::app()->user->id;
        $criteria->params[':id_pemohon'] = Yii::app()->user->id;
        $criteria->params[':status_pemesanan'] = 'P';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getNamaModel(){
        return __CLASS__;
    }

    public function getTombolStatus(){
        $criteria = new CDbCriteria;
        $criteria->addCondition("id_approval = (
            SELECT MIN(app_child.id_approval) AS id_approval FROM ". TrxApproval::model()->tableName() ." app_child
            WHERE t.id_transaksi = app_child.id_transaksi AND app_child.status = 'O'
        )");
        $criteria->compare('id_transaksi',$this->pesanbarang_id);
        $criteria->compare('app_id',Yii::app()->session['app_key']);
        $app = TrxApproval::model()->find($criteria);
        $label = '<label class="label label-info">Sedang diproses > '. $app->approver .'</label>';
        if($this->status_pemesanan == 'V'){
            $label = '<label class="label label-warning">Butuh Verifikasi Data</label>';
        }else{
            if(!empty($app) && $app->id_pegawai == Yii::app()->user->id){
                $label = '<label class="label label-warning">Butuh Persetujuan Anda</label>';
            }
            if($this->status_pemesanan == 'F'){
                $label = '<label class="label label-success">Selesai</label>';
            }
        }
        return $label;
    }

    public function getTombolTrans(){
        $tombol = CHtml::link('<i class="fa fa-file"></i>', 'javascript:void(0)', array(
            'class'=>'btn btn-info mdl-info',
            'onclick'=>'tampilModal(this)',
            'uri'=>Yii::app()->createUrl('gudangUnit/pemesananBarang/detailBarang', array('id'=>$this->pesanbarang_id)),
            'title'=>'Detail Barang'
        ));

        if($this->status_pemesanan == 'P'){
            $app = TrxApproval::model()->findByAttributes(array(
                'id_transaksi'=>$this->pesanbarang_id,
                'status'=>'O',
                'id_pegawai'=>Yii::app()->user->id
            ));
            if($app){
                $tombol .= CHtml::link('<i class="fa fa-check"></i>', 'javascript:void(0)', array(
                    'class'=>'btn btn-success mdl-info',
                    'onclick'=>'tampilModal(this)',
                    'uri'=>Yii::app()->createUrl('gudangUnit/pemesananBarang/approvedPemesanan', array('id'=>$this->pesanbarang_id)),
                    'title'=>'Approve Pemesanan'
                ));
            }
        }
        if($this->status_pemesanan == 'V'){
            $tombol .= CHtml::link('<i class="fa fa-table"></i>', array('/gudangUnit/verifikasiPemesanan/verifikasiBarang', 'id'=>$this->pesanbarang_id), array(
                'class'=>'btn btn-success mdl-info',
                'title'=>'Verifikasi Pemesanan'
            ));
        }
        /*
        $map = MapApproval::model()->findByAttributes(array(
            'app_id'=>Yii::app()->session['app_key'],
            'idx'=>$app->idx
        ));
        if($map->approver == 'OperatorGudangPusat'){
            $tombol .= CHtml::link('<i class="fa fa-check"></i>', 'javascript:void(0)', array(
                'class'=>'btn btn-success mdl-info',
                'onclick'=>'tampilModal(this)',
                'uri'=>Yii::app()->createUrl('gudangUnit/pemesananBarang/verifikasiPemesanan', array('id'=>$this->pesanbarang_id)),
                'title'=>'Verifikasi Pemesanan'
            ));
        }else{

        }
        */
        if($this->status_pemesanan == 'D'){
            $tombol .= CHtml::link('<i class="fa fa-pencil"></i>', array('gudangUnit/pemesananBarang/editPemesanan', 'id'=>$this->pesanbarang_id), array('class'=>'btn btn-primary'));
        }

        if($this->status_pemesanan == 'S'){
            $tombol .= CHtml::link('<i class="fa fa-print"></i>', '#', array('class'=>'btn btn-success'));
        }
        return $tombol;
    }

}