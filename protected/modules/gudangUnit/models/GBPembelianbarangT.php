<?php
class GBPembelianbarangT extends PembelianbarangT{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchByStatus()
	{
		$criteria = new CDbCriteria;
        $criteria->addCondition('terimapersediaan_id IS NULL');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getPermintaanBarangBeli($params){
        $minta_barang_det = GBPermintaanBeliBarangDetT::model()->findAllByAttributes(array(
			'permintaanbelibarang_id'=>$params['id']
		));
        $hasil = array();
        foreach($minta_barang_det as $key => $value){
            $hasil[] = $value;
        }
        return $hasil;
    }

    public function getTombolTrans(){
        $tombol = CHtml::link('<i class="fa fa-file"></i>', 'javascript:void(0)', array(
            'class'=>'btn btn-info mdl-info',
            'onclick'=>'tampilModal(this)',
            'uri'=>Yii::app()->createUrl('gudangUnit/pembelianBarang/detailBarang', array('id'=>$this->pembelianbarang_id)),
            'title'=>'Detail Barang'
        ));
        return $tombol;
    }

}