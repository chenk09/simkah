<?php
class GBPermintaanBeliBarangT extends PermintaanBeliBarangT{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function getTombolStatus(){
        $label = '<label class="label label-default">Perlu Pembuatan PO</label>';
        if($this->status_permintaan == 'P'){
            $label = '<label class="label label-info">Proses Pembelian</label>';
        }
        return $label;
    }
    public function getTombolTrans(){
        $tombol = CHtml::link('<i class="fa fa-file"></i>', 'javascript:void(0)', array(
            'class'=>'btn btn-info mdl-info',
            'onclick'=>'tampilModal(this)',
            'uri'=>Yii::app()->createUrl('gudangUnit/permintaanPembelian/detailPermintaan', array('id'=>$this->permintaanbelibarang_id)),
            'title'=>'Detail Barang'
        ));
        $role = AssignmentsK::model()->findAllByAttributes(array(
            'userid'=>Yii::app()->user->id
        ));
        $roles = array();
        foreach ($role as $key => $value){
            $roles[] = $value->itemname;
        }
        if(in_array('ManagerLogistik', $roles) && $this->status_permintaan == 'O'){
            $tombol .= CHtml::link('<i class="fa fa-file-o"></i>', array('/gudangUnit/pembelianBarang/tambahPembelian', 'id'=>$this->permintaanbelibarang_id), array(
                'class'=>'btn btn-warning mdl-info',
                'title'=>'Pembuatan PO'
            ));
        }
        return $tombol;
    }
}