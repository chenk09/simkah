<?php
class GBPermintaanpembelianT extends PermintaanpembelianT{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('ruangan_id', $this->ruangan_id);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    public function getTombolTrans(){
        $tombol = CHtml::link('<i class="fa fa-file"></i>', 'javascript:void(0)', array(
            'class'=>'btn btn-info mdl-info',
            'onclick'=>'tampilModal(this)',
            'uri'=>Yii::app()->createUrl('gudangUnit/permintaanPembelian/detailPermintaan', array('id'=>$this->permintaanpembelian_id)),
            'title'=>'Detail Barang'
        ));
        return $tombol;
    }
    
}