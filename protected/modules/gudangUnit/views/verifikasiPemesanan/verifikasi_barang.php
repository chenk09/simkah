<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/font-awesome/css/font-awesome.min.css'); ?>
<fieldset>
    <legend class="rim2">Informasi Pemesanan</legend>
    <div class="row-fluid">
        <div class="span6">
            <table>
                <tr>
                    <td width="100">No. Pemesanan</td>
                    <td width="10">:</td>
                    <td><?php echo $pesan_barang->nopemesanan ?></td>
                </tr>
                <tr>
                    <td>Tgl. Pemesanan</td>
                    <td width="10">:</td>
                    <td><?php echo $pesan_barang->tglpesanbarang ?></td>
                </tr>
                </tr>
                <tr>
                    <td>Ruangan</td>
                    <td width="10">:</td>
                    <td><?php echo $pesan_barang->ruanganpemesan->ruangan_nama ?></td>
                </tr>
            </table>
        </div>
        <div class="span6">
            <table>
                <tr>
                    <td width="50">Keterangan</td>
                    <td width="10">:</td>
                    <td><?php echo $pesan_barang->keterangan_pesan ?></td>
                </tr>
            </table>
        </div>
    </div>
</fieldset>
<hr>
<fieldset>
    <legend class="rim">Detail Pemesanan</legend>
    <div class="row-fluid">
        <div class="span12">
            <table>
                <tr>
                    <td width="50%"><b>Detail Pememesanan</b></td>
                    <td><b>Stock Gudang</b></td>
                </tr>
                <?php foreach ($pesan_barang_det as $key => $value): ?>
                <tr>
                    <td><?php echo $value->barang->barang_type ?> / <?php echo $value->barang->barang_nama ?> - <?php echo $value->qty_pesan ?> <?php echo $value->satuanbarang ?></td>
                    <td>
                        <?php
                            $criteria = new CDbCriteria;
                            $criteria->select = 'SUM(inventarisasi_qty_skrg) AS inventarisasi_qty_skrg';
                            $criteria->compare('barang_id', $value->barang_id);
                            $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                            $criteria->group = 'barang_id';
                            $inv = InventarisasiruanganT::model()->find($criteria);
                            $stock = '<label class="label label-warning"><i class="fa fa-remove"></i> Tidak ada stock digudang, silahkan melakukan > Permintaan Pembelian</label>';
                            if($inv){
                                $stock = $inv->inventarisasi_qty_skrg;
                            }
                            echo $stock;
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</fieldset>
<div class="form-group">
    <?php echo CHtml::link('<i class="fa fa-home"></i> Kembali', array('/gudangUnit/verifikasiPemesanan/daftarVerifikasi'), array(
        'class'=>'btn btn-error btn-sm'
    ))?>
    <?php echo CHtml::tag('button', array(
        'name'=>'verifikasi',
        'class'=>'btn btn-info btn-sm kirim'
    ), '<i class="fa fa-check"></i> Verifikasi & Mutasi')?>
    <?php echo CHtml::link('<i class="fa fa-send"></i> Permintaan Pembelian', array('/gudangUnit/permintaanPembelian/tambahPermintaan','pesanbarang_id'=>$pesan_barang->pesanbarang_id), array(
        'name'=>'submit',
        'class'=>'btn btn-success btn-sm'
    ))?>
</div>
