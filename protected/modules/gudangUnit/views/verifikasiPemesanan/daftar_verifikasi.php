<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/font-awesome/css/font-awesome.min.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-modal.js'); ?>

<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'pesanbarang-form',
    'enableAjaxValidation'=>false,
        'type'=>'vertical',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)')
)); ?>
<fieldset>
    <legend class="rim2">Verifikasi Pemesanan Barang</legend>
    <table width="100%" cellpadding="0" cellspacing="0" class="x-table">
        <tr>
            <td width="50%">
                <table width="100%" cellpadding="0" cellspacing="0" class="x-table">
                    <tr>
                        <td width="120"><?php echo $form->labelEx($pesan_barang, 'nopemesanan', array('class'=>'control-label')) ?></td>
                        <td>
                            <?php echo $form->textField($pesan_barang, 'nopemesanan', array(
                                'onkeypress' => "return $(this).focusNextInputField(event);",
                                'maxlength' => 20,
                                'class' => 'form-control'
                            )); ?>
                            <?php echo $form->error($pesan_barang, 'nopemesanan'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="120"><?php echo $form->labelEx($pesan_barang, 'ruanganpemesan_id', array('class' => 'control-label')); ?></td>
                        <td>
                            <?php echo $form->dropDownList($pesan_barang, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'),
                                array(
                                    'empty' => '-- Pilih --',
                                    'class' => 'span2',
                                    'onkeypress' => "return $(this).focusNextInputField(event);",
                                    'ajax' => array(
                                        'type' => 'POST',
                                        'url' => Yii::app()->createUrl('ActionDynamic/ruanganDariInstalasi', array(
                                            'encode' => false,
                                            'namaModel' => '' . $pesan_barang->getNamaModel() . ''
                                        )),
                                        'update' => '#' . CHtml::activeId($pesan_barang, 'ruanganpemesan_id') . ''
                                    )
                                )
                            );?>
                            <?php echo $form->dropDownList($pesan_barang, 'ruanganpemesan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'),
                                array(
                                    'empty' => '-- Pilih --',
                                    'class' => 'span2',
                                    'onkeypress' => "return $(this).focusNextInputField(event);",
                                    'onchange' => 'clearAll()'
                                )
                            ); ?>
                            <?php echo $form->error($pesan_barang, 'ruanganpemesan_id'); ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <?php echo CHtml::tag('button', array(
                        'name'=>'draft',
                        'class'=>'btn btn-info btn-sm'
                    ), '<i class="fa fa-check"></i> Cari')?>
                    <?php echo CHtml::tag('button', array(
                        'name'=>'submit',
                        'class'=>'btn btn-success btn-sm'
                    ), '<i class="fa fa-check"></i> Reset')?>
                </div>
            </td>
        </tr>
    </table>
</fieldset>
<?php $this->endWidget(); ?>

<fieldset>
    <legend class="rim">Detail Barang</legend>
    <?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    	'id'=>'sainstalasi-m-grid',
    	'dataProvider'=>$pesan_barang->searchByVerifikasi(),
        'template'=>"{summary}\n{items}{pager}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
            array(
                'type' => 'raw',
                'header' => 'No',
                'htmlOptions' => array(
                    'style' => 'text-align:left',
                ),
                'headerHtmlOptions' => array(
                    'style' => 'text-align:left'
                ),
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            ),
            'nopemesanan',
            'keterangan_pesan',
            'ruanganpemesan.ruangan_nama',
            'tglpesanbarang',
            'pegawaipemesan.nama_pegawai',
            array(
                'value'=>'$data->tombolStatus',
                'type'=>'raw',
                'htmlOptions' => array(
                    'style' => 'text-align:center;white-space:nowrap',
                ),
                'headerHtmlOptions' => array(
                    'style' => 'width: 50px;'
                )
            ),
            array(
                'value'=>'$data->tombolTrans',
                'type'=>'raw',
                'htmlOptions' => array(
                    'style' => 'text-align:center;white-space:nowrap',
                ),
                'headerHtmlOptions' => array(
                    'style' => 'width: 50px;'
                )
            )
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); ?>
</fieldset>

<div id="mdlDetInfo" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop='static'>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modal header</h3>
    </div>
    <div class="modal-body">
        <div style="width:850px">
            <p >One fine body…</p>
        </div>
    </div>
</div>
<script type="text/javascript">
function tampilModal(obj){
    var title = $(obj).attr('title');
    var uri = $(obj).attr('uri');
    $('#myModalLabel').text(title);
    $('#mdlDetInfo').find('.modal-body > div').load(uri, function( response, status, xhr ) {
        if (status == "error"){
            var msg = "Sorry but there was an error: ";
            $('#mdlDetInfo').find('.modal-body > div').html( msg + xhr.status + " " + xhr.statusText);
        }
    });
    $('#mdlDetInfo').modal('show');
};
</script>
