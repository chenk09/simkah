<tr class="hasil">
    <td>
        <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model' => $pesan_barang_det,
            'attribute' => '[0]nama_barang',
            'source' => 'js: function(request, response){
                $.ajax({
                    url: "' . Yii::app()->createUrl('actionAutoComplete/barang') . '",
                    dataType: "json",
                    data: {
                        term: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
            'options' => array(
                'showAnim' => 'fold',
                'minLength' => 2,
                'focus' => 'js:function( event, ui ) {
                    $(this).val( ui.item.label);
                    return false;
                }',
                'select' => 'js:function( event, ui ){
                    $("#idBarang").val(ui.item.barang_id);
                    return false;
                }',
            ),
            'htmlOptions' => array(
                'onkeypress' => "return $(this).focusNextInputField(event)",
                'class' => 'form-control typeahead inputan',
            )
        ));?>
        <?php echo $form->hiddenField($pesan_barang_det, '[0]barang_id', array(
            'class'=>'form-control span-6 inputan'
        ));?>
    </td>
    <td>
        <?php echo $form->textField($pesan_barang_det, '[0]barang_type', array(
            'class'=>'form-control span-6 inputan'
        ));?>
    </td>
    <td>
        <?php echo $form->textField($pesan_barang_det, '[0]qty_pesan', array(
            'class'=>'form-control span-2 inputan'
        ));?>
    </td>
    <td>
        <?php echo $form->dropDownList($pesan_barang_det, '[0]satuanbarang', Satuanbarang::items(), array(
            'empty' => '-- Pilih --',
            'class'=>'form-control span-2 inputan',
            'onkeypress' => "return $(this).focusNextInputField(event)"
        ));?>
    </td>
    <td style="text-align: center;">
        <?=Chtml::link('<i class="icon icon-remove"></i>', 'javascript:void(0);', array(
            'class'=>'remove',
            'onClick'=>'removeMe(this)'
        ));?>
    </td>
</tr>