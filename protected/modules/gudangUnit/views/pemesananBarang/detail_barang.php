<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="width:10px">No</th>
            <th>Nama</th>
            <th>Type</th>
            <th>Qty</th>
            <th>Satuan</th>
        </tr>
    </thead>
    <?php $idx = 1; ?>
    <?php foreach ($pesan_barang_det as $key => $value): ?>
        <tr>
            <td><?php echo $idx ?></td>
            <td><?php echo $value->barang->barang_nama ?></td>
            <td><?php echo $value->barang->barang_type ?></td>
            <td><?php echo $value->qty_pesan ?></td>
            <td><?php echo $value->satuanbarang ?></td>
        </tr>
    <?php $idx++; ?>
    <?php endforeach; ?>
</table>
