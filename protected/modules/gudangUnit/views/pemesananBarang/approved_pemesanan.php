<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'verifikasi-pemesanan-form',
    'enableAjaxValidation'=>false,
    'type'=>'vertical',
    'htmlOptions'=>array(
        'action'=>Yii::app()->createUrl('gudangUnit/pemesananBarang/verifikasiPemesanan', array('id'=>$trxApproval->id_transaksi)),
        'onKeyPress'=>'return disableKeyPress(event)',
    )
)); ?>
<div class="control-group">
    <?php echo $form->labelEx($trxApproval, 'keterangan', array('class'=>'control-label')) ?>
    <div class="controls">
        <?php echo $form->textArea($trxApproval, 'keterangan', array(
            'onkeypress' => "return $(this).focusNextInputField(event);",
            'style' => "width:100%",
        )); ?>
        <?php echo $form->error($trxApproval, 'keterangan'); ?>
    </div>
</div>
<?php echo $form->hiddenField($trxApproval, 'status'); ?>
<div class="form-group">
    <?php echo CHtml::tag('button', array(
        'name'=>'approve',
        'class'=>'btn btn-primary btn-sm kirim'
    ), '<i class="fa fa-check"></i> Approve')?>
    <?php echo CHtml::tag('button', array(
        'name'=>'reject',
        'class'=>'btn btn-danger btn-sm kirim'
    ), '<i class="fa fa-remove"></i> Reject')?>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
$('.kirim').on('click', function(){
    var btn_name = $(this).attr('name');
    var status = 'A';
    if(btn_name == 'reject'){
        status = 'R';
    }
    $('#<?=CHtml::activeId($trxApproval, 'status')?>').val(status);
    $('#verifikasi-pemesanan-form').trigger('submit');
    return false;
});
$('#verifikasi-pemesanan-form').on('submit', function(){
    var formdata = new FormData(this);
    swal.queue([{
        confirmButtonText: 'Simpan',
        showCancelButton: true,
        text:'Yakin akan menyimpan data?',
        showLoaderOnConfirm: true,
        allowOutsideClick: false,
        preConfirm: function (){
            return new Promise(function (resolve){
                $.ajax({
                    type: "POST",
                    data:formdata,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    url: $('#verifikasi-pemesanan-form').attr('action'),
                    success: function(response, statusText, xhr, $form){
                        var type_notif = 'error';
                        if(response.status == 'OK'){
                            type_notif = 'success';
                        }
                        swal({
                            title: response.pesan,
                            text: response.message,
                            type: type_notif,
                            preConfirm: function (){
                                return new Promise(function(resolve, reject){
                                    if(type_notif == 'success'){
                                        window.location.reload(false);
                                    }
                                    resolve();
                                });
                            }
                        });
                    }
                });
            })
        }
    }])
    return false;
});
</script>
