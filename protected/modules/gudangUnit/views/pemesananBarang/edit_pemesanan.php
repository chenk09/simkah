<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.js'); ?>

<style type="text/css">
.x-table{
    margin-bottom:0px;
}.x-table td {
    padding: 2px;
}
</style>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'pesanbarang-form',
    'enableAjaxValidation'=>false,
        'type'=>'vertical',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)')
)); ?>
<fieldset>
    <legend class="rim2">Pemesanan Barang</legend>
    <table width="100%" cellpadding="0" cellspacing="0" class="x-table">
        <tr>
            <td width="50%">
                <table width="100%" cellpadding="0" cellspacing="0" class="x-table">
                    <tr>
                        <td width="120"><?php echo $form->labelEx($pesan_barang, 'tglpesanbarang', array('class'=>'control-label')) ?></td>
                        <td>
                            <?php $this->widget('MyDateTimePicker', array(
                                'model' => $pesan_barang,
                                'attribute' => 'tglpesanbarang',
                                'mode' => 'datetime',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions' => array(
                                    'readonly' => true,
                                    'class' => 'dtPicker3 form-control',
                                    'onkeypress' => "return $(this).focusNextInputField(event)"
                                ),
                            ));?>
                            <?php echo $form->error($pesan_barang, 'tglpesanbarang'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="120"><?php echo $form->labelEx($pesan_barang, 'tglmintadikirim', array('class'=>'control-label')) ?></td>
                        <td>
                            <?php $this->widget('MyDateTimePicker', array(
                                'model' => $pesan_barang,
                                'attribute' => 'tglmintadikirim',
                                'mode' => 'datetime',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions' => array(
                                    'readonly' => true,
                                    'class' => 'dtPicker3 form-control',
                                    'onkeypress' => "return $(this).focusNextInputField(event)"
                                ),
                            ));?>
                            <?php echo $form->error($pesan_barang, 'tglmintadikirim'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="120"><?php echo $form->labelEx($pesan_barang, 'nopemesanan', array('class'=>'control-label')) ?></td>
                        <td>
                            <?php echo $form->textField($pesan_barang, 'nopemesanan', array(
                                'readonly'=>true,
                                'onkeypress' => "return $(this).focusNextInputField(event);",
                                'maxlength' => 20,
                                'class' => 'form-control'
                            )); ?>
                            <?php echo $form->error($pesan_barang, 'nopemesanan'); ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" class="x-table">
                    <tr>
                        <td width="120"><?php echo $form->labelEx($pesan_barang, 'ruanganpemesan_id', array('class' => 'control-label')); ?></td>
                        <td>
                            <?php echo $form->dropDownList($pesan_barang, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'),
                                array(
                                    'empty' => '-- Pilih --',
                                    'class' => 'span2',
                                    'onkeypress' => "return $(this).focusNextInputField(event);",
                                    'ajax' => array(
                                        'type' => 'POST',
                                        'url' => Yii::app()->createUrl('ActionDynamic/ruanganDariInstalasi', array(
                                            'encode' => false,
                                            'namaModel' => '' . $pesan_barang->getNamaModel() . ''
                                        )),
                                        'update' => '#' . CHtml::activeId($pesan_barang, 'ruanganpemesan_id') . ''
                                    )
                                )
                            );?>
                            <?php echo $form->dropDownList($pesan_barang, 'ruanganpemesan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'),
                                array(
                                    'empty' => '-- Pilih --',
                                    'class' => 'span2',
                                    'onkeypress' => "return $(this).focusNextInputField(event);",
                                    'onchange' => 'clearAll()'
                                )
                            ); ?>
                            <?php echo $form->error($pesan_barang, 'ruanganpemesan_id'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="120"><?php echo $form->labelEx($pesan_barang, 'keterangan_pesan', array('class'=>'control-label')) ?></td>
                        <td>
                            <?php echo $form->textArea($pesan_barang, 'keterangan_pesan', array(
                                'onkeypress' => "return $(this).focusNextInputField(event);",
                                'style' => "width:100%",
                            )); ?>
                            <?php echo $form->error($pesan_barang, 'keterangan_pesan'); ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset>
    <legend class="rim">Detail Barang</legend>
    <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i> Tambah',array(
        'class' => 'btn btn-primary addBarang',
        'type' => 'button',
        'onKeypress' => 'return false;'
    ));
    ?>
    <div class="row-fluid">
        <div class="span12">
            <table id="grid_pemesanan" class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th rowspan="2">Nama Barang</th>
                        <th rowspan="2">Type Barang</th>
                        <th colspan="2">Qty</th>
                        <th rowspan="2" style="width: 30px;">&nbsp;</th>
                    </tr>
                    <tr>
                        <th style="width: 50px;">Jml</th>
                        <th style="width: 100px;">Satuan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $id_barang = array(); ?>
                    <?php if (count($list_barang) > 0): ?>
                        <?php foreach ($list_barang as $key => $value): ?>
                            <?php $barang_det = new GBPesanbarangdetailT; ?>
                            <?php $barang_det->nama_barang = $value->barang->barang_nama; ?>
                            <?php $barang_det->barang_type = $value->barang->barang_type; ?>
                            <?php $barang_det->qty_pesan = $value->qty_pesan; ?>
                            <?php $barang_det->satuanbarang = $value->satuanbarang; ?>
                            <?php $barang_det->barang_id = $value->barang_id; ?>
                            <?php $id_barang[$value->barang_id] = 'YA'; ?>
                            <tr>
                                <td>
                                    <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                        'model' => $barang_det,
                                        'attribute' => '['. $key .']nama_barang',
                                        'source' => 'js: function(request, response){
                                            $.ajax({
                                                url: "' . Yii::app()->createUrl('actionAutoComplete/barang') . '",
                                                dataType: "json",
                                                data: {
                                                    term: request.term,
                                                },
                                                success: function (data) {
                                                    response(data);
                                                }
                                            })
                                        }',
                                        'options' => array(
                                            'showAnim' => 'fold',
                                            'minLength' => 2,
                                            'focus' => 'js:function( event, ui ) {
                                                $(this).val( ui.item.label);
                                                return false;
                                            }',
                                            'select' => 'js:function( event, ui ){
                                                $("#idBarang").val(ui.item.barang_id);
                                                return false;
                                            }',
                                        ),
                                        'htmlOptions' => array(
                                            'onkeypress' => "return $(this).focusNextInputField(event)",
                                            'class' => 'form-control typeahead inputan',
                                        )
                                    ));?>
                                    <?php echo $form->hiddenField($barang_det, '['. $key .']barang_id', array(
                                        'class'=>'form-control span-6 inputan'
                                    ));?>
                                </td>
                                <td>
                                    <?php echo $form->textField($barang_det, '['. $key .']barang_type', array(
                                        'class'=>'form-control span-6 inputan'
                                    ));?>
                                </td>
                                <td>
                                    <?php echo $form->textField($barang_det, '['. $key .']qty_pesan', array(
                                        'class'=>'form-control span-2 inputan'
                                    ));?>
                                </td>
                                <td>
                                    <?php echo $form->dropDownList($barang_det, '['. $key .']satuanbarang', Satuanbarang::items(), array(
                                        'empty' => '-- Pilih --',
                                        'class'=>'form-control span-2 inputan',
                                        'onkeypress' => "return $(this).focusNextInputField(event)"
                                    ));?>
                                </td>
                                <td style="text-align: center;">
                                    <?=Chtml::link('<i class="icon icon-remove"></i>', 'javascript:void(0);', array(
                                        'class'=>'remove',
                                        'onClick'=>'removeMe(this)'
                                    ));?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr class="rec">
                            <td colspan="6"><center>Data kosong</center></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</fieldset>
<div class="form-group">
    <?php echo $form->hiddenField($pesan_barang, 'status_pemesanan', array()); ?>
    <?php echo CHtml::link('<i class="fa fa-remove"></i> Cancel', array('/gudangUsb'), array(
        'class'=>'btn btn-error btn-sm'
    ))?>
    <?php echo CHtml::tag('button', array(
        'name'=>'draft',
        'class'=>'btn btn-info btn-sm kirim'
    ), '<i class="fa fa-check"></i> Draft')?>
    <?php echo CHtml::tag('button', array(
        'name'=>'submit',
        'class'=>'btn btn-success btn-sm kirim'
    ), '<i class="fa fa-check"></i> Submit')?>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    var temp_grid = '<?=CJSON::encode(preg_replace('/\r+/', '', preg_replace('/\n+/', '', $this->renderPartial('_form_barang', array(
        'form'=>$form,
        'pesan_barang_det'=>$pesan_barang_det
    ), true))));?>';
    
    var barang_id = '<?php echo json_encode($id_barang) ?>';
    var idx_barang = '<?php echo (count($list_barang) > 0 ? (count($list_barang) + 1) : 0) ?>';
    $('.addBarang').on('click', function(){
        var cloning = $(temp_grid).clone();
        idx_barang = idx_barang + 1;
        $('#grid_pemesanan').find('tbody .rec').remove();
        $(cloning).find('[name*="[0]"]').each(function(i){
            $(this).attr('name', $(this).attr('name').replace("[0]", '['+ idx_barang +']' ));
        });
        $('#grid_pemesanan').find('tbody').append(cloning);
        setTimeout(function(){
            $('#grid_pemesanan').find('.typeahead').autocomplete({
                'showAnim':'fold',
                'minLength':2,
                'focus':function( event, ui ) {
                    $(this).val( ui.item.label);
                    return false;
                },
                'select':function( event, ui ){
                    var id_item = ui.item.value;
                    if(barang_id[id_item] != undefined){
                        alert('Barang telah terdaftar');
                        $(this).val('');
                    }else{
                        barang_id[id_item] = 'OK';
                        var that = this;
                        $(this).parent().find('[name*="[barang_id]"]').val(ui.item.value);
                        $.ajax({
                            url: "/simrs/index.php?r=actionAjax/detailBarang",
                            dataType: "json",
                            data:{
                                map: ui.item.barang_id,
                            },
                            success: function(data){
                                if(Object.keys(data.record).length > 0){
                                    $.each(data.record, function(index, el){
                                        $(that).closest('tr').find('[name*="['+ index +']"]').val(el);
                                    });
                                }
                            }
                        })
                    }
                    
                    return false;
                },
                'source': function(request, response){
                    $.ajax({
                       url: "/simrs/index.php?r=actionAutoComplete/barang",
                       dataType: "json",
                       data: {
                           term: request.term,
                       },
                       success: function (data) {
                           response(data);
                       }
                    })
                }
            });
        }, 500);
        return false;
    })
    function removeMe(obj){
        var barang_id = $(obj).closest('tr').find('[name*="[barang_id]"]').val();
        delete(barang_id[barang_id]);
        $(obj).parents('tr').remove();
        var tr_lenght = $('#grid_pemesanan').find('tbody tr').length;
        if(tr_lenght == 0){
            var temp = '<tr><td class="rec" colspan="7"><center>Data kosong</center></td></tr>';
            $('#grid_pemesanan').find('tbody').append(temp);
        }
    }
    
    $('.kirim').on('click', function(){
        var status_proses_id = 'P';
        if($(this).attr('name') == 'draft'){
            status_proses_id = 'D';
        }
        $("#<?php echo CHtml::activeId($pesan_barang, 'status_pemesanan')?>").val(status_proses_id);
    })
    
    $('#pesanbarang-form').on('submit', function(){
        if(barang_id.length > 0){
            var that = this;
            var formdata = new FormData(this);
            swal.queue([{
                confirmButtonText: 'Simpan',
                showCancelButton: true,
                text:'Yakin akan menyimpan data?',
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: function (){
                    return new Promise(function (resolve){
                        $.ajax({
                            type: "POST",
                            data:formdata,
                            contentType: false,
                            processData: false,
                            dataType: 'json',
                            url: $('#pesanbarang-form').attr('action'),
                            success: function(response, statusText, xhr, $form){
                                var type_notif = 'error';
                                if(response.status == 'OK'){
                                    type_notif = 'success';
                                }
                                swal({
                                    title: response.pesan,
                                    text: response.message,
                                    type: type_notif,
                                    preConfirm: function (){
                                        return new Promise(function(resolve, reject){
                                            if(type_notif == 'success'){
                                                window.location.reload(false);
                                            }
                                            resolve();
                                        });
                                    }
                                });
                            }
                        });
                    })
                }
            }])
        }else{
            alert('Silahkan input daftar barang terlebih dahulu');
        }
        return false;
    });
        
</script>
