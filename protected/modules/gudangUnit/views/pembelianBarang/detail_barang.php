<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="width:10px">No</th>
            <th>Nama</th>
            <th>Type</th>
            <th width="10" style="white-space:nowrap">Qty</th>
            <th width="50" style="white-space:nowrap">Satuan</th>
            <th width="50" style="white-space:nowrap">Harga Beli</th>
        </tr>
    </thead>
    <?php $idx = 1; ?>
    <?php foreach ($barang_det as $key => $value): ?>
        <tr>
            <td><?php echo $idx ?></td>
            <td><?php echo $value->barang->barang_nama ?></td>
            <td><?php echo $value->barang->barang_type ?></td>
            <td style="text-align:right"><?php echo $value->jmlbeli ?></td>
            <td><?php echo $value->satuanbeli ?></td>
            <td style="text-align:right"><?php echo number_format($value->hargabeli) ?></td>
        </tr>
    <?php $idx++; ?>
    <?php endforeach; ?>
</table>
