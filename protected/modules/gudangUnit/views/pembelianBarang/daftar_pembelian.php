<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/font-awesome/css/font-awesome.min.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-modal.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'pembelian-form',
    'enableAjaxValidation'=>false,
        'type'=>'vertical',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)')
)); ?>
<fieldset>
    <legend class="rim2">Transaksi Pembelian Barang</legend>
</fieldset>
<?php $this->endWidget(); ?>

<fieldset>
    <legend class="rim">Detail Barang</legend>
    <?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    	'id'=>'sainstalasi-m-grid',
    	'dataProvider'=>$model->searchByStatus(),
        'template'=>"{summary}\n{items}{pager}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
            array(
                'type' => 'raw',
                'header' => 'No',
                'htmlOptions' => array(
                    'style' => 'text-align:left',
                ),
                'headerHtmlOptions' => array(
                    'style' => 'text-align:left'
                ),
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            ),
            'nopembelian',
            'sumberdana.sumberdana_nama',
            'supplier.supplier_nama',
            'tglpembelian',
            array(
                'value'=>'$data->tombolTrans',
                'type'=>'raw',
                'htmlOptions' => array(
                    'style' => 'text-align:center;white-space:nowrap',
                ),
                'headerHtmlOptions' => array(
                    'style' => 'width: 50px;'
                )
            )            
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); ?>
</fieldset>

<div id="mdlDetInfo" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop='static'>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modal header</h3>
    </div>
    <div class="modal-body">
        <div style="width:850px">
            <p >One fine body…</p>
        </div>
    </div>
</div>
<script type="text/javascript">
function tampilModal(obj){
    var title = $(obj).attr('title');
    var uri = $(obj).attr('uri');
    $('#myModalLabel').text(title);
    $('#mdlDetInfo').find('.modal-body > div').load(uri, function( response, status, xhr ) {
        if (status == "error"){
            var msg = "Sorry but there was an error: ";
            $('#mdlDetInfo').find('.modal-body > div').html( msg + xhr.status + " " + xhr.statusText);
        }
    });
    $('#mdlDetInfo').modal('show');
};
</script>
