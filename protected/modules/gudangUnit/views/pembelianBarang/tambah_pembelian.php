<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.css'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/font-awesome/css/font-awesome.min.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.js'); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugins/jquery-input-mask/inputmask.js', CClientScript::POS_BEGIN); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugins/jquery-input-mask/jquery.inputmask.js', CClientScript::POS_BEGIN); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugins/jquery-input-mask/inputmask.numeric.extensions.js', CClientScript::POS_BEGIN); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugins/jquery-input-mask/inputmask.date.extensions.js', CClientScript::POS_BEGIN); ?>

<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'pembelian-form',
		'enableAjaxValidation'=>false,
		'type'=>'horizontal',
		'htmlOptions'=>array(
			'enctype'=>"multipart/form-data",
			'onKeyPress'=>'return disableKeyPress(event)',
		)
	)
);?>
<fieldset>
    <legend class="rim2">Transaksi Pembelian Barang</legend>
    <div class="row-fluid">
        <div class="span6">
            <div class="control-group">
                <?php echo $form->labelEx($model, 'nopembelian', array(
                    'class'=>'control-label'
                ))?>
                <div class="controls">
                    <?php echo $form->textField($model,'nopembelian',array(
                        'readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)"
                    ));?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'tglpembelian', array(
                    'class'=>'control-label'
                ))?>
                <div class="controls">
					<?php $this->widget('MyDateTimePicker',array(
							'model'=>$model,
							'attribute'=>'tglpembelian',
							'options'=> array(
							'dateFormat'=>Params::DATE_TIME_FORMAT,
							'maxDate'=>'d',
						),
						'htmlOptions'=>array(
							'readonly'=>true,
							'class'=>'dtPicker3',
							'onkeypress'=>"return $(this).focusNextInputField(event)"
						),
					)); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'tgldikirim', array(
                    'class'=>'control-label'
                ))?>
                <div class="controls">
					<?php $this->widget('MyDateTimePicker',array(
							'model'=>$model,
							'attribute'=>'tgldikirim',
							'options'=> array(
							'dateFormat'=>Params::DATE_TIME_FORMAT,
							'maxDate'=>'d',
						),
						'htmlOptions'=>array(
							'readonly'=>true,
							'class'=>'dtPicker3',
							'onkeypress'=>"return $(this).focusNextInputField(event)"
						),
					)); ?>
                </div>
            </div>
        </div>
        <div class="span6">
			<div class="control-group">
                <?php echo $form->labelEx($model, 'sumberdana_id', array(
                    'class'=>'control-label'
                ))?>
                <div class="controls">
					<?php echo $form->dropDownList($model,'sumberdana_id',
						CHtml::listData(SumberdanaM::model()->findAll('sumberdana_aktif = true'), 'sumberdana_id', 'sumberdana_nama'),
						array(
							'empty'=>'-- Pilih --',
							'class'=>'span3',
							'onkeypress'=>"return $(this).focusNextInputField(event);"
					));?>
                </div>
            </div>
			<div class="control-group">
                <?php echo $form->labelEx($model, 'supplier_id', array(
                    'class'=>'control-label'
                ))?>
                <div class="controls">
					<?php echo $form->dropDownList($model,'supplier_id',
						CHtml::listData(SupplierM::model()->findAll("supplier_aktif = true and supplier_jenis ilike '%Gudang Umum%'"), 'supplier_id', 'supplier_nama'),
						array(
							'empty'=>'-- Pilih --',
							'class'=>'span3',
							'onkeypress'=>"return $(this).focusNextInputField(event);"
					));?>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend class="rim">Detail Pembelian</legend>
	<?php if (!isset($_GET['id'])): ?>
		<?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i> Tambah',array(
			'class' => 'btn btn-primary addBarang',
			'type' => 'button',
			'onKeypress' => 'return false;'
		));?>
		<?php echo CHtml::htmlButton('<i class="icon-download icon-white"></i> Import Dari Permintaan',array(
			'class' => 'btn btn-info',
			'type' => 'button',
			'onKeypress' => 'return false;'
		));?>
	<?php endif; ?>
	<div class="row-fluid">
        <div class="span12">
            <table id="grid_pembelian" class="items table table-bordered">
                <thead>
                    <tr>
                        <th>Nama Barang</th>
                        <th>Kelompok</th>
                        <th width="50" style="white-space:nowrap">Jumlah Beli</th>
                        <th width="50" style="white-space:nowrap">Satuan</th>
						<th width="50" style="white-space:nowrap">Isi</th>
						<th width="50" style="white-space:nowrap">Harga Beli</th>
						<th width="50" style="white-space:nowrap">Harga Satuan</th>
						<th width="50" style="white-space:nowrap">PPN</th>
						<th width="50" style="white-space:nowrap">Discount(%)</th>
                        <th width="20">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
					<?php if (count($detail) > 0): ?>
						<?php foreach($detail as $key => $value): ?>
							<tr>
								<td>
									<?php $belibrgdetail->barang_id = $value->barang_id; ?>
									<?php $belibrgdetail->satuanbeli = $value->satuanbarang; ?>
									<?php $belibrgdetail->jmlbeli = $value->jml_permintaan; ?>

									<?php echo $form->hiddenField($belibrgdetail,'['. $key .']barang_id'); ?>
									<?php echo $value->barang->barang_nama?>
								</td>
								<td><?php echo $value->barang->barang_type?></td>
								<td><?php echo $form->textField($belibrgdetail,'['. $key .']jmlbeli', array('class'=>'input-mini decimal')); ?></td>
								<td>
									<?php echo $form->dropDownList($belibrgdetail, '['. $key .']satuanbeli', Satuanbarang::items(), array(
							            'empty' => '-- Pilih --',
							            'class'=>'form-control span-2',
							            'onkeypress' => "return $(this).focusNextInputField(event)"
							        ));?>
								</td>
								<td><?php echo $form->textField($belibrgdetail,'['. $key .']jmldlmkemasan', array('class'=>'input-mini decimal')); ?></td>
								<td><?php echo $form->textField($belibrgdetail,'['. $key .']hargabeli', array(
									'class'=>'input-small money',
									'onkeyup'=>'hitungNominal(this);'
								)); ?></td>
								<td><?php echo $form->textField($belibrgdetail,'['. $key .']hargasatuan', array('class'=>'input-small money')); ?></td>
								<td><?php echo $form->textField($belibrgdetail,'['. $key .']ppn', array('class'=>'input-small money')); ?></td>
								<td><?php echo $form->textField($belibrgdetail,'['. $key .']persendiskon', array('class'=>'input-mini decimal')); ?></td>
								<td style="text-align: center;">
							        <?=Chtml::link('<span class="fa-stack"><i class="text-success fa fa-circle fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>', 'javascript:void(0);', array(
							            'class'=>'remove',
							            'onClick'=>'removeMe(this)'
							        ));?>
							    </td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>
						<tr class="rec">
							<td colspan="10" style="text-align:center">Data kosong</td>
						</tr>
					<?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</fieldset>
<div class="form-group">
    <?php echo CHtml::link('<i class="fa fa-home"></i> Kembali', array('/gudangUnit/pembelianBarang/daftarPembelian'), array(
        'class'=>'btn btn-error btn-sm'
    ))?>
    <?php echo CHtml::tag('button', array(
        'name'=>'verifikasi',
        'class'=>'btn btn-info btn-sm kirim'
    ), '<i class="fa fa-save"></i> Simpan Pembelian')?>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
$(".decimal").inputmask("decimal",{
	digits:2
});
$(".money").inputmask("currency",{
	digits:0,
	prefix: ""
});
var temp_grid = '<?=CJSON::encode(preg_replace('/\r+/', '', preg_replace('/\n+/', '', $this->renderPartial('_form_barang', array(
    'form'=>$form,
    'permintaanbarangdetail'=>$permintaanbarangdetail
), true))));?>';

var barang_id = '<?php echo json_encode($id_barang) ?>';
var idx_barang = <?php echo count($barang_det) ?>;
$('.addBarang').on('click', function(){
    var cloning = $(temp_grid).clone();
    idx_barang = idx_barang + 1;
    $('#grid_pembelian').find('tbody .rec').remove();
    $(cloning).find('[name*="[0]"]').each(function(i){
        $(this).attr('name', $(this).attr('name').replace("[0]", '['+ idx_barang +']' ));
    });
    $('#grid_pembelian').find('tbody').append(cloning);
    setTimeout(function(){
        $('#grid_pembelian').find('.typeahead').autocomplete({
            'showAnim':'fold',
            'minLength':2,
            'focus':function( event, ui ) {
                $(this).val( ui.item.label);
                return false;
            },
            'select':function( event, ui ){
                var id_item = ui.item.value;
                if(barang_id[id_item] != undefined){
                    alert('Barang telah terdaftar');
                    $(this).val('');
                }else{
                    barang_id[id_item] = 'OK';
                    var that = this;
                    $(this).parent().find('[name*="[barang_id]"]').val(ui.item.value);
                    $.ajax({
                        url: "<?=Yii::app()->createUrl('actionAjax/detailBarang')?>",
                        dataType: "json",
                        data:{
                            map: ui.item.barang_id,
                        },
                        success: function(data){
                            if(Object.keys(data.record).length > 0){
                                $.each(data.record, function(index, el){
                                    $(that).closest('tr').find('[name*="['+ index +']"]').val(el);
                                });
                            }
                        }
                    })
                }

                return false;
            },
            'source': function(request, response){
                $.ajax({
                   url: "<?=Yii::app()->createUrl('actionAutoComplete/barang')?>",
                   dataType: "json",
                   data: {
                       term: request.term,
                   },
                   success: function (data) {
                       response(data);
                   }
                })
            }
        });
    }, 500);
    return false;
})
function removeMe(obj){
    $(obj).closest('tr').remove();
    var tr_lenght = $('#grid_pembelian').find('tbody tr').length;
    if(tr_lenght == 0){
        var temp = '<tr><td class="rec" colspan="10"><center>Data kosong</center></td></tr>';
        $('#grid_pembelian').find('tbody').append(temp);
    }
    return false;
};
$('#pembelian-form').on('submit', function(){
    if(barang_id.length > 0){
		$(".decimal").inputmask("remove");
		$(".money").inputmask("remove");
        var that = this;
        swal.queue([{
            confirmButtonText: 'Simpan',
            showCancelButton: true,
            text:'Yakin akan menyimpan data?',
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            preConfirm: function(){
                return new Promise(function(resolve){
                    $.ajax({
                        type: "POST",
                        data: $('#pembelian-form').serialize(),
                        url: $('#pembelian-form').attr('action'),
						dataType: 'json',
                        success: function(response, statusText, xhr, $form){
                            var type_notif = 'error';
                            if(response.status == 'OK'){
                                type_notif = 'success';
                            }
                            swal({
                                title: response.pesan,
                                text: response.message,
                                type: type_notif,
                                preConfirm: function (){
                                    return new Promise(function(resolve, reject){
                                        if(type_notif == 'success'){
											window.location.href = '<?=Yii::app()->createUrl('gudangUnit/pembelianBarang/daftarPembelian')?>';
                                        }
                                        resolve();
                                    });
                                }
                            });
                        }
                    });
                })
            }
        }])
    }else{
        alert('Silahkan input daftar barang terlebih dahulu');
    }
    return false;
});

function hitungNominal(obj){
	$(".decimal").inputmask("unmaskedvalue");
	$(".money").inputmask("unmaskedvalue");
	var hargabeli = $(obj).closest('tr').find('[name*="[hargabeli]"]').inputmask("unmaskedvalue");
	var jmlbeli = $(obj).closest('tr').find('[name*="[jmlbeli]"]').inputmask("unmaskedvalue");
	var jmldlmkemasan = $(obj).closest('tr').find('[name*="[jmldlmkemasan]"]').inputmask("unmaskedvalue");
	var hargasatuan = hargabeli / jmldlmkemasan;
	var ppn = (hargasatuan * 2.5) / 100;
	$(obj).closest('tr').find('[name*="[hargasatuan]"]').val(hargasatuan);
	$(obj).closest('tr').find('[name*="[ppn]"]').val(ppn);
}
</script>
