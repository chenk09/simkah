<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/font-awesome/css/font-awesome.min.css'); ?>
<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'permintaan-pembalian-form',
		'enableAjaxValidation'=>false,
		'type'=>'horizontal',
		'htmlOptions'=>array(
			'enctype'=>"multipart/form-data",
			'onKeyPress'=>'return disableKeyPress(event)',
		)
	)
);
 ?>
<fieldset>
    <legend class="rim2">Permintaan Pembelian</legend>
    <div class="row-fluid">
        <div class="span6">
            <div class="control-group">
                <?php echo $form->labelEx($permintaanpembelianT, 'no_permintaan_beli', array(
                    'class'=>'control-label'
                ))?>
                <div class="controls">
                    <?php echo $form->textField($permintaanpembelianT,'no_permintaan_beli',array(
                        'readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)"
                    ));?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($permintaanpembelianT, 'tanggal_pengajuan', array(
                    'class'=>'control-label'
                ))?>
                <div class="controls">
					<?php $this->widget('MyDateTimePicker',array(
							'model'=>$permintaanpembelianT,
							'attribute'=>'tanggal_pengajuan',
							'options'=> array(
							'dateFormat'=>Params::DATE_TIME_FORMAT,
							'maxDate'=>'d',
						),
						'htmlOptions'=>array(
							'readonly'=>true,
							'class'=>'dtPicker3',
							'onkeypress'=>"return $(this).focusNextInputField(event)"
						),
					)); ?>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="control-group">
                <?php echo $form->labelEx($permintaanpembelianT, 'keterangan', array(
                    'class'=>'control-label'
                ))?>
                <div class="controls">
                    <?php echo $form->textArea($permintaanpembelianT,'keterangan',array(
                        'onkeypress'=>"return $(this).focusNextInputField(event)"
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend class="rim">Detail Pembelian</legend>
    <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i> Tambah',array(
        'class' => 'btn btn-primary addBarang',
        'type' => 'button',
        'onKeypress' => 'return false;'
    ));?>
    <div class="row-fluid">
        <div class="span12">
            <table id="grid_pembelian" class="items table table-bordered">
                <thead>
                    <tr>
                        <th>Nama Barang</th>
                        <th width="50">Jumlah</th>
                        <th width="50">Satuan</th>
                        <th width="20">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $id_barang = array(); ?>
                    <?php if (count($barang_det) > 0): ?>
                        <?php foreach ($barang_det as $key => $value): ?>
                            <?php $id_barang[$value->barang_id] = 'YA'; ?>
                            <tr>
                                <td>
									<?php $permintaanbarangdetail->barang_id = $value->barang_id; ?>
									<?php $permintaanbarangdetail->satuanbarang = $value->satuanbarang; ?>
                                    <?php echo $form->hiddenField($permintaanbarangdetail,'['. $key .']barang_id'); ?>
                                    <?php echo $value->barang->barang_type; ?> - <?php echo $value->barang->barang_nama; ?>
                                </td>
                                <td>
                                    <?php echo $form->textField($permintaanbarangdetail,'['. $key .']jml_permintaan',array(
                                        'class'=>'input-mini',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)"
                                    )); ?>
                                </td>
							    <td>
							        <?php echo $form->dropDownList($permintaanbarangdetail, '['. $key .']satuanbarang', Satuanbarang::items(), array(
							            'empty' => '-- Pilih --',
							            'class'=>'form-control span-2 inputan',
							            'onkeypress' => "return $(this).focusNextInputField(event)"
							        ));?>
							    </td>
                                <td style="text-align:center">
                                    <?php echo CHtml::link('<span class="fa-stack"><i class="text-success fa fa-circle fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>',
                                        'javascript:void(0);',
                                        array(
                                            'onclick'=>'removeMe(this);'
                                        )
                                    );?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td colspan="4" class="rec" style="text-align:center">Data kosong</td>
						</tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</fieldset>
<div class="form-group">
    <?php echo CHtml::link('<i class="fa fa-home"></i> Kembali', array('/gudangUnit/permintaanPembelian/daftarPermintaan'), array(
        'class'=>'btn btn-error btn-sm'
    ))?>
    <?php echo CHtml::tag('button', array(
        'name'=>'verifikasi',
        'class'=>'btn btn-info btn-sm kirim'
    ), '<i class="fa fa-save"></i> Simpan Permintaan')?>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
var temp_grid = '<?=CJSON::encode(preg_replace('/\r+/', '', preg_replace('/\n+/', '', $this->renderPartial('_form_barang', array(
    'form'=>$form,
    'permintaanbarangdetail'=>$permintaanbarangdetail
), true))));?>';

var barang_id = '<?php echo json_encode($id_barang) ?>';
var idx_barang = <?php echo count($barang_det) ?>;
$('.addBarang').on('click', function(){
    var cloning = $(temp_grid).clone();
    idx_barang = idx_barang + 1;
    $('#grid_pembelian').find('tbody .rec').remove();
    $(cloning).find('[name*="[0]"]').each(function(i){
        $(this).attr('name', $(this).attr('name').replace("[0]", '['+ idx_barang +']' ));
    });
    $('#grid_pembelian').find('tbody').append(cloning);
    setTimeout(function(){
        $('#grid_pembelian').find('.typeahead').autocomplete({
            'showAnim':'fold',
            'minLength':2,
            'focus':function( event, ui ) {
                $(this).val( ui.item.label);
                return false;
            },
            'select':function( event, ui ){
                var id_item = ui.item.value;
                if(barang_id[id_item] != undefined){
                    alert('Barang telah terdaftar');
                    $(this).val('');
                }else{
                    barang_id[id_item] = 'OK';
                    var that = this;
                    $(this).parent().find('[name*="[barang_id]"]').val(ui.item.value);
                    $.ajax({
                        url: "<?=Yii::app()->createUrl('actionAjax/detailBarang')?>",
                        dataType: "json",
                        data:{
                            map: ui.item.barang_id,
                        },
                        success: function(data){
                            if(Object.keys(data.record).length > 0){
                                $.each(data.record, function(index, el){
                                    $(that).closest('tr').find('[name*="['+ index +']"]').val(el);
                                });
                            }
                        }
                    })
                }

                return false;
            },
            'source': function(request, response){
                $.ajax({
                   url: "<?=Yii::app()->createUrl('actionAutoComplete/barang')?>",
                   dataType: "json",
                   data: {
                       term: request.term,
                   },
                   success: function (data) {
                       response(data);
                   }
                })
            }
        });
    }, 500);
    return false;
})
function removeMe(obj){
    $(obj).closest('tr').remove();
    var tr_lenght = $('#grid_pembelian').find('tbody tr').length;
    if(tr_lenght == 0){
        var temp = '<tr><td class="rec" colspan="7"><center>Data kosong</center></td></tr>';
        $('#grid_pembelian').find('tbody').append(temp);
    }
    return false;
};
$('#permintaan-pembalian-form').on('submit', function(){
    if(barang_id.length > 0){
        var that = this;
        var formdata = new FormData(this);
        swal.queue([{
            confirmButtonText: 'Simpan',
            showCancelButton: true,
            text:'Yakin akan menyimpan data?',
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            preConfirm: function (){
                return new Promise(function (resolve){
                    $.ajax({
                        type: "POST",
                        data:formdata,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        url: $('#permintaan-pembalian-form').attr('action'),
                        success: function(response, statusText, xhr, $form){
                            var type_notif = 'error';
                            if(response.status == 'OK'){
                                type_notif = 'success';
                            }
                            swal({
                                title: response.pesan,
                                text: response.message,
                                type: type_notif,
                                preConfirm: function (){
                                    return new Promise(function(resolve, reject){
                                        if(type_notif == 'success'){
											window.location.href = '<?=Yii::app()->createUrl('gudangUnit/permintaanPembelian/informasiPermintaan')?>';
                                        }
                                        resolve();
                                    });
                                }
                            });
                        }
                    });
                })
            }
        }])
    }else{
        alert('Silahkan input daftar barang terlebih dahulu');
    }
    return false;
});
</script>
