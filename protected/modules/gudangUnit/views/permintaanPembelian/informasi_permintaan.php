<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/plugins/sweet-alert/sweetalert2.min.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/plugins/font-awesome/css/font-awesome.min.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-modal.js'); ?>

<fieldset>
    <legend class="rim2">Transaksi Permintaan Pembelian</legend>
</fieldset>
<fieldset>
    <legend class="rim">Detail Barang</legend>
    <?php echo CHtml::link('<i class="icon-plus icon-white"></i> Tambah',array('/gudangUnit/permintaanPembelian/tambahPermintaan'), array(
        'class' => 'btn btn-primary addBarang',
    ));?>
    <?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    	'id'=>'sainstalasi-m-grid',
    	'dataProvider'=>$permintaanpembelian->search(),
        'template'=>"{summary}\n{items}{pager}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
            array(
                'type' => 'raw',
                'header' => 'No',
                'htmlOptions' => array(
                    'style' => 'text-align:right;width:15px;white-space:nowrap',
                ),
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            ),
            'no_permintaan_beli',
            'tanggal_pengajuan',
            'keterangan',
            array(
                'type'=>'raw',
                'name'=>'ruangan_id',
                'value'=>'$data->ruangans->ruangan_nama'
            ),
            array(
                'value'=>'$data->tombolStatus',
                'type'=>'raw',
                'htmlOptions' => array(
                    'style' => 'text-align:center;white-space:nowrap',
                ),
                'headerHtmlOptions' => array(
                    'style' => 'width: 50px;'
                )
            ),
            array(
                'value'=>'$data->tombolTrans',
                'type'=>'raw',
                'htmlOptions' => array(
                    'style' => 'text-align:center;white-space:nowrap',
                ),
                'headerHtmlOptions' => array(
                    'style' => 'width: 50px;'
                )
            )
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); ?>
</fieldset>
<div id="mdlDetInfo" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop='static'>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modal header</h3>
    </div>
    <div class="modal-body">
        <div style="width:850px">
            <p>Loading…</p>
        </div>
    </div>
</div>
<script type="text/javascript">
function tampilModal(obj){
    var title = $(obj).attr('title');
    var uri = $(obj).attr('uri');
    $('#myModalLabel').text(title);
    $('#mdlDetInfo').find('.modal-body > div').load(uri, function( response, status, xhr ) {
        if (status == "error"){
            var msg = "Sorry but there was an error: ";
            $('#mdlDetInfo').find('.modal-body > div').html( msg + xhr.status + " " + xhr.statusText);
        }
    });
    $('#mdlDetInfo').modal('show');
};
</script>
