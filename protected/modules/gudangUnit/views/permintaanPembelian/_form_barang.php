<tr class="hasil">
    <td>
        <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model' => $permintaanbarangdetail,
            'attribute' => '[0]nama_barang',
            'source' => 'js: function(request, response){
                $.ajax({
                    url: "' . Yii::app()->createUrl('actionAutoComplete/barang') . '",
                    dataType: "json",
                    data: {
                        term: request.term,
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            }',
            'options' => array(
                'showAnim' => 'fold',
                'minLength' => 2,
                'focus' => 'js:function( event, ui ) {
                    $(this).val( ui.item.label);
                    return false;
                }',
                'select' => 'js:function( event, ui ){
                    $("#idBarang").val(ui.item.barang_id);
                    return false;
                }',
            ),
            'htmlOptions' => array(
                'onkeypress' => "return $(this).focusNextInputField(event)",
                'class' => 'form-control typeahead inputan',
            )
        ));?>
        <?php echo $form->hiddenField($permintaanbarangdetail, '[0]barang_id', array(
            'class'=>'form-control span-6 inputan'
        ));?>
    </td>
    <td>
        <?php echo $form->textField($permintaanbarangdetail, '[0]jml_permintaan', array(
            'class'=>'form-control span-2 inputan'
        ));?>
    </td>
    <td>
        <?php echo $form->dropDownList($permintaanbarangdetail, '[0]satuanbarang', Satuanbarang::items(), array(
            'empty' => '-- Pilih --',
            'class'=>'form-control span-2 inputan',
            'onkeypress' => "return $(this).focusNextInputField(event)"
        ));?>
    </td>
    <td style="text-align: center;">
        <?=CHtml::link('<span class="fa-stack"><i class="text-success fa fa-circle fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>',
            'javascript:void(0);',
            array(
                'class'=>'remove',
                'onclick'=>'removeMe(this);'
            )
        );?>
    </td>
</tr>