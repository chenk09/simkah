<?php

class DaftarPasienController extends SBaseController
{
        public $successSave = false;
        public $successSaveJadwal = true;
        public $successSaveHasil = true;
        
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','buatJadwal','hasilPemeriksaan'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        public function actionIndex()
	{
               $this->pageTitle = Yii::app()->name." - Daftar Pasien";
               $modPasienMasukPenunjang = new RMMasukPenunjangV;
               $format = new CustomFormat();
               $modPasienMasukPenunjang->tglAwal = date("d M Y").' 00:00:00';
               $modPasienMasukPenunjang->tglAkhir = date('d M Y H:i:s');
               if(isset ($_REQUEST['RMMasukPenunjangV'])){
                    $modPasienMasukPenunjang->attributes=$_REQUEST['RMMasukPenunjangV'];
                    $modPasienMasukPenunjang->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMMasukPenunjangV']['tglAwal']);
                    $modPasienMasukPenunjang->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMMasukPenunjangV']['tglAkhir']);
               
                    $modPasienMasukPenunjang->ceklis = $_REQUEST['RMMasukPenunjangV']['ceklis'];
               }
               $this->render('index',array(
                                 'modPasienMasukPenunjang'=>$modPasienMasukPenunjang                                 
                ));
	}
        
        public function actionBuatJadwal($id)
        {
            $this->pageTitle = Yii::app()->name." - Buat Jadwal";
            $modHasilPemeriksaan = $this->loadAllByPasienMasukPenunjang($id);
            $modPasienPenunjang = RMMasukPenunjangV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$id)); //data pasien penunjang
            $modTindakanPelayanan = new RMTindakanPelayananT;
            $modTindakanKomponen = new RMTindakanKomponenT;
            $modJadwalKunjungan = new JadwalkunjunganrmT;
            $modNewHasil = new HasilpemeriksaanrmT;
            $listJadwalKunjungan = JadwalkunjunganrmT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$id));
            
            if(isset ($_POST['JadwalKunjungan']))
            {
                $transaction = Yii::app()->db->beginTransaction();
                try
                {
                $modJadwalKunjungan = $this->saveJadwalKunjungan($_POST['JadwalKunjungan'],$modPasienPenunjang);
                
                if ($this->successSave && $this->successSaveJadwal && $this->successSaveHasil){
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                }
                else{
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('buatJadwal',array(
                                 'modPasienPenunjang'=>$modPasienPenunjang,                                 
                                 'modTindakanPelayanan'=>$modTindakanPelayanan,                                 
                                 'modTindakanKomponen'=>$modTindakanKomponen,                                 
                                 'modJadwalKunjungan'=>$modJadwalKunjungan,                                 
                                 'modNewHasil'=>$modNewHasil,  
                                 'listJadwalKunjungan'=>$listJadwalKunjungan,
                                 'id'=>$id
                ));
        }
        
        /**
         * Fungsi untuk menyimpan ke tabel jadwalkunjungan_t
         * @param type $attrJadwal
         * @param type $modPasienPenunjang 
         */
        protected function saveJadwalKunjungan($attrJadwal,$modPasienPenunjang)
        {
            $format = new CustomFormat();
            $arrSave = array();
            $validJadwal = true;
            $arrTindakan= array(); // array untuk menampung tindakan yg nantinnya digunakan pada proses saveHasilpemeriksaan
            $arrIdHasilPemeriksaan= array(); // array untuk menampung hasilpemeriksaan_id yg nantinnya digunakan pada proses saveHasilpemeriksaan
            for ($f = 0; $f < $_POST['lamaterapi']; $f++) 
            {
                    $modJadwalKunjungan = new JadwalkunjunganrmT;
                    $modJadwalKunjungan->pegawai_id = (!empty($attrJadwal['pegawai_id'][$f])) ? $attrJadwal['pegawai_id'][$f] : null ;
                    $modJadwalKunjungan->pasien_id = $modPasienPenunjang->pasien_id ;
                    $modJadwalKunjungan->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id ;
                    $modJadwalKunjungan->pendaftaran_id = $modPasienPenunjang->pendaftaran_id ;
                    $modJadwalKunjungan->nojadwal = Generator::noUrutJadwalRencanaRM();
                    $modJadwalKunjungan->nourutjadwal = $f + 1;
                    $modJadwalKunjungan->tgljadwalrm = $attrJadwal['tgljadwalrm'][$f] ;
                    $modJadwalKunjungan->harijadwalrm = $this->getNamaHari($attrJadwal['tgljadwalrm'][$f]) ;
                    $modJadwalKunjungan->lamaterapikunjungan = $_POST['lamaterapi'];
                    $modJadwalKunjungan->paramedis1_id = (!empty($attrJadwal['paramedis1_id'][$f])) ? $attrJadwal['paramedis1_id'][$f] : null ;
                    $modJadwalKunjungan->paramedis2_id = (!empty($attrJadwal['paramedis2_id'][$f])) ? $attrJadwal['paramedis2_id'][$f] : null ;
                    
                    $modJadwalKunjungan->create_loginpemakai_id = Yii::app()->user->id;
                    $modJadwalKunjungan->create_ruangan = Yii::app()->user->getState('ruangan_id');
                    $modJadwalKunjungan->create_time = date('Y-m-d H:i:s');
                    
                    $arrIdHasilPemeriksaan[$f] =array(
                                            'hasilpemeriksaanrm_id'=> $attrJadwal['hasilpemeriksaanrm_id'][$f]
                                        );
                    
                    if ($modJadwalKunjungan->validate()){
                            $arrSave[$f] = $modJadwalKunjungan; // menyimpan objek JadwalkunjunganrmT ke dalam sebuah array dan siap untuk disave *kaya masak ya :p
                                                        
                    }else{
                        $validJadwal = false;
                    }
                    
            } //ENDING FOR
            if($validJadwal) //kondisi apabila semua Jadwal tindakan valid dan siap untuk di save
            {   
                $arrIdHasil = array(); //membuang nilai array yg empty . . 
                
                foreach ($arrIdHasilPemeriksaan as $z=>$idHasil)
                {
                    if ($idHasil['hasilpemeriksaanrm_id'] == '')
                    {
                        unset($idHasil[$z]);
                    }
                    else
                    {
                        $arrIdHasil[$z] = array(
                            'hasilpemeriksaanrm_id'=> $idHasil['hasilpemeriksaanrm_id']
                        );
                    }
                }
                
                foreach ($arrSave as $x => $simpan) 
                {
                    $simpan->save();
                    if($x < 1) // kondisi dimana proses save pada baris pertama, yang asumsinya bahwa jadwal pertama sudah pasti mempunyai hasilpemeriksaanrm_t maka akan diupdate
                    {
                        $this->updateHasilPemeriksaan($simpan,$arrIdHasil);
                    }
                    else
                    {
                       $this->saveHasilPemeriksaan($modPasienPenunjang,$attrJadwal,$simpan,$x);
                    }
                } //ENDING FOREACH
            }
            
            else
            {
                $this->successSave = false;
            }
            return $modJadwalKunjungan;
        }
        
        protected function saveHasilPemeriksaan($attrPenunjang,$attrTindakan,$modJadwal,$index)
        {
            $arrSave = array();
            $validTindakan = true;
            $arrTindakan = array(); // array untuk menampung tindakan yg nantinnya digunakan pada proses saveTindakanPelayanan
            for ($i = 0; $i < count($attrTindakan['tindakanrm_id'][$index]); $i++) {
                    
                        $modHasil = new HasilpemeriksaanrmT;
                        $modHasil->nohasilrm = Generator::noHasilPemeriksaanRM();
                        $modHasil->jadwalkunjunganrm_id = $modJadwal->jadwalkunjunganrm_id;
                        $modHasil->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                        $modHasil->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                        $modHasil->pasien_id = $attrPenunjang->pasien_id;
                        $modHasil->ruangan_id = $attrPenunjang->ruangan_id;
                        $modHasil->pegawai_id = $attrPenunjang->pegawai_id;
                        $modHasil->tglpemeriksaanrm = date('Y-m-d H:i:s');
                        $modHasil->kunjunganke = $modJadwal->nourutjadwal; //di default untuk kunjungan pertama
                        
                        $modHasil->tindakanrm_id = $attrTindakan['tindakanrm_id'][$index][$i];
                        $modHasil->jenistindakanrm_id = RMTindakanrmM::model()->findByPk($modHasil->tindakanrm_id)->jenistindakanrm_id;
                        
                        $modHasil->create_time=date('Y-m-d H:i:s');
                        $modHasil->create_loginpemakai_id=Yii::app()->user->id;
                        $modHasil->create_ruangan=Yii::app()->user->getState('ruangan_id');
                        
                        if ($modHasil->validate()){
                            $arrSave[$i] = $modHasil; // menyimpan objek BSRencanaOperasiT ke dalam sebuah array dan siap untuk disave

                        }else{
                            $validTindakan = false;
                        }
                } //ENDING FOR 
                if($validTindakan) //kondisi apabila semua rencana operasi valid dan siap untuk di save
                {
                    foreach ($arrSave as $f => $simpan) 
                    {
                        $simpan->save();
//                        $this->saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$simpan,$attrTindakanPelayanan,$f);
                    }
                    $this->successSave = true;
                }
                else
                {
                    $this->successSave = false;
                }
            return $modHasil;
        }
        
        /**
         * Fungsi untuk mengupdate hasilpemeriksaanrm_t pada saat kunjungan pertama yg asumsinya dia sudah punya hasil pemeriksaan
         * @param type $attrHasil 
         */
        protected function updateHasilPemeriksaan($modJadwal,$attrHasil)    
        {   
            $validHasil = true;
            for ($i = 0; $i < count($attrHasil); $i++) 
            {
                $modHasil = $this->loadHasilPemeriksaan($attrHasil[$i]['hasilpemeriksaanrm_id']);
                $modHasil->jadwalkunjunganrm_id = $modJadwal->jadwalkunjunganrm_id;
                $modHasil->pegawai_id = (!empty($modJadwal->pegawai_id)) ? $modJadwal->pegawai_id : null ;
                $modHasil->paramedis1_id = (!empty($modJadwal->paramedis1_id)) ? $modJadwal->paramedis1_id : null ;
                $modHasil->paramedis2_id = (!empty($modJadwal->paramedis2_id)) ? $modJadwal->paramedis2_id : null ;
                if($modHasil->validate())
                {
                   $arrSave[$i] = $modHasil; // menyimpan objek 
                }else{
                   $validHasil = false;
                }
            } //ENDING FOR
            
            if($validHasil) //kondisi apabila semua hasil valid dan siap untuk di save
                {
                    foreach ($arrSave as $f => $simpan) 
                    {
                        $simpan->save();
                        $this->successSave = true;
                    }
                }
                else
                {
                    $this->successSave = false;
                }
            return $modHasil;
            
        }
        
        
        public function actionHasilPemeriksaan($idPendaftaran,$idPasien,$idPasienMasukPenunjang)
        {
            $modPasienMasukPenunjang = RMMasukPenunjangV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            
            $modJadwalKunjungan = JadwalkunjunganrmT::model()->findAll('pendaftaran_id = '.$idPendaftaran.' and 
                                                                        pasienmasukpenunjang_id = '.$idPasienMasukPenunjang.' and
                                                                        pasien_id = '.$idPasien.' and
                                                                        tglkunjunganrm is not null
                                                                        order by nourutjadwal');
            if(isset($_POST['hasilpemeriksaanrm']))
            {
               $transaction = Yii::app()->db->beginTransaction();
                try
                { 
                    
                    for ($i = 0; $i < count($_POST['hasilpemeriksaanrm']['hasilpemeriksaanrm_id']); $i++) {
                        $modHasil = $this->loadHasilPemeriksaan($_POST['hasilpemeriksaanrm']['hasilpemeriksaanrm_id'][$i]);
                        $modHasil->hasilpemeriksaanrm = $_POST['hasilpemeriksaanrm']['hasilpemeriksaanrm'][$i];
                        $modHasil->keteranganhasilrm = $_POST['hasilpemeriksaanrm']['keteranganhasilrm'][$i];
                        $modHasil->peralatandigunakan = $_POST['hasilpemeriksaanrm']['peralatandigunakan'][$i];
                        if($_POST['hasilpemeriksaanrm']['hasilpemeriksaanrm'][$i] == '' && 
                           $_POST['hasilpemeriksaanrm']['keteranganhasilrm'][$i]  == '' && 
                           $_POST['hasilpemeriksaanrm']['peralatandigunakan'][$i]  == '')
                        {
                            $update = TRUE;
                        }
                        else
                        {
                            $update = JadwalkunjunganrmT::model()->updateByPk($modHasil->jadwalkunjunganrm_id, array('statusterapi'=>1));
                        }
                        if($modHasil->save() && $update)
                        {
                             $this->successSaveHasil = TRUE;
                        }
                        else{
                            $this->successSaveHasil = FALSE;
                        }
                    }
                    if ($this->successSaveHasil){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    }
                    else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('hasilPemeriksaan',array('modJadwalKunjungan'=>$modJadwalKunjungan,
                                                   'modPasienPenunjang'=>$modPasienMasukPenunjang,
                                                   ));
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dengan method findAllByAttributes yang nanti digunakan untuk mendeskripsikan operasi_id
         * @param type $id
         * @return type 
         */
        protected function loadAllByPasienMasukPenunjang($id)
        {
                $model= HasilpemeriksaanrmT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$id));
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dengan method findByAttributes yang nanti digunakan untuk mendeskripsikan hasilpemeriksanrm_t
         * @param type $id
         * @return type 
         */
        protected function loadHasilPemeriksaan($id)
        {
                $model= HasilpemeriksaanrmT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        
        

	
}