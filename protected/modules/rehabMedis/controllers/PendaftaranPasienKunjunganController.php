<?php

class PendaftaranPasienKunjunganController extends SBaseController
{
        public $successSave = false;
        /**
	 * @return array action filters
	 */
         
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex()
	{
            
            $this->pageTitle = Yii::app()->name." - Pendaftaran Pasien Kunjungan ";
            $model = $this->loadModel($_POST['pendaftaran_id']);
            $modPasien = $this->loadModelPasien($model->pasien_id);
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->umur = "00 Thn 00 Bln 00 Hr";
            $model->ruangan_id = Yii::app()->user->ruangan_id;
            // $model->kelaspelayanan_id = Params::kelasPelayanan('bedah_sentral');
            // $model->kelaspelayanan_id = 5;

            $modPasienPenunjang = new RMPasienMasukPenunjangT;
            $modTindakanPelayanan = new RMTindakanPelayananT;
            $modTindakanKomponen = new RMTindakanKomponenT;
            $modJenisTindakan = RMJenisTindakanrmM::model()->findAllByAttributes(array('jenistindakanrm_aktif'=>true),array('order'=>'jenistindakanrm_nama'));
            $modTindakan = RMTindakanrmM::model()->findAllByAttributes(array('tindakanrm_aktif'=>true),array('order'=>'tindakanrm_nama'));
            $modHasilPemeriksaan = new HasilpemeriksaanrmT;
            
            if (isset($_POST['RMTindakanPelayananT'])){
                if(isset($_POST['tindakanrm_id']))
                {
                    $attrTindakan = $_POST['tindakanrm_id'];
                    $attrCeklis = $_POST['ceklis'];
                }
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    
                    
                    //==AwalSimpan Psien Masuk Penunjang========================
                      $modPasienPenunjang = $this->savePasienPenunjang($model);
                    //Akhir Pasien MAsuk Penunjang
                      
                    //==AwalSimpan Rencana Operasi========================  
                      if(!empty($_POST['tindakanrm_id']))
                      {
                          $modHasilPemeriksaan = $this->saveHasilPemeriksaan($model,$modPasienPenunjang,$attrTindakan,$attrCeklis,$_POST['RMTindakanPelayananT']);
                      }
                      else
                      {
                          $modHasilPemeriksaan->validate();
                          $this->successSave = false;
                      }
                    //Akhir Rencana Operasi
                      
                    if ($this->successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    }
                    else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('index',array(
                'model'=>$model, 
                'modPasien'=>$modPasien, 
                'modPasienPenunjang'=>$modPasienPenunjang,
                'modTindakanKomponen'=>$modTindakanKomponen,
                'modHasilPemeriksaan'=>$modHasilPemeriksaan,
                'modTindakanPelayanan'=>$modTindakanPelayanan,
                'modJenisTindakan'=>$modJenisTindakan,
                'modTindakan'=>$modTindakan,
                
            ));
	}
        
        /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=  RMPendaftaranMp::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        
        /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModelPasien($id)
	{
		$model= RMPasienM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function savePasienPenunjang($attrPendaftaran){
            
            $modPasienPenunjang = new RMPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $attrPendaftaran->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
            switch ($modPasienPenunjang->ruangan_id) {
                case Params::RUANGAN_ID_LAB:$inisial_ruangan = 'LK';break;
                case Params::RUANGAN_ID_RAD:$inisial_ruangan = 'RO';break;
                case Params::RUANGAN_ID_REHAB:$inisial_ruangan = 'RE';break;
                case Params::RUANGAN_ID_IRM:$inisial_ruangan = 'RM';break;
                default:break;
            }
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang($inisial_ruangan);
            $modPasienPenunjang->tglmasukpenunjang = $attrPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
            
            
            if ($modPasienPenunjang->validate()){
                
                $modPasienPenunjang->Save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }
        
         public function saveHasilPemeriksaan($attrPendaftaran,$attrPenunjang,$attrTindakan,$attrCeklis,$attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakan = 'true';
            $arrTindakan = array(); // array untuk menampung tindakan yg nantinnya digunakan pada proses saveTindakanPelayanan
            for ($i = 0; $i < count($attrCeklis); $i++) {
                    $patokan = $attrCeklis[$i];

                        $modHasil = new HasilpemeriksaanrmT;
                        $modHasil->nohasilrm = Generator::noHasilPemeriksaanRM();
                        $modHasil->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                        $modHasil->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                        $modHasil->pasien_id = $attrPenunjang->pasien_id;
                        $modHasil->ruangan_id = $attrPenunjang->ruangan_id;
                        $modHasil->pegawai_id = $attrPenunjang->pegawai_id;
                        $modHasil->tglpemeriksaanrm = date('Y-m-d H:i:s');
                        $modHasil->kunjunganke = 1; //di default untuk kunjungan pertama
                        
                        $modHasil->tindakanrm_id = $attrTindakan[$patokan];
                        $modHasil->jenistindakanrm_id = RMTindakanrmM::model()->findByPk($attrTindakan[$patokan])->jenistindakanrm_id;
                        $arrTindakan[$i]=array(
                                            'tindakan'=> $attrTindakan[$patokan]
                                        );
                        $modHasil->create_time=date('Y-m-d H:i:s');
                        $modHasil->create_loginpemakai_id=Yii::app()->user->id;
                        $modHasil->create_ruangan=Yii::app()->user->getState('ruangan_id');
                        
                        
                        if ($modHasil->validate()){
                            $arrSave[$i] = $modHasil; // menyimpan objek BSRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                            $validTindakan = 'true'; // variabel untuk menentukan rencana operasi valid

                        }else{
                            $modHasil->tglpemeriksaanrm = Yii::app()->dateFormatter->formatDateTime(
                                    CDateTimeParser::parse($modHasil->tglpemeriksaanrm, 'yyyy-MM-dd'), 'medium', null);

                            $validTindakan = $validTindakan.'false';
                        }
                } //ENDING FOR 
                if($validTindakan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
                {
                    foreach ($arrTindakan as $x => $hasilTindakan) {
                        $tindakanNya[$x] = $hasilTindakan['tindakan'];
                    }
                    
                    foreach ($arrSave as $f => $simpan) {
                        
                        $simpan->save();
                        $this->saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$simpan,$attrTindakanPelayanan,$tindakanNya[$f]);
                    }
                    $this->successSave = true;
                }
                else
                {
                    $this->successSave = false;
                }
            return $modHasil;
        }
        
        public function saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$attrHasil,$attrTindakanPelayanan,$attrTindakan)
        {
            $validTindakanPelayanan = 'true';
            $arrSave = array();
            
            $daftar_tindakan = $attrTindakanPelayanan['daftartindakan_id'][$attrTindakan];
            
            $kelaspelayanan_id = $attrTindakanPelayanan['kelaspelayanan_id'][$attrTindakan];
            
            //Tarif-tarif dari tariftindakan_m
            $tarifTotal = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                
            $tarifRS = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_RS));

            $tarifBHP = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_BHP));

            $tarifParamedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_PARAMEDIS));

            $tarifMedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_MEDIS));

                $modTindakanPelayanan = new RMTindakanPelayananT;
                $modTindakanPelayanan->penjamin_id = $attrPendaftaran->penjamin_id;
                $modTindakanPelayanan->pasien_id = $attrPenunjang->pasien_id;
                $modTindakanPelayanan->kelaspelayanan_id = $kelaspelayanan_id;
                $modTindakanPelayanan->hasilpemeriksaanrm_id = $attrHasil->hasilpemeriksaanrm_id ;
                $modTindakanPelayanan->tipepaket_id = 1;
                $modTindakanPelayanan->instalasi_id = Params::INSTALASI_ID_REHAB;
                $modTindakanPelayanan->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayanan->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                $modTindakanPelayanan->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayanan->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                $modTindakanPelayanan->daftartindakan_id = $daftar_tindakan;
                $modTindakanPelayanan->carabayar_id = $attrPendaftaran->carabayar_id;
                $modTindakanPelayanan->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayanan->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayanan->tarif_tindakan = $attrTindakanPelayanan['tarif_tindakan'][$attrTindakan];
                $modTindakanPelayanan->tarif_satuan = $modTindakanPelayanan->tarif_tindakan;
                $modTindakanPelayanan->tarif_rsakomodasi = (!empty($tarifRS)) ? $tarifRS->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_medis = (!empty($tarifMedis)) ? $tarifMedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_paramedis = (!empty($tarifParamedis)) ? $tarifParamedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_bhp = (!empty($tarifBHP)) ? $tarifBHP->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->satuantindakan = $attrTindakanPelayanan['satuantindakan'][$attrTindakan];
                $modTindakanPelayanan->qty_tindakan = $attrTindakanPelayanan['qty_tindakan'][$attrTindakan];
                $modTindakanPelayanan->cyto_tindakan = $attrTindakanPelayanan['cyto_tindakan'][$attrTindakan];
                if($modTindakanPelayanan->cyto_tindakan){
                    $modTindakanPelayanan->tarifcyto_tindakan = $modTindakanPelayanan->tarif_tindakan * ($attrTindakanPelayanan['persencyto_tind'][$attrTindakan]/100);
                } else {
                    $modTindakanPelayanan->tarifcyto_tindakan = 0;
                }
                $modTindakanPelayanan->discount_tindakan = 0;
                $modTindakanPelayanan->subsidiasuransi_tindakan=0;
                $modTindakanPelayanan->subsidipemerintah_tindakan=0;
                $modTindakanPelayanan->subsisidirumahsakit_tindakan=0;
                $modTindakanPelayanan->iurbiaya_tindakan=0;
                
                if ($modTindakanPelayanan->validate()){
                    $arrSave[$i] = $modTindakanPelayanan; // menyimpan objek RMRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                    $validTindakanPelayanan = 'true';

                }else
                {   
                    $validTindakanPelayanan = $validTindakanPelayanan.'false';
                }
//            } //ENDING FOR
            if($validTindakanPelayanan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $x => $simpan) {
                    $simpan->save();
                    $this->saveTindakanKomponenT($simpan);
                    $this->upadateHasilTindakan($simpan);
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }

            return $modTindakanPelayanan;
        }
        
        public function saveTindakanKomponenT($attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakanKomponen = 'true';
            $daftarTindakan_id = $attrTindakanPelayanan->daftartindakan_id;
            $kelaspelayanan_id = $attrTindakanPelayanan->kelaspelayanan_id;
            $arrTarifTindakan = "select * from tariftindakan_m where daftartindakan_id = ".$daftarTindakan_id." and kelaspelayanan_id = ".$kelaspelayanan_id." and komponentarif_id <> ".Params::KOMPONENTARIF_ID_TOTAL." ";
            $query = Yii::app()->db->createCommand($arrTarifTindakan)->queryAll();
            foreach ($query as $i => $tarifKomponen) {
                $modTarifKomponen = new RMTindakanKomponenT;
                $modTarifKomponen->tindakanpelayanan_id = $attrTindakanPelayanan->tindakanpelayanan_id;
                $modTarifKomponen->komponentarif_id = $tarifKomponen['komponentarif_id'];
                $modTarifKomponen->tarif_tindakankomp = $tarifKomponen['harga_tariftindakan'];
                $modTarifKomponen->tarif_kompsatuan = $modTarifKomponen->tarif_tindakankomp;
                if($attrTindakanPelayanan->cyto_tindakan){
                    $modTarifKomponen->tarifcyto_tindakankomp = $tarifKomponen['harga_tariftindakan'] * ($tarifKomponen['persencyto_tind']/100);
                } else {
                    $modTarifKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTarifKomponen->subsidiasuransikomp = 0;
                $modTarifKomponen->subsidipemerintahkomp = 0;
                $modTarifKomponen->subsidirumahsakitkomp = 0;
                $modTarifKomponen->iurbiayakomp = 0;
                if ($modTarifKomponen->validate()){
                    $arrSave[$i] = $modTarifKomponen; // menyimpan objek tarif komponen ke dalam sebuah array dan siap untuk disave
                    $validTindakanKomponen = 'true';

                }else
                {
                    $validTindakanKomponen = $validTindakanKomponen.'false';
                }
            } // ending foreach
            if($validTindakanKomponen == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $f => $simpan) {
                    $simpan->save();
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }
            return $modTarifKomponen;
        }
        
        /**
         * Fungsi untuk mengupadte hasil pemeriksaan rehab medis menset tindakanpelayanan id
         * @param type $modTindPelayanan model object
         */
        protected function upadateHasilTindakan($modTindPelayanan)
        {
            $modHasil = $this->loadById($modTindPelayanan->hasilpemeriksaanrm_id);
            $modHasil->tindakanpelayanan_id = $modTindPelayanan->tindakanpelayanan_id;
            $modHasil->save();
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dengan method findByPk yang nanti digunakan untuk menyimpan data-data hasil pemeriksaan
         * @param type $id
         * @return type 
         */
        public function loadById($id)
        {       $model= HasilpemeriksaanrmT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }

	
}
