<?php
Yii::import('rawatJalan.controllers.TindakanController');
Yii::import('rawatJalan.models.*');
class TindakanTRMController extends TindakanController
{
        
}
//class TindakanController extends SBaseController
//{
//    public $succesSave = false;
//    protected $successSaveBmhp = true;
//    protected $successSavePemakaianBahan = true;
//
//
//    /**
//	 * @return array action filters
//	 */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}
//
//	/**
//	 * Specifies the access control rules.
//	 * This method is used by the 'accessControl' filter.
//	 * @return array access control rules
//	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','AjaxDeleteTindakanPelayanan','UpdateStok'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
//        
//	public function actionIndex($idPendaftaran)
//	{
//            $modPendaftaran = RMPendaftaranMp::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
//            $modPasien = RMPasienM::model()->findByPk($modPendaftaran->pasien_id);
//            $modViewTindakans = RMTindakanPelayananT::model()
//                                ->with('daftartindakan','dokter1','dokter2','dokterPendamping','dokterAnastesi',
//                                       'dokterDelegasi','bidan','suster','perawat','tipePaket')
//                                ->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
//            
//            $modTindakan = new RMTindakanPelayananT;
//            $modTindakan->tarifcyto_tindakan = 0;
//            
//            if(isset($_POST['RMTindakanPelayananT']) || isset($_POST['TindakanpelayananT']))
//            {
//                $modTindakans = $this->saveTindakan($modPasien, $modPendaftaran);
//                if($this->succesSave)
//                    $this->redirect(array('tindakan/','idPendaftaran'=>$idPendaftaran));
//            }
//            
//            $modViewBmhp = RMObatalkesPasienT::model()->with('obatalkes')->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
//            
//            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
//                                        'modPasien'=>$modPasien,
//                                        'modTindakans'=>$modTindakans,
//                                        'modTindakan'=>$modTindakan,
//                                        'modViewTindakans'=>$modViewTindakans,
//                                        'modViewBmhp'=>$modViewBmhp));
//	}
//        
//        public function saveTindakan($modPasien,$modPendaftaran)
//        {
//            $post = isset($_POST['TindakanpelayananT']) ? $_POST['TindakanpelayananT'] : $_POST['RMTindakanPelayananT'];
//            $valid=true; //echo $_POST['RMTindakanPelayananT'][0]['tipepaket_id'];exit;
//            foreach($post as $i=>$item)
//            {
//                if(!empty($item)){
//                    $modTindakans[$i] = new RMTindakanPelayananT;
//                    $modTindakans[$i]->attributes=$item;
//                    $modTindakans[$i]->tipepaket_id = $_POST['RMTindakanPelayananT'][0]['tipepaket_id'];
//                    $modTindakans[$i]->pasien_id = $modPasien->pasien_id;
//                    $modTindakans[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//                    $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
//                    $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
//                    $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
//                    $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//                    $modTindakans[$i]->tgl_tindakan = date('Y-m-d H:i:s');
//                    $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
//                    $modTindakans[$i]->tarif_tindakan = $modTindakans[$i]->tarif_satuan * $modTindakans[$i]->qty_tindakan;
//                    if($item['cyto_tindakan'])
//                        $modTindakans[$i]->tarifcyto_tindakan = ($item['persenCyto'] / 100) * $modTindakans[$i]->tarif_tindakan;
//                    else
//                        $modTindakans[$i]->tarifcyto_tindakan = 0;
//                    $modTindakans[$i]->discount_tindakan = 0;
//                    $modTindakans[$i]->subsidiasuransi_tindakan = 0;
//                    $modTindakans[$i]->subsidipemerintah_tindakan = 0;
//                    $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
//                    $modTindakans[$i]->iurbiaya_tindakan = 0;
//                    $modTindakans[$i]->instalasi_id = $modPendaftaran->instalasi_id;
//                    
//                    $tarifTindakan= TariftindakanM::model()->findAll('daftartindakan_id='.$item['daftartindakan_id'].' AND kelaspelayanan_id='.$modTindakans[$i]->kelaspelayanan_id.'');
//                    foreach($tarifTindakan AS $dataTarif):
//                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
//                                $modTindakans[$i]->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
//                            }
//                             if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
//                                $modTindakans[$i]->tarif_medis=$dataTarif['harga_tariftindakan'];
//                            }
//                             if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
//                                $modTindakans[$i]->tarif_paramedis=$dataTarif['harga_tariftindakan'];
//                            }
//                             if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
//                                $modTindakans[$i]->tarif_bhp=$dataTarif['harga_tariftindakan'];
//                            }
//                    endforeach;
//                    $valid = $modTindakans[$i]->validate() && $valid;
//                }
//            }
//
//            $transaction = Yii::app()->db->beginTransaction();
//            try {
//                if($valid){
//                    foreach($modTindakans as $i=>$tindakan){
//                        $tindakan->save();
//                        $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
//                        if(isset($_POST['paketBmhp'])){
//                            $modObatPasiens = $this->savePaketBmhp($modPendaftaran, $_POST['paketBmhp'],$tindakan);
//                        }
//                        if(isset($_POST['pemakaianBahan'])){
//                            $modPemakainBahans = $this->savePemakaianBahan($modPendaftaran, $_POST['pemakaianBahan'],$tindakan);
//                        }
//                    }
//                    if($statusSaveKomponen && $this->successSaveBmhp && $this->successSavePemakaianBahan) {
//                        $transaction->commit();
//                        $this->succesSave = true;
//                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
//                    } else {
//                        $transaction->rollback();
//                        Yii::app()->user->setFlash('error',"Data tidak valid ");
//                        //Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceObatAlkesPasien($modPemakainBahans));
//                    }
//                } else {
//                    $transaction->rollback();
//                    Yii::app()->user->setFlash('error',"Data tidak valid ");
//                    //Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceTindakan($modTindakans));
//                }
//            } catch (Exception $exc) {
//                $transaction->rollback();
//                Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
//            }
//            
//            return $modTindakans;
//        }
//        
//        public function saveTindakanKomponen($tindakan)
//        {   
//            $valid = true;
//            $criteria = new CDbCriteria();
//            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
//            $modTarifs = TariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$tindakan->daftartindakan_id),$criteria);
//            foreach ($modTarifs as $i => $tarif) {
//                $modTindakanKomponen = new TindakankomponenT;
//                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
//                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
//                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
//                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
//                if($tindakan->cyto_tindakan){
//                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
//                } else {
//                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
//                }
//                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
//                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
//                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
//                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
//                $valid = $modTindakanKomponen->validate() && $valid;
//                if($valid)
//                    $modTindakanKomponen->save();
//            }
//            
//            return $valid;
//        }
//
//        private function traceTindakan($modTindakans)
//        {
//            foreach ($modTindakans as $key => $modTindakan) {
//                $echo .= "<pre>".print_r($modTindakan->attributes,1)."</pre>";
//            }
//            return $echo;
//        }
//        
//        public function actionAjaxDeleteTindakanPelayanan()
//        {
//            if(Yii::app()->request->isAjaxRequest) {
//            $idTindakanpelayanan = $_POST['idTindakanpelayanan'];
//            $transaction = Yii::app()->db->beginTransaction();
//            try {
//                $deleteTindakan = RMTindakanPelayananT::model()->deleteByPk($idTindakanpelayanan);
//                $deleteTindakanKomponen = TindakankomponenT::model()->deleteAllByAttributes(array('tindakanpelayanan_id'=>$idTindakanpelayanan));
//                $obatAlkesT = RMObatalkesPasienT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$idTindakanpelayanan));
//                $deleteObatPasien = RMObatalkesPasienT::model()->deleteAllByAttributes(array('tindakanpelayanan_id'=>$idTindakanpelayanan));
//                $this->kembalikanStok($obatAlkesT);
//                if($deleteTindakan && $deleteTindakanKomponen){
//                    $data['success'] = true;
//                    $transaction->commit();
//                }else{
//                    $data['success'] = false;
//                    $transaction->rollback();
//                }
//            } catch (Exception $exc) {
//                $transaction->rollback();
//                echo MyExceptionMessage::getMessage($exc,true);
//                $data['success'] = false;
//            }
//
//            
//
//            echo json_encode($data);
//             Yii::app()->end();
//            }
//        }
//        
//        protected function savePaketBmhp($modPendaftaran,$paketBmhp,$tindakan)
//        {
//            $valid = true;
//            foreach ($paketBmhp as $i => $bmhp) {
//                if($tindakan->daftartindakan_id == $bmhp['daftartindakan_id']){
//                    $modObatPasien[$i] = new RMObatalkesPasienT;
//                    $modObatPasien[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//                    $modObatPasien[$i]->penjamin_id = $modPendaftaran->penjamin_id;
//                    $modObatPasien[$i]->carabayar_id = $modPendaftaran->carabayar_id;
//                    $modObatPasien[$i]->daftartindakan_id = $bmhp['daftartindakan_id'];
//                    $modObatPasien[$i]->sumberdana_id = $bmhp['sumberdana_id'];
//                    $modObatPasien[$i]->pasien_id = $modPendaftaran->pasien_id;
//                    $modObatPasien[$i]->satuankecil_id = $bmhp['satuankecil_id'];
//                    $modObatPasien[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
//                    $modObatPasien[$i]->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
//                    $modObatPasien[$i]->tipepaket_id = $tindakan->tipepaket_id;
//                    $modObatPasien[$i]->obatalkes_id = $bmhp['obatalkes_id'];
//                    $modObatPasien[$i]->pegawai_id = $modPendaftaran->pegawai_id;
//                    $modObatPasien[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//                    $modObatPasien[$i]->shift_id = Yii::app()->user->getState('shift_id');
//                    $modObatPasien[$i]->tglpelayanan = date('Y-m-d H:i:s');
//                    $modObatPasien[$i]->qty_oa = $bmhp['qtypemakaian'];
//                    $modObatPasien[$i]->hargajual_oa = $bmhp['hargapemakaian'];
//                    $modObatPasien[$i]->harganetto_oa = $bmhp['harganetto'];
//                    $modObatPasien[$i]->hargasatuan_oa = $bmhp['hargasatuan'];
//
//                    $valid = $modObatPasien[$i]->validate() && $valid;
//                    if($valid) {
//                        $modObatPasien[$i]->save();
//                        $this->kurangiStok($modObatPasien[$i]->qty_oa, $modObatPasien[$i]->obatalkes_id);
//                        $this->successSaveBmhp = true;
//                    } else {
//                        $this->successSaveBmhp = false;
//                    }
//                }
//            }
//            
//            return $modObatPasien;
//        }
//        
//        protected function savePemakaianBahan($modPendaftaran,$pemakaianBahan,$tindakan)
//        {
//            $valid = true;
//            foreach ($pemakaianBahan as $i => $bmhp) {
//                if($tindakan->daftartindakan_id == $bmhp['daftartindakan_id']){
//                    $modPakaiBahan[$i] = new RMObatalkesPasienT;
//                    $modPakaiBahan[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//                    $modPakaiBahan[$i]->penjamin_id = $modPendaftaran->penjamin_id;
//                    $modPakaiBahan[$i]->carabayar_id = $modPendaftaran->carabayar_id;
//                    $modPakaiBahan[$i]->daftartindakan_id = $bmhp['daftartindakan_id'];
//                    $modPakaiBahan[$i]->sumberdana_id = $bmhp['sumberdana_id'];
//                    $modPakaiBahan[$i]->pasien_id = $modPendaftaran->pasien_id;
//                    $modPakaiBahan[$i]->satuankecil_id = $bmhp['satuankecil_id'];
//                    $modPakaiBahan[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
//                    $modPakaiBahan[$i]->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
//                    $modPakaiBahan[$i]->tipepaket_id = $tindakan->tipepaket_id;
//                    $modPakaiBahan[$i]->obatalkes_id = $bmhp['obatalkes_id'];
//                    $modPakaiBahan[$i]->pegawai_id = $modPendaftaran->pegawai_id;
//                    $modPakaiBahan[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//                    $modPakaiBahan[$i]->shift_id = Yii::app()->user->getState('shift_id');
//                    $modPakaiBahan[$i]->tglpelayanan = date('Y-m-d H:i:s');
//                    $modPakaiBahan[$i]->qty_oa = $bmhp['qty'];
//                    $modPakaiBahan[$i]->hargajual_oa = $bmhp['subtotal'];
//                    $modPakaiBahan[$i]->harganetto_oa = $bmhp['harganetto'];
//                    $modPakaiBahan[$i]->hargasatuan_oa = $bmhp['hargasatuan'];
//
//                    $valid = $modPakaiBahan[$i]->validate() && $valid;
//                    if($valid) {
//                        $modPakaiBahan[$i]->save();
//                        $this->kurangiStok($modPakaiBahan[$i]->qty_oa, $modPakaiBahan[$i]->obatalkes_id);
//                        $this->successSavePemakaianBahan = true;
//                    } else {
//                        $this->successSavePemakaianBahan = false;
//                    }
//                }
//            }
//            
//            return $modPakaiBahan;
//        }
//
//        private function traceObatAlkesPasien($modObatPasiens)
//        {
//            foreach ($modObatPasiens as $key => $modObatPasien) {
//                $echo .= "<pre>".print_r($modObatPasien->attributes,1)."</pre>";
//            }
//            return $echo;
//        }
//
//        /**
//         * 
//         * @param ObatalkespasienT $modObatPasien 
//         */
//        protected function saveObatAlkesKomponen($modObatPasien)
//        {
//            $modObatPasien = new ObatalkespasienT;
//            $obat = ObatalkesM::model()->findByPk($modObatPasien->obatalkes_id);
//            $obat = new ObatalkesM;
//            $modObatPasienKomponen = new ObatalkeskomponenT;
//            $modObatPasienKomponen->obatalkespasien_id = $modObatPasien->obatalkespasien_id;
//            $modObatPasienKomponen->hargajualkomponen = $obat->hargajual;
//            $modObatPasienKomponen->harganettokomponen = $obat->harganetto;
//            $modObatPasienKomponen->hargasatuankomponen = $obat->hargajual;
//            $modObatPasienKomponen->iurbiaya = 0;
//            $modObatPasienKomponen->komponentarif_id = null;
//            
//        }
//        
//        protected function kurangiStok($qty,$idobatAlkes)
//        {
//            $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $idobatAlkes ORDER BY tglstok_in";
//            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
//            $selesai = false;
////            while(!$selesai){
//                foreach ($stoks as $i => $stok) {
//                    if($qty <= $stok['qtystok_current']) {
//                        $stok_current = $stok['qtystok_current'] - $qty;
//                        $stok_out = $stok['qtystok_out'] + $qty;
//                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_current'=>$stok_current,'qtystok_out'=>$stok_out));
//                        $selesai = true;
//                        break;
//                    } else {
//                        $qty = $qty - $stok['qtystok_current'];
//                        $stok_current = 0;
//                        $stok_out = $stok['qtystok_out'] + $stok['qtystok_current'];
//                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('stok_current'=>$stok_current,'qtystok_out'=>$stok_out));
//                    }
//                }
////            }
//        }
//        
//        protected function kembalikanStok($obatAlkesT)
//        {
//            foreach ($obatAlkesT as $i => $obatAlkes) {
//                $stok = new RMStokObatalkesT;
//                $stok->obatalkes_id = $obatAlkes->obatalkes_id;
//                $stok->sumberdana_id = $obatAlkes->sumberdana_id;
//                $stok->ruangan_id = Yii::app()->user->getState('ruangan_id');
//                $stok->tglstok_in = date('Y-m-d H:i:s');
//                $stok->tglstok_out = date('Y-m-d H:i:s');
//                $stok->qtystok_in = $obatAlkes->qty_oa;
//                $stok->qtystok_out = 0;
//                $stok->qtystok_current = $obatAlkes->qty_oa;
//                $stok->harganetto_oa = $obatAlkes->harganetto_oa;
//                $stok->hargajual_oa = $obatAlkes->hargasatuan_oa;
//                $stok->discount = $obatAlkes->discount;
//                $stok->satuankecil_id = $obatAlkes->satuankecil_id;
//                $stok->save();
//            }
//        }
//        
//        private function cekKomponenTarifTindakan($idKomponenTarifTindakan)
//        {
//            switch ($idTarifTindakan) {
//                case Params::KOMPONENTARIF_ID_RS:return 'tarif_rsakomodasi';
//                case Params::KOMPONENTARIF_ID_MEDIS:return 'tarif_medis';
//                case Params::KOMPONENTARIF_ID_PARAMEDIS:return 'tarif_paramedis';
//                case Params::KOMPONENTARIF_ID_BHP:return 'tarif_bhp';
//
//                default:return null;
//                    break;
//            }
//        }
//
////        public function actionUpdateStok()
////        {
////            $qty = $_POST['qty'];
////            $idobatAlkes = $_POST['idObatAlkes'];
////            $sql = "select stokobatalkes_id,qtystok_in,qtystok_out from stokobatalkes_t order by tglstok_in";
////            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
////            $selesai = false;
////            while(!$selesai){
////                foreach ($stoks as $i => $stok) {
////                    if($qty <= $stok['qtystok_in']) {
////                        $stok_in = $stok['qtystok_in'] - $qty;
////                        $stok_out = $stok['qtystok_out'] + $qty;
////                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_in'=>$stok_in,'qtystok_out'=>$stok_out));
////                        $selesai = true;
////                        break;
////                    } else {
////                        $qty = $qty - $stok['qtystok_in'];
////                        $stok_in = 0;
////                        $stok_out = $stok['qtystok_out'] + $stok['qtystok_in'];
////                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_in'=>$stok_in,'qtystok_out'=>$stok_out));
////                    }
////                }
////            }
////            $data['input'] = 'qty: '.$qty.' | ID Obat: '.$idobatAlkes;
////            echo CJSON::encode($data);
////            Yii::app()->end();
////        }
//
//
//        // Uncomment the following methods and override them if needed
//	/*
//	public function filters()
//	{
//		// return the filter configuration for this controller, e.g.:
//		return array(
//			'inlineFilterName',
//			array(
//				'class'=>'path.to.FilterClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//
//	public function actions()
//	{
//		// return external action classes, e.g.:
//		return array(
//			'action1'=>'path.to.ActionClass',
//			'action2'=>array(
//				'class'=>'path.to.AnotherActionClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//	*/
//}