<?php
Yii::import('rawatJalan.controllers.TindakanController');
class RujukanPenunjangController extends SBaseController
{
        /**
	 * @return array action filters
	 */
    
        public $successSave = false;
         
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','masukPenunjang'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
        
	public function actionIndex()
	{
            $this->pageTitle = Yii::app()->name." - Pasien Rujukan";
            $criteria = new CDbCriteria;
            if(isset($_GET['ajax']) && $_GET['ajax']=='pasienpenunjangrujukan-m-grid') {
                $format = new CustomFormat;
                echo $format->formatDateTimeMediumForDB($_GET['tglAkhir']);
                $criteria->compare('LOWER(no_pendaftaran)', strtolower($_GET['noPendaftaran']),true);
                $criteria->compare('LOWER(nama_pasien)', strtolower($_GET['namaPasien']),true);
                $criteria->compare('LOWER(no_rekam_medik)', strtolower($_GET['noRekamMedik']),true);
                if($_GET['cbTglMasuk'])
                    $criteria->addBetweenCondition('tgl_kirimpasien', "'".$format->formatDateTimeMediumForDB($_GET['tglAwal'])."'", "'".$format->formatDateTimeMediumForDB($_GET['tglAkhir'])."'");
            } else {
                //$criteria->addBetweenCondition('tgl_pendaftaran', date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59');
            }
            $criteria->compare('instalasi_id', Params::INSTALASI_ID_REHAB); //NANTI DIGANTI AMA SESSION, SEMENTARA PAKE PARAM DL
            
            $dataProvider = new CActiveDataProvider(PasienkirimkeunitlainV::model(), array(
			'criteria'=>$criteria,
		));
            $this->render('index',array('dataProvider'=>$dataProvider));
	}
        
        public function actionMasukPenunjang($idPasienKirimKeUnitLain,$idPendaftaran)
        {            
            $this->pageTitle = Yii::app()->name." - Pemeriksaaan Rehab Medis";
            $modPendaftaran = RMPendaftaranMp::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = RMPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modPermintaan = RMPermintaanKePenunjangT::model()->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            $modJenisTindakan = RMJenisTindakanrmM::model()->findAllByAttributes(array('jenistindakanrm_aktif'=>true),array('order'=>'jenistindakanrm_nama'));
            $modTindakan = RMTindakanrmM::model()->findAllByAttributes(array('tindakanrm_aktif'=>true),array('order'=>'tindakanrm_nama'));
            $modHasilPemeriksaan = new HasilpemeriksaanrmT;
            $modPenunjang = new RMMasukPenunjangV;
            $modPenunjangSave = new RMPasienMasukPenunjangT;
            $modTindakanPelayanan = new RMTindakanPelayananT;
            $modTindakanKomponen = new RMTindakanKomponenT;
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            if(isset($_POST['RMTindakanPelayananT'])) {
                
                if(!empty($_POST['tindakanrm_id']))
                {
                    $attrTindakan = $_POST['tindakan_id'];
                    
                    
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $modPenunjangSave = $this->savePasienPenunjang($modPendaftaran);
                        
                        $modHasilPemeriksaan = $this->saveHasilPemeriksaan($modPendaftaran,$modPenunjangSave,$attrTindakan,$_POST['RMTindakanPelayananT'],$_POST['RekeningakuntansiV']);
                        
                        if ($this->successSave){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    } catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }
                else{
                    Yii::app()->user->setFlash('error',"Data gagal disimpan, anda belum memilih operasi");
                }
            }
            
            
            $modRiwayatKirimKeUnitLain = RMPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                    'ruangan_id'=>Params::RUANGAN_ID_REHAB,),
                                                                                                    'pasienmasukpenunjang_id IS NULL');
            $modRiwayatPenunjang = RMPasienMasukPenunjangT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                             'ruangan_id'=>Params::RUANGAN_ID_REHAB,));
            
            $this->render('masukPenunjang',array('modPermintaan'=>$modPermintaan,
                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
                                        'modJenisTindakan'=>$modJenisTindakan,
                                        'modTindakan'=>$modTindakan,
                                        'modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modRiwayatPenunjang'=>$modRiwayatPenunjang,
                                        'modHasilPemeriksaan'=>$modHasilPemeriksaan,
                                        'modPenunjang'=>$modPenunjang,
                                        'modPenunjangSave'=>$modPenunjangSave,
                                        'modTindakanPelayanan'=>$modTindakanPelayanan,
                                        'modTindakanKomponen'=>$modTindakanKomponen
                          ));
        }
        
        /**
         * Fungsi untuk menyimpan data ke model RMPasienMasukPenunjangT
         * @param type $modPendaftaran
         * @param type $modPasien
         * @return ROPasienMasukPenunjangT 
         */
        protected function savePasienPenunjang($modPendaftaran){
            
            $modPasienPenunjang = new RMPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $modPendaftaran->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $modPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_REHAB;   //$modPendaftaran->ruangan_id;
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang('RM');
            $modPasienPenunjang->tglmasukpenunjang = date('Y-m-d H:i:s');    //$modPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $modPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $modPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $modPendaftaran->ruangan_id;
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
                $this->updatePasienKirimKeUnitLain($modPasienPenunjang);
            } else {
                $this->successSave = false;
            }
            
            return $modPasienPenunjang;
        }
        
        public function saveHasilPemeriksaan($attrPendaftaran,$attrPenunjang,$attrTindakan,$attrTindakanPelayanan,$postRekenings = array())
         {
            $arrSave = array();
            $validTindakan = 'true';
            $arrTindakan = array(); // array untuk menampung tindakan yg nantinnya digunakan pada proses saveTindakanPelayanan
            for ($i = 0; $i < count($attrTindakan); $i++) {
                    
                        $modHasil = new HasilpemeriksaanrmT;
                        $modHasil->nohasilrm = Generator::noHasilPemeriksaanRM();
                        $modHasil->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                        $modHasil->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                        $modHasil->pasien_id = $attrPenunjang->pasien_id;
                        $modHasil->ruangan_id = $attrPenunjang->ruangan_id;
                        $modHasil->pegawai_id = $attrPenunjang->pegawai_id;
                        $modHasil->tglpemeriksaanrm = date('Y-m-d H:i:s');
                        $modHasil->kunjunganke = 1; //di default untuk kunjungan pertama
                        
                        $modHasil->tindakanrm_id = $attrTindakan[$i];
                        $modHasil->jenistindakanrm_id = RMTindakanrmM::model()->findByPk($attrTindakan[$i])->jenistindakanrm_id;
                        $arrTindakan[$i]=array(
                                            'tindakan'=> $attrTindakan[$i]
                                        );
                        $modHasil->create_time=date('Y-m-d H:i:s');
                        $modHasil->create_loginpemakai_id=Yii::app()->user->id;
                        $modHasil->create_ruangan=Yii::app()->user->getState('ruangan_id');
                        
                        
                        if ($modHasil->validate()){
                            $arrSave[$i] = $modHasil; // menyimpan objek BSRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                            $validTindakan = 'true'; // variabel untuk menentukan rencana operasi valid

                        }else{
                            $modHasil->tglpemeriksaanrm = Yii::app()->dateFormatter->formatDateTime(
                                    CDateTimeParser::parse($modHasil->tglpemeriksaanrm, 'yyyy-MM-dd'), 'medium', null);

                            $validTindakan = $validTindakan.'false';
                        }
                } //ENDING FOR 
                if($validTindakan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
                {
                    foreach ($arrTindakan as $x => $hasilTindakan) {
                        $tindakanNya[$x] = $hasilTindakan['tindakan'];
                    }
                    
                    foreach ($arrSave as $f => $simpan) {
                        
                        $simpan->save();
                        $this->saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$simpan,$attrTindakanPelayanan,$f,$postRekenings);
                    }
                    $this->successSave = true;
                }
                else
                {
                    $this->successSave = false;
                }
            return $modHasil;
        }
        
        public function saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$attrHasil,$attrTindakanPelayanan,$index,$postRekenings=array())
        {
            $validTindakanPelayanan = 'true';
            $arrSave = array();
            
            $daftar_tindakan = $attrTindakanPelayanan['daftartindakan_id'][$index];
            
            $kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
            
            $tarifTotal = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                
            $tarifRS = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_RS));

            $tarifBHP = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_BHP));

            $tarifParamedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_PARAMEDIS));

            $tarifMedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_MEDIS));
            
            
                $modTindakanPelayanan = new RMTindakanPelayananT;
                $modTindakanPelayanan->penjamin_id = $attrPendaftaran->penjamin_id;
                $modTindakanPelayanan->pasien_id = $attrPenunjang->pasien_id;
                $modTindakanPelayanan->kelaspelayanan_id = $kelaspelayanan_id;
                $modTindakanPelayanan->hasilpemeriksaanrm_id = $attrHasil->hasilpemeriksaanrm_id ;
                $modTindakanPelayanan->tipepaket_id = 1;
                $modTindakanPelayanan->instalasi_id = Params::INSTALASI_ID_REHAB;
                $modTindakanPelayanan->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                $modTindakanPelayanan->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayanan->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                $modTindakanPelayanan->daftartindakan_id = $daftar_tindakan;
                $modTindakanPelayanan->carabayar_id = $attrPendaftaran->carabayar_id;
                $modTindakanPelayanan->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayanan->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayanan->tarif_tindakan = $attrTindakanPelayanan['tarif_tindakan'][$index];
                $modTindakanPelayanan->tarif_satuan = $modTindakanPelayanan->tarif_tindakan;
                $modTindakanPelayanan->tarif_rsakomodasi = (!empty($tarifRS)) ? $tarifRS->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_medis = (!empty($tarifMedis)) ? $tarifMedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_paramedis = (!empty($tarifParamedis)) ? $tarifParamedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_bhp = (!empty($tarifBHP)) ? $tarifBHP->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->satuantindakan = $attrTindakanPelayanan['satuantindakan'][$index];
                $modTindakanPelayanan->qty_tindakan = $attrTindakanPelayanan['qty_tindakan'][$index];
                $modTindakanPelayanan->cyto_tindakan = $attrTindakanPelayanan['cyto_tindakan'][$index];
                
                if($modTindakanPelayanan->cyto_tindakan){
                    $modTindakanPelayanan->tarifcyto_tindakan = $modTindakanPelayanan->tarif_tindakan * ($attrTindakanPelayanan['persencyto_tind'][$index]/100);
                } else {
                    $modTindakanPelayanan->tarifcyto_tindakan = 0;
                }
                $modTindakanPelayanan->discount_tindakan = 0;
                $modTindakanPelayanan->subsidiasuransi_tindakan=0;
                $modTindakanPelayanan->subsidipemerintah_tindakan=0;
                $modTindakanPelayanan->subsisidirumahsakit_tindakan=0;
                $modTindakanPelayanan->iurbiaya_tindakan=0;
                $modTindakanPelayanan->ruangan_id =  Yii::app()->user->getState('ruangan_id');
                
                if ($modTindakanPelayanan->validate()){
                    $arrSave[$i] = $modTindakanPelayanan; // menyimpan objek RMRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                    $validTindakanPelayanan = 'true';

                }else
                {   
                    $validTindakanPelayanan = $validTindakanPelayanan.'false';
                }
//            } //ENDING FOR
            if($validTindakanPelayanan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $x => $simpan) {
                    $simpan->save();
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $simpan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $simpan->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $simpan, 'tm');
//                        $this->succesSave = $saveDetailJurnal;
                    }
                    $this->saveTindakanKomponenT($simpan);
                    $this->upadateHasilTindakan($simpan);
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }

            return $modTindakanPelayanan;
        }
        
        public function saveTindakanKomponenT($attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakanKomponen = 'true';
            $daftarTindakan_id = $attrTindakanPelayanan->daftartindakan_id;
            $kelaspelayanan_id = $attrTindakanPelayanan->kelaspelayanan_id;
            $arrTarifTindakan = "select * from tariftindakan_m where daftartindakan_id = ".$daftarTindakan_id." and kelaspelayanan_id = ".$kelaspelayanan_id." and komponentarif_id <> ".Params::KOMPONENTARIF_ID_TOTAL." ";
            $query = Yii::app()->db->createCommand($arrTarifTindakan)->queryAll();
            foreach ($query as $i => $tarifKomponen) {
                $modTarifKomponen = new RMTindakanKomponenT;
                $modTarifKomponen->tindakanpelayanan_id = $attrTindakanPelayanan->tindakanpelayanan_id;
                $modTarifKomponen->komponentarif_id = $tarifKomponen['komponentarif_id'];
                $modTarifKomponen->tarif_tindakankomp = $tarifKomponen['harga_tariftindakan'];
                $modTarifKomponen->tarif_kompsatuan = $modTarifKomponen->tarif_tindakankomp;
                if($attrTindakanPelayanan->cyto_tindakan){
                    $modTarifKomponen->tarifcyto_tindakankomp = $tarifKomponen['harga_tariftindakan'] * ($tarifKomponen['persencyto_tind']/100);
                } else {
                    $modTarifKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTarifKomponen->subsidiasuransikomp = 0;
                $modTarifKomponen->subsidipemerintahkomp = 0;
                $modTarifKomponen->subsidirumahsakitkomp = 0;
                $modTarifKomponen->iurbiayakomp = 0;
                if ($modTarifKomponen->validate()){
                    $arrSave[$i] = $modTarifKomponen; // menyimpan objek tarif komponen ke dalam sebuah array dan siap untuk disave
                    $validTindakanKomponen = 'true';

                }else
                {
                    $validTindakanKomponen = $validTindakanKomponen.'false';
                }
            } // ending foreach
            if($validTindakanKomponen == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $f => $simpan) {
                    $simpan->save();
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }
            return $modTarifKomponen;
        }
        
        /**
         * Fungsi untuk mengupadte hasil pemeriksaan rehab medis menset tindakanpelayanan id
         * @param type $modTindPelayanan model object
         */
        protected function upadateHasilTindakan($modTindPelayanan)
        {
            $modHasil = $this->loadById($modTindPelayanan->hasilpemeriksaanrm_id);
            $modHasil->tindakanpelayanan_id = $modTindPelayanan->tindakanpelayanan_id;
            $modHasil->save();
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dengan method findByPk yang nanti digunakan untuk menyimpan data-data hasil pemeriksaan
         * @param type $id
         * @return type 
         */
        protected function loadById($id)
        {       $model= HasilpemeriksaanrmT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        
        protected function updatePasienKirimKeUnitLain($modPasienPenunjang) {
            
            if(!empty($_POST['permintaanPenunjang'])){
                foreach($_POST['permintaanPenunjang'] as $i => $item) {
                    PasienkirimkeunitlainT::model()->updateByPk($item['idPasienKirimKeUnitLain'], 
                                                                array('pasienmasukpenunjang_id'=>$modPasienPenunjang->pasienmasukpenunjang_id));
                }
            }
        }
        
        

	
}