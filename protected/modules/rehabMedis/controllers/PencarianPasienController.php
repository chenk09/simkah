<?php

class PencarianPasienController extends SBaseController
{
	
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionIndex()
        {
                $format = new CustomFormat();
                $modRJ = new RMInfoKunjunganRJV;
                $modRI = new RMInfoKunjunganRIV;
                $modRD = new RMInfoKunjunganRDV;
                
                $cekRJ = true;
                $cekRI = false;
                $cekRD = false;
                
                $modRJ->tglAwal = date("d M Y").' 00:00:00';
                $modRJ->tglAkhir =date('d M Y H:i:s');
                
                $modRI->tglAwal = date("d M Y").' 00:00:00';
                $modRI->tglAkhir = date('d M Y H:i:s');
                
                $modRD->tglAwal = date("d M Y").' 00:00:00';
                $modRD->tglAkhir =date('d M Y H:i:s');
       
                
                if(isset ($_POST['instalasi']))
                {
                    switch ($_POST['instalasi']) {
                        
                        case 'RJ':
                            $cekRJ = true;
                            $cekRI = false;
                            $cekRD = false;
                            $modRJ->attributes = $_POST['RMInfoKunjunganRJV'];
                            if(!empty($_POST['RMInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMInfoKunjunganRJV']['tglAwal']);
                            }
                            if(!empty($_POST['RMInfoKunjunganRJV']['tglAkhir']))
                            {
                                $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMInfoKunjunganRJV']['tglAkhir']);
                            }
                            
                            break;
                            
                        case 'RI':
                            $cekRI = true;
                            $cekRJ = false;
                            $cekRD = false;
                            $modRI->attributes = $_POST['RMInfoKunjunganRIV'];
                            if(!empty($_POST['RMInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMInfoKunjunganRIV']['tglAwal']);
                            }
                            if(!empty($_POST['RMInfoKunjunganRIV']['tglAkhir']))
                            {
                                $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMInfoKunjunganRIV']['tglAkhir']);
                            }
                            break;
                            
                        case 'RD':
                            $cekRD = true;
                            $cekRI = false;
                            $cekRJ = false;
                            $modRD->attributes = $_POST['RMInfoKunjunganRDV'];
                            if(!empty($_POST['RMInfoKunjunganRDV']['tglAwal']))
                            {
                                $modRD->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMInfoKunjunganRDV']['tglAwal']);
                            }
                            if(!empty($_POST['RMInfoKunjunganRDV']['tglAkhir']))
                            {
                                $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMInfoKunjunganRDV']['tglAkhir']);
                            }
                           
                            break;
                    }
                }
                
                $this->render('index',array(
                                 'modRJ'=>$modRJ,
                                 'modRI'=>$modRI,
                                 'modRD'=>$modRD,
                                 'cekRJ'=>$cekRJ,
                                 'cekRI'=>$cekRI,
                                 'cekRD'=>$cekRD,
                                 
                ));
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}