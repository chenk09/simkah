<?php
Yii::import('rawatJalan.controllers.PemakaianBahanController');
Yii::import('rawatJalan.models.*');
class PemakaianBahanTRMController extends PemakaianBahanController
{
        
}
//class PemakaianBahanController extends SBaseController
//{
//    protected $successSavePemakaianBahan = true;
//    
//	public function actionIndex($idPendaftaran)
//	{
//            $modPendaftaran = RMPendaftaranMp::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
//            $modPasien = RMPasienM::model()->findByPk($modPendaftaran->pasien_id);
//		
//            if(isset($_POST['pemakaianBahan'])){
//                $transaction = Yii::app()->db->beginTransaction();
//                try {
//                    $modPemakainBahans = $this->savePemakaianBahan($modPendaftaran, $_POST['pemakaianBahan']);
//                    if($this->successSavePemakaianBahan) {
//                        $transaction->commit();
//                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
//                    } else {
//                        $transaction->rollback();
//                        //Yii::app()->user->setFlash('error',"Data tidak valid ");
//                        Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceObatAlkesPasien($modPemakainBahans));
//                    }
//                } catch (Exception $exc) {
//                    $transaction->rollback();
//                    Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
//                }
//            }
//            
//            $modViewBmhp = RMObatalkesPasienT::model()->with('obatalkes')->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
//            
//            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
//                                        'modPasien'=>$modPasien,
//                                        'modViewBmhp'=>$modViewBmhp,
//                                    ));
//	}
//        
//        protected function savePemakaianBahan($modPendaftaran,$pemakaianBahan)
//        {
//            $valid = true;
//            foreach ($pemakaianBahan as $i => $bmhp) {
//                $modPakaiBahan[$i] = new RMObatalkesPasienT;
//                $modPakaiBahan[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//                $modPakaiBahan[$i]->penjamin_id = $modPendaftaran->penjamin_id;
//                $modPakaiBahan[$i]->carabayar_id = $modPendaftaran->carabayar_id;
//                $modPakaiBahan[$i]->daftartindakan_id = $bmhp['daftartindakan_id'];
//                $modPakaiBahan[$i]->sumberdana_id = $bmhp['sumberdana_id'];
//                $modPakaiBahan[$i]->pasien_id = $modPendaftaran->pasien_id;
//                $modPakaiBahan[$i]->satuankecil_id = $bmhp['satuankecil_id'];
//                $modPakaiBahan[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
//                $modPakaiBahan[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
//                $modPakaiBahan[$i]->obatalkes_id = $bmhp['obatalkes_id'];
//                $modPakaiBahan[$i]->pegawai_id = $modPendaftaran->pegawai_id;
//                $modPakaiBahan[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//                $modPakaiBahan[$i]->shift_id = Yii::app()->user->getState('shift_id');
//                $modPakaiBahan[$i]->tglpelayanan = date('Y-m-d H:i:s');
//                $modPakaiBahan[$i]->qty_oa = $bmhp['qty'];
//                $modPakaiBahan[$i]->hargajual_oa = $bmhp['subtotal'];
//                $modPakaiBahan[$i]->harganetto_oa = $bmhp['harganetto'];
//                $modPakaiBahan[$i]->hargasatuan_oa = $bmhp['hargasatuan'];
//
//                $valid = $modPakaiBahan[$i]->validate() && $valid;
//                if($valid) {
//                    $modPakaiBahan[$i]->save();
//                    $this->kurangiStok($modPakaiBahan[$i]->qty_oa, $modPakaiBahan[$i]->obatalkes_id);
//                    $this->successSavePemakaianBahan = true;
//                } else {
//                    $this->successSavePemakaianBahan = false;
//                }
//            }
//            
//            return $modPakaiBahan;
//        }
//
//        private function traceObatAlkesPasien($modObatPasiens)
//        {
//            foreach ($modObatPasiens as $key => $modObatPasien) {
//                $echo .= "<pre>".print_r($modObatPasien->attributes,1)."</pre>";
//            }
//            return $echo;
//        }
//        
//        protected function kurangiStok($qty,$idobatAlkes)
//        {
//            $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $idobatAlkes ORDER BY tglstok_in";
//            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
//            $selesai = false;
//                foreach ($stoks as $i => $stok) {
//                    if($qty <= $stok['qtystok_current']) {
//                        $stok_current = $stok['qtystok_current'] - $qty;
//                        $stok_out = $stok['qtystok_out'] + $qty;
//                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_current'=>$stok_current,'qtystok_out'=>$stok_out));
//                        $selesai = true;
//                        break;
//                    } else {
//                        $qty = $qty - $stok['qtystok_current'];
//                        $stok_current = 0;
//                        $stok_out = $stok['qtystok_out'] + $stok['qtystok_current'];
//                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('stok_current'=>$stok_current,'qtystok_out'=>$stok_out));
//                    }
//                }
//        }
//        
//        protected function kembalikanStok($obatAlkesT)
//        {
//            foreach ($obatAlkesT as $i => $obatAlkes) {
//                $stok = new RMStokObatalkesT;
//                $stok->obatalkes_id = $obatAlkes->obatalkes_id;
//                $stok->sumberdana_id = $obatAlkes->sumberdana_id;
//                $stok->ruangan_id = Yii::app()->user->getState('ruangan_id');
//                $stok->tglstok_in = date('Y-m-d H:i:s');
//                $stok->tglstok_out = date('Y-m-d H:i:s');
//                $stok->qtystok_in = $obatAlkes->qty_oa;
//                $stok->qtystok_out = 0;
//                $stok->harganetto_oa = $obatAlkes->harganetto_oa;
//                $stok->hargajual_oa = $obatAlkes->hargasatuan_oa;
//                $stok->discount = $obatAlkes->discount;
//                $stok->satuankecil_id = $obatAlkes->satuankecil_id;
//                $stok->save();
//            }
//        }
//
//	// Uncomment the following methods and override them if needed
//	/*
//	public function filters()
//	{
//		// return the filter configuration for this controller, e.g.:
//		return array(
//			'inlineFilterName',
//			array(
//				'class'=>'path.to.FilterClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//
//	public function actions()
//	{
//		// return external action classes, e.g.:
//		return array(
//			'action1'=>'path.to.ActionClass',
//			'action2'=>array(
//				'class'=>'path.to.AnotherActionClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//	*/
//}