<?php

class PendaftaranRehabMedisController extends SBaseController
{
	 public $successSave = false;
         public $successSavePJ = true; //variabel untuk validasi data opsional (penanggung jawab)
        
        /**
	 * @return array action filters
	 */ 
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','getOperasi'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex($id=NULL)
	{
		
            $format = new CustomFormat;
            $this->pageTitle = Yii::app()->name." - Pendaftaran Pasien Luar Rehab Medis";
            $model = new RMPendaftaranMp;
            $modPasienAdmisi = new PasienadmisiT; //hanya untuk mengambil kamarruangan_m
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->umur = "00 Thn 00 Bln 00 Hr";
            $model->ruangan_id = Params::RUANGAN_ID_REHAB;
            $model->kelaspelayanan_id = Params::kelasPelayanan('bedah_sentral');
            $modPasien = new RMPasienM;
            $modPasien->tanggal_lahir = date('d M Y');
            $modPenanggungJawab = new RMPenanggungJawabM;
            $modRujukan = new RMRujukanT;
            $modRujukan->tanggal_rujukan = date('d M Y H:i:s');
            $modPasienPenunjang = new RMPasienMasukPenunjangT;
            $modJenisTindakan = RMJenisTindakanrmM::model()->findAllByAttributes(array('jenistindakanrm_aktif'=>true),array('order'=>'jenistindakanrm_nama'));
            $modTindakan = RMTindakanrmM::model()->findAllByAttributes(array('tindakanrm_aktif'=>true),array('order'=>'tindakanrm_nama'));
            $modHasilPemeriksaan = new HasilpemeriksaanrmT;
            
            if(!empty($id)){
                $model = RMPendaftaranMp::model()->findByPk($id);
                $modPasien = RMPasienM::model()->findByPk($model->pasien_id);
                if(!empty($model->penanggungjawab_id)){
                    $modPenanggungJawab = RMPenanggungJawabM::model()->findByPk($model->penanggungjawab_id);    
                }
                if(!empty($model->rujukan_id))
                    $modRujukan = RMRujukanT::model()->findByPk($model->rujukan_id);
                
                // $modCaraBayar = CarabayarM::model()->findByPk($model->carabayar_id);
                
                $model->noRekamMedik = $modPasien->no_rekam_medik;
                $model->ruangan_id = $model->ruangan_id;
                $model->pegawai_id = $model->pegawai_id;
            }

            if (isset($_POST['RMPendaftaranMp'])){
                
                $model->attributes = $_POST['RMPendaftaranMp'];
                $modPasien->attributes = $_POST['RMPasienM'];
                $modPenanggungJawab->attributes = $_POST['RMPenanggungJawabM'];
                $modRujukan->attributes = $_POST['RMRujukanT'];
                $modRujukan->tanggal_rujukan = $format->formatDateTimeMediumForDB($_POST['RMRujukanT']['tanggal_rujukan']);
                $modRujukan->asalrujukan_id = $_POST['RMRujukanT']['asalrujukan_id'];
                


                if(isset($_POST['tindakanrm_id']))
                {
                    
                    $attrTindakan = $_POST['tindakanrm_id'];
                    $attrCeklis = $_POST['ceklis'];
                }
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
                                    
                    //==Penyimpanan dan Update Pasien===========================  


                    if(!isset($_POST['isPasienLama'])){
                        $modPasien = $this->savePasien($_POST['RMPasienM']);
                    }
                    else{
                        $model->isPasienLama = true;
                        // var_dump($_POST['noRekamMedik']);exit();
                        if (!empty($_POST['noRekamMedik'])){
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = RMPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            if(!empty($modPasien) && isset($_POST['isUpdatePasien'])) {
                                $modPasien = $this->updatePasien($modPasien, $_POST['RMPasienM']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }
                    
                    //==Akhir Penyimpanan dan Update Pasien=====================
                    
                    
                    //===penyimpanan Penanggung Jawab===========================                   
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['RMPenanggungJawabM']);
                    }
                    //===Akhir Penyimpanan Penanggung Jawab=====================                    
                    
                    if(isset ($_POST['pendaftaranAda'])) // kondisi dimana no pendaftaran sudah ada, jadi tidak usah simpan ke pendaftaran_t
                    {
                        $model = $this->loadPendaftaran($_POST['id_pendaftaran']);
                        
                        //==AwalSimpan Psien Masuk Penunjang========================
                        $modPasienPenunjang = $this->savePasienPenunjangJadwal($model,$_POST['JadwalKunjungan']);
                        //Akhir Pasien MAsuk Penunjang
                       
                    }
                    else
                    {
                       $modRujukan = $this->saveRujukan($_POST['RMRujukanT']); //Save Rujukan
                        
                       //==Awal Simpan Pendaftaran=============================

                       $model = $this->savePendaftaranMP($model,$modPasien,$modRujukan,$modPenanggungJawab);
                       //==Akhir Simpan Pendaftaran============================ 
                       
                       //==Awal Simpan Ubah Cara Bayar=============================
                       $this->saveUbahCaraBayar($model);
                       //==Akhir Simpan Ubah Cara Bayar
                       
                       //==AwalSimpan Psien Masuk Penunjang========================
                       $modPasienPenunjang = $this->savePasienPenunjang($model);
                       //Akhir Pasien MAsuk Penunjang
                       
                       //==AwalSimpan Hasil Pemeriksaan========================  
                       if(!empty($_POST['tindakanrm_id']))
                       {
                          $modHasilPemeriksaan = $this->saveHasilPemeriksaan($model,$modPasienPenunjang,$attrTindakan,$attrCeklis,$_POST['RMTindakanPelayananT']);
                       }
                       else
                       {
                          $modHasilPemeriksaan->validate();
                          $this->successSave = false;
                       }
                       //Akhir Hasil Pemeriksaan
                    }
   
                    if (count($modPasien) != 1){ //Jika Pasien Tidak Ada
                        $modPasien = New RMPasienM;
                        $this->successSave = false;
                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
                        Yii::app()->user->setFlash('warning',"Data Pasien tidak ditemukan isi Nomor Rekam Medik dengan benar");
                    }
                   
                    if ($this->successSave && $this->successSavePJ = true){
                        $transaction->commit();
                        $model->isNewRecord = FALSE;
                        $this->redirect(array('index','id'=>$model->pendaftaran_id));

                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");

                    }
                    else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $model->isRujukan = true;
            $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            
            $this->render('index',array(
                'model'=>$model, 
                'modPasien'=>$modPasien, 
                'modPenanggungJawab'=>$modPenanggungJawab,
                'modRujukan'=>$modRujukan,
                'modPasienPenunjang'=>$modPasienPenunjang,
                'modPasienAdmisi'=>$modPasienAdmisi,
                'modTindakanKomponen'=>$modTindakanKomponen,
                'modTindakanPelayanan'=>$modTindakanPelayanan,
                'modJenisTindakan'=>$modJenisTindakan,
                'modTindakan'=>$modTindakan,
                'modHasilPemeriksaan'=>$modHasilPemeriksaan
                
            ));
	}
        
        public function savePasien($attrPasien)
        {
            $modPasien = new RMPasienM;
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modPasien->no_rekam_medik = Generator::noRekamMedikPenunjang(Params::singkatanNoPendaftaranRehabMedis());
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = 'AKTIF';
            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');

            if($modPasien->validate()) {
                // form inputs are valid, do something here
                $modPasien->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            return $modPasien;
        }
        
        public function updatePasien($modPasien,$attrPasien)
        {
            $modPasienupdate = $modPasien;
            $modPasienupdate->attributes = $attrPasien;
            $modPasienupdate->kelompokumur_id = Generator::kelompokUmur($modPasienupdate->tanggal_lahir);
            
            if($modPasienupdate->validate()) {
                // form inputs are valid, do something here
                $modPasienupdate->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasienupdate->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasienupdate->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            
            return $modPasienupdate;
        }
        
        public function saveRujukan($attrRujukan)
        {
            $format = new CustomFormat;
            $modRujukan = new RMRujukanT;
            $modRujukan->attributes = $attrRujukan;
            $modRujukan->asalrujukan_id = $attrRujukan['asalrujukan_id'];
            $modRujukan->tanggal_rujukan = $format->formatDateTimeMediumForDB($attrRujukan['tanggal_rujukan']);

            // echo"<pre>";
            // print_r($modRujukan->attributes);
            // exit();
            
            // var_dump($modRujukan->save());
            if($modRujukan->validate()) {
                // form inputs are valid, do something here
                $modRujukan->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modRujukan->tanggal_rujukan = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modRujukan->tanggal_rujukan, 'yyyy-MM-dd'),'medium',null);
                
            }


            return $modRujukan;
        }
        
        public function savePenanggungJawab($attrPenanggungJawab)
        {
            $modPenanggungJawab = new RMPenanggungJawabM;
            $modPenanggungJawab->attributes = $attrPenanggungJawab;
            if($modPenanggungJawab->validate()) {
                // form inputs are valid, do something here
                $modPenanggungJawab->save();
                $this->successSavePJ = TRUE;
            } else {
                // mengembalikan format tanggal contoh 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                //if($modPenanggungJawab->tgllahir_pj != null)
                //$modPenanggungJawab->tgllahir_pj = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenanggungJawab->tgllahir_pj, 'yyyy-MM-dd'),'medium',null);
                $this->successSavePJ = FALSE;
            }

            return $modPenanggungJawab;
        }
        
         public function savePendaftaranMP($model, $modPasien, $modRujukan, $modPenanggungJawab){
            $modelNew = new RMPendaftaranMp;
            $modelNew->attributes = $model->attributes;
            $format = new CustomFormat();

            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            if (isset($modelNew->ruangan_id)){
                  $inisial_ruangan = 'RE';
            }
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);

            $modelNew->instalasi_id = Generator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
            $modelNew->no_pendaftaran = Generator::noPendaftaran(Params::singkatanNoPendaftaranRehabMedis());

            $modelNew->no_urutantri = Generator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);

            $modelNew->golonganumur_id = Generator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
            $modelNew->statuspasien = Generator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = Generator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->statusmasuk = Params::statusMasuk('rujukan');
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);


            $modelNew->tgl_pendaftaran = $format->formatDateTimeMediumForDB($modelNew->tgl_pendaftaran);
            // echo"<pre>";
            // print_r($modelNew->attributes);
            // exit();
           
            if ($modelNew->validate()){
                
                $modelNew->Save();
                $this->successSave = true;
            }else{
                $modelNew->tgl_pendaftaran = $format->formatDateTimeMediumForDB($modelNew->tgl_pendaftaran);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
            
            return $modelNew;
        }
        
        public function savePasienPenunjang($attrPendaftaran){
           
                    $modPasienPenunjang = new RMPasienMasukPenunjangT;
                    $modPasienPenunjang->pasien_id = $attrPendaftaran->pasien_id;
                    $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                    $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
                    $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
                    $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
                    $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
                    switch ($modPasienPenunjang->ruangan_id) {
                        case Params::RUANGAN_ID_LAB:$inisial_ruangan = 'LK';break;
                        case Params::RUANGAN_ID_RAD:$inisial_ruangan = 'RO';break;
                        case Params::RUANGAN_ID_REHAB:$inisial_ruangan = 'RE';break;
                        case Params::RUANGAN_ID_IRM:$inisial_ruangan = 'RM';break;
                        default:break;
                    }
                    $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang($inisial_ruangan);
                    $modPasienPenunjang->tglmasukpenunjang = $attrPendaftaran->tgl_pendaftaran;
                    $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
                    $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
                    $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
                    $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;


                    if ($modPasienPenunjang->validate()){
                        $modPasienPenunjang->Save();
                        $this->successSave = true;
                    } else {
                        $this->successSave = false;
                        $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
                    }
                
            
            return $modPasienPenunjang;
        }
        
        public function savePasienPenunjangJadwal($attrPendaftaran,$attrJadwal){
            
            for ($i = 0; $i < count($attrJadwal['ceklis']); $i++) {
                    $patokan = $attrJadwal['ceklis'][$i];
                    $modPasienPenunjang = new RMPasienMasukPenunjangT;
                    $modPasienPenunjang->pasien_id = $attrPendaftaran->pasien_id;
                    $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                    $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
                    $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
                    $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
                    $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
                    switch ($modPasienPenunjang->ruangan_id) {
                        case Params::RUANGAN_ID_LAB:$inisial_ruangan = 'LK';break;
                        case Params::RUANGAN_ID_RAD:$inisial_ruangan = 'RO';break;
                        case Params::RUANGAN_ID_REHAB:$inisial_ruangan = 'RE';break;
                        case Params::RUANGAN_ID_IRM:$inisial_ruangan = 'RM';break;
                        default:break;
                    }
                    $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang($inisial_ruangan);
                    $modPasienPenunjang->tglmasukpenunjang = date('Y-m-d h:i:s');
                    $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
                    $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
                    $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
                    $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
                    
                    
                    if ($modPasienPenunjang->validate()){
                        $modPasienPenunjang->Save();
                        $this->successSave = true;
                        $this->updateJadwalKunjungan($attrJadwal,$attrPendaftaran,$modPasienPenunjang,$patokan);
                    } else {
                        $this->successSave = false;
                        $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
                    }
                
                
            }//ENDING FOR
            
            return $modPasienPenunjang;
        }
        
        public function saveUbahCaraBayar($model) 
        {
            $modUbahCaraBayar = new RMUbahCaraBayarR;
            $modUbahCaraBayar->pendaftaran_id = $model->pendaftaran_id;
            $modUbahCaraBayar->carabayar_id = $model->carabayar_id;
            $modUbahCaraBayar->penjamin_id = $model->penjamin_id;
            $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d');
            $modUbahCaraBayar->alasanperubahan = 'x';
            if($modUbahCaraBayar->validate())
            {
                // form inputs are valid, do something here
                
                $modUbahCaraBayar->save();
            }
            
        }
        
        public function saveHasilPemeriksaan($attrPendaftaran,$attrPenunjang,$attrTindakan,$attrCeklis,$attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakan = 'true';
            $arrTindakan = array(); // array untuk menampung tindakan yg nantinnya digunakan pada proses saveTindakanPelayanan
            for ($i = 0; $i < count($attrCeklis); $i++) {
                    $patokan = $attrCeklis[$i];

                        $modHasil = new HasilpemeriksaanrmT;
                        $modHasil->nohasilrm = Generator::noHasilPemeriksaanRM();
                        $modHasil->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                        $modHasil->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                        $modHasil->pasien_id = $attrPenunjang->pasien_id;
                        $modHasil->ruangan_id = $attrPenunjang->ruangan_id;
                        $modHasil->pegawai_id = $attrPenunjang->pegawai_id;
                        $modHasil->tglpemeriksaanrm = date('Y-m-d H:i:s');
                        $modHasil->kunjunganke = 1; //di default untuk kunjungan pertama
                        
                        $modHasil->tindakanrm_id = $attrTindakan[$patokan];
                        $modHasil->jenistindakanrm_id = RMTindakanrmM::model()->findByPk($attrTindakan[$patokan])->jenistindakanrm_id;
                        $arrTindakan[$i]=array(
                                            'tindakan'=> $attrTindakan[$patokan]
                                        );
                        $modHasil->create_time=date('Y-m-d H:i:s');
                        $modHasil->create_loginpemakai_id=Yii::app()->user->id;
                        $modHasil->create_ruangan=Yii::app()->user->getState('ruangan_id');
                        
                        
                        if ($modHasil->validate()){
                            $arrSave[$i] = $modHasil; // menyimpan objek BSRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                            $validTindakan = 'true'; // variabel untuk menentukan rencana operasi valid

                        }else{
                            $modHasil->tglpemeriksaanrm = Yii::app()->dateFormatter->formatDateTime(
                                    CDateTimeParser::parse($modHasil->tglpemeriksaanrm, 'yyyy-MM-dd'), 'medium', null);

                            $validTindakan = $validTindakan.'false';
                        }
                } //ENDING FOR 
                if($validTindakan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
                {
                    foreach ($arrTindakan as $x => $hasilTindakan) {
                        $tindakanNya[$x] = $hasilTindakan['tindakan'];
                    }
                    
                    foreach ($arrSave as $f => $simpan) {
                        
                        $simpan->save();
                        $this->saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$simpan,$attrTindakanPelayanan,$tindakanNya[$f]);
                    }
                    $this->successSave = true;
                }
                else
                {
                    $this->successSave = false;
                }
            return $modHasil;
        }
        
        /**
         * Fungsi untuk mengupdate hasilpemeriksaanrm_t 
         * @param type $attrHasil 
         */
        protected function updateHasilPemeriksaan($modJadwal,$attrPenunjang,$attrPendaftaran,$attrHasil,$index)    
        {   
            $validHasil = true;
                for ($i = 0; $i < count($attrHasil['hasilpemeriksaanrm_id'][$index]); $i++) {  
                    $modHasil = $this->loadHasilPemeriksaan($attrHasil['hasilpemeriksaanrm_id'][$index][$i]);
                    $modHasil->jadwalkunjunganrm_id = $modJadwal->jadwalkunjunganrm_id;
                    $modHasil->pasienmasukpenunjang_id = $modJadwal->pasienmasukpenunjang_id;
                    $modHasil->pegawai_id = (!empty($modJadwal->pegawai_id)) ? $modJadwal->pegawai_id : null ;
                    $modHasil->paramedis1_id = (!empty($modJadwal->paramedis1_id)) ? $modJadwal->paramedis1_id : null ;
                    $modHasil->paramedis2_id = (!empty($modJadwal->paramedis2_id)) ? $modJadwal->paramedis2_id : null ;
                    if($modHasil->validate())
                    {
                       $arrSave[$i] = $modHasil; // menyimpan objek 
                    }else{
                       $validHasil = false;
                    }
                }
            
            if($validHasil) //kondisi apabila semua hasil valid dan siap untuk di save
                {
                    foreach ($arrSave as $f => $simpan) 
                    {
                        $simpan->save();
                        $this->successSave = true;
                        if(empty($simpan->tindakanpelayanan_id)) //kondisi dimana tindakanpelayanan_id di hasilpemeriksaanrm_t masih kosong(belum diisi)
                        {
                            $modTindakan = $this->saveTindakanPelayanJadwal($attrPendaftaran,$attrPenunjang, $simpan, $index,$attrHasil);
                        }
                        
                    }
                }
                else
                {
                    $this->successSave = false;
                }
            return $modHasil;
            
        }
        
        /**
         * Fungsi untuk mengupdate jadwalkunjunganrm_t
         * @param type $attrHasil 
         */
        protected function updateJadwalKunjungan($attrJadwal,$attrPendaftaran,$modPenunjang,$i)    
        {   
            
            $validJadwal = true;
                $modJadwal = $this->loadJadwalKunjungan($attrJadwal['jadwalKunjunganrm_id'][$i]);
                $modJadwal->pasienmasukpenunjang_id = $modPenunjang->pasienmasukpenunjang_id;
                $modJadwal->tglkunjunganrm = date('Y-m-d h:i:s');
                $modJadwal->pegawai_id = (!empty($attrJadwal['pegawai_id'][$i])) ? $attrJadwal['pegawai_id'][$i] : null ;
                $modJadwal->paramedis1_id = (!empty($attrJadwal['paramedis1_id'][$i])) ? $attrJadwal['paramedis1_id'][$i] : null ;
                $modJadwal->paramedis2_id = (!empty($attrJadwal['paramedis2_id'][$i])) ? $attrJadwal['paramedis2_id'][$i] : null ;
                $modJadwal->update_time = date('Y-m-d h:i:s') ;
                $modJadwal->update_loginpemakai_id = Yii::app()->user->id ;
                if($modJadwal->validate())
                {
                   $arrSave[$i] = $modJadwal; // menyimpan objek 
                }else{
                   $validJadwal = false;
                }
                
            if($validJadwal) //kondisi apabila semua hasil valid dan siap untuk di save
                {
                    foreach ($arrSave as $f => $simpan) 
                    {
                        $simpan->save();
                        $this->successSave = true;
                        $modHasilPemeriksaan = $this->updateHasilPemeriksaan($simpan, $modPenunjang,$attrPendaftaran,$attrJadwal,$i);
                    }
                }
                else
                {
                    $this->successSave = false;
                }
            return $modHasil;
            
        }
        
        public function saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$attrHasil,$attrTindakanPelayanan,$attrTindakan)
        {
            $validTindakanPelayanan = 'true';
            $arrSave = array();
            
            $daftar_tindakan = $attrTindakanPelayanan['daftartindakan_id'][$attrTindakan];
            
            $kelaspelayanan_id = $attrTindakanPelayanan['kelaspelayanan_id'][$attrTindakan];
            
                $tarifTotal = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                
                $tarifRS = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_RS));
                
                $tarifBHP = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_BHP));
                
                $tarifParamedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_PARAMEDIS));
                
                $tarifMedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_MEDIS));

                $modTindakanPelayanan = new RMTindakanPelayananT;
                $modTindakanPelayanan->penjamin_id = $attrPendaftaran->penjamin_id;
                $modTindakanPelayanan->pasien_id = $attrPenunjang->pasien_id;
                $modTindakanPelayanan->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTindakanPelayanan->kelaspelayanan_id = $attrTindakanPelayanan['kelaspelayanan_id'][$attrTindakan];
                $modTindakanPelayanan->hasilpemeriksaanrm_id = $attrHasil->hasilpemeriksaanrm_id ;
                $modTindakanPelayanan->tipepaket_id = 1;
                $modTindakanPelayanan->instalasi_id = Params::INSTALASI_ID_REHAB;
                $modTindakanPelayanan->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                $modTindakanPelayanan->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayanan->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                $modTindakanPelayanan->daftartindakan_id = $attrTindakanPelayanan['daftartindakan_id'][$attrTindakan];
                $modTindakanPelayanan->carabayar_id = $attrPendaftaran->carabayar_id;
                $modTindakanPelayanan->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayanan->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayanan->tarif_tindakan = $attrTindakanPelayanan['tarif_tindakan'][$attrTindakan];
                $modTindakanPelayanan->tarif_satuan = $modTindakanPelayanan->tarif_tindakan;
                $modTindakanPelayanan->tarif_rsakomodasi = (!empty($tarifRS)) ? $tarifRS->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_medis = (!empty($tarifMedis)) ? $tarifMedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_paramedis = (!empty($tarifParamedis)) ? $tarifParamedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_bhp = (!empty($tarifBHP)) ? $tarifBHP->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->satuantindakan = $attrTindakanPelayanan['satuantindakan'][$attrTindakan];
                $modTindakanPelayanan->qty_tindakan = $attrTindakanPelayanan['qty_tindakan'][$attrTindakan];
                $modTindakanPelayanan->cyto_tindakan = $attrTindakanPelayanan['cyto_tindakan'][$attrTindakan];
                if($modTindakanPelayanan->cyto_tindakan){
                    $modTindakanPelayanan->tarifcyto_tindakan = $modTindakanPelayanan->tarif_tindakan * ($attrTindakanPelayanan['persencyto_tind'][$attrTindakan]/100);
                } else {
                    $modTindakanPelayanan->tarifcyto_tindakan = 0;
                }
                $modTindakanPelayanan->discount_tindakan = 0;
                $modTindakanPelayanan->subsidiasuransi_tindakan=0;
                $modTindakanPelayanan->subsidipemerintah_tindakan=0;
                $modTindakanPelayanan->subsisidirumahsakit_tindakan=0;
                $modTindakanPelayanan->iurbiaya_tindakan=0;
                
                if ($modTindakanPelayanan->validate()){
                    $arrSave[$i] = $modTindakanPelayanan; // menyimpan objek RMRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                    $validTindakanPelayanan = 'true';

                }else
                {   
                    $validTindakanPelayanan = $validTindakanPelayanan.'false';
                }
//            } //ENDING FOR
            if($validTindakanPelayanan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $x => $simpan) {
                    $simpan->save();
                    $this->saveTindakanKomponenT($simpan);
                    $this->upadateHasilTindakan($simpan);
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }

            return $modTindakanPelayanan;
        }
        
        public function saveTindakanKomponenT($attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakanKomponen = 'true';
            $daftarTindakan_id = $attrTindakanPelayanan->daftartindakan_id;
            $kelaspelayanan_id = $attrTindakanPelayanan->kelaspelayanan_id;
            $arrTarifTindakan = "select * from tariftindakan_m where daftartindakan_id = ".$daftarTindakan_id." and kelaspelayanan_id = ".$kelaspelayanan_id." and komponentarif_id <> ".Params::KOMPONENTARIF_ID_TOTAL." ";
            $query = Yii::app()->db->createCommand($arrTarifTindakan)->queryAll();
            foreach ($query as $i => $tarifKomponen) {
                $modTarifKomponen = new RMTindakanKomponenT;
                $modTarifKomponen->tindakanpelayanan_id = $attrTindakanPelayanan->tindakanpelayanan_id;
                $modTarifKomponen->komponentarif_id = $tarifKomponen['komponentarif_id'];
                $modTarifKomponen->tarif_tindakankomp = $tarifKomponen['harga_tariftindakan'];
                if($attrTindakanPelayanan->cyto_tindakan){
                    $modTarifKomponen->tarifcyto_tindakankomp = $tarifKomponen['harga_tariftindakan'] * ($tarifKomponen['persencyto_tind']/100);
                } else {
                    $modTarifKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTarifKomponen->subsidiasuransikomp = 0;
                $modTarifKomponen->subsidipemerintahkomp = 0;
                $modTarifKomponen->subsidirumahsakitkomp = 0;
                $modTarifKomponen->iurbiayakomp = 0;
                $modTarifKomponen->tarif_kompsatuan = $modTarifKomponen->tarif_tindakankomp;
                if ($modTarifKomponen->validate()){
                    $arrSave[$i] = $modTarifKomponen; // menyimpan objek tarif komponen ke dalam sebuah array dan siap untuk disave
                    $validTindakanKomponen = 'true';

                }else
                {
                    $validTindakanKomponen = $validTindakanKomponen.'false';
                }
            } // ending foreach
            if($validTindakanKomponen == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $f => $simpan) {
                    $simpan->save();
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }
            return $modTarifKomponen;
        }
        
        public function saveTindakanPelayanJadwal($attrPendaftaran,$attrPenunjang,$attrHasil,$index,$attrPOST)
        {
            $validTindakanPelayanan = 'true';
            $arrSave = array();
                $daftar_tindakan = Tindakanrm::model()->findByPk($attrHasil->tindakanrm_id)->daftartindakan_id;
                
                $tarifTotal = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                
                $tarifRS = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_RS));
                
                $tarifBHP = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_BHP));
                
                $tarifParamedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_PARAMEDIS));
                
                $tarifMedis = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$attrPendaftaran->kelaspelayanan_id,
                    'daftartindakan_id'=>$daftar_tindakan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_MEDIS));
            
                $modTindakanPelayanan = new RMTindakanPelayananT;
                $modTindakanPelayanan->penjamin_id = $attrPendaftaran->penjamin_id;
                $modTindakanPelayanan->pasien_id = $attrPenunjang->pasien_id;
                $modTindakanPelayanan->kelaspelayanan_id = $tarifTotal->kelaspelayanan_id;
                $modTindakanPelayanan->hasilpemeriksaanrm_id = $attrHasil->hasilpemeriksaanrm_id;
                $modTindakanPelayanan->tipepaket_id = 1;
                $modTindakanPelayanan->instalasi_id = Params::INSTALASI_ID_REHAB;
                $modTindakanPelayanan->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                $modTindakanPelayanan->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayanan->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                $modTindakanPelayanan->daftartindakan_id = $daftar_tindakan;
                $modTindakanPelayanan->carabayar_id = $attrPendaftaran->carabayar_id;
                $modTindakanPelayanan->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayanan->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayanan->tarif_tindakan = $tarifTotal->harga_tariftindakan;
                $modTindakanPelayanan->tarif_satuan = $modTindakanPelayanan->tarif_tindakan;
                $modTindakanPelayanan->tarif_rsakomodasi = (!empty($tarifRS)) ? $tarifRS->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_medis = (!empty($tarifMedis)) ? $tarifMedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_paramedis = (!empty($tarifParamedis)) ? $tarifParamedis->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->tarif_bhp = (!empty($tarifBHP)) ? $tarifBHP->harga_tariftindakan : 0 ;
                $modTindakanPelayanan->satuantindakan = 'KALI';
                $modTindakanPelayanan->qty_tindakan = 1;
                $modTindakanPelayanan->cyto_tindakan = 0;
                $modTindakanPelayanan->dokterpemeriksa1_id = $attrHasil->pegawai_id;
                
                if($modTindakanPelayanan->cyto_tindakan){
                    $modTindakanPelayanan->tarifcyto_tindakan = $modTindakanPelayanan->tarif_tindakan * ($attrTindakanPelayananTotal->persencyto_tind/100);
                } else {
                    $modTindakanPelayanan->tarifcyto_tindakan = 0;
                }
                
                $arrTindakan[$i]=array(
                                            'tindakanrm_id'=> $attrHasil->tindakanrm_id
                                        );
                
                $modTindakanPelayanan->discount_tindakan = 0;
                $modTindakanPelayanan->subsidiasuransi_tindakan=0;
                $modTindakanPelayanan->subsidipemerintah_tindakan=0;
                $modTindakanPelayanan->subsisidirumahsakit_tindakan=0;
                $modTindakanPelayanan->iurbiaya_tindakan=0;
                
                if ($modTindakanPelayanan->validate()){
                    $arrSave[$i] = $modTindakanPelayanan; // menyimpan objek RMRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                    $validTindakanPelayanan = 'true';

                }else
                {   
                    $validTindakanPelayanan = $validTindakanPelayanan.'false';
                }
            
            if($validTindakanPelayanan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrTindakan as $y => $hasilTindakan) {
                        $tindakanNya[$y] = $hasilTindakan['tindakanrm_id'];
                }
                
                foreach ($arrSave as $x => $simpan) {
                    $simpan->save();
                    $this->saveTindakanKomponenJadwal($simpan);
                    $this->upadateHasilTindakanAll($simpan,$attrHasil,$tindakanNya[$x]);
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }

            return $modTindakanPelayanan;
        }
        
        public function saveTindakanKomponenJadwal($attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakanKomponen = 'true';
            $daftarTindakan_id = $attrTindakanPelayanan->daftartindakan_id;
            $kelaspelayanan_id = $attrTindakanPelayanan->kelaspelayanan_id;
            $arrTarifTindakan = "select * from tariftindakan_m where daftartindakan_id = ".$daftarTindakan_id." and kelaspelayanan_id = ".$kelaspelayanan_id." and komponentarif_id <> ".Params::KOMPONENTARIF_ID_TOTAL." ";
            
            $query = Yii::app()->db->createCommand($arrTarifTindakan)->queryAll();
            foreach ($query as $i => $tarifKomponen) {
                $modTarifKomponen = new RMTindakanKomponenT;
                $modTarifKomponen->tindakanpelayanan_id = $attrTindakanPelayanan->tindakanpelayanan_id;
                $modTarifKomponen->komponentarif_id = $tarifKomponen['komponentarif_id'];
                $modTarifKomponen->tarif_tindakankomp = $tarifKomponen['harga_tariftindakan'];
                if($attrTindakanPelayanan->cyto_tindakan){
                    $modTarifKomponen->tarifcyto_tindakankomp = $tarifKomponen['harga_tariftindakan'] * ($tarifKomponen['persencyto_tind']/100);
                } else {
                    $modTarifKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTarifKomponen->subsidiasuransikomp = 0;
                $modTarifKomponen->subsidipemerintahkomp = 0;
                $modTarifKomponen->subsidirumahsakitkomp = 0;
                $modTarifKomponen->iurbiayakomp = 0;
                $modTarifKomponen->tarif_kompsatuan = $modTarifKomponen->tarif_tindakankomp;
                if ($modTarifKomponen->validate()){
                    
                    $arrSave[$i] = $modTarifKomponen; // menyimpan objek tarif komponen ke dalam sebuah array dan siap untuk disave
                    $validTindakanKomponen = 'true';

                }else
                {
                    $validTindakanKomponen = $validTindakanKomponen.'false';
                }
            } // ending foreach
            if($validTindakanKomponen == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $f => $simpan) {
                    $simpan->save();
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }
            return $modTarifKomponen;
        }
        
        /**
         * Fungsi untuk mengupadte hasil pemeriksaan rehab medis menset tindakanpelayanan id
         * @param type $modTindPelayanan model object
         */
        protected function upadateHasilTindakan($modTindPelayanan)
        {
            $modHasil = $this->loadById($modTindPelayanan->hasilpemeriksaanrm_id);
            $modHasil->tindakanpelayanan_id = $modTindPelayanan->tindakanpelayanan_id;
            $modHasil->save();
        }
        
        /**
         * Fungsi untuk mengupadte hasil pemeriksaan rehab medis menset tindakanpelayanan id berdasarkan jadwalkunjunganrm_id
         * @param type $modTindPelayanan model object
         */
        protected function upadateHasilTindakanAll($modTindPelayanan,$attrHasil,$tindakan)
        {
//            $sqlUpdate = "update hasilpemeriksaanrm_t set tindakanpelayanan_id = $modTindPelayanan->tindakanpelayanan_id where jadwalkunjunganrm_id = $attrJadwal->jadwalkunjunganrm_id and tindakanrm_id = $tindakan";
//            $jalankanSql = Yii::app()->db->createCommand($sqlUpdate)->queryAll();
            HasilpemeriksaanrmT::model()->updateByPk($attrHasil->hasilpemeriksaanrm_id, array('tindakanpelayanan_id'=>$modTindPelayanan->tindakanpelayanan_id ));
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dengan method findByPk yang nanti digunakan untuk menyimpan data-data hasil pemeriksaan
         * @param type $id
         * @return type 
         */
        public function loadById($id)
        {       $model= HasilpemeriksaanrmT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dari pendaftaran_t dengan method findByPk yang 
         * @param type $id
         * @return type 
         */
        public function loadPendaftaran($id)
        {       $model= PendaftaranT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dari pendaftaran_t dengan method findByPk yang 
         * @param type $id
         * @return type 
         */
        public function loadJadwalKunjungan($id)
        {       $model= JadwalkunjunganrmT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        /**
         * Fungsi untuk mengembalikan object $model dengan method findByAttributes yang nanti digunakan untuk mendeskripsikan hasilpemeriksanrm_t
         * @param type $id
         * @return type 
         */
        protected function loadHasilPemeriksaan($id)
        {
                $model= HasilpemeriksaanrmT::model()->findByPk($id);
		if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }
        
        
        
}
