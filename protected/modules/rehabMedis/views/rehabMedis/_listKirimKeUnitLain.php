<table id="tblListPermintaanRehab" class="table table-bordered table-condensed" >
    <thead>
        <tr>
            <th>Tgl Kirim Ke Rehab Medis</th>
            <th>Permintaan Tindakan</th>
            <th>Tarif</th>
            <th>Qty</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
<?php
foreach ($modRiwayatKirimKeUnitLain as $i => $riwayat) {
    $modPermintaan = RMPermintaanPenunjangT::model()->with('daftartindakan','tindakanrm')->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$riwayat->pasienkirimkeunitlain_id));
    ?>
    <tr>
        <td><?php echo $riwayat->tgl_kirimpasien; ?></td>
        <td>
            <?php
            foreach($modPermintaan as $j => $permintaan){
                echo $permintaan->tindakanrm->tindakanrm_nama.'<br/>';
            } ?>
        </td>
        <td>
            <?php
            foreach($modPermintaan as $j => $permintaan){
                $modTarif = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$riwayat->kelaspelayanan_id,
                                                                            'daftartindakan_id'=>$permintaan->daftartindakan_id,
                                                                            'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                
                echo (!empty($modTarif->harga_tariftindakan))? MyFunction::formatNumber($modTarif->harga_tariftindakan).'<br/>':'0 <br/>';
            } ?>
        </td>
        <td>
            <?php
            foreach($modPermintaan as $j => $permintaan){
                echo $permintaan->qtypermintaan.'<br/>';
            } ?>
        </td>
        <td>
            <?php echo CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'batalKirim('.$riwayat->pasienkirimkeunitlain_id.','.$riwayat->pendaftaran_id.');return false;','rel'=>'tooltip','title'=>'Klik untuk membatalkan kirim pasien')); ?>
        </td>
    </tr>
    <?php
}
?>
    
</table>