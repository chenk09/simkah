<table class="items table table-striped table-bordered table-condensed" id="tblListKonsul">
    <thead>
        <tr>
            <th>Tanggal Konsul</th>
            <th>No. Pendaftaran</th>
            <th>Poliklinik Asal</th>
            <th>Poliklinik Tujuan</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($modRiwayatKonsul as $i => $konsul) { ?>
        <tr>
            <td><?php echo $konsul->tglkonsulpoli ?></td>
            <td><?php echo $konsul->pendaftaran->no_pendaftaran ?></td>
            <td><?php echo $konsul->poliasal->ruangan_nama ?></td>
            <td><?php echo $konsul->politujuan->ruangan_nama ?></td>
            <td><?php echo CHtml::link("<i class='icon-eye-open'></i>", '#', array('onclick'=>'viewDetailKonsul('.$konsul->konsulpoli_id.');return false;','rel'=>'tooltip','title'=>'Klik untuk melihat detail konsul')); ?>
                <?php echo CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'batalKonsul('.$konsul->konsulpoli_id.','.$konsul->pendaftaran_id.');return false;','rel'=>'tooltip','title'=>'Klik untuk membatalkan konsul')); ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>