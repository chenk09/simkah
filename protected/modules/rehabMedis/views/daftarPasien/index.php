<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
    <legend class="rim2">Informasi Daftar Pasien </legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$modPasienMasukPenunjang->searchRM(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            'tglmasukpenunjang', 
            
            array(
            'header'=>'Instalasi / Ruangan Asal',
            'value'=>'$data->insatalasiRuanganAsal'
            ),
            
            'no_pendaftaran',
            'no_rekam_medik',
            
            array(
            'header'=>'Nama Pasien / Bin',
            'value'=>'$data->namaPasienNamaBin'
            ),
            
            array(
                'header'=>'Kasus Penyakit / <br> Kelas Pelayanan',
                'type'=>'raw',
                'value'=>'"$data->jeniskasuspenyakit_nama"."<br/>"."$data->kelaspelayanan_nama"',
            ),
            'umur',
            'alamat_pasien',
           
            array(
            'header'=>'Cara Bayar / Penjamin',
            'value'=>'$data->caraBayarPenjamin',
            ),
            
            'nama_pegawai',
//            'kelaspelayanan_nama',
            
            array(
                'name'=>'Pemeriksaan Pasien',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/rehabMedis/anamnesaTRM",array("idPendaftaran"=>$data->pendaftaran_id)),array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien"))',
                'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
            array(
                'header'=>'Buat Jadwal',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=icon-pencil></i>",Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/buatJadwal",array("id"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik Untuk Membuat Jadwal Rehab Medis"))',    
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            
            array(
                'name'=>'masukanHasil',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=icon-pencil></i>",Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/hasilPemeriksaan",array("idPendaftaran"=>$data->pendaftaran_id,"idPasien"=>$data->pasien_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik Untuk Memasukkan hasil"))',    
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php
 //CHtml::link($text, $url, $htmlOptions)
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'id'=>'daftarPasien-form',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <table class="table-condensed">
        <tr>
            <td>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
            </td>
            <td>
                <div class="control-group ">
                    <label for="namaPasien" class="control-label">
                        <?php echo CHtml::activecheckBox($modPasienMasukPenunjang, 'ceklis', array('uncheckValue'=>0,'rel'=>'tooltip', 'onClick'=>'cekTanggal()','data-original-title'=>'Cek untuk pencarian berdasarkan tanggal')); ?>
                        Tgl Masuk 
                    </label>
                    <div class="controls">
                        <?php   $format = new CustomFormat;
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPasienMasukPenunjang,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'disabled'=>true),
                        )); 
                               ?>
                      
                   </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' s/d', array('class'=>'control-label')) ?>

                   <div class="controls"> 
                            <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPasienMasukPenunjang,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'disabled'=>true),
                        )); ?>
                    </div>
                </div> 
                 
                 
            </td>
            
            
        </tr>
    </table>
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
     ?>
	 <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('daftarPasien/index'), array('class'=>'btn btn-danger')); ?>
	 		<?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
		
</fieldset>  
<?php $this->endWidget();?>
</fieldset>

<script type="text/javascript">
    
    
    function cekTanggal(){
        
        var checklist = $('#RMMasukPenunjangV_ceklis');
        var pilih = checklist.attr('checked');
        var tgl_masuk = $(document)
        if(pilih){
            document.getElementById('RMMasukPenunjangV_tglAwal').disabled = false;
            document.getElementById('RMMasukPenunjangV_tglAkhir').disabled = false;
            document.getElementById('RMMasukPenunjangV_tglAwal_date').disabled = false;
            document.getElementById('RMMasukPenunjangV_tglAkhir_date').disabled = false;
        }else{
            document.getElementById('RMMasukPenunjangV_tglAwal').disabled = true;
            document.getElementById('RMMasukPenunjangV_tglAkhir').disabled = true;
            document.getElementById('RMMasukPenunjangV_tglAwal_date').disabled = true;
            document.getElementById('RMMasukPenunjangV_tglAkhir_date').disabled = true;
        }
    }

</script>