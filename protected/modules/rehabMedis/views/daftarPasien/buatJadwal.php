<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'buatjadwal-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#lamaterapi',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

<?php echo $form->errorSummary(array($modNewHasil,$modJadwalKunjungan,$modTindakanPelayanan,$modTindakanKomponen)); ?>

<?php echo $this->renderPartial('_formDataPasien',array('form'=>$form,'modPasienPenunjang'=>$modPasienPenunjang)) ?>

<?php echo $this->renderPartial('_formJadwalKunjungan',array('form'=>$form,'modPasienPenunjang'=>$modPasienPenunjang,'id'=>$id,'listJadwalKunjungan'=>$listJadwalKunjungan,)) ?>



<?php if(empty($listJadwalKunjungan)){?>
<div class='form-actions'>
        <?php echo CHtml::htmlButton($modRO->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                       Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                        array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('updateRencana',array('id'=>$modPasienPenunjang->pasienmasukpenunjang_id)), array('class'=>'btn btn-danger')); ?>
        <?php //if(!$modRO->isNewRecord) echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false")); ?>
</div>
<?php }else{ ?>
<?php //echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('updateRencana',array('id'=>$modPasienPenunjang->pasienmasukpenunjang_id)), array('class'=>'btn btn-danger')); ?>
<?php } ?>

<?php $this->endWidget(); ?>

<?php $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>'penjelasan transaksi')); ?>


