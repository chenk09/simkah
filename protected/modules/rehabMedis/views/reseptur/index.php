<?php
$this->breadcrumbs=array(
	'Reseptur',
);

$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));

$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'Anamnesis', 'url'=>$this->createUrl('/rehabMedis/anamnesa',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Periksa Fisik', 'url'=>$this->createUrl('/rehabMedis/PemeriksaanFisik',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
        array('label'=>'Laboratorium', 'url'=>$this->createUrl('/rehabMedis/laboratorium',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Radiologi', 'url'=>$this->createUrl('/rehabMedis/radiologi',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rehab Medis', 'url'=>$this->createUrl('/rehabMedis/rehabMedis',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Konsultasi Gizi', 'url'=>$this->createUrl('/rehabMedis/konsulGizi',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Konsul Poliklinik', 'url'=>$this->createUrl('/rehabMedis/konsulPoli',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Tindakan', 'url'=>$this->createUrl('/rehabMedis/tindakan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
        array('label'=>'Diagnosis', 'url'=>$this->createUrl('/rehabMedis/diagnosa',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Bedah Sentral', 'url'=>$this->createUrl('/rehabMedis/bedahSentral',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rujukan Ke Luar', 'url'=>$this->createUrl('/rehabMedis/rujukanKeluar',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Reseptur', 'url'=>'', 'active'=>true),
        array('label'=>'Pemakaian Bahan', 'url'=>$this->createUrl('/rehabMedis/pemakaianBahan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
    ),
)); ?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>1,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rjreseptur-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#RJResepturT_noresep',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInput();'),
)); ?>

<?php $this->renderPartial('_formInputObat',array('form'=>$form,'modReseptur'=>$modReseptur)); ?>

<table id="tblDaftarResep" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>Recipe</th>
            <th>R ke</th>
            <th>Nama Obat</th>
            <th>Sumber Dana</th>
            <th>Satuan Kecil</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Sub Total</th>
            <th>Signa</th>
            <th>Etiket</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
        <tr>
            <td colspan="7" style="text-align: right;"><b>Total Harga</b></td>
            <td><input type="text" readonly name="totalHargaReseptur" id="totalHargaReseptur" class="inputFormTabel lebar2 currency" /></td>
            <td colspan="3">&nbsp;</td>
        </tr>
    </tfoot>
</table>

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>

<?php $this->endWidget(); ?>