<?php
$this->breadcrumbs=array(
	'Rmjenis Tindakanrm Ms'=>array('index'),
	$model->jenistindakanrm_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jenis Tindakan #'.$model->jenistindakanrm_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                // array_push($arrMenu,array('label'=>Yii::t('mds','List').' RMJenisTindakanrmM', 'icon'=>'list', 'url'=>array('index'))) ;
                // (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' RMJenisTindakanrmM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                // (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' RMJenisTindakanrmM', 'icon'=>'pencil','url'=>array('update','id'=>$model->jenistindakanrm_id))) :  '' ;
                // array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' RMJenisTindakanrmM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->jenistindakanrm_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jenis Tindakan ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'jenistindakanrm_id',
		'jenistindakanrm_nama',
		'jenistindakanrm_namalainnya',
		'jenistindakanrm_aktif',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>