 <?php  $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                                'id'=>'dialogTindakan',
                                'options'=>array(
                                    'title'=>'Pilih Tindakan',
                                    'autoOpen'=>false,
                                    'width'=>450,
                                    'height'=>300,
                                    'modal'=>false,
                                    'hide'=>'explode',
                                    'resizelable'=>false,
                                ),
                            ));
                            ?>
<div class="box">
    <?php foreach($modJenisTindakan as $i=>$jenisTindakan){ 
            $ceklist = false;
    ?>
            <div class="boxtindakan">
                <h6><?php echo $jenisTindakan->jenistindakanrm_nama; ?></h6>
                <?php foreach ($modTindakan as $j => $tindakanRM) {
                          if($jenisTindakan->jenistindakanrm_id == $tindakanRM->jenistindakanrm_id) {
                             echo CHtml::checkBox("tindakanrm_id[]", $ceklist, array('value'=>$tindakanRM->tindakanrm_id,
                                                                                      'onclick' => "inputTindakan(this)"));
                             echo "<span>".$tindakanRM->tindakanrm_nama."</span><br/>";
                         }
                     } ?>
            </div>
    <?php } ?>
</div>

 <?php $this->endWidget('zii.widgets.jui.CJuiDialog');?><br/>
 
 <table id="tblFormTindakanRM" class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th><a class="btn btn-primary" onclick="setKelasPelayanan();return false;" href="#" data-original-title="Klik untuk pilih Tindakan Rehab Medis" rel="tooltip"><i class="icon-zoom-in icon-white"></i></a></th>
                    <th>Jenis Tindakan/<br/>Tindakan</th>
                    <th>Tarif</th>
                    <th>Qty</th>
                    <th>Satuan</th>
                    <th>Cyto</th>
                    <th>Tarif Cyto</th>
                </tr>
            </thead>
        </table>
        
       
</fieldset>
<script>
function inputTindakan(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanRM = obj.value;
        var kelasPelayan_id = $('#RMPendaftaranMp_kelaspelayanan_id').val();
        
        if(kelasPelayan_id==''){
                $(obj).attr('checked', 'false');
                alert('Anda Belum Memilih Kelas Pelayanan');
                
            }
        else
        {
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRMPendRM')?>',
                 'data':{idPemeriksaanRM:idPemeriksaanRM,kelasPelayan_id:kelasPelayan_id},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                     if (data.form == ''){$(obj).removeAttr("checked");alert("Tarif tindakan belum tersedia");} else {
                         $('#tblFormTindakanRM').append(data.form);
                     }
                 } ,
                 'cache':false});
        }     
    } 
    else 
    {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idTindakan)
{
    $('#tblFormTindakanRM #tindakan_'+idTindakan).detach();
}


function hitungCyto(id,obj)
{
    if(obj == 1)
    {
        var persen_cytotind = $('#RMTindakanPelayananT_persencyto_tind_'+id+'').val(); 
        var harga_tarif = $('#RMTindakanPelayananT_tarif_tindakan_'+id+'').val(); 
        var tarif_cyto = harga_tarif * (persen_cytotind/100);

        $('#RMTindakanPelayananT_tarif_cyto_'+id+'').val(tarif_cyto);
    }
    else
    {
        $('#RMTindakanPelayananT_tarif_cyto_'+id+'').val(0);
    }
    
}

function setKelasPelayanan(){
    if (jQuery.isNumeric($("#RMPendaftaranMp_kelaspelayanan_id").val())){
        $('#dialogTindakan').dialog('open');
    }else{
        alert("Kelas Pelayanan harap diisi terlebih dahulu");
    }
}
$("form#ropendaftaran-mp-form").submit(function(){
    jumlah = $("#tblFormPemeriksaanRad tbody tr").length;
    if (jumlah > 0 ){return true;} else{ alert("Pemeriksaan harap diisi");return false;};
});
</script>