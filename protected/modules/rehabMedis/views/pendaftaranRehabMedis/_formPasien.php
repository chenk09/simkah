
<div class="control-group" id="controlNoRekamMedik">
    <label class="control-label">
      <i class="icon-user"></i>  <?php echo CHtml::checkBox('isPasienLama', $model->isPasienLama, array('rel'=>'tooltip','title'=>'Pilih jika pasien lama','onclick'=>'enableInputPasien(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        No Rekam Medik
    </label>
    <?php //echo CHtml::label('No Rekam Medik', 'noRekamMedik', array('class'=>'control-label')) ?>
    <div class="controls">
        <?php 
                $enable['readonly'] = true;
                $readOnly = ($model->isPasienLama) ? '' : $enable ; 
        ?>
        <?php $this->widget('MyJuiAutoComplete',array(
                    'name'=>'noRekamMedik',
                    'value'=>$model->noRekamMedik,
                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienLama'),
                    'options'=>array(
                       'showAnim'=>'fold',
                       'minLength' => 4,
                       'focus'=> 'js:function( event, ui ) {
                            $("#noRekamMedik").val( ui.item.value );
                            return false;
                        }',
                       'select'=>'js:function( event, ui ) {
                            $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);
                            $("#'.CHtml::activeId($modPasien,'jenisidentitas').'").val(ui.item.jenisidentitas);
                            $("#'.CHtml::activeId($modPasien,'no_identitas_pasien').'").val(ui.item.no_identitas_pasien);
                            $("#'.CHtml::activeId($modPasien,'namadepan').'").val(ui.item.namadepan);
                            $("#'.CHtml::activeId($modPasien,'nama_pasien').'").val(ui.item.nama_pasien);
                            $("#'.CHtml::activeId($modPasien,'nama_bin').'").val(ui.item.nama_bin);
                            $("#'.CHtml::activeId($modPasien,'tempat_lahir').'").val(ui.item.tempat_lahir);
                            $("#'.CHtml::activeId($modPasien,'tanggal_lahir').'").val(ui.item.tanggal_lahir);
                            $("#'.CHtml::activeId($modPasien,'kelompokumur_id').'").val(ui.item.kelompokumur_id);
                            $("#'.CHtml::activeId($modPasien,'jeniskelamin').'").val(ui.item.jeniskelamin);
                            setJenisKelaminPasien(ui.item.jeniskelamin);
                            setRhesusPasien(ui.item.rhesus);
                            loadDaerahPasien(ui.item.propinsi_id, ui.item.kabupaten_id, ui.item.kecamatan_id);
                            $("#'.CHtml::activeId($modPasien,'statusperkawinan').'").val(ui.item.statusperkawinan);
                            $("#'.CHtml::activeId($modPasien,'golongandarah').'").val(ui.item.golongandarah);
                            $("#'.CHtml::activeId($modPasien,'rhesus').'").val(ui.item.rhesus);
                            $("#'.CHtml::activeId($modPasien,'alamat_pasien').'").val(ui.item.alamat_pasien);
                            $("#'.CHtml::activeId($modPasien,'rt').'").val(ui.item.rt);
                            $("#'.CHtml::activeId($modPasien,'rw').'").val(ui.item.rw);
                            $("#'.CHtml::activeId($modPasien,'propinsi_id').'").val(ui.item.propinsi_id);
                            $("#'.CHtml::activeId($modPasien,'kabupaten_id').'").val(ui.item.kabupaten_id);
                            $("#'.CHtml::activeId($modPasien,'kecamatan_id').'").val(ui.item.kecamatan_id);
                            $("#'.CHtml::activeId($modPasien,'kelurahan_id').'").val(ui.item.kelurahan_id);
                            $("#'.CHtml::activeId($modPasien,'no_telepon_pasien').'").val(ui.item.no_telepon_pasien);
                            $("#'.CHtml::activeId($modPasien,'no_mobile_pasien').'").val(ui.item.no_mobile_pasien);
                            $("#'.CHtml::activeId($modPasien,'suku_id').'").val(ui.item.suku_id);
                            $("#'.CHtml::activeId($modPasien,'alamatemail').'").val(ui.item.alamatemail);
                            $("#'.CHtml::activeId($modPasien,'anakke').'").val(ui.item.anakke);
                            $("#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'").val(ui.item.jumlah_bersaudara);
                            $("#'.CHtml::activeId($modPasien,'pendidikan_id').'").val(ui.item.pendidikan_id);
                            $("#'.CHtml::activeId($modPasien,'pekerjaan_id').'").val(ui.item.pekerjaan_id);
                            $("#'.CHtml::activeId($modPasien,'agama').'").val(ui.item.agama);
                            $("#'.CHtml::activeId($modPasien,'warga_negara').'").val(ui.item.warga_negara);
                            loadUmur(ui.item.tanggal_lahir);
                            cariPendaftaranRehabMedis(ui.item.pasien_id);
                            return false;
                        }', 

                    ),'tombolDialog'=>array('idDialog'=>'dialogPasien'),
                    'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'numbersOnly'),
        )); ?>
        <?php // echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                           // array('onclick'=>'$("#dialogPasien").dialog("open");return false;',
                                //  'class'=>'btn btn-primary',
                                //  'onkeypress'=>"return $(this).focusNextInputField(event)",
                                //  'rel'=>"tooltip",
                               //   'title'=>"Klik untuk mencari pasien",)); ?>
        <?php echo $form->error($model, 'noRekamMedik'); ?>
    </div>
</div>

<fieldset id='fieldsetPasien'>
    <legend class="rim">
        Data Pasien <?php echo CHtml::checkBox('isUpdatePasien', '', array('rel'=>'tooltip','title'=>'Pilih untuk update data pasien','onclick'=>'updateInputPasien(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    </legend>
    <?php //echo $form->textFieldRow($modPasien,'no_rekam_medik'); ?>
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'no_identitas_pasien', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo $form->dropDownList($modPasien,'jenisidentitas', JenisIdentitas::items(),  
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2'
                                                )); ?>   
            <?php echo $form->textField($modPasien,'no_identitas_pasien', array('placeholder'=>'No Identitas','onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>            
            <?php echo $form->error($modPasien, 'jenisidentitas'); ?><?php echo $form->error($modPasien, 'no_identitas'); ?>
        </div>
    </div>

    <?php //echo $form->dropDownListRow($modPasien,'jenisidentitas', JenisIdentitas::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php //echo $form->textFieldRow($modPasien,'no_identitas_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'nama_pasien', array('class'=>'control-label')) ?>
        <div class="controls inline">
            
            <?php echo $form->dropDownList($modPasien,'namadepan', NamaDepan::items(),  
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2'
                                                )); ?>   
            <?php echo $form->textField($modPasien,'nama_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>            

            <?php echo $form->error($modPasien, 'namadepan'); ?><?php echo $form->error($modPasien, 'nama_pasien'); ?>
        </div>
    </div>
    
    <?php //echo $form->dropDownListRow($modPasien,'namadepan', NamaDepan::items(),array('empty'=>'-- Pilih --','class'=>'span1','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php //echo $form->textFieldRow($modPasien,'nama_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
     <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'Alias', array('class'=>'control-label')) ?>
        <div class="controls inline">
            <?php echo $form->textField($modPasien,'nama_bin', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        </div>
    </div>
    <?php echo $form->textFieldRow($modPasien,'tempat_lahir', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php //echo $form->textFieldRow($modPasien,'tanggal_lahir'); ?>
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'tanggal_lahir', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php   
                    $this->widget('MyDateTimePicker',array(
                                    'model'=>$modPasien,
                                    'attribute'=>'tanggal_lahir',
                                    'mode'=>'date',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        'maxDate' => 'd',
                                        //
                                        'onkeypress'=>"js:function(){getUmur(this);}",
                                        'onSelect'=>'js:function(){getUmur(this);}',
                                        'yearRange'=> "-60:+0",
                                    ),
                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                    ),
            )); ?>
            <?php echo $form->error($modPasien, 'tanggal_lahir'); ?>
        </div>
    </div>
    
    <?php //echo $form->textFieldRow($model,'umur', array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahir(this)')); ?>
    <div class="control-group ">
        <?php echo $form->labelEx($model,'umur', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php
                $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'umur',
                'mask' => '99 Thn 99 Bln 99 Hr',
                'htmlOptions' => array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahir(this)')
                ));
                ?>
            <?php echo $form->error($model, 'umur'); ?>
        </div>
    </div>

    <?php //echo $form->dropDownListRow($modPasien,'kelompokumur', KelompokUmur::items(),array('empty'=>'-- Pilih --', 'class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php echo $form->radioButtonListInlineRow($modPasien, 'jeniskelamin', JenisKelamin::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php //echo $form->dropDownListRow($modPasien,'jeniskelamin', JenisKelamin::items(),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php echo $form->dropDownListRow($modPasien,'statusperkawinan', StatusPerkawinan::items(),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
 
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'golongandarah', array('class'=>'control-label')) ?>
        
        <div class="controls">
 
            <?php echo $form->dropDownList($modPasien,'golongandarah', GolonganDarah::items(),  
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>   
            <div class="radio inline">
                <div class="form-inline">
                <?php echo $form->radioButtonList($modPasien,'rhesus',Rhesus::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            
                </div>
           </div>
            <?php echo $form->error($modPasien, 'golongandarah'); ?>
            <?php echo $form->error($modPasien, 'rhesus'); ?>
        </div>
    </div>
    <?php //echo $form->dropDownListRow($modPasien,'golongandarah', GolonganDarah::items(),array('empty'=>'-- Pilih --','class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    
    <?php //echo $form->dropDownListRow($modPasien,'rhesus', Rhesus::items(),array('empty'=>'-- Pilih --','class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    
    <?php echo $form->textAreaRow($modPasien,'alamat_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php //echo $form->textFieldRow($modPasien,'rt',array('class'=>'span1','maxlength'=>3, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'rt', array('class'=>'control-label inline')) ?>
        
        <div class="controls">
            <?php echo $form->textField($modPasien,'rt', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1','maxlength'=>3)); ?>   / 
            <?php echo $form->textField($modPasien,'rw', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1','maxlength'=>3)); ?>            
            <?php echo $form->error($modPasien, 'rt'); ?>
            <?php echo $form->error($modPasien, 'rw'); ?>
        </div>
    </div>
    <?php //echo $form->textFieldRow($modPasien,'rw',array('class'=>'span1','maxlength'=>3, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'propinsi_id', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php $modPasien->propinsi_id = (!empty($modPasien->propinsi_id))?$modPasien->propinsi_id:Yii::app()->user->getState('propinsi_id');?>
        <?php echo $form->dropDownList($modPasien,'propinsi_id', CHtml::listData($modPasien->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'RMPasienM')),
                                                              'update'=>'#RMPasienM_kabupaten_id',),
                                                'onchange'=>"clearKecamatan();clearKelurahan();",)); ?>
            <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                            array('class'=>'btn btn-primary','onclick'=>"{addPropinsi(); $('#dialogAddPropinsi').dialog('open');}",
                                                  'id'=>'btnAddPropinsi','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                  'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modPasien->getAttributeLabel('propinsi_id'))) ?>
            <?php echo $form->error($modPasien, 'propinsi_id'); ?>
        </div>
    </div>

    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'kabupaten_id', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php $modPasien->kabupaten_id = (!empty($modPasien->kabupaten_id))?$modPasien->kabupaten_id:Yii::app()->user->getState('kabupaten_id');?>
            <?php echo $form->dropDownList($modPasien,'kabupaten_id',CHtml::listData($modPasien->getKabupatenItems($modPasien->propinsi_id), 'kabupaten_id', 'kabupaten_nama'),
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                    'ajax'=>array('type'=>'POST',
                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'RMPasienM')),
                                                                  'update'=>'#RMPasienM_kecamatan_id'),
                                                    'onchange'=>"clearKelurahan();",)); ?>
            <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                            array('class'=>'btn btn-primary','onclick'=>"{addKabupaten(); $('#dialogAddKabupaten').dialog('open');}",
                                                  'id'=>'btnAddKabupaten','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                  'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modPasien->getAttributeLabel('kabupaten_id'))) ?>
            <?php echo $form->error($modPasien, 'kabupaten_id'); ?>
        </div>
    </div>
    
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'kecamatan_id', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php $modPasien->kecamatan_id = (!empty($modPasien->kecamatan_id))?$modPasien->kecamatan_id:Yii::app()->user->getState('kecamatan_id');?>
            <?php echo $form->dropDownList($modPasien,'kecamatan_id',CHtml::listData($modPasien->getKecamatanItems($modPasien->kabupaten_id), 'kecamatan_id', 'kecamatan_nama'),
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                    'ajax'=>array('type'=>'POST',
                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'RMPasienM')),
                                                                  'update'=>'#RMPasienM_kelurahan_id'))); ?>
            <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                            array('class'=>'btn btn-primary','onclick'=>"{addKecamatan(); $('#dialogAddKecamatan').dialog('open');}",
                                                  'id'=>'btnAddKecamatan','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                  'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modPasien->getAttributeLabel('kecamatan_id'))) ?>
            <?php echo $form->error($modPasien, 'kecamatan_id'); ?>
        </div>
    </div>
    
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'kelurahan_id', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php $modPasien->kelurahan_id = (!empty($modPasien->kelurahan_id))?$modPasien->kelurahan_id:Yii::app()->user->getState('kelurahan_id');?>
            <?php echo $form->dropDownList($modPasien,'kelurahan_id',CHtml::listData($modPasien->getKelurahanItems($modPasien->kecamatan_id), 'kelurahan_id', 'kelurahan_nama'),
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                            array('class'=>'btn btn-primary','onclick'=>"{addKelurahan(); $('#dialogAddKelurahan').dialog('open');}",
                                                  'id'=>'btnAddKelurahan','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                  'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modPasien->getAttributeLabel('kelurahan_id'))) ?>
            <?php echo $form->error($modPasien, 'kelurahan_id'); ?>
        </div>
    </div>

    <?php //echo $form->textFieldRow($modPasien,'tgl_rekam_medik'); ?>
    <?php //echo $form->textFieldRow($modPasien,'statusrekammedis'); ?>
    <?php $modPasien->agama = (!empty($modPasien->agama))?$modPasien->agama:Params::DEFAULT_AGAMA;?>
    <?php echo $form->dropDownListRow($modPasien,'agama', Agama::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php echo $form->dropDownListRow($modPasien,'pendidikan_id', CHtml::listData($modPasien->getPendidikanItems(), 'pendidikan_id', 'pendidikan_nama'),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php echo $form->dropDownListRow($modPasien,'pekerjaan_id', CHtml::listData($modPasien->getPekerjaanItems(), 'pekerjaan_id', 'pekerjaan_nama'),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php $modPasien->warga_negara = (!empty($modPasien->warga_negara))?$modPasien->warga_negara:Params::DEFAULT_WARGANEGARA;?>
    <?php echo $form->dropDownListRow($modPasien,'warga_negara', WargaNegara::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    
    <?php //echo $form->textFieldRow($modPasien,'tgl_meninggal'); ?>
    
</fieldset>

<fieldset id="fieldsetDetailPasien" class="">
    <legend class="accord1"><?php echo CHtml::checkBox('cex_detaildatapasien', '', array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Detail Data Pasien 
    </legend>
    <div id='detail_data_pasien' class="toggle">
       
    <?php //echo $form->textFieldRow($modPasien,'jumlah_bersaudara',array('class'=>'span1','maxlength'=>2, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
     <div id='detail_data_pasien' class="toggle">
    <?php //echo $form->textFieldRow($modPasien,'anakke',array('class'=>'span1','maxlength'=>2, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'anakke', array('class'=>'control-label')) ?>
        <div class="controls">
            
        <?php echo $form->textField($modPasien,'anakke', array('maxlength'=>2,'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly','style'=>'width:30px;')).' dari '; ?> 
        <?php echo $form->textField($modPasien,'jumlah_bersaudara', array('maxlength'=>2,'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly','style'=>'width:30px;')).' bersaudara'; ?>
        <?php //echo CHtml::button('', array('class'=>'buttonTambahIcon','onclick'=>"{addPropinsi(); $('#dialogAddPropinsi').dialog('open');}",'id'=>'btnAddPropinsi',)) ?>
            <?php echo $form->error($modPasien, 'anakke'); ?><?php echo $form->error($modPasien, 'jumlah_bersaudara'); ?>
        </div>
    </div>
    <?php echo $form->textFieldRow($modPasien,'no_telepon_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly')); ?>
    <?php echo $form->textFieldRow($modPasien,'no_mobile_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2 numbersOnly')); ?>
    <?php echo $form->dropDownListRow($modPasien,'suku_id', CHtml::listData($modPasien->getSukuItems(), 'suku_id', 'suku_nama'),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
<!--    <img id="imgFotoPasien" src="" alt="photo pasien">-->
    <?php //echo $form->fileFieldRow($modPasien,'photopasien', array('onchange'=>'previewFoto(this)','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    <?php echo $form->textFieldRow($modPasien,'alamatemail', arraY('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    </div>
</fieldset>


<?php
$urlGetTglLahir = Yii::app()->createUrl('ActionAjax/GetTglLahir');
$urlGetUmur = Yii::app()->createUrl('ActionAjax/GetUmur');
$urlGetDaerah = Yii::app()->createUrl('ActionAjax/getListDaerahPasien');
$urlGetPendaftaran = Yii::app()->createUrl('ActionAjax/getListPendaftaran');
$urlGetJadwalKunjungan = Yii::app()->createUrl('ActionAjax/getListJadwalKunjungan');
$idTagUmur = CHtml::activeId($model,'umur');
$js = <<< JS
function previewFoto(obj)
{
    var pathFile = $(obj).val();
    $('#imgFotoPasien').attr('src','file://'+pathFile);
    $('#imgFotoPasien').load();
}

function enableInputPasien(obj)
{
    if(!obj.checked) {
        $('#fieldsetPasien input').removeAttr('disabled');
        $('#fieldsetPasien input').removeAttr('checked');
        $('#fieldsetPasien #isUpdatePasien').hide();
        $('#fieldsetPasien select').removeAttr('disabled');
        $('#fieldsetPasien textarea').removeAttr('disabled');
        $('#fieldsetPasien button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#controlNoRekamMedik button').attr('disabled','true');
        $('#noRekamMedik').attr('readonly','true');
        //$('#detail_data_pasien').slideUp(500);
        //$('#cex_detaildatapasien').removeAttr('checked','checked');

        $('#noRekamMedik').val('');
        $('#fieldsetPasien input').not(':radio').val('');
        $('#fieldsetPasien select').val('');
        $('#fieldsetPasien textarea').val('');
        $('#fieldsetPasien button').val('');
        $('#fieldsetDetailPasien input').val('');
        $('#fieldsetDetailPasien select').val('');
    }
    else {
        $('#fieldsetPasien input').attr('disabled','true');
        $('#fieldsetPasien #isUpdatePasien').show();
        $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
        $('#fieldsetPasien select').attr('disabled','true');
        $('#fieldsetPasien textarea').attr('disabled','true');
        $('#fieldsetPasien button').attr('disabled','true');
        $('#fieldsetDetailPasien input').attr('disabled','true');
        $('#fieldsetDetailPasien select').attr('disabled','true');
        $('#controlNoRekamMedik button').removeAttr('disabled');
        $('#noRekamMedik').removeAttr('readonly');
        $('#detail_data_pasien').slideDown(500);
        $('#cex_detaildatapasien').attr('checked','checked');
    }
}

function updateInputPasien(obj)
{
    if(!obj.checked) {
        $('#fieldsetPasien input').attr('disabled','true');
        $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
        $('#fieldsetPasien select').attr('disabled','true');
        $('#fieldsetPasien textarea').attr('disabled','true');
        $('#fieldsetPasien button').attr('disabled','true');
        $('#fieldsetDetailPasien input').attr('disabled','true');
        $('#fieldsetDetailPasien select').attr('disabled','true');
        $('#controlNoRekamMedik button').removeAttr('disabled');
        $('#noRekamMedik').removeAttr('readonly');
        //$('#detail_data_pasien').slideDown(500);
        //$('#cex_detaildatapasien').attr('checked','checked');
    }
    else {
        $('#fieldsetPasien input').removeAttr('disabled');
        $('#fieldsetPasien select').removeAttr('disabled');
        $('#fieldsetPasien textarea').removeAttr('disabled');
        $('#fieldsetPasien button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#controlNoRekamMedik button').attr('disabled','true');
        $('#noRekamMedik').attr('readonly','true');
        //$('#detail_data_pasien').slideUp(500);
        //$('#cex_detaildatapasien').removeAttr('checked','checked');
    }
}

function getTglLahir(obj)
{
    var str = obj.value;
    obj.value = str.replace(/_/gi, "0");
    $.post("${urlGetTglLahir}",{umur: obj.value},
        function(data){
           $('#RMPasienM_tanggal_lahir').val(data.tglLahir); 
    },"json");
}

function getUmur(obj)
{
    //alert(obj.value);
    if(obj.value == '')
        obj.value = 0;
    $.post("${urlGetUmur}",{tglLahir: obj.value},
        function(data){

           $('#RMPendaftaranRj_umur').val(data.umur); 
           $('#RMPendaftaranMp_umur').val(data.umur); 
           $('#RMPendaftaranRd_umur').val(data.umur); 

           $("#${idTagUmur}").val(data.umur);
    },"json");
}

function loadUmur(tglLahir)
{
    $.post("${urlGetUmur}",{tglLahir: tglLahir},
        function(data){
           $("#${idTagUmur}").val(data.umur);
    },"json");
}
           
function cariPendaftaranRehabMedis(idPasien)
{
   $.post("${urlGetPendaftaran}",{idPasien: idPasien},
        function(data){
           if(data.status == 'Ada')
           {
              $("#pendaftaranAda").attr("checked","true");
              $("#divPendaftaran").html(data.form);
              $('#divPendaftaran').slideToggle(500);
              $('#tblFormTindakanRM').hide();
           }
           else
           {
              $("#pendaftaranAda").removeAttr("checked");
              $("#listPendaftaran").remove();
              $('#tblFormTindakanRM').show();
           }
           
    },"json");
}
   
function getListJadwalKunjungan(idPendaftaran)
{
   $.post("${urlGetJadwalKunjungan}",{idPendaftaran : idPendaftaran},
        function(data){
           if(data.status == 'Ada')
           {
              $("#divJadwalKunjungan").html(data.form);
              $("#RMPendaftaranMp_jeniskasuspenyakit_id").val(data.jeniskasuspenyakit_id);
              $("#RMPendaftaranMp_pegawai_id").val(data.pegawai_id);
              $("#RMPendaftaranMp_carabayar_id").val(data.carabayar_id);
              $("#RMPendaftaranMp_penjamin_id").val(data.penjamin_id);
              $("#RMRujukanT_asalrujukan_id").val(data.asalrujukan_id);
              $("#RMRujukanT_no_rujukan").val(data.no_rujukan);
              $("#RMRujukanT_tanggal_rujukan").val(data.tanggal_rujukan);
              jQuery('.dtPicker3').datetimepicker(jQuery.extend({showMonthAfterYear:false}, 
              jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','minDate'  : 'd','timeText':'Waktu','hourText':'Jam',
                 'minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih   Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
              $('#divJadwalKunjungan').slideToggle(500);
          }
           else
           {
//              $("#divJadwalKunjungan").remove();
           }
           
    },"json");
}

function setJenisKelaminPasien(jenisKelamin)
{
    $('input[name="RMPasienM[jeniskelamin]"]').each(function(){
            if(this.value == jenisKelamin)
                $(this).attr('checked',true);
        }
    );
}

function setRhesusPasien(rhesus)
{
    $('input[name="RMPasienM[rhesus]"]').each(function(){
            if(this.value == rhesus)
                $(this).attr('checked',true);
        }
    );
}

function loadDaerahPasien(idProp,idKab,idKec)
{
    $.post("${urlGetDaerah}", { idProp: idProp, idKab: idKab, idKec: idKec },
        function(data){
            $('#RMPasienM_propinsi_id').html(data.listPropinsi);
            $('#RMPasienM_kabupaten_id').html(data.listKabupaten);
            $('#RMPasienM_kecamatan_id').html(data.listKecamatan);
            // $('#RMPasienM_kelurahan_id').html(data.listKelurahan);
    }, "json");
}

function clearKecamatan()
{
    $('#RMPasienM_kecamatan_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}

function clearKelurahan()
{
    $('#RMPasienM_kelurahan_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}
JS;
Yii::app()->clientScript->registerScript('formPasien',$js,CClientScript::POS_HEAD);

$enableInputPasien = ($model->isPasienLama) ? 1 : 0;
$js = <<< JS
if(${enableInputPasien}) {
    $('#fieldsetPasien input').attr('disabled','true');
    $('#fieldsetPasien #isUpdatePasien').show();
    $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
    $('#fieldsetPasien select').attr('disabled','true');
    $('#fieldsetPasien textarea').attr('disabled','true');
    $('#fieldsetDetailPasien input').attr('disabled','true');
    $('#fieldsetDetailPasien select').attr('disabled','true');
    $('#noRekamMedik').removeAttr('readonly');
    $('#controlNoRekamMedik button').removeAttr('disabled');
    $('#fieldsetPasien button').attr('disabled','true');
}
else {
    $('#fieldsetPasien input').removeAttr('disabled');
    $('#fieldsetPasien #isUpdatePasien').hide();
    $('#fieldsetPasien select').removeAttr('disabled');
    $('#fieldsetPasien textarea').removeAttr('disabled');
    $('#fieldsetDetailPasien input').removeAttr('disabled');
    $('#fieldsetDetailPasien select').removeAttr('disabled');
    $('#noRekamMedik').attr('readonly','true');
    $('#controlNoRekamMedik button').attr('disabled','true');
    $('#fieldsetPasien button').removeAttr('disabled');
}
JS;
Yii::app()->clientScript->registerScript('formPasien',$js,CClientScript::POS_READY);


$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

<?php 
//========= Dialog buat cari data pasien =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modDataPasien = new RMPasienM('searchWithDaerahPenunjang');
$modDataPasien->unsetAttributes();
if(isset($_GET['RMPasienM'])) {
    $modDataPasien->attributes = $_GET['RMPasienM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasien-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modDataPasien->searchWithDaerahPenunjang(),
	'filter'=>$modDataPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "
                                                $(\"#dialogPasien\").dialog(\"close\");
                                                $(\"#noRekamMedik\").val(\"$data->no_rekam_medik\");
                                                $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                                setJenisKelaminPasien(\"$data->jeniskelamin\");
                                                setRhesusPasien(\"$data->rhesus\");
                                                loadDaerahPasien($data->propinsi_id,$data->kabupaten_id,$data->kecamatan_id);
                                                $(\"#'.CHtml::activeId($modPasien,'pasien_id').'\").val(\"$data->pasien_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'jenisidentitas').'\").val(\"$data->jenisidentitas\");
                                                $(\"#'.CHtml::activeId($modPasien,'no_identitas_pasien').'\").val(\"$data->no_identitas_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'namadepan').'\").val(\"$data->namadepan\");
                                                $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'nama_bin').'\").val(\"$data->nama_bin\");
                                                $(\"#'.CHtml::activeId($modPasien,'tempat_lahir').'\").val(\"$data->tempat_lahir\");
                                                $(\"#'.CHtml::activeId($modPasien,'tanggal_lahir').'\").val(\"$data->tanggal_lahir\");
                                                $(\"#'.CHtml::activeId($modPasien,'kelompokumur_id').'\").val(\"$data->kelompokumur_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'jeniskelamin').'\").val(\"$data->jeniskelamin\");
                                                $(\"#'.CHtml::activeId($modPasien,'statusperkawinan').'\").val(\"$data->statusperkawinan\");
                                                $(\"#'.CHtml::activeId($modPasien,'golongandarah').'\").val(\"$data->golongandarah\");
                                                $(\"#'.CHtml::activeId($modPasien,'rhesus').'\").val(\"$data->rhesus\");
                                                $(\"#'.CHtml::activeId($modPasien,'alamat_pasien').'\").val(\"$data->alamat_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'rt').'\").val(\"$data->rt\");
                                                $(\"#'.CHtml::activeId($modPasien,'rw').'\").val(\"$data->rw\");
                                                $(\"#'.CHtml::activeId($modPasien,'propinsi_id').'\").val(\"$data->propinsi_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'kabupaten_id').'\").val(\"$data->kabupaten_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'kecamatan_id').'\").val(\"$data->kecamatan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'kelurahan_id').'\").val(\"$data->kelurahan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'no_telepon_pasien').'\").val(\"$data->no_telepon_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'no_mobile_pasien').'\").val(\"$data->no_mobile_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'suku_id').'\").val(\"$data->suku_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'alamatemail').'\").val(\"$data->alamatemail\");
                                                $(\"#'.CHtml::activeId($modPasien,'anakke').'\").val(\"$data->anakke\");
                                                $(\"#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'\").val(\"$data->jumlah_bersaudara\");
                                                $(\"#'.CHtml::activeId($modPasien,'pendidikan_id').'\").val(\"$data->pendidikan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'pekerjaan_id').'\").val(\"$data->pekerjaan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'agama').'\").val(\"$data->agama\");
                                                $(\"#'.CHtml::activeId($modPasien,'warga_negara').'\").val(\"$data->warga_negara\");
                                                loadUmur(\"$data->tanggal_lahir\");
                                                cariPendaftaranRehabMedis(\"$data->pasien_id\");
                                            "))',
                        ),
                'no_rekam_medik',
                'nama_pasien',
                'nama_bin',
                'alamat_pasien',
                'rw',
                'rt',
                array(
                    'name'=>'propinsiNama',
                    'value'=>'$data->propinsi->propinsi_nama',
                ),
                array(
                    'name'=>'kabupatenNama',
                    'value'=>'$data->kabupaten->kabupaten_nama',
                ),
                array(
                    'name'=>'kecamatanNama',
                    'value'=>'$data->kecamatan->kecamatan_nama',
                ),
                array(
                    'name'=>'kelurahanNama',
                    'value'=>'$data->kelurahan->kelurahan_nama',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pasien dialog =============================
?>

<?php 
// Dialog buat nambah data propinsi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddPropinsi',
    'options'=>array(
        'title'=>'Menambah data Propinsi',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>450,
        'minHeight'=>350,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end propinsi dialog =============================

// Dialog buat nambah data kabupaten =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddKabupaten',
    'options'=>array(
        'title'=>'Menambah data Kabupaten',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormKabupaten"></div>';


$this->endWidget();
//========= end kabupaten dialog =============================

// Dialog buat nambah data kecamatan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddKecamatan',
    'options'=>array(
        'title'=>'Menambah data Kecamatan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormKecamatan"></div>';


$this->endWidget();
//========= end kecamatan dialog =============================

// Dialog buat nambah data kelurahan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddKelurahan',
    'options'=>array(
        'title'=>'Menambah data Kelurahan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormKelurahan"></div>';


$this->endWidget();
//========= end kelurahan dialog =============================
?>

<script type="text/javascript">
// here is the magic
function addPropinsi()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addPropinsi'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddPropinsi div.divForForm').html(data.div);
                    $('#dialogAddPropinsi div.divForForm form').submit(addPropinsi);
                }
                else
                {
                    $('#dialogAddPropinsi div.divForForm').html(data.div);
                    $('#RMPasienM_propinsi_id').html(data.propinsi);
                    setTimeout(\"$('#dialogAddPropinsi').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addKabupaten()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addKabupaten'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddKabupaten div.divForFormKabupaten').html(data.div);
                    $('#dialogAddKabupaten div.divForFormKabupaten form').submit(addKabupaten);
                }
                else
                {
                    $('#dialogAddKabupaten div.divForFormKabupaten').html(data.div);
                    $('#RMPasienM_kabupaten_id').html(data.kabupaten);
                    setTimeout(\"$('#dialogAddKabupaten').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addKecamatan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addKecamatan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddKecamatan div.divForFormKecamatan').html(data.div);
                    $('#dialogAddKecamatan div.divForFormKecamatan form').submit(addKecamatan);
                }
                else
                {
                    $('#dialogAddKecamatan div.divForFormKecamatan').html(data.div);
                    $('#RMPasienM_kecamatan_id').html(data.kecamatan);
                    setTimeout(\"$('#dialogAddKecamatan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addKelurahan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addKelurahan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddKelurahan div.divForFormKelurahan').html(data.div);
                    $('#dialogAddKelurahan div.divForFormKelurahan form').submit(addKelurahan);
                }
                else
                {
                    $('#dialogAddKelurahan div.divForFormKelurahan').html(data.div);
                    $('#RMPasienM_kelurahan_id').html(data.kelurahan);
                    setTimeout(\"$('#dialogAddKelurahan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}
</script>

<?php Yii::app()->clientScript->registerScript('detail_data_pasien',"
    $('#detail_data_pasien').hide();
    $('#cex_detaildatapasien').change(function(){
        if ($(this).is(':checked')){
                $('#fieldsetDetailPasien input').not('input[type=checkbox]').removeAttr('disabled');
                $('#fieldsetDetailPasien select').removeAttr('disabled');
        }else{
                $('#fieldsetDetailPasien input').not('input[type=checkbox]').attr('disabled','true');
                $('#fieldsetDetailPasien select').attr('disabled','true');
                $('#fieldsetDetailPasien input').attr('value','');
                $('#fieldsetDetailPasien select').attr('value','');
        }
        $('#detail_data_pasien').slideToggle(500);
    });
");
?>
<script type="text/javascript">
// here is the magic
function convertToUpper(obj)
{
    var string = obj.value;
    $(obj).val(string.toUpperCase());
}

function setNamaGelar()
{
    var statusperkawinan = $('#PPPasienM_statusperkawinan').val();
    var namadepan = $('#PPPasienM_namadepan');
    var umur = $("#<?php echo CHtml::activeId($model,'umur');?>").val().substr(0,2);
    umur = parseInt(umur);
    if(umur <= 5){
        var namadepan = $('#PPPasienM_namadepan').val('BY. Ny.');
        if(statusperkawinan.length > 0){
            $('#PPPasienM_statusperkawinan').val('');
            alert('Maaf status perkawinan belum cukup usia');
        }
    }else if(umur <= 15){
        var namadepan = $('#PPPasienM_namadepan').val('An.');
        if(statusperkawinan.length > 0){
            $('#PPPasienM_statusperkawinan').val('');
            alert('Maaf status perkawinan belum cukup usia');
        }
    }else{
        if($('#PPPasienM_jeniskelamin_0').is(':checked')){
            if(statusperkawinan !== 'JANDA'){
                var namadepan = $('#PPPasienM_namadepan').val('Tn.');
            }else{
                alert('Pilih status pernikahan yang sesuai');
                $('#PPPasienM_statusperkawinan').val('KAWIN');
                var namadepan = $('#PPPasienM_namadepan').val('Tn.');
            }
        }
        
        if($('#PPPasienM_jeniskelamin_1').is(':checked')){
            if(statusperkawinan !== 'DUDA'){
                if(statusperkawinan === 'KAWIN' || statusperkawinan == 'JANDA' || statusperkawinan == 'NIKAH SIRIH' || statusperkawinan == 'POLIGAMI'){
                    var namadepan = $('#PPPasienM_namadepan').val('Ny.');
                }else{
                    var namadepan = $('#PPPasienM_namadepan').val('Nn');
                }                
            }else{
                alert('Pilih status pernikahan yang sesuai');
                $('#PPPasienM_statusperkawinan').val('KAWIN');
                var namadepan = $('#PPPasienM_namadepan').val('Ny.');
            }
        }        
    }
    
}

function cekJenisKelamin(obj)
{
    var is_true = true;
    var namadepan = $('#PPPasienM_namadepan').val();
    if(namadepan.length != 0)
    {
        if(obj.value == 'PEREMPUAN')
        {
            if(namadepan != 'Nn.' && namadepan != 'Ny.' && namadepan != 'BY. Ny.')
            {
                alert('Pilih Jenis kelamin yang sesuai');
                $('#PPPasienM_jeniskelamin_0').attr('checked',true);
                is_true = false;
            }
        }else{
            if(namadepan != 'Tn.' && namadepan != 'An.' && namadepan != 'BY. Ny.')
            {
                alert('Pilih Jenis kelamin yang sesuai');
                $('#PPPasienM_jeniskelamin_1').attr('checked',true);
                is_true = false;
            }
        }
    }else{
        $(obj).attr('checked',false);
        alert('Pilih gelar kehormatan terlebih dahulu');
    }
}

function setValueStatus(obj)
{
    var gelar = obj.value;
    if(gelar === 'Tn.')
    {
        $('#PPPasienM_jeniskelamin_0').attr('checked',true);
        $('#PPPasienM_statusperkawinan').val('KAWIN');
        
    }else if(gelar === 'An.'){
        $('#PPPasienM_jeniskelamin_0').attr('checked',true);
        $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
    }else{
        if(gelar === 'Nn' || gelar === 'BY. Ny.')
        {
            $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
        }else{
            $('#PPPasienM_statusperkawinan').val('KAWIN');
        }
        $('#PPPasienM_jeniskelamin_1').attr('checked',true);
    }
}

function setStatusPerkawinan(obj)
{
    var status = obj.value;
    var namaDepan = $('#PPPasienM_namadepan').val();
    
    if(status === 'BELUM KAWIN')
    {
        if(namaDepan !== 'An.' && namaDepan !== 'Nn' && namaDepan !== 'BY. Ny.')
        {
            alert('Pilih status perkawinan yang sesuai');
            $('#PPPasienM_statusperkawinan').val('KAWIN');
        }
    }else{
        if(status === 'KAWIN' || status === 'NIKAH SIRIH' || status === 'POLIGAMI')
        {
            if(namaDepan !== 'Tn.' && namaDepan !== 'Ny.')
            {
                alert('Pilih status perkawinan yang sesuai');
                $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
            }
        }
        else if(status === 'JANDA')
        {
            if(namaDepan !== 'Ny.')
            {
                alert('Pilih status perkawinan yang sesuai');
                if(namaDepan === 'Tn.')
                {
                    $('#PPPasienM_statusperkawinan').val('KAWIN');
                }else{
                    $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
                }
            }
        }
        else if(status === 'DUDA')
        {
            if(namaDepan !== 'Tn.')
            {
                alert('Pilih status perkawinan yang sesuai');
                if(namaDepan === 'Ny.')
                {
                    $('#PPPasienM_statusperkawinan').val('KAWIN');
                }else{
                    $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
                }
            }
        }
    }
}

function cekStatusPekerjaan(obj)
{
    var namaDepan = $('#PPPasienM_namadepan').val();
    var namaPekerjaan = obj.value;
    var umur = $("#<?php echo CHtml::activeId($model,'umur');?>").val().substr(0,2);
    umur = parseInt(umur);
    
    if(namaDepan.length > 0)
    {
        if(umur < 15){
            if(namaPekerjaan !== '12'){
                if(namaPekerjaan !== ''){
                    alert('Pasien masih di bawah umur, coba cek ulang');
                }
                $(obj).val('');
            }else{
                $(obj).val(namaPekerjaan);
            }
        }else{
            if(namaPekerjaan === '12'){
                if(namaDepan === 'Ny.'){
                    $(obj).val('9');
                }else if(namaDepan === 'Nn' && namaPekerjaan === '9'){
                    alert('Pasien belum menikah, coba cek ulang');
                    $(obj).val('');
                }else{
                    $(obj).val('');
                }
                alert('Pilih pekerjaan yang tepat');
            }else{
                if(namaPekerjaan === '9'){
                    if(namaDepan !== 'Ny.'){
                        alert('Pasien seorang laki - laki, coba cek ulang');
                        $(obj).val('');                        
                    }
                }
            }
        }
/*        
        if(namaPekerjaan === '12' && umur < 17)
        {
            if(namaDepan !== 'BY. Ny.' && namaDepan !== 'An.' && namaDepan !== 'Nn')
            {
                alert('Pilih pekerjaan yang sesuai');
                $(obj).val('');
            }
        }else{
            if(namaDepan === 'BY. Ny.')
            {
                alert('Pilih pekerjaan yang sesuai');
                $(obj).val('');
            }else{
                if(namaPekerjaan === '11' || namaPekerjaan === '10')
                {
                    if(namaDepan !== 'An.' && namaDepan !== 'Nn'){
                        alert('Pilih pekerjaan yang sesuai');
                        $(obj).val('');
                    }
                }else{
                    if(namaPekerjaan !== '13' && namaPekerjaan !== '14')
                    {
                        if(namaPekerjaan === '9' && namaDepan !== 'Ny.')
                        {
                            alert('Pilih pekerjaan yang sesuai');
                            $(obj).val('');
                        }else{
                            if((namaDepan === 'An.' || namaDepan === 'Nn') && umur < 25){
                                alert('Pilih pekerjaan yang sesuai');
                                $(obj).val('');
                            }                        
                        }
                    }
                }
            }
        }
*/
    }else{
        $(obj).val('');
        alert('Pilih gelar kehormatan terlebih dahulu');
    }

}
</script>
