<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-mp-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset>
        <legend class="rim2">Transaksi  Pendaftaran Pasien Luar</legend>
        <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        
        <?php echo $form->errorSummary(array($model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienPenunjang,$modHasilPemeriksaan)); ?>
        
        <table class='table-condensed'>
            <tr>
                <td width="50%">
                    <div class='control-group'>
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                            )); ?>
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>
                        </div>
                    </div>
                    
                    <?php echo $this->renderPartial('_formPasien', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien)); ?>
                    
                </td>
                <td width="50%">
                    
                    <?php echo $this->renderPartial('_formCariPendaftaran', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien)); ?>
                    
                    <?php echo $form->textFieldRow($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData(RuanganrehabmedisV::model()->findAll(), 'ruangan_id', 'ruangan_nama'), array('empty'=>'--Pilih--','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                            'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'RMPendaftaranMp')),
                                                              'update'=>'#RMPendaftaranMp_jeniskasuspenyakit_id'),'onChange'=>'listDokterRuangan(this.value);'
                                                            )); ?>

                    <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems($model->ruangan_id), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'RMPendaftaranMp')), 
                                                    'update'=>'#'.CHtml::activeId($model,'penjamin_id').''  //selector to update
                                                ),
                        )); ?>
                    
                    <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>

                    <?php echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form)); ?>

                    <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>
                    
                    <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan)); ?>
                    
                    <?php echo $this->renderPartial('_formTindakanRM', 
                                array('model'=>$model,'form'=>$form,'modHasilPemeriksaan'=>$modHasilPemeriksaan,'modJenisTindakan'=>$modJenisTindakan,
                                      'modTindakan'=>$modTindakan,'modTindakanKomponen'=>$modTindakanKomponen,
                                      'modTindakanPelayanan'=>$modTindakanPelayanan)); ?>

                </td>
            <tr>
        </table>
    </fieldset>

    <div class='form-actions'>
        <?php
            if($model->isNewRecord==true){
                echo CHtml::htmlButton( Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); 
            }else{
                echo CHtml::htmlButton( Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('disabled'=>true, 'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); 
            }
        ?>


        <?php //echo CHtml::htmlButton($model->isNewRecord==true ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
        //Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        //array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('index'), array('class'=>'btn btn-danger')); ?>
        <?php if(!$model->isNewRecord) echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false")); ?>
		
<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
	
    </div>

<?php $this->endWidget(); ?>

<?php //$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>'penjelasan transaksi')); ?>

<?php $url = CController::createUrl('ActionDynamic/GetKasusPenyakit'); ?>

<?php
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoli',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$jscript = <<< JS
function print(idPendaftaran)
{
        if(document.getElementById('isPasienLama').checked == true){
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=280');
        }else{
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');
            window.open('${urlPrintKartuPasien}'+idPendaftaran,'printwi','left=100,top=100,width=400,height=280');
        }
}

function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#RMPendaftaranMp_pegawai_id').html(data.listDokter);
    }, "json");
}
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
