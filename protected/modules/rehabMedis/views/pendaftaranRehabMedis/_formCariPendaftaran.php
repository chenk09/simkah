<fieldset>
    <legend>
        <?php echo CHtml::checkBox('pendaftaranAda', false , array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                  'rel'=>"tooltip",'data-title'=>"Cek apabila sudah mempunyai jadwal")) ?>
        No Pendaftaran
    </legend>
    <div id="divPendaftaran" class="control-group toggle" style="display: none;">
        
    </div>
    
    <div id="divJadwalKunjungan" class="control-group toggle" style="display: none;">

    </div>
</fieldset>

<?php
$js = <<< JS


$('#pendaftaranAda').change(function(){
        if ($(this).is(':checked')){
                $('#divPendaftaran input').removeAttr('disabled');
                $('#divPendaftaran select').removeAttr('disabled');
                $('#tblFormTindakanRM').hide();
        }else{
                $('#divPendaftaran input').attr('disabled','true');
                $('#divPendaftaran select').attr('disabled','true');
                $('#tblFormTindakanRM').show();
        }
        $('#divPendaftaran').slideToggle(500);
        $('#divJadwalKunjungan').slideToggle(500);
        
    });
    
JS;
Yii::app()->clientScript->registerScript('divPendaftaran',$js,CClientScript::POS_READY);
?>