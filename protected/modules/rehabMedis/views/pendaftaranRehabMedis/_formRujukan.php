
<fieldset class="span6">
    <legend class="accord1">
        <?php echo CHtml::checkBox('isRujukan', $model->isRujukan, array('onkeypress'=>"return $(this).focusNextInputField(event)")); //, 'onclick'=>'return false;' ?>
        Rujukan
    </legend>
    <div id="divRujukan" class="toggle <?php echo ($model->isRujukan) ? '':'hide' ?> toggle" >
        <div class="control-group ">
                    <?php echo $form->labelEx($modRujukan,'asalrujukan_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->dropDownList($modRujukan,'asalrujukan_id',
                                       CHtml::listData($modRujukan->getAsalRujukanItems(), 'asalrujukan_id', 'asalrujukan_nama'),
                                       array('class'=>'span3 ', 'onkeypress'=>"return $(this).focusNextInputField(event)",'onChange'=>'setAsalRujukanId(this);',
                                       'empty'=>'-- Pilih --','disabled'=>true)); ?>
                    <?php 
//                    echo $form->dropDownList($modRujukan,'asalrujukan_id', CHtml::listData($modRujukan->getAsalRujukanItems(), 'asalrujukan_id', 'asalrujukan_nama'), 
//                                                      array('empty'=>'-- Pilih --', 'onkeypress'=>"return nextFocus(this,event,'".CHtml::activeId($modRujukan, 'nama_perujuk')."')", 
//                                                            'ajax'=>array('type'=>'POST',
//                                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetRujukanDari',array('encode'=>false,'namaModel'=>'PPRujukanT')),
//                                                                          'update'=>'#PPRujukanT_rujukandari_id',),
//                                                            'onchange'=>"clearRujukan();",)); ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                                        array('class'=>'btn btn-primary','onclick'=>"{addAsalRujukan(); $('#dialogAddAsalRujukan').dialog('open');}",
                                                              'id'=>'btnAddAsalRujukan','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modRujukan->getAttributeLabel('asalrujukan_id'))) ?>
                        <?php echo $form->error($modRujukan, 'asalrujukan_id'); ?>
                    </div>
                </div>
        <?php //echo $form->dropDownListRow($modRujukan,'asalrujukan_id', CHtml::listData($modRujukan->getAsalRujukanItems(), 'asalrujukan_id', 'asalrujukan_nama'), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
       <?php //echo $form->dropDownListRow($modRujukan,'nama_perujuk', CHtml::listData(RujukandariM::model()->findAll(), 'rujukandari_id', 'namaperujuk'), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
       <div class="control-group ">
            <?php echo $form->labelEx($modRujukan,'nama_perujuk', array('class'=>'control-label')); ?>
            <div class="controls">
                <div style="float:left; width:208px;">
                    <?php echo CHtml::activeHiddenField($modRujukan, 'rujukandari_id');?> 
                    <?php $this->widget('MyJuiAutoComplete',array(
                            'model'=>$modRujukan,
                            'attribute'=>'nama_perujuk',
                             'sourceUrl'=>'js:function(request,response){
                                        asalRujukanId = $("#RMRujukanT_asalrujukan_id").val();
                                        $.get("'.Yii::app()->createUrl('actionAutoComplete/RujukanDari').'&asalRujukanId="+asalRujukanId,request,response,"json")
                                    }',
                            'options'=>array(
                               'showAnim'=>'fold',
                               'minLength' => 2,
                               'select'=>'js:function( event, ui ) {
                                          $("#RMRujukanT_rujukandari_id").val(ui.item.rujukandari_id);
                                }',
                            ),
                            'htmlOptions'=>array(
                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                'class'=>'span3',
//                                'onfocus'=>'cekAsalRujukan()', 
                            ),
                            'tombolDialog'=>array('idDialog'=>'dialogRujukanDari', 'jsFunction'=>'if(cekAsalRujukan()){ $("#dialogRujukanDari").dialog(\'open\');}'),

                )); ?>
                </div>
                <!--<div style="float:left;">-->
                <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                            array('class'=>'btn btn-primary','onclick'=>"{addNamaRujukan(); $('#dialogAddNamaRujukan').dialog('open');}",
                                                  'id'=>'btnAddNamaRujukan','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                  'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modRujukan->getAttributeLabel('nama_perujuk'))) ?>
                <!--</div>-->
                <?php 
//                echo $form->dropDownList($modRujukan,'rujukandari_id',CHtml::listData($modRujukan->getDaftarRujukanItems($modRujukan->asalrujukan_id), 'rujukandari_id', 'namaperujuk'),
//                                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return nextFocus(this,event,'".CHtml::activeId($modRujukan, 'asalrujukan_id')."')")); ?>
                
                <?php echo $form->error($modRujukan, 'nama_perujuk'); ?>
            </div>
        </div>
        <?php echo $form->textFieldRow($modRujukan,'no_rujukan', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No. Rujukan')); ?>
        <div class="control-group ">
            <?php echo $form->labelEx($modRujukan,'tanggal_rujukan', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modRujukan,
                                        'attribute'=>'tanggal_rujukan',
                                        'mode'=>'date',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                )); ?>
                <?php echo $form->error($modRujukan, 'tanggal_rujukan'); ?>
            </div>
        </div>
        <?php echo $form->textFieldRow($modRujukan,'diagnosa_rujukan', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Diagnosa Rujukan')); ?> 
    </div>
</fieldset>

<?php

// Dialog buat nambah data kabupaten =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddAsalRujukan',
    'options'=>array(
        'title'=>'Menambah data Asal Rujukan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormAsalRujukan"></div>';


$this->endWidget();
//========= end kabupaten dialog =============================

// Dialog buat nambah data kabupaten =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddNamaRujukan',
    'options'=>array(
        'title'=>'Menambah data Nama Rujukan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormNamaRujukan"></div>';


$this->endWidget();
//========= end kabupaten dialog =============================

?>

<?php
$enableInputPJ = ($model->isRujukan) ? 1 : 0;
$urlGetAsalRujukan = Yii::app()->createUrl('actionAjax/getAsalRujukan');
$urlHalamanIni = Yii::app()->createUrl($this->route);
$js = <<< JS
if(${enableInputPJ}) {
    $('#divRujukan input').removeAttr('disabled');
    $('#divRujukan select').removeAttr('disabled');
}
else {
    $('#divRujukan input').attr('disabled','true');
    $('#divRujukan select').attr('disabled','true');
}

$('#isRujukan').change(function(){
    if ($(this).is(':checked')){
            $('#divRujukan input').removeAttr('disabled');
            $('#divRujukan select').removeAttr('disabled');
    }else{
            $('#divRujukan input').attr('disabled','true');
            $('#divRujukan select').attr('disabled','true');
    }
    $('#divRujukan').slideToggle(500);
});

function clearRujukan()
{
    $('#RMRujukanT_nama_perujuk').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}

JS;
Yii::app()->clientScript->registerScript('rujukan',$js,CClientScript::POS_READY);
       
?>



<script>

    
function addNamaRujukan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addNamaRujukan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddNamaRujukan div.divForFormNamaRujukan').html(data.div);
                    $('#dialogAddNamaRujukan div.divForFormNamaRujukan form').submit(addNamaRujukan);
                }
                else
                {
                    $('#dialogAddNamaRujukan div.divForFormNamaRujukan').html(data.div);
                    $('#RMRujukanT_nama_perujuk').html(data.namarujukan);
                    $('#RMRujukanT_nama_perujuk').val(data.namaperujuk);
                    $('#RMRujukanT_rujukandari_id').val(data.rujukandari_id);
                    $('#RMRujukanT_asalrujukan_id').val(data.asalrujukan_id);
                    setTimeout(\"$('#dialogAddNamaRujukan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addAsalRujukan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addAsalRujukan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddAsalRujukan div.divForFormAsalRujukan').html(data.div);
                    $('#dialogAddAsalRujukan div.divForFormAsalRujukan form').submit(addAsalRujukan);
                }
                else
                {
                    $('#dialogAddAsalRujukan div.divForFormAsalRujukan').html(data.div);
                    $('#RMRujukanT_asalrujukan_id').html(data.asalrujukan);
                    $('#RMRujukanT_asalrujukan_id').val(data.asalrujukan_id);
                    setTimeout(\"$('#dialogAddAsalRujukan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}
function setAsalRujukanId(obj){
    id = $(obj).val();
//    alert('set asalrujukan id'+id);
//    $("#LKRujukanT_asalrujukan_id").val(id);
    $.fn.yiiGridView.update('rujukandari-m-grid', {
		data: {'RujukandariM[asalrujukan_id]':id}
	});
    
}
function cekAsalRujukan(){
    if($("#PPRujukanT_asalrujukan_id").val() == ""){
        alert("Silahkan isi terlebih dahulu Asal Rujukan !");
        return false;
    }else{
        return true;
    }
}
</script>

<?php 
//========= Dialog buat cari data Perujuk =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRujukanDari',
    'options'=>array(
        'title'=>'Pencarian Perujuk',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modRujukanDari = new RujukandariM('search');
$modRujukanDari->unsetAttributes();
$modRujukanDari->asalrujukan_id = 1; 
if(isset($_GET['RujukandariM'])) {
    $modRujukanDari->attributes = $_GET['RujukandariM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'rujukandari-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRujukanDari->search(),
	'filter'=>$modRujukanDari,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "href"=>"",
                                            "onClick" => "$(\"#RMRujukanT_rujukandari_id\").val(\"$data->rujukandari_id\");
                                                          $(\"#RMRujukanT_nama_perujuk\").val(\"$data->namaperujuk\");
                                                          $(\"#'.CHtml::activeId($modRujukan,'namaperujuk').'\").val(\"".$data->namaperujuk."\");
                                                          $(\"#dialogRujukanDari\").dialog(\"close\"); 
                                                          return false;
                                                "))',
                        ),
//                'asalrujukan_id',
                'namaperujuk',
                'spesialis',
                'alamatlengkap',
                'notelp',
               
	),
        'afterAjaxUpdate' => 'function(id, data){
        $("#RujukandariM_asalrujukan_id").val($("#RMRujukanT_asalrujukan_id").val());
        jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
$this->endWidget();
//========= end Perujuk dialog =============================
?>