
<fieldset>
    <legend class="rim2">Informasi Pasien Rujukan</legend>

<?php 
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasienpenunjangrujukan-m-grid',
	'dataProvider'=>$dataProvider,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tgl_pendaftaran',
                'tgl_kirimpasien',
                array(
                    'header'=>'Instalasi / Ruangan Asal',
                    'value'=>'$data->InstalasiNamaRuanganNama',
                ),
                'no_pendaftaran',
                'no_rekam_medik',
                array(
                    'header'=>'Nama Pasien',
                    'value'=>'$data->NamaPasienNamaBin',
                ),
                array(
                    'header'=>'Cara Bayar / Penjamin',
                    'value'=>'$data->CaraBayarPenjaminNama',
                ),
                'jeniskasuspenyakit_nama',
//                'umur',
                'alamat_pasien',
//                'pemeriksaanrad_nama',
                array(
                    'header'=>'&nbsp;&nbsp;Pemeriksaan&nbsp;&nbsp;',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\'icon-user\'></i>",Yii::app()->controller->createUrl("rujukanPenunjang/masukPenunjang/",array("idPasienKirimKeUnitLain"=>$data->pasienkirimkeunitlain_id,"idPendaftaran"=>$data->pendaftaran_id)),
                                    array("class"=>"icon-user", 
                                          "id" => "selectPasien",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk rencana operasi pasien",
                                          "target"=>"blank",
                                    ))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->renderPartial('_formSearch',array()); 
?>
</fieldset>
