<table class="table-condensed">
    <tr>
        <td width="50%">
            <div class="box">
                <?php foreach($modJenisTindakan as $i=>$jenisTindakan){ 
                        $ceklist = false;
                ?>
                        <div class="boxtindakan">
                            <h6><?php echo $jenisTindakan->jenistindakanrm_nama; ?></h6>
                            <?php foreach ($modTindakan as $j => $tindakanRM) {
                                      if($jenisTindakan->jenistindakanrm_id == $tindakanRM->jenistindakanrm_id) {
                                         echo CHtml::checkBox("tindakanrm_id[]", $ceklist, array('value'=>$tindakanRM->tindakanrm_id,
                                                                                                  'onclick' => "inputTindakan(this)"));
                                         echo "<span>".$tindakanRM->tindakanrm_nama."</span><br/>";
                                     }
                                 } ?>
                        </div>
                <?php } ?>
            </div>
        </td>
        <td width="50%">
            <table id="tblFormTindakanRM" class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>
                            <a class="btn btn-primary" onclick="checkRahasia('kingCheck','ceklis');return false;" href="#" data-original-title="Klik untuk cek semua operasi" rel="tooltip">
                                <i class="icon-check icon-white"></i>
                        </a>
                        <?php echo CHtml::checkBox('kingCheck', true, array('onclick'=>'checkAll("ceklis",this);',
                                            'data-original-title'=>'Klik untuk cek semua operasi' ,'rel'=>'tooltip','style'=>'display:none;')) ?>
                        </th>
                        <th>Jenis Tindakan/<br/>Tindakan</th>
                        <th>Tarif</th>
                        <th>Qty</th>
                        <th>Satuan</th>
                        <th>Cyto</th>
                        <th>Tarif Cyto</th>
                    </tr>
                </thead>
            </table>
        </td>
    </tr>
</table>
 
</fieldset>
<script>
function inputTindakan(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanRM = obj.value;
        var kelasPelayan_id = <?php echo $model->kelaspelayanan_id ?>;
        
        if(kelasPelayan_id==''){
                $(obj).attr('checked', 'false');
                alert('Anda Belum Memilih Kelas Pelayanan');
                
            }
        else{
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRMPendRM')?>',
                 'data':{idPemeriksaanRM:idPemeriksaanRM,kelasPelayan_id:kelasPelayan_id},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         $('#tblFormTindakanRM').append(data.form);
                 } ,
                 'cache':false});
        }     
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idTindakan)
{
    $('#tblFormTindakanRM #tindakan_'+idTindakan).detach();
}


function hitungCyto(id,obj)
{
    if(obj == 1)
    {
        var persen_cytotind = $('#RMTindakanPelayananT_persencyto_tind_'+id+'').val(); 
        var harga_tarif = $('#RMTindakanPelayananT_tarif_tindakan_'+id+'').val(); 
        var tarif_cyto = harga_tarif * (persen_cytotind/100);

        $('#RMTindakanPelayananT_tarif_cyto_'+id+'').val(tarif_cyto);
    }
    else
    {
        $('#RMTindakanPelayananT_tarif_cyto_'+id+'').val(0);
    }
    
}
</script>