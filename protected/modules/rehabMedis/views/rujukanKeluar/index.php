<?php
$this->breadcrumbs=array(
	'Rujukan Keluar',
);

$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));

$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'Anamnesis', 'url'=>$this->createUrl('/rehabMedis/anamnesa',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Periksa Fisik', 'url'=>$this->createUrl('/rehabMedis/pemeriksaanFisik',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Laboratorium', 'url'=>$this->createUrl('/rehabMedis/laboratorium',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Radiologi', 'url'=>$this->createUrl('/rehabMedis/radiologi',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rehab Medis', 'url'=>$this->createUrl('/rehabMedis/rehabMedis',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Konsultasi Gizi', 'url'=>$this->createUrl('/rehabMedis/konsulGizi',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Konsul Poliklinik', 'url'=>$this->createUrl('/rehabMedis/konsulPoli',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Tindakan', 'url'=>$this->createUrl('/rehabMedis/tindakan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
        array('label'=>'Diagnosis', 'url'=>$this->createUrl('/rehabMedis/diagnosa',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Bedah Sentral', 'url'=>$this->createUrl('/rehabMedis/bedahSentral',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rujukan Ke Luar', 'url'=>'','linkOptions'=>array(),'active'=>true),
        array('label'=>'Reseptur', 'url'=>$this->createUrl('/rehabMedis/reseptur',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Pemakaian Bahan', 'url'=>$this->createUrl('/rehabMedis/pemakaianBahan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
    ),
));
?>


<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjpasien-dirujuk-keluar-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($modRujukanKeluar); ?>

    <table class="items">
        <tr>
            <td width="50%">
                <?php //echo $form->textFieldRow($modRujukanKeluar,'pasienadmisi_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($modRujukanKeluar,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modRujukanKeluar,'tgldirujuk', array('class'=>'control-label')) ?>
                    <?php $modRujukanKeluar->tgldirujuk = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modRujukanKeluar->tgldirujuk, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modRujukanKeluar,
                                                    'attribute'=>'tgldirujuk',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($modRujukanKeluar,'pegawai_id', CHtml::listData($modRujukanKeluar->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($modRujukanKeluar,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->dropDownListRow($modRujukanKeluar,'rujukankeluar_id', CHtml::listData($modRujukanKeluar->getRujukanItems(), 'rujukankeluar_id', 'rumahsakitrujukan'),
                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modRujukanKeluar,'nosuratrujukan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <?php //echo $form->textFieldRow($modRujukanKeluar,'tgldirujuk',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modRujukanKeluar,'ythdokter',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textFieldRow($modRujukanKeluar,'dirujukkebagian',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
                <?php //echo $form->dropDownListRow($modRujukanKeluar,'ruanganasal_id', CHtml::listData($modRujukanKeluar->getRuanganInstalasiItems($modPendaftaran->instalasi_id), 'ruangan_id', 'ruangan_nama'),
                                                //array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modRujukanKeluar,'catatandokterperujuk',array('rows'=>3, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modRujukanKeluar,'alasandirujuk',array('rows'=>3, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
            <td width="50%">
                <?php echo $form->textAreaRow($modRujukanKeluar,'hasilpemeriksaan_ruj',array('rows'=>3, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modRujukanKeluar,'diagnosasementara_ruj',array('rows'=>3, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modRujukanKeluar,'pengobatan_ruj',array('rows'=>3, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modRujukanKeluar,'lainlain_ruj',array('rows'=>3, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
    </table>
            
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>

<?php $this->endWidget(); ?>

<?php $this->renderPartial('_listRujukanKeluar',array('modRiwayatRujukanKeluar'=>$modRiwayatRujukanKeluar)); ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogDetailrujukan',
    'options'=>array(
        'title'=>'Detail Rujukan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'resizable'=>false,
        'position'=>'top',
    ),
));

    echo '<div id="contentDetailRujukan">dialog content here</div>';

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<script type="text/javascript">
function viewDetailRujukan(idPasienDirujuk)
{
    $.post('<?php echo $this->createUrl('ajaxDetailRujukanKeluar') ?>', {idPasienDirujuk: idPasienDirujuk}, function(data){
        $('#contentDetailRujukan').html(data.result);
    }, 'json');
    $('#dialogDetailrujukan').dialog('open');
}
</script>