<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjpasien-dirujuk-keluar-t-formupdate',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'action'=>'#',
        'focus'=>'#',
)); ?>
<table class="table-condensed">
    <tr>
        <td width="50%">
            <?php echo $form->textFieldRow($modRujukanKeluar,'tgldirujuk',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'disabled'=>true)); ?>
            <?php echo $form->dropDownListRow($modRujukanKeluar,'pegawai_id', CHtml::listData($modRujukanKeluar->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                            array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'disabled'=>true)); ?>
            <?php echo $form->dropDownListRow($modRujukanKeluar,'rujukankeluar_id', CHtml::listData($modRujukanKeluar->getRujukanItems(), 'rujukankeluar_id', 'rumahsakitrujukan'),
                                            array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'disabled'=>true)); ?>
            <?php echo $form->textFieldRow($modRujukanKeluar,'nosuratrujukan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'disabled'=>true)); ?>
            <?php echo $form->textFieldRow($modRujukanKeluar,'ythdokter',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100,'disabled'=>true)); ?>
            <?php echo $form->textFieldRow($modRujukanKeluar,'dirujukkebagian',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30,'disabled'=>true)); ?>
            <?php echo $form->dropDownListRow($modRujukanKeluar,'ruanganasal_id', CHtml::listData($modRujukanKeluar->getRuanganInstalasiItems(''), 'ruangan_id', 'ruangan_nama'),
                                            array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'disabled'=>true)); ?>
            <?php echo $form->textAreaRow($modRujukanKeluar,'catatandokterperujuk',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'disabled'=>true)); ?>
        </td>
        <td width="50%">
            <?php echo $form->textAreaRow($modRujukanKeluar,'alasandirujuk',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'disabled'=>true)); ?>
            <?php echo $form->textAreaRow($modRujukanKeluar,'hasilpemeriksaan_ruj',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'disabled'=>true)); ?>
            <?php echo $form->textAreaRow($modRujukanKeluar,'diagnosasementara_ruj',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'disabled'=>true)); ?>
            <?php echo $form->textAreaRow($modRujukanKeluar,'pengobatan_ruj',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'disabled'=>true)); ?>
            <?php echo $form->textAreaRow($modRujukanKeluar,'lainlain_ruj',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'disabled'=>true)); ?>
        </td>
    </tr>
</table>
            
    <div class="form-actions">
            <?php echo CHtml::link(Yii::t('mds','{icon} Ok',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),'#',
                                    array('class'=>'btn btn-primary', 'onclick'=>'$("#dialogDetailrujukan").dialog("close");return false;')); ?>
    </div>
<?php $this->endWidget(); ?>