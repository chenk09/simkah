<?php
$this->breadcrumbs=array(
	'Konsul Gizi',
);

$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));

$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'Anamnesis', 'url'=>$this->createUrl('/rehabMedis/anamnesa',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Periksa Fisik', 'url'=>$this->createUrl('/rehabMedis/PemeriksaanFisik',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
        array('label'=>'Laboratorium', 'url'=>$this->createUrl('/rehabMedis/laboratorium',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Radiologi', 'url'=>$this->createUrl('/rehabMedis/radiologi',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rehab Medis', 'url'=>$this->createUrl('/rehabMedis/rehabMedis',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Konsultasi Gizi', 'url'=>'', 'active'=>true),
        array('label'=>'Konsul Poliklinik', 'url'=>$this->createUrl('/rehabMedis/konsulPoli',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Tindakan', 'url'=>$this->createUrl('/rehabMedis/tindakan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
        array('label'=>'Diagnosis', 'url'=>$this->createUrl('/rehabMedis/diagnosa',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Bedah Sentral', 'url'=>$this->createUrl('/rehabMedis/bedahSentral',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rujukan Ke Luar', 'url'=>$this->createUrl('/rehabMedis/rujukanKeluar',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Reseptur', 'url'=>$this->createUrl('/rehabMedis/reseptur',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Pemakaian Bahan', 'url'=>$this->createUrl('/rehabMedis/pemakaianBahan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
    ),
));

?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjpasien-konsulGizi-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($modKirimKeUnitLain, $modPasienMasukPenunjang); ?>
                
    <table class="table-condensed">
        <tr>
            <td width="50%">
                <div class="control-group ">
                    <?php echo $form->labelEx($modKirimKeUnitLain,'tgl_kirimpasien', array('class'=>'control-label')) ?>
                    <?php $modKirimKeUnitLain->tgl_kirimpasien = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modKirimKeUnitLain->tgl_kirimpasien, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modKirimKeUnitLain,
                                                    'attribute'=>'tgl_kirimpasien',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($modKirimKeUnitLain,'pegawai_id', CHtml::listData($modKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->dropDownListRow($modKirimKeUnitLain,'ruangan_id', CHtml::listData($modKirimKeUnitLain->getRuanganGiziItems(), 'ruangan_id', 'ruangan_nama'),
                                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                                      'onchange'=>'loadFormTindakanGizi(this.value)')); ?>
                
            </td>
            <td width="50%">
                <?php echo $form->textAreaRow($modKirimKeUnitLain,'catatandokterpengirim',array('onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td width="50%">
                    <table id="tblFormPermintaanGizi" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Pilih</th>
                                <th>Pelayanan</th>
                                <th>Tarif</th>
                            </tr>
                        </thead>
                        <tbody id="bodyTblFormPermintaanGizi"></tbody>
                    </table> 
            </td>
            <td width="50%">
                <?php $this->renderPartial('_listKirimKeUnitLain',array('modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain)) ?>
            </td>
        </tr>
    </table>
    
    
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
function loadFormTindakanGizi(idRuangan)
{
    var idKelasPelayanan = <?php echo $modPendaftaran->kelaspelayanan_id; ?>;
    $.post("<?php echo Yii::app()->createUrl('ActionAjax/loadFormTindakanGizi')?>", {idRuangan:idRuangan, idKelasPelayanan:idKelasPelayanan}, function(data){
        $('#bodyTblFormPermintaanGizi').html(data.form);
    }, "json");
}

function batalKirim(idPasienKirimKeUnitLain,idPendaftaran)
{
    if(confirm('Apakah anda akan membatalkan kirim pasien?')){
        $.post('<?php echo $this->createUrl('ajaxBatalKirim') ?>', {idPasienKirimKeUnitLain: idPasienKirimKeUnitLain, idPendaftaran:idPendaftaran}, function(data){
            $('#tblListPemeriksaanRad').html(data.result);
        }, 'json');
    }
}
</script>