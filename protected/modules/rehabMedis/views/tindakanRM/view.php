<?php
$this->breadcrumbs=array(
	'Rmtindakanrm Ms'=>array('index'),
	$model->tindakanrm_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Tindakan #'.$model->tindakanrm_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Tindakan ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'tindakanrm_id',
		'jenistindakanrm_id',
		'daftartindakan_id',
		'tindakanrm_nama',
		'tindakanrm_namalainnya',
        array(
                        'name'=>'Status',
                        'value'=>('$data->jenistindakanrm_aktif' == TRUE ) ? "Aktif" : "Tidak Aktif",
                        'htmlOptions'=>array('style'=>'text-align:center;'),
        ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>