
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rmtindakanrm-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

            <?php //echo $form->textFieldRow($model,'jenistindakanrm_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

            <?php echo $form->dropDownListRow($model,'jenistindakanrm_id', CHtml::listData($model->getJenisTindakanItems(), 'jenistindakanrm_id', 'jenistindakanrm_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>

            <?php //echo $form->textFieldRow($model,'daftartindakan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->label($model, 'daftartindakan_id', array('class' => 'control-label')); ?>
                <?php echo CHtml::ActiveHiddenField($model,'daftartindakan_id', '', array('readonly'=>true)) ?>
                <div class="controls">
                        <?php
                            $this->widget('MyJuiAutoComplete', array(
                                'model' => $model,
                                'attribute' => 'daftartindakan_nama',
                                'sourceUrl' => 'js: function(request, response) {
                                           $.ajax({
                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/getDaftarTindakanForRM').'",
                                               dataType: "json",
                                               data: {
                                                   term: request.term,
                                               },
                                               success: function (data) {
                                                       response(data);
                                               }
                                           })
                                        }',
                                'options' => array(
                                    'showAnim' => 'fold',
                                    'minLength' => 2,
                                    'focus' => 'js:function( event, ui ) {
                                                                $(this).val( ui.item.label);
                                                                return false;
                                                            }',
                                    'select' => 'js:function( event, ui ) {
                                                                      $("#RMTindakanrmM_daftartindakan_id").val(ui.item.daftartindakan_id);
                                                                      $(this).val(ui.item.label);
                                                                      return false;
                                                            }',
                                ),
                                'htmlOptions' => array('value' => '', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                    'class' => 'span3 ',
                                ),
                                'tombolDialog'=>array('idDialog'=>'dialogDaftarTindakan'),
                        ));
                        ?>
                </div>
            </div>

            <?php echo $form->textFieldRow($model,'tindakanrm_nama',array('class'=>'span3', 'onkeyup'=>"namaLain(this)", 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'tindakanrm_namalainnya',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/tindakanRM/admin'), 
                                    array('class'=>'btn btn-danger',
                                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('sistemAdministrator.views.tips.tipsaddedit3a',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'update','content'=>$content)); 
                        ?>
	</div>

<?php $this->endWidget(); ?>

<?php
//========= Dialog buat cari data daftar tindakan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDaftarTindakan',
    'options' => array(
        'title' => 'Pencarian Daftar Tindakan',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 600,
        'resizable' => false,
    ),
));

$modTarifTindakan = new RMTarifTindakanM('search');
$modTarifTindakan->unsetAttributes();
if (isset($_GET['RMTarifTindakanM'])) {
    $modTarifTindakan->attributes = $_GET['RMTarifTindakanM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id'=>'satarif-tindakan-m-grid', 
    //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider' => $modTarifTindakan->searchDaftarTindakan(),
    'filter' => $modTarifTindakan,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectDaftarTindakan",
                                            "onClick" => "$(\"#RMTindakanrmM_daftartindakan_id\").val(\"$data->daftartindakan_id\");
                                                          $(\"#RMTindakanrmM_daftartindakan_nama\").val(\"".$data->daftartindakan->daftartindakan_nama." - ".$data->kelaspelayanan->kelaspelayanan_nama." - ".$data->harga_tariftindakan."\");
                                                          $(\"#dialogDaftarTindakan\").dialog(\"close\");    
                                                "))',
        ),
        // array( 
        //                 'name'=>'tariftindakan_id', 
        //                 'value'=>'$data->tariftindakan_id', 
        //                 'filter'=>false, 
        //         ),
        array( 
                        'name'=>'daftartindakan_id', 
                        'value'=>'$data->daftartindakan->daftartindakan_nama',
                ),
        array( 
                        'name'=>'kelaspelayanan_id', 
                        'value'=>'$data->kelaspelayanan->kelaspelayanan_nama',
                        'filter'=>CHtml::listData($modTarifTindakan->KelasPelayanan, 'kelaspelayanan_id', 'kelaspelayanan_nama'),
                ),
        array( 
                        'name'=>'harga_tariftindakan', 
                        'value'=>'number_format($data->harga_tariftindakan,0,".",",")', 
                        'filter'=>false, 
                ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
//========= end daftar tindakan dialog =============================
?>

<script type="text/javascript">
    function namaLain(nama)
    {
        document.getElementById('RMTindakanrmM_tindakanrm_namalainnya').value = nama.value.toUpperCase();
    }
</script>