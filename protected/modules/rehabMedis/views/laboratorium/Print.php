
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

?>
<table width="100%" style='margin-left:auto; margin-right:auto;'>
    <tr>
        <td>
            <label class='control-label'><?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('nama_pasien')); ?>:</label>
            <?php echo CHtml::encode($modPendaftaran->pasien->nama_pasien); ?>
            <br/>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('jeniskelamin')); ?>:</label>
                <?php echo CHtml::encode($modPendaftaran->pasien->jeniskelamin); ?>
            <br/>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('umur')); ?>:</label>
                <?php echo CHtml::encode($modPendaftaran->umur); ?>
            <br/>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('Cara Bayar / Penjamin')); ?>:</label>
                <?php echo CHtml::encode($modPendaftaran->carabayar->carabayar_nama); ?> / <?php echo CHtml::encode($modPendaftaran->penjamin->penjamin_nama); ?>
                   
        </td>
        <Td width="30%">
            
            </td>
        <td>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('tgl_pendaftaran')); ?>:</label>
            <?php echo CHtml::encode($modPendaftaran->tgl_pendaftaran); ?>
            <br/>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('no_pendaftaran')); ?>:</label>
                <?php echo CHtml::encode($modPendaftaran->no_pendaftaran); ?>
            <br/>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('kelaspelayanan')); ?>:</label>
            <?php echo CHtml::encode($modPendaftaran->kelaspelayanan->kelaspelayanan_nama); ?>
            <br/>
                <label class='control-label'><?php echo CHtml::encode($modPendaftaran->getAttributeLabel('Nama Dokter')); ?>:</label>
            <?php echo CHtml::encode($modPendaftaran->pegawai->nama_pegawai); ?>
        </td>
        </tr>
    </table>
<br/>
<table id="tblListPemeriksaanLab" class="table table-bordered table-condensed" >
    <thead>
        <tr>
            <th>Jenis Pemeriksaan</th>
            <th>Permintaan Pemeriksaan</th>
            <th>Tarif</th>
            <th>Qty</th>
                        
        </tr>
    </thead>
    <tbody>
<?php
foreach ($modRiwayatKirimKeUnitLain as $i => $riwayat) {
    $modPermintaan = RJPermintaanPenunjangT::model()->with('daftartindakan','pemeriksaanlab')->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$riwayat->pasienkirimkeunitlain_id));
    ?>
    <tr>
        <td><?php
            foreach($modPermintaan as $j => $permintaan){
                echo strip_tags($permintaan->pemeriksaanlab->jenispemeriksaan->jenispemeriksaanlab_nama).'<br/>';
            } ?></td>
        <td>
            <?php
            foreach($modPermintaan as $j => $permintaan){
                echo strip_tags($permintaan->pemeriksaanlab->pemeriksaanlab_nama).'<br/>';
            } ?>
        </td>
        <td>
            <?php
            $temp_datartind = '';
            foreach($modPermintaan as $j => $permintaan){
                $daftartindakan_id = $permintaan->pemeriksaanlab->daftartindakan_id;
                if($temp_datartind != $daftartindakan_id) {
                    $modTarif = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$riwayat->kelaspelayanan_id,
                                                                                'daftartindakan_id'=>$daftartindakan_id,
                                                                                'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                    echo (!empty($modTarif->harga_tariftindakan))? MyFunction::formatNumber($modTarif->harga_tariftindakan).'<br/>':'Belum ada tarif <br/>';
                }
                $temp_datartind = $daftartindakan_id;
            } ?>
        </td>
        <td>
            <?php
            foreach($modPermintaan as $j => $permintaan){
                echo $permintaan->qtypermintaan.'<br/>';
            } ?>
        </td>
<!--        <td>
            <?php //echo CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'batalKirim('.$riwayat->pasienkirimkeunitlain_id.','.$riwayat->pendaftaran_id.');return false;','rel'=>'tooltip','title'=>'Klik untuk membatalkan kirim pasien')); ?>
            <?php //echo $riwayat->catatandokterpengirim; ?>
        </td>-->
    </tr>
    <?php
}
?>
<!--        <tr id="trListKosong"><td colspan="5"></td></tr>-->
    </tbody>
    
</table>
<table align="RIGHT">
    <tr>
        <td>
<div align="CENTER">
     Dokter Pemeriksa
    <br/><br/><br/><br/>
   ( <?php echo CHtml::encode($modPendaftaran->pegawai->nama_pegawai); ?> )
</div>
        </td>
        
    </tr>
</table>
<table align="LEFT">
    <tr>
        <td>
<div align="CENTER">
     Catatan Dokter : <?php echo CHtml::encode($riwayat->catatandokterpengirim); ?>
   
</div>
        </td>
        
    </tr>
    
</table>