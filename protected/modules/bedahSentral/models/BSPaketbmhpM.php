<?php

/**
 * This is the model class for table "paketbmhp_m".
 *
 * The followings are the available columns in table 'paketbmhp_m':
 * @property integer $paketbmhp_id
 * @property integer $daftartindakan_id
 * @property integer $tipepaket_id
 * @property integer $satuankecil_id
 * @property integer $obatalkes_id
 * @property integer $kelompokumur_id
 * @property double $qtypemakaian
 * @property double $qtystokout
 * @property double $hargapemakaian
 */
class BSPaketbmhpM extends PaketbmhpM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PaketbmhpM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function searchPaket()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
		$criteria->with = array('obatalkes','daftartindakan','kelompokumur');
		
		if(!empty($this->paketbmhp_id)){
			$criteria->addCondition("paketbmhp_id = ".$this->paketbmhp_id);		
		}
		if(!empty($this->daftartindakan_id)){
			$criteria->addCondition("daftartindakan_id = ".$this->daftartindakan_id);		
		}
		if(!empty($this->tipepaket_id)){
			$criteria->addCondition("tipepaket_id = ".$this->tipepaket_id);		
		}
		if(!empty($this->satuankecil_id)){
			$criteria->addCondition("satuankecil_id = ".$this->satuankecil_id);		
		}
		if(!empty($this->obatalkes_id)){
			$criteria->addCondition("obatalkes_id = ".$this->obatalkes_id);		
		}
		if(!empty($this->kelompokumur_id)){
			$criteria->addCondition("kelompokumur_id = ".$this->kelompokumur_id);		
		}
		$criteria->compare('qtypemakaian',$this->qtypemakaian);
		$criteria->compare('qtystokout',$this->qtystokout);
		$criteria->compare('hargapemakaian',$this->hargapemakaian);
		$criteria->compare('LOWER(obatalkes.obatalkes_nama)',strtolower($this->obatalkesNama),true);
		$criteria->compare('LOWER(daftartindakan.daftartindakan_nama)',strtolower($this->daftartindakanNama),true);
		$criteria->compare('LOWER(kelompokumur.kelompokumur_nama)',strtolower($this->kelompokumurNama),true);
                $criteria->limit=10;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}