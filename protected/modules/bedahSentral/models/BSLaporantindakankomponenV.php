<?php
class BSLaporantindakankomponenV extends LaporantindakankomponenV
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        // -- REKAP JASA DOKTER -- //
        
        public function searchJasaDokter()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->select = 'pegawai_id,nama_pegawai,gelardepan,gelarbelakang_nama,komponentarif_id,sum(qty_tindakan) as qty_tindakan';
                $criteria->group = 'pegawai_id,nama_pegawai,gelardepan,gelarbelakang_nama,komponentarif_id';
                $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir,true);
                $criteria->compare('pegawai_id',$this->pegawai_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function searchPrintJasaDokter()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->select = 'pegawai_id,nama_pegawai,gelardepan,gelarbelakang_nama,komponentarif_id,sum(qty_tindakan) as qty_tindakan';
                $criteria->group = 'pegawai_id,nama_pegawai,gelardepan,gelarbelakang_nama,komponentarif_id';
                $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir,true);
                
                $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
                $criteria->compare('pegawai_id',$this->pegawai_id);
                $criteria->limit = -1;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}
        // -- END REKAP JASA DOKTER -- //
        
        // -- DETAIL JASA DOKTER -- //
        public function searchDetailJasaDokter()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->select = 'pendaftaran_id, pasien_id, nama_pasien, namaperusahaan,no_pendaftaran, no_rekam_medik,tgl_pendaftaran,ruangan_nama,
                                    ruangan_id,gelardepan,nama_pegawai,gelarbelakang_nama,instalasi_nama,instalasi_id,
                                    sum(tarif_tindakan) As tarif_tindakan,
                                    sum(tarif_tindakan) As total,
                                    sum(tarif_tindakankomp) As tarif_rsakomodasi,
                                    sum(qty_tindakan) As qty_tindakan
                                    ';
                $criteria->group = 'pendaftaran_id, pasien_id, nama_pasien, namaperusahaan,no_pendaftaran, 
                                    no_rekam_medik,tgl_pendaftaran,ruangan_nama,ruangan_id,gelardepan,nama_pegawai,gelarbelakang_nama,instalasi_nama,instalasi_id';
                
                $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
                $criteria->compare('pegawai_id',$this->pegawai_id);
                $criteria->compare('ruangan_id',$this->ruangan_id);
                $criteria->compare('instalasi_id',$this->ruangan_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function searchPrintDetailJasaDokter()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
                
                $criteria->select = 'pendaftaran_id, pasien_id, nama_pasien, namaperusahaan,no_pendaftaran, no_rekam_medik,tgl_pendaftaran,ruangan_nama,
                                    ruangan_id,gelardepan,nama_pegawai,gelarbelakang_nama,instalasi_nama,instalasi_id,
                                    sum(tarif_tindakan) As tarif_tindakan,
                                    sum(tarif_tindakan) As total,
                                    sum(tarif_tindakankomp) As tarif_rsakomodasi,
                                    sum(qty_tindakan) As qty_tindakan
                                    ';
                $criteria->group = 'pendaftaran_id, pasien_id, nama_pasien, namaperusahaan,no_pendaftaran, 
                                    no_rekam_medik,tgl_pendaftaran,ruangan_nama,ruangan_id,gelardepan,nama_pegawai,gelarbelakang_nama,instalasi_nama,instalasi_id';
                
                $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
                $criteria->compare('pegawai_id',$this->pegawai_id);
                $criteria->compare('ruangan_id',$this->ruangan_id);
                $criteria->compare('instalasi_id',$this->instalasi_id);
                $criteria->limit = -1;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}
        // -- END DETAIL JASA DOKTER -- //
        
        public function getDokterItems()
        {
            return DokterV::model()->findAll();
        }
}