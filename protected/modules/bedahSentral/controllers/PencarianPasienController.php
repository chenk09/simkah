<?php

class PencarianPasienController extends SBaseController
{
	
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionIndex()
        {
                $format = new CustomFormat();
                $modRJ = new BSInfoKunjunganRJV;
                $modRI = new BSInfoKunjunganRIV;
                $modRD = new BSInfoKunjunganRDV;
                
                $cekRJ = true;
                $cekRI = false;
                $cekRD = false;
                
                $modRJ->tglAwal = date("Y-m-d").' 00:00:00';
                $modRJ->tglAkhir =date('Y-m-d H:i:s');
                
                $modRI->tglAwal = date("Y-m-d").' 00:00:00';
                $modRI->tglAkhir = date('Y-m-d H:i:s');
                
                $modRD->tglAwal = date("Y-m-d").' 00:00:00';
                $modRD->tglAkhir =date('Y-m-d H:i:s');
       
                
                if(isset ($_POST['instalasi']))
                {
                    switch ($_POST['instalasi']) {
                        
                        case 'RJ':
                            $cekRJ = true;
                            $cekRI = false;
                            $cekRD = false;
                            $modRJ->attributes = $_POST['BSInfoKunjunganRJV'];
                            if(!empty($_POST['BSInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSInfoKunjunganRJV']['tglAwal']);
                            }
                            if(!empty($_POST['BSInfoKunjunganRJV']['tglAkhir']))
                            {
                                $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSInfoKunjunganRJV']['tglAkhir']);
                            }
                            
                            break;
                            
                        case 'RI':
                            $cekRI = true;
                            $cekRJ = false;
                            $cekRD = false;
                            $modRI->attributes = $_POST['BSInfoKunjunganRIV'];
                            if(!empty($_POST['BSInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSInfoKunjunganRIV']['tglAwal']);
                            }
                            if(!empty($_POST['BSInfoKunjunganRIV']['tglAkhir']))
                            {
                                $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSInfoKunjunganRIV']['tglAkhir']);
                            }
                            break;
                            
                        case 'RD':
                            $cekRD = true;
                            $cekRI = false;
                            $cekRJ = false;
                            $modRD->attributes = $_POST['BSInfoKunjunganRDV'];
                            if(!empty($_POST['BSInfoKunjunganRDV']['tglAwal']))
                            {
                                $modRD->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSInfoKunjunganRDV']['tglAwal']);
                            }
                            if(!empty($_POST['BSInfoKunjunganRDV']['tglAkhir']))
                            {
                                $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSInfoKunjunganRDV']['tglAkhir']);
                            }
                           
                            break;
                    }
                }
                
                $this->render('index',array(
                                 'modRJ'=>$modRJ,
                                 'modRI'=>$modRI,
                                 'modRD'=>$modRD,
                                 'cekRJ'=>$cekRJ,
                                 'cekRI'=>$cekRI,
                                 'cekRD'=>$cekRD,
                                 
                ));
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}