<?php

class LaporanController extends SBaseController {
    
    public function actionLaporanSensusHarian() {
        $model = new BSLaporansensuspenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        $kunjungan = Kunjungan::items();
        $model->kunjungan = $kunjungan;
        if (isset($_GET['BSLaporansensuspenunjangV'])) {
            $model->attributes = $_GET['BSLaporansensuspenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporansensuspenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporansensuspenunjangV']['tglAkhir']);
        }

        $this->render('sensus/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanSensusHarian() {
        $model = new BSLaporansensuspenunjangV('search');
        $judulLaporan = 'Laporan Sensus Harian Bedah Sentral';
        
        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSLaporansensuspenunjangV'])) {
            $model->attributes = $_REQUEST['BSLaporansensuspenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporansensuspenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporansensuspenunjangV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'sensus/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikSensusHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSLaporansensuspenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['BSLaporansensuspenunjangV'])) {
            $model->attributes = $_GET['BSLaporansensuspenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporansensuspenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporansensuspenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanKunjungan() {
        $model = new BSLaporanpasienpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        $model->kunjungan = Kunjungan::items();
        if (isset($_GET['BSLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['BSLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanpasienpenunjangV']['tglAkhir']);
        }

        $this->render('kunjungan/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjungan() {
        $model = new BSLaporanpasienpenunjangV('search');
        $judulLaporan = 'Laporan Kunjungan Bedah Sentral';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Bedah Sentral';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSLaporanpasienpenunjangV'])) {
            $model->attributes = $_REQUEST['BSLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanpasienpenunjangV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjungan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjungan() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSLaporanpasienpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['BSLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['BSLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanpasienpenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporan10BesarPenyakit() {
        $model = new BSLaporan10besarpenyakit('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        $model->jumlahTampil = 10;

        if (isset($_GET['BSLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['BSLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporan10besarpenyakit']['tglAkhir']);
        }

        $this->render('10Besar/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporan10BesarPenyakit() {
        $model = new BSLaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit Pasien Bedah Sentral';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Pasien Bedah Sentral';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSLaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['BSLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '10Besar/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSLaporan10besarpenyakit('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Bedah Sentral';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BSLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['BSLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporan10besarpenyakit']['tglAkhir']);
        }
               
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPemakaiObatAlkes()
    {
        $model = new BSLaporanpemakaiobatalkesV;
        $model->unsetAttributes();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        $jenisObat =CHtml::listData(JenisobatalkesM::model()->findAll('jenisobatalkes_aktif = true'),'jenisobatalkes_id','jenisobatalkes_id');
        $model->jenisobatalkes_id = $jenisObat;
        if(isset($_GET['BSLaporanpemakaiobatalkesV']))
        {
            $model->attributes = $_GET['BSLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        $this->render('pemakaiObatAlkes/index',array('model'=>$model));
    }

    public function actionPrintLaporanPemakaiObatAlkes() {
        $model = new BSLaporanpemakaiobatalkesV('search');
        $judulLaporan = 'Laporan Info Pemakai Obat Alkes Bedah Sentral';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Bedah Sentral';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_REQUEST['BSLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemakaiObatAlkes/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    

    public function actionFrameGrafikLaporanPemakaiObatAlkes() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSLaporanpemakaiobatalkesV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Bedah Sentral';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BSLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['BSLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanpemakaiobatalkesV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanJasaInstalasi() {
        $model = new BSLaporanjasainstalasi('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $tindakan = array('sudah', 'belum');
        $model->tindakansudahbayar_id = $tindakan;
        if (isset($_GET['BSLaporanjasainstalasi'])) {
            $model->attributes = $_GET['BSLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanjasainstalasi']['tglAkhir']);
        }

        $this->render('jasaInstalasi/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanJasaInstalasi() {
        $model = new BSLaporanjasainstalasi('search');
        $judulLaporan = 'Laporan Jasa Instalasi Bedah Sentral';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSLaporanjasainstalasi'])) {
            $model->attributes = $_REQUEST['BSLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanjasainstalasi']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jasaInstalasi/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJasaInstalasi() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSLaporanjasainstalasi('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BSLaporanjasainstalasi'])) {
            $model->attributes = $_GET['BSLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanjasainstalasi']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBiayaPelayanan() {
        $model = new BSLaporanbiayapelayanan('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE'),'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['BSLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['BSLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanbiayapelayanan']['tglAkhir']);
        }

        $this->render('biayaPelayanan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanBiayaPelayanan() {
        $model = new BSLaporanbiayapelayanan('search');
        $judulLaporan = 'Laporan Biaya Pelayanan Bedah Sentral';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Bedah Sentral';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSLaporanbiayapelayanan'])) {
            $model->attributes = $_REQUEST['BSLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanbiayapelayanan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'biayaPelayanan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanBiayaPelayanan() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSLaporanbiayapelayanan('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Bedah Sentral';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BSLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['BSLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanbiayapelayanan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPendapatanRuangan() {
        $model = new BSLaporanpendapatanruanganV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['BSLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['BSLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanpendapatanruanganV']['tglAkhir']);
        }

        $this->render('pendapatanRuangan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanPendapatanRuangan() {
        $model = new BSLaporanpendapatanruanganV('search');
        $judulLaporan = 'Laporan Grafik Pendapatan Ruangan Bedah Sentral';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSLaporanpendapatanruanganV'])) {
            $model->attributes = $_REQUEST['BSLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporanpendapatanruanganV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pendapatanRuangan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPendapatanRuangan() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSLaporanpendapatanruanganV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BSLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['BSLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporanpendapatanruanganV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBukuRegister() {
        $model = new BSBukuregisterpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['BSBukuregisterpenunjangV'])) {
            $model->attributes = $_GET['BSBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSBukuregisterpenunjangV']['tglAkhir']);
        }

        $this->render('bukuRegister/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanBukuRegister() {
        $model = new BSBukuregisterpenunjangV('search');
        $judulLaporan = 'Laporan Buku Register Pasien Bedah Sentral';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Bedah Sentral';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSBukuregisterpenunjangV'])) {
            $model->attributes = $_REQUEST['BSBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSBukuregisterpenunjangV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuRegister/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSBukuregisterpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Bedah Sentral';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BSBukuregisterpenunjangV'])) {
            $model->attributes = $_GET['BSBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSBukuregisterpenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanCaraMasukPasien() {
        $model = new BSLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        $asalrujukan = CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'), 'asalrujukan_id', 'asalrujukan_id');
        $model->asalrujukan_id = $asalrujukan;
        $ruanganasal = CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'),'ruangan_id','ruangan_id');
        $model->ruanganasal_id = $ruanganasal;
        if (isset($_GET['BSLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['BSLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('caraMasuk/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanCaraMasukPasien() {
        $model = new BSLaporancaramasukpenunjangV('search');
        $judulLaporan = 'Laporan Cara Masuk Pasien Bedah Sentral';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSLaporancaramasukpenunjangV'])) {
            $model->attributes = $_REQUEST['BSLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'caraMasuk/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanCaraMasukPasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BSLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['BSLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }
    
    public function actionLaporanTransaksiBedahSentral() {
        $this->pageTitle = Yii::app()->name." - Laporan Transaksi Bedah Sentral";
        $model = new BSTindakandanoasudahbayarV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        if (isset($_GET['BSTindakandanoasudahbayarV'])) {
            $model->attributes = $_GET['BSTindakandanoasudahbayarV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSTindakandanoasudahbayarV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSTindakandanoasudahbayarV']['tglAkhir']);
        }

        $this->render('rekapTransaksiBedah/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanTransaksiBedahSentral() {
        $model = new BSTindakandanoasudahbayarV('search');
        $judulLaporan = 'Laporan Transaksi Pasien Bedah Sentral';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Transaksi Pasien Bedah Sentral';
        $data['type'] = $_REQUEST['type'];
        $data['filter'] = $_GET['filter_tab'];
        if (isset($_REQUEST['BSTindakandanoasudahbayarV'])) {
            $model->attributes = $_REQUEST['BSTindakandanoasudahbayarV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSTindakandanoasudahbayarV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSTindakandanoasudahbayarV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'rekapTransaksiBedah/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanTransaksiBedahSentral() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSTindakandanoasudahbayarV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Transaksi Pasien Bedah Sentral';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BSTindakandanoasudahbayarV'])) {
            $model->attributes = $_GET['BSTindakandanoasudahbayarV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSTindakandanoasudahbayarV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSTindakandanoasudahbayarV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    // END //
    /*
     * actionLaporanRekapJasaDokter
     * by Miranitha Fasha
     */
    public function actionLaporanRekapJasaDokter() {
        $this->pageTitle = Yii::app()->name." - Laporan Rekap Jasa Dokter";
        $model = new BSLaporantindakankomponenV();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        if (isset($_GET['BSLaporantindakankomponenV'])) {
            $model->attributes = $_GET['BSLaporantindakankomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporantindakankomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporantindakankomponenV']['tglAkhir']);
        }

        $this->render('rekapJasaDokter/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanRekapJasaDokter() {
        $model = new BSLaporantindakankomponenV();
        $judulLaporan = 'Laporan Rekap Jasa Dokter';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Rekap Jasa Dokter';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BSLaporantindakankomponenV'])) {
            $model->attributes = $_REQUEST['BSLaporantindakankomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporantindakankomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSLaporantindakankomponenV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'rekapJasaDokter/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanRekapJasaDokter() {
        $this->layout = '//layouts/frameDialog';
        $model = new BSLaporantindakankomponenV();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Rekap Jasa Dokter';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BSLaporantindakankomponenV'])) {
            $model->attributes = $_GET['BSLaporantindakankomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSLaporantindakankomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSLaporantindakankomponenV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    /** end actionLaporanRekapJasaDokter **/
    public function getTabularFormTabs($form, $model)
    {
        $tabs = array();
        $count = 0;
        foreach (array('en'=>'English', 'fi'=>'Finnish', 'sv'=>'Swedish') as $locale => $language)
        {
            $tabs[] = array(
                'active'=>$count++ === 0,
                'label'=>$language,
                'content'=>$this->renderPartial('rekapTransaksiBedah/_table', array('form'=>$form, 'model'=>$model, 'locale'=>$locale, 'language'=>$language), true),
            );
        }
        return $tabs;
    }
}