<?php

class PendaftaranPasienKunjunganController extends SBaseController
{
        public $successSave = false;
        /**
	 * @return array action filters
	 */
         
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex()
	{
            
            $this->pageTitle = Yii::app()->name." - Pendaftaran Pasien Kunjungan ";
            $model = $this->loadModel($_POST['pendaftaran_id']);
            $modPasien = BSPasienM::model()->findByPk($model->pasien_id);
            $modPasienAdmisi = new PasienadmisiT; //hanya untuk mengambil kamarruangan_m
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->umur = "00 Thn 00 Bln 00 Hr";
            $model->ruangan_id = Params::RUANGAN_ID_IBS;
            $model->kelaspelayanan_id = Params::kelasPelayanan('bedah_sentral');
            $modPasienPenunjang = new BSPasienMasukPenunjangT;
            $modRencanaOperasi = new BSRencanaOperasiT;
            $modRencanaOperasi->norencanaoperasi = Generator::noRencanaOperasi();
            $modTindakanPelayanan = new BSTindakanPelayananT;
            $modTindakanKomponen = new BSTindakanKomponenT;
            $modKegiatanOperasi = BSKegiatanOperasiM::model()->findAllByAttributes(array('kegiatanoperasi_aktif'=>true),array('order'=>'kegiatanoperasi_nama'));
            $modOperasi = BSOperasiM::model()->findAllByAttributes(array('operasi_aktif'=>true),array('order'=>'operasi_nama'));
            $modRencanaOperasi->statusoperasi = Params::KODE_RENCANA_OPERASI;
            $modRencanaOperasi->tglrencanaoperasi = date('Y-m-d h:i:s');
            $attrOperasi = '';
            $attrCeklis = '';
            
            if (isset($_POST['BSRencanaOperasiT'])){
                $modRencanaOperasi->attributes = $_POST['BSRencanaOperasiT'];
                
                if(isset($_POST['operasi_id']))
                {
                    $attrOperasi = $_POST['operasi_id'];
                    $attrCeklis = $_POST['ceklis'];
                }
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    
                    
                    //==AwalSimpan Psien Masuk Penunjang========================
                      $modPasienPenunjang = $this->savePasienPenunjang($model);
                    //Akhir Pasien MAsuk Penunjang
                      
                    //==AwalSimpan Rencana Operasi========================  
                      if(!empty($_POST['operasi_id']))
                      {
                          $modRencanaOperasi = $this->saveRencanaOperasi($model,$modPasienPenunjang,$modRencanaOperasi,$attrOperasi,$attrCeklis,$_POST['BSTindakanPelayananT']);
                      }
                      else
                      {
                          $modRencanaOperasi->validate();
                          $this->successSave = false;
                      }
                    //Akhir Rencana Operasi
                      
                    if ($this->successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    }
                    else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('index',array(
                'model'=>$model, 
                'modPasien'=>$modPasien, 
                'modPasienPenunjang'=>$modPasienPenunjang,
                'modKegiatanOperasi'=>$modKegiatanOperasi,
                'modOperasi'=>$modOperasi,
                'modRencanaOperasi'=>$modRencanaOperasi,
                'modPasienAdmisi'=>$modPasienAdmisi,
                'modTindakanKomponen'=>$modTindakanKomponen,
                'modTindakanPelayanan'=>$modTindakanPelayanan
                
            ));
	}
        
        /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=  BSPendaftaranMp::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$attrRencanaOperasi,$attrTindakanPelayanan,$attrOperasi)
        {
            $validTindakanPelayanan = 'true';
            $arrSave = array();

                $patokan = $operasiNya['operasi'];
                $modTindakanPelayanan = new BSTindakanPelayananT;
                $modTindakanPelayanan->penjamin_id = $attrPendaftaran->penjamin_id;
                $modTindakanPelayanan->pasien_id = $attrPendaftaran->pasien_id;
                $modTindakanPelayanan->kelaspelayanan_id = $attrTindakanPelayanan['kelaspelayanan_id'][$attrOperasi];
                $modTindakanPelayanan->tipepaket_id = 1;
                $modTindakanPelayanan->instalasi_id = Params::INSTALASI_ID_IBS;
                $modTindakanPelayanan->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
                $modTindakanPelayanan->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayanan->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                $modTindakanPelayanan->daftartindakan_id = $attrTindakanPelayanan['daftartindakan_id'][$attrOperasi];
                $modTindakanPelayanan->carabayar_id = $attrPendaftaran->carabayar_id;
                $modTindakanPelayanan->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayanan->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayanan->tarif_tindakan = $attrTindakanPelayanan['tarif_tindakan'][$attrOperasi];
                $modTindakanPelayanan->satuantindakan = $attrTindakanPelayanan['satuantindakan'][$attrOperasi];
                $modTindakanPelayanan->qty_tindakan = $attrTindakanPelayanan['qty_tindakan'][$attrOperasi];
                $modTindakanPelayanan->cyto_tindakan = $attrTindakanPelayanan['cyto_tindakan'][$attrOperasi];
                if($modTindakanPelayanan->cyto_tindakan){
                    $modTindakanPelayanan->tarifcyto_tindakan = $modTindakanPelayanan->tarif_tindakan * ($attrTindakanPelayanan['persencyto_tind'][$attrOperasi]/100);
                } else {
                    $modTindakanPelayanan->tarifcyto_tindakan = 0;
                }
                $modTindakanPelayanan->discount_tindakan = 0;
                $modTindakanPelayanan->dokterpemeriksa1_id = $attrRencanaOperasi->dokterpelaksana1_id;
                $modTindakanPelayanan->dokterpemeriksa2_id = (!empty($attrRencanaOperasi->dokterpelaksana2)) ? $attrRencanaOperasi->dokterpelaksana2 : null;
                $modTindakanPelayanan->perawat_id = (!empty($attrRencanaOperasi->perawat_id)) ? $attrRencanaOperasi->perawat_id : null;
                $modTindakanPelayanan->subsidiasuransi_tindakan=0;
                $modTindakanPelayanan->subsidipemerintah_tindakan=0;
                $modTindakanPelayanan->subsisidirumahsakit_tindakan=0;
                $modTindakanPelayanan->iurbiaya_tindakan=0;
                
                if ($modTindakanPelayanan->validate()){
                    $arrSave[$i] = $modTindakanPelayanan; // menyimpan objek BSRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                    $validTindakanPelayanan = 'true';

                }else
                {   
                    $validTindakanPelayanan = $validTindakanPelayanan.'false';
                }
            if($validTindakanPelayanan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $x => $simpan) {
                    $simpan->save();
                    $this->saveTindakanKomponenT($simpan);
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }

            return $modTindakanPelayanan;
        }
        
        public function saveTindakanKomponenT($attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakanKomponen = 'true';
            $daftarTindakan_id = $attrTindakanPelayanan->daftartindakan_id;
            $kelaspelayanan_id = $attrTindakanPelayanan->kelaspelayanan_id;
            $arrTarifTindakan = "select * from tariftindakan_m where daftartindakan_id = ".$daftarTindakan_id." and kelaspelayanan_id = ".$kelaspelayanan_id." and komponentarif_id <> ".Params::KOMPONENTARIF_ID_TOTAL." ";
            $query = Yii::app()->db->createCommand($arrTarifTindakan)->queryAll();
            foreach ($query as $i => $tarifKomponen) {
                $modTarifKomponen = new BSTindakanKomponenT;
                $modTarifKomponen->tindakanpelayanan_id = $attrTindakanPelayanan->tindakanpelayanan_id;
                $modTarifKomponen->komponentarif_id = $tarifKomponen['komponentarif_id'];
                $modTarifKomponen->tarif_tindakankomp = $tarifKomponen['harga_tariftindakan'];
                if($attrTindakanPelayanan->cyto_tindakan){
                    $modTarifKomponen->tarifcyto_tindakankomp = $tarifKomponen['harga_tariftindakan'] * ($tarifKomponen['persencyto_tind']/100);
                } else {
                    $modTarifKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTarifKomponen->subsidiasuransikomp = 0;
                $modTarifKomponen->subsidipemerintahkomp = 0;
                $modTarifKomponen->subsidirumahsakitkomp = 0;
                $modTarifKomponen->iurbiayakomp = 0;
                if ($modTarifKomponen->validate()){
                    $arrSave[$i] = $modTarifKomponen; // menyimpan objek tarif komponen ke dalam sebuah array dan siap untuk disave
                    $validTindakanKomponen = 'true';

                }else
                {
                    $validTindakanKomponen = $validTindakanKomponen.'false';
                }
            } // ending foreach
            if($validTindakanKomponen == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $f => $simpan) {
                    $simpan->save();
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }
            return $modTarifKomponen;
        }
        
        public function saveRencanaOperasi($attrPendaftaran,$attrPenunjang,$attrRencana,$attrOperasi,$attrCeklis,$attrTindakanPelayanan)
        {
            $arrSave = array();
            $validRencana = 'true';
            $arrOperasi = array(); // array untuk menampung operasi yg nantinnya digunakan pada proses saveTindakanPelayanan
            for ($i = 0; $i < count($attrCeklis); $i++) {
                    $patokan = $attrCeklis[$i];

                        $modRencana = new BSRencanaOperasiT;
                        $modRencana->attributes = $attrRencana->attributes;
                        $modRencana->norencanaoperasi = Generator::noRencanaOperasi();
                        $modRencana->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                        $modRencana->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
                        $modRencana->pasien_id = $attrPendaftaran->pasien_id;
                        $modRencana->pasienadmisi_id = (!empty($attrPendaftaran->pasienadmisi_id)) ? $modRencana->kamarruangan_id : null ;
                        
                        $modRencana->kamarruangan_id = (!empty($modRencana->kamarruangan_id)) ? $modRencana->kamarruangan_id : null ;
                        $modRencana->dokterpelaksana2_id = (!empty($modRencana->dokterpelaksana2_id)) ? $modRencana->dokterpelaksana2_id : null ;
                        $modRencana->perawat_id = (!empty($modRencana->perawat_id)) ? $modRencana->perawat_id : null ;
                        $modRencana->dokteranastesi_id = (!empty($modRencana->dokteranastesi_id)) ? $modRencana->dokteranastesi_id : null ;
                        
                        $modRencana->selesaioperasi = $modRencana->tglrencanaoperasi; //sementara di set sama dl, nanti pas proses fix operasi baru di update lg
                        $modRencana->mulaioperasi = $modRencana->tglrencanaoperasi; //sementara di set sama dl, nanti pas proses fix operasi baru di update lg
                        
                        $modRencana->operasi_id = $attrOperasi[$patokan];
                        $arrOperasi[$i]=array(
                                            'operasi'=> $attrOperasi[$patokan]
                                        );
                        
                        $modRencana->create_time=date('Y-m-d H:i:s');
                        $modRencana->create_loginpemakai_id=Yii::app()->user->id;
                        $modRencana->create_ruangan=Yii::app()->user->getState('ruangan_id');
                        
                        
                        if ($modRencana->validate()){
                            $arrSave[$i] = $modRencana; // menyimpan objek BSRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                            $validRencana = 'true'; // variabel untuk menentukan rencana operasi valid

                        }else{
                            $modRencana->tglrencanaoperasi = Yii::app()->dateFormatter->formatDateTime(
                                    CDateTimeParser::parse($modRencana->tglrencanaoperasi, 'yyyy-MM-dd'), 'medium', null);

                            $validRencana = $validRencana.'false';
                        }
                } //ENDING FOR 
                if($validRencana == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
                {
                    foreach ($arrOperasi as $x => $hasilOperasi) {
                        $operasiNya[$x] = $hasilOperasi['operasi'];
                    }
                    foreach ($arrSave as $f => $simpan) {
                        $simpan->save();
//                        $this->saveTindakanPelayanT($attrPendaftaran,$attrPenunjang,$modRencana,$attrTindakanPelayanan,$operasiNya[$f]);
                    }
                    $this->successSave = true;
                }
                else
                {
                    $this->successSave = false;
                }
            return $modRencana;
        }
        
        
        public function savePasienPenunjang($attrPendaftaran){
            
            $modPasienPenunjang = new BSPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $attrPendaftaran->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
            switch ($modPasienPenunjang->ruangan_id) {
                case Params::RUANGAN_ID_LAB:$inisial_ruangan = 'LK';break;
                case Params::RUANGAN_ID_RAD:$inisial_ruangan = 'RO';break;
                case Params::RUANGAN_ID_REHAB:$inisial_ruangan = 'RE';break;
                case Params::RUANGAN_ID_IBS:$inisial_ruangan = 'BS';break;
                default:break;
            }
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang($inisial_ruangan);
            $modPasienPenunjang->tglmasukpenunjang = $attrPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
            
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }

	
}
