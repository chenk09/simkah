<?php

class RinciantagihanpasienpenunjangBSVController extends SBaseController{
    public function actionIndex(){
        $this->pageTitle = Yii::app()->name." - Rincian Tagihan";
        $modPasienMasukPenunjang = new BSMasukPenunjangV;
        $format = new CustomFormat();
        $modPasienMasukPenunjang->tglAwal = date("Y-m-d").' 00:00:00';
        $modPasienMasukPenunjang->tglAkhir = date('Y-m-d H:i:s');
        if(isset ($_REQUEST['BSMasukPenunjangV'])){
             $modPasienMasukPenunjang->attributes=$_REQUEST['BSMasukPenunjangV'];
             $modPasienMasukPenunjang->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BSMasukPenunjangV']['tglAwal']);
             $modPasienMasukPenunjang->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BSMasukPenunjangV']['tglAkhir']);

             $modPasienMasukPenunjang->ceklis = $_REQUEST['BSMasukPenunjangV']['ceklis'];
        }
        $this->render('index',array(
                          'model'=>$modPasienMasukPenunjang                                 
         ));
    }
    public function actionRincianPenunjang($id, $idPenunjang){
        $this->layout = '//layouts/frameDialog';
        $data['judulLaporan'] = 'Rincian Biaya Sementara Bedah Sentral';
        $modPendaftaran = PendaftaranT::model()->findByPk($id);
        $criteria = new CDbCriteria();
        $criteria->addCondition('pendaftaran_id = '.$id);
        $criteria->addCondition('pasienmasukpenunjang_id = '.$idPenunjang);
        $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
        $criteria->order = 'ruangan_id ASC, tgl_tindakan ASC, daftartindakan_kode DESC';
        $criteria->limit = -1; //unlimited
        $modRincian = RinciantagihanpasienpenunjangV::model()->findAll($criteria);
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        $this->render('bedahSentral.views.rinciantagihanpasienpenunjangBSV.rincianSementaraPenunjang', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data));
    }
}

?>
