<?php

class PendaftaranBedahSentralController extends SBaseController
{
    public $successSave = false;
    public $successSavePJ = true; //variabel untuk validasi data opsional (penanggung jawab)
        
        /**
	 * @return array action filters
	 */ 
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','getOperasi'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{
		
            
            $this->pageTitle = Yii::app()->name." - Pendaftaran Pasien Luar Bedah Sentral";
            $model = new BSPendaftaranMp;
            $modPasienAdmisi = new PasienadmisiT; //hanya untuk mengambil kamarruangan_m
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->umur = "00 Thn 00 Bln 00 Hr";
            $model->ruangan_id = Params::RUANGAN_ID_IBS;
            $model->kelaspelayanan_id = Params::kelasPelayanan('bedah_sentral');
            $modPasien = new BSPasienM;
            $modPasien->tanggal_lahir = date('d M Y');
            $modPenanggungJawab = new BSPenanggungJawabM;
            $modRujukan = new BSRujukanT;
            $modPasienPenunjang = new BSPasienMasukPenunjangT;
            $modRencanaOperasi = new BSRencanaOperasiT;
            $modRencanaOperasi->tglrencanaoperasi = date('d M Y h:i:s');
            $modRencanaOperasi->norencanaoperasi = Generator::noRencanaOperasi();
            $modTindakanPelayanan = new BSTindakanPelayananT;
            $modTindakanKomponen = new BSTindakanKomponenT;
            $modKegiatanOperasi = BSKegiatanOperasiM::model()->findAllByAttributes(array('kegiatanoperasi_aktif'=>true),array('order'=>'kegiatanoperasi_nama'));
            $modOperasi = BSOperasiM::model()->findAllByAttributes(array('operasi_aktif'=>true),array('order'=>'operasi_nama'));
            $attrOperasi = '';
            $attrCeklis = '';
            
            if (isset($_POST['BSPendaftaranMp'])){
                $model->attributes = $_POST['BSPendaftaranMp'];
                $model->create_loginpemakai_id = Yii::app()->user->id;
                $model->create_time = date('Ymd H:i:s');
                $model->create_ruangan = Params::RUANGAN_ID_IBS;
                $modPasien->attributes = $_POST['BSPasienM'];
                $modPenanggungJawab->attributes = $_POST['BSPenanggungJawabM'];
                $modRujukan->attributes = $_POST['BSRujukanT'];
                $modRencanaOperasi->attributes = $_POST['BSRencanaOperasiT'];
                if(isset($_POST['operasi_id']))
                {
                    $attrOperasi = $_POST['operasi_id'];
                    $attrCeklis = $_POST['ceklis'];
                }
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
//                    echo '<pre>';
//                    echo print_r($_POST['BSPendaftaranMp']);
//                    echo '</pre>';
//                    if($_POST['caraAmbilPhoto']=='file')//Jika User Mengambil photo pegawai dengan cara upload file
//                              { 
//                                  $model->pegawai_aktif=true;
//                                  $model->photopegawai = CUploadedFile::getInstance($model, 'photopegawai');
//                                  $gambar=$model->photopegawai;
//
//                                  if(!empty($model->photopegawai))//Klo User Memasukan Logo
//                                  { 
//
//                                        $model->photopegawai =$random.$model->photopegawai;
//
//                                        Yii::import("ext.EPhpThumb.EPhpThumb");
//
//                                         $thumb=new EPhpThumb();
//                                         $thumb->init(); //this is needed
//
//                                         $fullImgName =$model->photopegawai;   
//                                         $fullImgSource = Params::pathPegawaiDirectory().$fullImgName;
//                                         $fullThumbSource = Params::pathPegawaiTumbsDirectory().'kecil_'.$fullImgName;
//
//                                         if($model->save())
//                                              {
//                                                   $gambar->saveAs($fullImgSource);
//                                                   $thumb->create($fullImgSource)
//                                                         ->resize(200,200)
//                                                         ->save($fullThumbSource);
//                                              }
//                                          else
//                                              {
//                                                   Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
//                                              }
//                                    }
//                              }   
//                             else 
//                              {
//                                 $model->photopegawai=$_POST['SAPegawaiM']['tempPhoto'];
//                                 if($model->validate())
//                                    {
//                                        $model->save();
//                                    }
//                                 else 
//                                    {
//                                         unlink(Params::pathPegawaiDirectory().$_POST['SAPegawaiM']['tempPhoto']);
//                                         unlink(Params::pathPegawaiTumbsDirectory().$_POST['SAPegawaiM']['tempPhoto']);
//                                    }
//                               }
                    
                    //==Penyimpanan dan Update Pasien===========================                    
                    if(!isset($_POST['isPasienLama'])){
                        $modPasien = $this->savePasien($_POST['BSPasienM']);
                    }
                    else{
                        $model->isPasienLama = true;
                        if (!empty($_POST['noRekamMedik'])){
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = BSPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            $modPasien->create_loginpemakai_id = Yii::app()->user->id;
                            $modPasien->create_time = date('Ymd H:i:s');
                            $modPasien->create_ruangan = Params::RUANGAN_ID_IBS;
                                        if(!empty($modPasien) && isset($_POST['isUpdatePasien'])) {
                                $modPasien = $this->updatePasien($modPasien, $_POST['BSPasienM']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }
                    
                    //==Akhir Penyimpanan dan Update Pasien=====================
                    
                    $modRujukan = $this->saveRujukan($_POST['BSRujukanT']); //Save Rujukan
                    
                    //===penyimpanan Penanggung Jawab===========================                   
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['BSPenanggungJawabM']);
                    }
                    //===Akhir Penyimpanan Penanggung Jawab=====================                    
                    
                    //==Awal Simpan Pendaftaran=============================
                       $model = $this->savePendaftaranMP($model,$modPasien,$modRujukan,$modPenanggungJawab);
                    //==Akhir Simpan Pendaftaran============================
                       
                    //==Awal Simpan Ubah Cara Bayar=============================
                      $this->saveUbahCaraBayar($model);
                    //==Akhir Simpan Ubah Cara Bayar
                    
                    //==AwalSimpan Psien Masuk Penunjang========================
                      $modPasienPenunjang = $this->savePasienPenunjang($model,$modPasien);
                    //Akhir Pasien MAsuk Penunjang
                      
                    //==AwalSimpan Rencana Operasi========================  
                      if(!empty($_POST['operasi_id']))
                      {
                          $modRencanaOperasi = $this->saveRencanaOperasi($model,$modPasien,$modPasienPenunjang,$modRencanaOperasi,$attrOperasi,$attrCeklis,$_POST['BSTindakanPelayananT']);
                      }
                      else
                      {
                          $modRencanaOperasi->validate();
                          $this->successSave = false;
                      }
                    //Akhir Rencana Operasi
                      
                    //==Awal Simpan Tindakan PelayanT 
//                      if(!empty($_POST['operasi_id']))
//                      {
//                           $modTindakanPelayanan = $this->saveTindakanPelayanT($model,$modPasien,$modPasienPenunjang,$modRencanaOperasi,$_POST['BSTindakanPelayananT']);
//                      }
//                      else
//                      {
//                          $modTindakanPelayanan->validate();
//                          $this->successSave = false;
//                      }
                    //==Akhir Simpan Tindakan pelayananT  
                       
                      
                    if (count($modPasien) != 1){ //Jika Pasien Tidak Ada
                        $modPasien = New BSPasienM;
                        $this->successSave = false;
                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
                        Yii::app()->user->setFlash('warning',"Data Pasien tidak ditemukan isi Nomor Rekam Medik dengan benar");
                    }
                   
                    if ($this->successSave && $modRujukan->save() && $this->successSavePJ = true){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    }
                    else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $model->isRujukan = true;
            $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            
            $this->render('index',array(
                'model'=>$model, 
                'modPasien'=>$modPasien, 
                'modPenanggungJawab'=>$modPenanggungJawab,
                'modRujukan'=>$modRujukan,
                'modPasienPenunjang'=>$modPasienPenunjang,
                'modKegiatanOperasi'=>$modKegiatanOperasi,
                'modOperasi'=>$modOperasi,
                'modRencanaOperasi'=>$modRencanaOperasi,
                'modPasienAdmisi'=>$modPasienAdmisi,
                'modTindakanKomponen'=>$modTindakanKomponen,
                'modTindakanPelayanan'=>$modTindakanPelayanan
                
            ));
	}
        
        public function saveTindakanPelayanT($attrPendaftaran,$attrPasien,$attrPenunjang,$attrRencanaOperasi,$attrTindakanPelayanan,$attrOperasi)
        {
            $validTindakanPelayanan = 'true';
            $arrSave = array();
//            for ($i= 0; $i < count($attrTindakanPelayanan['daftartindakan_id']); $i++) {
                $modTindakanPelayanan = new BSTindakanPelayananT;
                $modTindakanPelayanan->penjamin_id = $attrPendaftaran->penjamin_id;
                $modTindakanPelayanan->pasien_id = $attrPasien->pasien_id;
                $modTindakanPelayanan->create_loginpemakai_id = Yii::app()->user->id;
                $modTIndakanPelayanan->create_time = date('Ymd H:i:s');
                $modTindakanPelayanan->create_ruangan = Params::RUANGAN_ID_IBS;
                $modTindakanPelayanan->kelaspelayanan_id =$attrTindakanPelayanan['kelaspelayanan_id'][$attrOperasi];
                $modTindakanPelayanan->tipepaket_id = 1;
                $modTindakanPelayanan->instalasi_id = Params::INSTALASI_ID_IBS;
                $modTindakanPelayanan->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
                $modTindakanPelayanan->shift_id = Yii::app()->user->getState('shift_id');
                $modTindakanPelayanan->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                $modTindakanPelayanan->daftartindakan_id = $attrTindakanPelayanan['daftartindakan_id'][$attrOperasi];
                $modTindakanPelayanan->carabayar_id = $attrPendaftaran->carabayar_id;
                $modTindakanPelayanan->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
                $modTindakanPelayanan->tgl_tindakan = date('Y-m-d H:i:s');
                $modTindakanPelayanan->tarif_tindakan = $attrTindakanPelayanan['tarif_tindakan'][$attrOperasi];
                $modTindakanPelayanan->satuantindakan = $attrTindakanPelayanan['satuantindakan'][$attrOperasi];
                $modTindakanPelayanan->qty_tindakan = $attrTindakanPelayanan['qty_tindakan'][$attrOperasi];
                $modTindakanPelayanan->cyto_tindakan = $attrTindakanPelayanan['cyto_tindakan'][$attrOperasi];
                if($modTindakanPelayanan->cyto_tindakan){
                    $modTindakanPelayanan->tarifcyto_tindakan = $modTindakanPelayanan->tarif_tindakan * ($attrTindakanPelayanan['persencyto_tind'][$attrOperasi]/100);
                } else {
                    $modTindakanPelayanan->tarifcyto_tindakan = 0;
                }
                $modTindakanPelayanan->discount_tindakan = 0;
                $modTindakanPelayanan->dokterpemeriksa1_id = $attrRencanaOperasi->dokterpelaksana1_id;
                $modTindakanPelayanan->dokterpemeriksa2_id = (!empty($attrRencanaOperasi->dokterpelaksana2)) ? $attrRencanaOperasi->dokterpelaksana2 : null;
                $modTindakanPelayanan->perawat_id = (!empty($attrRencanaOperasi->perawat_id)) ? $attrRencanaOperasi->perawat_id : null;
                $modTindakanPelayanan->subsidiasuransi_tindakan=0;
                $modTindakanPelayanan->subsidipemerintah_tindakan=0;
                $modTindakanPelayanan->subsisidirumahsakit_tindakan=0;
                $modTindakanPelayanan->iurbiaya_tindakan=0;
                
                if ($modTindakanPelayanan->validate()){
                    $arrSave[$i] = $modTindakanPelayanan; // menyimpan objek BSRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                    $validTindakanPelayanan = 'true';

                }else
                {   
                    $validTindakanPelayanan = $validTindakanPelayanan.'false';
                }
//            } //ENDING FOR
            if($validTindakanPelayanan == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $x => $simpan) {
                    $simpan->save();
                    $this->saveTindakanKomponenT($simpan);
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }

            return $modTindakanPelayanan;
        }
        
        public function saveTindakanKomponenT($attrTindakanPelayanan)
        {
            $arrSave = array();
            $validTindakanKomponen = 'true';
            $daftarTindakan_id = $attrTindakanPelayanan->daftartindakan_id;
            $kelaspelayanan_id = $attrTindakanPelayanan->kelaspelayanan_id;
            $arrTarifTindakan = "select * from tariftindakan_m where daftartindakan_id = ".$daftarTindakan_id." and kelaspelayanan_id = ".$kelaspelayanan_id." and komponentarif_id <> ".Params::KOMPONENTARIF_ID_TOTAL." ";
            $query = Yii::app()->db->createCommand($arrTarifTindakan)->queryAll();
            foreach ($query as $i => $tarifKomponen) {
                $modTarifKomponen = new BSTindakanKomponenT;
                $modTarifKomponen->tindakanpelayanan_id = $attrTindakanPelayanan->tindakanpelayanan_id;
                $modTarifKomponen->komponentarif_id = $tarifKomponen['komponentarif_id'];
                $modTarifKomponen->tarif_tindakankomp = $tarifKomponen['harga_tariftindakan'];
                if($attrTindakanPelayanan->cyto_tindakan){
                    $modTarifKomponen->tarifcyto_tindakankomp = $tarifKomponen['harga_tariftindakan'] * ($tarifKomponen['persencyto_tind']/100);
                } else {
                    $modTarifKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTarifKomponen->subsidiasuransikomp = 0;
                $modTarifKomponen->subsidipemerintahkomp = 0;
                $modTarifKomponen->subsidirumahsakitkomp = 0;
                $modTarifKomponen->iurbiayakomp = 0;
                if ($modTarifKomponen->validate()){
                    $arrSave[$i] = $modTarifKomponen; // menyimpan objek tarif komponen ke dalam sebuah array dan siap untuk disave
                    $validTindakanKomponen = 'true';

                }else
                {
                    $validTindakanKomponen = $validTindakanKomponen.'false';
                }
            } // ending foreach
            if($validTindakanKomponen == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
            {
                foreach ($arrSave as $f => $simpan) {
                    $simpan->save();
                }
                $this->successSave = true;
            }
            else
            {
                $this->successSave = false;
            }
            return $modTarifKomponen;
        }
        
        public function savePasien($attrPasien)
        {
            $modPasien = new BSPasienM;
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modPasien->no_rekam_medik = Generator::noRekamMedikPenunjang(Params::singkatanNoPendaftaranIBS());
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = 'AKTIF';
            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modPasien->ispasienluar = 1;
            $modPasien->create_loginpemakai_id = Yii::app()->user->id;
            $modPasien->create_time = date('Ymd H:i:s');
            $modPasien->create_ruangan = Params::RUANGAN_ID_IBS;

            if($modPasien->validate()) {
                // form inputs are valid, do something here
                $modPasien->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            return $modPasien;
        }
        
        public function updatePasien($modPasien,$attrPasien)
        {
            $modPasienupdate = $modPasien;
            $modPasienupdate->attributes = $attrPasien;
            $modPasienupdate->kelompokumur_id = Generator::kelompokUmur($modPasienupdate->tanggal_lahir);
            $modPasienupdate->ispasienluar = 1;
            
            if($modPasienupdate->validate()) {
                // form inputs are valid, do something here
                $modPasienupdate->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasienupdate->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasienupdate->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            
            return $modPasienupdate;
        }
        
        public function saveRujukan($attrRujukan)
        {
            $modRujukan = new BSRujukanT;
            $modRujukan->attributes = $attrRujukan;
            $modRujukan->tanggal_rujukan = ($attrRujukan['tanggal_rujukan'] == '') ? date('Y-m-d') : $attrRujukan['tanggal_rujukan'] ;
            if($modRujukan->validate()) {
                // form inputs are valid, do something here
                $modRujukan->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modRujukan->tanggal_rujukan = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modRujukan->tanggal_rujukan, 'yyyy-MM-dd'),'medium',null);
                
            }
            return $modRujukan;
        }
        
        public function savePenanggungJawab($attrPenanggungJawab)
        {
            $modPenanggungJawab = new BSPenanggungJawabM;
            $modPenanggungJawab->attributes = $attrPenanggungJawab;
            if($modPenanggungJawab->validate()) {
                // form inputs are valid, do something here
                $modPenanggungJawab->save();
                $this->successSavePJ = TRUE;
            } else {
                // mengembalikan format tanggal contoh 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                //if($modPenanggungJawab->tgllahir_pj != null)
                //$modPenanggungJawab->tgllahir_pj = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenanggungJawab->tgllahir_pj, 'yyyy-MM-dd'),'medium',null);
                $this->successSavePJ = FALSE;
            }

            return $modPenanggungJawab;
        }
        
         public function savePendaftaranMP($model, $modPasien, $modRujukan, $modPenanggungJawab){
            $modelNew = new BSPendaftaranMp;
            $modelNew->attributes = $model->attributes;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            if (isset($modelNew->ruangan_id)){
                  $inisial_ruangan = 'BS';
            }
            $modelNew->instalasi_id = Generator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
            $modelNew->no_pendaftaran = Generator::noPendaftaran(Params::singkatanNoPendaftaranIBS());
            $modelNew->no_urutantri = Generator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = Generator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modelNew->statuspasien = Generator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = Generator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->statusmasuk = Params::statusMasuk('rujukan');
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
           
            if ($modelNew->validate()){
                $modelNew->Save();
                $this->successSave = true;
            }else{
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'), 'medium', null);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
            
            return $modelNew;
        }
        
        public function saveRencanaOperasi($attrPendaftaran,$attrPasien,$attrPenunjang,$attrRencana,$attrOperasi,$attrCeklis,$attrTindakanPelayanan)
        {
            $arrSave = array();
            $validRencana = 'true';
            $arrOperasi = array(); // array untuk menampung operasi yg nantinnya digunakan pada proses saveTindakanPelayanan
            for ($i = 0; $i < count($attrCeklis); $i++) {
                    $patokan = $attrCeklis[$i];

                        $modRencana = new BSRencanaOperasiT;
                        $modRencana->attributes = $attrRencana->attributes;
                        $modRencana->norencanaoperasi = Generator::noRencanaOperasi();
                        $modRencana->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                        $modRencana->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
                        $modRencana->pasien_id = $attrPasien->pasien_id;
                        
                        $modRencana->kamarruangan_id = (!empty($modRencana->kamarruangan_id)) ? $modRencana->kamarruangan_id : null ;
                        $modRencana->dokterpelaksana2_id = (!empty($modRencana->dokterpelaksana2_id)) ? $modRencana->dokterpelaksana2_id : null ;
                        $modRencana->perawat_id = (!empty($modRencana->perawat_id)) ? $modRencana->perawat_id : null ;
                        $modRencana->dokteranastesi_id = (!empty($modRencana->dokteranastesi_id)) ? $modRencana->dokteranastesi_id : null ;
                        
                        $modRencana->selesaioperasi = $modRencana->tglrencanaoperasi; //sementara di set sama dl, nanti pas proses fix operasi baru di update lg
                        $modRencana->mulaioperasi = $modRencana->tglrencanaoperasi; //sementara di set sama dl, nanti pas proses fix operasi baru di update lg
                        
                        $modRencana->operasi_id = $attrOperasi[$patokan];
                        $arrOperasi[$i]=array(
                                            'operasi'=> $attrOperasi[$patokan]
                                        );
                        
                        $modRencana->create_time=date('Y-m-d H:i:s');
                        $modRencana->create_loginpemakai_id=Yii::app()->user->id;
                        $modRencana->create_ruangan=Yii::app()->user->getState('ruangan_id');
                        
                        
                        
                        if ($modRencana->validate()){
                            $arrSave[$i] = $modRencana; // menyimpan objek BSRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                            $validRencana = 'true'; // variabel untuk menentukan rencana operasi valid

                        }else{
                            $modRencana->tglrencanaoperasi = Yii::app()->dateFormatter->formatDateTime(
                                    CDateTimeParser::parse($modRencana->tglrencanaoperasi, 'yyyy-MM-dd'), 'medium', null);

                            $validRencana = $validRencana.'false';
                        }
                } //ENDING FOR 
                if($validRencana == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
                {
                    foreach ($arrOperasi as $x => $hasilOperasi) {
                        $operasiNya[$x] = $hasilOperasi['operasi'];
                    }
                    
                    foreach ($arrSave as $f => $simpan) {
                        
                        $simpan->save();
//                        $this->saveTindakanPelayanT($attrPendaftaran,$attrPasien,$attrPenunjang,$modRencana,$attrTindakanPelayanan,$operasiNya[$f]);
                    }
                    $this->successSave = true;
                }
                else
                {
                    $this->successSave = false;
                }
            return $modRencana;
        }
        
        
        public function savePasienPenunjang($attrPendaftaran,$attrPasien){
            
            $modPasienPenunjang = new BSPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $attrPasien->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
            switch ($modPasienPenunjang->ruangan_id) {
                case Params::RUANGAN_ID_LAB:$inisial_ruangan = 'LK';break;
                case Params::RUANGAN_ID_RAD:$inisial_ruangan = 'RO';break;
                case Params::RUANGAN_ID_REHAB:$inisial_ruangan = 'RE';break;
                case Params::RUANGAN_ID_IBS:$inisial_ruangan = 'BS';break;
                default:break;
            }
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang($inisial_ruangan);
            $modPasienPenunjang->tglmasukpenunjang = $attrPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
            
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }
        
        public function saveUbahCaraBayar($model) 
        {
            $modUbahCaraBayar = new BSUbahCaraBayarR;
            $modUbahCaraBayar->pendaftaran_id = $model->pendaftaran_id;
            $modUbahCaraBayar->carabayar_id = $model->carabayar_id;
            $modUbahCaraBayar->penjamin_id = $model->penjamin_id;
            $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d');
            $modUbahCaraBayar->alasanperubahan = 'x';
            if($modUbahCaraBayar->validate())
            {
                // form inputs are valid, do something here
                $modUbahCaraBayar->save();
            }
            
        }
        
        
        public function actionGetOperasi()
        {
           if(Yii::app()->request->isAjaxRequest){
                 
                $kegiatanoperasi_id = $_GET['kegiatanoperasi'];
                $kelaspelayanan_id = $_GET['kelaspelayanan'];
                
                $criteria = new CDbCriteria();
                $criteria->compare('LOWER(operasi_nama)', strtolower($_GET['term']), true);
                $criteria->compare('kegiatanoperasi_id', $kegiatanoperasi_id);
                $criteria->order = 'operasi_nama';
                $models = OperasiM::model()->findAll($criteria);

                  $returnVal = '';
                  foreach($models as $i=>$model)
                  {
                     $returnVal .= $model['operasi_nama'].'|'
                                   .$model['operasi_id'].'|'
                                   .$model['daftartindakan_id']."\n";
                                   
                  }

                  echo $returnVal;
             }
             Yii::app()->end();
        }
        
        

}
