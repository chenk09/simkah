<?php

class InformasiBedahSentralController extends SBaseController
{
        
    
	public function actionInformasiJadwalOperasi()
	{
                $model = new BSRencanaoperasiT('search');
                $model->tglAwal = date('Y-m-d 00:00:00');
                $model->tglAkhir = date('Y-m-d 23:59:59');
                if (isset($_GET['BSRencanaoperasiT'])) {
                        $format = new CustomFormat;
                        $model->attributes = $_GET['BSRencanaoperasiT'];
                        $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BSRencanaoperasiT']['tglAwal']);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BSRencanaoperasiT']['tglAkhir']);
                }
                    
		$this->render('jadwalOperasi/admin',array('model'=>$model));
	}
        
       

}