<?php

class RujukanPenunjangController extends SBaseController
{
        /**
	 * @return array action filters
	 */
    
        public $successSave = false;
         
//	DIKOMEN KARENA FILTER MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','masukPenunjang'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
        
	public function actionIndex()
	{
            $this->pageTitle = Yii::app()->name." - Pasien Rujukan";
            $criteria = new CDbCriteria;
            if(isset($_GET['ajax']) && $_GET['ajax']=='pasienpenunjangrujukan-m-grid') {
                $format = new CustomFormat;
                echo $format->formatDateTimeMediumForDB($_GET['tglAkhir']);
                $criteria->compare('LOWER(no_pendaftaran)', strtolower($_GET['noPendaftaran']),true);
                $criteria->compare('LOWER(nama_pasien)', strtolower($_GET['namaPasien']),true);
                $criteria->compare('LOWER(no_rekam_medik)', strtolower($_GET['noRekamMedik']),true);
                if($_GET['cbTglMasuk'])
                    $criteria->addBetweenCondition('tgl_kirimpasien', "'".$format->formatDateTimeMediumForDB($_GET['tglAwal'])."'", "'".$format->formatDateTimeMediumForDB($_GET['tglAkhir'])."'");
            } else {
                //$criteria->addBetweenCondition('tgl_pendaftaran', date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59');
            }
            $criteria->compare('instalasi_id', Params::INSTALASI_ID_IBS); //NANTI DIGANTI AMA SESSION, SEMENTARA PAKE PARAM DL
            $criteria->order='tgl_kirimpasien DESC';
            
            $dataProvider = new CActiveDataProvider(PasienkirimkeunitlainV::model(), array(
			'criteria'=>$criteria,
		));
            $this->render('index',array('dataProvider'=>$dataProvider));
	}
        



        /**
        * Dimodifikasi oleh @author David Yanuar | EHJ-1905 | 20-05-2014
        * 
        * tambahkan $format = new CustomFormat;
        * lalu ganti :
        * $modRencanaOperasi->tglrencanaoperasi = date('Y-m-d h:i:s');
        * menjadi :
        * $modRencanaOperasi->tglrencanaoperasi = $format->formatDateINAtime(date('Y-m-d h:i:s'));
        */
        public function actionMasukPenunjang($idPasienKirimKeUnitLain,$idPendaftaran)
        {            
            $this->pageTitle = Yii::app()->name." - Rencana Operasi";
            $format = new CustomFormat;
            $modPendaftaran = BSPendaftaranMp::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = BSPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modOperasi = BSOperasiM::model()->findAllByAttributes(array('operasi_aktif'=>true));
            $modKegiatanOperasi = BSKegiatanOperasiM::model()->findAllByAttributes(array('kegiatanoperasi_aktif'=>true));
            $modPermintaan = BSPermintaanKePenunjangT::model()->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            $modPasienKirimKeunitLain = PasienkirimkeunitlainT::model()->findByPk($idPasienKirimKeUnitLain);
            $modRencanaOperasi = new BSRencanaOperasiT;
            $modRencanaOperasi->norencanaoperasi = KeyGenerator::noRencanaOperasi();
//            $modRencanaOperasi->tglrencanaoperasi = date('Y-m-d h:i:s');
            $modRencanaOperasi->tglrencanaoperasi = $format->formatDateINAtimeNew(date('Y-m-d h:i:s'));
            $modRencanaOperasi->statusoperasi = Params::KODE_RENCANA_OPERASI;
            $modPenunjang = new BSMasukPenunjangV;
            $modPenunjangSave = new BSPasienMasukPenunjangT;
                  
            if(!empty($modPendaftaran->pasienadmisi_id)){
                $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                if(!empty($modPasienAdmisi->pasienpulang_id)){
                    $status = "SUDAH PULANG";
                }
            }else{
                $status = $modPendaftaran->statusperiksa;
            }
            
            if($status == "SUDAH PULANG"){
                echo "<script>
                        alert('Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan rencana operasi. ');
                        window.top.location.href='".Yii::app()->createUrl('bedahSentral/RujukanPenunjang/index')."';
                    </script>";
            }
            
            if(isset($_POST['BSRencanaOperasiT'])) {
                $_POST['BSRencanaOperasiT']['tglrencanaoperasi'] = $format->formatDateTimeMediumForDB($_POST['BSRencanaOperasiT']['tglrencanaoperasi']);

                if(!empty($_POST['operasi_id']))
                {
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        if(!empty($_POST['PasienkirimkeunitlainT']['kelaspelayanan_id'])){
                            $modPasienKirimKeunitLain->kelaspelayanan_id = $_POST['PasienkirimkeunitlainT']['kelaspelayanan_id'];
                            $modPasienKirimKeunitLain->save();
                        }
                        $modPenunjangSave = $this->savePasienPenunjang($modPendaftaran,$modPasienKirimKeunitLain);
                        $modRencanaOperasi = $this->saveRencanaOperasi($modPenunjangSave,$_POST['BSRencanaOperasiT'],$_POST['operasi_id']);
                        if ($this->successSave){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                            $module = Yii::app()->controller->module->id.'/DaftarPasien';
                            $this->redirect(array('/'.$module));
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    } catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }
                else{
                    Yii::app()->user->setFlash('error',"Data gagal disimpan, anda belum memilih operasi");
                }
            }
            
            
            $modRiwayatKirimKeUnitLain = BSPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                    'ruangan_id'=>Params::RUANGAN_ID_IBS,),
                                                                                                    'pasienmasukpenunjang_id IS NULL');
            $modRiwayatPenunjang = BSPasienMasukPenunjangT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                             'ruangan_id'=>Params::RUANGAN_ID_IBS,));
            
            $this->render('masukPenunjang',array('modPermintaan'=>$modPermintaan,
                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
                                        'modKegiatanOperasi'=>$modKegiatanOperasi,
                                        'modOperasi'=>$modOperasi,
                                        'modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modRiwayatPenunjang'=>$modRiwayatPenunjang,
                                        'modRencanaOperasi'=>$modRencanaOperasi,
                                        'modPenunjang'=>$modPenunjang,
                                        'modPenunjangSave'=>$modPenunjangSave,
                                        'modPasienKirimKeunitLain'=>$modPasienKirimKeunitLain
                          ));
        }
        
        /**
         * Fungsi untuk menyimpan data ke model BSPasienMasukPenunjangT
         * @param type $modPendaftaran
         * @param type $modPasien
         * @return ROPasienMasukPenunjangT 
         */
        protected function savePasienPenunjang($modPendaftaran,$modPasienKirimKeunitLain){
            $modPasienPenunjang = new BSPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $modPendaftaran->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $modPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $modPasienKirimKeunitLain->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_IBS;   //$modPendaftaran->ruangan_id;
            $modPasienPenunjang->no_masukpenunjang = KeyGenerator::noMasukPenunjang('BS');
            $modPasienPenunjang->tglmasukpenunjang = date('Y-m-d H:i:s'); 
            $modPasienPenunjang->no_urutperiksa =  KeyGenerator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $modPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $modPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $modPendaftaran->ruangan_id;
            $modPasienPenunjang->pasienkirimkeunitlain_id = $modPasienKirimKeunitLain->pasienkirimkeunitlain_id;

            if ($modPasienPenunjang->validate()){ 
                $modPasienPenunjang->Save();
                $this->successSave = true;
//                $this->updatePasienKirimKeUnitLain($modPasienPenunjang); << TIDAK MENGUPDATE
                $modPasienKirimKeunitLain->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
                $modPasienKirimKeunitLain->save();
            } else {
                $this->successSave = false;
            }
            
            return $modPasienPenunjang;
        }
        
        
        /**
        * Dimodifikasi oleh @author David Yanuar | EHJ-1905 | 20-05-2014
        *
        * Hapus : 
        * $modRencana->tglrencanaoperasi = Yii::app()->dateFormatter->formatDateTime(
        * CDateTimeParser::parse($modRencana->tglrencanaoperasi, 'yyyy-MM-dd'), 'medium', null);
        *                 
        * Ganti dengan :
        * $modRencana->tglrencanaoperasi = $format->formatDateINAtime($modRencana->tglrencanaoperasi);
        */
        public function saveRencanaOperasi($attrPenunjang,$attrRencana,$attrOperasi)
        {
            $arrSave = array();
            $validRencana = 'true';
            $arrOperasi = array(); // array untuk menampung operasi yg nantinnya digunakan pada proses saveTindakanPelayanan
            for ($i = 0; $i < count($attrOperasi); $i++) {
                        $format = new CustomFormat();
                        $modRencana = new BSRencanaOperasiT;
                        $modRencana->attributes = $attrRencana;
                        $modRencana->norencanaoperasi = KeyGenerator::noRencanaOperasi();
                        $modRencana->pasienmasukpenunjang_id = $attrPenunjang->pasienmasukpenunjang_id;
                        $modRencana->pendaftaran_id = $attrPenunjang->pendaftaran_id;
                        $modRencana->pasien_id = $attrPenunjang->pasien_id;
                        
                        $modRencana->dokterpelaksana1_id = $attrRencana['dokterpelaksana1_id'];
                        $modRencana->kamarruangan_id = (!empty($modRencana->kamarruangan_id)) ? $modRencana->kamarruangan_id : null ;
                        $modRencana->dokterpelaksana2_id = (!empty($modRencana->dokterpelaksana2_id)) ? $modRencana->dokterpelaksana2_id : null ;
                        $modRencana->perawat_id = (!empty($modRencana->perawat_id)) ? $modRencana->perawat_id : null ;
                        $modRencana->dokteranastesi_id = (!empty($modRencana->dokteranastesi_id)) ? $modRencana->dokteranastesi_id : null ;
                        
                        
                        $modRencana->selesaioperasi = $format->formatDateTimeMediumForDB($modRencana->tglrencanaoperasi); //sementara di set sama dl, nanti pas proses fix operasi baru di update lg
                        $modRencana->mulaioperasi = $format->formatDateTimeMediumForDB($modRencana->tglrencanaoperasi); //sementara di set sama dl, nanti pas proses fix operasi baru di update lg
                        
                        $modRencana->operasi_id = $attrOperasi[$i];
                        $arrOperasi[$i]=array(
                                            'operasi'=> $attrOperasi[$i]
                                        );
                        
                        $modRencana->create_time=date('Y-m-d H:i:s');
                        $modRencana->create_loginpemakai_id=Yii::app()->user->id;
                        $modRencana->create_ruangan=Yii::app()->user->getState('ruangan_id');

                        
                        if ($modRencana->validate()){
                            $arrSave[$i] = $modRencana; // menyimpan objek BSRencanaOperasiT ke dalam sebuah array dan siap untuk disave
                            $validRencana = 'true'; // variabel untuk menentukan rencana operasi valid

                        }else{
                            // $modRencana->tglrencanaoperasi = Yii::app()->dateFormatter->formatDateTime(
                            //         CDateTimeParser::parse($modRencana->tglrencanaoperasi, 'yyyy-MM-dd'), 'medium', null);
                            $modRencana->tglrencanaoperasi = $format->formatDateINAtimeNew($modRencana->tglrencanaoperasi);
                            
                            $validRencana = $validRencana.'false';

                            
                        }
                } //ENDING FOR 
                if($validRencana == 'true') //kondisi apabila semua rencana operasi valid dan siap untuk di save
                {
                    foreach ($arrOperasi as $x => $hasilOperasi) {
                        $operasiNya[$x] = $hasilOperasi['operasi'];
                    }
                    
                    foreach ($arrSave as $f => $simpan) {
                        
                        $simpan->save();
                        
//                        $this->saveTindakanPelayanT($attrPendaftaran,$attrPasien,$attrPenunjang,$modRencana,$attrTindakanPelayanan,$operasiNya[$f]);
                    }
                    $this->successSave = true;
                }
                else
                {
                    $this->successSave = false;
                }
            return $modRencana;
        }
        
        protected function updatePasienKirimKeUnitLain($modPasienPenunjang) {
            
            if(!empty($_POST['permintaanPenunjang'])){
                foreach($_POST['permintaanPenunjang'] as $i => $item) {
                    PasienkirimkeunitlainT::model()->updateByPk($item['idPasienKirimKeUnitLain'], 
                                                                array('pasienmasukpenunjang_id'=>$modPasienPenunjang->pasienmasukpenunjang_id));
                }
            }
        }
        /**
         * membatalkan rujukan dari daftar pasien rujukan
         */
        public function actionBatalRujuk(){
            if(Yii::app()->request->isAjaxRequest) {
            $idPendaftaran = $_POST['idPendaftaran'];
            $idKirimUnit = $_POST['idKirimUnit'];
            PermintaankepenunjangT::model()->deleteAllByAttributes(array('pasienkirimkeunitlain_id'=>$idKirimUnit));
            PasienkirimkeunitlainT::model()->deleteByPk($idKirimUnit);
            $data['status'] = 'ok';
            $data['keterangan']= "<div class='flash-success'>pasien berhasil dibatalkan</div>";

            echo json_encode($data);
             Yii::app()->end();
            }
        }

	
}