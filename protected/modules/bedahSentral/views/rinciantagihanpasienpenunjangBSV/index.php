<legend class="rim2">Informasi Rincian Tagihan Pasien Bedah Sentral</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
        $('#rjrinciantagihanpasien-v-grid').addClass('srbacLoading');
	$.fn.yiiGridView.update('rjrinciantagihanpasien-v-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php //echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>


    <?php
//    $data[] = array(
//'rowid' => 1,
//'id' => 2,
//'name' =>3,
//'qty' => 4,
//'price' => 5,
//'subtotal' => 6
//);
//    echo print_r($data[0]['price']);
    ?>

<?php 
$module  = $this->module->name; 
$controller = $this->id;
?><?php $this->widget('bootstrap.widgets.BootAlert');	?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'rjrinciantagihanpasien-v-grid',
	'dataProvider'=>$model->searchBS(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tgl_pendaftaran',
                array(
                    'header'=>'No Rekam Medik<br/>No Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->no_rekam_medik.\'<br/>\'.$data->no_pendaftaran',
                ),
                array(
                    'header'=>'Nama Pasien',
                    'type'=>'raw',
                    'value'=>'$data->nama_pasien.\'<br/>\'.$data->nama_bin',
                ),
                array(
                    'header'=>'Cara Bayar<br/>Penjamin',
                    'type'=>'raw',
                    'value'=>'$data->CaraBayarPenjamin',
                ),
                'nama_pegawai',
                'jeniskasuspenyakit_nama',
                array(
                    'header'=>'Status Bayar',
                    'type'=>'raw',
                    'value'=>'(empty($data->pembayaranpelayanan_id)) ? "Belum Lunas" : "Lunas"' ,
                ),
                array(
                    'header'=>'Total Tagihan',
                    'type'=>'raw',
                    'value'=>'number_format($data->Totaltagihan,2,\',\',\'.\')',  
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Rincian',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<icon class=\'icon-list-brown\'></idcon>", Yii::app()->createUrl("'.$module.'/'.$controller.'/rincianPenunjang", array("id"=>$data->pendaftaran_id, "idPenunjang"=>$data->pasienmasukpenunjang_id)), array("target"=>"frameRincian", "onclick"=>"$(\'#dialogRincian\').dialog(\'open\');"))','htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
    </div>

<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogRincian',
    'options' => array(
        'title' => 'Rincian Tagihan Pasien',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 550,
        'resizable' => false,
    ),
));
?>
<iframe name='frameRincian' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>