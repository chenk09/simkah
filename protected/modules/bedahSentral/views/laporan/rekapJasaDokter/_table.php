<?php 
    $rim = 'max-width:1300px;overflow-x:scroll;';
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $data = $model->searchJasaDokter();
    $template = "{pager}{summary}\n{items}";
    $sort = true;
    if (isset($caraPrint)){
      $sort = false;
      $data = $model->searchPrintJasaDokter(); 
      $rim = '';
      $template = "{items}";
      if ($caraPrint == "EXCEL")
          $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
?>

<div id="div_rekap">
    <div style="<?php echo $rim; ?>">
        <?php
            if(isset($caraPrint)){
                $dataDetail = $model->searchPrintJasaDokter();
            }else{
                $dataDetail = $model->searchJasaDokter();
        ?>
        <legend class="rim"> Table Rekap Jasa Dokter </legend>
        <?php } ?>
    <?php 
        $this->widget($table,array(
            'id'=>'laporanrekapjasadokter-grid',
            'dataProvider'=>$dataDetail,
            'enableSorting'=>$sort,
            'template'=>$template,
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                 'mergeHeaders'=>array(
                    array(
                        'name'=>'<center>Dokter</center>',
                        'start'=>1, //indeks kolom 3
                        'end'=>3, //indeks kolom 4
                    ),
                    array(
                        'name'=>'<center>Jasa</center>',
                        'start'=>9, //indeks kolom 3
                        'end'=>15, //indeks kolom 4
                    ),
                ),
                'columns'=>array(
                    array(
                        'header' => 'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                    ),
                    array(
                      'header'=>'Gelar Depan',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Nama Dokter',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Gelar Belakang',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Kategori',
                      'type'=>'raw',
                      'value'=>'$data->instalasi_nama',
                    ),
                    array(
                      'header'=>'Kode Bedah',
                      'type'=>'raw',
                      'value'=>'$data->ruangan_nama',
                    ),
                    array(
                      'header'=>'Tindakan Bedah',
                      'type'=>'raw',
                      'value'=>'$data->ruangan_nama',
                    ),
                    array(
                      'header'=>'Bedah',
                      'type'=>'raw',
                      'value'=>'$data->qty_tindakan',
                    ),
                    array(
                      'header'=>'BAHP Anastesi',
                      'type'=>'raw',
                      'value'=>'(empty($data->gelardepan) ? "-" : "$data->gelardepan" )',
                    ),
                    array(
                      'header'=>'Operator',
                      'type'=>'raw',
                      'value'=>'(empty($data->nama_pegawai) ? "-" : "$data->nama_pegawai" )',
                    ),
                    array(
                      'header'=>'Anastesi',
                      'type'=>'raw',
                      'value'=>'(empty($data->gelarbelakang_nama) ? "-" : "$data->gelarbelakang_nama" )',
                    ),
                    array(
                      'header'=>'Assanastesi',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifVisit",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Assaoperator',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifKonsul",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Perujuk',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifKonsul",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Sewa Alat',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTindakan",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Sewa Ruang',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifOperator",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Assisten Operator',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Assisten Operator 2',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Assisten Anastesi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Assisten Anastesi 2',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Anastesi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Is Custom',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Is Penyulit',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Insentive',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Resusitasi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Ass Resusitasi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Observasi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Gas Anasthesi',
                      'type'=>'raw',
                      'value'=>'(empty($data->gelardepan) ? "-" : "$data->gelardepan" )',
                    ),
                    array(
                      'header'=>'Obat Anasthesi',
                      'type'=>'raw',
                      'value'=>'(empty($data->gelardepan) ? "-" : "$data->gelardepan" )',
                    ),
                    array(
                      'header'=>'Item Lainnya',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Qty',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Item P3',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Qty P3',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Claim P3',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Sub Total P3',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Discount',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'ADM',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Cyto',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Selisih Tarif',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Total',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Posting',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Closing',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Status Transaksi',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Keterangan',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),

                ),
                'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
            )); 
        ?>
    </div>
</div>

<div id="div_detail">
    <div style="<?php echo $rim; ?>">
        <?php
            if(isset($caraPrint)){
                $dataDetail = $model->searchPrintDetailJasaDokter();
            }else{
                $dataDetail = $model->searchDetailJasaDokter();
        ?>
        <legend class="rim"> Table Detail Jasa Dokter </legend>
        <?php } ?>
    <?php 
        $this->widget($table,array(
            'id'=>'laporandetailjasadokter-grid',
            'dataProvider'=>$dataDetail,
            'enableSorting'=>$sort,
            'template'=>$template,
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                 'mergeHeaders'=>array(
                    array(
                        'name'=>'<center>Dokter</center>',
                        'start'=>5, //indeks kolom 3
                        'end'=>7, //indeks kolom 4
                    ),
                    array(
                        'name'=>'<center>Jasa</center>',
                        'start'=>13, //indeks kolom 3
                        'end'=>19, //indeks kolom 4
                    ),
                ),
                'columns'=>array(
                    array(
                        'header' => 'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                    ),
                    array(
                      'header'=>'Tgl Transaksi',
                      'type'=>'raw',
                      'value'=>'$data->tgl_pendaftaran',
                    ),
                    array(
                      'header'=>'No Pendaftaran /<br> No Rekam Medik',
                      'type'=>'raw',
                      'value'=>'$data->no_rekam_medik',
                    ),
                    array(
                        'header'=>'Nama Pasien',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien',
                    ),
                    array(
                        'header'=>'Ruangan',
                        'type'=>'raw',
                        'value'=>'$data->ruangan_nama',
                    ),
                    array(
                      'header'=>'Gelar Depan',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Nama Dokter',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Gelar Belakang',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Kategori',
                      'type'=>'raw',
                      'value'=>'$data->instalasi_nama',
                    ),
                    array(
                      'header'=>'Kode Bedah',
                      'type'=>'raw',
                      'value'=>'$data->ruangan_nama',
                    ),
                    array(
                      'header'=>'Tindakan Bedah',
                      'type'=>'raw',
                      'value'=>'$data->ruangan_nama',
                    ),
                    array(
                      'header'=>'Bedah',
                      'type'=>'raw',
                      'value'=>'$data->qty_tindakan',
                    ),
                    array(
                      'header'=>'BAHP Anastesi',
                      'type'=>'raw',
                      'value'=>'(empty($data->gelardepan) ? "-" : "$data->gelardepan" )',
                    ),
                    array(
                      'header'=>'Operator',
                      'type'=>'raw',
                      'value'=>'(empty($data->nama_pegawai) ? "-" : "$data->nama_pegawai" )',
                    ),
                    array(
                      'header'=>'Anastesi',
                      'type'=>'raw',
                      'value'=>'(empty($data->gelarbelakang_nama) ? "-" : "$data->gelarbelakang_nama" )',
                    ),
                    array(
                      'header'=>'Assanastesi',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifVisit",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Assaoperator',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifKonsul",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Perujuk',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifKonsul",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Sewa Alat',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTindakan",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Sewa Ruang',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifOperator",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Assisten Operator',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Assisten Operator 2',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Assisten Anastesi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Assisten Anastesi 2',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Anastesi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Is Custom',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Is Penyulit',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Insentive',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Resusitasi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Ass Resusitasi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Observasi',
                      'type'=>'raw',
                      'value'=>'$data->nama_pasien',
                    ),
                    array(
                      'header'=>'Gas Anasthesi',
                      'type'=>'raw',
                      'value'=>'(empty($data->gelardepan) ? "-" : "$data->gelardepan" )',
                    ),
                    array(
                      'header'=>'Obat Anasthesi',
                      'type'=>'raw',
                      'value'=>'(empty($data->gelardepan) ? "-" : "$data->gelardepan" )',
                    ),
                    array(
                      'header'=>'Item Lainnya',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Qty',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Item P3',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Qty P3',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Claim P3',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Sub Total P3',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Discount',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'ADM',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Cyto',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Selisih Tarif',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Total',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Posting',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Closing',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Status Transaksi',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                    array(
                      'header'=>'Keterangan',
                      'type'=>'raw',
                      'value'=>'$this->grid->owner->renderPartial("bedahSentral.views.laporan/rekapJasaDokter/_tarifTotal",array(pendaftaran_id=>"$data->pendaftaran_id",ruangan_id=>"$data->ruangan_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                      'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),

                ),
                'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
            )); 
        ?>
    </div>
</div>

<br/>