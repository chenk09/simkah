<?php
    $criteria = new CDbCriteria;
    $criteria->select = 'komponentarif_id, instalasi_id, tarif_tindakankomp,daftartindakan_tindakan,pendaftaran_id,ruangan_id,tgl_pendaftaran,pegawai_id';
    if($pendaftaran_id == null || $ruangan_id == null || $tgl_pendaftaran == null){
         $criteria->compare('pegawai_id',$pegawai_id);
    }else{
        $criteria->compare('pendaftaran_id',$pendaftaran_id);
        $criteria->compare('ruangan_id',$ruangan_id);
        $criteria->compare('tgl_pendaftaran',$tgl_pendaftaran);
    }
    $modTarif = LaporantindakankomponenV::model()->findAll($criteria);
    $totTarif = 0;
    
    foreach($modTarif as $key=>$tarif){
       if($tarif->daftartindakan_tindakan == true && $tarif->instalasi_id != PARAMS::INSTALASI_ID_IBS){ //instalasi_id != IBS
           $totTarif += $tarif->tarif_tindakankomp;
       }else{
       }
   }
   echo MyFunction::formatNumber($totTarif);
?>