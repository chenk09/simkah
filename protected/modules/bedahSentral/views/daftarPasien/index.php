<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
        $('#daftarPasien-grid').addClass('srbacLoading');
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
    <legend class="rim2">Informasi Pasien Bedah Sentral</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$modPasienMasukPenunjang->searchBS(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            'tglmasukpenunjang', 
            
            array(
            'header'=>'Instalasi / Ruangan Asal',
            'value'=>'$data->insatalasiRuanganAsal'
            ),
            
            'no_pendaftaran',
            'no_rekam_medik',
            
            array(
            'header'=>'Nama Pasien / Bin',
            'value'=>'$data->namaPasienNamaBin'
            ),
            array(
                'header'=>'Kasus Penyakit / <br> Kelas Pelayanan',
                'type'=>'raw',
                'value'=>'"$data->jeniskasuspenyakit_nama"."<br/>"."$data->kelaspelayanan_nama"',
            ),
//            'jeniskasuspenyakit_nama',    
            'umur',
            'alamat_pasien',
           
            array(
            'header'=>'Cara Bayar / Penjamin',
            'value'=>'$data->caraBayarPenjamin',
            ),
            
            'nama_pegawai',
            'kelaspelayanan_nama',
            
             array(
            'header'=>'Operasi',
            'type'=>'raw',
            'value'=>'CHtml::link("<i class=\"icon-pencil\"></i>","",array("onclick"=>"statusPeriksa($data->pendaftaran_id,$data->pasienmasukpenunjang_id);return false;","href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Rencana Operasi"))',
            'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
//            'value'=>'CHtml::link("<i class=icon-pencil></i>",Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/updateRencana",array("id"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik Untuk Mengubah Rencana Operasi"))',    
            ),
            array(
               'header'=>'Batal Periksa',
               'type'=>'raw',
               'value'=>'CHtml::link("<i class=\'icon-remove\'></i>", "javascript:batalPeriksa($data->pasienmasukpenunjang_id)",array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk membatalkan Pemeriksaan"))',
               'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php
 //CHtml::link($text, $url, $htmlOptions)
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'id'=>'daftarPasien-form',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <table class="table-condensed">
        <tr>
            <td>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
            </td>
            <td>
                <div class="control-group ">
                    <label for="namaPasien" class="control-label">
                        <?php echo CHtml::activecheckBox($modPasienMasukPenunjang, 'ceklis', array('uncheckValue'=>0,'rel'=>'tooltip' ,'data-original-title'=>'Cek untuk pencarian berdasarkan tanggal')); ?>
                        Tgl Masuk 
                    </label>
                    <div class="controls">
                        <?php   $format = new CustomFormat;
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPasienMasukPenunjang,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                        )); ?>
                      
                   </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' s/d', array('class'=>'control-label')) ?>

                   <div class="controls"> 
                            <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPasienMasukPenunjang,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                        )); ?>
                    </div>
                </div> 
                 </div>
                 
            </td>
            
            
        </tr>
    </table>
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
     ?>         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
			
	 		<?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
		
</fieldset>  
<?php $this->endWidget();?>
</fieldset>
<script type="text/javascript">
function batalPeriksa(idPenunjang){
   var con = confirm('Apakah anda yakin akan membatalkan pemeriksaan Operasi Bedah pasien ini? ');
   if(con){
       $.post('<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id.'/BatalPeriksa')?>',{idPenunjang:idPenunjang},
                 function(data){
                     if(data.status == 'not' && data.pesan == 'exist'){
                        $('#dialogKonfirm div.divForForm').html(data.keterangan);
                        $('#dialogKonfirm').dialog('open');

                        // setTimeout($('#dialogKonfirm').dialog('close'),1000);
                     }else{
                      
                        $('#dialogKonfirm div.divForForm').html(data.keterangan);
                        $('#dialogKonfirm').dialog('open');
                        $('#daftarPasien-grid').addClass('srbacLoading');
                        $.fn.yiiGridView.update('daftarPasien-grid', {
                                data: $(this).serialize()
                        });
                           // setTimeout($('#dialogKonfirm').dialog('close'),1000); 
                         
                     }
                 },'json'
             );
        
   }
}

function statusPeriksa(pendaftaran_id,pasienmasukpenunjang_id){
//    alert(status);
    var statusperiksa = '';
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = pasienmasukpenunjang_id;
    
    $.post("<?php echo Yii::app()->createUrl('bedahSentral/daftarPasien/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id},
        function(data){
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa menambahkan tindakan transaksi. ");
            }else{
                window.location.href= "<?php echo Yii::app()->controller->createUrl('daftarPasien/updateRencana',array()); ?>&id="+data.pasienmasukpenunjang_id,"";
            }
    },"json");
    return false; 
}
</script>



<?php 
// Dialog untuk masukkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogKonfirm',
    'options'=>array(
        'title'=>'',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>500,
        'minHeight'=>200,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end masukkamar_t dialog =============================
?>