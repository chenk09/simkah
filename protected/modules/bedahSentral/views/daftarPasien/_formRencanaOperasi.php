<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
//        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>2
    )
));
?>
<fieldset>
    <legend>
        Operasi
    </legend>
    <table class="table-condensed">
        <tr>
            <td width="50%">
                <?php echo $form->textFieldRow($modRencanaOperasiAttrib,'norencanaoperasi',array('readonly'=>true)) ?>
                <div class="control-group">
                    <?php
                        echo $form->labelEx($modRencanaOperasiAttrib,'tglrencanaoperasi',
                            array(
                                'class'=>'control-label'
                            )
                        );
                    ?>
                    <div class="controls">
                        <?php   
                            $this->widget('MyDateTimePicker',
                                array(
                                    'model'=>$modRencanaOperasiAttrib,
                                    'attribute'=>'tglrencanaoperasi',
                                    'mode'=>'datetime',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    ),
                                    'htmlOptions'=>array(
                                        'readonly'=>true,
                                        'class'=>'dtPicker3',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'style'=>'width:110px;'
                                    ),
                                )
                            );
                        ?>
                    </div>
                </div
                <?php
                    echo $form->dropDownListRow($modRencanaOperasiAttrib,'kamarruangan_id',
                        CHtml::listData($modPenunjang->getKamarKosongItems(Params::RUANGAN_ID_IBS), 'kamarruangan_id', 'KamarDanTempatTidur'),
                        array(
                            'empty'=>'-- Pilih --',
                            'onkeypress'=>"return $(this).focusNextInputField(event)"
                        )
                    );
                ?>
                <div class="control-group">
                    <?php echo $form->labelEx($modRencanaOperasiAttrib,'dokterpelaksana1_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php 
                            echo $form->dropDownList($modRencanaOperasiAttrib,'dokterpelaksana1_id',
                                CHtml::listData($modPenunjang->getDokterItems(Params::RUANGAN_ID_IBS), 'pegawai_id', 'nama_pegawai'),
                                array(
                                    'empty'=>'-- Pelaksana 1 --',
                                    'onkeypress'=>"return $(this).focusNextInputField(event)"
                                )
                            );
                        ?>
                        <?php echo $form->error($modRencanaOperasiAttrib, 'dokterpelaksana1_id'); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx($modRencanaOperasiAttrib,'dokterpelaksana2_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php 
                            echo $form->dropDownList($modRencanaOperasiAttrib,'dokterpelaksana2_id', 
                                CHtml::listData($modPenunjang->getDokterItems(Params::RUANGAN_ID_IBS), 'pegawai_id', 'nama_pegawai'),
                                array(
                                    'empty'=>'-- Pelaksana 2 --',
                                    'onkeypress'=>"return $(this).focusNextInputField(event)"
                                )
                            );
                            echo $form->error($modRencanaOperasiAttrib, 'dokterpelaksana2_id');
                        ?>
                    </div>
                </div>
                <?php
                    echo $form->dropDownListRow($modRencanaOperasiAttrib,'paramedis_id',
                        CHtml::listData($modPenunjang->getParamedisItems(Params::RUANGAN_ID_IBS), 'pegawai_id', 'nama_pegawai'),
                        array(
                            'empty'=>'-- Pilih --',
                            'onkeypress'=>"return $(this).focusNextInputField(event)"
                        )
                    );
                    echo $form->dropDownListRow($modRencanaOperasiAttrib,'bidan_id', 
                        CHtml::listData($modPenunjang->getParamedisItems(Params::RUANGAN_ID_IBS), 'pegawai_id', 'nama_pegawai'),
                        array(
                            'empty'=>'-- Pilih --',
                            'onkeypress'=>"return $(this).focusNextInputField(event)"
                        )
                    );
                    echo $form->dropDownListRow($modRencanaOperasiAttrib,'suster_id', 
                        CHtml::listData($modPenunjang->getParamedisItems(Params::RUANGAN_ID_IBS), 'pegawai_id', 'nama_pegawai'),
                        array(
                            'empty'=>'-- Pilih --',
                            'onkeypress'=>"return $(this).focusNextInputField(event)"
                        )
                    );
                    echo $form->textAreaRow($modRencanaOperasiAttrib,'keterangan_rencana',
                        array(
                            'class'=>'span3'
                        )
                    );
                ?>
            </td>
            <td width="50%">
                <?php
                echo $this->renderPartial('_formPasienAnastesi',
                    array(
                        'form'=>$form,
                        'modAnastesi'=>$modAnastesi,
                        'modRencanaOperasiAttrib'=>$modRencanaOperasiAttrib,
                        'modPenunjang'=>$modPenunjang
                    )
                );
                ?>
            </td>
        </tr>
    </table>

    <?php  
        $this->beginWidget('zii.widgets.jui.CJuiDialog',
            array(
                'id'=>'dialogOperasi',
                'options'=>array(
                    'title'=>'Pilih Operasi',
                    'autoOpen'=>false,
                    'width'=>660,
                    'height'=>500,
                    'modal'=>false,
                    'hide'=>'explode',
                    'resizelable'=>false,
                ),
            )
        );
    ?>
    <?php
        echo $this->renderPartial('_formOperasi',
            array(
                'modKegiatanOperasi'=>$modKegiatanOperasi,
                'modOperasi'=>$modOperasi
            )
        );
    ?>
    <?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>
    <br/>
    <fieldset>
        <legend>
               Detail Operasi
        </legend>
          <table id="tblFormRencanaOperasi" class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th>
                        <?php
                            echo CHtml::checkBox('kingCheck', true,
                                array(
                                    'onclick'=>'checkAll("ceklis",this);',
                                    'data-original-title'=>'Klik untuk cek semua operasi' ,
                                    'rel'=>'tooltip',
                                )
                            )
                        ?>
                    </th>
                    <th>Mulai Operasi</th>
                    <th>Selesai Operasi</th>
                    <!--<th>Golongan Operasi</th> GOLONGAN BESAR / KECIL DIBUAT TINDAKAN YG BERBEDA-->
                    <th>
                        <a href="#" onclick="openDialogItem(false);return false;" data-original-title="Klik untuk tambah Operasi" rel="tooltip">
                            Jenis Operasi
                            <i class="icon-zoom-in"></i>
                        </a>
                    </th>
                    <th>Tarif</th>
                    <th>Type Anastesi</th>
<!--                    <th>Cyto</th>
                    <th>Tarif Cyto</th>-->
                    <th>Status Operasi</th>
                    <th style="display: none;">Jenis Penyulit</th>
                    <th style="text-align:center">
                        <?php
                            echo CHtml::link("<i class='icon-plus'></i>", '#', array('onclick'=>"openDialogItem(true);return false;",'rel'=>'tooltip','title'=>'Klik untuk menambah operasi bersama')); 
                        ?>                        
                    </th>
                </tr>
            </thead>
                <?php 
                    $idx = -1;
                    foreach ($modRencanaOperasi as $rencanaOperasi) 
                    {
                        $idx++;
                        /* @var $modTindOperasi OperasiM  */
                        /* @var $modTindakanTarif TariftindakanM  */
                        
                        $modTindOperasi = BSOperasiM::model()->with('kegiatanoperasi')->findByPk(
                            $rencanaOperasi->operasi_id
                        ); 
                        $modTindakanTarif = TariftindakanM::model()->findByAttributes(
                            array(
                                'daftartindakan_id'=>$modTindOperasi->daftartindakan_id,
                                'kelaspelayanan_id'=>$modPasienPenunjang->kelaspelayanan_id,
                                'komponentarif_id'=> Params::KOMPONENTARIF_ID_TOTAL
                            )
                        );
                        $cekTindakan = TindakanpelayananT::model()->findByAttributes(
                            array(
                                'daftartindakan_id'=>$modTindOperasi->daftartindakan_id,
                                'pasienmasukpenunjang_id'=>$modPasienPenunjang->pasienmasukpenunjang_id
                            )
                        );
                        if($cekTindakan){
                            $tarif = (!empty($cekTindakan->tarif_tindakan)) ? $cekTindakan->tarif_tindakan : 0 ;
                            $tarifSatuan = (!empty($cekTindakan->tarif_satuan)) ? $cekTindakan->tarif_satuan : 0 ;
                            $persen_cyto = (!empty($cekTindakan->persencyto_tind)) ? $cekTindakan->persencyto_tind : 0 ;
                        }else{
                            $tarif = (!empty($modTindakanTarif->harga_tariftindakan)) ? $modTindakanTarif->harga_tariftindakan : 0 ;
                            $tarifSatuan = (!empty($modTindakanTarif->harga_tariftindakan)) ? $modTindakanTarif->harga_tariftindakan : 0 ;
                            $persen_cyto = (!empty($modTindakanTarif->persencyto_tind)) ? $modTindakanTarif->persencyto_tind : 0 ;
                        }
                        $idDaftarTindakan = $modTindOperasi->daftartindakan_id;
                        $idOperasi = $rencanaOperasi->operasi_id;
                        $kegiatanOperasi = $modTindOperasi->kegiatanoperasi->kegiatanoperasi_nama . ' - ' . $modTindOperasi->operasi_nama;
                        $daftarTindakanNama = $kegiatanOperasi;
                        
                        $tarif_cyto = 0;
                        if($modTindakanPelayanan)
                        {
                            $tarifcyto_tindakan = 0;

                            //UBAH METODE PENULISAN
//                    PERHITUNGAN TARIF CYTO DINONAKTIFKAN KARENA DIBUAT TINDAKAN YANG BERBEDA
//                            $criteria = new CDbCriteria();
//                            $criteria->addInCondition(komponentarif_id, Params::komponenTarif());
//                            $criteria->addCondition("daftartindakan_id = ".$modTindOperasi->daftartindakan_id);
//                            $criteria->addCondition("kelaspelayanan_id = ".$modPasienPenunjang->kelaspelayanan_id);
//                            $record = TariftindakanM::model()->findAll($criteria);
//                                
//                            foreach($record as $idxz=>$values)
//                            {
//                                /** penentuan tarif cyto  **/
//                                if($values['komponentarif_id'] == Params::komponenTarif('jasa_operator_bedah'))
//                                {
//                                    $cyto_tarif_operator = $values['harga_tariftindakan'] * ($values['persencyto_tind']/100);
//                                }
//
//                                if($values['komponentarif_id'] == Params::komponenTarif('jasa_asisten_operator'))
//                                {
//                                    $cyto_tarif_asisten = $values['harga_tariftindakan'] * ($values['persencyto_tind']/100);
//                                }
//                                /** end penentuan tarif cyto  **/
//                                $tarifcyto_tindakan = $cyto_tarif_operator + $cyto_tarif_asisten;
//                            }
                            $tarif_cyto = $tarifcyto_tindakan;
                        }
                ?>
            
                <tr id="operasi_<?php echo $idOperasi; ?>">
                    <td>
                        <?php 
                            echo CHtml::checkBox('BSTindakanPelayananT['.$idx.'][ceklis]',true,
                                array(
                                    'value'=>$idx,
                                    'id'=>'BSTindakanPelayananT_'.$idx.'_ceklis',
                                    'class'=>'ceklis'
                                )
                            );
                        ?>
                    </td>
                    <td>
                        <?php
                            $this->widget('MyDateTimePicker',
                                array(
                                    'name'=>'BSTindakanPelayananT['.$idx.'][mulaioperasi]',
                                    'value'=>$rencanaOperasi->mulaioperasi,
                                    'mode'=>'datetime',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    ),
                                    'htmlOptions'=>array(
                                        'readonly'=>true,
                                        'class'=>'dtPicker3',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'style'=>'width:110px;'
                                    ),
                                )
                            );
                        ?>
                    </td>
                    <td>
                        <?php
                            $this->widget('MyDateTimePicker',
                                array(
                                    'name'=>'BSTindakanPelayananT['.$idx.'][selesaioperasi]',
                                    'value'=>$rencanaOperasi->selesaioperasi,
                                    'mode'=>'datetime',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    ),
                                    'htmlOptions'=>array(
                                        'readonly'=>true,
                                        'class'=>'dtPicker3',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'style'=>'width:110px;'
                                    ),
                                )
                            ); 
                        ?>
                    </td>
                    <td style="display:none;">
                        <?php
                            echo CHtml::dropDownList('BSTindakanPelayananT['.$idx.'][golonganoperasi_id]', $rencanaOperasi->golonganoperasi_id,
                                CHtml::listData(GolonganoperasiM::model()->getAll(), 'golonganoperasi_id', 'golonganoperasi_nama'),
                                array(
                                    'class'=>'inputFormTabel lebar3',
                                    'empty'=>'- Pilih -'
                                )
                            );
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $kegiatanOperasi;                            
                            echo CHtml::hiddenField("BSTindakanPelayananT[".$idx."][daftartindakan_nama]",
                                $daftarTindakanNama,
                                array(
                                    'class'=>'inputFormTabel',
                                    'readonly'=>true
                                )
                            );
                            echo CHtml::hiddenField("BSTindakanPelayananT[".$idx."][daftartindakan_id]",
                                $idDaftarTindakan,
                                array(
                                    'class'=>'inputFormTabel',
                                    'readonly'=>true
                                )
                            );
                            echo CHtml::hiddenField("BSTindakanPelayananT[".$idx."][rencanaoperasi_id]",
                                $rencanaOperasi->rencanaoperasi_id,
                                array(
                                    'class'=>'inputFormTabel lebar3',
                                    'readonly'=>true
                                )
                            );
                            echo CHtml::hiddenField('BSTindakanPelayananT['.$idx.'][is_operasibersama]',
                                ($rencanaOperasi->is_operasibersama) ? 1:0,
                                array(
                                    'class'=>'inputFormTabel lebar3',
                                    'readonly'=>true
                                )
                            );
                            echo CHtml::hiddenField('BSTindakanPelayananT['.$idx.'][operasi_id]',
                                $idOperasi,
                                array(
                                    'class'=>'inputFormTabel lebar3',
                                    'readonly'=>true
                                )                                        
                            );
                            echo CHtml::hiddenField('BSTindakanPelayananT['.$idx.'][tindakanpelayanan_id]',
                                $rencanaOperasi->tindakanpelayanan_id,
                                array(
                                    'class'=>'inputFormTabel lebar3',
                                    'readonly'=>true
                                )                                        
                            );
                        ?>
                    </td>
                    <td>
                        <?php
                            echo CHtml::textField("BSTindakanPelayananT[".$idx."][tarif_tindakan]",
                                number_format($tarif,2),
                                array(
                                    'class'=>'inputFormTabel lebar3 currency',
                                    'readonly'=>true
                                )
                            );
                            echo CHtml::hiddenField("BSTindakanPelayananT[".$idx."][tarif_satuan]",
                                $tarifSatuan,
                                array(
                                    'class'=>'inputFormTabel lebar3',
                                    'readonly'=>true
                                )
                            );
//                    TARIF CYTO DINONAKTIFKAN KARENA DIBUAT TINDAKAN YANG BERBEDA
                            echo CHtml::hiddenField("BSTindakanPelayananT[".$idx."][tarifcyto_tindakan]", 
//                                $tarif_cyto,
                                0,
                                array(
                                    'class'=>'inputFormTabel lebar3 currency'
                                )
                            );
                            //SET EDITABLE EHJ-1168
//                            $komponenReadOnly = true;
//                            if($rencanaOperasi->is_operasibersama){
                                echo '<a rel="tooltip" data-original-title="Klik untuk mengubah Komponen Tarif" onclick="openKomponenTarif(this)" href="javascript:void(0);">'.
                                    '&nbsp;<i class="icon-pencil"></i></a>';
                                $komponenReadOnly = false;
//                            }else{
//                                echo '<a rel="tooltip" data-original-title="Klik untuk melihat Komponen Tarif" onclick="openKomponenTarif(this)" href="javascript:void(0);">'.
//                                    '&nbsp;<i class="icon-list"></i></a>';
//                            }
                                $cekKomponenTindakan = TariftindakanM::model()->findAllByAttributes(
                                        array(
                                            'daftartindakan_id'=>$modTindOperasi->daftartindakan_id,
                                            'kelaspelayanan_id'=>$modPasienPenunjang->kelaspelayanan_id,
                                        ),'komponentarif_id <> '.Params::KOMPONENTARIF_ID_TOTAL);
                                if(count($cekKomponenTindakan) > 0){
                                    echo "<div id='frmKomponenTarif' style='display:none;'>";
                                    $komponenTotal = 0;
                                    foreach($cekKomponenTindakan AS $x => $tarifkomp){
                                        $hargaTersimpan = 0;
                                        $tindakanKomponen = TindakankomponenT::model()->findByAttributes(array('tindakanpelayanan_id'=>$rencanaOperasi->tindakanpelayanan_id, 'komponentarif_id'=>$tarifkomp->komponentarif_id));
                                        $hargaTersimpan = $tindakanKomponen->tarif_tindakankomp;
                                        echo "<div class='control-group'>";
                                        echo "<label class='control-label'>".$tarifkomp->komponentarif->komponentarif_nama."</label>";
                                        echo "<div class='controls komponen'>";
                                        echo CHtml::hiddenField("BSTindakanPelayananT[".$idx."][tindakankomponen_id][".$x."]",
                                            $tindakanKomponen->tindakankomponen_id,
                                            array(
                                                'class'=>'inputFormTabel lebar3',
                                                'readonly'=>true,
                                                'onkeyup'=>'hitungTotalTarif(this);',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            )
                                        );
                                        echo CHtml::hiddenField("BSTindakanPelayananT[".$idx."][default]",
                                            number_format($tarifkomp->harga_tariftindakan,2),
                                            array(
                                                'class'=>'inputFormTabel lebar3 currency',
                                                'readonly'=>true,
                                                'onkeyup'=>'hitungTotalTarif(this);',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            )
                                        );
                                        echo CHtml::hiddenField("BSTindakanPelayananT[".$idx."][komponen_id][".$tarifkomp->komponentarif_id."]",
                                           $tarifkomp->komponentarif_id,
                                            array(
                                                'class'=>'inputFormTabel lebar3 currency komponen_id',
                                                'readonly'=>true,
                                                'onkeyup'=>'hitungTotalTarif(this);',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            )
                                        );
                                        echo CHtml::textField("BSTindakanPelayananT[".$idx."][komponentarif_id][".$tarifkomp->komponentarif_id."]",
                                            number_format($tindakanKomponen->tarif_tindakankomp,2),
                                            array(
                                                'class'=>'inputFormTabel lebar3 currency hasil',
                                                'readonly'=>$komponenReadOnly,
                                                'onkeyup'=>'hitungTotalTarif(this);',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            )
                                        );
                                        echo "</div></div>";
                                        $komponenTotal += $tarifkomp->harga_tariftindakan;
                                    }
                                    echo "</div>";
                                }
                        ?>
                    </td>
                    <td>
                        <?php 
                        echo CHtml::hiddenField('BSTindakanPelayananT['.$idx.'][typeanastesi_id_sebelum]', (isset($rencanaOperasi->pasienanastesi_id)) ? $rencanaOperasi->pasienanastesi->typeanastesi_id : null, array('readonly'=>true)); 
                        $rencanaOperasiDefault = (isset($rencanaOperasi->pasienanastesi_id)) ? $rencanaOperasi->pasienanastesi->typeanastesi_id : null;
                        echo CHtml::dropDownList('BSTindakanPelayananT['.$idx.'][typeanastesi_id]', null, //nilai di load dengan javascript
//                                TypeAnastesiM::getItems(), //DISET NULL KARENA HARUS PILIH JENIS & ANASTESI DULU
                                array(),
                                array('value'=>$rencanaOperasiDefault,'disabled'=>false,'class'=>'inputFormTabel labar3 typeanastesi','style'=>'width:150px;','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)","onchange"=>"hitungAnastesi(this);"),array('options' => array($rencanaOperasiDefault=>array('selected'=>true))));
                        ?>
                    </td>
                    <td style="display: none;">
                        <?php
                            echo CHtml::dropDownList('BSTindakanPelayananT['.$idx.'][cyto_tindakan]', 0,
                                array(
                                    1=>'Ya',
                                    0=>'Tidak'
                                ),
                                array(
                                    'class'=>'inputFormTabel lebar3',
                                    'onchange'=>'hitungCyto('.$idx.',this.value)'
                                )
                            );
                        ?>
                        <?php
                            echo CHtml::hiddenField("BSTindakanPelayananT[".$idx."][persencyto_tind]", 0,
                                array(
                                    'class'=>'inputFormTabel lebar3',
                                    'readonly'=>true
                                )
                            );
                        ?>
                    </td>
                    <td style="display: none;">
                        <?php
                            echo CHtml::textField("BSTindakanPelayananT[".$idx."][tarif_cyto]", 0,
                                array(
                                    'class'=>'inputFormTabel lebar3 currency'
                                )
                            );
                        ?>
                    </td>
                    <td>
                        <?php
                            echo CHtml::dropDownList('BSTindakanPelayananT['.$idx.'][statusoperasi]', $rencanaOperasi->statusoperasi,
                                StatusOperasi::items(),  
                                array(
                                    'class'=>'inputFormTabel lebar4',
                                    'empty'=>'-- Pilih --',
                                    'onkeypress'=>"return $(this).focusNextInputField(event)"
                                )
                            );
                        ?>
                    </td>
                    <td style="display: none;">
                        <?php
                            echo CHtml::dropDownList('BSTindakanPelayananT['.$idx.'][jenis_penyulit]',
                                $rencanaOperasi->jenis_penyulit,
                                JenisPenyulit::items(),
                                array(
                                    'class'=>'inputFormTabel lebar3',
                                    'empty'=>'-- Pilih --',
                                    'onkeypress'=>"return $(this).focusNextInputField(event)"
                                )
                            );
                        ?>
                    </td>
                    <td style="text-align:center;">
                        <?php
//                            echo CHtml::link("<i class='icon-minus'></i>", '#', array('onclick'=>'hapusRowTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk hapus item'));
                        ?>
                    </td>
                </tr>
                <?php  $idx++;} ?>
            </table>  
            <table>
                <tr>
                    <td width="60%">
                        <?php
                            $this->renderPartial('_formPemakaianBahan',
                                array(
                                    'modViewBmhp'=>$modViewBmhp
                                )
                            );
                        ?>
                        <br>
                        <?php
                        //FORM REKENING
                            $this->renderPartial('bedahSentral.views.daftarPasien.rekening._formRekening',
                                array(
                                    'form'=>$form,
                                    'modRekenings'=>$modRekenings,
                                )
                            );
                        ?>
                    </td>
                    <td>
                        <?php
                            $this->renderPartial('_formPaketBmhp',
                                array(
                                    'modViewBmhp'=>$modViewBahp,
                                )
                            );
                        ?>
                    </td>
                </tr>
            </table>        
    </fieldset>
</fieldset>

<script>
function hapusRowTindakan(obj)
{
    var daftartindakan_id = $(obj).parents('tr').find('input[name$="[daftartindakan_id]"]').val();
    removeRekeningTindakanBedahSentral(daftartindakan_id);
    $(obj).parents('tr').detach();
}

function openDialogItem(params)
{
    $('#dialogOperasi').dialog('open');
    $("#is_operasi").val(params);
    $("#is_operasibersama").val(1);
}

function hitungCyto(id, obj)
{
    
    if(obj == 1)
    {
        /*
        var persen_cytotind = $('#BSTindakanPelayananT_persencyto_tind_'+id+'').val(); 
        var harga_tarif = $('#BSTindakanPelayananT_tarif_tindakan_'+id+'').val(); 
        var tarif_cyto = harga_tarif * (persen_cytotind/100);
        $('#BSTindakanPelayananT_tarif_cyto_'+id+'').val(tarif_cyto);
        */
       var tarif_cyto = $('#BSTindakanPelayananT_'+id+'_tarifcyto_tindakan').val(); 
       $('#BSTindakanPelayananT_'+id+'_tarif_cyto').val(tarif_cyto);
    }
    else
    {
        $('#BSTindakanPelayananT_'+id+'_tarif_cyto').val(0);
    }
    
}

function anastesiOn(id)
{
    if(id != '')
    {
        $('#divAnastesi input').removeAttr('disabled');
        $('#divAnastesi select').removeAttr('disabled');
        $('#PasienanastesiT_dokteranastesi_id').val(id);
        if ($('#pakeAnastesi').is(':checked')){
            ''
        }else{
            ''  //$('#divAnastesi').slideToggle(500);
        }
        $('#pakeAnastesi').attr('checked',true);
        
        
    }
    if(id == '')
    {
        $('#pakeAnastesi').removeAttr('checked');
        $('#divAnastesi input').attr('disabled','true');
        $('#divAnastesi select').attr('disabled','true');
    }
    
    
}


function validasiItem()
{
    $('#tblFormRencanaOperasi > tbody').find('input[name$="[daftartindakan_id]"]').each(
        function()
        {
            
            var item = $(this).val();
            var index_item = $(this).val();
            item = 'key_' + item.toString();
            items[item] = 'yes';
            
            $("#dialogOperasi").find("input[tag="+ index_item +"]").attr('checked', 'true');
            $("#dialogOperasi").find("input[tag="+ index_item +"]").attr('disabled', 'true');
            
            var daftartindakan_id = $(this).parent().find("input[name$='[daftartindakan_id]']").val();
            var daftartindakan_nama = $(this).parent().find("input[name$='[daftartindakan_nama]']").val();
            tambahTindakanPemakaianBahan(daftartindakan_id, daftartindakan_nama);
        }
    );
}
validasiItem();

function tambahTindakanPemakaianBahan(value,label)
{
    $('#daftartindakanPemakaianBahan').append('<option value="'+value+'">'+label+'</option>');
}
//untuk akuntansi (jurnal rekening)
//load rekening tindakan yang sudah ada
function loadRekeningTindakan(){
    $("#tblFormRencanaOperasi tbody tr").each(function(){
        var daftartindakanId = $(this).find('input[name$="[daftartindakan_id]"]').val();
        var komponentarifId = $(this).find('input[name$="[komponen_id]"]').val();
        var harga = $(this).find('input[name$="[tarif_tindakan]"]').val();
        var tarifKomponen = $(this).find('input[name$="[komponentarif_id]"]').val();
        var qty = 1
        var kelaspelayananId = $("#BSMasukPenunjangV_kelaspelayanan_id").val();
        getDataRekeningBedahSentral(daftartindakanId,kelaspelayananId,1,'tm');
         setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya
            updateRekeningTindakanBedahSentral(daftartindakanId, komponentarifId, formatDesimal(tarifKomponen))
        },1500);
    });
}
loadRekeningTindakan();
function loadUpdateTindakan(){
    $("#frmKomponenTarif .komponen_id").each(function(){
        var daftartindakanId = $(this).parents("tr").find('input[name$="[daftartindakan_id]"]').val();
        var komponentarifId = $(this).parent().find('input[name*="[komponen_id]"]').val();
        var harga = $(this).parents("tr").find('input[name$="[tarif_tindakan]"]').val();
        var tarifKomponen = unformatNumber($(this).parent().find('input[name*="[komponentarif_id]"]').val());
        var qty = 1
        var kelaspelayananId = $("#BSMasukPenunjangV_kelaspelayanan_id").val();
         setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya
//             alert(tarifKomponen+' komponentarif_id = '+komponentarifId+" daftartindakan_id = "+daftartindakanId);
            updateRekeningTindakanBedahSentral(daftartindakanId, komponentarifId, formatDesimal(tarifKomponen))
        },1500);
    });
}
function openKomponenTarif(obj){
    $(obj).parents('tr').find("#frmKomponenTarif").slideToggle();
    return false;
}
function hitungTotalTarif(obj){
    var tarifKomponen = 0;
    $(obj).parents("tr").find("#frmKomponenTarif").find('.hasil').each(function(){
        tarifKomponen += unformatNumber($(this).val());
    });
    
    var idKomponen = $(obj).parent().find('input[name*="[komponen_id]"]').val();
    var daftartindakan_id = $(obj).parents("tr").find('input[name$="[daftartindakan_id]"]').val();
    var komponentarifId = idKomponen;
    var tarif_komponen = unformatNumber(obj.value);
    $(obj).parents('tr').find('input[name$="[tarif_tindakan]"]').val(formatDesimal(tarifKomponen));
    setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya
//        alert(tarif_komponen+' komponentarif_id = '+komponentarifId+" daftartindakan_id = "+daftartindakan_id);
        updateRekeningTindakanBedahSentral(daftartindakan_id, komponentarifId, formatDesimal(tarif_komponen))
        loadUpdateTindakan();
    },1500);
    
}
function hitungAnastesi(obj){
     var persenAnastesi = new Array();
     //convert php array to javascript array
    <?php $modTypeanastesi = TypeAnastesiM::model()->findAll("typeanastesi_aktif = TRUE"); 
    if(count($modTypeanastesi) > 0){
        foreach($modTypeanastesi AS $i=>$type){ ?>
            persenAnastesi[<?php echo $type->typeanastesi_id;?>] = <?php echo $type->persenanastesi;?>;
    <?php    }
    }
    ?>
    var typeAnastesiId = $(obj).val();
    var typeAnastesi = $(obj).find("option[value="+typeAnastesiId+"]").text();
    var komponenTarif = 0;
    $(obj).parents("tr").find('.komponen').each(function(){
        var hargaTarif = 0;
        var hargaDefault = unformatNumber($(this).find('input[name$="[default]"]').val());
        var jasaOperatorBedah = unformatNumber($(this).parents("tr").find('input[name$="[komponentarif_id][<?php echo Params::komponenTarif('jasa_operator_bedah');?>]"]').val());
        hargaTarif = hargaDefault;
        if(typeAnastesi == "<?php echo Params::TYPEANASTESI_STANDAR_TEXT?>"){ //==STANDAR
//            TIDAK ADA PERHITUNGAN KHUSUS
        }else{ //Perhitungan ASA
            if(persenAnastesi[typeAnastesiId] == 0){ //==LOKAL
                if($(this).find('input').is('[name$="[komponentarif_id][<?php echo Params::komponenTarif('jasa_dokter_anestesi'); ?>]"]')
                || $(this).find('input').is('[name$="[komponentarif_id][<?php echo Params::komponenTarif('jasa_asisten_anestesi'); ?>]"]')
                || $(this).find('input').is('[name$="[komponentarif_id][<?php echo Params::komponenTarif('remunerasi_dokter_anastesi'); ?>]"]')
                || $(this).find('input').is('[name$="[komponentarif_id][<?php echo Params::komponenTarif('remunerasi_paramedis_2'); ?>]"]')
                ){
                    hargaTarif = 0;
                }
            }else{ //==ASA I / ASA II / ASA III
                var jasaDokterAnastesi = (jasaOperatorBedah * (persenAnastesi[typeAnastesiId] / 100));
                if($(this).find('input').is('[name$="[komponentarif_id][<?php echo Params::komponenTarif('jasa_dokter_anestesi'); ?>]"]')){
                    hargaTarif = jasaDokterAnastesi;
                }else if($(this).find('input').is('[name$="[komponentarif_id][<?php echo Params::komponenTarif('remunerasi_dokter_anastesi'); ?>]"]')){
                    hargaTarif = (7.5 * (jasaDokterAnastesi / 92.5)); 
                }else{
                    hargaTarif = hargaDefault;
                }
            }
            if(typeAnastesiId == ""){
                hargaTarif = 0;
            }
            hitungTotalTarif(obj);
            var idKomponen = $(this).parent().find('input[name*="[komponen_id]"]').val();
            var daftartindakan_id = $(obj).parents("tr").find('input[name$="[daftartindakan_id]"]').val();
            var komponentarifId = idKomponen;
            var tarif_komponen = hargaTarif;
            setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya
//                alert(tarif_komponen+' komponentarif_id = '+komponentarifId+" daftartindakan_id = "+daftartindakan_id);
                updateRekeningTindakanBedahSentral(daftartindakan_id, komponentarifId, formatDesimal(tarif_komponen))
            },1500);
        }
            $(this).find('.hasil').val(formatDesimal(hargaTarif));
    });    
}
</script>
