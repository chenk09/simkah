<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'bsdetail-operasi-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'detailoperasi_id',array('class'=>'span5')); ?>

	<?php echo $form->dropDownlistRow($model,'operasi_id',  CHtml::listData(OperasiM::model()->getAllItems(), 'operasi_id', 'operasi_nama'),array('empty'=>'- Pilih -','class'=>'span3', 'style'=>'width:160px', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

	<?php echo $form->textFieldRow($model,'detailoperasi_nama',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'detailoperasi_namalainnya',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->checkBoxRow($model,'detailoperasi_aktif',array('checked'=>'checked')); ?>

	<div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
