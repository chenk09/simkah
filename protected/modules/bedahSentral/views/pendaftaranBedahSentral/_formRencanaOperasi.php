<fieldset>
    <legend class="rim">
        Rencana Operasi
    </legend>
        <div class="control-group ">
            <?php echo $form->labelEx($modRencanaOperasi,'tglrencanaoperasi', array('class'=>'control-label')) ?>
            <div class="controls">
                    <?php   
                            $this->widget('MyDateTimePicker',array(
                                            'model'=>$modRencanaOperasi,
                                            'attribute'=>'tglrencanaoperasi',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                    )); ?>
                    <?php echo $form->error($modRencanaOperasi, 'tglrencanaoperasi'); ?>
            </div>
        </div>
        
        <?php echo $form->textFieldRow($modRencanaOperasi,'norencanaoperasi',array('readonly'=>true)) ?>
        <?php echo $form->dropDownListRow($modRencanaOperasi,'kamarruangan_id', CHtml::listData($modPasienAdmisi->getKamarKosongItems($model->ruangan_id), 'kamarruangan_id', 'KamarDanTempatTidur') ,
                                                      array('empty'=>'-- Pilih --',
                                                            'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <div class="control-group">
            <?php echo CHtml::activeLabelEx($modRencanaOperasi,'dokterpelaksana1_id',array('class'=>"control-label")) ?>
            <div class="controls">
                <?php echo $form->dropDownList($modRencanaOperasi,'dokterpelaksana1_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pelaksana 1* --','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                <?php echo $form->dropDownList($modRencanaOperasi,'dokterpelaksana2_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pelaksana 2 --','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                <?php echo $form->error($modRencanaOperasi, 'dokterpelaksana1_id'); ?>
            </div>
        </div>
        
        
        <?php echo $form->dropDownListRow($modRencanaOperasi,'dokteranastesi_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->dropDownListRow($modRencanaOperasi,'perawat_id', CHtml::listData($model->getParamedisItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->dropDownListRow($modRencanaOperasi,'statusoperasi', StatusOperasi::items(),  
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>   
        <?php echo $form->textAreaRow($modRencanaOperasi,'keterangan_rencana',array('class'=>'span3')) ?>
        <?php  $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                                'id'=>'dialogOperasi',
                                'options'=>array(
                                    'title'=>'Pilih Operasi',
                                    'autoOpen'=>false,
                                    'width'=>450,
                                    'height'=>300,
                                    'modal'=>false,
                                    'hide'=>'explode',
                                    'resizelable'=>false,
                                ),
                            ));
                            ?>
        <?php echo $this->renderPartial('_formOperasi', array('modKegiatanOperasi'=>$modKegiatanOperasi,'modOperasi'=>$modOperasi)); ?>
        
        <?php $this->endWidget('zii.widgets.jui.CJuiDialog');?><br/>
        
        <table id="tblFormRencanaOperasi" class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th><a class="btn btn-primary" onclick="$('#dialogOperasi').dialog('open');return false;" href="#" data-original-title="Klik untuk pilih Operasi" rel="tooltip"><i class="icon-zoom-in icon-white"></i></a></th>
                    <th>Jenis Operasi/<br/>Operasi</th>
                    <th>Tarif</th>
                    <th>Qty</th>
                    <th>Satuan</th>
                    <th>Cyto</th>
                    <th>Tarif Cyto</th>
                </tr>
            </thead>
        </table>
        
       
</fieldset>
<script>
function inputOperasi(obj)
{
    if($(obj).is(':checked')) {
        var idOperasi = obj.value;
        var kelasPelayan_id = $('#BSPendaftaranMp_kelaspelayanan_id').val();
        
        if(kelasPelayan_id==''){
                $(obj).attr('checked', 'false');
                alert('Anda Belum Memilih Kelas Pelayanan');
                
            }
        else{
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormOperasi')?>',
                 'data':{idOperasi:idOperasi,kelasPelayan_id:kelasPelayan_id},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         $('#tblFormRencanaOperasi').append(data.form);
                 } ,
                 'cache':false});
        }     
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idOperasi)
{
    $('#tblFormRencanaOperasi #operasi_'+idOperasi).detach();
}

function getOperasi(item)
{
    $('#operasi').val(item[0]);    
    $('#BSRencanaOperasiT_operasi_id').val(item[1]);    
}

function hitungCyto(id,obj)
{
    if(obj == 1)
    {
        var persen_cytotind = $('#BSTindakanPelayananT_persencyto_tind_'+id+'').val(); 
        var harga_tarif = $('#BSTindakanPelayananT_tarif_tindakan_'+id+'').val(); 
        var tarif_cyto = harga_tarif * (persen_cytotind/100);

        $('#BSTindakanPelayananT_tarif_cyto_'+id+'').val(tarif_cyto);
    }
    else
    {
        $('#BSTindakanPelayananT_tarif_cyto_'+id+'').val(0);
    }
    
}
</script>
