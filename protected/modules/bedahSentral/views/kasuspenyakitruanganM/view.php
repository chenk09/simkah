<?php
$this->breadcrumbs=array(
	'Rjkasuspenyakitruangan Ms'=>array('index'),
	$model->ruangan_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Kasus Penyakit Ruangan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Kasus Penyakit Ruangan ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ruangan.ruangan_nama',
                                array(
                                    'label'=>'Jenis Kasus',
                                    'type'=>'raw',
                                    'value'=>$this->renderPartial($this->pathView.'_kasuspenyakit', array('ruangan_id'=>$model->ruangan_id,'jeniskasuspenyakit_id'=>$model->jeniskasuspenyakit_id), true),
                                ),
	),
)); ?>
<?php $this->widget('TipsMasterData',array('type'=>'view'));?>
