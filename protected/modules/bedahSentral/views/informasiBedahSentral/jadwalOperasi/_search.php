<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'reinformasipenjualanprodukpos-v-search',
        'type'=>'horizontal',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
           <div class="control-group">
                       <?php echo CHtml::label('Tgl Rencana Operasi','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                            ?>
                        </div>
                    </div>


            <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','style'=>'width:140px','maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'nama_bin',array('class'=>'span3','style'=>'width:140px','maxlength'=>200)); ?>
        </td>
        <td>
             <div class="control-group">
                       <?php echo CHtml::label('Sampai Dengan','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                            ?>
                        </div>
                    </div>


            <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','style'=>'width:140px','maxlength'=>200)); ?>
            <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','style'=>'width:140px','maxlength'=>200)); ?>
        </td>
    </tr>
</table>

<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
     <?php
         echo CHtml::link(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                Yii::app()->createUrl($this->module->id.'/'.InformasiBedahSentral.'/informasiJadwalOperasi'), 
                                array('class'=>'btn btn-danger'));
//                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
    ?>
    <?php  
        $content = $this->renderPartial('../tips/informasi',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
     ?>
</div>

<?php $this->endWidget(); ?>
