<legend class="rim2">
   Informasi Jadwal Operasi
</legend>
<?php
$this->breadcrumbs=array(
	'Reinformasipenjualanprodukpos Vs'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reinformasipenjualanprodukpos-v-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootGroupGridView',array(
	'id'=>'reinformasipenjualanprodukpos-v-grid',
	'dataProvider'=>$model->searchInformasi(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'NO',
                    'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                    
                ),
                array(
                    'header'=>'Tgl Rencana Operasi',
                    'type'=>'raw',
                    'value'=>'$data->tglrencanaoperasi'
                ),
                array(
                    'header'=>'Mulai Operasi /'."<br>".'Selesai Operasi',
                    'type'=>'raw',
                    'value'=>'$data->mulaioperasi." / "."<br>".$data->selesaioperasi',
                ),
               array(
                    'header'=>'Golongan Operasi',
                    'type'=>'raw',
                    'value'=>'$data->golonganoperasi->golonganoperasi_nama'
                ),
                array(
                    'header'=>'Jenis Operasi /'."<br>".'Operasi',
                    'type'=>'raw',
                    'value'=>'$data->operasi->kegiatanoperasi->kegiatanoperasi_nama." /"."<br>".$data->operasi->operasi_nama',
                ),
                array(
                    'header'=>'Status Operasi',
                    'type'=>'raw',
                    'value'=>'$data->statusoperasi',
                ),
                array(
                    'header'=>'Dokter Pelaksana I /'."<br>".'Dokter Pelaksana II',
                    'type'=>'raw',
                    'value'=>'$data->dokter1->nama_pegawai." /"."<br>".$data->dokter2->nama_pegawai',
                ),
                array(
                    'header'=>'Dokter Anastesi',
                    'type'=>'raw',
                    'value'=>'$data->dokteranastesi->nama_pegawai',
                ),
                array(
                    'header'=>'No Rekam Medik/'."<br>".'No Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->pasien->no_rekam_medik." /"."<br>".$data->pasien->no_pendaftaran',
                ),
                 array(
                    'header'=>'Nama Pasien '."<br>".'Bin - Binti',
                    'type'=>'raw',
                    'value'=>'$data->pasien->nama_pasien." bin"."<br>".$data->pasien->nama_bin',
                ),
                 array(
                    'header'=>'Umur /'."<br>".'Jenis Kelamin',
                    'type'=>'raw',
                    'value'=>'$data->pendaftaran->umur." /"."<br>".$data->pasien->jeniskelamin',
                ),
//             
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>


<?php //echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form">
<?php 
$this->renderPartial('jadwalOperasi/_search',array(
	'model'=>$model,
)); 
?>
