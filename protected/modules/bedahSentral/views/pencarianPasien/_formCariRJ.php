
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchRJ(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                       'name'=>'tgl_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->tgl_pendaftaran'
                    ),
                    array(
                        'header'=>'Instalasi / Poliklinik',
                        'value'=>'$data->insatalasiRuangan'
                    ),
                    array(
                       'name'=>'no_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->no_pendaftaran',
                    ),
                    array(
                       'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->no_rekam_medik',
                    ),
                    array(
                        'header'=>'Nama Pasien / Bin',
                        'value'=>'$data->namaPasienNamaBin'
                    ),
                    array(
                        'header'=>'Cara Bayar / Penjamin',
                        'value'=>'$data->caraBayarPenjamin',
                    ),
                    array(
                       'name'=>'nama_pegawai',
                        'type'=>'raw',
                        'value'=>'$data->nama_pegawai',
                    ),
                    array(
                        'header'=>'Kasus Penyakit / <br> Kelas Pelayanan ',
                        'type'=>'raw',
                        'value'=>'"$data->jeniskasuspenyakit_nama"."<br>"."$data->kelaspelayanan_nama"',
                    ),
                    array(
                       'name'=>'umur',
                        'type'=>'raw',
                        'value'=>'$data->umur',
                    ),
                    array(
                       'name'=>'alamat_pasien',
                        'type'=>'raw',
                        'value'=>'$data->alamat_pasien',
                    ),
                    array(
                       'header'=>'Rencana Operasi',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=\"icon-user\"></i>","",array("onclick"=>"statusPeriksa($data->pendaftaran_id);return false;","href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Membuat Rencana Operasi"))',
                        'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
//                        'value'=>'CHtml::link("<i class=\"icon-user\"></i>", "javascript:daftarPasienKunjungan(\'$data->pendaftaran_id\');", array("rel"=>"tooltip","title"=>"Klik untuk membuat rencana operasi"))', 'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr></hr>

<?php echo $this->renderPartial('_formKriteriaPencarian', array('model'=>$model,'form'=>$form),true);  ?>

