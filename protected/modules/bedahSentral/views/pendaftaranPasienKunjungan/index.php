<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-mp-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset>
        <legend>Pendaftaran Pasien Kunjungan</legend>
        
        
        
        <?php echo $form->errorSummary(array($modPasienPenunjang,$modRencanaOperasi,$modTindakanPelayanan,$modTindakanKomponen)); ?>
        
         <?php echo $this->renderPartial('_formDataPasien',array('form'=>$form,'model'=>$model,'modPasien'=>$modPasien)) ?>
        
        <table class='table-condensed'>
            <tr>
                 <td width="50%">
                    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
                    </br>
                    <?php echo $this->renderPartial('_formRencanaOperasi', 
                                array('model'=>$model,'form'=>$form,'modRencanaOperasi'=>$modRencanaOperasi,'modKegiatanOperasi'=>$modKegiatanOperasi,
                                      'modOperasi'=>$modOperasi,'modPasienAdmisi'=>$modPasienAdmisi,'modTindakanKomponen'=>$modTindakanKomponen,
                                      'modTindakanPelayanan'=>$modTindakanPelayanan)); ?>

                </td>
            <tr>
        </table>
    </fieldset>
    
    <?php echo CHtml::hiddenField('pendaftaran_id',$model->pendaftaran_id) ?>
    
    <div class='form-actions'>
        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                       Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                        array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
        <?php if(!$model->isNewRecord) echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false")); ?>
        <?php $content = $this->renderPartial('../tips/transaksi',array(),true);
                    $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
    </div>

<?php $this->endWidget(); ?>
<?php $url = CController::createUrl('ActionDynamic/GetKasusPenyakit'); ?>

<?php
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoli',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$jscript = <<< JS
function print(idPendaftaran)
{
        if(document.getElementById('isPasienLama').checked == true){
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=280');
        }else{
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');
            window.open('${urlPrintKartuPasien}'+idPendaftaran,'printwi','left=100,top=100,width=400,height=280');
        }
}

function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#BSPendaftaranMp_pegawai_id').html(data.listDokter);
    }, "json");
}
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
