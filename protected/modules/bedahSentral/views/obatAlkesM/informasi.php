<legend class="rim2">Informasi Obat Alkes</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('#rjinfostokobatalkesruangan-i-search').submit(function(){
	$.fn.yiiGridView.update('informasistokobatalkesruangan-i-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'informasistokobatalkesruangan-i-grid',
	'dataProvider'=>$model->searchInformasi(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Qty</center>',
                'start'=>8, //indeks kolom 3
                'end'=>10, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                    array(
                        'header'=>'Jenis Obat Alkes',
                        'value'=>'$data->jenisobatalkes_nama',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                        array(
                            'header'=>'Golongan',
                            'value'=>'$data->obatalkes_golongan',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Kode',
                            'value'=>'$data->obatalkes_kode',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Nama',
                            'value'=>'$data->obatalkes_nama',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Sumber Dana',
                            'value'=>'$data->sumberdana_nama',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Satuan',
                            'value'=>'$data->satuankecil_nama',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Harga',
                            'value'=>'$data->hargajual_oa',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Tanggal Kadaluarsa',
                            'value'=>'$data->tglkadaluarsa',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'In',
                            'value'=>'$data->qtystok_in',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Out',
                            'value'=>'$data->qtystok_out',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Current',
                            'value'=>'$data->qtystok_current',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Diskon',
                            'value'=>'$data->discount',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php $this->renderPartial('_searchinformasi',array('model'=>$model)); ?>