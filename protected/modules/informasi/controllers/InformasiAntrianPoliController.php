
<?php

class InformasiAntrianPoliController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new INInformasiAntrianPoliV;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['INInformasiAntrianPoliV']))
		{
			$model->attributes=$_POST['INInformasiAntrianPoliV'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->ruangan_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['INInformasiAntrianPoliV']))
		{
			$model->attributes=$_POST['INInformasiAntrianPoliV'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->ruangan_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $model = INInformasiAntrianPoliV::model()->findAll();
            $row = $this->renderKamarRuangan($model);
            if (isset($_GET['test'],$_GET['ruangan_id'],$_GET['nama_pegawai'],$_GET['limit'],$_GET['start'])){
                $date = date('Y-m-d');
                $poli = InfokunjunganrjV::model()->findAll('ruangan_id = '.$_GET['ruangan_id'].' and DATE(tgl_pendaftaran) = '."'".$date."'".' and  nama_pegawai ILIKE '."'".$_GET['nama_pegawai']."'".' order by no_urutantri limit '.$_GET['limit'].' offset '.$_GET['start']);
                $result .='<div id="ininformasiantrianpoli-v-form" class="grid-view" unik_id ="'.$v['ruangan_id'].'-'.$w.'">
                            <div class="summary">';
                
                $result .='<TABLE class="table table-bordered table-condensed">
                                    <THEAD><TR>
                                        <TH>No. Antrian</TH>
                                        <TH>Tgl Pendaftaran</TH>
                                        <TH>No. RM</TH>
                                        <TH>Nama Pasien</TH>
                                        <TH>Jenis Kelamin</TH>
                                        <TH>No. Pendaftaran</TH>
                                        <TH>Status Periksa</TH>
                                    </TR></THEAD><TBODY>';
                $no = 1;
                    if (count($poli) != 0){
                        foreach ($poli as $i=>$poliantri){
//                           $status = Yii::app()->db->createCommand()->select('(statusperiksa) as statusperiksa, (pasien_id) as pasien_id')->from('infokunjunganrj_v')->where("pasien_id = $poliantri->pasien_id AND statusperiksa = "."'".$poliantri->statusperiksa."'"."")->queryAll();
                           if(strtolower($poliantri->statusperiksa) == strtolower('SEDANG PERIKSA')){
                                $status = ' style="background-color:green;" id="green"';
                           }else if(strtolower($poliantri->statusperiksa) == strtolower('ANTRIAN')){
                                $status = 'style="background-color:yellow;" id="yellow"';
                           }else if (strtolower($poliantri->statusperiksa) == strtolower('SUDAH PERIKSA'))
                           {
                                $status = 'style="background-color:blue;" id="blue"';
                           }else{
                                $status = 'style="background-color:white;"';
                            }
                        $result .='<TR '.$status.'>
                                        <TD>'.$poliantri->no_urutantri.'</TD>
                                        <TD>'.$poliantri->tgl_pendaftaran.'</TD>
                                        <TD>'.$poliantri->no_rekam_medik.'</TD>
                                        <TD>'.$poliantri->nama_pasien.'</TD>
                                        <TD>'.$poliantri->jeniskelamin.'</TD>
                                        <TD>'.$poliantri->no_pendaftaran.'</TD>
                                        <TD>'.$poliantri->statusperiksa.'</TD>
                                    </TR>';
//                                        }
                    }
                }
                $result .=' </TBODY></TABLE>';
                $data['table']=$result;
                $data['unik_id'] = $_GET['ruangan_id'].'-'.$_GET['nama_pegawai'];
                $jumlahLimit = InfokunjunganrjV::model()->count('ruangan_id = '.$_GET['ruangan_id'].' and nama_pegawai ILIKE '."'".$_GET['nama_pegawai']."'");
                $url = Yii::app()->createUrl($this->route, array('ruangan_id'=>$_GET['ruangan_id'],'nama_pegawai'=>$_GET['nama_pegawai'], 'test'=>true));
                $data['pagination'] = $this->buatPagination($_GET['limit'], $url, $jumlahLimit, $_GET['start']);
                echo json_encode($data);
                Yii::app()->end();
                 $this->refresh();
            }
                
            $this->render('index',array(
                    'model'=>$model,
                    'row'=>$row,
            ));
	}
        
        protected function buatPagination($limit,$url,$jumlah,$start = null){
            
            $page = $jumlah/$limit;
            $page = floor($page);
            if (!isset($start) || $start < 1){
                $prev = 'disabled';
            }else{
                $prev = '';
                $onclick = 'pagination(this);return false';
            }
            $hasil = '<div class="pagination" style="margin-top:-6px;"><ul><li class="previous '.$prev.'" onclick='.$onclick.'><a href="" >← Sebelumnya</a></li>';
            
                
            for ($i=0;$i<=$page;$i++){
                    $hasil .='<li><a href="'.$url.'&start='.(($i)*$limit).'&limit='.$limit.'" onclick="pagination(this);return false">'.($i+1).'</a></li>';
            }
            
            if (!isset($start) || $start < 1){
                $next = '';
                $onclick = 'pagination(this);return false';
                $href = '"'.$url.'&start='.(($limit)).'&limit='.$limit.'"';
            }else{
                $next = 'disabled';
               
            }
            $hasil .= '<li class="next '.$next.'" onclick="'.$onclick.'"><a href='.$href.' >← Berikutnya</a></li></ul>';
            $hasil .= '</div>';
            
            return $hasil;
            $this->refresh();
        }
        
        protected function renderKamarRuangan($model){
            $result = '';
                $tempRuangan = '';
                $list1 = array();
                foreach ($model as $i=>$row){
                    if ($row->ruangan_id != $tempRuangan){
                        $list1[$row->ruangan_id]['name'] = $row->ruangan_nama;
                        $list1[$row->ruangan_id]['ruangan_id'] = $row->ruangan_id;                        
                        
//                        $list1[$row->ruangan_id]['dokter'][$row->instalasi_id]['dokter'][$row->ruangan_nama]['poli'][$i]['id'] = $row->ruangan_id;
//                        $list1[$row->ruangan_id]['dokter'][$row->instalasi_id]['dokter'][$row->ruangan_nama]['poli'][$i]['dokter_name'] = $row->nama_pegawai;
                        $tempRuangan = $row->ruangan_id;
                    }
//                    else{                      
//                        $list1[$tempRuangan][$row->ruangan_id]['name'] = $row->ruangan_nama;
//                        $list1[$tempRuangan][$row->ruangan_id]['ruangan_id'] = $row->ruangan_id;
//                        $list1[$tempRuangan][$row->ruangan_id]['dokter'][$row->instalasi_id]['dokter_name'] = $row->nama_pegawai;
//                        $list1[$tempRuangan]['dokter'][$row->ruangan_id]['dokter'][$row->ruangan_nama]['poli'][$i]['id'] = $row->ruangan_id;
//                        $list1[$tempRuangan]['dokter'][$row->ruangan_id]['dokter'][$row->ruangan_nama]['poli'][$i]['dokter_name'] = $row->nama_pegawai;
//                      
//                    }
                    $list1[$row->ruangan_id]['dokter'][$row->nama_pegawai] = $row->nama_pegawai;
                }
//                echo '<pre>';
//                echo print_r($list1);
//                exit();
                foreach ($list1 as $i=>$v){
                 $result .= '<div class="contentKamar">';
                        $ruangan = RuanganM::model()->findByPk($v['ruangan_id']);
                        $dataRuangan ='';
                        foreach ($v['dokter'] as $j=>$w){
                            $limit = 10;
                            $unique_id = ''.$v['ruangan_id'];
                              $date = date('Y-m-d');
                            $url = Yii::app()->createUrl($this->route, array('ruangan_id'=>$v['ruangan_id'],'nama_pegawai'=>$w, 'test'=>true));
                            $jumlahLimit = InfokunjunganrjV::model()->count('ruangan_id = '.$v['ruangan_id'].' and DATE(tgl_pendaftaran) = '."'".$date."'".' and nama_pegawai ILIKE '."'".$w."'");
                            if ($jumlahLimit <= $limit){
                               $limit = 'all';
                               $pagination ='';
                            }else{
                               $pagination = $this->buatPagination($limit,$url,$jumlahLimit,$start);
                            }
                             $date = date('Y-m-d');
                            $poli = InfokunjunganrjV::model()->findAll('ruangan_id = '.$v['ruangan_id'].' and DATE(tgl_pendaftaran) = '."'".$date."'".' and nama_pegawai ILIKE '."'".$w."'".' order by no_urutantri limit '.$limit);
//                            foreach($poli as $i=>$polis){
//                            $status = Yii::app()->db->createCommand()->select('(statusperiksa) as statusperiksa, (pasien_id) as pasien_id')->from('infokunjunganrj_v')->where("pasien_id = $polis->pasien_id")->queryAll();
//                             if($status->statusperiksa == 'SEDANG DIPERIKSA'){
//                                    $status = '<span style="color:black;" id="green">'.$status->statusperiksa.'</span>';
//                                }else if($status->statusperiksa = 'ANTRIAN'){
//                                      $status = '<span style="color:black;" id="yellow">'.$status->statusperiksa.'</span>';
//                                }else if ($status->statusperiksa = 'SUDAH DIPERIKSA')
//                                {
//                                     $status = '<span style="color:black; id="blue">'.$status->statusperiksa.'</span>';
//                                }else{
//                                    $status = '<span style="color:black;>'.$status->statusperiksa.'</span>';
//                                }
//                                                    return $status;
//                            }
//                            echo print_r($w);
//                            exit();
//                             $sql='SELECT * FROM infokujunganrj_v WHERE ruangan_id = '.$v['ruangan_id'].' and nama_pegawai ILIKE '."'".$w."'".'';
//                         $dataProvider=new CSqlDataProvider($sql,array(
//                           'keyField' => 'id',
//                           'totalItemCount'=>InfokunjunganrjV::model()->count(),
//                           'pagination'=>array(
//                               'pageSize'=>10,
//                            ),
//                         ));
//                         $page = new CPagination(InfokunjunganrjV::model()->count());
//                         $page->pageSize = 10;
//                         $this->widget('CLinkPager',array('pages'=>$page));
                            $result .='<h5 class="popover-title">'.$v['name'].' - '.$w.'<div class="pull-right">'.$pagination.'</div></h3>
                                <ul>';
                            $result .='</ul>';
//                            foreach ($w as $y){
//                                echo $y;
//                                exit();
//                             $count = InfokunjunganrjV::model()->count();  
                                $result .='
                                    <div id="ininformasiantrianpoli-v-form" class="grid-view" unik_id ="'.$v['ruangan_id'].'-'.$w.'" >
                                        <div class="summary">
                                        ';
                                        $result .='<TABLE class="table table-bordered table-condensed">
                                                            <THEAD><TR>
                                                                <TH>No. Antrian</TH>
                                                                <TH>Tgl Pendaftaran</TH>
                                                                <TH>No. RM</TH>
                                                                <TH>Nama Pasien</TH>
                                                                <TH>Jenis Kelamin</TH>
                                                                <TH>No. Pendaftaran</TH>
                                                                <TH>Status Periksa</TH>
                                                            </TR></THEAD><TBODY>';
//                                    foreach ($y['poli'] as $a=>$b){
//                                        $namapegawai = $b['dokter_name'];
//                                        $id = $v['ruangan_id'];
//                                        $date = date('Y-m-d');
//                                         and DATE(tgl_pendaftaran)='."'".$date."'".'
//                                        $jam = explode($poli->tgl_pendaftaran);
                                        
//                                         $poli = Yii::app()->db->createCommand()->select('(tgl_pendaftaran) AS tgl_pendaftaran, (tgl_pendaftaran::time) AS jam, (no_urutantri) AS no_urutantri, (no_rekam_medik) AS no_rekam_medik, (nama_pasien) AS nama_pasien, (jeniskelamin) AS jeniskelamin, (no_pendaftaran) AS no_pendaftaran, (statusperiksa) AS statusperiksa')->from('infokunjunganrj_v')->where("ruangan_id='$id' ")->queryAll();
//                                        echo $poli->jam;
//                                        echo $jam;
//                                        $tgl_pendaftaran = DATE('Y-m');
//                                        $jam = explode('-',$tgl_pendaftaran);
                                        $dataPasien = '';
                                            $no = 1;
                                        if (count($poli) != 0){
                                            foreach ($poli as $i=>$poliantri){
//                                                 $status = Yii::app()->db->createCommand()->select('(statusperiksa) as statusperiksa, (pasien_id) as pasien_id')->from('infokunjunganrj_v')->where("pasien_id = $poliantri->pasien_id AND statusperiksa = "."'".$poliantri->statusperiksa."'"."")->queryAll();
                                                       if(strtolower($poliantri->statusperiksa) == strtolower('SEDANG PERIKSA')){
                                                            $status = ' style="background-color:green;" id="green"';
                                                       }else if(strtolower($poliantri->statusperiksa) == strtolower('ANTRIAN')){
                                                            $status = 'style="background-color:yellow;" id="yellow"';
                                                       }else if (strtolower($poliantri->statusperiksa) == strtolower('SUDAH PERIKSA'))
                                                       {
                                                            $status = 'style="background-color:blue;" id="blue"';
                                                       }else{
                                                            $status = 'style="background-color:white;"';
                                                        }
//                                                        return $status;
//                                                 $status = $poliantri->statusperiksa;
//                                                 $id = $poliantri->pasien_id;
                                            $result .='
                                                    <TR '.$status.'>
                                                        <TD>'.$poliantri->no_urutantri.'</TD>
                                                        <TD>'.$poliantri->tgl_pendaftaran.'</TD>
                                                        <TD>'.$poliantri->no_rekam_medik.'</TD>
                                                        <TD>'.$poliantri->nama_pasien.'</TD>
                                                        <TD>'.$poliantri->jeniskelamin.'</TD>
                                                        <TD>'.$poliantri->no_pendaftaran.'</TD>
                                                        <TD>'.$poliantri->statusperiksa.'</TD>
                                                    </TR>';
//                                        }
                                        }
                                    }$result .=' </TBODY></TABLE>';
                                        $result .='</div>
                                    </div>';
//                            }
                        }
                    $result .='</div>';
                }
            
//            exit();
            return $result;
            $this->refresh();
        }
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new INInformasiAntrianPoliV('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['INInformasiAntrianPoliV']))
			$model->attributes=$_GET['INInformasiAntrianPoliV'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=INInformasiAntrianPoliV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ininformasiantrianpoli-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new INInformasiAntrianPoliV;
            $model->attributes=$_REQUEST['INInformasiAntrianPoliV'];
            $judulLaporan='Data Antrian Poli Klinik';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
