<?php
class InformasikamarinapVController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'daftarRuangan'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new INInformasikamarinapV;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['INInformasikamarinapV']))
		{
			$model->attributes=$_POST['INInformasikamarinapV'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->ruangan_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['INInformasikamarinapV']))
		{
			$model->attributes=$_POST['INInformasikamarinapV'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->ruangan_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
//		$model = INInformasikamarinapV::model()->findAll('kamarruangan_aktif = true order by ruangan_id, kelaspelayanan_id, kamarruangan_nokamar, kamarruangan_nobed');
            $model =INInformasikamarinapV::model()->findAll('kamarruangan_aktif = true order by ruangan_id, kelaspelayanan_id, kamarruangan_nokamar, kamarruangan_nobed');
            $row = $this->renderKamarRuangan($model);
            if ((isset($_POST['ajax']))&&(isset($_POST['ruangan']))&&(isset($_POST['statuskamar']))){
                $ruangan = $_POST['ruangan'];
                $statuskamar = $_POST['statuskamar'];
                if($statuskamar != ''){
                    $search = "and keterangan_kamar ILIKE '%".$statuskamar."%'";
                    $search2 = "keterangan_kamar ILIKE '%".$statuskamar."%'";
                    if($statuskamar =='KOSONG'){
                        $search = "and keterangan_kamar ILIKE '%".$statuskamar."%' or keterangan_kamar = ''";
                        $search2 = "keterangan_kamar ILIKE '%".$statuskamar."%' or keterangan_kamar = ''";
                    }
                }else{
                    $search = '';
                }
                
                if(empty($ruangan) && $statuskamar != ''){
                    $model =INInformasikamarinapV::model()->findAll("".$search2." and kamarruangan_aktif = true order by ruangan_id, kelaspelayanan_id, kamarruangan_nokamar, kamarruangan_nobed");
                }else{
                    $model =INInformasikamarinapV::model()->findAll(((!empty($ruangan)) ? "ruangan_id =".$ruangan." ".$search." and " : "").'kamarruangan_aktif = true order by ruangan_id, kelaspelayanan_id, kamarruangan_nokamar, kamarruangan_nobed');
                }
                
                $row = $this->renderKamarRuangan($model);
                
                echo json_encode($row);
                Yii::app()->end();
            }
                
            $this->render('index',array(
                    'model'=>$model,
                    'row'=>$row
            ));
	}
        
	protected function renderKamarRuangan($model){
            $result = '';
//            if (!is_array($model)){
//                $model = array($model);
//            }
                $tempRuangan = '';
                $list1 = array();
                
                foreach ($model as $i=>$row){
                    if ($row->ruangan_id != $tempRuangan){
                        $tempJumlah = 0;
                        $list1[$row->ruangan_id]['name'] = $row->ruangan_nama;
                        $list1[$row->ruangan_id]['ruangan_id'] = $row->ruangan_id;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['name'] = $row->kamarruangan_nokamar;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kelaspelayanan'] = $row->kelaspelayanan_namalainnya;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['jml'] = $row->kamarruangan_jmlbed;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['name'] = $row->kamarruangan_nokamar;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['no'] = $row->kamarruangan_nobed;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['status'] = $row->kamarruangan_status;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['keterangan'] = $row->keterangan_kamar;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['id'] = $row->kamarruangan_id;
                        $tempJumlah = $row->kamarruangan_jmlbed;
                        $tempRuangan = $row->ruangan_id;
                    }
                    else{
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['name'] = $row->kamarruangan_nokamar;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kelaspelayanan'] = $row->kelaspelayanan_namalainnya;
                        if ($row->kamarruangan_jmlbed >= $tempJumlah){
                            $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['jml'] = $row->kamarruangan_jmlbed;
                            $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['keterangan'] = $row->keterangan_kamar;
                            $tempJumlah = $row->kamarruangan_jmlbed;
                        }
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['name'] = $row->kamarruangan_nokamar;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['no'] = $row->kamarruangan_nobed;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['status'] = $row->kamarruangan_status;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['keterangan'] = $row->keterangan_kamar;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['id'] = $row->kamarruangan_id;
                    }
                }
//                echo '<pre>';
//                echo print_r($list1);
//                exit();
                foreach ($list1 as $i=>$v){
				
                 $result .= '<div class="contentKamar">';
				 			
						$ruangan = RuanganM::model()->findByPk($v['ruangan_id']);
                        $dataRuangan ='';
						
                        if (count($ruangan) == 1){
                            $dataRuangan .='<table width=\'100px\'>';
                            $dataRuangan .='<tr><td rowspan=2><img src=\''.Yii::app()->baseUrl.'/images/'.$ruangan->ruangan_image.'\' class=\'image_ruangan\'></td><td>Fasilitas</td><td>'.((!empty($ruangan->ruangan_fasilitas)) ? $ruangan->ruangan_fasilitas : " - ").'</td></tr>';
                            $dataRuangan .='<tr><td>Lokasi</td><td>'.((!empty($ruangan->ruangan_lokasi)) ? $ruangan->ruangan_lokasi : " - ").'</td></tr>';
                            $dataRuangan .='</table>';
                        }
                        foreach ($v['kamar'] as $j=>$w){
                            $result .='<div class="pintu"></div><h3 class="popover-title"><img src=\''. Yii::app()->baseUrl.'/images/blue-home-icon.png\' style=\'height:30px;\'/>'.$v['name'].' - '.$w['kelaspelayanan'].' - '.$w['jml'].'<a href="" class="pull-right poping" data-content="'.$dataRuangan.'" onclick="return false;"><img src=\''. Yii::app()->baseUrl.'/images/fasilitas.png\' style=\'height:30px;\'/>Detail</a></h3>
                                <ul>';
                            foreach ($w['kamar'] as $x=>$y){
                                $result .='<li class="bed">
                                    <div class="popover-inner">
                                        <h6 class="popover-title">'.$y['name'].'</h3>
                                        <div class="popover-content">';
                                    foreach ($y['bed'] as $a=>$b){                
//                                        echo $b['id']."<br>";
                                        $kamar = MasukkamarT::model()->find('kamarruangan_id = '.$b['id'].' order by tglmasukkamar desc');
//                                        echo "<pre>";
//                                        echo print_r($kamar->admisi->pasien->nama_pasien);
//                                        echo "</pre>";
                                      
                                        $dataPasien = '';
                                        if (count($kamar) == 1){
                                            $dataPasien .='<table>';
                                            $dataPasien .='<tr><td>No RM </td><td>: '.$kamar->admisi->pasien->no_rekam_medik.'</td></tr>';
                                            $dataPasien .='<tr><td>Nama </td><td>: '.$kamar->admisi->pasien->nama_pasien.'</td></tr>';
                                            $dataPasien .='<tr><td>Jenis Kelamin </td><td>: '.$kamar->admisi->pasien->jeniskelamin.'</td></tr>';
                                            $dataPasien .='<tr><td>Cara Bayar</td><td>: '.$kamar->admisi->pendaftaran->carabayar->carabayar_nama.'</td></tr>';
                                            $dataPasien .='<tr><td>Penjamin</td><td>: '.$kamar->admisi->pendaftaran->penjamin->penjamin_nama.'</td></tr>';
                                            $dataPasien .= '</table>';
//                                            $dataPasien .='<p><label class=\'control-label\'>Nama :</label> '.$kamar->admisi->pasien->nama_pasien.'</p>';
//                                            $dataPasien .='<p><label class=\'control-label\'>Jenis Kelamin :</label> '.$kamar->admisi->pasien->jeniskelamin.'</p>';
                                        }
                                        if($b['keterangan'] == 'KOSONG' || $b['keterangan'] == '' && $b['status'] == true){
                                            $status = 'RanjangRumahSakit2';
                                            $class = 'btn-danger';
                                            $dataStatus = 'Pasien Kosong';
                                        }else if($b['keterangan'] == 'ISI' || $b['keterangan'] == 'IN USE'){
                                            $status = 'RanjangRumahSakit';
                                            $class = 'btn-primary';
                                            $dataStatus = $dataPasien;
                                            if(empty($dataPasien)){
                                                $status = 'RanjangRumahSakit2';
                                                $class = 'btn-danger';
                                                $dataStatus = 'Pasien Kosong';
                                            }else if(empty($dataStatus) && $b['keterangan'] == 'BOOKING'){
                                                $status = 'RanjangRumahSakit';
                                                $class = 'btn-primary-blue';
                                                $dataStatus = 'BOOKING';
                                            }else if($b['status'] == false && $b['keterangan'] == 'BOOKING'){
                                                $status = 'RanjangRumahSakit';
                                                $class = 'btn-primary-blue';
                                                $dataStatus = 'BOOKING';
                                            }
                                        }else if($b['keterangan'] == 'CLEANING'){
                                            $status = 'RanjangRumahSakit2';
                                            $class = 'btn-primary-blue';
                                            $dataStatus = 'CLEANING';
                                        }else if($b['keterangan'] == 'BOOKING'){
                                            $status = 'RanjangRumahSakit';
                                            $class = 'btn-primary-blue';
                                            $dataStatus = 'BOOKING';
                                        }else if($b['keterangan'] == 'STERILISASI'){
                                            $status = 'RanjangRumahSakit3';
                                            $class = 'btn-danger';
                                            $dataStatus = 'STERILISASI';
                                        }else if($b['keterangan'] == 'RUSAK'){
                                            $status = 'RanjangRumahSakit3';
                                            $class = 'btn-primary-blue';
                                            $dataStatus = 'RUSAK';
                                        }else if($b['keterangan'] == 'OPEN' || $b['keterangan'] == 'open' ){
                                            $status = 'RanjangRumahSakit2';
                                            $class = 'btn-danger';
                                            $dataStatus = 'OPEN';
                                        }else if($b['status'] != true && (!empty($dataPasien))){
                                            $status = 'RanjangRumahSakit';
                                            $class = 'btn-primary';
                                            $dataStatus = $dataPasien;
                                        }
                                                
                                        $result .='<p><a href="" class="btn '.$class.'" rel="popover" data-content="'.$dataStatus.'" onclick="return false"><img src=\''. Yii::app()->baseUrl.'/images/'.$status.'.png\'/ width=30px; height=30px>No Bed : '.$b['no'].'</a></p>';
                                    }
                                    for($d=1;$d<=($w['jml'] - (count($y['bed'])));$d++){
//                                        echo $d;
//                                        $result .='<p><a href="" class="btn btn-danger" onclick="return false"><img src=\''. Yii::app()->baseUrl.'/images/RanjangRumahSakit2.png\'/>Kosong</a></p>';
//                                        $result .='<p><a href="" class="btn '.$class.'" rel="popover" data-content="'.$dataStatus.'" onclick="return false"><img src=\''. Yii::app()->baseUrl.'/images/'.$status.'.png\'/ width=30px; height=30px>No Bed : '.$b['no'].'</a></p>';
                                    }
                                        $result .='</div>
                                    </div>
                                </li>';
                            }
                            $result .='</ul>';
                        }
                       
                    $result .='</div>';
                }
            
//            exit();
            return $result;
        }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new INInformasikamarinapV('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['INInformasikamarinapV']))
			$model->attributes=$_GET['INInformasikamarinapV'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=INInformasikamarinapV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ininformasikamarinap-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
	public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
	public function actionPrint()
	{
		$model= new INInformasikamarinapV;
		$model->attributes=$_REQUEST['INInformasikamarinapV'];
		$judulLaporan='Data INInformasikamarinapV';
		$caraPrint=$_REQUEST['caraPrint'];
		if($caraPrint=='PRINT') {
			$this->layout='//layouts/printWindows';
			$this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
		}
		else if($caraPrint=='EXCEL') {
			$this->layout='//layouts/printExcel';
			$this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
		}
		else if($_REQUEST['caraPrint']=='PDF') {
			$ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
			$posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
			$mpdf = new MyPDF('',$ukuranKertasPDF); 
			$mpdf->useOddEven = 2;  
			$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
			$mpdf->WriteHTML($stylesheet,1);  
			$mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
			$mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
			$mpdf->Output();
		}
	}
	
	public function actionDaftarRuangan()
	{
		$model = new INInformasikamarinapV;
		if(isset($_GET["INInformasikamarinapV"]))
		{
			$model->attributes = $_GET["INInformasikamarinapV"];
			if(strlen($_GET["INInformasikamarinapV"]["no_rekam_medik"]) > 0)
			{
				$criteria = new CDbCriteria;
				$criteria->compare("no_rekam_medik", trim($_GET["INInformasikamarinapV"]["no_rekam_medik"]));
				$criteria->with = array("pasien");
				$pasien = PasienadmisiT::model()->find($criteria);
				if($pasien)
				{
					$model->kamarruangan_id = $pasien->kamarruangan_id;
				}else $model->kamarruangan_id = '-1';
			}
			
			if(strlen($_GET["INInformasikamarinapV"]["nama_pasien"]) > 0)
			{
				$model->tipe_pencarian = $_GET["INInformasikamarinapV"]["tipe_pencarian"];
				$model->nama_pasien = $_GET["INInformasikamarinapV"]["nama_pasien"];
			}
		}
		$this->render('daftarRuangan',array(
			"model"=>$model
		));
	}
	
}
