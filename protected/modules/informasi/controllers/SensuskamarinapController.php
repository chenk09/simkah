<?php

class SensuskamarinapController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $defaultAction = 'index';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('grid', 'datasensus'),
                'users' => array('*'),
            ),
        );
    }

    public function actionGrid() {
        $this->layout = '//layouts/extjs';
        $this->render('sensus_frame');
    }

    public function actionDatasensus() {
        $list = array();
        $list['datakamar'] = array();
        $data = Yii::app()->db->createCommand()
                ->select('ruangan_m.ruangan_id,
                        ruangan_m.instalasi_id,
                        ruangan_m.ruangan_nama,
                        ruangan_m.ruangan_namalainnya,
                        ruangan_m.ruangan_jenispelayanan,
                        ruangan_m.ruangan_lokasi,
                        ruangan_m.ruangan_singkatan,
                        ruangan_m.ruangan_fasilitas,
                        ruangan_m.ruangan_image,
                        kelaspelayanan_m.kelaspelayanan_id,
                        kelaspelayanan_m.kelaspelayanan_nama,
                        kelaspelayanan_m.kelaspelayanan_namalainnya,
                        kelaspelayanan_m.kelaspelayanan_aktif,
                        kamarruangan_m.kamarruangan_nokamar,
                        kamarruangan_m.kamarruangan_jmlbed,
                        kamarruangan_m.kamarruangan_nobed,
                        kamarruangan_m.kamarruangan_status,
                        kamarruangan_m.kamarruangan_aktif,
                        kamarruangan_m.kamarruangan_image,
                        kamarruangan_m.kamarruangan_id,
                        kamarruangan_m.keterangan_kamar
                        ')
                ->from('ruangan_m')
                ->join('kamarruangan_m', 'ruangan_m.ruangan_id = kamarruangan_m.ruangan_id')
                ->join('kelaspelayanan_m', 'kamarruangan_m.kelaspelayanan_id = kelaspelayanan_m.kelaspelayanan_id')
                ->where('kamarruangan_m.kamarruangan_aktif = true AND ruangan_m.instalasi_id <> 7')
                ->order('ruangan_m.ruangan_nama, kelaspelayanan_m.kelaspelayanan_nama, kamarruangan_m.kamarruangan_nokamar, kamarruangan_m.kamarruangan_nobed')
                ->queryAll();
        $kelaspelayanan = '';
        $newnokamar = '';
        $nobed = '';
        $listkelaspelayanan = array(
            'peri' => array(21, 25),
            'pratama_c' => array(17),
            'pratama_b' => array(16),
            'pratama_a' => array(15),
            'madya_b' => array(14),
            'madya_1' => array(13),
            'utama' => array(18),
            'vip_b' => array(10),
            'vip_a' => array(9),
            'vvip' => array(8),
            'icvcu' => array(6),
            'gkicu' => array(31),
            'gkutama' => array(36),
            'gkvip' => array(37),
            'gkvvip' => array(34),
            'supervvip' => array(35),
        );
        foreach ($data as $key => $value) {
            $pasien = '';
            $pasien_rm = '';
            $carabayar_nama = '';
            $shk = "";
            $dhk = "";
            $p3 = '';
            $pasienbooking = '';
            $pasienbooking_rm = '';
            $dataKelas = array();
            foreach ($listkelaspelayanan as $kelas => $valueKelas) {
                if (in_array($value['kelaspelayanan_id'], $valueKelas)) {
                    $dataKelas[$kelas] = 0;
                    if ($value['keterangan_kamar'] == 'IN USE' || $value['keterangan_kamar'] == "RENCANA PULANG")
                        $dataKelas[$kelas] = 1;
                    break;
                }
            }
            if ($value['keterangan_kamar'] == 'IN USE' || $value['keterangan_kamar'] == "RENCANA PULANG") {
                $kamar = MasukkamarT::model()->with()->find('kamarruangan_id = ' . $value['kamarruangan_id'] . ' order by tglmasukkamar desc');
                $kelurahan = ($kamar->admisi->pasien->kelurahan->kelurahan_nama) ? " - " . $kamar->admisi->pasien->kelurahan->kelurahan_nama : "";
                $className = "in-use";
                if ($value['keterangan_kamar'] == "RENCANA PULANG"){
                    $className = 'in-rencana-pulang';
                }
                $pasien = '<div class="label '.$className.'">'.$kamar->admisi->pasien->nama_pasien . '<br>' . $kamar->admisi->pasien->kecamatan->kecamatan_nama . $kelurahan.'</div>';
                $pasien_rm = $kamar->admisi->pasien->no_rekam_medik;
                $penjamin = $kamar->penjamin;
                $carabayar = $kamar->carabayar;
                $carabayar_nama = $carabayar->carabayar_nama;
                if ($carabayar_nama == 'BPJS') {
                    if ($penjamin->penjamin_id == '399') {
                        $shk = 1;
                    } else {
                        $dhk = 1;
                    }
                }
                if (in_array($carabayar->carabayar_id, array(2, 9))) {
                    $p3 = 1;
                }
            }
            if ($value['keterangan_kamar'] == 'BOOKING' || $value['keterangan_kamar'] == 'IN USE' || $value['keterangan_kamar'] == "RENCANA PULANG") {
                $listBookingKamar = BookingkamarT::model()->findAll('kamarruangan_id = ' . $value['kamarruangan_id'] . ' and pasienadmisi_id is null order by tglbookingkamar asc');
                if ($listBookingKamar){
                    $first = true;
                    foreach ($listBookingKamar as $bookingKamar){
                        $bookingalamat = $bookingKamar->alamat ? ' - ' . $bookingKamar->alamat : '';
                        $pasienbooking .= '<div class="label in-booking" '.(!$first ? 'style="margin-top:5px;"' : '').'>'.$bookingKamar->nama . '<br/>' . $bookingKamar->no_telp . $bookingalamat.'</div>';
                        $first = false;
                    }
                }
            }
            $newkelaspelayanan = '';
            if ($kelaspelayanan != $value['ruangan_id'] . $value['kelaspelayanan_id']) {
                $kelaspelayanan = $value['ruangan_id'] . $value['kelaspelayanan_id'];
                $newkelaspelayanan = $value['kelaspelayanan_nama'];
            }
            $newnokamar = '';
            if ($nokamar != $value['kamarruangan_nokamar']) {
                $nokamar = $value['kamarruangan_nokamar'];
                $newnokamar = $value['kamarruangan_nokamar'];
            }
            $newnobed = '';
            if ($nobed != $value['kamarruangan_nokamar'] . $value['kamarruangan_nobed']) {
                $nobed = $value['kamarruangan_nokamar'] . $value['kamarruangan_nobed'];
                $newnobed = $value['kamarruangan_nobed'];
                if ($newnobed == 1) {
                    if (isset($data[$key + 1]['kamarruangan_nobed']) && is_numeric($data[$key + 1]['kamarruangan_nobed']) && $data[$key + 1]['kamarruangan_nobed'] > 1) {
                        
                    } else {
                        $newnobed = '';
                    }
                }
            }

            $list['datakamar'][$key] = array(
                'ruangan_nama' => $value['ruangan_nama']. ($value['ruangan_lokasi'] ? " - " : "").$value['ruangan_lokasi'].($value['ruangan_jenispelayanan'] ? " - " : "").$value['ruangan_jenispelayanan'],
                'kelaspelayanan_nama' => $newkelaspelayanan,
                'kamarruangan_nokamar' => $newnokamar,
                'kamarruangan_nobed' => $newnobed,
                'pasien_nama' => $pasien,
                'booking' => '',
                'diagnosa' => '',
                'booking' => $pasienbooking,
                'no_rm' => $pasien_rm,
                'carabayar_nama' => $carabayar_nama,
                'shk' => $shk,
                'dhk' => $dhk,
                'p3' => $p3,
            );
            foreach ($dataKelas as $keyKelas => $valueKelas) {
                $list['datakamar'][$key][$keyKelas] = $valueKelas;
            }
        }
        echo json_encode($list);
    }

}
