<legend class="rim2">Informasi Antrian Poli Klinik</legend> 
<?php 
//echo CHtml::dropDownList('ruangan', '', CHtml::listData(RuanganM::model()->findAll('instalasi_id = '.Params::INSTALASI_ID_RJ), 'ruangan_nama', 'ruangan_nama'), array('empty'=>'-- Pilih --', 'onchange'=>'getListRuangan();')); 
?>
<style>
    .contentKamar, .bed{
        -moz-box-shadow: 0px 5px 10px rgba(0,0,0,.6);
        -webkit-box-shadow: 0px 5px 10px rgba(0,0,0,.6);
        -o-box-shadow: 0px 5px 10px rgba(0,0,0,.6);
        -moz-border-radius:3px;
        -webkit-border-radius:3px;
        -o-border-radius:3px;
    }
    .contentKamar{
        border:1px solid black;
        padding:10px;
        margin:10px;
    }
    .bed{
        display:inline-block;
        width:13%;
        border-color:#ccc;
        margin:10px;
    }
    .popover-inner{
        width:100%;        
    }
    .image_ruangan{
        height:100px;
        width:100px;
    }
</style>

<!--<div style='max-height:600px;overflow-y: scroll;'>-->
<div class="isi">
<?php echo $row; 
$this->widget('CLinkPager',array('pages'=>$page));
        
?>
</div>
<!--</div>-->
<?php 
$url = Yii::app()->createUrl($this->route);
Yii::app()->clientScript->registerScript('list', '
    function getListRuangan(){
        ruangan = $("#ruangan").val();
        $.post("'.$url.'", {ajax:true,ruangan:ruangan},function(data){
            $(".isi").html(data);
            jQuery(\'a[rel="popover"]\').popover();
            jQuery(\'.poping\').popover({placement:"bottom"});
        },"json");
    }
    
    function pagination(obj){
      url = $(obj).attr("href");
      
        $.get(url,{},function(hasilpage){
          $(document).find(\'[unik_id="\'+hasilpage.unik_id+\'"]\').html(hasilpage.table);
          $(obj).parents(".pull-right").html(hasilpage.pagination);
        },"json");
    }
',  CClientScript::POS_HEAD); ?>
