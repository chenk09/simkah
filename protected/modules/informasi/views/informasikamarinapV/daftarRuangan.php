<legend class="rim2">Informasi Kamar</legend>
<?php

Yii::app()->clientScript->registerScript('search', "
$('#search-laporan').submit(function(){
	$.fn.yiiGridView.update('laporan-grid', {
		data: $(\"#search-laporan\").serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootMenu', array(
    'type' => 'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked' => false, // whether this is a stacked menu
    'items' => array(
        array(
			'label' => 'Lokasi Ruangan',
			'url' =>array('index'),
			'active' =>false
		),
		array(
			'label' => 'Daftar Ruangan',
			'url' =>"#",
			'active' =>true
		),
        		array(
			'label' => 'Sensus',
			'url' =>array('sensuskamarinap/grid'),
			'active' =>false
		),
    ),
));
?>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'search-laporan',
	'type'=>'horizontal',
)); ?>
<table>
	<tr>
		<td>
			<?php echo $form->dropDownListRow($model, 'ruangan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true AND instalasi_id = 4 ORDER BY ruangan_nama'), 'ruangan_id', 'ruangan_nama'),array(
				'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --'
			));?>
			<?php echo $form->dropDownListRow($model, 'kelaspelayanan_id', CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_nama'),array(
				'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --'
			));?>
			<?php echo $form->dropDownListRow($model, 'keterangan_kamar', Statuskamar::items(),array(
				'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --'
			));?>
		</td>
		<td>
			<?php echo $form->textFieldRow($model, 'no_rekam_medik');?>
			<div class="control-group ">
				<label for="INInformasikamarinapV_nama_pasien" class="control-label">Nama Pasien</label>
				<div class="controls">
					<?php echo $form->dropDownList($model, 'tipe_pencarian',array(
							"semua"=>"Semua",
							"depan"=>"Kata Depan",
							"belakang"=>"Kata Belakang"
						),array(
							'class'=>'span2',
							'onkeypress'=>"return $(this).focusNextInputField(event)",
						)
					);?>
					<?php echo $form->textField($model, 'nama_pasien', array());?>
				</div>
			</div>
		</td>
	</tr>
</table>
<div class="form-actions">
	<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
		array('class'=>'btn btn-primary', 'type'=>'submit')
	); ?>
	<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
		array('class'=>'btn btn-danger', 'type'=>'reset')
	); ?>
</div>

<?php $this->endWidget(); ?>
<style>
    .label.in-rencana-pulang{
        background-color:#FFD324;
    }
</style>
<?php 
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'laporan-grid',
	'dataProvider'=>$model->searchRuangan(),
	'template'=>"{pager}{summary}\n{items}",
	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		"ruangan_nama",
		"kelaspelayanan_nama",
		"kamarruangan_nokamar",
		"kamarruangan_nobed",
		array(
			"name"=>"keterangan_kamar",
			"type"=>"raw",
			'value'=>array($model, 'ambilStatusKamar')
		),
		array(
			"header"=>"Data Pasien",
			"type"=>"raw",
			'value'=>array($model, 'ambilDataPasien'),
		)
	),
	'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
?>