<legend class="rim2">Informasi Tarif</legend> 
<?php
    Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#search').submit(function(){
	$.fn.yiiGridView.update('ininformasiTarif-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ininformasiTarif-grid',
	'dataProvider'=>$modTarifTindakanRuanganV->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                    'header'=>'Instalasi / Ruangan ',
                    'type'=>'raw',
                    'value'=>'$data->InstalasiRuangan',
                ),
                'kelompoktindakan_nama',
                'kategoritindakan_nama',
                'daftartindakan_nama',
                'kelaspelayanan_nama',
              array(
                        'name'=>'tarifTotal',
                        'value'=>'$this->grid->getOwner()->renderPartial(\'_tarifTotal\',array(\'kelaspelayanan_id\'=>$data[kelaspelayanan_id],\'daftartindakan_id\'=>$data[daftartindakan_id]),true)',
                ),
            'persencyto_tind',
            'persendiskon_tind',
            array(
                        'name'=>'Komponen Tarif',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/detailsTarif",array("idKelasPelayanan"=>$data->kelaspelayanan_id,"idDaftarTindakan"=>$data->daftartindakan_id, "idKategoriTindakan"=>$data->kategoritindakan_id)) ,array("title"=>"Klik Untuk Melihat Detail Tarif","target"=>"iframe", "onclick"=>"$(\"#dialogDetailsTarif\").dialog(\"open\");", "rel"=>"tooltip"))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<legend class="rim">Pencarian</legend> 
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search',
        'type'=>'horizontal',
)); ?>
    <?php echo $form->hiddenField($modTarifTindakanRuanganV,'instalasi_id', array('readonly'=>true, 'class'=>'span1', 'id'=>'instalasi')); ?>
    <?php 
        echo $form->dropDownListRow($modTarifTindakanRuanganV, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll(), 'instalasi_id', 'instalasi_nama'), array('empty'=>'-- Pilih --', 'id'=>'instalasi_id')); 
    ?>
    <?php echo $form->dropDownListRow($modTarifTindakanRuanganV,'kategoritindakan_id',
                                        CHtml::listData($modTarifTindakanRuanganV->getTariftindakanperdaruanganItems(),
                                                        'kategoritindakan_id', 'kategoritindakan_nama'),
                                                            array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --')); ?>
    <?php echo $form->dropDownListRow($modTarifTindakanRuanganV,'kelaspelayanan_id',
                                        CHtml::listData($modTarifTindakanRuanganV->getTariftindakanperdaruangankelas(), 
                                                        'kelaspelayanan_id', 'kelaspelayanan_nama'),
                                                            array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --')); ?>
    <?php echo $form->textFieldRow($modTarifTindakanRuanganV,
                                    'daftartindakan_id',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
    <div class="form-actions">
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                                                array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
         <?php 
            $content = $this->renderPartial('../tips/informasi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
         ?>
        <?php
            echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp";                 
        ?>
    </div>
<?php $this->endWidget(); ?>

<?php
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/PrintTarif');
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint+"&d"+$('#search').serialize(),"",'location=_new, width=900px');
}

JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD); 
?>
<?php
// ===========================Dialog Details Tarif=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogDetailsTarif',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Komponen Tarif',
                        'autoOpen'=>false,
                        'width'=>350,
                        'height'=>350,
                        'resizable'=>false,
                        'scroll'=>false    
                         ),
                    ));
?>
<iframe src="" name="iframe" width="100%" height="100%">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details Tarif================================
?>
   
<?php 
//    $url =Yii::app()->createUrl($this->route);
//    $urlshift = $this->createUrl('GantiInstalasi');
//    $js = <<< JS
//   
//    function ajaxGetInstalasi(){
//    instalasi = $("#instalasi_id").val();
//    $("#instalasi").val(instalasi);
//        $.fn.yiiGridView.update('ininformasiTarif-grid', {
//		data: $("#search").serialize()
//	});
////        instalasi_id = $('#instalasi_id').val();
////        $.post('${urlshift}', {instalasi_id:instalasi_id},function(data){
////            $('#instalasi').val(data.instalasi_id);
////             $('#ininformasiTarif-grid tbody').html(data);
////        },'json');
//    }
//    
//JS;
//    Yii::app()->clientScript->registerScript('onheadDialog', $js, CClientScript::POS_HEAD);
    ?>

<?php 
//$url = $this->createUrl('GantiInstalasi');
//Yii::app()->clientScript->registerScript('list', '
//    function ajaxGetInstalasi(){
//        instalasi_id = $("#instalasi_id").val();
//        $.post("'.$url.'", {ajax:true,instalasi_id:instalasi_id},function(data){
//            $(".grid-view").html(data.isi);
//            jQuery(\'a[rel="popover"]\').popover();
//            jQuery(\'.poping\').popover({placement:"bottom"});
//        },"json");
//    }
//',  CClientScript::POS_HEAD); 
?>