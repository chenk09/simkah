<?php
class INInformasikamarinapV  extends InformasikamarinapV
{
	public $nama_pasien, $no_rekam_medik, $tipe_pencarian;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	
	public function primaryKey() {
		return 'ruangan_id';
	}
	
	public function searchRuangan()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('ruangan_id', $this->ruangan_id);
		$criteria->compare('kelaspelayanan_id', $this->kelaspelayanan_id);
		$criteria->compare('kamarruangan_id', $this->kamarruangan_id);
		$criteria->compare('LOWER(keterangan_kamar)', strtolower($this->keterangan_kamar));
		$criteria->compare('kamarruangan_aktif',true);
		$criteria->order = "ruangan_id, kelaspelayanan_id, kamarruangan_nokamar, kamarruangan_nobed";
		
		if(strlen($this->nama_pasien) > 0)
		{
			$criteria_dua = new CDbCriteria;
			if($this->tipe_pencarian == "depan")
			{
				$criteria_dua->condition = "LOWER(nama_pasien) LIKE LOWER(:nama_pasien)";
				$criteria_dua->params = array(
					":nama_pasien"=>$this->nama_pasien . " %"
				);
				
			}else if($this->tipe_pencarian == "belakang")
			{
				$criteria_dua->condition = "LOWER(nama_pasien) LIKE LOWER(:nama_pasien)";
				$criteria_dua->params = array(
					":nama_pasien"=>"% " . $this->nama_pasien
				);
			}else
			{
				$criteria_dua->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien), true);
			}
			
			$criteria_dua->addCondition('tglpulang IS NULL');
			$criteria_dua->with = array("pasien");
			$pasiens = PasienadmisiT::model()->findAll($criteria_dua);
			if($pasiens && count($pasiens) > 0)
			{
				$kamarruangan_id = array();
				foreach($pasiens as $key=>$val)
				{
					$kamarruangan_id[] = $val->kamarruangan_id;
				}
				$criteria->addInCondition('kamarruangan_id', $kamarruangan_id);
			}else $criteria->compare('kamarruangan_id', '-1');
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));		
	}
	
	public function ambilStatusKamar($data, $row)
	{
		$label = "label label-warning";
		if($data->keterangan_kamar == 'OPEN')
		{
			$label = "label label-success";
		}
		if($data->keterangan_kamar == 'BOOKING')
		{
			$label = "label label-info";
		}
		if($data->keterangan_kamar == 'RENCANA PULANG')
		{
			$label = "label in-rencana-pulang";
		}
		return '<span class="'. $label .'">'. $data->keterangan_kamar .'</span>';
	}
	
	public function ambilDataPasien($data, $row)
	{
		$pasien = "-";
		if ($data->keterangan_kamar == 'IN USE' || $data->keterangan_kamar == "RENCANA PULANG") {
                    $kamar = MasukkamarT::model()->find('kamarruangan_id = ' . $data->kamarruangan_id . ' order by tglmasukkamar desc');
                    $pasien = $kamar->admisi->pasien->no_rekam_medik . " - " . $kamar->admisi->pasien->nama_pasien;
                }
                if ($data->keterangan_kamar == 'BOOKING'){
                    $kamar = BookingkamarT::model()->find('kamarruangan_id = ' . $data->kamarruangan_id . ' order by tglbookingkamar asc');
                    $pasien = $kamar->nama . ' - '.$kamar->no_telp;
                }
		return $pasien;
	}
}