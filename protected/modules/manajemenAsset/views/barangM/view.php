<?php
$this->breadcrumbs=array(
	'Sabarang Ms'=>array('index'),
	$model->barang_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Barang', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' SABarangM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' SABarangM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' SABarangM', 'icon'=>'pencil','url'=>array('update','id'=>$model->barang_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' SABarangM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->barang_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Barang', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'barang_id',
		'bidang_id',
		'barang_type',
		'barang_kode',
		'barang_nama',
		'barang_namalainnya',
		'barang_merk',
		'barang_noseri',
		'barang_ukuran',
		'barang_bahan',
		'barang_thnbeli',
		'barang_warna',
		'barang_statusregister',
		'barang_ekonomis_thn',
		'barang_satuan',
		'barang_jmldlmkemasan',
		'barang_harganetto',
        'barang_image',
		'barang_aktif',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>