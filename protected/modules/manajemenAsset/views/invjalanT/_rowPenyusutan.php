<?php
	$namaModel = "PenyusutanperalatanT";
    $tgl_guna		= $invperalatan_tglguna[2];
    if(empty($ceksusut)){
    	$bulan_guna		= $invperalatan_tglguna[1];
    }else{
    	$bulan_guna		= $invperalatan_tglguna[1] + 1;
    	if($bulan_guna<10){
			$bulan_guna = "0".$bulan_guna;
		}
    }
    $tahun_guna		= $invperalatan_tglguna[0];

    $nilairesidu 	= empty($model->invperalatan_nilairesidu) ? 0 : $model->invperalatan_nilairesidu;
    $penyusutan 	= $model->invperalatan_harga;
    $umurekonomis 	= $model->umurekonomis;
	$bbnpenyusutanbrjlnper = number_format((($penyusutan - $nilairesidu) / $umurekonomis),0);
	
	if(empty($ceksusut)){
		$akumpenyusutanper = MyFunction::number_unformat($bbnpenyusutanbrjlnper);
		$penambahakum 	= $akumpenyusutanper;
		$nilaibukuperalatan = $model->invperalatan_harga - $penambahakum;
	}else{
		$akumpenyusutanper = MyFunction::number_unformat($bbnpenyusutanbrjlnper)+$model->invperalatan_akumsusut;
		$penambahakum 	= MyFunction::number_unformat($bbnpenyusutanbrjlnper);
		$nilaibukuperalatan = $model->invperalatan_harga - $akumpenyusutanper;
	}

	for ($i=0; $i < $jml_penyusutan; $i++) { 
		$bulanpenyusutanperalatan = $tahun_guna."-".$bulan_guna."-".$tgl_guna;
		$urutan = $i + 1;
		echo('<tr>');
			echo "<td>".
                CHtml::textField($namaModel."[$i][umurekonmisbln]", $urutan,array('readonly'=>true, 'class'=>'span1')).
            "</td>";
            echo "<td>".
                CHtml::textField($namaModel."[$i][bulanpenyusutanperalatan]", $bulanpenyusutanperalatan,array('readonly'=>true, 'class'=>'span3')).
            "</td>";
            echo "<td>".
                CHtml::textField($namaModel."[$i][bbnpenyusutanbrjlnper]", $bbnpenyusutanbrjlnper,array('readonly'=>true, 'class'=>'span3')).
            "</td>";
            echo "<td>".
                CHtml::textField($namaModel."[$i][akumpenyusutanper]", number_format($akumpenyusutanper,0),array('readonly'=>true, 'class'=>'span3')).
            "</td>";
            echo "<td>".
                CHtml::textField($namaModel."[$i][nilaibukuperalatan]", number_format($nilaibukuperalatan,0),array('readonly'=>true, 'class'=>'span3')).
            "</td>";
		echo('</tr>');

		$akumpenyusutanper 		+= $penambahakum;
		$nilaibukuperalatan 	-= $penambahakum;
		$bulan_guna = intval($bulan_guna) + 1;
		if($bulan_guna>12){
			$bulan_guna = $bulan_guna%12;
			$tahun_guna = intval($tahun_guna) + 1;
			if($bulan_guna<10){
				$bulan_guna = "0".$bulan_guna;
			}
		}elseif($bulan_guna<10){
			$bulan_guna = "0".$bulan_guna;
		}
	}

?>