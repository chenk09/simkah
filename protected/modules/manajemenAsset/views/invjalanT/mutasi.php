<?php
$this->breadcrumbs=array(
	'Guinvperalatan Ts'=>array('mutasi'),
	'Create',
);
$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('_mutasiPeralatan', array('model'=>$model,'modlama'=>$modlama ));

$arrMenu = array();
array_push($arrMenu,array('label'=>Yii::t('mds','Mutasi').' Mutasi Inventarisasi Kendaraan', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master')));

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formMutasi', array('model'=>$model,'modlama'=>$modlama)); ?>
