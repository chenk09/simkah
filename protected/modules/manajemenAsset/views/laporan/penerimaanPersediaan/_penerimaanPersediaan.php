<?php
$table = 'ext.bootstrap.widgets.BootGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchPenerimaanPersediaanPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchPenerimaanPersediaan();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
    'mergeColumns'=>array('nopenerimaan'),
	'columns'=>array(
                     array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                        'type'=>'raw',
                    ),
                   'nopenerimaan',
                    array(
                        'header'=>'Tanggal Terima',
                        'type'=>'raw',
                        'value'=>'$data->tglterima',
                    ),
                    array(
                        'header'=>'Nama Supplier',
                        'type'=>'raw',
                        'value'=>'$data->pembelianbarang->supplier->supplier_nama',
                    ),
                   array(
                        'header'=>'No Pembelian',
                        'type'=>'raw',
                        'value'=>'$data->pembelianbarang->nopembelian',
                        'footer'=>'<b><right>Total:</right></b>',
                         'htmlOptions'=>array(
                            'style'=>'text-align:right;padding-right:10%;'
                        ),
                    ),
                     array(
                        'header'=>'Total Harga',
                        'type'=>'raw',
                        'value'=>'number_format($data->totalharga,0,"",",")',
                        'footer'=>number_format($model->getTotalharga()),
                    ),
                    array(
                        'header'=>'Pegawai Penerima',
                        'type'=>'raw',
                        'value'=>'$data->penerima->nama_pegawai',
                    ),
                   array(
                        'header'=>'Pegawai Mengetahui',
                        'type'=>'raw',
                        'value'=>'$data->mengetahui->nama_pegawai',
                    ),
                 array(
                        'header'=>'Ruangan',
                        'type'=>'raw',
                        'value'=>'$data->ruangan->ruangan_nama',
                    ),
	),
)); ?>