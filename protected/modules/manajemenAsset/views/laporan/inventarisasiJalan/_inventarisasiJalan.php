<?php
$table = 'ext.bootstrap.widgets.BootGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchPrintKendaraan();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchInformasiKendaraan();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
	'columns'=>array(
                    array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                        'type'=>'raw',
                    ),
                   array(
                       'header'=>'Inventaris Kode',
                       'value'=>'$data->inventaris_kode',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'No Register',
                       'value'=>'$data->no_register',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Tanggal Register',
                       'value'=>'$data->tgl_register',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Nama Golongan',
                       'value'=>'$data->golongan_nama',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Nama Kelompok',
                       'value'=>'$data->kelompok_nama',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Nama Subkelompok',
                       'value'=>'$data->subkelompok_nama',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Nama Bidang',
                       'value'=>'$data->bidang_nama',
                       'type'=>'raw',
                   ), 
                   array(
                       'header'=>'Nama Barang',
                       'value'=>'$data->barang_nama',
                       'type'=>'raw',
                   ),

	),
)); ?>