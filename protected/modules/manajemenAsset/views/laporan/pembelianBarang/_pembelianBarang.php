<?php
$table = 'ext.bootstrap.widgets.BootGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchPembelianBarangPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchPembelianBarang();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
     'mergeColumns'=>array('nopembelian'),
	'columns'=>array(
                    array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                        'type'=>'raw',
                    ),
                   'nopembelian',
                   array(
                       'header'=>'Sumber Dana',
                       'value'=>'$data->sumberdana->sumberdana_nama',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Nama Supplier',
                       'value'=>'$data->supplier->supplier_nama',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Tanggal Pembelian',
                       'value'=>'$data->tglpembelian',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Tanggal Dikirim',
                       'value'=>'$data->tgldikirim',
                       'type'=>'raw',
                       'footer'=>'<b><right>Total:</right></b>',
                         'htmlOptions'=>array(
                            'style'=>'text-align:right;padding-right:10%;'
                        ),
                   ),
                   array(
                       'header'=>'Total Pembelian',
                       'value'=>'number_format($data->hargabeli * $data->jmlbeli,0,"",",")',
                       'type'=>'raw',
                       'footer'=>number_format($model->getTotalharga()),
                   ),
                   array(
                       'header'=>'Pegawai Mengetahui',
                       'value'=>'$data->mengetahui->nama_pegawai',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Pegawai Menyetujui',
                       'value'=>'$data->menyetujui->nama_pegawai',
                       'type'=>'raw',
                   ),
                   array(
                       'header'=>'Pegawai Pemesan',
                       'value'=>'$data->pemesan->nama_pegawai',
                       'type'=>'raw',
                   ),
            
	),
)); ?>