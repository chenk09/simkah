<?php
$table = 'ext.bootstrap.widgets.BootGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  	$data = $model->searchGroupByPrint();
  	$template = "{items}";
  	if ($caraPrint=='EXCEL') {
    	$table = 'ext.bootstrap.widgets.BootExcelGridView';
  	}
  	$this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
    'itemsCssClass'=>'table table-bordered table-striped table-condensed',
    'template'=>$template,
	'columns'=>array(
        array(
            'header'=>'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            'type'=>'raw',
        ),
        array(
           'header'=>'Tipe Barang',
           'value'=>'$data->barang_type',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Nama Barang',
           'value'=>'$data->barang_nama',
           'type'=>'raw',
       	),
       	array(
           'header'=>'No. Register',
           'value'=>'$data->inventarisasi_noregister',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Tgl. Penerimaan',
           'value'=>'$data->tglterimagedung',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Nama Pemilik',
           'value'=>'$data->pemilikbarang_nama',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Harga Perolehan',
           'value'=>'number_format($data->inventarisasi_harga)',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Beban Penyusutan',
           'value'=>'number_format($data->bebanpenyusutanberjalan)',
           'type'=>'raw',
       	),
	),
	));
} else{
  	$data = $model->searchGroupBy();
  	$this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
    'itemsCssClass'=>'table table-bordered table-striped table-condensed',
    'template'=>$template,
	'columns'=>array(
        array(
            'header'=>'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            'type'=>'raw',
        ),
        array(
           'header'=>'Tipe Barang',
           'value'=>'$data->barang_type',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Nama Barang',
           'value'=>'$data->barang_nama',
           'type'=>'raw',
       	),
       	array(
           'header'=>'No. Register',
           'value'=>'$data->inventarisasi_noregister',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Tgl. Penerimaan',
           'value'=>'$data->tglterimagedung',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Nama Pemilik',
           'value'=>'$data->pemilikbarang_nama',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Harga Perolehan',
           'value'=>'number_format($data->inventarisasi_harga)',
           'type'=>'raw',
       	),
       	array(
           'header'=>'Beban Penyusutan',
           'value'=>'number_format($data->bebanpenyusutanberjalan)',
           'type'=>'raw',
       	),
       	array(
           	'header'=>'Rincian Penyusutan',
           	'type'=>'raw',
           	'value'=>'CHtml::link("<i class=icon-list-alt></i>", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/rincianPenyusutan",array("inventarisasi_noregister"=>"$data->inventarisasi_noregister","kodeInventaris"=>"$data->kodeinventaris")), 
           	array("class"=>"", 
                  "target"=>"iframePenghapusanAset",
                  "onclick"=>"$(\"#dialogPenghapusanAset\").dialog(\"open\");",
                  "rel"=>"tooltip",
                  "title"=>"Klik Rincian Penyusutan Aset",
            ))', 
           	'htmlOptions'=>array(
                'style'=>'text-align: center',
           	)
        ),
	),
	)); 
}
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPenghapusanAset',
    'options'=>array(
        'title'=>'Detail Penyusutan Aset',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>200,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframePenghapusanAset" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>