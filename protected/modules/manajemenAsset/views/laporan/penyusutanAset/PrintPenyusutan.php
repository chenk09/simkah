<?php
	if($kdinventaris=='02'){
		$namabarang = $model->invgedung_namabrg;
		$noInventaris = $model->invgedung_noregister;
		$harga = $model->invgedung_harga;
	}else{
		$namabarang = $model->invperalatan_namabrg;
		$noInventaris = $model->invperalatan_noregister;
		$harga = $model->invperalatan_harga;
	}
?>

<fieldset>
    <table>
    	<tr>
    		<th>Nama Barang</th>
    		<th width="250px">: <?php echo $namabarang; ?></th>
    		<th>No. Inventarisasi</th>
    		<th>: <?php echo $noInventaris; ?></th>
    	</tr>
    	<tr>
    		<th>Harga Perolehan</th>
    		<th>: <?php echo number_format($harga); ?></th>
    	</tr>
    </table>
    <table class="table table-condensed">    	
    	<thead>
			<th>No.</th>
			<th>Tanggal Penyusutan</th>
			<th>Beban Penyusutan</th>
			<th>Akumulasi Penyusutan</th>
			<th>Nilai Buku Aktiva</th>
    	</thead>
	<?php
		$no = 1;
		foreach ($rincian as $key => $value) {
			echo"<tr>";
				echo"<td>".$no."</td>";
				echo"<td>".$value['tgl_penyusutan']."</td>";
				echo"<td>".number_format($value['bebanpenyusutanberjalan'])."</td>";
				echo"<td>".number_format($value['akumulasipenyusutan'])."</td>";
				echo"<td>".number_format($value['nilaibukuaktiva'])."</td>";
			echo"</tr>";		
			$no++;
		}	
	?>
    </table>
</fieldset>