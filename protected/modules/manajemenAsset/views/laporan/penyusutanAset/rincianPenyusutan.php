<?php
	if($kdinventaris=='02'){
		$namabarang = $model->invgedung_namabrg;
		$noInventaris = $model->invgedung_noregister;
		$harga = $model->invgedung_harga;
	}else{
		$namabarang = $model->invperalatan_namabrg;
		$noInventaris = $model->invperalatan_noregister;
		$harga = $model->invperalatan_harga;
	}
?>

<fieldset>
    <legend class="rim">Rincian Penyusutan Aset</legend>
    <table>
    	<tr>
    		<td>Nama Barang</td>
    		<td>: <?php echo $namabarang; ?></td>
    		<td>No. Inventarisasi</td>
    		<td>: <?php echo $noInventaris; ?></td>
    	</tr>
    	<tr>
    		<td>Harga Perolehan</td>
    		<td>: <?php echo $harga; ?></td>
    	</tr>
    </table>
    <table class="table table-condensed">    	
    	<thead>
			<th>No.</th>
			<th>Tanggal Penyusutan</th>
			<th>Beban Penyusutan</th>
			<th>Akumulasi Penyusutan</th>
			<th>Nilai Buku Aktiva</th>
    	</thead>
	<?php
		$no = 1;
		foreach ($rincian as $key => $value) {
			echo"<tr>";
				echo"<td>".$no."</td>";
				echo"<td>".$value['tgl_penyusutan']."</td>";
				echo"<td>".number_format($value['bebanpenyusutanberjalan'])."</td>";
				echo"<td>".number_format($value['akumulasipenyusutan'])."</td>";
				echo"<td>".number_format($value['nilaibukuaktiva'])."</td>";
			echo"</tr>";		
			$no++;
		}	
	?>
    </table>
</fieldset>
<?php
	if(empty($caraPrint)){
		echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'onKeypress'=>'print("PRINT");', 'onclick'=>'print("PRINT");'));
	}

	$urlPrint = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/PrintrincianPenyusutan&inventarisasi_noregister='.$noInventaris.'&kodeInventaris='.$kdinventaris);
$jscript = <<< JS

function print(string){
    window.open("${urlPrint}&caraPrint=PRINT","",'location=_new, width=900px');
}
JS;
Yii::app()->clientScript->registerScript('printPenyusutan',$jscript, CClientScript::POS_HEAD);
?>