<?php
$this->breadcrumbs=array(
	'Guinvgedung Ts'=>array('index'),
	'Manage',
);

$arrMenu = array();
                // (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Inventarisasi Gedung dan Bangunan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GUInvgedungT', 'icon'=>'list', 'url'=>array('index'))) ;
               // (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Inventarisasi Gedung dan Bangunan', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('guinvgedung-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>
<legend class='rim2'>Informasi Inventarisasi Gedung dan Bangunan </legend>
<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'guinvgedung-t-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
                        'name'=>'invgedung_id',
                        'value'=>'$data->invgedung_id',
                        'filter'=>false,
                ),
		array(
                        'name'=>'pemilikbarang_id',
                        'filter'=>  CHtml::listData($model->PemilikItems, 'pemilikbarang_id', 'pemilikbarang_nama'),
                        'value'=>'$data->pemilik->pemilikbarang_nama',
                ),
				array(
                        'name'=>'barang_id',
                        'filter'=>  CHtml::listData($model->BarangItems, 'barang_id', 'barang_nama'),
                        'value'=>'$data->barang->barang_nama',
                
                ),
				array(
                        'name'=>'asalaset_id',
                        'filter'=>  CHtml::listData($model->AsalAsetItems, 'asalaset_id', 'asalaset_nama'),
                        'value'=>'$data->asal->asalaset_nama',
                ),
				array(
                        'name'=>'lokasi_id',
                        'filter'=>  CHtml::listData($model->LokasiAsetItems, 'lokasi_id', 'lokasiaset_namalokasi'),
                        'value'=>'$data->lokasi->lokasiaset_namalokasi',
                ),
		'invgedung_kode',
		'invgedung_namabrg',
		'invgedung_kontruksi',
		'invgedung_luaslantai',
		'invgedung_alamat',
		'invgedung_nodokumen',
		'invgedung_harga',		
		array(
            'header'=>'Print Label',
            'type'=>'raw',
            'value'=>'CHtml::link("<i class=icon-print></i>", "#", 
              array("rel"=>"tooltip",
                "title"=>"Print Inventaris Gedung",
                "onclick"=>"printLabel($data->invgedung_id)"
              ))',
            'htmlOptions'=>array(
                'style'=>'text-align: center',
            )
        ),
		array(
            'header'=>Yii::t('zii','Update'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
            'template'=>'{update}',
            'buttons'=>array(
                'update' => array (
                  	'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
                ),
        	),
		),
		array(
           	'header'=>'Penyusutan',
           	'type'=>'raw',
           	'value'=>'CHtml::link("<i class=icon-list-alt></i>", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/penyusutan",array("invgedung_id"=>"$data->invgedung_id")), array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Penyusutan"))',
           	'htmlOptions'=>array(
                'style'=>'text-align: center',
           	)
        ), 
        array(
            'header'=>'Pemusnahan Aset',
            'type'=>'raw',
            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            'value'=>'CHtml::Link("<i class=\"icon-trash\"></i>",Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/penghapusanAset",array("id"=>$data->invgedung_id,"tipehapus"=>"1")),
            array("class"=>"", 
                  "target"=>"iframePenghapusanAset",
                  "onclick"=>"$(\"#dialogPenghapusanAset\").dialog(\"open\");",
                  "rel"=>"tooltip",
                  "title"=>"Klik Pemusnahan Aset",
            ))',          
            'htmlOptions'=>array('style'=>'text-align: center; width:40px')
        ),
        array(
            'header'=>'Penjualan Aset',
            'type'=>'raw',
            'value'=>'CHtml::link("<i class=icon-share></i>", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/penghapusanAset",array("id"=>"$data->invgedung_id", "tipehapus"=>"2")), 
            array("class"=>"", 
                  "target"=>"iframePenghapusanAset",
                  "onclick"=>"$(\"#dialogPenghapusanAset\").dialog(\"open\");",
                  "rel"=>"tooltip",
                  "title"=>"Klik Pemusnahan Aset",
            ))', 
            'htmlOptions'=>array(
                'style'=>'text-align: center',
            )
        ),

		// array(
            // 'header'=>'Batal Register',
		// 	'class'=>'bootstrap.widgets.BootButtonColumn',
            // 'template'=>'{delete}',
            // 'buttons'=>array(
            //     'delete'=> array(
            //         'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)',
            //     ),
            // )
		// ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php 
 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
$content = $this->renderPartial('../tips/master',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrintLabel=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printInventaris');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#guinvgedung-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
function printLabel(id)
{
    window.open("${urlPrintLabel}/"+"&id="+id,"",'location=_new, width=600px, height=250px, scrollbars=yes');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPenghapusanAset',
    'options'=>array(
        'title'=>'Penjualan Aset',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>1000,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframePenghapusanAset" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>