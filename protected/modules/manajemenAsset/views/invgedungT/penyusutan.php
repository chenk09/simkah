<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'penyusutan-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<fieldset>
    <legend class="rim2">Data Inventarisasi Gedung Dan Bangunan</legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'invgedung_kode',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invgedung_kode', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($model, 'invgedung_noregister',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invgedung_noregister', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'invgedung_namabrg',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invgedung_namabrg', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($model, 'invgedung_alamat',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invgedung_alamat', array('readonly'=>true)); ?></td>
            <?php echo CHtml::activeHiddenField($model, 'invgedung_id', array('readonly'=>true)); ?>
            <?php echo CHtml::activeHiddenField($model, 'barang_id', array('readonly'=>true)); ?>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'harga perolehan',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invgedung_harga', array('readonly'=>true,'class'=>'number')); ?></td>
            
            <td><?php echo CHtml::activeLabel($model, 'nilai residu',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invgedung_nilairesidu', array('readonly'=>true,'class'=>'number')); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'umurekonomis',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'umurekonomis', array('readonly'=>true, 'class'=>'span1')).' Bulan'; ?></td>
            
            <td><?php echo CHtml::activeLabel($model, 'tanggal diterima barang',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invgedung_tglguna', array('readonly'=>true)); ?></td>
        </tr> 
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'jumlah bulan dalam 1 tahun',array('class'=>'control-label')); ?></td>
            <td>12 Bulan</td>
        </tr>  
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'penyusutan',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'bbnpenyusutanbrjlngdng', array('readonly'=>true, 'class'=>'number')); ?></td>
        </tr>
    </table>
</fieldset>

<fieldset class="">
    <legend class="rim" style="width:452px;">Penyusutan</legend>
    <div class="control-group ">
	    <?php echo $form->labelEx($model, 'tglpenyusutangedung', array('class' => 'control-label')); ?>
	    <div class="controls">
	        <?php $this->widget('MyDateTimePicker',array(
	            'model'=>$model,
	            'attribute'=>'tglpenyusutangedung',
	            'mode'=>'date',
	            'options'=> array(
	                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
	            ),
	            'htmlOptions'=>array(
	                'onkeypress'=>"return $(this).focusNextInputField(event)",
	                'class'=>'dtPicker3',
                    'onChange'=>'hitungSusut()',
	            ),
	        )); ?> 
	    </div>
	</div>

    <table id="tabledetailpenyusutan" class="table table-bordered table-condensed">
	    <thead>
		    <tr>
		        <th>No.</th>
		        <th>Bulan</th>
		        <th>Beban Penyusutan Berjalan</th>
		        <th>Akumulasi Penyusutan</th>
		        <th>Nilai Buku Aktiva</th>
		    </tr>
	    </thead>
	    <tbody>
            <?php 
                if(!$modPenyusutan->isNewRecord){
                    foreach ($modPenyusutan as $key => $value) {
                        echo"<tr>";
                            echo"<td>".$value->umurekonmisbln."</td>";
                            echo"<td>".$value->bulanpenyusutangedung."</td>";
                            echo"<td>".number_format($value->bbnpenyusutanbrjlngdng)."</td>";
                            echo"<td>".number_format($value->akumpenyusutangdng)."</td>";
                            echo"<td>".number_format($value->nilaibukugedung)."</td>";
                        echo"</tr>";
                    }
                }
            ?>
	    </tbody>
	</table>
</fieldset>
<?php
    $this->renderPartial('manajemenAsset.views.invgedungT.rekening._formRekening',
        array(
            'form'=>$form,
            'modRekenings'=>$modRekenings,
        )
    );
?>

<div class="form-actions">
    <?php echo CHtml::htmlButton($modPenyusutan->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','id'=>'btn_simpan', 'disabled'=>true)); ?>
  	<?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
            Yii::app()->createUrl($this->module->id.'/'.$this->id.'/Admin'), 
            array('class'=>'btn btn-danger')); 
    ?>

    <?php 
        $content = $this->renderPartial('rawatJalan.views.tips.tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
    ?>
</div>

<?php $this->endWidget(); ?>
<?php
    $url = Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/hitungsusut');

?>

<script type="text/javascript">

    function hitungSusut()
    {
        var tglpenyusutangedung = $('#GUInvgedungT_tglpenyusutangedung').val();
        var invgedung_id = $('#GUInvgedungT_invgedung_id').val();

        $.post('<?php echo $url; ?>', {tglpenyusutangedung:tglpenyusutangedung, invgedung_id:invgedung_id}, function(data){
        
            if(data.baru == null || data.baru==''){
                $('#tabledetailpenyusutan').find('tbody').empty();
                $("#tabledetailpenyusutan > tbody").append(data.replace());
                $("#btn_simpan").removeAttr('disabled');

                // var nilaisusut = $("#tabledetailpenyusutan > tbody > tr:last").find("input[name$='[akumpenyusutangdng]']").val();
                var nilaisusut = 0;
                $("#tabledetailpenyusutan > tbody").find("tr").each(function(){
                    a = unformatNumber($(this).find('input[name$="[bbnpenyusutanbrjlngdng]"]').val());
                    nilaisusut = nilaisusut + a;
                });

                $('#JenispengeluaranrekeningV_0_saldodebit').val(nilaisusut);
                $('#JenispengeluaranrekeningV_1_saldokredit').val(nilaisusut);

            }else if(data.baru=="false"){
                $('#tabledetailpenyusutan').find('tbody').empty();
                alert("Penyusutan untuk periode yang dipilih telah dilakukan");
                $('#JenispengeluaranrekeningV_0_saldodebit').val(0);
                $('#JenispengeluaranrekeningV_1_saldokredit').val(0);
            }
            
        }, 'json');
    }

    setJurnal(9);
    function setJurnal(jenispengeluaran_id){     
        var aktivatetap = 0;
        var akumsusut   = 0;
        var kerugian    = 0;  

        setTimeout(function(){
            getDataManajemenAset(jenispengeluaran_id,akumsusut,kerugian,aktivatetap);
        },500);
    }

    function unformatNumbers(){
        $('.number').each(function(){this.value = unformatNumber(this.value)});
    }

</script>