<?php
	$namaModel = "PenyusutangedungT";
    // $invgedung_tglguna = explode("-", $invgedung_tglguna);
    $tgl_guna		= $invgedung_tglguna[2];
    if(empty($ceksusut)){
    	$bulan_guna		= $invgedung_tglguna[1];
    }else{
    	$bulan_guna		= $invgedung_tglguna[1] + 1;
    	if($bulan_guna<10){
			$bulan_guna = "0".$bulan_guna;
		}
    }
    $tahun_guna		= $invgedung_tglguna[0];

    $nilairesidu 	= empty($model->invgedung_nilairesidu) ? 0 : $model->invgedung_nilairesidu;
    $penyusutan 	= $model->invgedung_harga;
    $umurekonomis 	= $model->umurekonomis;
	$bbnpenyusutanbrjlngdng = number_format((($penyusutan - $nilairesidu) / $umurekonomis),0);
	
	if(empty($ceksusut)){
		$akumpenyusutangdng = MyFunction::number_unformat($bbnpenyusutanbrjlngdng);
		$penambahakum 	= $akumpenyusutangdng;
		$nilaibukugedung = $model->invgedung_harga - $penambahakum;
	}else{
		$akumpenyusutangdng = MyFunction::number_unformat($bbnpenyusutanbrjlngdng)+$model->invgedung_akumsusut;
		$penambahakum 	= MyFunction::number_unformat($bbnpenyusutanbrjlngdng);
		$nilaibukugedung = $model->invgedung_harga - $akumpenyusutangdng;
	}

	for ($i=0; $i < $jml_penyusutan; $i++) { 
		$bulanpenyusutangedung = $tahun_guna."-".$bulan_guna."-".$tgl_guna;
		$urutan = $i + 1;
		echo('<tr>');
			echo "<td>".
                CHtml::textField($namaModel."[$i][umurekonmisbln]", $urutan,array('readonly'=>true, 'class'=>'span1')).
            "</td>";
            echo "<td>".
                CHtml::textField($namaModel."[$i][bulanpenyusutangedung]", $bulanpenyusutangedung,array('readonly'=>true, 'class'=>'span3')).
            "</td>";
            echo "<td>".
                CHtml::textField($namaModel."[$i][bbnpenyusutanbrjlngdng]", $bbnpenyusutanbrjlngdng,array('readonly'=>true, 'class'=>'span3 number')).
            "</td>";
            echo "<td>".
                CHtml::textField($namaModel."[$i][akumpenyusutangdng]", number_format($akumpenyusutangdng,0),array('readonly'=>true, 'class'=>'span3')).
            "</td>";
            echo "<td>".
                CHtml::textField($namaModel."[$i][nilaibukugedung]", number_format($nilaibukugedung,0),array('readonly'=>true, 'class'=>'span3')).
            "</td>";
		echo('</tr>');

		$akumpenyusutangdng += $penambahakum;
		$nilaibukugedung 	-= $penambahakum;
		$bulan_guna = intval($bulan_guna) + 1;
		if($bulan_guna>12){
			$bulan_guna = $bulan_guna%12;
			$tahun_guna = intval($tahun_guna) + 1;
			if($bulan_guna<10){
				$bulan_guna = "0".$bulan_guna;
			}
		}elseif($bulan_guna<10){
			$bulan_guna = "0".$bulan_guna;
		}
	}

?>