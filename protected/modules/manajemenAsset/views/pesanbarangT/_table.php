<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action = $this->getAction()->getId();
$currentUrl = Yii::app()->createUrl($module . '/' . $controller . '/' . $action);
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'gupesanbarang-t-grid',
	'dataProvider'=>$model->searchInformasi(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'pesanbarang_id',
//		array(
//                        'name'=>'pesanbarang_id',
//                        'value'=>'$data->pesanbarang_id',
//                        'filter'=>false,
//                ),
		//'mutasibrg_id',
		'nopemesanan',
		'tglpesanbarang',
		'tglmintadikirim',
                'keterangan_pesan',
		array(
                    'header'=>'Pegawai Pemesan',
                    'value'=>'$data->pegawaipemesan->nama_pegawai',
                ),
                array(
                  'header'=>'Pegawai Mengetahui',
                  'value'=>'$data->pegawaimengetahui->nama_pegawai',
                ),
//		'pegmengetahui_id',
                array(
                    'header'=>'Ruangan Pemesan',
                    'value'=>'$data->ruanganpemesan->ruangan_nama',
                ),
//		'ruanganpemesan.ruangan_nama',
		/*
		
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
		array(
                    'header'=>'Detail',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",  Yii::app()->controller->createUrl("'.$controller.'/detailPesanBarang",array("id"=>$data->pesanbarang_id)),array("id"=>"$data->pesanbarang_id","target"=>"frameDetail","rel"=>"tooltip","title"=>"Klik untuk Detail Pemesanan Barang", "onclick"=>"window.parent.$(\'#dialogDetail\').dialog(\'open\')"));',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>