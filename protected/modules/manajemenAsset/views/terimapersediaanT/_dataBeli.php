<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::activeLabel($modBeli, 'nopembelian', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php
                    echo CHtml::activeTextField($modBeli, 'nopembelian', array('readonly'=>true))
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::activeLabel($modBeli, 'tglpembelian', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php
                    echo CHtml::activeTextField($modBeli, 'tglpembelian', array('readonly'=>true))
                    ?>
                </div>
            </div>
            
            <div class="control-group ">
                <?php echo CHtml::activeLabel($modBeli, 'supplier_id', array('class'=>'control-label')) ?>
                <div class="controls">
				
                    <?php
                    echo CHtml::activeTextField($modBeli, 'supplier_id', array('readonly'=>true, 'value'=>$modBeli->supplier->supplier_nama))
                    ?>
                </div>
            </div>
        </td>
    </tr>
</table>