<?php
$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'Golongan', 'url'=>$this->createUrl('/manajemenAsset/golonganM/admin'), 'active'=>true),
        array('label'=>'Kelompok', 'url'=>$this->createUrl('/manajemenAsset/kelompokM/admin'),),
        array('label'=>'Sub Kelompok', 'url'=>$this->createUrl('/manajemenAsset/subkelompokM/admin'),),
        array('label'=>'Bidang', 'url'=>$this->createUrl('/manajemenAsset/bidangM/admin'),),
         array('label'=>'Satuan Barang', 'url'=>$this->createUrl('/manajemenAsset/lookupM/admin'),),
        
    ),
)); 
?>