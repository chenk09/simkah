<?php
$this->breadcrumbs=array(
	'Guinvperalatan Ts'=>array('index'),
	'Manage',
);

$arrMenu = array();
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('guinvperalatan-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>
<legend class='rim2'>Informasi Mutasi Aset Peralatan  </legend>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'guinvperalatan-t-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		'nolama',
		'invperalatan_noregister',
		array(
            'name'=>'tglmutasi',
            'value'=>'$data->tglmutasi',
            'filter'=>false,
        ),
		'invperalatan_namabrg',
		'invperalatan_ket',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<legend class="rim">Pencarian</legend>
<div class="search-form">
<?php $this->renderPartial('_searchMutasi',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->