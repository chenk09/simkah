<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefaultExcel',array('judulLaporan'=>$judulLaporan, 'colspan'=>6));      
?>

<style>
    /*==Untuk Label Dengan DIV==*/
    .labelPrint {
        background-color: #FFFFFF;
        color: #0C0C0C;
        display: inline-block;
        margin: 0px;
        padding: 0px;
        width: 49%;
        text-align: left;
        font-size-adjust: 0.55;
        line-height:145%;
    }
    .labelPrintLab {
        background-color: #FFFFFF;
        color: #0C0C0C;
        display: inline-block;
        margin: 5px;
        padding: 0px;
        width: 49%;
        text-align: left;
        font-size-adjust: 0.60;
        line-height:110%;
        /*font-size: 40pt;*/
    }

    .labelxray{
        background-color: #FFFFFF;
        color: #0C0C0C;
        display: inline-block;
        margin: 5px;
        padding: 0px;
        width: 50%;
        text-align: left;
        font-size-adjust: 0.70;
        line-height:100%;
    }

    .printLab{
        font-family: arial;
        font-size: 8pt;
    }

    .labelPrintKecil {
        background-color: #FFFFFF;
        color: #0C0C0C;
        display: inline-block;
        margin: 0px;
        padding: 0px 50px;
        width: 75%;
        /*text-align: center;*/
        font-size: 4pt;
    }
    /*=====*/
    .border {
        border: 1px solid;
        margin: 0px;
        padding: 0px;
        width: 100%;
    }
    .utama{
        padding:20px;
        text-align: left;
        font-size: 12pt;
        /*border:1px solid;*/
        /*width: 210mm; height: 297mm;  ukuran A4 */
        width: 100%; /* height: 297mm; ukuran A4 */
        vertical-align: top;
    }
    .rontgentd{
        font-family: arial;
        font-size: 12pt;
    }
    td {
        font-family: arial;
        font-size: 14pt;
        vertical-align: top;
    }
    div .nama_periksa{
        font-family: arial;
        font-size: 12pt;
        font-weight: bold;
        text-align: center;
    }
    .trrontgen {
        padding: 0px;
    }
    
</style>
<div class="labelPrintKecil">
    <table class="border">
        <tr>
            <td>&nbsp;</td>
            <td colspan="3">&nbsp;
            </td>
        </tr>
        <tr>
             <td>No. Register Asset</td>
            <td colspan="3">: <?php echo $model->invperalatan_noregister; ?>
        </tr> 
        <tr>
            <td>&nbsp;</td>
            <td colspan="3">&nbsp;</td>
        </tr>      
        <!--<tr  class="border"><td colspan="3"><table><b>Laboratorium Klinik Utama Budi Kartini<b></table></td></tr>-->
    </table>
</div>


