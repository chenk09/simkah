<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action = $this->getAction()->getId();
$currentUrl = Yii::app()->createUrl($module . '/' . $controller . '/' . $action);
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'gumutasibrg-t-grid',
	'dataProvider'=>$model->searchInformasi(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'mutasibrg_id',
//		array(
//                        'name'=>'mutasibrg_id',
//                        'value'=>'$data->mutasibrg_id',
//                        'filter'=>false,
//                ),
		'nomutasibrg',
		'tglmutasibrg',
		'pegawaipengirim.nama_pegawai',
                'pegawaimengetahui.nama_pegawai',
		'totalhargamutasi',
		'ruangtujuan.ruangan_nama',
                'keterangan_mutasi',
                'pesanbarang.nopemesanan',
                array(
                    'header'=>'Detail',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",  Yii::app()->controller->createUrl("'.$controller.'/detailMutasiBarang",array("id"=>$data->mutasibrg_id)),array("id"=>"$data->mutasibrg_id","target"=>"frameDetail","rel"=>"tooltip","title"=>"Klik untuk Detail Mutasi Barang", "onclick"=>"window.parent.$(\'#dialogDetail\').dialog(\'open\')"));',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
                array(
                    'header'=>'Batal Mutasi',
                    'type'=>'raw',
                    'value'=>'($data->testingData == false) ? CHtml::link("<i class=\'icon-list-silver\'></i> ",  Yii::app()->controller->createUrl("'.$controller.'/batalMutasiBarang",array("id"=>$data->mutasibrg_id)),array("id"=>"$data->mutasibrg_id","target"=>"frameDetail","rel"=>"tooltip","title"=>"Klik untuk Pembatalan Mutasi Barang", "onclick"=>"window.parent.$(\'#dialogDetail\').dialog(\'open\')")) : "Telah Dibatalkan"',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>