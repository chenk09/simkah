<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php
if(!empty($modBarang)){
?>
<fieldset>
    <legend class="rim">Data Barang</legend>
    <?php echo CHtml::css ('table.table tr td.img img{max-width:120px;max-height:120px;}'); ?>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modBarang, 'barang_id',array('class'=>'control-label')); ?></td>
            <td>
               
            <div class="control-group ">
                    <label class="control-label" for="bidang"></label>
                    <div class="controls">
                        <?php //echo $form->hiddenField($model,'barang_id'); ?>
                    <?php 
                            $this->widget('MyJuiAutoComplete', array(
                                            
                                            'name'=>'barang_nama',
                                            'value'=>$modBarang->barang_nama,
                                            'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/getBarang').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                             'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'focus'=> 'js:function( event, ui ) {
                                                        $(this).val( ui.item.label);
                                                        return false;
                                                    }',
                                                   'select'=>'js:function( event, ui ) { 
                                                        
                                                  $("#'.CHtml::activeId($modBarang,'barang_id').'").val(ui.item.barang_id);
                                                  $("#'.CHtml::activeId($modBarang,'barang_type').'").val(ui.item.barang_type);   
                                                  $("#'.CHtml::activeId($modBarang,'barang_image').'").val(ui.item.barang_image);     
                                                  $("#'.CHtml::activeId($modBarang,'barang_kode').'").val(ui.item.barang_kode);
                                                  $("#'.CHtml::activeId($modBarang,'barang_nama').'").val(ui.item.barang_nama);  
                                                  $("#'.CHtml::activeId($modBarang,'barang_noseri').'").val(ui.item.barang_noseri);   
                                                  $("#'.CHtml::activeId($modBarang,'barang_thnbeli').'").val(ui.item.barang_thnbeli);     
                                                  $("#'.CHtml::activeId($modBarang,'barang_satuan').'").val(ui.item.barang_satuan);  
                                                  $("#'.CHtml::activeId($modBarang,'barang_jmldlmkemasan').'").val(ui.item.barang_jmldlmkemasan);
                                                  
                                                  $("#'.CHtml::activeId($model,'pemilikbarang_id').'").val("");
                                                  $("#'.CHtml::activeId($model,'lokasi_id').'").val("");
                                                  $("#'.CHtml::activeId($model,'invperalatan_noregister').'").val("");
                                                  $("#'.CHtml::activeId($model,'invtanah_noregister').'").val("");
                                                  $("#'.CHtml::activeId($model,'invgedung_noregister').'").val("");
                                                  $("#'.CHtml::activeId($model,'invjalan_noregister').'").val("");
                                                  $("#'.CHtml::activeId($model,'invasetlain_noregister').'").val("");

                                                    if(ui.item.barang_image != null){
                                                        $("td.img img").attr(\'src\',\''.Params::urlBarangDirectory().'\'+ui.item.barang_image);
                                                    } else {
                                                        $("td.img img").attr(\'src\',\''.Params::urlBarangDirectory().'no_photo.jpeg\');
                                                    }
                                                        return false;
                                                    }',
                                            ),
                                            'htmlOptions'=>array(
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogBarang'),
                                        )); 
                        ?>
                    </div>
                </div>
            </td>
            
            <td><?php echo CHtml::activeLabel($modBarang, 'barang_type',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modBarang, 'barang_type', array('readonly'=>true)); ?></td>
            <td rowspan="4" class='img'>
                <?php 
                    if(!empty($modBarang->barang_image)){
                        echo CHtml::image(Params::urlBarangDirectory().$modBarang->barang_image, 'barang_image', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlBarangDirectory().'no_photo.jpeg', 'barang_image', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modBarang, 'barang_kode',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modBarang, 'barang_kode', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modBarang, 'barang_nama',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modBarang, 'barang_nama', array('readonly'=>true)); ?></td>
            
            
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modBarang, 'barang_thnbeli',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modBarang, 'barang_thnbeli', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modBarang, 'barang_satuan',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modBarang, 'barang_satuan', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modBarang, 'barang_jmldlmkemasan',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::activeTextField($modBarang,'barang_jmldlmkemasan', array('readonly'=>true)); ?>
                            </td>
            <td><?php echo CHtml::activeLabel($modBarang, 'barang_noseri',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modBarang, 'barang_noseri', array('readonly'=>true)); ?></td>
            
            </tr>
    </table>
</fieldset>
<?php
//========= Dialog buat cari data Bidang =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogBarang',
    'options'=>array(
        'title'=>'Data Barang',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>400,
        'resizable'=>false,
    ),
));

$barang= new BarangM('search');
$barang->unsetAttributes();
if(isset($_GET['BarangM']))
    $barang->attributes = $_GET['BarangM'];

if(isset($_GET['MAAssetV']))
    $barang->attributes = $_GET['MAAssetV'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'barang-v-grid',
    'dataProvider'=>$barang->search(),
    'filter'=>$barang,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
      'barang_kode','barang_type','barang_nama',
      'barang_merk','barang_noseri',
      'barang_ukuran','barang_bahan',
      'barang_warna', 'barang_satuan',
      // array(
      //               'header'=>'Merk / No Seri',
      //               'filter'=>'true'
      //               'value'=>'$data->barang_merk." / ".$data->barang_noseri',
      //           ),
        //'golongan_id',
        //'golongan_kode',
        //'golongan_nama',
          // 'bidang.subkelompok.kelompok.golongan.golongan_nama',
        //'kelompok_id',
        //'kelompok_kode',
       // 'kelompok_nama',
         // 'bidang.subkelompok.kelompok.kelompok_nama',
        
        //'subkelompok_id',
        //'subkelompok_kode',
       // 'subkelompok_nama',
        // 'bidang.subkelompok.subkelompok_nama',
        //'bidang_id',
        //'bidang_kode',
        //'bidang_nama',
        // 'bidang.bidang_nama',
        ////'barang_id',
//        array(
//                        'name'=>'barang_id',
//                        'value'=>'$data->barang_id',
//                        'filter'=>false,
//                ),
       // 'barang_type',
//        'barang_kode',
//        'barang_namalainnya',
//        'barang_merk',
//        'barang_noseri',
//        'barang_ukuran',
//        'barang_bahan',
//        'barang_thnbeli',
//        'barang_warna',
//        'barang_statusregister',
//        'barang_ekonomis_thn',
//        'barang_satuan',
//        'barang_jmldlmkemasan',
//        'barang_image',
//        'barang_aktif',


                
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",
                                "#",
                                array(
                                    "class"=>"btn-small", 
                                    "id" => "selectKelompoks",
                                    "onClick" => "
                                    $(\"#'.CHtml::activeId($modBarang,'barang_id').'\").val($data->barang_id);
                                                  $(\"#'.CHtml::activeId($modBarang,'barang_type').'\").val(\"$data->barang_type\");   
                                                  $(\"#'.CHtml::activeId($modBarang,'barang_image').'\").val(\"$data->barang_image\");     
                                                  $(\"#'.CHtml::activeId($modBarang,'barang_kode').'\").val(\"$data->barang_kode\");
                                                  $(\"#barang_nama\").val(\"$data->barang_nama\");
                                                  $(\"#'.CHtml::activeId($modBarang,'barang_nama').'\").val(\"$data->barang_nama\");   
          
                                                  $(\"#'.CHtml::activeId($modBarang,'barang_noseri').'\").val(\"$data->barang_noseri\");   
                                                  $(\"#'.CHtml::activeId($modBarang,'barang_thnbeli').'\").val($data->barang_thnbeli);     
                                                  $(\"#'.CHtml::activeId($modBarang,'barang_satuan').'\").val(\"$data->barang_satuan\");  
                                                  $(\"#'.CHtml::activeId($modBarang,'barang_jmldlmkemasan').'\").val($data->barang_jmldlmkemasan);
                                   if(\"$data->barang_image\" != \"\"){
                                                        $(\"td.img img\").attr(\'src\',\''.Params::urlBarangDirectory().'\'+\"$data->barang_image\");
                                                    } else {
                                                        $(\"td.img img\").attr(\'src\',\''.Params::urlBarangDirectory().'no_photo.jpeg\');
                                                    }

                                                  $(\"#'.CHtml::activeId($model,'pemilikbarang_id').'\").val(\"\");
                                                  $(\"#'.CHtml::activeId($model,'lokasi_id').'\").val(\"\");
                                                  $(\"#'.CHtml::activeId($model,'invperalatan_noregister').'\").val(\"\");
                                                  $(\"#'.CHtml::activeId($model,'invtanah_noregister').'\").val(\"\");
                                                  $(\"#'.CHtml::activeId($model,'invgedung_noregister').'\").val(\"\");
                                                  $(\"#'.CHtml::activeId($model,'invjalan_noregister').'\").val(\"\");
                                                  $(\"#'.CHtml::activeId($model,'invjalannvasetlain_noregister').'\").val(\"\");

                                                       $(\"#dialogBarang\").dialog(\"close\");
                                                    
                                    "))
                                        ',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget();
?>
<?php } ?>


