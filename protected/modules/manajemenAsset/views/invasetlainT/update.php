
<?php
$this->breadcrumbs=array(
	'Guinvperalatan Ts'=>array('index'),
	$model->invperalatan_id=>array('view','id'=>$model->invperalatan_id),
	'Update',
);
$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('/_dataBarang', array('modBarang' => $modBarang ,));
$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Inventarisasi Peralatan Non Medis', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GUInvperalatanT', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' GUInvperalatanT', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','View').' GUInvperalatanT', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->invperalatan_id))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Inventarisasi Peralatan Medis', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model,'data'=>$data,'dataAsalAset'=>$dataAsalAset , 'dataLokasi'=>$dataLokasi)); ?>
