<fieldset>
    <legend class="rim">Mutasi Non Medis</legend>
	<table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modlama, 'nama inventarisasi',array('class'=>'control-label')); ?>
            </td>
            <td>
            <div class="control-group ">
                <label class="control-label" for="bidang"></label>
                <div class="controls">
                <?php 
                        $this->widget('MyJuiAutoComplete', array(
                            'name'=>'InvperalatanT[invperalatan_namabrg]',
                            'value'=>$modlama->invperalatan_namabrg,
                            // 'source'=>'js: function(request, response) {
                            //    	$.ajax({
                            //        	url: "'.Yii::app()->createUrl('ActionAutoComplete/getInventarisasi').'",
                            //        	dataType: "json",
                            //        	data: {
                            //            term: request.term,
                            //        	},
                            //        	success: function (data) {
                            //            response(data);
                            //        	}
                            //    	})
                            // }',
                            // 'options'=>array(
                            //     'showAnim'=>'fold',
                            //     'minLength' => 2,
                    	       //  'focus'=> 'js:function( event, ui ) {
                            //         $(this).val( ui.item.label);
                            //         return false;
                            //     }',
                            //     'select'=>'js:function( event, ui ) { 
             	              //   $("#'.CHtml::activeId($modlama,'invperalatan_id').'").val(ui.item.invperalatan_id);  
                            //     }',
                            // ),
                            'htmlOptions'=>array(
                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                            ),
                            'tombolDialog'=>array('idDialog'=>'dialogInventarisasi'),
                        )); 
                    ?>
                </div>
            </div>
            </td>
            <td><?php echo CHtml::activeLabel($modlama, 'invperalatan_merk',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modlama, 'invperalatan_merk', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
        	<td><?php echo CHtml::activeLabel($modlama, 'invperalatan_kode',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modlama, 'invperalatan_kode', array('readonly'=>true)); ?></td>
            <td><?php echo CHtml::activeLabel($modlama, 'invperalatan_thnpembelian',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modlama, 'invperalatan_thnpembelian', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
        	<td><?php echo CHtml::activeLabel($modlama, 'invperalatan_noregister',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modlama, 'invperalatan_noregister', array('readonly'=>true)); ?></td>

            <td><?php echo CHtml::activeLabel($modlama, 'barang_nama',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modlama, 'barang_nama', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
        	<td><?php echo CHtml::activeLabel($modlama, 'lokasiaset_namalokasi',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modlama, 'lokasiaset_namalokasi', array('readonly'=>true)); ?></td>
        	<td><?php echo CHtml::activeLabel($modlama, 'asalaset_nama',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modlama, 'asalaset_nama', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
        	<td><?php echo CHtml::activeLabel($modlama, 'pemilikbarang_nama',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modlama, 'pemilikbarang_nama', array('readonly'=>true)); ?></td>
    </table>

</fieldset>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogInventarisasi',
    'options'=>array(
        'title'=>'Data Inventarisasi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modPeralatan	= new InvperalatanT('searchMutasi');
$modPeralatan->unsetAttributes();
if(isset($_GET['InvperalatanT']))
    $modPeralatan->attributes = $_GET['InvperalatanT'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'invperalatan-v-grid',
    'dataProvider'=>$modPeralatan->searchMutasi(),
    'filter'=>$modPeralatan,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
	    'invperalatan_kode','invperalatan_noregister','invperalatan_namabrg',
      	'invperalatan_merk','invperalatan_thnpembelian',
      	'invperalatan_harga','invperalatan_nilairesidu',
      	'invperalatan_umurekonomis',
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",
                "#",
                array(
                    "class"=>"btn-small", 
                    "id" => "selectKelompoks",
                    "onClick" => "
                    $(\"#'.CHtml::activeId($modlama,'invperalatan_kode').'\").val(\"$data->invperalatan_kode\"); 
                    $(\"#'.CHtml::activeId($modlama,'invperalatan_noregister').'\").val(\"$data->invperalatan_noregister\"); 
                    $(\"#'.CHtml::activeId($modlama,'invperalatan_merk').'\").val(\"$data->invperalatan_merk\"); 
                    $(\"#'.CHtml::activeId($modlama,'invperalatan_thnpembelian').'\").val(\"$data->invperalatan_thnpembelian\"); 
                    $(\"#'.CHtml::activeId($modlama,'invperalatan_namabrg').'\").val(\"$data->invperalatan_namabrg\"); 
                    $(\"#'.CHtml::activeId($modlama,'barang_nama').'\").val(\"$data->barang_nama\"); 
                    $(\"#'.CHtml::activeId($modlama,'lokasiaset_namalokasi').'\").val(\"$data->lokasiaset_namalokasi\");
                    $(\"#'.CHtml::activeId($modlama,'pemilikbarang_nama').'\").val(\"$data->pemilikbarang_nama\");
                    $(\"#'.CHtml::activeId($modlama,'asalaset_nama').'\").val(\"$data->asalaset_nama\");
                    $(\"#'.CHtml::activeId($modlama,'invperalatan_id').'\").val($data->invperalatan_id);
                    $(\"#'.CHtml::activeId($modlama,'barang_id').'\").val($data->barang_id);
                    $(\"#'.CHtml::activeId($model,'invperalatan_namabrg').'\").val(\"$data->invperalatan_namabrg\");
                    $(\"#'.CHtml::activeId($model,'invperalatan_merk').'\").val(\"$data->invperalatan_merk\");
                    $(\"#'.CHtml::activeId($model,'invperalatan_ukuran').'\").val(\"$data->invperalatan_ukuran\");
                    $(\"#'.CHtml::activeId($model,'invperalatan_bahan').'\").val(\"$data->invperalatan_bahan\");
                    $(\"#'.CHtml::activeId($model,'invperalatan_thnpembelian').'\").val(\"$data->invperalatan_thnpembelian\");
                    $(\"#'.CHtml::activeId($model,'invperalatan_tglguna').'\").val(\"$data->invperalatan_tglguna\");
                    $(\"#'.CHtml::activeId($model,'invperalatan_harga').'\").val(\"$data->invperalatan_harga\");
                    $(\"#'.CHtml::activeId($model,'invperalatan_nilairesidu').'\").val(\"$data->invperalatan_nilairesidu\");
                    $(\"#'.CHtml::activeId($model,'invperalatan_keadaan').'\").val(\"$data->invperalatan_keadaan\");
                    $(\"#'.CHtml::activeId($model,'invperalatan_umurekonomis').'\").val(\"$data->invperalatan_umurekonomis\");

                    $(\"#dialogInventarisasi\").dialog(\"close\");
                                        
                "))
            ',
        ),
	),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

	$this->endWidget();
?>