<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'penyusutan-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<fieldset>
    <legend class="rim2">Penyusutan Peralatan Medis, Non Medis dan Kendaraan</legend>
    <?php
    	if(isset($_GET['status'])){
    		echo '<div class="alert alert-block alert-success">
		            <a class="close" data-dismiss="alert">×</a>
		            Penyusutan berhasil disimpan
		        </div>';
    	}
    ?>
    <legend class="rim" style="width:452px;">Pencarian</legend>
	<div class="control-group ">
	    <?php echo $form->labelEx($model, 'jenis inventaris', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo $form->dropDownList($model,'jenisinventaris', array('03'=>'Kendaraan', '04'=>'Peralatan Medis', '05'=>'Peralatan Non Medis'), array('onchange'=>'pilihJenis();', 'class'=>'span3')); ?>
		</div>
	</div>
    <div class="control-group ">
	    <?php echo $form->labelEx($model, 'tglpenyusutanperalatan', array('class' => 'control-label')); ?>
	    <div class="controls">
	        <?php $this->widget('MyDateTimePicker',array(
	            'model'=>$model,
	            'attribute'=>'tglpenyusutanperalatan',
	            'mode'=>'date',
	            'options'=> array(
	                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
	            ),
	            'htmlOptions'=>array(
	                'onkeypress'=>"return $(this).focusNextInputField(event)",
	                'class'=>'dtPicker3',
                    'onChange'=>'hitungSusut()',
	            ),
	        )); ?> 
	    </div>
	</div>

	<table id="tabledetailpenyusutan" class="table table-bordered table-condensed">
	    <thead>
		    <tr>
		        <th>No.</th>
		        <th>Nama Barang</th>
		        <th>No. Register</th>
		        <th>Bulan</th>
		        <th>Nilai Perolehan</th>
		        <th>Beban Penyusutan Berjalan</th>
		        <th>Akumulasi Penyusutan</th>
		        <th>Nilai Buku Aktiva</th>
		    </tr>
	    </thead>
	    <tbody>
	    </tbody>
	</table>

	<?php
	    $this->renderPartial('manajemenAsset.views.invgedungT.rekening._formRekening',
	        array(
	            'form'=>$form,
	            'modRekenings'=>$modRekenings,
	        )
	    );
	?>
</fieldset>

<div class="form-actions">
    <?php echo CHtml::htmlButton($modPenyusutan->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','id'=>'btn_simpan', 'disabled'=>true)); ?>
  	<?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
            Yii::app()->createUrl($this->module->id.'/'.$this->id.'/index'), 
            array('class'=>'btn btn-danger')); 
    ?>

    <?php 
        $content = $this->renderPartial('rawatJalan.views.tips.tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
    ?>
</div>

<?php $this->endWidget(); ?>
<?php
    $url = Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/hitungsusut');
?>
<script type="text/javascript">

	function pilihJenis()
	{
		$('#GUInvperalatanT_tglpenyusutanperalatan').val('');
		$('#tabledetailpenyusutan').find('tbody').empty();
		$("#btn_simpan").attr('disabled','disabled');
		var jenis = $('#GUInvperalatanT_jenisinventaris').val();
		if(jenis=='03'){
			setJurnal(10);
		}else if(jenis=='04'){
			setJurnal(12);
		}else if(jenis=='05'){
			setJurnal(11);
		}

		$('#JenispengeluaranrekeningV_0_saldodebit').val(0);
        $('#JenispengeluaranrekeningV_1_saldokredit').val(0);
	}

	function hitungSusut()
    {
    	var tglpenyusutanperalatan = $('#GUInvperalatanT_tglpenyusutanperalatan').val();
    	var jenisinventaris = $('#GUInvperalatanT_jenisinventaris').val();
    	
    	$.post('<?php echo $url; ?>', {tglpenyusutanperalatan:tglpenyusutanperalatan, jenisinventaris:jenisinventaris}, function(data){
    			$('#tabledetailpenyusutan').find('tbody').empty();
    			$("#tabledetailpenyusutan > tbody").append(data.replace());

    			var nilaisusut = $('#GUInvperalatanT_totalpenyusutan').val();
    			if(unformatNumber(nilaisusut) > 0){
    				$("#btn_simpan").removeAttr('disabled');
    			}
    			$('#JenispengeluaranrekeningV_0_saldodebit').val(nilaisusut);
                $('#JenispengeluaranrekeningV_1_saldokredit').val(nilaisusut);

		}, 'json');
    }

    setJurnal(10);
    function setJurnal(jenispengeluaran_id){     
        var aktivatetap = 0;
        var akumsusut   = 0;
        var kerugian    = 0;  

        setTimeout(function(){
            getDataManajemenAset(jenispengeluaran_id,akumsusut,kerugian,aktivatetap);
        },500);
    }

    function unformatNumbers(){
        $('.number').each(function(){this.value = unformatNumber(this.value)});
    }

</script>