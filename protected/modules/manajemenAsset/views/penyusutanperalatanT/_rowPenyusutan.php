<?php
	$namaModel = "PenyusutanperalatanT";
	$urutan = 1;
	$index = 0;
	$jumlahpenyusutan = 0;
	$totalakumulasi = 0;
	$totalnilaibuku = 0;
	foreach ($data as $key => $value) {
		$invperalatan_id = $value['invperalatan_id'];
		if($value['jml_penyusutan']>=0){
			$tgl_penyusutan = explode("-", $value['bulansusut']);
			$bulan_susut	= $tgl_penyusutan[1];
			if($bulan_susut<10){
				$bulan_susut	= "0".intval($bulan_susut);
			}
			if($tgl_penyusutan[1]>12){
	            $tahun_susut	= $tgl_penyusutan[0]+1;
	            $bulan_susut	= "01";
	        }
	        else{
	        	$tahun_susut	= $tgl_penyusutan[0];
	        }
	        $penyusutan 	= $value['invperalatan_harga'];
	        $nilairesidu 	= $value['nilairesidu'];
	        $umurekonomis 	= $value['umurekonomis'];
	        $bbnpenyusutanbrjlngdng = number_format((($penyusutan - $nilairesidu) / $umurekonomis),0);
	        if(empty($value['akumpenyusutanper']))
	        	$akumulasipenyusutan = MyFunction::number_unformat($bbnpenyusutanbrjlngdng);
	        else
	        	$akumulasipenyusutan = MyFunction::number_unformat($bbnpenyusutanbrjlngdng) + $value['akumpenyusutanper'];

	        $nilaibuku = $penyusutan - $akumulasipenyusutan; 

	        if(empty($value['cekbulansusut'])){
	        	$i = -1;
	        }else{
	        	$i = 0;
	        	$bulan_susut = $bulan_susut + 1;
	        }
            $tgl_susut = $tahun_susut."-".$bulan_susut."-".$tgl_penyusutan[2];
			for($i; $i<$value['jml_penyusutan']; $i++){
				echo"<tr>
					<td>".$urutan."".CHtml::hiddenField($namaModel."[$index][invperalatan_id]", $invperalatan_id,array())."</td>
					<td>".$value['namabarang']."</td>
					<td>".$value['noregister']."</td>
					<td>".CHtml::textField($namaModel."[$index][bulanpenyusutanperalatan]", $tgl_susut,array('readonly'=>true, 'class'=>'span2'))."</td> 
					<td>".number_format($value['invperalatan_harga'],0)."</td> 
					<td>".CHtml::textField($namaModel."[$index][bbnpenyusutanbrjlngdng]", $bbnpenyusutanbrjlngdng,array('readonly'=>true, 'class'=>'span3 number'))."</td>
					<td>".CHtml::textField($namaModel."[$index][akumpenyusutanper]", number_format($akumulasipenyusutan,0),array('readonly'=>true, 'class'=>'span3'))."</td>
					<td>".CHtml::textField($namaModel."[$index][nilaibukuperalatan]", number_format($nilaibuku,0),array('readonly'=>true, 'class'=>'span3'))."</td>
				</tr>";

		        $bulan_susut	= intval($bulan_susut) + 1;
				if($bulan_susut<10){
					$bulan_susut	= "0".$bulan_susut;
				}
				if(intval($bulan_susut)>12){
		            $tahun_susut	= $tahun_susut+1;
		            $bulan_susut	= "01";
		        }
				$tgl_susut = $tahun_susut."-".$bulan_susut."-".$tgl_penyusutan[2];

				$jumlahpenyusutan += MyFunction::number_unformat($bbnpenyusutanbrjlngdng);
				$totalakumulasi += MyFunction::number_unformat($akumulasipenyusutan);
				$totalnilaibuku += MyFunction::number_unformat($nilaibuku);

				$akumulasipenyusutan += MyFunction::number_unformat($bbnpenyusutanbrjlngdng);
				$nilaibuku = $nilaibuku - $akumulasipenyusutan; 

				$urutan++;
				$index++;
			}
		}
	}
	if(count($data)>0){
		echo "<tr>
			<td colspan='5'>Jumlah</td>
			<td>".CHtml::textField("GUInvperalatanT[totalpenyusutan]", number_format($jumlahpenyusutan,0),array('readonly'=>true, 'class'=>'span3'))."</td>
			<td>".CHtml::textField("GUInvperalatanT[totalakumulasi]", number_format($totalakumulasi,0),array('readonly'=>true, 'class'=>'span3'))."</td>
			<td>".CHtml::textField("GUInvperalatanT[totalnilaibuku]", number_format($totalnilaibuku,0),array('readonly'=>true, 'class'=>'span3'))."</td>
		</tr>";
	}

?>