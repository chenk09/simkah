<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'.',
        'thousands'=>',',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'penghapusan-t-form',
	'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onsubmit'=>'unformatNumbers();'),
    'focus'=>'#',
)); ?>
<fieldset>
    <legend class="rim2">Data Penjualan Aset</legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'invtanah_kode',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invtanah_kode', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($model, 'invtanah_noregister',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invtanah_noregister', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'invtanah_namabrg',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invtanah_namabrg', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($model, 'invtanah_nosertifikat',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invtanah_nosertifikat', array('readonly'=>true)); ?></td>
            <?php echo CHtml::activeHiddenField($model, 'invtanah_id', array('readonly'=>true)); ?>
            <?php echo CHtml::activeHiddenField($model, 'barang_id', array('readonly'=>true)); ?>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'harga perolehan',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invtanah_harga', array('readonly'=>true,'class'=>'number')); ?></td>
            
            <td><?php echo CHtml::activeLabel($model, 'nilai residu',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invtanah_nilairesidu', array('readonly'=>true,'class'=>'number')); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'umurekonomis',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'umurekonomis', array('readonly'=>true, 'class'=>'span1')).' Bulan'; ?></td>
            
            <td><?php echo CHtml::activeLabel($model, 'tanggal diterima barang',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invtanah_tglguna', array('readonly'=>true)); ?></td>
        </tr> 
        <tr>
            <td><?php echo CHtml::activeLabel($model, 'invtanah_alamat',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invtanah_alamat', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($model, 'invtanah_luas',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($model, 'invtanah_luas', array('readonly'=>true,'class'=>'span1 number')); ?></td>
        </tr>
    </table>
</fieldset>
<?php
    if($model->tipepenghapusan == "pemusnahan"){
        $judul = "Pemusnahan Aktiva";
        $form_hargajual = null;
        $label_hargajual = null;
        $label = "Kerugian";
    }else{
        $judul = "Penjualan Aktiva";
        $label_hargajual = CHtml::activeLabel($model, 'hargajualaktiva',array('class'=>'control-label'));
        $form_hargajual = CHtml::activeTextField($model, 'hargajualaktiva', array('class'=>'number','onBlur'=>'hitungPenjualan();'));
        $label = "Untung / Rugi";
    }
?>
<fieldset class="">
    <legend class="rim" style="width:452px;"><?php echo $judul; ?></legend>
    <div class="control-group ">
	    <?php echo $form->labelEx($model, 'tglpenghapusan', array('class' => 'control-label')); ?>
	    <div class="controls">
	        <?php $this->widget('MyDateTimePicker',array(
	            'model'=>$model,
	            'attribute'=>'tglpenghapusan',
	            'mode'=>'date',
	            'options'=> array(
	                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
	            ),
	            'htmlOptions'=>array(
	                'onkeypress'=>"return $(this).focusNextInputField(event)",
	                'class'=>'dtPicker3',
                    'onChange'=>'hitungSusut()',
	            ),
	        )); ?> 
	    </div>
	</div>
    <table class="table table-condensed">
    	<tr>
    		<td><?php echo CHtml::activeLabel($model, 'Aktiva Tetap',array('class'=>'control-label')); ?></td>
    		<td><?php echo CHtml::activeTextField($model, 'invtanah_harga', array('readonly'=>true,'class'=>'number')); ?></td>
    	</tr>
    	<tr>
    		<td><?php //echo CHtml::activeLabel($model, 'akumulasi penyusutan',array('class'=>'control-label')); ?></td>
    		<td><?php //echo CHtml::activeTextField($model, 'invtanah_akumsusut', array('readonly'=>true,'class'=>'number')); ?></td>
    	</tr>
    	<tr>
    		<td><?php echo CHtml::activeLabel($model, $label,array('class'=>'control-label')); ?></td>
    		<td><?php echo CHtml::activeTextField($model, 'kerugian', array('readonly'=>true,'class'=>'number')); ?></td>
    	</tr>
        <tr>
            <td><?php echo $label_hargajual; ?></td>
            <td><?php echo $form_hargajual; ?></td>
        </tr>
    </table>
<fieldset>
<?php
if($model->tipepenghapusan == "pemusnahan"){
	$this->renderPartial('manajemenAsset.views.invperalatanT.rekening._formRekening',
	    array(
	        'form'=>$form,
	        'modRekenings'=>$modRekenings,
	    )
	);
}
?>
<div class="form-actions">
    <?php
        if(!$model->isNewRecord)
            echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','id'=>'btn_simpan')); 
        else
            echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','id'=>'btn_simpan','disabled'=>true)); 
    ?>
    <?php 
        $content = $this->renderPartial('rawatJalan.views.tips.tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
    ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	setJurnal(8);
    function setJurnal(jenispengeluaran_id){     
    	var aktivatetap = $('#GUInvgedungT_invtanah_harga').val();
        var akumsusut 	= $('#GUInvgedungT_invtanah_akumsusut').val();
        var kerugian	= $('#GUInvgedungT_kerugian').val();  

        setTimeout(function(){
            getDataManajemenAset(jenispengeluaran_id,akumsusut,kerugian,aktivatetap);
        },500);
    }

    function hitungPenjualan()
    {
        var aktivatetap = unformatNumber($('#GUInvtanahT_invtanah_harga').val());
        var akumsusut = unformatNumber($('#GUInvtanahT_invtanah_akumsusut').val());

        var sisa = aktivatetap - akumsusut;
        var hargajual = unformatNumber($('#GUInvtanahT_hargajualaktiva').val());
        var untungrugi = hargajual - sisa;
        $('#GUInvtanahT_kerugian').val(untungrugi);

    }

    function unformatNumbers(){
        $('.number').each(function(){this.value = unformatNumber(this.value)});
    }

</script>