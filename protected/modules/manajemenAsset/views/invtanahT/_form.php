<?php
$this->widget('bootstrap.widgets.BootAlert');

?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'guinvtanah-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
    <fieldset>
    <legend class="rim">Transaksi Inventarisasi Tanah</legend>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

            <?php echo $form->errorSummary($model); ?>
                <table>
            <tr><td>
            <?php echo $form->dropDownListRow($model,'pemilikbarang_id',CHtml::listData(PemilikbarangM::model()->findAll(), 'pemilikbarang_id', 'pemilikbarang_nama'),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --', 'onChange'=>"addKodeRegister(this,'pemilikbarang');")); ?>
            <?php echo $form->hiddenField($modBarang,'barang_id'); ?>
            <?php echo $form->hiddenField($model,'barang_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->dropDownListRow($model,'asalaset_id',CHtml::listData(AsalasetM::model()->findAll(), 'asalaset_id', 'asalaset_nama'),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --')); ?>
            <?php echo $form->dropDownListRow($model,'lokasi_id',CHtml::listData(LokasiasetM::model()->findAll(), 'lokasi_id', 'lokasiaset_namalokasi'),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --', 'onChange'=>"addKodeRegister(this,'lokasiaset');")); ?>
            <?php echo $form->textFieldRow($model,'invtanah_kode',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'invtanah_noregister',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'invtanah_namabrg',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'invtanah_luas',array('class'=>'span2 ', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
            <?php echo $form->textFieldRow($model,'invtanah_thnpengadaan',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>5)); ?>
            <?php echo $form->hiddenField($model,'terimapersdetail_id',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <div class="control-group ">
                        <?php echo $form->labelEx($model,'invtanah_tglguna', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'invtanah_tglguna',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                        //
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                            <?php echo $form->error($model, 'invtanah_tglguna'); ?>
                        </div>
                    </div>
            <?php echo $form->textFieldRow($model,'invtanah_status',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                </td>
                <td>
            
            <?php echo $form->textAreaRow($model,'invtanah_alamat',array('rows'=>5, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
            
            <div class="control-group ">
                        <?php echo $form->labelEx($model,'invtanah_tglsertifikat', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'invtanah_tglsertifikat',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                        //
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                            <?php echo $form->error($model, 'invtanah_tglsertifikat'); ?>
                        </div>
                    </div>
            <?php echo $form->textFieldRow($model,'invtanah_nosertifikat',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'invtanah_penggunaan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'invtanah_harga',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'invtanah_ket',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
        
            <div class="control-group ">
                <?php echo $form->labelEx($model,'umurekonomis', array('class'=>'control-label')) ?>
                <div class="controls">

                <?php echo $form->textField($model,'umurekonomis', array('class'=>'span1 numbersOnly','onkeypress'=>"return $(this).focusNextInputField(event)", )).' bulan '; ?> 
                </div>
            </div>
        </td>
            <?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php // echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php // echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </table>
                </fieldset>	
    <div class="form-actions">
        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                Yii::app()->createUrl($this->module->id.'/'.invtanahT.'/admin'), 
                array('class'=>'btn btn-danger',
                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
        <?php 
            if(!$model->isNewRecord){
                echo CHtml::htmlButton(Yii::t('mds','{icon} Print Label',array('{icon}'=>'<i class="icon-print icon-white"></i>')),
                    array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
            }
        ?>
        <?php $content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); ?>
	</div>

<?php $this->endWidget(); ?>
<?php
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/printLabel', array('idTanah'=>$model->invtanah_id));
$urlKodeRegistrasi = Yii::app()->createAbsoluteUrl($this->module->id.'/InvperalatanT/getKodeRegistrasiNonMutasi');
$kodeInventarisasi = strtolower($this->id);
// $idTagUmurThn = CHtml::activeId($modPasien,'thn');
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}

function addKodeRegister(kode,komponen)
{
    var kode = $(kode).val();
    var kodeRegistrasi_lama = $('#GUInvtanahT_invtanah_noregister').val();
    var kodeInventarisasi = '${kodeInventarisasi}';
    var kodeBarang = $('#MAAssetV_barang_kode').val();
    var tahun = $('#MAAssetV_barang_thnbeli').val();
    var barangId = $('#MAAssetV_barang_id').val();

    $.post('${urlKodeRegistrasi}', {idKomponen:kode, komponen:komponen, kodereg:kodeRegistrasi_lama, kodeinventarisasi:kodeInventarisasi, kodebarang:kodeBarang, tahun:tahun, barangid:barangId}, function(data){
        
        var kodeRegistrasi = data.kode;
        $('#GUInvtanahT_invtanah_noregister').val(kodeRegistrasi);
        $('#GUInvtanahT_invtanah_kode').val(data.urutan);
        
    }, 'json');

}

JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);   
?> 

<?php JS;
Yii::app()->clientScript->registerScript('jsBarang',$jscript, CClientScript::POS_BEGIN);

$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);?>

