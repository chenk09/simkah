<?php
$this->breadcrumbs=array(
	'Guinvtanah Ts'=>array('index'),
	'Manage',
);

$arrMenu = array();
                //(Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Inventarisasi Tanah ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
                //array_push($arrMenu,array('label'=>Yii::t('mds','List').' GUInvtanahT', 'icon'=>'list', 'url'=>array('index'))) ;
                //(Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Inventarisasi Tanah', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('guinvtanah-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>
<legend class='rim2'>Informasi Inventaris Tanah</legend>
<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'guinvtanah-t-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'invtanah_id',
		array(
                        'header'=>'ID',
                        'value'=>'$data->invtanah_id',
                        'filter'=>false,
                ),
				array(
                        'name'=>'pemilikbarang_id',
                        'filter'=>  CHtml::listData($model->PemilikItems, 'pemilikbarang_id', 'pemilikbarang_nama'),
                        'value'=>'$data->pemilik->pemilikbarang_nama',
                ),
				array(
                        'name'=>'barang_id',
                        'filter'=>  CHtml::listData($model->BarangItems, 'barang_id', 'barang_nama'),
                        'value'=>'$data->barang->barang_nama',
                
                ),
				array(
                        'name'=>'asalaset_id',
                        'filter'=>  CHtml::listData($model->AsalAsetItems, 'asalaset_id', 'asalaset_nama'),
                        'value'=>'$data->asal->asalaset_nama',
                ),
				array(
                        'name'=>'lokasi_id',
                        'filter'=>  CHtml::listData($model->LokasiAsetItems, 'lokasi_id', 'lokasiaset_namalokasi'),
                        'value'=>'$data->lokasi->lokasiaset_namalokasi',
                ),
                array(
                    'header'=>'Tahun/<br/>Tanggal',
                    'type'=>'raw',
                    'value'=>'$data->tahunNama',
                ),
                array(
                    'header'=>'No Sertifikat/<br/>Tanggal',
                    'type'=>'raw',
                    'value'=>'$data->sertifikat',
                ),

		'invtanah_kode',
		
		//'invtanah_noregister',
		'invtanah_namabrg',
		'invtanah_luas',
            
		//'invtanah_thnpengadaan',
		//'invtanah_tglguna',
		'invtanah_alamat',
		'invtanah_status',
		//'invtanah_tglsertifikat',
		//'invtanah_nosertifikat',
		'invtanah_penggunaan',
		'invtanah_harga',
		//'invtanah_ket',
		//'create_time',
		//'update_time',
		//'create_loginpemakai_id',
		//'update_loginpemakai_id',
		//'create_ruangan',
		
		array(
            'header'=>'Print Label',
            'type'=>'raw',
            'value'=>'CHtml::link("<i class=icon-print></i>", "#", 
              array("rel"=>"tooltip",
                "title"=>"Print Inventaris Tanah",
                "onclick"=>"printLabel($data->invtanah_id)"
              ))',
            'htmlOptions'=>array(
                'style'=>'text-align: center',
            )
        ),  
		array(
                        'header'=>Yii::t('zii','Update'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{update}',
                        'buttons'=>array(
                            'update' => array (
                                          'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
                                        ),
                         ),
		),
        array(
            'header'=>'Penjualan Aset',
            'type'=>'raw',
            'value'=>'CHtml::link("<i class=icon-share></i>", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/penghapusanAset",array("id"=>"$data->invtanah_id", "tipehapus"=>"2")), 
            array("class"=>"", 
                  "target"=>"iframePenghapusanAset",
                  "onclick"=>"$(\"#dialogPenghapusanAset\").dialog(\"open\");",
                  "rel"=>"tooltip",
                  "title"=>"Klik Penjualan Aset",
            ))', 
            'htmlOptions'=>array(
                'style'=>'text-align: center',
            )
        ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php 
 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
$content = $this->renderPartial('../tips/master',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrintLabel=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printInventaris');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#guinvtanah-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
function printLabel(id)
{
    window.open("${urlPrintLabel}/"+"&id="+id,"",'location=_new, width=600px, height=250px, scrollbars=yes');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPenghapusanAset',
    'options'=>array(
        'title'=>'Penjualan Aset',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>1000,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframePenghapusanAset" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>