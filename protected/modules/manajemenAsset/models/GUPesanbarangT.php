<?php

class GUPesanbarangT extends PesanbarangT {
    

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function getNamaModel(){
        return __CLASS__;
    }
    
    public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->addBetweenCondition('date(tglpesanbarang)', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('pesanbarang_id',$this->pesanbarang_id);
		$criteria->compare('mutasibrg_id',$this->mutasibrg_id);
		$criteria->compare('LOWER(nopemesanan)',strtolower($this->nopemesanan),true);
//		$criteria->compare('LOWER(tglpesanbarang)',strtolower($this->tglpesanbarang),true);
		$criteria->compare('LOWER(tglmintadikirim)',strtolower($this->tglmintadikirim),true);
		$criteria->compare('ruanganpemesan_id',Yii::app()->user->getState('ruangan_id'));
		$criteria->compare('LOWER(keterangan_pesan)',strtolower($this->keterangan_pesan),true);
		$criteria->compare('pegpemesan_id',$this->pegpemesan_id);
		$criteria->compare('pegmengetahui_id',$this->pegmengetahui_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
    public function searchInformasiGudang()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->addBetweenCondition('date(tglpesanbarang)', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('pesanbarang_id',$this->pesanbarang_id);
		$criteria->compare('mutasibrg_id',$this->mutasibrg_id);
		$criteria->compare('LOWER(nopemesanan)',strtolower($this->nopemesanan),true);
//		$criteria->compare('LOWER(tglpesanbarang)',strtolower($this->tglpesanbarang),true);
		$criteria->compare('LOWER(tglmintadikirim)',strtolower($this->tglmintadikirim),true);
		$criteria->compare('ruanganpemesan_id',$this->ruanganpemesan_id);
		$criteria->compare('LOWER(keterangan_pesan)',strtolower($this->keterangan_pesan),true);
		$criteria->compare('pegpemesan_id',$this->pegpemesan_id);
		$criteria->compare('pegmengetahui_id',$this->pegmengetahui_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}