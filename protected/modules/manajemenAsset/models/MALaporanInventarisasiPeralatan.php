<?php

class MALaporanInventarisasiPeralatan extends LaporaninventarisasiperalatanV{
    
	public $tick;
	
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }


    public function searchInformasi()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invperalatan_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
//        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('SUBSTR(no_register,8,2)','04',true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
        $criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
        $criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
        $criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
        $criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
        $criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
        $criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
        $criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
        $criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
        $criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
        $criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
        $criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }   

    public function searchPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

         $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invperalatan_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
//        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('SUBSTR(no_register,8,2)','04',true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
        $criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
        $criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
        $criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
        $criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
        $criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
        $criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
        $criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
        $criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
        $criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
        $criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
        $criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);


        $criteria->limit = -1;
                
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
    }


    public function searchInventarisasiPeralatangrafik()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

         $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invperalatan_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
//        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('SUBSTR(no_register,8,2)','04',true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
        $criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
        $criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
        $criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
        $criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
        $criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
        $criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
        $criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
        $criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
        $criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
        $criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
        $criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    
    public function searchInformasiNonMedis()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invperalatan_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
//        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('SUBSTR(no_register,8,2)','05',true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
        $criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
        $criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
        $criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
        $criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
        $criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
        $criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
        $criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
        $criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
        $criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
        $criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
        $criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);

		
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    
    public function searchPrintNonMedis()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

         $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invperalatan_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
//        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('SUBSTR(no_register,8,2)','05',true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
        $criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
        $criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
        $criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
        $criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
        $criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
        $criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
        $criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
        $criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
        $criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
        $criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
        $criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);


        $criteria->limit = -1;
                
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
    }
    
    public function searchInventarisasiPeralatangrafikNonMedis()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

         $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invperalatan_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
//        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('SUBSTR(no_register,8,2)','05',true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
        $criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
        $criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
        $criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
        $criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
        $criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
        $criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
        $criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
        $criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
        $criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
        $criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
        $criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);
        $criteria->limit = -1;
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    
    public function searchInformasiKendaraan()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invperalatan_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
//        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('SUBSTR(no_register,8,2)','03',true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
        $criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
        $criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
        $criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
        $criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
        $criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
        $criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
        $criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
        $criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
        $criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
        $criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
        $criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    
    public function searchPrintKendaraan()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

         $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invperalatan_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
//        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('SUBSTR(no_register,8,2)','03',true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
        $criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
        $criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
        $criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
        $criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
        $criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
        $criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
        $criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
        $criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
        $criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
        $criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
        $criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);


        $criteria->limit = -1;
                
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
    }
    
    public function searchInventarisasiPeralatangrafikKendaraan()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

         $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invperalatan_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
//        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('SUBSTR(no_register,8,2)','03',true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
        $criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
        $criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
        $criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
        $criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
        $criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
        $criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
        $criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
        $criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
        $criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
        $criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
        $criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
        
        public function getNamaModel(){
            return __CLASS__;
        }
        

}

?>
