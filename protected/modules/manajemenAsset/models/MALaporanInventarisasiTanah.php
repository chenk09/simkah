<?php

class MALaporanInventarisasiTanah extends LaporaninventarisasitanahV{
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }


    public function searchInformasi()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invtanah_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('invtanah_namabrg',$this->invtanah_namabrg,true);
        $criteria->compare('invtanah_luas',$this->invtanah_luas,true);
        $criteria->compare('invtanah_thnpengadaan',$this->invtanah_thnpengadaan,true);
        $criteria->compare('invtanah_tglguna',$this->invtanah_tglguna,true);
        $criteria->compare('invtanah_alamat',$this->invtanah_alamat,true);
        $criteria->compare('invtanah_status',$this->invtanah_status,true);
        $criteria->compare('invtanah_tglsertifikat',$this->invtanah_tglsertifikat,true);
        $criteria->compare('invtanah_nosertifikat',$this->invtanah_nosertifikat,true);
        $criteria->compare('invtanah_penggunaan',$this->invtanah_penggunaan,true);
        $criteria->compare('invtanah_harga',$this->invtanah_harga);
        $criteria->compare('invtanah_ket',$this->invtanah_ket,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function searchPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invtanah_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('invtanah_namabrg',$this->invtanah_namabrg,true);
        $criteria->compare('invtanah_luas',$this->invtanah_luas,true);
        $criteria->compare('invtanah_thnpengadaan',$this->invtanah_thnpengadaan,true);
        $criteria->compare('invtanah_tglguna',$this->invtanah_tglguna,true);
        $criteria->compare('invtanah_alamat',$this->invtanah_alamat,true);
        $criteria->compare('invtanah_status',$this->invtanah_status,true);
        $criteria->compare('invtanah_tglsertifikat',$this->invtanah_tglsertifikat,true);
        $criteria->compare('invtanah_nosertifikat',$this->invtanah_nosertifikat,true);
        $criteria->compare('invtanah_penggunaan',$this->invtanah_penggunaan,true);
        $criteria->compare('invtanah_harga',$this->invtanah_harga);
        $criteria->compare('invtanah_ket',$this->invtanah_ket,true);

        $criteria->limit = -1;
                
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
    }


    public function searchInventarisasiTanahgrafik()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('invtanah_tglguna',$this->tglAwal,$this->tglAkhir,true);
        $criteria->compare('inventaris_id',$this->inventaris_id);
        $criteria->compare('inventaris_kode',$this->inventaris_kode,true);
        $criteria->compare('no_register',$this->no_register,true);
        $criteria->compare('tgl_register',$this->tgl_register,true);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('barang_satuan',$this->barang_satuan,true);
        $criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
        $criteria->compare('barang_image',$this->barang_image,true);
        $criteria->compare('barang_harga',$this->barang_harga);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('asalaset_nama',$this->asalaset_nama,true);
        $criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
        $criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
        $criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
        $criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
        $criteria->compare('invtanah_namabrg',$this->invtanah_namabrg,true);
        $criteria->compare('invtanah_luas',$this->invtanah_luas,true);
        $criteria->compare('invtanah_thnpengadaan',$this->invtanah_thnpengadaan,true);
        $criteria->compare('invtanah_tglguna',$this->invtanah_tglguna,true);
        $criteria->compare('invtanah_alamat',$this->invtanah_alamat,true);
        $criteria->compare('invtanah_status',$this->invtanah_status,true);
        $criteria->compare('invtanah_tglsertifikat',$this->invtanah_tglsertifikat,true);
        $criteria->compare('invtanah_nosertifikat',$this->invtanah_nosertifikat,true);
        $criteria->compare('invtanah_penggunaan',$this->invtanah_penggunaan,true);
        $criteria->compare('invtanah_harga',$this->invtanah_harga);
        $criteria->compare('invtanah_ket',$this->invtanah_ket,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
        
        public function getNamaModel(){
            return __CLASS__;
        }
        

}

?>
