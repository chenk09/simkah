
<?php

class InvtanahTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	// public function accessRules()
	// {
	// 	return array(
	// 		array('allow',  // allow all users to perform 'index' and 'view' actions
	// 			'actions'=>array('index','view'),
	// 			'users'=>array('@'),
	// 		),
	// 		array('allow', // allow authenticated user to perform 'create' and 'update' actions
	// 			'actions'=>array('create','update','print'),
	// 			'users'=>array('@'),
	// 		),
	// 		array('allow', // allow admin user to perform 'admin' and 'delete' actions
	// 			'actions'=>array('admin','delete','RemoveTemporary'),
	// 			'users'=>array('@'),
	// 		),
	// 		array('deny',  // deny all users
	// 			'users'=>array('*'),
	// 		),
	// 	);
	// }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id = null)
	{
        
		$model=new GUInvtanahT;
        $modBarang = new MAAssetV;
        $format = new CustomFormat();

		if(isset($_POST['GUInvtanahT']))
		{
			$model->attributes=$_POST['GUInvtanahT']; 
			$model->barang_id = $_POST['MAAssetV']['barang_id'];
			$model->invtanah_tglguna = $format->formatDateMediumForDB($_POST['GUInvtanahT']['invtanah_tglguna']);
			$model->invtanah_tglsertifikat = $format->formatDateMediumForDB($_POST['GUInvtanahT']['invtanah_tglsertifikat']);
			$model->tglpenghapusan = date('Y-m-d H:i:s');
			if($model->validate()){
				if($model->save()){
	                BarangM::model()->updateByPk($model->barang_id, array('barang_statusregister'=>true));                            
	                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
	                $this->redirect(array('create','id'=>$model->invtanah_id));
	                $model->isNewRecord = false;
	            }
	        }
		}

		$this->render('create',array(
			'model'=>$model,'modBarang'=>$modBarang,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
                $modBarang = $this->loadModelBarang($model->barang_id);
                $data['pemilikbarang_nama'] = $model->pemilik->pemilikbarang_nama;
                $dataAsalAset['asalaset_nama'] = $model->asal->asalaset_nama;
                $dataLokasi['lokasiaset_namalokasi'] = $model->lokasi->lokasiaset_namalokasi;

//                $modBarang->pemilikbarang_nama = $model->pemilik->pemilikbarang_nama;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GUInvtanahT']))
		{
			$model->attributes=$_POST['GUInvtanahT'];
			if($model->save()){
                                 BarangM::model()->updateByPk($model->barang_id, array('barang_statusregister'=>true));
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				//$this->redirect(array('admin','id'=>$model->invtanah_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,'modBarang'=>$modBarang, 'data'=>$data ,'dataAsalAset'=>$dataAsalAset ,'dataLokasi'=>$dataLokasi
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = $this->loadModel($id);
                        BarangM::model()->updateByPk($model->barang_id, array('barang_statusregister'=>false));
                        $this->loadModel($id)->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('GUInvtanahT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	 public function actionInformasi()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUInvtanahT('search');
//		$model->unsetAttributes();  // clear any default values
                $model->tglAwal = date('Y-m-d');
                $model->tglAkhir = date('Y-m-d');
                $format = new CustomFormat();
                
		 if (isset($_GET['GUInvtanahT'])) {
            $model->attributes = $_GET['GUInvtanahT'];
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['GUInvtanahT']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['GUInvtanahT']['tglAkhir']);
        }

		$this->render('informasi',array(
			'model'=>$model,
		));
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUInvtanahT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GUInvtanahT']))
			$model->attributes=$_GET['GUInvtanahT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GUInvtanahT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function loadModelBarang($id){
            $model=BarangM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
        }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='guinvtanah-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new GUInvtanahT;
            $model->attributes=$_REQUEST['GUInvtanahT'];
            $judulLaporan='Data Inventarisasi Tanah';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        /**
         * Module   : manajemenAsset/InvtanahT/Create&modulId=47
         * @author  : Miranitha Fasha
         * Tanggal  : 05-05-2014 /d-m-y
         * Issue    : EHJ-1634
         * Desc     : Menambahkan print label setelah simpan.
         * Action   : actionPrintLabel
         */
        
        public function actionPrintLabel($idTanah, $caraPrint){
            $judulLaporan = '';
            $model = GUInvtanahT::model()->findByPk($idTanah); 
            $modTerimaDet = TerimapersdetailT::model()->findByPk($model->terimapersdetail_id);
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('PrintLabel',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'model'=>$model,
                                            'modTerimaDet'=>$modTerimaDet
                ));
//                
//                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
//                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
//                $mpdf = new MyPDF('',$ukuranKertasPDF); 
//                $mpdf->useOddEven = 2;  
//                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
//                $mpdf->WriteHTML($stylesheet,1);  
//                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
//                $mpdf->WriteHTML(
//                $this->renderPartial('PrintLabel',array('judulLaporan'=>$judulLaporan,
//                                            'caraPrint'=>$caraPrint,
//                                            'model'=>$model,
//                        ),true
//                    )
//                );
//                $mpdf->Output();
            }
        }    

    /**
     * Module   : manajemenAsset/InvTanahT/pemusnahanaset&modulId=47
     * @author  : Hardi
     * Tanggal  : 04-08-2014
     * Issue    : EHJ-2585
     */
    
    public function actionPenghapusanAset($id=null, $tipehapus=null){
        $this->layout = '//layouts/frameDialog';
        $model = GUInvtanahT::model()->findByPk($id);
        // $model->invtanah_akumsusut = isset($model->invtanah_akumsusut) ? $model->invtanah_akumsusut : 0;
        
        $model->tglpenghapusan = date('Y-m-d H:i:s');

        if($tipehapus=='1'){
    		$model->tipepenghapusan = "pemusnahan";
    		$model->kerugian = $model->invtanah_harga;
    	}
    	else{
    		$model->tipepenghapusan = "penjualan";
    		$model->kerugian = 0;
    	}
        if(isset($_POST['GUInvtanahT']))
		{
        	$format = new CustomFormat();
        	$model->tglpenghapusan = $format->formatDateMediumForDB($_POST['GUInvtanahT']['tglpenghapusan']); 
        	$model->invtanah_tglguna = $format->formatDateMediumForDB($model->invtanah_tglguna);
        	$model->create_time = $format->formatDatetimeMediumForDB($model->create_time);
        	$model->update_time = $format->formatDatetimeMediumForDB($model->update_time);

        	if($tipehapus=='2'){
        		if($_POST['GUInvtanahT']['kerugian'] < 0 ){
        			$model->kerugian = $_POST['GUInvtanahT']['kerugian'];
        			$model->keuntungan = 0;
        		}else{
        			$model->keuntungan = $_POST['GUInvtanahT']['kerugian'];
        			$model->kerugian = 0;
        		}
        	}
        	$model->hargajualaktiva = $_POST['GUInvtanahT']['hargajualaktiva'];  

        	if($model->validate()){
        		$model->save();

        		if(isset($_POST['JenispenerimaanrekeningV'])){
	        		if($tipehapus=='1'){
		        		//=========== Save Jurnal Rekening =================
		                $modJurnalRekening = $this->saveJurnalRekening($model, $_POST['GUInvtanahT']);
		                $noUrut = 0;
		                foreach($_POST['JenispenerimaanrekeningV'] AS $i => $post){
		                    $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
		                    $noUrut ++;
		                }
		                //==================================================
		            }
		        }
		        $model->isNewRecord = true;
        		Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Penjualan berhasil dilakukan.');
        	}
        }

		$this->render('penghapusan',array(
			'model'=>$model,
		));
    }    

    public function actionprintInventaris($id=null)
    {
        $model = GUInvtanahT::model()->findByPk($id);
        $judulLaporan = 'Print Nomor Register Inventaris Tanah';

        $data['type'] = "Print";
        $caraPrint = "PRINT";
        $target = 'PrintLabel';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = null;

        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } 
    }
            
}
