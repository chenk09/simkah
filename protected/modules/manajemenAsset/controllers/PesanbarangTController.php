
<?php

class PesanbarangTController extends SBaseController
{
        private $_valid;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'informasi', 'detailPesanBarang', 'informasiGudang'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex($id=null)
	{
                
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUPesanbarangT;
                $insert_notifikasi = new MyFunction();
                $instalasi_id = Yii::app()->user->getState('instalasi_id');
                $model->nopemesanan = Generator::noPemesananBarang($instalasi_id);
                $model->tglpesanbarang = date('d M Y H:i:s');
                $modLogin = LoginpemakaiK::model()->findByAttributes(array('loginpemakai_id'=>Yii::app()->user->id));
                $model->pegpemesan_id = $modLogin->pegawai_id;
                $model->pegpemesan_nama = $modLogin->pegawai->nama_pegawai;
                $model->ruanganpemesan_id = Yii::app()->user->getState('ruangan_id');
                $model->instalasi_id = $model->ruanganpemesan->instalasi->instalasi_id;
                if (isset($id)){
                    $modelPesan = GUPesanbarangT::model()->findByPk($id);
                    if (count($modelPesan) == 1){
                        $model = $modelPesan;
                        $model->instalasi_id = $model->ruanganpemesan->instalasi->instalasi_id;
                        $model->pegpemesan_nama = $model->pegawaipemesan->nama_pegawai;
                        $model->pegmengetahui_nama = $model->pegawaimengetahui->nama_pegawai;
                        $modDetails = GUPesanbarangdetailT::model()->findAll('pesanbarang_id = '.$id);
                    }
                }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GUPesanbarangT']))
		{
			$model->attributes=$_POST['GUPesanbarangT'];
                        if (count($_POST['PesanbarangdetailT']) > 0){
                            $modDetails = $this->validasiTabularInput($_POST['PesanbarangdetailT'], $model);
                            if ($model->validate()){
                                $transaction = Yii::app()->db->beginTransaction();
                                try{
                                    $success = true;
                                    if($model->save()){
                                        $modDetails = $this->validasiTabularInput($_POST['PesanbarangdetailT'], $model);
                                        foreach ($modDetails as $i=>$data){
                                            if ($data->qty_pesan > 0){
                                                if ($data->save()){

                                                }
                                                else{
                                                    $success = false;
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        $success = false;
                                    }
                                    
                                    $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                                    $params['create_time'] = date( 'Y-m-d H:i:s');
                                    $params['create_loginpemakai_id'] = Yii::app()->user->id;
                                    $params['instalasi_id'] = 14;
                                    $params['modul_id'] = 17;
                                    $ruangan = RuanganM::model()->findByPk($model->ruanganpemesan_id);
                                    $params['isinotifikasi'] = $ruangan->ruangan_nama . '-' . $model->nopemesanan;
                                    $params['create_ruangan'] = 32;
                                    $params['judulnotifikasi'] = 'Pesan Barang';
                                    $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                                    
                                    if ($success == true){
                                        $transaction->commit();
                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                        $this->redirect(array('index','id'=>$model->pesanbarang_id));
                                    }
                                    else{
                                        $transaction->rollback();
                                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                                    }
                                }
                                catch (Exception $ex){
                                     $transaction->rollback();
                                     Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                                }
                            }
                        }
                        else{
                            $model->validate();
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
                        }
                        
		}
                
		$this->render('manajemenAsset.views.pesanbarangT.index',array(
			'model'=>$model, 'modDetail'=>$modDetails,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GUPesanbarangT']))
		{
			$model->attributes=$_POST['GUPesanbarangT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pesanbarang_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
//	public function actionIndex()
//	{
//		$dataProvider=new CActiveDataProvider('GUPesanbarangT');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
//	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUPesanbarangT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GUPesanbarangT']))
			$model->attributes=$_GET['GUPesanbarangT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GUPesanbarangT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gupesanbarang-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
//        public function actionPrint()
//        {
//            $model= new GUPesanbarangT;
//            $model->attributes=$_REQUEST['GUPesanbarangT'];
//            $judulLaporan='Data Pemesanan Barang';
//            $caraPrint=$_REQUEST['caraPrint'];
//            if($caraPrint=='PRINT') {
//                $this->layout='//layouts/printWindows';
//                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
//            }
//            else if($caraPrint=='EXCEL') {
//                $this->layout='//layouts/printExcel';
//                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
//            }
//            else if($_REQUEST['caraPrint']=='PDF') {
//                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
//                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
//                $mpdf = new MyPDF('',$ukuranKertasPDF); 
//                $mpdf->useOddEven = 2;  
//                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
//                $mpdf->WriteHTML($stylesheet,1);  
//                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
//                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
//                $mpdf->Output();
//            }                       
//        }
        
        //$data = diisi POST request yang ingin d validasi; tipe:array();
        //$model = Nama model yang akan divalidate
        protected function validasiTabularInput($datas, $model){ 
            $valid = true;
            foreach ($datas as $i=>$data){
                $modDetail[$i] = new PesanbarangdetailT();
                $modDetail[$i]->attributes = $data;
                $modDetail[$i]->pesanbarang_id = $model->pesanbarang_id;
                $valid = $modDetail[$i]->validate() && $valid;
                
            }
            $this->_valid = $valid;
            return $modDetail;
        }
        
        public function actionInformasi()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUPesanbarangT('search');
//		$model->unsetAttributes();  // clear any default values
                $model->tglAwal = date('d M Y H:i:s');
                $model->tglAkhir = date('d M Y H:i:s');
		if(isset($_GET['GUPesanbarangT'])){
                    $model->attributes=$_GET['GUPesanbarangT'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                }

        if (Yii::app()->request->isAjaxRequest) {
                    echo $this->renderPartial('_table', array('model'=>$model),true);
                }else{
                   $this->render('manajemenAsset.views.pesanbarangT.informasi',array(
					'model'=>$model,
				));
                }


	}
        
        public function actionInformasiGudang()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUPesanbarangT('search');
//		$model->unsetAttributes();  // clear any default values
                $model->tglAwal = date('d M Y H:i:s');
                $model->tglAkhir = date('d M Y H:i:s');
		if(isset($_GET['GUPesanbarangT'])){
                    $model->attributes=$_GET['GUPesanbarangT'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                }

		$this->render('manajemenAsset.views.pesanbarangT.informasiGudang',array(
			'model'=>$model,
		));
	}
        
        public function actionDetailPesanBarang($id){
            $this->layout ='//layouts/frameDialog';
            $modPesan = PesanbarangT::model()->findByPk($id);
            $modDetailPesan = PesanbarangdetailT::model()->findAllByAttributes(array('pesanbarang_id'=>$modPesan->pesanbarang_id));
            $this->render('manajemenAsset.views.pesanbarangT.detailInformasi', array(
                'modPesan'=>$modPesan,
                'modDetailPesan'=>$modDetailPesan,
            ));
        }
        
        public function actionPrint($id){
            $this->layout='//layouts/printWindows'; 
            $caraPrint = $_REQUEST['caraPrint'];
            $judulLaporan='Data Pemesanan Barang';
            $modPesan = PesanbarangT::model()->findByPk($id);
            $modDetailPesan = PesanbarangdetailT::model()->findAllByAttributes(array('pesanbarang_id'=>$modPesan->pesanbarang_id));
            $this->render('manajemenAsset.views.pesanbarangT.detailInformasi', array(
                'judulLaporan'=>$judulLaporan,
                'modPesan'=>$modPesan,
                'modDetailPesan'=>$modDetailPesan,
                'caraPrint'=>$caraPrint,
            ));
        }
}
