<?php

class TerimapersediaanTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'informasi', 'detailTerimaPersediaan', 'returPenerimaan'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUTerimapersediaanT;
                $instalasi_id = Yii::app()->user->getState('instalasi_id');
                $model->nopenerimaan = Generator::noPenerimaanPersediaan($instalasi_id);
                $modLogin = LoginpemakaiK::model()->findByAttributes(array('loginpemakai_id' => Yii::app()->user->id));
                $model->peg_penerima_id = $modLogin->pegawai_id;
                $model->peg_penerima_nama = $modLogin->pegawai->nama_pegawai;
                $model->ruanganpenerima_id = Yii::app()->user->getState('ruangan_id');
                $model->instalasi_id = $model->ruangan->instalasi_id;
                $model->tglterima = date('Y-m-d H:i:s');
                $model->totalharga = 0 ;
                $model->discount = 0;
                $model->biayaadministrasi = 0;
                $model->pajakpph = 0;
                $model->pajakppn =0;
                
                if (isset($_GET['id'])){
                    $id = $_GET['id'];
                    $modBeli = PembelianbarangT::model()->find('pembelianbarang_id = '.$id.' and terimapersediaan_id is null');
                    if (count($modBeli) == 1){
                        $modDetailBeli = BelibrgdetailT::model()->findAllByAttributes(array('pembelianbarang_id'=>$id));
                        $model->pembelianbarang_id = $id;
                        $model->sumberdana_id = $modBeli->sumberdana_id;
                        foreach ($modDetailBeli as $i=>$row){
                            $modDetails[$i] = new TerimapersdetailT();
                            $modDetails[$i]->attributes = $row->attributes;
                            $modDetails[$i]->jmlterima = $row->jmlbeli;
                            $modDetails[$i]->jmlbeli = $row->jmlbeli;
                            $modDetails[$i]->jmldalamkemasan = $row->barang->barang_jmldlmkemasan;
                        }
                    }
                }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GUTerimapersediaanT']))
		{
			$model->attributes=$_POST['GUTerimapersediaanT'];
			if (count($_POST['TerimapersdetailT']) > 0){
                            $modDetails = $this->validasiTabular($model, $_POST['TerimapersdetailT'], $modDetailBeli);
                            if ($model->validate()){
                                $transaction = Yii::app()->db->beginTransaction();
                                try{
                                    $success = true;
                                    if($model->save()){
                                        if (!empty($model->pembelianbarang_id)){
                                            PembelianbarangT::model()->updateByPk($model->pembelianbarang_id, array('terimapersediaan_id'=>$model->terimapersediaan_id));
                                        }
                                        $modDetails = $this->validasiTabular($model, $_POST['TerimapersdetailT'], $modDetailBeli);
                                        foreach ($modDetails as $i=>$data){
                                            if ($data->jmlterima > 0){
                                                $modInven = new InventarisasiruanganT();
                                                $modInven->ruangan_id = $model->ruanganpenerima_id;
                                                $modInven->barang_id = $data->barang_id;
                                                $modInven->tgltransaksi = date('Y-m-d H:i:s');
                                                $modInven->inventarisasi_kode = Generator::kodeTerimaPersediaan();
                                                $modInven->inventarisasi_hargabeli = $data->hargabeli;
                                                $modInven->inventarisasi_hargasatuan = $data->hargasatuan;
                                                $modInven->inventarisasi_qty_in = $data->jmlterima;
                                                $modInven->inventarisasi_qty_out = 0;
                                                $modInven->inventarisasi_qty_skrg = $data->jmlterima;
                                                $modInven->inventarisasi_keadaan = $data->kondisibarang;
                                                if ($modInven->save()){
                                                    $data->inventarisasi_id = $modInven->inventarisasi_id;
                                                    if ($data->save()){
                                                        InventarisasiruanganT::model()->updateByPk($modInven->inventarisasi_id, array('terimapersdetail_id'=>$data->terimapersdetail_id));
                                                    }
                                                    else{
                                                        $success = false;
                                                    }
                                                }
                                                else{
                                                    $success = false;
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        $success = false;
                                    }
                                    if ($success == true){
                                        $transaction->commit();
                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                        if (isset($model->pembelianbarang_id)){
                                            $this->redirect(array('index','id'=>$model->pembelianbarang_id));
                                        }
                                        else{
                                            $this->refresh();
                                        }
                                    }
                                    else{
                                        $transaction->rollback();
                                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                                    }
                                }
                                catch (Exception $ex){
                                     $transaction->rollback();
                                     Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                                }
                            }
                        }else{
                            $model->validate();
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
                        }
		}

		$this->render('index',array(
			'model'=>$model, 'modDetails'=>$modDetails, 'modBeli'=>$modBeli, 'modDetailBeli'=>$modDetailBeli,
		));
	}
        
        protected function validasiTabular($model, $data, $beli){
            $valid = true;
            foreach ($data as $i=>$row){
                $modDetails[$i] = new TerimapersdetailT();
                $modDetails[$i]->attributes = $row;
                $modDetails[$i]->terimapersediaan_id = $model->terimapersediaan_id;
                if (isset($beli)){
                    $modDetails[$i]->jmlbeli = $beli[$i]->jmlbeli;
                }
                $valid = $modDetails[$i]->validate() && $valid;
            }
            
            return $modDetails;
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GUTerimapersediaanT']))
		{
			$model->attributes=$_POST['GUTerimapersediaanT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->terimapersediaan_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
//	public function actionIndex()
//	{
//		$dataProvider=new CActiveDataProvider('GUTerimapersediaanT');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
//	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUTerimapersediaanT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GUTerimapersediaanT']))
			$model->attributes=$_GET['GUTerimapersediaanT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GUTerimapersediaanT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='guterimapersediaan-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionInformasi()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUTerimapersediaanT('search');
//		$model->unsetAttributes();  // clear any default values
                $model->tglAwal = date('Y-m-d H:i:s');
                $model->tglAkhir = date('Y-m-d H:i:s');
		if(isset($_GET['GUTerimapersediaanT'])){
                    $model->attributes=$_GET['GUTerimapersediaanT'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                }

		$this->render('informasi',array(
			'model'=>$model,
		));
	}
        
        public function actionDetailTerimaPersediaan($id){
            $this->layout ='//layout/frameDialog';
            $modTerima = TerimapersediaanT::model()->findByPk($id);
            $modDetailTerima = TerimapersdetailT::model()->findAllByAttributes(array('terimapersediaan_id'=>$modTerima->terimapersediaan_id));
            $this->render('detailInformasi', array(
                'modTerima'=>$modTerima,
                'modDetailTerima'=>$modDetailTerima,
            ));
        }
        
        public function actionPrint($id){
            $this->layout='//layouts/printWindows';
            $judulLaporan='Data Pembelian Barang';
            $modTerima = TerimapersediaanT::model()->findByPk($id);
            $modDetailTerima = TerimapersdetailT::model()->findAllByAttributes(array('terimapersediaan_id'=>$modTerima->terimapersediaan_id));
            $this->render('detailInformasi', array(
                'judulLaporan'=>$judulLaporan, 
                'modTerima'=>$modTerima,
                'modDetailTerima'=>$modDetailTerima,
            ));
        }
        
    public function actionReturPenerimaan($id){
        $this->layout = 'frameDialog';
        $model = new ReturpenerimaanT();
        $modTerima = TerimapersediaanT::model()->find('terimapersediaan_id  = '.$id.' and returpenerimaan_id is null');
        $modDetailTerima = TerimapersdetailT::model()->findAll('terimapersediaan_id = '.$id.' and retpendetail_id is null');
        if ((count($modTerima) == 1) && (count($modDetailTerima) > 0)){
            $model->tglreturterima = date('Y-m-d H:i:s');
            $model->terimapersediaan_id = $modTerima->terimapersediaan_id;
            $model->noreturterima = Generator::noReturTerima();
            $this->render('returPenerimaan', array(
                'model'=>$model,
            ));
        }
        else{
            echo 'Barang telah dibatal mutasikan';
        }
        if (isset($_POST['BatalmutasibrgT'])){
            $modBatals = $this->validateTableBatal($_POST['BatalmutasibrgT']);
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $success = true;
                $modBatals = $this->validateTableBatal($_POST['BatalmutasibrgT']);
                foreach ($modBatals as $i => $data) {
                    if ($data->qty_batal > 0){
                        $modInventaris = InventarisasiruanganT::model()->findByAttributes(array('barang_id'=>$data->barang_id),array('order'=>'tgltransaksi', 'limit'=>1));
                        if ($data->save()) {
                            InventarisasiruanganT::kembalikanStok($data->qty_batal, $data->barang_id);
                            MutasibrgdetailT::model()->updateByPk($_POST['BatalmutasibrgT']['barang_id'][$i]['mutasibrgdetail_id'], array('batalmutasibrg_id'=>$data->batalmutasibrg_id));
                            InventarisasiruanganT::model()->updateAll(array('batalmutasibrg_id'=>$data->batalmutasibrg_id),'mutasibrgdetail_id = '.$_POST['BatalmutasibrgT']['barang_id'][$i]['mutasibrgdetail_id'].' and barang_id = '.$data->barang_id);
                        } else {
                            $success = false;
                        }
                    }
                }

                if ($success == true) {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                    $this->refresh();
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($ex, true));
            }
        }
        
        
    }
        
        
}
