
<?php

class InvperalatanTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	protected $successSave = true;
    protected $pesan = "succes";
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id=null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUInvperalatanT;
                $modBarang = new SABarangM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GUInvperalatanT']))
		{
			$model->attributes=$_POST['GUInvperalatanT'];
			$model->barang_id	= $_POST['SABarangM']['barang_id'];
			$model->umurekonomis = $_POST['GUInvperalatanT']['invperalatan_umurekonomis'];
			$model->tglpenghapusan = date('Y-m-d H:i:s');
			if($model->save()){
                            BarangM::model()->updateByPk($model->barang_id, array('barang_statusregister'=>true));
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('create','id'=>$model->invperalatan_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,'modBarang'=>$modBarang
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
                $modBarang = $this->loadModelBarang($model->barang_id);
                $data['pemilikbarang_nama'] = $model->pemilik->pemilikbarang_nama;
                $dataAsalAset['asalaset_nama'] = $model->asal->asalaset_nama;
                $dataLokasi['lokasiaset_namalokasi'] = $model->lokasi->lokasiaset_namalokasi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GUInvperalatanT']))
		{
			$model->attributes=$_POST['GUInvperalatanT'];
			if($model->save()){
                            BarangM::model()->updateByPk($model->barang_id, array('barang_statusregister'=>true));
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				//$this->redirect(array('admin','id'=>$model->invperalatan_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,'modBarang'=>$modBarang, 'data'=>$data ,'dataAsalAset'=>$dataAsalAset ,'dataLokasi'=>$dataLokasi
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$model = $this->loadModel($id);
                        BarangM::model()->updateByPk($model->barang_id, array('barang_statusregister'=>false));
                        $this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('GUInvperalatanT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUInvperalatanT('search');
		$format = new CustomFormat();
		$model->unsetAttributes();  // clear any default values
		$model->tglAwal = date('Y-m-d H:i:s');
		$model->tglAkhir = date('Y-m-d H:i:s');
		if(isset($_GET['GUInvperalatanT'])){
			$model->attributes = $_GET['GUInvperalatanT'];
			$model->tglAwal = $format->formatDateMediumForDB($_GET['GUInvperalatanT']['tglAwal']);
			$model->tglAkhir = $format->formatDateMediumForDB($_GET['GUInvperalatanT']['tglAkhir']);
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	public function actionInformasi()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUInvperalatanT('search');
//		$model->unsetAttributes();  // clear any default values
                $model->tglAwal = date('d M Y');
                $model->tglAkhir = date('d M Y');
                $format = new CustomFormat();
	
         if (isset($_GET['GUInvperalatanT'])) {
            $model->attributes = $_GET['GUInvpseralatanT'];
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['GUInvperalatanT']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['GUInvperalatanT']['tglAkhir']);
        }

		$this->render('informasi',array(
			'model'=>$model,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GUInvperalatanT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        public function loadModelBarang($id){
        $model=BarangM::model()->findByPk($id);
        if($model===null)
                throw new CHttpException(404,'The requested page does not exist.');
        return $model;
        }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='guinvperalatan-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new InvperalatanT;
            $model->attributes=$_REQUEST['InvperalatanT'];
            $judulLaporan='Data Inventory Peralatan';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        /**
         * Module   : manajemenAsset/InvperalatanT/Create&modulId=47
         * @author  : Miranitha Fasha
         * Tanggal  : 05-05-2014 /d-m-y
         * Issue    : EHJ-1634
         * Desc     : Menambahkan print label setelah simpan.
         * Action   : actionPrintLabel
         */
        
        public function actionPrintLabel($idPeralatan, $caraPrint){
            $judulLaporan = '';
            $model = InvperalatanT::model()->findByPk($idPeralatan);  
            $modTerimaDet = TerimapersdetailT::model()->findByPk($model->terimapersdetail_id);
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('PrintLabel',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'model'=>$model,
                                            'modTerimaDet'=>$modTerimaDet
                ));
            }
        }
    public function actiongetKodeRegistrasiNonMutasi()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idKomponen = $_POST['idKomponen'];
            $komponen 	= $_POST['komponen'];
            $kodereg 	= $_POST['kodereg'];
            $kodebarang         = $_POST['kodebarang'];
            $kodeinventarisasi 	= $_POST['kodeinventarisasi'];
            $tahun	 	= $_POST['tahun'];
            $barangid	 	= $_POST['barangid'];
            $a = explode("-", $kodereg);
            if($kodeinventarisasi=='invtanaht'){
                $a[2] = '01';
            }elseif($kodeinventarisasi=='invperalatant'){
                $a[2] = '03';
            }elseif($kodeinventarisasi=='invgedungt'){
                $a[2] = '02';
            }elseif($kodeinventarisasi=='invjalant'){
                $a[2] = '05';
            }elseif($kodeinventarisasi=='invasetlaint'){
                $a[2] = '04';
            }

            if(empty($kodebarang)){
                $a[3] = "00";
            }else{
                $a[3] = $kodebarang;
            }
            if(empty($tahun)){
                $a[4] = date('Y');
            }else{
                $a[4] = $tahun;
            }
            
            $urutan = Generator::noUrutRegisterBarang($barangid,$kodeinventarisasi,$a[2]);                      
            $data['urutan'] = $urutan;

            if($komponen=='pemilikbarang')
            {
                $modPemilikbarang = PemilikbarangM::model()->findByPk($idKomponen);
                if(empty($a[0])){
                        $data['kode']	= $modPemilikbarang->pemilikbarang_kode;
                }else{
                        $data['kode']	= $modPemilikbarang->pemilikbarang_kode."-".$a[1]."-".$a[2]."-".$a[3]."-".$a[4]."-".$urutan;
                }

            }
            elseif($komponen=='lokasiaset')
            {
                $modLokasiAset = LokasiasetM::model()->findByPk($idKomponen);
                // if(empty($a[1])){
                // 	$data['kode']	= $a[0]."-".$modLokasiAset->lokasiaset_kode."-".$a[2]."-".$a[3]."-".$a[4];
                // }else{
                        $data['kode']	= $a[0]."-".$modLokasiAset->lokasiaset_kode."-".$a[2]."-".$a[3]."-".$a[4]."-".$urutan;
                // }
            }
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actiongetKodeRegistrasi()
	{
		if(Yii::app()->request->isAjaxRequest) {
            $idKomponen = $_POST['idKomponen'];
            $komponen 	= $_POST['komponen'];
            $kodereg 	= $_POST['kodereg'];
            $kodebarang         = $_POST['kodebarang'];
            $kodeinventarisasi 	= $_POST['kodeinventarisasi'];
            $tahun	 	= $_POST['tahun'];
            $barangid	 	= $_POST['barangid'];
            $idPemilik          = $_POST['idPemilik'];
            $idPemilikPilih     = $_POST['idPemilikPilih']; 
            $kodeinv            = $_POST['kodeInv'];
            $a = explode("-", $kodereg);
            if($kodeinventarisasi=='invtanaht'){
            	$a[2] = '01';
            }elseif($kodeinventarisasi=='invperalatant'){
            	$a[2] = '04';
            }elseif($kodeinventarisasi=='invgedungt'){
            	$a[2] = '02';
            }elseif($kodeinventarisasi=='invjalant'){
            	$a[2] = '03';
            }elseif($kodeinventarisasi=='invasetlaint'){
            	$a[2] = '05';
            }

            if(empty($kodebarang)){
            	$a[3] = "00";
            }else{
            	$a[3] = $kodebarang;
            }
            if(empty($tahun)){
            	$a[4] = date('Y');
            }else{
            	$a[4] = $tahun;
            }
            
            if ($idPemilik==$idPemilikPilih){
                $urutan = $kodeinv;
            } else {
                $urutan = Generator::noUrutRegisterBarang($barangid,$kodeinventarisasi,$a[2],$idPemilikPilih,$komponen);
            }                        
            $data['urutan'] = $urutan;
            
            if($komponen=='pemilikbarang')
            {
            	$modPemilikbarang = PemilikbarangM::model()->findByPk($idKomponen);
            	if(empty($a[0])){
            		$data['kode']	= $modPemilikbarang->pemilikbarang_kode;
            	}else{
            		$data['kode']	= $modPemilikbarang->pemilikbarang_kode."-".$a[1]."-".$a[2]."-".$a[3]."-".$a[4]."-".$urutan;
            	}
            	
            }
            elseif($komponen=='lokasiaset')
            {
            	$modLokasiAset = LokasiasetM::model()->findByPk($idKomponen);
            	// if(empty($a[1])){
            	// 	$data['kode']	= $a[0]."-".$modLokasiAset->lokasiaset_kode."-".$a[2]."-".$a[3]."-".$a[4];
            	// }else{
        		$data['kode']	= $a[0]."-".$modLokasiAset->lokasiaset_kode."-".$a[2]."-".$a[3]."-".$a[4]."-".$urutan;
            	// }
            }
            echo json_encode($data);
            Yii::app()->end();
        }
	}

	/**
     * Module   : manajemenAsset/InvPeralatanT/Admin&modulId=47
     * @author  : Hardi
     * Tanggal  : 18-07-2014
     * Issue    : EHJ-2525
     */
    
    public function actionPenyusutan($invperalatan_id, $penyusutanperalatan_id=null){
        $model = GUInvperalatanT::model()->findByPk($invperalatan_id);
        $modPenyusutan = new PenyusutanperalatanT;
        $format = new CustomFormat();

        $nilairesidu 	= empty($model->invperalatan_nilairesidu) ? 0 : $model->invperalatan_nilairesidu;
        $penyusutan 	= $model->invperalatan_harga;
        $umurekonomis 	= $model->umurekonomis;
		$model->bbnpenyusutanbrjlnper = number_format((($penyusutan - $nilairesidu) / $umurekonomis),0);

		if(!empty($penyusutanperalatan_id)){
			$modPenyusutan = PenyusutanperalatanT::model()->findAllbyAttributes(array('invperalatan_id'=>$invperalatan_id));
		}

        if(isset($_POST['PenyusutanperalatanT']))
		{
			$penyusutan     = $_POST['PenyusutanperalatanT'];
			$status = false;
			foreach ($penyusutan as $key => $data) {
				$modPenyusutan = new PenyusutanperalatanT();
				$modPenyusutan->attributes = $data;
				$modPenyusutan->bbnpenyusutanbrjlnper = MyFunction::number_unformat($data['bbnpenyusutanbrjlnper']);
				$modPenyusutan->akumpenyusutanper = MyFunction::number_unformat($data['akumpenyusutanper']);
				$modPenyusutan->nilaibukuperalatan = MyFunction::number_unformat($data['nilaibukuperalatan']);
				$modPenyusutan->ruangan_id = Yii::app()->user->getState('ruangan_id');
				$modPenyusutan->invperalatan_id = $model->invperalatan_id;
				$modPenyusutan->pegawai_id = Yii::app()->user->pegawai_id;
				$modPenyusutan->tglpenyusutanperalatan = $format->formatDateMediumForDB($_POST['GUInvperalatanT']['tglpenyusutanperalatan']); 
				$modPenyusutan->hargaperolehanperalatan = $model->invperalatan_harga; 
				$modPenyusutan->nilairesiduperalatan = empty($model->invperalatan_nilairesidu) ? 0 : $model->invperalatan_nilairesidu; 
				$modPenyusutan->tglterimaperalatan = $format->formatDateMediumForDB($model->invperalatan_tglguna);  
				$modPenyusutan->create_time = date('Y-m-d');
				$modPenyusutan->update_time = date('Y-m-d');
				$modPenyusutan->create_ruangan = Yii::app()->user->getState('ruangan_id');
				$modPenyusutan->create_loginpemakai_id = Yii::app()->user->id;

				if($modPenyusutan->validate()){
					$modPenyusutan->save();
					GUInvperalatanT::model()->updateByPk($model->invperalatan_id, array('invperalatan_akumsusut'=>$modPenyusutan->akumpenyusutanper));
					$status = true;
				}
			}
			if($status){
				$model->tglpenghapusan = $format->formatDateMediumForDB($_POST['GUInvperalatanT']['tglpenyusutanperalatan']);
				$uraian = 'Penyusutan';
				$modJurnalRekening = $this->saveJurnalRekening($model, $_POST['GUInvperalatanT'], $uraian);
	            $noUrut = 0;
	            foreach($_POST['JenispengeluaranrekeningV'] AS $i => $post){
	                $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
	                $noUrut ++;
	            }

				Yii::app()->user->setFlash('success',"Penyusutan berhasil disimpan!"); 
				$model->isNewRecord = False;
				$this->redirect(array('penyusutan','invperalatan_id'=>$model->invperalatan_id, 'penyusutanperalatan_id'=>$modPenyusutan->penyusutanperalatan_id));
			}
		}

		$this->render('penyusutan',array(
			'model'=>$model,'modPenyusutan'=>$modPenyusutan,
		));
    }

    public function actionhitungsusut()
	{
		if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $tglpenyusutanperalatan 	= $_POST['tglpenyusutanperalatan'];
            $invperalatan_id 			= $_POST['invperalatan_id'];

            $model = GUInvperalatanT::model()->findByPk($invperalatan_id);
            
            $format = new CustomFormat();
            $tglpenyusutanperalatan = $format->formatDateMediumForDB($tglpenyusutanperalatan);
            $tgl_penyusutan = explode("-", $tglpenyusutanperalatan);
            $bulan_susut	= $tgl_penyusutan[1];
            $tahun_susut	= $tgl_penyusutan[0];

            $ceksql="SELECT bulanpenyusutanperalatan FROM penyusutanperalatan_t WHERE invperalatan_id=$invperalatan_id ORDER BY penyusutanperalatan_id DESC LIMIT 1";
	        $ceksusut = Yii::app()->db->createCommand($ceksql)->queryRow();

	        if(empty($ceksusut)){
            	$invperalatan_tglguna = $format->formatDateMediumForDB($model->invperalatan_tglguna);
            }else{
            	$invperalatan_tglguna = $format->formatDateMediumForDB($ceksusut['bulanpenyusutanperalatan']);
            }

            $invperalatan_tglguna = explode("-", $invperalatan_tglguna);
            $bulan_guna		= $invperalatan_tglguna[1];
            $tahun_guna		= $invperalatan_tglguna[0];

            $selisihtahun = intval($tahun_susut)-intval($tahun_guna);
            if(empty($ceksusut)){
	            if($selisihtahun > 0){
	            	$jml_bulansusut = $selisihtahun * 12 + intval($bulan_susut) + 1;
	            }else{
	            	$jml_bulansusut = intval($bulan_susut) + 1;
	            }
	        }else{
	        	if($selisihtahun > 0){
	            	$jml_bulansusut = $selisihtahun * 12 + intval($bulan_susut);
	            }else{
	            	$jml_bulansusut = intval($bulan_susut);
	            }
	        }

            $jml_penyusutan = $jml_bulansusut - intval($bulan_guna);

            $periode_susut = $tahun_susut.'-'.$bulan_susut;
            $sql="SELECT count(*) as jumlah FROM penyusutanperalatan_t WHERE to_char(bulanpenyusutanperalatan,'yyyy-mm')='$periode_susut' AND invperalatan_id=$invperalatan_id";
	        $noNopendRI = Yii::app()->db->createCommand($sql)->queryRow();
	        $modPenyusutan = $noNopendRI['jumlah'];

            if($modPenyusutan==0){
            	// $data['baru'] = true;
			
                echo CJSON::encode(
                    $this->renderPartial('manajemenAsset.views.invperalatanT._rowPenyusutan', array('model'=>$model, 'jml_penyusutan'=>$jml_penyusutan, 'invperalatan_tglguna'=>$invperalatan_tglguna, 'ceksusut'=>$ceksusut), true)
                );               
	                       	
            }else{
            	$data['bulan_guna'] = $jml_bulansusut;
            	$data['baru'] = "false";
            	echo json_encode($data);
            }
            Yii::app()->end();
        }
	}

	/**
     * Module   : manajemenAsset/InvPeralatanT/mutasiPeralatan&modulId=47
     * @author  : Hardi
     * Tanggal  : 18-07-2014
     * Issue    : EHJ-2497
     */
    
    public function actionmutasiPeralatan($id=null){
        $model = new GUInvperalatanT;
        $modLama = new InvperalatanT;
        $format = new CustomFormat();

        if(isset($_POST['GUInvperalatanT']))
		{
			$model->attributes=$_POST['GUInvperalatanT'];
			$model->barang_id	= $_POST['InvperalatanT']['barang_id'];
			$model->umurekonomis = $_POST['GUInvperalatanT']['invperalatan_umurekonomis'];
			$model->invperalatan_tglguna = $format->formatDateMediumForDB($model->invperalatan_tglguna);
			$id_inventory_lama = $_POST['InvperalatanT']['invperalatan_id'];
			$model->create_time = date('Y-m-d H:i:s');
			$model->update_time = date('Y-m-d H:i:s');
			$model->tglpenghapusan = date('Y-m-d H:i:s');
			
	        $model->create_loginpemakai_id = Yii::app()->user->id;
	        $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
	        $model->invperalatan_kapasitasrata = $id_inventory_lama;

			if($model->save()){
                InvperalatanT::model()->updateByPk($id_inventory_lama, array('invperalatan_ket'=>'Dimutasikan'));
                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('mutasiPeralatan','id'=>$model->invperalatan_id));
            }
		}

		$this->render('mutasi',array(
			'model'=>$model, 'modlama'=>$modLama,
		));
    }

    /**
     * Module   : manajemenAsset/InvPeralatanT/pemusnahanaset&modulId=47
     * @author  : Hardi
     * Tanggal  : 21-07-2014
     * Issue    : EHJ-2533
     */
    
    public function actionPenghapusanAset($id=null, $tipehapus=null){
        $this->layout = '//layouts/frameDialog';
        $model = GUInvperalatanT::model()->findByPk($id);
        $model->invperalatan_akumsusut = isset($model->invperalatan_akumsusut) ? $model->invperalatan_akumsusut : 0;
        $urutan = explode("-", $model->invperalatan_noregister);
        $model->kd_urutan = $urutan[2];
        $model->tglpenghapusan = date('Y-m-d H:i:s');

        if($tipehapus=='1'){
    		$model->tipepenghapusan = "pemusnahan";
    		$model->kerugian = $model->invperalatan_harga - $model->invperalatan_akumsusut;
    	}
    	else{
    		$model->tipepenghapusan = "penjualan";
    		$model->kerugian = 0;
    	}
    	// $model->isNewRecord = true;
        if(isset($_POST['GUInvperalatanT']))
		{
        	$format = new CustomFormat();
        	$model->tglpenghapusan = $format->formatDateMediumForDB($_POST['GUInvperalatanT']['tglpenghapusan']); 
        	$model->invperalatan_tglguna = $format->formatDateMediumForDB($model->invperalatan_tglguna);
        	$model->create_time = $format->formatDatetimeMediumForDB($model->create_time);
        	$model->update_time = $format->formatDatetimeMediumForDB($model->update_time);

        	if($tipehapus=='2'){
        		if($_POST['GUInvperalatanT']['kerugian'] < 0 ){
        			$model->kerugian = $_POST['GUInvperalatanT']['kerugian'];
        			$model->keuntungan = 0;
        		}else{
        			$model->keuntungan = $_POST['GUInvperalatanT']['kerugian'];
        			$model->kerugian = 0;
        		}
        	}
        	$model->hargajualaktiva = $_POST['GUInvperalatanT']['hargajualaktiva'];
        	if($model->validate()){
        		$model->save();

        		if(isset($_POST['JenispenerimaanrekeningV'])){
	        		if($tipehapus=='1'){
		        		//=========== Save Jurnal Rekening =================
		        		$uraian = $model->tipepenghapusan;
		                $modJurnalRekening = $this->saveJurnalRekening($model, $_POST['GUInvperalatanT'], $uraian);
		                // if($_POST['BKReturbayarpelayananT']['is_posting']=='posting')
		                // {
		                //     $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
		                // }else{
		                //     $modJurnalPosting = null;
		                // }
		                $noUrut = 0;
		                foreach($_POST['JenispenerimaanrekeningV'] AS $i => $post){
		                    $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
		                    $noUrut ++;
		                }
		                //==================================================
		            }
		        }
		        $model->isNewRecord = true;
        		Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Penghapusan berhasil dilakukan.');
        	}
        }

		$this->render('penghapusan',array(
			'model'=>$model,
		));
    }

    protected function saveJurnalRekening($model, $postPenUmum, $uraian=null)
    {
        $modJurnalRekening = new JurnalrekeningT;
        $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');
        $modJurnalRekening->nobuktijurnal = Generator::noBuktiJurnalRek();
        $modJurnalRekening->kodejurnal = Generator::kodeJurnalRek();
        $modJurnalRekening->noreferensi = 0;
        $modJurnalRekening->tglreferensi = $model->tglpenghapusan;
        $modJurnalRekening->nobku = "";
        $modJurnalRekening->urianjurnal = $uraian." ".$model->tglpenghapusan;
        
        $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENGELUARAN_KAS;
        $periodeID = Yii::app()->session['periodeID'];
        $modJurnalRekening->rekperiod_id = $periodeID[0];
        $modJurnalRekening->create_time = date('Y-m-d H:i:s');
        $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
        $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
        $modJurnalRekening->ruangan_id = Yii::app()->user->getState('ruangan_id');
        
        if($modJurnalRekening->validate()){
            $modJurnalRekening->save();
            $this->successSave = true;
        } else {
            $this->successSave = false;
            $this->pesan = $modJurnalRekening->getErrors();
        }

        return $modJurnalRekening;
    }

    public function saveJurnalDetail($modJurnalRekening, $post, $noUrut=0, $modJurnalPosting){
        $modJurnalDetail = new JurnaldetailT();
        $modJurnalDetail->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
        $modJurnalDetail->rekperiod_id = $modJurnalRekening->rekperiod_id;
        $modJurnalDetail->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
        $modJurnalDetail->uraiantransaksi = $modJurnalRekening->urianjurnal;
        $modJurnalDetail->saldodebit = $post['saldodebit'];
        $modJurnalDetail->saldokredit = $post['saldokredit'];
        $modJurnalDetail->nourut = $noUrut;
        $modJurnalDetail->rekening1_id = $post['struktur_id'];
        $modJurnalDetail->rekening2_id = $post['kelompok_id'];
        $modJurnalDetail->rekening3_id = $post['jenis_id'];
        $modJurnalDetail->rekening4_id = $post['obyek_id'];
        $modJurnalDetail->rekening5_id = $post['rincianobyek_id'];
        $modJurnalDetail->catatan = "";

        if($modJurnalDetail->validate()){
            $modJurnalDetail->save();
        }
        return $modJurnalDetail;        
    }

    protected function saveJurnalPosting($arrJurnalPosting)
    {
        $modJurnalPosting = new JurnalpostingT;
        $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
        $modJurnalPosting->keterangan = "Posting automatis";
        $modJurnalPosting->create_time = date('Y-m-d H:i:s');
        $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
        $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
        if($modJurnalPosting->validate()){
            $modJurnalPosting->save();
            $this->successSave = true;
        } else {
            $this->successSave = false;
            $this->pesan = $modJurnalPosting->getErrors();
        }
        return $modJurnalPosting;
    } 

    /**
     * Module   : manajemenAsset/InvPeralatanT/informasiMutasiAset&modulId=47
     * @author  : Hardi
     * Tanggal  : 26-08-2014
     * Issue    : EHJ-2712
     */
    
    public function actioninformasiMutasiAset(){
        $model = new MutasiasetperalatanV;
        $format = new CustomFormat();
        $model->tglAwal 	= date('Y-m-d');
        $model->tglAkhir 	= date('Y-m-d');

        if(isset($_GET['MutasiasetperalatanV']))
		{
			$model->attributes 	= $_REQUEST['MutasiasetperalatanV'];
			$model->tglAwal		= $format->formatDateMediumForDB($_REQUEST['MutasiasetperalatanV']['tglAwal']);
			$model->tglAkhir 	= $format->formatDateMediumForDB($_REQUEST['MutasiasetperalatanV']['tglAkhir']);
		}

		$this->render('informasiMutasi',array(
			'model'=>$model,
		));
    }

    public function actionprintInventaris($id=null)
    {
        $model = GUInvperalatanT::model()->findByPk($id);
        $judulLaporan = 'Print Nomor Register Inventaris';

        $data['type'] = "Print";
        $caraPrint = "PRINT";
        $target = 'PrintLabel';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = null;

        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } 
    }
}
