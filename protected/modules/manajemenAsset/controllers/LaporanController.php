<?php

class LaporanController extends SBaseController
{
	public $layout='//layouts/column1';
        public $defaultAction ='PembelianBarang';
	public function actionIndex()
	{
		$this->render('index');
	}
        
        // Laporan Pembelian Barang Manajemen Asset //
                public function actionPembelianBarang()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new GULaporanPembelianbarangT;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['GULaporanPembelianbarangT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanPembelianbarangT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAkhir']);
                            $model->sumberdana_id = $_GET['GULaporanPembelianbarangT']['sumberdana_id'];
                            $model->supplier_id = $_GET['GULaporanPembelianbarangT']['supplier_id'];
                            $model->peg_pemesanan_id = $_GET['GULaporanPembelianbarangT']['peg_pemesanan_id'];
                        }
                        $this->render('pembelianBarang/pembelianBarang',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintPembelianBarang()
                {
                    
                    $model = new GULaporanPembelianbarangT;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Pembelian Barang';

                    //Data Grafik
                    $data['title'] = 'Grafik Pembelian Barang';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['GULaporanPembelianbarangT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanPembelianbarangT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'pembelianBarang/printPembelianBarang';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFramePembelianBarang() {
                    $this->layout = '//layouts/frameDialog';

                    $model = new GULaporanPembelianbarangT;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Pembelian Barang';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['GULaporanPembelianbarangT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanPembelianbarangT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAkhir']);
                    }
                    $searchdata = $model->searchPembelianBaranggrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
                
           // Akhir Laporan Pembelian Barang Manajemen Asset //   
                
           // Laporan Penerimaan Persediaan Manajemen Asset //
           public function actionPenerimaanPersediaan()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new GULaporanPenerimaanpersediaanT;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['GULaporanPenerimaanpersediaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanPenerimaanpersediaanT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAkhir']);
                            $model->sumberdana_id = $_GET['GULaporanPenerimaanpersediaanT']['sumberdana_id'];
                            $model->ruanganpenerima_id = $_GET['GULaporanPenerimaanpersediaanT']['ruanganpenerima_id'];
                            $model->peg_penerima_id = $_GET['GULaporanPenerimaanpersediaanT']['peg_penerima_id'];
                        }
                        $this->render('penerimaanPersediaan/penerimaanPersediaan',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintPenerimaanPersediaan()
                {
                    
                    $model = new GULaporanPenerimaanpersediaanT;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Penerimaan Persediaan';

                    //Data Grafik
                    $data['title'] = 'Grafik Penerimaan Persediaan';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['GULaporanPenerimaanpersediaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanPenerimaanpersediaanT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'penerimaanPersediaan/printPenerimaanPersediaan';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFramePenerimaanPersediaan() {
                    $this->layout = '//layouts/frameDialog';

                    $model = new GULaporanPenerimaanpersediaanT;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Penerimaan Persediaan';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['GULaporanPenerimaanpersediaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanPenerimaanpersediaanT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAkhir']);
                    }
                    $searchdata = $model->searchPenerimaanPersediaangrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
           
           // Akhir Laporan Penerimaan Persediaan Manajemen Asset //
           
           // Laporan Retur Penerimaan Manajemen Asset //
            public function actionReturPenerimaan()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new GULaporanReturPenerimaanT;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['GULaporanReturPenerimaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanReturPenerimaanT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAkhir']);
                            $model->peg_retur_id = $_GET['GULaporanReturPenerimaanT']['peg_retur_id'];
                            $model->peg_mengetahui_id = $_GET['GULaporanReturPenerimaanT']['peg_mengetahui_id'];
                        }
                        $this->render('returPenerimaan/returPenerimaan',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintReturPenerimaan()
                {
                    
                    $model = new GULaporanReturPenerimaanT;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Retur Penerimaan';

                    //Data Grafik
                    $data['title'] = 'Grafik Retur Penerimaan';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['GULaporanReturPenerimaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanReturPenerimaanT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'returPenerimaan/printReturPenerimaan';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFrameReturPenerimaan() {
                    $this->layout = '//layouts/frameDialog';

                    $model = new GULaporanReturPenerimaanT;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Retur Penerimaan';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['GULaporanReturPenerimaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanReturPenerimaanT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAkhir']);
                    }
                    $searchdata = $model->searchReturPenerimaangrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }  




             public function actioninventarisasiTanah()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new MALaporanInventarisasiTanah;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['MALaporanInventarisasiTanah'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiTanah'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiTanah']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiTanah']['tglAkhir']);
                        }
                        $this->render('inventarisasiTanah/inventarisasiTanah',array(
                            'model'=>$model,
                        ));
                }  

                 public function actionPrintInventarisasiTanah()
                {
                    
                    $model = new MALaporanInventarisasiTanah;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Inventarisasi Tanah';

                    //Data Grafik
                    $data['title'] = 'Grafik Inventarisasi Tanah';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['GULaporanReturPenerimaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiTanah'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiTanah']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiTanah']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'inventarisasiTanah/printInventarisasiTanah';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   


                //--------- INVENTARISASI PERALATAN -----------

                 public function actioninventarisasiPeralatan()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new MALaporanInventarisasiPeralatan;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['MALaporanInventarisasiPeralatan'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiPeralatan'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAkhir']);
                        }
                        $this->render('inventarisasiPeralatan/inventarisasiPeralatan',array(
                            'model'=>$model,
                        ));
                }  

                 public function actionPrintInventarisasiPeralatan()
                {
                    
                    $model = new MALaporanInventarisasiPeralatan;
                    $model->tglAwal = date('Y-m-d 00:00:0   0');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Inventarisasi Peralatan dan Mesin';

                    //Data Grafik
                    $data['title'] = 'Grafik Inventarisasi Peralatan dan Mesin';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['MALaporanInventarisasiPeralatan'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiPeralatan'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'inventarisasiPeralatan/printInventarisasiPeralatan';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   


                public function actionFrameInventarisasiPeralatan() {
                    $this->layout = '//layouts/frameDialog';

                    $model = new MALaporanInventarisasiPeralatan;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Inventarisasi Peralatan dan Mesin';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['MALaporanInventarisasiPeralatan'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiPeralatan'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAkhir']);
                    }
                    $searchdata = $model->searchInventarisasiPeralatangrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }


                //---------- INVENTARISASI GEDUNG -----------


                public function actioninventarisasiGedung()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new MALaporanInventarisasiGedung;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['MALaporanInventarisasiGedung'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiGedung'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiGedung']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiGedung']['tglAkhir']);
                        }
                        $this->render('inventarisasiGedung/inventarisasiGedung',array(
                            'model'=>$model,
                        ));
                }  

                 public function actionPrintInventarisasiGedung()
                {
                    
                    $model = new MALaporanInventarisasiGedung;
                    $model->tglAwal = date('Y-m-d 00:00:0   0');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Inventarisasi Gedung dan Bangunan';

                    //Data Grafik
                    $data['title'] = 'Grafik Inventarisasi Gedung dan Bangunan';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['MALaporanInventarisasiGedung'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiGedung'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiGedung']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiGedung']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'inventarisasiGedung/printInventarisasiGedung';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   


                public function actionFrameInventarisasiGedung() {
                    $this->layout = '//layouts/frameDialog';
                    $model = new MALaporanInventarisasiGedung;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Inventarisasi Gedung dan Bangunan';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['MALaporanInventarisasiGedung'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiGedung'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiGedung']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiGedung']['tglAkhir']);
                    }
                    $searchdata = $model->searchInventarisasiGedunggrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }

                //------------ INVENTARISASI JALAN ---------------

                 public function actioninventarisasiJalan()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new MALaporanInventarisasiPeralatan;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['MALaporanInventarisasiPeralatan'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiPeralatan'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAkhir']);
                        }
                        $this->render('inventarisasiJalan/inventarisasiJalan',array(
                            'model'=>$model,
                        ));
                }  

                 public function actionPrintInventarisasiJalan()
                {
                    
                    $model = new MALaporanInventarisasiPeralatan;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Inventarisasi Kendaraan';

                    //Data Grafik
                    $data['title'] = 'Grafik Inventarisasi Kendaraan';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['MALaporanInventarisasiPeralatan'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiPeralatan'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'inventarisasiJalan/printInventarisasiJalan';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   


                public function actionFrameInventarisasiJalan() {
                    $this->layout = '//layouts/frameDialog';
                    $model = new MALaporanInventarisasiPeralatan;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Inventarisasi Kendaraan';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['MALaporanInventarisasiPeralatan'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiPeralatan'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAkhir']);
                    }
                    $searchdata = $model->searchInventarisasiPeralatangrafikKendaraan();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }




                //------------ INVENTARISASI JALAN ---------------

                 public function actioninventarisasiAsetLain()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new MALaporanInventarisasiPeralatan;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['MALaporanInventarisasiPeralatan'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiPeralatan'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAkhir']);
                        }
                        $this->render('inventarisasiAsetLain/inventarisasiAsetLain',array(
                            'model'=>$model,
                        ));
                }  

                 public function actionPrintInventarisasiAsetLain()
                {
                    
                    $model = new MALaporanInventarisasiPeralatan;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Inventarisasi Peralatan Non Medis';

                    //Data Grafik
                    $data['title'] = 'Grafik Inventarisasi Peralatan Non Medis';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['MALaporanInventarisasiPeralatan'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiPeralatan'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'inventarisasiAsetLain/printInventarisasiAsetLain';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   


                public function actionFrameInventarisasiAsetLain() {
                    $this->layout = '//layouts/frameDialog';
                    $model = new MALaporanInventarisasiPeralatan;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Inventarisasi Peralatan Non Medis';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['MALaporanInventarisasiPeralatan'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['MALaporanInventarisasiPeralatan'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanInventarisasiPeralatan']['tglAkhir']);
                    }
                    $searchdata = $model->searchInventarisasiPeralatangrafikNonMedis();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }


                //--------------------------------------------------------------------


                
                protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
                    $format = new CustomFormat();
                    $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

                    if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
                        $this->layout = '//layouts/printWindows';
                        $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($caraPrint == 'EXCEL') {
                        $this->layout = '//layouts/printExcel';
                         $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($_REQUEST['caraPrint'] == 'PDF') {
                        $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                        $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('', $ukuranKertasPDF);
                        $mpdf->useOddEven = 2;
                        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet, 1);
                        $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
                        $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
                        $mpdf->Output();
                    }
                }
                
           
                public function actionStock()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new RELaporanstokprodukposV;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['RELaporanstokprodukposV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['RELaporanstokprodukposV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAkhir']);
                        }
                        $this->render('stock/stock',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintStock()
                {
                    $model = new RELaporanstokprodukposV;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Stock Barang';

                    //Data Grafik
                    $data['title'] = 'Grafik Stock';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['RELaporanstokprodukposV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['RELaporanstokprodukposV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'stock/printStock';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFrameStock() {
                    $this->layout = '//layouts/frameDialog';

                    $model = new RELaporanstokprodukposV;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Stock Barang';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['RELaporanstokprodukposV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['RELaporanstokprodukposV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAkhir']);
                    }
                    $searchdata = $model->searchGrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
    
                protected function parserTanggal($tgl){
                    $tgl = explode(' ', $tgl);
                    $result = array();
                    foreach ($tgl as $row){
                        if (!empty($row)){
                            $result[] = $row;
                        }
                    }
                    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];

                }

    //------------ Laporan Penyusutan Aset ---------------
    public function actionPenyusutanAset()
    {
        $model = new MALaporanpenyusutanasetV;
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        if (isset($_GET['MALaporanpenyusutanasetV'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['MALaporanpenyusutanasetV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanpenyusutanasetV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanpenyusutanasetV']['tglAkhir']);
        }
        $this->render('penyusutanAset/index',array(
            'model'=>$model,
        ));
    }

    public function actionrincianPenyusutan($noregister=null, $kdinventaris=null)
    {
        $this->layout = '//layouts/frameDialog';
        $noregister     = $_GET['inventarisasi_noregister'];
        $kdinventaris   = $_GET['kodeInventaris'];
        $caraPrint = $_REQUEST['caraPrint'];

        if($kdinventaris=='02'){
            $model = InvgedungT::model()->findbyAttributes(array('invgedung_noregister'=>$noregister));
            $rincian = MALaporanpenyusutanasetV::model()->findAllbyAttributes(array('inventarisasi_noregister'=>$noregister));
        }else{
            $model = InvperalatanT::model()->findbyAttributes(array('invperalatan_noregister'=>$noregister));
            $rincian = MALaporanpenyusutanasetV::model()->findAllbyAttributes(array('inventarisasi_noregister'=>$noregister));
        }
        $this->render('penyusutanAset/rincianPenyusutan',array(
            'model'=>$model,'rincian'=>$rincian, 'kdinventaris'=>$kdinventaris, 'caraPrint'=>$caraPrint
        ));
    }

    public function actionprintPenyusutanaset()
    {
        $model = new MALaporanpenyusutanasetV;
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d 23:59:59');
        $judulLaporan = 'Laporan Penyusutan Aset';

        $data['title'] = 'Grafik Penyusutan Aset';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['MALaporanpenyusutanasetV'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['MALaporanpenyusutanasetV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['MALaporanpenyusutanasetV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['MALaporanpenyusutanasetV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penyusutanAset/printPenyusutanAset';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionPrintrincianPenyusutan($noregister=null, $kdinventaris=null)
    {
        $noregister     = $_GET['inventarisasi_noregister'];
        $kdinventaris   = $_GET['kodeInventaris'];
        $caraPrint = $_REQUEST['caraPrint'];
        $judulLaporan = 'Rincian Penyusutan Aset';
        if($kdinventaris=='02'){
            $model = InvgedungT::model()->findbyAttributes(array('invgedung_noregister'=>$noregister));
            $rincian = MALaporanpenyusutanasetV::model()->findAllbyAttributes(array('inventarisasi_noregister'=>$noregister));
        }else{
            $model = InvperalatanT::model()->findbyAttributes(array('invperalatan_noregister'=>$noregister));
            $rincian = MALaporanpenyusutanasetV::model()->findAllbyAttributes(array('inventarisasi_noregister'=>$noregister));
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penyusutanAset/PrintrincianPenyusutan';

        $this->printFunction($model, $rincian, $caraPrint, $judulLaporan, $target);
    }
}