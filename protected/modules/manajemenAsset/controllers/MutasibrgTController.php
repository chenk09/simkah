<?php

class MutasibrgTController extends SBaseController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $defaultAction = 'admin';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'informasi', 'detailMutasiBarang', 'batalMutasiBarang', 'informasiGudang'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'print'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'RemoveTemporary'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionIndex($id = null) {
        if (!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) {
            throw new CHttpException(401, Yii::t('mds', 'You are prohibited to access this page. Contact Super Administrator'));
        }
        $model = new GUMutasibrgT;
        if (isset($id)){
            $modPesan = PesanbarangT::model()->find('pesanbarang_id = '.$id.' and mutasibrg_id is null');
            $model->pesanbarang_id = $modPesan->pesanbarang_id;
            $model->ruangantujuan_id = $modPesan->ruanganpemesan_id;
            if (count($modPesan) == 1){
                $modDetailPesan = PesanbarangdetailT::model()->findAll('pesanbarang_id ='.$id);
                foreach ($modDetailPesan as $i=>$row){
                    $modDetails[$i] = new MutasibrgdetailT();
                    $modDetails[$i]->attributes = $row->attributes;
                    $modDetails[$i]->barang_id = $row->barang_id;
                    $modDetails[$i]->satuanbrg = $row->satuanbarang;
                    $modDetails[$i]->qty_mutasi = $row->qty_pesan;
                    if (KonfigsystemK::getKonfigKurangiStokUmum() == true){
                        if (InventarisasiruanganT::validasiStok($modDetails[$i]->qty_mutasi, $modDetails[$i]->barang_id) == false){
                            $modDetails[$i]->qty_mutasi = 0;
                        }
                    }
                }
            }
        }
        $model->tglmutasibrg = date('Y-m-d H:i:s');
        $instalasi_id = Yii::app()->user->getState('instalasi_id');
        $model->nomutasibrg = Generator::noMutasiBarang($instalasi_id);
        $model->totalhargamutasi = 0;
        $modLogin = LoginpemakaiK::model()->findByAttributes(array('loginpemakai_id' => Yii::app()->user->id));
        $model->pegpengirim_id = $modLogin->pegawai_id;
        $model->pegpengirim_nama = $modLogin->pegawai->nama_pegawai;
        if (isset($_GET['idMutasi'])){
            $idMutasi = $_GET['idMutasi'];
            $modelMutasi = GUMutasibrgT::model()->findByPk($idMutasi);
            if (count($modelMutasi) == 1){
                $model = $modelMutasi;
                $model->pegpengirim_nama = $model->pegawaipengirim->nama_pegawai;
                $model->pegmengetahui_nama = $model->pegawaimengetahui->nama_pegawai;
                $model->instalasi_id = $model->ruangtujuan->instalasi_id;
                $modDetails = MutasibrgdetailT::model()->findAll('mutasibrg_id = '.$model->mutasibrg_id);
            }
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['GUMutasibrgT'])) {
            $model->attributes = $_POST['GUMutasibrgT'];
            if (count($_POST['MutasibrgdetailT']) > 0) {
                $modDetails = $this->validateTable($_POST['MutasibrgdetailT'], $model);
                if ($model->validate()) {
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $total = 0;
                        $success = true;
                        if ($model->save()) {
                            if (!empty($modPesan->pesanbarang_id)){
                                PesanbarangT::model()->updateByPk($modPesan->pesanbarang_id, array('mutasibrg_id'=>$model->mutasibrg_id));
                            }
                            $modDetails = $this->validateTable($_POST['MutasibrgdetailT'], $model);
                            foreach ($modDetails as $i => $data) {
                                if ($data->qty_mutasi > 0){
                                    $modInventaris = InventarisasiruanganT::model()->findByAttributes(array('barang_id'=>$data->barang_id), array('order'=>'tgltransaksi', 'limit'=>1));
                                    $data->inventarisasi_id = $modInventaris->inventarisasi_id;
                                    $total += $modInventaris->inventarisasi_hargasatuan*$data->qty_mutasi;
                                    if ($data->save()) {
                                        InventarisasiruanganT::model()->updateByPk($modInventaris->inventarisasi_id, array('mutasibrgdetail_id'=>$data->mutasibrgdetail_id));
                                        if (KonfigsystemK::getKonfigKurangiStokUmum() == true){
                                            if (InventarisasiruanganT::validasiStok($data->qty_mutasi, $data->barang_id) == true){
                                                InventarisasiruanganT::kurangiStok($data->qty_mutasi, $data->barang_id);
                                            }
                                            else{
                                                $success = false;
                                            }
                                        }
                                    } else {
                                        $success = false;
                                    }
                                }
                            }
                            if ($total != 0){
                                MutasibrgT::model()->updateByPk($model->mutasibrg_id, array('totalhargamutasi'=>$total));
                            }
                        }
                        if ($success == true) {
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('index', 'idMutasi'=>$model->mutasibrg_id));
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                        }
                    } catch (Exception $ex) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($ex, true));
                    }
                }
            } else {
                $model->validate();
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
            }
        }

        $this->render('manajemenAsset.views.mutasibrgT.index', array(
            'model' => $model, 'modDetails' => $modDetails, 'modPesan'=>$modPesan,
        ));
    }
    
    protected function validateTable($datas, $model) {
        $valid = true;
        foreach ($datas as $i => $data) {
            $modDetails[$i] = new MutasibrgdetailT();
            $modDetails[$i]->attributes = $data;
            $modDetails[$i]->mutasibrg_id = $model->mutasibrg_id;

            $valid = $modDetails[$i]->validate() && $valid;
        }

        return $modDetails;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        if (!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) {
            throw new CHttpException(401, Yii::t('mds', 'You are prohibited to access this page. Contact Super Administrator'));
        }
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['GUMutasibrgT'])) {
            $model->attributes = $_POST['GUMutasibrgT'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                $this->redirect(array('view', 'id' => $model->mutasibrg_id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            if (!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)) {
                throw new CHttpException(401, Yii::t('mds', 'You are prohibited to access this page. Contact Super Administrator'));
            }
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
//	public function actionIndex()
//	{
//		$dataProvider=new CActiveDataProvider('GUMutasibrgT');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
//	}

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        if (!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) {
            throw new CHttpException(401, Yii::t('mds', 'You are prohibited to access this page. Contact Super Administrator'));
        }
        $model = new GUMutasibrgT('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['GUMutasibrgT']))
            $model->attributes = $_GET['GUMutasibrgT'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = GUMutasibrgT::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'gumutasibrg-t-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Mengubah status aktif
     * @param type $id 
     */
    public function actionRemoveTemporary($id) {
        if (!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) {
            throw new CHttpException(401, Yii::t('mds', 'You are prohibited to access this page. Contact Super Administrator'));
        }
        //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
        //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

//    public function actionPrint() {
//        $model = new GUMutasibrgT;
//        $model->attributes = $_REQUEST['GUMutasibrgT'];
//        $judulLaporan = 'Data GUMutasibrgT';
//        $caraPrint = $_REQUEST['caraPrint'];
//        if ($caraPrint == 'PRINT') {
//            $this->layout = '//layouts/printWindows';
//            $this->render('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
//        } else if ($caraPrint == 'EXCEL') {
//            $this->layout = '//layouts/printExcel';
//            $this->render('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
//        } else if ($_REQUEST['caraPrint'] == 'PDF') {
//            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
//            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
//            $mpdf = new MyPDF('', $ukuranKertasPDF);
//            $mpdf->useOddEven = 2;
//            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
//            $mpdf->WriteHTML($stylesheet, 1);
//            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
//            $mpdf->WriteHTML($this->renderPartial('Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
//            $mpdf->Output();
//        }
//    }

    public function actionInformasi() {
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model = new GUMutasibrgT('search');
//		$model->unsetAttributes();  // clear any default values
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['GUMutasibrgT'])) {
            $model->attributes = $_GET['GUMutasibrgT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
        }

        if (Yii::app()->request->isAjaxRequest) {
                    echo $this->renderPartial('_table', array('model'=>$model),true);
                }else{
                   $this->render('manajemenAsset.views.mutasibrgT.informasi', array(
                    'model' => $model,
                    ));
                }
       
    }
    
    public function actionInformasiGudang() {
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model = new GUMutasibrgT('search');
//		$model->unsetAttributes();  // clear any default values
        $model->tglAwal = date('Y-m-d H:i:s');
        $model->tglAkhir = date('Y-m-d H:i:s');
        if (isset($_GET['GUMutasibrgT'])) {
            $model->attributes = $_GET['GUMutasibrgT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
        }

        $this->render('manajemenAsset.views.mutasibrgT.informasiGudang', array(
            'model' => $model,
        ));
    }

    public function actionDetailMutasiBarang($id) {
        $this->layout = '//layouts/frameDialog';
        $modMutasi = MutasibrgT::model()->findByPk($id);
        if (count($modMutasi) == 1){
            $modDetailMutasi = MutasibrgdetailT::model()->findAllByAttributes(array('mutasibrg_id' => $modMutasi->mutasibrg_id));
            $this->render('manajemenAsset.views.mutasibrgT.detailInformasi', array(
                'modMutasi' => $modMutasi,
                'modDetailMutasi' => $modDetailMutasi,
            ));
        }
    }
    
    public function actionPrint($id) {
        $this->layout='//layouts/printWindows';
        $judulLaporan='Data Mutasi Barang';
        $caraPrint = $_REQUEST['caraPrint'];
        $modMutasi = MutasibrgT::model()->findByPk($id);
        if (count($modMutasi) == 1){
            $modDetailMutasi = MutasibrgdetailT::model()->findAllByAttributes(array('mutasibrg_id' => $modMutasi->mutasibrg_id));
            $this->render('manajemenAsset.views.mutasibrgT.detailInformasi', array(
                'judulLaporan'=>$judulLaporan,
                'modMutasi' => $modMutasi,
                'modDetailMutasi' => $modDetailMutasi,
                'caraPrint'=>$caraPrint,
            ));
        }
    }
    
    public function actionBatalMutasiBarang($id){
        $this->layout = '//layouts/frameDialog';
        if (isset($_POST['BatalmutasibrgT'])){
            $modBatals = $this->validateTableBatal($_POST['BatalmutasibrgT']);
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $success = true;
                $modBatals = $this->validateTableBatal($_POST['BatalmutasibrgT']);
                foreach ($modBatals as $i => $data) {
                    if ($data->qty_batal > 0){
                        $modInventaris = InventarisasiruanganT::model()->findByAttributes(array('barang_id'=>$data->barang_id),array('order'=>'tgltransaksi', 'limit'=>1));
                        if ($data->save()) {
                            InventarisasiruanganT::kembalikanStok($data->qty_batal, $data->barang_id);
                            MutasibrgdetailT::model()->updateByPk($_POST['BatalmutasibrgT']['barang_id'][$i]['mutasibrgdetail_id'], array('batalmutasibrg_id'=>$data->batalmutasibrg_id));
                            InventarisasiruanganT::model()->updateAll(array('batalmutasibrg_id'=>$data->batalmutasibrg_id),'mutasibrgdetail_id = '.$_POST['BatalmutasibrgT']['barang_id'][$i]['mutasibrgdetail_id'].' and barang_id = '.$data->barang_id);
                        } else {
                            $success = false;
                        }
                    }
                }

                if ($success == true) {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                    $this->refresh();
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($ex, true));
            }
        }
        $modMutasi = MutasibrgT::model()->find('mutasibrg_id  = '.$id);
        $modDetailMutasi = BatalmutasibrgT::model()->findAll('mutasibrg_id = '.$id);
        if ((count($modMutasi) == 1) && (count($modDetailMutasi) < 1)){
            $model = new BatalmutasibrgT();
            $model->tglbatalmutasibrg = date('Y-m-d H:i:s');
            $modDetailMutasi = MutasibrgdetailT::model()->findAllByAttributes(array('mutasibrg_id' => $modMutasi->mutasibrg_id));
            $modMutasi->ruangan_nama = $modMutasi->ruangtujuan->ruangan_nama;
            $model->mutasibrg_id = $modMutasi->mutasibrg_id;
            $this->render('manajemenAsset.views.mutasibrgT.batalMutasi', array(
                'modBatals'=>$modBatals,
                'model'=>$model,
                'modMutasi' => $modMutasi,
                'modDetailMutasi' => $modDetailMutasi,
            ));
        }
    }
    
    protected function validateTableBatal($datas){
        $valid = true;
        foreach ($datas['barang_id'] as $i=>$data){
            $modDetails[$i] = new BatalmutasibrgT();
            $modDetails[$i]->attributes = $data;
            $modDetails[$i]->alasan_pembatalan = $datas['alasan_pembatalan'];
            $modDetails[$i]->mutasibrg_id = $datas['mutasibrg_id'];
            $modDetails[$i]->tglbatalmutasibrg = $datas['tglbatalmutasibrg'];
            $valid = $modDetails[$i]->validate() && $valid;
        }
        return $modDetails;
    }
}
    