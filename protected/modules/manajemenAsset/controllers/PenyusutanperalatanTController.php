<?php

class PenyusutanperalatanTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
    public $defaultAction = 'index';
    protected $successSave = true;
    protected $pesan = "succes";
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function actionIndex()
	{
		$model 			= new GUInvperalatanT;
        $modPenyusutan 	= new PenyusutanperalatanT;
        $format = new CustomFormat();

		if(isset($_POST['PenyusutanperalatanT']))
		{
			$penyusutan     = $_POST['PenyusutanperalatanT'];
			$status = false;
			foreach ($penyusutan as $key => $data) {
				$modPenyusutan = new PenyusutanperalatanT();
				$modPenyusutan->attributes = $data;
				$modPenyusutan->bbnpenyusutanbrjlnper = MyFunction::number_unformat($data['bbnpenyusutanbrjlngdng']);
				$modPenyusutan->akumpenyusutanper = MyFunction::number_unformat($data['akumpenyusutanper']);
				$modPenyusutan->nilaibukuperalatan = MyFunction::number_unformat($data['nilaibukuperalatan']);
				$modPenyusutan->ruangan_id = Yii::app()->user->getState('ruangan_id');
				$modPenyusutan->invperalatan_id = $data['invperalatan_id'];
				$modPenyusutan->pegawai_id = Yii::app()->user->pegawai_id;
				$modPenyusutan->tglpenyusutanperalatan = $format->formatDateMediumForDB($_POST['GUInvperalatanT']['tglpenyusutanperalatan']); 

				$model = GUInvperalatanT::model()->findbyPk($data['invperalatan_id']);
				$modPenyusutan->hargaperolehanperalatan = $model->invperalatan_harga; 
				$modPenyusutan->nilairesiduperalatan = empty($model->invperalatan_nilairesidu) ? 0 : $model->invperalatan_nilairesidu; 
				$modPenyusutan->tglterimaperalatan = $format->formatDateMediumForDB($model->invperalatan_tglguna);  
				$modPenyusutan->create_time = date('Y-m-d');
				$modPenyusutan->update_time = date('Y-m-d');
				$modPenyusutan->create_ruangan = Yii::app()->user->getState('ruangan_id');
				$modPenyusutan->create_loginpemakai_id = Yii::app()->user->id;
				$modPenyusutan->umurekonmisbln = 1;

				if($modPenyusutan->validate()){
					$modPenyusutan->save();
					GUInvperalatanT::model()->updateByPk($data['invperalatan_id'], array('invperalatan_akumsusut'=>$modPenyusutan->akumpenyusutanper));
					$status = true;
				}
			}

			if($status){
				$model->tglpenghapusan = $format->formatDateMediumForDB($_POST['GUInvperalatanT']['tglpenyusutanperalatan']);
				$uraian = 'Penyusutan';
				$modJurnalRekening = $this->saveJurnalRekening($model, $_POST['GUInvperalatanT'], $uraian);
	            $noUrut = 0;

	            foreach($_POST['JenispengeluaranrekeningV'] AS $i => $post){
	                $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
	                $noUrut ++;
	            }

				Yii::app()->user->setFlash('success',"Penyusutan berhasil disimpan!"); 
				$model->isNewRecord = False;
				$this->redirect(array('index','status'=>$status));
			}
			
		}

		$this->render('index',array(
			'model'=>$model,'modPenyusutan'=>$modPenyusutan,
		));
	}

	public function actionhitungsusut()
	{
		if(Yii::app()->getRequest()->getIsAjaxRequest()) 
		{
			$tglpenyusutanperalatan 	= $_POST['tglpenyusutanperalatan'];
            $jenisinventaris 			= $_POST['jenisinventaris'];
            $format = new CustomFormat();
            $tglpenyusutan = $format->formatDateMediumForDB($tglpenyusutanperalatan);
            $tgl_penyusutan = explode("-", $tglpenyusutan);
            $bulan_susut	= $tgl_penyusutan[1];
            $tahun_susut	= $tgl_penyusutan[0];

            $ceksql="SELECT invperalatan_t.invperalatan_id, invperalatan_t.barang_id, invperalatan_t.invperalatan_kode, invperalatan_t.invperalatan_noregister, invperalatan_t.invperalatan_namabrg, invperalatan_t.invperalatan_harga,invperalatan_t.invperalatan_akumsusut, invperalatan_t.invperalatan_umurekonomis, invperalatan_t.invperalatan_nilairesidu,invperalatan_t.invperalatan_tglguna,count(penyusutanperalatan_t.*) as jmlpenyusutan, max(penyusutanperalatan_t.bulanpenyusutanperalatan) as bulansusut, SUM(penyusutanperalatan_t.bbnpenyusutanbrjlnper) as akumpenyusutanper FROM invperalatan_t LEFT JOIN penyusutanperalatan_t ON penyusutanperalatan_t.invperalatan_id = invperalatan_t.invperalatan_id WHERE split_part(invperalatan_t.invperalatan_noregister, '-',3)='$jenisinventaris' GROUP BY invperalatan_t.invperalatan_id, invperalatan_t.barang_id, invperalatan_t.invperalatan_kode, invperalatan_t.invperalatan_noregister, invperalatan_t.invperalatan_namabrg, invperalatan_t.invperalatan_harga,invperalatan_t.invperalatan_akumsusut, invperalatan_t.invperalatan_umurekonomis, invperalatan_t.invperalatan_nilairesidu,invperalatan_t.invperalatan_tglguna";
            $ceksusut = Yii::app()->db->createCommand($ceksql)->queryAll();

            $i = 0;
            foreach ($ceksusut as $key => $value) {
            	$jmlpenyusutan 		= $value['jmlpenyusutan'];
            	$umurekonomis 		= $value['invperalatan_umurekonomis'];
            	$bulansusutakhir 	= $value['bulansusut'];
            	$tglguna 			= $value['invperalatan_tglguna'];

            	if($jmlpenyusutan < $umurekonomis){
            		if(empty($bulansusutakhir)){
            			$invperalatan_tglgunaperalatan = $tglguna; 
            		}else{
            			$invperalatan_tglgunaperalatan = $bulansusutakhir; 
            		}

            		$invperalatan_tglguna = explode("-", $invperalatan_tglgunaperalatan);
		            $bulan_guna		= $invperalatan_tglguna[1];
		            $tahun_guna		= $invperalatan_tglguna[0];

		            $selisihtahun = intval($tahun_susut)-intval($tahun_guna);
		            if(empty($ceksusut)){
			            if($selisihtahun > 0){
			            	$jml_bulansusut = $selisihtahun * 12 + intval($bulan_susut) + 1;
			            }else{
			            	$jml_bulansusut = intval($bulan_susut) + 1;
			            }
			        }else{
			        	if($selisihtahun > 0){
			            	$jml_bulansusut = $selisihtahun * 12 + intval($bulan_susut);
			            }else{
			            	$jml_bulansusut = intval($bulan_susut);
			            }
			        }

			        $jml_penyusutan = $jml_bulansusut - intval($bulan_guna);

			        $data[$i]['namabarang'] = $value['invperalatan_namabrg'];
			        $data[$i]['noregister'] = $value['invperalatan_noregister'];
			        $data[$i]['jml_penyusutan'] = $jml_penyusutan;
			        $data[$i]['harga_perolehan'] = $value['invperalatan_harga'];
			        $data[$i]['akum_susut'] = $value['invperalatan_akumsusut'];
			        $data[$i]['umurekonomis'] = $value['invperalatan_umurekonomis'];
			        $data[$i]['nilairesidu'] = $value['invperalatan_nilairesidu'];
			        $data[$i]['bulansusut'] = $invperalatan_tglgunaperalatan;
			        $data[$i]['invperalatan_id'] = $value['invperalatan_id'];
			        $data[$i]['invperalatan_harga'] = $value['invperalatan_harga'];
			        $data[$i]['akumpenyusutanper'] = $value['akumpenyusutanper'];
			        $data[$i]['cekbulansusut'] = $value['bulansusut'];

			        $i++;
            	}

            }

            if(count($data)>0){
                echo CJSON::encode(
                    $this->renderPartial('manajemenAsset.views.penyusutanperalatanT._rowPenyusutan', array('data'=>$data, 'jml_penyusutan'=>$jml_penyusutan), true)
                );               	                       	
            }else{
            	$data['bulan_guna'] = 0;
            	$data['baru'] = "false";
            	echo json_encode($data);
            }

			Yii::app()->end();
		}
	}

	protected function saveJurnalRekening($model, $postPenUmum, $uraian=null)
    {
        $modJurnalRekening = new JurnalrekeningT;
        $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');
        $modJurnalRekening->nobuktijurnal = Generator::noBuktiJurnalRek();
        $modJurnalRekening->kodejurnal = Generator::kodeJurnalRek();
        $modJurnalRekening->noreferensi = 0;
        $modJurnalRekening->tglreferensi = $model->tglpenghapusan;
        $modJurnalRekening->nobku = "";
        $modJurnalRekening->urianjurnal = $uraian." ".$model->tglpenghapusan;
        
        $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENGELUARAN_KAS;
        $periodeID = Yii::app()->session['periodeID'];
        $modJurnalRekening->rekperiod_id = $periodeID[0];
        $modJurnalRekening->create_time = date('Y-m-d H:i:s');
        $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
        $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
        $modJurnalRekening->ruangan_id = Yii::app()->user->getState('ruangan_id');
        
        if($modJurnalRekening->validate()){
            $modJurnalRekening->save();
            $this->successSave = true;
        } else {
            $this->successSave = false;
            $this->pesan = $modJurnalRekening->getErrors();
        }

        return $modJurnalRekening;
    }

    public function saveJurnalDetail($modJurnalRekening, $post, $noUrut=0, $modJurnalPosting){
        $modJurnalDetail = new JurnaldetailT();
        $modJurnalDetail->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
        $modJurnalDetail->rekperiod_id = $modJurnalRekening->rekperiod_id;
        $modJurnalDetail->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
        $modJurnalDetail->uraiantransaksi = $modJurnalRekening->urianjurnal;
        $modJurnalDetail->saldodebit = MyFunction::number_unformat($post['saldodebit']);
        $modJurnalDetail->saldokredit = MyFunction::number_unformat($post['saldokredit']);
        $modJurnalDetail->nourut = $noUrut;
        $modJurnalDetail->rekening1_id = $post['struktur_id'];
        $modJurnalDetail->rekening2_id = $post['kelompok_id'];
        $modJurnalDetail->rekening3_id = $post['jenis_id'];
        $modJurnalDetail->rekening4_id = $post['obyek_id'];
        $modJurnalDetail->rekening5_id = $post['rincianobyek_id'];
        $modJurnalDetail->catatan = "";

        if($modJurnalDetail->validate()){
            $modJurnalDetail->save();
        }
        return $modJurnalDetail;        
    }

}

?>