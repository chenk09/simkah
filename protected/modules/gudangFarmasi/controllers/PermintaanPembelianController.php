<?php

class PermintaanPembelianController extends SBaseController
{
    private $_validasi = true;
    private $_message = '';
         public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','search','details', 'print'),
				'users'=>array('@'),
			),
		
		);
	}
        
        
        /**
         * method untuk permintaan pembelian
         * digunakan di :
         * 1. gudang farmasi -> transaksi -> permintaan pembelian
         * 2. gudang farmasi -> informasi rencana kebutuhan -> pembelian
         * 3. gudang farmasi -> informasi permintaan penawaran -> pembelian
         * @param int $idPenawaran permintaanpenawaran_id default null
         * @param int $idRencana rencanakebutuhan_id default null
         * @param int $idPembelian permintaanpembelian_id default null
         */
	public function actionIndex($idPenawaran = null,$idRencana = null, $idPembelian=null)
	{
                $modPermintaanPembelian = new GFPermintaanPembelianT;
                $modUangMuka = new GFUangMukaBeliT;
                $modPermintaanPembelian->istermasukppn=0;
                $modPermintaanPembelian->istermasukpph=0;
                $modPermintaanPembelian->nopermintaan=Generator::noPembelian();
                $modPermintaanPembelian->tglpermintaanpembelian=date('Y-m-d H:i:s');
                $modRencanaKebFarmasi = null;
                $modPermintaanPenawaran = null;
                $modPermintaanDetail = null;
//                $modUangMuka = null;
                $var = array();
                $var['supplier_id']=0;
//                $var['isUangMuka'] = false;
                $format = new CustomFormat();
                $jumlahPermintaan = 0;
                if (!empty($idPenawaran) && (empty($idPembelian))){
                    $modPermintaanPenawaran = GFPermintaanPenawaranT::model()->findByPk($idPenawaran);
                    $jumlahPermintaan = PermintaanpembelianT::model()->countByAttributes(array('permintaanpenawaran_id'=>$modPermintaanPenawaran->permintaanpenawaran_id));
                    $modPermintaanPembelian->attributes = $modPermintaanPenawaran->attributes;
                    if (count($modPermintaanPenawaran)==1){
                        $modPenawaranDetail = GFPenawaranDetailT::model()->findAllByAttributes(array('permintaanpenawaran_id'=>$modPermintaanPenawaran->permintaanpenawaran_id));
                        if (count($modPenawaranDetail) > 0){
                            $totalSebelumDiskon = 0;
                            foreach ($modPenawaranDetail as $key => $value) {
                                $obat = ObatalkesM::model()->findByPk($value->obatalkes_id);
                                $stok = StokobatalkesT::getStokBarang($obat->obatalkes_id, Yii::app()->user->getState('ruangan_id'));
                                $jmlKemasan = ($obat->kemasanbesar > 0) ? $obat->kemasanbesar : 1;
                                $maxStok = StokobatalkesT::MaxStokInOut($value->obatalkes_id, Yii::app()->user->getState('ruangan_id'),'out');
                                $modPermintaanDetail[$key] = new PermintaandetailT();
                                $modPermintaanDetail[$key]->attributes = $value->attributes;
                                $modPermintaanDetail[$key]->stokakhir = $stok;
                                $modPermintaanDetail[$key]->jmlPenawaran = $value->qty;
                                $modPermintaanDetail[$key]->jmlpermintaan = $value->qty;
                                $modPermintaanDetail[$key]->harganettoper = $value->harganetto;
                                $modPermintaanDetail[$key]->satuankecil_id = empty($value->satuankecil_id) ? $obat->satuankecil_id : $value->satuankecil_id;
                                $modPermintaanDetail[$key]->satuanbesar_id = empty($value->satuanbesar_id) ? $obat->satuanbesar_id : $value->satuanbesar_id;
                                $modPermintaanDetail[$key]->hargabelibesar = $value->hargabelibesar;
                                $modPermintaanDetail[$key]->jmlkemasan = $value->jmlkemasan;
                                $modPermintaanDetail[$key]->tglkadaluarsa = $obat->tglkadaluarsa;
                                $modPermintaanDetail[$key]->persendiscount = $obat->discount;
                                $modPermintaanDetail[$key]->jmldiscount = ($modPermintaanDetail[$key]->jmlpermintaan * $modPermintaanDetail[$key]->hargabelibesar * $modPermintaanDetail[$key]->persendiscount/100);
                                $modPermintaanDetail[$key]->maksimalstok = $maxStok;
                                $modPermintaanDetail[$key]->hargappnper = 0;
                                $modPermintaanDetail[$key]->hargasatuanper = 0;
                                $modPermintaanDetail[$key]->hargapphper = 0;
                                $modPermintaanDetail[$key]->biaya_lainlain = 0;
                                $totalSebelumDiskon += $modPermintaanDetail[$key]->jmlpermintaan*$modPermintaanDetail[$key]->harganettoper;
                            }   
                        }
                    }
                }else if (!empty($idRencana) && (empty($idPembelian))){
                    $modRencanaKebFarmasi = GFRencanaKebFarmasiT::model()->findByPk($idRencana);
                    if (count($modRencanaKebFarmasi) == 1){
                        $modRencanaDetail = GFRencDetailkebT::model()->findAllByAttributes(array('rencanakebfarmasi_id'=>$modRencanaKebFarmasi->rencanakebfarmasi_id));
                        if (count($modRencanaDetail) > 0){
                            $totalSebelumDiskon = 0;
                            foreach ($modRencanaDetail as $key => $value) {
                                $obat = ObatalkesM::model()->findByPk($value->obatalkes_id);
                                $stok = StokobatalkesT::getStokBarang($obat->obatalkes_id, Yii::app()->user->getState('ruangan_id'));
                                $jmlKemasan = ($value->jmlkemasan > 0) ? $value->jmlkemasan : $obat->kemasanbesar;
                                $maxStok = StokobatalkesT::MaxStokInOut($value->obatalkes_id, Yii::app()->user->getState('ruangan_id'),'out');
                                $modPermintaanDetail[$key] = new PermintaandetailT();
                                $modPermintaanDetail[$key]->attributes = $value->attributes;
                                $modPermintaanDetail[$key]->stokakhir = $stok;
                                $modPermintaanDetail[$key]->jmlpermintaan = $value->jmlpermintaan;
                                $modPermintaanDetail[$key]->harganettoper = $value->harganettorenc;
                                $modPermintaanDetail[$key]->satuankecil_id = empty($value->satuankecil_id) ? $obat->satuankecil_id : $value->satuankecil_id;
                                $modPermintaanDetail[$key]->satuanbesar_id = empty($value->satuanbesar_id) ? $obat->satuanbesar_id : $value->satuanbesar_id;
                                $modPermintaanDetail[$key]->hargabelibesar = $value->harganettorenc * $value->jmlkemasan; //krn harga beli besar belum di tentukan di rencana
                                $modPermintaanDetail[$key]->jmlkemasan = $value->jmlkemasan;
                                $modPermintaanDetail[$key]->tglkadaluarsa = $value->tglkadaluarsa;
                                $modPermintaanDetail[$key]->persendiscount = $obat->discount;
                                $modPermintaanDetail[$key]->jmldiscount = ($modPermintaanDetail[$key]->jmlpermintaan * $modPermintaanDetail[$key]->hargabelibesar * $modPermintaanDetail[$key]->persendiscount/100);
                                $modPermintaanDetail[$key]->maksimalstok = $maxStok;
                                $modPermintaanDetail[$key]->hargappnper = (!empty($value->hargappnrenc)) ? $value->hargappnrenc : 0;
                                $modPermintaanDetail[$key]->hargasatuanper = 0;
                                $modPermintaanDetail[$key]->hargapphper = (!empty($value->hargapphnrenc)) ? $value->hargapphrenc : 0;
                                $modPermintaanDetail[$key]->biaya_lainlain = 0;
                                $totalSebelumDiskon += $modPermintaanDetail[$key]->jmlpermintaan*$modPermintaanDetail[$key]->harganettoper;
                            }
                        }
                    }
                }else if (!empty($idPembelian)){
                    $modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($idPembelian);
//                    $modUangMuka = GFUangMukaBeliT::model()->find('permintaanpembelian_id ='.$idPembelian);
                    if ((boolean)count($modPermintaanPembelian)){
                        $modPermintaanDetail = GFPermintaanDetailT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$idPembelian));
                    }
                }
                
                $var['totalSebelumDiskon'] = (!empty($totalSebelumDiskon)) ? $totalSebelumDiskon : 0;
                $var['jumlahDiskon'] = 0;
                $var['diskon'] = 0;
                $var['totalSetelahDiskon'] = 0;
                $var['totalPPN'] = 0;
                $var['totalPPH'] = 0;
                $var['termasukPPH'] = $var['termasukPPN'] = false;
                
                if(isset($_POST['GFPermintaanPembelianT']) && (($jumlahPermintaan < 1)) && (empty($idPembelian))){
                    $var['totalSebelumDiskon'] = (!empty($_POST['totalSebelumDiskon']) ? $_POST['totalSebelumDiskon'] : 0) ;
                    $var['diskon'] = (!empty($_POST['diskon']) ? $_POST['diskon'] : 0) ;
                    $var['totalSetelahDiskon'] = (!empty($_POST['totalSetelahDiskon']) ? $_POST['totalSetelahDiskon'] : 0) ;
                    $var['jumlahDiskon'] = (!empty($_POST['jumlahDiskon']) ? $_POST['jumlahDiskon'] : 0) ;
                    $var['totalPPN'] = (!empty($_POST['totalPPN']) ? $_POST['totalPPN'] : 0) ;
                    $var['totalPPH'] = (!empty($_POST['totalPPH']) ? $_POST['totalPPH'] : 0) ;
                    $var['termasukPPN'] = ((isset($_POST['termasukPPN'])) ? (($_POST['termasukPPN'] == 1) ? true : false) : null);
                    $var['termasukPPH'] = ((isset($_POST['termasukPPN'])) ? (($_POST['termasukPPH'] == 1) ? true : false) : null);
                    $var['totalPPH'] = (!empty($_POST['totalPPH']) ? $_POST['totalPPH'] : 0) ;
                    
                    $modPermintaanDetail = $this->validasiTabular($_POST['PermintaandetailT']);
                    $var['isUangMuka'] = ((isset($_POST['isUangMuka'])) ? (($_POST['isUangMuka'] == 1) ? true : null) : null);
                 $transaction = Yii::app()->db->beginTransaction();
                 try {    
                    $modPermintaanPembelian = new GFPermintaanPembelianT;
                    $modPermintaanPembelian->attributes=$_POST['GFPermintaanPembelianT'];
                    $modPermintaanPembelian->rencanakebfarmasi_id = $_POST['GFRencanaKebFarmasiT']['rencanakebfarmasi_id'];

                    $var['supplier_id'] = (isset($modPermintaanPembelian->supplier_id)) ? $modPermintaanPembelian->supplier_id : $var['supplier_id'];
//                    $modPermintaanPembelian->pegawai_id=Yii::app()->user->id;
                    //Pencarian pegawai_id berdasarkan login
                    $modLoginPemakai = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
                    $modPermintaanPembelian->pegawai_id = $modLoginPemakai->pegawai_id;
                    $modPermintaanPembelian->instalasi_id=Yii::app()->user->getState('instalasi_id');
                    $modPermintaanPembelian->ruangan_id=Yii::app()->user->getState('ruangan_id');
                    $modPermintaanPembelian->tglpermintaanpembelian=$format->formatDateTimeMediumForDB($_POST['GFPermintaanPembelianT']['tglpermintaanpembelian']);                    
                    $jumlahObat=COUNT($modPermintaanDetail);

                    $modUpdateObatAlkesM = GFObatalkesM::model()->findByAttributes(array('obatalkes_id'=>$modPermintaanDetail[5]->obatalkes_id));
                    
                    if($modPermintaanPembelian->save()){//Jika Model Rencana Berhasil Disimpan
                          if (isset($_POST['isUangMuka'])){
                                if ($_POST['isUangMuka'] == '1') {//Jika Uang Muka Dipilih
                                    $modUangMuka->attributes = $_POST['GFUangMukaBeliT'];
                                    $modUangMuka->supplier_id = $_POST['GFPermintaanPembelianT']['supplier_id'];
                                    $modUangMuka->permintaanpembelian_id = $modPermintaanPembelian->permintaanpembelian_id;
                                    if ($modUangMuka->validate()){
//                                        echo "<pre>";
//                                        echo print_r($modUangMuka->getAttributes());exit;
                                        if (!$modUangMuka->save()){
                                            $this->_message = 'Uang muka gagal disimpan';
                                        }
                                    }else{
//                                        echo "nonos";
//                                        echo print_r($modUangMuka->getAttributes());exit;
                                        $this->_message = 'Uang muka gagal disimpan';
                                    }
                                }
                            }
                          $jumlahCek=0;//digunakan untuk mengecek jumlah obat yang dipilih dengan jumlah yang tersimpan
                          if ($jumlahObat > 0){
                              
                              foreach ($modPermintaanDetail as $key => $value) {
                                  $value->permintaanpembelian_id=$modPermintaanPembelian->permintaanpembelian_id;
                                  if (!$value->hasErrors()){
                                      if($value->save()){
                                           $jumlahCek++; 
//                                           GFObatalkesM::model()->UpdateByPk($value->obatalkes_id,array('harganetto'=>$value->harganettoper, 'ppn_persen'=>$value->hargappnper));
//                                           ERROR + BAHAYA: BISA UPDATE SELURUH DATA MASTER, ichan >> GFObatalkesM::model()->UpdateAll(array('harganetto' =>$value->harganettoper),'obatalkes_id=:obatalkes_id',array(':obatalkes_id' =>$value->obatalkes_id));
//                                           ERROR + BAHAYA: BISA UPDATE SELURUH DATA MASTER, ichan  >> ObatsupplierM::model()->UpdateAll(array('harganettoppn' =>$value->harganettoper),'obatalkes_id=:obatalkes_id and supplier_id=:supplier_id',array(':obatalkes_id'=>$value->obatalkes_id,':supplier_id'=>$modPermintaanPembelian->supplier_id));
//                                            
//                                            ==== Harusnya Master Obat Supplier dan Obat Alkes di load dulu untuk di update supaya data yang di updatenya jelas
//                                            $modUpdateObatSupplierM = ObatsupplierM::model()->findByAttributes(array(
//                                                      'supplier_id'=>$modPermintaanPembelian->supplier_id,
//                                                      'obatalkes_id'=>$modPermintaanDetail[5]->obatalkes_id,
//                                                    ));
//                                            $modUpdateObatSupplierM->[attributes yang mau diupdate] = [Data untuk diupdate]
//                                            $modUpdateObatAlkesM->update();
//                                            ====
                                            
                                      }
                                  }
                              }
                          }
                    }
//                    $jumlahObat = 0;
                    if (($jumlahObat == $jumlahCek)&&($jumlahObat > 0)&&($jumlahCek>0)){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Permintaan Berhasil Disimpan");
                        $this->redirect(array('index','idPembelian'=>$modPermintaanPembelian->permintaanpembelian_id));
                    }else{
                        $modPermintaanPembelian->isNewRecord = true; 
                        if ($jumlahObat < 1){
                            $modPermintaanPembelian->addError('error', "Detail obat alkes harus dipilih");
                        }
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                         
                }catch(Exception $exc){
                    
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }    
            }
                
        $modPermintaanPembelian->tglpermintaanpembelian = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($modPermintaanPembelian->tglpermintaanpembelian, 'yyyy-MM-dd'));
        
        if(isset($_GET['GFPermintaanPenawaranT'])) {
            $modPermintaanPenawaran->attributes = $_GET['GFPermintaanPenawaranT'];
            $modPermintaanPenawaran->tglpenawaran=$format->formatDateMediumForDB($_GET['GFPermintaanPenawaranT']['tglpenawaran']);
        }
            
        if(isset($_GET['GFRencanaKebFarmasiT'])) {
            $modRencanaKebFarmasi->attributes = $_GET['GFRencanaKebFarmasiT'];
            $modRencanaKebFarmasi->tglperencanaan=$format->formatDateMediumForDB($_GET['GFRencanaKebFarmasiT']['tglperencanaan']);
        }    
        
        $this->render('index',array('modPermintaanPembelian'=>$modPermintaanPembelian,
                                    'modRencanaKebFarmasi'=>$modRencanaKebFarmasi,
                                    'modPermintaanDetail'=>$modPermintaanDetail,
                                    'modPermintaanPenawaran'=>$modPermintaanPenawaran,
                                    'modUangMuka'=>$modUangMuka,
                                    'var'=>$var
                                    ));
	}
        
        /**
         * method untuk validasi tabular
         * @param array $datas
         * @return PenawarandetailT 
         */
        private function validasiTabular($datas){
            $models = null;
            $format = new CustomFormat();
            if (count($datas) > 0){
                foreach ($datas as $key => $data) {
                    $models[$key] = new PermintaandetailT();
                    $models[$key]->attributes = $data;
                    $models[$key]->permintaanpembelian_id = 1; //fake id
                    $models[$key]->jmlpermintaan = $data['jmlpermintaan']; //fake id
                    $diskon = (($models[$key]->harganettoper > 0) && ($models[$key]->persendiscount)) ? ($models[$key]->harganettoper*$models[$key]->persendiscount)/100 : 0;
                    $models[$key]->hargasatuanper=($models[$key]->harganettoper + $models[$key]->hargappnper + $models[$key]->hargapphper ) - $diskon;
                    if(!empty($data['tglkadaluarsa'])){
                        $models[$key]->tglkadaluarsa=$format->formatDateMediumForDB($data['tglkadaluarsa']);
                    }else{
                        $models[$key]->tglkadaluarsa=null;
                    }
                    $models[$key]->validate();
                    if ($models[$key]->jmlpermintaan < 1){
                        $models[$key]->addError('$models[$key]', 'Jumlah Permintaan tidak boleh kurang dari 1');
                    }
                }
            }
            return $models;
        }
        
        public function actionSearch()
        {
            $modPermintaanPembelian = new GFPermintaanPembelianT;
            $format = new CustomFormat();
            $modPermintaanPembelian->tglAwal=date('d M Y 00:00:00');
            $modPermintaanPembelian->tglAkhir=date('d M Y H:i:s');
            if(isset($_GET['GFPermintaanPembelianT'])){
                $modPermintaanPembelian->attributes=$_GET['GFPermintaanPembelianT'];
                $modPermintaanPembelian->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['GFPermintaanPembelianT']['tglAwal']);
                $modPermintaanPembelian->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFPermintaanPembelianT']['tglAkhir']);
            }
            
            $this->render('search',array('modPermintaanPembelian'=>$modPermintaanPembelian));
        }
        
        public function actionDetails($idPermintaanPembelian)
        {
            $this->layout='//layouts/frameDialog';
            $modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($idPermintaanPembelian);
            $modPermintaanPembelianDetails = GFPermintaanDetailT::model()->findAll('permintaanpembelian_id='.$idPermintaanPembelian.'');
        
            $this->render('details',array('modPermintaanPembelian'=>$modPermintaanPembelian,
                                          'modPermintaanPembelianDetails'=>$modPermintaanPembelianDetails));
            
        }
        
        /**
         * method untuk hasil print detail permintaan pembelian dengan parameter input id permintaan pembelian
         * digunakan pada halaman 
         * 1. Informasi Permintaan Pembelian -> detail permintaan pembelian -> print
         * 2. Transaksi Permintaan Pembelian setelah save tombol print akan muncul
         * 
         * @param int $idPembelian
         * @param string $caraPrint 
         * 
         * print detail permintaan pembelian
         */
        public function actionPrint($idPembelian,$caraPrint){
            $judulLaporan = 'Laporan Detail Permintaan Pembelian';
            $modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($idPembelian);
            $modPermintaanPembelianDetails = GFPermintaanDetailT::model()->findAll('permintaanpembelian_id=:idPembelian', array('idPembelian'=>$idPembelian));
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modPermintaanPembelian'=>$modPermintaanPembelian,
                                            'modPermintaanPembelianDetails'=>$modPermintaanPembelianDetails));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('modDetailRencana'=>$modDetailRencana,
                                            'judulLaporan'=>$judulLaporan,
                                            'modRencanaKebutuhan'=>$modRencanaKebutuhan));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('modDetailRencana'=>$modDetailRencana,
                                                                    'judulLaporan'=>$judulLaporan,
                                                                    'modRencanaKebutuhan'=>$modRencanaKebutuhan),true));
                $mpdf->Output();
            }   
        }

	public function actionBatal($permintaanpembelian_id = null)
	{
		if(is_null($permintaanpembelian_id))
		{
			Yii::app()->user->setFlash('error',"ID Penerimaan kosong, coba cek ulang");
			$this->redirect(array('search'));
		}
		
		$modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($permintaanpembelian_id);
		if(!$modPermintaanPembelian)
		{
			Yii::app()->user->setFlash('error',"ID tidak  terdaftar");
			$this->redirect(array('search'));
		}
		
		if($modPermintaanPembelian && !is_null($modPermintaanPembelian->penerimaanbarang_id))
		{
			Yii::app()->user->setFlash('error',"Barang sudah diterima, coba cek ulang");
			$this->redirect(array('search'));
		}
		
		$modPermintaanDetail = PermintaandetailT::model()->findAllByAttributes(array(
			"permintaanpembelian_id"=>$permintaanpembelian_id
		));
		
		$transaction = Yii::app()->db->beginTransaction();
		try{
			$pesan = array();
			foreach($modPermintaanDetail as $key=>$val)
			{
				$_modPermintaanDetail = PermintaandetailT::model()->findByPk($val->permintaandetail_id)->delete();
				if(!$_modPermintaanDetail)
				{
					foreach($_modPermintaanDetail->getErrors() as $a=>$b)
					{
						$pesan[] = $a . " : " . implode(", ", $b);
					}
				}
			}
			if(!$modPermintaanPembelian->delete())
			{
				foreach($modPermintaanPembelian->getErrors() as $a=>$b)
				{
					$pesan[] = $a . " : " . implode(", ", $b);
				}			
			}
			
			if(count($pesan) > 0)
			{
				throw new Exception(implode("<br>", $pesan));
			}else{
				$transaction->commit();
				Yii::app()->user->setFlash('success',"Data berhasil dibatalkan");
				$this->redirect(array('search'));
			}			
		}catch(Exception $exc){
			$transaction->rollback();
			Yii::app()->user->setFlash('error',"Data gagal dibatalkan ". MyExceptionMessage::getMessage($exc,true));
		}
		
	}	
		
}