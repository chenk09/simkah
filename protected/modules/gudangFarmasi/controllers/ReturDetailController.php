
<?php

class ReturDetailController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        
        
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('rencanaKebutuhan','search','details', 'print'),
				'users'=>array('@'),
			),
		
		);
	}


	/**
	 * Manages all models.
	 */
	public function actionSearch()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GFReturDetailT('searchGudangFarmasi  ');
                $format = new CustomFormat();
                $model->tglAwal = date('d M Y 00:00:00');
                $model->tglAkhir = date('d M Y  H:i:s');
//		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GFReturDetailT'])){
			$model->attributes=$_GET['GFReturDetailT'];
                        $model->tglAwal  = $format->formatDateMediumForDB($_REQUEST['GFReturDetailT']['tglAwal']);
                        $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['GFReturDetailT']['tglAkhir']);
                        $model->namaObat = $_GET['GFReturDetailT']['namaObat'];
                        $model->noRetur = $_GET['GFReturDetailT']['noRetur'];
                        $model->noFaktur = $_GET['GFReturDetailT']['noFaktur'];
                }
                
		$this->render('search',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GFReturDetailT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gfretur-detail-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
       
}
