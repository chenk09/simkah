<?php
class PenerimaanItemsController extends SBaseController {

    private $_validasi = true;
    private $_message = '';
    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'search', 'details', 'detailsFaktur'),
                'users' => array('@'),
            ),
        );
    }

    public function actionDetails($idPenerimaanBarang) {
        $this->layout = '//layouts/frameDialog';
        $modPenerimaan = GFPenerimaanBarangT::model()->findByPk($idPenerimaanBarang);
        $modPenerimaanDetails = GFPenerimaanDetailT::model()->findAll('penerimaanbarang_id=' . $idPenerimaanBarang . '');

        $this->render('details', array('modPenerimaan' => $modPenerimaan,
            'modPenerimaanDetails' => $modPenerimaanDetails));
    }

    public function actionDetailsFaktur($idFakturPembelian) {
        $this->layout = '//layouts/frameDialog';
        $modFakturPembelian = GFFakturPembelianT::model()->findByPk($idFakturPembelian);
        $modFakturPembelianDetails = GFFakturDetailT::model()->findAll('fakturpembelian_id=' . $idFakturPembelian . '');

        $this->render('detailsFaktur', array('modFakturPembelian' => $modFakturPembelian,
            'modFakturPembelianDetails' => $modFakturPembelianDetails));
    }

    public function actionSearch() {
        $modPenerimaanItems = new GFPenerimaanBarangT;
        $format = new CustomFormat();
        $modPenerimaanItems->tglAwal = date('d M Y 00:00:00');
        $modPenerimaanItems->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['GFPenerimaanBarangT'])) {
            $modPenerimaanItems->attributes = $_GET['GFPenerimaanBarangT'];
            $modPenerimaanItems->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GFPenerimaanBarangT']['tglAwal']);
            $modPenerimaanItems->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFPenerimaanBarangT']['tglAkhir']);
        }

        $this->render('search', array('modPenerimaanItems' => $modPenerimaanItems));
    }

    /**
     * method untuk url penerimaan items
     * digunakan di modul :
     * 1. gudang farmasi -> transaksi -> penerimaan items
     * 2. gudang farmasi -> informasi permintaan pembelian -> penerimaan
     * @param string $id // permintaanpembelian_id default null
     * @param type $idPenerimaan // penerimaanbarang_id default null
     */
    
	public function actionIndex($id = null)
	{
		$format = new CustomFormat();
        $konfigFarmasi = KonfigfarmasiK::model()->find();
		$modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($id);

		$modPenerimaanBarang = new GFPenerimaanBarangT;
        $modPenerimaanBarang->tglterima = date('d M Y H:i:s');
        $modPenerimaanBarang->tglsuratjalan = date('d M YH:i:s');
        $modPenerimaanBarang->noterima = Generator::noTerimaBarang();
		$modPenerimaanBarang->supplier_id = $modPermintaanPembelian->supplier->supplier_id;
		$modPenerimaanBarang->permintaanpembelian_id = $id;
		
		if(isset($_POST["GFPenerimaanBarangT"]))
		{
			$pesan = array();
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$modPenerimaanBarang->attributes = $_POST["GFPenerimaanBarangT"];
				$modPenerimaanBarang->gudangpenerima_id = Yii::app()->user->getState('ruangan_id');
				
				$total_harga = 0;
				$total_harga_netto = 0;
				$total_jml_discount = 0;
				$total_pajak_ppn = 0;
				$total_pajak_pph = 0;
				
				if(!$modPenerimaanBarang->validate())
				{
					foreach($modPenerimaanBarang->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}else{
					$modPenerimaanBarang->save();
					if(isset($_POST["GFPenerimaanDetailT"]))
					{
						foreach($_POST["GFPenerimaanDetailT"] as $key=>$val)
						{
							$modStokObatAlkes = new GFStokObatAlkesT;
							$obatalkesM = ObatalkesM::model()->findByPk($val['obatalkes_id']);
							foreach($obatalkesM as $x=>$z)
							{
								if($modStokObatAlkes->hasAttribute($x))
								{
									$modStokObatAlkes->$x = $z;
								}
							}
							$modStokObatAlkes->ruangan_id = Yii::app()->user->getState('ruangan_id');
							$modStokObatAlkes->tglkadaluarsa = $format->formatDateMediumForDB($val["tglkadaluarsa"]);
							$modStokObatAlkes->discount = $val["persendiscount"];
							$modStokObatAlkes->tglstok_in = $modPenerimaanBarang->tglterima;
							$modStokObatAlkes->tglstok_out = null;
							$modStokObatAlkes->qtystok_in = $val["jmlterima"] * $val["jmlkemasan"];
							$modStokObatAlkes->qtystok_out = 0;
							$modStokObatAlkes->qtystok_current = $modStokObatAlkes->qtystok_in;
							$modStokObatAlkes->harganetto_oa = $val["harganettoper"];
							$modStokObatAlkes->hargajual_oa = $obatalkesM->hargajual;
							
							$total_harga += (($modStokObatAlkes->qtystok_in * $val["harganettoper"]) + $val["hargappnper"]) - $val["jmldiscount"];
							$total_harga_netto += ($modStokObatAlkes->qtystok_in * $val["harganettoper"]);
							$total_jml_discount += $val["jmldiscount"];
							$total_pajak_ppn += $val["hargappnper"];
							$total_pajak_pph += $val["hargapphper"];
							
							if(!$modStokObatAlkes->validate())
							{
								foreach($modStokObatAlkes->getErrors() as $a=>$b)
								{
									$pesan[] = $a . " : " . implode(", ", $b);
								}								
							}else{
								$modStokObatAlkes->save();
								$GFPenerimaanDetailT = new GFPenerimaanDetailT;
								$GFPenerimaanDetailT->attributes = $_POST["GFPenerimaanDetailT"][$key];
								$GFPenerimaanDetailT->tglkadaluarsa = $format->formatDateMediumForDB($val["tglkadaluarsa"]);
								$GFPenerimaanDetailT->penerimaanbarang_id = $modPenerimaanBarang->penerimaanbarang_id;
								$GFPenerimaanDetailT->stokobatalkes_id = $modStokObatAlkes->stokobatalkes_id;
								
								if(!$GFPenerimaanDetailT->validate())
								{
									foreach($GFPenerimaanDetailT->getErrors() as $a=>$b)
									{
										$pesan[] = $a . " : " . implode(", ", $b);
									}
								}else{
									$GFPenerimaanDetailT->save();
									
									$stokObatAlkesT = GFStokObatAlkesT::model()->findByPk($modStokObatAlkes->stokobatalkes_id);
									$stokObatAlkesT->penerimaandetail_id = $GFPenerimaanDetailT->penerimaandetail_id;
									$stokObatAlkesT->save();
								}
							}
						}
					}
				}
				
				$penerimaanBarangT = GFPenerimaanBarangT::model()->findByPk($modPenerimaanBarang->penerimaanbarang_id);
				$penerimaanBarangT->harganetto = $total_harga_netto;
				$penerimaanBarangT->jmldiscount = $total_jml_discount;
				$penerimaanBarangT->persendiscount = 0;
				$penerimaanBarangT->totalpajakppn = $total_pajak_ppn;
				$penerimaanBarangT->totalpajakpph = $total_pajak_pph;
				$penerimaanBarangT->totalharga = $total_harga;
				if(!$penerimaanBarangT->save())
				{
					$pesan[] = "Update Penerimaan barang gagal";
				}
				
				$permintaanPembelianT = GFPermintaanPembelianT::model()->updateByPk($modPenerimaanBarang->permintaanpembelian_id, array(
					"penerimaanbarang_id" => $modPenerimaanBarang->penerimaanbarang_id
				));
				if(!$permintaanPembelianT)
				{
					$pesan[] = " penerimaanbarang_id : gagal";
				}
				
				if(count($pesan) > 0)
				{
					throw new Exception(implode("<br>", $pesan));
				}else{
					Yii::app()->user->setFlash('success', "Data berhasil disimpan");
					$transaction->commit();
					$this->redirect(array(
						'update', 'id'=>$modPenerimaanBarang->penerimaanbarang_id
					));
				}
			}catch(Exception $exc){
				$transaction->rollback();
				Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
			}
		}
		
        $this->render('penerimaan/create', array(
			'modPenerimaanBarang' => $modPenerimaanBarang,
            'modPermintaanPembelian' => $modPermintaanPembelian
		));
	}
	
	public function actionUpdate($id = null)
	{
		$modPenerimaanBarang = GFPenerimaanBarangT::model()->findByPk($id);
		$modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($modPenerimaanBarang->permintaanpembelian_id);
		
        $this->render('penerimaan/update', array(
			'modPenerimaanBarang' => $modPenerimaanBarang,
            'modPermintaanPembelian' => $modPermintaanPembelian
		));		
	}
	
	public function actionIndex_($id = null, $idPenerimaan=null)
	{
		//gudangpenerima_id
        $id = $_GET['id'];
        $konfigFarmasi = KonfigfarmasiK::model()->find();
        $modUangMuka = new GFUangMukaBeliT;
        $modPenerimaanBarang = new GFPenerimaanBarangT;
        $modPermintaanPembelian = new GFPermintaanPembelianT;
        $modFakturPembelian = new GFFakturPembelianT;
//        $modFakturPembelian->nofaktur=Generator::noFaktur();
        $modPenerimaanBarang->tglterima = date('d M Y H:i:s');
        $modPenerimaanBarang->tglsuratjalan = date('d M YH:i:s');
        $modPenerimaanDetail = null;
        $modObatAlkes = null;
        $modFakturPembelian->biayamaterai = 0;
        $modPenerimaanBarang->noterima = Generator::noTerimaBarang();

        $format = new CustomFormat();
        
        $scenario = 'insert';
        $var = array();
        if (isset($id) && (empty($idPenerimaan))) {
            $modPermintaanPembelian = GFPermintaanPembelianT::model()->findByAttributes(array('permintaanpembelian_id' => $id, 'penerimaanbarang_id' => null));
            if (count($modPermintaanPembelian) == 1) {
                $modPenerimaanBarang->supplier_id = $modPermintaanPembelian->supplier_id;
                $modPenerimaanBarang->permintaanpembelian_id = $id;
                $modPermintaanDetail = GFPermintaanDetailT::model()->findAllByAttributes(array('permintaanpembelian_id' => $modPermintaanPembelian->permintaanpembelian_id));
                if (count($modPermintaanDetail) > 0) {
                    foreach ($modPermintaanDetail as $counter => $detail) {
                        $modPenerimaanDetail[$counter] = new PenerimaandetailT;
                        $modPenerimaanDetail[$counter]->attributes = $detail->attributes;
                        $modPenerimaanDetail[$counter]->jmlterima = $detail->jmlpermintaan;
                        $modPenerimaanDetail[$counter]->jmlSudahTerima = $this->hitungJmlSudahTerima($detail->permintaanpembelian_id,$detail->obatalkes_id);
                    }
                }
                $scenario = "dariPermintaanPembelian";
            }else{
                $modPermintaanPembelian = new GFPermintaanPembelianT;
            }
        }else if (!empty($idPenerimaan)){
            $modPenerimaanBarang=GFPenerimaanBarangT::model()->findByPk($idPenerimaan);
            if ((boolean)count($modPenerimaanBarang)){
                $modUang = GFUangMukaBeliT::model()->findByAttributes(array('permintaanpembelian_id'=>$modPenerimaanBarang->permintaanpembelian_id));
                if ((boolean)count($modUang)){
                    $modUangMuka = $modUang;
                    $var['isUangMuka'] = true;
                }
                $modPenerimaanDetail = GFPenerimaanDetailT::model()->findAllByAttributes(array('penerimaanbarang_id'=>$modPenerimaanBarang->penerimaanbarang_id));
                if (isset($modPenerimaanBarang->fakturpembelian_id)){
                    $modFakturPembelian = GFFakturPembelianT::model()->findByPk($modPenerimaanBarang->fakturpembelian_id);
                    $var['isLangsung'] = true;
                }
            }
        }
        $modFakturPembelian->totharganetto = (empty($modFakturPembelian->totharganetto)) ? 0 : $modFakturPembelian->totharganetto;
        $modFakturPembelian->totalpajakppn = (empty($modFakturPembelian->totalpajakppn)) ? 0 : $modFakturPembelian->totalpajakppn;
        $modFakturPembelian->totalpajakpph = (empty($modFakturPembelian->totalpajakpph)) ? 0 : $modFakturPembelian->totalpajakpph;
        $modFakturPembelian->jmldiscount = (empty($modFakturPembelian->jmldiscount)) ? 0 : $modFakturPembelian->jmldiscount;
        $modFakturPembelian->totalhargabruto = (empty($modFakturPembelian->totalhargabruto)) ? 0 : $modFakturPembelian->totalhargabruto;
        
		if (isset($_POST['GFPenerimaanBarangT']) && (empty($idPenerimaan))) {
            $modUangMuka->attributes = $_POST['GFUangMukaBeliT'];
            $modFakturPembelian->attributes = $_POST['GFFakturPembelianT'];
            $modPenerimaanBarang->attributes = $_POST['GFPenerimaanBarangT'];
            $modPenerimaanBarang->tglterimafaktur = null;
            $modPenerimaanBarang->fakturpembelian_id = null;
            $modPenerimaanBarang->returpembelian_id = null;
            $modPenerimaanBarang->gudangpenerima_id = Yii::app()->user->getState('ruangan_id');
//            ===mulai
            $modPenerimaanBarang->harganetto = (!empty($modPenerimaanBarang->harganetto)) ? $modPenerimaanBarang->harganetto : $modFakturPembelian->totharganetto;
            $modPenerimaanBarang->jmldiscount = (!empty($modPenerimaanBarang->jmldiscount)) ? $modPenerimaanBarang->jmldiscount : $modFakturPembelian->jmldiscount;
//            setting diskon ppn pph nol dulu
            $modPenerimaanBarang->persendiscount = (!empty($modPenerimaanBarang->persendiscount)) ? $modPenerimaanBarang->persendiscount : 0;
            $modPenerimaanBarang->totalpajakppn = (!empty($modPenerimaanBarang->totalpajakppn)) ? $modPenerimaanBarang->totalpajakppn : $modFakturPembelian->totalpajakppn;
            $modPenerimaanBarang->totalpajakpph = (!empty($modPenerimaanBarang->totalpajakpph)) ? $modPenerimaanBarang->totalpajakpph : $modFakturPembelian->totalpajakpph;
            $modPenerimaanBarang->totalharga = (!empty($modPenerimaanBarang->totalharga)) ? $modPenerimaanBarang->totalharga : $modFakturPembelian->totalhargabruto;
//            ===akhir
                        
            $modPenerimaanBarang->harganetto = (isset($_POST['GFFakturPembelianT']['totharganetto'])) ? $_POST['GFFakturPembelianT']['totharganetto'] : $modFakturPembelian->totharganetto;
            $modPenerimaanBarang->jmldiscount = (isset($_POST['GFFakturPembelianT']['jmldiscount'])) ? $_POST['GFFakturPembelianT']['jmldiscount'] : $modFakturPembelian->jmldiscount;
            $modPenerimaanBarang->persendiscount = ((isset($_POST['GFFakturPembelianT']['persendiscount'])) ? $_POST['GFFakturPembelianT']['persendiscount'] : null);
            $modPenerimaanBarang->totalpajakppn = ((isset($_POST['GFFakturPembelianT']['totalpajakppn'])) ? $_POST['GFFakturPembelianT']['totalpajakppn'] : $modFakturPembelian->totalpajakppn);
            $modPenerimaanBarang->totalpajakpph = ((isset($_POST['GFFakturPembelianT']['totalpajakpph'])) ? $_POST['GFFakturPembelianT']['totalpajakpph'] : $modFakturPembelian->totalpajakpph);
            $modPenerimaanBarang->totalharga = ((isset($_POST['GFFakturPembelianT']['totalhargabruto'])) ? $_POST['GFFakturPembelianT']['totalhargabruto'] : $modFakturPembelian->totalhargabruto);
            
            if (isset($_POST['PenerimaandetailT'])) {
                $modPenerimaanDetail = $this->validateTabular($_POST['PenerimaandetailT'],$scenario);
            }
            $var['isLangsung'] = ((isset($_POST['isLangsungFaktur'])) ? (($_POST['isLangsungFaktur'] == 1) ? true : null) : null);;
            $var['isUangMuka'] = ((isset($_POST['isUangMuka'])) ? (($_POST['isUangMuka'] == 1) ? true : null) : null);
//                 $modPenerimaanBarang->tglkadaluarsa = $format->formatDateMediumForDB($_POST['PenerimaandetailT']['tglkadaluarsa'][$i]);
            if ($modPenerimaanBarang->validate()) {
              
//              =======digunakan untuk mengecek validasi persen diskon                
                    $modPenerimaanBarang->persendiscount = (!empty($_POST['GFFakturPembelianT']['persendiscount'])) ? $_POST['GFFakturPembelianT']['persendiscount'] : 0 ;                
                    $modPenerimaanBarang->totalpajakppn = (!empty($_POST['GFFakturPembelianT']['totalpajakppn'])) ? $_POST['GFFakturPembelianT']['totalpajakppn'] : 0 ;
//              ======
//                print_r($this->_validasi);
                if ($this->_validasi)
                {
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        if ($modPenerimaanBarang->save()) {
                            if (isset($_POST['isUangMuka'])){
                                if ($_POST['isUangMuka'] == '1') {//Jika Uang Muka Dipilih
                                    $modUangMuka->supplier_id = $modPenerimaanBarang->supplier_id;
                                    $modUangMuka->penerimaanbarang_id = $modPenerimaanBarang->penerimaanbarang_id;
                                    if ($modUangMuka->validate()){
                                        if (!$modUangMuka->save()){
                                            $this->_message = 'Uang muka gagal disimpan';
                                        }
                                    }else{
                                        $this->_message = 'Uang muka gagal disimpan';
                                    }
                                }
                            }
                            if (isset($id) || !empty($id)){
                                if($_POST['isPenerimaanTerakhir']){
                                    $update = GFPermintaanPembelianT::model()->updateByPk($id, array('penerimaanbarang_id' => $modPenerimaanBarang->penerimaanbarang_id));
                                }else{
                                    $update = GFPermintaanPembelianT::model()->updateByPk($id, array('penerimaanbarang_id' => $modPenerimaanBarang->penerimaanbarang_id));
                                    $update = true;
                                }
                                if (!$update){
                                    $this->_validasi = false;
                                    $this->_message = 'Update permintaan pembelian gagal';
                                }
                            }
                            //$jumlahObat = COUNT($_POST['PenerimaandetailT']);
//                            SORT INI JADI ERROR OBAT DAN JUMLAH JADI TIDAK COCOK DENGAN YANG DI INPUT
//                            sort($_POST['PenerimaandetailT']);
//                            sort($modPenerimaanDetail);
                            $idSupplier = $_REQUEST['idSupplier'];
//                          SALAH : ERROR SIMPAN >>>  for ($i = 0; $i < $jumlahObat; $i++):
                            foreach ($_POST['PenerimaandetailT'] AS $i => $postDetail):
                                $modStokObatAlkes = new GFStokObatAlkesT;
                                $jmlKemasan = (!empty($_POST['PenerimaandetailT'][$i]['jmlkemasan'])) ? $_POST['PenerimaandetailT'][$i]['jmlkemasan'] : 1;
                                $modStokObatAlkes->satuankecil_id = $_POST['PenerimaandetailT'][$i]['satuankecil_id'];
                                $modStokObatAlkes->obatalkes_id = $_POST['PenerimaandetailT'][$i]['obatalkes_id'];
                                $modStokObatAlkes->sumberdana_id = $_POST['PenerimaandetailT'][$i]['sumberdana_id'];
                                $modStokObatAlkes->ruangan_id = Yii::app()->user->getState('ruangan_id');
                                $modStokObatAlkes->tglstok_in = $modPenerimaanBarang->tglterima;
                                $modStokObatAlkes->tglstok_out = null;
                                $modStokObatAlkes->qtystok_in = $_POST['PenerimaandetailT'][$i]['jmlterima'] * $jmlKemasan;
                                $modStokObatAlkes->qtystok_out = 0;
                                $modStokObatAlkes->qtystok_current = $modStokObatAlkes->qtystok_in;
                                $modStokObatAlkes->harganetto_oa = $_POST['PenerimaandetailT'][$i]['harganettoper'];
                                //Jika Ada Persen Diskon Untuk Pasien
                                $modStokObatAlkes->discount = $_POST['PenerimaandetailT'][$i]['persendiscount'];
                                if($konfigFarmasi->persdiskpasien > 0){
                                    $modStokObatAlkes->discount = $modStokObatAlkes->discount * $konfigFarmasi->persdiskpasien / 100;
                                }else{
                                    $modStokObatAlkes->discount = 0;
                                }
                                if ($_POST['PenerimaandetailT'][$i]['tglkadaluarsa'] != '') {
                                    $modStokObatAlkes->tglkadaluarsa = $format->formatDateMediumForDB($_POST['PenerimaandetailT'][$i]['tglkadaluarsa']);
                                } else {
                                    $modStokObatAlkes->tglkadaluarsa = null;
                                }
                                $modObatTerakhir = ObatalkesM::model()->findByPk($modStokObatAlkes->obatalkes_id);
                                if(!empty($modObatTerakhir->obatalkes_id)){
                                    $modStokObatAlkes->hjaresep = $modObatTerakhir->hjaresep;
                                    $modStokObatAlkes->hjanonresep = $modObatTerakhir->hjanonresep;
                                    $modStokObatAlkes->marginresep = $modObatTerakhir->marginresep;
                                    $modStokObatAlkes->marginnonresep = $modObatTerakhir->marginnonresep;
                                    $modStokObatAlkes->hpp = $modObatTerakhir->hpp;
                                    $modStokObatAlkes->hargajual_oa = $modObatTerakhir->hargajual;
                                }
                                $modStokObatAlkes->create_time = date('Y-m-d H:i:s');
                                $modStokObatAlkes->create_loginpemakai_id = Yii::app()->user->id;
                                $modStokObatAlkes->create_ruangan = Yii::app()->user->getState('ruangan_id');
//                                echo "<pre>"; print_r($modStokObatAlkes->attributes); exit;
                                if ($modStokObatAlkes->save()) { //Jika Stok Obat Alkes Berhasil Disimpan
                                    $modPenerimaanDetail[$i]->attributes = $modStokObatAlkes->attributes;
                                    $modPenerimaanDetail[$i]->satuanbesar_id = (!empty($modPenerimaanDetail[$i]->satuanbesar_id)) ? $modPenerimaanDetail[$i]->satuanbesar_id : null;
                                    $modPenerimaanDetail[$i]->fakturdetail_id = (!empty($modPenerimaanDetail[$i]->fakturdetail_id)) ? $modPenerimaanDetail[$i]->fakturdetail_id : null;
                                    $modPenerimaanDetail[$i]->jmlkemasan = (!empty($modPenerimaanDetail[$i]->jmlkemasan)) ? $modPenerimaanDetail[$i]->jmlkemasan : 1;
                                    $modPenerimaanDetail[$i]->penerimaanbarang_id = $modPenerimaanBarang->penerimaanbarang_id;                                    
                                    $modPenerimaanDetail[$i]->persendiscount = $_POST['PenerimaandetailT'][$i]['persendiscount'];                                    
                                    $modPenerimaanDetail[$i]->jmldiscount = $_POST['PenerimaandetailT'][$i]['jmldiscount'];                                    
                                    if ($modPenerimaanDetail[$i]->save()) {//Jika Penerimaan Detail Berhasil Disimpan
                                        GFStokObatAlkesT::model()->updateByPk($modPenerimaanDetail[$i]->stokobatalkes_id, array('penerimaandetail_id' => $modPenerimaanDetail[$i]->penerimaandetail_id));
                                        /*
										GFObatalkesM::model()->updateByPk(
											$_POST['PenerimaandetailT'][$i]['obatalkes_id'], 
                                            array(
												'kemasanbesar'=>$_POST['PenerimaandetailT'][$i]['jmlkemasan']
											)
										);
										*/
										
                                        if (isset($_POST['isLangsungFaktur'])){
                                            if ($_POST['isLangsungFaktur'] == '1') {
                                                $modFakturPembelian->attributes = $_POST['GFFakturPembelianT'];
                                                $modFakturPembelian->penerimaanbarang_id = $modPenerimaanBarang->penerimaanbarang_id;
                                                $modFakturPembelian->supplier_id = $modPenerimaanBarang->supplier_id;
                                                $modFakturPembelian->tglfaktur = $format->formatDateTimeMediumForDB($modFakturPembelian->tglfaktur);
                                                $modFakturPembelian->tgljatuhtempo = $format->formatDateTimeMediumForDB($modFakturPembelian->tgljatuhtempo);
                                                $modFakturPembelian->ruangan_id = Yii::app()->user->getState('ruangan_id');
                                                if ($modFakturPembelian->save()) {
                                                    $updatePenerimaanBarang = GFPenerimaanBarangT::model()->updateByPk($modPenerimaanBarang->penerimaanbarang_id, array('tglterimafaktur' => $modFakturPembelian->tglfaktur, 'fakturpembelian_id' => $modFakturPembelian->fakturpembelian_id));
                                                    $modFakturDetail = new GFFakturDetailT;
                                                    $modFakturDetail->satuankecil_id = $modPenerimaanDetail[$i]->satuankecil_id;
                                                    $modFakturDetail->fakturpembelian_id = $modFakturPembelian->fakturpembelian_id;
                                                    $modFakturDetail->obatalkes_id = $modPenerimaanDetail[$i]->obatalkes_id;
                                                    $modFakturDetail->penerimaandetail_id = $modPenerimaanDetail[$i]->penerimaandetail_id;
                                                    $modFakturDetail->satuanbesar_id = $modPenerimaanDetail[$i]->satuanbesar_id;
                                                    $modFakturDetail->sumberdana_id = $modPenerimaanDetail[$i]->sumberdana_id;
                                                    $modFakturDetail->jmlterima = $modPenerimaanDetail[$i]->jmlterima;
                                                    $modFakturDetail->harganettofaktur = $modPenerimaanDetail[$i]->harganettoper;
                                                    $modFakturDetail->hargappnfaktur = $modPenerimaanDetail[$i]->hargappnper;
                                                    $modFakturDetail->hargapphfaktur = $modPenerimaanDetail[$i]->hargapphper;
                                                    $modFakturDetail->persendiscount = $modPenerimaanDetail[$i]->persendiscount;
                                                    $modFakturDetail->jmldiscount = $modPenerimaanDetail[$i]->jmldiscount;
                                                    $modFakturDetail->hargasatuan = $modPenerimaanDetail[$i]->hargasatuanper;
                                                    $modFakturDetail->tglkadaluarsa = $tampilDetail[$i]->tglkadaluarsa;
                                                    $modFakturDetail->jmlkemasan = $modPenerimaanDetail[$i]->jmlkemasan;
                                                    if ($modFakturDetail->save()) {
                                                        $updatePenerimaanDetail = GFPenerimaanDetailT::model()->updateByPk($modPenerimaanDetail[$i]->penerimaandetail_id, array('fakturdetail_id' => $modFakturDetail->fakturdetail_id));
                                                        //=== Start update obatalkes_m dan obatsupplier_m
//                                                            $persenJual = ($konfigFarmasi->totalpersenhargajual > 0) ? $konfigFarmasi->totalpersenhargajual/100 : 1;
//                                                            $persenPPN = 0; //default ppn
//                                                            if($_POST['GFFakturPembelianT']['totalpajakppn'] > 0){
//                                                                $persenPPN = $konfigFarmasi->persenppn;
//                                                                if($persenPPN > 0)
//                                                                    $PPN = $value->harganettofaktur * $persenPPN / 100;
//                                                                else
//                                                                    $PPN = 0;  
//                                                                $nettoPPN = $modPenerimaanDetail[$i]->harganettoper + $PPN;
//                                                                $hargaJual = $nettoPPN*$persenJual; 
//                                                            }else {
//                                                                $nettoPPN = $modPenerimaanDetail[$i]->harganettoper;
//                                                                $hargaJual = $nettoPPN*$persenJual;
//                                                            }
                                                            
                                                            //Update Obat Alkes
                                                            if($modFakturDetail->hargappnfaktur > 0){
                                                                $persenPPN = 10;
                                                            }else{
                                                                $persenPPN = 0;
                                                            }
                                                            if($modFakturDetail->hargapphfaktur > 0){
                                                                $persenPPH = 2; //belum digunakan
                                                            }else{
                                                                $persenPPH = 0;
                                                            }
                                                            
															$idObatAlkes=$modPenerimaanDetail[$i]->obatalkes_id;                                        
                                                            
															/*
															GFObatalkesM::model()->updateByPk(
																$idObatAlkes, 
                                                                array(
																	'harganetto'=>$modPenerimaanDetail[$i]->harganettoper,
                                                                    'hargajual'=>$hargaJual, <<< DIGUNAKAN UNTUK APA ???
                                                                    'kemasanbesar'=>$modPenerimaanDetail[$i]->jmlkemasan,
                                                                    'ppn_persen'=>$persenPPN,
                                                                    'tglkadaluarsa'=>$format->formatDateMediumForDB($_POST['ObatalkesM'][$i]['tglkadaluarsa']),
																)
															);
															*/
															
                                                            //Tambah Obat Supplier Baru
                                                            $idObatSupplier = ObatsupplierM::model()->findByAttributes(array(
																'obatalkes_id'=>$modPenerimaanDetail[$i]->obatalkes_id, 'obatsupplier_id'=>$modPenerimaanBarang->supplier_id
															))->obatsupplier_id;
															
                                                            if(empty($idObatSupplier) || $idObatSupplier < 1){
                                                                $this->tambahObatSupplier($modPenerimaanBarang->supplier_id,$modPenerimaanDetail[$i],$persenPPN);
                                                            }else{
                                                                //Update Obat Supplier
                                                                ObatsupplierM::model()->updateByPk($idObatSupplier, 
                                                                    array(
																		'satuankecil_id'=>$modPenerimaanDetail[$i]->satuankecil_id,
                                                                        'satuanbesar_id'=>$modPenerimaanDetail[$i]->satuanbesar_id,
                                                                        'hargabelikecil'=>$modPenerimaanDetail[$i]->harganettoper,
                                                                        'hargabelibesar'=>$modPenerimaanDetail[$i]->hargabelibesar,
                                                                        'diskon_persen'=>$modPenerimaanDetail[$i]->persendiscount,
                                                                        'ppn_persen'=>$persenPPN,
                                                                ));
                                                            }
                                                            
                                                           //Update Stok Obat Alkes
															$idStokObatAlkes=GFPenerimaanDetailT::model()->findByPk($value->penerimaandetail_id)->stokobatalkes_id;
                                                            GFStokObatAlkesT::model()->updateByPk($idStokObatAlkes,
                                                                array(
																	'harganetto_oa'=>$modPenerimaanDetail[$i]->harganettoper,
																	'hargajual_oa'=>$modPenerimaanDetail[$i]->harganettoper * (Params::persenHargaJual() / 100),
																	'discount'=>$modPenerimaanDetail[$i]->persendiscount
																)
															);
                                                        //=== End Update
                                                    }
                                                }else{
                                                    $this->_validasi = false;
                                                    $this->_message = 'Faktur gagal disimpan';
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        $this->_validasi = false;
                                        $this->_message = 'Penerimaan detail gagal disimpan';
                                    }
                                }
                                else{
                                    $this->_validasi = false;
                                    $this->_message = 'Stok Obatalkes gagal disimpan';
                                }
                            endforeach;
                        }
                        if ($this->_validasi){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', "Data Berhasil Disimpan ");
                            $this->redirect(array('index','idPenerimaan'=>$modPenerimaanBarang->penerimaanbarang_id));
                        }else{
                            $modPenerimaanBarang->isNewRecord = true;
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', $this->_message);
                        }
                    } catch (Exception $exc) {
                        $transaction->rollback();
//                        echo "<pre>"; print_r($modPenerimaanBarang->attributes); exit;
                        Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
                    }
                } else{
                    exit;
                    Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                }
            }
        }

        $this->render('index', array(
			'modPenerimaanBarang' => $modPenerimaanBarang,
            'modPenerimaanDetail' => $modPenerimaanDetail,
            'modPermintaanPembelian' => $modPermintaanPembelian,
            'modFakturPembelian' => $modFakturPembelian,
            'modObatAlkes' => $modObatAlkes,
            'var'=>$var,
            'modUangMuka' => $modUangMuka
		));
    }
    
    public function hitungJmlSudahTerima($idPembelian, $idObat){
        $jmlSudahTerima = 0;
        $modPenerimaans = GFPenerimaanBarangT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$idPembelian));
        foreach($modPenerimaans AS $i => $modPenerimaan){
            $modPenerimaanDet = GFPenerimaanDetailT::model()->findByAttributes(array('penerimaanbarang_id'=>$modPenerimaan->penerimaanbarang_id,'obatalkes_id'=>$idObat));
            $jmlSudahTerima += $modPenerimaanDet->jmlterima;
        }
//        $criteria = new CDbCriteria();
//        $criteria->join = "
//            JOIN penerimaanbarang_t ON penerimaanbarang_t.penerimaanbarang_id = t.penerimaanbarang_id
//            JOIN permintaanpembelian_t ON permintaanpembelian_t.permintaanpembelian_id = penerimaanbarang_t.permintaanpembelian_id
//            JOIN permintaandetail_t ON permintaandetail_t.permintaanpembelian_id = permintaanpembelian_t.permintaanpembelian_id
//        ";
//        $criteria->addCondition("permintaandetail_t.permintaandetail_id = '".$idPermintaanDetail."'");
//        $criteria->addCondition("t.obatalkes_id = '".$idObat."'");
//        $criteria->select = 'permintaandetail_t.permintaandetail_id, t.obatalkes_id AS obatalkes_id, SUM(t.jmlterima) AS jmlterima';
//        $criteria->group = 'permintaandetail_t.permintaandetail_id, t.obatalkes_id,t.jmlterima';
//        $modPenerimaanTerakhir = GFPenerimaanDetailT::model()->find($criteria);
        
//        return 1234;
        return $jmlSudahTerima;
    }
    
    /**
    * tambahObatSupplier = insert data ObatsupplierM baru jika ada yang baru
    * @param type $supplierId,$obatDetail,$persenPPN
    * @return boolean
    */
    private function tambahObatSupplier($supplierId,$obatDetail,$persenPPN = 0){
        $sukses = false;
        $obatBaru = new ObatsupplierM;
        $obatBaru->attributes = $obatDetail->attributes;
        $obatBaru->supplier_id = $supplierId;
        $obatBaru->ppn_persen = $persenPPN;
        $obatBaru->diskon_persen = $obatDetail->persendiscount;
        $obatBaru->hargabelikecil = $obatDetail->hargabelibesar / $obatDetail->jmlkemasan;
        if($obatBaru->save()){
            $sukses = true;
        }
        return $sukses;
    }

    /**
     * validasi tabular
     * @param array $data
     * @return PenerimaandetailT 
     */
    private function validateTabular($data,$scenario='insert') {
        $format = new CustomFormat();
        $model = null;
        if (count($data) > 0) {
            foreach ($data as $counter => $row) {
                $model[$counter] = new PenerimaandetailT($scenario);
                $model[$counter]->attributes = $row;
                $diskon = (($row['persendiscount'] > 0) && ($row['harganettoper'] > 0)) ? ($row['harganettoper']*$row['persendiscount'])/100 : 0;
                $model[$counter]->hargasatuanper = ($row['harganettoper'] + $row['hargappnper'] + $row['hargapphper'] ) - $diskon;
                $model[$counter]->penerimaanbarang_id = 10;
                if ($row['tglkadaluarsa'] != '') {
                     $model[$counter]->tglkadaluarsa = $format->formatDateMediumForDB($row['tglkadaluarsa']);
                } else {
                     $model[$counter]->tglkadaluarsa = null;
                }
                if($scenario == 'insert'){
                    $model[$counter]->jmlpermintaan = 0;
                }

                $model[$counter]->validate();
                if (count($model[$counter]->getErrors()) > 0){
                    $this->_validasi = false;
                }
            }
        }
        return $model;
    }
    
    /**
     * method untuk hasil print detail penerimaan items dengan parameter input id penerimaanitems
     * digunakan pada halaman 
     * 1. Informasi Penerimaan items -> detail penerimaan items -> print
     * 2. Transaksi Penerimaan items setelah save tombol print akan muncul
     * 
     * @param int $idPenerimaan
     * @param string $caraPrint 
     * 
     * print detail penerimaan items
     */
    public function actionPrint($idPenerimaan,$caraPrint){
        $judulLaporan = 'Laporan Detail Penerimaan Barang';
        $modPenerimaan = GFPenerimaanBarangT::model()->findByPk($idPenerimaan);
        $modPenerimaanDetails = GFPenerimaanDetailT::model()->findAll('penerimaanbarang_id=:idPenerimaan', array('idPenerimaan'=>$idPenerimaan));
        $modPenerimaan->tglterima = date('d-m-Y H:i:s', strtotime($modPenerimaan->tglterima));
        $modPenerimaan->tglsuratjalan = date('d-m-Y H:i:s', strtotime($modPenerimaan->tglsuratjalan));
        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                        'caraPrint'=>$caraPrint,
                                        'modPenerimaan'=>$modPenerimaan,
                                        'modPenerimaanDetails'=>$modPenerimaanDetails));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('Print',array('modDetailRencana'=>$modDetailRencana,
                                        'judulLaporan'=>$judulLaporan,
                                        'modRencanaKebutuhan'=>$modRencanaKebutuhan));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;  
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);  
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('Print',array('modDetailRencana'=>$modDetailRencana,
                                                                'judulLaporan'=>$judulLaporan,
                                                                'modRencanaKebutuhan'=>$modRencanaKebutuhan),true));
            $mpdf->Output();
        }   
    }
    
    /**
     * method untuk hasil print detail penerimaan items dengan parameter input id penerimaanitems
     * digunakan pada halaman 
     * 1. Informasi Penerimaan items -> detail penerimaan items -> print
     * 2. Transaksi Penerimaan items setelah save tombol print akan muncul
     * 
     * @param int $idPenerimaan
     * @param string $caraPrint 
     * 
     * print detail penerimaan items
     */
    public function actionPrintFaktur($idFaktur,$caraPrint){
        $judulLaporan = 'Detail Faktur Pembelian';
        $modFakturPembelian = GFFakturPembelianT::model()->findByPk($idFaktur);
        $modFakturPembelianDetails = GFFakturDetailT::model()->findAll('fakturpembelian_id=:idFaktur',array('idFaktur'=>$idFaktur));
        $modFakturPembelian->tglfaktur = date('d-m-Y H:i:s', strtotime($modFakturPembelian->tglfaktur));
        $modFakturPembelian->tgljatuhtempo = date('d-m-Y H:i:s', strtotime($modFakturPembelian->tgljatuhtempo));
        $modUangMuka = UangmukabeliT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$modFakturPembelian->penerimaan->permintaanpembelian_id));
            foreach($modUangMuka as $u=>$uang){
                $jmlUang += $uang->jumlahuang;
            }
        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('PrintFaktur',array('judulLaporan'=>$judulLaporan,
                                        'caraPrint'=>$caraPrint,
                                        'modFakturPembelian'=>$modFakturPembelian,
                                        'modFakturPembelianDetails'=>$modFakturPembelianDetails,'jmlUang'=>$jmlUang));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('PrintFaktur',array('modDetailRencana'=>$modDetailRencana,
                                        'judulLaporan'=>$judulLaporan,
                                        'modRencanaKebutuhan'=>$modRencanaKebutuhan,'jmlUang'=>$jmlUang));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;  
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);  
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('PrintFaktur',array('modDetailRencana'=>$modDetailRencana,
                                                                'judulLaporan'=>$judulLaporan,
                                                                'modRencanaKebutuhan'=>$modRencanaKebutuhan,'jmlUang'=>$jmlUang),true));
            $mpdf->Output();
        }   
    }
}