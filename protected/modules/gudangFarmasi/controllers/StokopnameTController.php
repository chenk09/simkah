
<?php

class StokopnameTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $pathView = 'gudangFarmasi.views.stokopnameT.';

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex($id = null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                
		$model=new GFStokopnameT;
                $model->totalharga = 0;
                $model->totalnetto = 0;
                $model->tglstokopname =date('d M Y H:i:s');
                $instalasi_id = Yii::app()->user->getState('instalasi_id');
                $model->nostokopname = Generator::noStokOpname($instalasi_id);
                $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
//                $modObat = new GFObatalkesM();
                $modObat = new GFObatAlkesFarmasiV();
                $modObat->obatalkes_id = 0 ;
                if (!empty($id)){
                    $modFormulir = FormuliropnameR::model()->find('formuliropname_id ='.$id.' and stokopname_id is null');
                    if (count($modFormulir) == 1){
                        $model->formuliropname_id = $modFormulir->formuliropname_id;
                        $modDetailFormulir = FormstokopnameR::model()->findAll('formuliropname_id = '.$modFormulir->formuliropname_id.' and stokopnamedet_id is null');
                        $modObat->obatalkes_id = CHtml::listData($modDetailFormulir, 'obatalkes_id', 'obatalkes_id');
                        $model->jenisstokopname = Params::JENIS_STOKOPNAME_PENYESUAIAN;
                        $model->disableJenisStokOpname = true;
                    }
                }
                $model->petugas1_id = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai_id;
                $model->petugas1_nama = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GFStokopnameT']))
		{
                    $model->attributes=$_POST['GFStokopnameT'];   
                    ($model->jenisstokopname == Params::JENIS_STOKOPNAME_PENYESUAIAN) ? $model->isstokawal = FALSE :$model->isstokawal = true ;

                    if (count($_POST['GFObatAlkesFarmasiV']) > 0){
                        $modObat->obatalkes_id = $this->sortPilih($_POST['GFObatAlkesFarmasiV']);
                        $modDetails = $this->validasiTabular($_POST['GFObatAlkesFarmasiV']);
                    }
                    if ($model->validate()){
                        $model->tglstokopname = trim($_POST['GFStokopnameT']['tglstokopname']);
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                            $hasil = 0;

                            if($model->save()){
                                if (count($modDetails) > 0){
                                    $jumlah = count($modDetails);
                                    foreach ($modDetails as $i => $v) {
                                        if (!empty($model->formuliropname_id)){
                                            FormuliropnameR::model()->updateByPk($model->formuliropname_id, array('stokopname_id'=>$model->stokopname_id));
                                            $v->formstokopname_id = FormstokopnameR::model()->find('formuliropname_id = '.$model->formuliropname_id.' and obatalkes_id = '.$v->obatalkes_id)->formstokopname_id;
                                        }
                                        $v->stokopname_id = $model->stokopname_id;
                                        //Input nilai stok opname dari obatalkes_m karena yg digunakan harga terupdate
                                        //by: ichan
                                        $modObat = ObatAlkesM::model()->findByPk($v->obatalkes_id);
                                        if(!empty($modObat->obatalkes_id)){
                                            $v->harganetto = $modObat->harganetto;
                                            $v->jumlahnetto = $modObat->harganetto * $v->volume_fisik;
                                            $v->hargasatuan = $modObat->hpp;
                                            $v->jumlahharga = $modObat->hpp * $v->volume_fisik;
                                        }
                                        if ($v->save()){
                                            if (!empty($v->formstokopname_id)){
                                                FormstokopnameR::model()->updateByPk($v->formstokopname_id, array('stokopnamedet_id'=>$v->stokopnamedet_id));
                                            }
                                            if ($model->isstokawal){
                                                StokobatalkesT::tambahStok($v,$v->volume_fisik);
                                            }else{
                                                $selisih = $v->volume_fisik - $v->volume_sistem;
                                                if ($selisih > 0){
                                                    StokobatalkesT::tambahStok($v, $selisih);
                                                } else { //jika selisih minus = tambah stok
                                                    $selisih = abs($selisih);
                                                    StokobatalkesT::kurangiStok($selisih, $v->obatalkes_id, Yii::app()->user->getState('ruangan_id'));
                                                }
                                            }
                                            $hasil++;
                                        }
                                    }
                                }
                            }

                            if(($hasil>0)&&($hasil == $jumlah)){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                                $this->redirect(Yii::app()->createUrl($this->route));
                            }
                            else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data Gagal Disimpan ");
                            }
                        }catch(Exception $ex){
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.'.MyExceptionMessage::getMessage($ex, true));
                        }
                    }
		}
                
                if(isset($_GET['GFObatAlkesFarmasiV']))
		{

                $format = new CustomFormat();
                        $modObat->unsetAttributes();
			$modObat->attributes=$_GET['GFObatAlkesFarmasiV'];	
            $modObat->tglkadaluarsa_awal    =  $format->formatDateMediumForDB($_GET['GFObatAlkesFarmasiV']['tglkadaluarsa_awal']);
            $modObat->tglkadaluarsa_akhir   =  $format->formatDateMediumForDB($_GET['GFObatAlkesFarmasiV']['tglkadaluarsa_akhir']);	
		}

		$this->render($this->pathView.'index',array(
			'model'=>$model, 'modObat'=>$modObat, 'modDetails'=>$modDetails, 'modFormulir'=>$modFormulir
		));
	}
        
        
        protected function sortPilih($data){
            $result = array();
            foreach ($data as $i=>$row){
                if ($row['cekList'] == 1){
                    $result[] = $row['obatalkes_id'];
                }
            }
            
            return $result;
        }
        
        protected function validasiTabular($data){
            foreach ($data as $i=>$row){
                if($row['cekList']){
                    $obat = ObatalkesM::model()->findByPk($row['obatalkes_id']);
                    $modDetails[$i] = new StokopnamedetT();
                    $modDetails[$i]->attributes = $row;
                    $modDetails[$i]->hargasatuan = $obat->hargajual;
                    $modDetails[$i]->harganetto = $obat->hargajual;
                    $modDetails[$i]->jumlahharga = $modDetails[$i]->hargasatuan*$modDetails[$i]->volume_fisik;
                    $modDetails[$i]->jumlahnetto = $modDetails[$i]->harganetto*$modDetails[$i]->volume_fisik;
                    $modDetails[$i]->satuankecil_id = $obat->satuankecil_id;
                    $modDetails[$i]->sumberdana_id = $obat->sumberdana_id;
                    $modDetails[$i]->tglkadaluarsa = $obat->tglkadaluarsa;
                    $modDetails[$i]->validate();
                }
            }
            return $modDetails;
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GFStokopnameT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function actionInformasi(){
            $model = new GFStokopnameT('search');
            $model->tglstokopname = date('d M Y 00:00:00');
            $model->tglAkhir = date('d M Y  23:59:59');
            if (isset($_GET['GFStokopnameT'])){
                $model->unsetAttributes();
                $model->attributes = $_GET['GFStokopnameT'];
                $format = new CustomFormat();
                $model->tglstokopname = $format->formatDateTimeMediumForDB($model->tglstokopname);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
            }
            $this->render($this->pathView.'informasi', array('model'=>$model));
        }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gfstokopname-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new GFStokopnameT;
            $model->attributes=$_REQUEST['GFStokopnameT'];
            $judulLaporan='Data GFStokopnameT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        public function actionPrintNilaiPersediaan($id)
        {
            $model = StokopnameT::model()->findByPk($id);
//            $modDetails = StokopnamedetT::model()->findAllByAttributes(array('stokopname_id'=>$model->stokopname_id));
            $modTable = new GFStokopnamedetT('search');
            $modTable->stokopname_id = $model->stokopname_id;
            $model->attributes=$_REQUEST['GFStokopnameT'];
            $judulLaporan='Rincian Obat Alkes Nilai Persediaan Stok Opname';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'PrintNilaiPersediaan',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'modDetails'=>$modDetails,'modTable'=>$modTable));
            }
              
        }
        
        public function actionDetails($id){
            $this->layout = '//layouts/frameDialog';
            $model = StokopnameT::model()->findByPk($id);
//            $modDetails = StokopnamedetT::model()->findAllByAttributes(array('stokopname_id'=>$model->stokopname_id));
            $modTable = new GFStokopnamedetT('search');
            $modTable->stokopname_id = $model->stokopname_id;
//            $modObat->obatalkes_id = CHtml::listData($modDetails, 'obatalkes_id', 'obatalkes_id');
            $this->render($this->pathView.'details', array('model'=>$model, 'modDetails'=>$modDetails, 'modTable'=>$modTable));
        }
}
