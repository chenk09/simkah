<?php

class ObatAlkesMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $successSaveObatAlkes=false;
        public $succesSaveModObatAlkesDetail=false;

	/**
	 * @return array action filters
	 */
//	FILTER USER SUDAH MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view', 'informasi'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print','UbahHarga','cekOtoritas','printInformasi', 'updateHarga'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
        
        public function actionCekOtoritas()
        {
           if(Yii::app()->request->isAjaxRequest) {
                $username=$_POST['username'];
                $password=md5($_POST['password']);
                $idLoginPemakai='null';
                $modLoginPemakai=  GFLoginPemakaiK::model()->find('nama_pemakai=\''.$username.'\' AND katakunci_pemakai=\''.$password.'\'');
                if(COUNT($modLoginPemakai)>0){//Jika Username dan Passwordnya Bnear
                    $idUser=$modLoginPemakai->loginpemakai_id;
                    $modAssigmentK=GFAssignmentsK::model()->find("userid=$idUser")->itemname;
                    if($modAssigmentK==Params::DEFAULT_ROLES_SUPERVISOR){//Jika Mempunyai Hak Akses Superpisor
                        $message = 'Supervisor';
                        $idLoginPemakai=$modLoginPemakai->loginpemakai_id;
                    }else{//Jika username yang dimasukan tidak mempunyai hak akses supervisor
                        $message = 'Anda Tidak Diijinkan Untuk Mengubah Harga Obat';
                    }
                }else{//Jika Usename atau password salah
                        $message = 'Terjadi Kesalahan dan memasukan Username atau Password';
                }
                $data['message']=$message;
                $data['loginpemakai_id']=$idLoginPemakai;
                echo CJSON::encode($data);
            }
            Yii::app()->end();

        }
        
      

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $model=new GFObatAlkesM;
                $model->harga_besar = 0;
                $model->harganetto = 0;
                $model->discount = 0;
                $model->ppn_persen = 0;
                $model->hargajual = 0;
                $model->hjanonresep_asal = 0;
                $model->hjaresep_asal = 0;
                 $model->tglkadaluarsa = date('d M Y');
                $model->formObatAlkesDetail = true;
                $modObatAlkesDetail = new ObatalkesdetailM;
                $konfigFarmasi = KonfigfarmasiK::model()->find();
		$format = new CustomFormat();
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                if(isset($_POST['GFObatAlkesM']))
		{
                   $cekObatAlkes=0; 
                   $transaction = Yii::app()->db->beginTransaction();
                   try {
			$model->attributes=$_POST['GFObatAlkesM'];
                        $model->obatalkes_farmasi=TRUE;
                        $model->activedate = date('Y-m-d');
                        if(!empty($_POST['GFObatAlkesM']['tglkadaluarsa'])){//Jika User memasukan Tanggal Kadaluarsa
                        $model->tglkadaluarsa  = $format->formatDateMediumForDB($_POST['GFObatAlkesM']['tglkadaluarsa']);
                        }
			if($model->validate()){//Jika Data Untuk Model Obat Alkes Valid
                           if($model->save()){//Jika Model Obat Alkes Sudah Disimpan
                            $modObatAlkesDetail->attributes=$_POST['ObatalkesdetailM'];
                            $modObatAlkesDetail->obatalkes_id=$model->obatalkes_id;
                            $modObatAlkesDetail->save();
                            $this->successSaveObatAlkes=true;   
                            $jumlahSupplier=COUNT($_POST['supplier_id']);
                            $idObatAlkes=$model->obatalkes_id;
                            for($i=0; $i<=$jumlahSupplier; $i++):
                              $modObatSupplier = new GFObatSupplierM;
                              $modObatSupplier->supplier_id=$_POST['supplier_id'][$i];
                              $modObatSupplier->satuanbesar_id=$_POST['GFObatAlkesM']['satuanbesar_id'];
                              $modObatSupplier->satuankecil_id=$_POST['GFObatAlkesM']['satuankecil_id'];
                              $modObatSupplier->hargabelibesar=0;
                              $modObatSupplier->hargabelikecil=0;
                              $modObatSupplier->obatalkes_id=$idObatAlkes;
                              if($modObatSupplier->validate()){//Jika Data ObatSupplierM Valid
                                 if($modObatSupplier->save()){//Jika ObatSupplierM Sudah Berhasil Disimpan
                                    $cekObatAlkes++; 
                                 }else{
                                     Yii::app()->user->setFlash('error', "Data Obat Supplier Gagal Disimpan");
                                 }   
                              }
                            endfor;
                           }else{//JIka Model Supplier Gagal Disimpan
                                Yii::app()->user->setFlash('error', "Data Obat Alkes Gagal Disimpan");
                           }  
                        }else{//Jika Data Supplier Tidak Valid
                           Yii::app()->user->setFlash('error', "Data Obat Alkes Tidak Valid");
                        }
                        
                       if($this->successSaveObatAlkes && ($cekObatAlkes==$jumlahSupplier)){
                          //exit();
                           $transaction->commit();
                           Yii::app()->user->setFlash('success', "Data Obat Akles dan Obat Supplier Berhasil Disimpan");
                           $this->redirect(array('admin'));
                       }else{
                          Yii::app()->user->setFlash('error', "Data Obat Akles dan Obat Supplier Gagal Disimpan");
                       }     

                   }catch(Exception $exc){
                        //exit();
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                   }
		}

		$this->render('create',array(
			'model'=>$model,
      'modObatAlkesDetail'=>$modObatAlkesDetail,
                        'konfigFarmasi'=>$konfigFarmasi,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
              //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
                $model->jenisobatalkes_id = $model->jenisobatalkes_id;
                $model->therapiobat_id = $model->therapiobat_id;
                $model->jnskelompok = $model->jnskelompok;
                $model->harganetto_asal = $model->harganetto;
                $model->hargajual_asal  = $model->hargajual;
                $model->hjanonresep_asal  = $model->hjanonresep;
                $model->hjaresep_asal  = $model->hjaresep;
                $modObatAlkesDetail = ObatalkesdetailM::model()->findByAttributes(array('obatalkes_id'=>$id));
     		$format = new CustomFormat();
                $modObatSupplier = GFObatSupplierM::model()->findAll('obatalkes_id='.$id.'');
                $konfigFarmasi = KonfigfarmasiK::model()->find();
                $modUbahHarga = new GFUbahHargaObatR;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                
                
		 if(isset($_POST['GFObatAlkesM']))
		{
                   $cekObatAlkes=0; 
                   $transaction = Yii::app()->db->beginTransaction();
                   try {
      $model->attributes=$_POST['GFObatAlkesM'];
			$model->ppn_persen=$_POST['GFObatAlkesM']['ppn_persen'];
                        $model->activedate = $format->formatDateMediumForDB($model->activedate);
                        if(!empty($_POST['GFObatAlkesM']['tglkadaluarsa'])){//Jika User memasukan Tanggal Kadaluarsa
                          $model->tglkadaluarsa  = $format->formatDateMediumForDB($_POST['GFObatAlkesM']['tglkadaluarsa']);
                        }
			if($model->validate()){//Jika Data Untuk Model Obat Alkes Valid
//                            exit();
                        if($model->tglkadaluarsa == date('Y-m-d') OR $model->tglkadaluarsa <= date('Y-m-d')){
                             echo "<script>
                                      alert('Obat Sudah Kadaluarsa, Data Tidak Dapat Diproses');
                                        window.top.location.href='".Yii::app()->createUrl('gudangFarmasi/obatAlkesM/update&id='.$model->obatalkes_id)."';
                                    </script>";
                        }else{
                           if($model->save()){//Jika Model Obat Alkes Sudah Disimpan
                              $modUbahHarga->obatalkes_id     = $model->obatalkes_id;
                              $modUbahHarga->loginpemakai_id  = Yii::app()->user->id;
                              $modUbahHarga->sumberdana_id    = $model->sumberdana_id;
                              $modUbahHarga->tglperubahan     = date('Y-m-d H:i:s');
                              $modUbahHarga->harganettoasal   = $model->harganetto_asal;
                              $modUbahHarga->hargajualasal    = $model->hargajual_asal;
                              $modUbahHarga->harganettoperubahan    = $model->harganetto;
                              $modUbahHarga->hargajualperubahan     = $model->hargajual;
                              $modUbahHarga->alasanperubahan  = '-';
                              $modUbahHarga->disetujuioleh    = Yii::app()->user->id;
                              $modUbahHarga->create_time      = date('Y-m-d H:i:s');
                              $modUbahHarga->create_loginpemakai_id = Yii::app()->user->id;
                              $modUbahHarga->create_ruangan   = Yii::app()->user->getState('ruangan_id');

                              $modUbahHarga->save();

                               if($modObatAlkesDetail != null) {
                                    $modObatAlkesDetail->attributes=$_POST['ObatalkesdetailM'];
                                    $modObatAlkesDetail->save();
                               } else {
                                    $modObatAlkesDetail = new ObatalkesdetailM;
                                    $modObatAlkesDetail->attributes=$_POST['ObatalkesdetailM'];
                                    $modObatAlkesDetail->obatalkes_id = $model->obatalkes_id;
                                    $modObatAlkesDetail->save();
                               }
                            $this->successSaveObatAlkes=true;   
                            $jumlahSupplier=COUNT($_POST['supplier_id']);
                            $idObatAlkes=$model->obatalkes_id;
                            GFObatSupplierM::model()->deleteAll('obatalkes_id='.$idObatAlkes.'');   
                            for($i=0; $i<=$jumlahSupplier; $i++):
                              $modObatSupplier = new GFObatSupplierM;
                              $modObatSupplier->hargabelibesar = $_POST['GFObatAlkesM']['harga_besar'];
                              $modObatSupplier->hargabelikecil = $_POST['GFObatAlkesM']['harganetto'];
                              $modObatSupplier->ppn_persen = $_POST['GFObatAlkesM']['ppn_persen'];
                              $modObatSupplier->diskon_persen = $_POST['GFObatAlkesM']['discount'];
                              $modObatSupplier->satuanbesar_id = $_POST['GFObatAlkesM']['satuanbesar_id'];
                              $modObatSupplier->satuankecil_id = $_POST['GFObatAlkesM']['satuankecil_id'];
							  
                              $modObatSupplier->supplier_id = $_POST['supplier_id'][$i];
                              $modObatSupplier->obatalkes_id = $idObatAlkes;
							  
                              if($modObatSupplier->validate()){//Jika Data ObatSupplierM Valid
                                 if($modObatSupplier->save()){//Jika ObatSupplierM Sudah Berhasil Disimpan
                                    $cekObatAlkes++; 
                                 }else{
                                     Yii::app()->user->setFlash('error', "Data Obat Supplier Gagal Disimpan");
                                 }   
                              }
                            endfor;
                           }else{//JIka Model Supplier Gagal Disimpan
                                Yii::app()->user->setFlash('error', "Data Obat Alkes Gagal Disimpan");
                           } 
                        }
                        }else{//Jika Data Supplier Tidak Valid
                           Yii::app()->user->setFlash('error', "Data Obat Alkes Tidak Valid");
                        }
                       if($this->successSaveObatAlkes && ($cekObatAlkes==$jumlahSupplier)){
                           $transaction->commit();
                           Yii::app()->user->setFlash('success', "Data Obat Akles dan Obat Supplier Berhasil Disimpan");
                           $this->redirect(array('admin'));
                       }else{
                          Yii::app()->user->setFlash('error', "Data Obat Akles dan Obat Supplier Gagal Disimpan");
                       }     
                       
                   }catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                   }
		}

		$this->render('update',array(
			'model'=>$model,'$modObatAlkesDetail'=>$modObatAlkesDetail,'modObatSupplier'=>$modObatSupplier, 'modUbahHarga'=>$modUbahHarga,
                        'konfigFarmasi'=>$konfigFarmasi,
		));
	}


	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('GFObatAlkesM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GFObatalkesfarmasiV('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GFObatalkesfarmasiV']))
			$model->attributes=$_GET['GFObatalkesfarmasiV'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GFObatAlkesM::model()->findByPk($id);
                $obatdetail=ObatalkesdetailM::model()->findByAttributes(array('obatalkes_id'=>$id));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gfobat-alkes-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionDelete()
				{              
					//if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
					if(Yii::app()->request->isPostRequest)
					{
                                                $id = $_POST['id'];
                                                $this->loadModel($id)->delete();
                                                if (Yii::app()->request->isAjaxRequest)
                                                    {
                                                        echo CJSON::encode(array(
                                                            'status'=>'proses_form', 
                                                            'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                                                            ));
                                                        exit;               
                                                    }
				                    
						// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
						if(!isset($_GET['ajax']))
							$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
					}
					else
						throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
				}
        
                 /**
                 *Mengubah status aktif
                 * @param type $id 
                 */
                public function actionRemoveTemporary()
                {
                            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        //                    SAPropinsiM::model()->updateByPk($id, array('propinsi_aktif'=>false));
        //                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                          
                    
                    $id = $_POST['id'];   
                    if(isset($_POST['id']))
                    {
                       $update = ObatalkesM::model()->updateByPk($id,array('obatalkes_aktif'=>false));
                       if($update)
                        {
                            if (Yii::app()->request->isAjaxRequest)
                            {
                                echo CJSON::encode(array(
                                    'status'=>'proses_form', 
                                    ));
                                exit;               
                            }
                         }
                    } else {
                            if (Yii::app()->request->isAjaxRequest)
                            {
                                echo CJSON::encode(array(
                                    'status'=>'proses_form', 
                                    ));
                                exit;               
                            }
                    }

               }
        
        public function actionPrintInformasi()
        {
    
            $model= new GFInfostokobatalkesruanganV('searchPrintInformasi');
            $model->unsetAttributes();
            if(isset($_GET['GFInfostokobatalkesruanganV']))
            {
                $model->attributes=$_GET['GFInfostokobatalkesruanganV'];
            }
            $judulLaporan='Data Obat Alkes';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
//        public function actionPrintInformasiOld()
//        {
//             $model=new GFInfostokobatalkesruanganV('search');
//            $model->unsetAttributes();
//            $model->tglAwal = date('Y-m-d 00:00:00');
//            $model->tglAkhir = date('Y-m-d H:i:s');
//            if(isset($_GET['GFInfostokobatalkesruanganV']))
//            {
//                $model->attributes=$_GET['GFInfostokobatalkesruanganV'];
//                if(!empty($_GET['GFInfostokobatalkesruanganV']['filterTanggal']))
//                    $model->filterTanggal = true;
//                else 
//                    $model->filterTanggal = false;
//                $format = new CustomFormat();
//                $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['GFInfostokobatalkesruanganV']['tglAwal']);
//                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFInfostokobatalkesruanganV']['tglAkhir']);
//            }
//            $judulLaporan='Data Obat Alkes';
//            $caraPrint=$_REQUEST['caraPrint'];
//            if($caraPrint=='PRINT') {
//                $this->layout='//layouts/printWindows';
//                $this->render('PrintInformasi',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
//            }
//            else if($caraPrint=='EXCEL') {
//                $this->layout='//layouts/printExcel';
//                $this->render('PrintInformasi',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
//            }
//            else if($_REQUEST['caraPrint']=='PDF') {
//                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
//                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
//                $mpdf = new MyPDF('',$ukuranKertasPDF); 
//                $mpdf->useOddEven = 2;  
//                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
//                $mpdf->WriteHTML($stylesheet,1);  
//                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
//                $mpdf->WriteHTML($this->renderPartial('PrintInformasi',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
//                $mpdf->Output();
//            }                       
//        }


        
        public function actionUbahHarga()
        {
            if(isset($_REQUEST['GFUbahHargaObatR'])){
                $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
                $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
                $transaction = Yii::app()->db->beginTransaction();
                  try {
                        $modUbahHarga= new GFUbahHargaObatR;
                        $modUbahHarga->attributes=$_REQUEST['GFUbahHargaObatR'];
                        $modUbahHarga->tglperubahan=date('Y-m-d H:i:s');
                        if($modUbahHarga->save()){
                        $updateObatAlkes=GFObatAlkesM::model()->updateByPk($modUbahHarga->obatalkes_id,array('harganetto'=>$modUbahHarga->harganettoperubahan,
                                                                                                             'hargajual'=>$modUbahHarga->hargajualperubahan));
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data Ubah Harga Berhasil Disimpan.');
                                $transaction->commit();
                                $urlUbahHargaBerhasil= Yii::app()->controller->createUrl($controller.'/update',array('id'=>$modUbahHarga->obatalkes_id));
                                $this->redirect($urlUbahHargaBerhasil);
                        }        
                   }catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    $urlUbahHargaGagal=  Yii::app()->createUrl($controller.'/update',array('id'=>$_REQUEST['GFUbahHargaObatR']['obatalkes_id']));

                    }
            
            }
        }
        
        public function actionInformasi()
        {
            $model=new GFInfostokobatalkesruanganV('searchInformasi');
            $model->unsetAttributes();
			
			$model->ruangan_id = Yii::app()->user->ruangan_id;
			$model->jenisobatalkes_id = 1;
			
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('d M Y  H:i:s');
            if(isset($_GET['GFInfostokobatalkesruanganV']))
            {
                $model->attributes=$_GET['GFInfostokobatalkesruanganV'];
                //$model->isGroupObat = $_GET['GFInfostokobatalkesruanganV']['isGroupObat'];
                $model->isGroupObat = FALSE;
                if(!empty($_GET['GFInfostokobatalkesruanganV']['filterTanggal']))
                    $model->filterTanggal = true;
                else 
                    $model->filterTanggal = false;
                $format = new CustomFormat();
                $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['GFInfostokobatalkesruanganV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFInfostokobatalkesruanganV']['tglAkhir']);
            }
            $this->render('informasi',array('model'=>$model));
        }
        
        /**
         * actionInformasiStok = menampilkan informasi stok barang ruangan login
         */
        public function actionInformasiStok()
        {
            $model = new GFInfostokobatalkesruanganV('searchInformasiStok');
            $format = new CustomFormat();
            $model->unsetAttributes();
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d H:i:s');
			
            if(isset($_GET['GFInfostokobatalkesruanganV']))
			{
                $model->attributes = $_GET['GFInfostokobatalkesruanganV'];
            }else{
				$model->ruangan_id = Yii::app()->user->getState('ruangan_id');
			}
			
            $this->render('informasiStok',array('model'=>$model));
        }
         public function actionUpdateHarga($idObat, $status){
            $this->layout='//layouts/frameDialog';
            $model = GFObatAlkesM::model()->findByPk($idObat);
            $model->harganetto_asal = $model->harganetto;
            $model->hargajual_asal  = $model->hargajual;
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $tersimpan = 'Tidak';
            if(!empty($_POST['GFObatAlkesM'])){
                $format = new CustomFormat();
                $model->attributes = $_POST['GFObatAlkesM'];
                if($model->validate()){
                    $transaction = Yii::app()->db->beginTransaction(); 
                    try{
                        if($status == "harganetto"){
                            ObatalkesM::model()->updateByPk($model->obatalkes_id,array(
                                            'harganetto'=>$model->harganetto, 
                                            'discount'=>$model->discount,
                                            'ppn_peersen'=>$model->ppn_persen,
                                            'hpp'=>$model->hpp,
                            ));
                        }else if($status == "hargajual"){
                            ObatalkesM::model()->updateByPk($model->obatalkes_id,array(
                                            'hjaresep'=>$model->hjaresep, 
                                            'hjanonresep'=>$model->hjanonresep,
                                            'marginresep'=>$model->marginresep,
                                            'marginnonresep'=>$model->marginnonresep,
                                            'jasadokter'=>$model->jasadokter,
                            ));
                        }
                        $modUbahHarga = new GFUbahHargaObatR;
                        $modUbahHarga->obatalkes_id     = $model->obatalkes_id;
                        $modUbahHarga->loginpemakai_id  = Yii::app()->user->id;
                        $modUbahHarga->sumberdana_id    = $model->sumberdana_id;
                        $modUbahHarga->tglperubahan     = date('Y-m-d H:i:s');
                        $modUbahHarga->harganettoasal   = $model->harganetto_asal;
                        $modUbahHarga->hargajualasal    = $model->hargajual_asal;
                        $modUbahHarga->harganettoperubahan    = $model->harganetto;
                        $modUbahHarga->hargajualperubahan     = $model->hargajual;
                        $modUbahHarga->alasanperubahan  = '-';
                        $modUbahHarga->disetujuioleh    = Yii::app()->user->id;
                        $modUbahHarga->create_time      = date('Y-m-d H:i:s');
                        $modUbahHarga->create_loginpemakai_id = Yii::app()->user->id;
                        $modUbahHarga->create_ruangan   = Yii::app()->user->getState('ruangan_id');

                        $modUbahHarga->save();

                        $transaction->commit();                                
                        Yii::app()->user->setFlash('success', '<strong>Berhasil</strong> Data Ubah Harga Berhasil Disimpan.');
                        $tersimpan='Ya';
                        
                    } catch (Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }else{
                     Yii::app()->user->setFlash('error', '<strong>Silahkan Lengkapi Field Yang Bertanda Bintang</strong> ');
                }
            }
            $this->render('updateHargaObat', array('model'=>$model,'konfigFarmasi'=>$konfigFarmasi, 'tersimpan'=>$tersimpan));
        }


  public function actionPrint()
    {
      $model=new GFObatalkesfarmasiV('searchGudangFarmasi');
      $model->attributes = $_REQUEST['GFObatalkesfarmasiV'];
      
      $judulLaporan='Data Obat Alkes';
      $caraPrint=$_REQUEST['caraPrint'];
      if($caraPrint=='PRINT') {
          $this->layout='//layouts/printWindows';
          $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
      }
      else if($caraPrint=='EXCEL') {
          $this->layout='//layouts/printExcel';
          $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
      }
      else if($_REQUEST['caraPrint']=='PDF') {
          $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
          $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
          $mpdf = new MyPDF('',$ukuranKertasPDF); 
          $mpdf->useOddEven = 2;  
          $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
          $mpdf->WriteHTML($stylesheet,1);  
          $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
          $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
          $mpdf->Output();
      }                       
    }
}
