<?php

class ObatSupplierController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $successSaveSupplier=false;
        
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','deleteupdate'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $model=GFSupplierM::model()->findByPk($id);
                $modSupplier = GFObatSupplierM::model()->findAllByAttributes(array('supplier_id'=>$id));
		$this->render('view',array(
			'model'=>$model,
                        'modSupplier'=>$modSupplier,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GFSupplierM;
                $modObatSupplier = new ObatsupplierM;
                $konfigFarmasi = KonfigfarmasiK::model()->find();
		if(isset($_POST['ObatsupplierM']))
		{
                    
                    $modObatSupplier->attributes = $_POST['ObatsupplierM'];
                    $modObatSupplier->supplier_id = $_POST['idSupplier'];
                     
                    if(count($_POST['ObatsupplierM']) > 0){
                     $transaction = Yii::app()->db->beginTransaction();
                        try{
                            $success = false;
                                $modDetails = $this->validasiTabular($_POST['ObatsupplierM'], $modObatSupplier);
                                foreach ($modDetails as $i=>$data){
                                    if ($data->obatalkes_id > 0){
                                        if ($data->save()){
//                                            ObatalkesM::model()->updateAll(array('obatalkes_aktif'=>TRUE),'obatalkes_id>=:obatalkes_id',array(':obatalkes_id'=>$data->obatalkes_id));
                                            $success = true;
                                        }
                                        else{
                                          $success = false;
                                        }
                                    }
                                }
                            if ($success == true){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                $this->redirect(array('admin'/*,'idSupplier'=>$model->supplier_id*/));
                            }
                            else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan");
                            }
                        }
                        catch (Exception $ex){
                             $transaction->rollback();
                             Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($ex)); 
                        }
                       }else{
                           $modObatSupplier->validate();
                           Yii::app()->user->setFlash('error','<strong>Gagal</strong>Data Gagal Disimpan'.MyExceptionMessage::getMessage($ex));
                       }
		}

		$this->render('create',array(
			'model'=>$model,'modObatSupplier'=>$modObatSupplier,'modDetail'=>$modDetail, 'modDetails'=>$modDetails, 'konfigFarmasi'=>$konfigFarmasi
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
        
        protected function validasiTabular($datas){
            $valid = true;
            foreach ($datas as $i=>$row){
                $modDetails[$i] = new ObatsupplierM();
                $modDetails[$i]->attributes = $row;
                if(!empty($row['obatalkes_id']) && !empty($row['supplier_id'])){
                    $valid = $modDetails[$i]->validate() && $valid;
                }
            }
            return $modDetails;
        }

	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		//$model=RESupplierM::model()->findByAttributes(array('supplier_id'=>$supplier_id));

		$modObatSupplier=GFObatSupplierM::model()->findAllByAttributes(array('obatsupplier_id'=>$id));
                $model =GFSupplierM::model()->findByPk($id);
                
                $modObat = new ObatalkesM;
                $konfigFarmasi = KonfigfarmasiK::model()->find();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GFObatSupplierM']))
		{
                        if (count($_POST['GFObatSupplierM']) > 0){
                            $modDetails = $this->validasiTabularUpdate($_POST['GFObatSupplierM']);
//                            echo "<pre>".print_r($modDetails,1)."</pre>"; exit;
//                            if ($model->validate()){
                                $transaction = Yii::app()->db->beginTransaction();
                                try{
                                    $success = false;
//                                    if($model->save()){
                                        $modDetails = $this->validasiTabularUpdate($_POST['GFObatSupplierM']);
                                        foreach ($modDetails as $i=>$data){
                                            GFObatSupplierM::model()->deleteAllByAttributes(array('supplier_id'=>$data->supplier_id, 'obatalkes_id'=>$data->obatalkes_id));
                                            if ($data->obatalkes_id > 0){
                                                if ($data->save()){
                                                    GFObatalkesM::model()->updateAll(array('obatalkes_aktif'=>TRUE),'obatalkes_id>=:obatalkes_id',array(':obatalkes_id'=>$data->obatalkes_id));
                                                    $success = true;
                                                }
                                                else{
                                                  $success = false;
                                                }
                                            }
                                        }
                                    if ($success == true){
                                        $transaction->commit();
                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                        $this->redirect(array('admin'));
                                    }
                                    else{
                                        $transaction->rollback();
                                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                                    }
                                }
                                catch (Exception $ex){
                                     $transaction->rollback();
                                     Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($ex)); 
                                }
                                
//                            }
                        }
                        else{
                            $model->validate();
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
                        }
		}
		$this->render('update',array(
			'model'=>$model,
                        'modObatSupplier'=>$modObatSupplier,
                        'modObat'=>$modObat,
                        'modDetails'=>$modDetails,
                        'modDetail'=>$modDetail,
                        'konfigFarmasi'=>$konfigFarmasi,
		));
	}
          
        protected function validasiTabularUpdate($datas){ 
             $valid = true;
            foreach ($datas as $i=>$row){
                $modDetails[$i] = new GFObatsupplierM();
                $modDetails[$i]->attributes = $row;
               // $modDetails[$i]->harganetto = (ceil($row['harganettoppn'] / 1.1));
                $valid = $modDetails[$i]->validate() && $valid;
//                echo "<pre>".print_r($datas[$i],1)."</pre>";
//                echo "<pre>".print_r($modDetails[$i]->getAttributes(),1)."</pre>";
//                exit;
            }
            return $modDetails;
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
//	public function actionDelete($id)
//	{
//		if(Yii::app()->request->isPostRequest)
//		{
//			// we only allow deletion via POST request
//                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
////			  $transaction = Yii::app()->db->beginTransaction();
////                          try {
////                                $hapusObatSupplier=GFObatSupplierM::model()->deleteAll('supplier_id='.$id.'');       
//                                $this->loadModel($id)->delete();
////                                $transaction->commit();
////                          }catch(Exception $exc){
////                            $transaction->rollback();
////                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
////                           }
//                           
//			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//			if(!isset($_GET['ajax']))
//				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//		}
//		else
//			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
//	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('GFSupplierM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id='')
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                if ($id==1) {
                    Yii::app()->user->setFlash('success','<strong>Berhasil</strong> Data berhasil disimpan');
                }
		$model=new GFObatSupplierM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GFObatSupplierM'])) {
			$model->attributes=$_GET['GFObatSupplierM'];
            // var_dump($_GET['GFObatSupplierM']['supplier']);
            // $model->supplier_nama       = $_GET['GFObatSupplierM']['supplier_nama'];
            // $model->supplier_alamat     = $_GET['GFObatSupplierM']['supplier_alamat'];
            $model->satuankecil_nama    = $_GET['GFObatSupplierM']['satuankecil_nama'];
            $model->satuanbesar_nama    = $_GET['GFObatSupplierM']['satuanbesar_nama'];

            // echo "<pre>";
            // print_r($_GET['GFObatSupplierM']);
            // exit();
        }
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($obatalkes_id, $supplier_id)
	{
		 $model=ObatsupplierM::model()->findByAttributes(array('obatalkes_id'=>$obatalkes_id, 'supplier_id'=>$supplier_id));
		
                 if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        public function actionDelete()
                {
            if(Yii::app()->request->isPostRequest)
					{
               $obatalkes_id = $_POST['id'];
                  $supplier_id = $_POST['idsupplier'];
               $this->loadModel($obatalkes_id, $supplier_id)->delete();
                 if (Yii::app()->request->isAjaxRequest)
                                                    {
                                                        echo CJSON::encode(array(
                                                            'status'=>'proses_form', 
                                                            'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                                                            ));
                                                        exit;               
                                                    }
				                    
						// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
						if(!isset($_GET['ajax']))
							$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
					}
					else
						throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
//                        if(!isset($_GET['ajax']))
//                                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                }
                
        public function actionDeleteupdate($obatalkes_id, $supplier_id)
        {
                $this->loadModel($obatalkes_id, $supplier_id)->delete();
                if(!isset($_GET['ajax']))
                        $this->redirect(Yii::app()->createUrl('gudangFarmasi/obatSupplier/admin'));
                $this->refresh();
        }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gfsupplier-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                GFSupplierM::model()->updateByPk($id, array('supplier_aktif'=>false));
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new GFObatSupplierM('search');
            $model->attributes = $_REQUEST['GFObatSupplierM'];
            // echo $_REQUEST['page'];
            // exit();
            
            $judulLaporan='Data Obat Alkes Supplier';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
