<?php
Yii::import('billingKasir.controllers.PembayaranController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class FakturPembelianController extends SBaseController
{
        protected $pathView = 'gudangFarmasi.views.fakturPembelian.';
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','search','details','detailsFaktur','retur'),
				'users'=>array('@'),
			),
		
		);
	}
        
        public function actionRetur($idFakturPembelian)
        {
            $this->layout='//layouts/frameDialog';
            $modFaktur = GFFakturPembelianT::model()->findByPk($idFakturPembelian);
            $modFakturDetail = GFFakturDetailT::model()->findAll('fakturpembelian_id='.$idFakturPembelian.'');
            $modRetur = new GFReturPembelianT;
            $modRetur->fakturpembelian_id=$modFaktur->fakturpembelian_id;
            $modRetur->noretur=  Generator::noRetur();
            $modRetur->totalretur=0;
            $modRetur->tglretur=date('Y-m-d H:i:s');
            $modRetur->supplier_id=$modFaktur->supplier_id;
            $modRetur->create_loginpemakai_id = Yii::app()->user->id;
            $modRetur->update_loginpemakai_id = Yii::app()->user->id;
            $modRetur->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modRetur->create_time = date('Y-m-d H:i:s');
            $modRetur->update_time = date('Y-m-d H:i:s');
            $modRetur->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modReturDetails = new GFReturDetailT;
            $tersimpan=false;
            
            if(isset($_POST['GFReturPembelianT'])){
//                
                $modRetur->attributes = $_POST['GFReturPembelianT'];
                $modRetur->penerimaanbarang_id = $modFaktur->penerimaanbarang_id;

            $transaction = Yii::app()->db->beginTransaction();
            try {     
//                                 
                $jumlahCekList=0;
                $jumlahSave=0;
                $modRetur = new GFReturPembelianT;
              
                $modRetur->attributes=$_POST['GFReturPembelianT'];
                $modRetur->ruangan_id=Yii::app()->user->getState('ruangan_id');
                $modRetur->penerimaanbarang_id = $modFaktur->penerimaanbarang_id;
                $modRetur->create_loginpemakai_id = Yii::app()->user->id;
                $modRetur->update_loginpemakai_id = Yii::app()->user->id;
                $modRetur->create_ruangan = Yii::app()->user->getState('ruangan_id');
                $modRetur->create_time = date('Y-m-d H:i:s');
                $modRetur->update_time = date('Y-m-d H:i:s');
                if($modRetur->save()){
                $jumlahObat=COUNT($_POST['GFReturDetailT']['obatalkes_id']);
                    for($i=0; $i<=$jumlahObat; $i++):
                       if($_POST['checkList'][$i]=='1'){
                            $jumlahCekList++;
                            $modReturDetails = new GFReturDetailT;
                            $modReturDetails->penerimaandetail_id=$_POST['GFReturDetailT']['penerimaandetail_id'][$i];
                            $modReturDetails->obatalkes_id=$_POST['GFReturDetailT']['obatalkes_id'][$i];
                            $modReturDetails->satuanbesar_id=$_POST['GFReturDetailT']['satuanbesar_id'][$i];
                            $modReturDetails->fakturdetail_id=$_POST['GFReturDetailT']['fakturdetail_id'][$i];
                            $modReturDetails->sumberdana_id=$_POST['GFReturDetailT']['sumberdana_id'][$i];
                            $modReturDetails->returpembelian_id=$modRetur->returpembelian_id;
                            $modReturDetails->satuankecil_id=$_POST['GFReturDetailT']['satuankecil_id'][$i];
                            $modReturDetails->jmlretur=$_POST['GFReturDetailT']['jmlretur'][$i];                       
                            $modReturDetails->harganettoretur=$_POST['GFReturDetailT']['harganettoretur'][$i];
                            $modReturDetails->hargappnretur=$_POST['GFReturDetailT']['hargappnretur'][$i];
                            $modReturDetails->hargapphretur=$_POST['GFReturDetailT']['hargapphretur'][$i];
                            $modReturDetails->jmldiscount=$_POST['GFReturDetailT']['jmldiscount'][$i];
                            $modReturDetails->hargasatuanretur=$_POST['GFReturDetailT']['hargasatuanretur'][$i];
                            
                            //ini digunakan untuk mendapatkan jumalah terima dari tabel faktur detail
                            $fd = FakturdetailT::model()->findByPk($modReturDetails->fakturdetail_id);
                            $idfd = $fd->fakturdetail_id;
                            $jum1 = $fd->jmlterima;
                            $jum2 = $modReturDetails->jmlretur;
                            $jumupdate = $jum1-$jum2;
                           
                            
                            if($modReturDetails->save()){
                                $jumlahSave++;
                                GFPenerimaanDetailT::model()->updateByPk($modReturDetails->penerimaandetail_id,
                                                                        array('returdetail_id'=>$modReturDetails->returdetail_id));
                                
                                //ini digunakan untuk mengupdata tabel faktur detail dan penerimaan detail ketika terjadi retur
                                FakturdetailT::model()->updateByPk($idfd, array('jmlterima'=>$jumupdate));
                                PenerimaandetailT::model()->updateByPk($modReturDetails->penerimaandetail_id, array('jmlterima'=>$jumupdate));
                                //========================================================
                                
                                $idStokObatAlkes=GFPenerimaanDetailT::model()->findByPk($modReturDetails->penerimaandetail_id)->stokobatalkes_id;
                                $stokObatAlkesIN=GFStokObatAlkesT::model()->findByPk($idStokObatAlkes)->qtystok_in;
                                $stokCurrent=GFStokObatAlkesT::model()->findByPk($idStokObatAlkes)->qtystok_current;
                                $stokINBaru=$stokObatAlkesIN - $modReturDetails->jmlretur;
                                $stokCurrentBaru=$stokCurrent - $modReturDetails->jmlretur;
                                GFStokObatAlkesT::model()->updateByPk($idStokObatAlkes,array('qtystok_in'=>$stokINBaru,
                                                                                             'qtystok_current'=>$stokCurrentBaru));
                            }
                    }
                    endfor;
                    
                 }

                 if(($jumlahCekList==$jumlahSave) and ($jumlahCekList>0)){
                     $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                        $tersimpan=true;
                     
                 }else{
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        $transaction->rollback();
                 }
             }catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }    
                    
            }
            
           
             
            
            $this->render($this->pathView.'retur',array('modFaktur'=>$modFaktur,
                                        'modFakturDetail'=>$modFakturDetail,
                                        'modRetur'=>$modRetur,
                                        'modReturDetails'=>$modReturDetails,
                                        'tersimpan'=>$tersimpan
                                       ));
        }
        
       
        public function actionSearch()
        {
            $modFaktur = new GFFakturPembelianT;
            $format = new CustomFormat();
            $modFaktur->tglAwal=date('d M Y 00:00:00');
            $modFaktur->tglAkhir=date('d M Y  H:i:s');
            
            if(isset($_GET['GFFakturPembelianT'])){
                $modFaktur->attributes=$_GET['GFFakturPembelianT'];
                $modFaktur->tglAwal  = $format->formatDateMediumForDB($_REQUEST['GFFakturPembelianT']['tglAwal']);
                $modFaktur->tglAkhir = $format->formatDateMediumForDB($_REQUEST['GFFakturPembelianT']['tglAkhir']);
            }
            
            $this->render($this->pathView.'search',array('modFaktur'=>$modFaktur));
        }


		public function actionIndex()
		{
			$insert_notifikasi = new MyFunction();
			$format = new CustomFormat();
			$fakturDetail = null;
			$modPenerimaanItems = new GFPenerimaanBarangT;
			$modFakturPembelian = new GFFakturPembelianT;
			
			$modFakturPembelian->tglfaktur=date('d M Y H:i:s');
			$modFakturPembelian->ruangan_id = Yii::app()->user->getState('ruangan_id');
			$modFakturPembelian->notifikasi = 0;
			$modFakturPembelian->tglfaktur = Yii::app()->dateFormatter->formatDateTime(
				CDateTimeParser::parse($modFakturPembelian->tglfaktur, 'yyyy-MM-dd'),'medium',null
			);
			$modFakturPembelian->tgljatuhtempo = Yii::app()->dateFormatter->formatDateTime(
				CDateTimeParser::parse($modFakturPembelian->tgljatuhtempo, 'yyyy-MM-dd'),'medium',null
			);
			
			if(isset($_POST["GFFakturPembelianT"]))
			{
				$pesan = array();
				$transaction = Yii::app()->db->beginTransaction();
				try{
					$modPenerimaanItems->attributes = $_POST["GFPenerimaanBarangT"];
					$modFakturPembelian->attributes = $_POST["GFFakturPembelianT"];
					$modFakturPembelian->tglfaktur = $format->formatDateMediumForDB($_POST['GFFakturPembelianT']['tglfaktur']);
					$modFakturPembelian->tgljatuhtempo = $format->formatDateMediumForDB($_POST['GFFakturPembelianT']['tgljatuhtempo']);
					
					if($modFakturPembelian->validate())
					{
						$modFakturPembelian->save();
					}else{
						foreach($modFakturPembelian->getErrors() as $key=>$val)
						{
							$pesan[] = $key . " : " . implode(", ", $val);
						}
					}
					
					if(isset($_POST["FakturdetailT"]))
					{
						$fakturDetail = null;
						foreach($_POST["FakturdetailT"] as $key=>$val)
						{
							$fakturDetail[$key] = $_POST["FakturdetailT"][$key];
							
							$modFakturDetail = new FakturdetailT;
							$modFakturDetail->attributes = $_POST["FakturdetailT"][$key];
							$modFakturDetail->fakturpembelian_id = $modFakturPembelian->fakturpembelian_id;
							
							if($modFakturDetail->validate())
							{
								$modFakturDetail->save();
							}else{
								foreach($modFakturDetail->getErrors() as $key=>$val)
								{
									$pesan[] = $key . " : " . implode(", ", $val);
								}
							}
						}
					}
					
					$penerimaanBarangT = GFPenerimaanBarangT::model()->findByPk($modFakturPembelian->penerimaanbarang_id);
					$penerimaanBarangT->fakturpembelian_id = $modFakturPembelian->fakturpembelian_id;
					if($penerimaanBarangT->validate())
					{
						$penerimaanBarangT->save();
					}else{
						foreach($penerimaanBarangT->getErrors() as $key=>$val)
						{
							$pesan[] = $key . " : " . implode(", ", $val);
						}
					}
					
					if(count($pesan) > 0)
					{
						throw new Exception(implode($pesan, "<br>"));
						$transaction->rollback();
						
					}else{
						$transaction->commit();
						Yii::app()->user->setFlash('success',"Data berhasil disimpan");
					}
				}catch(Exception $exc){
					$transaction->rollback();
					Yii::app()->user->setFlash('error',"Data gagal disimpan ". MyExceptionMessage::getWarning($exc,true));
				}
			}
			
			$this->render($this->pathView.'index',array(
				'fakturDetail'=>$fakturDetail,
				'modPenerimaanItems'=>$modPenerimaanItems,
				'modFakturPembelian'=>$modFakturPembelian,
				'modRekenings'=>$modRekenings,
				'jmlUang'=>$jmlUang,
			));
		}
        
		public function actionIndex_bak($id=null,$idFaktur=null)
		{
                $insert_notifikasi = new MyFunction();
                $modFakturDetail = null;
                $modPenerimaanItems = new GFPenerimaanBarangT;
                $modFakturPembelian = new GFFakturPembelianT;
//                $modFakturPembelian->nofaktur=Generator::noFaktur();
                $modFakturPembelian->tglfaktur=date('d M Y H:i:s');
                // $modFakturPembelian->tgljatuhtempo= mktime(0,0,0,date('d')+($modFakturPembelian->notifikasi),date('M')+0,date('Y')+0);
                $modFakturPembelian->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modFakturPembelian->notifikasi = 0;
                // $modFakturPembelian->modul_id = ModulK::model()->findByAttributes(array('modul_id'));
                $format = new CustomFormat();
                $modRekenings = array();
                // echo "Test<pre>";
                //   print_r($modFakturPembelian->attributes);
                //   exit;
                
                if (!empty($id) && (empty($idFaktur))){
                    $modPenerimaanItems = GFPenerimaanBarangT::model()->findByAttributes(array('penerimaanbarang_id'=>$id,'fakturpembelian_id'=>null));
                    if (count($modPenerimaanItems) == 1){
                        $modFakturPembelian->supplier_id = $modPenerimaanItems->supplier_id;
                        $modFakturPembelian->penerimaanbarang_id = $modPenerimaanItems->penerimaanbarang_id;
                        $modPenerimaanDetail = PenerimaandetailT::model()->findAllByAttributes(array("penerimaanbarang_id"=>$modPenerimaanItems->penerimaanbarang_id));
                        $totalDiskon = 0;
                        $persenDiskon = 0;
                        $totHarga = 0;
                        $totHargaBruto = 0;
                        $totPPN = 0;
                        $totPPH = 0;
                        foreach ($modPenerimaanDetail as $key => $value) {
                            $obatAlkes = ObatalkesM::model()->findByPk($value->obatalkes_id);
                            $modFakturDetail[$key] = new FakturdetailT();
                            $modFakturDetail[$key]->attributes = $value->attributes;
                            $modFakturDetail[$key]->hargappnfaktur = $value->hargappnper;
                            $modFakturDetail[$key]->hargapphfaktur = $value->hargapphper;
//                            $modFakturDetail[$key]->harganettofaktur = ($obatAlkes->harganetto * $value->jmlterima) * $obatAlkes->kemasanbesar;
                            $modFakturDetail[$key]->harganettofaktur = ($obatAlkes->harganetto * $obatAlkes->kemasanbesar);
                            $modFakturDetail[$key]->hargasatuan = $value->hargasatuanper ;
                            $modFakturDetail[$key]->jmlpermintaan = $value->jmlpermintaan;
                            $totalDiskon += $modFakturDetail[$key]->jmldiscount;
//                            $persenDiskon = $modFakturDetail[$key]->persendiscount;
                            $totHarga += ($modFakturDetail[$key]->harganettofaktur);
                            $totHargaBruto += (($modFakturDetail[$key]->harganettofaktur+$modFakturDetail[$key]->hargappnfaktur+$modFakturDetail[$key]->hargapphfaktur)*$modFakturDetail[$key]->jmlterima)-$modFakturDetail[$key]->jmldiscount;
                            //$totPPN += $modFakturDetail[$key]->hargappnfaktur*$modFakturDetail[$key]->jmlterima;
                            $totPPN += $modFakturDetail[$key]->hargappnfaktur;
                            //$totPPH += $modFakturDetail[$key]->hargapphfaktur*$modFakturDetail[$key]->jmlterima;
                            $totPPH += $modFakturDetail[$key]->hargapphfaktur;
                        }
//                        $modFakturPembelian->persendiscount = $persenDiskon;
                        $modFakturPembelian->persendiscount = $modPenerimaanItems->persendiscount;
                        $modFakturPembelian->jmldiscount = $totalDiskon;
                        $modFakturPembelian->totharganetto = $totHarga;
                        $modFakturPembelian->totalhargabruto = $totHargaBruto - $modFakturPembelian->jmldiscount;
                        $modFakturPembelian->totalpajakppn = $totPPN;
                        $modFakturPembelian->totalpajakpph = $totPPH;
                    }
                    else{
                        $modPenerimaanItems = new GFPenerimaanBarangT;
                    }
                }else if (!empty($idFaktur)){
                    $modFakturPembelian = GFFakturPembelianT::model()->findByPk($idFaktur);
                    if ((boolean)count($modFakturPembelian)){
                        $modFakturDetail = GFFakturDetailT::model()->findAllByAttributes(array('fakturpembelian_id'=>$modFakturPembelian->fakturpembelian_id));
                    }
                    $modUangMuka = UangmukabeliT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$modFakturPembelian->penerimaan->permintaanpembelian_id));
                    foreach($modUangMuka as $u=>$uang){
                        $jmlUang += $uang->jumlahuang;
                    }
                }
                $modFakturPembelian->persendiscount = (!empty($modFakturPembelian->persendiscount)) ? $modFakturPembelian->persendiscount : 0;
                $modFakturPembelian->jmldiscount = (!empty($modFakturPembelian->jmldiscount)) ? $modFakturPembelian->jmldiscount : 0;
                $modFakturPembelian->totharganetto = (!empty($modFakturPembelian->totharganetto)) ? $modFakturPembelian->totharganetto : 0;
                $modFakturPembelian->totalhargabruto = (!empty($modFakturPembelian->totalhargabruto)) ? $modFakturPembelian->totalhargabruto : 0;
                //$modFakturPembelian->totalpajakppn = (!empty($modFakturPembelian->totalpajakppn)) ? $modFakturPembelian->totalpajakppn : 0;
                //$modFakturPembelian->totalpajakpph = (!empty($modFakturPembelian->totalpajakpph)) ? $modFakturPembelian->totalpajakpph : 0;
                
                if(isset($_POST['GFFakturPembelianT'])){       
                   $modFakturPembelian->attributes=$_POST['GFFakturPembelianT'];
                   // $modFakturPembelian->supplier_id=$_POST['GFFakturPembelianT']['supplier_id'];
                   //      $day = $_POST['GFFakturPembelianT']['notifikasi'];
                        // $tglnotif = ;
                        // $modFakturPembelian->tgljatuhtempo = date('d M Y', strtotime('-'.$day.' days'));
                    
                  //   $params['create_time'] = date( 'Y-m-d H:i:s');
                  //   $params['create_loginpemakai_id'] = Yii::app()->user->id;
                  //   $params['instalasi_id'] = 129;
                  //   $params['modul_id'] = 26;
                  //   $params['isinotifikasi'] = 'No Faktur '.$modFakturPembelian->nofaktur . ' dengan supplier' . $modFakturPembelian->supplier_id
                  //                              . ' pembayaran akan jatuh tempo pada tanggal ' . $modFakturPembelian->tgljatuhtempo;
                  //   $params['create_ruangan'] = $model->ruangan_id;
                  //   $params['judulnotifikasi'] = 'Pemberitahuan tanggal jatuh tempo';
                  //   $params['tglnotifikasi'] = ($modFakturPembelian->tgljatuhtempo = date('d M Y', strtotime('-'.$day.' days')));
                  //   $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                    
                  //   echo "Test<pre>";
                  // print_r($params);
                  // exit;
                   if (isset($_POST['FakturdetailT'])){
                        $data = $this->validasiTabular($_POST['FakturdetailT']);
                        $modFakturDetail = $data['isi'];
                        $return = $data["data"];
                        //$modFakturPembelian->totharganetto = $return['totHarga'];
                        //$modFakturPembelian->totalpajakppn = $return['totPPN'];
                        //$modFakturPembelian->totalpajakpph = $return['totPPH'];
                   }
                   if (!empty($_POST['idPenerimaanBarang'])){
                        $modPenerimaanItems = GFPenerimaanBarangT::model()->findByAttributes(array('penerimaanbarang_id'=>$_POST['idPenerimaanBarang'],'fakturpembelian_id'=>null));
                        if (count($modPenerimaanItems) == 1){
                            $modFakturPembelian->penerimaanbarang_id = $modPenerimaanItems->penerimaanbarang_id;
                            $modFakturPembelian->supplier_id = $modPenerimaanItems->supplier_id;
                        }
                   }
                   $transaction = Yii::app()->db->beginTransaction();
                   try {  
                       $modFakturPembelian->tglfaktur=$format->formatDateMediumForDB($_POST['GFFakturPembelianT']['tglfaktur']);
                       $modFakturPembelian->tgljatuhtempo=$format->formatDateMediumForDB($_POST['GFFakturPembelianT']['tgljatuhtempo']);
                       $modFakturPembelian->ruangan_id=Yii::app()->user->getState('ruangan_id');
                       if($modFakturPembelian->validate()){
                           $postRekenings = $_POST['RekeningsupplierV'];
                           if(isset($postRekenings)){//simpan jurnal rekening
                               if(count($postRekenings) > 0){
                                $modJurnalRekening = PembayaranController::saveJurnalRekening();
                                //update jurnalrekening_id
                                $modFakturPembelian->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
        //                      ADA DI BAWAH >> $modTandaBukti->save();
                                $saveDetailJurnal = FakturPembelianController::saveJurnalDetails($modJurnalRekening, $postRekenings, null);
                               }
                           }
                           $modFakturPembelian->save();
                           GFPenerimaanBarangT::model()->updateByPk($modFakturPembelian->penerimaanbarang_id, array(
                                                                                'fakturpembelian_id'=>$modFakturPembelian->fakturpembelian_id,
                                                                                'tglterimafaktur'=>$modFakturPembelian->tglfaktur,
                                                                                'jmldiscount'=>$modFakturPembelian->jmldiscount,
                                                                                'persendiscount'=>$modFakturPembelian->persendiscount,
                                                                                'totalpajakppn'=>$modFakturPembelian->totalpajakppn,
                                                                                'totalpajakpph'=>$modFakturPembelian->totalpajakpph,
                                                                                'totalharga'=>$modFakturPembelian->totalhargabruto,
                                                                                'harganetto'=>$modFakturPembelian->totharganetto,    
                                                                        ));
                           $jumlahPengecekan=0;
                           $jumlahPenerimaan =COUNT($modFakturDetail);
                           if ($jumlahPenerimaan > 0){
                               $idSupplier = $modFakturPembelian->supplier_id;
                               foreach ($modFakturDetail as $key => $value) {
                                   $value->fakturpembelian_id = $modFakturPembelian->fakturpembelian_id;
                                   if ($value->save()){
                                       $jumlahPengecekan++;
                                       $konfigFarmasi = KonfigfarmasiK::model()->find();
                                        $persenJual = $konfigFarmasi->totalpersenhargajual/100;
                                        $persenPPN = 0; //default persen ppn
                                        $obatAlkes = ObatalkesM::model()->findByPk($value->obatalkes_id);
                                        if(!empty($_POST['GFFakturPembelianT']['totalpajakppn'])){
                                            $persenPPN = $konfigFarmasi->persenppn;
                                            if($persenPPN > 0)
                                                $PPN = $value->harganettofaktur * $persenPPN / 100;
                                            else
                                                $PPN = 0;        
                                            $nettoPPN = $value->harganettofaktur + $PPN;
                                            $hargaJual = $nettoPPN*$persenJual; 
                                        }else {
                                            $nettoPPN = $value->harganettofaktur;
                                            $hargaJual = $nettoPPN*$persenJual;
                                        }
                                        //Update Obat Alkes
                                        $idObatAlkes = $value->obatalkes_id;
										/*
                                        GFObatalkesM::model()->updateByPk($idObatAlkes, 
                                            array(
                                                'harganetto'=>($value->harganettofaktur/$obatAlkes->kemasanbesar),
                                                'discount'=>$value->persendiscount,
//                                                'harganetto'=>$value->harganettofaktur,
                                                'ppn_persen'=>$persenPPN,
                                        ));
                                        */
                                        //Tambah Obat Supplier Baru
                                        $idObatSupplier = ObatsupplierM::model()->findByAttributes(array('obatalkes_id'=>$value->obatalkes_id, 'obatsupplier_id'=>$idSupplier))->obatsupplier_id;
                                        if(empty($idObatSupplier) || $idObatSupplier < 1){
                                            $this->tambahObatSupplier($idSupplier,$value,$persenPPN);
                                        }else{
                                            //Update Obat Supplier
                                            ObatsupplierM::model()->updateByPk($idObatSupplier, 
                                                array(
													'satuankecil_id'=>$value->satuankecil_id,
                                                    'satuanbesar_id'=>$value->satuanbesar_id,
                                                    'hargabelikecil'=>$value->harganettoper,
                                                    'hargabelibesar'=>$value->hargabelibesar,
                                                    'diskon_persen'=>$value->persendiscount,
                                                    'ppn_persen'=>$persenPPN,
                                            ));
                                        }
                                       //Update Stok Obat Alkes
                                       $idStokObatAlkes=GFPenerimaanDetailT::model()->findByPk($value->penerimaandetail_id)->stokobatalkes_id;
                                       GFStokObatAlkesT::model()->updateByPk($idStokObatAlkes,
                                            array('harganetto_oa'=>$value->harganettofaktur,
                                             'hargajual_oa'=>$value->harganettofaktur * (Params::persenHargaJual() / 100),
                                             'discount'=>$value->persendiscount
                                        ));
                                       //Update Penerimaan Detail
                                       GFPenerimaanDetailT::model()->updateByPk($value->penerimaandetail_id,
                                            array('persendiscount'=>$value->persendiscount,
                                               'jmldiscount'=>$value->jmldiscount,
                                               'hargappnper'=>$value->hargappnfaktur,
                                               'hargapphper'=>$value->hargapphfaktur,
                                               'fakturdetail_id'=>$value->fakturdetail_id,
                                        ));
                                   }
                               }
                           }
                           // $modFakturPembelian->tgljatuhtempo= mktime(0,0,0,date('d')+($modFakturPembelian->notifikasi),date('M')+0,date('Y')+0);
                           //  $modFakturPembelian->tgljatuhtempo;exit();
                            $modFakturPembelian->supplier_id=$_POST['GFFakturPembelianT']['supplier_id'];
                            $day = $_POST['GFFakturPembelianT']['notifikasi'];
                            $params['create_time'] = date( 'Y-m-d H:i:s');
                            $params['create_loginpemakai_id'] = Yii::app()->user->id;
                            $params['instalasi_id'] = 129;
                            $params['modul_id'] = 26;
                            $params['isinotifikasi'] = 'No Faktur '.$modFakturPembelian->nofaktur . ' dengan supplier' . $modFakturPembelian->supplier_id
                                                       . ' pembayaran akan jatuh tempo pada tanggal ' . $modFakturPembelian->tgljatuhtempo;
                            $params['create_ruangan'] = $model->ruangan_id;
                            $params['judulnotifikasi'] = 'Pemberitahuan tanggal jatuh tempo';
                            $params['tglnotifikasi'] = ($modFakturPembelian->tgljatuhtempo = date('d M Y', strtotime('-'.$day.' days')));
                            $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                            
                          //   echo "Test<pre>";
                          // print_r($params);
                          // exit;
                           if($jumlahPengecekan==$jumlahPenerimaan && $jumlahPenerimaan > 0){
                               $transaction->commit();
                               Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                               $this->redirect(array('index','idFaktur'=>$modFakturPembelian->fakturpembelian_id));
                           }else{
                               $modFakturPembelian->isNewRecord = true;
                               if ($jumlahPenerimaan < 1){
                                   $modFakturPembelian->addError("error",'detail Faktur harus diisi');
                               }
                               $transaction->rollback();
                               Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                           }
                       }
                   }catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }
                
                $modFakturPembelian->tglfaktur = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modFakturPembelian->tglfaktur, 'yyyy-MM-dd'),'medium',null);
                    
                $modFakturPembelian->tgljatuhtempo = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modFakturPembelian->tgljatuhtempo, 'yyyy-MM-dd'),'medium',null);
                
		$this->render($this->pathView.'index',array(
                                            'modFakturDetail'=>$modFakturDetail,
                                            'modPenerimaanItems'=>$modPenerimaanItems,
                                            'modFakturPembelian'=>$modFakturPembelian,
                                            'modRekenings'=>$modRekenings,
                                            'jmlUang'=>$jmlUang,
                                            ));
	}
        private function validasiTabular($datas){
            $model = null;
            if (count($datas)>0){
                $totalDiskon = 0;
                $persenDiskon = 0;
                $totHarga = 0;
                $totHargaBruto = 0;
                $totPPN = 0;
                $totPPH = 0;
                foreach ($datas as $key => $value) {
                    $models[$key] = new FakturdetailT();
                    $models[$key]->attributes = $value;
                    $models[$key]->fakturpembelian_id = 1;
                    $diskon = (($models[$key]->persendiscount > 0) && ($models[$key]->harganettofaktur > 0)) ? $models[$key]->harganettofaktur*($models[$key]->persendiscount/100) : 0;
                    $models[$key]->hargasatuan = $models[$key]->harganettofaktur + $models[$key]->hargappnfaktur + $models[$key]->hargapphfaktur - $diskon;
                    $models[$key]->validate();
                    
                    $totalDiskon += $models[$key]->jmldiscount;
                    $persenDiskon = $models[$key]->persendiscount;
                    $totHarga += ($models[$key]->harganettofaktur*$models[$key]->jmlterima);
                    $totHargaBruto += (($models[$key]->harganettofaktur+$models[$key]->hargappnfaktur+$models[$key]->hargapphfaktur)*$models[$key]->jmlterima)-$models[$key]->jmldiscount;
                    //$totPPN += $models[$key]->hargappnfaktur;
                    $totPPN += $models[$key]->hargappnfaktur*$models[$key]->jmlterima;
                    //$totPPH += $models[$key]->hargapphfaktur;
                    $totPPH += $models[$key]->hargapphfaktur*$models[$key]->jmlterima;
                }
                $return['totalDiskon'] = (!empty($totalDiskon)) ? $totalDiskon : 0 ;
                $return['persenDiskon'] = (!empty($persenDiskon)) ? $persenDiskon : 0 ;
                $return['totHarga'] = (!empty($totHarga)) ? $totHarga : 0 ;
                $return['totHargaBruto'] = (!empty($totHargaBruto)) ? $totHargaBruto : 0 ;
                $return['totPPN'] = (!empty($totPPN)) ? $totPPN : 0 ;
                $return['totPPH'] = (!empty($totPPH)) ? $totPPH : 0 ;
                
                $model['isi'] = $models;
                $model['data'] = $return;
                
//                echo "<pre>";
//                echo print_r($models[$key]->getAttributes());exit;
            }
            
            return $model;
        }
    /**
    * tambahObatSupplier = insert data ObatsupplierM baru jika ada yang baru
    * @param type $supplierId,$obatDetail,$persenPPN
    * @return boolean
    */
    private function tambahObatSupplier($supplierId,$obatDetail,$persenPPN = 0){
        $sukses = false;
        $obatBaru = new ObatsupplierM;
        $obatBaru->attributes = $obatDetail->attributes;
        $obatBaru->supplier_id = $supplierId;
        $obatBaru->ppn_persen = $persenPPN;
        $obatBaru->diskon_persen = $obatDetail->persendiscount;
        $obatBaru->hargabelikecil = $obatDetail->harganettofaktur;
        $obatBaru->hargabelibesar = $obatDetail->harganettofaktur * $obatDetail->jmlkemasan;
        if($obatBaru->save()){
            $sukses = true;
        }
        return $sukses;
    }
    
    /**
     * insert detail dari post rekening digunakan di:
     * - rawatJalan/TindakanController
     * @param type $modJurnalRekening
     * @param type $postRekenings
     * @param type $jenisPelayanan = 'tm' / 'oa' / 'ap'
     * @param type $jenisSimpan
     * @return boolean
     */
    public function saveJurnalDetails($modJurnalRekening, $postRekenings, $jenisSimpan = null){
        $valid = true;
        $modJurnalPosting = null;
        if($jenisSimpan == 'posting')
        {
            $modJurnalPosting = new JurnalpostingT;
            $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
            $modJurnalPosting->keterangan = "Posting automatis";
            $modJurnalPosting->create_time = date('Y-m-d H:i:s');
            $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
            $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
            if($modJurnalPosting->validate()){
                $modJurnalPosting->save();
            }
        }

        foreach($postRekenings AS $i => $rekening){
            $model[$i] = new JurnaldetailT();
            $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
            $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
            $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
            $model[$i]->uraiantransaksi = $rekening['nama_rekening'];
            $model[$i]->saldodebit = $rekening['saldodebit'];
            $model[$i]->saldokredit = $rekening['saldokredit'];
            $model[$i]->nourut = $i+1;
            $model[$i]->rekening1_id = $rekening['struktur_id'];
            $model[$i]->rekening2_id = $rekening['kelompok_id'];
            $model[$i]->rekening3_id = $rekening['jenis_id'];
            $model[$i]->rekening4_id = $rekening['obyek_id'];
            $model[$i]->rekening5_id = $rekening['rincianobyek_id'];
            $model[$i]->catatan = "FAKTUR PEMBELIAN";
            if($model[$i]->validate()){
                $model[$i]->save();
            }else{
//                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $model[$i]->getErrors();
                $valid = false;
                break;
            }
        }
        return $valid;        
    }
}