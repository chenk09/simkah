<?php

class PermintaanPenawaranController extends SBaseController
{
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','search','details'),
				'users'=>array('@'),
			),
		
		);
	}
        
        public function actionDetails($idPermintaanPenawaran)
        {
            $this->layout='//layouts/frameDialog';
            $modPermintaanPenawaran = GFPermintaanPenawaranT::model()->findByPk($idPermintaanPenawaran);
            $modPermintaanPenawaranDetails = PenawarandetailT::model()->findAll('permintaanpenawaran_id='.$idPermintaanPenawaran.'');
        
            $this->render('details',array('modPermintaanPenawaran'=>$modPermintaanPenawaran,
                                         'modPermintaanPenawaranDetails'=>$modPermintaanPenawaranDetails));
        }
        
        public function actionSearch()
        {
            $modPermintaanPenawaran = new GFPermintaanPenawaranT;
            $format = new CustomFormat();
            $modPermintaanPenawaran->tglAwal=date('d M Y 00:00:00');
            $modPermintaanPenawaran->tglAkhir=date('d M Y H:i:s');
            
            if(isset($_GET['GFPermintaanPenawaranT'])){
                $modPermintaanPenawaran->attributes=$_GET['GFPermintaanPenawaranT'];
                $modPermintaanPenawaran->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['GFPermintaanPenawaranT']['tglAwal']);
                $modPermintaanPenawaran->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFPermintaanPenawaranT']['tglAkhir']);
            }
            $this->render('search',array('modPermintaanPenawaran'=>$modPermintaanPenawaran));
	}
        
	public function actionIndex($idRencana=null, $idPenawaran=null)
	{
            $modRencanaKebFarmasi = new GFRencanaKebFarmasiT;
            $modPermintaanPenawaran = new GFPermintaanPenawaranT;
            $modPermintaanPenawaran->nosuratpenawaran=Generator::noPenawaran();
            $modPermintaanPenawaran->harganettopenawaran=0; 
            $modPermintaanPenawaran->tglpenawaran=date('Y-m-d H:i:s');
            $modDetailRencana = array();
            $modDetails = null;
            $var['supplier_id'] = 0;
			
			if(empty($idPenawaran))
			{
				$permintaanpenawaran = GFPermintaanPenawaranT::model()->findByAttributes(array(
					"rencanakebfarmasi_id"=>$idRencana
				));
				if($permintaanpenawaran)
				{
					$idPenawaran = $permintaanpenawaran->permintaanpenawaran_id;
				}
			}
			
            if(!empty($idRencana) && (empty($idPenawaran))){
                $modRencanaKebFarmasi = GFRencanaKebFarmasiT::model()->findByPk($idRencana);
                if (count($modRencanaKebFarmasi) == 1){
                    $modDetailRencana = GFRencDetailkebT::model()->findAllByAttributes(array('rencanakebfarmasi_id'=>$idRencana));
                    $hargaPermintaanPenawaran = 0;
                    if (count($modDetailRencana) > 0){
                        foreach ($modDetailRencana as $i => $rencana) {
                            $harga = $rencana->jmlpermintaan * $rencana->harganettorenc;
                            $hargaPermintaanPenawaran = $hargaPermintaanPenawaran + $harga;
                            $modDetails[$i] = new PenawarandetailT();
                            $modDetails[$i]->attributes = $rencana->attributes;
                            $modDetails[$i]->harganetto = $rencana->harganettorenc;
                            $modDetails[$i]->qty = $rencana->jmlpermintaan;
                            $modDetails[$i]->jmlkemasan = $rencana->jmlkemasan;
                            $modDetails[$i]->hargabelibesar = $rencana->harganettorenc * $rencana->jmlkemasan;
                        }
                    }
                    
                    $modPermintaanPenawaran->rencanakebfarmasi_id = $modRencanaKebFarmasi->rencanakebfarmasi_id;
                    $modPermintaanPenawaran->harganettopenawaran = $hargaPermintaanPenawaran;
                }
            }else if (!empty($idPenawaran)){
                $modPermintaanPenawaran = GFPermintaanPenawaranT::model()->findByPk($idPenawaran);
                if (count($modPermintaanPenawaran) == 1){
                    $modDetails = GFPenawaranDetailT::model()->findAllByAttributes(array('permintaanpenawaran_id'=>$idPenawaran));
                }
            }
            
            $format = new CustomFormat();

             if(isset($_POST['GFPermintaanPenawaranT'])){
                 $modDetails = $this->validasiTabular($_POST['PenawarandetailT']);
                 $transaction = Yii::app()->db->beginTransaction();
                 try {    
                    $modPermintaanPenawaran->attributes=$_POST['GFPermintaanPenawaranT'];
                    $var['supplier_id'] = (isset($modPermintaanPenawaran->supplier_id)) ? $modPermintaanPenawaran->supplier_id : $var['supplier_id'];
                    $modPermintaanPenawaran->tglpenawaran=$format->formatDateTimeMediumForDB($_POST['GFPermintaanPenawaranT']['tglpenawaran']);
                    $jumlahObat=COUNT($modDetails);
                    if($modPermintaanPenawaran->save()){//Jika Model Rencana Berhasil Disimpan
                          $jumlahCek=0;//digunakan untuk mengecek jumlah obat yang dipilih dengan jumlah yang tersimpan
                          if ($jumlahObat > 0){
                              foreach ($modDetails as $key => $value) {
                                  $value->permintaanpenawaran_id = $modPermintaanPenawaran->permintaanpenawaran_id;
                                  if (!$value->hasErrors()){
                                      if ($value->save()){
                                          $jumlahCek++;
                                      }
                                  }
                              }
                          }
                      }
                      
                    if($jumlahObat==$jumlahCek && ($jumlahCek > 0) && ($jumlahObat > 0)){//Jika Jumlah Obat yang di ceklist sama dengan jumlah Obat Yang Disimpan
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Permintaan Berhasil Disimpan");
                        $this->redirect(array('index', 'idPenawaran'=>$modPermintaanPenawaran->permintaanpenawaran_id));
                    } else {
                        $modPermintaanPenawaran->isNewRecord = true;
                        if ($jumlahObat < 1){
                            $modPermintaanPenawaran->addError('error', "Detail obat alkes harus dipilih");
                        }
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }catch(Exception $exc){
                    $modPermintaanPenawaran->isNewRecord = true;
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }    
            }
            
            $modPermintaanPenawaran->tglpenawaran = Yii::app()->dateFormatter->formatDateTime(
            CDateTimeParser::parse($modPermintaanPenawaran->tglpenawaran, 'yyyy-MM-dd hh:mm:ss')); 
                

            $this->render('index',array('modRencanaKebFarmasi'=>$modRencanaKebFarmasi,
                                        'modPermintaanPenawaran'=>$modPermintaanPenawaran,
                                        'modDetails'=>$modDetails,
                                        'var'=>$var,
                                        'modDetailRencana'=>$modDetailRencana));
	} 
        
        /**
         * method untuk validasi tabular
         * @param array $datas
         * @return PenawarandetailT 
         */
        private function validasiTabular($datas){
            $models = null;
            if (count($datas) > 0){
                foreach ($datas as $key => $data) {
                    $models[$key] = new PenawarandetailT();
                    $models[$key]->attributes = $data;
                    $models[$key]->permintaanpenawaran_id = 1; //fake id
                    $models[$key]->validate();
                    if ($models[$key]->qty < 1){
                        $models[$key]->addError('qty', 'Jumlah Quantity tidak boleh kurang dari 1');
                    }
                }
            }
            return $models;
        }
        
        /**
         * method untuk hasil print detail permintaan penawaran dengan parameter input id permintaan penawaran
         * digunakan pada halaman 
         * 1. Informasi Permintaan Penawaran -> detail permintaan penawaran -> print
         * 2. Transaksi Permintaan Penawaran setelah save tombol print akan muncul
         * 
         * @param int $idPenawaran
         * @param string $caraPrint 
         * 
         * print detail permintaan penawaran
         */
        public function actionPrint($idPenawaran,$caraPrint){
            $judulLaporan = 'Laporan Detail Permintaan Penawaran';
            $modPermintaanPenawaran = GFPermintaanPenawaranT::model()->findByPk($idPenawaran);
            $modPermintaanPenawaranDetails = PenawarandetailT::model()->findAll('permintaanpenawaran_id=:idPenawaran',array('idPenawaran'=>$idPenawaran));
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modPermintaanPenawaran'=>$modPermintaanPenawaran,
                                            'modPermintaanPenawaranDetails'=>$modPermintaanPenawaranDetails));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('modDetailRencana'=>$modDetailRencana,
                                            'judulLaporan'=>$judulLaporan,
                                            'modRencanaKebutuhan'=>$modRencanaKebutuhan));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('modDetailRencana'=>$modDetailRencana,
                                                                    'judulLaporan'=>$judulLaporan,
                                                                    'modRencanaKebutuhan'=>$modRencanaKebutuhan),true));
                $mpdf->Output();
            }   
        }
	
}