<?php
class TrxPermintaanPembelianController extends SBaseController
{
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions'=>array('index','search','details', 'print'),
				'users'=>array('@'),
			),
		);
	}

	public function actionIndex()
	{
		//error_reporting(1);
		//ini_set('display_errors', true);

		$format = new CustomFormat();
		$modPermintaanDetail = null;

		$modPermintaanPembelian = new GFPermintaanPembelianT;
		$modPermintaanPembelian->istermasukppn = 0;
		$modPermintaanPembelian->istermasukpph = 0;
		$modPermintaanPembelian->nopermintaan = Generator::noPembelian();
		$modPermintaanPembelian->tglpermintaanpembelian = $format->formatDateTimeMediumForDB(date('Y-m-d H:i:s'));

		if(isset($_POST["GFPermintaanPembelianT"]))
		{
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$pesan = array();
				$modLoginPemakai = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
				$modPermintaanPembelian->attributes = $_POST["GFPermintaanPembelianT"];
				$modPermintaanPembelian->pegawai_id = $modLoginPemakai->pegawai_id;
				$modPermintaanPembelian->instalasi_id = Yii::app()->user->getState('instalasi_id');
				$modPermintaanPembelian->ruangan_id = Yii::app()->user->getState('ruangan_id');
				$modPermintaanPembelian->tglpermintaanpembelian = $format->formatDateTimeMediumForDB($_POST['GFPermintaanPembelianT']['tglpermintaanpembelian']);

				if(!$modPermintaanPembelian->save())
				{
					foreach($modPermintaanPembelian->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}

				if(isset($_POST['PermintaandetailT']))
				{
					foreach($_POST['PermintaandetailT'] as $idx=>$params)
					{
						$new_PermintaandetailT = new PermintaandetailT;
						$new_PermintaandetailT->permintaanpembelian_id = 1;
						foreach($params as $x=>$z)
						{
							if($x != 'subTotal') $new_PermintaandetailT->$x = $z;
						}
						$new_PermintaandetailT->hargasatuanper = $new_PermintaandetailT->harganettoper;
						$new_PermintaandetailT->permintaanpembelian_id = $modPermintaanPembelian->permintaanpembelian_id;

						if(!$new_PermintaandetailT->save())
						{
							foreach($new_PermintaandetailT->getErrors() as $a=>$b)
							{
								$pesan[] = $a . " : " . implode(", ", $b);
							}
						}
					}
				}

				if(count($pesan) > 0)
				{
					throw new Exception(implode("<br>", $pesan));
				}else{
					$transaction->commit();
					Yii::app()->user->setFlash('success',"Data berhasil disimpan");
					$this->redirect(array('view', 'permintaanpembelian_id'=>$modPermintaanPembelian->permintaanpembelian_id));
				}

			}catch(Exception $exc){
				$transaction->rollback();
				Yii::app()->user->setFlash('error',"Data gagal disimpan ". MyExceptionMessage::getMessage($exc,true) );
			}
		}

        $this->render('index',array(
			"modPermintaanPembelian"=>$modPermintaanPembelian,
			"modPermintaanDetail"=>$modPermintaanDetail
		));
	}

	public function actionView($permintaanpembelian_id = null)
	{
		if(is_null($permintaanpembelian_id))
		{
			$this->redirect(array('index'));
		}

		$modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($permintaanpembelian_id);
		if(!$modPermintaanPembelian)
		{
			Yii::app()->user->setFlash('error',"ID tidak  terdaftar");
			$this->redirect(array('index'));
		}

		$modPermintaanDetail = PermintaandetailT::model()->findAllByAttributes(array(
			"permintaanpembelian_id"=>$permintaanpembelian_id
		));

        $this->render('view',array(
			"modPermintaanPembelian"=>$modPermintaanPembelian,
			"modPermintaanDetail"=>$modPermintaanDetail
		));
	}

	public function actionBatal($permintaanpembelian_id = null)
	{
		if(is_null($permintaanpembelian_id))
		{
			Yii::app()->user->setFlash('error',"ID Penerimaan kosong, coba cek ulang");
			$this->redirect(array('index'));
		}

		$modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($permintaanpembelian_id);
		if(!$modPermintaanPembelian)
		{
			Yii::app()->user->setFlash('error',"ID tidak  terdaftar");
			$this->redirect(array('index'));
		}

		if($modPermintaanPembelian && !is_null($modPermintaanPembelian->penerimaanbarang_id))
		{
			Yii::app()->user->setFlash('error',"Barang sudah diterima, coba cek ulang");
			$this->redirect(array('index'));
		}

		$modPermintaanDetail = PermintaandetailT::model()->findAllByAttributes(array(
			"permintaanpembelian_id"=>$permintaanpembelian_id
		));

		$transaction = Yii::app()->db->beginTransaction();
		try{
			$pesan = array();
			foreach($modPermintaanDetail as $key=>$val)
			{
				$_modPermintaanDetail = PermintaandetailT::model()->findByPk($val->permintaandetail_id)->delete();
				if(!$_modPermintaanDetail)
				{
					foreach($_modPermintaanDetail->getErrors() as $a=>$b)
					{
						$pesan[] = $a . " : " . implode(", ", $b);
					}
				}
			}
			if(!$modPermintaanPembelian->delete())
			{
				foreach($modPermintaanPembelian->getErrors() as $a=>$b)
				{
					$pesan[] = $a . " : " . implode(", ", $b);
				}
			}

			if(count($pesan) > 0)
			{
				throw new Exception(implode("<br>", $pesan));
			}else{
				$transaction->commit();
				Yii::app()->user->setFlash('success',"Data berhasil dibatalkan");
				$this->redirect(array('index'));
			}
		}catch(Exception $exc){
			$transaction->rollback();
			Yii::app()->user->setFlash('error',"Data gagal disimpan ". MyExceptionMessage::getMessage($exc,true));
		}

	}

	private function validasiTabular($datas)
	{
		$models = null;
		$format = new CustomFormat();
		if(count($datas) > 0)
		{
			foreach ($datas as $key => $data)
			{
				$models[$key] = new PermintaandetailT();
				$models[$key]->attributes = $data;
				if(!empty($data['tglkadaluarsa']))
				{
					$models[$key]->tglkadaluarsa=$format->formatDateMediumForDB($data['tglkadaluarsa']);
				}else{
					$models[$key]->tglkadaluarsa=null;
				}

				/*
				$models[$key]->permintaanpembelian_id = 1; //fake id
				$models[$key]->jmlpermintaan = $data['jmlpermintaan']; //fake id
				$diskon = (($models[$key]->harganettoper > 0) && ($models[$key]->persendiscount)) ? ($models[$key]->harganettoper*$models[$key]->persendiscount)/100 : 0;
				$models[$key]->hargasatuanper=($models[$key]->harganettoper + $models[$key]->hargappnper + $models[$key]->hargapphper ) - $diskon;

				if(!empty($data['tglkadaluarsa']))
				{
					$models[$key]->tglkadaluarsa=$format->formatDateMediumForDB($data['tglkadaluarsa']);
				}else{
					$models[$key]->tglkadaluarsa=null;
				}
				$models[$key]->validate();

				if($models[$key]->jmlpermintaan < 1)
				{
					$models[$key]->addError('$models[$key]', 'Jumlah Permintaan tidak boleh kurang dari 1');
				}
				*/
				$models[$key]->validate();
			}
		}
		return $models;
	}
}
