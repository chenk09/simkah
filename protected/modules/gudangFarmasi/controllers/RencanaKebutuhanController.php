<?php

class RencanaKebutuhanController extends SBaseController
{
        public $defaultAction ='rencanaKebutuhan';
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('rencanaKebutuhan','search','details', 'print'),
				'users'=>array('@'),
			),
		
		);
	}
        
        public function actionDetails($idRencana)
        {
            $this->layout='//layouts/frameDialog';
            $modDetailRencana = GFRencDetailkebT::model()->findAllByAttributes(array('rencanakebfarmasi_id'=>$idRencana));
            $modRencanaKebutuhan = GFRencanaKebFarmasiT::model()->findByPk($idRencana);
            
            $this->render('details',array('modDetailRencana'=>$modDetailRencana,
                                          'modRencanaKebutuhan'=>$modRencanaKebutuhan));

        }
        public function actionSearch()
        {
            $modRencanaKebFarmasi=new GFRencanaKebFarmasiT;
            $format = new CustomFormat();
            $modRencanaKebFarmasi->tglAwal=date('d M Y 00:00:00');
            $modRencanaKebFarmasi->tglAkhir=date('d M Y H:i:s');
            
            if(isset($_GET['GFRencanaKebFarmasiT'])){
                $modRencanaKebFarmasi->attributes=$_GET['GFRencanaKebFarmasiT'];
                $modRencanaKebFarmasi->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['GFRencanaKebFarmasiT']['tglAwal']);
                $modRencanaKebFarmasi->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFRencanaKebFarmasiT']['tglAkhir']);
            }
            $this->render('search',array('modRencanaKebFarmasi'=>$modRencanaKebFarmasi));
	}
        
        /**
         * method untuk transaksi rencana kebutuhan
         * digunakan di :
         * 1. gudang farmasi -> transaksi -> rencana kebutuhan
         * @param int $id rencanakebutuhan_id default null
         */
        public function actionRencanaKebutuhan($id=null)
	{
            $modRencanaKebFarmasi=new GFRencanaKebFarmasiT;
            $modDetails = null;
            $modRencanaKebFarmasi->noperencnaan=Generator::noPerencanaan();
            $modRencanaKebFarmasi->tglperencanaan=date('d M Y H:i:s');
            $format = new CustomFormat();
            
            if (!empty($id)){
                $modRencana = GFRencanaKebFarmasiT::model()->findByAttributes(array('rencanakebfarmasi_id'=>$id));
                if ((boolean)count($modRencana)){
                    unset($modRencanaKebFarmasi);
                    $modRencanaKebFarmasi = $modRencana;
                    $modDetails = GFRencDetailkebT::model()->findAllByAttributes(array('rencanakebfarmasi_id'=>$modRencanaKebFarmasi->rencanakebfarmasi_id));
                }
            }

            if(isset($_POST['GFRencanaKebFarmasiT']) && (empty($id))){
               
                 $modDetails = $this->validasiTabular($_POST['RencdetailkebT']);
               
                 $transaction = Yii::app()->db->beginTransaction();
                   try {
                          $modRencanaKebFarmasi=new GFRencanaKebFarmasiT;
                          $modRencanaKebFarmasi->attributes=$_POST['GFRencanaKebFarmasiT'];
                          $modRencanaKebFarmasi->ruangan_id=Yii::app()->user->getState('ruangan_id');
                          $modLogin = LoginpemakaiK::model()->findByPK(Yii::app()->user->id);
                          $modRencanaKebFarmasi->pegawai_id= $modLogin->pegawai_id;
                          $modRencanaKebFarmasi->tglperencanaan=$format->formatDateTimeMediumForDB($_POST['GFRencanaKebFarmasiT']['tglperencanaan']);
                          $modRencanaKebFarmasi->create_time = date('Y-m-d H:i:s');
                          $modRencanaKebFarmasi->create_loginpemakai_id = Yii::app()->user->id;
                          $modRencanaKebFarmasi->create_ruangan = Yii::app()->user->ruangan_id;
                          $jumlahObat=COUNT($modDetails);
                          
                          if($modRencanaKebFarmasi->save()){//Jika Model Rencana Berhasil Disimpan
                              $jumlahCek=0;//digunakan untuk mengecek jumlah obat yang dipilih dengan jumlah yang tersimpan
                              if ($jumlahObat > 0){
                                  foreach ($modDetails as $key => $detail) {
                                      $detail->rencanakebfarmasi_id = $modRencanaKebFarmasi->rencanakebfarmasi_id;
                                      $detail->tglkadaluarsa = (!empty($detail->tglkadaluarsa)) ? $format->formatDateMediumForDB($detail->tglkadaluarsa) : null;
                                      if (!$detail->hasErrors()){
                                          if ($detail->save()){
                                              $jumlahCek++;
                                          }
                                      }
                                  }
                              }
                          }
                          
                          if($jumlahCek==$jumlahObat && ($jumlahObat > 0) && ($jumlahCek > 0)){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data Rencana Berhasil Disimpan ");
                                $this->redirect(array('rencanaKebutuhan','id'=>$modRencanaKebFarmasi->rencanakebfarmasi_id));
                          } else {
                                $modRencanaKebFarmasi->isNewRecord = true;
                                if ($jumlahObat < 1){
                                    $modRencanaKebFarmasi->addError('error', 'Detail Rencana Kebutuhan belum Diisi');
                                }
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan");
                          }
                          
                       } catch(Exception $exc){
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                   }
                            
            }
                
            
//            $modRencanaKebFarmasi->tglperencanaan = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($modRencanaKebFarmasi->tglperencanaan, 'yyyy-MM-dd'),'medium',null);
            
            
            $this->render('RencanaKebutuhan',array('modDetails'=>$modDetails,'modRencanaKebFarmasi'=>$modRencanaKebFarmasi));
	}
        
        
        /**
         * method untuk validasi tabular
         * @param array $datas
         * @return RencdetailkebT 
         */
        private function validasiTabular($datas){
            $models = null;
            if (count($datas) > 0){
                foreach ($datas as $key => $data) {
                    $models[$key] = new RencdetailkebT();
                    $models[$key]->attributes = $data;
                    $models[$key]->rencanakebfarmasi_id = 1; //fake id
                    $models[$key]->hargappnrenc = 0; 
                    $models[$key]->hargapphrenc = 0; 
                    $models[$key]->hargatotalrenc=$models[$key]->jmlpermintaan * $models[$key]->jmlkemasan * ($models[$key]->harganettorenc + $models[$key]->hargappnrenc + $models[$key]->hargapphrenc);
                    $models[$key]->validate();
                    if ($models[$key]->jmlpermintaan < 1){
                        $models[$key]->addError('jmlpermintaan', 'Jumlah Permintaan tidak boleh kurang dari 1');
                    }
                }
            }
            return $models;
        }
 
        /**
         * method untuk hasil print detail rencana kebutuhan dengan parameter input id rencana kebutuhan
         * digunakan pada halaman 
         * 1. Informasi Rencana Kebutuhan -> detail rencana -> print
         * 2. Transaksi Recana Kebutuhan setelah save tombol print akan muncul
         * 
         * @param int $idRencana
         * @param string $caraPrint 
         * 
         * print detail rencana kebutuhan
         */
        public function actionPrint($idRencana,$caraPrint){
            $judulLaporan = 'Laporan Detail Rencana Kebutuhan';
            $modDetailRencana = GFRencDetailkebT::model()->findAllByAttributes(array('rencanakebfarmasi_id'=>$idRencana));
            $modRencanaKebutuhan = GFRencanaKebFarmasiT::model()->findByPk($idRencana);
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modDetailRencana'=>$modDetailRencana,
                                            'modRencanaKebutuhan'=>$modRencanaKebutuhan));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('modDetailRencana'=>$modDetailRencana,
                                            'judulLaporan'=>$judulLaporan,
                                            'modRencanaKebutuhan'=>$modRencanaKebutuhan));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('modDetailRencana'=>$modDetailRencana,
                                                                    'judulLaporan'=>$judulLaporan,
                                                                    'modRencanaKebutuhan'=>$modRencanaKebutuhan),true));
                $mpdf->Output();
            }   
        }

}