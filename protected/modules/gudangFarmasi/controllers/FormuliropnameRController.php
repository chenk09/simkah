
<?php

class FormuliropnameRController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $pathView = 'gudangFarmasi.views.formuliropnameR.';

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex($id = null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $modObat = new GFObatalkesM('search');
		if (!empty($id)){
                    $model = FormuliropnameR::model()->findByPK($id);
                    if ($model == 1){
                        $modDetail = FormstokopnameR::model()->findAllByAttributes(array('formuliropname_id'=>$model->formuliropname_id));
                        $modObat->obatalkes_id = CHtml::listData($modDetail, 'obatalkes_id', 'obatalkes_id');
                    }
                }
                else{
                    $format = new CustomFormat();
                    $model=new GFFormuliropnameR;
//                    $modObat->obatalkes_id = '';
                    $model->tglformulir = date('d M Y');
                    $model->totalharga = 0;
                    $model->totalvolume = 0;
                    $model->noformulir = Generator::noFormulirOpname();
                }               
                
//                $modObat->obatalkes_id = 0;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

                if(isset($_POST['GFFormuliropnameR']))
		{
			$model->attributes=$_POST['GFFormuliropnameR'];
                        $model->tglformulir = $format->formatDateMediumForDB($_POST['GFFormuliropnameR']['tglformulir']);
                        $modObat->unsetAttributes();
                        $modObat->obatalkes_id = $this->sortPilih($_POST['GFObatalkesM']);                        
                        if ($model->validate()){
                            $transaction = Yii::app()->db->beginTransaction();
                            try{
                                $hasil = 0;
                                if($model->save()){
                                    $jumlah = count($modObat->obatalkes_id);
                                    foreach ($modObat->obatalkes_id as $data){
                                        $modDetail = new FormstokopnameR();
                                        $modDetail->obatalkes_id = $data;
                                        $modDetail->formuliropname_id = $model->formuliropname_id;
                                        $modDetail->volume_stok = StokobatalkesT::getStokBarang($data);
                                        if ($modDetail->save()){
                                            $hasil++;
                                        }
                                    }
                                }

                                if(($hasil>0)&&($hasil == $jumlah)){
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                                    $this->redirect(array('index', 'id'=>$model->formuliropname_id));
                                }
                                else{
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error',"Data Gagal Disimpan ");
                                }
                            }
                            catch(Exception $ex){
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                            }
                        }
		}
                
		if(isset($_GET['GFObatalkesM']))
		{
                        $modObat->unsetAttributes();
			$modObat->attributes=$_GET['GFObatalkesM'];			
		}

		$this->render($this->pathView.'index',array(
			'model'=>$model, 'modObat'=>$modObat,
		));
	}
        
        protected function sortPilih($data){
            $result = array();
            foreach ($data as $i=>$row){
                if ($row['cekList'] == 1){
                    $result[] = $row['obatalkes_id'];
                }
            }
            
            return $result;
        }

	
	/**
	 * Manages all models.
	 */
	public function actionInformasi()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GFFormuliropnameR('search');
                $model->tglAkhir = date('d M Y H:i:s');
                $model->tglformulir = date('d M Y 00:00:00');
//		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GFFormuliropnameR'])){
			$model->attributes=$_GET['GFFormuliropnameR'];
                        $format = new CustomFormat();
                        $model->tglformulir = $format->formatDateTimeMediumForDB($model->tglformulir);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                        $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
                }

		$this->render($this->pathView.'informasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GFFormuliropnameR::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gfformuliropname-r-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint($id)
        {
//            $id = $_REQUEST['id'];
            $model= GFFormuliropnameR::model()->findByPK($id);
            $modDetails = FormstokopnameR::model()->findAllByAttributes(array('formuliropname_id'=>$id));
            $modObat = new GFObatalkesM('searchDataObat');
            $modObat->obatalkes_id = CHtml::listData($modDetails, 'obatalkes_id', 'obatalkes_id');
            $judulLaporan='Data Formulir Obatalkes Opname';
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT'){
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('model'=>$model, 'modObat'=>$modObat, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint, 'data'=>$data));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('model'=>$model,'modObat'=>$modObat,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('model'=>$model,'modObat'=>$modObat,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
