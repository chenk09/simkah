<?php
class TrxPenerimaanBarangController extends SBaseController{
    public function filters()
    {
        return array('accessControl');
    }

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions'=>array('index','search','details', 'print'),
				'users'=>array('@'),
			),
		);
	}

    public function actionIndex()
    {
        $modPermintaanPembelian = new GFPermintaanPembelianT;
        $format = new CustomFormat();
        $modPermintaanPembelian->tglAwal=date('d M Y 00:00:00');
        $modPermintaanPembelian->tglAkhir=date('d M Y H:i:s');
        if(isset($_GET['GFPermintaanPembelianT'])){
            $modPermintaanPembelian->attributes=$_GET['GFPermintaanPembelianT'];
            $modPermintaanPembelian->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['GFPermintaanPembelianT']['tglAwal']);
            $modPermintaanPembelian->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFPermintaanPembelianT']['tglAkhir']);
        }
        $this->render('index',array(
            "modPermintaanPembelian"=>$modPermintaanPembelian
        ));
    }

    public function actionDetails($idPermintaanPembelian)
    {
        $this->layout='//layouts/frameDialog';
        $modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($idPermintaanPembelian);
        $modPermintaanPembelianDetails = GFPermintaanDetailT::model()->findAll('permintaanpembelian_id='.$idPermintaanPembelian.'');

        $this->render('details',array(
            'modPermintaanPembelian'=>$modPermintaanPembelian,
            'modPermintaanPembelianDetails'=>$modPermintaanPembelianDetails
        ));

    }

    public function actionCreate($id = null)
	{
		$format = new CustomFormat();
        $konfigFarmasi = KonfigfarmasiK::model()->find();
		$modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($id);

		$modPenerimaanBarang = new GFPenerimaanBarangT;
        $modPenerimaanBarang->tglterima = date('d M Y H:i:s');
        $modPenerimaanBarang->tglsuratjalan = date('d M YH:i:s');
        $modPenerimaanBarang->noterima = Generator::noTerimaBarang();
		$modPenerimaanBarang->supplier_id = $modPermintaanPembelian->supplier->supplier_id;
		$modPenerimaanBarang->permintaanpembelian_id = $id;

		if(isset($_POST["GFPenerimaanBarangT"]))
		{
			$pesan = array();
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$modPenerimaanBarang->attributes = $_POST["GFPenerimaanBarangT"];
				$modPenerimaanBarang->gudangpenerima_id = Yii::app()->user->getState('ruangan_id');

				$total_harga = 0;
				$total_harga_netto = 0;
				$total_jml_discount = 0;
				$total_pajak_ppn = 0;
				$total_pajak_pph = 0;

				if(!$modPenerimaanBarang->validate())
				{
					foreach($modPenerimaanBarang->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}else{
					$modPenerimaanBarang->save();
					if(isset($_POST["GFPenerimaanDetailT"]))
					{
						foreach($_POST["GFPenerimaanDetailT"] as $key=>$val)
						{
							$modStokObatAlkes = new GFStokObatAlkesT;
							$obatalkesM = ObatalkesM::model()->findByPk($val['obatalkes_id']);
							foreach($obatalkesM as $x=>$z)
							{
								if($modStokObatAlkes->hasAttribute($x))
								{
									$modStokObatAlkes->$x = $z;
								}
							}
							$modStokObatAlkes->ruangan_id = Yii::app()->user->getState('ruangan_id');
							$modStokObatAlkes->tglkadaluarsa = $format->formatDateMediumForDB($val["tglkadaluarsa"]);
							$modStokObatAlkes->discount = $val["persendiscount"];
							$modStokObatAlkes->tglstok_in = $modPenerimaanBarang->tglterima;
							$modStokObatAlkes->tglstok_out = null;
							$modStokObatAlkes->qtystok_in = $val["jmlterima"] * $val["jmlkemasan"];
							$modStokObatAlkes->qtystok_out = 0;
							$modStokObatAlkes->qtystok_current = $modStokObatAlkes->qtystok_in;
							$modStokObatAlkes->harganetto_oa = $val["harganettoper"];
							$modStokObatAlkes->hargajual_oa = $obatalkesM->hargajual;

							$total_harga += (($modStokObatAlkes->qtystok_in * $val["harganettoper"]) + $val["hargappnper"]) - $val["jmldiscount"];
							$total_harga_netto += ($modStokObatAlkes->qtystok_in * $val["harganettoper"]);
							$total_jml_discount += $val["jmldiscount"];
							$total_pajak_ppn += $val["hargappnper"];
							$total_pajak_pph += $val["hargapphper"];

							if(!$modStokObatAlkes->validate())
							{
								foreach($modStokObatAlkes->getErrors() as $a=>$b)
								{
									$pesan[] = $a . " : " . implode(", ", $b);
								}
							}else{
								$modStokObatAlkes->save();
								$GFPenerimaanDetailT = new GFPenerimaanDetailT;
								$GFPenerimaanDetailT->attributes = $_POST["GFPenerimaanDetailT"][$key];
								$GFPenerimaanDetailT->tglkadaluarsa = $format->formatDateMediumForDB($val["tglkadaluarsa"]);
								$GFPenerimaanDetailT->penerimaanbarang_id = $modPenerimaanBarang->penerimaanbarang_id;
								$GFPenerimaanDetailT->stokobatalkes_id = $modStokObatAlkes->stokobatalkes_id;

								if(!$GFPenerimaanDetailT->validate())
								{
									foreach($GFPenerimaanDetailT->getErrors() as $a=>$b)
									{
										$pesan[] = $a . " : " . implode(", ", $b);
									}
								}else{
									$GFPenerimaanDetailT->save();

									$stokObatAlkesT = GFStokObatAlkesT::model()->findByPk($modStokObatAlkes->stokobatalkes_id);
									$stokObatAlkesT->penerimaandetail_id = $GFPenerimaanDetailT->penerimaandetail_id;
									$stokObatAlkesT->save();
								}
							}
						}
					}
				}

				$penerimaanBarangT = GFPenerimaanBarangT::model()->findByPk($modPenerimaanBarang->penerimaanbarang_id);
				$penerimaanBarangT->harganetto = $total_harga_netto;
				$penerimaanBarangT->jmldiscount = $total_jml_discount;
				$penerimaanBarangT->persendiscount = 0;
				$penerimaanBarangT->totalpajakppn = $total_pajak_ppn;
				$penerimaanBarangT->totalpajakpph = $total_pajak_pph;
				$penerimaanBarangT->totalharga = $total_harga;
				if(!$penerimaanBarangT->save())
				{
					$pesan[] = "Update Penerimaan barang gagal";
				}

				$permintaanPembelianT = GFPermintaanPembelianT::model()->updateByPk($modPenerimaanBarang->permintaanpembelian_id, array(
					"penerimaanbarang_id" => $modPenerimaanBarang->penerimaanbarang_id
				));
				if(!$permintaanPembelianT)
				{
					$pesan[] = " penerimaanbarang_id : gagal";
				}

				if(count($pesan) > 0)
				{
					throw new Exception(implode("<br>", $pesan));
				}else{
					Yii::app()->user->setFlash('success', "Data berhasil disimpan");
					$transaction->commit();
					$this->redirect(array(
						'update', 'id'=>$modPenerimaanBarang->penerimaanbarang_id
					));
				}
			}catch(Exception $exc){
				$transaction->rollback();
				Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
			}
		}

        $this->render('penerimaan/create', array(
			'modPenerimaanBarang' => $modPenerimaanBarang,
            'modPermintaanPembelian' => $modPermintaanPembelian
		));
	}

    public function actionUpdate($id = null)
	{
		$modPenerimaanBarang = GFPenerimaanBarangT::model()->findByPk($id);
		$modPermintaanPembelian = GFPermintaanPembelianT::model()->findByPk($modPenerimaanBarang->permintaanpembelian_id);

        $this->render('penerimaan/update', array(
			'modPenerimaanBarang' => $modPenerimaanBarang,
            'modPermintaanPembelian' => $modPermintaanPembelian
		));
	}

    public function actionPrint($idPenerimaan,$caraPrint){
        $judulLaporan = 'Laporan Detail Penerimaan Barang';
        $modPenerimaan = GFPenerimaanBarangT::model()->findByPk($idPenerimaan);
        $modPenerimaanDetails = GFPenerimaanDetailT::model()->findAll('penerimaanbarang_id=:idPenerimaan', array('idPenerimaan'=>$idPenerimaan));
        $modPenerimaan->tglterima = date('d-m-Y H:i:s', strtotime($modPenerimaan->tglterima));
        $modPenerimaan->tglsuratjalan = date('d-m-Y H:i:s', strtotime($modPenerimaan->tglsuratjalan));
        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                        'caraPrint'=>$caraPrint,
                                        'modPenerimaan'=>$modPenerimaan,
                                        'modPenerimaanDetails'=>$modPenerimaanDetails));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('Print',array('modDetailRencana'=>$modDetailRencana,
                                        'judulLaporan'=>$judulLaporan,
                                        'modRencanaKebutuhan'=>$modRencanaKebutuhan));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('Print',array('modDetailRencana'=>$modDetailRencana,
                                                                'judulLaporan'=>$judulLaporan,
                                                                'modRencanaKebutuhan'=>$modRencanaKebutuhan),true));
            $mpdf->Output();
        }
    }        


}
