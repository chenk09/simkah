<?php

class LaporanController extends SBaseController
{
    public function actionIndex()
    {
            $this->render('index');
    }
    
    public function actionLaporanMutasiObatAlkes(){
        $model = new IVMutasioaruangan;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        
        if (isset($_GET['IVMutasioaruangan'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['IVMutasioaruangan'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['IVMutasioaruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['IVMutasioaruangan']['tglAkhir']);
//            if($_GET['IVMutasioaruangan']['ruanganasal_id']){
//            $model->ruanganasal_id = Yii::app()->user->getState('ruangan_id');
        }
        $this->render('mutasiObatAlkes/index',array(
            'model'=>$model,
        ));
    }
    public function actionPrintLaporanMutasiObatAlkes(){
        $model = new IVMutasioaruangan;
        $judulLaporan = 'Laporan Mutasi Obat Alkes';

        //Data Grafik
        $data['title'] = 'Grafik Mutasi Obat Alkes';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['IVMutasioaruangan'])) {
            $model->attributes = $_REQUEST['IVMutasioaruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['IVMutasioaruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['IVMutasioaruangan']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'mutasiObatAlkes/Print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    /**
     * actionLaporanMutasiIntern laporan mutasi obat alkes
     * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
     */
    public function actionLaporanMutasiIntern(){
        $model = new IVMutasioaruangan;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        
        if (isset($_GET['IVMutasioaruangan'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['IVMutasioaruangan'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['IVMutasioaruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['IVMutasioaruangan']['tglAkhir']);
//            if($_GET['IVMutasioaruangan']['ruanganasal_id']){
//            $model->ruanganasal_id = Yii::app()->user->getState('ruangan_id');
        }
        $this->render('mutasiIntern/index',array(
            'model'=>$model,
        ));
    }
    /**
     * actionLaporanMutasiIntern print laporan mutasi obat alkes
     * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
     */
    public function actionPrintLaporanMutasiIntern(){
        $model = new IVMutasioaruangan('searchLaporanMutasiIntern');
        $judulLaporan = 'Laporan Mutasi Obat Alkes';
        //Data Grafik
        $data['title'] = 'Grafik Mutasi Obat Alkes';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['IVMutasioaruangan'])) {
            $model->attributes = $_REQUEST['IVMutasioaruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['IVMutasioaruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['IVMutasioaruangan']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'mutasiIntern/Print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    /**
     * actionPrintLaporanMutasiInternDetail print detail dari tombol di grid
     */
    public function actionLaporanMutasiInternDetail($id, $caraPrint){
        $this->layout = '//layouts/frameDialog';
        $judulLaporan = 'Laporan Mutasi Obat Alkes Detail';
        $format = new CustomFormat();
        $modDetail = array();
        if($caraPrint == "PRINT")
            $this->layout = '//layouts/printWindows';
        if (isset($_REQUEST['IVMutasioaruangan'])) {
            $criteria = new CdbCriteria();
            $tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['IVMutasioaruangan']['tglAwal']);
            $tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['IVMutasioaruangan']['tglAkhir']);
            $criteria->addBetweenCondition('tglmutasioa',$tglAwal,$tglAkhir);
            $criteria->compare('mutasioaruangan_id', $id);
            $model = IVMutasioaruangan::model()->find($criteria);
            $modDetail = IVMutasioadetail::model()->findAllByAttributes(array('mutasioaruangan_id'=>$model->mutasioaruangan_id));
        }
        $ruanganAsal = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'))->ruangan_nama;
        $periode = date('d-m-Y H:i:s',  strtotime($tglAwal))." s/d ".date('d-m-Y H:i:s',  strtotime($tglAkhir));
        $this->render('mutasiIntern/PrintDetail', array('model'=>$model, 'modDetail'=>$modDetail, 'judulLaporan'=>$judulLaporan, 'ruanganAsal'=>$ruanganAsal, 'periode'=>$periode));
    }
    public function actionFrameGrafikLaporanMutasiObatAlkes(){
         $this->layout = '//layouts/frameDialog';

        $model = new IVMutasioaruangan;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Mutasi Obat Alkes';
        $data['type'] = $_GET['type'];

        if (isset($_GET['IVMutasioaruangan'])) {
            $format = new CustomFormat();
            $model->attributes = $_GET['IVMutasioaruangan'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['IVMutasioaruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['IVMutasioaruangan']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data'=>$data,
        ));
    }
    
//    ====================================AWAL LAPORAN PENERIMAAN ITEMS
    public function actionLaporanPenerimaanItems()
    {
        $model = new GFPenerimaanBarangT;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        if (isset($_GET['GFPenerimaanBarangT'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['GFPenerimaanBarangT'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['GFPenerimaanBarangT']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['GFPenerimaanBarangT']['tglAkhir']);
        }
        $this->render('penerimaanItems/index',array(
            'model'=>$model,
        ));
    }
    public function actionPrintLaporanPenerimaanItems() {
        $model = new GFPenerimaanBarangT;
        $judulLaporan = 'Laporan Penerimaan Items';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['GFPenerimaanBarangT'])) {
            $model->attributes = $_REQUEST['GFPenerimaanBarangT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['GFPenerimaanBarangT']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['GFPenerimaanBarangT']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penerimaanItems/Print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
   }
   /**
    * actionLaporanPenerimaanObatAlkes untuk laporan penerimaan obat alkes
    * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
    */
    public function actionLaporanPenerimaanObatAlkes()
    {
        // $model = new GFPenerimaanBarangT('searchLaporanPenerimaanObatAlkes');
        $model = new GFLaporanpenerimaanobatalkesV('');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $criteria = new CDbCriteria;
        if (isset($_GET['GFLaporanpenerimaanobatalkesV'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['GFLaporanpenerimaanobatalkesV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFLaporanpenerimaanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFLaporanpenerimaanobatalkesV']['tglAkhir']);

            $criteria->compare('supplier_id',$model->supplier_id);
            $criteria->compare('kelompokobat',$model->kelompokobat);
            $criteria->compare('nofaktur',$model->nofaktur);
            $criteria->compare('noterima',$model->noterima);
        }

        $criteria->addBetweenCondition('tglterima', $model->tglAwal, $model->tglAkhir);
        $criteria->order = 'supplier_id, kelompokobat, tglterima';
        $modPenerimaan = GFLaporanpenerimaanobatalkesV::model()->findAll($criteria);
        $this->render('penerimaanObatAlkes/index',array(
            'model'=>$model,'modPenerimaan'=>$modPenerimaan,
        ));
    }
    /**
    * actionLaporanPenerimaanObatAlkes untuk laporan penerimaan obat alkes
    * @author Jembar | Jembar hardian <jembarhardian@gmail.com>
    */
    public function actionLaporanPenerimaanObatAlkesPerSupplier()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new LaporanpenerimaanpembelianobatalkesV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $criteria = new CDbCriteria;
        if (isset($_GET['LaporanpenerimaanpembelianobatalkesV'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['LaporanpenerimaanpembelianobatalkesV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);

            $criteria->compare('supplier_id',$model->supplier_id);
            $criteria->compare('kelompokobat',$model->kelompokobat);
            $criteria->compare('nofaktur',$model->nofaktur);
            $criteria->compare('noterima',$model->noterima);
        }

        $criteria->addBetweenCondition('tglterima', $model->tglAwal, $model->tglAkhir);
        $criteria->order = 'supplier_id, kelompokobat, tglterima';
        $modPenerimaan = LaporanpenerimaanpembelianobatalkesV::model()->findAll($criteria);
        $this->render('penerimaanObatAlkes/penerimaanPerSupplier/index',array(
            'model'=>$model,'modPenerimaan'=>$modPenerimaan,
        ));
    }
    /**
     * actionPrintLaporanPenerimaanObatAlkes untuk print laporan penerimaan obat alkes
     * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
     */
    public function actionPrintLaporanPenerimaanObatAlkes() {
        $model = new LaporanpenerimaanpembelianobatalkesV;
        $judulLaporan = 'Laporan Penerimaan Obat Alkes Per Supplier';
        $criteria=new CDbCriteria;
        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['LaporanpenerimaanpembelianobatalkesV'])) {
            $model->attributes = $_REQUEST['LaporanpenerimaanpembelianobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);


            
            $criteria->compare('supplier_id',$model->supplier_id);
            $criteria->compare('kelompokobat',$model->kelompokobat);
            $criteria->compare('nofaktur',$model->nofaktur);
            $criteria->compare('noterima',$model->noterima);
        }
        $criteria->addBetweenCondition('tglterima', $model->tglAwal, $model->tglAkhir);
        $periode = date('d-m-Y H:i:s',  strtotime($model->tglAwal))." s/d ".date('d-m-Y H:i:s',  strtotime($model->tglAkhir));

        $criteria->order = 'supplier_id, kelompokobat, tglterima';

        $modPenerimaan = LaporanpenerimaanpembelianobatalkesV::model()->findAll($criteria);


        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penerimaanObatAlkes/penerimaanPerSupplier/Print';
        $this->printFunction2($model, $modPenerimaan, $data, $caraPrint, $judulLaporan, $target);
        // $this->render($target, array('model'=>$model, 'modPenerimaan'=>$modPenerimaan, 'judulLaporan'=>$judulLaporan, 'data'=>$data, 'periode'=>$periode, 'caraPrint'=>$caraPrint));
   }

    /**
    * actionLaporanPenerimaanObatAlkes untuk laporan penerimaan obat alkes
    * @author Jembar | Jembar hardian <jembarhardian@gmail.com>
    */
    public function actionLaporanPenerimaanObatAlkesPerObat()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new GFLaporanpenerimaanobatalkesV();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $criteria = new CDbCriteria;
        if (isset($_GET['GFLaporanpenerimaanobatalkesV'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['GFLaporanpenerimaanobatalkesV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFLaporanpenerimaanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFLaporanpenerimaanobatalkesV']['tglAkhir']);

            $criteria->compare('supplier_id',$model->supplier_id);
            $criteria->compare('obatalkes_kode',$model->obatalkes_kode);
            $criteria->compare('nofaktur',$model->nofaktur);
            $criteria->compare('noterima',$model->noterima);
        }

        $criteria->addBetweenCondition('tglterima', $model->tglAwal, $model->tglAkhir);
        $criteria->order = 'supplier_id, namaobat, tglterima';
        $modPenerimaan = GFLaporanpenerimaanobatalkesV::model()->findAll($criteria);
        $this->render('penerimaanObatAlkes/penerimaanPerObat/index',array(
            'model'=>$model,'modPenerimaan'=>$modPenerimaan,
        ));
    }

    /**
    * actionPrintLaporanPenerimaanObatAlkesPerObat untuk print laporan penerimaan obat alkes per obat
    * @author Jembar | Jembar hardian <jembarhardian@gmail.com>
    */

    public function actionPrintLaporanPenerimaanObatAlkesPerObat() {
        $model = new LaporanpenerimaanpembelianobatalkesV;
        $judulLaporan = 'Laporan Penerimaan Obat Alkes Per Obat';
        $criteria=new CDbCriteria;
        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['LaporanpenerimaanpembelianobatalkesV'])) {
            $model->attributes = $_REQUEST['LaporanpenerimaanpembelianobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);


            $criteria->compare('supplier_id',$model->supplier_id);
            $criteria->compare('obatalkes_kode',$model->obatalkes_kode);
            $criteria->compare('nofaktur',$model->nofaktur);
            $criteria->compare('noterima',$model->noterima);
        }
        $criteria->addBetweenCondition('tglterima', $model->tglAwal, $model->tglAkhir);
        $periode = date('d-m-Y H:i:s',  strtotime($model->tglAwal))." s/d ".date('d-m-Y H:i:s',  strtotime($model->tglAkhir));

        $criteria->order = 'supplier_id, namaobat, tglterima';

        $modPenerimaan = LaporanpenerimaanpembelianobatalkesV::model()->findAll($criteria);


        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penerimaanObatAlkes/penerimaanPerObat/Print';
        $this->printFunction2($model, $modPenerimaan, $data, $caraPrint, $judulLaporan, $target);
        // $this->render($target, array('model'=>$model, 'modPenerimaan'=>$modPenerimaan, 'judulLaporan'=>$judulLaporan, 'data'=>$data, 'periode'=>$periode, 'caraPrint'=>$caraPrint));
   }

   /**
    * actionLaporanPenerimaanObatAlkesPerKelompok untuk laporan penerimaan obat alkes
    * @author Jembar | Jembar hardian <jembarhardian@gmail.com>
    */
    public function actionLaporanPenerimaanObatAlkesPerKelompok()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new LaporanpenerimaanpembelianobatalkesV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $criteria = new CDbCriteria;
        if (isset($_GET['LaporanpenerimaanpembelianobatalkesV'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['LaporanpenerimaanpembelianobatalkesV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);

            $criteria->compare('supplier_id',$model->supplier_id);
            $criteria->compare('namaobat',$model->namaobat);
            $criteria->compare('kelompokobat',$model->kelompokobat);
            $criteria->compare('nofaktur',$model->nofaktur);
            $criteria->compare('noterima',$model->noterima);

        }

        $criteria->addBetweenCondition('tglterima', $model->tglAwal, $model->tglAkhir);
        $criteria->order = 'kelompokobat, namaobat, tglterima';
        $modPenerimaan = LaporanpenerimaanpembelianobatalkesV::model()->findAll($criteria);
        $this->render('penerimaanObatAlkes/penerimaanPerKelompokObat/index',array(
            'model'=>$model,'modPenerimaan'=>$modPenerimaan,
        ));
    }

    /**
    * actionPrintLaporanPenerimaanObatAlkesPerKelompok untuk print laporan penerimaan obat alkes per obat
    * @author Jembar | Jembar hardian <jembarhardian@gmail.com>
    */

    public function actionPrintLaporanPenerimaanObatAlkesPerKelompok() {
        $model = new LaporanpenerimaanpembelianobatalkesV;
        $judulLaporan = 'Laporan Penerimaan Obat Alkes Per Kelompok Obat';
        $criteria=new CDbCriteria;
        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['LaporanpenerimaanpembelianobatalkesV'])) {
            $model->attributes = $_REQUEST['LaporanpenerimaanpembelianobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);


            
            $criteria->compare('supplier_id',$model->supplier_id);
            $criteria->compare('namaobat',$model->namaobat);
            $criteria->compare('kelompokobat',$model->kelompokobat);
            $criteria->compare('nofaktur',$model->nofaktur);
            $criteria->compare('noterima',$model->noterima);
        }
        $criteria->addBetweenCondition('tglterima', $model->tglAwal, $model->tglAkhir);
        $periode = date('d-m-Y H:i:s',  strtotime($model->tglAwal))." s/d ".date('d-m-Y H:i:s',  strtotime($model->tglAkhir));

        $criteria->order = 'kelompokobat, namaobat, tglterima';

        $modPenerimaan = LaporanpenerimaanpembelianobatalkesV::model()->findAll($criteria);


        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penerimaanObatAlkes/penerimaanPerKelompokObat/Print';
        $this->printFunction2($model, $modPenerimaan, $data, $caraPrint, $judulLaporan, $target);
        // $this->render($target, array('model'=>$model, 'modPenerimaan'=>$modPenerimaan, 'judulLaporan'=>$judulLaporan, 'data'=>$data, 'periode'=>$periode, 'caraPrint'=>$caraPrint));
   }

    /**
    * actionLaporanPenerimaanObatAlkesPerProdusen untuk laporan penerimaan obat alkes
    * @author Jembar | Jembar hardian <jembarhardian@gmail.com>
    */
    public function actionLaporanPenerimaanObatAlkesPerProdusen()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new LaporanpenerimaanpembelianobatalkesV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $criteria = new CDbCriteria;
        if (isset($_GET['LaporanpenerimaanpembelianobatalkesV'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['LaporanpenerimaanpembelianobatalkesV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);

            $criteria->compare('produsen_id',$model->produsen_id);
            $criteria->compare('kelompokobat',$model->kelompokobat);
            $criteria->compare('nofaktur',$model->nofaktur);
            $criteria->compare('noterima',$model->noterima);
        }

        $criteria->addBetweenCondition('tglterima', $model->tglAwal, $model->tglAkhir);
        $criteria->order = 'kelompokobat, namaobat, tglterima';
        $modPenerimaan = LaporanpenerimaanpembelianobatalkesV::model()->findAll($criteria);
        $this->render('penerimaanObatAlkes/penerimaanPerProdusen/index',array(
            'model'=>$model,'modPenerimaan'=>$modPenerimaan,
        ));
    }

    /**
    * actionPrintLaporanPenerimaanObatAlkesPerKelompok untuk print laporan penerimaan obat alkes per obat
    * @author Jembar | Jembar hardian <jembarhardian@gmail.com>
    */

    public function actionPrintLaporanPenerimaanObatAlkesPerProdusen() {
        $model = new LaporanpenerimaanpembelianobatalkesV;
        $judulLaporan = 'Laporan Penerimaan Obat Alkes Per Produsen';
        $criteria=new CDbCriteria;
        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['LaporanpenerimaanpembelianobatalkesV'])) {
            $model->attributes = $_REQUEST['LaporanpenerimaanpembelianobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);


            
            $criteria->compare('produsen_id',$model->produsen_id);
            $criteria->compare('kelompokobat',$model->kelompokobat);
            $criteria->compare('nofaktur',$model->nofaktur);
            $criteria->compare('noterima',$model->noterima);
        }
        $criteria->addBetweenCondition('tglterima', $model->tglAwal, $model->tglAkhir);
        $periode = date('d-m-Y H:i:s',  strtotime($model->tglAwal))." s/d ".date('d-m-Y H:i:s',  strtotime($model->tglAkhir));

        $criteria->order = 'kelompokobat, namaobat, tglterima';

        $modPenerimaan = LaporanpenerimaanpembelianobatalkesV::model()->findAll($criteria);


        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penerimaanObatAlkes/penerimaanPerProdusen/Print';
        $this->printFunction2($model, $modPenerimaan, $data, $caraPrint, $judulLaporan, $target);
        //$this->render($target, array('model'=>$model, 'modPenerimaan'=>$modPenerimaan, 'judulLaporan'=>$judulLaporan, 'data'=>$data, 'periode'=>$periode, 'caraPrint'=>$caraPrint));
   }

    /**
    * actionLaporanDetailFakturPenerimaanObatAlkes untuk laporan penerimaan obat alkes
    * @author Jembar | Jembar hardian <jembarhardian@gmail.com>
    */
    public function actionLaporanDetailFakturPenerimaanObatAlkes()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new RincianfakturgudangfarmasiV;
        $criteria = new CDbCriteria;
        if (isset($_GET['RincianfakturgudangfarmasiV'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['RincianfakturgudangfarmasiV'];
            $model->tglterima = $format->formatDateTimeMediumForDB($_GET['tglterima']);
            $model->tglfaktur = $format->formatDateTimeMediumForDB($_GET['tglfaktur']);
            $model->tgljatuhtempo = $format->formatDateTimeMediumForDB($_GET['tgljatuhtempo']);

            $criteria->compare('noterima',$model->noterima,true);
            $criteria->compare('nofaktur',$model->nofaktur,true);
            $criteria->compare('supplier_nama',$model->supplier_nama,true);
            $criteria->compare('nopermintaan',$model->nopermintaan,true);
            $criteria->addCondition("tglterima='".$model->tglterima."' AND tglfaktur='".$model->tglfaktur."'");
            $criteria->order = 'tglterima';
        $modPenerimaan = RincianfakturgudangfarmasiV::model()->findAll($criteria);
        }
        
        
        $this->render('penerimaanObatAlkes/detailFaktur/index',array(
            'model'=>$model,'modPenerimaan'=>$modPenerimaan,
        ));
    }

    /**
    * actionPrintLaporanDetailFakturPenerimaanObatAlkes untuk print laporan penerimaan obat alkes per obat
    * @author Jembar | Jembar hardian <jembarhardian@gmail.com>
    */

    public function actionPrintLaporanDetailFakturPenerimaanObatAlkes() {
        $model = new RincianfakturgudangfarmasiV;
        $judulLaporan = 'Laporan Detail Faktur Penerimaan Obat Alkes';
        $criteria=new CDbCriteria;
        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['RincianfakturgudangfarmasiV'])) {
            $model->attributes = $_REQUEST['RincianfakturgudangfarmasiV'];
            $format = new CustomFormat();
            $model->tglterima = $format->formatDateTimeMediumForDB($_GET['tglterima']);
            $model->tglfaktur = $format->formatDateTimeMediumForDB($_GET['tglfaktur']);
            $model->tgljatuhtempo = $format->formatDateTimeMediumForDB($_GET['tgljatuhtempo']);

            $criteria->compare('noterima',$model->noterima,true);
            $criteria->compare('nofaktur',$model->nofaktur,true);
            $criteria->compare('supplier_nama',$model->supplier_nama,true);
            $criteria->compare('nopermintaan',$model->nopermintaan,true);
            $criteria->addCondition("tglterima='".$model->tglterima."' AND tglfaktur='".$model->tglfaktur."'");

        }
        $noterima = $model->noterima;
        $nofaktur = $model->nofaktur;
        $nopermintaan = $model->nopermintaan;
        $tglterima = $model->tglterima;
        $tglfaktur = $model->tglfaktur;
        $tgljatuhtempo = $model->tgljatuhtempo;
        $supplier_nama = $model->supplier_nama;



        $criteria->order = 'tglterima';
        $modPenerimaan = RincianfakturgudangfarmasiV::model()->findAll($criteria);


        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penerimaanObatAlkes/detailFaktur/Print';
        //$this->printFunction($model, $modPenerimaan, $data, $caraPrint, $judulLaporan, $target);
        $this->printFunction3($model, $modPenerimaan, $data, $caraPrint, $judulLaporan, $target);
   }

    protected function printFunction2($model, $modPenerimaan, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $tglAwal = $format->formatDateMediumForDB($model->tglAwal);
        $tglAkhir = $format->formatDateMediumForDB($model->tglAkhir);
        $periode = $this->parserTanggal($tglawal).' s/d '.$this->parserTanggal($tglakhir);

        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'modPenerimaan' =>$modPenerimaan,  'data' => $data, 'periode'=>$periode, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'modPenerimaan' =>$modPenerimaan,  'data' => $data, 'periode'=>$periode, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'modPenerimaan' =>$modPenerimaan, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    protected function printFunction3($model, $modPenerimaan, $data, $caraPrint, $judulLaporan, $target){

        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'modPenerimaan' =>$modPenerimaan,  'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'modPenerimaan' =>$modPenerimaan,  'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'modPenerimaan' =>$modPenerimaan, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }


    /**
     * actionPrintLaporanPenerimaanObatAlkesByObat untuk print laporan penerimaan obat alkes group berdasarkan obat
     * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
     */
    public function actionPrintLaporanPenerimaanObatAlkesByObat() {
        $this->layout = '//layouts/frameDialog';
        $caraPrint = $_REQUEST['caraPrint'];
        $judulLaporan = 'Laporan Penerimaan Obat Alkes';
        $format = new CustomFormat();
        $modDetail = array();
        if($caraPrint == "PRINT")
            $this->layout = '//layouts/printWindows';
        
        if (isset($_REQUEST['LaporanpenerimaanpembelianobatalkesV'])) {
            $criteria = new CdbCriteria();
            $supplierId = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['supplier_id'];
            $noTerima = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['noterima'];
            $nofaktur = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['nofaktur'];
            $kelompokobat = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['kelompokobat'];
            $tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);
            $criteria->addBetweenCondition('tglterima',$tglAwal,$tglAkhir);
            $criteria->compare('LOWER(nofaktur)', strtolower($nofaktur),true);
            $criteria->compare('LOWER(kelompokobat)', strtolower($kelompokobat),true);
            $criteria2 = new CdbCriteria;
            $criteria2->join = 'JOIN penerimaanbarang_t pb ON pb.penerimaanbarang_id = t.penerimaanbarang_id
                            JOIN obatalkes_m oa ON oa.obatalkes_id = t.obatalkes_id
            ';
            $criteria2->group = 't.obatalkes_id, oa.obatalkes_nama, t.satuanbesar_id ,pb.supplier_id';
            $criteria2->select = 't.obatalkes_id, oa.obatalkes_nama, t.satuanbesar_id ,
                            SUM(t.jmlterima) AS jmlterima,
                            SUM(t.hargabelibesar*t.jmlterima) AS hargabelibruto,
                            SUM(t.hargabelibesar*t.persendiscount*t.jmlterima/100) AS mergediskon,
                            SUM(((t.hargabelibesar - (t.hargabelibesar*t.persendiscount*t.jmlterima/100))*(CASE WHEN t.hargappnper > 0 THEN 10 ELSE 0 END)/100) *t.jmlterima) AS mergeppn,
                            SUM(t.hargabelibesar - (t.hargabelibesar*t.persendiscount/100) + ((t.hargabelibesar - (t.hargabelibesar*t.persendiscount/100))*(CASE WHEN t.hargappnper > 0 THEN 10 ELSE 0 END)/100) * t.jmlterima) AS subtotalobat,
                            pb.supplier_id';
            $criteria2->order = 'oa.obatalkes_nama';
            $criteria2->addBetweenCondition('pb.tglterima',$tglAwal,$tglAkhir);
            if(empty($supplierId) && empty($noTerima)){ //jika supplier atau noterima tidak ditentukan
                $model = new LaporanpenerimaanpembelianobatalkesV;
            }else{
                $criteria->compare('supplier_id', $supplierId);
                $criteria->compare('LOWER(noterima)', strtolower($noTerima),true);
                $model = LaporanpenerimaanpembelianobatalkesV::model()->find($criteria);
                $criteria2->compare('pb.supplier_id', $supplierId);
                $criteria2->compare('LOWER(pb.noterima)', strtolower($noTerima),true);
            }
            $modDetail = GFPenerimaanDetailT::model()->findAll($criteria2);
        }
        $ruanganAsal = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'))->ruangan_nama;
        $periode = date('d-m-Y H:i:s',  strtotime($tglAwal))." s/d ".date('d-m-Y H:i:s',  strtotime($tglAkhir));

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_REQUEST['type'];
        $target = 'penerimaanObatAlkes/PrintByObat';
        $this->render($target, array('model'=>$model, 'modDetail'=>$modDetail, 'judulLaporan'=>$judulLaporan, 'ruanganAsal'=>$ruanganAsal, 'periode'=>$periode, 'noTerima'=>$noTerima,'caraPrint'=>$caraPrint));
   }
    /**
     * actionPrintLaporanPenerimaanObatAlkesByPenerimaan untuk print laporan penerimaan obat alkes group berdasarkan Penerimaan
     * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
     */
    public function actionPrintLaporanPenerimaanObatAlkesByPenerimaan() {
        $this->layout = '//layouts/frameDialog';
        $caraPrint = $_REQUEST['caraPrint'];
        $judulLaporan = 'Laporan Penerimaan Obat Alkes';
        $format = new CustomFormat();
        $models = array();
        if($caraPrint == "PRINT")
            $this->layout = '//layouts/printWindows';
        
        if (isset($_REQUEST['LaporanpenerimaanpembelianobatalkesV'])) {
            $supplierId = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['supplier_id'];
            $noTerima = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['noterima'];
            $nofaktur = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['nofaktur'];
            $kelompokobat = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['kelompokobat'];
            $tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);
            $criteria = new CdbCriteria();
            $criteria->addBetweenCondition('tglterima',$tglAwal,$tglAkhir);
            $criteria->compare('supplier_id', $supplierId);
            $criteria->compare('LOWER(noterima)', strtolower($noTerima),true);
            $criteria->compare('LOWER(nofaktur)', strtolower($nofaktur),true);
            $criteria->compare('LOWER(kelompokobat)', strtolower($kelompokobat),true);
            $criteria->order = 'supplier_id, tglterima ASC';
            $models = LaporanpenerimaanpembelianobatalkesV::model()->findAll($criteria);
        }
        $ruanganAsal = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'))->ruangan_nama;
        $periode = date('d-m-Y H:i:s',  strtotime($tglAwal))." s/d ".date('d-m-Y H:i:s',  strtotime($tglAkhir));

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_REQUEST['type'];
        $target = 'penerimaanObatAlkes/PrintByPenerimaan';
        $this->render($target, array('models'=>$models, 'judulLaporan'=>$judulLaporan, 'ruanganAsal'=>$ruanganAsal, 'periode'=>$periode, 'noTerima'=>$noTerima,'caraPrint'=>$caraPrint));
   }
   /**
    * actionPrintLaporanPenerimaanObatAlkesDetail untuk print laporan penerimaan obat alkes detail
    * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
    */
    public function actionPrintLaporanPenerimaanObatAlkesDetail($caraPrint = "") {
        $this->layout = '//layouts/frameDialog';
        $judulLaporan = 'Laporan Penerimaan Obat Alkes Detail';
        $format = new CustomFormat();
        $modDetail = array();
        if($caraPrint == "PRINT")
            $this->layout = '//layouts/printWindows';
        
        if (isset($_REQUEST['LaporanpenerimaanpembelianobatalkesV'])) {
            $criteria = new CdbCriteria();
            $supplierId = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['supplier_id'];
            $noTerima = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['noterima'];
            $nofaktur = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['nofaktur'];
            $kelompokobat = $_REQUEST['LaporanpenerimaanpembelianobatalkesV']['kelompokobat'];
            $tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAwal']);
            $tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpenerimaanpembelianobatalkesV']['tglAkhir']);
            $criteria->addBetweenCondition('tglterima',$tglAwal,$tglAkhir);
            $criteria->compare('LOWER(nofaktur)', strtolower($nofaktur),true);
            $criteria->compare('LOWER(kelompokobat)', strtolower($kelompokobat),true);
            $criteria2 = new CdbCriteria;
            $criteria2->with = 'penerimaanbarang';
            $criteria2->with = 'penerimaanbarang.fakturpembelian';
            $criteria2->addBetweenCondition('penerimaanbarang.tglterima',$tglAwal,$tglAkhir);
            $criteria2->compare('LOWER(fakturpembelian.nofaktur)', strtolower($nofaktur),true);
            $criteria2->order = "obatalkes_id ASC";
            if(empty($supplierId) && empty($noTerima)){ //jika supplier atau noterima tidak ditentukan
                $model = LaporanpenerimaanpembelianobatalkesV::model()->find($criteria);
            }else{
                $criteria->compare('supplier_id', $supplierId);
                $criteria->compare('LOWER(noterima)', strtolower($noTerima),true);
                $criteria2->compare('penerimaanbarang.supplier_id', $supplierId);
                $criteria2->compare('LOWER(penerimaanbarang.noterima)', strtolower($noTerima),true);
                $model = LaporanpenerimaanpembelianobatalkesV::model()->find($criteria);
            }
            $modDetail = GFPenerimaanDetailT::model()->findAll($criteria2);
        }
        $ruanganAsal = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'))->ruangan_nama;
        $periode = date('d-m-Y H:i:s',  strtotime($tglAwal))." s/d ".date('d-m-Y H:i:s',  strtotime($tglAkhir));

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_REQUEST['type'];
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penerimaanObatAlkes/PrintDetail';
        $this->render($target, array('model'=>$model, 'modDetail'=>$modDetail, 'judulLaporan'=>$judulLaporan, 'ruanganAsal'=>$ruanganAsal, 'periode'=>$periode, 'caraPrint'=>$caraPrint));
   }
    public function actionFrameGrafikLaporanPenerimaanItems() {
        $this->layout = '//layouts/frameDialog';

        $model = new GFPenerimaanBarangT;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Items';
        $data['type'] = $_GET['type'];

        if (isset($_GET['GFPenerimaanBarangT'])) {
            $format = new CustomFormat();
            $model->attributes = $_GET['GFPenerimaanBarangT'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['GFPenerimaanBarangT']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['GFPenerimaanBarangT']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data'=>$data,
        ));
    }
//    ====================AKHIR LAPORAN PENERIMAAN ITEMS
    
    public function actionLaporanPembelian()
    {
        $model = new GFFakturPembelianT('laporanPembelian');
        $model_det = new GFFakturDetailT();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        
        if(isset($_GET['GFFakturPembelianT']))
		{
            $format = new CustomFormat;
            $model->attributes = $_GET['GFFakturPembelianT'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAkhir']);
			
			$model_det->attributes = $_GET['GFFakturPembelianT'];
            $model_det->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAwal']);
            $model_det->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAkhir']);
        }
        $this->render('fakturPembelianT/index',array(
            'model_det'=>$model_det,
            'model'=>$model,
            'tglAwal'=>$model->tglAwal,
            'tglAkhir'=>$model->tglAkhir,
        ));
    }
	
	public function actionPrintLapPembelian()
    {
        $model = new GFFakturPembelianT('laporanPembelian');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        
        if(isset($_GET['GFFakturPembelianT']))
		{
            $format = new CustomFormat;
            $model->attributes = $_GET['GFFakturPembelianT'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAkhir']);
        }
        $this->render('fakturPembelianT/index',array(
            'model'=>$model,
            'tglAwal'=>$model->tglAwal,
            'tglAkhir'=>$model->tglAkhir,
        ));
    }

    public function actionPrintLaporanPembelian()
	{
        $model = new GFFakturpembelianT();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
            
        $judulLaporan = "Laporan Faktur Pembelian";
		
        if($_GET['filter_tab'] == "rekap")
		{
            $judulLaporan = 'Total Faktur Pembelian';
            $data['title'] = 'Grafik Total Faktur Pembelian';
        }else if($_REQUEST['filter_tab'] == "detail"){
             $judulLaporan = 'Detail Faktur';
             $data['title'] = 'Grafik Detail Faktur';
        }
       
        //Data Grafik
        $data['type'] = $_REQUEST['type'];
        if(isset($_REQUEST['GFFakturpembelianT']))
		{
            $model->attributes = $_REQUEST['GFFakturpembelianT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GFFakturpembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFFakturpembelianT']['tglAkhir']);
        }
		
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'fakturPembelianT/Print';
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionPrintDetailFakturPembelian($idFaktur = null) {
        $idFaktur = $idFaktur;
        $model = new GFFakturpembelianT();
        $modFaktur = GFFakturpembelianT::model()->findByPk($idFaktur);
        $modFakturDetail = GFFakturDetailT::model()->findAllByAttributes(array('fakturpembelian_id'=>$idFaktur));
        $modTerima = GFPenerimaanBarangT::model()->findAllByAttributes(array('fakturpembelian_id'=>$idFaktur));
        $modDetail = GFPenerimaanDetailT::model()->findAllByAttributes(array('penerimaanbarang_id'=>$modTerima->penerimaanbarang_id));

        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        $judulLaporan = 'Detail Faktur';

        //Data Grafik
        $data['title'] = 'Grafik Detail Faktur';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['GFFakturpembelianT'])) {
            $model->attributes = $_REQUEST['GFFakturpembelianT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GFFakturpembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFFakturpembelianT']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'fakturPembelianT/detailPrint';

        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

        $this->layout = '//layouts/printWindows';
        $this->render($target, array('model' => $model, 'idFaktur'=>$idFaktur,'tglAwal'=>$model->tglAwal, 'modFaktur'=>$modFaktur,'tglAkhir'=>$model->tglAkhir,'modDetail'=>$modDetail,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
    }

    public function actionFrameGrafikLaporanPembelian() {
        $this->layout = '//layouts/frameDialog';

        $model = new GFFakturpembelianT('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Faktur Pembelian';
        $data['type'] = $_GET['type'];

        if (isset($_GET['GFFakturpembelianT'])) {
            $format = new CustomFormat();
            $model->attributes = $_GET['GFFakturpembelianT'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['GFFakturpembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['GFFakturpembelianT']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data'=>$data,
        ));
    }

    public function actionLaporanStock() {
		//if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model = new GFInfostokobatalkesruanganV;
		$model->tgl_awal = date('d M Y 00:00:00');
		$model->tgl_akhir = date('d M Y 23:59:59');
		$model->qtystok_in = '0';
		$model->qtystok_out = '0';

		if (isset($_GET['GFInfostokobatalkesruanganV']))
		{
			$format = new CustomFormat;
			$model->attributes = $_GET['GFInfostokobatalkesruanganV'];
			//$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFInfostokobatalkesruanganV']['tgl_awal']);
			//$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFInfostokobatalkesruanganV']['tgl_awal']);
			$model->qtystok_in = $_GET['GFInfostokobatalkesruanganV']['qtystok_in'];
			$model->qtystok_out = $_GET['GFInfostokobatalkesruanganV']['qtystok_out'];
		}

		$this->render('stock/stock',array(
			'model'=>$model,
		));
    }

    public function actionPrintStock()
    {
         //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model = new GFInfostokobatalkesruanganV;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $judulLaporan = 'Stock Barang';

        //Data Grafik
        $data['title'] = 'Grafik Stock';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['GFInfostokobatalkesruanganV'])) {
                $format = new CustomFormat;
                $model->attributes = $_GET['GFInfostokobatalkesruanganV'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFInfostokobatalkesruanganV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFInfostokobatalkesruanganV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'stock/printStock';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }   

    public function actionFrameStock() {
         //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $this->layout = '//layouts/frameDialog';

        $model = new GFInfostokobatalkesruanganV;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Stock Barang';
        $data['type'] = $_GET['type'];

        if (isset($_REQUEST['GFInfostokobatalkesruanganV'])) {
                $format = new CustomFormat;
                $model->attributes = $_GET['GFInfostokobatalkesruanganV'];
                $model->tglAwal = $format->formatDateMediumForDB($_GET['GFInfostokobatalkesruanganV']['tglAwal']);
                $model->tglAkhir = $format->formatDateMediumForDB($_GET['GFInfostokobatalkesruanganV']['tglAkhir']);
        }
        $searchdata = $model->searchGrafik();
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
            'searchdata'=>$searchdata,
        ));
    }

    // Add in February, 22 2013 by Miranitha Fasha //
    public function actionLaporanRencanaKebutuhan() {
         //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $model = new GFRencanaKebFarmasiT;
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('d M Y 23:59:59');
            if (isset($_GET['GFRencanaKebFarmasiT'])) {
                $format = new CustomFormat;
                $model->attributes = $_GET['GFRencanaKebFarmasiT'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFRencanaKebFarmasiT']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFRencanaKebFarmasiT']['tglAkhir']);
                $model->noperencnaan = $_GET['GFRencanaKebFarmasiT']['noperencnaan'];
            }
            $this->render('rencanaKebutuhan/rencanaKebutuhan',array(
                'model'=>$model,
            ));
    }

    public function actionPrintRencanaKebutuhan()
    {
         //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model = new GFRencanaKebFarmasiT;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $judulLaporan = 'Laporan Rencana Kebutuhan';

        //Data Grafik
        $data['title'] = 'Grafik Rencana Kebutuhan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['GFRencanaKebFarmasiT'])) {
                $format = new CustomFormat;
                $model->attributes = $_GET['GFRencanaKebFarmasiT'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFRencanaKebFarmasiT']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFRencanaKebFarmasiT']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'rencanaKebutuhan/printRencana';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }   

    public function actionFrameRencanaKebutuhan() {
         //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $this->layout = '//layouts/frameDialog';

        $model = new GFRencanaKebFarmasiT;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Rencana Kebutuhan';
        $data['type'] = $_GET['type'];

        if (isset($_REQUEST['GFRencanaKebFarmasiT'])) {
                $format = new CustomFormat;
                $model->attributes = $_GET['GFRencanaKebFarmasiT'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFRencanaKebFarmasiT']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFRencanaKebFarmasiT']['tglAkhir']);
        }
        $searchdata = $model->searchGrafik();
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
            'searchdata'=>$searchdata,
        ));
    }

    // End Added //

     /* laporan stock opname */
     public function actionLaporanStockOpname() {
        $model = new GFLaporanfarmasikopnameV;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        if (isset($_GET['GFLaporanfarmasikopnameV'])) {
            $format = new CustomFormat();
            $model->attributes = $_GET['GFLaporanfarmasikopnameV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFLaporanfarmasikopnameV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFLaporanfarmasikopnameV']['tglAkhir']);
            $model->jenisobatalkes_id = $_GET['GFLaporanfarmasikopnameV']['jenisobatalkes_id'];
        }

        $this->render('stockOpname/admin', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanStockOpname() {
        $model = new GFLaporanfarmasikopnameV('search');
        $judulLaporan = 'Laporan Stock Opname';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Stock Opname';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['GFLaporanfarmasikopnameV'])) {
            $model->attributes = $_REQUEST['GFLaporanfarmasikopnameV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GFLaporanfarmasikopnameV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFLaporanfarmasikopnameV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'stockOpname/_printStockOpname';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikStockOpname() {
        $this->layout = '//layouts/frameDialog';
        $model = new GFLaporanfarmasikopnameV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Stock Opname';
        $data['type'] = $_GET['type'];
        if (isset($_GET['GFLaporanfarmasikopnameV'])) {
            $model->attributes = $_GET['GFLaporanfarmasikopnameV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFLaporanfarmasikopnameV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFLaporanfarmasikopnameV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    /* end laporan stock opname */
    
    /*
     * Laporan Permintaan Pembelian , 20 September 2013, owner : Miranitha Fasha
     */
        public function actionLaporanPermintaanPembelian()
        {
            $model = new GFPermintaanPembelianT;
            $model->tglAwal = date('d M Y H:i:s');
            $model->tglAkhir = date('d M Y H:i:s');
            if (isset($_GET['GFPermintaanPembelianT'])) {
                $format = new CustomFormat;
                $model->attributes = $_GET['GFPermintaanPembelianT'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFPermintaanPembelianT']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFPermintaanPembelianT']['tglAkhir']);
            }
            $this->render('permintaanPembelian/index',array(
                'model'=>$model,
            ));
        }

        public function actionPrintLaporanPermintaanPembelian() {
            $model = new GFPermintaanPembelianT('search');
            $judulLaporan = 'Laporan Permintaan Pembelian';
            $model->tglAwal = date('d M Y H:i:s');
            $model->tglAkhir = date('d M Y H:i:s');

            //Data Grafik
            $data['title'] = 'Grafik Laporan Permintaan Pembelian';
            $data['type'] = $_REQUEST['type'];
            if (isset($_REQUEST['GFPermintaanPembelianT'])) {
                $model->attributes = $_REQUEST['GFPermintaanPembelianT'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GFPermintaanPembelianT']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFPermintaanPembelianT']['tglAkhir']);
            }

            $caraPrint = $_REQUEST['caraPrint'];
            $target = 'permintaanPembelian/Print';

            $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }
        public function actionPrintDetailLaporanPermintaanPembelian($id = null, $idPembelian = null) {
            $model = new GFPermintaanPembelianT();
            $modDetail = GFPermintaanDetailT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$idPembelian));
            $judulLaporan = 'Laporan Permintaan Pembelian';

            //Data Grafik
            $data['title'] = 'Grafik Laporan Permintaan Pembelian';
            $data['type'] = $_REQUEST['type'];
            if (isset($_REQUEST['GFPermintaanPembelianT'])) {
                $model->attributes = $_REQUEST['GFPermintaanPembelianT'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['GFPermintaanPembelianT']['tglAwal']);
                $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['GFPermintaanPembelianT']['tglAkhir']);
            }

            $caraPrint = $_REQUEST['caraPrint'];
            $target = 'permintaanPembelian/detailPrint';

            $format = new CustomFormat();
            $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'modDetail'=>$modDetail,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        }

        public function actionFrameGrafikLaporanPermintaanPembelian() {
            $this->layout = '//layouts/frameDialog';

            $model = new GFPermintaanPembelianT;
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('d M Y 23:59:59');

            //Data Grafik
            $data['title'] = 'Grafik Laporan Permintaan Pembelian';
            $data['type'] = $_GET['type'];

            if (isset($_GET['GFPermintaanPembelianT'])) {
                $format = new CustomFormat();
                $model->attributes = $_GET['GFPermintaanPembelianT'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFPermintaanPembelianT']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFPermintaanPembelianT']['tglAkhir']);
            }

            $this->render('_grafik', array(
                'model' => $model,
                'data'=>$data,
            ));
        }
    /*
     * end Laporan Permintaan Pembelian
     */
        
    /*
    * Laporan Penerimaan Items Berdasarkan Jenis, 20 Sep 2013 owner : Miranitha Fasha
    */
        public function actionLaporanPenerimaanJenisItems()
        {
            $model = new GFPenerimaanBarangT('searchPenerimaanItems');
            $model->tglAwal = date('d M Y H:i:s');
            $model->tglAkhir = date('d M Y H:i:s');
            if (isset($_GET['GFPenerimaanBarangT'])) {
                $format = new CustomFormat;
                $model->attributes = $_GET['GFPenerimaanBarangT'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFPenerimaanBarangT']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFPenerimaanBarangT']['tglAkhir']);
            }
            $this->render('penerimaanJenisItems/index',array(
                'model'=>$model,
                'tglAwal'=>$model->tglAwal,
                'tglAkhir'=>$model->tglAkhir,
            ));
        }
        public function actionPrintLaporanPenerimaanJenisItems() {
            $model = new GFPenerimaanBarangT;
            $model->tglAwal = date('d M Y H:i:s');
            $model->tglAkhir = date('d M Y H:i:s');
            
            $sumberdana = SumberdanaM::model()->findByPk($_GET['GFPenerimaanBarangT']['sumberdana_id']);
            $jenisobat = JenisobatalkesM::model()->findByPk($_GET['GFPenerimaanBarangT']['jenisobatalkes_id']);
            if(count($sumberdana) > 0){
                $kondisi = $sumberdana->sumberdana_nama;
            }else if(count($jenisobat) > 0){
                $kondisi = $jenisobat->jenisobatalkes_nama;
            }else{
                $kondisi = '-';
            }
            $judulLaporan = 'Laporan Penerimaan Items Berdasarkan Kelompok : Obat '.$kondisi.'';

            //Data Grafik
            $data['title'] = 'Grafik Laporan Penerimaan Items Berdasarkan Jenis ';
            $data['type'] = $_REQUEST['type'];
            if (isset($_REQUEST['GFPenerimaanBarangT'])) {
                $model->attributes = $_REQUEST['GFPenerimaanBarangT'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['GFPenerimaanBarangT']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['GFPenerimaanBarangT']['tglAkhir']);
//                var_dump($model->tglAwal);
            }

            $caraPrint = $_REQUEST['caraPrint'];
            $target = 'penerimaanJenisItems/Print';

            $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
       }
        public function actionPrintDetLapTerimaJenis($idTerimaBarang = null) {
            $model = new GFPenerimaanBarangT();
            $modTerima = GFPenerimaanBarangT::model()->findByPk($idTerimaBarang);
            $modDetail = GFPenerimaanDetailT::model()->findAllByAttributes(array('penerimaanbarang_id'=>$modTerima->penerimaanbarang_id));
            
            $sumberdana = SumberdanaM::model()->findByPk($_GET['GFPenerimaanBarangT']['sumberdana_id']);
            $jenisobat = JenisobatalkesM::model()->findByPk($_GET['GFPenerimaanBarangT']['jenisobatalkes_id']);
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d 23:59:59');
            
            if(count($sumberdana) > 0){
                $kondisi = $sumberdana->sumberdana_nama;
            }else if(count($jenisobat) > 0){
                $kondisi = $jenisobat->jenisobatalkes_nama;
            }else{
                $kondisi = '-';
            }
            $judulLaporan = 'Laporan Penerimaan Items Berdasarkan Kelompok : Obat '.$kondisi.'';

            //Data Grafik
            $data['title'] = 'Grafik Penerimaan Items Berdasarkan Kelompok : Obat '.$kondisi.'';
            $data['type'] = $_REQUEST['type'];
            if (isset($_REQUEST['GFPenerimaanBarangT'])) {
                $model->attributes = $_REQUEST['GFPenerimaanBarangT'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['GFPenerimaanBarangT']['tglAwal']);
                $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['GFPenerimaanBarangT']['tglAkhir']);
            }

            $caraPrint = $_REQUEST['caraPrint'];
            $target = 'penerimaanJenisItems/detailPrint';

            $format = new CustomFormat();
            $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'tglAwal'=>$model->tglAwal, 'tglAkhir'=>$model->tglAkhir,'modDetail'=>$modDetail,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        }
        public function actionFrameGrafikLaporanPenerimaanJenisItems() {
            $this->layout = '//layouts/frameDialog';

            $model = new GFPenerimaanBarangT;
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('d M Y 23:59:59');

            //Data Grafik
            $data['title'] = 'Grafik Laporan Penerimaan Items Berdasarkan Jenis ';
            $data['type'] = $_GET['type'];

            if (isset($_GET['GFPenerimaanBarangT'])) {
                $format = new CustomFormat();
                $model->attributes = $_GET['GFPenerimaanBarangT'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFPenerimaanBarangT']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFPenerimaanBarangT']['tglAkhir']);
            }

            $this->render('_grafik', array(
                'model' => $model,
                'data'=>$data,
            ));
        }
    /*
     * end Laporan Penerimaan Items Berdasarkan Jenis
     */
/* ============================= Keperluan function laporan ======================================== */
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $model->tglAwal = date('d M Y h:i:s',strtotime($model->tglAwal));
        $model->tglAkhir = date('d M Y h:i:s',strtotime($model->tglAkhir));
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
//        var_dump($model->tglAwal);
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    protected function parserTanggal($tgl){
        $tgl = date('Y-m-d h:i:s',strtotime($tgl));
        $tgl = explode(' ', $tgl);
        $result = array();
        foreach ($tgl as $row){
            if (!empty($row)){
                $result[] = $row;
            }
        }
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];

    }

	public function actionLaporanStockMinimal()
	{
		$model = new GFInfostokobatalkesruanganV;
		$model->ruangan_id = Yii::app()->user->ruangan_id;
		
		if(isset($_GET['GFInfostokobatalkesruanganV']))
		{
			$model->attributes = $_GET['GFInfostokobatalkesruanganV'];
		}
		
		$this->render('stock_min/stock',array(
			'model'=>$model,
		));		
	}
	
	public function actionCetakLaporanStockMinimal()
	{
		$model = new GFInfostokobatalkesruanganV;
		$model->ruangan_id = Yii::app()->user->ruangan_id;
		
		if(isset($_GET["GFInfostokobatalkesruanganV"]))
		{
			$model->attributes = $_GET['GFInfostokobatalkesruanganV'];
		}

		if($_GET["caraPrint"] == 'pdf')
		{
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage("L", '', '', '', '', 5, 5, 5, 5, 5, 5);
			
			$mpdf->WriteHTML($this->renderPartial('stock_min/_table', array(
				'model' => $model,
				'caraPrint'=>$_GET["caraPrint"]
			), true));
            $mpdf->Output();			
		}else{
			if($_GET["caraPrint"] == 'print'){
				$this->layout = "//layouts/printWindows";
			}else $this->layout = '//layouts/printExcel';
			
			$this->render('stock_min/_table', array(
				'model' => $model,
				'caraPrint'=>$_GET["caraPrint"]
			));		
		}		
	}
	
	public function actionLaporanObatKadaluarsa()
	{
		$model = new GFInfostokobatalkesruanganV;
		$model->ruangan_id = Yii::app()->user->ruangan_id;
		
		if(isset($_GET['GFInfostokobatalkesruanganV']))
		{
			$format = new CustomFormat;
			$model->attributes = $_GET['GFInfostokobatalkesruanganV'];
		}
		
		$this->render('obat_kadaluarsa/index',array(
			'model'=>$model,
		));		
	}
	
}
