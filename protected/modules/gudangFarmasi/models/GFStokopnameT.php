<?php

class GFStokopnameT extends StokopnameT{
    public $disableJenisStokOpname = false; //default dropdown jenis stok opname
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		// echo $this->tglstokopname,
		// exit;

		$criteria->compare('stokopname_id',$this->stokopname_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('formuliropname_id',$this->formuliropname_id);
		$criteria->addBetweenCondition('date(tglstokopname)',$this->tglstokopname, $this->tglAkhir);
		$criteria->compare('LOWER(nostokopname)',strtolower($this->nostokopname),true);
		$criteria->compare('isstokawal',$this->isstokawal);
		$criteria->compare('LOWER(jenisstokopname)',strtolower($this->jenisstokopname),true);
		$criteria->compare('LOWER(keterangan_opname)',strtolower($this->keterangan_opname),true);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('totalnetto',$this->totalnetto);
		$criteria->compare('mengetahui_id',$this->mengetahui_id);
		$criteria->compare('petugas1_id',$this->petugas1_id);
		$criteria->compare('petugas2_id',$this->petugas2_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('create_ruangan',Yii::app()->user->getState('ruangan_id'));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

?>
