<?php

class GFFakturDetailT extends FakturdetailT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FakturdetailT the static model class
	 */
	
	public $total_faktur,$supplier_id,$tglAwal,$tglAkhir,$nofaktur;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * getHargaBeliBesar menampilkan harga belibesar dari penerimaandetail_t
	*/
	public function jumlahTotalFaktur($data){
		$sub_total = ((($data->harganettofaktur * $data->jmlkemasan) * $data->jmlterima) + $data->hargappnfaktur + $data->hargapphfaktur) - $data->jmldiscount;
		return $sub_total;
	}
	
	public function sumTotalFaktur($data)
	{
		return count($data);
	}
	
	public function getHargaBeliBesar(){
		$hbb = PenerimaandetailT::model()->findByAttributes(array('penerimaandetail_id'=>$this->penerimaandetail_id))->hargabelibesar;
		return $hbb;
	}
	
	public function laporanPembelianDetail($print = false){
		$format = new CustomFormat;

		$criteria = new CDbCriteria;
		$criteria->select = "t.*, ((((t.harganettofaktur * t.jmlkemasan) * t.jmlterima) + t.hargappnfaktur + t.hargapphfaktur) - t.jmldiscount) AS total_faktur";
		$criteria->with = array('fakturpembelian');
		$this->tglAwal  = $format->formatDateMediumForDB($this->tglAwal);
		$this->tglAkhir = $format->formatDateMediumForDB($this->tglAkhir);
		$criteria->addBetweenCondition('fakturpembelian.tglfaktur',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('fakturpembelian.supplier_id', $this->supplier_id);
		
		if($print)
		{
			return new CActiveDataProvider($this, array(
					'criteria'=>$criteria,
					'pagination'=>false,
			));
		}else{
			return new CActiveDataProvider($this, array(
					'criteria'=>$criteria,
					'pagination'=>array(
						'pageSize'=>10
					)
			));
		}		
	}
	
}