<?php

/**
 * This is the model class for table "pbf_m".
 *
 * The followings are the available columns in table 'pbf_m':
 * @property integer $pbf_id
 * @property string $pbf_kode
 * @property string $pbf_nama
 * @property string $pbf_singkatan
 * @property string $pbf_alamat
 * @property string $pbf_propinsi
 * @property string $pbf_kabupaten
 * @property boolean $pbf_aktif
 */
class GFPbfM extends PbfM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PbfM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
        
       
}