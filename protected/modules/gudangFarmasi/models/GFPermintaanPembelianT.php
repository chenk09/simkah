<?php

class GFPermintaanPembelianT extends PermintaanpembelianT
{
        public  $tglkadaluarsa, $total_harganetto;
        public $tglAwal, $tglAkhir;
        public $tick;
        public $data;
        public $jumlah;
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchPenerimaanItems()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('date(tglterimabarang)',$this->tglterimabarang);
		$criteria->compare('LOWER(nopermintaan)',strtolower($this->nopermintaan),true);
                $criteria->addCondition('penerimaanbarang_id isNUll');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
         public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = 'supplier';
		$criteria->addCondition('date(tglpermintaanpembelian) BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
		$criteria->compare('LOWER(nopermintaan)',strtolower($this->nopermintaan),true);
                $criteria->compare('t.supplier_id', $this->supplier_id);
                $criteria->compare('t.syaratbayar_id', $this->syaratbayar_id);
                $criteria->addCondition('penerimaanbarang_id is null');
                $criteria->addCondition("supplier.supplier_jenis='Farmasi'");
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        /*
         * untuk Laporan Permintaan Pembelian
         */
        public function searchPermintaanPembelian()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->select = 't.supplier_id, t.permintaanpembelian_id, supplier_m.supplier_nama, supplier_m.supplier_alamat, t.nopermintaan, t.tglpermintaanpembelian,
                                     sum(obatalkes_m.harganetto) as total_harganetto
                                     ';
                $criteria->group = 't.supplier_id, t.permintaanpembelian_id, supplier_m.supplier_nama, supplier_m.supplier_alamat, t.nopermintaan, t.tglpermintaanpembelian';
		$criteria->addCondition('date(t.tglpermintaanpembelian) BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
		$criteria->compare('LOWER(t.nopermintaan)',strtolower($this->nopermintaan),true);
                $criteria->compare('t.supplier_id', $this->supplier_id);
                $criteria->compare('t.syaratbayar_id', $this->syaratbayar_id);
                $criteria->addCondition('t.penerimaanbarang_id is null');
                $criteria->addCondition("supplier_m.supplier_jenis='Farmasi'");
                $criteria->join = 'LEFT JOIN supplier_m ON t.supplier_id = supplier_m.supplier_id
                                ';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function searchPrintPermintaanPembelian()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->select = 't.supplier_id, t.permintaanpembelian_id,supplier_m.supplier_nama, supplier_m.supplier_alamat, t.nopermintaan, t.tglpermintaanpembelian,
                                     sum(obatalkes_m.harganetto) as total_harganetto
                                     ';
                $criteria->group = 't.supplier_id, t.permintaanpembelian_id,supplier_m.supplier_nama, supplier_m.supplier_alamat, t.nopermintaan, t.tglpermintaanpembelian';
		$criteria->addCondition('date(t.tglpermintaanpembelian) BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
		$criteria->compare('LOWER(t.nopermintaan)',strtolower($this->nopermintaan),true);
                $criteria->compare('t.supplier_id', $this->supplier_id);
                $criteria->compare('t.syaratbayar_id', $this->syaratbayar_id);
                $criteria->addCondition('t.penerimaanbarang_id is null');
                $criteria->addCondition("supplier_m.supplier_jenis='Farmasi'");
                $criteria->join = 'LEFT JOIN supplier_m ON t.supplier_id = supplier_m.supplier_id
                                   LEFT JOIN permintaandetail_t ON t.permintaanpembelian_id = permintaandetail_t.permintaanpembelian_id
                                   LEFT JOIN obatalkes_m ON permintaandetail_t.obatalkes_id = obatalkes_m.obatalkes_id
                                ';
                $criteria->limit = -1;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}

	public function searchGrafik()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
				//$criteria->with = 'supplier';
                $criteria->select = 'COUNT(supplier_id) as jumlah, supplier_id as data';
                $criteria->group = 'supplier_id';
				$criteria->addCondition('date(t.tglpermintaanpembelian) BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
				
		$criteria->compare('LOWER(nopermintaan)',strtolower($this->nopermintaan),true);
                $criteria->compare('supplier_id', $this->supplier_id);
                $criteria->compare('syaratbayar_id', $this->syaratbayar_id);
                $criteria->addCondition('penerimaanbarang_id is null');
                //$criteria->addCondition("supplier.supplier_jenis='Farmasi'");
                //$criteria->join = 'LEFT JOIN supplier_m ON t.supplier_id = supplier_m.supplier_id';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchPenerimaanItemFarmasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = 'supplier';
		$criteria->addCondition('date(tglpermintaanpembelian) BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
		$criteria->compare('LOWER(nopermintaan)',strtolower($this->nopermintaan),true);
                $criteria->compare('LOWER(supplier.supplier_nama)', strtolower($this->supplier_id), true);
                $criteria->compare('t.syaratbayar_id', $this->syaratbayar_id);
                $criteria->addCondition('penerimaanbarang_id is null');
                $criteria->addCondition("supplier.supplier_jenis='Farmasi'");
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
		
	}
        
        /*
         * end Laporan Permintaan Pembelian
         */
        
}