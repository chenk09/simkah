<?php

/**
 * This is the model class for table "generik_m".
 *
 * The followings are the available columns in table 'generik_m':
 * @property integer $generik_id
 * @property string $generik_nama
 * @property string $generik_namalain
 * @property boolean $generik_aktif
 */
class GFGenerikM extends GenerikM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GenerikM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}