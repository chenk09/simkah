<?php
class GFLaporanfarmasikopnameV extends LaporanfarmasikopnameV
{   
                public $tglAwal, $tglAkhir;
                public $tick;
                public $data;
                public $jumlah;

    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function searchTable()
    {
            $criteria=new CDbCriteria;

            //$criteria->addBetweenCondition('tglstokopname',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
            $criteria->compare('ruangan_id',20);
          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    
    public function searchTableGF()
    {
            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tglstokopname',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
            $criteria->compare('ruangan_id',86);
          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
        
        
    public function searchPrint()
    {
        $criteria=new CDbCriteria;

        //$criteria->addBetweenCondition('tglstokopname',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
        $criteria->compare('ruangan_id',20);

        // Klo limit lebih kecil dari nol itu berarti ga ada limit 
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
    }
    
    public function searchPrintGF()
    {
        $criteria=new CDbCriteria;

        $criteria->addBetweenCondition('tglstokopname',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
        $criteria->compare('ruangan_id',86);

        // Klo limit lebih kecil dari nol itu berarti ga ada limit 
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
    }

    public function searchGrafik()
    {
            $criteria=new CDbCriteria;
                $criteria->select = 'sum(volume_fisik) as jumlah, obatalkes_nama as data';
                $criteria->group = 'obatalkes_nama';
                //$criteria->addBetweenCondition('tglstokopname', $this->tglAwal, $this->tglAkhir);

            $criteria->addBetweenCondition('tglstokopname',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
            $criteria->compare('ruangan_id',86);
          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }

}