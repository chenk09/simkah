<?php

class GFFormuliropnameR extends FormuliropnameR {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('formuliropname_id',$this->formuliropname_id);
		$criteria->compare('stokopname_id',$this->stokopname_id);
		$criteria->addBetweenCondition('date(tglformulir)', $this->tglformulir, $this->tglAkhir);
		$criteria->compare('LOWER(noformulir)',strtolower($this->noformulir),true);
		$criteria->compare('totalvolume',$this->totalvolume);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('create_ruangan',Yii::app()->user->getState('ruangan_id'));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

?>
