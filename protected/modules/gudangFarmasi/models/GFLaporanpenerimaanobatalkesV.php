<?php

class GFLaporanpenerimaanobatalkesV extends LaporanpenerimaanobatalkesV {
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function searchByDate()
	{
		$criteria=new CDbCriteria;
		$criteria->addBetweenCondition('tglterima',$this->tglAwal, $this->tglAkhir);
		$criteria->compare('namaobat',$this->namaobat,true);
		$criteria->compare('noterima',$this->noterima,true);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('nofaktur',$this->nofaktur,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}





}
