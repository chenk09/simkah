<?php
echo CHtml::css('.control-label{
        float:left; 
        text-align: right; 
        width:120px;
        color:black;
        padding-right:10px;
    }
    table{
        font-size:11px;
    }
');
?><br/><br/>
<table width="100%" style="margin:0px;margin-left:30px;" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" style='margin-top:-30px;'>
                <tr>
                    <td width="10%">
                             <b><?php echo CHtml::encode($modPenerimaan->getAttributeLabel('noterima')); ?></b>
                    </td>
                    <Td width="5%"><b>:</b></td>
                    <td width="20%" style="margin-left:20px;"><?php echo CHtml::encode($modPenerimaan->noterima); ?></td>

                    <td width="10%">
                            <b><?php echo CHtml::encode($modPenerimaan->getAttributeLabel('supplier_id')); ?></b>
                    </td>
                    <td width="5%"><b>:</b></td>
                    <td width="20%">
                        <?php
                                echo ($modPenerimaan->supplier_id == null) ? "-" : CHtml::encode($modPenerimaan->supplier->supplier_nama);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td width="10%"><b>Tanggal Penerimaan</b></td>
                    <td width="5%"><b>:</b></td>
                    <Td width="20%"> <?php echo CHtml::encode($modPenerimaan->tglterima); ?></td>
                    
                    <td width="10%"> 
                            <b><?php echo CHtml::encode($modPenerimaan->getAttributeLabel('nosuratjalan')); ?></b>
                    <td width="1%"><b>:</b></td>
                    <td width="20%">
                        <?php
                                echo ($modPenerimaan->nosuratjalan == null) ? "-" : CHtml::encode($modPenerimaan->nosuratjalan);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td width="10%"></td>
                    <td width="5%"></td>
                    <Td width="20%"></td>
                    
                    <td width="10%"> 
                            <b>Tanggal Surat Jalan</b>
                    <td width="1%"><b>:</b></td>
                    <td width="20%">
                        <?php
                                echo ($modPenerimaan->tglsuratjalan == null) ? "-" : CHtml::encode($modPenerimaan->tglsuratjalan);
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table id="tableObatAlkes" class="table table-bordered table-condensed" style="margin-top:5px;">
    <thead>
    <tr>
        <th rowspan="2" style='text-align:center;'>No.Urut</th>
        <th rowspan="2" style='text-align:center;'>Asal Barang</th>
        <th rowspan="2" style='text-align:center;'>Kategori /&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th rowspan="2" style='text-align:center;'>Jumlah Kemasan<br/></th>
        <th rowspan="2" style='text-align:center;' width="30px;">Jumlah Permintaan</th>
        <th rowspan="2" style='text-align:center;' width="30px;">Jumlah Diterima</th>
<!--        <th rowspan="2" style='text-align:center;'>Harga Kemasan</th>-->
        <!--<th rowspan="2" style='text-align:center;'>Harga PPN</th>
        <th>Harga PPH</th>-->
<!--        <th colspan="2" style='text-align:center;'>Jumlah Diskon</th>-->
        <!-- <th rowspan="2" style='text-align:center;'>Harga Satuan</th> -->
<!--        <th rowspan="2" style='text-align:center;'>Sub Total</th>-->
    </tr>
<!--    <tr>
        <th style='text-align:center;'>%</th>
        <th style='text-align:center;'>Rp.</th>
    </tr>-->
    </thead>
    <?php
    $no=1;
    $totalSubTotal = 0;
    $totalJumlahPermintaan = 0;
    $totalDiterima = 0;
    $totalPersenDiskon = 0;
    $totalJumlahDiskon = 0;
    $totalNetto = 0;
    $totalPPN = 0;
    $totalPPH = 0;
    $totalHargaSatuan = 0;
    if (count($modPenerimaanDetails) > 0) {
        foreach($modPenerimaanDetails AS $tampilData):
            $subTotal = ($tampilData['jmlterima']*($tampilData['hargabelibesar']))-$tampilData['jmldiscount'];
            $totalSubTotal += $subTotal;
            echo "<tr>
                    <td style='text-align:center'>".$no."</td>
                    <td style='valign-text:middle'>".$tampilData->sumberdana['sumberdana_nama']."</td>  
                    <td class='right-align' style='text-align:center;'>".$tampilData->obatalkes['obatalkes_kategori']." / ".$tampilData->obatalkes['obatalkes_nama']."</td>   
                    <td class='right-align' style='text-align:center;'>".MyFunction::formatNumber($tampilData['jmlkemasan'])
                    ." ".$tampilData->satuankecil['satuankecil_nama']."/".$tampilData->satuanbesar['satuanbesar_nama']."</td>
                    <td class='right-align' style='text-align:center;'>".MyFunction::formatNumber($tampilData['jmlpermintaan'])." ".$tampilData->satuanbesar['satuanbesar_nama']."</td>
                    <td class='right-align' style='text-align:center;'>".MyFunction::formatNumber($tampilData['jmlterima'])." ".$tampilData->satuanbesar['satuanbesar_nama']."</td> 
                    
                 </tr>";  
            //                    <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($tampilData['hargasatuanper'],2)."</td>      
                 //<td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($tampilData['hargappnper'],1)."</td> 
//                    <td class='right-align'>".MyFunction::formatNumber($tampilData['hargappnper'])."</td>     
//                    <td class='right-align'>".MyFunction::formatNumber($tampilData['hargapphper'])."</td> 
            
//            <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($tampilData['hargabelibesar'],2)."</td> 
//                    <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($tampilData['persendiscount'],2)."</td>     
//                    <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($tampilData['jmldiscount'],2)."</td>     
//                     <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($subTotal,1)."</td>  
            $no++;
            $totalJumlahPermintaan+=$tampilData['jmlpermintaan'];
            $totalDiterima+=$tampilData['jmlterima'];
            $totalJumlahDiskon+=$tampilData['jmldiscount'];
            $totalPersenDiskon+=$tampilData['persendiscount'];
            $totalNetto+=$tampilData['hargabelibesar'];
            $totalPPN+=$tampilData['hargappnper'];
            $totalPPH+=$tampilData['hargapphper'];
            $totalHargaSatuan+=$tampilData['hargasatuanper'];    
        endforeach;
    }
    else{
        echo '<tr><td colspan=11>'.Yii::t('zii','No results found.').'</td></tr>';
    }
//    echo "<tr>
//            <td colspan='3' style='text-align:right'><b>Total</b></td>
//            <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($totalJumlahPermintaan)."</td>
//            <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($totalDiterima)."</td>
//            <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($totalNetto,2)."</td>
//            <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($totalPPN,2)."</td>
//            <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($totalPersenDiskon,2)."</td>
//            <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($totalJumlahDiskon,2)."</td>
//            <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($totalSubTotal+$totalPPN,2)."</td>
//         </tr>";    
//    //            <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($totalHargaSatuan,2)."</td>
////            <td class='right-align'>".MyFunction::formatNumber($totalPPN)."</td>
////            <td class='right-align'>".MyFunction::formatNumber($totalPPH)."</td>
//    
    ?>
<!--    <tr>
        <td colspan="9" style="text-align: right; font-weight: bold;">Total :</td>
        <td style="text-align: right; font-weight: bold;"><?php //echo MyFunction::formatNumber($totalSubTotal);?></td>
    </tr>
    <tr>
        <td colspan="9" style="text-align: right; font-weight: bold;">PPN :</td>
        <td style="text-align: right; font-weight: bold;"><?php //echo MyFunction::formatNumber($modPenerimaan->totalpajakppn);?></td>
    </tr>
    <tr>
        <td colspan="9" style="text-align: right; font-weight: bold;">Total Pembayaran :</td>
        <td style="text-align: right; font-weight: bold;"><?php //echo MyFunction::formatNumber($totalSubTotal + $modPenerimaan->totalpajakppn);?></td>
    </tr>-->
</table>

<?php
if (!isset($caraPrint)){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idPenerimaan'=>$modPenerimaan->penerimaanbarang_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}
?>    