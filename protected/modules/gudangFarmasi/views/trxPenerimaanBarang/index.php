<?php

Yii::app()->clientScript->registerScript('search', "
$('#divSearch-form form').submit(function(){
	$.fn.yiiGridView.update('penawaran-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2">Transaksi Penerimaan Items</legend>
<fieldset>
    <legend class="rim">Daftar Penerimaan</legend>
    <?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'penawaran-m-grid',
        'dataProvider'=>$modPermintaanPembelian->searchPenerimaanItemFarmasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
            'tglpermintaanpembelian',
            'nopermintaan',
            array(
                'name'=>'supplier_id',
                'type'=>'raw',
                'value'=>'$data->supplier->supplier_nama',
            ),
            array(
                'name'=>'syaratbayar_id',
                'type'=>'raw',
                'value'=>'$data->syaratbayar->syaratbayar_nama',
            ),
            array(
                'header'=>'Details',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("Details",array("idPermintaanPembelian"=>$data->permintaanpembelian_id)),
                array(
                    "class"=>"",
                    "target"=>"iframe",
                    "onclick"=>"$(\"#dialogDetails\").dialog(\"open\");",
                    "rel"=>"tooltip",
                    "title"=>"Klik Untuk melihat details Permintaan Pembelian",
                ))',
            ),
            array(
                'header'=>'Penerimaan',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>","'.$this->createUrl('create').'&id=$data->permintaanpembelian_id",
                    array("class"=>"",
                        "title"=>"Klik Mendaftarkan Ke Penerimaan Items",
                    )
                )',
            )
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); ?>
</fieldset>


<div id="divSearch-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'divSearch-form',
        'type'=>'horizontal',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                    <?php echo CHtml::label('Tgl Permintaan Pembelian','tglPermintaanPembelian', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPermintaanPembelian,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPermintaanPembelian,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div>
                <?php echo $form->dropDownListRow($modPermintaanPembelian,'syaratbayar_id',
                                                               CHtml::listData($modPermintaanPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
                                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                               'empty'=>'-- Pilih --',)); ?>
        </td>
        <td>
              <?php echo $form->textFieldRow($modPermintaanPembelian,'nopermintaan',array('class'=>'numberOnly')); ?>
              <?php echo $form->textFieldRow($modPermintaanPembelian,'supplier_id',
                                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                               'empty'=>'-- Pilih --',)); ?>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?><?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
?>
</div>
<?php $this->endWidget(); ?>

<!--===========================Dialog Details=========================================-->
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogDetails',
    'options'=>array(
        'title'=>'Details Permintaan Pembelian',
        'autoOpen'=>false,
        'minWidth'=>900,
        'minHeight'=>100,
        'resizable'=>false,
    ),
));
?>
<iframe src="" name="iframe" width="100%" height="500"></iframe>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>
<!--===========================END Dialog Details=========================================-->
