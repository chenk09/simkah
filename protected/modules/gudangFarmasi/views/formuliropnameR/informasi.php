<?php
$this->breadcrumbs=array(
	'Gfformuliropname Rs'=>array('index'),
	'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>'Informasi Formulir Obat Alkes Opname ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GFFormuliropnameR', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' GFFormuliropnameR', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gfformuliropname-r-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'gfformuliropname-r-grid',
	'dataProvider'=>$model->searchInformasi(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		'formuliropname_id',
                'tglformulir',
                'noformulir',
                'totalvolume',
                array(
                  'header'=>'Total Harga',
                  'type'=>'raw',
                  'value'=>'"Rp. ".MyFunction::formatNumber($data->totalharga)',
                ),
//		'totalharga',
                array(
                    'header'=>'Formulir',
                    'type'=>'raw',
                    'value'=>'CHtml::link(\'<icon class="icon-list">\', \'#\', array(\'onclick\'=>\'print("PRINT", \'.$data->formuliropname_id.\');\', \'style\'=>\'\'))',
                ),
                array(
                    'header'=>'Stok Opname',
                    'type'=>'raw',
                    'value'=>'(!empty($data->stokopname_id)) ? "Sudah Stok Opname" : (Yii::app()->controller->module->id == "farmasiApotek") ? 
                            CHtml::link(\'<icon class="icon-list">\', Yii::app()->createUrl(\'farmasiApotek/StokOpnameFarmasi/Index\', array(\'id\'=>$data->formuliropname_id))) 
                            : CHtml::link(\'<icon class="icon-list">\', Yii::app()->createUrl(\'gudangFarmasi/StokopnameT/Index\', array(\'id\'=>$data->formuliropname_id)))',
                ),
//		array(
//                        'name'=>'formuliropname_id',
//                        'value'=>'$data->formuliropname_id',
//                        'filter'=>false,
//                ),
//		'stokopname_id',
//		'tglformulir',


		/*
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php //echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form">
<?php $this->renderPartial($this->pathView.'_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 

        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
$formulir = $model->formuliropname_id;
$js = <<< JSCRIPT
function print(caraPrint, id)
{
    window.open("${urlPrint}/&id="+id+"&caraPrint="+caraPrint,"",'location=_new, width=1100px');

}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
<?php 
 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
//        $this->widget('TipsMasterData',array('type'=>'admin'));
//        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
//        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
//
//$js = <<< JSCRIPT
//function print(caraPrint)
//{
//    window.open("${urlPrint}/"+$('#gfformuliropname-r-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
//}
//JSCRIPT;
//Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>