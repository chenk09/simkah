<?php 
Yii::app()->clientScript->registerScript('search', "
//$('.search-button').click(function(){
//    $('.search-form').slideToggle();
//    return false;
//});
$('#pencarianobat-form').submit(function(){
    $.fn.yiiGridView.update('obatalkes-m-grid', {
        data: $(this).serialize()
    });
    getTotal();
    return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php // echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<legend class="rim">Pencarian Obat Alkes</legend>
<div class="search-form">
<?php $this->renderPartial($this->pathView.'_searchObat',array(
    'model'=>$model,
)); ?>
</div>

<?php 
Yii::app()->clientScript->registerScript('head', '
    function getTotal(){
        var totalStok = 0;
        var totalHarga = 0;
        $(".stok").each(function(){
            if ($(this).parents("tr").find(".cekList").is(":checked")){
                totalStok += parseFloat($(this).val());
                totalHarga += parseFloat($(this).parents("tr").find("#harga").val());
            }
        });
        $("#GFFormuliropnameR_totalvolume").val(totalStok);
        $("#GFFormuliropnameR_totalharga").val(totalHarga);
    }
',  CClientScript::POS_HEAD); 
?>