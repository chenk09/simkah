<?php 
echo CHtml::css('#isiScroll{max-height:300px;overflow-y:scroll;margin-bottom:10px;}'); 
?>
<!-- search-form -->
<div id='isiScroll'>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'obatalkes-m-grid',
    'dataProvider'=>$model->searchDataObat(),
//    'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        ////'obatalkes_id',
            array(
                'header'=> 'Pilih',
                'type'=>'raw',
                'value'=>'
                    CHtml::activeHiddenField($data, \'[\'.$data->obatalkes_id.\']obatalkes_id\').
                    CHtml::checkBox(\'GFObatalkesM[\'.$data->obatalkes_id.\'][cekList]\', true, array(\'onclick\'=>\'setUrutan()\', \'class\'=>\'cekList\', \'onclick\'=>\'getTotal();\'));
                    ',
            ),
            array(
                'name'=>'jenisobatalkes_id',
                'value'=>'$data->jenisobatalkes->jenisobatalkes_nama',
                ),
            'obatalkes_kode',
//            array(
//                'header'=>'Nama Obat <br/> Kategori',
//                'type'=>'raw',
//                'value'=>'$data->obatalkes_nama.\'<br/>\'.$data->obatalkes_kategori',
//            ),
//            'obatalkes_golongan',
            array(
                'header'=>'Nama Obat <br/> Kekuatan',
                'type'=>'raw',
                'value'=>'$data->obatalkes_nama.\'<br/>\'.$data->kekuatan',
            ),
            array(
                'header'=>'Golongan<br/>Kategori',
                'type'=>'raw',
                'value'=>'$data->obatalkes_golongan.\'<br/>\'.$data->obatalkes_kategori',
            ),
            array(
                'name'=>'sumberdana_id',
                'value'=>'$data->sumberdana->sumberdana_nama',
            ),
            'tglkadaluarsa',
            array(
                'header'=>'Harga Jual',
                'type'=>'raw',
                'value'=>'CHtml::textField(\'harga\', $data->hargajual, array(\'class\'=>\'span1 harga\', \'readonly\'=>true))'
            ),
//            array(
//                'header'=>'HJA Resep',
//                'type'=>'raw',
//                'value'=>'CHtml::textField(\'harga\', $data->hjaresep, array(\'class\'=>\'span1 harga\', \'readonly\'=>true))'
//            ),
//            array(
//                'header'=>'HJA Non-Resep',
//                'type'=>'raw',
//                'value'=>'CHtml::textField(\'harga\', $data->hjanonresep, array(\'class\'=>\'span1 harga\', \'readonly\'=>true))'
//            ),
            array(
                'header'=>'Harga Netto',
                'type'=>'raw',
                'value'=>'CHtml::textField(\'hargaNetto\', $data->harganetto, array(\'class\'=>\'span1\', \'readonly\'=>true))'
            ),
//            array(
//                'header'=>'Harga Satuan <br/> harga Netto',
//                'type'=>'raw',
//                'value'=>'CHtml::textField(\'harga\', $data->hargajual, array(\'class\'=>\'span1 harga\', \'readonly\'=>true))
//                            .\'<br/>\'.
//                          '
//            ),
            'minimalstok',
            array(
                'header'=>'Stok Sistem',
                'type'=>'raw',
                'value'=> 'CHtml::textField(\'stok\', StokobatalkesT::getStokBarang($data->obatalkes_id, '.Yii::app()->user->getState('ruangan_id').'), array(\'class\'=>\'stok span1\', \'readonly\'=>true))',
            ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                    getTotal();
                }',
)); ?> 
    </div>
