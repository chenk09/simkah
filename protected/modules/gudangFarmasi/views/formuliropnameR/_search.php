<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'gfformuliropname-r-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'formuliropname_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'stokopname_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'tglformulir',array('class'=>'span5')); ?>
        <div class="control-group ">
            <label class='control-label'>
                <?php
                    echo CHtml::encode($model->getAttributeLabel('tglformulir'));
                ?>

                </label>
            <div class="controls">
                <?php
                $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglformulir',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_TIME_FORMAT,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </div>
        </div>
        <div class="control-group ">
            <label class='control-label'>
                <?php
                    echo CHtml::label('Sampai Dengan','sampaiDengan',array());
                ?>

                </label>
            <div class="controls">
                <?php
                $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglAkhir',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_TIME_FORMAT,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </div>
        </div>

	<?php echo $form->textFieldRow($model,'noformulir',array('class'=>'span3','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'totalvolume',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'totalharga',array('class'=>'span5')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
<?php
$content = $this->renderPartial('gudangFarmasi.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
	</div>

<?php $this->endWidget(); ?>
