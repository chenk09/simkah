
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'kadarobat-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#LookupM_1_lookup_name',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

            <?php //echo $form->dropDownListRow($model,'lookup_type', Params::daftarLookUp(),array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_name','')", 'maxlength'=>100)); ?>
            <?php //echo $form->textFieldRow($model,'lookup_type',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_name','')", 'maxlength'=>100,)); ?>
            <?php echo $form->textFieldRow($model,'lookup_name',array('class'=>'span3', 'onkeyup'=>"namaLain(this)", 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_value','LookupM_lookup_type')", 'maxlength'=>200)); ?>
            <?php echo $form->textFieldRow($model,'lookup_value',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_kode','LookupM_lookup_name')", 'maxlength'=>200)); ?>
            <?php echo $form->textFieldRow($model,'lookup_kode',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_urutan','LookupM_lookup_value')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'lookup_urutan',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_aktif','LookupM_lookup_kode')")); ?>
            <?php echo $form->checkBoxRow($model,'lookup_aktif',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_aktif','LookupM_lookup_kode')")); ?>
            
<!--	<table id="tbl-Lookup" class="table table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th>Nama</th>
                <th>Value</th>
                <th>Kode</th>
                <th>Urutan</th>
                <th>Aktif</th>
                <th></th>
            </tr>
            </thead>-->
            <?php// $modLookup = LookupM::model()->findAllByAttributes(array('lookup_type'=>$model->lookup_type)); ?>
<!--            <tbody>-->
            <?php //foreach($modLookup as $x=>$row){ ?>
            
               
<!--            <tr>
                    
                <td>
                        <?php //echo $form->hiddenField($row, '['.$x.']lookup_id');?>
                        <?php //echo $form->hiddenField($row, '['.$x.']lookup_type');?>
                        <?php //echo $form->textField($row,'['.$x.']lookup_name',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_value','LookupM_lookup_type')", 'maxlength'=>200,'placeholder'=>$model->getAttributeLabel('lookup_name'))); ?>
                        <span class="required">*</span>
                    </td>
                    <td>
                        <?php //echo $form->textField($row,'['.$x.']lookup_value',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_kode','LookupM_lookup_name')", 'maxlength'=>200, 'placeholder'=> $model->getAttributeLabel('lookup_value'))); ?>
                    </td>
                    <td>
                        <?php //echo $form->textField($row,'['.$x.']lookup_kode',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_urutan','LookupM_lookup_value')", 'maxlength'=>50, 'placeholder'=> $model->getAttributeLabel('lookup_kode'))); ?>
                    </td>
                   <td>
                        <?php //echo $form->textField($row,'['.$x.']lookup_urutan',array( 'class'=>'span3 numbersOnly', 'style'=>'width:80px', 'onkeypress'=>"return nextFocus(this,event,'LookupM_lookup_aktif','LookupM_lookup_kode')", 'placeholder'=> $model->getAttributeLabel('lookup_urutan'))); ?>
                    </td>
                   <td>
                        <?php //echo $form->checkBox($row,'['.$x.']lookup_aktif',array('class'=>'span1 lookupAktif','style'=>'width:80px')); ?>
                   </td>
                    <td>
                        <?php //echo CHtml::link('<i class="icon-plus-sign icon-white"></i>', '#', array('class'=>'btn btn-primary','onclick'=>'addRow(this)','id'=>'row1-plus')); ?>
                    </td>
                    
                </tr>
            
            <? //} ?>
                <tbody>
        </table>-->
        
        <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.kadarObatM.'/admin'), 
                                    array('class'=>'btn btn-danger',
                                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('gudangFarmasi.views.tips.tipsaddedit',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
        </div>

<?php $this->endWidget(); ?>
<?php
$buttonMinus = CHtml::link('<i class="icon-minus-sign icon-white"></i>', '#', array('class'=>'btn btn-danger','onclick'=>'delRow(this); return false;'));
$confimMessage = Yii::t('mds','Do You want to remove?');
$js = <<< JSCRIPT
$(document).ready(function(){
    $('.numbersOnly').keyup(function() {
        if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
           this.value = this.value.replace(/[^0-9\.]/g, '');
        }
    });
});

function addRow(obj)
{
    var tr = $('#tbl-Lookup tbody tr:first').html();
    $('#tbl-Lookup tr:last').after('<tr>'+tr+'</tr>');
    $('#tbl-Lookup tr:last td:last').append('$buttonMinus');
        renameInput('LookupM','lookup_id');
        renameInput('LookupM','lookup_name');
        renameInput('LookupM','lookup_value');
        renameInput('LookupM','lookup_kode');
        renameInput('LookupM','lookup_urutan');
        renameInput('LookupM','lookup_type');
        renameInput('LookupM','lookup_aktif');
$('#tbl-Lookup tr:last').find('input').val('');
$('#tbl-Lookup tr:last').find(".lookupAktif").val(1);
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tbl-Lookup tbody tr').length;
    var i = 1;
    $('#tbl-Lookup tbody tr').each(function(){
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
    i++;    
    });
}

function delRow(obj)
{
    if(!confirm("$confimMessage")) return false;
    else {
        $(obj).parent().parent().remove();
        renameInput('SALookupM','lookup_id');
        renameInput('SALookupM','lookup_name');
        renameInput('SALookupM','lookup_value');
        renameInput('SALookupM','lookup_kode');
        renameInput('SALookupM','lookup_urutan');
    }
}
JSCRIPT;
Yii::app()->clientScript->registerScript('multiple input',$js, CClientScript::POS_HEAD);
?>

<script type="text/javascript">
    function namaLain(nama)
    {
        document.getElementById('LookupM_1_lookup_value').value = nama.value.toUpperCase();
    }
</script>