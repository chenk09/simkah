
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfsupplier-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
        <table class="table-condensed">
            <tr>
                <td>
                    <?php echo $form->textFieldRow($model,'supplier_kode',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_namalain',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textAreaRow($model,'supplier_alamat',array('rows'=>4, 'cols'=>30, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php echo $form->textFieldRow($model,'supplier_kodepos',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                </td>
                <td>
                    <?php echo $form->dropDownListRow($model,'supplier_propinsi', CHtml::listData($model->PropinsiItems, 'propinsi_nama', 'propinsi_nama'),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",'ajax'=>array('type'=>'POST','url'=>Yii::app()->createUrl('ActionDynamic/GetKabupatendrNamaPropinsi',array('encode'=>false,'namaModel'=>'GFSupplierM','attr'=>'supplier_propinsi')),'update'=>'#GFSupplierM_supplier_kabupaten'))); ?>
                    <?php echo $form->dropDownListRow($model,'supplier_kabupaten',array(),array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>  
                    <?php echo $form->textFieldRow($model,'supplier_telp',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                
                    <?php echo $form->textFieldRow($model,'supplier_fax',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_npwp',array('class'=>'span3 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_website',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_email',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_cp',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_norekening',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_jenis',array('class'=>'span2 ', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100,'readOnly'=>true)); ?>
                    
                
                </td>
            </tr>
            <tr>
<!--                <td colspan="2">
                    <div class="control-group">
                        <?php //echo Chtml::label('Obat Alkes','Obat Alkes',array('class'=>'control-label'));?>
                        <div class="controls">
                           <?php 
//                               $this->widget('application.extensions.emultiselect.EMultiSelect',
//                                             array('sortable'=>true, 'searchable'=>true)
//                                        );
//                                echo CHtml::dropDownList(
//                                'obatalkes_id[]',
//                                '',
//                                CHtml::listData(GFObatalkesfarmasiV::model()->findAll('obatalkes_aktif=TRUE AND obatalkes_farmasi=TRUE ORDER BY obatalkes_nama'), 'obatalkes_id', 'obatalkes_nama'),
//                                array('multiple'=>'multiple','key'=>'obatalkes_id', 'class'=>'multiselect','style'=>'width:500px;height:150px')
//                                        );
                          ?>
                        </div>
                    </div>      
                </td> -->
            </tr>
        </table>
            <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.supplierM.'/admin'), 
                                    array('class'=>'btn btn-danger',
                                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('gudangFarmasi.views.tips.tipsaddedit',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
            </div>

<?php $this->endWidget(); ?>
<?php
                       
$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

