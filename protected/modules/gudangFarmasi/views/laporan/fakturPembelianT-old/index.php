<?
$url = Yii::app()->createUrl('gudangFarmasi/laporan/FrameGrafikLaporanPembelian&id=1');
Yii::app()->clientScript->registerScript('search', "
$('#search-laporan').submit(function(){
	$.fn.yiiGridView.update('laporan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");php
?>
<legend class="rim2">Laporan Faktur Pembelian</legend>
<?php $this->renderPartial('fakturPembelianT/_search',array('model'=>$model)); ?>
<fieldset>
    <?php $this->renderPartial('fakturPembelianT/_table',array('model'=>$model)); ?>
    <?php $this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0' onload="javascript:resizeIframe(this);">
    </iframe>
</fieldset>
<?php        
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanPembelian');
$this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>