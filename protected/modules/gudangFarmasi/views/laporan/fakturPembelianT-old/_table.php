<?php 
    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchLaporanPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchLaporan();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php $this->widget($table,array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                        'header'=>'Nama Supplier',
                        'value'=>'$data->supplier->supplier_nama',
                        'footer'=>'Total',
                    ),
                    'nofaktur',
                    'tglfaktur',
                    'tgljatuhtempo',
                    'keteranganfaktur',
                    array(
                        'header'=>'Total tagihan',
                        'value'=>'"Rp.".MyFunction::formatNumber($data->totalhargabruto)',
                    ),
                    array(
                        'header'=>'Jumlah Dibayarkan',
                        'type'=>'raw',
                        'value'=>'(($data->jmldibayarkan<1)? "Rp.0" : "Rp. ".MyFunction::formatNumber($data->jmldibayarkan))',
                    ),
                    array(
                        'header'=>'Sisa tagihan',
                        'value'=>'(($data->jmldibayarkan<1)? "Rp.".MyFunction::formatNumber($data->totalhargabruto - 0) : "Rp.".MyFunction::formatNumber($data->totalhargabruto-$data->jmldibayarkan))',
                    ),
//                    array(
//                        'header'=>'Tanggal Bayar',
//                        'value'=>'$data->create_time',
//                    )
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>