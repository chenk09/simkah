<?php
    Yii::app()->clientScript->registerScript('search', "
    $('#search-laporan').submit(function(){
		$('#laporan-grid').addClass('srbacLoading');
		$.fn.yiiGridView.update('laporan-grid', {
				data: $(this).serialize()
		});
		return false;
    });
    ");
?>
<legend class="rim2"> Laporan Obat Kadaluarsa</legend>
<fieldset>
	<legend class="rim">Pencarian</legend>
	<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
		'type'=>'horizontal',
		'id'=>'search-laporan',
	));?>
	<?php echo $form->textFieldRow($model, 'obatalkes_nama',array('class'=>'span3')); ?>
	<?php echo $form->textFieldRow($model, 'obatalkes_kode',array('class'=>'span3')); ?>
	<?php echo $form->dropDownListRow($model, 'ruangan_id', CHtml::listData(RuanganM::model()->findAll("instalasi_id IN(2,4,5,6,9) AND ruangan_aktif = true ORDER BY instalasi_id, ruangan_nama"),'ruangan_id', 'ruangan_nama'),array(
		'empty'=>'-- Pilih --',
		'class'=>'span3'
	)); ?>
    <div class="form-actions">
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array(
			'class'=>'btn btn-primary', 'type'=>'submit'
		)); ?>
    </div>	
	<?php $this->endWidget();?>	
</fieldset>
<fieldset>
	<legend class="rim">Daftar Obat</legend>
<?php
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$template = "{pager}{summary}\n{items}";
if(isset($caraPrint)){
	$data = $model->searchStock();
	$template = "{items}";
	if($caraPrint=='EXCEL'){
		$table = 'ext.bootstrap.widgets.BootExcelGridView';
	}
}else{
	$data = $model->searchStock();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
	'itemsCssClass'=>'table table-bordered table-striped table-condensed',
	'template'=>$template,
	'mergeHeaders'=>array(
		array(
			'name'=>'<center>Qty</center>',
			'start'=>4, //indeks kolom 3
			'end'=>6, //indeks kolom 4
		),
	),
	'columns'=>array(
		array(
			'header'=>'No',
			'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
			'footerHtmlOptions'=>array('colspan'=>4, 'style'=>'text-align:right;'),
			'type'=>'raw',
			 'htmlOptions'=>array(
				'style'=>'text-align:right;padding-right:10%;'
			),
		),
		"obatalkes_kode",
		"obatalkes_nama",
		"jenisobatalkes_nama",
		"minimalstok",
		"qtystok_in",
		"qtystok_out",
		"qtystok_current",
		"ruangan_nama",
		"tglkadaluarsa"
	),
)); ?>	
	
	
</fieldset>