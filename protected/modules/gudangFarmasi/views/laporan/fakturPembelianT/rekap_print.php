<div style='border:1px solid #cccccc; border-radius:2px;padding:10px; width: 15%;float:right;margin-right:30px;margin-top:-80px;text-align:center;'>
	<font style='font-size:9pt;text-align:center;'><B>PURCHASE RECEIVED REPORT</B></font><br>
</div>
<?php $this->widget("ext.bootstrap.widgets.HeaderGroupGridViewNonRp",
	array(
		'id'=>'rekapLaporanFakturPembelian',
		'dataProvider'=>$model->laporanPembelian(true),
		'template'=>"{items}",
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'columns'=>array(
			array(
				'header'=>'Nama Supplier',
				'name'=>'supplier_id',
				'value'=>'$data->supplier->supplier_nama',
				'footer'=>'Total',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'No Faktur',
				'name'=>'nofaktur',
				'type'=>'raw',
				'value'=>'$data->nofaktur',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'Tgl Faktur',
				'name'=>'tglfaktur',
				'type'=>'raw',
				'value'=>'$data->tglfaktur',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
			),
			array(
				'header'=>'Tgl Jatuh Tempo',
				'name'=>'tgljatuhtempo',
				'type'=>'raw',
				'value'=>'$data->tgljatuhtempo',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
				'footerHtmlOptions'=>array(
						'colspan'=>3,
						'style'=>'text-align:right;font-style:italic;'
				),
				'footer'=>'Total',
			),
			array(
				'name'=>'totalhargabruto',
				'value'=>'MyFunction::formatNumber($data->totalhargabruto)',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array(
					'style'=>'text-align:right',
					'class'=>'currency'
				),                            
				'footer'=>'sum(totalhargabruto)',
			),
			array(
				'name'=>'jmldiscount',
				'value'=>'MyFunction::formatNumber($data->jmldiscount)',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array(
					'style'=>'text-align:right',
					'class'=>'currency'
				),                            
				'footer'=>'sum(jmldiscount)',
			),
			array(
				'name'=>'totalpajakppn',
				'value'=>'MyFunction::formatNumber($data->totalpajakppn)',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array(
					'style'=>'text-align:right',
					'class'=>'currency'
				),                            
				'footer'=>'sum(totalpajakppn)',
			),
			array(
				'name'=>'totalpajakpph',
				'value'=>'MyFunction::formatNumber($data->totalpajakpph)',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array(
					'style'=>'text-align:right',
					'class'=>'currency'
				),                            
				'footer'=>'sum(totalpajakpph)',
			),
			array(
				'name'=>'biayamaterai',
				'value'=>'MyFunction::formatNumber($data->biayamaterai)',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array(
					'style'=>'text-align:right',
					'class'=>'currency'
				),                            
				'footer'=>'sum(biayamaterai)',
			),
			array(
				'name'=>'totharganetto',
				'value'=>'MyFunction::formatNumber($data->totharganetto)',
				'headerHtmlOptions'=>array('style'=>'text-align:center;'),
				'htmlOptions'=>array('style'=>'text-align:right;'),
				'footerHtmlOptions'=>array(
					'style'=>'text-align:right',
					'class'=>'currency'
				),                            
				'footer'=>'sum(totharganetto)',
			)
		),
		'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>