<?php 

if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array('judulLaporan'=>$judulLaporan, 'periode'=>$periode, 'colspan'=>10));  

if ($caraPrint != 'GRAFIK'){
	$format = new CustomFormat;
	
	if($_GET["filter_tab"] == "rekap"){
		$model = new GFFakturPembelianT();
        if(isset($_GET['GFFakturPembelianT']))
		{   
            $model->attributes = $_GET['GFFakturPembelianT'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAkhir']);
        }
		$this->renderPartial('fakturPembelianT/rekap_print', array('model'=>$model, 'caraPrint'=>$caraPrint));
	}else{
		$model = new GFFakturDetailT();
        if(isset($_GET['GFFakturPembelianT']))
		{   
            $model->attributes = $_GET['GFFakturPembelianT'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GFFakturPembelianT']['tglAkhir']);
            $model->supplier_id = $_GET['GFFakturPembelianT']['supplier_id'];
        }
		$this->renderPartial('fakturPembelianT/det_rekap_print', array('model'=>$model, 'caraPrint'=>$caraPrint));
	}	
}
//$this->renderPartial('fakturPembelianT/rekapPrint', array('model'=>$model, 'caraPrint'=>$caraPrint)); 

if ($caraPrint == 'GRAFIK')
{
	echo $this->renderPartial('_grafik', array('model'=>$model, 'data'=>$data, 'caraPrint'=>$caraPrint), true); 
}


?>