<?php
    $url = Yii::app()->createUrl('gudangFarmasi/laporan/frameGrafikStockOpname&id=1');
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('#search-laporan').submit(function(){
        $('#Grafik').attr('src','').css('height','0px');
        $.fn.yiiGridView.update('tableLaporan', {
                data: $(this).serialize()
        });
        return false;
    });
    ");
?>
<legend class="rim2">Laporan Stock Opname</legend>
<div class="search-form">
<?php $this->renderPartial('stockOpname/_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->
<fieldset>
    <legend class="rim">Tabel Stock Opname</legend>
    <?php //$model = new GFLaporanfarmasikopnameV; ?>
    <?php $this->renderPartial('stockOpname/_tableStockOpname', array('model'=>$model)); ?>
    <?php $this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
    </iframe>        
</fieldset>

<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanStockOpname');
    $this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); ?>