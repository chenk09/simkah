<?php
/**
* modules/gudangFarmasi/views/laporan/penerimaanPerSupplier/index.php
* Updated by    : Jembar
* Date          : 25-04-2014
* Issue         : EHJ-1080
* Deskripsi     : view Index yang berisi tab laporan
**/
?>

<?php
    $this->breadcrumbs=array(
        'falaporan Ms'=>array('index'),
        'Manage',
    );
?>
<legend class="rim2">Laporan Penerimaan Obat Alkes</legend>
<br><br>
<?php $this->renderPartial('penerimaanObatAlkes/_tabMenu',array()); ?>
<?php $this->renderPartial('penerimaanObatAlkes/_jsFunctions',array()); ?>
<div>
    <iframe id="frame" src="" width='100%' frameborder="0" style="overflow-y:scroll" ></iframe>
</div>