<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array('judulLaporan'=>$judulLaporan, 'periode'=>$periode, 'colspan'=>10));  
?>
<style type="text/css">
	tabel td,tabel{
		padding: 0;
		margin: 0;
	}
</style>
<table class='tabel'>
	<tr>
		<td><b>No Penerimaan</b></td>
		<td><b>Tgl Terima</b></td>
		<td><b>No Pembelian</b></td>
	</tr>
	<tr>
		<td><?php echo $noterima; ?></td>
		<td><?php echo $tglterima; ?></td>
		<td><?php echo $nopermintaan; ?></td>
	</tr>
	<tr>
		<td><b>Faktur/Surat</b></td>
		<td><b>Tgl Faktur</b></td>
		<td><b>Tgl Jatuh Tempo</b></td>
	</tr>
	<tr>
		<td><?php echo $nofaktur; ?></td>
		<td><?php echo $tglfaktur; ?></td>
		<td><?php echo $tgljatuhtempo; ?></td>
	</tr>
	<tr>
		<td colspan=3>Nama Supplier : <?php echo $supplier_nama; ?></td>
	</tr>
</table>
<?php $this->renderPartial('penerimaanObatAlkes/detailFaktur/_table',array('modPenerimaan'=>$modPenerimaan)); ?>

<?php  //$ruanganAsal = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'))->ruangan_nama;?>
<!-- <div style="text-align: center;">
    <h2><?php //echo $judulLaporan; ?></h2>
    <b>Periode : <?php //echo $periode; ?></b><br>
    <b>Ruangan Asal : <?php //echo $ruanganAsal; ?></b>
</div> -->
<?php
// if ($caraPrint != 'GRAFIK')
// $this->renderPartial('penerimaanObatAlkes/_table', array('model'=>$model, 'caraPrint'=>$caraPrint)); 

// if ($caraPrint == 'GRAFIK')
// echo $this->renderPartial('_grafik', array('model'=>$model, 'data'=>$data, 'caraPrint'=>$caraPrint), true); 
// ?>