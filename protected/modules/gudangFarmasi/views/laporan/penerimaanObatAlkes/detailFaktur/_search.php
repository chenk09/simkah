<?php
    /**
    * gudangFarmasi/views/laporan/penerimaanObatAlkes/_search.php
    * Updated by    : Jembar
    * Date          : 22-04-2014
    * Issue         : EHJ-1080
    * Deskripsi     : Penambahan search by Supplier
    **/
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<script type="text/javascript">
    function reseting()
    {
        setTimeout(function(){
            $.fn.yiiGridView.update('LapPenerimaanOA-m-grid', {
                    data: $('#LapPenerimaanOA-m-search').serialize()
            });
        },1000);

    }   
</script>


<div class="search-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search-laporan',
        'type'=>'horizontal',
)); ?>
                <legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('No. Penerimaan','noterima', array('class'=>'control-label')) ?>
                    <div class="controls">
                                <?php //echo $form->hiddenField($model, 'noterima', array('value'=>$model->noterima, 'readonly'=>true)); ?>
                                       <?php $this->widget('MyJuiAutoComplete',array(
                                                'model'=>$model,
                                                'attribute'=>'noterima',
                                                'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/nomorTerimaGF'),
                                                'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'select'=>'js:function( event, ui ) {
                                                              $("#'.CHtml::activeId($model,'noterima').'").val(ui.item.noterima);
                                                              $("#'.CHtml::activeId($model,'tglterima').'").val(ui.item.tglterima);
                                                    }',
                                                ),
                                                'htmlOptions'=>array('readonly'=>false,'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3 numbersOnly'), 
                                                'tombolDialog'=>array('idDialog'=>'dialogFaktur'),
                                )); ?>
                    </div>
            </div>
             <div class="control-group ">
                    <label class="control-label" for="nofaktur">Nomor Faktur</label>
                    <div class="controls">
                        <?php echo $form->textField($model,'nofaktur', '', array('disabled'=>false,'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3')) ?>
                    </div>
                </div>
            <div class="control-group ">
                    <label class="control-label" for="nopermintaan">Nomor Permintaan</label>
                    <div class="controls">
                        <?php echo $form->textField($model,'nopermintaan', '', array('disabled'=>false,'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3')) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label" for="supplier_nama">Nama Supplier</label>
                    <div class="controls">
                        <?php echo $form->textField($model,'supplier_nama', '', array('disabled'=>false,'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3')) ?>
                    </div>
                </div>
            </td>
            <td>
            <div class="control-group ">
                    <?php echo CHtml::label('Tgl Terima','tglterima', array('class'=>'control-label')) ?>
                        <div class="controls">
                             <?php $minDate = (Yii::app()->user->getState('tglpemakai')) ? '' : 'd'; ?>
                             <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglterima',
                                                    'name'=>'tglterima',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',
                                                        //'minDate'=>$minDate,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                              )); ?>
                        </div>
             </div>
             <div class="control-group ">
                    <?php echo CHtml::label('Tgl Faktur','tglfaktur', array('class'=>'control-label')) ?>
                        <div class="controls">
                             <?php $minDate = (Yii::app()->user->getState('tglpemakai')) ? '' : 'd'; ?>
                             <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglfaktur',
                                                    'name'=>'tglfaktur',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',
                                                        //'minDate'=>$minDate,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                              )); ?>
                        </div>
             </div>
            <div class="control-group ">
                    <?php echo CHtml::label('Tgl Jatuh Tempo','tgljatuhtempo', array('class'=>'control-label')) ?>
                        <div class="controls">
                             <?php $minDate = (Yii::app()->user->getState('tgljatuhtempo')) ? '' : 'd'; ?>
                             <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgljatuhtempo',
                                                    'name'=>'tgljatuhtempo',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',
                                                        //'minDate'=>$minDate,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                              )); ?>
                        </div>
             </div>

        </td>
    </tr>

</table>
	<div class="form-actions">
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'search-laporan')); ?>
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onclick'=>'resetForm();')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
<?php
// Yii::app()->clientScript->registerScript('search', "
// $('#LapPenerimaanOA-m-search').submit(function(){
//     $.fn.yiiGridView.update('LapPenerimaanOA-m-grid', {
//         data: $(this).serialize()
//     });
//     return false;
// });
// ");
?>

<?php 
//========= Dialog buat cari no Penerimaan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogFaktur',
    'options'=>array(
        'title'=>'Pencarian Nomor Penerimaan Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modeldg = new RincianfakturgudangfarmasiV();
$modeldg->unsetAttributes();
if(isset($_GET['RincianfakturgudangfarmasiV'])) {
    $modeldg->attributes = $_GET['RincianfakturgudangfarmasiV'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
  'id'=>'ObatalkesM-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
  'dataProvider'=>$modeldg->searchNoTerima(),
  'filter'=>$modeldg,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
  'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectObat",
                                    "onClick" => "
                                                  $(\"#idObatAlkes\").val(\"$data->noterima\");
                                                  $(\"#'.CHtml::activeId($model,'noterima').'\").val(\"$data->noterima\");
                                                  $(\"#tglterima\").val(\"".$data->tglterima."\");
                                                  $(\"#RincianfakturgudangfarmasiV_nofaktur\").val(\"".$data->nofaktur."\");
                                                  $(\"#tglfaktur\").val(\"".$data->tglfaktur."\");
                                                  $(\"#RincianfakturgudangfarmasiV_nopermintaan\").val(\"".$data->nopermintaan."\");
                                                  $(\"#tgljatuhtempo\").val(\"".$data->tgljatuhtempo."\");
                                                  $(\"#RincianfakturgudangfarmasiV_supplier_nama\").val(\"".$data->supplier_nama."\");
                                                  $(\"#dialogFaktur\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'Nomor Penerimaan',
                    'value'=>'$data->noterima',
                ),
                array(
                    'header'=>'Nomor Faktur',
                    'value'=>'$data->nofaktur',
                ),
                array(
                    'header'=>'Tgl Faktur',
                    'value'=>'$data->tglfaktur',
                ),
                array(
                    'header'=>'No Permintaan',
                    'value'=>'$data->nopermintaan',
                ),
                array(
                    'header'=>'Tgl Terima',
                    'value'=>'$data->tglterima',
                ),
                array(
                    'header'=>'Tgl Jatuh Tempo',
                    'value'=>'$data->tgljatuhtempo',
                ),
                array(
                    'header'=>'Nama Supplier',
                    'filter'=>  CHtml::activeTextField($model, 'supplier_nama'),
                    'value'=>'$data->supplier_nama',
                ),
            ),
            'afterAjaxUpdate' => 'function(id, data){
            jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
        ));
$this->endWidget();
//========= end ObatalkesM dialog =============================
?>