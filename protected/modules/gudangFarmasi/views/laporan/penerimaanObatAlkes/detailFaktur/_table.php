<?php
/**
* modules/farmasiApotek/views/laporan/PenerimaanObatAlkes/penerimaanPerKelompokObat/_table.php
* Updated by    : Jembar
* Date          : 22-04-2014
* Issue         : EHJ-1079
* Deskripsi     : Table
**/
?>
<div id="LapPenerimaanOA-m-grid" class="grid-view">
	<table class="table table-striped table-bordered table-condensed">
		<thead>
		    <tr>
		        <th id="LapPenerimaanOA-m-grid_c4"> Kode </th>
		        <th id="LapPenerimaanOA-m-grid_c5"> Obat Alkes </th>
		        <th id="LapPenerimaanOA-m-grid_c6"> Harga Satuan </th>
		        <th id="LapPenerimaanOA-m-grid_c7"> Qty </th>
		        <th id="LapPenerimaanOA-m-grid_c8"> Satuan Besar </th>
		        <th id="LapPenerimaanOA-m-grid_c9"> Bruto </th>
		        <th id="LapPenerimaanOA-m-grid_c10"> Disc(%) </th>
		        <th id="LapPenerimaanOA-m-grid_c11"> Ppn(%) </th>
		        <th id="LapPenerimaanOA-m-grid_c12"> Total </th>
		    </tr>
		</thead>
		<tbody>
				<?php 
					$i=1;
					$jml_row = count($modPenerimaan);

					$totBruto = 0;
					$totDiskon = 0;
					$totPpn = 0;
					$totNetto = 0;
					$totTransaksi = 0;
					$totBayar = 0;
					$totSisa = 0;
					if($jml_row == 0){
						echo "<tr><td colspan='9'>Data Tidak ditemukan</td></tr>";
					}
					foreach ($modPenerimaan as $key => $mod) {

						echo "<tr>";
						echo "<td>".$mod['obatalkes_kode']."</td>";
						echo "<td>".$mod['obatalkes_nama']."</td>";
						echo "<td>".number_format($mod['hargabelisatuan'],2)."</td>";
						echo "<td>".number_format($mod['qty'],2)."</td>";
						echo "<td>".$mod['satuanbesar_nama']."</td>";
						echo "<td>".number_format($mod['bruto'],2)."</td>";
						echo "<td>".number_format($mod['persendiscount'],2)."</td>";
						echo "<td>".number_format($mod['persenppn'],2)."</td>";
						echo "<td>".number_format($mod['netto'],2)."</td>";
						echo "</tr>";

						$totBruto += $mod['bruto'];
						$totDiskon += $mod['nominaldiscount'];  
						$totPpn += $mod['hargappnfaktur']; 
						$totNetto += $mod['netto'];
						$i++;
					}
					echo "<tr>";
					echo "<td colspan=5></td>";
					echo "<td colspan=2>Total</td>";
					echo "<td colspan=2 textalign=right>".number_format($totBruto,2)."</td>";
					echo "</tr>";

					echo "<tr>";
					echo "<td colspan=5></td>";
					echo "<td colspan=2>Discount</td>";
					echo "<td colspan=2 textalign=right>".number_format($totDiskon,2)."</td>";
					echo "</tr>";

					echo "<tr>";
					echo "<td colspan=5></td>";
					echo "<td colspan=2>Ppn</td>";
					echo "<td colspan=2 textalign=right>".number_format($totPpn,2)."</td>";
					echo "</tr>";

					echo "<tr>";
					echo "<td colspan=5></td>";
					echo "<td colspan=2>Materai</td>";
					echo "<td colspan=2 textalign=right>".number_format($mod['biayamaterai'],2)."</td>";
					echo "</tr>";

					$totTransaksi=$totNetto+$mod['jumlahuang']-$totBayar;

					echo "<tr>";
					echo "<td colspan=5></td>";
					echo "<td colspan=2>Total Transaksi</td>";
					echo "<td colspan=2 textalign=right>".number_format($totTransaksi,2)."</td>";
					echo "</tr>";

					echo "<tr>";
					echo "<td colspan=5></td>";
					echo "<td colspan=2>Uang Muka</td>";
					echo "<td colspan=2 textalign=right>".number_format($mod['jumlahuang'],2)."</td>";
					echo "</tr>";

					echo "<tr>";
					echo "<td colspan=5></td>";
					echo "<td colspan=2>Bayar</td>";
					echo "<td colspan=2 textalign=right>".number_format($totBayar,2)."</td>";
					echo "</tr>";

					$totSisa=$totTransaksi-$totBayar-$mod['jumlahuang'];

					echo "<tr>";
					echo "<td colspan=5></td>";
					echo "<td colspan=2>Sisa</td>";
					echo "<td colspan=2 text-align=right>".number_format($totTransaksi,2)."</td>";
					echo "</tr>";
				?>
		</tbody>
	</table>
</div>