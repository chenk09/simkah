<?php
/**
* modules/gudangFarmasi/views/laporan/penerimaanPerSupplier/index.php
* Updated by    : Jembar
* Date          : 25-04-2014
* Issue         : EHJ-1080
* Deskripsi     : view Index yang berisi tab laporan
**/
?>
<?php
$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', 
    'stacked'=>false, 
    'items'=>array(
    	
        array('label'=>'Per Obat Alkes', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'tab-default','onclick'=>'setTab(this);', 'tab'=>'gudangFarmasi/laporan/laporanPenerimaanObatAlkesPerObat')),
        array('label'=>'Per Kelompok Obat', 'url'=>'javascript:void(0);', 'itemOptions'=>array('onclick'=>'setTab(this);', 'tab'=>'gudangFarmasi/laporan/laporanPenerimaanObatAlkesPerKelompok')),
        array('label'=>'Per Supplier', 'url'=>'javascript:void(0);', 'itemOptions'=>array('onclick'=>'setTab(this);', 'tab'=>'gudangFarmasi/laporan/laporanPenerimaanObatAlkesPerSupplier')),
        array('label'=>'Per Produsen', 'url'=>'javascript:void(0);', 'itemOptions'=>array('onclick'=>'setTab(this);', 'tab'=>'gudangFarmasi/laporan/laporanPenerimaanObatAlkesPerProdusen')),
        array('label'=>'Detail Faktur', 'url'=>'javascript:void(0);', 'itemOptions'=>array('onclick'=>'setTab(this);', 'tab'=>'gudangFarmasi/laporan/laporanDetailFakturPenerimaanObatAlkes')),
 
    ),
));
?>