<div id="LapPenerimaanOA-m-grid" class="grid-view">
	<table class="table table-striped table-bordered table-condensed">
		<thead>
		    <tr>
		        <th id="LapPenerimaanOA-m-grid_c0" width="10px"> No Penerimaan </th>
		        <th id="LapPenerimaanOA-m-grid_c1"> Tanggal Penerimaan </th>
		        <th id="LapPenerimaanOA-m-grid_c2"> No Faktur </th>
		        <th id="LapPenerimaanOA-m-grid_c3"> Tanggal Faktur </th>
		        <th id="LapPenerimaanOA-m-grid_c4"> Supplier </th>
		        <th id="LapPenerimaanOA-m-grid_c5"> Harga Satuan </th>
		        <th id="LapPenerimaanOA-m-grid_c6"> Qty </th>
		        <th id="LapPenerimaanOA-m-grid_c7"> Satuan Besar </th>
		        <th id="LapPenerimaanOA-m-grid_c8"> Bruto </th>
		        <th id="LapPenerimaanOA-m-grid_c9"> Disc(%) </th>
		        <th id="LapPenerimaanOA-m-grid_c10"> Ppn(%) </th>
		        <th id="LapPenerimaanOA-m-grid_c11"> Total </th>
		    </tr>
		</thead>
		<tbody>
				<?php
					if(count($modPenerimaan) > 0)
					{
						$data = array();
						foreach ($modPenerimaan as $key => $mod){
							$data[$mod->obatalkes_kode] = $mod->attributes;
						}
					}
				?>
		</tbody>
	</table>
</div>