<?php
    /**
    * gudangFarmasi/views/laporan/penerimaanObatAlkes/_search.php
    * Updated by    : Jembar
    * Date          : 22-04-2014
    * Issue         : EHJ-1080
    * Deskripsi     : Penambahan search by Supplier
    **/
?>
<script type="text/javascript">
    function reseting()
    {
        setTimeout(function(){
            $.fn.yiiGridView.update('LapPenerimaanOA-m-grid', {
                    data: $('#LapPenerimaanOA-m-search').serialize()
            });
        },1000);

    }   
</script>


<div class="search-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search-laporan',
        'type'=>'horizontal',
)); ?>
                <legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Penerimaan Items','tglAwal', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    'timeFormat'=>Params::TIME_FORMAT,
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                    </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Sampai Dengan','tglAkhir', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    'timeFormat'=>Params::TIME_FORMAT,
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                    </div>
            </div> 
                <div class="control-group">
                    <?php echo CHtml::label('Kelompok Obat','kelompokobat', array('class'=>'control-label')); ?> 
                    <div class="controls">
                        <?php echo $form->dropDownList($model, 'kelompokobat', JnsKelompok::items(),array('class'=>'span3', 'empty'=>'-- Pilih Kelompok Obat --')); ?>
                    </div>
                </div>
        </td>
    </tr>

</table>
	<div class="form-actions">
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'search-laporan')); ?>
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onclick'=>'resetForm();')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
<?php
// Yii::app()->clientScript->registerScript('search', "
// $('#LapPenerimaanOA-m-search').submit(function(){
//     $.fn.yiiGridView.update('LapPenerimaanOA-m-grid', {
//         data: $(this).serialize()
//     });
//     return false;
// });
// ");
?>