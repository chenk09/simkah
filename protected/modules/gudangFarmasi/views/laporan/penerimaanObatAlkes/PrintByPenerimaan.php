<style>
    th{
        border-top: 1px #000 solid;
        border-bottom: 1px #000 solid;
    }
    tfoot td, .totSup{
        border-top: 1px #000 solid;
        border-bottom: 1px #000 solid;
        text-align: right;
        font-weight: bold;
    }
</style>
<div style="text-align: center;">
    <h2><?php echo $judulLaporan; ?></h2>

    <b>Periode : <?php echo $periode; ?></b><br>
</div>

<table width="100%">
    <thead>
        <th>No.</th>
        <th>No. Penerimaan</th>
        <th>Tanggal</th>
        <th>No. Pembelian</th>
        <th>No. Faktur</th>
        <th>Tanggal</th>
        <th>Jatuh Tempo </th>
        <th>Bruto </th>
        <th>Discount</th>
        <th>PPN</th>
        <th>Materai</th>
        <th>Netto</th>
        <th>Bayar</th>
        <th>Sisa</th>
    </thead>
    <tbody>
<?php
$i = 0;
$totalBruto = 0;
$totalDiskon = 0;
$totalPPN = 0;
$totalMaterai = 0;
$totalNetto = 0;
$totalBayar = 0;
$totalSisa = 0;
$totalBrutoSupplier = 0;
$totalDiskonSupplier = 0;
$totalPPNSupplier = 0;
$totalMateraiSupplier = 0;
$totalNettoSupplier = 0;
$totalBayarSupplier = 0;
$totalSisaSupplier = 0;
foreach($models as $i => $mod){
    $totalBruto += $mod->getTotal('bruto');
    $totalDiskon += $mod->getTotal('diskon');
    $totalPPN += $mod->getTotal('ppn');
    $totalMaterai += $mod->fakturpembelian->biayamaterai;
    $totalNetto += $mod->getTotal('netto');
    $totalBayar += $mod->fakturpembelian->bayarkesupplier->jmldibayarkan;
    $totalSisa += ($mod->getTotal('netto') - $mod->fakturpembelian->bayarkesupplier->jmldibayarkan);
    $totalBrutoSupplier += $mod->getTotal('bruto');
    $totalDiskonSupplier += $mod->getTotal('diskon');
    $totalPPNSupplier += $mod->getTotal('ppn');
    $totalMateraiSupplier += $mod->fakturpembelian->biayamaterai;
    $totalNettoSupplier += $mod->getTotal('netto');
    $totalBayarSupplier += $mod->fakturpembelian->bayarkesupplier->jmldibayarkan;
    $totalSisaSupplier += ($mod->getTotal('netto') - $mod->fakturpembelian->bayarkesupplier->jmldibayarkan);
    
    //tampilkan keterangan supplier
    if($i == 0 | $models[$i-1]->supplier_id != $models[$i]->supplier_id){
        $tr .= "<tr><td colspan=2>Nama Supplier </td><td colspan=12>: ".$mod->supplier->supplier_nama."</td></tr>";
        $tr .= "<tr><td colspan=2>Alamat Supplier </td><td colspan=12>: ".((!empty($mod->supplier->supplier_alamat)) ? $mod->supplier->supplier_alamat : "-")."</td></tr>";
    }
    $tr .= "<tr>";
    $tr .= "<td style='text-align:center;'>".($i+1)."</td>";
    $tr .= "<td>".$mod->noterima."</td>";
    $tr .= "<td>".$mod->tglterima."</td>";
    $tr .= "<td>".(empty($mod->penerimaanbarang->permintaanpembelian->nopembelian) ? "-":$mod->permintaanpembelian->nopembelian)."</td>";
    $tr .= "<td>".(empty($mod->fakturpembelian->nofaktur) ? "-":$mod->fakturpembelian->nofaktur)."</td>";
    $tr .= "<td>".(empty($mod->fakturpembelian->tglfaktur) ? "-" : date('d-m-Y H:i:s',  strtotime($mod->fakturpembelian->tglfaktur)))."</td>";
    $tr .= "<td>".(empty($mod->fakturpembelian->tgljatuhtempo) ? "-" : date('d-m-Y H:i:s',  strtotime($mod->fakturpembelian->tgljatuhtempo)))."</td>";
    $tr .= "<td style='text-align:right;'>".MyFunction::formatNumber($mod->getTotal('bruto'))."</td>";
    $tr .= "<td style='text-align:right;'>".MyFunction::formatNumber($mod->getTotal('diskon'))."</td>";
    $tr .= "<td style='text-align:right;'>".MyFunction::formatNumber($mod->getTotal('ppn'),2,'.',',')."</td>";
    $tr .= "<td style='text-align:right;'>".MyFunction::formatNumber($mod->fakturpembelian->biayamaterai)."</td>";
    $tr .= "<td style='text-align:right;'>".MyFunction::formatNumber($mod->getTotal('netto'))."</td>";
    $tr .= "<td style='text-align:right;'>".MyFunction::formatNumber($mod->fakturpembelian->bayarkesupplier->jmldibayarkan)."</td>";
    $tr .= "<td style='text-align:right;'>".MyFunction::formatNumber($mod->getTotal('netto') - $mod->fakturpembelian->bayarkesupplier->jmldibayarkan)."</td>";
    $tr .= "</tr>";
    //tampilkan total per supplier
    if($models[$i+1]->supplier_id != $models[$i]->supplier_id){
        $tr .= "<tr class='totSup'>";
        $tr .= "<td colspan=7 style='text-align: right;'>Total Supplier ".$mod->supplier->supplier_nama." :</td>";
        $tr .= "<td class='totSup'>".MyFunction::formatNumber($totalBrutoSupplier)."</td>";
        $tr .= "<td class='totSup'>".MyFunction::formatNumber($totalDiskonSupplier)."</td>";
        $tr .= "<td class='totSup'>".MyFunction::formatNumber($totalPPNSupplier)."</td>";
        $tr .= "<td class='totSup'>".MyFunction::formatNumber($totalMateraiSupplier)."</td>";
        $tr .= "<td class='totSup'>".MyFunction::formatNumber($totalNettoSupplier)."</td>";
        $tr .= "<td class='totSup'>".MyFunction::formatNumber($totalBayarSupplier)."</td>";
        $tr .= "<td class='totSup'>".MyFunction::formatNumber($totalSisaSupplier)."</td>";
        $tr .= "</tr>";
        //reset total supplier
        $totalBrutoSupplier = 0;
        $totalDiskonSupplier = 0;
        $totalPPNSupplier = 0;
        $totalMateraiSupplier = 0;
        $totalNettoSupplier = 0;
        $totalBayarSupplier = 0;
        $totalSisaSupplier = 0;
    }
}
echo $tr;
?>
        <tr><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td></tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="7">Grand Total:</td>
            <td><?php echo MyFunction::formatNumber($totalBruto) ?></td>
            <td><?php echo MyFunction::formatNumber($totalDiskon) ?></td>
            <td><?php echo MyFunction::formatNumber($totalPPN) ?></td>
            <td><?php echo MyFunction::formatNumber($totalMaterai) ?></td>
            <td><?php echo MyFunction::formatNumber($totalNetto) ?></td>
            <td><?php echo MyFunction::formatNumber($totalBayar) ?></td>
            <td><?php echo MyFunction::formatNumber($totalSisa) ?></td>
        </tr>
    </tfoot>
</table>
<?php 
if(isset($_GET['caraPrint']))
    $this->renderPartial('mutasiIntern/_tandatangan', array('model'=>$model, 'caraPrint'=>$caraPrint)); 
?>