<?php
$url = Yii::app()->createUrl('gudangFarmasi/laporan/FrameGrafikLaporanpenerimaanObatAlkes&id=1');
Yii::app()->clientScript->registerScript('search', "
$('#search-laporan').submit(function(){
	$('#LapPenerimaanOA-m-grid').addClass('srbacLoading');
	$.fn.yiiGridView.update('LapPenerimaanOA-m-grid', {
		data: $(this).serialize()
	});
	
	return false;
});
");
?>

<?php $this->renderPartial('penerimaanObatAlkes/penerimaanPerSupplier/_search',array('model'=>$model)); ?>
<fieldset>
    <?php $this->renderPartial('penerimaanObatAlkes/penerimaanPerSupplier/_table',array('modPenerimaan'=>$modPenerimaan)); ?>
    <?php // $this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0' onload="javascript:resizeIframe(this);"></iframe>
</fieldset>
<?php        
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintLaporanPenerimaanObatAlkes');
$this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>
