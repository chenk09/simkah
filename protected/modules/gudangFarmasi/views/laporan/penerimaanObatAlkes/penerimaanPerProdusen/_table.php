<?php
/**
* modules/farmasiApotek/views/laporan/PenerimaanObatAlkes/penerimaanPerProdusen/_table.php
* Updated by    : Jembar
* Date          : 28-04-2014
* Issue         : EHJ-1702
* Deskripsi     : Table
**/
?>
<div id="LapPenerimaanOA-m-grid" class="grid-view">
	<table class="table table-striped table-bordered table-condensed">
		<thead>
		    <tr>
		        <th id="LapPenerimaanOA-m-grid_c0" width="10px"> No Penerimaan </th>
		        <th id="LapPenerimaanOA-m-grid_c1"> Tanggal Penerimaan </th>
		        <th id="LapPenerimaanOA-m-grid_c2"> No Faktur </th>
		        <th id="LapPenerimaanOA-m-grid_c3"> Tanggal Faktur </th>
		        <th id="LapPenerimaanOA-m-grid_c4"> Kode </th>
		        <th id="LapPenerimaanOA-m-grid_c5"> Obat Alkes </th>
		        <th id="LapPenerimaanOA-m-grid_c6"> Harga Satuan </th>
		        <th id="LapPenerimaanOA-m-grid_c7"> Qty </th>
		        <th id="LapPenerimaanOA-m-grid_c8"> Satuan Besar </th>
		        <!--<th id="LapPenerimaanOA-m-grid_c9"> Bruto </th>
		        <th id="LapPenerimaanOA-m-grid_c10"> Disc(%) </th>
		        <th id="LapPenerimaanOA-m-grid_c11"> Ppn(%) </th>
		        <th id="LapPenerimaanOA-m-grid_c12"> Total </th>-->
		    </tr>
		</thead>
		<tbody>
				<?php 
					$i=1;
					$namaProdusen[-1] = '';
					$jml_row = count($modPenerimaan);

					$totBruto = 0;
					$totDiskon = 0;
					$totPpn = 0;
					$totNetto = 0;
					if($jml_row == 0){
						echo "<tr><td colspan='13'>Data Tidak ditemukan</td></tr>";
					}
					foreach ($modPenerimaan as $key => $mod) {
						$namaProdusen[$key] 		= $mod['produsen_nama'];
						

						if($namaProdusen[$key]!=$namaProdusen[$key-1])
						{
							if($key>0){
								echo "<tr>";
								echo "<td colspan=9><b>Total Produsen :".$namaProdusen[$key-1]."</b></td>";
								//echo "<td>".number_format($totBruto,2)."</td>";
								//echo "<td>".number_format($totDiskon,2)."</td>";
								//echo "<td>".number_format($totPpn,2)."</td>";
								//echo "<td>".number_format($totNetto,2)."</td>";
								echo "</tr>";
								$totBruto = 0;
								$totDiskon = 0;
								$totPpn = 0;
								$totNetto = 0;


								echo "<tr>";
								echo "<td colspan=13></td>";
								echo "</tr>";
							}		
							echo "<tr>";
							echo "<td colspan=13><b>Nama Produsen : ".$mod['produsen_nama']."</b></td>";
							echo "</tr>";

						}


						echo "<tr>";
						echo "<td>".$mod['noterima']."</td>";
						echo "<td>".date('d M Y H:i:s',strtotime($mod['tglterima']))."</td>";
						echo "<td>".$mod['nofaktur']."</td>";
						echo "<td>".date('d M Y H:i:s',strtotime($mod['tglfaktur']))."</td>";
						echo "<td>".$mod['obatalkes_kode']."</td>";
						echo "<td>".$mod['namaobat']."</td>";
						echo "<td>".number_format($mod['hargabelisatuan'],2)."</td>";
						echo "<td>".number_format($mod['qty'],2)."</td>";
						echo "<td>".$mod['satuanbesar_nama']."</td>";
						//echo "<td>".number_format($mod['bruto'],2)."</td>";
						//echo "<td>".number_format($mod['persendiscount'],2)."</td>";
						//echo "<td>".number_format($mod['persenppn'],2)."</td>";
						//echo "<td>".number_format($mod['netto'],2)."</td>";
						echo "</tr>";
						
						/*
						$totBruto += $mod['bruto'];
						$totDiskon += $mod['nominaldiscount'];  
						$totPpn += $mod['nominalppn']; 
						$totNetto += $mod['netto'];
						if($i==$jml_row){
							if($key>0){
								echo "<tr>";
								echo "<td colspan=9><b>Total Produsen :".$namaProdusen[$key]."</b></td>";
								echo "<td>".number_format($totBruto,2)."</td>";
								echo "<td>".number_format($totDiskon,2)."</td>";
								echo "<td>".number_format($totPpn,2)."</td>";
								echo "<td>".number_format($totNetto,2)."</td>";
								echo "</tr>";

								echo "<tr>";
								echo "<td colspan=13></td>";
								echo "</tr>";
							}elseif($key==0&&$i==$jml_row){
								echo "<tr>";
								echo "<td colspan=9><b>Total Produsen :".$namaProdusen[$key]."</b></td>";
								echo "<td>".number_format($totBruto,2)."</td>";
								echo "<td>".number_format($totDiskon,2)."</td>";
								echo "<td>".number_format($totPpn,2)."</td>";
								echo "<td>".number_format($totNetto,2)."</td>";
								echo "</tr>";

								echo "<tr>";
								echo "<td colspan=13></td>";
								echo "</tr>";
							}
						}
						*/
						$i++;
					}
				?>
		</tbody>
	</table>
</div>