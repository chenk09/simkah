<div class="search-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search-laporan',
        'type'=>'horizontal',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                    <?php echo CHtml::label('Tgl Penerimaan Items','tglAwal', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Sampai Dengan','tglAkhir', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 
                                                                         'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div> 
        </td>
        <td>
              <?php echo $form->textFieldRow($model,'noterima',array('class'=>'numberOnly')); ?>
              <?php echo $form->dropDownListRow($model,'supplier_id',
                           CHtml::listData($model->SupplierItems, 'supplier_id', 'supplier_nama'),
                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                'empty'=>'-- Pilih --',)); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo $form->dropDownListRow($model,'sumberdana_id',
                           CHtml::listData(SumberdanaM::model()->findAll(), 'sumberdana_id', 'sumberdana_nama'),
                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                'empty'=>'-- Pilih --',)); ?>
        </td>
        <td>
            <?php echo $form->dropDownListRow($model,'jenisobatalkes_id',
                           CHtml::listData(JenisobatalkesM::model()->findAll(), 'jenisobatalkes_id', 'jenisobatalkes_nama'),
                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                'empty'=>'-- Pilih --',)); ?>
        </td>
    </tr>
</table>
	<div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                    array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                    array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>