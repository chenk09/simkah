<?php
    echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array('judulLaporan'=>$judulLaporan, 
        'periode'=>$periode, 'colspan'=>10));  
?>
<div style='border:1px solid #cccccc; border-radius:2px;padding:10px; width: 15%;float:right;margin-right:30px;margin-top:-80px;text-align:center;'>
                <font style='font-size:9pt;text-align:center;'><B>PURCHASE RECEIVED REPORT</B></font><br></div>
<?php
    $criteria2 = new CDbCriteria();
    $format2 = new CustomFormat();
    
    $criteria2->select ='t.*';
    if(isset($_GET['GFPenerimaanBarangT']['tglAwal'])){
        $tglAwal = $format2->formatDateTimeMediumForDB($_GET['GFPenerimaanBarangT']['tglAwal']);
    }
    if(isset($_GET['GFPenerimaanBarangT']['tglAkhir'])){
        $tglAkhir = $format2->formatDateTimeMediumForDB($_GET['GFPenerimaanBarangT']['tglAkhir']);
    }
    $criteria2->addBetweenCondition('t.tglterima',$tglAwal,$tglAkhir);
    $model = GFPenerimaanBarangT::model()->findAll($criteria2);
    $noterima_temp = "";
    foreach($model as $key=>$models){
         if($models->noterima){
                $noterima = $models->noterima;
                $tglterima = $models->tglterima;
                $tglfaktur = $models->fakturpembelian->tglfaktur;
                $nofaktur = $models->fakturpembelian->nofaktur;
                $supplier = $models->supplier->supplier_nama;
                $penerimaanId = $models->penerimaanbarang_id;
            } else{
                $noterima = '';
            }           
            if($noterima_temp != $noterima)
            {
            echo "                
                <table width='100%' border='0' style='font-size:small;border-top:1px solid black;border-bottom:1px solid black'>
                    <tr>
                        <td> No. Terima </td>
                        <td> Tanggal </td>
                        <td> No. Faktur</td>
                        <td> Tanggal Faktur </td>
                        <td> Supplier</td>
                    </tr>
                    <tr style='border-top:1px solid black;background-color:#C0C0C0;'>
                        <td> $noterima </td>
                        <td> $tglterima </td>
                        <td> $nofaktur </td>
                        <td> $tglfaktur </td>
                        <td> $supplier </td>
                    </tr>
                    </table></br>";
                echo "<table width='100%' border='1'>
                            <tr style='font-weight:bold;'>
                                <td align='center'>Kode</td>
                                <td align='center'>Barang</td>
                                <td align='center'>Satuan</td>
                                <td align='center'>Qty</td>
                                <td align='center'>Harga</td>
                                <td align='center'>Bruto</td>
                                <td align='center'>Disc (%)</td>
                                <td align='center'>Ppn (%)</td>
                                <td align='center'>HPP</td>
                                <td align='center'>Tot HPP / Netto</td>
                            </tr>";
            
            $format = new CustomFormat();
//            echo $tglAwal;
//            echo $tglAkhir;
//            exit;
            $criteria = new CDbCriteria;
             if(isset($_GET['GFPenerimaanBarangT']['tglAwal'])){
                $tglAwal = $format2->formatDateTimeMediumForDB($_GET['GFPenerimaanBarangT']['tglAwal']);
            }
            if(isset($_GET['GFPenerimaanBarangT']['tglAkhir'])){
                $tglAkhir = $format2->formatDateTimeMediumForDB($_GET['GFPenerimaanBarangT']['tglAkhir']);
            }
            $criteria->addBetweenCondition('pb.tglterima',$tglAwal,$tglAkhir);
            $term = $noterima;
            $condition  = "pb.noterima ILIKE '%".$term."%' OR pb.noterima ILIKE '%".$term."%'";
                    $criteria->select = 'pb.noterima, pb.tglterima, sum(pb.totalharga) as totalharga, pb.penerimaanbarang_id,pb.tglterima,
                        fp.nofaktur as nofaktur, fp.tglfaktur as tglfaktur,o.sumberdana_id,
                        sum(t.hargabelibesar) as hargabelibesar, 
                        sum(t.jmlterima) as jumlahterima, 
                        sum(o.ppn_persen) as hargappn, sum(o.discount) as persendiscount, sd.sumberdana_nama,t.sumberdana_id,o.jenisobatalkes_id,
                        o.obatalkes_kode as obatalkes_kode,o.obatalkes_nama as nama_obat, 
                        sb.satuanbesar_nama as satuanbesar_nama, 
                        s.supplier_nama as supplier_nama,
                        sum(((t.hargabelibesar  * t.jmlterima) - ((t.hargabelibesar * t.jmlterima) * (o.discount / 100))) + ((t.hargabelibesar * t.jmlterima)*(o.ppn_persen / 100))) as total_harga,
                        sum(((t.hargabelibesar) - ((t.hargabelibesar) * (o.discount / 100))) + ((t.hargabelibesar)*(o.ppn_persen / 100))) as total_hpp,
                        sum(t.hargabelibesar * t.jmlterima) as hargabruto
                        ';
                    $criteria->join = '
                        JOIN penerimaanbarang_t pb ON pb.penerimaanbarang_id=t.penerimaanbarang_id 
                        JOIN supplier_m s ON s.supplier_id=pb.supplier_id 
                        LEFT JOIN satuanbesar_m sb ON sb.satuanbesar_id=t.satuanbesar_id 
                        LEFT JOIN fakturpembelian_t fp ON fp.fakturpembelian_id=pb.fakturpembelian_id 
                        LEFT JOIN obatalkes_m o ON t.obatalkes_id=o.obatalkes_id
                        LEFT JOIN sumberdana_m sd ON t.sumberdana_id=sd.sumberdana_id
                        LEFT JOIN jenisobatalkes_m js ON o.jenisobatalkes_id=js.jenisobatalkes_id';
                    $criteria->group = 't.sumberdana_id, o.jenisobatalkes_id, pb.noterima,pb.tglterima,fp.nofaktur,fp.tglfaktur,
                                        sb.satuanbesar_nama, o.obatalkes_kode,o.obatalkes_nama,
                                        s.supplier_nama,sd.sumberdana_nama,o.sumberdana_id,pb.penerimaanbarang_id,pb.tglterima
                                        ';
                    $criteria->compare('pb.supplier_id',$_GET['GFPenerimaanBarangT']['supplier_id']);
                    $criteria->compare('LOWER(pb.noterima)',$_GET['GFPenerimaanBarangT']['noterima'],true);
                    $criteria->compare('o.sumberdana_id',$_GET['GFPenerimaanBarangT']['sumberdana_id']);
//                    $criteria->compare('o.jenisobatalkes_id',$_GET['GFPenerimaanBarangT']['jenisobatalkes_id']);
                    $criteria->compare('pb.create_ruangan',Yii::app()->user->getState('ruangan_id'));
                    $criteria->addCondition("s.supplier_jenis='Farmasi'");
                    $criteria->addCondition($condition);
            
            $totQty = 0;
            $totHarga = 0;
            $totBruto = 0;
            $totDiscount = 0;
            $totPpn = 0;
            $totHpp = 0;
            $totHppNetto = 0;
            $detail = PenerimaandetailT::model()->findAll($criteria);
            foreach($detail as $key=>$details){
                    $qty = $details->jumlahterima;
                    $harga = $details->hargabelibesar;
                    $bruto = $details->hargabruto;
                    $discount = $details->persendiscount;
                    $ppn = $details->hargappn;
                    $hpp = $details->total_hpp;
                    $hppNetto = $details->total_harga;
                    
                    $totQty += $qty;
                    $totHarga += $harga;
                    $totBruto += $bruto;
                    $totDiscount += $discount;
                    $totPpn += $ppn;
                    $totHpp += $hpp;
                    $totHppNetto += $hppNetto;
                    
                    echo "<tr>
                              <td width='150px;'>".$details->obatalkes_kode."</td>
                              <td width='280px;'>".$details->nama_obat."</td>
                              <td width='220px;' style='text-align:center'>".$details->satuanbesar_nama."</td>
                              <td width='70px;' style='text-align:center'>".$qty."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($harga)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($bruto)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($discount)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($ppn)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($hpp)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($hppNetto)."</td>
                          </tr>";
            }
            
                    echo "<tr style='background-color:#ffffff;'>
                                <td colspan=3 style='text-align:right;font-weight:bold;'>Total : </td>
                                <td width='150px;' style='text-align:center'>".MyFunction::formatNumber($totQty)."</td>
                                <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totHarga)."</td>
                                <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totBruto)."</td>
                                <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totDiscount)."</td>
                                <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totPpn)."</td>
                                <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totHpp)."</td>
                                <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totHppNetto)."</td>
                          </tr>";
                     echo "</table><br/>";
            }
            
            $noterima_temp = $noterima;
    }
?>