<?php
    echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array('judulLaporan'=>$judulLaporan, 
        'periode'=>$periode, 'colspan'=>10));  
?>
<div style='border:1px solid #cccccc; border-radius:2px;padding:10px;float:right;margin-right:30px;margin-top:-100px;text-align:center;'>
                <font style='font-size:9pt;text-align:center;'><B>PURCHASE ORDER REPORT</B></font><br></div><br/>
<?php
    $model = GFPermintaanPembelianT::model()->findAll();
    $supplier_temp = "";
    foreach($model as $key=>$models){
         if($models->supplier_id){
                $supplier = $models->supplier->supplier_nama;
                $alamat = $models->supplier->supplier_alamat;
                $nopermintaan = $models->nopermintaan;
            } else{
                $supplier = '';
            }           
            if($supplier_temp != $supplier)
            {
            echo "                
                <table width='100%' border='0' style='font-size:small;'>
                <div style='border:1px solid #cccccc; border-radius:2px;padding:10px;float:left;margin-left:3px;margin-top:-20px;'>
                <font style='font-size:9pt'><B>$models->nopermintaan</B></font><br></div>
                <div style='border:1px solid #cccccc; border-radius:2px;padding:10px;float:left;margin-left:40px;margin-top:-20px;'>
                <font style='font-size:9pt'><B>".$models->tglpermintaanpembelian."</B></font><br></div>
                    <tr>
                        <td colspan=7 style='font-size:9pt;'><b> Nama Supplier: $supplier </b></td>
                    </tr>
                    <tr>
                        <td colspan=7 style='font-size:9pt;'><b> Alamat Supplier: $alamat </b></td>
                    </tr>
                    <tr><td>";
                echo "<table width='100%' border='1'>
                            <tr style='font-weight:bold;background-color:#C0C0C0'>
                                <td align='center'>Kode</td>
                                <td align='center'>Nama</td>
                                <td align='center'>Satuan</td>
                                <td align='center'>Qty</td>
                                <td align='center'>Harga</td>
                                <td align='center'>Bruto</td>
                                <td align='center'>Disc %</td>
                                <td align='center'>Ppn %</td>
                                <td align='center'>Netto %</td>
                                <td align='center'>Keterangan %</td>
                            </tr>";
            
            $format = new CustomFormat();
            $criteria = new CDbCriteria;
            $term = $supplier;
            $condition  = "supplier_m.supplier_nama ILIKE '%".$term."%' OR supplier_nama ILIKE '%".$term."%'";
            $criteria->select = 't.supplier_id, t.permintaanpembelian_id, supplier_m.supplier_nama, 
                                 supplier_m.supplier_alamat, t.nopermintaan, t.tglpermintaanpembelian,
                                 obatalkes_m.obatalkes_kode, obatalkes_m.obatalkes_nama,
                                 satuankecil_m.satuankecil_nama, satuanbesar_m.satuanbesar_nama,
                                 sum(permintaandetail_t.jmlkemasan) as jmlkemasan,
                                 sum(obatalkes_m.harganetto) as total_harganetto,
                                 sum(permintaandetail_t.harganettoper) as harganetto,
                                 sum(permintaandetail_t.hargappnper) as jmlppn,
                                 sum(permintaandetail_t.jmldiscount) as jmldiscount,
                                 sum(permintaandetail_t.hargabelibesar) as hargabelibesar
                                 
                                 ';
                $criteria->group = 't.supplier_id, t.permintaanpembelian_id, supplier_m.supplier_nama, 
                                    supplier_m.supplier_alamat, t.nopermintaan, t.tglpermintaanpembelian,
                                    obatalkes_m.obatalkes_kode,satuankecil_m.satuankecil_nama,satuanbesar_m.satuanbesar_nama,obatalkes_m.obatalkes_nama
                                    ';
//		$criteria->addBetweenCondition('t.tglpermintaanpembelian',$format->formatDateTimeMediumForDB($_GET['GFPermintaanPembelianT']['tglAwal']),$format->formatDateTimeMediumForDB($_GET['GFPermintaanPembelianT']['tglAkhir']));
//		$criteria->compare('LOWER(t.nopermintaan)',strtolower($this->nopermintaan),true);
                $criteria->compare('t.supplier_id', $_GET['GFPermintaanPembelianT']['supplier_id']);
                $criteria->addCondition($condition);
                $criteria->addCondition('t.penerimaanbarang_id is null');
                $criteria->addCondition("supplier_m.supplier_jenis='Farmasi'");
                $criteria->join = 'LEFT JOIN supplier_m ON t.supplier_id = supplier_m.supplier_id
                                   LEFT JOIN permintaandetail_t ON t.permintaanpembelian_id = permintaandetail_t.permintaanpembelian_id
                                   LEFT JOIN obatalkes_m ON permintaandetail_t.obatalkes_id = obatalkes_m.obatalkes_id
                                   LEFT JOIN satuankecil_m ON permintaandetail_t.satuankecil_id = satuankecil_m.satuankecil_id
                                   LEFT JOIN satuanbesar_m ON permintaandetail_t.satuanbesar_id = satuanbesar_m.satuanbesar_id
                                    ';
            $criteria->compare('t.ruangan_id',Yii::app()->user->getState('ruangan_id'));
            $criteria->limit = -1;
            
            $totNetto = 0;
            $totBruto = 0;
            $totPpn = 0;
            $totDiscount = 0;
            $detail = PermintaanpembelianT::model()->findAll($criteria);
            foreach($detail as $key=>$details){
                    $harga = $details->harganetto;
                    $bruto = $details->hargabelibesar;
                    $discount = $details->jmldiscount;
                    $ppn = $details->jmlppn;
                    $netto = (($details->hargabelibesar + $details->jmlppn) - $details->jmldiscount);
                    
                    
                    $totNetto += $netto;
                    $totDiscount += $discount;
                    $totPpn += $ppn;
                    $totBruto += $details->hargabelibesar;
                    echo "<tr>
                              <td width='150px;'>".$details->obatalkes_kode."</td>
                              <td width='280px;'>".$details->obatalkes_nama."</td>
                              <td width='220px;'>".$details->satuanbesar_nama."</td>
                              <td width='70px;' style='text-align:center'>".$details->jmlkemasan."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($harga)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($bruto)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($discount)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($ppn)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($netto)."</td>
                              <td width='70px;' style='text-align:right'>-</td>
                          </tr>";
            }
            
                    echo "<tfoot style='background-color:#ffffff;'>
                            <div>
                                <table align=right>
                                    <tr>
                                        <td colspan=9 style='text-align:right'>Total : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totNetto)."</td>
                                    </tr>
                                    <tr>
                                        <td colspan=9 style='text-align:right'>Discount : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totDiscount)."</td>
                                    </tr>
                                    <tr>
                                        <td colspan=9 style='text-align:right'>Ppn : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totPpn)."</td>
                                    </tr>
                                    <tr>
                                        <td colspan=9 style='text-align:right'>Total Transaksi : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totBruto)."</td>
                                    </tr>
                                </table>
                            </div>
                            <div style='border:0px solid #cccccc;padding:10px; width: 10%;float:right;margin-top:5px;margin-right:60px;'>
                                    <font style='font-size:9pt'><B><CENTER>Purchasing<CENTER></B><br><br><br/>
                                    <font style='font-size:9pt'><B><CENTER>MELI<CENTER></B><hr style='height:3px;background:#000000;margin-top:-2px;' />
</div>
                            </div>
                          </tfoot>";
                     echo "</table><br/>";
            }
            
            $supplier_temp = $supplier;
    }
      echo "</td></tr></table>";
?>