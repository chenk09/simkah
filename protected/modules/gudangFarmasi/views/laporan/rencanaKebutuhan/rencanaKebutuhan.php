<legend class="rim2"> Laporan Rencana Kebutuhan</legend>
<legend class="rim">Pencarian</legend>
<?php
    $url = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/FrameRencanaKebutuhan&id=1');
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('#search-laporan').submit(function(){
            $.fn.yiiGridView.update('laporan-grid', {
                    data: $(this).serialize()
            });
            return false;
    });
    ");
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
                'id'=>'search-laporan',
)); ?>
    <table>
        <tr>
            <td>
                    <div class="control-group">
                       <?php echo CHtml::label('Periode','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                            ?>
                        </div>
                    </div>
            </td>
            <td>
                    <div class="control-group">
                       <?php echo CHtml::label('Sampai Dengan','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                            ?>
                        </div>
                    </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="control-group">
                       <?php echo CHtml::label('No Perencanaan','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
                                   echo $form->textField($model,'noperencnaan',array('class'=>'span3'));
                            ?>
                        </div>
                    </div>
            </td>
        </tr>
    </table>
    <div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'submit')); ?>

                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                            Yii::app()->createUrl($this->module->id.'/'.laporan.'/laporanrencanaKebutuhan'), 
                            array('class'=>'btn btn-danger',
                                  'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    </div>
<?php
$this->endWidget();
?>

<legend class="rim">Tabel Rencana Kebutuhan</legend>
<?php $this->renderPartial('rencanaKebutuhan/_tableRencana',array('model'=>$model)); ?>
<?php $this->renderPartial('_tab'); ?>
<iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
</iframe>  
<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintRencanaKebutuhan');
$this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>