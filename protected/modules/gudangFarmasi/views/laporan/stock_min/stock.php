<?php
$urlPrint = Yii::app()->createUrl('gudangFarmasi/Laporan/cetakLaporanStockMinimal');
Yii::app()->clientScript->registerScript('search', "
$('.cetak').click(function(){
	var caraPrint = $(this).attr('jenis_cetak');
	window.open('${urlPrint}&'+ $('#searchLaporan').serialize() +'&caraPrint='+caraPrint,'','location=_new, width=900px, scrollbars=yes');
    return false;
});
$('#search-laporan').submit(function(){
	$('#laporan-grid').addClass('srbacLoading');
	$.fn.yiiGridView.update('laporan-grid', {
			data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2"> Laporan Stock Minimal</legend>
<fieldset>
	<legend class="rim">Pencarian</legend>
	<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
		'type'=>'horizontal',
		'id'=>'search-laporan',
	));?>
	<?php echo $form->textFieldRow($model, 'obatalkes_nama',array('class'=>'span3')); ?>
	<?php echo $form->textFieldRow($model, 'obatalkes_kode',array('class'=>'span3')); ?>
	<?php echo $form->dropDownListRow($model, 'ruangan_id', CHtml::listData(RuanganM::model()->findAll("instalasi_id IN(2,4,5,6,9) AND ruangan_aktif = true ORDER BY instalasi_id, ruangan_nama"),'ruangan_id', 'ruangan_nama'),array(
		'empty'=>'-- Pilih --',
		'class'=>'span3'
	)); ?>
    <div class="form-actions">
		<div class="row">
			<div class="pull-left" style="margin-right:5px;">
				<?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), 
					array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
				?>
				<?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Reset', array('{icon}' => '<i class="icon-refresh icon-white"></i>')), 
					array('class' => 'btn btn-danger', 'type' => 'reset', 'id' => 'btn_simpan'));
				?>
			</div>
			<div class="pull-left">
				<?php $this->widget('bootstrap.widgets.BootButtonGroup', array(
					'type'=>'info',
					'buttons'=>array(
						array(
							'label'=>'Print',
							'type'=>'success',
							'icon'=>'icon-print icon-white',
							'url'=>'javascript:void(0)',
							'htmlOptions'=>array('class'=>'cetak', 'jenis_cetak'=>'print')
						),
						array('label'=>'Export','icon'=>'icon-download icon-white', 'items'=>array(
							array('label'=>'PDF', 'icon'=>'icon-pdf', 'url'=>'javascript:void(0)', 'itemOptions'=>array('class'=>'cetak', 'jenis_cetak'=>'pdf')),
							array('label'=>'Excel','icon'=>'icon-book', 'url'=>'javascript:void(0)', 'itemOptions'=>array('class'=>'cetak', 'jenis_cetak'=>'excel')),
						)),
					),
				)); ?>
			</div>
		</div>
    </div>	
	<?php $this->endWidget();?>	
</fieldset>
<fieldset>
	<legend class="rim">Daftar Obat</legend>
	<?php $this->renderPartial('stock_min/_table', array('model'=>$model));?>
</fieldset>