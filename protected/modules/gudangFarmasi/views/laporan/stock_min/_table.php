<?php
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$template = "{pager}{summary}\n{items}";

if(isset($caraPrint)){
	$data = $model->searchStockPrint();
	$template = "{items}";
	
	if($caraPrint=='excel')
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="laporan_stock_min-'.date("Y/m/d").'.xls"');
		header('Cache-Control: max-age=0');
		$table = 'ext.bootstrap.widgets.BootExcelGridView';
	}
}else{
	$data = $model->searchStock();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
	'itemsCssClass'=>'table table-bordered table-striped table-condensed',
	'template'=>$template,
	'mergeHeaders'=>array(
		array(
			'name'=>'<center>Qty</center>',
			'start'=>4,
			'end'=>6,
		),
	),
	'columns'=>array(
		array(
			'header'=>'No',
			'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
			'footerHtmlOptions'=>array('colspan'=>4, 'style'=>'text-align:right;'),
			'type'=>'raw',
			 'htmlOptions'=>array(
				'style'=>'text-align:right;padding-right:10%;'
			),
		),
		"obatalkes_kode",
		"obatalkes_nama",
		"jenisobatalkes_nama",
		"minimalstok",
		"qtystok_in",
		"qtystok_out",
		"qtystok_current",
		"ruangan_nama",
		"tglkadaluarsa"
	),
)); ?>	