<?php
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->search();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
        'itemsCssClass'=>'table table-bordered table-striped table-condensed',
        'template'=>$template,
       'mergeHeaders'=>array(
            array(
                'name'=>'<center>Qty</center>',
                'start'=>4, //indeks kolom 3
                'end'=>6, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                    array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
//                        'footer'=>'<b>Total:</b>',
                        'footerHtmlOptions'=>array('colspan'=>4, 'style'=>'text-align:right;'),
                        'type'=>'raw',
                         'htmlOptions'=>array(
                            'style'=>'text-align:right;padding-right:10%;'
                        ),
                    ),
                    array(
                        'header'=>'Kode Obat Alkes',
                        'value'=>'$data->obatalkes_kode',
                    ),
                    array(
                        'header'=>'Nama Obat Alkes',
                        'value'=>'$data->obatalkes_nama',
                    ),
                    array(
                        'header'=>'Jenis Obat',
                        'value'=>'$data->jenisobatalkes_nama',
                    ),
                    array(
                        'name'=>'qty_in',
                        'type'=>'raw',
                        'header'=>'<center>Qty In</center>',
                        'value'=>'$data->qty_in',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
//                        'footer'=>'sum(qty_in)',
                        
//                        'footer'=>MyFunction::formatNumber($model->totalqtystok_in),
                    ),
                    array(
                        'name'=>'qty_out',
                        'type'=>'raw',
                        'header'=>'<center>Qty Out</center>',
                        'value'=>'$data->qty_out',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
//                        'footer'=>'sum(qty_out)',
                        
//                        'footer'=>MyFunction::formatNumber($model->totalqtystok_in),
                    ),
                    array(
                        'name'=>'qty_current',
                        'type'=>'raw',
                        'header'=>'<center>Qty Current</center>',
                        'value'=>'$data->qty_current',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;width:100px;'),
//                        'footer'=>'sum(qty_current)',
                        
//                        'footer'=>MyFunction::formatNumber($model->totalqtystok_in),
                    ),
//                    array(
//                        'header'=>'Harga Jual',
//                        'value'=>'MyFunction::formatNumber($data->hargajual_oa)',
//                        'footer'=>MyFunction::formatNumber($model->totalhargajual),
//                    ),
//                    array(
////                        'header'=>'Total Harga',
////                        'value'=>'MyFunction::formatNumber($data->hargajual_oa * $data->qty)',
////                        'footer'=>MyFunction::formatNumber($model->totalharga),
//                        'name'=>'totalharga',
//                        'type'=>'raw',
//                        'header'=>'Total Harga',
//                        'value'=>'MyFunction::formatNumber($data->hargajual_oa * $data->qty)',
//                        'htmlOptions'=>array('style'=>'text-align:right;'),
//                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
//                        'footer'=>'sum(totalharga)',
//                    ),
	),
)); ?>