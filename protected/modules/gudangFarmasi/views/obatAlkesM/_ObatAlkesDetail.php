
<fieldset class="">
    <legend class="accord1">
        <?php echo CHtml::checkBox('pakeDetailObat',true , array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Obat Alkes Detail
    </legend>
    <div id="divObatalkesdetail" class="toggle">
        <table>
            <tr>
                <td>
                    <div class="control-label">Indikasi</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('name'=>'indikasi','model'=>$modObatAlkesDetail,'attribute'=>'indikasi','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                </td>
                <td>
                    <div class="control-label">Kontraindikasi</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('name'=>'kontraindikasi','model'=>$modObatAlkesDetail,'attribute'=>'kontraindikasi','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                </td>
            </tr>
            
            <tr>
                <td>
                    <div class="control-label">Komposisi</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('name'=>'komposisi','model'=>$modObatAlkesDetail,'attribute'=>'komposisi','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                </td>
                <td>
                    <div class="control-label">Efek Samping</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('name'=>'efeksamping','model'=>$modObatAlkesDetail,'attribute'=>'efeksamping','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                </td>
            </tr>
            
            <tr>
                <td>
                    <div class="control-label">Interaksi Obat</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('name'=>'interaksiobat','model'=>$modObatAlkesDetail,'attribute'=>'interaksiobat','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                </td>
                <td>
                    <div class="control-label">Cara Penyimpanan</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('name'=>'carapenyimpanan','model'=>$modObatAlkesDetail,'attribute'=>'carapenyimpanan','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                </td>
            </tr>
            
            <tr>
                <td>
                    <div class="control-label">Peringatan</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('name'=>'peringatan','model'=>$modObatAlkesDetail,'attribute'=>'peringatan','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                </td>
                <td></td>
            </tr>
                        
        </table>
    </div>
</fieldset>

<?php
$enableInputobatdetail = ($model->formObatAlkesDetail) ? 1 : 0;
$js = <<< JS
if(${enableInputobatdetail}) {
    $('#divObatalkesdetail input').removeAttr('disabled');
    $('#divObatalkesdetail select').removeAttr('disabled');
}
else {
    $('#divObatalkesdetail input').attr('disabled','true');
    $('#divObatalkesdetail select').attr('disabled','true');
}

$('#pakeDetailObat').change(function(){
        if ($(this).is(':checked')){
                $('#divObatalkesdetail input').removeAttr('disabled');
                $('#divObatalkesdetail select').removeAttr('disabled');
        }else{
                $('#divObatalkesdetail input').attr('disabled','true');
                $('#divObatalkesdetail select').attr('disabled','true');
                $('#divObatalkesdetail input').attr('value','');
                $('#divObatalkesdetail select').attr('value','');
        }
        $('#divObatalkesdetail').slideToggle(500);
    });

JS;
Yii::app()->clientScript->registerScript('obatalkesdetail',$js,CClientScript::POS_READY);
?>
<script>
//Set focus setelah page load semua
window.onload = function(){
    setTimeout('$(\'#GFObatAlkesM_obatalkes_kode\').focus();',500);
}
</script>