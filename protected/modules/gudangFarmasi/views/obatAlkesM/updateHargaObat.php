<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'.',
        'thousands'=>',',
        'precision'=>0,
        'symbol'=>'',
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>2,
    )
));

?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfobat-alkes-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onSubmit'=>'return cekInput();'),
        'focus'=>'#',
)); 
$this->widget('bootstrap.widgets.BootAlert'); ?>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
        
        <table>
            <tr>
                <td>
                     <?php echo $form->textFieldRow($model,'obatalkes_nama',array('class'=>'span4', 'readonly'=>true)); ?>
                     <?php //echo $form->textFieldRow($model,'satuankecilNama',array('class'=>'span2', 'readonly'=>true)); ?>
                     
                     <?php echo CHtml::label('Jenis Obat Alkes', 'obatAlkes', array('class'=>'control-label')) ?>
                         <div class="controls">
                             <?php echo $form->textField($model,'obatAlkes',array('class'=>'span3', 'readonly'=>true));  ?>
                         </div>                 
                     <?php echo $form->textFieldRow($model,'obatalkes_kategori',array('class'=>'span2', 'readonly'=>true)); ?>
                    
                    
                    
<!--                     <div class='control-group'>
                            <?php //echo $form->labelEx($model,'tglperubahan', array('class'=>'control-label')) ?>
                         <div class="controls">
                             <?php 
//                                 $this->widget('MyDateTimePicker',array(
//                                        'model'=>$model,
//                                        'attribute'=>'tglperubahan',
//                                        'mode'=>'date',
//                                        'options'=> array(
//                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//    //                                                        'maxDate' => 'd',
//                                            'minDate'=>$minDate,
//                                        ),
//                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
//                                  )); 
                             ?>
                         </div>
                     </div>    -->
                     <?php// echo $form->textAreaRow($model,'alasanperubahan',array('class'=>'span3', 'cols'=>50, 'rows'=>3)); ?>
 
                   <?php $a = $_GET['status']; if( $a == "hargajual"){ ?>
                    
                    <fieldset id="fieldsetHargaJualApotek">
                            <legend class="rim">Harga Jual Apotek</legend>
                            <div >
                                <table>
                                     <tr>
                                        <td>
                                            <div class="control-group" style="margin-left:-30px;">
                                                <?php echo $form->labelEx($model,'marginnonresep',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                   <?php echo $form->textField($model,'marginnonresep',array('class'=>'span1 number', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'marginNonResep();')); ?> %
                                                </div>
                                            </div> 
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                            <td>
                                                <div class="control-group" style="margin-left:-30px;">
                                                    <?php echo $form->labelEx($model,'hjanonresep',array('class'=>'control-label'));?>
                                                    <div class="controls">
                                                       <?php echo CHtml::textField('hjanonresep',0,array('class'=>'span2 currency','value'=>0,
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;')); ?> Rupiah
                                                    </div>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="control-group" style="margin-left:-50px;">
                                                    <?php echo $form->labelEx($model,'hjanonresep',array('class'=>'control-label'));?>
                                                    <div class="controls">
                                                       <?php echo $form->textField($model,'hjanonresep',array('class'=>'span2 currency', 
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;')); ?> Rupiah
                                                    </div>
                                                </div> 
                                            </td>
                                    </tr> 
                                    <tr>
                                        <td>
                                            <div class="control-group" style="margin-left:-30px;">
                                                <?php echo $form->labelEx($model,'marginresep',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                   <?php echo $form->textField($model,'marginresep',array('class'=>'span1 number', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'marginResep();')); ?> %
                                                                    
                                                     <?php echo $form->hiddenField($model,'harganetto',array('class'=>'span1 currency', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                                </div>
                                            </div> 
                                        </td>
                                        <td>
                                            <?php echo $form->hiddenField($model,'jasadokter',array('class'=>'span1 number', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                            <?php echo $form->hiddenField($model,'hpp',array('class'=>'span1 currency', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <div class="control-group" style="margin-left:-30px;">
                                                <?php echo CHtml::label('Resep Dokter','jasadokter',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                    <?php echo CHtml::hiddenField('persenjasadokter_kons'); ?>
                                                     <?php echo CHtml::dropDownList('persenjasadokter','persenjasadokter', 
                                                             CHtml::listData(JasaresepM::model()->findAll(), 'persenjasa', 'persenjasa') ,
                                                                array('empty'=>'-- Pilih --',
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:70px;','onclick'=>'jasaDokter(this);')); ?> %
                                                </div>
                                            </div> 
                                        </td>
                                        <td></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <div class="control-group" style="margin-left:-30px;">
                                                <?php echo $form->labelEx($model,'hjaresep',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                   <?php echo CHtml::textField('hjaresep',0,array('class'=>'span2 currency','value'=>0,
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;')); ?> Rupiah
                                                </div>
                                            </div> 
                                        </td>
                                        <td>
                                             <div class="control-group" style="margin-left:-50px;">
                                                <?php echo $form->labelEx($model,'hjaresep',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                   <?php echo $form->textField($model,'hjaresep',array('class'=>'span2 currency', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;')); ?> Rupiah
                                                </div>
                                            </div> 
                                        </td>
                                    </tr>
                                    
                                   
                                                                               
                                </table>
                            </div>
                    </fieldset>
                    <?php } ?>
                    
                    <?php if($a == "harganetto"){ ?>
                        <fieldset id="fieldsetHargaNetto">
                            <legend class="rim">Harga Netto Apotek</legend>
                            <div>
                                 <div class="control-group" style="margin-left:-30px;">
                                    <?php echo $form->labelEx($model,'harganetto',array('class'=>'control-label'));?>
                                    <div class="controls">
                                       <?php echo $form->hiddenField($model,'harganetto_asal',array('onkeyup'=>'hitungHargaJual();','class'=>'span2 currency', 
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHpp();')); ?> 
                                       <?php echo $form->textField($model,'harganetto',array('onkeyup'=>'hitungHargaJual();','class'=>'span2 currency', 
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHpp();')); ?>
                                    </div>
                                </div> 
                                
                                <div class="control-group" style="margin-left:-30px;">
                                    <?php echo $form->labelEx($model,'discount',array('class'=>'control-label'));?>
                                    <div class="controls">
                                       <?php echo $form->textField($model,'discount',array('class'=>'span1 number', 
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHpp();')); ?> %
                                    </div>
                                </div> 
                                
                                <div class="control-group" style="margin-left:-30px;">
                                    <?php echo $form->labelEx($model,'ppn_persen',array('class'=>'control-label'));?>
                                    <div class="controls">
                                       <?php echo $form->textField($model,'ppn_persen',array('class'=>'span1 number', 
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHpp();')); ?> %
                                    </div>
                                </div>  
                                
                                <div class="control-group" style="margin-left:-30px;">
                                    <?php echo Chtml::label('HPP','hpp',array('class'=>'control-label'));?>
                                    <div class="controls">
                                       <?php echo $form->textField($model,'hpp',array('class'=>'span1 currency', 
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> <font size="1px;">(Harga Netto - (Discount + Ppn)) </font>
                                    </div>
                                </div>  
                                 
                            </div>
                    </fieldset>
                    <?php } ?>
                    <?php //echo $form->labelEx($model,'disetujuioleh', array('class'=>'control-label')); ?> 
<!--                    <div class="controls">
                        <?php //echo CHtml::activeHiddenField($model,'disetujuioleh');?>
                            <div style="float:left;">
                                <?php
//                                    $this->widget('MyJuiAutoComplete',array(
//                                        'model'=>$model,
//                                        'attribute'=>'disetujuioleh',
//                                        'sourceUrl'=>  Yii::app()->createUrl('ActionAutoComplete/ListDokter'),
//                                        'options'=>array(
//                                            'showAnim'=>'fold',
//                                            'minLength'=>2,
//                                            'select'=>'js:function( event, ui ) {
//                                                    $("#UbahhargaobatR_disetujuioleh").val(ui.item.nama_pegawai);
//                                                        }',
//                                        ),
////                                        'tombolDialog'=>array('idDialog'=>'dialogDisetujui'),
//                                        'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2','style'=>'float:left;')
//                                    ));
                                ?>
                            </div>
                    </div>-->
                </td> 
            </tr>
        </table>
           <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        //Yii::app()->createUrl($this->module->id.'/'.obatAlkesM.'/admin'),
                        '#',
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) {return false;}else{window.parent.$("#doalogUpdateHarga").dialog("close")}')); ?>
           </div>

<?php $this->endWidget(); ?>
<?php
//===============Dialog buat pegawai
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogDisetujui',
    'options'=>array(
        'title'=>'Pencarian Pegawai',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new PegawaiM('search');
$modPegawai->unsetAttributes();
if(isset($_GET['PegawaiM'])){
    $modPegawai->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawaiYangMengajukan-m-grid',
    'dataProvider'=>$modPegawai->search(),
    'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn_small",
                "id"=>"selectPegawai",
                "onClick"=>"$(\"#UbahhargaobatR_disetujuioleh\").val(\"$data->nama_pegawai\");
//                            $(\"#'.CHtml::activeId($model,'yangmengajukan').'\").val(\"$data->gelardepan $data->nama_pegawai\");
                            $(\"#dialogDisetujui\").dialog(\"close\");
                            return false;"
                ))'
        ),
        
        'nama_pegawai',
        'jeniskelamin',
        'nomorindukpegawai',
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
        
$this->endWidget();
if($tersimpan=='Ya'){ ?>
    <script type="text/javascript">
       parent.location.reload();
    </script>
<?php } ?>
<script type="text/javascript">

function hitungHargaJual()
{
    var harganetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
    var hargajual = unformatNumber($('#GFObatAlkesM_hargajual').val());
    
    var totalhargaJual = (parseFloat(harganetto) * ( parseFloat(<?php echo ($konfigFarmasi->totalpersenhargajual == 0) ? 100 :MyFunction::calculate_string($konfigFarmasi->totalpersenhargajual) ?>)/ parseFloat(100)));
  
   $('#GFObatAlkesM_hargajual').val(formatUang(totalhargaJual));
}

function hitungHpp()
{
    var harganetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
    var discount = unformatNumber($('#GFObatAlkesM_discount').val());
    var ppn = unformatNumber($('#GFObatAlkesM_ppn_persen').val());
    
    jumlahDiskon = (harganetto - (harganetto * (discount/100)))
    jumlahPpn = (jumlahDiskon * (ppn / 100));
    hpp = jumlahDiskon + jumlahPpn;
    
    $('#GFObatAlkesM_hpp').val(formatUang(hpp));
}

function marginResep(){
    var hpp = unformatNumber($('#GFObatAlkesM_hpp').val());
    var marginResep = parseFloat(unformatNumber($('#GFObatAlkesM_marginresep').val()));
    var hargaNetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
    var hja = unformatNumber($('#GFObatAlkesM_hjaresep').val());
    var jasaDokter = $('#persenjasadokter').val();

    if(jasaDokter == ''){
        jasaDokter = 0;
    }
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/getPersenDokter');?>', {hargaNetto:hargaNetto},function(data){
                $('#persenjasadokter').val(data.jasaResep);
                $('#persenjasadokter_kons').val(data.jasaResep);
        },'json');
        
    hjaResep = (hpp + (hpp * (marginResep / 100)) + (hja * (jasaDokter / 100)));
    totJasaDokter = Math.ceil(hja * (jasaDokter / 100));

    
    $('#GFObatAlkesM_hjaresep').val(formatUang(hjaResep));
    $('#hjaresep').val(formatUang(hjaResep));
    $('#GFObatAlkesM_hargajual').val(formatUang(hjaResep));
    $('#GFObatAlkesM_jasadokter').val(totJasaDokter);
    
}

function marginNonResep(){
    var hpp = unformatNumber($('#GFObatAlkesM_hpp').val());
    var marginNonResep = unformatNumber($('#GFObatAlkesM_marginnonresep').val());
    var hargaNetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
    var hargaJual = unformatNumber($('#GFObatAlkesM_hargajual').val());
    var hja = unformatNumber($('#GFObatAlkesM_hjaresep').val());
    
    if(hpp == ''){
        hpp = hargaNetto;
    }
    hjanonResep = (hpp + (hpp * (marginNonResep / 100)));
    
    
    $('#GFObatAlkesM_hjanonresep').val(formatUang(hjanonResep));
    $('#hjanonresep').val(formatUang(hjanonResep));
}

function jasaDokter(obj){
    alert("Jasa resep dokter tidak bisa diubah !");
    $(obj).val($('#persenjasadokter_kons').val());
}
function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
</script>