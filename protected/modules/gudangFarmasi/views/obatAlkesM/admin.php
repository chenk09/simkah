<?php
$this->breadcrumbs=array(
	'Gfobat Alkes Ms'=>array('index'),
	'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Obat Alkes ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GFObatAlkesM', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Obat Alkes ', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#search').submit(function(){
	$.fn.yiiGridView.update('gfobat-alkes-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'gfobat-alkes-m-grid',
	'dataProvider'=>$model->searchGudangFarmasi(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                        'header'=>'Kode Obat',
                        'name'=>'obatalkes_kode',
                        'value'=>'$data->obatalkes_kode',
                ),
		array(
                        'header'=>'Nama Obat',
                        'name'=>'obatalkes_nama',
                        'value'=>'$data->obatalkes_nama',
                ),
		array(
                        'header'=>'Asal Barang',
                        'name'=>'sumberdanaNama',
//                        'filter'=>  CHtml::listData(SumberdanaM::model()->findAll(), 'sumberdana_id', 'sumberdana_nama'),
                        'value'=>'$data->sumberdana->sumberdana_nama',
                ),
		array(
                        'header'=>'Jenis Obat',
                        'name'=>'jenisobatalkes_id',
                        'filter'=>  CHtml::listData($model->JenisObatAlkesItems, 'jenisobatalkes_id', 'jenisobatalkes_nama'),
                        'value'=>'$data->jenisobatalkes->jenisobatalkes_nama',
                ),
		array(
                        'header'=>'Satuan Besar',
                        'name'=>'satuanbesar_id',
                        'filter'=>  CHtml::listData($model->SatuanBesarItems, 'satuanbesar_id', 'satuanbesar_nama'),
                        'value'=>'$data->satuanbesar->satuanbesar_nama',
                ),
                
		array(
                        'header'=>'Satuan Kecil',
                        'name'=>'satuankecil_id',
                        'filter'=>  CHtml::listData($model->SatuanKecilItems, 'satuankecil_id', 'satuankecil_nama'),
                        'value'=>'$data->satuankecil->satuankecil_nama',
                ),
                array(
                        'header'=>'Tgl Kadaluarsa',
                        'name'=>'tglkadaluarsa',
                        'value'=>'$data->tglkadaluarsa',
                ),
		array(
                        'header'=>'Isi Kemasan  / <br> Min. Stok',
//                        'name'=>'obatalkes_kategori',
                        'type'=>'raw',
                        'value'=>'$data->kemasanbesar ."/ <br/>". $data->minimalstok',
                ),
                array(
                    'header'=>'Harga Netto',
                    'name'=>'harganetto',
                    'type'=>'raw',
//                    'value'=>'"Rp. ".MyFunction::formatNumber($data->harganetto)',
                    'value'=>'CHtml::link("<i class=\'icon-pencil\'></i>", Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/updateHarga", array("idObat"=>$data->obatalkes_id, "status"=>"harganetto")), array("title"=>"Update harga netto", "target"=>"iframeUpdateHarga", "onclick"=>"$(\"#doalogUpdateHarga\").dialog(\"open\")", "rel"=>"tooltip"))."&nbsp;&nbsp;".MyFunction::formatNumber($data->harganetto)',
                ),
//                array(
//                  'header'=>'Harga Jual',
//                  'name'=>'hargajual',
//                  'type'=>'raw',
////                  'value'=>'"Rp. ".MyFunction::formatNumber($data->hargajual)',
//                    'value'=>'CHtml::link("<i class=\'icon-pencil\'></i>", Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/updateHarga", array("idObat"=>$data->obatalkes_id, "status"=>"hargajual")), array("title"=>"Update harga jual", "target"=>"iframeUpdateHarga", "onclick"=>"$(\"#doalogUpdateHarga\").dialog(\"open\")", "rel"=>"tooltip"))."&nbsp;&nbsp;".MyFunction::formatNumber($data->hargajual)',
//                ),
                array(
                  'header'=>'HJA Resep',
                  'name'=>'hjaresep',
                  'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-pencil\'></i>", Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/updateHarga", array("idObat"=>$data->obatalkes_id, "status"=>"hargajual")), array("title"=>"Update HJA Resep", "target"=>"iframeUpdateHarga", "onclick"=>"$(\"#doalogUpdateHarga\").dialog(\"open\")", "rel"=>"tooltip"))."&nbsp;&nbsp;".MyFunction::formatNumber($data->hjaresep)',
                ),
                array(
                  'header'=>'HJA Non-Resep',
                  'name'=>'hjanonresep',
                  'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-pencil\'></i>", Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/updateHarga", array("idObat"=>$data->obatalkes_id, "status"=>"hargajual")), array("title"=>"Update HJA Non-Resep", "target"=>"iframeUpdateHarga", "onclick"=>"$(\"#doalogUpdateHarga\").dialog(\"open\")", "rel"=>"tooltip"))."&nbsp;&nbsp;".MyFunction::formatNumber($data->hjanonresep)',
                ),
                 array(
                    'header'=>'<center>Status</center>',
                    'value'=>'($data->obatalkes_aktif == 1 ) ? "Aktif" : "Tidak Aktif"',
                    'htmlOptions'=>array('style'=>'text-align:center;'),
                ),
//                array(
//                    'header'=>'Aktif',
//                    'class'=>'CCheckBoxColumn',
//                    'id'=>'rows',
//                    'checked'=>'$data->obatalkes_aktif',
//                    'selectableRows'=>0,
//                ),
		array(
                        'header'=>Yii::t('zii','View'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{view}',
		),
		array(
                        'header'=>Yii::t('zii','Update'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{update}',
                        'buttons'=>array(
                            'update' => array (
                                          'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
                                        ),
                         ),
		),
                 array(
                    'header'=>'Hapus',
                    'type'=>'raw',
                    'value'=>'($data->obatalkes_aktif)?CHtml::link("<i class=\'icon-remove\'></i> ","javascript:removeTemporary($data->obatalkes_id)",array("id"=>"$data->obatalkes_id","rel"=>"tooltip","title"=>"Menonaktifkan"))." ".CHtml::link("<i class=\'icon-trash\'></i> ", "javascript:deleteRecord($data->obatalkes_id)",array("id"=>"$data->obatalkes_id","rel"=>"tooltip","title"=>"Hapus")):CHtml::link("<i class=\'icon-trash\'></i> ", "javascript:deleteRecord($data->obatalkes_id)",array("id"=>"$data->obatalkes_id","rel"=>"tooltip","title"=>"Hapus"));',
                    'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            $("table").find("input[type=text]").each(function(){
                cekForm(this);
            })
            $("table").find("select").each(function(){
                cekForm(this);
            })
        }',
)); ?>

<?php 
 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
       $content = $this->renderPartial('../tips/master',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        $url=Yii::app()->createAbsoluteUrl($module.'/'.$controller);

$js = <<< JSCRIPT
function cekForm(obj)
{
    $("#search :input[name='"+ obj.name +"']").val(obj.value);
}
function print(caraPrint)
{
    page = $(".pagination .active a").text();
    window.open("${urlPrint}/"+$('#search').serialize()+"&caraPrint="+caraPrint+"&GFObatalkesfarmasiV_page="+page,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
<?php 
// Dialog untuk update harga =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'doalogUpdateHarga',
    'options'=>array(
        'title'=>'Update Harga Obat',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>900,
        'minHeight'=>450,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeUpdateHarga" width="100%" height="450">
</iframe>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    function removeTemporary(id){
        var url = '<?php echo $url."/removeTemporary"; ?>';
        var answer = confirm('Yakin akan menonaktifkan data ini untuk sementara?');
            if (answer){
                 $.post(url, {id: id},
                     function(data){
                        if(data.status == 'proses_form'){
                                $.fn.yiiGridView.update('gfobat-alkes-m-grid');
                            }else{
                                alert('Data Gagal di Nonaktifkan')
                            }
                },"json");
           }
    }
    
    function deleteRecord(id){
        var id = id;
        var url = '<?php echo $url."/delete"; ?>';
        var answer = confirm('Yakin Akan Menghapus Data ini ?');
            if (answer){
                 $.post(url, {id: id},
                     function(data){
                        if(data.status == 'proses_form'){
                                $.fn.yiiGridView.update('gfobat-alkes-m-grid');
                            }else{
                                alert('Data Gagal di Hapus')
                            }
                },"json");
           }
    }
</script>