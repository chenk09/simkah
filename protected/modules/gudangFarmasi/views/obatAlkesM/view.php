<?php
$this->breadcrumbs=array(
	'Gfobat Alkes Ms'=>array('index'),
	$model->obatalkes_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Obat Alkes ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GFObatAlkesM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' GFObatAlkesM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' GFObatAlkesM', 'icon'=>'pencil','url'=>array('update','id'=>$model->obatalkes_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' GFObatAlkesM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->obatalkes_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Obat Alkes', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
                array(
                  'label'=>'ID',
                  'type'=>'raw',
                  'value'=>$model->obatalkes_id,
                ),
                
                array(
                   'label'=>'Kode Obat Alkes',
                   'type'=>'raw',
                   'value'=>$model->obatalkes_kode,
                ),
                array(
                   'label'=>'Nama Obat Alkes',
                   'type'=>'raw',
                   'value'=>$model->obatalkes_nama,
                ),
                
                array(
                   'label'=>'Harga Netto',
                   'type'=>'raw',
                   'value'=>$model->harganetto,
                ),
                array(
                   'label'=>'Harga Jual',
                   'type'=>'raw',
                   'value'=>$model->hargajual,
                ),
                array(
                   'label'=>'Diskon',
                   'type'=>'raw',
                   'value'=>$model->discount,
                ),
                array(
                   'label'=>'Tgl Kadaluarsa',
                   'type'=>'raw',
                   'value'=>$model->tglkadaluarsa,
                ),
                array(
                   'label'=>'Lokasi Gudang',
                   'type'=>'raw',
                   'value'=>$model->lokasigudang->lokasigudang_nama,
                ),
                array(
                   'label'=>'Therapi Obat',
                   'type'=>'raw',
                   'value'=>$model->therapiobat->therapiobat_nama,
                ),
                array(
                   'label'=>'PBF',
                   'type'=>'raw',
                   'value'=>$model->pbf->pbf_nama,
                ),
                array(
                   'label'=>'Generik',
                   'type'=>'raw',
                   'value'=>$model->generik->generik_nama,
                ),
                array(
                   'label'=>'Satuan Besar',
                   'type'=>'raw',
                   'value'=>$model->satuanbesar->satuanbesar_nama,
                ),
                array(
                   'label'=>'Satuan Kecil',
                   'type'=>'raw',
                   'value'=>$model->satuankecil->satuankecil_nama,
                ),
                array(
                   'label'=>'Asal Barang',
                   'type'=>'raw',
                   'value'=>$model->sumberdana->sumberdana_nama,
                ),
                array(
                   'label'=>'Jenis Obat Alkes',
                   'type'=>'raw',
                   'value'=>$model->jenisobatalkes->jenisobatalkes_nama,
                ),
                array(
                   'label'=>'Golongan',
                   'type'=>'raw',
                   'value'=>$model->obatalkes_golongan,
                ),
                array(
                   'label'=>'Kategori',
                   'type'=>'raw',
                   'value'=>$model->obatalkes_kategori,
                ),
                array(
                   'label'=>'Kadar Obat',
                   'type'=>'raw',
                   'value'=>$model->obatalkes_kadarobat,
                ),
                array(
                   'label'=>'Kemasan Besar',
                   'type'=>'raw',
                   'value'=>$model->kemasanbesar,
                ),
		'kekuatan',
		'satuankekuatan',
		'minimalstok',
		'formularium',
		'discountinue',
		array(            
                    'label'=>'Aktif',
                    'type'=>'raw',
                    'value'=>(($model->obatalkes_aktif==1)? ''.Yii::t('mds','Yes').'' : ''.Yii::t('mds','No').''),
                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>