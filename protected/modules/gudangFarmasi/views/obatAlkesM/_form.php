<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>2,
    )
));

?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfobat-alkes-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onsubmit'=>'return cekInput();'),
        'focus'=>'#GFObatAlkesM_obatalkes_kode',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary($model); ?>
        
        <table>
            <tr>
                <td>
                     <?php echo $form->dropDownListRow($model,'jeniskode_id',
                                               CHtml::listData($model->JenisKodeItems, 'jeniskode_id', 'jeniskode_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --','style'=>'width:100px;','onchange'=>'setJenisKode();')); ?>
                    
                     <?php  echo $form->dropDownListRow($model,'jnskelompok',JnsKelompok::items(),
                                            array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            'empty'=>'-- Pilih --','style'=>'width:100px;')); ?>

                     <?php echo $form->textFieldRow($model,'obatalkes_kode',array('class'=>'span2', 
                                            'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,
                                            )); ?>
                    
                     <?php echo $form->textFieldRow($model,'obatalkes_nama',array('class'=>'span3',
                                            'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>200,
                                            'onkeyup'=>'generateKode(this)')); ?>
                    <?php 
                            echo $form->dropDownListRow($model,'obatalkes_kadarobat',ObatAlkesKadarObat::items(),
                                            array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            'empty'=>'-- Pilih --','style'=>'width:100px;')); ?>
                    
                    <?php echo CHtml::label('Kekuatan', 'kekuatan', array('class'=>'control-label')); ?>
                        <div class="controls">
                              <?php echo $form->textField($model,'kekuatan',array('class'=>'span2 numbersOnly', 
                                            'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                              <?php echo $form->dropDownList($model,'satuankekuatan',  SatuanKekuatan::items(),
                                            array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            'empty'=>'-- Pilih --','style'=>'width:70px;')); ?>
                        </div>
                    
                    <?php echo $form->dropDownListRow($model,'sumberdana_id',
                                               CHtml::listData($model->SumberDanaItems, 'sumberdana_id', 'sumberdana_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --','style'=>'width:100px;')); ?>
					<div class="control-group">
						<?php echo $form->labelEx($model,'jenisobatalkes_id', array('class'=>'control-label')); ?>
						<div class="controls">
							<?php echo $form->hiddenField($model,'jenisobatalkes_id'); ?>
							<?php $this->widget('MyJuiAutoComplete', array(
							   'name'=>'jenisobatalkes', 
								'source'=>'js: function(request, response) {
									   $.ajax({
										   url: "'.Yii::app()->createUrl('ActionAutoComplete/JenisObatAlkes').'",
										   dataType: "json",
										   data: {
											   term: request.term,
										   },
										   success: function (data) {
											   response(data);
										   }
									   })
									}',
								'options'=>array(
										   'showAnim'=>'fold',
										   'minLength' => 2,
										   'focus'=> 'js:function( event, ui ){
												$(this).val(ui.item.label);
												return false;
											}',
										   'select'=>'js:function( event, ui ){
												$(\'#GFObatAlkesM_jenisobatalkes_id\').val(ui.item.jenisobatalkes_id);
												$(\'#jenisobatalkes\').val(ui.item.jenisobatalkes_nama);
												return false;
											}',
								),
								'htmlOptions'=>array(
									'readonly'=>false,
									'placeholder'=>'Jenis Obat Alkes',
									'size'=>13,
									'class'=>'span2',
									'onkeypress'=>"return $(this).focusNextInputField(event);",
								),
								'tombolDialog'=>array('idDialog'=>'dialogjenisobatalkes'),
							)); ?>				
							<?php echo $form->error($model,'jenisobatalkes_id'); ?>
						</div>
					</div>
					
                    <?php echo $form->dropDownListRow($model,'obatalkes_golongan',  ObatAlkesGolongan::items(),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --','style'=>'width:150px;')); ?>
                    
                    <?php echo $form->dropDownListRow($model,'obatalkes_kategori',ObatAlkesKategori::items(),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --','style'=>'width:100px;')); ?>
                    
                    <?php echo $form->dropDownListRow($model,'formularium',  Formularium::items(),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --','style'=>'width:140px;')); ?>
                    
                    <?php echo $form->dropDownListRow($model,'generik_id',
                                               CHtml::listData($model->generikItems, 'generik_id', 'generik_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --','style'=>'width:140px;')); ?>
                    
                    <div class="control-label">
                        
                        <?php echo CHtml::label('Therapi Obat', 'therapiobat_id'); ?>
                        <?php echo $form->hiddenField($model,'therapiobat_id'); ?>
                    </div>
                    <div class="controls">
                    <?php $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'therapiobat', 
                                            'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/TherapiObat').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                            'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'focus'=> 'js:function( event, ui )
                                                           {
                                                            $(this).val(ui.item.label);
                                                            return false;
                                                            }',
                                                       'select'=>'js:function( event, ui ) {
                                                           $(\'#GFObatAlkesM_therapiobat_id\').val(ui.item.therapiobat_id);
                                                           $(\'#therapiobat\').val(ui.item.therapiobat_nama);
                                                            return false;
                                                        }',
                                            ),
                                            'htmlOptions'=>array(
                                                'readonly'=>false,
                                                'placeholder'=>'Therapi Obat',
                                                'size'=>13,
                                                'class'=>'span2',
                                                'onkeypress'=>"return $(this).focusNextInputField(event);",
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogtherapiobat'),
                                    )); ?>
                    </div>
                    
                    <div class='control-group'>
                            <?php echo $form->labelEx($model,'tglkadaluarsa', array('class'=>'control-label')) ?>
                         <div class="controls">
                             <?php $minDate = (Yii::app()->user->getState('tglpemakai')) ? '' : 'd'; ?>
                             <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglkadaluarsa',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                                        'maxDate' => 'd',
                                                        'minDate'=>$minDate,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                              )); ?>
                         </div>
                     </div>
                    
                    <?php echo $form->textFieldRow($model,'noregister',array('class'=>'span3', 
                                        'onkeypress'=>"return $(this).focusNextInputField(event);"));?>
                    
                    <?php echo $form->textFieldRow($model,'nobatch',array('class'=>'span3', 
                                        'onkeypress'=>"return $(this).focusNextInputField(event);"));?>
                    
                    <?php echo $form->dropDownListRow($model,'pbf_id',
                                               CHtml::listData($model->PbfItems, 'pbf_id', 'pbf_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --','style'=>'width:130px;')); ?>
                    
                     <?php echo $form->hiddenField($model,'hargajual',array('onkeyup'=>'hitungHargaJual();','class'=>'span2 currency', 
                                           'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>


                     <?php// echo $form->dropDownListRow($model,'discountinue',array('1'=>'Ya','0'=>'Tidak'),
                                  //         array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                   //        'empty'=>'-- Pilih --',)); ?>   
                    <?php //echo $form->checkBoxRow($model,'isjasadibayarkan',array('checked'=>$model->isjasadibayarkan)); ?>
                    <?php echo $form->checkBoxRow($model,'discountinue',array('checked'=>$model->discountinue)); ?>                     
                     <?php echo $form->checkBoxRow($model,'isjasadibayarkan',array('checked'=>$model->isjasadibayarkan)); ?>
                     <?php echo $form->checkBoxRow($model,'obatalkes_aktif',array('checked'=>$model->obatalkes_aktif)); ?>
                </td>
                <td>
                    <fieldset id="fieldsetStok">
                            <legend class="rim">Stok</legend>
                            <div class="toggle" >
                                <div class="control-group" style="margin-left:-30px;">
                                    <?php echo $form->labelEx($model,'satuanbesar_id',array('class'=>'control-label'));?>
                                    <div class="controls">
                                       <?php echo $form->dropDownList($model,'satuanbesar_id',
                                               CHtml::listData($model->SatuanBesarItems, 'satuanbesar_id', 'satuanbesar_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --','style'=>'width:130px;')); ?>
                                    </div>
                                </div> 
                                 <div class="control-group" style="margin-left:-30px;">
                                    <?php echo $form->labelEx($model,'harga_besar',array('class'=>'control-label'));?>
                                    <div class="controls">
										<?php echo $form->textField($model,'harga_besar',array(
											'class'=>'span2 currency', 
											'onkeyup'=>"hitungHargaNetto()",
											'onkeypress'=>"return $(this).focusNextInputField(event);",
										)); ?>
                                    </div>
                                </div>
                                <div class="control-group" style="margin-left:-30px;">
                                    <?php echo $form->labelEx($model,'satuankecil_id',array('class'=>'control-label'));?>
                                    <div class="controls">
                                       <?php echo $form->dropDownList($model,'satuankecil_id',
                                               CHtml::listData($model->SatuanKecilItems, 'satuankecil_id', 'satuankecil_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                                    </div>
                                </div> 
                                
                                 <div class="control-group" style="margin-left:-30px;">
                                    <?php echo $form->labelEx($model,'minimalstok',array('class'=>'control-label'));?>
                                    <div class="controls">
                                       <?php echo $form->textField($model,'minimalstok',array('class'=>'span1 numbersOnly', 
                                              'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                    </div>
                                </div> 
                                
                                <div class="control-group" style="margin-left:-30px;">
                                    <?php echo $form->labelEx($model,'kemasanbesar',array('class'=>'control-label'));?>
                                    <div class="controls">
                                       <?php echo $form->textField($model,'kemasanbesar',array(
											'class'=>'span1 numbersOnly', 
                                            'onkeypress'=>"return $(this).focusNextInputField(event);",
											'onkeyup'=>"hitungHargaNetto()"
										)); ?>
                                    </div>
                                </div> 
                                
                                <div class="control-group" style="margin-left:-30px;">
                                    <?php echo $form->labelEx($model,'lokasigudang_id',array('class'=>'control-label'));?>
                                    <div class="controls">
                                       <?php echo $form->dropDownList($model,'lokasigudang_id',
                                               CHtml::listData($model->lokasiGudangItems, 'lokasigudang_id', 'lokasigudang_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --','style'=>'width:130px;')); ?>
                                    </div>
                                </div> 
                                  
                            </div>
                    </fieldset></br>
                    
                    <fieldset id="fieldsetHargaNetto">
                            <legend class="rim">Harga Netto Apotek (Dalam Satuan Kecil)</legend>
                            <div class="toggle">
								 <table>
                                     <tr>
                                        <td>
                                            <div class="control-group" style="margin-left:-30px;">
												<?php echo $form->labelEx($model,'harganetto',array('class'=>'control-label'));?>
												<div class="controls">
												  <?php echo $form->hiddenField($model,'harganetto_asal',array('onkeyup'=>'hitungHargaJual();','class'=>'span2 currency', 
																	'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHpp();marginResep();hitungHargaJual();')); ?>
												   <?php echo $form->textField($model,'harganetto',array('onkeyup'=>'hitungHargaJual();','class'=>'span2 currency', 
																	'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHpp();marginResep();hitungHargaJual();')); ?>
												</div>
											</div> 
											
											<div class="control-group" style="margin-left:-30px;">
												<?php echo $form->labelEx($model,'discount',array('class'=>'control-label'));?>
												<div class="controls">
												   <?php echo $form->textField($model,'discount',array('class'=>'span1 number', 
																	'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHpp();')); ?> %
												</div>
											</div> 
											
											<div class="control-group" style="margin-left:-30px;">
												<?php echo $form->labelEx($model,'ppn_persen',array('class'=>'control-label'));?>
												<div class="controls">
												   <?php echo $form->textField($model,'ppn_persen',array('class'=>'span1 number', 
																	'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHpp();')); ?> %
												</div>
											</div>  
                                        </td>
                                        <td>
											<div class="control-group" style="margin-left:-30px;">
												<?php echo Chtml::label('HPP','hpp',array('class'=>'control-label'));?>
												<div class="controls">
												   <?php echo $form->textField($model,'hpp',array('class'=>'span1 number', 'onkeyup'=>'marginResep();', 
																	'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
												<br/>
												HARGA NETTO + PPN
												</div>
											</div>  
                                        </td>
                                    </tr>
                                </table>  
                                 
                            </div>
                    </fieldset></br>
                    
                    <fieldset id="fieldsetHargaJualApotek">
                            <legend class="rim">Harga Jual Apotek (Dalam Satuan Kecil)</legend>
                            <div class="toggle">
                                <table>
                                     <tr>
                                        <td>
                                            <div class="control-group" style="margin-left:-30px;">
                                                <?php echo $form->labelEx($model,'marginnonresep',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                   <?php echo $form->textField($model,'marginnonresep',array('class'=>'span1 number', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHargaJual()')); ?> %
                                                </div>
                                            </div> 
                                        </td>
                                        <td>
                                            <div class="control-group" style="margin-left:-50px;">
                                                    <?php echo $form->labelEx($model,'hjanonresep_asal',array('class'=>'control-label'));?>
                                                    <div class="controls">
                                                       <?php echo $form->textField($model,'hjanonresep_asal',array('class'=>'span1 currency', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;','readonly'=>true)); ?> Rp.
                                                    </div>
                                                </div> 
                                        </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                <div class="control-group" style="margin-left:-30px;">
                                                    <?php echo $form->labelEx($model,'hjanonresep',array('class'=>'control-label'));?>
                                                    <div class="controls">
                                                       <?php echo CHtml::textField('hjanonresep',0,array('class'=>'span2 currency','value'=>0,'readonly'=>true,
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;')); //,'onkeyup'=>'marginNonResep();'?> Rp.
																																				<br/>
																		(HARGA NETTO * MARGIN NON RESEP) + PPN
                                                    </div>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="control-group" style="margin-left:-50px;">
                                                    <?php echo $form->labelEx($model,'hjanonresep',array('class'=>'control-label'));?>
                                                    <div class="controls">
                                                       <?php echo $form->textField($model,'hjanonresep',array('class'=>'span2 currency', 
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;')); //,'onkeyup'=>'marginNonResep();'?> Rp.
                                                    </div>
                                                </div> 
                                            </td>
                                    </tr>  
                                    
                                    <tr>
                                        <td>
                                            <div class="control-group" style="margin-left:-30px;">
                                                <?php echo $form->labelEx($model,'marginresep',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                   <?php echo $form->textField($model,'marginresep',array('class'=>'span1 number',
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'onkeyup'=>'hitungHargaJual();')); ?> %
                                                </div>
                                            </div> 
                                        </td>
                                        <td>
                                            <?php echo $form->hiddenField($model,'jasadokter',array('class'=>'span1 currency',
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                            
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <div class="control-group" style="margin-left:-30px;">
                                                <?php echo CHtml::label('Resep Dokter','jasadokter',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                    <?php echo CHtml::hiddenField('persenjasadokter_kons'); ?>
                                                     <?php echo CHtml::dropDownList('persenjasadokter','persenjasadokter', 
                                                             CHtml::listData(JasaresepM::model()->findAll(), 'persenjasa', 'persenjasa') ,
                                                                array('empty'=>'-- Pilih --',
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:70px;','onclick'=>'jasaDokter(this);')); ?> %
                                                </div>
                                            </div> 
                                        </td>
                                        <td>
                                            <div class="control-group" style="margin-left:-50px;">
                                                    <?php echo $form->labelEx($model,'hjaresep_asal',array('class'=>'control-label'));?>
                                                    <div class="controls">
                                                       <?php echo $form->textField($model,'hjaresep_asal',array('class'=>'span1 currency', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;','readonly'=>true)); ?> Rp.
                                                    </div>
                                                </div> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="control-group" style="margin-left:-30px;">
                                                <?php echo $form->labelEx($model,'hjaresep',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                   <?php echo CHtml::textField('hjaresep',0,array('class'=>'span2 currency','value'=>0,'readonly'=>true, 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;')); ?> Rp.
																	<br/>
																	(HARGA NETTO * MARGIN RESEP) + PPN
                                                </div>
                                            </div> 
                                        </td>
                                        <td>
                                             <div class="control-group" style="margin-left:-50px;">
                                                <?php echo $form->labelEx($model,'hjaresep',array('class'=>'control-label'));?>
                                                <div class="controls">
                                                   <?php echo $form->textField($model,'hjaresep',array('class'=>'span2 currency', 
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",'style'=>'width:80px;')); ?> Rp.
                                                </div>
                                            </div> 
                                        </td>
                                    </tr>
                                    
                                                                             
                                </table>
                            </div>
                    </fieldset>
                     
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="control-group">
                        <?php echo Chtml::label('Supplier','Supplier',array('class'=>'control-label'));?>
                        <div class="controls">
                           <?php 
                               $this->widget('application.extensions.emultiselect.EMultiSelect',
                                             array('sortable'=>true, 'searchable'=>true)
                                        );
                                echo CHtml::dropDownList(
                                'supplier_id[]',
                                '',
                                CHtml::listData(GFSupplierM::model()->findAll('supplier_aktif=TRUE ORDER BY supplier_nama'), 'supplier_id', 'supplier_nama'),
                                array('multiple'=>'multiple','key'=>'supplier_id', 'class'=>'multiselect','style'=>'width:500px;height:150px')
                                        );
                          ?>
                        </div>
                    </div>      
                </td> 
            </tr>
        </table>
        <?php echo $this->renderPartial('_ObatAlkesDetail', array('model'=>$model,'modObatAlkesDetail'=>$modObatAlkesDetail, 'form'=>$form)); ?>
           <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.obatAlkesM.'/admin'), 
                                    array('class'=>'btn btn-danger',
                                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('gudangFarmasi.views.tips.tipsaddedit',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
           </div>

<?php $this->endWidget(); ?>
        
<!-- =============================== beginWidget Therapi Obat ============================= -->        
<?php
   $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogtherapiobat',
    'options'=>array(
        'title'=>'Pencarian Therapi Obat',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>400,
        'resizable'=>false,
        ),
    ));
   
$modTherapiobat = new TherapiobatM('search');
$modTherapiobat->unsetAttributes();
if(isset($_GET['TherapiobatM'])) {
    $modTherapiobat->attributes = $_GET['TherapiobatM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'therapiobat-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modTherapiobat->search(),
	'filter'=>$modTherapiobat,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selecttherapiobat",
                                                    "onClick" => "\$(\"#GFObatAlkesM_therapiobat_id\").val($data->therapiobat_id);
                                                                          \$(\"#therapiobat\").val(\"$data->therapiobat_nama\");
                                                                          \$(\"#dialogtherapiobat\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'therapiobat_nama',
                'therapiobat_namalain',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<!-- ============================== endWidget TherapiObat ================================= -->

<!-- =============================== beginWidget Jenis Obat Alkes ============================= -->        
<?php
   $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogjenisobatalkes',
    'options'=>array(
        'title'=>'Pencarian Jenis Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>400,
        'resizable'=>false,
        ),
    ));
   
$modJenisObat = new JenisobatalkesM('search');
$modJenisObat->unsetAttributes();
if(isset($_GET['JenisobatalkesM'])) {
    $modJenisObat->attributes = $_GET['JenisobatalkesM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'jenisobatalkes-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modJenisObat->search(),
	'filter'=>$modJenisObat,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selectjenisobatalkes",
                                                    "onClick" => "\$(\"#GFObatAlkesM_jenisobatalkes_id\").val($data->jenisobatalkes_id);
                                                      \$(\"#jenisobatalkes\").val(\"$data->jenisobatalkes_nama\");
                                                      \$(\"#dialogjenisobatalkes\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'jenisobatalkes_nama',
                'jenisobatalkes_namalain',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<!-- ============================== endWidget TherapiObat ================================= -->

 <?php
$urlgetKodeObatAlkes=Yii::app()->createUrl('ActionAjax/GetKodeObatAlkes');
$kodeObat = CHtml::activeId($model,'obatalkes_kode');
                       
$js = <<< JS
function generateKode(obj)
{
//   Generate kode obat di nonaktifkan (Input Manual)
//   namaObat =obj.value;
//   if(namaObat!=''){//Jika nama Obat Tidak Kosong  
//       $.post("${urlgetKodeObatAlkes}",{namaObat: namaObat},
//            function(data){
//                $('#${kodeObat}').val(data.kodeObatBaru);      
//        },"json");
//   }else{//Jika Nama Obat Kosong
//                $('#${kodeObat}').val('');      
//   } 
}

JS;
Yii::app()->clientScript->registerScript('sfdasdasda',$js,CClientScript::POS_HEAD);

                       
$js = <<< JS
$('.numbersOnly').keyup(function() {
    var d = $(this).attr('numeric');
    var value = $(this).val();
    var orignalValue = value;
    value = value.replace(/[0-9]*/g, "");
    var msg = "Only Integer Values allowed.";

    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
}

if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(this).val(orignalValue);
    }
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>
<script type="text/javascript">

function hitungHargaNetto()
{
	var harga_besar = unformatNumber($('#GFObatAlkesM_harga_besar').val()); 
	var kemasanbesar = unformatNumber($('#GFObatAlkesM_kemasanbesar').val()); 
	harganetto = harga_besar / kemasanbesar;
	$('#GFObatAlkesM_harganetto').val(harganetto); 
	hitungHpp();
	marginResep();
	hitungHargaJual();
}

function hitungSemua()
{
   var harganetto = unformatNumber($('#GFObatAlkesM_harganetto').val()); 
   var discount = unformatNumber($('#GFObatAlkesM_discount').val()) / 100.0;
   var ppn = unformatNumber($('#GFObatAlkesM_ppn_persen').val()) / 100.0;
   var marginresep = unformatNumber($('#GFObatAlkesM_marginresep').val()) / 100.0;
   var marginnonresep = unformatNumber($('#GFObatAlkesM_marginnonresep').val()) / 100.0;
   var hargamarginresep = harganetto * marginresep;
   var hargamarginnonresep = harganetto * marginnonresep;
   var persenjasadokter = parseFloat($('#persenjasadokter').val());
   var hppSebelumPajak = harganetto - (harganetto * discount);
   var hpp = hppSebelumPajak + (hppSebelumPajak * ppn);
//   var hpp = harganetto + (harganetto * ppn) - (harganetto * discount);   
//   var hpp = (harganetto) + ((harganetto)*ppn);   
   $('#GFObatAlkesM_hpp').val(formatDesimal(hpp)); 
   var hjanonresep = (harganetto + hargamarginnonresep) + ((harganetto + hargamarginnonresep) * ppn);
//   var hjanonresep = ((harganetto + (100/marginnonresep)/100)) * ((100+marginnonResep)/100);
	var hjanonresep_asal = unformatNumber($('#GFObatAlkesM_hjanonresep_asal').val());
	if(hjanonresep_asal > 0)
	{
		$('#GFObatAlkesM_hjanonresep').val(formatUang(hjanonresep_asal));
	}else{
		$('#GFObatAlkesM_hjanonresep').val(formatUang(hjanonresep));
	}
	$('#hjanonresep').val(formatUang(hjanonresep));
   
	if(persenjasadokter == '')
	{
		persenjasadokter = 0;
	}
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/getPersenDokter');?>', {hargaNetto:harganetto},function(data){
		if(hjanonresep_asal == 0)
		{
			$('#persenjasadokter').val(data.jasaResep);
			$('#GFObatAlkesM_jasadokter').val(data.jasaResep);			
		}		
    },'json');
	
//   var hjaresep = (hpp + (hpp * marginresep) + (hpp * (persenjasadokter/100)));
	
	var hjaresep = (harganetto + hargamarginresep) + ((harganetto + hargamarginresep) * ppn);
	var hjaresep_asal = unformatNumber($('#GFObatAlkesM_hjaresep_asal').val());
	if(hjaresep_asal > 0)
	{
		$('#GFObatAlkesM_hjaresep').val(formatUang(hjaresep_asal));
	}else{
		$('#GFObatAlkesM_hjaresep').val(formatUang(hjaresep));
	}
	$('#hjaresep').val(formatUang(hjaresep));
//   $('#GFObatAlkesM_hargajual').val(formatUang(hjaresep));

    var jasadokter = hjaresep * (persenjasadokter / 100)
    $('#persenjasadokter_kons').val(jasadokter);
   
}

function hitungHargaJual()
{
//    var harganetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
//    var hargajual = unformatNumber($('#GFObatAlkesM_hargajual').val());
//    
//    var totalhargaJual = (parseFloat(harganetto) * ( parseFloat(<?php echo ($konfigFarmasi->totalpersenhargajual == 0) ? 100 :MyFunction::calculate_string($konfigFarmasi->totalpersenhargajual) ?>)/ parseFloat(100)));
//  
//   $('#GFObatAlkesM_hargajual').val(formatUang(totalhargaJual));
    hitungSemua();
}

function hitungHpp()
{
//    var harganetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
//    var discount = unformatNumber($('#GFObatAlkesM_discount').val());
//    var ppn = unformatNumber($('#GFObatAlkesM_ppn_persen').val());
//    
//    jumlahDiskon = (harganetto - (harganetto * (discount/100)))
//    jumlahPpn = (jumlahDiskon * (ppn / 100));
//    hpp = jumlahDiskon + jumlahPpn;
//    
//    $('#GFObatAlkesM_hpp').val(formatUang(hpp));
    
    
//    var harganetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
//    var discount = parseFloat(unformatNumber($('#GFObatAlkesM_discount').val()));
//    var ppn = parseFloat(unformatNumber($('#GFObatAlkesM_ppn_persen').val()));
//    
//    
//    var hppsaja = harganetto + (harganetto + (ppn/100)) - (harganetto * (discount/100));
//    
//    $('#GFObatAlkesM_hpp').val(formatUang(hppsaja));
    hitungSemua();
}

function marginResep(){
//    var hpp = unformatNumber($('#GFObatAlkesM_hpp').val());
//    var marginResep = parseFloat(unformatNumber($('#GFObatAlkesM_marginresep').val()));
//    var hargaNetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
//    var hja = unformatNumber($('#GFObatAlkesM_hjaresep').val());
//    var jasaDokter = $('#persenjasadokter').val();
//
//    if(jasaDokter == ''){
//        jasaDokter = 0;
//    }
//        $.post('<?php //echo Yii::app()->createUrl('ActionAjax/getPersenDokter');?>', {hargaNetto:hargaNetto},function(data){
//                $('#persenjasadokter').val(data.jasaResep);
//                $('#persenjasadokter_kons').val(data.jasaResep);
//        },'json');
//        
//    hjaResep = (hpp + (hpp * (marginResep / 100)) + (hja * (jasaDokter / 100)));
//    totJasaDokter = Math.ceil(hja * (jasaDokter / 100));
//
//    
//    $('#GFObatAlkesM_hjaresep').val(formatUang(hjaResep));
//    $('#hjaresep').val(formatUang(hjaResep));
//    $('#GFObatAlkesM_hargajual').val(formatUang(hjaResep));
//    $('#GFObatAlkesM_jasadokter').val(totJasaDokter);
    
//    var hpp = unformatNumber($('#GFObatAlkesM_hpp').val());
//    var marginResep = parseFloat(unformatNumber($('#GFObatAlkesM_marginresep').val()))/100;
//    var marginNonResep = unformatNumber($('#GFObatAlkesM_marginnonresep').val()) / 100;
//    var hargaNetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
//    var hja = unformatNumber($('#GFObatAlkesM_hjaresep').val());
//    var jasaDokter = $('#persenjasadokter').val();
//    var ppn = unformatNumber($('#GFObatAlkesM_ppn_persen').val()) / 100;
//
//    if(jasaDokter == ''){
//        jasaDokter = 0;
//    }
//        $.post('<?php //echo Yii::app()->createUrl('ActionAjax/getPersenDokter');?>', {hargaNetto:hargaNetto},function(data){
//                $('#persenjasadokter').val(data.jasaResep);
//                $('#persenjasadokter_kons').val(data.jasaResep);
//        },'json');
//    
//    hjanonResep = (hargaNetto + (hargaNetto * ppn)) + ((hargaNetto + (hargaNetto * ppn)) * marginNonResep);
//    hjaResep = hjanonResep + (hjanonResep * marginResep);
//    totJasaDokter = Math.ceil(hja * (jasaDokter / 100));
//
//    
//    $('#GFObatAlkesM_hjaresep').val(formatUang(hjaResep));
//    $('#hjaresep').val(formatUang(hjaResep));
//    $('#GFObatAlkesM_hargajual').val(formatUang(hjaResep));
//    $('#GFObatAlkesM_jasadokter').val(totJasaDokter);
      hitungSemua();
}

function marginNonResep(){
//    var hpp = unformatNumber($('#GFObatAlkesM_hpp').val());
//    var marginNonResep = unformatNumber($('#GFObatAlkesM_marginnonresep').val()) / 100;
//    var hargaNetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
//    var hja = unformatNumber($('#GFObatAlkesM_hjaresep').val());
//    
//    hjanonResep = (hpp + (hpp * (marginNonResep / 100)));
//    
//    
//    $('#GFObatAlkesM_hjanonresep').val(formatUang(hjanonResep));
//    $('#hjanonresep').val(formatUang(hjanonResep));
    
//    var hpp = unformatNumber($('#GFObatAlkesM_hpp').val());
//    var marginNonResep = unformatNumber($('#GFObatAlkesM_marginnonresep').val()) / 100;
//    var hargaNetto = unformatNumber($('#GFObatAlkesM_harganetto').val());
//    var hja = unformatNumber($('#GFObatAlkesM_hjaresep').val());
//    var ppn = unformatNumber($('#GFObatAlkesM_ppn_persen').val()) / 100;
//    
//    hjanonResep = (hargaNetto + (hargaNetto * ppn)) + ((hargaNetto + (hargaNetto * ppn)) * marginNonResep);
//    
//    
//    $('#GFObatAlkesM_hjanonresep').val(formatUang(hjanonResep));
//    $('#hjanonresep').val(formatUang(hjanonResep));
      hitungSemua();
}

function jasaDokter(obj){
    alert("Jasa resep dokter tidak bisa diubah !");
    $(obj).val($('#persenjasadokter_kons').val());
}
function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}

function setJenisKode(){

    var jeniskode_id = $('#GFObatAlkesM_jeniskode_id').val();
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/GetJenisKode'); ?>',{jeniskode_id: jeniskode_id },function(data){
  
                $('#GFObatAlkesM_obatalkes_kode').val(data.jeniskode);
            
        },"json");
}

</script>
