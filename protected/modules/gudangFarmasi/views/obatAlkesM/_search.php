<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'obatalkes_id',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'lokasigudangNama',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'therapiobatNama',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'pbf_id',array('class'=>'span3')); ?>

	<?php// echo $form->textFieldRow($model,'generik_id',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'satuanbesarNama',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'sumberdana_id',array('class'=>'span3')); ?>

	<?php echo $form->dropDownListRow($model,'satuankecil_id',CHtml::listData($model->getSatuankecilItems(),'satuankecil_id','satuankecil_nama'),array('class'=>'span3','empty'=>'-- Pilih --')); ?>
	<?php echo $form->dropDownListRow($model,'jenisobatalkes_id',CHtml::listData($model->getJenisobatalkesItems(),'jenisobatalkes_id','jenisobatalkes_nama'),array('class'=>'span3','empty'=>'-- Pilih --')); ?>
        <?php echo CHtml::hiddenfield('GFObatalkesfarmasiV[harganetto]');?>
        <?php echo CHtml::hiddenfield('GFObatalkesfarmasiV[hargajual]');?>

	<?php //echo $form->textFieldRow($model,'obatalkes_kode',array('class'=>'span3','maxlength'=>50)); ?>
	<?php echo $form->textFieldRow($model,'obatalkes_nama',array('class'=>'span3','maxlength'=>200)); ?>
	<?php //echo $form->textFieldRow($model,'obatalkes_golongan',array('class'=>'span3','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'obatalkes_kategori',array('class'=>'span3','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'obatalkes_kadarobat',array('class'=>'span3','maxlength'=>20)); ?>

	<?php //echo $form->textFieldRow($model,'kemasanbesar',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'kekuatan',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'satuankekuatan',array('class'=>'span3','maxlength'=>20)); ?>

	<?php //echo $form->textFieldRow($model,'harganetto',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'hargajual',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'discount',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'tglkadaluarsa',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'minimalstok',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'formularium',array('class'=>'span3','maxlength'=>50)); ?>

	<?php //echo $form->checkBoxRow($model,'discountinue',array('checked'=>'discountinue')); ?>

	<?php echo $form->checkBoxRow($model,'obatalkes_aktif',array('checked'=>'obatalkes_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
