<?php
Yii::app()->clientScript->registerScript('search', "
$('#divSearch-form form').submit(function(){
	$.fn.yiiGridView.update('penawaran-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2">Informasi Permintaan Penawaran</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'penawaran-m-grid',
	'dataProvider'=>$modPermintaanPenawaran->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tglpenawaran',
                'nosuratpenawaran',
                array(
                   'name'=>'supplier_id',
                    'type'=>'raw',
                    'value'=>'$data->supplier->supplier_nama',
                ),
            //EHJ-1680
//                array(
//                  'header'=>'Total Harga Netto',
//                  'type'=>'raw',
//                  'value'=>'"Rp. ".MyFunction::formatNumber($data->harganettopenawaran)',
////                  'htmlOptions'=>array('style'=>'text-align:right;'),
//                ),
//                'harganettopenawaran',
                'keteranganpenawaran',
		array('header'=>'Perencanaan',
                      'type'=>'raw',
                      'value'=>'(!empty($data->rencanakebfarmasi_id) ? "Ya" : "Tidak") ',
                    ),
                array(
               'header'=>'Details',
               'type'=>'raw',
               'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/Details",array("idPermintaanPenawaran"=>$data->permintaanpenawaran_id)),
                            array("class"=>"", 
                                  "target"=>"iframe",
                                  "onclick"=>"$(\"#dialogDetails\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat details Permintaan Penawaran",
                            ))',
                ),
            array(
               'header'=>'Pembelian',
               'type'=>'raw',
               'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>","'.$this->createUrl("permintaanPembelian/index").'&idPenawaran=$data->permintaanpenawaran_id",
                            array("class"=>"", 
                                  "title"=>"Klik Mendaftarkan Ke Pembelian",
                            ))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<div id="divSearch-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'rencana-t-search',
        'type'=>'horizontal',
        'focus'=>'#GFPermintaanPenawaranT_nosuratpenawaran',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                    <?php echo CHtml::label('Tgl Permintaan','tglPermintaan', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPermintaanPenawaran,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPermintaanPenawaran,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div> 
        </td>
        <td>
              <?php echo $form->textFieldRow($modPermintaanPenawaran,'nosuratpenawaran',array('class'=>'numberOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
              <?php echo $form->dropDownListRow($modPermintaanPenawaran,'supplier_id',
                                                               CHtml::listData($modPermintaanPenawaran->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                               'empty'=>'-- Pilih --',)); ?>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?><?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>
<?php $this->endWidget(); ?>
    
</div>
<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action=$this->getAction()->getId();
$currentUrl=  Yii::app()->createUrl($module.'/'.$controller.'/'.$action);
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hiddenPembelian',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('target'=>'_new'),
        'action'=>Yii::app()->createUrl($module.'/permintaanPembelian/index'),
)); ?>
    <?php echo CHtml::hiddenField('idPenawaranForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('noPenawaranForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('tglPenawaranForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('currentUrl',$currentUrl,array('readonly'=>true));?>
<?php $this->endWidget(); ?>
<?php
// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogDetails',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Details Permintaan Penawaran',
                        'autoOpen'=>false,
                        'minWidth'=>900,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="iframe" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================

$js = <<< JSCRIPT
function formPembelian(idPenawaran,noPenawaran,tglPenawaran)
{
    $('#idPenawaranForm').val(idPenawaran);
    $('#noPenawaranForm').val(noPenawaran);
    $('#tglPenawaranForm').val(tglPenawaran);
    $('#form_hiddenPembelian').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);