<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
    <?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
  'id'=>'gfrencanaKebFarmasi-m-form',
  'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'return cekSupplier();'),
        'focus'=>'#GFPermintaanPenawaranT_supplier_id',
)); 
$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php echo $form->errorSummary($modPermintaanPenawaran); ?>
<style>
    .numbersOnly, .float{
        text-align: right;
    }
</style>
<fieldset>
<legend class="rim2">Transaksi Permintaan Penawaran</legend>
<fieldset>
    <legend class="rim">Data Permintaan Penawaran</legend>
    <table>
        <tr>
            <td>
                <?php echo $form->textFieldRow($modPermintaanPenawaran,'nosuratpenawaran',array('readonly'=>true,'class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)",));?>
                <?php echo $form->dropDownListRow($modPermintaanPenawaran,'supplier_id',
                                                             CHtml::listData($modPermintaanPenawaran->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                             array('class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'onChange'=>'setValue();', 'class'=>'idSupplier', 
                                                             'empty'=>'-- Pilih --',)); ?>
                 
                <?php echo CHtml::hiddenField('idSupplier', ''); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modPermintaanPenawaran,'tglpenawaran', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                            'model'=>$modPermintaanPenawaran,
                                            'attribute'=>'tglpenawaran',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                'maxDate'=>'d',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                            ),
                            )); ?>
                        </div>
                  </div>
            </td>
            <td>
                    
                     <?php echo $form->textAreaRow($modPermintaanPenawaran,'keteranganpenawaran',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",));?>

                     <?php echo $form->hiddenField($modPermintaanPenawaran,'harganettopenawaran',array('class'=>'span3', 'readonly'=>TRUE,));?>
            </td>
        </tr>
    </table>
      
    
    
   
</fieldset>
<?php if (!isset($modRencanaKebFarmasi->rencanakebfarmasi_id)) { ?>
<table>
    <tr>
        <td>
            <fieldset id="form1">
                <?php if ($modPermintaanPenawaran->isNewRecord){ ?>
                <!-- ===================== Form berdasarkan pemilihan obat ================================== -->
                <legend class="rim">Berdasarkan Pemilihan Obat</legend>
                <table>
                        <tr>
                            <td>
                                <div class="control-group ">
                                    <?php echo $form->labelEx($modPermintaanPenawaran,'obatAlkes', array('class'=>'control-label')) ?>
                                    <div class="controls">
                                         <?php echo CHtml::hiddenField('idObatAlkes');?>
                                        <?php 
                                            $this->widget('MyJuiAutoComplete', array(
                                                            'model'=>$modPermintaanPenawaran,
                                                            'attribute'=>'obatAlkes',
                                                            'source'=>'js: function(request, response) {
                                                                           $.ajax({
                                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/ObatAlkesPenawaran').'", //tdk menggunakan /ObatAlkes
                                                                               dataType: "json",
                                                                               data: {
                                                                                   term: request.term,
                                                                                   idSupplier: $("#idSupplier").val(),
                                                                               },
                                                                               success: function (data) {
                                                                                       response(data);
                                                                               }
                                                                           })
                                                                        }',
                                                             'options'=>array(
                                                                   'showAnim'=>'fold',
                                                                   'minLength' => 2,
                                                                   'select'=>'js:function( event, ui ) {
                                                                        $(this).val( ui.item.label);
                                                                        $("#idObatAlkes").val(ui.item.obatalkes_id);
                                                                        
                                                                        return false;
                                                                    }',
                                                            ),
                                                            'htmlOptions'=>array('class'=>'span3','disabled'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                                            'tombolDialog'=>array('idDialog'=>'dialogObatalkesM'), //dialogObatsupplierM akak di switch otomastis jika berdasarkanSupplier di ceklis
                                                        )); 
                                        ?>
                                    </div> 
                                    <div class="controls"> 
                                        <?php echo CHtml::checkBox('berdasarkanSupplier', false,array('onclick'=>'setValue();', 'title'=>'Filter Obat Berdasarkan Supplier', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        <?php echo $form->label($modPermintaanPenawaran,'Cek Jika Ingin Menampilakan obat dan alkes berdasarkan supplier'); ?>
                                    </div>
                                </div>
                                 
                                
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                                <div class="control-group ">
                                    <?php echo CHtml::label('Qty (Satuan Besar)','Qty', array('class'=>'control-label'));?>       
                                    <div class="controls">
                                         <?php echo CHtml::textField('qtyObat','1',array('class'=>'span1',"onkeyup"=>"numberOnly(this);", 'onkeypress'=>"return $(this).focusNextInputField(event)"));?>
                                <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                    array('onclick'=>'submitObat();return false;',
                                          'class'=>'btn btn-primary',
                                          'onkeypress'=>"submitObat();return $(this).focusNextInputField(event)",
                                          'rel'=>"tooltip",
                                          'id'=>'tambahObat',  
                                          'title'=>"Klik Untuk Menambahkan Obat",)); ?>      
                                    </div>
                                </div>
                                                     
                            </td>
                            
                        </tr>
                    </table>

             </fieldset>
            <?php } ?>
            <!-- ==================== Akhir form berdasarkan pemilihan obat ==================================== -->
            
            <!-- ==================== Form berdasarkan perencanaan ==================================== -->
            <fieldset id="form2" class="hide">
                <legend class="rim">Berdasarkan Perencanaan</legend>
                <table>
                    <tr>
                        <?php $ceklis = (!empty($modDetailRencana)) ? true : false; ?>
                        <td><i class="icon-pencil"></i> <?php echo CHtml::checkBox('checkBoxrencana',$ceklis,array('onclick'=>'checkBoxRencana(this)'));?></td>
                        <td><?php echo $form->labelEx($modRencanaKebFarmasi,'noperencnaan', array('style'=>'width : 90px')) ?></td>
                        <td>
                            <?php echo CHtml::hiddenField('idPerencanaan');?>
                            <?php $this->widget('MyJuiAutoComplete',array(
                                    'model'=>$modRencanaKebFarmasi,
                                    'attribute'=>'noperencnaan',
                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/noPerencanaan'),
                                    'options'=>array(
                                       'showAnim'=>'fold',
                                       'minLength' => 2,
                                       'select'=>'js:function( event, ui ) {
                                                  $("#idPerencanaan").val(ui.item.rencanakebfarmasi_id);
                                                  $("#'.CHtml::activeId($modRencanaKebFarmasi,'tglperencanaan').'").val(ui.item.tglperencanaan);
                                                  submitRencana();    
                                        }',
                                    ), 
                                    'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                                         'class'=>'span3',
                                                         'readonly'=>TRUE ), 
                                    'tombolDialog'=>array('idDialog'=>'dialogPerencanaan'),
                            )); ?>
                                <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                            //array('onclick'=>'$("#dialogPerencanaan").dialog("open");return false;',
                              //    'class'=>'btn btn-primary',
                                 // 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                //  'rel'=>"tooltip",
                                 // 'title'=>"Klik Untuk Rencana Kebutuhan Farmasi Lebih Lanjut",
                                 // 'id'=>'buttonPerencanaan', 
                                 // 'disabled'=>TRUE)); ?>

                            <?php echo $form->textFieldRow($modRencanaKebFarmasi,'tglperencanaan',array('class'=>'span3','readonly'=>TRUE,'onkeypress'=>"return $(this).focusNextInputField(event)",));?>
                            </td>
                            </tr>
                            </table>
             </fieldset>
            <!-- ==================== Akhir form berdasarkan perencanaan ==================================== -->
        </td>
    </tr>
</table>
<?php } ?>
</fieldset>
<?php if (isset($modDetails)) { 
    echo $form->errorSummary($modDetails);
}
    ?>
<table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>No.Urut</th>
            <th>Kategori /<br/> Nama Obat</th>
            <th>Asal Barang</th>
            <th>Stok Akhir</th>
            <th>Jumlah Kemasan<br>(Satuan Kecil/Besar)</th>
            <th>Jumlah Permintaan<br>(Satuan Besar)</th>
            <!-- EHJ-984 -->
<!--            <th>Harga Kemasan</th>
            <th>Sub Total</th>-->
            <!-- END EHJ-984 -->
            <?php echo (($modPermintaanPenawaran->isNewRecord) ? "<th>Batal</th>" : ""); ?>
        </tr>
    </thead>
    <tbody>
        <?php 
        if (isset($modDetails)) {
            if (count($modDetails) > 0) {
                $no = 1;
                foreach($modDetails as $i=>$rencana): 
                    $modObatAlkes=ObatalkesM::model()->findByPk($rencana->obatalkes_id);
                    $modStokObatAlkes=StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$rencana->obatalkes_id));
                    $stokAkhir = 0;
                    $maxStok = 0;
                    foreach($modStokObatAlkes AS $tampil):
//                        $stokAkhir = $stokAkhir + $tampil['qtystok_in'];
                        $stokAkhir = $stokAkhir + $tampil['qtystok_current'];
                        $maxStok = $maxStok + $tampil['qtystok_out'];
                    endforeach;
                    $subtotal = $rencana->qty *  $rencana->hargabelibesar;
                    ?>
                <tr>
                    <td><?php 
                          echo CHtml::textField('noUrut',$no++,array('class'=>'span1 noUrut','readonly'=>TRUE));
                          echo CHtml::activeHiddenField($rencana, '['.$i.']satuankecil_id');
                          echo CHtml::activeHiddenField($rencana, '['.$i.']satuanbesar_id');
                          echo CHtml::activeHiddenField($rencana, '['.$i.']sumberdana_id');
                          echo CHtml::activeHiddenField($rencana, '['.$i.']obatalkes_id',array('class'=>"obatAlkes"));
                        ?>
                    </td>
                    <td><?php echo $modObatAlkes->obatalkes_kategori ?>/<br/><?php echo $modObatAlkes->obatalkes_nama ?></td>
                    <td><?php echo $modObatAlkes->sumberdana->sumberdana_nama ?></td>
                    <td><?php echo $stokAkhir." ".$modObatAlkes->satuankecil->satuankecil_nama; ?></td>
                    <td><?php echo CHtml::activeTextField($rencana, '['.$i.']jmlkemasan',array('class'=>'span1 numbersOnly qtykecil','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    <?php echo $rencana->satuankecil->satuankecil_nama."/".$rencana->satuanbesar->satuanbesar_nama; ?>
                    </td>
                    <td><?php echo CHtml::activeTextField($rencana, '['.$i.']qty',array('class'=>'span1 numbersOnly qty','onkeyup'=>'hitungSubtotal(this)','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    <?php echo $rencana->satuanbesar->satuanbesar_nama; ?><?php echo CHtml::activeHiddenField($rencana, '['.$i.']hargabelibesar',array('class'=>'span1 numbersOnly','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)", 'onkeyup'=>'hitungHargaNetto(this);hitungSubtotal(this);'))
                    .CHtml::activeHiddenField($rencana, '['.$i.']harganetto',array('class'=>'span1 numbersOnly netto','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    <?php echo CHtml::hiddenField('subtotal', $subtotal, array('class'=>'span2 subTotal','readonly'=>TRUE)) ?></td>
<!--                    <td></td>-->
                    <?php echo (($modPermintaanPenawaran->isNewRecord) ? "<td>".Chtml::link('<icon class="icon-remove"></icon>', '', array('onclick'=>'remove_obat(this);', 'style'=>'cursor:pointer;', 'class'=>'cancel'))."</td>" : ""); ?>
                </tr>
        <?php endforeach; } }?>
    </tbody>
</table>

<div class="form-actions">
        <?php 
         $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
         $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
         $action=$this->getAction()->getId();
         $currentAction=  Yii::app()->createUrl($module.'/'.$controller.'/'.$action);
         $urlRencana=Yii::app()->createUrl($module.'/RencanaKebutuhan/search');
                     
if (!$modPermintaanPenawaran->isNewRecord){
    $urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idPenawaran'=>$modPermintaanPenawaran->permintaanpenawaran_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}else{
    echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'cekValidasi()'));
    echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan', 'style'=>'display:none;'));
}
?>
                           
         <?php if (isset($_POST['idPerencanaanForm'])){//Jika Masuk ke Halaman ini memalui Informasi Rencana kebutuhan
                    echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                           $_POST['currentUrl'], 
                            array('class'=>'btn btn-danger',));
               }else{
                    echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                           $currentAction, 
                            array('class'=>'btn btn-danger',));   
                }  
         ?>
       <?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
      
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
checkRencana();
</script>


<?php
$urlGetObatAlkes = Yii::app()->createUrl('actionAjax/getObatAlkesPermintaanPenawaran');//MAsukan Dengan memilih Obat Alkes
$urlGetSupplier = Yii::app()->createUrl('actionAjax/getSupplier');//MAsukan Dengan memilih Perencanaa
$hargaNettoPenawaran=CHtml::activeId($modPermintaanPenawaran,'harganettopenawaran');
$obatAlkes=CHtml::activeId($modPermintaanPenawaran,'obatAlkes');
$noPerencanaan = CHtml::activeId($modRencanaKebFarmasi,'noperencnaan');
$tglPerencanaan =CHtml::activeId($modRencanaKebFarmasi,'tglperencanaan');
$urlHalamanIni = Yii::app()->createUrl($this->route);
$clearObatalkes = ($modPermintaanPenawaran->isNewRecord && (!isset($modPermintaanPenawaran->rencanakebfarmasi_id))) ? '$("#tableObatAlkes tbody").html("");': '';
$idSupplier = CHtml::activeId($modPermintaanPenawaran, 'supplier_id');
$inputObat = CHtml::activeId($modPermintaanPenawaran, 'obatAlkes');
$jscript = <<< JS

function setValue()
{
    id = $("#${idSupplier}").val();
    $('#${obatAlkes}').val('');
    $('#idObatAlkes').val('');
    //$//{clearObatalkes} 
    if(id == ""){
        $("#berdasarkanSupplier").removeAttr("checked");
        alert("Anda belum memilih Supplier !");
    }
    
    if($("#berdasarkanSupplier").is(":checked")){
        $("#idSupplier").val(id);
        //switch to dialogObatsupplierM
        $("#${inputObat}").parent().find("a").removeAttr("onclick");
        $("#${inputObat}").parent().find("a").attr("onclick",'$(\"#dialogObatsupplierM\").dialog(\"open\");return false;');
        $.post("${urlGetSupplier}", {idSupplier:id},
        function(data){
           $('#idSupplier').val(data.supplier_id);
           $('#idSupplier2').val(data.supplier_id);
//         ERROR INPUT number >>  $(".numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
           $.get('${urlHalamanIni}', {idSupplier:data.supplier_id}, function(datas){
                $.fn.yiiGridView.update('ObatsupplierM-m-grid', {
                    url: document.URL+'&ObatsupplierM%5Bsupplier_id%5D='+data.supplier_id,
                }); 
            });

        }, "json");
        
    }else{
        id = "";
        $("#idSupplier").val(id);
        //switch to dialogObatAlkesM
        $("#${inputObat}").parent().find("a").removeAttr("onclick");
        $("#${inputObat}").parent().find("a").attr("onclick",'$(\"#dialogObatalkesM\").dialog(\"open\");return false;');
    }
    
    
}
           
function checkRencana()
{
    if($('#checkBoxrencana').is(':checked')){
        $('#GFPermintaanPenawaranT_obatAlkes').attr('readonly','TRUE');
        $('#qtyObat').attr('readonly','TRUE');
        $('#buttonPemilihanObat').attr('disabled','TRUE');
        $('#tambahObat').attr('disabled','TRUE');

        $('#GFRencanaKebFarmasiT_noperencnaan').removeAttr('readonly','TRUE');
        $('#buttonPerencanaan').removeAttr('disabled','TRUE');
    }
}


function checkBoxRencanaclick(obj)
{
    hapusObat();
    if(obj.checked==true){
        $('#GFPermintaanPenawaranT_obatAlkes').attr('readonly','TRUE');
        $('#qtyObat').attr('readonly','TRUE');
        $('#buttonPemilihanObat').attr('disabled','TRUE');
        $('#tambahObat').attr('disabled','TRUE');

        $('#GFRencanaKebFarmasiT_noperencnaan').removeAttr('readonly','TRUE');
        $('#buttonPerencanaan').removeAttr('disabled','TRUE');
        $('#formpemilihanobat').hide();
        $('#formperencanaan').show();
    }else{
        $('#GFPermintaanPenawaranT_obatAlkes').removeAttr('readonly','TRUE');
        $('#qtyObat').removeAttr('readonly','TRUE');
        $('#buttonPemilihanObat').removeAttr('disabled','TRUE');
        $('#tambahObat').removeAttr('disabled','TRUE');
        $('#checkBoxrencana').attr('readonly',true);

        $('#GFRencanaKebFarmasiT_noperencnaan').attr('readonly','TRUE');
        $('#buttonPerencanaan').attr('disabled','TRUE');
        $('#tambahObat').removeAttr('disabled','TRUE');
        $('#formpemilihanobat').show();
        $('#formperencanaan').hide();
    }
}

function hapusObat()
{
    banyakRow=$('#tableObatAlkes tr').length;

    for(i=2; i<=banyakRow; i++){
    $('#tableObatAlkes tr:last').remove();
    }
    
    $('#${hargaNettoPenawaran}').val(0);
}

function cekValidasi()
{   
  
  banyaknyaObat = $('.obatAlkes').length;
  hargaNettoPenawaran =  $('#PenawarandetailT_harganetto').val(); 
  if ($('.isRequired').val()==''){
    alert ('Harap Isi Semua Data Yang Bertanda *');
  }else if(banyaknyaObat<1){
     alert('Anda belum memilih Obat Yang Akan Diminta');   
  }else if(hargaNettoPenawaran==0){
     alert('Anda Belum memilih Obat Yang Akan Diminta');   
  }else{
     $('#btn_simpan').click();
  }
}
             

function submitObat()
{
    
    idObat = $('#idObatAlkes').val();
    qtyObat = parseFloat($('#qtyObat').val());
    idSupplier = $('#GFPermintaanPenawaranT_supplier_id').val();
    
    if(idObat==''){
        alert('Silahkan Pilih Obat Terlebih Dahulu');
    }else{
        obat = $("#tableObatAlkes tbody").find(".obatAlkes[value="+idObat+"]");
        jumlah =  obat.length;
        if (jumlah == 0){
            $.post("${urlGetObatAlkes}", { idObat: idObat, qtyObat:qtyObat , idSupplier : idSupplier},
            function(data){
                $('#tableObatAlkes').append(data.tr);
                $('#tableObatAlkes').find(".integer").maskMoney({thousands:"", decimal:""});
                hitungUrutan();
            }, "json");
        }else{
            value = parseFloat(obat.parents("tr").find(".qty").val());
            value = parseFloat(value+qtyObat)
            obat.parents("tr").find(".qty").val(value);
            hitungUrutan();
        }
        $('#${obatAlkes}').val('');
        $('#idObatAlkes').val('');
        $('#qtyObat').val('1');
        
    }
        hitungSemuaTotal();
}

function submitRencana()
{
     
     idPerencanaan = $('#idPerencanaan').val();
                   
        if(idPerencanaan==''){
            alert('Silahkan Pilih Rencana Terlebih Dahulu');
        }else{
            $.post("${urlGetObatAlkesDariRencana}", { idPerencanaan: idPerencanaan },
            function(data){
                $('#tableObatAlkes tbody').html(data.tr);
//                $('#${noPerencanaan}').val('');
                $('#idPerencanaan').val('');
                hitungJumlahHargaDiskon();
                //hitungSemua();
                hitungSemuaTotal();
                $('#buttonPerencanaan').attr('disabled','TRUE');
            }, "json");
        }   
}


function submitRencanaDariInformasi(idPerencanaanF,noPerencanaanF,tglPerencanaanF)
{
        idPerencanaan = idPerencanaanF;
        if(idPerencanaan==''){
            alert('Silahkan Pilih Rencana Terlebih Dahulu');
        }else{
            alert('teeeeest');
            $('#form2').show();
            $('#checkBoxrencana').attr('readonly',false);
            $('#form1').hide();
            $.post("${urlGetObatAlkesDariRencana}", { idPerencanaan: idPerencanaan },
            function(data){
                $('#tableObatAlkes').append(data.tr);
                $('#${noPerencanaan}').val(noPerencanaanF);
                $('#${tglPerencanaan}').val(tglPerencanaaanF);
                $('#checkBoxrencana').attr('checked','checked');
                //hitungJumlahHargaDiskon();
                //hitungSemua();
                hitungSemuaTotal();
                $('#buttonPerencanaan').attr('disabled','TRUE');
                $('#GFPermintaanPenawaranT_obatAlkes').attr('readonly','TRUE');
                $('#qtyObat').attr('readonly','TRUE');
                $('#buttonPemilihanObat').attr('disabled','TRUE');
                $('#tambahObat').attr('disabled','TRUE');
            }, "json");
        }
}
                
function numberOnly(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;


    if (d == 'decimal' || d=='0') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }
    
    if (value == '0'){
    $(obj).val(1);
    }else if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }else{
        $(obj).val(1);
    }
}

function numberOnlyNol(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;


    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }else{
    $(obj).val(0);
    }
}

function hitungJumlahHargaDiskon()
{
    besarDiskon =$('#diskon').val();
    besarDiskonINT=parseFloat(besarDiskon);
    banyakNetto =$('.netto').length;
    totalNetto=0;
    $('.netto').each(function() {
           hargaNettoProduk=parseFloat($(this).val());
           totalNetto=totalNetto+hargaNettoProduk;
           qty = parseFloat($(this).parent().prev().find('input').val());
           $(this).parent().next().next().next().next().find('input').val(besarDiskonINT);
           persenDiskonObat=$(this).parent().next().next().next().next().find('input').val();
           jumlahDiskonObat=(hargaNettoProduk*(persenDiskonObat/100)) * qty;
           $(this).parent().next().next().next().next().next().find('input').val(jumlahDiskonObat);
        });
    jumlahDiskonHarga = (totalNetto *(besarDiskonINT/100)) / qty;
    $('#jumlahDiskon').val(jumlahDiskonHarga);
    //hitungSemua();
    hitungSemuaTotal();
    

}

function hitungSubtotal(obj)
{
  qty = parseFloat(obj.value);
  hargaNettoProduk = parseFloat($(obj).parents("tr").find(".netto").val());
  subtotalPerProduk = qty * hargaNettoProduk;   
  if (jQuery.isNumeric(subtotalPerProduk)){
    $(obj).parents("tr").find(".subTotal").val(subtotalPerProduk);   
  }
  //hitungSemua();
  hitungSemuaTotal();
}  
                
function hitungSemua() //untuk Meghitung Total Diskon dan Total Jumlah Diskon dari inputan yang ada di tabel
{      
    
     total =0;
     $('.subTotal').each(function() {
            netto = parseFloat($(this).parents("tr").find(".netto").val());
            qty = parseFloat($(this).parents("tr").find(".qtykecil").val());
            value = netto*qty;
            if (jQuery.isNumeric(value)){
                 $(this).val(value);
                 total += value;
            }
     });
     
     $('#${hargaNettoPenawaran}').val(total);
    
}

function hitungUrutan(){
    noUrut = 1;
     $('.noUrut').each(function() {
          $(this).val(noUrut);
          noUrut++;
     });
}



JS;
Yii::app()->clientScript->registerScript('masukanobat',$jscript, CClientScript::POS_HEAD);
?>
<?php 
//========= Dialog buat cari data ObatalkesM=========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatalkesM',
    'options'=>array(
        'title'=>'Pencarian Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatalkesM('search');
$modObatAlkes->unsetAttributes();
if(isset($_GET['ObatalkesM'])) {
    $modObatAlkes->attributes = $_GET['ObatalkesM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ObatalkesM-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modObatAlkes->search(),
	'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectObat",
                                    "onClick" => "
                                                  $(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                  $(\"#'.CHtml::activeId($modPermintaanPenawaran,'obatAlkes').'\").val(\"$data->obatalkes_nama\");
                                                  $(\"#dialogObatalkesM\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes_kategori',
                ),
                array(
                    'header'=>'Golongan Obat',
                    'value'=>'$data->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_kode'),
                    'value'=>'$data->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes_nama',
                ),
                array(
                    'header'=>'Asal Barang',
                    'value'=>'$data->sumberdana->sumberdana_nama',
                ),
                array(
                    'header'=>'Kadar Obat',
                    'value'=>'$data->obatalkes_kadarobat',
                ),
                array(
                    'header'=>'Kemasan Besar',
                    'value'=>'$data->kemasanbesar',
                ),
                array(
                    'header'=>'Kekuatan',
                    'value'=>'$data->kekuatan',
                ),
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->tglkadaluarsa',
                ),
            ),
            'afterAjaxUpdate' => 'function(id, data){
            jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
        ));
$this->endWidget();
//========= end ObatalkesM dialog =============================
?>
<?php 
//========= Dialog buat cari data ObatsupplierM =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatsupplierM',
    'options'=>array(
        'title'=>'Pencarian Obat Supplier',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatsupplierM('search');
$modObatAlkes->unsetAttributes();
//$modObatAlkes->supplier_id = $var['supplier_id']; << FATAL ERROR
$modObatAlkes->obatalkes->supplier_id = $var['supplier_id'];
if(isset($_GET['ObatsupplierM'])) {
    $modObatAlkes->attributes = $_GET['ObatsupplierM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ObatsupplierM-m-grid',
        'id'=>'ObatsupplierM-m-grid',
        'dataProvider'=>$modObatAlkes->searchObatSupplierGF(),
          'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectPasien",
                                    "onClick" => "$(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                  $(\"#'.CHtml::activeId($modPermintaanPenawaran,'obatAlkes').'\").val(\"".$data->obatalkes->obatalkes_nama."\");
                                                  $(\"#dialogObatsupplierM\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'Supplier',
                    'filter'=>CHtml::activeHiddenField($modObatAlkes, 'supplier_id', array('id'=>'idSupplier2')),
                    'value'=>'$data->supplier->supplier_nama',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes->obatalkes_kategori',
                ),
                array(
                    'header'=>'Golongan Obat',
                    'value'=>'$data->obatalkes->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_kodeobat'),
                    'value'=>'$data->obatalkes->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes->obatalkes_nama',
                ),
                array(
                    'header'=>'Asal Barang',
                    'value'=>'$data->obatalkes->sumberdana->sumberdana_nama',
                ),
                array(
                    'header'=>'Kadar Obat',
                    'value'=>'$data->obatalkes->obatalkes_kadarobat',
                ),
                array(
                    'header'=>'Kemasan Besar',
                    'value'=>'$data->obatalkes->kemasanbesar',
                ),
                array(
                    'header'=>'Kekuatan',
                    'value'=>'$data->obatalkes->kekuatan',
                ),
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->obatalkes->tglkadaluarsa',
                ),
  ),
        'afterAjaxUpdate' => 'function(id, data){
        $("#ObatsupplierM_supplier_id").val($("#idSupplier").val());
        jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
$this->endWidget();
//========= end ObatsupplierM dialog =============================
?>

<?php 
//========= Dialog buat Rencana Kebutuhan obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPerencanaan',
    'options'=>array(
        'title'=>'Pencarian Rencana kebutuhan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));
$format = new CustomFormat();
$modRencanaKebFarmasi = new GFRencanaKebFarmasiT('searchPermintaanPembelian');
$modRencanaKebFarmasi->unsetAttributes();
if(isset($_GET['GFRencanaKebFarmasiT'])) {
    $modRencanaKebFarmasi->attributes = $_GET['GFRencanaKebFarmasiT'];
    $modRencanaKebFarmasi->tglperencanaan=$format->formatDateMediumForDB($_GET['GFRencanaKebFarmasiT']['tglperencanaan']);
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
  'id'=>'rencanakebutuhan-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
  'dataProvider'=>$modRencanaKebFarmasi->searchPermintaanPembelian(),
  'filter'=>$modRencanaKebFarmasi,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
  'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectPasien",
                                    "onClick" => "$(\"#idPerencanaan\").val(\"$data->rencanakebfarmasi_id\");
                                                  $(\"#'.CHtml::activeId($modRencanaKebFarmasi,'noperencnaan').'\").val(\"$data->noperencnaan\");
                                                  submitRencana();
                                                  $(\"#'.CHtml::activeId($modRencanaKebFarmasi,'tglperencanaan').'\").val(\"$data->tglperencanaan\");
                                                  $(\"#dialogPerencanaan\").dialog(\"close\");    
                                        "))',
                ),
                'noperencnaan',
                array(
            'name' => 'tglperencanaan',
            'filter' =>$this->widget('MyDateTimePicker',array(
                                            'model'=>$modRencanaKebFarmasi,
                                            'attribute'=>'tglperencanaan',
                                            'mode'=>'date',
                                            'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT,

                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker1',
                                            ),
                                ),true), 
                    ),
                
  ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rencana dialog =============================
?>


<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => '',
        'thousands' => '',
        'precision' => 0,
    )
));
?>
<script>
//Created by: ichan90@yahoo.co.id
function hitungHargaNetto(obj){
     var jmlKemasan = unformatNumber($(obj).parent().parent().find("input[name$='[jmlkemasan]']").val());
     var hargaBeliBesar = unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
     var hargaNetto = hargaBeliBesar / jmlKemasan;
     $(obj).parent().parent().find("input[name$='[harganetto]']").val(hargaNetto.toFixed(2));
 }

//    Perhitungan subtotal efektif bisa digunakan di field mana saja selama satu baris / <tr> 
function hitungSubtotal(obj){
    var jmlPermintaan = unformatNumber($(obj).parent().parent().find("input[name$='[qty]']").val());
    var hargaBeliBesar =  unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
    var subtotal = (jmlPermintaan * hargaBeliBesar);
    $(obj).parent().parent().find("#subtotal").val(subtotal.toFixed(0));
    hitungSemuaTotal();
 }
 function cekSupplier(){
     var idSupplier = $("#GFPermintaanPenawaranT_supplier_id").val();
     if(!idSupplier){
         alert ('Silahkan Isi Supplier Terlebih Dahulu !');
         return false;
     }
     return true;
 }
function hitungSemuaTotal(){
    var total = 0;
    $(".subTotal").each(function(){
        total += parseFloat($(this).parents("tr").find("#subtotal").val());
    });
    $("#GFPermintaanPenawaranT_harganettopenawaran").val(total);
} 

/**
* @author : Hardi
* Issue : EHJ-1910
*/

function remove_obat(obj) {
    $(obj).parents("#tableObatAlkes tbody tr").remove();
}

hitungSemuaTotal(); //hitung saat load halaman
</script>