<table width="100%">
    <tr>
        <td width="50%">
             <b><?php echo CHtml::encode($modPermintaanPenawaran->getAttributeLabel('nosuratpenawaran')); ?>:</b>
            <?php echo CHtml::encode($modPermintaanPenawaran->nosuratpenawaran); ?>
            <br />
             <b><?php echo CHtml::encode($modPermintaanPenawaran->getAttributeLabel('tglpenawaran')); ?>:</b>
            <?php echo CHtml::encode($modPermintaanPenawaran->tglpenawaran); ?>
            <br />
        </td>
        <td>
             <b><?php echo CHtml::encode($modPermintaanPenawaran->getAttributeLabel('supplier_id')); ?>:</b>
            <?php echo CHtml::encode($modPermintaanPenawaran->supplier->supplier_nama); ?>
            <br />
             <b><?php echo CHtml::encode($modPermintaanPenawaran->getAttributeLabel('create_time')); ?>:</b>
            <?php echo CHtml::encode($modPermintaanPenawaran->create_time); ?>
            <br />
        </td>
    </tr>   
</table>
<hr/>
<table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Jumlah Kemasan</th>
        <th>Qty</th>
        <!-- EHJ-1680 -->
<!--        <th>Harga Kemasan</th>
        <th>Sub Total</th>-->
        
    </tr>
    </thead>
    <tbody>
    <?php
    $no=1;
    $totalSubTotal = 0;
    if (count($modPermintaanPenawaranDetails) > 0){
        $totalQty = 0;
        $totalNetto = 0;
        $totalSubTotal = 0;
        foreach($modPermintaanPenawaranDetails AS $tampilData):
            $subTotal = $tampilData['qty']*$tampilData['hargabelibesar'];
            echo "<tr>
                    <td>".$no."</td>
                    <td>".$tampilData->sumberdana['sumberdana_nama']."</td>  
                    <td>".$tampilData->obatalkes['obatalkes_kategori']."<br>".$tampilData->obatalkes['obatalkes_nama']."</td>   
                    <td class='right-align'>".MyFunction::formatNumber($tampilData['jmlkemasan'])." ".$tampilData->satuankecil['satuankecil_nama']."/".$tampilData->satuanbesar['satuanbesar_nama']."</td>
                    <td class='right-align'>".MyFunction::formatNumber($tampilData['qty'])." ".$tampilData->satuanbesar['satuanbesar_nama']."</td>                    
                 </tr>";  
            $no++;
            $totalQty+=$tampilData['qty'];
            $totalNetto+=$tampilData['harganetto'];
            $totalSubTotal+=$subTotal;
            // EHJ-1680
//            <td class='right-align'>Rp. ".MyFunction::formatNumber($tampilData['hargabelibesar'])."</td>     
//                    <td class='right-align'>Rp. ".MyFunction::formatNumber($subTotal)."</td>     
        endforeach;
    }else{
         echo '<tr><td colspan=5>'.Yii::t('zii','No results found.').'</td></tr>';
    } ?>
        <tbody><tfoot>
            <!-- EHJ-1680 -->
    <?php // echo "<tr>
//            <td colspan='6'>Total</td>
//            <td class='right-align'>Rp. ".MyFunction::formatNumber($totalSubTotal)."</td>
//         </tr>";  
//    <td class='right-align'>".MyFunction::formatNumber($totalQty)."</td>
//            <td class='right-align'>Rp. ".MyFunction::formatNumber($totalNetto)."</td>
    ?>
            </tfoot>
</table>

<?php
if (!isset($caraPrint)){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idPenawaran'=>$modPermintaanPenawaran->permintaanpenawaran_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}
?>    
