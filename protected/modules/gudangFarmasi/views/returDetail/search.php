<?php
Yii::app()->clientScript->registerScript('search', "
$('#divSearch-form form').submit(function(){
	$.fn.yiiGridView.update('retur-detail-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
<legend class="rim2">Informasi Retur Pembelian</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'retur-detail-t-grid',
	'dataProvider'=>$model->searchRetur(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Nama Obat Alkes',
                    'value'=>'$data->obatalkes->obatalkes_nama',
                ),
                array(
                    'header'=>'Tanggal Retur',
                    'value'=>'$data->retur->tglretur',
                ),
                array(
                    'header'=>'No Faktur',
                    'value'=>'$data->fakturdetail->fakturpembelian->nofaktur',
                ),
		//'sumberdana_id',
		array(
                    'header'=>'Alasan Retur',
                    'value'=>'$data->retur->alasanretur',
                ),
                array(
                    'header'=>'No Retur',
                    'value'=>'$data->retur->noretur',
                ),
		'jmlretur',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

</fieldset>

<div id="divSearch-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'gfretur-detail-t-search',
        'type'=>'horizontal',
)); ?> 
<fieldset>
    <legend class="rim">Pencarian</legend>
    <table>
        <tr>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Tgl Retur','', array('class'=>'control-label'));?>
                    <div class="controls">
                        <?php
                            $this->widget(
                                        'MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tglAwal',
                                        'mode'=>'datetime',
                                        'options'=>array('dateFormat'=>  Params::DATE_TIME_FORMAT,),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3','onkeypress'=>"return $(this).focusNextInputField(event"),  
                                        )
                                    );
                        ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label'));?>
                    <div class="controls">
                        <?php
                            $this->widget(
                                        'MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tglAkhir',
                                        'mode'=>'datetime',
                                        'options'=>array('dateFormat'=>  Params::DATE_TIME_FORMAT,),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3','onkeypress'=>"return $(this).focusNextInputField(event"),  
                                        )
                                    );
                        ?>
                    </div>
                </div>
            </td>
            <td>
                <?php echo $form->textFieldRow($model,'namaObat'); ?>
                <?php echo $form->textFieldRow($model,'noRetur'); ?>
                <?php echo $form->textFieldRow($model,'noFaktur'); ?>
            </td>
        </tr>
    </table>
    
    <div class="form-actions">
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?><?php
            $content = $this->renderPartial('../tips/informasi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
         ?>
    </div>
</fieldset>
    <?php $this->endWidget(); ?>
</div>