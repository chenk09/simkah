<tr>
	<td>
		<?php echo CHtml::TextField('noUrut',$no_urut,array('class'=>'span1 noUrut','readonly'=>TRUE));?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']obatalkes_id',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']satuankecil_id',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']sumberdana_id',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']satuanbesar_id',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']stokakhir',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']maksimalstok',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']minimalstok',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']tglkadaluarsa',array());?>
	</td>
	<td><?php echo $modObatAlkes->obatalkes_nama;?></td>
	<td>
		<?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']jmlkemasan',array('class'=>'span1 angka','onkeyup'=>'hitung_total_alkes(this)','readonly'=>false));?>
		<?php echo $modObatAlkes->satuankecil->satuankecil_nama ."/". $modObatAlkes->satuanbesar->satuanbesar_nama; ?>
	</td>
	<td>
		<?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']jmlpermintaan',array('class'=>'span1 angka','onkeyup'=>'hitung_total_alkes(this)','readonly'=>false));?>
		<?php echo $modObatAlkes->satuanbesar->satuanbesar_nama?>
	</td>
	<td>
		<?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']harganettoper',array('class'=>'span2 angka currency','onkeyup'=>'hitung_total_alkes(this)','readonly'=>FALSE));?>
		<?php echo CHtml::activehiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']hargabelibesar', array('class'=>'span2'));?>
	</td>
	<td><?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']hargappnper',array('class'=>'span1 ppn', 'readonly'=>true));?></td>
	<td><?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']hargapphper',array('class'=>'span1 pph', 'readonly'=>true));?></td>
	<td><?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']persendiscount',array('class'=>'span1', 'readonly'=>true));?>
	<td><?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']jmldiscount',array('class'=>'span2', 'readonly'=>true));?></td>
	<td><?php
		$subTotal = ($modPermintaanDetail->jmlpermintaan * $modPermintaanDetail->jmlkemasan) * $modPermintaanDetail->harganettoper;
		echo CHtml::textField('PermintaandetailT['. $modObatAlkes->obatalkes_id .'][subTotal]', $subTotal, array('class'=>'span2 subTotal currency','readonly'=>true));
	?></td>
</tr>