<?php
$url_print = Yii::app()->createUrl('gudangFarmasi/permintaanPembelian/print', array(
	'idPembelian'=>$modPermintaanPembelian->permintaanpembelian_id,
	'caraPrint'=>'PRINT'
));

$controller = Yii::app()->controller->id;
$module = Yii::app()->controller->module->id;
$action = "batal";
$url_batal = Yii::app()->createUrl($module.'/'.$controller.'/'.$action, array(
	'permintaanpembelian_id'=>$modPermintaanPembelian->permintaanpembelian_id
));

$jscript = <<< JS
function custom_format()
{   
	$('.currency').each(function(){
		var value = formatNumber($(this).val());
		$(this).val(value);
	});
}

function print_faktur()
{   
	window.open("{$url_print}","",'location=_new, width=900px');
}

function batal_faktur()
{ 
	var r = confirm("Apakah yakin akan dibatalkan");
	if (r == true){
		window.location.href = "{$url_batal}";
	} 	
	return false;
}

setTimeout(function(){
	custom_format();
}, 500);

JS;
Yii::app()->clientScript->registerScript('masukanobat',$jscript, CClientScript::POS_HEAD);

$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'gfrencanaKebFarmasi-m-form',
		'enableAjaxValidation'=>false,
		'type'=>'horizontal',
		'htmlOptions'=>array(
			'enctype'=>"multipart/form-data",
			'onKeyPress'=>'return disableKeyPress(event)',
		),
		'focus'=>'#GFPermintaanPembelianT_supplier_id',
	)
); 
$this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
<legend class="rim2">Transaksi Pemintaan Pembelian</legend>
<fieldset>
		<legend class="rim"> Data Permintaan Pembelian</legend>
		<table>
			<tr>
				<td>
					 <?php
						echo $form->dropDownListRow($modPermintaanPembelian,'supplier_id',
							CHtml::listData(
								SupplierM::model()->findAll("supplier_aktif=TRUE AND supplier_jenis ilike '%".PARAMS::DEFAULT_JENIS_SUPPLIER."%' ORDER BY supplier_nama")
							,'supplier_id', 'supplier_nama'),
							array(
								'disabled'=>true
							)
						);
					?>
					<?php
						echo $form->dropDownListRow($modPermintaanPembelian,'syaratbayar_id',
							CHtml::listData($modPermintaanPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
							array(
								'class'=>'span3',
								'disabled'=>true
							)
					);?>
					<?php echo $form->textFieldRow($modPermintaanPembelian,'nopermintaan',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",));?>
					<div class="control-group ">
						<?php echo $form->labelEx($modPermintaanPembelian,'tglpermintaanpembelian', array('class'=>'control-label')) ?>
						<div class="controls">
							<?php $this->widget('MyDateTimePicker',array(
									'model'=>$modPermintaanPembelian,
									'attribute'=>'tglpermintaanpembelian',
									'mode'=>'date',
									'options'=> array(
									'dateFormat'=>Params::DATE_TIME_FORMAT,
									'maxDate'=>'d',
								),
								'htmlOptions'=>array(
									'readonly'=>true,
									'class'=>'dtPicker3',
									'onkeypress'=>"return $(this).focusNextInputField(event)"
								),
							)); ?>
						</div>
					</div>
				</td>
				<td>
					<?php echo $form->textAreaRow($modPermintaanPembelian,'keteranganpermintaan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
					<?php echo $form->textAreaRow($modPermintaanPembelian,'alamatpengiriman',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
				</td>
			</tr>
		</table>
	</fieldset>
	<fieldset>
		<legend class="rim">Daftar Obat Alkes</legend>
		<hr>
		<table id="tableObatAlkes" class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th>No.Urut</th>
					<th>Kategori/<br>Nama Obat</th>
					<th>Jumlah Kemasan<br>(Satuan Kecil/Besar)</th>
					<th>Jumlah Pembelian<br>(Satuan Besar)</th>
					<th>Harga Satuan</th>
					<th>PPN</th>
					<th>PPH</th>
					<th>Diskon (%)</th>
					<th>Diskon Total (Rp)</th> 
					<th>Sub Total</th>
				</tr>
			</thead>
			<tbody>
			<?php
				$total_harga=0;
				if(count($modPermintaanDetail) > 0)
				{
					$no_urut = 1;
					foreach($modPermintaanDetail as $key=>$val)
					{
						$modObatAlkes = ObatalkesM::model()->findByPk($val->obatalkes_id);
						echo $this->renderPartial('_formObatAlkesPembelian',
							array(
								'modPermintaanDetail'=>$val,
								'modObatAlkes'=>$modObatAlkes,
								'no_urut'=>$no_urut
							), true
						);
						$total_harga += ($val->jmlpermintaan * $val->jmlkemasan) * $val->harganettoper;
						$no_urut++;
					}
				}
			?>			
			</tbody>
			<tfoot>
				<tr>
					<td colspan="9">Total</td>
					<td colspan="2"><?php echo CHtml::TextField('total_penerimaan',$total_harga,array('class'=>' currency','readonly'=>TRUE));?></td>
				</tr>
			</tfoot>
		</table>
		
		<div class="form-actions">
			<?php echo CHtml::htmlButton(
				'<i class="icon-print icon-white"></i> Print PO',
				array(
					'class'=>'btn btn-primary',
					'type'=>'button',
					'onClick'=>'print_faktur();'
				)
			);?>
			<?php echo CHtml::link(
				'<i class="icon-ban-circle icon-white"></i> Batal Permintaan',
				"javascript:void(0)",
				array(
					'class'=>'btn btn-danger',
					'onClick'=>'batal_faktur();'
				)
			);?>
		</div>		
	</fieldset>
</fieldset>
<?php $this->endWidget(); ?>