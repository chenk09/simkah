<?php
$urlGetObatSupplier = Yii::app()->createUrl('actionAjax/GetObatAlkesPembelian');//MAsukan Dengan memilih Obat Alkes
$urlGetObatAlkesDariRencana = Yii::app()->createUrl('actionAjax/getObatAlkesDariPerencanaan');//MAsukan Dengan memilih Perencanaa
$urlGetObatAlkesDariPermintaan = Yii::app()->createUrl('actionAjax/getObatAlkesDariPermintaan');//MAsukan Dengan memilih Perencanaa
$urlGetSupplier = Yii::app()->createUrl('actionAjax/getSupplier');//MAsukan Dengan memilih Supplier

$konfigFarmasi = KonfigfarmasiK::model()->find();
$persenPPN = $konfigFarmasi->persenppn;
$persenPPH = $konfigFarmasi->persenpph;

$idRencana =CHtml::activeId($modRencanaKebFarmasi,'rencanakebfarmasi_id');
$idPermintaan=CHtml::activeId($modPermintaanPembelian,'permintaanpenawaran_id');
$tglRencana=CHtml::activeId($modRencanaKebFarmasi,'tglperencanaan');
$obatAlkes=CHtml::activeId($modPermintaanPembelian,'obatAlkes');
$noPerencanaan = CHtml::activeId($modRencanaKebFarmasi,'noperencnaan');
$noPenawaran = CHtml::activeId($modPermintaanPenawaran,'nosuratpenawaran');
$tglPenawaran = CHtml::activeId($modPermintaanPenawaran,'tglpenawaran');
$idSupplier = CHtml::activeId($modPermintaanPembelian, 'supplier_id');
$inputObat = CHtml::activeId($modPermintaanPembelian,'obatAlkes');

$jscript = <<< JS
var id_obat = [];

function setValue()
{
    id = $("#${idSupplier}").val();
    $('#${obatAlkes}').val('');
    $('#idObatAlkes').val('');

    if(id == "")
	{
        $("#berdasarkanSupplier").removeAttr("checked");
        alert("Anda belum memilih Supplier !");
    }

    if($("#berdasarkanSupplier").is(":checked")){
        $("#idSupplier").val(id);

        //switch to dialogObatsupplierM
        $("#${inputObat}").parent().find("a").removeAttr("onclick");
        $("#${inputObat}").parent().find("a").attr("onclick",'$(\"#dialogObatsupplierM\").dialog(\"open\");return false;');
        $.post("${urlGetSupplier}", {idSupplier:id},
            function(data){
               $('#idSupplier').val(data.supplier_id);
               $('#idSupplier2').val(data.supplier_id);

               $.get('${urlHalamanIni}', {idSupplier:data.supplier_id}, function(datas){
                    $.fn.yiiGridView.update('obatSupplier-m-grid', {
                        url: document.URL+'&ObatsupplierM%5Bsupplier_id%5D='+data.supplier_id,
                    });
                });

        }, "json");

    }else{
        id = "";
        $("#idSupplier").val(id);

        //switch to dialogObatAlkesM
        $("#${inputObat}").parent().find("a").removeAttr("onclick");
        $("#${inputObat}").parent().find("a").attr("onclick",'$(\"#dialogObatalkesM\").dialog(\"open\");return false;');
    }
}

function hitung_total_alkes(obj)
{
	var harganettoper = $(obj).parents("tr").find("input[name$='[harganettoper]']").val();
	var hargabelibesar = $(obj).parents("tr").find("input[name$='[hargabelibesar]']").val();
	var jmlpermintaan = $(obj).parents("tr").find("input[name$='[jmlpermintaan]']").val();
	var hargappnper_temp = $(obj).parents("tr").find("input[name$='[hargappnper]']").val();
	var persendiscount = $(obj).parents("tr").find("input[name$='[persendiscount]']").val();

	if(jmlpermintaan.length == 0)
	{
		jmlpermintaan = 1;
		$(obj).parents("tr").find("input[name$='[jmlpermintaan]']").val(jmlpermintaan);
	}

	var jmlkemasan = $(obj).parents("tr").find("input[name$='[jmlkemasan]']").val();
	var jmlkemasan_temp = $(obj).parents("tr").find("input[name$='[jmlkemasan_temp]']").val();

	var harga_discount = 0;
	if(persendiscount.length > 0 && persendiscount > 0)
	{
		harga_discount = ((harganettoper * jmlkemasan) * jmlpermintaan) * (persendiscount/100);
	}


	var hargappnper = 0;
	if(hargappnper_temp > 0)
	{
		hargappnper = (((harganettoper * jmlkemasan) * jmlpermintaan) - harga_discount) * (10/100);
	}
/*
	var total_pesan = jmlpermintaan * jmlkemasan_temp;
	if(jmlkemasan > total_pesan)
	{
		alert('Jumlah permintaan tidak bisa lebih dari total jumlah kemasan');
		$(obj).parents("tr").find("input[name$='[jmlkemasan]']").val(jmlkemasan_temp);
	}else{
		var sub_total = 0;
		if(jmlpermintaan > 1)
		{
			sub_total = (jmlkemasan * harganettoper) * jmlpermintaan;
			if(jmlkemasan > jmlkemasan_temp)
			{
				sub_total = jmlkemasan * harganettoper;
			}
		}else{
			sub_total = (jmlkemasan * harganettoper) * jmlpermintaan;
		}
		total_sub_total = (sub_total + hargappnper)- harga_discount;
		$(obj).parents("tr").find("input[name$='[totalHargaBersih]']").val(sub_total);
		$(obj).parents("tr").find("input[name$='[subTotal]']").val(total_sub_total);
	}
*/
  sub_total = (jmlkemasan * harganettoper) * jmlpermintaan;
  total_sub_total = (sub_total + hargappnper)- harga_discount;
  hargappnper = hargappnper.toFixed(2);
  harga_discount = harga_discount.toFixed(2);
  $(obj).parents("tr").find("input[name$='[hargappnper]']").val(hargappnper);
  $(obj).parents("tr").find("input[name$='[jmldiscount]']").val(harga_discount);
  $(obj).parents("tr").find("input[name$='[totalHargaBersih]']").val(sub_total);
  $(obj).parents("tr").find("input[name$='[subTotal]']").val(total_sub_total);
	setTimeout(function(){
		hitungTotalSemua();
	}, 500);
}


function cekValidasi()
{
	if($('.isRequired').val()=='')
	{
		alert('Harap Isi Semua Data Yang Bertanda *');
	}else{
		var sub_total = 0;
		$('.subTotal').each(function(){

			sub_total += parseFloat($(this).val());
		});
		if(sub_total > 0)
		{
			$('#btn_simpan').click();
		}else{
			alert('Harap Isi nilai uang, silahkan cek kembali!!');
		}

	}
}

function submitObat()
{
    idObat = $('#idObatAlkes').val();
    qtyObat = 1;
    idSupplier = $('#GFPermintaanPembelianT_supplier_id').val();

    if(idObat == ''){
        alert('Silahkan Pilih Obat Terlebih Dahulu');
    }else{
		$('.angka').unmaskMoney();
		if(id_obat[idObat] == undefined)
		{
            $.post("${urlGetObatSupplier}", { idObat:idObat, qtyObat:qtyObat, idSupplier:idSupplier },
            function(data){

				$('#tableObatAlkes').append(data.tr);
                $('#${obatAlkes}').val('');
                $('#idObatAlkes').val('');
                //$('#qtyObat').val('1');
				id_obat[data.id_obat] = 'yes';

            }, "json");

		}else{
			var jumlah_obat = $("#tableObatAlkes tbody").find("input[name='PermintaandetailT["+ idObat +"][jmlpermintaan]']");
            value = parseFloat(jumlah_obat.val()) + parseFloat(qtyObat);
			jumlah_obat.val(value);

            $('#${obatAlkes}').val('');
            $('#idObatAlkes').val('');
            //$('#qtyObat').val('1');
		}

		setTimeout(function(){
			$(".angka").maskMoney({
				"defaultZero":true,
				"allowZero":true,
				"allowDecimal":false,
				"decimal":".",
				"thousands":",",
				"precision":0
			});
			hitungTotalSemua();
		}, 500);
    }
}

function hitungTotalSemua()
{
	var sub_total = 0;
	var idx = 1;
	$('.subTotal').each(function(){

		sub_total += parseFloat($(this).val());
		$(this).parents('tr').find('.noUrut').val(idx);

		idx++;
	});
	$("#total_penerimaan").val(sub_total);
}

function removeRow(obj) {
    $(obj).parents('tr').remove();
	var obatalkes_id = $(obj).parents("tr").find("input[name$='[obatalkes_id]']").val();
	delete id_obat[obatalkes_id];
	hitungTotalSemua();
}

function hitung_jumlah_discount(obj){
	var discount = $(obj).val();
	var hargapphper = $(obj).parents("tr").find("input[name$='[hargapphper]']").val();
	var jmlkemasan = $(obj).parents("tr").find("input[name$='[jmlkemasan]']").val();
	var jmlpermintaan = $(obj).parents("tr").find("input[name$='[jmlpermintaan]']").val();
	var harganettoper = $(obj).parents("tr").find("input[name$='[harganettoper]']").val();
	var hargappnper_temp = $(obj).parents("tr").find("input[name$='[hargappnper]']").val();
	var harga_discount = ((harganettoper * jmlkemasan) * jmlpermintaan) * (discount/100);

	$(obj).parents("tr").find("input[name$='[jmldiscount]']").val(harga_discount);

	var hargappnper = 0;
	if(hargappnper_temp > 0)
	{
		hargappnper = (((harganettoper * jmlkemasan) * jmlpermintaan) - harga_discount) * (10/100);
		$(obj).parents("tr").find("input[name$='[hargappnper]']").val(hargappnper);
	}

	var sub_total = (((harganettoper * jmlkemasan) * jmlpermintaan) + parseFloat(hargappnper) + parseFloat(hargapphper)) - parseFloat(harga_discount);
	$(obj).parents("tr").find("input[name$='[subTotal]']").val(sub_total);
	hitungTotalSemua();
}

function hitung_discount(obj){
	var persendiscount = $(obj).parents("tr").find("input[name$='[persendiscount]']").val();
	if(persendiscount == 0)
	{
		$(obj).parents("tr").find("input[name$='[persendiscount]']").val(0);
	}

	var harganettoper = $(obj).parents("tr").find("input[name$='[harganettoper]']").val();
	var harga_discount = $(obj).val();
	var hargappnper_temp = $(obj).parents("tr").find("input[name$='[hargappnper]']").val();
	var hargapphper = $(obj).parents("tr").find("input[name$='[hargapphper]']").val();
	var jmlkemasan = $(obj).parents("tr").find("input[name$='[jmlkemasan]']").val();
	var jmlpermintaan = $(obj).parents("tr").find("input[name$='[jmlpermintaan]']").val();

	if(harga_discount.length == 0)
	{
		harga_discount = 0;
	}

	var hargappnper = 0;
	if(hargappnper_temp > 0)
	{
		hargappnper = (((harganettoper * jmlkemasan) * jmlpermintaan) - harga_discount) * (10/100);
		$(obj).parents("tr").find("input[name$='[hargappnper]']").val(hargappnper);
	}

	var sub_total = 0;
	if(parseFloat(harga_discount) > ((harganettoper * jmlkemasan) * jmlpermintaan))
	{
		harga_discount = 0;
		$(obj).val(0);
		alert('Diskon tidak bisa lebih besar dari harga netto');
	}

	sub_total = (((harganettoper * jmlkemasan) * jmlpermintaan) + parseFloat(hargappnper) + parseFloat(hargapphper)) - parseFloat(harga_discount);
	$(obj).parents("tr").find("input[name$='[subTotal]']").val(sub_total);
	hitungTotalSemua();
}

function hitung_harga_ppn(obj){
	if($(obj).is( ":checked" ))
	{
		$('.ppn').each(function(){
			var harganettoper = $(this).parents("tr").find("input[name$='[harganettoper]']").val();
			var jmldiscount = $(this).parents("tr").find("input[name$='[jmldiscount]']").val();
			var hargapphper = $(this).parents("tr").find("input[name$='[hargapphper]']").val();
			var jmlkemasan = $(this).parents("tr").find("input[name$='[jmlkemasan]']").val();
			var jmlpermintaan = $(this).parents("tr").find("input[name$='[jmlpermintaan]']").val();

			var hargappnper = (((harganettoper * jmlkemasan) * jmlpermintaan) - jmldiscount) * (10/100);
			$(this).parents("tr").find("input[name$='[hargappnper]']").val(hargappnper);

			var sub_total = ((harganettoper * jmlkemasan) * jmlpermintaan) + parseFloat(hargappnper) + parseFloat(hargapphper) - parseFloat(jmldiscount);
			$(this).parents("tr").find("input[name$='[subTotal]']").val(sub_total);

		});
	}else{
		$('.ppn').each(function(){
			$(this).val(0);
			var harganettoper = $(this).parents("tr").find("input[name$='[harganettoper]']").val();
			var jmldiscount = $(this).parents("tr").find("input[name$='[jmldiscount]']").val();
			var hargapphper = $(this).parents("tr").find("input[name$='[hargapphper]']").val();
			var jmlkemasan = $(this).parents("tr").find("input[name$='[jmlkemasan]']").val();
			var jmlpermintaan = $(this).parents("tr").find("input[name$='[jmlpermintaan]']").val();
			var hargappnper = 0;

			var sub_total = ((harganettoper * jmlkemasan) * jmlpermintaan) + parseFloat(hargappnper) + parseFloat(hargapphper) - parseFloat(jmldiscount);
			$(this).parents("tr").find("input[name$='[subTotal]']").val(sub_total);
		});
	}

	setTimeout(function(){
		hitungTotalSemua();
	}, 100);

}

function hitung_ppn(obj)
{
	var harganettoper = $(obj).parents("tr").find("input[name$='[harganettoper]']").val();
	var jmldiscount = $(obj).parents("tr").find("input[name$='[jmldiscount]']").val();
	var hargapphper = $(obj).parents("tr").find("input[name$='[hargapphper]']").val();
	var jmlkemasan = $(obj).parents("tr").find("input[name$='[jmlkemasan]']").val();
	var jmlpermintaan = $(obj).parents("tr").find("input[name$='[jmlpermintaan]']").val();

	var hargappnper = $(obj).val();
	if(hargappnper > 0)
	{
		var _hargappnper = (((harganettoper * jmlkemasan) * jmlpermintaan) - jmldiscount) * ({$persenPPN}/100);
		if(_hargappnper == hargappnper)
		{
			_hargappnper = hargappnper;
		}else{
			alert('Harga ppn tidak sesuai');
			$(obj).val(0);
			hargappnper = 0;
		}
	}else{
		hargappnper = 0;
		$(obj).val(0);
	}

	var sub_total = ((harganettoper * jmlkemasan) * jmlpermintaan) + parseFloat(hargappnper) + parseFloat(hargapphper) - parseFloat(jmldiscount);
	$(obj).parents("tr").find("input[name$='[subTotal]']").val(sub_total);
	hitungTotalSemua();
}

JS;
Yii::app()->clientScript->registerScript('masukanobat',$jscript, CClientScript::POS_HEAD);
?>
<?php
$controller = Yii::app()->controller->id;
$module = Yii::app()->controller->module->id;
$action = $this->getAction()->getId();
$currentAction=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/'.$action);

$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'gfrencanaKebFarmasi-m-form',
		'enableAjaxValidation'=>false,
		'type'=>'horizontal',
		'htmlOptions'=>array(
			'enctype'=>"multipart/form-data",
			'onKeyPress'=>'return disableKeyPress(event)',
		),
		'focus'=>'#GFPermintaanPembelianT_supplier_id',
	)
);
$this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
<legend class="rim2">Transaksi Pemintaan Pembelian</legend>
<fieldset>
		<legend class="rim"> Data Permintaan Pembelian</legend>
		<table>
			<tr>
				<td>
					 <?php
						echo $form->dropDownListRow($modPermintaanPembelian,'supplier_id',
							CHtml::listData(
								SupplierM::model()->findAll("supplier_aktif=TRUE AND supplier_jenis ilike '%".PARAMS::DEFAULT_JENIS_SUPPLIER."%' ORDER BY supplier_nama")
							, 'supplier_id', 'supplier_nama'),
							array(
								'onkeypress'=>"return $(this).focusNextInputField(event)" ,
								'onChange'=>'setValue();',
								'class'=>'idSupplier',
							)
						);
					?>
					<?php echo CHtml::hiddenField('idSupplier',"", array('readonly'=>true)); ?>
					<?php
						echo $form->dropDownListRow($modPermintaanPembelian,'syaratbayar_id',
							CHtml::listData($modPermintaanPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
							array(
								'class'=>'span3',
								'onkeypress'=>"return $(this).focusNextInputField(event)",
							)
					);?>
					<?php echo $form->textFieldRow($modPermintaanPembelian,'nopermintaan',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",));?>
					<div class="control-group ">
						<?php echo $form->labelEx($modPermintaanPembelian,'tglpermintaanpembelian', array('class'=>'control-label')) ?>
						<div class="controls">
							<?php $this->widget('MyDateTimePicker',array(
									'model'=>$modPermintaanPembelian,
									'attribute'=>'tglpermintaanpembelian',
									'options'=> array(
									'dateFormat'=>Params::DATE_TIME_FORMAT,
									'maxDate'=>'d',
								),
								'htmlOptions'=>array(
									'readonly'=>true,
									'class'=>'dtPicker3',
									'onkeypress'=>"return $(this).focusNextInputField(event)"
								),
							)); ?>
						</div>
					</div>
				</td>
				<td>
					<?php echo $form->textAreaRow($modPermintaanPembelian,'keteranganpermintaan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
					<?php echo $form->textAreaRow($modPermintaanPembelian,'alamatpengiriman',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
				</td>
			</tr>
		</table>
	</fieldset>
	<fieldset>
		<legend class="rim">Daftar Obat Alkes</legend>
		<div class="control-group ">
			<?php echo $form->labelEx($modPermintaanPembelian,'obatAlkes ', array('class'=>'control-label')) ?>
			<div class="controls">
				<?php echo CHtml::hiddenField('idObatAlkes');?>
				<?php $this->widget('MyJuiAutoComplete',array(
					'model'=>$modPermintaanPembelian,
					'attribute'=>'obatAlkes',
					'sourceUrl'=>'js:function(request,response){
						idSupplier = $("#idSupplier").val();
						$.get("'.Yii::app()->createUrl('actionAutoComplete/obatAlkesSupplier').'&idSupplier="+idSupplier,request,response,"json");
					}',
					'options'=>array(
						'showAnim'=>'fold',
						'minLength' => 2,
						'select'=>'js:function( event, ui ){
							$("#idObatAlkes").val(ui.item.obatalkes_id);
						}',
					),
					'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3'),
					'tombolDialog'=>array('idDialog'=>'dialogObatalkesM'),
				)); ?>
			</div>
			<div class="controls">
				<?php echo CHtml::checkBox('berdasarkanSupplier', false,array('onclick'=>'setValue();', 'title'=>'Filter Obat Berdasarkan Supplier', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
				<?php echo $form->label($modPermintaanPembelian,'Cek Jika Ingin Menampilakan obat dan alkes berdasarkan supplier'); ?>
			</div>
		</div>
		<div class="control-group ">
			<?php //echo Chtml::label('Qty (Satuan Besar)', 'Qty',array("class"=>'control-label'));?>
			<div class="controls">
				<?php //echo Chtml::textField('qtyObat','1',array('class'=>'span1 angka', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
				<?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
					array('onclick'=>'submitObat(); $(\'#GFPermintaanPembelianT_obatAlkes\').focus();',
						'class'=>'btn btn-primary',
						'onkeypress'=>"submitObat(); $('#GFPermintaanPembelianT_obatAlkes').focus();",
						'rel'=>"tooltip",
						'id'=>'tambahObat',
						'title'=>"Klik Untuk Menambahkan Obat"
					)
				); ?>
			</div>
		</div>
		<div class="control-group ">
			<div class="controls" style="font-size:11px">
				*Rumus
				<div>1. Total Harga Netto = (Jumlah Kemasan * Jumlah Satuan Besar) * Harga Satuan</div>
				<div>2. Diskon = Total Harga Netto * Diskon(%)</div>
				<div>3. PPN = (Total Harga Netto - Diskon) * 10%</div>
				<div>4. PPH = (Total Harga Netto - Diskon) * 10%</div>
				<div>5. Sub Total = (Total Harga Netto - Diskon) + PPN + PPH</div>
			</div>
		</div>
		<hr>
		<table id="tableObatAlkes" class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th>No.Urut</th>
					<th>Kategori/<br>Nama Obat</th>
					<th>Jumlah Pembelian<br>(Satuan Besar)</th>
					<th>Jumlah Kemasan<br>(Satuan Kecil/Besar)</th>
					<th>Harga Besar</th>
					<th>Harga Satuan</th>
					<th>Total Harga Netto</th>
					<th>Diskon (%)</th>
					<th>Diskon Total (Rp)</th>
					<th><?=CHtml::checkBox('is_hargappnper',false,array(
						"onclick"=>"hitung_harga_ppn(this)"
					))?> PPN</th>
					<th>PPH</th>
					<th>Sub Total</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<td colspan="11">Total</td>
					<td colspan="2"><?php echo CHtml::TextField('total_penerimaan','',array('class'=>'input-small currency','readonly'=>TRUE));?></td>
				</tr>
			</tfoot>
		</table>

		<div class="form-actions">
			<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'cekValidasi()'));?>
			<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan', 'style'=>'display:none;'));?>
			<?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), $currentAction, array('class'=>'btn btn-danger',));?>
		</div>
	</fieldset>
</fieldset>
<?php $this->endWidget(); ?>

<?php
//========= Dialog buat cari data ObatalkesM=========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatalkesM',
    'options'=>array(
        'title'=>'Pencarian Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatalkesM('search');
$modObatAlkes->unsetAttributes();

if(isset($_GET['ObatalkesM']))
{
    $modObatAlkes->attributes = $_GET['ObatalkesM'];
    $modObatAlkes->obatalkes_aktif = true;
}

$this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'ObatalkesM-m-grid',
        'dataProvider'=>$modObatAlkes->search(),
        'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small",
                                    "href"=>"",
                                    "id" => "selectObat",
                                    "onClick" => "
                                                  $(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                  $(\"#'.CHtml::activeId($modPermintaanPembelian,'obatAlkes').'\").val(\"$data->obatalkes_nama\");
                                                  $(\"#dialogObatalkesM\").dialog(\"close\");
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes_kategori',
                ),
                array(
                    'header'=>'Golongan Obat',
                    'value'=>'$data->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_kode'),
                    'value'=>'$data->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes_nama',
                ),
                array(
                    'header'=>'Asal Barang',
                    'value'=>'$data->sumberdana->sumberdana_nama',
                ),
                array(
                    'header'=>'Kadar Obat',
                    'value'=>'$data->obatalkes_kadarobat',
                ),
                array(
                    'header'=>'Kemasan Besar',
                    'value'=>'$data->kemasanbesar',
                ),
                array(
                    'header'=>'Kekuatan',
                    'value'=>'$data->kekuatan',
                ),
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->tglkadaluarsa',
                ),
            ),
        'afterAjaxUpdate' => 'function(id, data){
        jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
$this->endWidget();
//========= end ObatalkesM dialog =============================
?>


<?php
//========= Dialog buat cari data ObatsupplierM =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatsupplierM',
    'options'=>array(
        'title'=>'Pencarian Obat Supplier',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatsupplierM('search');
$modObatAlkes->unsetAttributes();
$modObatAlkes->obatalkes->supplier_id = $var['supplier_id'];
if(isset($_GET['ObatsupplierM'])) {
    $modObatAlkes->attributes = $_GET['ObatsupplierM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'obatSupplier-m-grid',
        'dataProvider'=>$modObatAlkes->searchObatSupplierGF(),
        'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small",
						"id" => "selectPasien",
						"href"=>"",
						"onClick" => "$(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
							$(\"#'.CHtml::activeId($modPermintaanPembelian,'obatAlkes').'\").val(\"".$data->obatalkes->obatalkes_nama."\");
							$(\"#dialogObatsupplierM\").dialog(\"close\");
							return false;
						"))',
                ),
                array(
                    'header'=>'Supplier',
                    'filter'=>CHtml::activeHiddenField($modObatAlkes, 'supplier_id', array("id"=>'idSupplier2')),
                    'value'=>'$data->supplier->supplier_nama',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes->obatalkes_kategori',
                ),
                array(
                    'header'=>'Golongan Obat',
                    'value'=>'$data->obatalkes->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_kodeobat'),
                    'value'=>'$data->obatalkes->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes->obatalkes_nama',
                ),
                array(
                    'header'=>'Asal Barang',
                    'value'=>'$data->obatalkes->sumberdana->sumberdana_nama',
                ),
                array(
                    'header'=>'Kadar Obat',
                    'value'=>'$data->obatalkes->obatalkes_kadarobat',
                ),
                array(
                    'header'=>'Kemasan Besar',
                    'value'=>'$data->obatalkes->kemasanbesar',
                ),
                array(
                    'header'=>'Kekuatan',
                    'value'=>'$data->obatalkes->kekuatan',
                ),
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->obatalkes->tglkadaluarsa',
                ),
		),
        'afterAjaxUpdate' => 'function(id, data){
        $("#ObatsupplierM_supplier_id").val($("#idSupplier").val());
        jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
$this->endWidget();
//========= end ObatsupplierM dialog =============================
?>

<?php

$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.angka',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'allowDecimal'=>false,
        'decimal' => '.',
        'thousands' => ',',
        'precision' => 0,
    )
));

?>
