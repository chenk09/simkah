
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

$table = 'ext.bootstrap.widgets.BootGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchObatSupplierGFPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchObatSupplierGFPrint();
         $template = "{pager}{summary}\n{items}";
    }
$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>$sort,
	'dataProvider'=>$data,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeColumns' => array('supplier_nama','supplier_kode','supplier_alamat'),
	'columns'=>array(
		array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
                array(
                    'header'=>'Kode Supplier',
                    'type'=>'raw',
                    'name'=>'supplier_kode',
                    'value'=>'$data->supplier->supplier_kode',
                ),
                array(
                    'header'=>'Nama Supplier',
                    'type'=>'raw',
                    'name'=>'supplier_nama',
                    'value'=>'$data->supplier->supplier_nama',
                ),
                array(
                    'header'=>'Alamat Supplier',
                    'type'=>'raw',
                    'name'=>'supplier_alamat',
                    'value'=>'$data->supplier->supplier_alamat',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'type'=>'raw',
                    'name'=>'obatalkes_nama',
                    'value'=>'($data->obatalkes->obatalkes_nama == "" ) ? "Tidak Diset" : $data->obatalkes->obatalkes_nama',
                ),
                array(
                    'header'=>'Satuan Kecil',
                    'type'=>'raw',
                    'name'=>'satuankecil_id',
                    'value'=>'($data->satuankecil->satuankecil_nama == "" ) ? "Tidak Diset" : $data->satuankecil->satuankecil_nama',
                ),
                array(
                    'header'=>'Satuan Besar',
                    'type'=>'raw',
                    'name'=>'satuanbesar_id',
                    'value'=>'($data->satuanbesar->satuanbesar_nama == "" ) ? "Tidak Diset" : $data->satuanbesar->satuanbesar_nama',
                ),
                array(
                    'header'=>'Harga Beli <br/> Satuan Besar',
                    'type'=>'raw',
                    'name'=>'hargabelibesar',
                    'value'=>'($data->hargabelibesar == "" ) ? "Tidak Diset" : $data->hargabelibesar',
                ),
                array(
                    'header'=>'Harga Beli <br/> Satuan Kecil',
                    'type'=>'raw',
                    'name'=>'hargabelikecil',
                    'value'=>'($data->hargabelikecil == "" ) ? "Tidak Diset" : $data->hargabelikecil',
                ),
                array(
                    'header'=>'Diskon(%)',
                    'type'=>'raw',
                    'name'=>'diskon_persen',
                    'value'=>'($data->diskon_persen == "" ) ? "Tidak Diset" : $data->diskon_persen',
                ),
                array(
                    'header'=>'Ppn(%)',
                    'type'=>'raw',
                    'name'=>'ppn_persen',
                    'value'=>'($data->ppn_persen == "" ) ? "Tidak Diset" : $data->ppn_persen',
                ),
                 array(
                    'header'=>'<center>Status</center>',
                    'value'=>'($data->supplier->supplier_aktif == 1 ) ? "Aktif" : "Tidak Aktif"',
                    'htmlOptions'=>array('style'=>'text-align:center;'),
                ),
        ),
    )); 
?>