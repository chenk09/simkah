<?php
$this->breadcrumbs=array(
	'Gfsupplier Ms'=>array('index'),
	'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Obat Supplier ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Supplier', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Obat Supplier', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gfsupplier-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.BootGroupGridView',array(
	'id'=>'gfsupplier-m-grid',
	'dataProvider'=>$model->searchObatSupplierGF(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        // 'mergeColumns' => array('supplier_nama','supplier_kode','supplier_alamat'),
	'columns'=>array(
                array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
                array(
                    'header'=>'Kode Supplier',
                    'type'=>'raw',
                    'name'=>'supplier_kode',
                    'value'=>'$data->supplier->supplier_kode',
                ),
                array(
                    'header'=>'Nama Supplier',
                    'type'=>'raw',
                    'name'=>'supplier_nama',
                    'value'=>'$data->supplier->supplier_nama',
                ),
                array(
                    'header'=>'Alamat Supplier',
                    'type'=>'raw',
                    'name'=>'supplier_alamat',
                    'value'=>'$data->supplier->supplier_alamat',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'type'=>'raw',
                    'name'=>'obatalkes_nama',
                    'value'=>'($data->obatalkes->obatalkes_nama == "" ) ? "Tidak Diset" : $data->obatalkes->obatalkes_nama',
                ),
                array(
                    'header'=>'Satuan Kecil',
                    'type'=>'raw',
                    'name'=>'satuankecil_nama',
                    'value'=>'($data->satuankecil->satuankecil_nama == "" ) ? "Tidak Diset" : $data->satuankecil->satuankecil_nama',
                ),
                array(
                    'header'=>'Satuan Besar',
                    'type'=>'raw',
                    'name'=>'satuanbesar_nama',
                    'value'=>'($data->satuanbesar->satuanbesar_nama == "" ) ? "Tidak Diset" : $data->satuanbesar->satuanbesar_nama',
                ),
                array(
                    'header'=>'Harga Beli <br/> Satuan Besar',
                    'type'=>'raw',
                    'name'=>'hargabelibesar',
                    'value'=>'($data->hargabelibesar == "" ) ? "Tidak Diset" : $data->hargabelibesar',
                ),
                array(
                    'header'=>'Harga Beli <br/> Satuan Kecil',
                    'type'=>'raw',
                    'name'=>'hargabelikecil',
                    'value'=>'($data->hargabelikecil == "" ) ? "Tidak Diset" : $data->hargabelikecil',
                ),
                array(
                    'header'=>'Diskon(%)',
                    'type'=>'raw',
                    'name'=>'diskon_persen',
                    'value'=>'($data->diskon_persen == "" ) ? "Tidak Diset" : $data->diskon_persen',
                ),
                array(
                    'header'=>'Ppn(%)',
                    'type'=>'raw',
                    'name'=>'ppn_persen',
                    'value'=>'($data->ppn_persen == "" ) ? "Tidak Diset" : $data->ppn_persen',
                ),
                
//                array(
//                     'name'=>'obatalkes_nama',
//                     'type'=>'raw',
////                     'filter'=>
//                     'value'=>'$this->grid->getOwner()->renderPartial(\'_obatSupplier\',array(\'supplier_id\'=>$data[supplier_id],\'obatalkes_id\'=>$data[obatalkes_id]),true)',
//                ),
                // array(
                //     'header'=>'<center>Status</center>',
                //     'value'=>'($data->supplier->supplier_aktif == 1 ) ? "Aktif" : "Tidak Aktif"',
                //     'htmlOptions'=>array('style'=>'text-align:center;'),
                // ),
		array(
                        'header'=>Yii::t('zii','View'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{view}',
                        'buttons'=>array(
                                'view' => array (
                                        'label'=>"<i class='icon-view'></i>",
                                        'options'=>array('title'=>Yii::t('mds','View')),
                                        'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/view",array("id"=>"$data->supplier_id"))',
                                        //'visible'=>'($data->supplier->supplier_aktif && Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ? TRUE : FALSE',
//                                               
                                ),
                        ),
		),
		array(
                        'header'=>Yii::t('zii','Update'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{update}',
                        'buttons'=>array(
                            'update' => array (
                                          'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
                                        ),
                         ),
		),
//		array(
//                        'header'=>Yii::t('zii','Delete'),
//			'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{delete}',
//                        'buttons'=>array(
//                                        // 'remove' => array (
//                                        //         'label'=>"<i class='icon-remove'></i>",
//                                        //         'options'=>array('title'=>Yii::t('mds','Remove Temporary')),
//                                        //         'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/removeTemporary",array("id"=>"$data->supplier_id"))',
//                                        //         'visible'=>'($data->supplier->supplier_aktif && Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ? TRUE : FALSE',
//                                        //         'click'=>'function(){return confirm("'.Yii::t("mds","Do You want to remove this item temporary?").'");}',
//                                        // ),
//                                        'delete'=> array(
//                                                'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)',
//                                        ),
//                        )
//		),
               array(
                    'header'=>'Hapus',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-trash\'></i> ", "javascript:deleteRecord($data->obatalkes_id,$data->supplier_id)",array("id"=>"$data->obatalkes_id","idsupplier"=>"$data->supplier_id","rel"=>"tooltip","title"=>"Hapus"));',
                    'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            $("table").find("input[type=text]").each(function(){
                cekForm(this);
            })
        }',
)); ?>

<?php 
 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
    $content = $this->renderPartial('../tips/master',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
           $url=Yii::app()->createAbsoluteUrl($module.'/'.$controller);

// echo $_GET['GFSupplierM_page']."AAAAAAAAA";
// exit();

$js = <<< JSCRIPT
function cekForm(obj)
{
    if(obj.name == 'GFSupplierM[supplier_alamat]')
    {
        $("textarea[name='"+ obj.name +"']").val(obj.value);
    }else{
        $("#gfsupplier-m-search :input[name='"+ obj.name +"']").val(obj.value);
    }
}
function print(caraPrint)
{
    page = $(".pagination .active a").text();
    window.open("${urlPrint}/"+$('#gfsupplier-m-search').serialize()+"&caraPrint="+caraPrint+"&GFObatSupplierM_page="+page,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);      


?>

<script type="text/javascript">
    
    function deleteRecord(id,idsupplier){
        var id = id;
        var idsupplier = idsupplier;
        var url = '<?php echo $url."/delete"; ?>';
        var answer = confirm('Yakin Akan Menghapus Data ini ?');
            if (answer){
                 $.post(url, {id: id, idsupplier:idsupplier},
                     function(data){
                        if(data.status == 'proses_form'){
                                $.fn.yiiGridView.update('gfsupplier-m-grid');
                            }else{
                                alert('Data Gagal di Hapus')
                            }
                },"json");
           }
    }
</script>
