<?php
$this->breadcrumbs=array(
	'Obat Supplier Ms'=>array('index'),
	$model->supplier_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Obat Supplier ', 'header'=>true, 
                            'itemOptions'=>array('class'=>'heading-master'))) ;
                
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Obat Supplier', 
                            'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
                array(
                    'label'=>'ID Supplier',
                    'type'=>'raw',
                    'value'=>$model->supplier_id,
                ),
                array(
                    'label'=>'Kode',
                    'type'=>'raw',
                    'value'=>$model->supplier_kode,
                ),
                array(
                    'label'=>'Nama',
                    'type'=>'raw',
                    'value'=>$model->supplier_nama,
                ),
                array(
                    'label'=>'Alamat',
                    'type'=>'raw',
                    'value'=>$model->supplier_alamat,
                ),
	),
)); ?>
<legend class="rim">Data Obat Supplier</legend>
<table  class="table table-bordered table-condensed middle-center" style="width:600px">
    <thead>
        <tr>
            <th>Nama Obat Alkes</th>
            <th>Satuan Besar</th>
            <th>Satuan Kecil</th>
            <th>Harga Beli <br/> Satuan Besar</th>
            <th>Harga Beli <br/> Satuan Kecil</th>
        </tr>
    </thead>
    <tbody>
        <?php
             foreach($modSupplier as $i=>$obat){
        ?>
        <tr>
            <td><?php echo $obat->obatalkes->obatalkes_nama ?></td>
            <td><?php echo $obat->satuanbesar->satuanbesar_nama ?></td>
            <td><?php echo $obat->satuankecil->satuankecil_nama ?></td>
            <td><?php echo $obat->hargabelibesar?></td>
            <td><?php echo $obat->hargabelikecil?></td>
        </tr>
        <?php
             }
        ?>
    </tbody>
</table>
<?php $this->widget('TipsMasterData',array('type'=>'view'));?>

