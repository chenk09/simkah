<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfpenerimaanbarang-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); 
$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php
if (isset($_POST['idPembelianForm']))
{   
$idPembelian=$_POST['idPembelianForm'];
$noPembelian=$_POST['noPembelianForm'];
$tglPembelian=$_POST['tglPembelianForm'];

$jscript = <<< JS
    submitPermintaanPembelianDariInformasi('${idPembelian}','${noPembelian}','${tglPembelian}');
JS;
Yii::app()->clientScript->registerScript('submitRencanaDari Informasi',$jscript, CClientScript::POS_HEAD);
}
?>
<fieldset>
<legend class="rim2">Transaksi Penerimaan Barang</legend>
<?php echo $form->errorSummary($modPenerimaanBarang); ?>
<table>
    <tr>
        <td width="50%">    
            <!-- ================================ Form berdasarkan permintaan pembelian ==================== -->
            <fieldset id="formpermintaanpembelian" class="hide">
               <legend class="rim">Berdasarkan Permintaan Pembelian</legend>
                   <table>
                       <tr>
<!--                           <td width="1px"></td>-->
<!--                           <td colspan="2"><?php //echo $form->labelEx($modPermintaanPembelian,'nopermintaan', array()) ?></td>-->
                           <td colspan="3">
                               <div class="control-group ">
                                   
                                   
                                   <label class="control-label">
                                       <?php echo CHtml::checkBox('checkBoxPermintaan',true,array('onclick'=>'hapusObat();hilangkanReadonly(this);'));?>
                                       No. PO
                                   </label>
                                   <div class="controls">
                                       <?php $this->widget('MyJuiAutoComplete',array(
                                                'model'=>$modPermintaanPembelian,
                                                'attribute'=>'nopermintaan',
                                                'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/noPermintaanPembelian'),
                                                'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'select'=>'js:function( event, ui ) {
                                                              $("#'.CHtml::activeId($modPenerimaanBarang,'permintaanpembelian_id').'").val(ui.item.permintaanpembelian_id);
                                                              $("#'.CHtml::activeId($modPermintaanPembelian,'tglpermintaanpembelian').'").val(ui.item.tglpermintaanpembelian);
                                                              submitPermintaanPembelian();    
                                                    }',
                                                ),
                                                'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly','readonly'=>FALSE), 'tombolDialog'=>array('idDialog'=>'dialogPermintaanPembelian'),
                                            )); ?>
                                   </div>
                               </div>
                                    <?php //echo $form->hiddenField($modPenerimaanBarang,'permintaanpembelian_id',array('class'=>'span1','readonly'=>TRUE));?>
                                    
                                   <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                      //  array('onclick'=>'$("#dialogPermintaanPembelian").dialog("open");return false;',
                                             // 'class'=>'btn btn-primary',
                                              //'onkeypress'=>"return $(this).focusNextInputField(event)",
                                              //'rel'=>"tooltip",
                                             // 'title'=>"Klik Untuk Permintaan Pembelian Lebih Lanjut",
                                            //  'id'=>'buttonPermintaanPembelian')); ?>
                          </td>
                       </tr>
                       <tr>
                           <td colspan="3">
                               <?php echo $form->textFieldRow($modPermintaanPembelian,'tglpermintaanpembelian', array('class'=>'span3')) ?>
                           </td>
                       </tr>
                       <tr>
                           <td colspan="3">
                               <?php echo $form->dropDownListRow($modPenerimaanBarang,'supplier_id',
                                                                   CHtml::listData($modPenerimaanBarang->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                                   array('class'=>'span3 ', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                   'empty'=>'-- Pilih --',)); ?>
                           </td>
                       </tr>  
                       
                   </table>
               </div>    
             </fieldset>
            <!-- ================================ Akhir form berdasarkan permintaan pembelian ==================== -->
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <fieldset>
                <legend class="rim">Data Penerimaan Barang</legend>
                <table>
                    <tr>
                        <td>
                             <?php echo $form->textFieldRow($modPenerimaanBarang,'noterima', array('class'=>'span3 isRequired')) ?>
                             <div class="control-group ">
                                    <?php echo $form->labelEx($modPenerimaanBarang,'tglterima', array('class'=>'control-label')) ?>
                                        <div class="controls">
                                            <?php $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modPenerimaanBarang,
                                                        'attribute'=>'tglterima',
                                                        'mode'=>'date',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_FORMAT,

                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                            )); ?>
                                        </div>
                             </div>
                             <?php echo $form->textAreaRow($modPenerimaanBarang,'keteranganterima', array('class'=>'span3 isRequired')) ?>
                             <?php echo $form->textFieldRow($modPenerimaanBarang,'nosuratjalan', array('class'=>'span3')) ?>
                             <div class="control-group ">
                                    <?php echo $form->labelEx($modPenerimaanBarang,'tglsuratjalan', array('class'=>'control-label')) ?>
                                        <div class="controls">
                                            <?php $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modPenerimaanBarang,
                                                        'attribute'=>'tglsuratjalan',
                                                        'mode'=>'date',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_FORMAT,

                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                            )); ?>
                                        </div>
                             </div>
                            <div class="control-group">
                               <?php echo $form->dropDownListRow($modPenerimaanBarang,'supplier_id',
                                                                   CHtml::listData($modPenerimaanBarang->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                                   array('class'=>'span3 ', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                   'empty'=>'-- Pilih --',)); ?>
                            </div>
                             <?php echo $form->hiddenField($modPenerimaanBarang,'permintaanpembelian_id', array('class'=>'span3 isRequired')) ?>
                        </td>
                        <td>
                            <fieldset>
                                <legend><?php echo CHtml::checkBox('isUangMuka',false,array('onclick'=>'isUangMukaJava(this)')) ?> Uang Muka</legend>
                                <table>
                                    <tr>
                                        <td><?php echo $form->labelEx($modUangMuka,'namabank',array("class"=>""));?></td>
                                        <td><?php echo $form->textField($modUangMuka,'namabank',array('readonly'=>TRUE,'class'=>'span2')); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $form->labelEx($modUangMuka,'norekening',array("class"=>""));?></td>
                                        <td><?php echo $form->textField($modUangMuka,'norekening',array('readonly'=>TRUE,'class'=>'span2')); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $form->labelEx($modUangMuka,'rekatasnama',array("class"=>""));?></td>
                                        <td><?php echo $form->textField($modUangMuka,'rekatasnama',array('readonly'=>TRUE,'class'=>'span2')); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $form->labelEx($modUangMuka,'jumlahuang',array("class"=>""));?></td>
                                        <td><?php echo $form->textField($modUangMuka,'jumlahuang',array('readonly'=>TRUE,'class'=>'span2','onkeyup'=>"numberOnlyNol(this);")); ?></td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </fieldset> 
        </td>
    </tr>
    <tr>
        <td>
            <fieldset>
                <legend class="rim" style="padding-left: 20px;"><?php echo CHtml::checkBox('isLangsungFaktur',false,array('onclick'=>'aktifForm(this)')); ?>Terima Langsung Faktur</legend>
                <div id="fakturPembelian">        
                    <?php echo $form->textFieldRow($modFakturPembelian,'nofaktur', array('class'=>'span3 isRequiredFaktur','readonly'=>TRUE)) ?>
                         <div class="control-group ">
                                <?php echo $form->labelEx($modFakturPembelian,'tglfaktur', array('class'=>'control-label')) ?>
                                    <div class="controls">
                                        <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modFakturPembelian,
                                                    'attribute'=>'tglfaktur',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT,

                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                                        )); ?>
                                    </div>
                         </div>
                        <div class="control-group ">
                                    <?php echo $form->labelEx($modFakturPembelian,'tgljatuhtempo', array('class'=>'control-label')) ?>
                                        <div class="controls">
                                            <?php $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modFakturPembelian,
                                                        'attribute'=>'tgljatuhtempo',
                                                        'mode'=>'date',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_FORMAT,

                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                            )); ?>
                                        </div>
                             </div>
                    <?php echo $form->textFieldRow($modFakturPembelian,'biayamaterai', array('class'=>'span3','readonly'=>TRUE)) ?>
                    <?php echo $form->textAreaRow($modFakturPembelian,'keteranganfaktur', array('class'=>'span3','readonly'=>TRUE)) ?>
                    <?php echo $form->dropDownListRow($modFakturPembelian,'syaratbayar_id',
                                                   CHtml::listData($modFakturPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
                                                   array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                   'empty'=>'-- Pilih --',)); ?>
                </div>    
            </fieldset>
        </td>
        <td>
            <fieldset>
                <legend class="rim">Informasi Harga</legend>
                    <table>
                        <tr>
                            <td>&nbsp</td>
                            <td colspan="2"><?php echo $form->textFieldRow($modFakturPembelian,'totharganetto', array('class'=>'span2 isRequired','readonly'=>TRUE)) ?></td>
                        </tr>
                        <tr>
                            <td width="5px"><?php echo CHtml::checkBox('termasukPPN',false,array('onclick'=>'persenPPN(this)','disabled'=>TRUE,'style'=>'width : 10px'))?></td>
                            <td colspan="2"><?php echo $form->textFieldRow($modFakturPembelian,'totalpajakppn', array('class'=>'span2 isRequired','readonly'=>TRUE)) ?></td>
                        </tr>
                        <tr>
                            <td width="5px"><?php echo CHtml::checkBox('termasukPPH',false,array('onclick'=>'persenPPH(this)','disabled'=>TRUE,'style'=>'width : 10px'))?></td>
                            <td colspan="2"><?php echo $form->textFieldRow($modFakturPembelian,'totalpajakpph', array('class'=>'span2 isRequired','readonly'=>TRUE)) ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp</td>
                            <td colspan="2"><?php echo $form->textFieldRow($modFakturPembelian,'jmldiscount', array('class'=>'span2 isRequired','readonly'=>TRUE)) ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp</td>
                            <td colspan="2"><?php echo $form->textFieldRow($modFakturPembelian,'totalhargabruto', array('class'=>'span2 isRequired','readonly'=>TRUE)) ?></td>
                        </tr>
                    </table>
            </fieldset>
        </td>
    </tr>
</table>
</fieldset>
            

            <!-- ============================== Form berdasarkan pemilihan obat ======================= -->
            <fieldset id="formpemilihanobat">
                <legend class="rim">Pemilihan Obat</legend>
                  <table>
                        <tr>
                            <td width="20">
                                 <?php echo $form->labelEx($modPermintaanPembelian,'obatAlkes', array('class'=>'control-label',"style"=>"width :60px")) ?>
                                 <?php echo CHtml::hiddenField('idObatAlkes');?>                            
                            </td>
                            <td width="17">
                                <?php $this->widget('MyJuiAutoComplete',array(
                                                'model'=>$modPermintaanPembelian,
                                                'attribute'=>'obatAlkes',
                                                'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/ObatAlkes'),
                                                'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'select'=>'js:function( event, ui ) {
                                                              $("#idObatAlkes").val(ui.item.obatalkes_id);
                                                    }',
                                                ),
                                                'htmlOptions'=>array(
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                    'class'=>'span2 numbersOnly',
                                                ),'tombolDialog'=>array('idDialog'=>'dialogObatAlkes'),
                                    )); ?>                            
                            </td>
                             <td width="102">
                                <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                       // array('onclick'=>'$("#dialogObatAlkes").dialog("open");return false;',
                                       //       'class'=>'btn btn-primary',
                                          //    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                           //   'rel'=>"tooltip",
                                            //  'title'=>"Klik Untuk Pencarian Obat Alkes Lebih Lanjut",
                                            //  'id'=>'buttonPemilihanObat',
                                            //  'disabled'=>TRUE  
                                          //  )); ?>                            
                             </td>
                        </tr>    
                        <tr>
                            <td width="20">
                                <?php echo CHtml::label('Qty','Qty');?>                            
                            </td>
                       
                            <td width="17">
                                <?php echo CHtml::textField('qtyObat','1',array('class'=>'span1',"onkeyup"=>"numberOnly(this);", 'onkeypress'=>'return $(this).focusNextInputField(event)'));?>    <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                    array('onclick'=>'submitObat();return false;',
                                          'class'=>'btn btn-primary',
                                          'onkeypress'=>"submitObat();return $(this).focusNextInputField(event)",
                                          'rel'=>"tooltip",
                                          'id'=>'tambahObat',
                                          'title'=>"Klik Untuk Menambahkan Obat",)); ?>
                            </td>
                        </tr>
                    </table>
             </fieldset> 
            <!-- ================================ Akhirform berdasarkan pemilihan obat ====================== -->
 

<table id="tablePenerimaan" class="table table-bordered table-condensed">
    <thead>
    <tr>
<!--        <th><?php // echo CHtml::checkBox('checkListUtama',true,array('onclick'=>'checkAll(\'cekList\',this);hitungSemua();'));?></th>-->
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Jumlah <br/> Permintaan</th>
        <th>Jumlah <br/>Diterima</th>
        <th>Satuan Kecil</th>
        <th>Satuan Besar</th>
        <th>Jumlah Kemasan</th>
        <th>Diskon (%)</th>
        <th>Diskon (Rp)</th>
        <th>Harga Netto</th>
        <th>PPN</th>
        <th>PPH</th>
        <th>Tanggal <br/>Kadaluarsa</th>
        <th>Batal</th>
     
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
        <tr>
            
        </tr>
    </tfoot>
</table>
<div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'cekValidasi()'));?>
                    <div style="display: none">     
                         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan')); ?>
                    </div>
        <?php 
             $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
             $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
             $action=$this->getAction()->getId();
             $currentAction=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/'.$action);
             if (isset($_POST['idPembelianForm'])){//Jika Masuk ke Halaman ini memalui Informasi Rencana kebutuhan
                               echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                              $_POST['currentUrl'], 
                               array('class'=>'btn btn-danger',));
              }else{
                   echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                   $currentAction, 
                   array('class'=>'btn btn-danger',));
              }  
        ?> 
         <?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>
<?php $this->endWidget(); ?>
<?php
$urlGetObatAlkes =  Yii::app()->createUrl('actionAjax/getPermintaanPembelianDariObatAlkes');
$urlGetPermintaanPembelian = Yii::app()->createUrl('actionAjax/getPermintaanPembelian');
$idPermintaanPembelian = CHtml::activeId($modPenerimaanBarang,'permintaanpembelian_id');
$idPersenDiskon=CHtml::activeId($modFakturPembelian, 'persendiscount');
$idTotalHargaNetto=CHtml::activeId($modFakturPembelian, 'totharganetto');
$idTotalPajakPPN=CHtml::activeId($modFakturPembelian,'totalpajakppn');
$idTotalPajakPPH=CHtml::activeId($modFakturPembelian, 'totalpajakpph');
$idJumlahDiskon=CHtml::activeId($modFakturPembelian, 'jmldiscount');
$idSyaratBayar=CHtml::activeId($modFakturPembelian, 'syaratbayar_id');
$idTotalHargaBruto=CHtml::activeId($modFakturPembelian, 'totalhargabruto');
$idSupplier=CHtml::activeId($modPenerimaanBarang,'supplier_id');
$idPermintaanPembelian=CHtml::activeId($modPenerimaanBarang,'permintaanpembelian_id');

$persenPPN=  Params::persenPPN();
$persenPPH=  Params::persenPPH();

$namaBank = CHtml::activeId($modUangMuka,'namabank');
$noRekening = CHtml::activeId($modUangMuka,'norekening');
$rekAtasNama = CHtml::activeId($modUangMuka,'rekatasnama');
$jumlahUang = CHtml::activeId($modUangMuka,'jumlahuang');

$jscript = <<< JS

function hitungJumlahDariDiterima(obj)
{
    jumlahDiterima = parseFloat(obj.value);
    jumlahPermintaan = parseFloat($(obj).parent().prev().find('input').val());
    persenDiskon = parseFloat($(obj).parent().next().find('input').val());
    hargaProdukNetto = parseFloat($(obj).parent().next().next().next().find('input').val());
    if(jumlahDiterima>jumlahPermintaan){
        alert('Jumlah Diterima Tidak boleh melebihi jumlah permintaan');
    }else{
        jumlahDiskonProduk = (hargaProdukNetto * (persenDiskon/100)) * jumlahDiterima;
        $(obj).parent().next().next().find('input').val(jumlahDiskonProduk);
    }
    hitungSemua();
}


function hitungJumlahDariPersentaseDiskon(obj)
{
    persenDiskon = parseFloat(obj.value);
    jumlahDiterima = parseFloat($(obj).parent().prev().find('input').val());
    hargaProdukNetto = parseFloat($(obj).parent().next().next().find('input').val());
    
        jumlahDiskonProduk = (hargaProdukNetto * (persenDiskon/100)) * jumlahDiterima;
        $(obj).parent().next().find('input').val(jumlahDiskonProduk);

    hitungSemua();
}


function hitungJumlahDariDiskon(obj)
{
    jumlahDiterima = parseFloat($(obj).parent().prev().prev().find('input').val());
    jumlahDiskon = parseFloat(obj.value);
    hargaProdukNetto = parseFloat($(obj).parent().next().find('input').val());
    jumlahHarga = jumlahDiterima * hargaProdukNetto;
    
    if(jumlahDiskon>jumlahHarga){
        alert('Jumlah Diskon Tidak Boleh Melebihi Jumlah Diterima * Harga Netto');
    }else{
         persenDiskon = ((jumlahDiskon * 100)/hargaProdukNetto) / jumlahDiterima;
       $(obj).parent().prev().find('input').val(persenDiskon); 
    }
    hitungSemua();
}

function persenPPN(obj)
{
    if(obj.checked==true){ //Jika tidak termasuk PPN
      $('.ppn').each(function() {
               hargaNettoProduk=parseFloat($(this).parent().prev().find('input').val());
               jumlahPPN=hargaNettoProduk * (parseFloat(${persenPPN})/100);
               $(this).val(jumlahPPN);
            });       
    }else{//Jika Termasuk PPN
    $('.ppn').each(function() {
               $(this).val(0);
            });
    $('#termasukPPH').removeAttr('checked'); 
    $('.pph').each(function() {
                   $(this).val(0);
                   $(this).attr('readonly','TRUE');
                }); 
    $('#totalPPH').val(0);           
             
    }
}
               
function persenPPH(obj)
{
    isPPH = obj.value;
    if($('#termasukPPN').is(':checked'))
    {           
        if(obj.checked==true){ //Jika tidak termasuk PPN
                 totalPPH=0;  
                 $('.pph').each(function() {
                   hargaNettoProduk=parseFloat($(this).parent().prev().prev().find('input').val());
                   jumlahPPN=parseFloat($(this).parent().prev().find('input').val());
                   NettoDikurangPPN = hargaNettoProduk - jumlahPPN;
                   jumlahPPH=NettoDikurangPPN * (parseFloat(${persenPPH})/100);
                   $(this).val(jumlahPPH);
                   totalPPH = totalPPH + parseFloat(jumlahPPH);
                   $('#totalPPH').val(totalPPH);
    //               $(this).removeAttr('readonly');
                });  

        }else{ //Jika Termasuk PPN
                 $('.pph').each(function() {
                   $(this).val(0);
                   $(this).attr('readonly','TRUE');
                }); 
                $('#totalPPH').val(0);  
        }
    }else{
         alert('Anda Harus memilih PPN sebelum menambah PPH');
         $(obj).removeAttr('checked');          
    }               
}          

function isUangMukaJava(obj)
{
    if(obj.checked==true){
    
        $('#${namaBank}').removeAttr('readonly');
        $('#${noRekening}').removeAttr('readonly');
        $('#${rekAtasNama}').removeAttr('readonly');
        $('#${jumlahUang }').removeAttr('readonly');  
        
    }else{
        
        $('#${namaBank}').attr('readonly','TRUE');
        $('#${noRekening}').attr('readonly','TRUE');
        $('#${rekAtasNama}').attr('readonly','TRUE');
        $('#${jumlahUang }').attr('readonly','TRUE');  
        
        $('#${namaBank}').val('');
        $('#${noRekening}').val('');
        $('#${rekAtasNama}').val('');
        $('#${jumlahUang }').val(0);  
        
    }

}

function cekValidasi()
{   
  langsungFaktur ='Tidak';  
  if ($('#isLangsungFaktur').is(':checked')){
     langsungFaktur ='Ya';
  }
 
  banyaknyaObat = $('.cekList').length;
  if ($('.isRequired').val()==''){
    alert ('Harap Isi Semua Data Yang Bertanda *');
  }else if(banyaknyaObat<1){
     alert('Anda Belum memimlih Obat Yang Akan Diminta');   
  }else if((langsungFaktur=='Ya') && ($('.isRequiredFaktur').val()=='')){
     alert('harap isi kolom yang bertanda * Pada Data Faktur');
  }else if($('.tgl isRequired').val()==''){
    alert('Anda Belum Mengisi Tanggal Kadaluarsa');
  }else{
    $('#btn_simpan').click();
  }
}

function submitPermintaanPembelian()
{
     idPermintaanPembelian = $('#${idPermintaanPembelian}').val();
        if(idPermintaanPembelian==''){
            alert('Silahkan Pilih Permintaan Pembelian Terlebih Dahulu');
        }else{
            $.post("${urlGetPermintaanPembelian}", { idPermintaanPembelian: idPermintaanPembelian },
            function(data){
                $('#tablePenerimaan').append(data.tr);
                $('#${idSupplier}').val(data.supplier_id);
                $('#GFPermintaanPembelianT_nopermintaan').attr('disabled','TRUE');
                $('#buttonPermintaanPembelian').attr('disabled','TRUE');
                $('#GFPermintaanPembelianT_tglpermintaanpembelian').attr('readonly','TRUE');
                $('#${idSupplier}').val(data.supplier_id);
                $('#GFPenerimaanBarangT_permintaanpembelian_id').val(data.permintaanpembelian_id);
                $('#${idSyaratBayar}').val(data.syaratbayar_id);
                if(data.isPPN=='1'){ //Jika termasuk PPN
                 $('#termasukPPN').attr('checked','checked');
                }
                
                if(data.isPPH=='1'){ //Jika termasuk PPH
                 $('#termasukPPH').attr('checked','checked');
                }
              
                hitungSemua();
            }, "json");
           
                        
        }   
}

function submitPermintaanPembelianDariInformasi(idPembelian,noPembelian,tglPembelian)
{
     idPermintaanPembelian = idPembelian;
        if(idPermintaanPembelian==''){
            alert('Silahkan Pilih Permintaan Pembelian Terlebih Dahulu');
        }else{
            $.post("${urlGetPermintaanPembelian}", { idPermintaanPembelian: idPermintaanPembelian },
            function(data){
                $('#tablePenerimaan').append(data.tr);
                $('#${idSupplier}').val(data.supplier_id);
                $('#GFPermintaanPembelianT_nopermintaan').attr('disabled','TRUE');
                $('#buttonPermintaanPembelian').attr('disabled','TRUE');
                $('#GFPermintaanPembelianT_tglpermintaanpembelian').attr('readonly','TRUE');
                $('#${idSupplier}').val(data.supplier_id);
                $('#GFPenerimaanBarangT_permintaanpembelian_id').val(data.permintaanpembelian_id);
                $('#${idSyaratBayar}').val(data.syaratbayar_id);
                $('#formpermintaanpembelian').show();
                $('#formpemilihanobat').hide();
                if(data.isPPN=='1'){ //Jika termasuk PPN
                 $('#termasukPPN').attr('checked','checked');
                }
                
                if(data.isPPH=='1'){ //Jika termasuk PPH
                 $('#termasukPPH').attr('checked','checked');
                }
                
                    $('#buttonPermintaanPembelian').attr('disabled','TRUE');
                    $('#GFPermintaanPembelianT_nopermintaan').val(noPembelian);
                    $('#GFPermintaanPembelianT_tglpermintaanpembelian').val(tglPembelian);
              
                hitungSemua();
            }, "json");
           
                        
        }   
}                

function hitungSemua()
{
    totalNetto=0;
    totalPajakPPN=0;
    totalPajakPPH=0;
    totalJumlahDiskon=0;
    totalHargaBruto=0;
    hargaBrutoPerProduk=0;        
    totalDiskonSemua=0;        
    
    $('.terima').each(function(){
            
      if($(this).parent().prev().prev().prev().prev().prev().find('input').is(':checked'))
        {//Jika Di ceklist
           nettoPerProduk = parseFloat($(this).parent().next().next().next().find('input').val());
           PPNPreProduk = parseFloat($(this).parent().next().next().next().next().find('input').val());
           PPHPreProduk = parseFloat($(this).parent().next().next().next().next().next().find('input').val());
           jumlahDiterima = parseFloat($(this).val());
           persenDiskonPerProduk = parseFloat($(this).parent().next().find('input').val());

           jumlahDiskonProduk = (nettoPerProduk * (persenDiskonPerProduk/100)) * jumlahDiterima;
           $(this).parent().next().next().find('input').val(jumlahDiskonProduk);
            
           nettoDiterima =  nettoPerProduk * jumlahDiterima;
           diskonPerProduk = parseFloat($(this).parent().next().next().find('input').val());
            
           hargaBrutoPerProduk = jumlahDiterima * nettoPerProduk;
           totalHargaBruto = totalHargaBruto + hargaBrutoPerProduk;  
            
           totalNetto = totalNetto + nettoPerProduk;
           totalPajakPPN = totalPajakPPN + PPNPreProduk;
           totalPajakPPH = totalPajakPPH + PPHPreProduk;
           totalJumlahDiskon = totalJumlahDiskon + diskonPerProduk;
            
        }
   });
            
    totalDiskonSemua=   (totalJumlahDiskon * 100)/totalHargaBruto; 
       
    $('#${idTotalHargaNetto}').val(totalNetto);
    $('#${idTotalPajakPPN}').val(totalPajakPPN);
    $('#${idTotalPajakPPH}').val(totalPajakPPH);
    $('#${idJumlahDiskon}').val(totalJumlahDiskon);
    $('#${idTotalHargaBruto}').val(totalHargaBruto);
    $('#${idPersenDiskon}').val(totalDiskonSemua);    
    
    noUrut = 1;
     $('.noUrut').each(function() {
          $(this).val(noUrut);
          noUrut = noUrut + 1;
     });

}

function numberOnly(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;


    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }else{
    $(obj).val(1);
    }
}

function hitungJumlahHargaDiskon(obj)
{
    besarDiskon =obj.value;
   
    $('.persenDiskon').each(function() {
          $(this).val(besarDiskon);
        });
    hitungSemua();
}

function numberOnlyNol(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;


    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }else{
    $(obj).val(0);
    }
}

function aktifForm(obj)
{
    if(obj.checked==true){
        $('#fakturPembelian input').removeAttr('readonly','false');
        $('#fakturPembelian textArea').removeAttr('readonly','false');
        $('#termasukPPN').removeAttr('disabled');
        $('#termasukPPH').removeAttr('disabled');

    }else{
        $('#fakturPembelian input').attr('readonly',"TRUE");
        $('#fakturPembelian textArea').attr('readonly',"TRUE");
        $('#termasukPPN').attr('disabled','TRUE');
        $('#termasukPPH').attr('disabled','TRUE');

    }    
}

function hilangkanReadonly(obj)
{
    if(obj.checked==true){
        $('#buttonPermintaanPembelian').removeAttr('disabled','false');
        $('#GFPermintaanPembelianT_nopermintaan').removeAttr('disabled','false');
        $('#GFPermintaanPembelianT_obatAlkes').attr('readonly',"TRUE");
        $('#buttonPemilihanObat').attr('disabled',"TRUE");
        $('#buttonPilih').attr('disabled',"TRUE");
        $('#qtyObat').attr('readonly',"TRUE");
        $('#tambahObat').attr('disabled',"TRUE");
    }else{
        $('#buttonPermintaanPembelian').attr('disabled',"TRUE");
        $('#GFPermintaanPembelianT_nopermintaan').attr('disabled',"TRUE");
        $('#GFPermintaanPembelianT_obatAlkes').removeAttr('readonly',"TRUE");
        $('#buttonPemilihanObat').removeAttr('disabled',"TRUE");
        $('#buttonPilih').removeAttr('disabled',"TRUE");
        $('#qtyObat').removeAttr('readonly',"TRUE");
        $('#tambahObat').removeAttr('disabled',"TRUE");
        $('#GFPermintaanPembelianT_nopermintaan').val('');
        $('#GFPermintaanPembelianT_tglpermintaanpembelian').val('');
        $('#GFPenerimaanBarangT_supplier_id').val('');
        $('#formpermintaanpembelian').hide();
        $('#formpemilihanobat').show();
    
    
    }    
}
function hapusObat()
{
    banyakRow=$('#tablePenerimaan tr').length;

    for(i=2; i<=banyakRow; i++){
    $('#tablePenerimaan tr:last').remove();
    }
}

function submitObat()
{
    idObat = $('#idObatAlkes').val();
    qty = $('#qtyObat').val();
    if(idObat==''){
        alert('Silahkan Pilih Obat Terlebih Dahulu');
    }else{
        $.post("${urlGetObatAlkes}", { idObat: idObat, qty:qty },
        function(data){
            $('#tablePenerimaan tbody').append(data.tr);
             hitungSemua();
          
        }, "json");
    }   
}

function remove(obj) {
    $(obj).parents('tr').remove();
}
JS;
Yii::app()->clientScript->registerScript('faktur',$jscript, CClientScript::POS_HEAD);
?>


<?php 
//========= Dialog buat Permintaan Kebutuhan obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPermintaanPembelian',
    'options'=>array(
        'title'=>'Pencarian Permintaan Pembelian',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'permintaan-m-grid',
	'dataProvider'=>$modPermintaanPembelian->searchPenerimaanItems(),
	'filter'=>$modPermintaanPembelian,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPasien",
                                    "onClick" => "$(\"#'.CHtml::activeId($modPermintaanPembelian,'nopermintaan').'\").val(\"$data->nopermintaan\");
                                                  $(\"#'.CHtml::activeId($modPenerimaanBarang,'permintaanpembelian_id').'\").val(\"$data->permintaanpembelian_id\");
                                                  $(\"#'.CHtml::activeId($modPermintaanPembelian,'tglpermintaanpembelian').'\").val(\"$data->tglpermintaanpembelian\");    
                                                  submitPermintaanPembelian();
                                                  $(\"#dialogPermintaanPembelian\").dialog(\"close\");    
                                        "))',
                ),
                'nopermintaan',
                array(
            'name' => 'tglpermintaanpembelian',
            'filter' =>$this->widget('MyDateTimePicker',array(
                                            'model'=>$modPermintaanPembelian,
                                            'attribute'=>'tglpermintaanpembelian',
                                            'mode'=>'date',
                                            'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT,

                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker1',
                                            ),
                                ),true), 
                    ),
                
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Permintaan dialog =============================
?>
<?php 
//========= Dialog buat cari data obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatAlkes',
    'options'=>array(
        'title'=>'Pencarian Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatalkesfarmasiV('search');
$modObatAlkes->unsetAttributes();
if(isset($_GET['ObatalkesfarmasiV'])) {
    $modObatAlkes->attributes = $_GET['ObatalkesfarmasiV'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'obatAlkes-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modObatAlkes->search(),
	'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "$(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                          $(\"#'.CHtml::activeId($modPermintaanPembelian,'obatAlkes').'\").val(\"$data->obatalkes_nama\");
                                                          submitObat();
                                                          $(\"#dialogObatAlkes\").dialog(\"close\");    
                                                "))',
                        ),
                'obatalkes_kategori',
                'obatalkes_golongan',
                'obatalkes_kode',
                'obatalkes_nama',
                'sumberdana_nama',
                'obatalkes_kadarobat',
                'kemasanbesar',
                'kekuatan',
                'tglkadaluarsa',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end obatAlkes dialog =============================
?>
