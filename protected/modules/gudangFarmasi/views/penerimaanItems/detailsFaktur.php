<?php
echo CHtml::css('.control-label{
        float:left; 
        text-align: right; 
        width:120px;
        color:black;
        padding-right:10px;
    }
    table{
        font-size:11px;
    }
');
?>
<br/><br/>
<table width="100%" style="margin:0px;margin-left:30px;" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" style='margin-top:-30px;'>
                <tr>
                    <td width="10%">
                             <b><?php echo CHtml::encode($modFakturPembelian->getAttributeLabel('nofaktur')); ?></b>
                    </td>
                    <Td width="5%"><b>:</b></td>
                    <td width="20%" style="margin-left:20px;"><?php echo CHtml::encode($modFakturPembelian->nofaktur); ?></td>

                    <td width="10%">
                            <b><?php echo CHtml::encode($modFakturPembelian->getAttributeLabel('supplier_id')); ?></b>
                    </td>
                    <td width="5%"><b>:</b></td>
                    <td width="20%">
                        <?php
                                echo ($modFakturPembelian->supplier_id == null) ? "-" : CHtml::encode($modFakturPembelian->supplier->supplier_nama);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td width="10%"><b>Tanggal Faktur</b></td>
                    <td width="5%"><b>:</b></td>
                    <Td width="20%"> <?php echo CHtml::encode($modFakturPembelian->tglfaktur); ?></td>
                    
                    <td width="10%"> 
                            <b><?php echo CHtml::encode($modFakturPembelian->getAttributeLabel('tgljatuhtempo')); ?></b>
                    <td width="1%"><b>:</b></td>
                    <td width="20%">
                        <?php
                            if(strlen($modFakturPembelian->tgljatuhtempo) != '')
                            {
                                echo CHtml::encode($modFakturPembelian->tgljatuhtempo);
                            }else{
                                " - ";
                            }
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table id="tableObatAlkes" class="table table-bordered table-condensed" style="margin-top:5px;">
    <thead>
    <tr style='text-align:center;'>
        <th rowspan="2" style='text-align:center;'>No.Urut</th>
        <th rowspan="2" style='text-align:center;'>Asal Barang</th>
        <th rowspan="2" style='text-align:center;'>Kategori /&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th rowspan="2" style='text-align:center;'>Jumlah Kemasan<br/></th>
        <th rowspan="2" style='text-align:center;'>Jumlah Diterima</th>
        <th rowspan="2" style='text-align:center;'>Harga Netto</th>
        <th rowspan="2" style='text-align:center;'>Ppn</th>
        <th rowspan="2" style='text-align:center;'>Pph</th>
        <th colspan="2" style='text-align:center;'>Diskon Harga</th>
        <th rowspan="2"style='text-align:center;'>Harga Satuan</th>
        <th rowspan="2" style='text-align:center;'>Sub Total</th>
    </tr>
    <tr>
        <th style='text-align:center;'> Rp. </th>
        <th style='text-align:center;'> % </th>
    </tr>
    </thead>
    <?php
    $no=1;
        $totalSubTotal = 0;
        $totalJumlahDiterima = 0;
        $totalNetto = 0;
        $totalPPN = 0;
        $totalPPH = 0;
        $totalJumlahDiskon = 0;
        $totalHargaSatuan = 0;
        if (count($modFakturPembelianDetails) > 0) {
            foreach($modFakturPembelianDetails AS $tampilData):
                $subTotal = $tampilData['harganettofaktur']*$tampilData['jmlterima'];
                $totalSubTotal += $subTotal;
                echo "<tr>
                        <td style='text-align:center'>".$no."</td>
                        <td>".$tampilData->sumberdana['sumberdana_nama']."</td>  
                        <td>".$tampilData->obatalkes['obatalkes_kategori']."<br>".$tampilData->obatalkes['obatalkes_nama']."</td>   
                        <td class='right-align' style='text-align:right;'>".MyFunction::formatNumber($tampilData['jmlkemasan'])
                        ." ".$tampilData->satuankecil['satuankecil_nama']."/".$tampilData->satuanbesar['satuanbesar_nama']."</td>
                        <td class='right-align'>".MyFunction::formatNumber($tampilData['jmlterima'])." ".$tampilData->satuanbesar['satuanbesar_nama']."</td>
                        <td class='right-align'>".MyFunction::formatNumber($tampilData['harganettofaktur'])."</td>
                        <td class='right-align'>".MyFunction::formatNumber($tampilData['hargappnfaktur'])."</td>     
                        <td class='right-align'>".MyFunction::formatNumber($tampilData['hargapphfaktur'])."</td>     
                        <td class='right-align'>".MyFunction::formatNumber($tampilData['jmldiscount'])."</td>   
                        <td class='right-align'>".$tampilData['persendiscount']."</td>   
                        <td class='right-align'>".MyFunction::formatNumber($tampilData['hargasatuan'])."</td>  
                        <td class='right-align'>".MyFunction::formatNumber($subTotal)."</td>      
                     </tr>";
                //

                $no++;
                $totalJumlahDiterima+=$tampilData['jmlterima'];
                $totalNetto+=$tampilData['harganettofaktur'];
                $totalPPN+=$tampilData['hargappnfaktur'];
                $totalPPH+=$tampilData['hargapphfaktur'];
                $totalJumlahDiskon+=$tampilData['jmldiscount'];
                $totalJumlahDiskonPersen+=$tampilData['persendiscount'];
                $totalHargaSatuan+=$tampilData['hargasatuan'];
                $totalBiayaMaterai =$modFakturPembelian->biayamaterai;
            endforeach;
        }
        else{
            echo '<tr><td colspan=12>'.Yii::t('zii','No results found.').'</td></tr>';
        }
//    echo "<tr>
//            <td colspan='4' style='text-align:right'><b>Jumlah</b></td>
//            <td class='right-align'>".MyFunction::formatNumber($totalJumlahDiterima)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalNetto)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalPPN)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalPPH)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalJumlahDiskon)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalJumlahDiskonPersen)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalHargaSatuan)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalSubTotal)."</td>
//         </tr>"; 
    echo "<tr>
            <td colspan='11' style='text-align:right'><b>Jumlah</b></td>
            <td class='right-align'>".MyFunction::formatNumber($modFakturPembelian->totharganetto)."</td>
         </tr>";
    echo "<tr>
            <td colspan='11' style='text-align:right'><b>Diskon </b></td>
            <td class='right-align'>".MyFunction::formatNumber($totalJumlahDiskon)."</td>
         </tr>"; 
    echo "<tr>
            <td colspan='11' style='text-align:right'><b>Ppn</b></td>
            <td class='right-align'>".MyFunction::formatNumber($totalPPN)."</td>
         </tr>"; 
    echo "<tr>
            <td colspan='11' style='text-align:right'><b>Pph</b></td>
            <td class='right-align'>".MyFunction::formatNumber($totalPPH)."</td>
         </tr>"; 
    echo "<tr>
            <td colspan='11' style='text-align:right'><b>Biaya Materai</b></td>
            <td class='right-align'>".MyFunction::formatNumber($totalBiayaMaterai)."</td>
         </tr>"; 
    echo "<tr>
            <td colspan='11' style='text-align:right'><b>Total</b></td>
            <td class='right-align'>".MyFunction::formatNumber($modFakturPembelian->totalhargabruto + $modFakturPembelian->biayamaterai)."</td>
         </tr>"; 
    echo "<tr>
            <td colspan='11' style='text-align:right'><b>Uang Muka</b></td>
            <td class='right-align'>".MyFunction::formatNumber($jmlUang)."</td>
         </tr>"; 
    echo "<tr>
            <td colspan='11' style='text-align:right'><b>Sisa Bayar</b></td>
            <td class='right-align'>".MyFunction::formatNumber(($modFakturPembelian->totalhargabruto + $modFakturPembelian->biayamaterai) - $jmlUang)."</td>
         </tr>"; 
    ?>
</table>

<?php
if (!isset($caraPrint)){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/printFaktur', array('idFaktur'=>$modFakturPembelian->fakturpembelian_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}
?>  