<?php
	$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'gfpenerimaanbarang-m-form',
		'enableAjaxValidation'=>false,
		'type'=>'horizontal',
		'htmlOptions'=>array(
			'onKeyPress'=>'return disableKeyPress(event)'
		),
		'focus'=>'#GFPenerimaanBarangT_keteranganterima',
	));
?>
<?php
	echo $this->renderPartial('penerimaan/index',array(
		'form'=>$form,
		'modPenerimaanBarang' => $modPenerimaanBarang,
		'modPermintaanPembelian' => $modPermintaanPembelian	
	));
	echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
?>
<?php $this->endWidget();?>