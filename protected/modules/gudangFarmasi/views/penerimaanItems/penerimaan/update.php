<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfpenerimaanbarang-m-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array(
		'onKeyPress'=>'return disableKeyPress(event)'
	),
	'focus'=>'#GFPenerimaanBarangT_keteranganterima',
));?>
<?php echo $this->renderPartial('penerimaan/index',array(
	'form'=>$form,
	'modPenerimaanBarang' => $modPenerimaanBarang,
	'modPermintaanPembelian' => $modPermintaanPembelian	
)); ?>

<?php
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idPenerimaan'=>$modPenerimaanBarang->penerimaanbarang_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
	window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
echo CHtml::htmlButton(Yii::t('mds','{icon} Print Berkas Penerimaan',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-success', 'type'=>'button','onclick'=>'print(\'PRINT\')'));
?>

<?php $this->endWidget();?>