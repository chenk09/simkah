<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php
$jscript = <<< JS
	$("#gfpenerimaanbarang-m-form").submit(function(event){
		var idx = 0;
		$('.isRequired').each(function(){
			var value = $(this).val();
			if(value.length == 0 && value == 0)
			{
				idx++;
			}
		});

		if(idx > 0)
		{
			alert("Lengkapi form yang sudah disediakan");
		}else{
			return true;
		}

		event.preventDefault();
	});
JS;
Yii::app()->clientScript->registerScript('penerimaan_js', $jscript);
?>
<style>
    .numbersOnly, .float{
        text-align: right;
    }
</style>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.float',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal' => '.',
        'thousands' => '',
        'precision'=>2,
    )
));
?>
<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => '',
        'thousands' => '',
        'precision' => 0,
    )
));?>
<legend class="rim2">Transaksi Penerimaan Items</legend>
<table>
	<tr>
		<td>
			<fieldset>
				<legend class="rim">Data Penerimaan Items</legend>
				<?php echo $form->textFieldRow($modPenerimaanBarang,'noterima', array('class'=>'span3 isRequired','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
				<div class="control-group ">
					<?php echo $form->labelEx($modPenerimaanBarang,'tglterima', array('class'=>'control-label')) ?>
						<div class="controls">
							<?php $this->widget('MyDateTimePicker',array(
								'model'=>$modPenerimaanBarang,
								'attribute'=>'tglterima',
								'mode'=>'datetime',
								'options'=> array(
									'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
									'maxDate'=>'d',
								),
								'htmlOptions'=>array(
									'readonly'=>true,
									'class'=>'dtPicker3',
									'onkeypress'=>"return $(this).focusNextInputField(event)"
								),
							)); ?>
						</div>
				</div>
				<?php echo $form->textAreaRow($modPenerimaanBarang,'keteranganterima', array('class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
				<?php echo $form->textFieldRow($modPenerimaanBarang,'nosuratjalan', array('class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
				<div class="control-group ">
					<?php echo $form->labelEx($modPenerimaanBarang,'tglsuratjalan', array('class'=>'control-label')) ?>
					<div class="controls">
						<?php $this->widget('MyDateTimePicker',array(
							'model'=>$modPenerimaanBarang,
							'attribute'=>'tglsuratjalan',
							'mode'=>'datetime',
							'options'=> array(
								'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
							),
							'htmlOptions'=>array(
								'readonly'=>true,
								'class'=>'dtPicker3',
								'onkeypress'=>"return $(this).focusNextInputField(event)"
							),
						)); ?>
					</div>
				</div>
			</fieldset>
		</td>
		<td>
			<fieldset>
				<legend class="rim">Informasi Permintaan Pembelian</legend>
				<?php echo $form->textFieldRow($modPermintaanPembelian,'nopermintaan', array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
				<?php echo $form->textFieldRow($modPermintaanPembelian,'tglpermintaanpembelian', array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
				<div class="control-group">
					<?php echo $form->labelEx($modPermintaanPembelian,'supplier_id', array('class'=>'control-label')) ?>
					<div class="controls"><?=$modPermintaanPembelian->supplier->supplier_nama;?></div>
				</div>
				<?php echo $form->hiddenField($modPermintaanPembelian,'supplier_id', array()) ?>
			</fieldset>
		</td>
	</tr>
</table>
<legend class="rim">Informasi Obat</legend>
<table id="tablePenerimaan" class="table table-bordered table-condensed middle-center">
    <thead>
    <tr>
        <th width="30" rowspan="2">No</th>
        <th width="150" rowspan="2">Asal Barang</th>
        <th rowspan="2">Kategori / Nama Obat</th>
        <th width="150" rowspan="2">Tanggal Kadaluarsa</th>
        <th colspan="2">Pembelian</th>
        <th colspan="2">Penerimaan</th>
    </tr>
	<tr>
        <th width="100">Kemasan</th>
        <th width="100">Jumlah</th>
		<th width="100">Kemasan</th>
        <th width="100">Jumlah</th>
    </tr>
    </thead>
    <tbody>
<?php
	$GFPermintaanDetailT = GFPermintaanDetailT::model()->findAllByAttributes(array(
		"permintaanpembelian_id"=>$modPermintaanPembelian->permintaanpembelian_id
	));
	$no_urut = 0;
	foreach($GFPermintaanDetailT as $key=>$val)
	{
		$GFPenerimaanDetailT = new GFPenerimaanDetailT;
		foreach($val as $x=>$z)
		{
			if($GFPenerimaanDetailT->hasAttribute($x))
			{
				$GFPenerimaanDetailT->$x = $z;
			}
			$GFPenerimaanDetailT->jmlterima = $GFPenerimaanDetailT->jmlpermintaan;
		}
?>
	<tr>
		<td>
			<?php echo $no_urut+1;?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']satuankecil_id', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']satuanbesar_id', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']obatalkes_id', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']sumberdana_id', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']persendiscount', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']jmldiscount', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']harganettoper', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']hargappnper', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']hargapphper', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']hargasatuanper', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']biaya_lainlain', array());?>
			<?php echo $form->hiddenField($GFPenerimaanDetailT,'['. $no_urut .']hargabelibesar', array());?>
		</td>
		<td><?=$GFPenerimaanDetailT->sumberdana->sumberdana_nama?></td>
		<td><?=$GFPenerimaanDetailT->obatalkes->obatalkes_nama?></td>
		<td><?php $this->widget(
			'MyDateTimePicker',array(
			'model'=>$GFPenerimaanDetailT,
			'attribute'=>'['. $no_urut .']tglkadaluarsa',
			'mode'=>'date',
			'options'=>array(
				'dateFormat'=>  Params::DATE_FORMAT_MEDIUM,
				'minDate'=>'d',
				'minYear'=>'d'
			),
			'htmlOptions'=>array(
				'readonly'=>true,
				'class'=>'span2 tgl',
				'style'=>'width:80px;',
				'class'=>'dtPicker2',
				'onkeypress'=>"return $(this).focusNextInputField(event)"),
			)
		);?></td>
		<td><?php
		echo $form->textField($GFPenerimaanDetailT,'['. $no_urut .']jmlkemasan', array(
			'readonly'=>true,
			'size'=>'5',
			'maxlength'=>'5',
			'style'=>'width:60px',
			'class'=>'numbersOnly',
			'onkeypress'=>"return $(this).focusNextInputField(event)"
		));
		echo " " . $val->satuankecil->satuankecil_nama;
		?></td>
		<td><?php
		echo $form->textField($GFPenerimaanDetailT,'['. $no_urut .']jmlpermintaan', array(
			'readonly'=>true,
			'size'=>'5',
			'maxlength'=>'5',
			'style'=>'width:60px',
			'class'=>'numbersOnly',
			'onkeypress'=>"return $(this).focusNextInputField(event)"
		));
		echo " " . $val->satuankecil->satuankecil_nama;
		?></td>
		<td><?php
		echo $form->textField($GFPenerimaanDetailT,'['. $no_urut .']jmlkemasan', array(
			'size'=>'5',
			'maxlength'=>'5',
			'style'=>'width:60px',
			'class'=>'numbersOnly',
			'onkeypress'=>"return $(this).focusNextInputField(event)"
		));
		echo " " . $val->satuankecil->satuankecil_nama;
		?></td>
		<td><?php
		echo $form->textField($GFPenerimaanDetailT,'['. $no_urut .']jmlterima', array(
			'size'=>'5',
			'maxlength'=>'5',
			'style'=>'width:60px',
			'class'=>'numbersOnly',
			'onkeypress'=>"return $(this).focusNextInputField(event)"
		));
		echo " " . $val->satuanbesar->satuanbesar_nama;
		?></td>
	</tr>
<?php
		$no_urut++;
	}
?>
	</tbody>
</table>
<hr>
<div>
    <?php
	/*
	if(is_null($modPenerimaanBarang->penerimaanbarang_id)){
		echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
	}else{
		$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idPenerimaan'=>$modPenerimaanBarang->penerimaanbarang_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
	window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
		Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
		echo CHtml::htmlButton(Yii::t('mds','{icon} Print Berkas Penerimaan',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-success', 'type'=>'button','onclick'=>'print(\'PRINT\')'));
	}
*/
	?>
</div>
