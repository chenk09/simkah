<?php
Yii::app()->clientScript->registerScript('search', "
$('#divSearch-form form').submit(function(){
	$.fn.yiiGridView.update('penawaran-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



<legend class="rim2">Informasi Penerimaan Items</legend>
<div id="divSearch-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'rencana-t-search',
        'type'=>'horizontal',
)); ?>
    <?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'penawaran-m-grid',
	'dataProvider'=>$modPenerimaanItems->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tglterima',
                'noterima',
//                array(
//                    'header'=>'Price /Unit',
//                    'type'=>'raw',
//                    'value'=>'"Rp. ".MyFunction::formatNumber($data->harganetto)',
//                ),
//                'harganetto',
//                'jmldiscount',
                array(
                    'name'=>'supplier_id',
                    'type'=>'raw',
                    'value'=>'$data->supplier->supplier_nama',
                ),
//                'totalpajakppn',
//                'totalpajakpph',
//                array(
//                    'header'=>'Total',
//                    'type'=>'raw',
//                    'value'=>'"Rp. ".MyFunction::formatNumber($data->totalharga)',
//                ),
//                'totalharga',
            
                array('header'=>'Permintaan Pembelian',
                      'type'=>'raw',
                      'value'=>'(!empty($data->permintaanpembelian_id) ? "Ya" : "Tidak") ',
                    ),
                 array('header'=>'Details',
                      'type'=>'raw',
                      'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/details",array("idPenerimaanBarang"=>$data->penerimaanbarang_id)) ,array("title"=>"Klik Untuk Melihat Detail Faktur","target"=>"iframePenerimaan", "onclick"=>"$(\"#dialogDetailsPenerimaan\").dialog(\"open\");", "rel"=>"tooltip"))',
                    ),
            /**     array('header'=>'Faktur',//Details Faktur Ini dipakai juga di Informasi Penerimaan Faktur
                      'type'=>'raw',
                      'value'=>'(!empty($data->fakturpembelian_id) ? CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/detailsFaktur",array("idFakturPembelian"=>$data->fakturpembelian_id)) ,array("title"=>"Klik Untuk Melihat Detail Faktur","target"=>"iframe", "onclick"=>"$(\"#dialogDetailsFaktur\").dialog(\"open\");", "rel"=>"tooltip")) : CHtml::link("<i class=\'icon-list-alt\'></i> ","'.$this->createUrl("fakturPembelian/index").'&id=$data->penerimaanbarang_id" ,array("title"=>"Klik Untuk Memasukan Faktur"))) ',
                    ), 
             * 
             */    
            
            
//            array(
//               'header'=>'Details',
//               'type'=>'raw',
//               'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/Details",array("idPermintaanPembelian"=>$data->permintaanpembelian_id)),
//                            array("class"=>"", 
//                                  "target"=>"iframe",
//                                  "onclick"=>"$(\"#dialogDetails\").dialog(\"open\");",
//                                  "rel"=>"tooltip",
//                                  "title"=>"Klik Untuk melihat details Permintaan Pembelian",
//                            ))',
//                ),
//                array(
//                   'header'=>'Penerimaan',
//                   'type'=>'raw',
//                   'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>","javascript:formPenerimaanItems(\'$data->permintaanpembelian_id\',\'$data->nopermintaan\',\'$data->tglpermintaanpembelian\')",
//                                array("class"=>"", 
//                                      "title"=>"Klik Mendaftarkan Ke Penerimaan Items",
//                                ))',
//                    ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?> 
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                    <?php echo $form->labelEx($modPenerimaanItems,'tglAwal', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPenerimaanItems,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenerimaanItems,'tglAkhir', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPenerimaanItems,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div> 
        </td>
        <td>
              <?php echo $form->textFieldRow($modPenerimaanItems,'noterima',array('class'=>'numberOnly')); ?>
              <?php echo $form->dropDownListRow($modPenerimaanItems,'supplier_id',
                                                               CHtml::listData($modPenerimaanItems->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                               'empty'=>'-- Pilih --',)); ?>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?><?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>
<?php $this->endWidget(); ?>
    
</div>
<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action=$this->getAction()->getId();
$currentUrl=  Yii::app()->createUrl($module.'/'.$controller.'/'.$action);
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hiddenFaktur',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('target'=>'_new'),
        'action'=>Yii::app()->createUrl($module.'/fakturPembelian/index'),
)); ?>
    <?php echo CHtml::hiddenField('idPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('noPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('tglPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('currentUrl',$currentUrl,array('readonly'=>true));?>
<?php $this->endWidget(); ?>
<?php
// ===========================Dialog Details Faktur=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogDetailsFaktur',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Details Faktur',
                        'autoOpen'=>false,
                        'minWidth'=>1100,
                        'minHeight'=>100,
                        'resizable'=>false,
                        'scrolling'=>false    
                            
                         ),
                    ));
?>
<iframe src="" name="iframe" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details Faktur================================



// ===========================Dialog Details Penerimaan=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogDetailsPenerimaan',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Details Penerimaan',
                        'autoOpen'=>false,
                        'minWidth'=>1100,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="iframePenerimaan" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details Penerimaan================================


$js = <<< JSCRIPT
function formFaktur(idPenerimaan,noPenerimaan,tglPenerimaan)
{
    $('#idPenerimaanForm').val(idPenerimaan);
    $('#noPenerimaanForm').val(noPenerimaan);
    $('#tglPenerimaanForm').val(tglPenerimaan);
    $('#form_hiddenFaktur').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);