<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<style>
    .numbersOnly, .float{
        text-align: right;
    }
</style>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfpenerimaanbarang-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'
//            ,'onsubmit'=>'return cekInputan();'
            ),
        'focus'=>'#GFPenerimaanBarangT_keteranganterima',
)); 
$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.float',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal' => '.',
        'thousands' => '',
        'precision'=>2,
    )
));
?>
<?php 
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => '',
        'thousands' => '',
        'precision' => 0,
    )
));?>
<?php
  if(isset($_GET['idPenerimaan'])){
    echo '<div class="alert alert-block alert-success">';
    echo '<a class="close" data-dismiss="alert">×</a>';
    echo 'Data berhasil disimpan';
    echo '</div>';
  }
?>
<fieldset>
<legend class="rim2">Transaksi Penerimaan Barang</legend>
<?php echo $form->errorSummary($modPenerimaanBarang); ?>
<?php $supplierInput = null;?>
<table>
    <tr>
        <td width="50%">    
            <?php if (isset($modPermintaanPembelian->permintaanpembelian_id)) : ?>
            <!-- ================================ Form berdasarkan permintaan pembelian ==================== -->
            <fieldset id="formpermintaanpembelian" class="<?php echo (isset($modPermintaanPembelian->permintaanpembelian_id)) ? '' : 'hide';?>">
               <legend class="rim">Berdasarkan Permintaan Pembelian</legend>
                   <table>
                       <tr>
<!--                           <td width="1px"></td>-->
<!--                           <td colspan="2"><?php //echo $form->labelEx($modPermintaanPembelian,'nopermintaan', array()) ?></td>-->
                           <td colspan="3">
                               <div class="control-group ">
                                   <label class="control-label">
                                       <?php //echo CHtml::checkBox('checkBoxPermintaan',true,array('onclick'=>'hapusObat();hilangkanReadonly(this);'));?>
                                       No. PO
                                   </label>
                                   <div class="controls">
                                       <?php echo $form->hiddenField($modPermintaanPembelian, 'permintaanpembelian_id', array('value'=>$modPermintaanPembelian->permintaanpembelian_id, 'readonly'=>true)); ?>
                                       <?php $this->widget('MyJuiAutoComplete',array(
                                                'model'=>$modPermintaanPembelian,
                                                'attribute'=>'nopermintaan',
                                                'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/noPermintaanPembelian'),
                                                'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'select'=>'js:function( event, ui ) {
                                                              $("#'.CHtml::activeId($modPenerimaanBarang,'permintaanpembelian_id').'").val(ui.item.permintaanpembelian_id);
                                                              $("#'.CHtml::activeId($modPermintaanPembelian,'tglpermintaanpembelian').'").val(ui.item.tglpermintaanpembelian);
                                                              submitPermintaanPembelian();    
                                                    }',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3 numbersOnly'), 
                                                'tombolDialog'=>array('idDialog'=>'dialogPermintaanPembelian'),
                                            )); ?>
                                   </div>
                               </div>
                                    <?php //echo $form->hiddenField($modPenerimaanBarang,'permintaanpembelian_id',array('class'=>'span1','readonly'=>TRUE));?>
                                    
                                   <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                      //  array('onclick'=>'$("#dialogPermintaanPembelian").dialog("open");return false;',
                                             // 'class'=>'btn btn-primary',
                                              //'onkeypress'=>"return $(this).focusNextInputField(event)",
                                              //'rel'=>"tooltip",
                                             // 'title'=>"Klik Untuk Permintaan Pembelian Lebih Lanjut",
                                            //  'id'=>'buttonPermintaanPembelian')); ?>
                          </td>
                       </tr>
                       <tr>
                           <td colspan="3">
                               <?php echo $form->textFieldRow($modPermintaanPembelian,'tglpermintaanpembelian', array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                           </td>
                       </tr>
                       <tr>
                           <td colspan="3">
                               <?php echo $form->dropDownListRow($modPermintaanPembelian,'supplier_id',
                                           CHtml::listData($modPenerimaanBarang->SupplierItems, 'supplier_id', 'supplier_nama'),
                                           array('class'=>'span3 ', 'onkeypress'=>"return $(this).focusNextInputField(event)",'onChange'=>'setValue();','class'=>'idSup',
                                           'empty'=>'-- Pilih --','disabled'=>true)); ?>
                               <?php echo CHtml::activeHiddenField($modPenerimaanBarang, 'supplier_id', array('values'=>$modPermintaanPembelian->supplier_id, 'readonly'=>true)); 
                               $supplierInput = 'disabled';
                               ?>
                           </td>
                       </tr>
                       
                   </table>
             </fieldset>
            <!-- ================================ Akhir form berdasarkan permintaan pembelian ==================== -->
            <?php endif; ?>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <fieldset>
                <legend class="rim">Data Penerimaan Barang</legend>
                <table>
                    <tr>
                        <td width="50%">
                             <?php echo $form->textFieldRow($modPenerimaanBarang,'noterima', array('class'=>'span3 isRequired','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                             <div class="control-group ">
                                    <?php echo $form->labelEx($modPenerimaanBarang,'tglterima', array('class'=>'control-label')) ?>
                                        <div class="controls">
                                            <?php $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modPenerimaanBarang,
                                                        'attribute'=>'tglterima',
                                                        'mode'=>'datetime',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                            'maxDate'=>'d',

                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                            )); ?>
                                        </div>
                             </div>
                             <?php echo $form->textAreaRow($modPenerimaanBarang,'keteranganterima', array('class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                             <?php echo $form->textFieldRow($modPenerimaanBarang,'nosuratjalan', array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                             <div class="control-group ">
                                    <?php echo $form->labelEx($modPenerimaanBarang,'tglsuratjalan', array('class'=>'control-label')) ?>
                                        <div class="controls">
                                            <?php $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modPenerimaanBarang,
                                                        'attribute'=>'tglsuratjalan',
                                                        'mode'=>'datetime',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                            )); ?>
                                        </div>
                             </div>
                            <div class="control-group">
                               <?php echo $form->dropDownListRow($modPenerimaanBarang,'supplier_id',
                                                   CHtml::listData($modPenerimaanBarang->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                   array('class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)",'onChange'=>'setValue();', 'class'=>'idSupplier','disabled'=>$supplierInput,
                                                   'empty'=>'-- Pilih --')); ?>
                                <?php echo CHtml::hiddenField('idSupplier','');?>
                            </div>
                            
                        </td>
                        <!-- EHJ-985 -->
<!--                        <td>
                            <fieldset>
                                <legend><?php //echo CHtml::checkBox('isUangMuka',((isset($var['isUangMuka'])) ? $var["isUangMuka"] : false),array('onclick'=>'isUangMukaJava(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?> Uang Muka</legend>
                                <div class="control-group ">
                                    <?php //echo $form->labelEx($modUangMuka,'namabank',array("class"=>"control-label"));?>
                                        <div class="controls">
                                           <?php //echo $form->textField($modUangMuka,'namabank',array('readonly'=>TRUE,'class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                        </div>
                                </div>
                                <div class="control-group ">
                                    <?php //echo $form->labelEx($modUangMuka,'norekening',array("class"=>"control-label"));?>
                                        <div class="controls">
                                           <?php //echo $form->textField($modUangMuka,'norekening',array('readonly'=>TRUE,'class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                        </div>
                                </div>
                                <div class="control-group ">
                                    <?php //echo $form->labelEx($modUangMuka,'rekatasnama',array("class"=>"control-label"));?>
                                        <div class="controls">
                                           <?php //echo $form->textField($modUangMuka,'rekatasnama',array('readonly'=>TRUE,'class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                        </div>
                                </div>
                                <div class="control-group ">
                                    <?php //echo $form->labelEx($modUangMuka,'jumlahuang',array("class"=>"control-label"));?>
                                        <div class="controls">
                                           <?php //echo $form->textField($modUangMuka,'jumlahuang',array('readonly'=>TRUE,'class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                        </div>
                                </div>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </fieldset> 
        </td>-->
                        <!-- END EHJ-985 -->
    </tr>
    <tr>
        <!-- EHJ-985 -->
<!--        <td>
            <fieldset>
                <legend class="rim" style="padding-left: 20px;"><?php echo CHtml::checkBox('isLangsungFaktur',((isset($var['isLangsung'])) ? $var["isLangsung"] : false),array('onclick'=>'aktifForm(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>Terima Langsung Faktur</legend>
                <div id="fakturPembelian">        
                    <?php //echo $form->textFieldRow($modFakturPembelian,'nofaktur', array('class'=>'span3 isRequiredFaktur','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                         <div class="control-group ">
                                <?php //echo $form->labelEx($modFakturPembelian,'tglfaktur', array('class'=>'control-label')) ?>
                                    <div class="controls">
                                        <?php 
//                                        $this->widget('MyDateTimePicker',array(
//                                                    'model'=>$modFakturPembelian,
//                                                    'attribute'=>'tglfaktur',
//                                                    'mode'=>'datetime',
//                                                    'options'=> array(
//                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
//
//                                                    ),
//                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
//                                                    ),
//                                        )); ?>
                                    </div>
                         </div>
                        <div class="control-group ">
                                    <?php //echo $form->labelEx($modFakturPembelian,'tgljatuhtempo', array('class'=>'control-label')) ?>
                                        <div class="controls">
                                            <?php 
//                                                $this->widget('MyDateTimePicker',array(
//                                                            'model'=>$modFakturPembelian,
//                                                            'attribute'=>'tgljatuhtempo',
//                                                            'mode'=>'datetime',
//                                                            'options'=> array(
//                                                                'dateFormat'=>Params::DATE_TIME_FORMAT,
//
//                                                            ),
//                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
//                                                            ),
//                                                )); 
                                            ?>
                                        </div>
                             </div>
                    <?php //echo $form->textFieldRow($modFakturPembelian,'biayamaterai', array('class'=>'span3 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    <?php //echo $form->textAreaRow($modFakturPembelian,'keteranganfaktur', array('class'=>'span3','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    <?php 
//                            echo $form->dropDownListRow($modFakturPembelian,'syaratbayar_id',
//                                   CHtml::listData($modFakturPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
//                                   array('readonly'=>true, 'disable'=>'disable','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
//                                   'empty'=>'-- Pilih --',)); 
                    ?>
                </div>    
            </fieldset>
        </td>-->
<!--        <td width="50%">
            <fieldset>
                <legend class="rim">Informasi Harga</legend>
                <?php echo $form->textFieldRow($modFakturPembelian,'totharganetto', array('class'=>'span2 isRequired numbersOnly','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                <div class="control-group ">
                    <label class='control-label'>
                        <?php //echo CHtml::checkbox('termasukPPN',false,array('onclick'=>'persenPPN(this)','disabled'=>TRUE,'style'=>'width : 10px', 'onkeypress'=>"return $(this).focusNextInputField(event)"))?>
                        <?php //echo CHtml::checkbox('termasukPPN',false,array('onclick'=>'addPPN(this)','disabled'=>TRUE,'style'=>'width : 10px'))?>
                        Ppn (Total)
                        </label>
                    <div class="controls">
                        <?php //echo $form->textField($modFakturPembelian,'totalpajakppn', array('class'=>'span2 isRequired numbersOnly','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                 </div>
                <div class="control-group ">
                    <label class='control-label'>
                        <?php //echo CHtml::checkBox('termasukPPH',false,array('onclick'=>'persenPPH(this)','disabled'=>TRUE,'style'=>'width : 10px', 'onkeypress'=>"return $(this).focusNextInputField(event)"))?>
                        Pph (Total)
                        </label>
                    <div class="controls">
                        <?php //echo $form->textField($modFakturPembelian,'totalpajakpph', array('class'=>'span2 isRequired numbersOnly','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                 </div>
                <div class="control-group ">
                    <label class='control-label'>
                        <?php //echo CHtml::checkbox('diskonSemua',false,array('onclick'=>'diskonFakturPersen()','disabled'=>TRUE,'style'=>'width : 10px', 'onkeypress'=>"return $(this).focusNextInputField(event)"))?>
                        Persen Diskon/ Faktur
                        </label>
                    <div class="controls">
                        <?php //echo $form->textField($modFakturPembelian,'persendiscount', array('onkeyup'=>'gantiDiskonFakturPersen(this);','class'=>'span2 isRequired float','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                 </div>
                <div class="control-group ">
                    <label class='control-label'>
                        <?php //echo CHtml::checkbox('diskonSemuaRp',false,array('onclick'=>'diskonFakturRp()','disabled'=>TRUE,'style'=>'width : 10px', 'onkeypress'=>"return $(this).focusNextInputField(event)"))?>
                        Diskon Rp / Faktur
                        </label>
                    <div class="controls">
                        <?php //echo $form->textField($modFakturPembelian,'jmldiscount', array('onkeyup'=>'gantiDiskonFakturRp(this);','class'=>'span2 isRequired numbersOnly','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                 </div>
                 <?php //echo $form->textFieldRow($modFakturPembelian,'totalhargabruto', array('class'=>'span2 isRequired numbersOnly','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
            </fieldset>
        </td>-->
        <!-- END EHJ-985 -->
    </tr>
</table>
</fieldset>
           <?php if (!isset($modPermintaanPembelian->permintaanpembelian_id) && ($modPenerimaanBarang->isNewRecord)) { ?>

            <!-- ============================== Form berdasarkan pemilihan obat ======================= -->
            <fieldset id="formpemilihanobat">
                <legend class="rim">Pemilihan Obat</legend>
                  <table>
                        <tr>
                            <td width="20">
                                <div class="control-group ">
                                   <?php echo $form->labelEx($modPermintaanPembelian,'obatAlkes', array('class'=>'control-label')) ?>
                                     <div class="controls">
                                     <?php echo CHtml::hiddenField('idObatAlkes');?>   
                                    <?php $this->widget('MyJuiAutoComplete',array(
                                                    'model'=>$modPermintaanPembelian,
                                                    'attribute'=>'obatAlkes',
//                                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/ObatAlkes'),
                                                     'sourceUrl'=>'js:function(request,response){
                                                                idSupplier = $("#GFPenerimaanBarangT_supplier_id").val();
                                                                $.get("'.Yii::app()->createUrl('actionAutoComplete/obatAlkesSupplier').'&idSupplier="+idSupplier,request,response,"json")
                                                            }',
                                                    'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'select'=>'js:function( event, ui ) {
                                                                  $("#idObatAlkes").val(ui.item.obatalkes_id);
                                                        }',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                        'class'=>'span3',
                                                        //'onfocus'=>'cekSupplier("xx")',
                                                    ),
                                                    'tombolDialog'=>array('idDialog'=>'dialogObatalkesM'),
                                                    
                                        )); ?>    
                                    </div>  
                                    <div class="controls"> 
                                        <?php echo CHtml::checkBox('berdasarkanSupplier', false,array('onclick'=>'setValue();', 'title'=>'Filter Obat Berdasarkan Supplier', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        <?php echo $form->label($modPermintaanPembelian,'Cek Jika Ingin Menampilakan obat dan alkes berdasarkan supplier'); ?>
                                    </div>
                                    </div>
                                </td>
<!--                             <td width="102">
                                <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                       // array('onclick'=>'$("#dialogObatAlkes").dialog("open");return false;',
                                       //       'class'=>'btn btn-primary',
                                          //    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                           //   'rel'=>"tooltip",
                                            //  'title'=>"Klik Untuk Pencarian Obat Alkes Lebih Lanjut",
                                            //  'id'=>'buttonPemilihanObat',
                                            //  'disabled'=>TRUE  
                                          //  )); ?>                            
                             </td>-->
                        </tr>    
                        <tr>
                            <td>
                                <div class="control-group ">
                                    <?php echo CHtml::label('Tgl Kadaluarsa','tglsuratjalan', array('class'=>'control-label')) ?>
                                        <div class="controls">
                                             <?php $minDate = (Yii::app()->user->getState('tglpemakai')) ? '' : 'd'; ?>
                                             <?php $this->widget('MyDateTimePicker',array(
                                                                    'model'=>$modPermintaanPembelian,
                                                                    'attribute'=>'tglkadaluarsa',
                                                                    'name'=>'tgl_kadaluarsa',
                                                                    'mode'=>'date',
                                                                    'options'=> array(
                                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                //                                                        'maxDate' => 'd',
                                                                        'minDate'=>$minDate,
                                                                    ),
                                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                              )); ?>
                                        </div>
                             </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="20">
                                <div class="control-group ">
                                   <?php echo CHtml::label('Qty (Satuan Besar)','Qty', array('class'=>'control-label'));?>     
                                   <div class="controls">
                                     <?php echo CHtml::textField('qtyObat','1',array('class'=>'span1 numbersOnly', 'onkeypress'=>'return $(this).focusNextInputField(event)'));?>    <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                    array('onclick'=>'submitObat();return false;',
                                          'class'=>'btn btn-primary',
                                          'onkeypress'=>"submitObat();return $('#GFPermintaanPembelianT_obatAlkes').focus()",
                                          'rel'=>"tooltip",
                                          'id'=>'tambahObat',
                                          'title'=>"Klik Untuk Menambahkan Obat",)); ?>
                                       </div>
                                    </div>
                                                       
                            </td>
                       
<!--                            <td width="17">
                                
                            </td>-->
                        </tr>
                    </table>
             </fieldset> 
            <!-- ================================ Akhirform berdasarkan pemilihan obat ====================== -->
 <?php } 
 
 if (isset($modPenerimaanDetail)){
     echo $form->errorSummary($modPenerimaanDetail);
 } 
 // DIHIDE KARENA BENTROK DENGAN FIELD YANG SAMA
// echo CHtml::activeHiddenField($modPenerimaanBarang, 'harganetto');
// echo CHtml::activeHiddenField($modPenerimaanBarang, 'jmldiscount');
// echo CHtml::activeHiddenField($modPenerimaanBarang, 'totalharga');
// echo CHtml::activeHiddenField($modPenerimaanBarang, 'persendiscount');
// echo CHtml::activeHiddenField($modPenerimaanBarang, 'totalpajakppn');
// echo CHtml::activeHiddenField($modPenerimaanBarang, 'totalpajakpph');
 ?>

<table id="tablePenerimaan" class="table table-bordered table-condensed middle-center">
    <thead>
    <tr>
<!--        <th><?php // echo CHtml::checkBox('checkListUtama',true,array('onclick'=>'checkAll(\'cekList\',this);hitungTotalSemua();'));?></th>-->
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Tanggal Kadaluarsa</th>
        <th>Jumlah Kemasan<br>(Satuan Kecil/Besar)</th>
        <?php echo (isset($modPermintaanPembelian->permintaanpembelian_id)) ? '<th>Jumlah Pembelian / Satuan</th>' : '';?>
        <th>Jumlah Diterima<br>(Satuan Besar)</th>
<!-- EHJ-985         <th>Harga Kemasan</th>
        <th>Diskon (%)</th>
        <th>Diskon Total (Rp)</th>-->
<!--        <th>PPN</th>
        <th>PPH</th>-->
       <!-- <th>Sub Total</th> -->
        <?php echo ($modPenerimaanBarang->isNewRecord) ? ((!isset($modPermintaanPembelian->permintaanpembelian_id)) ? '<th>Batal</th>' : '') : '';?>
     
    </tr>
    </thead>
    <tbody>
<!--        tboby untuk append data dari ajax-->
    </tbody>
        <?php
        if (isset($modPermintaanPembelian->permintaanpembelian_id)) {            
            if (count($modPenerimaanDetail) > 0){
                 foreach ($modPenerimaanDetail as $counter => $detail) {
                     $tampilDetail = ObatalkesM::model()->findByPk($detail->obatalkes_id);
                     $subTotal = ($detail->jmlterima*$detail->jmlkemasan*($detail->harganettoper + $detail->hargappnper + $detail->hargapphper)) - $detail->jmldiscount;
                     //penentuan harga beli besar dari harga pembelian
    //                    <td>".CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungTotalSemua()'))."</td>
                     ?>
                    <tbody>
                     <tr>
                         <td>
                             <?php
                              echo CHtml::TextField('noUrut',($counter+1),array('class'=>'span1 noUrut','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")).
                              CHtml::activeHiddenField($detail,'['.$counter.']satuankecil_id').
                              CHtml::activeHiddenField($detail,'['.$counter.']sumberdana_id'). 
                              CHtml::activeHiddenField($detail,'['.$counter.']obatalkes_id', array("class"=>'obatAlkes')). 
                              CHtml::activeHiddenField($detail,'['.$counter.']satuanbesar_id').
                              CHtml::activeHiddenField($detail,'['.$counter.']jmlkemasan').
                              CHtml::activeHiddenField($detail,'['.$counter.']tglkadaluarsa',array('value'=>$detail->tglkadaluarsa,'class'=>'span1 tglasal'));
                             ?>
                         </td>
                         <td><?php echo $tampilDetail->sumberdana['sumberdana_nama']; ?></td>
                         <td><?php echo $tampilDetail->obatalkes_kategori; ?> <br><?php echo $tampilDetail->obatalkes_nama; ?></td>
                        
                         <td><?php 
                                    $this->widget(
                                        'MyDateTimePicker',array(
                                        'model'=>$tampilDetail,
                                        'attribute'=>'['.$counter.']tglkadaluarsa',
                                         'mode'=>'date',
                                        'options'=>array('dateFormat'=>  Params::DATE_FORMAT_MEDIUM,
                                                         'minDate'=>'d',
                                                         'minYear'=>'d',),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'span2 tgl', 'style'=>'width:80px;', 'class'=>'dtPicker2', 'onkeypress'=>"return $(this).focusNextInputField(event)"),  
                                        )
                                    );
                                   ?>
                             
                            
                         </td>
                         <td><?php echo CHtml::activeTextField($detail,'['.$counter.']jmlkemasan',array('readonly'=>false,'class'=>'span1 numberOnly','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                         <?php echo $tampilDetail->satuankecil->satuankecil_nama; ?></td>
                         <td><?php echo CHtml::activeTextField($detail,'['.$counter.']jmlpermintaan',array('readonly'=>TRUE,'class'=>'span1 permintaan numberOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                         <?php echo $tampilDetail->satuanbesar->satuanbesar_nama; ?></td>
                         <td><?php echo CHtml::activeTextField($detail,'['.$counter.']jmlterima',array('class'=>'span1 terima integerOnly','onkeyup'=>'hitungSubtotal(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                             <?php echo $tampilDetail->satuanbesar->satuanbesar_nama; ?>
                             <?php echo CHtml::activeHiddenField($detail,'['.$counter.']hargabelibesar',array('readonly'=>false,'class'=>'span1 numberOnly','onkeyup'=>'hitungHargaNetto(this); hitungSubtotal(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                             <?php echo CHtml::activeHiddenField($detail,'['.$counter.']harganettoper',array('readonly'=>true,'class'=>'span1 numberOnly netto','onkeyup'=>'hitungJumlahDariHargaNetto(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                             <?php echo CHtml::activeHiddenField($detail,'['.$counter.']persendiscount',array('class'=>'span1 persenDiskon float','onkeyup'=>'hitungJmlDiskon(this);hitungSubtotal(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                             <?php echo CHtml::activeHiddenField($detail,'['.$counter.']jmldiscount',array('class'=>'span1 jmlDiskon numberOnly','onkeyup'=>'hitungPersenDiskon(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"));?>
                             <?php echo CHtml::activeHiddenField($detail,'['.$counter.']hargappnper',array('readonly'=>TRUE,'class'=>'span1 ppn')); ?>
                             <?php echo CHtml::activeHiddenField($detail,'['.$counter.']hargapphper',array('readonly'=>TRUE,'class'=>'span1 pph'));?>
                             <?php echo CHtml::HiddenField('subTotal',$subTotal, array('readonly'=>true,'class'=>'span2 numbersOnly subTotal', 'style'=>'width:70px;', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                         </td>
                         <!--<td></td>-->
                     </tr>
                     </tbody>
                     <?php    
//                     $cektanngal = CHtml::textField($detail, '['.$counter.']tglkadaluarsa');
//                     if($cektanggal == $cektanngal){
//                        echo "<input type=hidden id=cektgl value=1>";
//                     }else{
//                        echo "<input type=hidden id=cektgl value=0>";
//                     }
                      
                 }
                 echo "<tfoot>
                        <tr>
                             <td colspan='5'></td>
                             <td>".
                                  CHtml::activeHiddenField($modPenerimaanBarang,'jmldiscount', array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                                  "<br>".CHtml::activeHiddenField($modPenerimaanBarang,'totalpajakppn', array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                                  "<br>".CHtml::activeHiddenField($modPenerimaanBarang,'totalpajakpph', array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                             "</td>
                             <td>".
                                  CHtml::activeHiddenField($modPenerimaanBarang,'totalharga',array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                                  "<br>".CHtml::activeHiddenField($modPenerimaanBarang,'harganetto',array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                             "</td>
                         </tr>
                         </tfoot>
                    ";
//                 echo $tr;
            }
        } else {
            $no = 1;
            if (count($modPenerimaanDetail) > 0){
                $tr = '';
                foreach ($modPenerimaanDetail as $counter => $detail) {
                    $tampilDetail = ObatalkesM::model()->findByPk($detail->obatalkes_id);
                    $subTotal = ($detail->jmlterima*$detail->jmlkemasan*($detail->harganettoper + $detail->hargappnper + $detail->hargapphper)) - $detail->jmldiscount;
                    $tr .="<tbody><tr>
                            <td>".CHtml::TextField('noUrut',($no++),array('class'=>'span1 noUrut','readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")).
                                  CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']sumberdana_id'). 
                                  CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']obatalkes_id',array("class"=>'obatAlkes')). 
                           "</td>
                            <td>".$tampilDetail->sumberdana->sumberdana_nama."</td>
                            <td>".$tampilDetail->obatalkes_kategori."/<br/>".$tampilDetail->obatalkes_nama."</td>
                            <td>".CHtml::activeTextField($detail,'['.$detail->obatalkes_id.']tglkadaluarsa',array('readonly'=>TRUE,'class'=>'span2 tgl isRequired', 'style'=>'width:80px;','onkeypress'=>"return $(this).focusNextInputField(event)"))."</td>
                            <td>".CHtml::activeTextField($detail,'['.$detail->obatalkes_id.']jmlkemasan',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                            .CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']satuankecil_id',array('class'=>'span1')).$tampilDetail->satuankecil->satuankecil_nama. "</td>";
                    $tr .="<td>".CHtml::activeTextField($detail,'['.$detail->obatalkes_id.']jmlterima',array('class'=>'span1 terima numbersOnly','onkeyup'=>'hitungJumlahDariDiterima(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                            .CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']satuanbesar_id',array('class'=>'span1')).$tampilDetail->satuanbesar->satuanbesar_nama
                            .CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']hargabelibesar',array('readonly'=>FALSE,'class'=>'span1 numbersOnly','onkeyup'=>'hitungHargaNetto(this); hitungJumlahDariHargaNetto(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                            .CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']harganettoper',array('readonly'=>FALSE,'class'=>'span1 netto numbersOnly','onkeyup'=>'hitungJumlahDariHargaNetto(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                            .CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']persendiscount',array('class'=>'span1 persenDiskon float','onkeyup'=>'hitungJumlahDariPersentaseDiskon(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                            .CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']jmldiscount',array('class'=>'span1 jmlDiskon numbersOnly','maxlength'=>2,'onkeyup'=>'hitungJumlahDariDiskon(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                            .CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']hargappnper',array('readonly'=>false,'class'=>'span1 pph', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                            .CHtml::activeHiddenField($detail,'['.$detail->obatalkes_id.']hargappnper',array('readonly'=>FALSE,'class'=>'span1 ppn numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                            .CHtml::HiddenField('subTotal',$subTotal, array('readonly'=>true,'class'=>'span1 numbersOnly subTotal isRequired', 'style'=>'width:70px;', 'onkeypress'=>"return $(this).focusNextInputField(event)"))."</td>".
                            (($modPenerimaanBarang->isNewRecord) ? "<td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;'))."</td>" : '').
                            "</tr></tbody>";
                }
                
                echo $tr;
            }
            echo "<tfoot>
                        <tr>
                             <td colspan='4'></td>
                             <td>".
                                  CHtml::activeHiddenField($modPenerimaanBarang,'jmldiscount', array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                                  "<br>".CHtml::activeHiddenField($modPenerimaanBarang,'totalpajakppn', array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                                  "<br>".CHtml::activeHiddenField($modPenerimaanBarang,'totalpajakpph', array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                             "</td>
                             <td>".
                                  CHtml::activeHiddenField($modPenerimaanBarang,'totalharga',array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                                  "<br>".CHtml::activeHiddenField($modPenerimaanBarang,'harganetto',array('class'=>'span2 numbersOnly','style'=>'width:70px;','readonly'=>TRUE)).
                             "</td>
                             <td></td>
                         </tr>
                    </tfoot>
                    ";
        }
        ?>
    
    
</table>
<div class="form-actions">
<?php if (!$modPenerimaanBarang->isNewRecord){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idPenerimaan'=>$modPenerimaanBarang->penerimaanbarang_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}else{
    echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'cekValidasi()'));
    echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan', 'style'=>'display:none;'));
}

             $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
             $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
             $action=$this->getAction()->getId();
             $currentAction=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/'.$action);
             if (isset($_POST['idPembelianForm'])){//Jika Masuk ke Halaman ini memalui Informasi Rencana kebutuhan
                               echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                              $_POST['currentUrl'], 
                               array('class'=>'btn btn-danger',));
              }else{
                   echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                   $currentAction, 
                   array('class'=>'btn btn-danger',));
              }  
        ?> 
         <?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>
<?php $this->endWidget(); ?>
<?php
$urlGetSupplierPenerimaan = Yii::app()->createUrl('actionAjax/getSupplierPenerimaan');//MAsukan Dengan memilih Perencanaa
$urlGetObatAlkes =  Yii::app()->createUrl('actionAjax/getPermintaanPembelianDariObatAlkes');
$urlGetTglKadaluarsa =  Yii::app()->createUrl('actionAjax/getTglKadaluarsa');
$urlGetPermintaanPembelian = Yii::app()->createUrl('actionAjax/getPermintaanPembelian');
$idPermintaanPembelian = CHtml::activeId($modPenerimaanBarang,'permintaanpembelian_id');
$idPersenDiskon=CHtml::activeId($modFakturPembelian, 'persendiscount');
$idTotalHargaNetto=CHtml::activeId($modFakturPembelian, 'totharganetto');
$idTotalPajakPPN=CHtml::activeId($modFakturPembelian,'totalpajakppn');
$idTotalPajakPPH=CHtml::activeId($modFakturPembelian, 'totalpajakpph');
$idJumlahDiskon=CHtml::activeId($modFakturPembelian, 'jmldiscount');
$idSyaratBayar=CHtml::activeId($modFakturPembelian, 'syaratbayar_id');
$idTotalHargaBruto=CHtml::activeId($modFakturPembelian, 'totalhargabruto');
$idSupplier=CHtml::activeId($modPenerimaanBarang,'supplier_id');
$idPermintaanPembelian=CHtml::activeId($modPenerimaanBarang,'permintaanpembelian_id');

$konfigFarmasi = KonfigfarmasiK::model()->find();
$persenPPN=  $konfigFarmasi->persenppn;
$persenPPH=  $konfigFarmasi->persenpph;

$namaBank = CHtml::activeId($modUangMuka,'namabank');
$noRekening = CHtml::activeId($modUangMuka,'norekening');
$rekAtasNama = CHtml::activeId($modUangMuka,'rekatasnama');
$jumlahUang = CHtml::activeId($modUangMuka,'jumlahuang');
$urlHalamanIni = Yii::app()->createUrl($this->route);
$inputObat = CHtml::activeId($modPermintaanPembelian, 'obatAlkes');

$jscript = <<< JS
function setValue000(obj)
{
    id = $(obj).val();
    $.post("${urlGetSupplierPenerimaan}", {idSupplier:id},
        function(data)
        {
           $('#idSupplier').val(data.supplier_id);
           $('#idSupplier2').val(data.supplier_id);
           $('#tablePenerimaan tbody').html("");
//            $("#GFFakturPembelianT_biayamaterai").maskMoney({"defaultZero":true, "allowZero":true, "decimal":",", "thousands":".", "symbol":null, "precision":0});
           $.get('${urlHalamanIni}', {idSupplier:data.supplier_id}, function(datas){
                $.fn.yiiGridView.update('pasien-m-grid', {
                    url: document.URL+'&ObatsupplierM%5Bsupplier_id%5D='+data.supplier_id,
                }); 
            });
           
        }, "json");
}

function setValue()
{
    id = $("#${idSupplier}").val();
    $('#${obatAlkes}').val('');
    $('#idObatAlkes').val('');
    //$//{clearObatalkes} 
    if(id == "")
    {
        $("#berdasarkanSupplier").removeAttr("checked");
        alert("Anda belum memilih Supplier !");
    }
    
    if($("#berdasarkanSupplier").is(":checked"))
    {
        $("#idSupplier").val(id);
        //switch to dialogObatsupplierM
        $("#${inputObat}").parent().find("a").removeAttr("onclick");
        $("#${inputObat}").parent().find("a").attr("onclick",'$(\"#dialogObatAlkes\").dialog(\"open\");return false;');
        $.post("${urlGetSupplierPenerimaan}", {idSupplier:id},
        function(data)
        {
           $('#idSupplier').val(data.supplier_id);
           $('#idSupplier2').val(data.supplier_id);
           $('#tablePenerimaan tbody').html("");
//            $("#GFFakturPembelianT_biayamaterai").maskMoney({"defaultZero":true, "allowZero":true, "decimal":",", "thousands":".", "symbol":null, "precision":0});
           $.get('${urlHalamanIni}', {idSupplier:data.supplier_id}, function(datas){
                $.fn.yiiGridView.update('pasien-m-grid', {
                    url: document.URL+'&ObatsupplierM%5Bsupplier_id%5D='+data.supplier_id,
                }); 
            });
           
        }, "json");
        
    }
    else{
        id = "";
        $("#idSupplier").val(id);
        //switch to dialogObatAlkesM
        $("#${inputObat}").parent().find("a").removeAttr("onclick");
        $("#${inputObat}").parent().find("a").attr("onclick",'$(\"#dialogObatalkesM\").dialog(\"open\");return false;');
      }
    
    
}
           
function setDialogTindakan(){
   supplier =  $('#idSupplier').val();
   $.get('${urlHalamanIni}', {idSupplier:supplier}, function(datas){
        $.fn.yiiGridView.update('pasien-m-grid', {
            url: document.URL+'&ObatsupplierM%5Bsupplier_id%5D='+supplier,
        }); 
    });
}
function resetDiskonFaktur(){
   //mereset diskon faktur karena total brutonya berubah sehingga harus ada penghitungan ulang diskon untuk total harga bruto / hpp
    $('#GFFakturPembelianT_jmldiscount').val(0);
    $('#GFFakturPembelianT_persendiscount').val(0);
}
   
//function hitungJumlahDariDiterima(obj)
//{
//    jumlahKemasan = parseFloat($(this).parents("tr").find('input[name$="[jmlkemasan]"]').val());
//    jumlahDiterima = parseFloat(obj.value * jumlahKemasan);
//    jumlahPermintaan = parseFloat($(obj).parents("tr").find('.permintaan').val());
//    persenDiskon = parseFloat($(obj).parents("tr").find('.persenDiskon').val());
//    hargaProdukNetto = parseFloat($(obj).parents("tr").find('.netto').val());
//    if(jumlahDiterima>jumlahPermintaan){
//        $(obj).val(jumlahPermintaan);
//        hitungSemua();
//        alert('Jumlah Diterima Tidak boleh melebihi jumlah permintaan');
//        return false;
//    }else{
//        jumlahDiskonProduk = ((hargaProdukNetto * (persenDiskon/100)) * jumlahDiterima).toFixed(2);
//        if ($.isNumeric(jumlahDiskonProduk)){
//            $(obj).parents("tr").find('.jmlDiskon').val(jumlahDiskonProduk);
//        }
//    }
//    resetDiskonFaktur();
//    hitungSemua();
//}

//function hitungJumlahDariPersentaseDiskon(obj)
//{
//    jumlahKemasan = parseFloat($(this).parents("tr").find('input[name$="[jmlkemasan]"]').val());
//    persenDiskon = parseFloat(obj.value);
//    jumlahDiterima = parseFloat($(obj).parents("tr").find('.terima').val() * jumlahKemasan);
//    hargaProdukNetto = parseFloat($(obj).parents("tr").find('.netto').val());
//    
//    jumlahDiskonProduk = ((hargaProdukNetto * (persenDiskon/100)) * jumlahDiterima).toFixed(2);
//    if ($.isNumeric(jumlahDiskonProduk)){
//        $(obj).parents("tr").find('.jmlDiskon').val(jumlahDiskonProduk);
//    }
//    resetDiskonFaktur();
//    hitungSemua();
//}
//function hitungJumlahDariHargaNetto(obj)
//{
//   hargaProdukNetto = parseFloat(obj.value);
//   jumlahDiterima = parseFloat($(obj).parents('tr').find('.terima').val());
//   persenDiskon = parseFloat($(obj).parents("tr").find('.persenDiskon').val());
//   jumlahDiskonProduk = ((hargaProdukNetto * (persenDiskon/100)) * jumlahDiterima).toFixed(2);
//    if ($.isNumeric(jumlahDiskonProduk)){
//        $(obj).parents("tr").find('.jmlDiskon').val(jumlahDiskonProduk);
//    }
//   resetDiskonFaktur();
//   hitungSemua();
//}
   
//function hitungJumlahDariDiskon(obj)
//{
//    jumlahKemasan = parseFloat($(this).parents("tr").find('input[name$="[jmlkemasan]"]').val());
//    jumlahDiterima = $(obj).parents("tr").find('.terima').val() * jumlahKemasan;
//    jumlahDiskon = obj.value;
//    hargaProdukNetto = $(obj).parents("tr").find('.netto').val();
//    jumlahHarga = jumlahDiterima * hargaProdukNetto;    
//    if(jumlahDiskon=='' || jumlahDiskon==0){
//        $(obj).parents("tr").find('.persenDiskon').val(0);
//        $(obj).parents("tr").find('.jmlDiskon').val(0); 
//        $(obj).parents("tr").find('.jmlDiskon').select(); 
//    } else {
//        cekval = /^[0-9,]+$/.test(jumlahDiskon);
//        if(cekval){
//            if(jumlahDiskon>jumlahHarga){
//                alert('Jumlah Diskon Tidak Boleh Melebihi Jumlah Diterima * Harga Netto');
//                return false;
//            }else{
//                 persenDiskon = Math.round(((jumlahDiskon * 100)/hargaProdukNetto) / jumlahDiterima).toFixed(2);
//                 subtotal = ((hargaProdukNetto-jumlahDiskon)*jumlahDiterima).toFixed(2);
//                 $(obj).parents("tr").find('.persenDiskon').val(persenDiskon); 
//                 $(obj).parents("tr").find('.subTotal').val(subtotal); 
//            }
//        }else{
//            alert('Input harga diskon tidak valid');
//            $(obj).parents("tr").find('.jmlDiskon').focus(); 
//            $(obj).parents("tr").find('.jmlDiskon').select(); 
//        }
//    }
//   resetDiskonFaktur();
//   hitungSemua();
//}
   

function addPPN(obj){
   if(obj.checked==true){
       $('.terima').each(function(){
            hargaNettoProduk=parseFloat($(this).parents("tr").find('.nettoCadangan').val());
           jumlahPPN=hargaNettoProduk * (parseFloat(${persenPPN})/100);
           addNetto = hargaNettoProduk+jumlahPPN;
           $(this).parents("tr").find('.netto').val(addNetto);
       });
   } else {
       $('.terima').each(function(){
            hargaNettoProduk=parseFloat($(this).parents("tr").find('.nettoCadangan').val());
           $(this).parents("tr").find('.netto').val(hargaNettoProduk);
       });
   }
           hitungSemua();
}

function persenPPN(obj)
{
    if(obj.checked==true){ //Jika tambahkan ppn
      $('#tablePenerimaan tbody').find('.ppn').each(function() {
               hargaNettoProduk = parseFloat($(this).parents("tr").find('.subTotal').val());
               jumlahPPN = hargaNettoProduk * (parseFloat((${persenPPN}))/100);
               $(this).val(jumlahPPN);
            });
    }else{//Jika tidak tambahkan ppn
    $('#tablePenerimaan tbody tr .ppn').each(function() {
               $(this).val(0);
            });
    $('#termasukPPH').removeAttr('checked'); 
    $('#tablePenerimaan tbody tr .pph').each(function() {
                   $(this).val(0);
                   $(this).attr('readonly','TRUE');
                }); 
    $('#totalPPH').val(0);           
             
    }
 //hitungSemua();
 hitungTotalSemua();
}
               
function diskonAktif(obj){
   if(obj.checked==true){ 
      $('#GFFakturPembelianT_persendiscount').removeAttr('readonly','false');
      $('#diskonSemuaRp').attr('disabled','TRUE');
      $('.persenDiskon').val(0);
      //hitungSemua();
      
   } else {
      $('#GFFakturPembelianT_persendiscount').attr('readonly','TRUE');
      $('#diskonSemuaRp').removeAttr('disabled');
      $('#GFFakturPembelianT_persendiscount').val(0);
      $('.persenDiskon').val(0);
      //hitungSemua();
      
   }
    hitungTotalSemua();
}
               
function diskonAktifRp(obj){
   if(obj.checked==true){ 
      $('#GFFakturPembelianT_jmldiscount').removeAttr('readonly','false');
      $('#diskonSemua').attr('disabled','TRUE');
      $('.persenDiskon').val(0);
      //hitungSemua();
   } else {
      $('#GFFakturPembelianT_jmldiscount').attr('readonly','TRUE');
      $('#diskonSemua').removeAttr('disabled');
      $('#GFFakturPembelianT_jmldiscount').val(0);
      $('#GFFakturPembelianT_persendiscount').val(0);
      $('.persenDiskon').val(0);
      //hitungSemua();
   }
    hitungTotalSemua();
}
function persenPPH(obj)
{
    isPPH = obj.value;
    if($('#termasukPPN').is(':checked'))
    {           
        if(obj.checked==true){ //Jika tambah PPH
                 totalPPH=0;  
                 $('#tablePenerimaan tbody').find('.pph').each(function() {
                   hargaNettoProduk=parseFloat($(this).parents("tr").find('.netto').val());
                   jumlahPPN=parseFloat($(this).parents("tr").find('.ppn').val());
                   NettoDikurangPPN = hargaNettoProduk + jumlahPPN;
                   jumlahPPH=NettoDikurangPPN * (parseFloat(${persenPPH})/100);
                   $(this).val(jumlahPPH);
    //               $(this).removeAttr('readonly');
                });  

        }else{ //Jika Tidak tambah pph
                 $('.pph').each(function() {
                   $(this).val(0);
                   $(this).attr('readonly','TRUE');
                }); 
                $('#totalPPH').val(0);  
        }
    }else{
         alert('Anda Harus memilih PPN sebelum menambah PPH');
         $(obj).removeAttr('checked');          
    }       
                   //hitungSemua();
    hitungTotalSemua();
}          

function isUangMukaJava(obj)
{
    if(obj.checked==true){
    
        $('#${namaBank}').removeAttr('readonly');
        $('#${noRekening}').removeAttr('readonly');
        $('#${rekAtasNama}').removeAttr('readonly');
        $('#${jumlahUang }').removeAttr('readonly');  
        
    }else{
        
        $('#${namaBank}').attr('readonly','TRUE');
        $('#${noRekening}').attr('readonly','TRUE');
        $('#${rekAtasNama}').attr('readonly','TRUE');
        $('#${jumlahUang }').attr('readonly','TRUE');  
        
//        $('#${namaBank}').val('');
//        $('#${noRekening}').val('');
//        $('#${rekAtasNama}').val('');
//        $('#${jumlahUang }').val(0);  
        
    }

}

function cekValidasi()
{   
  langsungFaktur ='Tidak';  
  if ($('#isLangsungFaktur').is(':checked')){
     langsungFaktur ='Ya';
  }
 
  banyaknyaObat = $('.noUrut').length;
    data = false;
    $("#tablePenerimaan tbody .noUrut").each(function(){
      cektgl = $(this).parents("tr").find('.tgl').val();
      tglasal = $(this).parents("tr").find('.tglasal').val();
      if(cektgl == tglasal){
            data = true;
        }
    });

//    if (data){
//    conf = confirm("anda tidak akan merubah tanggal kardaluarsa");
//
//    }else{
//    conf = confirm("anda telah merubah tanggal kardaluarsa");
//    }
//    if(!conf){
//                return false;
//            }

  if ($('.isRequired').val()==''){
    alert ('Harap Isi Semua Data Yang Bertanda *');
  }else if(banyaknyaObat<1){
     alert('Anda Belum memimlih Obat Yang Akan Diminta');   
  }else if((langsungFaktur=='Ya') && ($('.isRequiredFaktur').val()=='')){
     alert('harap isi kolom yang bertanda * Pada Data Faktur');
  }else if($('.tgl isRequired').val()==''){
    alert('Anda Belum Mengisi Tanggal Kadaluarsa');
  }else{
    $('#btn_simpan').click();
  }
}

function submitPermintaanPembelian()
{
     idPermintaanPembelian = $('#${idPermintaanPembelian}').val();
        if(idPermintaanPembelian==''){
            alert('Silahkan Pilih Permintaan Pembelian Terlebih Dahulu');
        }else{
            $.post("${urlGetPermintaanPembelian}", { idPermintaanPembelian: idPermintaanPembelian },
            function(data){
                $('#tablePenerimaan').append(data.tr);
                $('#${idSupplier}').val(data.supplier_id);
                $('#GFPermintaanPembelianT_nopermintaan').attr('disabled','TRUE');
                $('#buttonPermintaanPembelian').attr('disabled','TRUE');
                $('#GFPermintaanPembelianT_tglpermintaanpembelian').attr('readonly','TRUE');
                $('#${idSupplier}').val(data.supplier_id);
                $('#GFPenerimaanBarangT_permintaanpembelian_id').val(data.permintaanpembelian_id);
                $('#${idSyaratBayar}').val(data.syaratbayar_id);
                if(data.isPPN=='1'){ //Jika termasuk PPN
                 $('#termasukPPN').attr('checked','checked');
                }
                
                if(data.isPPH=='1'){ //Jika termasuk PPH
                 $('#termasukPPH').attr('checked','checked');
                }
              
                //hitungSemua();
                hitungTotalSemua();
            }, "json");
           
                        
        }   
}

function submitPermintaanPembelianDariInformasi(idPembelian,noPembelian,tglPembelian)
{
     idPermintaanPembelian = idPembelian;
        if(idPermintaanPembelian==''){
            alert('Silahkan Pilih Permintaan Pembelian Terlebih Dahulu');
        }else{
            $.post("${urlGetPermintaanPembelian}", { idPermintaanPembelian: idPermintaanPembelian },
            function(data){
                $('#tablePenerimaan').append(data.tr);
                $('#${idSupplier}').val(data.supplier_id);
                $('#GFPermintaanPembelianT_nopermintaan').attr('disabled','TRUE');
                $('#buttonPermintaanPembelian').attr('disabled','TRUE');
                $('#GFPermintaanPembelianT_tglpermintaanpembelian').attr('readonly','TRUE');
                $('#${idSupplier}').val(data.supplier_id);
                $('#GFPenerimaanBarangT_permintaanpembelian_id').val(data.permintaanpembelian_id);
                $('#${idSyaratBayar}').val(data.syaratbayar_id);
                $('#formpermintaanpembelian').show();
                $('#formpemilihanobat').hide();
                if(data.isPPN=='1'){ //Jika termasuk PPN
                 $('#termasukPPN').attr('checked','checked');
                }
                
                if(data.isPPH=='1'){ //Jika termasuk PPH
                 $('#termasukPPH').attr('checked','checked');
                }
                
                    $('#buttonPermintaanPembelian').attr('disabled','TRUE');
                    $('#GFPermintaanPembelianT_nopermintaan').val(noPembelian);
                    $('#GFPermintaanPembelianT_tglpermintaanpembelian').val(tglPembelian);
              
                //hitungSemua();
                hitungTotalSemua();
            }, "json");
           
                        
        }   
}                

function hitungSemua()
{
    totalNetto=0;
    totalPajakPPN=0;
    totalPajakPPH=0;
    totalJumlahDiskon=0;
    totalHargaBruto=0;
    totalHargaNetto=0;
    hargaBrutoPerProduk=0;        
    totalDiskonSemua=0;    
    totalSubtotal=0;
    totDis = 0;
    
    $('.terima').each(function(){
           nettoPerProduk = parseFloat($(this).parents("tr").find('.netto').val());
           PPNPreProduk = parseFloat($(this).parents("tr").find('.ppn').val());
           PPHPreProduk = parseFloat($(this).parents("tr").find('.pph').val());
           jumlahKemasan = parseFloat($(this).parents("tr").find('input[name$="[jmlkemasan]"]').val());
           jumlahDiterima = parseFloat($(this).val());
           persenDiskonPerProduk = parseFloat($(this).parents("tr").find('.persenDiskon').val());
           
           jumlahDiskonProduk = Math.round((nettoPerProduk * (persenDiskonPerProduk/100)) * jumlahDiterima).toFixed(2);
           if (jQuery.isNumeric(jumlahDiskonProduk)){
               $(this).parents("tr").find('.jmlDiskon').val(jumlahDiskonProduk);
           }
            
           nettoDiterima =  nettoPerProduk * jumlahDiterima * jumlahKemasan;
           diskonPerProduk = parseFloat($(this).parents("tr").find('.jmlDiskon').val());
            
           hargaBrutoPerProduk = Math.round(nettoDiterima - diskonPerProduk) + ((PPNPreProduk + PPHPreProduk));
           hargaNettoPerProduk = Math.ceil(nettoDiterima);
                
           if (jQuery.isNumeric(hargaBrutoPerProduk)){
               $(this).parents("tr").find('.subTotal').val(hargaBrutoPerProduk);
           }
                
           totalHargaBruto+= Math.round(hargaBrutoPerProduk);  
           totalHargaNetto+= hargaNettoPerProduk;  
            
           totalNetto += nettoDiterima;
           totalPajakPPN += Math.round(PPNPreProduk);
           totalPajakPPH += (PPHPreProduk*jumlahDiterima);
           totalJumlahDiskon += diskonPerProduk;
           totalSubtotal += hargaBrutoPerProduk;
           totDis += persenDiskonPerProduk;
   });
            
    totalDiskonSemua=   Math.ceil(totalJumlahDiskon * 100)/totalHargaBruto; 
    if (jQuery.isNumeric(totalNetto)){
//        $('#${idTotalHargaNetto}').val(totalNetto);
        $('#GFPenerimaanBarangT_harganetto').val(totalNetto);       
        $('#totharganetto').val(totalNetto);       
    }
    if (jQuery.isNumeric(totalPajakPPN)){
        $('#${idTotalPajakPPN}').val(totalPajakPPN);        
        $('#GFPenerimaanBarangT_totalpajakppn').val(totalPajakPPN);
    }
    if (jQuery.isNumeric(totalPajakPPH)){
        $('#${idTotalPajakPPH}').val(totalPajakPPH);        
        $('#GFPenerimaanBarangT_totalpajakpph').val(totalPajakPPH);
    }
    if (jQuery.isNumeric(totalJumlahDiskon)){
         cek = ('#diskonSemuaRp').checked;
        if(cek==false){
//        $('#${idJumlahDiskon}').val(totalJumlahDiskon);
        }
//         $('#GFPenerimaanBarangT_jmldiscount').val(totalJumlahDiskon);
         $('#totdiskon').val(totalJumlahDiskon);
    }
    if (jQuery.isNumeric(totalHargaBruto)){
        $('#${idTotalHargaBruto}').val(totalHargaBruto);
        $('#tothargabruto').val(totalHargaBruto);

    }
    if (jQuery.isNumeric(totalDiskonSemua)){
//        $('#${idPersenDiskon}').val(totalDiskonSemua);          
//        $('#GFPenerimaanBarangT_persendiscount').val(totDis);
    }
    $('#GFPenerimaanBarangT_totalharga').val(totalSubtotal);
    noUrut = 1;
     $('.noUrut').each(function() {
          $(this).val(noUrut);
          noUrut = noUrut + 1;
     });

}

function numberOnly(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;


    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }else{
    $(obj).val(1);
    }
}

function hitungJumlahHargaDiskon(obj)
{
    besarDiskon =obj.value;
   
    $('.persenDiskon').each(function() {
          $(this).val(besarDiskon);
        });
    hitungSemua();
}

function numberOnlyNol(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;


    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }else{
    $(obj).val(0);
    }
}

function aktifForm(obj)
{
    if(obj.checked==true){
        $('#fakturPembelian input').not(".hasDatepicker").removeAttr('readonly','false');
        $('#fakturPembelian textArea').removeAttr('readonly','false');
        $('#GFFakturPembelianT_syaratbayar_id').removeAttr('readonly','false');
        $('#termasukPPN').removeAttr('disabled');
        $('#termasukPPH').removeAttr('disabled');
        $('#diskonSemua').removeAttr('disabled');
        $('#diskonSemuaRp').removeAttr('disabled');

    }else{
        $('#fakturPembelian input').attr('readonly',"TRUE");
        $('#fakturPembelian textArea').attr('readonly',"TRUE");
        $('#GFFakturPembelianT_syaratbayar_id').attr('readonly','TRUE');
        $('#GFFakturPembelianT_persendiscount').attr('readonly','TRUE');
        $('#termasukPPN').attr('disabled','TRUE');
        $('#termasukPPH').attr('disabled','TRUE');
        $('#termasukPPN').removeAttr('checked');
        $('#termasukPPH').removeAttr('checked');
        $('#diskonSemua').attr('disabled','TRUE');
        $('#diskonSemuaRp').attr('disabled','TRUE');
        $('#diskonSemua').removeAttr('checked');
        $('#diskonSemuaRp').removeAttr('checked');

    }
    hitungTotalSemua();
}

function hilangkanReadonly(obj)
{
    if(obj.checked==true){
        $('#buttonPermintaanPembelian').removeAttr('disabled','false');
        $('#GFPermintaanPembelianT_nopermintaan').removeAttr('disabled','false');
        $('#GFPermintaanPembelianT_obatAlkes').attr('readonly',"TRUE");
        $('#buttonPemilihanObat').attr('disabled',"TRUE");
        $('#buttonPilih').attr('disabled',"TRUE");
        $('#qtyObat').attr('readonly',"TRUE");
        $('#tambahObat').attr('disabled',"TRUE");
    }else{
        $('#buttonPermintaanPembelian').attr('disabled',"TRUE");
        $('#GFPermintaanPembelianT_nopermintaan').attr('disabled',"TRUE");
        $('#GFPermintaanPembelianT_obatAlkes').removeAttr('readonly',"TRUE");
        $('#buttonPemilihanObat').removeAttr('disabled',"TRUE");
        $('#buttonPilih').removeAttr('disabled',"TRUE");
        $('#qtyObat').removeAttr('readonly',"TRUE");
        $('#tambahObat').removeAttr('disabled',"TRUE");
        $('#GFPermintaanPembelianT_nopermintaan').val('');
        $('#GFPermintaanPembelianT_tglpermintaanpembelian').val('');
        $('#GFPenerimaanBarangT_supplier_id').val('');
        $('#formpermintaanpembelian').hide();
        $('#formpemilihanobat').show();
    
    
    }    
}
function hapusObat()
{
    banyakRow=$('#tablePenerimaan tr').length;

    for(i=2; i<=banyakRow; i++){
    $('#tablePenerimaan tr:last').remove();
    }
}

function submitObat()
{
    tgl_kadaluarsa = $('#tgl_kadaluarsa').val();
    idObat = $('#idObatAlkes').val();
    qty = parseFloat($('#qtyObat').val());
    idSupplier = $('#GFPenerimaanBarangT_supplier_id').val();
    diskon = $('#GFFakturPembelianT_persendiscount').val();
    if(tgl_kadaluarsa == ''){
        alert('Silahkan Isi Tgl Kadaluarsa Terlebih Dahulu');
        return false;
    }else
    if(idObat==''){
        alert('Silahkan Pilih Obat Terlebih Dahulu');
        return false;
    }else
    if(idSupplier==''){
        alert('Silahkan Pilih Supplier Terlebih Dahulu');
    }else{
        obat = $("#tablePenerimaan tbody").find(".obatAlkes[value="+idObat+"]");
        jumlah =  obat.length;
        if (jumlah == 0){
            $.post("${urlGetObatAlkes}", { idObat: idObat, qty:qty , tgl_kadaluarsa:tgl_kadaluarsa, idSupplier:idSupplier, diskon:diskon},
            function(data){
                $('#tablePenerimaan tbody').append(data.tr);
                $('#tablePenerimaan tbody tr:last').find(".numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":"","thousands":"","precision":0,"symbol":null});
                $('#tablePenerimaan tbody tr:last').find(".float").maskMoney({"defaultZero":true,"allowZero":true,"decimal":".","thousands":"","precision":2,"symbol":null});
                //hitungSemua();
                hitungTotalSemua();
            }, "json");
        }else{
            value = parseFloat(obat.parents("tr").find(".terima").val());
            value = parseFloat(value+qty)
            obat.parents("tr").find(".terima").val(value);
            //hitungSemua();
            hitungTotalSemua();
        }
    }
    $('#diskonSemuaRp').removeAttr('checked');
    $('#diskonSemua').removeAttr('checked');
    clear();
}

function remove(obj) {
    $(obj).parents('tr').remove();
    //hitungSemua();
    hitungTotalSemua();
}

function clear(){
    $("#GFPermintaanPembelianT_obatAlkes").val("");
    $("#idObatAlkes").val("");
    $("#qtyObat").val(1);
    urut = 1;
    $(".noUrut").each(function(){
        $(this).val(urut);
        urut++;
    });
}

function input_tgl(){
     tgl_kadaluarsa = $('#tgl_kadaluarsa').val();
        $.post('${urlGetTglKadaluarsa}', {tgl_kadaluarsa:tgl_kadaluarsa},function(data){
             $(".tgl").each(function(){
                $(this).val(data.tgl);
                urut++;
            });
        },'json');
}


JS;
Yii::app()->clientScript->registerScript('faktur',$jscript, CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('masukanobat',$jscript, CClientScript::POS_HEAD);
?>


<?php 
//========= Dialog buat cari data obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatAlkes',
    'options'=>array(
        'title'=>'Pencarian Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatsupplierM('search');
$modObatAlkes->unsetAttributes();
// $modObatAlkes->supplier_id = 0;
if(isset($_GET['ObatsupplierM'])) {
    $modObatAlkes->attributes = $_GET['ObatsupplierM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasien-m-grid',
	'dataProvider'=>$modObatAlkes->searchObatSupplierGF(),
	'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "href"=>"",
                                            "onClick" => "$(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                          $(\"#'.CHtml::activeId($modPermintaanPembelian,'obatAlkes').'\").val(\"".$data->obatalkes->obatalkes_nama."\");
                                                          $(\"#tgl_kadaluarsa\").val(\"".$data->obatalkes->tglkadaluarsa."\");
//                                                         submitObat();
                                                          $(\"#dialogObatAlkes\").dialog(\"close\"); 
                                                          return false;
                                                "))',
                        ),
                array(
                    'header'=>'Supplier',
                    'filter'=>CHtml::activeHiddenField($modObatAlkes, 'supplier_id', array("id"=>'idSupplier2')),
                    'value'=>'$data->supplier->supplier_nama',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes->obatalkes_kategori',
                ),
//                array(
//                    'header'=>'Golongan Obat',
//                    'value'=>'$data->obatalkes->obatalkes_golongan',
//                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_kodeobat'),
                    'value'=>'$data->obatalkes->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes->obatalkes_nama',
                ),
                array(
                    'header'=>'Asal Barang',
                    'value'=>'$data->obatalkes->sumberdana->sumberdana_nama',
                ),
//                array(
//                    'header'=>'Kadar Obat',
//                    'value'=>'$data->obatalkes->obatalkes_kadarobat',
//                ),
                array(
                    'header'=>'Jml Kemasan',
                    'value'=>'$data->obatalkes->kemasanbesar->kemasanbesar',
                ),
//                array(
//                    'header'=>'Kekuatan',
//                    'value'=>'$data->obatalkes->kekuatan',
//                ),
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->obatalkes->tglkadaluarsa',
                ),
                array(
                    'header'=>'Harga Kemasan',
                    'value'=>'number_format($data->hargabelibesar)',
                ),
	),
        'afterAjaxUpdate' => 'function(id, data){
        $("#ObatsupplierM_supplier_id").val($("#idSup").val());
        jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
$this->endWidget();
//========= end obatAlkes dialog =============================
?>

<?php 
//========= Dialog buat cari data ObatalkesM=========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatalkesM',
    'options'=>array(
        'title'=>'Pencarian Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatalkesM('search');
$modObatAlkes->unsetAttributes();
if(isset($_GET['ObatalkesM'])) {
    $modObatAlkes->attributes = $_GET['ObatalkesM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
  'id'=>'ObatalkesM-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
  'dataProvider'=>$modObatAlkes->search(),
  'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
  'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectObat",
                                    "onClick" => "
                                                  $(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                  $(\"#'.CHtml::activeId($modPermintaanPembelian,'obatAlkes').'\").val(\"$data->obatalkes_nama\");
                                                  $(\"#tgl_kadaluarsa\").val(\"".$data->tglkadaluarsa."\");
                                                  $(\"#dialogObatalkesM\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes_kategori',
                ),
                array(
                    'header'=>'Golongan Obat',
                    'value'=>'$data->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_kode'),
                    'value'=>'$data->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes_nama',
                ),
                array(
                    'header'=>'Asal Barang',
                    'value'=>'$data->sumberdana->sumberdana_nama',
                ),
                array(
                    'header'=>'Kadar Obat',
                    'value'=>'$data->obatalkes_kadarobat',
                ),
                array(
                    'header'=>'Kemasan Besar',
                    'value'=>'$data->kemasanbesar',
                ),
                array(
                    'header'=>'Kekuatan',
                    'value'=>'$data->kekuatan',
                ),
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->tglkadaluarsa',
                ),
            ),
            'afterAjaxUpdate' => 'function(id, data){
            jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
        ));
$this->endWidget();
//========= end ObatalkesM dialog =============================
?>
            
<script>
     function cekSupplier(params){
//         var id = $('#GFPenerimaanBarangT_supplier_id').val();
         var id = $('#idSupplier').val();
         if(id==''){
             alert('Silahkan Pilih terlebih dahulu Supplier');
         }else{
             if(params === 'tombol')
             {
                $('#dialogObatAlkes').dialog("open");return false;
             }             
         }
     }
     
     function diskonFakturRp(){
        if($('#diskonSemuaRp').is(':checked')){
              $('#GFFakturPembelianT_jmldiscount').removeAttr('readonly','false');
              $('#diskonSemua').attr('disabled','TRUE');
              $('#GFFakturPembelianT_persendiscount').val(0);
        }else{
              $('#GFFakturPembelianT_jmldiscount').attr('readonly','TRUE');
              $('#diskonSemua').removeAttr('disabled');
              $('#GFFakturPembelianT_jmldiscount').val(0);
              $('#GFFakturPembelianT_persendiscount').val(0);
        }
     }
     
     function diskonFakturPersen(){
         if($('#diskonSemua').is(':checked')){
              $('#GFFakturPembelianT_persendiscount').removeAttr('readonly','false');
              $('#diskonSemuaRp').attr('disabled','TRUE');
              $('#GFFakturPembelianT_jmldiscount').val(0);
        }else{
              $('#GFFakturPembelianT_persendiscount').attr('readonly','TRUE');
              $('#diskonSemuaRp').removeAttr('disabled');
              $('#GFFakturPembelianT_persendiscount').val(0);
              $('#GFFakturPembelianT_jmldiscount').val(0);
        }
     }
     
     function gantiDiskonFakturRp(obj){
         if($('#diskonSemuaRp').is(':checked')){
//              var hargaBruto = unformatNumber($('#GFFakturPembelianT_totalhargabruto').val());
              var hargaBruto = unformatNumber($('#tothargabruto').val());
              var CadanganHarga = unformatNumber($('#hargaBruto').val());
              var diskonRp = unformatNumber(obj.value);
              
              var totHargaBruto  = Math.round(hargaBruto - diskonRp);
              var diskonPersen = ((((hargaBruto - totHargaBruto)/hargaBruto)*100)).toFixed(1);
              
              $('#GFFakturPembelianT_totalhargabruto').val(totHargaBruto);
              $('#GFFakturPembelianT_persendiscount').val(diskonPersen);
              
        }else{
            var diskonRp = unformatNumber($('#GFFakturPembelianT_jmldiscount').val());
            $('#GFFakturPembelianT_jmldiscount').val(diskonRp);
        }
     }
     
     function gantiDiskonFakturPersen(obj){
         if($('#diskonSemua').is(':checked')){
              var hargaBruto = unformatNumber($('#GFPenerimaanBarangT_totalharga').val());
              var diskonPersen = unformatNumber(obj.value);
              var totHargaBruto  = Math.round(hargaBruto - (hargaBruto * (diskonPersen / 100)));
              var diskonRp = Math.round(hargaBruto * (diskonPersen/100));
              
              
              $('#GFFakturPembelianT_totalhargabruto').val(totHargaBruto);
              $('#GFFakturPembelianT_jmldiscount').val(diskonRp);
        }else{
            var diskonPersen = unformatNumber($('#GFFakturPembelianT_persendiscount').val());
            
            $('#GFFakturPembelianT_persendiscount').val(diskonPersen);
        }
     }
     
//     function cekInputan(){
//        $('.numbersOnly').each(function(){this.value = unformatNumber(this.value)});
//        return true;
//     }

//Created by: ichan90@yahoo.co.id
function hitungHargaNetto(obj){
     var jmlKemasan = unformatNumber($(obj).parent().parent().find("input[name$='[jmlkemasan]']").val());
     var hargaKemasan = unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
     var hargaNetto = hargaKemasan / jmlKemasan;
     $(obj).parent().parent().find("input[name$='[harganettoper]']").val(hargaNetto.toFixed(2));
 }
function hitungHargaPpnPPh(obj){ //dari hargabelibesar
    var persenPpn = <?php echo $persenPPN;?>;
    var persenPph = <?php echo $persenPPH;?>;
    var hargaPpn = 0;
    var hargaPph = 0;
    var hargaBeliBesar =  unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
    //hitung ppn $ pph
    if($('#termasukPPN').is(':checked')){
        hargaPpn = (hargaBeliBesar * persenPpn / 100);
        if($('#termasukPPH').is(':checked')){
            hargaPph = ((hargaBeliBesar + hargaPpn) * persenPph / 100);
        }
    }
    $(obj).parent().parent().find('input[name$="[hargappnper]"]').val(hargaPpn);
    $(obj).parent().parent().find('input[name$="[hargapphper]"]').val(hargaPph);
}
//    Perhitungan subtotal efektif bisa digunakan di field mana saja selama satu baris / <tr> 
function hitungSubtotal(obj){
    var jmlTerima = unformatNumber($(obj).parent().parent().find("input[name$='[jmlterima]']").val());
    var jmlKemasan =  unformatNumber($(obj).parent().parent().find("input[name$='[jmlkemasan]']").val());
    var hargaBeliBesar =  unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
    var persenDiskon =  unformatNumber($(obj).parent().parent().find("input[name$='[persendiscount]']").val());
    var subtotal = (jmlTerima * hargaBeliBesar) - (jmlTerima * hargaBeliBesar * persenDiskon / 100); 
    $(obj).parent().parent().find("#subTotal").removeAttr('value');
    $(obj).parent().parent().find("#subTotal").val(subtotal.toFixed(0));
    //hitung jml diskon 
    var jmlDiskon = (hargaBeliBesar * persenDiskon / 100) * jmlTerima;
    $(obj).parent().parent().find('input[name$="[jmldiscount]"]').val(jmlDiskon);
    hitungHargaPpnPPh(obj);
    hitungTotalSemua();
 }
 function hitungJmlDiskon(obj){ //dari persen diskon
     var hargaBeliBesar =  unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
     var jmlTerima = unformatNumber($(obj).parent().parent().find("input[name$='[jmlterima]']").val());
     var persenDiskon = parseFloat($(obj).val());
     var jmlDiskon = (hargaBeliBesar * persenDiskon / 100) * jmlTerima;
     $(obj).parent().parent().find('input[name$="[jmldiscount]"]').val(jmlDiskon.toFixed(2));
 }
 function hitungPersenDiskon(obj){ //dari diskon total
     var jmlTerima = unformatNumber($(obj).parent().parent().find("input[name$='[jmlterima]']").val());
     var hargaBeliBesar =  unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
     var jmlDiskon = parseFloat($(obj).val());
     var persenDiskon = jmlDiskon * 100 / (hargaBeliBesar * jmlTerima);
     $(obj).parent().parent().find('input[name$="[persendiscount]"]').val(persenDiskon.toFixed(2));
     //hitung subtotal
     var subtotal = (jmlTerima * hargaBeliBesar) - (jmlTerima * hargaBeliBesar * persenDiskon / 100)
     $(obj).parent().parent().find("#subTotal").val(subtotal);
     hitungTotalSemua();
 }
 
 function hitungTotalSemua(){
     var persenPpn = <?php echo $persenPPN;?>;
     var persenPph = <?php echo $persenPPH;?>;
     var total = 0;
     var totalNonDiskon = 0;
     var totalDiskon = 0;
     var totalPPN = 0;
     var totalPPH = 0;
     var noUrut = 1;
     $('#tablePenerimaan tbody tr').each(function(){
        $(this).find('.noUrut').val(noUrut);
        noUrut ++;
        hitungHargaPpnPPh(this);
        total += unformatNumber($(this).find('#subTotal').val()); 
        totalDiskon += unformatNumber($(this).find('input[name$="[jmldiscount]"]').val());
     });
     totalNonDiskon = total + totalDiskon;
     if($('#termasukPPN').is(':checked')){
        totalPPN = total * persenPpn / 100;
        if($('#termasukPPH').is(':checked')){
            totalPPH = (total + totalPPN) * persenPph / 100;
        }
     }
     totalNonDiskon = totalNonDiskon + totalPPN + totalPPH;
     total = total + totalPPN + totalPPH;
     $('#tablePenerimaan tfoot').find('input[name$="[jmldiscount]"]').val(totalDiskon.toFixed(0));
     $('#tablePenerimaan tfoot').find('input[name$="[totalharga]"]').val(total.toFixed(0));
     $('#GFFakturPembelianT_totalhargabruto').val(total.toFixed(0));
     $('#tablePenerimaan tfoot').find('input[name$="[harganetto]"]').val(totalNonDiskon.toFixed(0));
     $('#GFFakturPembelianT_totharganetto').val(totalNonDiskon.toFixed(0));
     $('#tablePenerimaan tfoot').find('input[name$="[totalpajakppn]"]').val(totalPPN);
     $('#GFFakturPembelianT_totalpajakppn').val(totalPPN);
     $('#tablePenerimaan tfoot').find('input[name$="[totalpajakpph]"]').val(totalPPH);
     $('#GFFakturPembelianT_totalpajakpph').val(totalPPH);
 }
 
</script>
<?php
if($modFakturPembelian->isNewRecord){ //jika load modFaktur jangan hitung semua dulu
    echo '<script>hitungTotalSemua();</script>';
}
?>