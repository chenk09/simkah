<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
    if (isset($_GET['id'])){
        Yii::app()->user->setFlash('success', 'Data Berhasil Disimpan !');
    }
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfrencanaKebFarmasi-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onSubmit'=>'return cekValidasi();'),
        'focus'=>'#GFRencanaKebFarmasiT_obatAlkes',
)); 
$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $form->errorSummary($modRencanaKebFarmasi); ?>
<style>
    .numbersOnly, .float{
        text-align: right;
    }
</style>
<fieldset>
    <legend class="rim2">Rencana Kebutuhan</legend>
    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
    
    <table>
        <tr>
            <td width="50%">
                    <?php echo $form->textFieldRow($modRencanaKebFarmasi,'noperencnaan',array('readonly'=>true,'class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modRencanaKebFarmasi,'tglperencanaan', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                            'model'=>$modRencanaKebFarmasi,
                                            'attribute'=>'tglperencanaan',
                                            'mode'=>'date',
                                            'options'=> array(
                                                //'dateFormat'=>'d M Y H:i:s',
                                                'maxDate'=>'d',

                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                            ),
                                )); ?>
                            </div>
                    </div>
            </td>
            <td>
                <?php if ($modRencanaKebFarmasi->isNewRecord) { ?>
                <div class="control-group">
                            <?php echo $form->labelEx($modRencanaKebFarmasi,'obatAlkes', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php echo CHtml::hiddenField('idObatAlkes');?>
                                <?php echo CHtml::hiddenField('kemasanbesar');?>
                                <div style="float:left;">
                                <?php $this->widget('MyJuiAutoComplete',array(
                                            'model'=>$modRencanaKebFarmasi,
                                            'attribute'=>'obatAlkes',
                                            'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/ObatAlkes'),
                                            'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'select'=>'js:function( event, ui ) {
                                                          $("#GFRencanaKebFarmasiT_obatAlkes").val(ui.item.obatalkes_nama);
                                                          $("#idObatAlkes").val(ui.item.obatalkes_id);
                                                          $("#kemasanbesar").val(ui.item.kemasanbesar);
                                                          $("#qtyObatK").val(ui.item.kemasanbesar);
                                                }',
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogObatAlkes'),
                                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3','style'=>'float:left;'),
                                )); ?>
                                </div>
                                <?php /* echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                    array('onclick'=>'$("#dialogObatAlkes").dialog("open");return false;',
                                          'class'=>'btn btn-primary',
                                          'onkeypress'=>"return $(this).focusNextInputField(event)",
                                          'rel'=>"tooltip",
                                          'title'=>"Klik Untuk Pencarian Obat Alkes Lebih Lanjut",)); */ ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <?php echo CHtml::label('Qty (Satuan Kecil)','QtyKecil',array('class'=>'control-label'));?>
                            <div class="controls">
                                <?php echo CHtml::textField('qtyObatK','',array('class'=>'span1 numbersOnly satuankecil','onkeypress'=>"return $(this).focusNextInputField(event)",'onkeyup'=>'hitungKemasanBesar();'));?>
                            </div>
                        </div>
                        <div class="control-group"><?php echo CHtml::label('Qty (Satuan Besar)','Qty',array('class'=>'control-label'));?>                        
                        <div class="controls">
                            <?php echo CHtml::textField('qtyObat','1',array('class'=>'span1 numbersOnly','onkeypress'=>"return $(this).focusNextInputField(event)"));?>
                            <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                array('onclick'=>'submitObat();return false;',
                                      'class'=>'btn btn-primary',
                                      'onkeypress'=>"submitObat();return $('#GFRencanaKebFarmasiT_obatAlkes').focus();",
                                      'rel'=>"tooltip",
                                      'title'=>"Klik Untuk Menambahkan Obat",
                                      
                                    )); ?>
                            </div>
                        </div>                        
                <?php } ?>
            </td>
        </tr>
    </table>
</fieldset>
<br/>
        <?php if (isset($modDetails)){
            echo $form->errorSummary($modDetails); }?>
<table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
    <tr>
<!--        <th><?php // echo CHtml::checkBox('checkListUtama',true,array('onclick'=>'checkAll(\'cekList\',this);hitungSemua();'));?></th>-->
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Tanggal <br/> Kadaluarsa</th>
        <th>Jumlah Kemasan<br>(Satuan Kecil/Besar)</th>
        <th>Jumlah Permintaan<br>(Satuan Besar)</th>
        <th>Stok Akhir</th>
        <th>Min Stok</th>
        <?php echo ($modRencanaKebFarmasi->isNewRecord) ? "<th>Batal</th>" : ''; ?>
    </tr>
    </thead>
    <tbody>
        <?php if (isset($modDetails)){
                if (count($modDetails) > 0){
                    $tr = '';
                    $no = 1;
                     foreach ($modDetails as $key => $detail) {
                         $modStokObatAlkes=StokobatalkesT::model()->findAll('obatalkes_id='.$detail->obatalkes_id.'');
                         $stokAkhir = 0;
                         $maxStok = 0;
                         foreach($modStokObatAlkes AS $tampil):
                            $stokAkhir+=$tampil['qtystok_in'];
                            $maxStok+=$tampil['qtystok_out'];
                         endforeach;
                         $stokAkhir = $stokAkhir;
                         $modObatAlkes = ObatalkesM::model()->findByPk($detail->obatalkes_id);
                         $tr.="<tr>
                            <td>". CHtml::TextField('noUrut',$no++,array('class'=>'span1 noUrut','readonly'=>TRUE)).
                                   CHtml::activeHiddenField($detail,'['.$key.']obatalkes_id',array('class'=>'obatAlkes')).
                                   CHtml::activeHiddenField($detail,'['.$key.']sumberdana_id'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']satuankecil_id'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']satuanbesar_id'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']harganettorenc'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']stokakhir'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']maksimalstok'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']minimalstok'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']tglkadaluarsa'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']jmlkemasan').
                           "</td>
                            <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                            <td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                            <td>".$modObatAlkes->tglkadaluarsa."</td>
                            <td>".CHtml::activeTextField($detail,'['.$key.']jmlkemasan',array('class'=>'span1 numbersOnly','readonly'=>true))
                                 ." ".$modObatAlkes->satuankecil->satuankecil_nama."/".$modObatAlkes->satuanbesar->satuanbesar_nama."</td>
                            <td>".CHtml::activeTextField($detail,'['.$key.']jmlpermintaan',array('class'=>'span1 numbersOnly permintaan','readonly'=>true))
                                 ." ".$modObatAlkes->satuanbesar->satuanbesar_nama."</td>
                            <td>".$stokAkhir
                                 ." ".$modObatAlkes->satuankecil->satuankecil_nama."</td>
                            <td>".$modObatAlkes->minimalstok." ".$modObatAlkes->satuankecil->satuankecil_nama."</td>".
                            (($modRencanaKebFarmasi->isNewRecord) ? "<td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;'))."</td>" : '').
                                 "</tr>   
                        ";
                     }
                     echo $tr;
                }
        } ?>
    </tbody>
</table>
        <div class="form-actions">
		<?php 
                    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
                    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
                    $action=$this->getAction()->getId();
                    $currentAction=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/'.$action);
                ?>
            <?php
                    if (!$modRencanaKebFarmasi->isNewRecord){
                    $urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idRencana'=>$modRencanaKebFarmasi->rencanakebfarmasi_id));
                    $js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
                    Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
                        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
                    }else{
                        echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                    }
                    ?>  
                <?php echo CHtml::link(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                       $currentAction, 
                        array('class'=>'btn btn-danger',));?>
       <?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
	</div>

<?php $this->endWidget(); ?>
<?php
$urlGetObatAlkes = Yii::app()->createUrl('actionAjax/getObatAlkes');
$obatAlkes=CHtml::activeId($modRencanaKebFarmasi,'obatAlkes');
$jscript = <<< JS
function submitObat()
{
    idObat = $('#idObatAlkes').val();
    qtyObat = parseFloat($('#qtyObat').val());
    qtyKemasan = parseFloat($('#qtyObatK').val());
    if(idObat==''){
        alert('Silahkan Pilih Obat Terlebih Dahulu');
    }else{
        obat = $("#tableObatAlkes tbody").find(".obatAlkes[value="+idObat+"]");
        jumlah =  obat.length;
        if (jumlah == 0){
            $.post("${urlGetObatAlkes}", { idObat: idObat,qtyObat:qtyObat,qtyKemasan:qtyKemasan},
            function(data){
                $('#tableObatAlkes').append(data.tr);
                $("#tableObatAlkes tr:last").find(".numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null})
                hitungSemua();
            }, "json");
        }else{
            value = parseFloat(obat.parents("tr").find(".permintaan").val());
            value = parseFloat(value+qtyObat)
            obat.parents("tr").find(".permintaan").val(value);
        }
        $('#${obatAlkes}').val('');
        $('#idObatAlkes').val('');
        $('#qtyObat').val('');
        $('#qtyObatK').val('');
        
    }   
}

function hitungSemua()
{
     noUrut = 1;
     $('.noUrut').each(function() {
          $(this).val(noUrut);
          noUrut++;
     });

}

noUrut = 1;
     $('.noUrut').each(function() {
          $(this).val(noUrut);
          noUrut = noUrut + 1;
     });

function numberOnly(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;
    value = value.replace(/[0-9]*/g, "");
    var msg = "Only Integer Values allowed.";

    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }
}


function remove_row(obj) {
    $(obj).parents('tr').detach();
    hitungSemua();
}

   
function cekValidasi(event)
{   
  banyaknyaObat = $('.obatAlkes').length;
  hargaNettoPenawaran =  $('#PenawarandetailT_harganetto').val(); 
  if ($('.isRequired').val()==''){
    alert ('Harap Isi Semua Data Yang Bertanda *');
    return false;
  }else if(banyaknyaObat<1){
     alert('Anda belum memilih Obat Yang Akan Diminta');   
     return false;
  }else if(hargaNettoPenawaran==0){
     alert('Anda Belum memilih Obat Yang Akan Diminta');   
     return false;
  }else{
     $('#btn_simpan').click();
     return true;
  }
  
}
JS;
Yii::app()->clientScript->registerScript('masukanobat',$jscript, CClientScript::POS_HEAD);
?>
<?php 
//========= Dialog buat cari data obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatAlkes',
    'options'=>array(
        'title'=>'Pencarian Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatalkesfarmasiV('search');
$modObatAlkes->unsetAttributes();
if(isset($_GET['ObatalkesfarmasiV'])) {
    $modObatAlkes->attributes = $_GET['ObatalkesfarmasiV'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasien-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modObatAlkes->search(),
	'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "$(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                          $(\"#kemasanbesar\").val(\"$data->kemasanbesar\");
                                                          $(\"#qtyObatK\").val(\"$data->kemasanbesar\");
                                                          $(\"#'.CHtml::activeId($modRencanaKebFarmasi,'obatAlkes').'\").val(\"$data->obatalkes_nama\");
//                                                          submitObat();
                                                          $(\"#dialogObatAlkes\").dialog(\"close\");    
                                                          return false;
                                                "))',
                        ),
                'obatalkes_kategori',
                'obatalkes_golongan',
                'obatalkes_kode',
                'obatalkes_nama',
                'sumberdana_nama',
                'obatalkes_kadarobat',
                'kemasanbesar',
                'kekuatan',
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->tglkadaluarsa',
                ),

	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end obatAlkes dialog =============================
?>

<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>
<script>
function hitungKemasanBesar(){
   var satuankecil = $('#qtyObatK').val();
   var kemasanbesar = $('#kemasanbesar').val();
   if(kemasanbesar == ''){
       kemasanbesar = 0;
   }
   satuanbesar = Math.ceil(satuankecil / kemasanbesar);
    
    
    if(satuanbesar > 0){
        satuanbesar = satuanbesar;
    }else{
        satuanbesar = 1;
    }
    
    $('#qtyObat').val(satuanbesar);
}    
</script>
