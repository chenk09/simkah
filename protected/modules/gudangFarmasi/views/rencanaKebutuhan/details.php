<table width="100%">
    <tr>
        <td width="50%">
             <b><?php echo CHtml::encode($modRencanaKebutuhan->getAttributeLabel('noperencnaan')); ?>:</b>
            <?php echo CHtml::encode($modRencanaKebutuhan->noperencnaan); ?>
            <br />
             <b><?php echo CHtml::encode($modRencanaKebutuhan->getAttributeLabel('tglperencanaan')); ?>:</b>
            <?php echo CHtml::encode($modRencanaKebutuhan->tglperencanaan); ?>
            <br />
        </td>
        <td>
             <b><?php echo CHtml::encode($modRencanaKebutuhan->getAttributeLabel('ruangan_id')); ?>:</b>
            <?php echo CHtml::encode($modRencanaKebutuhan->ruangan->ruangan_nama); ?>
<!--            <br />
             <b><?php // echo CHtml::encode($modRencanaKebutuhan->getAttributeLabel('create_time')); ?>:</b>
            <?php // echo CHtml::encode($modRencanaKebutuhan->create_time); ?>
            <br />-->
        </td>
    </tr>   
</table>
<hr/>
<style>
.table tr td.right-align{
    text-align: right;
}
</style>
<table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Jumlah Kemasan</th>
        <th>Jumlah Permintaan</th>
        <!--<th>Harga Netto</th>--> <!-- EHJ-1679 -->
<!--        <th>PPN</th>
        <th>PPH</th>-->
        <!--<th>SubTotal</th>--><!-- EHJ-1679 -->
    </tr>
    </thead>
    <tbody>
    <?php
    $no=1;
    if (count($modDetailRencana) > 0){
        $totalPermintaan = 0;
        $totalNetto = 0;
        $totalHPP = 0;
        $totalPPN = 0;
        $totalPPH = 0;
        $totalRencana = 0;
        foreach($modDetailRencana AS $tampilData):
//            echo "<tr>
//                    <td>".$no."</td>
//                    <td>".$tampilData->sumberdana['sumberdana_nama']."</td>  
//                    <td>".$tampilData->obatalkes['obatalkes_kategori']."<br>".$tampilData->obatalkes['obatalkes_nama']."</td>   
//                    <td>".$tampilData->satuankecil['satuankecil_nama']."<br>".$tampilData->satuanbesar['satuanbesar_nama']."</td>   
//                    <td class='right-align'>".MyFunction::formatNumber($tampilData['jmlpermintaan'])."</td>
//                    <td class='right-align'>".MyFunction::formatNumber($tampilData['harganettorenc'])."</td>     
//                    <td class='right-align'>".MyFunction::formatNumber($tampilData['hargappnrenc'])."</td>     
//                    <td class='right-align'>".MyFunction::formatNumber($tampilData['hargapphrenc'])."</td>     
//                    <td class='right-align'>".MyFunction::formatNumber($tampilData['hargatotalrenc'])."</td>     
//
//                 </tr>";  
//            $no++;
//            $totalPermintaan+=$tampilData['jmlpermintaan'];
//            $totalNetto+=$tampilData['harganettorenc'];
//            $totalPPN+=$tampilData['hargappnrenc'];
//            $totalPPH+=$tampilData['hargapphrenc'];
//            $totalRencana+=$tampilData['hargatotalrenc'];
            
            echo "<tr>
                    <td>".$no."</td>
                    <td>".$tampilData->sumberdana['sumberdana_nama']."</td>  
                    <td>".$tampilData->obatalkes['obatalkes_kategori']."<br>".$tampilData->obatalkes['obatalkes_nama']."</td>   
                    <td class='right-align'>".MyFunction::formatNumber($tampilData['jmlkemasan'])." ".$tampilData->satuankecil['satuankecil_nama']."/".$tampilData->satuanbesar['satuanbesar_nama']."</td>
                    <td class='right-align'>".MyFunction::formatNumber($tampilData['jmlpermintaan'])." ".$tampilData->satuanbesar['satuanbesar_nama']."</td>                       

                 </tr>";  
        // EHJ-1679
//        <td class='right-align'>Rp. ".MyFunction::formatNumber($tampilData['harganettorenc'])." /".$tampilData->satuankecil['satuankecil_nama']."</td>       
//                    <td class='right-align'>Rp. ".MyFunction::formatNumber($tampilData['hargatotalrenc'])."</td>  
        // END EHJ-1679
            $no++;
            $totalPermintaan+=$tampilData['jmlpermintaan'];
            $totalNetto+=$tampilData['harganettorenc'];
            $totalPPN+=$tampilData['hargappnrenc'];
            $totalPPH+=$tampilData['hargapphrenc'];
            $totalRencana+=$tampilData['hargatotalrenc'];
        endforeach; }else{
            echo '<tr><td colspan=9>'.Yii::t('zii','No results found.').'</td></tr>';
        } ?>
     <tbody><tfoot>
    <?php 
//    echo "<tr>
//            <td colspan='4'> Total</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalPermintaan)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalNetto)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalPPN)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalPPH)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalRencana)."</td>
//         </tr>"; 
   /* EHJ-1679
    * echo "<tr>
            <td colspan='6'> Total</td>
            <td class='right-align'>Rp. ".MyFunction::formatNumber($totalRencana)."</td>
         </tr>"; 
    * 
    */
//    <td class='right-align'>".MyFunction::formatNumber($totalPermintaan)."</td>
//            <td class='right-align'>Rp. ".MyFunction::formatNumber($totalNetto)."</td>
    ?>
    </tfoot>
</table>

<?php
if (!isset($caraPrint)){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idRencana'=>$modRencanaKebutuhan->rencanakebfarmasi_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}
?>    
