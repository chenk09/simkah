<?php
Yii::app()->clientScript->registerScript('search', "
$('#divSearch-form form').submit(function(){
	$.fn.yiiGridView.update('rencana-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
<legend class="rim2">Informasi Rencana Kebutuhan</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'rencana-m-grid',
	'dataProvider'=>$modRencanaKebFarmasi->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tglperencanaan',
                'noperencnaan',
                array(
                   'name'=>'ruangan_id',
                    'type'=>'raw',
                    'value'=>'$data->ruangan->ruangan_nama',
                ),
//		'create_time',
//                'create_loginpemakai_id',
                array(
               'header'=>'Details',
               'type'=>'raw',
               'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("RencanaKebutuhan/Details",array("idRencana"=>$data->rencanakebfarmasi_id)),
                            array("class"=>"", 
                                  "target"=>"rencana",
                                  "onclick"=>"$(\"#dialogRencana\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat details Rencana",
                            ))',
                ),
            array(
               'header'=>'Penawaran',
               'type'=>'raw',
               'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("PermintaanPenawaran/Index",array("idRencana"=>$data->rencanakebfarmasi_id)),
                            array("class"=>"", 
                                  "title"=>"Klik Mendaftarkan Ke Penawaran",
                            ))',
                ),
//            array(
//               'header'=>'Pembelian',
//               'type'=>'raw',
//               'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>","'.$this->createUrl("PermintaanPembelian/Index").'&idRencana=$data->rencanakebfarmasi_id",
//                            array("class"=>"", 
//                                  "title"=>"Klik Mendaftarkan Ke Penawaran",
//                            ))',
//                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
</fieldset>

<div id="divSearch-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'rencana-t-search',
        'type'=>'horizontal',
)); ?> 
<fieldset>
    <legend class="rim">Pencarian</legend>
    <table>
        <tr>
            <td>
                <div class="control-group ">
                        <?php echo CHtml::label('Tgl Rencana','tglRencanaKebutuhan', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php   
                                        $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modRencanaKebFarmasi,
                                                        'attribute'=>'tglAwal',
                                                        'mode'=>'datetime',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                )); ?>
                            </div>
                    </div>
                    <div class="control-group ">
                        <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php   
                                        $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modRencanaKebFarmasi,
                                                        'attribute'=>'tglAkhir',
                                                        'mode'=>'datetime',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                )); ?>
                            </div>
                    </div> 
            </td>
            <td>
                 <?php echo $form->textFieldRow($modRencanaKebFarmasi,'noperencnaan',array('class'=>'numberOnly')); ?>
                  <?php echo $form->dropDownListRow($modRencanaKebFarmasi,'ruangan_id',
                                                                   CHtml::listData($modRencanaKebFarmasi->RuanganItems, 'ruangan_id', 'ruangan_nama'),
                                                                   array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                   'empty'=>'-- Pilih --',)); ?>
            </td>
        </tr>
    </table>
    <div class="form-actions">
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?><?php
            $content = $this->renderPartial('../tips/informasi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
         ?>
    </div>
</fieldset>
<?php $this->endWidget(); ?>
</div>

<?php 
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action=$this->getAction()->getId();
$currentUrl=  Yii::app()->createUrl($module.'/'.$controller.'/'.$action);

$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hiddenPenawaran',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'action'=>Yii::app()->createAbsoluteUrl($module.'/permintaanPenawaran/index'),
)); ?>
    <?php echo CHtml::hiddenField('idPerencanaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('noPerencanaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('tglPenawaranForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('currentUrl',$currentUrl,array('readonly'=>true));?>
<?php $this->endWidget(); ?>
<?php 
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hiddenPembelian',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'action'=>Yii::app()->createAbsoluteUrl($module.'/permintaanPembelian/index'),
)); ?>
    <?php echo CHtml::hiddenField('idPerencanaanFormPembelian','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('noPerencanaanFormPembelian','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('tglPerencanaanFormPembelian','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('currentUrl',$currentUrl,array('readonly'=>true));?>

<?php $this->endWidget(); ?>
<?php
$js = <<< JSCRIPT
function formPenawaran(idPenawaran,noPerencanaan,tglPerencanaan)
{
    $('#idPerencanaanForm').val(idPenawaran);
    $('#noPerencanaanForm').val(noPerencanaan);
    $('#tglPenawaranForm').val(tglPerencanaan);
    $('#form_hiddenPenawaran').submit();
}

function formPembelian(idPenawaran,noPerencanaan,tglPerencanaan)
{
    $('#idPerencanaanFormPembelian').val(idPenawaran);
    $('#noPerencanaanFormPembelian').val(noPerencanaan);
    $('#tglPerencanaanFormPembelian').val(tglPerencanaan);
    $('#form_hiddenPembelian').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);                        



// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogRencana',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Details Rencana',
                        'autoOpen'=>false,
                        'minWidth'=>900,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="rencana" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================

?>