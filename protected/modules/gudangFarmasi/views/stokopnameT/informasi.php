<?php
$this->breadcrumbs=array(
	'Gfstokopname Ts'=>array('index'),
	'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>'Informasi Nilai Persediaan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GFStokopnameT', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' GFStokopnameT', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gfstokopname-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php //echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>


<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'gfstokopname-t-grid',
	'dataProvider'=>$model->searchInformasi(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tglstokopname',
                'jenisstokopname',
                'nostokopname',
                array(
                    'header'=>'Total Harga <br/> Total Netto',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->totalharga).\'<br/>\'."RP.".MyFunction::formatNumber($data->totalnetto)',
                ),
                'mengetahui.nama_pegawai',
                array(
                    'header'=>'Petugas I <br/> Petugas II',
                    'type'=>'raw',
                    'value'=>'$data->petugas1->nama_pegawai.\'<br/>\'.$data->petugas2->nama_pegawai',
                ),
                array(
                    'header'=>'Rincian Obat',
                    'type'=>'raw',
                    'value'=>'CHtml::link(\'<icon class="icon-list"></icon>\', Yii::app()->createUrl(\'gudangFarmasi/StokopnameT/details\', array(\'id\'=>$data->stokopname_id)), array("target"=>"frameDialog", "onclick"=>"$(\'#dialogDetails\').dialog(\'open\');"))'
                ),
//		'petugas1_id',
//		'petugas2_id',
		////'stokopname_id',
//		array(
//                        'name'=>'stokopname_id',
//                        'value'=>'$data->stokopname_id',
//                        'filter'=>false,
//                ),
//		'ruangan_id',
//		'formuliropname_id',
//		'isstokawal',
		/*
		
		'keterangan_opname',
		
		
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<div class="search-form">
<?php $this->renderPartial($this->pathView.'_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
//        $this->widget('TipsMasterData',array('type'=>'admin'));
//        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
//        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
//
//$js = <<< JSCRIPT
//function print(caraPrint)
//{
//    window.open("${urlPrint}/"+$('#gfstokopname-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
//}
//JSCRIPT;
//Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>

<?php
// ===========================Dialog Details Tarif=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogDetails',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Rincian Obat',
                        'autoOpen'=>false,
                        'width'=>1000,
                        'height'=>500,
                        'resizable'=>false,
                        'scroll'=>false,
                            'modal'=>true
                         ),
                    ));
?>
<iframe src="" name="frameDialog" width="100%" height="100%">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');