<?php 
echo CHtml::css('#isiScroll{max-height:500px;overflow-y:scroll;margin-bottom:10px;}#obatalkes-m-grid th{vertical-align:middle;}'); 
?>
<!-- search-form -->
<div id='isiScroll'>
<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
    'id'=>'obatalkes-m-grid',
    'dataProvider'=>$model->searchDataObat(),
    'mergeHeaders'=>array(
            array(
                'name'=>'<center>Stok</center>',
                'start'=>9, //indeks kolom 3
                'end'=>10, //indeks kolom 4
            ),
            
        ),
//    'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        ////'obatalkes_id',
            array(
                'header'=> 'Pilih',
                'type'=>'raw',
                'value'=>'
                    CHtml::activeHiddenField($data, \'[\'.$data->obatalkes_id.\']obatalkes_id\').
                    CHtml::checkBox(\'GFObatAlkesFarmasiV[\'.$data->obatalkes_id.\'][cekList]\', true, array(\'onclick\'=>\'setUrutan()\', \'class\'=>\'cekList\', \'onclick\'=>\'getTotal();\'));
                    ',
            ),
            array(
                'name'=>'jenisobatalkes_id',
                'value'=>'$data->jenisobatalkes_nama',
                ),
            'obatalkes_kode',
            array(
                'header'=>'Nama Obat <br/> Kekuatan',
                'type'=>'raw',
                'value'=>'$data->obatalkes_nama.\'<br/>\'.$data->kekuatan',
            ),
            array(
                'header'=>'Golongan<br/>Kategori',
                'type'=>'raw',
                'value'=>'$data->obatalkes_golongan.\'<br/>\'.$data->obatalkes_kategori',
            ),
//            'obatalkes_golongan',
            array(
                'name'=>'sumberdana_id',
                'value'=>'$data->sumberdana_nama',
            ),
            array(
                'header'=>false,
                'type'=>'raw',
                'headerHtmlOptions'=>array('style'=>'width:0%; display:none'),
                'htmlOptions'=>array('style'=>'width:0%; display:none'),
                'value'=>'CHtml::hiddenField(\'harga\', $data->hargajual, array(\'class\'=>\'span1 harga\', \'readonly\'=>false))'
            ),
            array(
                'header'=>'Harga Netto',
                'type'=>'raw',
                'value'=>'CHtml::textField(\'hargaNetto\', $data->harganetto, array(\'class\'=>\'span1 netto\', \'readonly\'=>true))'
            ),
             
//            array(
//                'header'=>'Harga Satuan <br/> harga Netto',
//                'type'=>'raw',
//                'value'=>'CHtml::textField(\'harga\', $data->hargajual, array(\'class\'=>\'span1 harga\', \'readonly\'=>true))
//                            .\'<br/>\'.
//                          CHtml::textField(\'hargaNetto\', $data->harganetto, array(\'class\'=>\'span1 netto\', \'readonly\'=>true))'
//            ),
            'minimalstok',
            array(
                'header'=>'Sistem',
                'type'=>'raw',
                'value'=> 'CHtml::textField(\'GFObatAlkesFarmasiV[\'.$data->obatalkes_id.\'][volume_sistem]\', StokobatalkesT::getStokBarang($data->obatalkes_id, '.Yii::app()->user->getState('ruangan_id').'), array(\'class\'=>\'stok span1\', \'readonly\'=>true))',
            ),
            array(
                'header'=>'<div class="test" style="cursor:pointer;" onclick="openDialogini()"> Fisik <icon class="icon-list"></icon></div> ',
                'type'=>'raw',
                'value'=> 'CHtml::textField(\'GFObatAlkesFarmasiV[\'.$data->obatalkes_id.\'][volume_fisik]\', 0  , array(\'class\'=>\'fisik span1 numbersOnly\', \'onblur\'=>\'getTotal();\'))',
            ),
            'tglkadaluarsa',
            array(
                'header'=>'Kondisi Barang',
                'type'=>'raw',
                'value'=> 'CHtml::dropDownList(\'GFObatAlkesFarmasiV[\'.$data->obatalkes_id.\'][kondisibarang]\', \'\', Inventarisasikeadaan::items(), array(\'class\'=>\'span2\'))',
            ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            $("#isiScroll .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null})
                getTotal();
                }',
)); ?> 
    </div>

<?php
// ===========================Dialog Details Tarif=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogDetails',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Volume Fisik',
                        'autoOpen'=>false,
                        'width'=>170,
                        'height'=>140,
                        'resizable'=>false,
                        'scroll'=>false,
                            'modal'=>true
                         ),
                    ));
?>
<div class="awawa" width="100%" height="100%">
    <?php echo CHtml::textField('fisiks', 0, array('class'=>'numbersOnly span2')); ?>
    <?php echo CHtml::button('submit', array('class'=>'btn btn-primary', 'onclick'=>'setVolume();', 'id'=>'submitJumlahVolume')); ?>
</div>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');

Yii::app()->clientScript->registerScript('openDialog','
    function openDialogini(){
        $("#dialogDetails").dialog("open");
    }
    
    function setVolume(){
        var value = $("#fisiks").val();
        $("#obatalkes-m-grid").find(".fisik").each(function(){
            $(this).val(value);
        });
        getTotal();
    }
',  CClientScript::POS_HEAD);