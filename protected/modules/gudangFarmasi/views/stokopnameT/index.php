
<?php
$this->breadcrumbs=array(
	'Gfstokopname Ts'=>array('index'),
	'Create',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Stok Obat Alkes Opname ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GFStokopnameT', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' GFStokopnameT', 'icon'=>'folder-open', 'url'=>array('Admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial($this->pathView.'_form', array('model'=>$model, 'modObat'=>$modObat, 'modDetails'=>$modDetails, 'modFormulir'=>$modFormulir)); ?>
<?php //$this->widget('TipsMasterData',array('type'=>'create'));?>