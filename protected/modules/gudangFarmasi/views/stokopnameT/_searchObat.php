<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'id' => 'obatalkes-m-search',
    'type' => 'horizontal',
        ));
?>
<table>
    <tr>
        <td width="50%">

            <?php echo $form->dropDownListRow($model, 'jenisobatalkes_id', CHtml::listData(JenisobatalkesM::model()->findAll('jenisobatalkes_aktif = true'), 'jenisobatalkes_id', 'jenisobatalkes_nama'), array('empty' => '-- Pilih --', 'class' => 'span3')); ?>

            <?php echo $form->textFieldRow($model, 'obatalkes_kode', array('class' => 'span3', 'maxlength' => 50)); ?>

            <?php echo $form->textFieldRow($model, 'obatalkes_nama', array('class' => 'span3', 'maxlength' => 200)); ?>
        </td>
        <td>
            <?php echo $form->dropDownListRow($model, 'obatalkes_golongan', obatalkes_golongan::items(), array('empty' => '-- Pilih --', 'class' => 'span3', 'maxlength' => 50)); ?>

            <?php echo $form->dropDownListRow($model, 'obatalkes_kategori', ObatAlkesKategori::items(), array('empty' => '-- Pilih --', 'class' => 'span3', 'maxlength' => 50)); ?>

            <?php //echo $form->textFieldRow($model,'obatalkes_kadarobat',array('class'=>'span3','maxlength'=>20)); ?>

            <?php //echo $form->textFieldRow($model,'tglkadaluarsa_awal',array('class'=>'span3')); ?>
            <div class="control-group ">
                
                <label class='control-label'>
                    <?php echo $form->checkBox($model, 'tglkadaluarsa').' ';
                        echo CHtml::encode($model->getAttributeLabel('tglkadaluarsa'));
                    ?>
                    
                    </label>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglkadaluarsa_awal',
                        'mode' => 'date',
                        'options' => array(
                            'dateFormat' => Params::DATE_TIME_FORMAT,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglkadaluarsa_akhir', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglkadaluarsa_akhir',
                        'mode' => 'date',
                        'options' => array(
                            'dateFormat' => Params::DATE_TIME_FORMAT,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
        </td>
    </tr>
</table>

<div class="form-actions">
<?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
</div>

<?php $this->endWidget(); ?>