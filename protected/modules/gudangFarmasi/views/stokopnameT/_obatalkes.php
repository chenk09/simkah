<?php 
Yii::app()->clientScript->registerScript('search', "
//$('.search-button').click(function(){
//    $('.search-form').slideToggle();
//    return false;
//});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('obatalkes-m-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php // echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<legend class="rim">Pencarian Obat Alkes</legend>
<div class="search-form"> <!--  style="display:none" -->
<?php $this->renderPartial($this->pathView.'_searchObat',array(
    'model'=>$model,
)); ?>
</div>

