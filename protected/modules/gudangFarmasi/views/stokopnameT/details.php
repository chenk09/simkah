<?php 

$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $modTable->searchDataObatPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $modTable->searchDataObat();
         $template = "{pager}{summary}\n{items}";
    }
?>
<table>
    <tr>
        <td>
             <b><?php echo CHtml::encode($model->getAttributeLabel('jenisstokopname')); ?>:</b>
            <?php echo CHtml::encode($model->jenisstokopname); ?>
            <br />
             <b><?php echo CHtml::encode($model->getAttributeLabel('nostokopname')); ?>:</b>
            <?php echo CHtml::encode($model->nostokopname); ?>
            <br />
             <b><?php echo CHtml::encode($model->getAttributeLabel('tglstokopname')); ?>:</b>
            <?php echo CHtml::encode($model->tglstokopname); ?>
            <br />
            <b><?php echo CHtml::encode($model->getAttributeLabel('mengetahui_id')); ?>:</b>
            <?php echo CHtml::encode(((!empty($model->mengetahui->nama_pegawai)) ? $model->mengetahui->nama_pegawai : " - " )); ?>
            <br />
        </td>
        <td>
            <b><?php echo CHtml::encode($model->getAttributeLabel('petugas1_id')); ?>:</b>
            <?php echo CHtml::encode(((!empty($model->petugas1->nama_pegawai)) ? $model->petugas1->nama_pegawai : " - " )); ?>
            <br />
            <b><?php echo CHtml::encode($model->getAttributeLabel('petugas2_id')); ?>:</b>
            <?php echo CHtml::encode(((!empty($model->petugas2->nama_pegawai)) ? $model->petugas2->nama_pegawai : " - " )); ?>
            <br />
             <b><?php echo CHtml::encode($model->getAttributeLabel('totalharga')); ?>:</b>
            <?php echo CHtml::encode($model->totalharga); ?>
            <br />
        </td>
    </tr>   
</table>
            

<?php 
echo CHtml::css('#obatalkes-m-grid th{vertical-align:middle;}'); 
?>
<!-- search-form -->

<?php $this->widget($table,array(
    'id'=>'obatalkes-m-grid',
    'dataProvider'=>$data,
    'mergeHeaders'=>array(
//            array(
//                'name'=>'<center>Stok</center>',
//                'start'=>8, //indeks kolom 3
//                'end'=>9, //indeks kolom 4
//            ),
            
        ),
//    'filter'=>$model,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        ////'obatalkes_id',
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                'type'=>'raw',
            ),
            array(
                'header'=>'ID<br>Obat Alkes',
                'value'=>'$data->obatalkes->obatalkes_id',
                'type'=>'raw',
            ),
            array(
                'header'=>'Nama<br>Obat Alkes',
                'value'=>'$data->obatalkes->obatalkes_nama',
                'type'=>'raw',
            ),
            array(
                'header'=>'Satuan<br>Kecil',
                'value'=>'$data->obatalkes->satuankecil->satuankecil_nama',
                'type'=>'raw',
            ),
            array(
                'header'=>'Satuan<br>Besar',
                'value'=>'$data->obatalkes->satuanbesar->satuanbesar_nama',
                'type'=>'raw',
            ),
            array(
                'header'=>'Jenis<br>Obat Alkes',
                'value'=>'$data->obatalkes->jenisobatalkes->jenisobatalkes_nama',
                'type'=>'raw',
            ),
            array(
                'header'=>'Kategori<br>Obat Alkes',
                'value'=>'$data->obatalkes->obatalkes_kategori',
                'type'=>'raw',
            ),
            array(
                'header'=>'Harga Netto<br>HNA + PPN',
                'value'=>'MyFunction::formatNumber($data->obatalkes->harganetto)',
                'type'=>'raw',
            ),
            array(
                'header'=>'Harga Jual<br>HNA + PPN + Margin(25%)',
                'value'=>'MyFunction::formatNumber($data->obatalkes->hargajual)',
                'type'=>'raw',
            ),
            array(
                'header'=>'Qty<br>(Satuan Kecil)',
                'value'=>'$data->volume_fisik',
                'type'=>'raw',
            ),
            array(
                'header'=>'Total I<br>(Qty X Harga Netto)',
                'value'=>'MyFunction::formatNumber($data->volume_fisik * $data->obatalkes->harganetto)',
                'type'=>'raw',
            ),
            array(
                'header'=>'Total II<br>(Qty X Harga Jual)',
                'value'=>'MyFunction::formatNumber($data->volume_fisik * $data->obatalkes->hargajual)',
                'type'=>'raw',
            ),
//            array(
//                'name'=>'obatalkes.jenisobatalkes_id',
//                'value'=>'$data->obatalkes->jenisobatalkes->jenisobatalkes_nama',
//                ),
//            'obatalkes.obatalkes_kode',
//            array(
//                'header'=>'Nama Obat <br/> Kekuatan',
//                'type'=>'raw',
//                'value'=>'$data->obatalkes->obatalkes_nama.\'<br/>\'.$data->obatalkes->kekuatan',
//            ),
//            array(
//                'header'=>'Golongan<br/>Kategori',
//                'type'=>'raw',
//                'value'=>'$data->obatalkes->obatalkes_golongan.\'<br/>\'.$data->obatalkes->obatalkes_kategori',
//            ),
//            'obatalkes_golongan',
//            array(
//                'name'=>'obatalkes.sumberdana_id',
//                'value'=>'$data->obatalkes->sumberdana->sumberdana_nama',
//            ),
            
//            array(
//                'header'=>'Harga Satuan <br/> harga Netto',
//                'type'=>'raw',
//                'value'=>'$data->obatalkes->hargajual
//                            .\'<br/>\'.
//                          $data->obatalkes->harganetto'
//            ),
//            'obatalkes.minimalstok',
//            array(
//                'header'=>'Sistem',
//                'type'=>'raw',
//                'value'=> '$data->volume_sistem',
//            ),
//            array(
//                'header'=>'Fisik',
//                'type'=>'raw',
//                'value'=> '$data->volume_fisik',
//            ),
//            'tglkadaluarsa',
//            array(
//                'header'=>'Kondisi Barang',
//                'type'=>'raw',
//                'value'=> '$data->kondisibarang',
//            ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            $(".numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null})
                getTotal();
                }',
)); ?> 
 <?php 
 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
//        $this->widget('TipsMasterData',array('type'=>'admin'));
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printNilaiPersediaan&id='.$model->stokopname_id);

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#obatalkes-m-grid').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>