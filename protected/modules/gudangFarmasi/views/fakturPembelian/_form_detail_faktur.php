<tr>
	<td>
		<?php echo CHtml::TextField('noUrut', $noUrut,array('class'=>'span1 noUrut','readonly'=>TRUE)); ?>
		<?php echo CHtml::activeHiddenField($modFakturDetail,'['.$i.']satuankecil_id',array()); ?>
		<?php echo CHtml::activeHiddenField($modFakturDetail,'['.$i.']obatalkes_id',array()); ?>
		<?php echo CHtml::activeHiddenField($modFakturDetail,'['.$i.']penerimaandetail_id',array()); ?>
		<?php echo CHtml::activeHiddenField($modFakturDetail,'['.$i.']satuanbesar_id',array()); ?>
		<?php echo CHtml::activeHiddenField($modFakturDetail,'['.$i.']sumberdana_id',array()); ?>
		<?php echo CHtml::activeHiddenField($modFakturDetail,'['.$i.']tglkadaluarsa',array()); ?>
		<?php echo CHtml::activeHiddenField($modFakturDetail,'['.$i.']jmlkemasan',array()); ?>
		<?php echo CHtml::activeHiddenField($modFakturDetail,'['.$i.']hargasatuan',array()); ?>
	</td>
	<td><?php echo $tampilDetail->sumberdana->sumberdana_nama;?></td>
	<td>
		<?php echo $tampilDetail->obatalkes_kategori;?> / <?php echo $tampilDetail->obatalkes_nama;?>
	</td>
	<td>
		<?php
			echo CHtml::activeTextField($modFakturDetail,'['.$i.']jmlpermintaan',array('readonly'=>TRUE,'class'=>'span1 angka permintaan'));
		?>
	</td>
	<td>
		<?php echo CHtml::activeTextField($modFakturDetail,'['.$i.']jmlterima',array('readonly'=>TRUE,'class'=>'span1 angka terima','onkeyup'=>'hitungJumlahDariDiterima(this);'));?>
		<?php echo CHtml::activeTextField($modFakturDetail,'['.$i.']jmlkemasan',array('readonly'=>TRUE,'class'=>'span1 angka jml_kemasan','onkeyup'=>'hitungJumlahDariDiterima(this);'));?>
	</td>
	<td>
		<?php echo CHtml::activeTextField($modFakturDetail,'['.$i.']persendiscount',array('maxlength'=>5,'class'=>'span1 float persenDiskon','onkeyup'=>'hitungJumlahDariPersentaseDiskon(this);'))?>
		<?php echo CHtml::hiddenField('diskonLama',$modFakturDetail->persendiscount,array('maxlength'=>3,'class'=>'span1 float diskonLama'))?>
	</td>
	<td>
		<?php echo CHtml::activeTextField($modFakturDetail,'['.$i.']jmldiscount',array('style'=>'text-align:right;','class'=>'span2 angka currency jmlDiskon','readonly'=>true))?>
	</td> 
	<td>
		<?php echo CHtml::activeTextField($modFakturDetail,'['.$i.']harganettofaktur',array('style'=>'text-align:right;','readonly'=>FALSE,'class'=>'span2 angka currency netto','onkeyup'=>'hitungSemua();'))?>
		<?php
			echo CHtml::hiddenField(
				'harga_bruto',
				ceil($harga_bruto),
				array(
					'class'=>'span2 currency harga_bruto'
				)
			);
		?>
	</td>
	<td>
		<?php echo CHtml::activeTextField($modFakturDetail,'['.$i.']hargappnfaktur',array('style'=>'text-align:right;','readonly'=>FALSE,'class'=>'span2 angka currency ppn','onkeyup'=>'hitungSemua();'));?>
	</td>
	<td>
		<?php echo CHtml::activeTextField($modFakturDetail,'['.$i.']hargapphfaktur',array('style'=>'text-align:right;','readonly'=>FALSE,'class'=>'span2 angka currency pph','onkeyup'=>'hitungSemua();'));?>
	</td>
	<td>
		<?php echo CHtml::textField('subTotal', ceil($harga_bruto),array('style'=>'text-align:right;','readonly'=>true,'class'=>'span2 angka currency subTotal'));?>
	</td>
	<!--
	<td>
		<?php echo CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'javascript:void(0);','onclick'=>'remove_row(this);','style'=>'text-decoration:none;'));?>
	</td>-->
</tr>