<?php
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gffakturpembelian-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onsubmit'=>'return cekInputan();'),
        'focus'=>'#',
)); 
$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php 
$disabled = (!empty($modFakturPembelian->penerimaanbarang_id)) ? true : ''; 
$dialog = (!empty($modFakturPembelian->penerimaanbarang_id)) ? array() : array('idDialog'=>'dialogPenerimaanBarang'); 
if(!empty($_GET['idFaktur'])){
    $jmlUang = $jmlUang;
}else{
    $jmlUang = 0;
}
?>
<legend class="rim2">Transaksi Faktur Pembelian</legend>
    <?php if ($modFakturPembelian->isNewRecord) { ?>
    <fieldset>
       <legend class="rim">Berdasarkan Penerimaan Items</legend>
           <table>
               <tr>
                   <td>
                       <div class="control-group ">
                           <?php echo $form->labelEx($modPenerimaanItems,'noterima', array('class'=>'control-label')); ?>
                           <div class="controls">
                            <?php echo CHtml::hiddenField('idPenerimaanBarang','',array('readonly'=>TRUE));?>
                            <?php $this->widget('MyJuiAutoComplete',array(
                                        'model'=>$modPenerimaanItems,
                                        'attribute'=>'noterima',
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/noPenerimaanBarang'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'select'=>'js:function( event, ui ) {
                                                      $("#'.CHtml::activeId($modPenerimaanItems,'penerimaanbarang_id').'").val(ui.item.penerimaanbarang_id);
                                                      $("#'.CHtml::activeId($modPenerimaanItems,'tglterima').'").val(ui.item.tglterima);   
                                                      $("#'.CHtml::activeId($modPenerimaanItems,'supplier_id').'").val(ui.item.supplier_id);     
                                                      $("#idPenerimaanBarang").val(ui.item.penerimaanbarang_id);     
                                                      submitPermintaanPembelian();
                                            }',
                                        ),
                                        'htmlOptions'=>array(
                                            'disabled'=>$disabled,
                                            'onkeypress'=>"$(this).focusNextInputField(event)",'class'=>'span2','readonly'=>FALSE), 
                                        'tombolDialog'=>$dialog,
                            )); ?>

                            <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                              //  array('onclick'=>'$("#dialogPenerimaanBarang").dialog("open");return false;',
                                    //  'class'=>'btn btn-primary',
                                    //  'onkeypress'=>"return $(this).focusNextInputField(event)",
                                      //'rel'=>"tooltip",
                                     // 'title'=>"Klik Untuk Penerimaan barang Lebih Lanjut",
                                  //    'id'=>'buttonpenerimaanBarang')); ?>
                           </div>
					</div>    
					<?php echo $form->textFieldRow($modPenerimaanItems,'tglterima', array('class'=>'span3', 'disabled'=>$disabled, 'readonly'=>true)) ?>
					<?php echo $form->dropDownListRow($modPenerimaanItems,'supplier_id',
						CHtml::listData($modPenerimaanItems->SupplierItems, 'supplier_id', 'supplier_nama'),
						array(
							'readonly'=>'readonly',
							'class'=>'span3 isRequired',
							'onkeypress'=>"return $(this).focusNextInputField(event)",
							'empty'=>'-- Pilih --'
						)
					);?>
                   </td>
               </tr>
           </table>  
     </fieldset>
    <?php } ?>
<table>
    <tr>
       <td>
            <fieldset>
                <legend class="rim">Data Faktur</legend>
					<?php echo $form->hiddenField($modFakturPembelian,'supplier_id',array());?>					
                    <?php echo $form->textFieldRow($modFakturPembelian,'nofaktur', array('class'=>'span3 isRequiredFaktur')) ?>
                         <div class="control-group ">
                                <?php echo $form->labelEx($modFakturPembelian,'tglfaktur', array('class'=>'control-label')) ?>
                                    <div class="controls">
                                        <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modFakturPembelian,
                                                    'attribute'=>'tglfaktur',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,

                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                                        )); ?>
                                    </div>
                         </div>
                        <div class="control-group ">
                                    <?php echo $form->labelEx($modFakturPembelian,'tgljatuhtempo', array('class'=>'control-label')) ?>
                                        <div class="controls">
                                            <?php $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modFakturPembelian,
                                                        'attribute'=>'tgljatuhtempo',
                                                        'mode'=>'date',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,

                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                            )); ?>
                                        </div>
                             </div>
                    <?php echo $form->textFieldRow($modFakturPembelian,'biayamaterai', array('class'=>'span3 currency')) ?>
                    <?php echo $form->textAreaRow($modFakturPembelian,'keteranganfaktur', array('class'=>'span3')) ?>
                    <?php echo $form->dropDownListRow($modFakturPembelian,'syaratbayar_id',
						CHtml::listData($modFakturPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
						array(
							'class'=>'span3',
							'onkeypress'=>"return $(this).focusNextInputField(event)",
							'empty'=>'-- Pilih --'
						)
					); ?>
       </td>
       <td>
            <fieldset id="informasi_harga">
                <legend class="rim">Informasi Harga</legend>
                <?php echo $form->textFieldRow($modFakturPembelian,'totharganetto', array('class'=>'span2 isRequired currency','readonly'=>TRUE)) ?>
                <div class="control-group ">
                    <label class='control-label'>
                        <?php echo CHtml::checkbox('totalPPN',false,array('onclick'=>'hitung_total_ppn(this)','style'=>'width : 10px','class'=>'currency'))?>
                        Ppn (Total)
                        </label>
                    <div class="controls">
                        <?php echo $form->textField($modFakturPembelian,'totalpajakppn', array('onkeyup'=>'hitungSemua()','class'=>'span2 currency isRequired','readonly'=>TRUE)) ?>
                    </div>
                 </div>
                <div class="control-group ">
                    <label class='control-label'>
                        <?php echo CHtml::checkBox('termasukPPH',false,array('onclick'=>'hitung_pph(this)','style'=>'width : 10px','class'=>'currency'))?>
                        Pph (Total)
                        </label>
                    <div class="controls">
                        <?php echo $form->textField($modFakturPembelian,'totalpajakpph', array('class'=>'span2 currency isRequired','readonly'=>TRUE)) ?>
                    </div>
                 </div>
                 
                <div class="control-group ">
                    <label class='control-label'>
                        <?php echo CHtml::checkbox('diskonSemuaRp',false,array('onclick'=>'diskonFakturRp(this)','style'=>'width : 10px','class'=>'currency'))?>
                        Diskon Rp / Faktur
                        </label>
                    <div class="controls">
                        <?php echo $form->textField($modFakturPembelian,'jmldiscount', array('onkeyup'=>'hitungSemua();','class'=>'span2 isRequired currency','readonly'=>TRUE)) ?>
                    </div>
                 </div>
                <div class="control-group ">
                    <label class='control-label'>
                        <?php echo CHtml::checkbox('diskonSemua',false,array('onclick'=>'diskonFakturPersen(this)','style'=>'width : 10px','class'=>'currency'))?>
                        Persen Diskon/ Faktur
                        </label>
                    <div class="controls">
                        <?php echo $form->textField($modFakturPembelian,'persendiscount', array('onkeyup'=>'gantiDiskonFakturPersen(this);','class'=>'span2 isRequired  numbersOnly','readonly'=>TRUE)) ?>
                    </div>
                 </div>
                 
                 <?php echo $form->textFieldRow($modFakturPembelian,'totalhargabruto', array('class'=>'span2 isRequired currency','readonly'=>TRUE)) ?>
                 <?php echo CHtml::hiddenField('cadangan')?>
            </fieldset>
        </td>
    </tr>
    <tr>
        <td>
            <fieldset>
                <legend class="rim">Notifikasi</legend> 

                Notifikasi
                <?php echo $form->textField($modFakturPembelian,'notifikasi', array('maxlength'=>'2','class'=>'numberOnly','style'=>'width:20px;',)) ?> 
                hari dari tanggal jatuh tempo
                
            </fieldset>
        </td>
    </tr>    
</table>

                    
<table id="tableFaktur" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Jumlah <br/> Permintaan</th>
        <th>Jumlah <br/>Diterima</th>
        <th>Diskon (%)</th>
        <th>Diskon (Rp)</th>
        <th>Harga Netto</th>
        <th>
			PPN (Rp)
			<?php echo CHtml::checkbox('termasukPPN',false,array('onclick'=>'hitung_ppn(this)','style'=>'width : 10px','class'=>'currency'))?>
		</th>
        <th>PPH (Rp)</th>
        <th>SubTotal</th>
        <?php if (!isset($modFakturDetail)) { ?>
        <th>Batal</th>     
        <?php } ?>
    </tr>
    </thead>
    <tbody>
        <?php if (isset($modFakturDetail)) {
            if (count($modFakturDetail) > 0){
                $tr = '';
                $no = 1;
                foreach ($modFakturDetail as $key => $value) {
                    $subTotal = (($value->harganettofaktur+$value->hargappnfaktur + $value->hargapphfaktur)*$value->jmlterima) - $value->jmldiscount;
                    $subTotalCadangan += (($value->harganettofaktur+$value->hargappnfaktur + $value->hargapphfaktur)*$value->jmlterima);
                    $hitungSubTotalCadangan +=(($value->harganettofaktur+$value->hargappnfaktur + $value->hargapphfaktur)*$value->jmlterima) - $value->jmldiscount;
                    $tampilDetail = ObatalkesM::model()->findByPk($value->obatalkes_id);
                    $tr .="<tr>
                        <td>".CHtml::TextField('noUrut',$no++,array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($value,'['.$key.']satuankecil_id').
                              CHtml::activeHiddenField($value,'['.$key.']obatalkes_id', array('class'=>'obatAlkes')). 
                              CHtml::activeHiddenField($value,'['.$key.']penerimaandetail_id'). 
                              CHtml::activeHiddenField($value,'['.$key.']satuanbesar_id').
                              CHtml::activeHiddenField($value,'['.$key.']sumberdana_id'). 
                              CHtml::activeHiddenField($value,'['.$key.']tglkadaluarsa').
                              CHtml::activeHiddenField($value,'['.$key.']jmlkemasan').   
                              CHtml::activeHiddenField($value,'['.$key.']persendiscount', array('class'=>'diskoncadangan')).   
                              
                       "</td>
                        <td>".$tampilDetail->sumberdana->sumberdana_nama."</td>
                        <td>".$tampilDetail->obatalkes_kategori."/<br/>".$tampilDetail->obatalkes_nama."</td>
                        <td>".CHtml::activeTextField($value,'['.$key.']jmlpermintaan',array('class'=>'span1 permintaan'))."</td>
                        <td>".CHtml::activeTextField($value,'['.$key.']jmlterima',array('readonly'=>TRUE,'class'=>'span1 terima numbersOnly','onkeyup'=>'hitungJumlahDariDiterima(this);'))."</td>
                        <td>".CHtml::activeTextField($value,'['.$key.']persendiscount',array('maxlength'=>3,'class'=>'span1 persenDiskon numbersOnly','onkeyup'=>'hitungJumlahDariPersentaseDiskon(this);'))
                             .CHtml::hiddenField('diskonLama','',array('maxlength'=>3,'class'=>'span1 numbersOnly'))
                            ."</td>
                        <td>".CHtml::activeTextField($value,'['.$key.']jmldiscount',array('class'=>'span2 currency jmlDiskon','readonly'=>true))."</td>
                        <td>".CHtml::activeTextField($value,'['.$key.']harganettofaktur',array('readonly'=>FALSE,'class'=>'span2 netto currency', 'onkeyup'=>'ubahNetto(this);'))."</td>
                        <td>".CHtml::activeTextField($value,'['.$key.']hargappnfaktur',array('readonly'=>FALSE,'class'=>'span2 ppn currency'))."</td>
                        <td>".CHtml::activeTextField($value,'['.$key.']hargapphfaktur',array('readonly'=>FALSE,'class'=>'span2 pph currency'))."</td>
                        <td>".CHtml::textField('subTotal',$subTotal,array('readonly'=>true,'class'=>'span2 subTotal currency'))."</td>
                    </tr>
                    ";
                }
                echo $tr;
                echo CHtml::hiddenField('subTotalCadangan', $subTotalCadangan);
                echo CHtml::hiddenField('hitungSubTotalCadangan', $hitungSubTotalCadangan);
            }
//            ,'onkeyup'=>'hitungJumlahDariDiskon(this);'
        }?>
    </tbody>
    <?php
    echo "<tfoot>
        <tr>
             <td colspan='6'>Total</td>
             <td>".
                  CHtml::textField('totdiskon','', array( 'style'=>'text-align:right;','readonly'=>TRUE,'class'=>'span2 currency')).
             "</td>
             <td>".
                  //CHtml::textField('totharganetto','', array('style'=>'text-align:right;','readonly'=>TRUE,'class'=>'span2 currency')) .
			"</td>
             <td></td>
             <td></td>
             <td>".
                  CHtml::textField('tothargabruto','', array('style'=>'text-align:right;','readonly'=>TRUE,'class'=>'span2 currency')).
             "</td>
         </tr>
        <tr>
             <td colspan='10'>Uang Muka</td>
             <td>".
                      CHtml::textField('uangmuka',$jmlUang, array( 'style'=>'text-align:right','readonly'=>TRUE,'class'=>'uangmuka span2 currency')).
             "</td>
         </tr>
        <tr>
             <td colspan='10'>Sisa Bayar</td>
             <td>".
                  CHtml::textField('sisabayar','', array( 'style'=>'text-align:right','readonly'=>TRUE,'class'=>'sisabayar span2 currency')).
             "</td>
         </tr>
         </tfoot>
    ";
    ?>
<!--    <tfoot>
        <tr>
            <th colspan="5">Total</th>
            <th><?php //echo $form->textField($modFakturPembelian,'persendiscount', array('class'=>'span1 isRequired numbersOnly','maxlength'=>2,'onkeyup'=>'hitungJumlahHargaDiskon(this);')) ?></th>
            <th><?php //echo $form->textField($modFakturPembelian,'jmldiscount', array('class'=>'span1 isRequired','readonly'=>TRUE)) ?></th>
            <th><?php //echo $form->textField($modFakturPembelian,'totharganetto', array('class'=>'span1 isRequired','readonly'=>TRUE)) ?></th>
            <th><?php //echo $form->textField($modFakturPembelian,'totalpajakppn', array('class'=>'span1 isRequired','readonly'=>TRUE)) ?></th>
            <th><?php //echo $form->textField($modFakturPembelian,'totalpajakpph', array('class'=>'span1 isRequired','readonly'=>TRUE)) ?></th>     
            <th><?php //echo CHtml::textField('totalSubTotal',$modFakturPembelian->totalhargabruto, array('class'=>'span1 isRequired','readonly'=>TRUE)) ?></th>
            <?php //if (!isset($modFakturDetail)) { ?>
            <th></th>
            <?php //} ?>
        </tr>
    </tfoot>-->
</table>
<?php
//FORM REKENING
    $this->renderPartial('gudangFarmasi.views.fakturPembelian.rekening._formRekening',
        array(
            'form'=>$form,
            'modRekenings'=>$modRekenings,
        )
    );
?>
<div class="form-actions">
<?php if (!$modFakturPembelian->isNewRecord){
$urlPrint=  Yii::app()->createAbsoluteUrl('gudangFarmasi/penerimaanItems/printFaktur', array('idFaktur'=>$modFakturPembelian->fakturpembelian_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}else{
    echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'cekValidasi()'));
    echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan', 'style'=>'display:none;'));
}
?>
       <?php
             echo "&nbsp;".CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-danger', 'type'=>'button'));
        ?>
    <?php
$content = $this->renderPartial('gudangFarmasi.views.tips.transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>            
<?php $this->endWidget(); ?>
<?php 
//========= Dialog buat Permintaan Kebutuhan obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPenerimaanBarang',
    'options'=>array(
        'title'=>'Pencarian Penerimaan Items',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$format = new CustomFormat();
if (isset($_GET['GFPenerimaanBarangT'])){
    $modPenerimaanItems->attributes = $_GET['GFPenerimaanBarangT'];
    $modPenerimaanItems->supplier_nama = $_GET['GFPenerimaanBarangT']['supplier_nama'];
    $modPenerimaanItems->tglterima = $format->formatDateMediumForDB($_GET['GFPenerimaanBarangT']['tglterima']);
}

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'permintaan-m-grid',
	'dataProvider'=>$modPenerimaanItems->searchFakturPembelian(),
	'filter'=>$modPenerimaanItems,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'columns'=>array(
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
					"id" => "selectPasien",
					"onClick" => "$(\"#'.CHtml::activeId($modPenerimaanItems,'noterima').'\").val(\"$data->noterima\");
								  $(\"#'.CHtml::activeId($modPenerimaanItems,'supplier_id').'\").val(\"$data->supplier_id\");
								  $(\"#'.CHtml::activeId($modPenerimaanItems,'tglterima').'\").val(\"$data->tglterima\");    
								  $(\"#idPenerimaanBarang\").val(\"$data->penerimaanbarang_id\");
								  submitPermintaanPembelian();
								  $(\"#dialogPenerimaanBarang\").dialog(\"close\");    
					")
				)',
			),
			'noterima',
			array(
				'name' => 'tglterima',
				'filter' =>$this->widget('MyDateTimePicker',array(
					'model'=>$modPenerimaanItems,
					'attribute'=>'tglterima',
					'mode'=>'date',
					'options'=> array(
						'dateFormat'=>Params::DATE_TIME_FORMAT,
					),
					'htmlOptions'=>array(
						'readonly'=>true,
						'class'=>'span2 dtPicker1',
						'id'=>'testing'
					),
				),true), 
			),
            array(
                'header'=>'Nama Supplier',
                'type'=>'raw',
                'name'=>'supplier_nama',
                'value'=>'$data->supplier->supplier_nama',
            )
		),
        'afterAjaxUpdate'=>'function(id, data){
            $("#testing").datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional["id"], {"dateFormat":"dd M yy","timeText":"Waktu","hourText":"Jam","minuteText":"Menit","secondText":"Detik","showSecond":true,"timeOnlyTitle":"Pilih Waktu","timeFormat":"hh:mm:ss","changeYear":true,"changeMonth":true,"showAnim":"fold","yearRange":"-80y:+20y"}));
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
	)
);
$this->endWidget();

//========= end Permintaan dialog =============================
?>

<?php
$urlGetPenerimaanBarang =  Yii::app()->createUrl('actionAjax/getPenerimaanItems');
$idSupplier = CHtml::activeId($modFakturPembelian,'supplier_id');
$konfigFarmasi = KonfigfarmasiK::model()->find();
$persenPPN=  $konfigFarmasi->persenppn;
$persenPPH=  $konfigFarmasi->persenpph;
$idPersenDiskon=CHtml::activeId($modFakturPembelian, 'persendiscount');
$idTotalHargaNetto=CHtml::activeId($modFakturPembelian, 'totharganetto');
$idTotalPajakPPN=CHtml::activeId($modFakturPembelian,'totalpajakppn');
$idTotalPajakPPH=CHtml::activeId($modFakturPembelian, 'totalpajakpph');
$idJumlahDiskon=CHtml::activeId($modFakturPembelian, 'jmldiscount');
$idSyaratBayar=CHtml::activeId($modFakturPembelian, 'syaratbayar_id');
$idTotalHargaBruto=CHtml::activeId($modFakturPembelian, 'totalhargabruto');
$jscript = <<< JS

function submitPermintaanPembelian()
{
    idPenerimaanBarang = $('#idPenerimaanBarang').val();
	if(idPenerimaanBarang == '')
	{
		alert('Silahkan Pilih penerimaan Terlebih Dahulu');
	}else{
		$("#tableFaktur tbody tr").remove();
		$.post("${urlGetPenerimaanBarang}", { idPenerimaanBarang: idPenerimaanBarang },
		function(data)
		{
			$('#informasi_harga').find('input').val(0);
			
			$('#tableFaktur').append(data.tr);
			$('#${idSupplier}').val(data.supplier_id);
			$('#uangmuka').val(formatUang(data.uangMuka));
			//$("#${idSupplier}").attr("disabled","disabled");
			
			if(data.isPPN == '1'){ //Jika termasuk PPN
				$('#termasukPPN').attr('checked','checked');
			}

			if(data.isPPH == '1'){ //Jika termasuk PPH
				$('#termasukPPH').attr('checked','checked');
			}
			
			var idObat = $("#tableFaktur tbody").parents().find('input[name$="[obatalkes_id]"]').val();
			var qty = $("#tableFaktur tbody").parents().find('input[name$="[jmlkemasan]"]').val();
			
			var supplier_id = $('#GFFakturPembelianT_supplier_id').val();
			var hargaSatuan = unformatNumber($("#tableFaktur tbody").parents().find('input[name$="[harganettofaktur]"]').val());                               
			var diskon = unformatNumber($('#tableFaktur tbody').parents().find('input[name$="[persendiscount]"]').val());
			var total = unformatNumber($('#GFFakturPembelianT_totalhargabruto').val());
			hitungSemua();
			
			var supplier_id = $('#GFFakturPembelianT_supplier_id').val();
			getDataRekeningFaktur(supplier_id, total);
			
			setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya
				hitungSemua();
			},1500);
			
		}, "json");
	}
}

function hitungSemua()
{
	$(".float").unmaskMoney();
	var total_netto = 0;
	var total_harga_bruto = 0;
	var total_ppn = 0;
	var total_pph = 0;
	var total_harga_discount = 0;
	var idx = 1;
	
	$('.terima').each(function(){
		/** perhitungan harga **/
		var harga_ppn = unformatNumber($(this).parents("tr").find('.ppn').val())
		total_ppn += harga_ppn;
		
		var harga_pph = unformatNumber($(this).parents("tr").find('.pph').val())
		total_pph += harga_pph;
		
		var harga_netto = unformatNumber($(this).parents("tr").find('.netto').val());
		var jum_terima = unformatNumber($(this).parents("tr").find('.terima').val());
		var jml_kemasan = unformatNumber($(this).parents("tr").find('.jml_kemasan').val());
		total_netto += (harga_netto * jml_kemasan) * jum_terima;
		
		var harga_discount = unformatNumber($(this).parents("tr").find('.jmlDiskon').val());
		total_harga_discount += harga_discount;
		
		var sub_total = (((harga_netto * jml_kemasan) * jum_terima) - harga_discount) + harga_ppn + harga_pph;
		var harga_bruto = unformatNumber($(this).parents("tr").find('.harga_bruto').val());
		total_harga_bruto += sub_total;
		
		/** set value **/
		//sub_total = Math.round(((harga_netto * jum_terima) - harga_discount) + harga_ppn);
		sub_total = Math.round(sub_total);
		$(this).parents("tr").find('.subTotal').val(sub_total);
		$(this).parents("tr").find('#noUrut').val(idx);
		
		idx++;
	});
	
	if(!$('#totalPPN').is(':checked')){
		$('#GFFakturPembelianT_totalpajakppn').val(total_ppn);
	}else{
		var totalpajakppn = unformatNumber($('#GFFakturPembelianT_totalpajakppn').val());
		total_harga_bruto += totalpajakppn;
	}

	/** ambil jumlah rupiah discount **/
	var jumlah_discount = $('#GFFakturPembelianT_jmldiscount').val();
	total_harga_bruto = Math.round(total_harga_bruto) - unformatNumber(jumlah_discount);
	
	$('#GFFakturPembelianT_totharganetto').val(total_netto);
	$('#GFFakturPembelianT_totalpajakpph').val(total_pph);
	$('#tothargabruto').val(total_harga_bruto);
	$('#GFFakturPembelianT_totalhargabruto').val(total_harga_bruto);
	$('#totdiskon').val(formatUang(total_harga_discount));
	$('#totharganetto').val(formatUang(total_netto));
	
	var uangmuka = $('#uangmuka').val();
	uangmuka = unformatNumber(uangmuka)
	if(total_harga_bruto > 0)
	{
		$('#sisabayar').val(total_harga_bruto - uangmuka);
	}else $('#sisabayar').val(0);
	
	var supplier_id = $('#GFFakturPembelianT_supplier_id').val();
	setTimeout(function(){
		updateRekeningFaktur(supplier_id, formatDesimal(total_harga_bruto));
	},1500);	
	
	$('.currency').each(function(index){
		var current_val = accounting.formatNumber($(this).val());
		$(this).val(current_val);
	});
	
	$(".float").maskMoney('mask');
}

function hitung_total_ppn(obj)
{
	if($('#termasukPPN').is(':checked'))
	{
		$('#termasukPPN').prop('checked',false);
		$('#termasukPPN').trigger('click');
	}
	
	if(obj.checked == true)
	{
        
		var total_harga_ppn = 0;
		$('.ppn').each(function()
		{
			var harga_netto = unformatNumber($(this).parents("tr").find('.netto').val());
			var harga_discount = unformatNumber($(this).parents("tr").find('.jmlDiskon').val());
			var jum_terima = unformatNumber($(this).parents("tr").find('.terima').val());
			var jml_kemasan = unformatNumber($(this).parents("tr").find('.jml_kemasan').val());
			total_harga_ppn += ((harga_netto * jml_kemasan) * jum_terima) - harga_discount;
        });
		total_harga_ppn = Math.round(total_harga_ppn);
		var jmldiscount = unformatNumber($('#GFFakturPembelianT_jmldiscount').val());
		var harga_total_ppn = (total_harga_ppn - jmldiscount) * (parseFloat(${persenPPN})/100);
		$('#GFFakturPembelianT_totalpajakppn').val(harga_total_ppn);
		
	}else{
		$('#GFFakturPembelianT_totalpajakppn').attr('readonly', true);
		$('#GFFakturPembelianT_totalpajakppn').val(0);
	}
	hitungSemua();
}

function hitung_ppn(obj)
{	
    if(obj.checked == true){ //Jika tidak termasuk PPN
		$('#totalPPN').prop('checked',false);
		
        $('.ppn').each(function()
		{
			var harga_netto = unformatNumber($(this).parents("tr").find('.netto').val());
			var harga_discount = unformatNumber($(this).parents("tr").find('.jmlDiskon').val());
			var jum_terima = unformatNumber($(this).parents("tr").find('.terima').val());
			var jml_kemasan = unformatNumber($(this).parents("tr").find('.jml_kemasan').val());
			
			var total = (((harga_netto * jml_kemasan) * jum_terima) - harga_discount);
			var harga_ppn = total * (parseFloat(${persenPPN})/100);
			
			harga_ppn = Math.round(harga_ppn);
			$(this).val(harga_ppn);
        });  
    }else{//Jika Termasuk PPN
		$('.ppn').each(function() {
			$(this).val(0);
		});
		
		$('#termasukPPH').removeAttr('checked'); 
		$('.pph').each(function(){
			$(this).val(0);
			$(this).attr('readonly','TRUE');
		}); 
		$('#totalPPH').val(0);     
    }
	hitungSemua();
}

function hitung_pph(obj)
{
	
    if(obj.checked == true)
	{
        $('.pph').each(function()
		{
			
			var harga_netto = unformatNumber($(this).parents("tr").find('.netto').val());
			var harga_discount = unformatNumber($(this).parents("tr").find('.jmlDiskon').val());
			var jum_terima = unformatNumber($(this).parents("tr").find('.terima').val());
			var jml_kemasan = unformatNumber($(this).parents("tr").find('.jml_kemasan').val());
			
			var total = (((harga_netto * jml_kemasan) * jum_terima) - harga_discount);
			var harga_pph = total * (parseFloat(${persenPPH})/100);
			
			harga_pph = Math.round(harga_pph);
			$(this).val(harga_pph);
			
        });  
    }else{
		$('.pph').each(function() {
			$(this).val(0);
		});
    }
	hitungSemua();
}

function hitungJumlahDariPersentaseDiskon(obj)
{
    var harga_netto = unformatNumber($(obj).parents("tr").find('.netto').val());
	var diskon_lama = unformatNumber($(obj).parents("tr").find('.diskonLama').val());
	var jum_terima = unformatNumber($(obj).parents("tr").find('.terima').val());
	var jml_kemasan = unformatNumber($(obj).parents("tr").find('.jml_kemasan').val());
	
	var discount = unformatNumber(obj.value);
	
    if(discount > 100)
	{
        alert('Maaf Diskon yang dimasukan tidak boleh lebih dari 100 %');
		$(obj).val(diskon_lama);
    }else{
		var total_disc = (((harga_netto * jml_kemasan) * jum_terima) * discount) / 100;
		$(obj).parents("tr").find('.jmlDiskon').val(total_disc);
	}
    hitungSemua();
}

function diskonFakturRp()
{
	if($('#diskonSemuaRp').is(':checked'))
	{
		$('#GFFakturPembelianT_jmldiscount').removeAttr('readonly','false');
		$('#diskonSemua').attr('disabled','TRUE');
		$('#GFFakturPembelianT_persendiscount').val(0);
	}else{
		$('#GFFakturPembelianT_jmldiscount').attr('readonly','TRUE');
		$('#diskonSemua').removeAttr('disabled');
		$('#GFFakturPembelianT_jmldiscount').val(0);
		$('#GFFakturPembelianT_persendiscount').val(0);
	}
	hitungSemua();
}

function diskonFakturPersen(){
	if($('#diskonSemua').is(':checked')){
		$('#GFFakturPembelianT_persendiscount').removeAttr('readonly','false');
		$('#diskonSemuaRp').attr('disabled','TRUE');
		$('#GFFakturPembelianT_jmldiscount').val(0);
	}else{
		$('#GFFakturPembelianT_persendiscount').attr('readonly','TRUE');
		$('#diskonSemuaRp').removeAttr('disabled');
		$('#GFFakturPembelianT_persendiscount').val(0);
		$('#GFFakturPembelianT_jmldiscount').val(0);
		hitungSemua();
	}
}

function gantiDiskonFakturPersen(obj)
{
    if($('#diskonSemua').is(':checked'))
	{
		var diskonPersen = unformatNumber(obj.value);
		var total_harga = 0;
        $('.pph').each(function()
		{
			
			var harga_netto = unformatNumber($(this).parents("tr").find('.netto').val());
			var harga_discount = unformatNumber($(this).parents("tr").find('.jmlDiskon').val());
			var jum_terima = unformatNumber($(this).parents("tr").find('.terima').val());
			var total = ((harga_netto * jum_terima) - harga_discount);
			
			var harga_ppn = unformatNumber($(this).parents("tr").find('.ppn').val());
			harga_ppn = Math.round(harga_ppn);
			
			var harga_pph = unformatNumber($(this).parents("tr").find('.pph').val());
			harga_pph = Math.round(harga_pph);
			
			total_harga += harga_pph + harga_ppn + total;
			
        });
		var discount = Math.round(total_harga * (diskonPersen/100));
		$('#GFFakturPembelianT_jmldiscount').val(discount);
		
	}else{
       $('#GFFakturPembelianT_persendiscount').val(0);
	   $('#GFFakturPembelianT_jmldiscount').val(0);
	}
	hitungSemua();
}

function remove_row(obj)
{
    $(obj).parents('tr').remove();
    
	var idObat = $("#tableFaktur tbody").parents().find('input[name$="[obatalkes_id]"]').val();
    removeRekeningObat(idObat);
	
    hitungSemua();
	return false;
}

function cekValidasi()
{ 
	hargaTotalNetto = unformatNumber($('#GFFakturPembelianT_totharganetto').val());
	if ($('.isRequired').val()==''){
		alert ('Harap Isi Semua Data Yang Bertanda *');
	}else if(hargaTotalNetto<1){
		alert('Anda Belum memimlih Obat Yang Akan Diminta');   
	}else{
		$('.currency').each(function(index){
			var current_val = accounting.unformat($(this).val());
			$(this).val(current_val);
		});
		$('.float').unmaskMoney();
		$('#btn_simpan').click();
	}
}

JS;
Yii::app()->clientScript->registerScript('faktur',$jscript, CClientScript::POS_HEAD);
?>
       
<?php

$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.float',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'allowDecimal'=>true,
        'decimal' => '.',
        'thousands' => '',
        'precision' => 4,
    )
));

?>