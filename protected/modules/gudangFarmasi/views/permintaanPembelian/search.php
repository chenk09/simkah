<?php
Yii::app()->clientScript->registerScript('search', "
$('#divSearch-form form').submit(function(){
	$.fn.yiiGridView.update('penawaran-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
$this->widget('bootstrap.widgets.BootAlert');
?>

<legend class="rim2">Informasi Pemesanan Items</legend>
<div id="divSearch-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'rencana-t-search',
        'type'=>'horizontal',
)); ?>
    <?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'penawaran-m-grid',
	'dataProvider'=>$modPermintaanPembelian->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tglpermintaanpembelian',
                'nopermintaan',
                array(
                   'name'=>'supplier_id',
                    'type'=>'raw',
                    'value'=>'$data->supplier->supplier_nama',
                ),
                array(
                   'name'=>'syaratbayar_id',
                    'type'=>'raw',
                    'value'=>'$data->syaratbayar->syaratbayar_nama',
                ),
                'tglterimabarang',
        	array(
               'header'=>'Details',
               'type'=>'raw',
               'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/Details",array("idPermintaanPembelian"=>$data->permintaanpembelian_id)),
                            array("class"=>"",
                                  "target"=>"iframe",
                                  "onclick"=>"$(\"#dialogDetails\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik Untuk melihat details Permintaan Pembelian",
                            ))',
            ),
		array(
		   'header'=>'',
		   'type'=>'raw',
		   'value'=>'CHtml::Link("<i class=\"icon-remove\"></i>","'.$this->createUrl(Yii::app()->controller->id . '/batal').'&permintaanpembelian_id=$data->permintaanpembelian_id",
				array(
				"class"=>"",
				"title"=>"Klik untuk membatalkan penerimaan",
			))',
			'htmlOptions'=>array(
				'style'=>'text-align:center'
			)
		),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                    <?php echo CHtml::label('Tgl Permintaan Pembelian','tglPermintaanPembelian', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPermintaanPembelian,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPermintaanPembelian,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                </div>
                <?php echo $form->dropDownListRow($modPermintaanPembelian,'syaratbayar_id',
                                                               CHtml::listData($modPermintaanPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
                                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                               'empty'=>'-- Pilih --',)); ?>
        </td>
        <td>
              <?php echo $form->textFieldRow($modPermintaanPembelian,'nopermintaan',array('class'=>'numberOnly')); ?>
              <?php echo $form->dropDownListRow($modPermintaanPembelian,'supplier_id',
                                                               CHtml::listData($modPermintaanPembelian->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                               'empty'=>'-- Pilih --',)); ?>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?><?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
?>
</div>
<?php $this->endWidget(); ?>

</div>
<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action=$this->getAction()->getId();
$currentUrl=  Yii::app()->createUrl($module.'/'.$controller.'/'.$action);
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hiddenPenerimaan',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('target'=>'_new'),
        'action'=>Yii::app()->createUrl($module.'/penerimaanItems/index'),
)); ?>
    <?php echo CHtml::hiddenField('idPembelianForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('noPembelianForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('tglPembelianForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('currentUrl',$currentUrl,array('readonly'=>true));?>
<?php $this->endWidget(); ?>
<?php
// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogDetails',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Details Permintaan Pembelian',
                        'autoOpen'=>false,
                        'minWidth'=>900,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="iframe" width="100%" height="500">
</iframe>
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================

$js = <<< JSCRIPT
function formPenerimaanItems(idPembelian,noPembelian,tglPembelian)
{
    $('#idPembelianForm').val(idPembelian);
    $('#noPembelianForm').val(noPembelian);
    $('#tglPembelianForm').val(tglPembelian);
    $('#form_hiddenPenerimaan').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);
