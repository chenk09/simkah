<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<style>
    .numbersOnly, .float{
        text-align: right;
    }
</style>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
  'id'=>'gfrencanaKebFarmasi-m-form',
  'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'return cekSupplier();'),
        'focus'=>'#GFPermintaanPembelianT_supplier_id',
)); 
$this->widget('bootstrap.widgets.BootAlert'); ?>


<fieldset>
<legend class="rim2">Transaksi Pemintaan Pembelian</legend>
<?php echo $form->errorSummary($modPermintaanPembelian); ?>
<fieldset>
    <legend class="rim"> Data Permintaan Pembelian</legend>

    <table>
        <tr>
            <td>
                <?php if(empty ($_GET['idPenawaran'])){ ?>
                 <?php echo $form->dropDownListRow($modPermintaanPembelian,'supplier_id',
                                                   CHtml::listData($modPermintaanPembelian->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                   array('class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)" ,'onChange'=>'setValue();','class'=>'idSupplier',
                                                   'empty'=>'-- Pilih --')); ?>
                <?php }else if(empty ($_GET['idRencana'])){ ?>
                <?php echo $form->dropDownListRow($modPermintaanPembelian,'supplier_id',
                                                   CHtml::listData($modPermintaanPembelian->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                   array('class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)" ,'class'=>'idSupplier',
                                                   'empty'=>'-- Pilih --')); ?>
                <?php }else{ ?>
                <?php echo $form->dropDownListRow($modPermintaanPembelian,'supplier_id',
                                                   CHtml::listData($modPermintaanPembelian->SupplierItems, 'supplier_id', 'supplier_nama'),
                                                   array('disabled'=>'disabled','class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'idSupplier',
                                                   'empty'=>'-- Pilih --')); ?>
                <?php echo CHtml::activeHiddenField($modPermintaanPembelian, 'supplier_id', array('value'=>$modPermintaanPembelian->supplier_id,'readonly'=>true)); ?>
                <?php } ?>
                <?php echo CHtml::hiddenField('idSupplier',"", array('readonly'=>true)); ?>
                 <?php echo $form->dropDownListRow($modPermintaanPembelian,'syaratbayar_id',
                                                   CHtml::listData($modPermintaanPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
                                                   array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                   'empty'=>'-- Pilih --',)); ?>
                 <?php echo $form->textFieldRow($modPermintaanPembelian,'nopermintaan',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                   )); ?>

                 <?php echo $form->textAreaRow($modPermintaanPembelian,'keteranganpermintaan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                 <?php echo $form->textAreaRow($modPermintaanPembelian,'alamatpengiriman',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                 
                 <div class="control-group ">
                        <?php echo $form->labelEx($modPermintaanPembelian,'tglpermintaanpembelian', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                            'model'=>$modPermintaanPembelian,
                                            'attribute'=>'tglpermintaanpembelian',
                                            'mode'=>'date',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                'maxDate'=>'d',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                            ),
                                )); ?>
                            </div>
                 </div>
            </td>
<!--            <td>-->
<!--                <fieldset>
                    <legend>Informasi Harga</legend>
                    <table>
                        <tr>
                            <td width="140px" colspan="2"><?php //echo CHtml::label('Total Harga Sebelum Diskon', 'Total Harga Sebelum Diskon') ;?></td>
                            <td>=<?php //echo CHtml::textField('totalSebelumDiskon',$var['totalSebelumDiskon'],array('class'=>'span1 numbersOnly','readonly'=>false,)); ?></td>
                        </tr>
                        <tr>
                            <td width="30px"><?php //echo CHtml::label('Diskon', 'Diskon') ;?></td>
                            <td width="80px"><?php //echo CHtml::textField('diskon',0,array('class'=>'span1 numbersOnly','onkeyup'=>'gantiDiskon(this)','maxlength'=>2,)); ?>&nbsp;%</td>
                            <td>=<?php //echo CHtml::textField('jumlahDiskon',0,array('class'=>'span1','readonly'=>true)); ?></td>

                            <td width="30px"><?php //echo CHtml::label('Diskon', 'Diskon') ;?></td>
                            <td width="80px"><?php //echo CHtml::textField('diskon',$var['diskon'],array('class'=>'span1 numbersOnly','onkeyup'=>'gantiDiskon(this)','maxlength'=>2,)); ?>&nbsp;%</td>
                            <td>=<?php //echo CHtml::textField('jumlahDiskon',$var['jumlahDiskon'],array('class'=>'span1','readonly'=>true)); ?></td>

                        </tr>
                        <tr>
                            <td width="140px" colspan="2"><?php //echo CHtml::label('Total Harga Setelah Diskon', 'Total Harga Setelah Diskon') ;?></td>
                            <td>=<?php //echo CHtml::textField('totalSetelahDiskon',$var['totalSebelumDiskon'],array('class'=>'span1','readonly'=>true,'onblur'=>'numberOnlyNol(this);')); ?></td>
                        </tr>
                        
                    <tr>
                        <td><?php //echo CHtml::checkBox('termasukPPN',false,array('onclick'=>'persenPPN(this)','style'=>'width : 10px'))?></td>
                        <td><?php //echo CHtml::label('PPN', 'PPN',array("class"=>""));?></td>
                        <td>=<?php //echo CHtml::textField('totalPPN',0,array('readonly'=>TRUE,'class'=>'span1')); ?></td>
                    </tr>
                    <tr>
                         <td><?php //echo CHtml::checkBox('termasukPPH',false,array('onclick'=>'persenPPH(this)','style'=>'width : 10px'))?></td>
                        <td><?php //echo CHtml::label('PPH', 'PPH',array("class"=>""));?></td>
                        <td>=<?php //echo CHtml::textField('totalPPH',0,array('readonly'=>TRUE,'class'=>'span1')); ?></td>
                        <td><?php //echo CHtml::checkBox('termasukPPN',$var['termasukPPN'],array('onclick'=>'persenPPN(this)','style'=>'width : 10px'))?></td>
                        <td><?php //echo CHtml::label('PPN', 'PPN',array("class"=>""));?></td>
                        <td>=<?php //echo CHtml::textField('totalPPN',$var['totalPPN'],array('readonly'=>TRUE,'class'=>'span1')); ?></td>
                    </tr>
                    <tr>
                         <td><?php // echo CHtml::checkBox('termasukPPH',$var['termasukPPH'],array('onclick'=>'persenPPH(this)','style'=>'width : 10px'))?></td>
                        <td><?php //echo CHtml::label('PPH', 'PPH',array("class"=>""));?></td>
                        <td>=<?php //echo CHtml::textField('totalPPH',$var['totalPPH'],array('readonly'=>TRUE,'class'=>'span1', )); ?></td>
                    </tr>
                        
                    </table>
                </fieldset>-->
               
               

<!--            </td>-->
        <!-- EHJ-984 -->
                        <td>
                            <fieldset>
                                <legend><?php echo CHtml::checkBox('isUangMuka',((isset($var['isUangMuka'])) ? $var["isUangMuka"] : false),array('onclick'=>'isUangMukaJava(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?> Uang Muka</legend>
                                <div class="control-group ">
                                    <?php echo $form->labelEx($modUangMuka,'namabank',array("class"=>"control-label"));?>
                                        <div class="controls">
                                           <?php echo $form->textField($modUangMuka,'namabank',array('readonly'=>TRUE,'class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                        </div>
                                </div>
                                <div class="control-group ">
                                    <?php echo $form->labelEx($modUangMuka,'norekening',array("class"=>"control-label"));?>
                                        <div class="controls">
                                           <?php echo $form->textField($modUangMuka,'norekening',array('readonly'=>TRUE,'class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                        </div>
                                </div>
                                <div class="control-group ">
                                    <?php echo $form->labelEx($modUangMuka,'rekatasnama',array("class"=>"control-label"));?>
                                        <div class="controls">
                                           <?php echo $form->textField($modUangMuka,'rekatasnama',array('readonly'=>TRUE,'class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                        </div>
                                </div>
                                <div class="control-group ">
                                    <?php echo $form->labelEx($modUangMuka,'jumlahuang',array("class"=>"control-label"));?>
                                        <div class="controls">
                                           <?php echo $form->textField($modUangMuka,'jumlahuang',array('readonly'=>TRUE,'class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                        </div>
                                </div>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </fieldset> 
        </td>
                        <!-- END EHJ-984 -->
            
        </tr>
       </table>
</fieldset> 
<table>
    <tr>
        <td>
            <?php if (!isset($modPermintaanDetail) || ((!isset($modRencanaKebFarmasi)) && (!isset($modPermintaanPenawaran) && ($modPermintaanPembelian->isNewRecord)))  ){ ?>
            <!-- ========================= Form berdasarkan Pemilihan Obat ============================= -->
            <fieldset id="formpemilihanobat">
                <legend class="rim" style="width:240px;">Pemilihan Obat</legend>
                <table>
                    <tr>
                        <td>
                            <div class="control-group ">
                                <?php echo $form->labelEx($modPermintaanPembelian,'obatAlkes ', array('class'=>'control-label')) ?> 
                                <div class="controls">
                                    <?php echo CHtml::hiddenField('idObatAlkes');?>
                                    <?php $this->widget('MyJuiAutoComplete',array(
                                                'model'=>$modPermintaanPembelian,
                                                'attribute'=>'obatAlkes',
//                                                'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/ObatAlkes'),
                                                'sourceUrl'=>'js:function(request,response){
                                                                    idSupplier = $("#idSupplier").val();
                                                                    $.get("'.Yii::app()->createUrl('actionAutoComplete/obatAlkesSupplier').'&idSupplier="+idSupplier,request,response,"json");
                                                                }',
                                                'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'select'=>'js:function( event, ui ) {
                                                              $("#idObatAlkes").val(ui.item.obatalkes_id);
                                                   }',
                                                ),
                                                'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3'),   
                                                'tombolDialog'=>array('idDialog'=>'dialogObatalkesM'), //dialogObatsupplierM akak di switch otomastis jika berdasarkanSupplier di ceklis
                                    )); ?>
                                    
                                    <?php //echo CHtml::checkBox('berdasarkanSupplier', false,array('onclick'=>'setValue();', 'title'=>'Filter Obat Berdasarkan Supplier', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                </div>
                                 <div class="controls"> 
                                        <?php echo CHtml::checkBox('berdasarkanSupplier', false,array('onclick'=>'setValue();', 'title'=>'Filter Obat Berdasarkan Supplier', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        <?php echo $form->label($modPermintaanPembelian,'Cek Jika Ingin Menampilakan obat dan alkes berdasarkan supplier'); ?>
                                 </div>                                
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="control-group ">
                                <?php echo Chtml::label('Qty (Satuan Besar)', 'Qty',array("class"=>'control-label'));?>
                                <div class="controls">
                                    <?php echo Chtml::textField('qtyObat','1',array('class'=>'span1 numbersOnlys', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                    <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                    array('onclick'=>'submitObat(); $(\'#GFPermintaanPembelianT_obatAlkes\').focus();',
                                          'class'=>'btn btn-primary',
                                          'onkeypress'=>"submitObat(); $('#GFPermintaanPembelianT_obatAlkes').focus();",
                                          'rel'=>"tooltip",
                                          'id'=>'tambahObat',  
                                          'title'=>"Klik Untuk Menambahkan Obat",)); ?>
                                </div>
                            </div>
                       </td>
                    </tr>
                </table>
             </fieldset>
            <?php } ?>
            <!-- ========================= Akhir Form berdasarkan pemilihan obat ============================= -->
            <!-- ========================= Form berdasarkan rencana kebutuhan ============================= -->
            <?php if (isset($modRencanaKebFarmasi)) { ?>
            <fieldset id="formrencanakebutuhan">
                <legend class="rim" style="width:270px;">Berdasarkan Rencana Kebutuhan</legend>
                
                        <?php // echo CHtml::checkBox('checkBoxRencana',false,array('onclick'=>'fungsiCheckBoxRencana(this)','readonly'=>true)) ?>
                        <div class="control-group">
                                    <?php echo $form->labelEx($modRencanaKebFarmasi,'noperencnaan', array('class'=>'control-label')) ?>
                                    <?php echo $form->hiddenField($modRencanaKebFarmasi,'rencanakebfarmasi_id',array('class'=>'span1'));?>
                                <div class="controls">
                                    <?php $this->widget('MyJuiAutoComplete',array(
                                                'model'=>$modRencanaKebFarmasi,
                                                'attribute'=>'noperencnaan',
                                                'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/noPerencanaan'),
                                                'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'select'=>'js:function( event, ui ) {
                                                              $("#'.CHtml::activeId($modRencanaKebFarmasi,'rencanakebfarmasi_id').'").val(ui.item.rencanakebfarmasi_id);
                                                              $("#'.CHtml::activeId($modRencanaKebFarmasi,'tglperencanaan').'").val(ui.item.tglperencanaan);
                                                              submitRencana();    
                                                    }',
                                                ),
                                                'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'class'=>'span3',
                                                'readonly'=>TRUE
                                                ),'tombolDialog'=>array('idDialog'=>'dialogPerencanaan'.'disable'),
                                    )); ?>
                                    <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                            //array('onclick'=>'$("#dialogPerencanaan").dialog("open");return false;',
                                             //     'class'=>'btn btn-primary',
                                               //   'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                //  'rel'=>"tooltip",
                                                //  'title'=>"Klik Untuk Rencana Kebutuhan Farmasi Lebih Lanjut",
                                                //  'disabled'=>TRUE, 
                                              //   'id'=>'buttonRencaKebutuhan'  )); ?>
                            </div>
                        </div>
                        <div class="control-group">
                               <?php echo $form->textFieldRow($modRencanaKebFarmasi,'tglperencanaan',array('class'=>'span2','readonly'=>divUE,'onkeypress'=>"return $(this).focusNextInputField(event)",));?>
                        </div>
            </fieldset>
            <?php } ?>
            <!-- ========================= Akhir form berdasarkan rencana kebutuhan ============================= -->
            
            <!-- ========================= Form berdasarkan permintaan penawaran ============================= -->
            <?php if (isset($modPermintaanPenawaran)) { ?>
            <fieldset id="formpenawaran">
                <legend class="rim" style="width:300px;">Berdasarkan Permintaan Penawaran</legend>
                    <div class="control-group">  
                            <?php echo $form->labelEx($modPermintaanPembelian,'nopermintaan', array('class'=>'control-label')) ?>
                            <?php echo $form->hiddenField($modPermintaanPembelian,'permintaanpenawaran_id',array('class'=>'span1','readonly'=>TRUE));?>
                        <div class="controls">
                            <?php $this->widget('MyJuiAutoComplete',array(
                            'model'=>$modPermintaanPenawaran,
                            'attribute'=>'nosuratpenawaran',
                            'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/noSuratPenawaran'),
                            'options'=>array(
                               'showAnim'=>'fold',
                               'minLength' => 2,
                               'select'=>'js:function( event, ui ) {
                                          $("#'.CHtml::activeId($modPermintaanPembelian,'permintaanpenawaran_id').'").val(ui.item.permintaanpenawaran_id);
                                          $("#'.CHtml::activeId($modPermintaanPenawaran,'tglpenawaran').'").val(ui.item.tglpenawaran);

                                          submitPermintaan();    
                                        }',
                            ),
                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",
                            'class'=>'span3',
                            'readonly'=>TRUE),  'tombolDialog'=>array('idDialog'=>'dialogPenawaran'.'disable'),
                            )); ?>
                        </div>
                            <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                   // array('onclick'=>'$("#dialogPenawaran").dialog("open");return false;',
                        //  'class'=>'btn btn-primary',
                        //  'onkeypress'=>"return $(this).focusNextInputField(event)",
                     //     'rel'=>"tooltip",
                       //   'title'=>"Klik Untuk Permintaan Penawaran Farmasi Lebih Lanjut",
                       //   'disabled'=>TRUE,
                      //    'id'=>'buttonpermintaanPenawaran',  )); ?>
                </div>
                <div class="control-group">
                       <?php echo $form->textFieldRow($modPermintaanPenawaran,'tglpenawaran',array('class'=>'span2','readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",));?>
                </div>
            </fieldset>
            <?php } 
            
            ?>
            <!-- ========================= Akhir form berdasarkan permintaan penawaran ============================= -->
        </td>
    </tr>
</table>
</fieldset>
<?php if (isset($modPermintaanDetail)) { 
    echo $form->errorSummary($modPermintaanDetail);
}
?>
<table id="tableObatAlkes" class="table table-bordered table-condensed" parent-informasi="<?php echo (isset($modPermintaanPenawaran)) ? "Penawaran" : ((isset($modRencanaKebFarmasi) ? "Rencana" : "")); ?>">
    <thead>
    <tr>
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Jumlah Kemasan<br>(Satuan Kecil/Besar)</th>
        <?php if (isset($modPermintaanPenawaran)) { ?>
        <th>Jumlah Penawaran<br>(Satuan Besar)</th>
        <?php } else if (isset($modRencanaKebFarmasi)) {?>
        <th>Jumlah Rencana<br>(Satuan Besar)</th>
        <?php } ?>
        <th>Jumlah Pembelian<br>(Satuan Besar)</th>
        <th>Harga Kemasan</th>
        <th>Stok Akhir</th>
        <th>PPN</th>
        <th>PPH</th>
        <th>Diskon (%)</th>
        <th>Diskon Total (Rp)</th> 
        <th>Sub Total</th>
         <!-- EHJ - 984  dikembalikan seperti awal-->
        <?php echo (($modPermintaanPembelian->isNewRecord) ? ((isset($modPermintaanPenawaran) || (isset($modRencanaKebFarmasi))) ? "" : "<th>Batal</th>") : ""); ?>
     
    </tr>
    </thead>
    <tbody>
        <?php
            $totalDiskon = 0;
            $total = 0;
            $colspanTotal = (!empty($_GET['idPembelian'])) ? 6 : 6;
        ?>
        <?php if (isset($modPermintaanDetail)){
           if (count($modPermintaanDetail) > 0) {
               $no = 1;
               $tr ='';
               foreach ($modPermintaanDetail as $key => $detail) {
                   $modObatAlkes = ObatalkesM::model()->findByPk($detail->obatalkes_id);
                   $subTotal = ($detail->jmlpermintaan*$modObatAlkes->kemasanbesar*($detail->harganettoper+$detail->hargapphper+$detail->hargappnper))-$detail->jmldiscount;
                   $stokAkhir = StokobatalkesT::getStokBarang($detail->obatalkes_id, Yii::app()->user->getState('ruangan_id'));
                   $totalDiskon += $detail->jmldiscount;
                   $total += $subTotal;
                   $tr .="<tr>
                            <td> ".CHtml::TextField('noUrut',$no++,array('class'=>'span1 noUrut','readonly'=>TRUE)).
                                   CHtml::activeHiddenField($detail,'['.$key.']obatalkes_id', array('class'=>'obatAlkes')).
                                   CHtml::activeHiddenField($detail,'['.$key.']satuankecil_id'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']sumberdana_id'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']satuanbesar_id'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']stokakhir',array('value'=>$stokAkhir)). 
                                   CHtml::activeHiddenField($detail,'['.$key.']maksimalstok'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']minimalstok'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']tglkadaluarsa'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']jmlkemasan').
                           "</td>
                            <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                            <td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                            <td>".CHtml::activetextField($detail, '['.$key.']jmlkemasan', array('readonly'=>true,'value'=>$modObatAlkes->kemasanbesar, 'class'=>'span1 numbersOnly'))
                           ." ".$modObatAlkes->satuankecil->satuankecil_nama."/".$modObatAlkes->satuanbesar->satuanbesar_nama."</td>";
                    if (isset($modPermintaanPenawaran)){
                        $tr .="<td>".$detail->jmlPenawaran." ".$detail->satuanbesar->satuanbesar_nama."</td>";
                        $colspanTotal = 11;
                    }else if(isset($modRencanaKebFarmasi)){
                        $tr .="<td>".$detail->jmlpermintaan." ".$detail->satuanbesar->satuanbesar_nama."</td>";
                        $colspanTotal = 11;
                    }
                     $tr .="<td>".CHtml::activetextField($detail,'['.$key.']jmlpermintaan',array('readonly'=>false,'class'=>'span1 numbersOnly qty','onkeyup'=>'hitungHargaNetto(this); hitungSubtotal(this);', 'maximum'=>((isset($modPermintaanPenawaran) || isset($modRencanaKebFarmasi)) ? $detail->jmlpermintaan : '')))
                            ." ".((empty($detail->satuanbesar->satuanbesar_nama)) ? $modObatAlkes->satuanbesar->satuanbesar_nama : $detail->satuanbesar->satuanbesar_nama)."</td>
                            <td>".CHtml::activetextField($detail,'['.$key.']hargabelibesar',array('class'=>'span1 numbersOnly','readonly'=>false, 'onkeyup'=>'hitungHargaNetto(this);hitungSubtotal(this);'))
                            .CHtml::activehiddenField($detail,'['.$key.']harganettoper',array('class'=>'span1 numbersOnly netto','readonly'=>true))."</td>
                            <td>".$stokAkhir." ".$detail->satuankecil->satuankecil_nama."</td>
                            <td>".CHtml::activetextField($detail,'['.$key.']hargappnper',array('class'=>'span1 numbersOnly ppn', 'readonly'=>true))."</td>
                            <td>".CHtml::activetextField($detail,'['.$key.']hargapphper',array('class'=>'span1 numbersOnly pph', 'readonly'=>true))."</td>
                            <td>".CHtml::activetextField($detail,'['.$key.']persendiscount',array('class'=>'span1 float persen','onkeyup'=>'hitungJmlDiskon(this);hitungSubtotal(this);'))."</td>
                            <td>".CHtml::activetextField($detail,'['.$key.']jmldiscount',array('class'=>'span2 numbersOnly jumlah','onkeyup'=>'hitungPersenDiskon(this);'))."</td>
                            <td>".CHtml::textField('subTotal',$subTotal,array('class'=>'span2 numbersOnly subTotal', 'readonly'=>true))."</td>".
                            (($modPermintaanPembelian->isNewRecord) ? ((isset($modPermintaanPenawaran) || (isset($modRencanaKebFarmasi))) ? "" : "<td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;'))."</td>") : "").
                            "</tr>";
               }
               echo $tr;
           }
        }?>
        <tfoot>
            <tr>
                <td colspan="<?php echo $colspanTotal;?>">
                    <?php echo CHtml::hiddenField('totalDiskon',$totalDiskon,array('class'=>'span2 numbersOnly'))?>
                    <?php echo CHtml::hiddenField('total',$total,array('class'=>'span2 numbersOnly'))?>
                </td>
<!--                <td></td>
                <td></td>-->
            </tr>
        </tfoot>
    </tbody>
</table>
        <div class="form-actions">
    <?php
                 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
                 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
                 $action=$this->getAction()->getId();
                 $currentAction=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/'.$action);
if (!$modPermintaanPembelian->isNewRecord)
{
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idPembelian'=>$modPermintaanPembelian->permintaanpembelian_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}else{
    echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'cekValidasi()'));
    echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan', 'style'=>'display:none;'));
}
?>

                    <?php if (isset($_POST['idPerencanaanFormPembelian'])){//Jika Masuk ke Halaman ini memalui Informasi Rencana kebutuhan
                                   echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                  $_POST['currentUrl'], 
                                   array('class'=>'btn btn-danger',));
                           }else{
                                   echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                   $currentAction, 
                                   array('class'=>'btn btn-danger',));
                            }  
                    ?> 
              <?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>            
  </div>

<?php $this->endWidget(); ?>

<?php
$urlGetObatSupplier = Yii::app()->createUrl('actionAjax/GetObatAlkesPembelian');//MAsukan Dengan memilih Obat Alkes
$urlGetObatAlkesDariRencana = Yii::app()->createUrl('actionAjax/getObatAlkesDariPerencanaan');//MAsukan Dengan memilih Perencanaa
$urlGetObatAlkesDariPermintaan = Yii::app()->createUrl('actionAjax/getObatAlkesDariPermintaan');//MAsukan Dengan memilih Perencanaa
$urlGetSupplier = Yii::app()->createUrl('actionAjax/getSupplier');//MAsukan Dengan memilih Supplier
$idRencana =CHtml::activeId($modRencanaKebFarmasi,'rencanakebfarmasi_id');
$idPermintaan=CHtml::activeId($modPermintaanPembelian,'permintaanpenawaran_id');
$tglRencana=CHtml::activeId($modRencanaKebFarmasi,'tglperencanaan');
$konfigFarmasi = KonfigfarmasiK::model()->find();
$persenPPN= $konfigFarmasi->persenppn;
$persenPPH= $konfigFarmasi->persenpph;
$obatAlkes=CHtml::activeId($modPermintaanPembelian,'obatAlkes');
$noPerencanaan = CHtml::activeId($modRencanaKebFarmasi,'noperencnaan');

$noPenawaran = CHtml::activeId($modPermintaanPenawaran,'nosuratpenawaran');
$tglPenawaran = CHtml::activeId($modPermintaanPenawaran,'tglpenawaran');
$namaBank = CHtml::activeId($modUangMuka,'namabank');
$noRekening = CHtml::activeId($modUangMuka,'norekening');   
$rekAtasNama = CHtml::activeId($modUangMuka,'rekatasnama');
$jumlahUang = CHtml::activeId($modUangMuka,'jumlahuang');
$urlHalamanIni = Yii::app()->createUrl($this->route);
$clearObatalkes = ($modPermintaanPembelian->isNewRecord && (empty($_GET['idRencana']))) ? '$("#tableObatAlkes tbody").html("");': '';
$idSupplier = CHtml::activeId($modPermintaanPembelian, 'supplier_id');
$inputObat = CHtml::activeId($modPermintaanPembelian, 'obatAlkes');
$jscript = <<< JS

function setValue()
{
    id = $("#${idSupplier}").val();
    $('#${obatAlkes}').val('');
    $('#idObatAlkes').val('');
    //$//{clearObatalkes}
    if(id == ""){
        $("#berdasarkanSupplier").removeAttr("checked");
        alert("Anda belum memilih Supplier !");
    }
    if($("#berdasarkanSupplier").is(":checked")){
        $("#idSupplier").val(id);
        //switch to dialogObatsupplierM
        $("#${inputObat}").parent().find("a").removeAttr("onclick");
        $("#${inputObat}").parent().find("a").attr("onclick",'$(\"#dialogObatsupplierM\").dialog(\"open\");return false;');
        $.post("${urlGetSupplier}", {idSupplier:id},
            function(data){
               $('#idSupplier').val(data.supplier_id);
               $('#idSupplier2').val(data.supplier_id);
               $(".numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":".","thousands":"","precision":0,"symbol":null});
               $(".float").maskMoney({"defaultZero":true,"allowZero":true,"decimal":".","thousands":"","precision":2,"symbol":null});
               $.get('${urlHalamanIni}', {idSupplier:data.supplier_id}, function(datas){
                    $.fn.yiiGridView.update('obatSupplier-m-grid', {
                        url: document.URL+'&ObatsupplierM%5Bsupplier_id%5D='+data.supplier_id,
                    }); 
                });

        }, "json");
    }else{
        id = "";
        $("#idSupplier").val(id);
        //switch to dialogObatAlkesM
        $("#${inputObat}").parent().find("a").removeAttr("onclick");
        $("#${inputObat}").parent().find("a").attr("onclick",'$(\"#dialogObatalkesM\").dialog(\"open\");return false;');       
    }
}
           
function setDialogTindakan(){
   supplier =  $('#idSupplier').val();
   $.get('${urlHalamanIni}', {idSupplier:supplier}, function(datas){
        $.fn.yiiGridView.update('obatSupplier-m-grid', {
            url: document.URL+'&ObatsupplierM%5Bsupplier_id%5D='+supplier,
        }); 
    });
}
   
function gantiDiskon(obj){
    value = parseFloat($(obj).val());
    hasil = 0;
    $(".persen").each(function(){
        netto = parseFloat($(this).parents("tr").find(".netto").val());
        qty = parseFloat($(this).parents("tr").find(".qty").val());
        if (jQuery.isNumeric(value)){
            $(this).val(value);
        }
        hasil = (netto * qty*value)/100
        $(this).parents("tr").find(".jumlah").val(hasil);
    });
    hitungSemua();
}

function isUangMukaJava(obj)
{
    if(obj.checked==true){
    
        $('#${namaBank}').removeAttr('readonly');
        $('#${noRekening}').removeAttr('readonly');
        $('#${rekAtasNama}').removeAttr('readonly');
        $('#${jumlahUang }').removeAttr('readonly');  
        
    }else{
        
        $('#${namaBank}').attr('readonly','TRUE');
        $('#${noRekening}').attr('readonly','TRUE');
        $('#${rekAtasNama}').attr('readonly','TRUE');
        $('#${jumlahUang }').attr('readonly','TRUE');  
        
        $('#${namaBank}').val('');
        $('#${noRekening}').val('');
        $('#${rekAtasNama}').val('');
        $('#${jumlahUang }').val(0);  
        
    }

}
function fungsiCheckBoxRencana(obj)
{
    hapusObat();
    if(obj.checked==true){
        $('#GFRencanaKebFarmasiT_noperencnaan').removeAttr('readonly');
        $('#buttonRencaKebutuhan').removeAttr('disabled');
        $('#tambahObat').attr('disabled','TRUE');
        $('#checkBoxPermintaanPenawaran').removeAttr('checked');
        $('#GFPermintaanPenawaranT_nosuratpenawaran').attr('disabled','true');
        $('#buttonpermintaanPenawaran').attr('disabled','true');
        
        $('#buttonObatAlkes').attr('disabled','true');
        $('#GFPermintaanPembelianT_obatAlkes').attr('readonly','true');
        
        $('#${tglRencana}').val('');
        $('#${noPerencanaan}').val('');
        
        $('#${noPenawaran}').val('');
        $('#${tglPenawaran}').val('');
        $('#tambahObat').attr('disabled','TRUE');

    }else{
        $('#GFRencanaKebFarmasiT_noperencnaan').attr('readonly','true');
        $('#buttonRencaKebutuhan').attr('disabled','true');
        
        $('#buttonObatAlkes').removeAttr('disabled','true');
        $('#GFPermintaanPembelianT_obatAlkes').removeAttr('readonly','true');
        
        $('#${tglRencana}').val('');
        $('#${noPerencanaan}').val('');
        $('#${noPenawaran}').val('');
        $('#${tglPenawaran}').val('');
        $('#tambahObat').removeAttr('disabled','TRUE');
        $('#checkBoxRencana').attr('readonly',true);
        $('#formrencanakebutuhan').hide();
        $('#formpemilihanobat').show();
        $('#formpenawaran').hide();
    }
     
}

function fungsiCheckBoxPermintaan(obj)
{
   hapusObat();
   if(obj.checked==true){
        $('#GFPermintaanPenawaranT_nosuratpenawaran').removeAttr('readonly');
        $('#buttonpermintaanPenawaran').removeAttr('disabled');

        $('#checkBoxRencana').removeAttr('checked');
        $('#GFRencanaKebFarmasiT_noperencnaan').attr('disabled','true');
        $('#buttonRencaKebutuhan').attr('disabled','true');
        
        $('#buttonObatAlkes').attr('disabled','true');
        $('#GFPermintaanPembelianT_obatAlkes').attr('readonly','true');
        $('#${tglRencana}').val('');
        $('#${noPerencanaan}').val('');
        $('#${noPenawaran}').val('');
        $('#${tglPenawaran}').val('');
        $('#tambahObat').attr('disabled','TRUE');

    }else{
        $('#${tglRencana}').val('');
        $('#${noPerencanaan}').val('');
        $('#${noPenawaran}').val('');
        $('#${tglPenawaran}').val('');
        
        $('#GFPermintaanPenawaranT_nosuratpenawaran').attr('readonly','true');
        $('#buttonpermintaanPenawaran').attr('disabled','true');
        
        $('#buttonObatAlkes').removeAttr('disabled','true');
        $('#GFPermintaanPembelianT_obatAlkes').removeAttr('readonly','true');
        $('#tambahObat').removeAttr('disabled','TRUE');
        $('#checkBoxPermintaanPenawaran').attr('readonly',true);
        $('#formpenawaran').hide();
        $('#formpemilihanobat').show();
        $('#formrencanakebutuhan').hide();

    }
}

function hapusObat()
{
    banyakRow=$('#tableObatAlkes tr').length;

    for(i=2; i<=banyakRow; i++){
    $('#tableObatAlkes tr:last').remove();
    }
    
    $('#totalSebelumDiskon').val('0');
    $('#jumlahDiskon').val('0');
    $('#totalSetelahDiskon').val('0');
    $('#diskon').val('0');
}

function cekValidasi()
{   
  
  banyaknyaObat = $('.obatAlkes').length;
  hargaSebelumDiskon = parseFloat($('#totalSebelumDiskon').val());
  if ($('.isRequired').val()==''){
    alert ('Harap Isi Semua Data Yang Bertanda *');
  }else if(banyaknyaObat<1){
     alert('Anda Belum memimilih Obat Yang Akan Diminta');   
  }else if(hargaSebelumDiskon==0){
     alert('Harga sebelum diskon tidak boleh nol');   
  }else if ($('#isUangMuka').is(':checked') && $('#${jumlahUang}').val()<1){
     alert('Jumlah Uang Muka Harus Diisi');
  }else{
     $('#btn_simpan').click();
  }
}

function persenPPN(obj)
{
    if(obj.checked==true){ //Jika tidak termasuk PPN
        totalPPN=0;
        $('.ppn').each(function() {
           hargaNettoProduk=parseFloat($(this).parents("tr").find('.netto').val());
           jumlahPPN=hargaNettoProduk * (parseFloat(${persenPPN})/100);
           if (jQuery.isNumeric(jumlahPPN)){
                $(this).val(jumlahPPN);
           }
           totalPPN+=parseFloat(jumlahPPN);
        }); 
        $('.subTotal').each(function() {
           hargaNettoProduk=parseFloat($(this).parents("tr").find('.netto').val());
           subTotal=parseFloat($(this).parents("tr").find('.subTotal').val());
           jumlahPPN=hargaNettoProduk * (parseFloat(${persenPPN})/100);
           subTotal = subTotal + jumlahPPN
           if (jQuery.isNumeric(subTotal)){
                $(this).val(subTotal);
           }
        }); 
        $('#totalPPN').val(totalPPN);
    }else{ //Jika Termasuk PPN
        if($('#termasukPPH').is(':checked')){   
            $('.ppn').each(function() {
               $(this).val(0);
               $(this).attr('readonly','TRUE');
            });    
        }
        $('#termasukPPH').removeAttr('checked'); 
        $('.pph').each(function() {
           $(this).val(0);
           $(this).attr('readonly','TRUE');
        }); 
        $('#totalPPH').val(0);     
        $('#totalPPN').val(0);      
    }
    hitungSemua();
}
               
function onchangePPN()
{
   $('.ppn').each(function() {
       hargaNettoProduk=parseFloat($(this).parent().prev().prev().find('input').val());
       jumlahPPN=hargaNettoProduk * (parseFloat(${persenPPN})/100);
       $(this).val(jumlahPPN);
       $(this).removeAttr('readonly');
       totalPPN = totalPPN + parseFloat(jumlahPPN);

    });            
               
}               
               
function persenPPH(obj)
{
    isPPH = obj.value;
    if($('#termasukPPN').is(':checked'))
    {           
        if(obj.checked==true){ //Jika tidak termasuk PPN
             totalPPH=0;  
             $('.pph').each(function() {
                   hargaNettoProduk=parseFloat($(this).parents("tr").find('.netto').val());
                   jumlahPPN=parseFloat($(this).parents("tr").find('.ppn').val());
                   NettoDikurangPPN = hargaNettoProduk - jumlahPPN;
                   jumlahPPH=NettoDikurangPPN * (parseFloat(${persenPPH})/100);
                   if (jQuery.isNumeric(totalPPH)){
                        $(this).val(jumlahPPH);
                   }
                   totalPPH +=parseFloat(jumlahPPH);
                   if (jQuery.isNumeric(totalPPH)){
                        $('#totalPPH').val(totalPPH);
                   }
            });
            $('.subTotal').each(function() {
                   hargaNettoProduk=parseFloat($(this).parents("tr").find('.netto').val());
                   subTotal=parseFloat($(this).parents("tr").find('.subTotal').val());
                   jumlahPPN=parseFloat($(this).parents("tr").find('.ppn').val());
                   NettoDikurangPPN = hargaNettoProduk + jumlahPPN;
                   jumlahPPH=NettoDikurangPPN * (parseFloat(${persenPPH})/100);
                   subTotal = subTotal + jumlahPPH;
                   if (jQuery.isNumeric(subTotal)){
                        $(this).val(subTotal);
                   }
                   totalPPH +=parseFloat(jumlahPPH);
                   if (jQuery.isNumeric(totalPPH)){
                        $('#totalPPH').val(totalPPH);
                   }
            }); 

        }else{ //Jika Termasuk PPN
             $('.pph').each(function() {
                   $(this).val(0);
                   $(this).attr('readonly','TRUE');
            }); 
            $('#totalPPH').val(0);  
        }
    }else{
         alert('Anda Harus memilih PPN sebelum menambah PPH');
         $(obj).removeAttr('checked');          
    }     
    hitungSemua();
}               

function submitObat()
{
    idObat = $('#idObatAlkes').val();
    qtyObat = parseFloat($('#qtyObat').val()); 
    idSupplier = $('#GFPermintaanPembelianT_supplier_id').val();
    if(idObat==''){
        alert('Silahkan Pilih Obat Terlebih Dahulu');
    }else{
        obat = $("#tableObatAlkes tbody").find(".obatAlkes[value="+idObat+"]");
        jumlah =  obat.length;
        if (jumlah == 0){
            $.post("${urlGetObatSupplier}", { idObat: idObat, qtyObat : qtyObat, idSupplier:idSupplier },
            function(data){
                $('#tableObatAlkes').append(data.tr);
                $('#${obatAlkes}').val('');
                $('#idObatAlkes').val('');
                $("#tableObatAlkes tr:last").find(".numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":".","thousands":"","precision":0,"symbol":null})
                $("#tableObatAlkes tr:last").find(".float").maskMoney({"defaultZero":true,"allowZero":true,"decimal":".","thousands":"","precision":2,"symbol":null})
                hitungUrutan();
                //hitungPajak();
                //hitungJumlahHargaDiskon();
                $('#qtyObat').val('1'); 
                //hitungNettoKK();
                //hitungSemua();
                
            }, "json");
        }else{
            value = parseFloat(obat.parents("tr").find(".qty").val());
            value = parseFloat(value+qtyObat)
            obat.parents("tr").find(".qty").val(value);
            //hitungJumlahHargaDiskon();
            $('#${obatAlkes}').val('');
            $('#idObatAlkes').val('');
            $('#qtyObat').val('1'); 
            //hitungSemua();
        }
    }
    setTimeout(function(){hitungTotalSemua();},2000);
}

function hitungPajak(){
    netto = parseFloat($("#tableObatAlkes tr:last").find(".netto").val());
    if ($("#termasukPPN").is(":checked")){
        ppn = (${persenPPN}/100)*netto;
        $("#tableObatAlkes tr:last").find(".ppn").val(ppn);
        if ($("#termasukPPH").is(":checked")){
            pph = (${persenPPH}/100)*(netto+ppn);
            $("#tableObatAlkes tr:last").find(".pph").val(pph);
        }
    }
}

function submitRencana()
{
     idPerencanaan = $('#${idRencana}').val();
        if(idPerencanaan==''){
            alert('Silahkan Pilih Rencana Terlebih Dahulu');
        }else{
            $.post("${urlGetObatAlkesDariRencana}", { idPerencanaan: idPerencanaan },
            function(data){
                $('#tableObatAlkes').append(data.tr);
//                $('#${noPerencanaan}').val('');
                $('#idPerencanaan').val('');
                $('#buttonRencaKebutuhan').attr('disabled','TRUE');
                $('#${noPerencanaan}').attr('readonly','TRUE');
                hitungJumlahHargaDiskon();
            }, "json");
        }   
}

function submitRencanaDariInformasi(idPerencanaanF,noPerencanaanF,tglPerencanaanF)
{
     idPerencanaan = idPerencanaanF;
        if(idPerencanaan==''){
            alert('Silahkan Pilih Rencana Terlebih Dahulu');
        }else{
            $.post("${urlGetObatAlkesDariRencana}", { idPerencanaan: idPerencanaan },
            function(data){
                $('#tableObatAlkes').append(data.tr);
//                $('#${noPerencanaan}').val('');
                $('#idPerencanaan').val('');
                $('#buttonRencaKebutuhan').attr('disabled','TRUE');
                $('#${noPerencanaan}').attr('readonly','TRUE');
                hitungJumlahHargaDiskon();
                
                $('#checkBoxRencana').attr('checked','checked');
                $('#tambahObat').attr('disabled','TRUE');
                $('#checkBoxPermintaanPenawaran').removeAttr('checked');
                $('#GFPermintaanPenawaranT_nosuratpenawaran').attr('disabled','true');
                $('#buttonpermintaanPenawaran').attr('disabled','true');

                $('#buttonObatAlkes').attr('disabled','true');
                $('#GFPermintaanPembelianT_obatAlkes').attr('readonly','true');

                $('#${tglRencana}').val(tglPerencanaanF);
                $('#${noPerencanaan}').val(noPerencanaanF);

                $('#${noPenawaran}').val('');
                $('#${tglPenawaran}').val('');
                $('#tambahObat').attr('disabled','TRUE');
                $('#checkBoxRencana').attr('readonly',false);
                $('#formrencanakebutuhan').show();
                $('#formpemilihanobat').hide();
                $('#formpenawaran').hide();
            }, "json");
        }   
}                

function submitPermintaan()
{
     idPermintaan = $('#${idPermintaan}').val();
        if(idPermintaan==''){
            alert('Silahkan Pilih Permintaan Terlebih Dahulu');
        }else{
            
            $.post("${urlGetObatAlkesDariPermintaan}", { idPermintaan: idPermintaan },
            function(data){
                $('#tableObatAlkes').append(data.tr);
                $('#${noPerencanaan}').val('');
                $('#idPerencanaan').val('');
                $('#buttonpermintaanPenawaran').attr('disabled','TRUE');
                $('#${noPenawaran}').attr('readonly','TRUE');
                hitungJumlahHargaDiskon();
            }, "json");
        }   
}
                
function hitungNettoKK(){
    nettoTotal = 0;
    $(".netto").each(function(){
        nettoTotal += parseFloat($(this).val());
    });
    $("#totalSebelumDiskon").val(nettoTotal);
}

function submitPermintaanDariInformasi(idPermintaanF,noPermintaanF,tglPermintaanF)
{
     idPermintaan = idPermintaanF;
        if(idPermintaan==''){
            alert('Silahkan Pilih Permintaan Terlebih Dahulu');
        }else{
            
            $.post("${urlGetObatAlkesDariPermintaan}", { idPermintaan: idPermintaan },
            function(data){
                $('#tableObatAlkes').append(data.tr);
                $('#${noPerencanaan}').val('');
                $('#idPerencanaan').val('');
                $('#buttonpermintaanPenawaran').attr('disabled','TRUE');
                $('#${noPenawaran}').attr('readonly','TRUE');
                hitungJumlahHargaDiskon();
                
                $('#checkBoxPermintaanPenawaran').attr('checked','checked');              

                $('#checkBoxRencana').removeAttr('checked');
                $('#GFRencanaKebFarmasiT_noperencnaan').attr('disabled','true');
                $('#buttonRencaKebutuhan').attr('disabled','true');

                $('#buttonObatAlkes').attr('disabled','true');
                $('#GFPermintaanPembelianT_obatAlkes').attr('readonly','true');
                $('#${tglRencana}').val('');
                $('#${noPerencanaan}').val('');
                $('#${noPenawaran}').val(noPermintaanF);
                $('#${tglPenawaran}').val(tglPermintaanF);
                $('#checkBoxPermintaanPenawaran').attr('readonly',false);
                $('#formpenawaran').show();
                $('#formpemilihanobat').hide();
                $('#formrencanakebutuhan').hide();
                    
                $('#tambahObat').attr('disabled','TRUE');
            }, "json");
        }   
}

function hitungJumlahHargaDiskon()
{
    besarDiskon =$('#diskon').val();
    if (jQuery.isNumeric(besarDiskon)){
        besarDiskonINT=parseFloat(besarDiskon);
        banyakNetto =$('.netto').length;
        totalNetto=0;
        $('.netto').each(function() {
               jumlahKemasan = parseFloat($(this).parents("tr").find('input[name$="[jmlkemasan]"]').val());
               hargaNettoProduk=parseFloat($(this).val());
               totalNetto=totalNetto+hargaNettoProduk;
               qty = parseFloat($(this).parent().prev().find('input').val() * jumlahKemasan);
               $(this).parent().next().next().next().next().find('input').val(besarDiskonINT);
               persenDiskonObat=$(this).parent().next().next().next().next().find('input').val();
               jumlahDiskonObat=(hargaNettoProduk*(persenDiskonObat/100)) * qty;
               $(this).parent().next().next().next().next().next().find('input').val(jumlahDiskonObat);
            });
        jumlahDiskonHarga = (totalNetto *(besarDiskonINT/100));
        jumlahDiskonHarga = 0;
        $('#jumlahDiskon').val(jumlahDiskonHarga);
    }
    //hitungSemua();
    

}

function hitungDiskonDariJumlah(obj)
{
   jumlahKemasan = parseFloat($(obj).parents("tr").find('input[name$="[jmlkemasan]"]').val());
   jumlahDiskon = parseFloat(obj.value);
   hargaNettoProduk = parseFloat($(obj).parents("tr").find('.netto').val());
   qty = parseFloat($(obj).parents("tr").find('.qty').val() * jumlahKemasan);
   hargaBeli = hargaNettoProduk * qty;
   hargappn = parseFloat($(obj).parents("tr").find('.hargappnper').val());
   hargapph = hargappn = parseFloat($(obj).parents("tr").find('.hargapphper').val());
   
   if(jumlahDiskon>=hargaBeli){
        $(obj).val(0);
        $(obj).parents("tr").find('.persen').val(0);
        if (jQuery.isNumeric(hargaBeli)){
             $(obj).parents("tr").find('.subTotal').val(hargaBeli); 
        }
        alert('Jumlah Diskon Tidak Boleh Melebihi Neto * qty');
        return false;
   }else{ 
       persenDiskon = ((jumlahDiskon * 100)/hargaNettoProduk) / qty;
       if (jQuery.isNumeric(persenDiskon)){
            $(obj).parents("tr").find('.persen').val(persenDiskon);
            
       }
       
       subTotal = (hargaNettoProduk*qty) - jumlahDiskon + (hargappn + hargapph);
       if (jQuery.isNumeric(subTotal)){
            $(obj).parents("tr").find('.subTotal').val(subTotal); 
       }
   }
  
   //hitungSemua();
}

function hitungJumlahDariQty(obj)
{
   jumlahKemasan = parseFloat($(obj).parents("tr").find('input[name$="[jmlkemasan]"]').val());
   qty = parseFloat(obj.value * jumlahKemasan); 
   maximum = parseFloat($(obj).attr("maximum")); 
   parent = $("#tableObatAlkes").attr("parent-informasi"); 
   if (jQuery.isNumeric(maximum)){
       if (qty > maximum){
            $(obj).val(maximum);
            //hitungSemua();
            alert("Jumlah Pembelian tidak boleh lebih dari jumlah "+parent);
            return false;
       }
   }
   
   persenDiskon = parseFloat($(obj).parents("tr").find('.persen').val());
   
   hargaNettoProduk = parseFloat($(obj).parents("tr").find('.netto').val());
   jumlahDiskon = (qty * hargaNettoProduk) * (persenDiskon / 100);
   if (jQuery.isNumeric(jumlahDiskon)){
      $(obj).parents("tr").find('.jumlah').val(jumlahDiskon); 
   }
   
   subTotal = (hargaNettoProduk*qty) - jumlahDiskon;
   
   if (jQuery.isNumeric(subTotal)){
        $(obj).parents("tr").find('.subTotal').val(subTotal); 
   }
   
   //hitungSemua();
}

function hitungJumlahDariDiskon(obj)
{
   jumlahKemasan = parseFloat($(obj).parents("tr").find('input[name$="[jmlkemasan]"]').val());
   persenDiskon = parseFloat(obj.value * jumlahKemasan);
   qty = parseFloat($(obj).parents("tr").find('.qty').val() * jumlahKemasan);
   hargaNettoProduk = parseFloat($(obj).parents("tr").find('.netto').val());
   
   jumlahDiskon= (qty * hargaNettoProduk) * (persenDiskon / 100);
   if (jQuery.isNumeric(jumlahDiskon)){
        $(obj).parents("tr").find('.jumlah').val(jumlahDiskon);
   }
   
   subTotal = (hargaNettoProduk*qty) - jumlahDiskon;
   if (jQuery.isNumeric(subTotal)){
        $(obj).parents("tr").find('.subTotal').val(subTotal); 
   }
   //hitungSemua();
  
   
}

function hitungSubTotal(){
    subTotal = 0;
    $(".subTotal").each(function(){
        subTotal += parseFloat($(this).val());
    });
}

function hitungSemua() //untuk Meghitung Total Diskon dan Total Jumlah Diskon dari inputan yang ada di tabel
{
    hargaNettoQty=0;
    jumlahDiskonSemua=0;
    totalPPNSemua = 0;
    totalPPHSemua = 0;
    $('.netto').each(function() {
          jumlahKemasan = parseFloat($(this).parents("tr").find(".jmlkemasan").val());
          hargaNetto = parseFloat($(this).val());
          qty = parseFloat($(this).parents("tr").find('.qty').val());
          jumlahDiskon = parseFloat($(this).parents("tr").find(".jumlah").val());
          ppn = parseFloat($(this).parents("tr").find(".ppn").val());
          pph = parseFloat($(this).parents("tr").find(".pph").val());
          subTotal = (qty*(hargaNetto*jumlahKemasan+ppn+pph));
          subTotalKurangDiskon = subTotal - jumlahDiskon;
          totalPPNSemua += (qty*ppn);
          totalPPHSemua += (qty*pph);
          hargaNettoQty += subTotal;
          jumlahDiskonSemua += jumlahDiskon;
          if (jQuery.isNumeric(subTotalKurangDiskon)){
                $(this).parents("tr").find(".subTotal").val(subTotalKurangDiskon);
          }
    });
    hargaSetelahDiskon = hargaNettoQty - jumlahDiskonSemua;
    
    if (jQuery.isNumeric(jumlahDiskonSemua)){
        $("#jumlahDiskon").val(jumlahDiskonSemua);
    }
    if ($("#termasukPPN").is(":checked")){
        if (jQuery.isNumeric(totalPPNSemua)){
            $("#totalPPN").val(totalPPNSemua);
        }
    }
    if ($("#termasukPPH").is(":checked")){
        if (jQuery.isNumeric(totalPPHSemua)){
            $("#totalPPH").val(totalPPHSemua);
        }
    }
    if (jQuery.isNumeric(hargaNettoQty)){
        $("#totalSebelumDiskon").val(hargaNettoQty);
    }
    if (jQuery.isNumeric(hargaSetelahDiskon)){
        $("#totalSetelahDiskon").val(hargaSetelahDiskon);
    }
    
    
   
}

function removeRow(obj) {
    $(obj).parents('tr').remove();
    hitungUrutan();
    //hitungSemua();
}

function hitungUrutan(){
    noUrut = 1;
    $("#tableObatAlkes").find(".noUrut").each(function(){
        $(this).val(noUrut);
        noUrut++;
    });
}
JS;
Yii::app()->clientScript->registerScript('masukanobat',$jscript, CClientScript::POS_HEAD);
?>

<?php 
//========= Dialog buat cari data ObatalkesM=========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatalkesM',
    'options'=>array(
        'title'=>'Pencarian Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatalkesM('search');
$modObatAlkes->unsetAttributes();
if(isset($_GET['ObatalkesM'])) {
    $modObatAlkes->attributes = $_GET['ObatalkesM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'ObatalkesM-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
        'dataProvider'=>$modObatAlkes->search(),
        'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectObat",
                                    "onClick" => "
                                                  $(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                  $(\"#'.CHtml::activeId($modPermintaanPembelian,'obatAlkes').'\").val(\"$data->obatalkes_nama\");
                                                  $(\"#dialogObatalkesM\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes_kategori',
                ),
                array(
                    'header'=>'Golongan Obat',
                    'value'=>'$data->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_kode'),
                    'value'=>'$data->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes_nama',
                ),
                array(
                    'header'=>'Asal Barang',
                    'value'=>'$data->sumberdana->sumberdana_nama',
                ),
                array(
                    'header'=>'Kadar Obat',
                    'value'=>'$data->obatalkes_kadarobat',
                ),
                array(
                    'header'=>'Kemasan Besar',
                    'value'=>'$data->kemasanbesar',
                ),
                array(
                    'header'=>'Kekuatan',
                    'value'=>'$data->kekuatan',
                ),
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->tglkadaluarsa',
                ),
            ),
        'afterAjaxUpdate' => 'function(id, data){
        jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
$this->endWidget();
//========= end ObatalkesM dialog =============================
?>

<?php 
//========= Dialog buat cari data ObatsupplierM =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatsupplierM',
    'options'=>array(
        'title'=>'Pencarian Obat Supplier',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatsupplierM('search');
$modObatAlkes->unsetAttributes();
//$modObatAlkes->supplier_id = $var['supplier_id']; << FATAL ERROR
$modObatAlkes->obatalkes->supplier_id = $var['supplier_id'];
if(isset($_GET['ObatsupplierM'])) {
    $modObatAlkes->attributes = $_GET['ObatsupplierM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'obatSupplier-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
        'dataProvider'=>$modObatAlkes->searchObatSupplierGF(),
        'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "id" => "selectPasien",
                                    "href"=>"",
                                    "onClick" => "$(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                  $(\"#'.CHtml::activeId($modPermintaanPembelian,'obatAlkes').'\").val(\"".$data->obatalkes->obatalkes_nama."\");
//                                                         submitObat();
                                                  $(\"#dialogObatsupplierM\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'Supplier',
                    'filter'=>CHtml::activeHiddenField($modObatAlkes, 'supplier_id', array("id"=>'idSupplier2')),
                    'value'=>'$data->supplier->supplier_nama',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes->obatalkes_kategori',
                ),
                array(
                    'header'=>'Golongan Obat',
                    'value'=>'$data->obatalkes->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_kodeobat'),
                    'value'=>'$data->obatalkes->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes->obatalkes_nama',
                ),
                array(
                    'header'=>'Asal Barang',
                    'value'=>'$data->obatalkes->sumberdana->sumberdana_nama',
                ),
                array(
                    'header'=>'Kadar Obat',
                    'value'=>'$data->obatalkes->obatalkes_kadarobat',
                ),
                array(
                    'header'=>'Kemasan Besar',
                    'value'=>'$data->obatalkes->kemasanbesar',
                ),
                array(
                    'header'=>'Kekuatan',
                    'value'=>'$data->obatalkes->kekuatan',
                ),
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->obatalkes->tglkadaluarsa',
                ),
  ),
        'afterAjaxUpdate' => 'function(id, data){
        $("#ObatsupplierM_supplier_id").val($("#idSupplier").val());
        jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
$this->endWidget();
//========= end ObatsupplierM dialog =============================
?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.float',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>2,
    )
));
?>
<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>
<script>
//Created by: ichan90@yahoo.co.id
function hitungHargaNetto(obj){
     var jmlKemasan = unformatNumber($(obj).parent().parent().find("input[name$='[jmlkemasan]']").val());
     var hargaKemasan = unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
     var hargaNetto = hargaKemasan / jmlKemasan;
     $(obj).parent().parent().find("input[name$='[harganettoper]']").val(hargaNetto.toFixed(2));
 }

//    Perhitungan subtotal efektif bisa digunakan di field mana saja selama satu baris / <tr> 
function hitungSubtotal(obj){
    var jmlPermintaan = unformatNumber($(obj).parent().parent().find("input[name$='[jmlpermintaan]']").val());
    var jmlKemasan =  unformatNumber($(obj).parent().parent().find("input[name$='[jmlkemasan]']").val());
    var hargaBeliBesar =  unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
    var persenDiskon =  unformatNumber($(obj).parent().parent().find("input[name$='[persendiscount]']").val());
    var subtotal = (jmlPermintaan * hargaBeliBesar) - (jmlPermintaan * hargaBeliBesar * persenDiskon / 100);
    $(obj).parent().parent().find("#subTotal").removeAttr('value');
    $(obj).parent().parent().find("#subTotal").val(subtotal.toFixed(0));
    //hitung jml diskon 
    var jmlDiskon = (hargaBeliBesar * persenDiskon / 100) * jmlPermintaan;
    $(obj).parent().parent().find('input[name$="[jmldiscount]"]').val(jmlDiskon);
    hitungTotalSemua();
 }
 function cekSupplier(){
     var idSupplier = $("#GFPermintaanPembelianT_supplier_id").val();
     if(!idSupplier){
         alert ('Silahkan Isi Supplier Terlebih Dahulu !');
         return false;
     }else{
         return true;
     }
     
 }
 function hitungJmlDiskon(obj){ //dari persen diskon
     var hargaBeliBesar =  unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
     var jmlPermintaan = unformatNumber($(obj).parent().parent().find("input[name$='[jmlpermintaan]']").val());
     var persenDiskon = parseFloat($(obj).val());
     var jmlDiskon = (hargaBeliBesar * persenDiskon / 100) * jmlPermintaan;
     if(jmlDiskon <= 0){
         jmlDiskon = 0;
     }
     if(hargaBeliBesar <=0  && persenDiskon >0 ){
         $(obj).parent().parent().find('input[name$="[persendiscount]"]').val(0);
     }
     if(persenDiskon > 100){
         alert('Maaf persen diskon tidak boleh lebih dari 100 %');
         jmlDiskon = 0;
         $(obj).parent().parent().find('input[name$="[persendiscount]"]').val(0);
     }
     $(obj).parent().parent().find('input[name$="[jmldiscount]"]').val(jmlDiskon.toFixed(2));
     hitungTotalSemua();
 }
 function hitungPersenDiskon(obj){ //dari diskon total
     var jmlPermintaan = unformatNumber($(obj).parent().parent().find("input[name$='[jmlpermintaan]']").val());
     var hargaBeliBesar =  unformatNumber($(obj).parent().parent().find("input[name$='[hargabelibesar]']").val());
     var jmlDiskon = parseFloat($(obj).val());
     if(jmlDiskon <= 0){
         jmlDiskon = 0;
     }
     
     if(hargaBeliBesar <= 0){
         hargaBeliBesar = 0;
     }
     if(jmlPermintaan <= 0){
         jmlPermintaan = 0;
     }
     
     if(jmlDiskon > hargaBeliBesar){
         alert('Maaf jumlah diskon tidak boleh melebihi Harga Kemasan');
         var persenDiskon = 0;
         $(obj).parent().parent().find('input[name$="[persendiscount]"]').val(0);
         $(obj).parent().parent().find('input[name$="[jmldiscount]"]').val(0);
     }else{
         //hitung diskon
        var persenDiskon = ((jmlDiskon * 100) / (hargaBeliBesar * jmlPermintaan));     
        if(persenDiskon <= 0 || !jQuery.isNumeric(persenDiskon)){
            persenDiskon = 0;
        }
        $(obj).parent().parent().find('input[name$="[persendiscount]"]').val(persenDiskon.toFixed(2));
     }
               
     //hitung subtotal
     var subtotal = (jmlPermintaan * hargaBeliBesar) - (jmlPermintaan * hargaBeliBesar * persenDiskon / 100)
     if(subtotal <= 0){
         subtotal = 0;
     }
     $(obj).parent().parent().find("#subTotal").val(subtotal.toFixed(0));
     hitungTotalSemua();
 }
 function hitungTotalSemua(){
     var total = 0;
     var totalDiskon = 0;
     $('#tableObatAlkes tbody tr').each(function(){
        total += unformatNumber($(this).find('#subTotal').val()); 
        totalDiskon += unformatNumber($(this).find('input[name$="[jmldiscount]"]').val());
     });
     $('#totalDiskon').val(totalDiskon);
     $('#total').val(total);
 }
 hitungTotalSemua();
</script>