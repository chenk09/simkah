<table width="100%">
    <tr>
        <td width="50%">
             <b><?php echo CHtml::encode($modPermintaanPembelian->getAttributeLabel('nopermintaan')); ?>:</b>
            <?php echo CHtml::encode($modPermintaanPembelian->nopermintaan); ?>
            <br />
             <b><?php echo CHtml::encode($modPermintaanPembelian->getAttributeLabel('tglpermintaanpembelian')); ?>:</b>
            <?php echo CHtml::encode($modPermintaanPembelian->tglpermintaanpembelian); ?>
            <br />
        </td>
        <td>
             <b><?php echo CHtml::encode($modPermintaanPembelian->getAttributeLabel('supplier_id')); ?>:</b>
            <?php echo CHtml::encode($modPermintaanPembelian->supplier->supplier_nama); ?>
            <br />
             <b><?php echo CHtml::encode($modPermintaanPembelian->getAttributeLabel('syaratbayar_id')); ?>:</b>
            <?php echo CHtml::encode((isset($modPermintaanPembelian->syaratbayar)) ? $modPermintaanPembelian->syaratbayar->syaratbayar_nama : ' - '); ?>
            <br />
        </td>
    </tr>   
</table>
<hr/>
<table id="tableObatAlkes" class="table table-bordered table-condensed middle-center">
    <thead>
    <tr>
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <!--<th>Satuan Kecil/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Satuan Besar</th>-->
        <th>Jumlah Kemasan</th>
        <th>Jumlah Pembelian</th>
        <th>Harga Kemasan</th>
        <th>Diskon (%)</th>
        <th>Diskon Total (Rp)</th>
        <th>PPN</th>
        <th>PPH</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $no=1;
    $persenDiskon = 0;
    $totalSubTotal = 0;
    if (count($modPermintaanPembelianDetails) > 0){
        $totalJumlahPermintaan = 0;
        $totalJumlahDiskon = 0;
        $totalNetto = 0;
        $totalPPN = 0;
        $totalPPH = 0;
        $totalHargaSatuan = 0;
        $persenDiskon = 0;
        foreach($modPermintaanPembelianDetails AS $tampilData):
            //$hargaBeliBesar = $tampilData['jmlpermintaan'] * $tampilData['jmlkemasan'] * ($tampilData['harganettoper'] + $tampilData['hargapphper']);
            $hargaBeliBesar = ($tampilData['jmlpermintaan'] * $tampilData['jmlkemasan']) * $tampilData['harganettoper'];
            //$subTotal = ($tampilData['jmlpermintaan']*($tampilData['hargabelibesar'] + $tampilData['hargappnper'] + $tampilData['hargapphper'])) - $tampilData['jmldiscount'];
            $subTotal = ($hargaBeliBesar + $tampilData['hargappnper'] + $tampilData['hargapphper']) - $tampilData['jmldiscount'];
            $totalSubTotal += $subTotal;

             echo "<tr>
                    <td>".$no."</td>
                    <td>".$tampilData->sumberdana['sumberdana_nama']."</td>  
                    <td>".$tampilData->obatalkes['obatalkes_kategori']." - ".$tampilData->obatalkes['obatalkes_nama']."</td>   
                    <td class='right-align'>".MyFunction::formatNumber($tampilData['jmlkemasan'])
                     ." ".$tampilData->obatalkes->satuankecil->satuankecil_nama."/".$tampilData->obatalkes->satuanbesar->satuanbesar_nama."</td>
                    <td class='right-align'>".MyFunction::formatNumber($tampilData['jmlpermintaan'])
                     ." ".$tampilData->obatalkes->satuanbesar->satuanbesar_nama."</td>
                    <td class='right-align'>Rp. ".MyFunction::formatNumber($hargaBeliBesar)."</td>           
                    <td class='right-align'>".MyFunction::formatNumber($tampilData['persendiscount'],2,'.',',')."</td>     
                    <td class='right-align'>Rp. ".MyFunction::formatNumber($tampilData['jmldiscount'])."</td>     
                    <td class='right-align'>Rp. ".MyFunction::formatNumber($tampilData['hargappnper'])."</td>     
                    <td class='right-align'>Rp. ".MyFunction::formatNumber($tampilData['hargapphper'])."</td>     
                    <td class='right-align'>Rp. ".MyFunction::formatNumber($subTotal)."</td>     
                 </tr>";  
             //                    <td>".$tampilData->satuankecil['satuankecil_nama']."<br>".$tampilData->satuanbesar['satuanbesar_nama']."</td>   
            $no++;
            $totalJumlahPermintaan+=$tampilData['jmlpermintaan'];
            $totalJumlahDiskon+=$tampilData['jmldiscount'];
            $totalNetto+=$tampilData['harganettoper'];
//            $totalPPN+=$tampilData['hargappnper'];
//            $totalPPH+=$tampilData['hargapphper'];
            $totalHargaSatuan +=$tampilData['hargasatuanper'];
            $persenDiskon += $tampilData['persendiscount'];
            endforeach;
    }
    else{
        echo '<tr><td colspan=9>'.Yii::t('zii','No results found.').'</td></tr>';
    }
//    echo "</tbody><tfoot><tr>
//            <td colspan='4'> Total</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalJumlahPermintaan)."</td>            
//            <td class='right-align'>".MyFunction::formatNumber($totalNetto)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalPPN)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalPPH)."</td>
//            <td class='right-align'></td>
//            <td class='right-align'>".MyFunction::formatNumber($totalJumlahDiskon)."</td>
//            <td class='right-align'>".MyFunction::formatNumber($totalSubTotal)."</td>
//         </tr></tfoot>";
     echo "</tbody><tfoot><tr>
         <td colspan='10'> Total</td>
            <td class='right-align'>Rp. ".MyFunction::formatNumber($totalSubTotal)."</td>
         </tr></tfoot>"; 
//     
//            <td class='right-align'>".MyFunction::formatNumber($totalJumlahPermintaan)."</td>            
//            <td class='right-align'>Rp. ".MyFunction::formatNumber($totalNetto)."</td>
//            <td class='right-align'></td>
//            <td class='right-align'>Rp. ".MyFunction::formatNumber($totalJumlahDiskon)."</td>
    ?>
</table>

<?php
if (!isset($caraPrint)){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idPembelian'=>$modPermintaanPembelian->permintaanpembelian_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}
?>    