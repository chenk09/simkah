
<?php

class TarifTindakanMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $_lastDaftarTindakanId = null;
        public $_lastDaftarTindakanId2 = null;
        public $_lastTindakanTarifId = null;
        public $_lastKelasPelayanan_id = null;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('tindakan','admin','delete','RemoveTemporary','AjaxGetDaftarTindakan','DeleteKomponenTarif',
                                                 'gridJenisTarif','gridDaftarTindakan','gridKategoriTindakan','loadTarifPerdaV','gridKomponenTarif','kategori'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $model = $this->loadTarifPerdaV($id);
                $modKomponenTarif = TariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$model->daftartindakan_id,'jenistarif_id'=>$model->jenistarif_id,'kelaspelayanan_id'=>$model->kelaspelayanan_id),array('order'=>'komponentarif_id'));
		$this->render('view',array(
                                        'model'=>$model,
                                        'modKomponenTarif'=>$modKomponenTarif
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SATarifTindakanM;
                $modkomp=new KomponentarifM('searchGrid');

                $var['daftartindakan_id'] = '';
                $var['daftartindakan'] = '';
                $var['kategoritindakan'] = '';
                $var['kategoritindakan_id'] = '';
                
                if(isset($_POST['SATarifTindakanM']))
                {
                    $var['kategoritindakan_id'] = $_POST['kategoritindakan_id'];
                    $var['kategoritindakan'] = $_POST['kategoritindakan'];
                    $var['daftartindakan'] = $_POST['daftartindakan'];
                    $var['daftartindakan_id'] = $_POST['daftartindakan_id'][0];
                    
                    if(!empty($_POST['komponen'])){
                        $model->attributes=$_POST['SATarifTindakanM'];
                        $jmlKomponen = $_POST['komponen'];
//                        echo count($jmlKomponen);exit;
                        $jmlTarif = $_POST['tarif'];
                         foreach($jmlKomponen as $i=>$komponens){
//                              echo "<pre>";
//                              print_r($_POST);
//                             foreach($jmlTarif[$i] as $k=>$tarif){
//                                print_r($komponens);
                               
                                    if($komponens != Params::KOMPONENTARIF_ID_TOTAL) //kondisi apabila komponentarif_id bukan tarif total
                                   { 
                                        $model = new SATarifTindakanM;
                                        $model->jenistarif_id = $_POST['SATarifTindakanM']['jenistarif_id'];
                                        $model->kelaspelayanan_id = $_POST['SATarifTindakanM']['kelaspelayanan_id'];
                                        $model->persendiskon_tind = $_POST['SATarifTindakanM']['persendiskon_tind'];
                                        $model->persencyto_tind = $_POST['SATarifTindakanM']['persencyto_tind'];
                                        $model->hargadiskon_tind = $_POST['SATarifTindakanM']['hargadiskon_tind'];
                                        $model->perdatarif_id =  Params::DEFAULT_PERDA_TARIF;
                                        $model->daftartindakan_id = $_POST['daftartindakan_id'][1];
                                        $model->komponentarif_id = $komponens;
                                        $model->harga_tariftindakan = $_POST['tarif'][$i];
                                        if($model->validate()){
                                            $model->save();
                                            $status="success";
                                        }
                                        else
                                        {
                                            Yii::app()->user->setFlash('error', '<strong>Gagal!!</strong> Data tidak valid.');
                                        } 

                                    }
                                    else //kondisi apabila komponentarif_id merupakan tarif total
                                    {
    //                                    
                                        $count = TariftindakanM::model()->findTarifTindakan('',$_POST['SATarifTindakanM']['kelaspelayanan_id'],$_POST['daftartindakan_id'][1], Params::KOMPONENTARIF_ID_TOTAL,'BY');
                                        if(count($count) > 0){ //kondisi apabila komponen tarif total ternyata sudah ada di tabel tariftindakan_m berdasarkan daftartindakan_id, maka tarif total akan diupdate
                                            $komponenTarifTotal =  Params::KOMPONENTARIF_ID_TOTAL;
                                            $daftartindakan_id = $_POST['daftartindakan_id'][1];
                                            $kelaspelayanan_id = $_POST['SATarifTindakanM']['kelaspelayanan_id'];
                                            $hargaKomponen = $_POST['totalHarga'][$i] + $count->harga_tariftindakan;
                                            $sql = "UPDATE tariftindakan_m 
                                                        SET harga_tariftindakan = $hargaKomponen 
                                                        WHERE daftartindakan_id = $daftartindakan_id and komponentarif_id = $komponenTarifTotal and kelaspelayanan_id = $kelaspelayanan_id";
                                            Yii::app()->db->createCommand($sql)->queryAll();
                                            $this->redirect(array('admin','id'=>$model->tariftindakan_id));
                                        }
                                        else //kondisi apabila komponen tarif total tidak ada di tabel tariftindakan_m berdasarkan daftartindakan_id, maka tarif total akan diinsert
                                        {
                                            $model = new SATarifTindakanM;
                                            $model->jenistarif_id = $_POST['SATarifTindakanM']['jenistarif_id'];
                                            $model->kelaspelayanan_id = $_POST['SATarifTindakanM']['kelaspelayanan_id'];
                                            $model->persendiskon_tind = $_POST['SATarifTindakanM']['persendiskon_tind'];
                                            $model->persencyto_tind = $_POST['SATarifTindakanM']['persencyto_tind'];
                                            $model->hargadiskon_tind = $_POST['SATarifTindakanM']['hargadiskon_tind'];
                                            $model->perdatarif_id = Params::DEFAULT_PERDA_TARIF;
                                            $model->daftartindakan_id = $_POST['daftartindakan_id'][1];
                                            $model->komponentarif_id = $komponens;
                                            $model->harga_tariftindakan = $_POST['totalHarga'][$i];
                                            if($model->validate())
                                            {
                                                $model->save();    
                                                $status = "success";
                                            }
                                            else
                                            {
                                                Yii::app()->user->setFlash('error', '<strong>Gagal!!</strong> Data tidak valid2.');
                                            } 
                                        }
                                    }
//                             }
                        }
//                        exit;
                        if($status == "success"){
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('admin','id'=>$model->tariftindakan_id));
                        }
                    }else{
                        Yii::app()->user->setFlash('error', '<strong>Gagal!!</strong> Data tidak valid.');
                        $this->redirect(array('create'));
                    }   
                }

		$this->render('create',array(
			'model'=>$model,'modkomp'=>$modkomp, 'var'=>$var,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $model = $this->loadModel($id);
                $modDaftarTindakan = TariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$model->daftartindakan_id,'jenistarif_id'=>$model->jenistarif_id,'kelaspelayanan_id'=>$model->kelaspelayanan_id),array('order'=>'komponentarif_id'));
                       
                if(isset($_POST['tariftind'])){
                    $tarifTind = $_POST['tariftind'];
                    
                    $jenisTarif = $_POST['jenistarif_id'];
                    $kelaspelayanan_id = $_POST['kelaspelayanan_id'];
                    $persenDiskon = $_POST['persendiskon_tind'];
                    $hargaDiskon = $_POST['hargadiskon_tind'];
                    $persenCyto = $_POST['persencyto_tind'];
                    $daftartindakanId = $_POST['daftartindakan_id'];
                    if(isset($_POST['new'])){
                        $newKomp = $_POST['new'];
                        for($i=0;$i<count($newKomp['komponenId']);$i++){
                            $newKompTarif = new TariftindakanM;
                            $newKompTarif->jenistarif_id = $jenisTarif;
                            $newKompTarif->daftartindakan_id = $daftartindakanId;
                            $newKompTarif->komponentarif_id = $newKomp['komponenId'][$i];
                            $newKompTarif->kelaspelayanan_id = $kelaspelayanan_id;
                            $newKompTarif->perdatarif_id = Params::DEFAULT_PERDA_TARIF;
                            $newKompTarif->harga_tariftindakan = $newKomp['hargatariftindakan'][$i];
                            $newKompTarif->hargadiskon_tind = $hargaDiskon;
                            $newKompTarif->persendiskon_tind = $persenDiskon;
                            $newKompTarif->persencyto_tind = $persenCyto;
                            $newKompTarif->save();
                        }
                    }
                    
                    $max = count($tarifTind) - 1;
                    $subTarif = 0;
                    for($i=0;$i<count($tarifTind);$i++)
                    {
                       if($i != $max)
                       {
                            TariftindakanM::model()->updateByPk($tarifTind[$i]['tariftindakanId'],
                                array(
                                    'harga_tariftindakan'=>$tarifTind[$i]['hargatariftindakan'],
                                    'persendiskon_tind'=>$persenDiskon,
                                    'hargadiskon_tind'=>$hargaDiskon,
                                    'persencyto_tind'=>$persenCyto,
                                    'kelaspelayanan_id'=>$kelaspelayanan_id,
                                    'jenistarif_id'=>$jenisTarif
                                )
                            );
                       }else{
                           $subTarif += $tarifTind[$i]['hargatariftindakan'];
                            TariftindakanM::model()->updateByPk($tarifTind[$i]['tariftindakanId'],
                                array(
                                    'harga_tariftindakan'=>$subTarif,
                                    'persendiskon_tind'=>$persenDiskon,
                                    'hargadiskon_tind'=>$hargaDiskon,
                                    'persencyto_tind'=>$persenCyto,
                                    'kelaspelayanan_id'=>$kelaspelayanan_id,
                                    'jenistarif_id'=>$jenisTarif
                                )
                            );
                       }
                       
                    }
                    /*
                    foreach ($tarifTind as $i=>$tarif){
                        TariftindakanM::model()->updateByPk($tarif['tariftindakanId'],
                            array(
                                'harga_tariftindakan'=>$tarif['hargatariftindakan'],
                                'persendiskon_tind'=>$persenDiskon,
                                'hargadiskon_tind'=>$hargaDiskon,
                                'persencyto_tind'=>$persenCyto,
                                'kelaspelayanan_id'=>$kelaspelayanan_id,
                                'jenistarif_id'=>$jenisTarif
                            )
                        );
                    }
                    */
                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                    $this->redirect(array('admin','modulId'=>Yii::app()->session['modulId']));
                } 

		$this->render('update',array(
			'modDaftarTindakan'=>$modDaftarTindakan,
                                                'model'=>$model,
		));
	}
        
        public function actionDeleteKomponenTarif()
        {
            if(Yii::app()->request->isAjaxRequest){
                $idTotal = $_POST['idTarifTotalTindakan'];
                $idTarif = $_POST['idTarifTindakan'];
                
                $tarifTotal = TariftindakanM::model()->findByPk($idTotal);
                $hapusTarif = TariftindakanM::model()->findByPk($idTarif);
                $tarifTotalBaru = $tarifTotal->harga_tariftindakan - $hapusTarif->harga_tariftindakan;
                
                TariftindakanM::model()->deleteByPk($idTarif);
                TariftindakanM::model()->updateByPk($idTotal, array('harga_tariftindakan'=>$tarifTotalBaru));
                
                $json['totaltarif'] = $tarifTotalBaru; 
                
                echo CJSON::encode($json); 
            }
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        $model = $this->loadModel($id);

                        $criteria = new CDbCriteria();
                        $criteria->compare('jenistarif_id', $model->jenistarif_id);
                        $criteria->compare('daftartindakan_id', $model->daftartindakan_id);
                        $criteria->compare('kelaspelayanan_id', $model->kelaspelayanan_id);
                        $criteria->compare('komponentarif_id',$model->komponentarif_id);
                        if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $hapus = TariftindakanM::model()->deleteAll($criteria);
                                                
                                                /*
                                                 * Validasi Apabila Ingin Mendelete Tarif Total
                                                 */
//                                                if($model->komponentarif_id == Params::KOMPONENTARIF_ID_TOTAL)
//                                                {
//                                                    throw new CHttpException(404,Yii::t('mds','You can not delete this data, because there are tariff measures relating to this data.'));
//                                                }
                                                /*
                                                 * Validasi Apabila Delete Tarif Tindakan Ternyata Tinggal dua baris lagi dan salah satunya tarif total dan komponen tarif biasa
                                                 */
//                                                $cariKomponen = TariftindakanM::model()->findTarifTindakan($model->jenistarif_id,$model->kelaspelayanan_id, $model->daftartindakan_id,'',"ALL");
//                                                if(count($cariKomponen) == 2)
//                                                {
//                                                    TariftindakanM::model()->deleteAll('jenistarif_id = :idJenistarif AND daftartindakan_id = :idTindakan  AND kelaspelayanan_id = :idKelasPelayanan AND komponentarif_id = :idKomponenTarif', 
//                                                                                   array(':idJenistarif'=>$model->jenistarif_id,
//                                                                                         ':idTindakan'=>$model->daftartindakan_id,
//                                                                                         ':idKelasPelayanan'=>$model->kelaspelayanan_id,
//                                                                                         ':idKomponenTarif'=>  Params::KOMPONENTARIF_ID_TOTAL));
//                                                }
//                                                $cariTotal = TariftindakanM::model()->findTarifTindakan($model->jenistarif_id,$model->kelaspelayanan_id, $model->daftartindakan_id, Params::KOMPONENTARIF_ID_TOTAL,"BY");
//                                                $komponenTarifTotal =  Params::KOMPONENTARIF_ID_TOTAL;
//                                                $totalTarif = (count($cariTotal > 0 ) ? $cariTotal->harga_tariftindakan - $model->harga_tariftindakan  : $model->harga_tariftindakan);
//                                                $sql = "UPDATE tariftindakan_m 
//                                                            SET harga_tariftindakan = $totalTarif
//                                                            WHERE daftartindakan_id = $model->daftartindakan_id  and komponentarif_id = $komponenTarifTotal";
//                                                
//                                                Yii::app()->db->createCommand($sql)->queryAll();
                                                
//			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SATarifTindakanM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new TariftindakanperdaV('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TariftindakanperdaV']))
			$model->attributes=$_GET['TariftindakanperdaV'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SATarifTindakanM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	public function loadTarifPerdaV($id)
	{
		$model=TariftindakanperdaV::model()->findByAttributes(array('tariftindakan_id'=>$id));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='satarif-tindakan-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
            /**
             *Mengubah status aktif
             * @param type $id 
             */
            public function actionRemoveTemporary($id)
            {
                    if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
    //                SATarifTindakanM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
    //                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }

            public function actionPrint()
            {
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $model= new TariftindakanperdaV;
                $model->unsetAttributes();
                    if (isset($_GET['TariftindakanperdaV'])) {
                              $model->attributes=$_GET['TariftindakanperdaV'];
                    }
                
                $judulLaporan='Data Tarif Tindakan';
                $caraPrint=$_REQUEST['caraPrint'];
                $perdaTarif = Params::DEFAULT_PERDA_TARIF;
                if($caraPrint=='PRINT') {
                    $this->layout='//layouts/printWindows';
                    $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
                else if($caraPrint=='EXCEL') {
                    $this->layout='//layouts/printExcel';
                    $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
                else if($_REQUEST['caraPrint']=='PDF') {
                    $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);  
                    $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                    $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'perdaTarif'=>$per
                        ),true));
                    $mpdf->Output();
                }                       
            }

            public function actionAjaxGetDaftarTindakan()
            {
                if(Yii::app()->request->isAjaxRequest){

                    $criteria = new CDbCriteria;
                    $criteria->select = array('daftartindakan_nama, daftartindakan_id, kelaspelayanan_id, kategoritindakan_id');
                    $criteria->compare('kategoritindakan_id', $_POST['kategoritindakan_id']);
                    $criteria->compare('daftartindakan_id', $_POST['daftartindakan_id']);
                    $criteria->order = 'daftartindakan_nama';

                    $datas = DaftartindakannontarifV::model()->findAll($criteria);
                    $kelas = KelaspelayananM::model()->findByPk($_POST['kelaspelayanan_id']);
                    $daftartindakan = DaftartindakanM::model()->findByPk($_POST['daftartindakan_id']);

                    $tarifTindakan = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$_POST['daftartindakan_id'], 'kelaspelayanan_id'=>$_POST['kelaspelayanan_id']));
                    $returnVal = array();
                    if (!((boolean)count($tarifTindakan))){
                        $inputHiddenKomponen = '<input type="hidden" size="4" name="komponen[1]" id="komponen_1" readonly="true" value="'.Params::KOMPONENTARIF_ID_TOTAL.'"  class="komponen"/>';
//                        $tr = '<table id="tblInputTarifTindakan"><th> Pilih Semua <br/>'.CHtml::checkBox('checkUncheck', true, array('onclick'=>'checkUncheckAll(this);')).'</th>
//                                           <th>Tindakan</th><th>'.$inputHiddenKomponen.'Tarif Total</th>';
                        $tr = '<table id="tblInputTarifTindakan"><thead><tr><th> Pilih Semua <br/>'.CHtml::checkBox('checkUncheck', true, array('onclick'=>'checkUncheckAll(this);')).'</th>
                                           <th>Tindakan</th><th>Komponen Tarif</th><th>Tarif</th><th>Hapus</th></tr></thead><tbody>';
                        foreach($datas as $data)
                        {
//                            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$data->daftartindakan_id));              
//                            if (count($modTarif) == 0){
                                $td = "<tr><td>";
                                $td .= CHtml::checkBox('daftartindakan_id[1]', true, array('value'=>$data->getAttribute('daftartindakan_id')));
                                $td .= '</td><td>'.$data->getAttribute('daftartindakan_nama');
                                $td .= '</td><td>';
                                $td .= '</td><td>';
                                $td .= "</td></tr>";
//                            }
                        }
                        $tdfoot = "</tbody><tfoot><tr class=trfooter><td>";
                        $tdfoot .= '</td><td>';
                        $tdfoot .= '</td><td>Total Tarif';
                        $tdfoot .= '</td><td>'.CHtml::textField('totalHarga[1]', '0', array('size'=>6,'class'=>'default'))
                                .CHtml::hiddenField('komponen[1]', Params::KOMPONENTARIF_ID_TOTAL, array('size'=>6,'class'=>'default','value'=>Params::KOMPONENTARIF_ID_TOTAL,'class'=>'komponen'));
                        $tdfoot .= "</td><td></td></tr></tfoot>";
                        $tr .= ((!empty($td)) ? $td : '');
                        $returnVal['table'] = $tr;
                        $returnVal['tfooter'] = $tdfoot;
                        $returnVal['status'] = 'Not Empty';
                    }
                     else{
                        $returnVal['status'] = 'Empty';
                        $returnVal['messege'] = "<div class='controls'><strong>Tindakan sudah memiliki tarif</strong></div>";
                    }
                    
                    
                    if(count($datas)>0){
                        echo CJSON::encode($returnVal);
                    }else{
                        $returnVal['status'] = 'Empty';
                        $returnVal['messege'] = "<div class='controls'><strong>Daftar Tindakan Tidak Ditemukan</strong></div>";
                        echo CJSON::encode($returnVal);
                    }
                    
                 } 
                 Yii::app()->end();
            }
            
            public function actionTindakan()
            {
                if(Yii::app()->request->isAjaxRequest) {
                    $returnVal = array();
                    $criteria = new CDbCriteria();
                    $criteria->select = array('daftartindakan_nama, daftartindakan_id,kategoritindakan_id');
                    $criteria->group = 'daftartindakan_nama, daftartindakan_id, kategoritindakan_id';
                    if(!empty($_GET['idKategori'])){
                        $idKategori = $_GET['idKategori'];
                        $criteria->compare('kategoritindakan_id',$idKategori);
                        $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['term']), true);
                    }else{
                        $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['term']), true);
                    }
                    $criteria->order = 'daftartindakan_nama';
                    
                    $models = DaftartindakannontarifV::model()->findAll($criteria);
                    foreach($models as $i=>$model)
                    {
                        $attributes = $model->attributeNames();
                        foreach($attributes as $j=>$attribute) {                            
                            $returnVal[$i]["$attribute"] = $model->$attribute;
                        }
                        $returnVal[$i]['label'] = $model->daftartindakan_nama;
                        $returnVal[$i]['value'] = $model->daftartindakan_nama;
                    }

                    echo CJSON::encode($returnVal);
                }
                Yii::app()->end();
            }
            
            public function actionKategori()
            {
                if(Yii::app()->request->isAjaxRequest) {
                    $returnVal = array();
                    $criteria = new CDbCriteria();
                    $criteria->select = array('kategoritindakan_id, kategoritindakan_nama');
                    $criteria->group = 'kategoritindakan_id, kategoritindakan_nama';
                    $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['term']), true);
                    $criteria->order = 'kategoritindakan_nama';
                    
                    $models = KategoritindakanM::model()->findAll($criteria);
                    foreach($models as $i=>$model)
                    {
                        $attributes = $model->attributeNames();
                        foreach($attributes as $j=>$attribute) {                            
                            $returnVal[$i]["$attribute"] = $model->$attribute;
                        }
                        $returnVal[$i]['label'] = $model->kategoritindakan_nama;
                        $returnVal[$i]['value'] = $model->kategoritindakan_nama;
                    }

                    echo CJSON::encode($returnVal);
                }
                Yii::app()->end();
            }
            
            protected function gridJenisTarif($data,$row)
            {
                if($this->_lastDaftarTindakanId != $data->daftartindakan_id)
                    return $data->jenistarif_nama;
                else
                    return;
            }
            
            protected function gridKelasPelayanan($data,$row)
            {
               if($this->_lastDaftarTindakanId != $data->daftartindakan_id)
                    return $data->kelaspelayanan_nama; 
               else 
                   return '';
            }
            
            protected function gridKategoriTindakan($data,$row)
            { 
                if($this->_lastDaftarTindakanId != $data->daftartindakan_id)
                {
                    return DaftartindakanM::model()->getKategoriTindakanNama($data->daftartindakan_id);
                }
                else
                     return '';
               
             }   
             
            protected function gridDaftarTindakan($data,$row)
            { 
                if($this->_lastDaftarTindakanId != $data->daftartindakan_id )
                {
                    $this->_lastDaftarTindakanId = $data->daftartindakan_id; //remember the last product id
                    $this->_lastKelasPelayanan_id = $data->kelaspelayanan_id; //remember the last product id
                    
                    return $data->daftartindakan_nama;
                }
                else
                     return '';
               
             }      
             
            protected function gridKomponenTarif($data,$row)
            { 
                if($this->_lastDaftarTindakanId2 == $data->daftartindakan_id && $this->_lastKelasPelayanan_id == $data->kelaspelayanan_id)
                {  
                    return $data->komponentarif_nama;
                }
                else
                     $this->_lastDaftarTindakanId2 = $data->daftartindakan_id; //remember the last product id
                     $this->_lastKelasPelayanan_id = $data->kelaspelayanan_id; //remember the last product id
                      return $data->komponentarif_nama;
             }                  
                           
}
