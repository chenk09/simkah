<?php
class AnamnesaRDController extends SBaseController
{
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	public function actionIndex()
	{
            $format = new CustomFormat();
            $modAnamnesa = new AnamnesaT;
            if(isset($_POST['AnamnesaT'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modAnamnesa->attributes=$_POST['AnamnesaT'];
                    
                    $modAnamnesa->pegawai_id= $_POST['AnamnesaT']['pegawai_id'];
                    $modAnamnesa->pendaftaran_id=$_POST['idPendaftaran'];
                    $modAnamnesa->pasien_id=$_POST['idPasien'];
                    $modAnamnesa->tglanamnesis=$format->formatDateTimeMediumForDB($_POST['AnamnesaT']['tglanamnesis']);
                    $modAnamnesa->keluhanutama = (count($_POST['AnamnesaT']['keluhanutama'])>0) ? implode(', ', $_POST['AnamnesaT']['keluhanutama']) : '';
                    $modAnamnesa->keluhantambahan = (count($_POST['AnamnesaT']['keluhantambahan'])>0) ? implode(', ', $_POST['AnamnesaT']['keluhantambahan']) : '';
                    $modAnamnesa->save();
                    $updateStatusPeriksa=PendaftaranT::model()->updateByPk($_POST['idPendaftaran'],array('statusperiksa'=>Params::statusPeriksa(2)));
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data Anamnesa berhasil disimpan");
//                    $this->redirect($_POST['url']);       
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
                
            $modAnamnesa->tglanamnesis = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modAnamnesa->tglanamnesis, 'yyyy-MM-dd hh:mm:ss'));    
            
            $modDataDiagnosa = new SADiagnosaM('search');
            $modDataDiagnosa->unsetAttributes();
            if(isset($_GET['SADiagnosaM']))
                $modDataDiagnosa->attributes = $_GET['SADiagnosaM'];
            
            $this->render('index',array(
                            'modAnamnesa'=>$modAnamnesa, 
                            'modDiagnosa'=>$modDiagnosa, 
                            'modDataDiagnosa'=>$modDataDiagnosa,
            ));
	}
        
}
