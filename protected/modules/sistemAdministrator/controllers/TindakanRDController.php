<?php
Yii::import('rawatJalan.models.RJPaketbmhpM');
class TindakanRDController extends SBaseController
{
    public $succesSave = false;
    protected $successSaveBmhp = true;
    protected $successSavePemakaianBahan = true;

	public function actionIndex()
	{
            $modPendaftaran = new SAPendaftaranT();
            $modPasien = new SAPasienM();
//            $modTindakans = null;
            $modTindakan = new SATindakanPelayananT;
            $modTindakan->tarifcyto_tindakan = 0;
            
            if(isset($_POST['SATindakanPelayananT']) || isset($_POST['TindakanpelayananT']))
            {
                $modTindakans = $this->saveTindakan($modPasien, $modPendaftaran);
               
//                if($this->succesSave)
//                    $this->redirect(array($this->id.'/','idPendaftaran'=>$idPendaftaran));
            }
            
            if (isset($_GET['FilterForm'])){
                $_GET['test'] = true;
                $_GET['term'] = $_GET['FilterForm'];
                $_GET['term2'] = $_GET['FilterForm2'];
            }
            
            if (isset($_GET['test'])){
                $_GET['term'] = (isset($_GET['term'])) ? $_GET['term'] : null;
                $_GET['term2'] = (isset($_GET['term2'])) ? $_GET['term2'] : null;
//                if(Yii::app()->request->isAjaxRequest) {
                
                if($_GET['idTipePaket'] == Params::TIPEPAKET_LUARPAKET)
                {
                    $criteria = new CDbCriteria();
                    $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['term2']), true);
                    $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['term']), true);
                    
                    if(Yii::app()->user->getState('tindakanruangan'))
                        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                    
                    if(Yii::app()->user->getState('tindakankelas'))
                        $criteria->compare('kelaspelayanan_id', $_GET['idKelasPelayanan']);
                    
                    $criteria->compare('tipepaket_id', Params::TIPEPAKET_LUARPAKET);
                    $criteria->order = 'daftartindakan_nama';
                    $models = 'PaketpelayananV';
                }else if($_GET['idTipePaket'] == Params::TIPEPAKET_NONPAKET)
                {
                    $criteria = new CDbCriteria();
                    $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['term2']), true);
                    $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['term']), true);
                    $criteria->order = 'daftartindakan_nama';
                    
                    if(Yii::app()->user->getState('tindakankelas'))
                        $criteria->compare('kelaspelayanan_id', $_GET['idKelasPelayanan']);
                    
                    if(Yii::app()->user->getState('tindakanruangan')){
                        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                        $models = 'TariftindakanperdaruanganV';
                    } else {
                        $models = 'TariftindakanperdaV';
                    }
                    
                }else
                {
                    $criteria = new CDbCriteria();
                    $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['term2']), true);
                    $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['term']), true);
                    
                    if(Yii::app()->user->getState('tindakanruangan'))
                        $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                    
                    if(Yii::app()->user->getState('tindakankelas'))
                        $criteria->compare('kelaspelayanan_id', $_GET['idKelasPelayanan']);
                    
                    $criteria->compare('tipepaket_id', $_GET['idTipePaket']);
                    $criteria->order = 'daftartindakan_nama';
                    
                    $models = 'PaketpelayananV';
                }

                $dataProvider = new CActiveDataProvider($models, array(
			'criteria'=>$criteria,
		));
                $route = Yii::app()->createUrl($this->route, array('idPendaftaran'=>$idPendaftaran, 'idKelasPelayanan'=>$_GET['idKelasPelayanan'], 'idTipePaket'=>$_GET['idTipePaket']));
                $this->renderPartial('_daftarTindakanPaket', array('dataProvider'=>$dataProvider, 'models'=>$models, 'route'=>$route));
                Yii::app()->end();
            }
            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modTindakans'=>$modTindakans,
                                        'modTindakan'=>$modTindakan,
                                        'modViewTindakans'=>$modViewTindakans,
                                        'modViewBmhp'=>$modViewBmhp));
	}
        
        public function saveTindakan($modPasien,$modPendaftaran)
        {
            $post = (isset($_POST['TindakanpelayananT'])) ? $_POST['TindakanpelayananT'] : $_POST['SATindakanPelayananT'];
            $valid=true; //echo $_POST['SATindakanPelayananT'][0]['tipepaket_id'];exit;
//            $modPendaftaran = PendaftaranT::model()->findAllByAttributes(array('pendaftaran_id'=>68));
//            echo "<pre>";
//            echo var_dump($modPendaftaran);
//            exit;
            foreach($post as $i=>$item)
            {
                if(!empty($item) && (!empty($item['daftartindakan_id']))){
                    $modTindakans[$i] = new SATindakanPelayananT;
                    $modTindakans[$i]->attributes=$item;
                    $modTindakans[$i]->tipepaket_id = $_POST['SATindakanPelayananT'][0]['tipepaket_id'];
                    $modTindakans[$i]->pasien_id = $item['pasien_id'];
                    $modTindakans[$i]->kelaspelayanan_id = $item['kelaspelayanan_id'];
                    $modTindakans[$i]->carabayar_id = $item['carabayar_id'];
                    $modTindakans[$i]->penjamin_id = $item['penjamin_id'];
                    $modTindakans[$i]->jeniskasuspenyakit_id = $item['jeniskasuspenyakit_id'];
                    $modTindakans[$i]->pendaftaran_id = $item['pendaftaran_id'];
                    $modTindakans[$i]->tgl_tindakan = $item['tgl_tindakan'];
                    $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                    $modTindakans[$i]->tarif_tindakan = $modTindakans[$i]->tarif_satuan * $modTindakans[$i]->qty_tindakan;
                    if($item['cyto_tindakan'])
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['persenCyto'] / 100) * $modTindakans[$i]->tarif_tindakan;
                    else
                        $modTindakans[$i]->tarifcyto_tindakan = 0;
                    $modTindakans[$i]->discount_tindakan = 0;
                    $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                    $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                    $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                    $modTindakans[$i]->iurbiaya_tindakan = 0;
                    $modTindakans[$i]->instalasi_id = $item['instalasi_id'];
//                    $modTindakans[$i]->ruangan_id =  Yii::app()->user->getState('ruangan_id');
                    $modTindakans[$i]->ruangan_id = $item['ruangan_id'];
                    $modTindakans[$i]->alatmedis_id = $this->cekAlatmedis($modTindakans[$i]->daftartindakan_id);
                                        
                    $tarifTindakan= TariftindakanM::model()->findAll('daftartindakan_id='.$item['daftartindakan_id'].' AND kelaspelayanan_id='.$modTindakans[$i]->kelaspelayanan_id.'');
                    foreach($tarifTindakan AS $dataTarif):
                        if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakans[$i]->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakans[$i]->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakans[$i]->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakans[$i]->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                    endforeach;
//                    echo "<pre>";
//                    echo print_r($modTindakans[$i]->getErrors());
//                    echo print_r($modTindakans[$i]->attributes,1);
//                    echo "</pre>";
//                    exit;
                    $valid = $modTindakans[$i]->validate() && $valid;
                }
            }

            $transaction = Yii::app()->db->beginTransaction();
            try {
                if($valid && (count($modTindakans) > 0)){
                    foreach($modTindakans as $i=>$tindakan){
                        $tindakan->save();
                        $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
                        if(isset($_POST['paketBmhp'])){
                            $modObatPasiens = $this->savePaketBmhp($modPendaftaran, $_POST['paketBmhp'],$tindakan);
                        }
                        if(isset($_POST['pemakaianBahan'])){
                            $modPemakainBahans = $this->savePemakaianBahan($modPendaftaran, $_POST['pemakaianBahan'],$tindakan);
                        }
                    }
                    if($statusSaveKomponen && $this->successSaveBmhp && $this->successSavePemakaianBahan) {
                        $updateStatusPeriksa=PendaftaranT::model()->updateByPk($_POST['idPendaftaran'],array('statusperiksa'=>Params::statusPeriksa(2)));
                        
                        PendaftaranT::model()->updateByPk($_POST['idPendaftaran'],
                            array(
                                'pembayaranpelayanan_id'=>null
                            )
                        );
                        
                        $transaction->commit();
                        $this->succesSave = true;
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                        $this->redirect(array('index'));
                        $this->refresh();
                        //Yii::app()->user->setFlash('error',"Data valid ".$this->traceObatAlkesPasien($modPemakainBahans));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data tidak valid ");
                        //Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceObatAlkesPasien($modPemakainBahans));
                    }
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data tidak valid ");
                    //Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceTindakan($modTindakans));
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
            }
            
            return $modTindakans;
        }
        
        protected function cekAlatmedis($idDaftartindakan)
        {
            $idAlatmedis = null;
            if(!empty($_POST['pemakaianAlat'])){
                foreach($_POST['pemakaianAlat'] as $k=>$item){
                    if($item['daftartindakan_id']==$idDaftartindakan){
                        $idAlatmedis = $item['alatmedis_id'];
                    }
                }
            }
            
            return $idAlatmedis;
        }

        public function saveTindakanKomponen($tindakan)
        {   
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $modTarifs = TariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$tindakan->daftartindakan_id, 'kelaspelayanan_id'=>$tindakan->kelaspelayanan_id),$criteria);
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = new TindakankomponenT;
                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen->validate() && $valid;
                if($valid)
//                   echo "<pre>";
//                    echo print_r($modTindakanKomponen[$i]->getErrors());
//                    echo print_r($modTindakanKomponen[$i]->attributes,1);
//                    echo "</pre>";
//                    exit;
                    $modTindakanKomponen->save();
            }
            
            return $valid;
        }

        private function traceTindakan($modTindakans)
        {
            foreach ($modTindakans as $key => $modTindakan) {
                $echo .= "<pre>".print_r($modTindakan->attributes,1)."</pre>";
            }
            return $echo;
        }
        
        public function actionAjaxDeleteTindakanPelayanan()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idTindakanpelayanan = $_POST['idTindakanpelayanan'];
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $deleteObatPasien = SAObatalkesPasienT::model()->deleteAllByAttributes(array('tindakanpelayanan_id'=>$idTindakanpelayanan));
                $obatAlkesT = SAObatalkesPasienT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$idTindakanpelayanan));
                $deleteTindakan = SATindakanPelayananT::model()->deleteByPk($idTindakanpelayanan);                
                
                $data['success'] = true;
                if(count($obatAlkesT) > 0){
                    $this->kembalikanStok($obatAlkesT);
                    if ($deleteObatPasien){
                        $data['success'] = true;
                    }else{
                        $data['success'] = false;
                    }
                }

                if ($deleteTindakan && $data['success']){
                    $data['success'] = true;
                    $transaction->commit();
                }else{
                    $data['success'] = false;
                    $transaction->rollback();
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                echo MyExceptionMessage::getMessage($exc,true);
                $data['success'] = false;
            }

            

            echo json_encode($data);
             Yii::app()->end();
            }
        }
        
        protected function savePaketBmhp($modPendaftaran,$paketBmhp,$tindakan)
        {
            $valid = true; $totalBmhp = 0;
            foreach ($paketBmhp as $i => $bmhp) {
                if($tindakan->daftartindakan_id == $bmhp['daftartindakan_id']){
                    $modObatPasien[$i] = new SAObatalkesPasienT;
                    $modObatPasien[$i]->pendaftaran_id = $bmhp['pendaftaran_id'];
                    $modObatPasien[$i]->penjamin_id = $bmhp['penjamin_id'];
                    $modObatPasien[$i]->carabayar_id = $bmhp['carabayar_id'];
                    $modObatPasien[$i]->daftartindakan_id = $bmhp['daftartindakan_id'];
                    $modObatPasien[$i]->sumberdana_id = $bmhp['sumberdana_id'];
                    $modObatPasien[$i]->pasien_id = $bmhp['pasien_id'];
                    $modObatPasien[$i]->satuankecil_id = $bmhp['satuankecil_id'];
//                    $modObatPasien[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                    $modObatPasien[$i]->ruangan_id = $tindakan->ruangan_id;
                    $modObatPasien[$i]->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                    $modObatPasien[$i]->tipepaket_id = Params::TIPEPAKET_BMHP; //$tindakan->tipepaket_id;
                    $modObatPasien[$i]->obatalkes_id = $bmhp['obatalkes_id'];
                    $modObatPasien[$i]->pegawai_id = $bmhp['pegawai_id'];
                    $modObatPasien[$i]->kelaspelayanan_id = $bmhp['kelaspelayanan_id'];
                    $modObatPasien[$i]->shift_id = Yii::app()->user->getState('shift_id');
                    $modObatPasien[$i]->tglpelayanan = date('Y-m-d H:i:s');
                    $modObatPasien[$i]->qty_oa = $bmhp['qtypemakaian'];
                    $modObatPasien[$i]->hargajual_oa = $bmhp['hargapemakaian'];
                    $modObatPasien[$i]->harganetto_oa = $bmhp['harganetto'];
                    $modObatPasien[$i]->hargasatuan_oa = $bmhp['hargapemakaian']; //$bmhp['hargasatuan'];
                    $totalBmhp = $totalBmhp + $bmhp['hargapemakaian'];

                    $valid = $modObatPasien[$i]->validate() && $valid;
                    if($valid) {
//                        echo "<pre>";
//                        echo print_r($modObatPasien[$i]->getErrors());
//                        echo print_r($modObatPasien[$i]->attributes,1);
//                        echo "</pre>";
//                        exit;
                        $modObatPasien[$i]->save();
                        $this->kurangiStok($modObatPasien[$i]->qty_oa, $modObatPasien[$i]->obatalkes_id);
                        $this->successSaveBmhp = true;
                    } else {
                        $this->successSaveBmhp = false;
                    }
                }
            }
            
            $totalBmhp = $totalBmhp + $tindakan->tarif_bhp;
            $tindakan->tarif_bhp = $totalBmhp;
            $tindakan->update();
            
            return $modObatPasien;
        }
        
        protected function savePemakaianBahan($modPendaftaran,$pemakaianBahan,$tindakan)
        {
            $valid = true;
            foreach ($pemakaianBahan as $i => $bmhp) {
                if($tindakan->daftartindakan_id == $bmhp['daftartindakan_id']){
                    $modPakaiBahan[$i] = new SAObatalkesPasienT;
                    $modPakaiBahan[$i]->pendaftaran_id = $bmhp['pendaftaran_id'];
                    $modPakaiBahan[$i]->penjamin_id = $bmhp['penjamin_id'];
                    $modPakaiBahan[$i]->carabayar_id = $bmhp['carabayar_id'];
                    $modPakaiBahan[$i]->daftartindakan_id = $bmhp['daftartindakan_id'];
                    $modPakaiBahan[$i]->sumberdana_id = $bmhp['sumberdana_id'];
                    $modPakaiBahan[$i]->pasien_id = $bmhp['pasien_id'];
                    $modPakaiBahan[$i]->satuankecil_id = $bmhp['satuankecil_id'];
//                    $modPakaiBahan[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                    $modPakaiBahan[$i]->ruangan_id = $tindakan->ruangan_id;
                    $modPakaiBahan[$i]->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                    $modPakaiBahan[$i]->tipepaket_id = $tindakan->tipepaket_id;
                    $modPakaiBahan[$i]->obatalkes_id = $bmhp['obatalkes_id'];
                    $modPakaiBahan[$i]->pegawai_id = $bmhp['pegawai_id'];
                    $modPakaiBahan[$i]->kelaspelayanan_id = $bmhp['kelaspelayanan_id'];
                    $modPakaiBahan[$i]->shift_id = Yii::app()->user->getState('shift_id');
                    $modPakaiBahan[$i]->tglpelayanan = date('Y-m-d H:i:s');
                    $modPakaiBahan[$i]->qty_oa = $bmhp['qty'];
                    $modPakaiBahan[$i]->hargajual_oa = $bmhp['subtotal'];
                    $modPakaiBahan[$i]->harganetto_oa = $bmhp['harganetto'];
                    $modPakaiBahan[$i]->hargasatuan_oa = $bmhp['hargasatuan'];

                    $valid = $modPakaiBahan[$i]->validate() && $valid;
                    if($valid) {
//                        echo "<pre>";
//                        echo print_r($modPakaiBahan[$i]->getErrors());
//                        echo print_r($modPakaiBahan[$i]->attributes,1);
//                        echo "</pre>";
//                        exit;
                        $modPakaiBahan[$i]->save();
                        $this->kurangiStok($modPakaiBahan[$i]->qty_oa, $modPakaiBahan[$i]->obatalkes_id);
                        $this->successSavePemakaianBahan = true;
                    } else {
                        $this->successSavePemakaianBahan = false;
                    }
                }
            }
            
            return $modPakaiBahan;
        }

        private function traceObatAlkesPasien($modObatPasiens)
        {
            foreach ($modObatPasiens as $key => $modObatPasien) {
                $echo .= "<pre>".print_r($modObatPasien->attributes,1)."</pre>";
            }
            return $echo;
        }

        /**
         * 
         * @param ObatalkespasienT $modObatPasien 
         */
        protected function saveObatAlkesKomponen($modObatPasien)
        {
            $modObatPasien = new ObatalkespasienT;
            $obat = ObatalkesM::model()->findByPk($modObatPasien->obatalkes_id);
            $obat = new ObatalkesM;
            $modObatPasienKomponen = new ObatalkeskomponenT;
            $modObatPasienKomponen->obatalkespasien_id = $modObatPasien->obatalkespasien_id;
            $modObatPasienKomponen->hargajualkomponen = $obat->hargajual;
            $modObatPasienKomponen->harganettokomponen = $obat->harganetto;
            $modObatPasienKomponen->hargasatuankomponen = $obat->hargajual;
            $modObatPasienKomponen->iurbiaya = 0;
            $modObatPasienKomponen->komponentarif_id = null;
            
        }
        
        protected function kurangiStok($qty,$idobatAlkes)
        {
            $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $idobatAlkes ORDER BY tglstok_in";
            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
            $selesai = false;
//            while(!$selesai){
                foreach ($stoks as $i => $stok) {
                    if($qty <= $stok['qtystok_current']) {
                        $stok_current = $stok['qtystok_current'] - $qty;
                        $stok_out = $stok['qtystok_out'] + $qty;
                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_current'=>$stok_current,'qtystok_out'=>$stok_out));
                        $selesai = true;
                        break;
                    } else {
                        $qty = $qty - $stok['qtystok_current'];
                        $stok_current = 0;
                        $stok_out = $stok['qtystok_out'] + $stok['qtystok_current'];
                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('stok_current'=>$stok_current,'qtystok_out'=>$stok_out));
                    }
                }
//            }
        }
        
        protected function kembalikanStok($obatAlkesT)
        {
            foreach ($obatAlkesT as $i => $obatAlkes) {
                $stok = new SAStokObatalkesT;
                $stok->obatalkes_id = $obatAlkes->obatalkes_id;
                $stok->sumberdana_id = $obatAlkes->sumberdana_id;
                $stok->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $stok->tglstok_in = date('Y-m-d H:i:s');
                $stok->tglstok_out = date('Y-m-d H:i:s');
                $stok->qtystok_in = $obatAlkes->qty_oa;
                $stok->qtystok_out = 0;
                $stok->qtystok_current = $obatAlkes->qty_oa;
                $stok->harganetto_oa = $obatAlkes->harganetto_oa;
                $stok->hargajual_oa = $obatAlkes->hargasatuan_oa;
                $stok->discount = $obatAlkes->discount;
                $stok->satuankecil_id = $obatAlkes->satuankecil_id;
                $stok->save();
            }
        }
        
        private function cekKomponenTarifTindakan($idKomponenTarifTindakan)
        {
            switch ($idTarifTindakan) {
                case Params::KOMPONENTARIF_ID_RS:return 'tarif_rsakomodasi';
                case Params::KOMPONENTARIF_ID_MEDIS:return 'tarif_medis';
                case Params::KOMPONENTARIF_ID_PARAMEDIS:return 'tarif_paramedis';
                case Params::KOMPONENTARIF_ID_BHP:return 'tarif_bhp';

                default:return null;
                    break;
            }
        }

}

?>