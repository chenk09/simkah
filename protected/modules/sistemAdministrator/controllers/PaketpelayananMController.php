<?php

class PaketpelayananMController extends SBaseController
{
    
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SAPaketpelayananM;
                $kelas = 0;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SAPaketpelayananM']))
		{
                        $model->attributes = $_POST['SAPaketpelayananM'];
                        $modPaket = $this->validasiTabular($_POST['PaketpelayananM']);
			$transaction = Yii::app()->db->beginTransaction();
                        try{
                            $success = 0;
                            $list = CHtml::listData(PaketpelayananM::model()->findAllByAttributes(array('tipepaket_id'=>$model->tipepaket_id)),'paketpelayanan_id','paketpelayanan_id');
                            foreach ($modPaket as $i=>$row){
                                if ($row->save()){
                                    unset($list[$row->paketpelayanan_id]);
                                    $success++;
                                }
                            }
                            if (count($list) > 0){
                                foreach ($list as $hasil){
                                    PaketpelayananM::model()->deleteByPk($hasil);
                                }
                            }

                            if ((count($modPaket) > 0) && ($success == count($modPaket))) {
                                $transaction->commit();
                                Yii::app()->user->setFlash('success', "Data Berhasil Disimpan ");
                                $this->redirect(Yii::app()->createUrl($this->module->id.'/'.paketpelayananM.'/admin'));
                            }
                            else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                            }
                        } catch (Exception $exc) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
                        }
		}

		$this->render('create',array(
			'model'=>$model, 'modPaket'=>$modPaket, 'kelas'=>$kelas
		));
	}
        
        protected function validasiTabular($data){
            $x = 0;
            foreach ($data as $i=>$row){
                if (!empty($row['paketpelayanan_id'])){
                    $paket[$x] = PaketpelayananM::model()->findByPk($row['paketpelayanan_id']);
                }else{
                    $paket[$x] = new PaketpelayananM();
                }
                $paket[$x]->attributes = $row;
                $paket[$x]->namatindakan = $row['namatindakan'];
                $paket[$x]->subsidiasuransi = floor($row['subsidiasuransi']);
                $paket[$x]->subsidipemerintah = floor($row['subsidipemerintah']);
                $paket[$x]->subsidirumahsakit = floor($row['subsidirumahsakit']);
                $paket[$x]->tarifpaketpel = floor($row['tarifpaketpel']);
                $paket[$x]->iurbiaya = floor($row['iurbiaya']);
                $paket[$x]->validate();
                
                $x++;
            }

            return $paket;
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
                $model->ruangan_id = '';
                $modTipePaket = TipepaketM::model()->findByPk($model->tipepaket_id);
                $model->tarifpaketpel=$modTipePaket->tarifpaket;
                $model->subsidiasuransi = $modTipePaket->paketsubsidiasuransi;
                $model->subsidirumahsakit = $modTipePaket->paketsubsidirs;
                $model->subsidipemerintah = $modTipePaket->paketsubsidipemerintah;
                $model->iurbiaya = $modTipePaket->paketiurbiaya;
                $kelas = $modTipePaket->kelaspelayanan_id;
                $dataPaketPelayanan = PaketpelayananM::model()->findAllByAttributes(array('tipepaket_id'=>$model->tipepaket_id));
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                 //echo $jumlahUlang;exit();
		if(isset($_POST['SAPaketpelayananM']))
		{
                    $model->attributes = $_POST['SAPaketpelayananM'];
                    $modPaket = $this->validasiTabular($_POST['PaketpelayananM']);
                    $dataPaketPelayanan = $modPaket;
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = 0;
//                        SAPaketpelayananM::model()->deleteAllByAttributes(array('tipepaket_id'=>$model->tipepaket_id));
                        $list = CHtml::listData(PaketpelayananM::model()->findAllByAttributes(array('tipepaket_id'=>$model->tipepaket_id)),'paketpelayanan_id','paketpelayanan_id');
                        foreach ($modPaket as $i=>$row){
                            if ($row->save()){
                                unset($list[$row->paketpelayanan_id]);
                                $success++;
                            }
                        }
                        
                        if (count($list) > 0){
                            foreach ($list as $hasil){
                                PaketpelayananM::model()->deleteByPk($hasil);
                            }
                        }

                        if ((count($modPaket) > 0) && ($success == count($modPaket))) {
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', "Data Berhasil Disimpan ");
                            $this->redirect(Yii::app()->createUrl($this->module->id.'/paketpelayananM/admin'));
                        }
                        else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                        }
                    } catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
                    }
		}

		$this->render('create',array(
			'model'=>$model, 'modPaket'=>$dataPaketPelayanan, 'kelas'=>$kelas,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			//$this->loadModel($id)->delete();
                        SAPaketpelayananM::model()->deleteAllByAttributes(array('tipepaket_id'=>$id));

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SAPaketpelayananM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SAPaketpelayananM('search');
		$modTipePaket=new SATipePaketM('search');
//		$modTipePaket->unsetAttributes();  // clear any default values
//		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SATipePaketM']))
			$modTipePaket->attributes=$_GET['SATipePaketM'];
		if(isset($_GET['SAPaketpelayananM']))
			$modTipePaket->tipepaket_nama=$_GET['SAPaketpelayananM']['tipepaketNama'];

		$this->render('admin',array(
			'model'=>$model,
			'modTipePaket'=>$modTipePaket,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
                $dataModel = SAPaketpelayananM::model()->findByAttributes(array('tipepaket_id'=>$id), array('limit'=>1));
		$model=SAPaketpelayananM::model()->findByPk($dataModel->paketpelayanan_id);
		if($model===null){
			$model = new SAPaketpelayananM();
                        $model->tipepaket_id = $id;
                }
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sapaketpelayanan-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new SAPaketpelayananM;
            $model->attributes=$_REQUEST['SAPaketpelayananM'];
            $judulLaporan='Data Paket Pelayanan';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
