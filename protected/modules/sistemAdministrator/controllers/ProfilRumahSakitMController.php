<?php

class ProfilRumahSakitMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print','printRS'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','print'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $modMisiRS=SAMisirsM::model()->findAllByAttributes(array('profilrs_id'=>$id));
		$this->render('view',array(
			'model'=>$this->loadModel($id),'modMisiRS'=>$modMisiRS
		));
	}
        
        /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionPrintRS($id)
	{
               //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                                              
                     $modMisiRS=SAMisirsM::model()->findAllByAttributes(array('profilrs_id'=>$id));
                     $judulLaporan='';
                     $caraPrint=$_REQUEST['caraPrint'];
                        if($caraPrint=='PRINT')
                            {
                                $this->layout='//layouts/printWindows';
                                $this->render('view',array(
                                                        'model'=>$this->loadModel($id),'modMisiRS'=>$modMisiRS
                                ));         
                            }
                        else if($caraPrint=='EXCEL')    
                            {
                                $this->layout='//layouts/printExcel';
                                $this->render('PrintRS',array(
                                              'model'=>$this->loadModel($id),'modMisiRS'=>$modMisiRS
                                ));                             
                            }
                        else if($_REQUEST['caraPrint']=='PDF')
                            {

                                $ukuranKertasPDF=Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                                $posisi=Yii::app()->user->getState('posisi_kertas');                            //Posisi L->Landscape,P->Portait
                                $mpdf=new MyPDF('',$ukuranKertasPDF); 
                                $mpdf->useOddEven = 2;  
                                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/protected/extensions');
                                $mpdf->WriteHTML($stylesheet,1);  
                                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                                $mpdf->WriteHTML($this->render('PrintRS',array(
                                              'model'=>$this->loadModel($id),'modMisiRS'=>$modMisiRS)),true);
                                $mpdf->Output();
                            } 
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                             
		$model=new SAProfilRumahSakitM;
                $modMisiRS = new SAMisirsM;
                $modProfilPict = new SAProfilpictureM;
                
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SAProfilRumahSakitM']))
		{
                      $transaction = Yii::app()->db->beginTransaction();
                      $model=new SAProfilRumahSakitM;
                      $model->attributes=$_POST['SAProfilRumahSakitM'];
                      $model->tglregistrasi = date('Y-m-d H:i:s');
                      $model->tglakreditasi = date('Y-m-d H:i:s');
                      if ($model->validate()) {
                        try {
                            // $random = rand(0000000, 9999999);
                            // $model->logo_rumahsakit = CUploadedFile::getInstance($model, 'logo_rumahsakit');
                            $gambar = $model->logo_rumahsakit;
                            if (!empty($model->logo_rumahsakit)) {//Klo User Memasukan Logo
                                $model->path_logorumahsakit = $random . $model->logo_rumahsakit;
                                //                   $model->path_logorumahsakit =Params::pathProfilRSDirectory().$random.$model->logo_rumahsakit;
                                $model->logo_rumahsakit = $random . $model->logo_rumahsakit;

                                Yii::import("ext.EPhpThumb.EPhpThumb");

                                $thumb = new EPhpThumb();
                                $thumb->init(); //this is needed

                                $fullImgName = $model->logo_rumahsakit;
                                $fullImgSource = Params::pathProfilRSDirectory() . $fullImgName;
                                $fullThumbSource = Params::pathProfilRSTumbsDirectory() . 'kecil_' . $fullImgName;

                                // $model->logo_rumahsakit = $fullImgName;
                                    // echo "<pre>"; print_r($model->logo_rumahsakit); exit();
                                if ($model->save()) {
                                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                    $gambar->saveAs($fullImgSource);
                                    $thumb->create($fullImgSource)
                                            ->resize(200, 200)
                                            ->save($fullThumbSource);
                                } else {
                                    Yii::app()->user->setFlash('error', 'Logo <strong>Gagal!</strong>  disimpan.');
                                }
                            } else { //Klo User Tidak Memasukan Logo
                                $model->save();
                            }


                            if (isset($_POST['SAMisirsM'])) {  //Jika Misi Diisi
                                $valid = true;
                                foreach ($_POST['SAMisirsM'] as $i => $item) {
                                    if (is_integer($i)) {
                                        $modMisiRS = new SAMisirsM;
                                        if (isset($_POST['SAMisirsM'][$i]))
                                            $modMisiRS->attributes = $_POST['SAMisirsM'][$i];
                                        $modMisiRS->profilrs_id = $model->profilrs_id;
                                        $modMisiRS->misi = $_POST['SAMisirsM'][$i]['misi'];
                                        $valid = $modMisiRS->validate();
                                        
                                        if ($valid) {
                                            $modMisiRS->save();
                                        }
                                    }
                                }
                            }
                            
                            if(isset($_POST['SAProfilpictureM']))
                            {
                                Yii::import("ext.EPhpThumb.EPhpThumb");
                                
                                foreach ($_POST['SAProfilpictureM'] as $i=>$item){
                                    $tempProfil = true;
                                    $thumb = new EPhpThumb();
                                    $thumb->init(); //this is needed
                                    
                                    $modProfil = new SAProfilpictureM();  
                                                                        
                                    $modProfil->attributes=$_POST['SAProfilpictureM'][$i];

                                    $modProfil->profilpicture_path = CUploadedFile::getInstance($modProfil, '['.$i.']profilpicture_path'); 
                                    if (empty($modProfil->profilpicture_path)){
                                        $modProfil->profilpicture_path = '1';
                                        $tempProfil =false;
                                    }
                                    $rand = rand(0000000,9999999);
                                    $fullImgName =$rand.$modProfil->profilpicture_path;   
                                                                        
                                    $modProfil->profilrs_id = $model->profilrs_id;
                                    $modProfil->profilpicture_tgl = date('Y-m-d');
                                    $gambar = $modProfil->profilpicture_path;
                                    $modProfil->profilpicture_path = $fullImgName;
                                    // echo "<pre>"; print_r($modProfil->attributes); exit();
                                    if($modProfil->save()){ 

                                         if (!empty($gambar)){
                                             if ($tempProfil == true){
                                                $fullImgSource = Params::pathAntrianSliderGambar().$fullImgName;
                                                $fullThumbSource = Params::pathAntrianSliderGambarThumbs().'kecil_' . $fullImgName;
                                                $gambar->saveAs($fullImgSource);
                                                $thumb->create($fullImgSource)
                                                      ->resize(200, 200)
                                                      ->save($fullThumbSource);
                                             }
                                         }
                                    }
                                     else
                                              {
                                                   Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
                                              }
                                }
                            }
                            $transaction->commit();
                            // echo "<pre>"; print_r($model->attributes); exit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('admin'));
                        } catch (Exception $e) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($e,true));
                        }
                    }
                                
		}
		$this->render('create',array(
			'model'=>$model,'modMisiRS'=>$modMisiRS, 'modProfilPict'=>$modProfilPict
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                                        
		$model=$this->loadModel($id);
                $modMisiRS=SAMisirsM::model()->findAllByAttributes(array('profilrs_id'=>$id));
                $modProfilPict = SAProfilpictureM::model()->findAllByAttributes(array("profilrs_id"=>$id), array('order'=>'profilpicture_tgl'));
                $temLogo = $model->logo_rumahsakit;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SAProfilRumahSakitM']))
		{
                    $transaction = Yii::app()->db->beginTransaction();
                      $model=$this->loadModel($id);
                      $model->attributes=$_POST['SAProfilRumahSakitM'];
                      if ($model->validate()) {
                        try {
                            $hapusMisiRS = SAMisirsM::model()->deleteAll('profilrs_id=' . $id . '');
                            $random = rand(0000000, 9999999);
                            $model->logo_rumahsakit = CUploadedFile::getInstance($model, 'logo_rumahsakit');
                            $gambar = $model->logo_rumahsakit;
                            if (!empty($model->logo_rumahsakit)) {
                                $model->path_logorumahsakit = $random . $model->logo_rumahsakit;
                                $model->logo_rumahsakit = $random . $model->logo_rumahsakit;

                                Yii::import("ext.EPhpThumb.EPhpThumb");

                                $thumb = new EPhpThumb();
                                $thumb->init(); //this is needed

                                $fullImgName = $model->logo_rumahsakit;
                                $fullImgSource = Params::pathProfilRSDirectory() . $fullImgName;
                                $fullThumbSource = Params::pathProfilRSTumbsDirectory() . 'kecil_' . $fullImgName;

                                $model->logo_rumahsakit = $fullImgName;

                                if ($model->save()) {
                                    if (!empty($temLogo)) {
                                        unlink(Params::pathProfilRSDirectory() . $temLogo);
                                        unlink(Params::pathProfilRSTumbsDirectory() . 'kecil_' . $temLogo);
                                    }
                                    $gambar->saveAs($fullImgSource);
                                    $thumb->create($fullImgSource)
                                            ->resize(200, 200)
                                            ->save($fullThumbSource);
                                }


                                if (isset($_POST['SAMisirsM'])) {  //Jika Misi Diisi
                                    $valid = true;
                                    foreach ($_POST['SAMisirsM'] as $i => $item) {
                                        if (is_integer($i)) {
                                            $modMisiRS = new SAMisirsM;
                                            if (isset($_POST['SAMisirsM'][$i]))
                                                $modMisiRS->attributes = $_POST['SAMisirsM'][$i];
                                            $modMisiRS->profilrs_id = $model->profilrs_id;
                                            $modMisiRS->misi = $_POST['SAMisirsM'][$i]['misi'];

                                            $valid = $modMisiRS->validate() && $valid;
                                            
                                            if ($valid) {
                                                $modMisiRS->save();
                                            }
                                        }
                                    }
                                }
                            } else {
                                $model->save();
                                if (isset($_POST['SAMisirsM'])) {  //Jika Misi Diisi
                                    $valid = true;
                                    foreach ($_POST['SAMisirsM'] as $i => $item) {
                                        if (is_integer($i)) {
                                            $modMisiRS = new SAMisirsM;
                                            if (isset($_POST['SAMisirsM'][$i]))
                                                $modMisiRS->attributes = $_POST['SAMisirsM'][$i];
                                            $modMisiRS->profilrs_id = $model->profilrs_id;
                                            $modMisiRS->misi = $_POST['SAMisirsM'][$i]['misi'];

                                            $valid = $modMisiRS->validate() && $valid;

                                            if ($valid) {
                                                $modMisiRS->save();
                                            }
                                        }
                                    }
                                }
                            }
                            
                            if(isset($_POST['SAProfilpictureM']))
                            {
                                Yii::import("ext.EPhpThumb.EPhpThumb");

                                SAProfilpictureM::model()->deleteAllByAttributes(array('profilrs_id'=>$model->profilrs_id));
                                foreach ($_POST['SAProfilpictureM'] as $i=>$item){
                                    $tempProfil = '';
                                    $dataGambar = true;
                                    $thumb = new EPhpThumb();
                                    $thumb->init(); //this is needed
                                    
                                    $modProfil = new SAProfilpictureM();  
                                    $modProfil->attributes=$_POST['SAProfilpictureM'][$i];
                                    
                                    if (!empty($_POST['SAProfilpictureM'][$i]['profilpicture_id'])){
                                        $tempProfil = $_POST['SAProfilpictureM'][$i]['temp_gambar'];
                                        $modProfil->profilpicture_id = $_POST['SAProfilpictureM'][$i]['profilpicture_id'];
                                    }
//                                     echo 'a'.$_POST['SAProfilpictureM'][$i]['temp_gambar'].$modProfil->temp_gambar;
                                    $modProfil->profilpicture_path = CUploadedFile::getInstance($modProfil, '['.$i.']profilpicture_path'); 
                                    if (empty($modProfil->profilpicture_path)){
                                        $dataGambar = false;
                                    }
                                    $rand = rand(0000000,9999999);
                                    $fullImgName =$rand.$modProfil->profilpicture_path;   
                                    
                                    if (empty($modProfil->profilpicture_path)){
                                        if(empty($tempProfil)){
                                            $tempProfil = '1';
                                        }
                                        $modProfil->profilpicture_path = $tempProfil;
                                        $fullImgName = $tempProfil;
                                    }
                                    
                                    $modProfil->profilrs_id = $model->profilrs_id;
                                    $gambar = $modProfil->profilpicture_path;
                                    $modProfil->profilpicture_tgl = date('Y-m-d H:i:s');
                                    $modProfil->profilpicture_path = $fullImgName;
                                    
                                    if($modProfil->save()){    
                                         if (!empty($gambar)){
                                             if (!empty($tempProfil)){
                                                 if ($dataGambar == true){
                                                     if (file_exists(Params::pathAntrianSliderGambar().$tempProfil)){
                                                        unlink(Params::pathAntrianSliderGambar() . $tempProfil);
                                                     }
                                                     if (file_exists(Params::pathAntrianSliderGambarThumbs().'kecil_'.$tempProfil)){
                                                        unlink(Params::pathAntrianSliderGambarThumbs(). 'kecil_' . $tempProfil);
                                                     }
                                                 }
                                             }
                                             if ($fullImgName != $tempProfil){
                                                $fullImgSource = Params::pathAntrianSliderGambar().$fullImgName;
                                                $fullThumbSource = Params::pathAntrianSliderGambarThumbs().'kecil_' . $fullImgName;
                                                $gambar->saveAs($fullImgSource);
                                                $thumb->create($fullImgSource)
                                                      ->resize(200, 200)
                                                      ->save($fullThumbSource);
                                             }
                                         }
                                    }
                                }
                            }

                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('admin'));
                        } catch (Exception $e) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');
                        }
                    }
		}

		$this->render('update',array(
			'model'=>$model,'modMisiRS'=>$modMisiRS, 'modProfilPict'=>$modProfilPict,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
            
		if(Yii::app()->request->isPostRequest)
		{
                    
                           
                         //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                         
			 $transaction = Yii::app()->db->beginTransaction();
                         try {
                                $hapusMisiRS = SAMisirsM::model()->deleteAll('profilrs_id='.$id.'');
                                $hapusMisiRS = SAProfilpictureM::model()->deleteAll('profilrs_id='.$id.'');
                                $model = $this->loadModel($id)->delete();
                                $temLogo = $model->logo_rumahsakit;
                             
                                 if(!empty($temLogo))
                                        { 
                                           unlink(Params::urlProfilRSDirectory().$temLogo);
                                           unlink(Params::urlProfilRSDirectory().$temLogo);
                                        }
                                      
                                $transaction->commit();
                                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                                
                                       
                            } 
                        catch (Exception $e)
                            {
                                $transaction->rollback();
                                echo 'error'.$e->getMessage();
                                
                            }   

		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SAProfilRumahSakitM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                     
		$model=new SAProfilRumahSakitM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SAProfilRumahSakitM']))
			$model->attributes=$_GET['SAProfilRumahSakitM'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SAProfilRumahSakitM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='saprofil-rumah-sakit-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
         public function actionPrint()
         {
             //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                                              
             $model= new SAProfilRumahSakitM;
             $model->attributes=$_REQUEST['SAProfilRumahSakitM'];
             $judulLaporan='';
             $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT')
                {
                    $this->layout='//layouts/printWindows';
                    $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
            else if($caraPrint=='EXCEL')    
                {
                    $this->layout='//layouts/printExcel';
                    $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
            else if($_REQUEST['caraPrint']=='PDF')
                {
                   
                    $ukuranKertasPDF=Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                    $posisi=Yii::app()->user->getState('posisi_kertas');                            //Posisi L->Landscape,P->Portait
                    $mpdf=new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);  
                    $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                    $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                    $mpdf->Output();
                }                       
         }
}
