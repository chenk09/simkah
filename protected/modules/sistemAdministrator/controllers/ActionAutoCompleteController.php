<?php

class ActionAutoCompleteController extends SBaseController
{
        /**
         * menampilkan seluruh pendaftaran pasien
         * digunakan di:
         * - tindakanTSA/_ringkasDataPasien
         */
        public function actionDaftarPasien()
	{
            if(Yii::app()->request->isAjaxRequest) {
                $format = new CustomFormat;
                $criteria = new CDbCriteria();
                if(isset($_GET['term'])){
                    $criteria->compare('LOWER(no_rekam_medik)', strtolower($_GET['term']), true);
                }else if(isset($_GET['noPendaftaran'])){
                    $criteria->compare('LOWER(no_pendaftaran)', strtolower($_GET['noPendaftaran']), true);
                }
                $criteria->limit = 10;
                $criteria->order = 'no_pendaftaran ASC';
                $models = InfopasienpengunjungV::model()->findAll($criteria);
                $returnVal = array();
                foreach($models as $i=>$model)
                {
                    $modPendaftaran = PendaftaranT::model()->findByPk($model->pendaftaran_id);
                    $attributes = $model->attributeNames();
                    foreach($attributes as $j=>$attribute) {                        
                        $returnVal[$i]["$attribute"] = $model->$attribute;
                    }
                    $tglPendaftaran = $format->formatDateINA(date('Y-m-d', strtotime($model->tgl_pendaftaran)));
                    $model->tgl_pendaftaran = date('d M Y H:i:s', strtotime($model->tgl_pendaftaran));
                    if(isset($_GET['term'])){
                        $returnVal[$i]['label'] = $model->no_rekam_medik.' - '.$model->ruangan_nama.' - '.$tglPendaftaran;
                        $returnVal[$i]['value'] = $model->no_rekam_medik;
                    }else if(isset($_GET['noPendaftaran'])){
                        $returnVal[$i]['label'] = $model->no_pendaftaran.' - '.$tglPendaftaran.' - '.$model->nama_pasien;
                        $returnVal[$i]['value'] = $model->no_pendaftaran;
                    }
                    $returnVal[$i]['jeniskasuspenyakit_id'] = $modPendaftaran->jeniskasuspenyakit_id;
                    $returnVal[$i]['jeniskasuspenyakit_nama'] = $modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama;
                }

                echo CJSON::encode($returnVal);
            }
            Yii::app()->end();
	}
}
?>