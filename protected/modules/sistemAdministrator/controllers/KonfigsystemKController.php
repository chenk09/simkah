
<?php

class KonfigsystemKController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'createTemplateXcel','importExcel'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SAKonfigsystemK;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SAKonfigsystemK']))
		{
			$model->attributes=$_POST['SAKonfigsystemK'];
                        $model->nodejs_host=$_POST['SAKonfigsystemK']['nodejs_host'];
			$model->nodejs_port=$_POST['SAKonfigsystemK']['nodejs_port'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('admin','id'=>$model->konfigsystem_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SAKonfigsystemK']))
		{
			$model->attributes=$_POST['SAKonfigsystemK'];
			$model->nodejs_host=$_POST['SAKonfigsystemK']['nodejs_host'];
			$model->nodejs_port=$_POST['SAKonfigsystemK']['nodejs_port'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('admin','id'=>$model->konfigsystem_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SAKonfigsystemK');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SAKonfigsystemK('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SAKonfigsystemK']))
			$model->attributes=$_GET['SAKonfigsystemK'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SAKonfigsystemK::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sakonfigsystem-k-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
         {
             //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                         
             $model= new SAKonfigsystemK();
             $model->attributes=$_REQUEST['SAKonfigsystemK()'];
             $judulLaporan='Data Konfigurasi Sistem';
             $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT')
                {
                    $this->layout='//layouts/printWindows';
                    $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
            else if($caraPrint=='EXCEL')    
                {
                    $this->layout='//layouts/printExcel';
                    $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
            else if($_REQUEST['caraPrint']=='PDF')
                {
                   
                    $ukuranKertasPDF=Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                    $posisi=Yii::app()->user->getState('posisi_kertas');                            //Posisi L->Landscape,P->Portait
                    $mpdf=new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);  
                    $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                    $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                    $mpdf->Output();
                }                       
         }
         
         /**
          * method import excel file into database
          * used in : 
          * 1. systemAdministrator -> konfig system -> import excel
          */
         public function actionImportExcel(){
            
            $files = dirname(__FILE__).'/test.xls';

            /**
             * ajax request handlers
             */
            if (Yii::app()->request->isAjaxRequest){
                if (isset($_FILES['upload'])){
                    $tableName = $_POST['tableName'];
                    $files = CUploadedFile::getInstanceByName('upload');
                    $object = Yii::app()->yexcel->readActiveSheet($files->tempName);
                    $table = Yii::app()->db->getSchema()->getTable($tableName);    
                    echo $this->renderTable($object,$table);
                }
                Yii::app()->end();
            }
            
            /**
             * form method post handlers
             */
            if (isset($_POST['tableName'],$_POST['Hasil'])){
                $tableName = $_POST['tableName'];
                if (isset($_POST['Hasil']))
                    $value = $_POST['Hasil'];
                $files = $files = CUploadedFile::getInstanceByName('upload');
                $object = Yii::app()->yexcel->readActiveSheet($files->tempName);
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $result = $this->saveMassTable($object,$tableName,$value);
                    if ($result){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success','<strong>Berhasil</strong> Data Berhasil disimpan');
                        $this->refresh();
                    }else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan');
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($exc));
                }
            }
            
            $this->render('test');
        }
        
        /**
         * rendering table based on excel files
         * @param array $object excel file that's already converted into array
         * @param object $table table schema
         */
        protected function renderTable($object,$table){
            $foreignKey = $table->foreignKeys; 
            $kolom = $table->columns;
            echo '<table class="table table-bordered table-condensed">
                <thead><th>Pilih</th>';
            foreach($object[1] as $key2=>$value2){
                echo '<th>'.$key2.'</th>';
            }
                echo '</thead><tbody>';
                $jumlahFK = count($foreignKey);
                $findKey = ($jumlahFK > 0) ? true : (($jumlahFK == 0) ? true: false);
                $list = array();
            foreach ($object as $key => $value) {
                echo '<tr valueField = "'.$key.'">';
                if (!$findKey){
                    echo '<td><input type="checkbox" name="Hasil['.$key.'][cek]"></td>';
                }else{
                    echo '<td></td>';
                }
                
                foreach ($value as $counter => $value2) {
                    if (isset($list[$counter]) && !$findKey && (empty($value2))){
                        echo '<td columnField="'.$counter.'">
                                <div class="input-append">
                                    <input type="hidden" name="Hasil['.$key.']['.$list[$counter][1].']" class="id"/>
                                    <input type="text" name="Hasil['.$key.']['.$list[$counter][1].'_nama]" id="tableName" style="float:left;">
                                        <span class="add-on"><i class="icon-list-alt"></i></span>
                                </div>
                              </td>';
                    }else{
                        echo '<td columnField="'.$counter.'">'.$value2.'</td>';
                    }
                    
                    
                    if ($findKey){
                        if (isset($foreignKey[$value2]) && count($foreignKey[$value2]) == 2){
                            $list[$counter] = $foreignKey[$value2];
                            unset($foreignKey[$value2]);
                        }
                        $findKey = (count($foreignKey) > 0) ? true : false;
                    }
                }
                echo '</tr>';
            }
            echo '</tbody></table>';
            $this->renderJavascript($list);
        }
        
        /**
         * rendering javascript file to be used in table view
         * @param array $list 
         */
        protected function renderJavascript($list){
            echo "<script>
                $(document).ready(function(){
                  var isMouseDown = false;
                  $('#excel table td')
                    .mousedown(function () {
                      isMouseDown = true;
                      $(this).parents('tr').toggleClass('yellow_background').find('input[name*=\'[cek]\']').attr('checked', function(idx, oldAttr) {
                            return !oldAttr;
                      });
                    })
                    .mouseover(function () {
                      if (isMouseDown) {
                        $(this).parents('tr').toggleClass('yellow_background').find('input[name*=\'[cek]\']').attr('checked', function(idx, oldAttr) {
                            return !oldAttr;
                        });
                      }
                    })
                    .bind('selectstart', function () {
                      return false; 
                    });

                  $(document)
                    .mouseup(function () {
                      isMouseDown = false;
                    });
                });
             ";
            if (count($list) > 0){
                foreach ($list as $value) {
                    echo '$("input[name*=\'['.$value[1].'_nama]\']").autocomplete({"minLength":"3","source":"/simrs/index.php?r=actionAutoComplete/getValuePrimaryKey&table='.$value[0].'&primaryKey='.$value[1].'","select":function(event,ui){$(this).parents("td").find(".id").val(ui.item.id);}});';
                }
            }
             echo '</script>';
        }
        
        /**
         * method to save into table 
         * @param array $objects
         * @param string $tableName table name
         * @param values $values variable contains post method 
         * @return boolean result
         */
        protected function saveMassTable($objects,$tableName,$values){
            $kolom = array();
            $table = Yii::app()->db->getSchema()->getTable($tableName);
            $columns = $table->columns;
            $listBoolean = array('Ya'=>'true','Tidak'=>'false');
            $builder=Yii::app()->db->schema->getCommandBuilder();
            $primaryKeys = $table->primaryKey;
            
            if (count($columns > 0)){
                foreach ($columns as $counter => $column) {
                     $kolom[] = $column->name;
                }
            }

            $data = array();
            $aktifPrimaryKey = false;
            $result = true;
            if (count($values) > 0){
                foreach ($values as $counter => $row) {
                    if (isset($objects[$counter])){
                        $i = 0;
                        foreach ($objects[$counter] as $key => $value) {
                            $data[$kolom[$i]] = (!empty($value)) ? ((isset($listBoolean[trim($value)])) ? $listBoolean[trim($value)] : $value ) : null;
                            if (!empty($row[$kolom[$i]])){
                                $data[$kolom[$i]] = $row[$kolom[$i]];
                            }
                            $i++;
                        }
                        if (!$aktifPrimaryKey){
                            if (is_string($primaryKeys)){
                                unset($data[$primaryKeys]);
                            }
                            else if (is_array($primaryKeys)){
                                foreach ($primaryKeys as $key => $primaryKey) {
                                    unset($data[$primaryKey]);
                                }
                            }
                        }
                        $command=$builder->createInsertCommand($table,$data);
                        $result = $command->execute() && $result;
                        echo $result;
                    }
                }
            }
            return $result;
        }
        
        /**
         * output method of file excel contains template of table 
         */
        public function actionCreateTemplateXcel(){
            if (isset($_GET['tableName'])){
                $this->layout='//layouts/printExcel';
                $tableName = $_GET['tableName'];
                $table = Yii::app()->db->getSchema()->getTable($tableName);     
                $model = null;
                if (!empty($table->name)){
                    $sql = "select *from {$table->name}";
                    $model = Yii::app()->db->createCommand($sql)->queryAll();
                }
                    
                $judul = "Template Excel $tableName";
                $this->render('_template',array('table'=>$table, 'judul'=>$judul, 'model'=>$model));
            }
        }
}