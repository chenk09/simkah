<?php

class PemeriksaanLABMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SANilaiRujukanM;
                $modPemeriksaanLab = new SAPemeriksaanLABM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
//                $modPemeriksaanLab = new SAPemeriksaanLABM;
//                            $modPemeriksaanLab->attributes=$_POST['SAPemeriksaanLABM'];
//                            $modPemeriksaanLab->pemeriksaanlab_aktif=TRUE;
//                            echo '<pre>';
//                            echo print_r($modPemeriksaanLab->attributes);exit();
//                            $modPemeriksaanLab->save();
                            
                if(isset($_POST['SANilaiRujukanM'])){
                    $transcaction=Yii::app()->db->beginTransaction();
                    try {
                            $modPemeriksaanLab = new SAPemeriksaanLABM;
                            $modPemeriksaanLab->attributes=$_POST['SAPemeriksaanLABM'];
                            $modPemeriksaanLab->pemeriksaanlab_aktif=TRUE;
                            if ($modPemeriksaanLab->save()){
                                $jumlahJenisKelaminNilaiRujukan=COUNT($_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin']);

                                for($i=0; $i<=$jumlahJenisKelaminNilaiRujukan; $i++):
                                   if ($_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i]!='N'){
                                       $jumlahKelompokUmur=COUNT($_POST['SANilaiRujukanM']['kelompokumur'][''.$_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i].'']);
                                      for($j=0; $j<=$jumlahKelompokUmur; $j++):
                                        if($_POST['SANilaiRujukanM']['nilairujukan_nama'][$_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i]][$j]!=''){ 
                                            $jenisKelamin=$_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i];

                                            $model=new SANilaiRujukanM;
                                            $model->attributes=$_POST['SANilaiRujukanM'];
                                            $model->nilairujukan_jeniskelamin=$_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i];
                                            $model->pemeriksaanlab_id=$modPemeriksaanLab->pemeriksaanlab_id;
                                            $model->kelompokumur=$_POST['SANilaiRujukanM']['kelompokumur'][$jenisKelamin][$j];
                                            $model->nilairujukan_min=$_POST['SANilaiRujukanM']['nilairujukan_min'][$jenisKelamin][$j];
                                            $model->nilairujukan_max=$_POST['SANilaiRujukanM']['nilairujukan_max'][$jenisKelamin][$j];
                                            $model->nilairujukan_nama=$_POST['SANilaiRujukanM']['nilairujukan_nama'][$jenisKelamin][$j];
                                            $model->nilairujukan_aktif=TRUE;
                                            $model->save(); 
                                        }
                                     endfor;  
                                     $j=0;
                                }
                                endfor;
                                $model->save();
                            }
                            $transcaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');  
                            $this->redirect(array('admin','id'=>$modPemeriksaanLab->pemeriksaanlab_id));


                    }
                    catch (Exception $ext){
                         $transcaction->rollback();
                         Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ext,true));  
                    }
                }    
			
		

		$this->render('create',array(
			'model'=>$model,'modPemeriksaanLab'=>$modPemeriksaanLab
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$modPemeriksaanLab=$this->loadModel($id);
                $model=new SANilaiRujukanM;
                $namaModel='SANilaiRujukanM';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                     
                $tableJk ='<table class="table">
                    <tr>';
                $jenisKelamin=JenisKelamin::items();
                $jumlahJK=count($jenisKelamin);
                foreach($jenisKelamin AS $dataJK):
//                    $sqlJK="SELECT * FROM nilairujukan_m WHERE nilairujukan_jeniskelamin='$dataJK'
//                              AND pemeriksaanlab_id=$id"; 
                    $sqlJK="SELECT * FROM nilairujukan_m, pemeriksaanlab_m WHERE nilairujukan_jeniskelamin='$dataJK'
                              AND pemeriksaanlab_id=$id";       
                    $dataJKSQL=Yii::app()->db->createCommand($sqlJK)->query();
                    if(COUNT($dataJKSQL)>0)
                    {
                        $cek='TRUE';
                    }
                    else
                    {
                         $cek='FALSE';
                    }           
                    $tableJk .="<td width='50%'>".CHtml::checkBox(''.$namaModel.'[nilairujukan_jeniskelamin][]',$cek,array('value'=>$dataJK,'onclick'=>'checkIni(this);', 'checked'=>'checked'))." ".$dataJK."
                            <table id='jenis'>";
                        $kelompokUmur=KelompokUmur::items();
                    foreach($kelompokUmur AS $dataKelUmur):
//                          $sql="SELECT * FROM nilairujukan_m WHERE nilairujukan_jeniskelamin='".$dataJK."' AND kelompokumur='".$dataKelUmur."'
//                              AND pemeriksaanlab_id=".$id.""; 
                          // $sql="SELECT * FROM nilairujukan_m, pemeriksaanlab_m WHERE nilairujukan_jeniskelamin='".$dataJK."' AND kelompokumur='".$dataKelUmur."'
                          //     AND pemeriksaanlab_id=".$id.""; 
                          $sql="select * from nilairujukan_m JOIN pemeriksaanlabdet_m ON pemeriksaanlabdet_m.nilairujukan_id = nilairujukan_m.nilairujukan_id
                              JOIN pemeriksaanlab_m ON pemeriksaanlabdet_m.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id 
                              JOIN jenispemeriksaanlab_m ON pemeriksaanlab_m.jenispemeriksaanlab_id = jenispemeriksaanlab_m.jenispemeriksaanlab_id WHERE  nilairujukan_m.nilairujukan_jeniskelamin='".$dataJK."' AND nilairujukan_m.kelompokumur='".$dataKelUmur."'
                              AND pemeriksaanlab_m.pemeriksaanlab_id = ".$id." AND nilairujukan_m.kelompokdet = jenispemeriksaanlab_m.jenispemeriksaanlab_nama"; 

                          $dataNilaiRujukan=Yii::app()->db->createCommand($sql)->query();

                          // echo print_r($dataNilaiRujukan);exit;
                          if(COUNT($dataNilaiRujukan)>0){
                              foreach ($dataNilaiRujukan AS $tampil):
                                     $tableJk .="<tr><td>".CHtml::hiddenField(''.$namaModel.'[nilairujukan_id]['.$dataJK.'][]',$tampil['nilairujukan_id'],array('readonly'=>TRUE)).
                                      $dataKelUmur.CHtml::hiddenField(''.$namaModel.'[kelompokumur]['.$dataJK.'][]',$dataKelUmur,array('readonly'=>TRUE))."</td>
                                                  <td>".CHtml::textField(''.$namaModel.'[nilairujukan_min]['.$dataJK.'][]',$tampil['nilairujukan_min'],array('class'=>'span1','group'=>'min', 'placeholder'=>$model->getAttributeLabel('nilairujukan_min'), 'onkeyup'=>'samaMin(this);')).
                                                    CHtml::textField(''.$namaModel.'[nilairujukan_max]['.$dataJK.'][]',$tampil['nilairujukan_max'],array('class'=>'span1','group'=>'max', 'placeholder'=>$model->getAttributeLabel('nilairujukan_max'), 'onkeyup'=>'samaMax(this);'))."</td>
                                              <td>".CHtml::textField(''.$namaModel.'[nilairujukan_nama]['.$dataJK.'][]',$tampil['nilairujukan_nama'],array('class'=>'span2', 'placeholder'=>$model->getAttributeLabel('nilairujukan_nama'), 'group'=>'nama'))."</td>
                                          </tr>";
                              $model->nilairujukan_keterangan=$tampil['nilairujukan_keterangan'];
                              $model->nilairujukan_metode=$tampil['nilairujukan_metode'];
                              $model->nilairujukan_satuan=$tampil['nilairujukan_satuan'];
                              endforeach;
                           }
                          else{
                                     $tableJk .="<tr><td>".CHtml::hiddenField(''.$namaModel.'[nilairujukan_id]['.$dataJK.'][]','',array('readonly'=>TRUE)).
                                             $dataKelUmur.CHtml::hiddenField(''.$namaModel.'[kelompokumur]['.$dataJK.'][]',$dataKelUmur,array('readonly'=>TRUE))."</td>
                                              <td>".CHtml::textField(''.$namaModel.'[nilairujukan_min]['.$dataJK.'][]','',array('class'=>'span1','group'=>'min', 'placeholder'=>$model->getAttributeLabel('nilairujukan_min'),'onkeyup'=>'samaMin(this);')).
                                                CHtml::textField(''.$namaModel.'[nilairujukan_max]['.$dataJK.'][]','',array('class'=>'span1', 'group'=>'max','placeholder'=>$model->getAttributeLabel('nilairujukan_max'), 'onkeyup'=>'samaMax(this);'))."</td>
                                                  <td>".CHtml::textField(''.$namaModel.'[nilairujukan_nama]['.$dataJK.'][]','',array('class'=>'span2','placeholder'=>$model->getAttributeLabel('nilairujukan_nama'), 'group'=>'nama'))."</td>
                                              </tr>";
                             } 
                    endforeach;
                    $tableJk .="      
                            </table>
                            </td>";    
                endforeach;    
                 
                 $tableJk .='</tr>   
                </table>';
                 
//==================================Proses Update==========================================================================                 
                if(isset($_POST['SANilaiRujukanM'])){
                    $transcaction=Yii::app()->db->beginTransaction();
                    try {
                            $modPemeriksaanLab->attributes=$_POST['SAPemeriksaanLABM'];
                            $modPemeriksaanLab->pemeriksaanlab_aktif=TRUE;
                            $modPemeriksaanLab->save();

                            $jumlahJenisKelaminNilaiRujukan=COUNT($_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin']);
                           
                            for($i=0; $i<=$jumlahJenisKelaminNilaiRujukan; $i++):
                               if ($_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i]!=''){
                             
                                   $jenisKelamin=$_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i];
                                   $jumlahKelompokUmur=COUNT($_POST['SANilaiRujukanM']['kelompokumur'][''.$_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i].'']);
                                   for($j=0; $j<=$jumlahKelompokUmur; $j++):

                                    if(($_POST['SANilaiRujukanM']['nilairujukan_nama'][$jenisKelamin][$j]!='')
                                            AND ($_POST['SANilaiRujukanM']['nilairujukan_id'][$jenisKelamin][$j]!='')){//Jika data Lama dan User Mengubahnya atau membiarkannya tetap ada
                                        $model=SANilaiRujukanM::model()->findByPk($_POST['SANilaiRujukanM']['nilairujukan_id'][$jenisKelamin][$j]);
                                        $model->attributes=$_POST['SANilaiRujukanM'];
                                        $model->nilairujukan_jeniskelamin=$_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i];
                                        $model->pemeriksaanlab_id=$modPemeriksaanLab->pemeriksaanlab_id;
                                        $model->kelompokumur=$_POST['SANilaiRujukanM']['kelompokumur'][$jenisKelamin][$j];
                                        $model->nilairujukan_min=$_POST['SANilaiRujukanM']['nilairujukan_min'][$jenisKelamin][$j];
                                        $model->nilairujukan_max=$_POST['SANilaiRujukanM']['nilairujukan_max'][$jenisKelamin][$j];
                                        $model->nilairujukan_nama=$_POST['SANilaiRujukanM']['nilairujukan_nama'][$jenisKelamin][$j];
                                        $model->nilairujukan_aktif=TRUE;
                                        if($model->validate())
                                        {
                                         $model->save(); 
                                        }
                                    }
                                    else if(($_POST['SANilaiRujukanM']['nilairujukan_nama'][$jenisKelamin][$j]=='')
                                            and !empty($_POST['SANilaiRujukanM']['nilairujukan_id'][$_POST['SANilaiRujukanM']['nilairujukan_jeniskelamin'][$i]][$j])){//Jika data Lama dan User Menghapusnya
                                            $hapusNilaiRujukan=SANilaiRujukanM::model()->deleteByPk($_POST['SANilaiRujukanM']['nilairujukan_id'][$jenisKelamin][$j]);
                                    }
                                    else if(($_POST['SANilaiRujukanM']['nilairujukan_nama'][$jenisKelamin][$j]!='')
                                            and empty($_POST['SANilaiRujukanM']['nilairujukan_id']['LAKI - LAKI'][$j])){//Jika data Lama dan User Menghapusnya
       
                                        $model=new SANilaiRujukanM;
                                        $model->attributes=$_POST['SANilaiRujukanM'];
                                        $model->nilairujukan_jeniskelamin=$jenisKelamin;
                                        $model->pemeriksaanlab_id=$modPemeriksaanLab->pemeriksaanlab_id;
                                        $model->kelompokumur=$_POST['SANilaiRujukanM']['kelompokumur'][$jenisKelamin][$j];
                                        $model->nilairujukan_min=$_POST['SANilaiRujukanM']['nilairujukan_min'][$jenisKelamin][$j];
                                        $model->nilairujukan_max=$_POST['SANilaiRujukanM']['nilairujukan_max'][$jenisKelamin][$j];
                                        $model->nilairujukan_nama=$_POST['SANilaiRujukanM']['nilairujukan_nama'][$jenisKelamin][$j];
                                        $model->nilairujukan_aktif=TRUE;
                                        if($model->validate())
                                        {
                                          $model->save(); 
                                        }
                                    }
                                    else{
                                    }
                                 endfor; 
                                 $j=0;
                            }
                            endfor;
                            $transcaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');   
                            $this->redirect(array('admin','id'=>$modPemeriksaanLab->pemeriksaanlab_id));

                    }
                    catch (Exception $ext){
                         $transcaction->rollback();
                         Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ext,true));  
                    }
                }  
//===============================Akhir Proses Update=====================================================================================================		

		$this->render('update',array(
			'model'=>$model,'modPemeriksaanLab'=>$modPemeriksaanLab,'tableJk'=>$tableJk
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $transcaction=Yii::app()->db->beginTransaction();
                        try {
                        //$hapusNilaiRujukan=SANilaiRujukanM::model()->deleteAll('pemeriksaanlab_id='.$id.'');    
                            
                        $this->loadModel($id)->delete();
                        $transcaction->commit();
                        }
                        catch (Exception $ext){
                             $transcaction->rollback();
                             Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ext,true));  
                        }

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SAPemeriksaanLABM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SAPemeriksaanLABM('search');
    $modPemeriksaanLab = new SAPemeriksaanLABM;
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SAPemeriksaanLABM']))
                                {
			$model->attributes=$_GET['SAPemeriksaanLABM'];
                                $model->daftartindakan_nama = $_GET['SAPemeriksaanLABM']['daftartindakan_nama'];
                                $model->jenispemeriksaanlab_nama = $_GET['SAPemeriksaanLABM']['jenispemeriksaanlab_nama'];
                        }

		$this->render('admin',array(
			'model'=>$model,
      'modPemeriksaanLab'=>$modPemeriksaanLab,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SAPemeriksaanLABM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sapemeriksaan-labm-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                SAPemeriksaanLABM::model()->updateByPk($id, array('pemeriksaanlab_aktif'=>false));
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new PemeriksaanlabM;
            $model->attributes=$_REQUEST['PemeriksaanlabM'];
            $judulLaporan='Data PemeriksaanlabM';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
