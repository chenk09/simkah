<?php

class RuanganMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
                public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print','removeTemporary',
                                'createJenisKasusPenyakit','createKelasRuangan'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','print','removeTemporary','createPegawaiRuangan',
                                'createJenisKasusPenyakit','createKelasRuangan','createDaftarTindakan'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        
        
        public function actionCreatePegawaiRuangan()
	{
           if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                           
           $model=new RuanganpegawaiM; 
                if(isset($_POST['RuanganpegawaiM']))
                    {
                       
                        $transaction = Yii::app()->db->beginTransaction();
                            try {
                                    $jumlahRuanganPegawai=COUNT($_POST['pegawai_id']);
                                    $ruangan_id=$_POST['RuanganpegawaiM']['ruangan_id'];
                                    $hapusTindakanRuangan=RuanganpegawaiM::model()->deleteAll('ruangan_id='.$ruangan_id.''); 
                                    for($i=0; $i<=$jumlahRuanganPegawai; $i++)
                                        {
                                            $modRuanganPegawai = new RuanganpegawaiM;
                                            $modRuanganPegawai->ruangan_id=$ruangan_id;
                                            $modRuanganPegawai->pegawai_id=$_POST['pegawai_id'][$i];
                                            $modRuanganPegawai->save();
                                            
                                        }
                                        
                                         Yii::app()->user->setFlash('success', "Data Ruangan Dan Pegawai Berhasil Disimpan");
                                         $transaction->commit();
                                         $this->redirect(array('admin'));
                                }   
                            catch (Exception $e)
                                {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error', "Data Ruangan Dan Pegawai Gagal Disimpan");
                                }     
                    }
           $this->render('createRuanganPegawai',array('model'=>$model
		));
	}
        
        public function actionCreateDaftarTindakan()
	{
           if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                           
           $model=new TindakanruanganM; 
                if(isset($_POST['TindakanruanganM']))
                    {
                       
                        $transaction = Yii::app()->db->beginTransaction();
                            try {
                                    $jumlahTindakanRuangan=COUNT($_POST['daftartindakan_id']);
                                    $ruangan_id=$_POST['TindakanruanganM']['ruangan_id'];
                                    $hapusTindakanRuangan=TindakanruanganM::model()->deleteAll('ruangan_id='.$ruangan_id.''); 
                                    for($i=0; $i<=$jumlahTindakanRuangan; $i++)
                                        {
                                            $modTindakanRuangan = new TindakanruanganM;
                                            $modTindakanRuangan->ruangan_id=$ruangan_id;
                                            $modTindakanRuangan->daftartindakan_id=$_POST['daftartindakan_id'][$i];
                                            $modTindakanRuangan->save();
                                            
                                        }
                                        
                                         Yii::app()->user->setFlash('success', "Data Ruangan Dan Kelas Ruangan Berhasil Disimpan");
                                         $transaction->commit();
                                         $this->redirect(array('admin'));
                                }   
                            catch (Exception $e)
                                {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error', "Data Ruangan Dan Kelas Ruangan Gagal Disimpan");
                                }     
                    }
           $this->render('createTindakanRuangan',array('model'=>$model
		));
	}
        
        public function actionCreateKelasRuangan()
	{
           if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                           
           $model=new KelasruanganM; 
                if(isset($_POST['KelasruanganM']))
                    {
                       
                        $transaction = Yii::app()->db->beginTransaction();
                            try {
                                    $jumlahKelasPelayanan=COUNT($_POST['kelaspelayanan_id']);
                                    $ruangan_id=$_POST['KelasruanganM']['ruangan_id'];
                                    $hapuskelasRuangan=KelasruanganM::model()->deleteAll('ruangan_id='.$ruangan_id.''); 
                                    for($i=0; $i<=$jumlahKelasPelayanan; $i++)
                                        {
                                            $modKasusRuangan = new KelasruanganM;
                                            $modKasusRuangan->ruangan_id=$ruangan_id;
                                            $modKasusRuangan->kelaspelayanan_id=$_POST['kelaspelayanan_id'][$i];
                                            $modKasusRuangan->save();
                                            
                                        }
                                        
                                         Yii::app()->user->setFlash('success', "Data Ruangan Dan Kelas Ruangan Berhasil Disimpan");
                                         $transaction->commit();
                                         $this->redirect(array('admin'));
                                }   
                            catch (Exception $e)
                                {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error', "Data Ruangan Dan Kelas Ruangan Gagal Disimpan");
                                }     
                    }
           $this->render('createKelasRuangan',array('model'=>$model
		));
	}
        
        public function actionCreateJenisKasusPenyakit()
	{
           if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                  
           $model=new KasuspenyakitruanganM; 
                if(isset($_POST['KasuspenyakitruanganM']))
                    {
                       
                        $transaction = Yii::app()->db->beginTransaction();
                            try {
                                    $jumlahJenisKasusPenyakit=COUNT($_POST['jeniskasuspenyakit_id']);
                                    $ruangan_id=$_POST['KasuspenyakitruanganM']['ruangan_id'];
                                    $hapusKasusPenyakitRuangan=KasuspenyakitruanganM::model()->deleteAll('ruangan_id='.$ruangan_id.''); 
                                    for($i=0; $i<=$jumlahJenisKasusPenyakit; $i++)
                                        {
                                            $modKasusRuangan = new KasuspenyakitruanganM;
                                            $modKasusRuangan->ruangan_id=$ruangan_id;
                                            $modKasusRuangan->jeniskasuspenyakit_id=$_POST['jeniskasuspenyakit_id'][$i];
                                            $modKasusRuangan->save();
                                            
                                        }
                                        
                                         Yii::app()->user->setFlash('success', "Data Ruangan Dan Jenis Kasus Penyakit Berhasil Disimpan");
                                         $transaction->commit();
                                         $this->redirect(array('admin'));
                                }   
                            catch (Exception $e)
                                {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error', "Data Ruangan Dan Jenis Kasus Penyakit Gagal Disimpan");
                                }     
                    }
           $this->render('createJenisKasusPenyakit',array('model'=>$model
		));
	}
        
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
              
		$this->render('view',array(
			'model'=>$this->loadModel($id),
                        'modKasusPenyakitRuangan'=>$modKasusPenyakitRuangan,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                  
		$model=new SARuanganM;
                $modRiwayatRuangan=new SARiwayatRuanganR;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                if(isset($_POST['SARuanganM']))
                {
                  $transaction = Yii::app()->db->beginTransaction();
                  try {
                        $modRiwayatRuangan=new SARiwayatRuanganR; 
                        $modRiwayatRuangan->attributes=$_POST['SARiwayatRuanganR'];
                        $modRiwayatRuangan->save();
                        $valid=true;
                        foreach($_POST['SARuanganM'] as $i=>$item):
                            if(is_integer($i)) {
                              $model=new SARuanganM;
                              $random=rand(0000000,9999999);
                              $model->attributes=$_POST['SARuanganM'][$i];
                              $model->instalasi_id=$_POST['instalasi_id'];
                              $model->ruangan_image = CUploadedFile::getInstance($model, '['.$i.']ruangan_image');
                              $model->riwayatruangan_id=$modRiwayatRuangan->riwayatruangan_id;
                              $gambar=$model->ruangan_image;
                              if(!empty($model->ruangan_image)){//Klo User Memasukan Logo

                                     $model->ruangan_image = $random.$model->ruangan_image;
                                     Yii::import("ext.EPhpThumb.EPhpThumb");
                                     $thumb=new EPhpThumb();
                                     $thumb->init(); //this is needed
                                     $fullImgName =$model->ruangan_image;   
                                     $fullImgSource = Params::pathRuanganDirectory().$fullImgName;
                                     $fullThumbSource = Params::pathRuanganTumbsDirectory().'kecil_'.$fullImgName;
                                     $model->ruangan_image = $fullImgName;
                                     if($model->save()){
                                               $gambar->saveAs($fullImgSource);
                                               $thumb->create($fullImgSource)
                                                     ->resize(200,200)
                                                     ->save($fullThumbSource);
                                     }
                              }else{//Klo User Tidak Memasukan Logo
                                  if($model->save()){
                                      $transaction->commit();
                                       Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                       $this->redirect(array('admin','id'=>$model->ruangan_id));
                                  }
                               }
                           }
                       endforeach;
                       
                       
                    } catch(Exception $exc){
                               $transaction->rollback();
                               Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data Gagal disimpan'.MyExceptionMessage::getMessage($exc,true).'');

                      }
                }   
		

		$this->render('create',array(
			'model'=>$model,'modRiwayatRuangan'=>$modRiwayatRuangan
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                             
		$model=$this->loadModel($id);
                $modKasusPenyakitRuangan=KasuspenyakitruanganM::model()->findAll('ruangan_id='.$id.'');
                $modKelasRuangan=KelasruanganM::model()->findAll('ruangan_id='.$id.'');
                $modTindakanRuangan=TindakanruanganM::model()->findAll('ruangan_id='.$id.'');
                $modRuanganPegawai=RuanganpegawaiM::model()->findAll('ruangan_id='.$id.'');
                $modRiwayatRuangan=RiwayatruanganR::model()->findByPk($model->riwayatruangan_id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                  
		if(isset($_POST['SARuanganM']))
		{
                    
                        $transaction = Yii::app()->db->beginTransaction();
                            try {
                                    $model->attributes=$_POST['SARuanganM'];
                                    $model->save();
                                    $jumlahKelasPelayanan=COUNT($_POST['kelaspelayanan_id']);
                                    $jumlahJenisKasusPenyakit=COUNT($_POST['jeniskasuspenyakit_id']);
                                    $jumlahDaftarTindakan=COUNT($_POST['daftartindakan_id']);
                                    $jumlahRuanganPegawai=COUNT($_POST['pegawai_id']);

    
                                    $ruangan_id=$model->ruangan_id;
                                    $hapusKasusPenyakitRuangan=KasuspenyakitruanganM::model()->deleteAll('ruangan_id='.$ruangan_id.''); 
                                    $hapuskelasRuangan=KelasruanganM::model()->deleteAll('ruangan_id='.$ruangan_id.''); 
                                    $hapusTindakanRuangan=TindakanruanganM::model()->deleteAll('ruangan_id='.$ruangan_id.''); 
                                    $hapusRuanganPegawai=RuanganpegawaiM::model()->deleteAll('ruangan_id='.$ruangan_id.''); 

                                    if($jumlahJenisKasusPenyakit>0)
                                        {
                                            for($i=0; $i<=$jumlahJenisKasusPenyakit; $i++)
                                                {
                                                    $modKasusRuangan = new KasuspenyakitruanganM;
                                                    $modKasusRuangan->ruangan_id=$ruangan_id;
                                                    $modKasusRuangan->jeniskasuspenyakit_id=$_POST['jeniskasuspenyakit_id'][$i];
                                                    $modKasusRuangan->save(); 
                                                }
                                        }
                                        
                                    if($jumlahKelasPelayanan>0)
                                        {    
                                            for($i=0; $i<=$jumlahKelasPelayanan; $i++)
                                                {
                                                    $modKasusRuangan = new KelasruanganM;
                                                    $modKasusRuangan->ruangan_id=$ruangan_id;
                                                    $modKasusRuangan->kelaspelayanan_id=$_POST['kelaspelayanan_id'][$i];
                                                    $modKasusRuangan->save();
                                                }
                                        }
                                        
                                      if($jumlahDaftarTindakan>0)
                                        {    
                                        
                                              for($j=0; $j<=$jumlahDaftarTindakan; $j++)
                                                {
                                                    $modTindakanRuangan = new TindakanruanganM;
                                                    $modTindakanRuangan->ruangan_id=$ruangan_id;
                                                    $modTindakanRuangan->daftartindakan_id=$_POST['daftartindakan_id'][$j];
                                                    $modTindakanRuangan->save();
                                                }
                                        }
                                        
                                      if($jumlahRuanganPegawai>0)
                                        {    
                                        
                                              for($j=0; $j<=$jumlahRuanganPegawai; $j++)
                                                {
                                                    $modRuanganPegawai = new RuanganpegawaiM;
                                                    $modRuanganPegawai->ruangan_id=$ruangan_id;
                                                    $modRuanganPegawai->pegawai_id=$_POST['pegawai_id'][$j];
                                                    $modRuanganPegawai->save();
                                                }
                                        }
                                        
                                        
                                        
                                        
                                         Yii::app()->user->setFlash('success', "Data Ruangan Dan Jenis Kasus Penyakit Berhasil Disimpan");
                                         $transaction->commit();
                                         $this->redirect(array('admin'));
                                }   
                            catch (Exception $e)
                                {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error', "Data Ruangan Dan Jenis Kasus Penyakit Gagal Disimpan");
                                }   
			
		}

		$this->render('update',array(
			'model'=>$model,
                        'modKasusPenyakitRuangan'=>$modKasusPenyakitRuangan,
                        'modKelasRuangan'=>$modKelasRuangan,
                        'modTindakanRuangan'=>$modTindakanRuangan,
                        'modRuanganPegawai'=>$modRuanganPegawai,
                        'modRiwayatRuangan'=>$modRiwayatRuangan
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SARuanganM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                         
		$model=new SARuanganM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SARuanganM']))
			$model->attributes=$_GET['SARuanganM'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SARuanganM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='saruangan-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
         public function actionDelete()
				{              
					//if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
					if(Yii::app()->request->isPostRequest)
					{
                                                $id = $_POST['id'];
                                                $this->loadModel($id)->delete();
                                                if (Yii::app()->request->isAjaxRequest)
                                                    {
                                                        echo CJSON::encode(array(
                                                            'status'=>'proses_form', 
                                                            'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                                                            ));
                                                        exit;               
                                                    }
				                    
						// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
						if(!isset($_GET['ajax']))
							$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
					}
					else
						throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
				}
        
                 /**
                 *Mengubah status aktif
                 * @param type $id 
                 */
                public function actionRemoveTemporary()
                {
                            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        //                    SAPropinsiM::model()->updateByPk($id, array('propinsi_aktif'=>false));
        //                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                          
                    
                    $id = $_POST['id'];   
                    if(isset($_POST['id']))
                    {
                       $update = SARuanganM::model()->updateByPk($id,array('ruangan_aktif'=>false));
                       if($update)
                        {
                            if (Yii::app()->request->isAjaxRequest)
                            {
                                echo CJSON::encode(array(
                                    'status'=>'proses_form', 
                                    ));
                                exit;               
                            }
                         }
                    } else {
                            if (Yii::app()->request->isAjaxRequest)
                            {
                                echo CJSON::encode(array(
                                    'status'=>'proses_form', 
                                    ));
                                exit;               
                            }
                    }

               }
        
        public function actionPrint()
        {
            if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                                    
            $model= new SARuanganM();
            $model->attributes=$_REQUEST['SARuanganM'];
            $judulLaporan='Laporan Ruangan';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                Yii::app()->bootstrap->coreCss = false;
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
