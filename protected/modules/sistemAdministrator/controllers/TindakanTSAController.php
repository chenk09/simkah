<?php
/**
 * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
 * di copy dari TindakanTRController pada 18 Des 2013
 * Di extend sebagian (tidak semua) karena terlalu kompleks dan ada algoritma yang berbeda
 * Beberapa view menggunakan view "rawatInap.views.tindakanTRI."
 * Beberapa model import dari "rawatInap.models"
 */
Yii::import('rawatInap.models.RIPendaftaranT');
Yii::import('rawatInap.models.RIPasienM');
Yii::import('rawatInap.models.RITindakanPelayananT');
Yii::import('rawatInap.models.RIObatAlkesM');
Yii::import('rawatInap.controllers.TindakanTRIController');
class TindakanTSAController extends TindakanTRIController
{
        protected $pathViewRI = 'rawatInap.views.tindakanTRI.';

	public function actionIndex()
	{
            $modAdmisi = new PasienadmisiT;
            $modPendaftaran = new RIPendaftaranT;
            $modPasien = new RIPasienM;
            $modTindakan = new RITindakanPelayananT;
            $modViewTindakans = array();
            $modViewBmhp = array();
            
            if(isset($_POST['RITindakanPelayananT']) && isset($_POST['RIPendaftaranT']))
            {
                $modPendaftaran = RIPendaftaranT::model()->findByPk($_POST['RIPendaftaranT']['pendaftaran_id']);
                $modPasien = RIPasienM::model()->findByPk($_POST['RIPendaftaranT']['pasien_id']);
                $modAdmisi->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                $ruanganTindakanId = $_POST['ruanganTindakanId'];
                $modTindakans = $this->saveTindakan($modPasien, $modPendaftaran, $modAdmisi,$ruanganTindakanId);
                if($this->succesSave){
                    //set null pembayaran supaya muncul di informasi belum bayar
                    PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id, 
                        array('pembayaranpelayanan_id'=>null)
                    );
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    $this->refresh();
                }else{
                    Yii::app()->user->setFlash('error',"Data gagal disimpan");
                }
            }
            //=== copy ===
                if (isset($_GET['FilterForm'])){
                    $_GET['test'] = true;
                    $_GET['term'] = $_GET['FilterForm'];
                }
                if (isset($_GET['FilterForm2'])){
                    $_GET['test'] = true;
                    $_GET['term2'] = $_GET['FilterForm2'];
                }
                if (isset($_GET['FilterForm3'])){
                    $_GET['test'] = true;
                    $_GET['term3'] = $_GET['FilterForm3'];
                }
                if (isset($_GET['FilterForm4'])){
                    $_GET['test'] = true;
                    $_GET['term4'] = $_GET['FilterForm4'];
                }
                if (isset($_GET['test'])){
                    $_GET['term'] = (isset($_GET['term'])) ? $_GET['term'] : null;
                    $_GET['term2'] = (isset($_GET['term2'])) ? $_GET['term2'] : null;
                    $_GET['term3'] = (isset($_GET['term3'])) ? $_GET['term3'] : null;
                    $_GET['term4'] = (isset($_GET['term4'])) ? $_GET['term4'] : null;
                    if(Yii::app()->request->isAjaxRequest) {
                    if($_GET['idTipePaket']==Params::TIPEPAKET_LUARPAKET){
                        $criteria = new CDbCriteria();
                        $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['term']), true);
                        $criteria->compare('LOWER(daftartindakan_kode)', strtolower($_GET['term2']), true);
                        $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['term3']), true);
                        if(Yii::app()->user->getState('tindakanruangan'))
                            $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                        if(Yii::app()->user->getState('tindakankelas'))
                            $criteria->compare('kelaspelayanan_id', $_GET['idKelasPelayanan']);
                        $criteria->compare('tipepaket_id', Params::TIPEPAKET_LUARPAKET);
                        $criteria->order = 'daftartindakan_nama';
                        $models = 'PaketpelayananV';
                    } else if($_GET['idTipePaket']==Params::TIPEPAKET_NONPAKET) {
                        $criteria = new CDbCriteria();
                        $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['term']), true);
                        $criteria->compare('LOWER(daftartindakan_kode)', strtolower($_GET['term2']), true);
                        $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['term3']), true);
                        $criteria->order = "daftartindakan_kode ASC, daftartindakan_nama ASC, substring(daftartindakan_kode from '.....$') ASC";
                        if(Yii::app()->user->getState('tindakankelas'))
                            $criteria->compare('kelaspelayanan_id', $_GET['idKelasPelayanan']);
                        if(Yii::app()->user->getState('tindakanruangan')){
                            $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                            $models = 'TariftindakanperdaruanganV';
                        } else {
                            $models = 'TariftindakanperdaV';
                        }
                    } else {
                        $criteria = new CDbCriteria();
                        $criteria->compare('LOWER(daftartindakan_kode)', strtolower($_GET['daftartindakan_kode']), true);
                        $criteria->compare('LOWER(daftartindakan_nama)', strtolower($_GET['daftartindakan_nama']), true);
                        $criteria->compare('LOWER(kategoritindakan_nama)', strtolower($_GET['kategoritindakan_nama']), true);
                        if(Yii::app()->user->getState('tindakanruangan'))
                            $criteria->compare('ruangan_id', Yii::app()->user->getState('ruangan_id'));
                        if(Yii::app()->user->getState('tindakankelas'))
                            $criteria->compare('kelaspelayanan_id', $_GET['idKelasPelayanan']);
                        $criteria->compare('tipepaket_id', $_GET['idTipePaket']);
                        $criteria->order = 'daftartindakan_nama';

                        $models = 'PaketpelayananV';
                    }

                    $dataProvider = new CActiveDataProvider($models, array(
                            'criteria'=>$criteria,
                    ));
                    $route = Yii::app()->createUrl($this->route, array('idPendaftaran'=>$idPendaftaran, 'idAdmisi'=>$idAdmisi, 'idKelasPelayanan'=>$_GET['idKelasPelayanan'], 'idTipePaket'=>$_GET['idTipePaket']));
                    $route2 = Yii::app()->createUrl($this->route, array('idPendaftaran'=>$idPendaftaran, 'idAdmisi'=>$idAdmisi, 'idKelasPelayanan'=>$_GET['idKelasPelayanan'], 'idTipePaket'=>$_GET['idTipePaket']));
                    $route3 = Yii::app()->createUrl($this->route, array('idPendaftaran'=>$idPendaftaran, 'idAdmisi'=>$idAdmisi, 'idKelasPelayanan'=>$_GET['idKelasPelayanan'], 'idTipePaket'=>$_GET['idTipePaket']));
                    $route4 = Yii::app()->createUrl($this->route, array('idPendaftaran'=>$idPendaftaran, 'idAdmisi'=>$idAdmisi, 'idKelasPelayanan'=>$_GET['idKelasPelayanan'], 'idTipePaket'=>$_GET['idTipePaket']));
                //=== end copy ====
                $this->renderPartial($this->pathViewRI.'_daftarTindakanPaket', array('dataProvider'=>$dataProvider,'models'=>$models, 'route'=>$route,'route2'=>$route2,'route3'=>$route3,'route4'=>$route4));
                Yii::app()->end();
                }
            }
            
            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modViewTindakans'=>$modViewTindakans,
                                        'modViewBmhp'=>$modViewBmhp,
                                        'modTindakans'=>$modTindakans,
                                        'modTindakan'=>$modTindakan,
                                        'modAdmisi'=>$modAdmisi));
	}
        /**
         * saveTindakan sama dengan yang di rawatInap/TindakanTRIbedanya ada params $ruanganTindakanId saja
         * @param type $modPasien
         * @param type $modPendaftaran
         * @param type $modAdmisi
         * @param type $ruanganTindakanId
         * @return \RITindakanPelayananT
         */
        public function saveTindakan($modPasien,$modPendaftaran,$modAdmisi,$ruanganTindakanId)
        {
            $post = isset($_POST['TindakanpelayananT']) ? $_POST['TindakanpelayananT'] : $_POST['RITindakanPelayananT'];
            $valid=true; //echo $_POST['RITindakanPelayananT'][0]['tipepaket_id'];exit;
            foreach($post as $i=>$item)
            {
                if(!empty($item) && (!empty($item['daftartindakan_id']))){
                    $modTindakans[$i] = new RITindakanPelayananT;
                    $modTindakans[$i]->attributes=$item;
                    $modTindakans[$i]->tipepaket_id = $_POST['RITindakanPelayananT'][0]['tipepaket_id'];
                    $modTindakans[$i]->pasien_id = $modPasien->pasien_id;
                    $modTindakans[$i]->pasienadmisi_id = $modAdmisi->pasienadmisi_id;
                    $modTindakans[$i]->kelaspelayanan_id = $modAdmisi->kelaspelayanan_id;
                    $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                    $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                    $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                    $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                    $modTindakans[$i]->tgl_tindakan = $item['tgl_tindakan'];
                    $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                    $modTindakans[$i]->tarif_tindakan = $modTindakans[$i]->tarif_satuan * $modTindakans[$i]->qty_tindakan;
                    if($item['cyto_tindakan'])
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['persenCyto'] / 100) * $modTindakans[$i]->tarif_tindakan;
                    else
                        $modTindakans[$i]->tarifcyto_tindakan = 0;
                    $modTindakans[$i]->discount_tindakan = 0;
                    $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                    $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                    $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                    $modTindakans[$i]->iurbiaya_tindakan = 0;
                    $modTindakans[$i]->instalasi_id = $modPendaftaran->instalasi_id;
                    $modTindakans[$i]->ruangan_id =  $ruanganTindakanId;
                    $modTindakans[$i]->alatmedis_id = $this->cekAlatmedis($modTindakans[$i]->daftartindakan_id);
                    
                    $tarifTindakan= TariftindakanM::model()->findAll('daftartindakan_id='.$item['daftartindakan_id'].' AND kelaspelayanan_id='.$modTindakans[$i]->kelaspelayanan_id.'');
                    
                    
                    foreach($tarifTindakan AS $dataTarif):
                        
                        if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                                $modTindakans[$i]->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                            }
                        if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                                $modTindakans[$i]->tarif_medis=$dataTarif['harga_tariftindakan'];
                            }
                        if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                                $modTindakans[$i]->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                            }
                        if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                                $modTindakans[$i]->tarif_bhp=$dataTarif['harga_tariftindakan'];
                            }
                    endforeach;
                    
                    $valid = $modTindakans[$i]->validate() && $valid;
                }
            }
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if($valid && (count($modTindakans) > 0)){
                    foreach($modTindakans as $i=>$tindakan){
                        $tindakan->save();
                        $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
                        if(isset($_POST['paketBmhp'])){
                            $modObatPasiens = $this->savePaketBmhp($modPendaftaran, $_POST['paketBmhp'],$tindakan);
                        }
                        if(isset($_POST['pemakaianBahan'])){
                            $modPemakainBahans = $this->savePemakaianBahan($modPendaftaran, $_POST['pemakaianBahan'],$tindakan);
                        }
                    }
                    if($statusSaveKomponen && $this->successSaveBmhp && $this->successSavePemakaianBahan) {
                        $transaction->commit();
                        $this->succesSave = true;
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data tidak valid ");
                        //Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceObatAlkesPasien($modPemakainBahans));
                    }
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data tidak valid ");
                    //Yii::app()->user->setFlash('error',"Data tidak valid ".$this->traceTindakan($modTindakans));
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
            }
            
            return $modTindakans;
        }
}