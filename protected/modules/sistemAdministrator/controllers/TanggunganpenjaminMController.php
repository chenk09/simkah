
<?php

class TanggunganpenjaminMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SATanggunganpenjaminM;

		if(isset($_POST['SATanggunganpenjaminM']))
		{
                    $model->attributes=$_POST['SATanggunganpenjaminM'];
                    $modTanggung = $this->validasiTabular($_POST['TanggunganpenjaminM']);
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                            $success = 0;
                            $list = CHtml::listData(TanggunganpenjaminM::model()->findAllByAttributes(array('carabayar_id'=>$model->carabayar_id,'kelaspelayanan_id'=>$model->kelaspelayanan_id)),'tanggunganpenjamin_id','tanggunganpenjamin_id');
                            foreach ($modTanggung as $i=>$row){
                                if ($row->save()){
                                    unset($list[$row->tanggunganpenjamin_id]);
                                    $success++;
                                }
                            }
                            
                            if (count($list) > 0){
                                foreach ($list as $isi){
                                    TanggunganpenjaminM::model()->deleteByPk($isi);
                                }
                            }

                            if ((count($modTanggung) > 0) && ($success == count($modTanggung))) {
                                $transaction->commit();
                                Yii::app()->user->setFlash('success', "Data Berhasil Disimpan ");
                                $this->redirect(Yii::app()->createUrl($this->module->id.'/'.TanggunganpenjaminM.'/admin'));
                            }
                            else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                            }
                        } catch (Exception $exc) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
                        }
                    }

		$this->render('create',array(
			'model'=>$model, 'modTanggung'=>$modTanggung
		));
	}
        
        protected function validasiTabular($data){
            $x = 0;
            foreach ($data as $i=>$row){
                if (!empty($row['tanggunganpenjamin_id'])){
                    $paket[$x] = TanggunganpenjaminM::model()->findByPk($row['tanggunganpenjamin_id']);
                    $paket[$x]->attributes = $row;
//                    $paket[$x]->subsidipemerintahtind = $row['subsidipemerintahtind'];
                }else{
                    $paket[$x] = new TanggunganpenjaminM();
                    $paket[$x]->attributes = $row;
                    
                }
                
                $paket[$x]->validate();
                
                $x++;
            }
            return $paket;
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id, $idKelas)
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
                $model->kelaspelayanan_id = $idKelas;
                $model->carabayar_id = $id;
                $modTanggung = TanggunganpenjaminM::model()->findAllByAttributes(array('carabayar_id'=>$model->carabayar_id, 'kelaspelayanan_id'=>$idKelas));
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SATanggunganpenjaminM']))
		{
			$model->attributes=$_POST['SATanggunganpenjaminM'];
			$modTanggung = $this->validasiTabular($_POST['TanggunganpenjaminM']);
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                                $success = 0;
                                $list = CHtml::listData(TanggunganpenjaminM::model()->findAllByAttributes(array('carabayar_id'=>$model->carabayar_id,'kelaspelayanan_id'=>$model->kelaspelayanan_id)),'tanggunganpenjamin_id','tanggunganpenjamin_id');
                                foreach ($modTanggung as $i=>$row){
                                    if ($row->save()){
                                        unset($list[$row->tanggunganpenjamin_id]);
                                        $success++;
                                    }
                                }

                                if (count($list) > 0){
                                    foreach ($list as $isi){
                                        TanggunganpenjaminM::model()->deleteByPk($isi);
                                    }
                                }

                                if ((count($modTanggung) > 0) && ($success == count($modTanggung))) {
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', "Data Berhasil Disimpan ");
                                    $this->redirect(Yii::app()->createUrl($this->module->id.'/'.TanggunganpenjaminM.'/admin'));
                                }
                                else{
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                                }
                            } catch (Exception $exc) {
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
                            }
		}

		$this->render('create',array(
			'model'=>$model, 'modTanggung'=>$modTanggung,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        SATanggunganpenjaminM::model()->deleteByPk($id);

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SATanggunganpenjaminM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SATanggunganpenjaminM('search');
                $model->unsetAttributes();  // clear any default values
//                $modCaraBayar = new SACaraBayarM('search');
//                $modCaraBayar->unsetAttributes();
////                if(isset($_GET['SATanggunganpenjaminM']))
////                    $modCaraBayar->carabayar_nama=$_GET['SATanggunganpenjaminM'];
//                $modCaraBayar->carabayar_aktif = true;

                if (isset($_GET['SATanggunganpenjaminM'])){
                    
                    $model->attributes = $_GET['SATanggunganpenjaminM'];
                    
                }

                $this->render('admin',array(
                    'model'=>$model,'modCaraBayar'=>$modCaraBayar
                ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
                $dataModel = SATanggunganpenjaminM::model()->findByAttributes(array('carabayar_id'=>$id), array('limit'=>1));
		$model=SATanggunganpenjaminM::model()->findByPk($dataModel->tanggunganpenjamin_id);
		if($model===null){
			$model = new SATanggunganpenjaminM;
                        $model->carabayar_id = $id;
                }
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='satanggunganpenjamin-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                
                SATanggunganpenjaminM::model()->updateAll(array('tanggunganpenjamin_aktif'=>false),'carabayar_id='.$id.'');
                
                
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
         {
             if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}                         
             $model= new SATanggunganpenjaminM();
             $model->attributes=$_REQUEST['SATanggunganpenjaminM'];
//             echo print_r($model->attributes);
//             exit();
             $judulLaporan='Data Tanggungan Penjamin';
             $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT')
                {
                    $this->layout='//layouts/printWindows';
                    $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
            else if($caraPrint=='EXCEL')    
                {
                    $this->layout='//layouts/printExcel';
                    $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                }
            else if($_REQUEST['caraPrint']=='PDF')
                {
                   
                    $ukuranKertasPDF=Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                    $posisi=Yii::app()->user->getState('posisi_kertas');                            //Posisi L->Landscape,P->Portait
                    $mpdf=new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);  
                    $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                    $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                    $mpdf->Output();
                }                       
         }
}        