
<?php

class PaketbmhpMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            if (isset($_GET['idKelasPelayanan'])){
            echo $_GET['idKelasPelayanan']; exit();
            }
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SAPaketbmhpM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
		if(isset($_POST['SAPaketbmhpM']))
		{
                        $model->attributes=$_POST['SAPaketbmhpM'];
			$transaction = Yii::app()->db->beginTransaction();
                        try{
                            $jumlahUlang = count($_POST['PaketbmhpM']['obatalkes_id']);
                            for ($i = 0; $i < $jumlahUlang; $i++) {
                                if ($_POST['checkList'][$i] == '1') {//Jika Diceklist
                                    $jumlahObatDiceklist++;
                                    $modPaketbmhp = new SAPaketbmhpM;
                                    $modPaketbmhp->attributes = $model->attributes;
                                    $modPaketbmhp->obatalkes_id = $_POST['PaketbmhpM']['obatalkes_id'][$i];
                                    $modPaketbmhp->daftartindakan_id = $_POST['PaketbmhpM']['daftartindakan_id'][$i];
                                    $modPaketbmhp->tipepaket_id = $_POST['PaketbmhpM']['tipepaket_id'][$i];
                                    $modPaketbmhp->satuankecil_id = $_POST['PaketbmhpM']['satuankecil_id'][$i];
                                    $modPaketbmhp->qtypemakaian = $_POST['PaketbmhpM']['qtypemakaian'][$i];
                                    $modPaketbmhp->qtystokout = 0;
                                    $modPaketbmhp->hargapemakaian = $_POST['PaketbmhpM']['hargapemakaian'][$i];
                                    
                                    if ($modPaketbmhp->save()) {
                                        $jumlahCek++;
                                    }
                                }
                            }

                            if ($jumlahCek == $jumlahObatDiceklist) {
                                $transaction->commit();
                                Yii::app()->user->setFlash('success', "Data Berhasil Disimpan ");
                                $this->redirect(Yii::app()->createUrl($this->module->id.'/'.paketbmhpM.'/admin'));
                            }
                        } catch (Exception $exc) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
                            $this->refresh();
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
                $dataPaketBMHP = SAPaketbmhpM::model()->with('daftartindakan', 'obatalkes', 'tipepaket', 'satuankecil', 'kelompokumur')->findAllByAttributes(array('tipepaket_id'=>$model->tipepaket_id));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SAPaketbmhpM']))
		{
			$model->attributes=$_POST['SAPaketbmhpM'];
			$transaction = Yii::app()->db->beginTransaction();
                        try{
                            
                            SAPaketbmhpM::model()->deleteAllByAttributes(array('tipepaket_id'=>$model->tipepaket_id));
                            $jumlahUlang = count($_POST['PaketbmhpM']['obatalkes_id']);

                            for ($i = 0; $i < $jumlahUlang; $i++) {
                                if ($_POST['checkList'][$i] == '1') {//Jika Diceklist
                                    $jumlahObatDiceklist++;
                                    $modPaketbmhp = new SAPaketbmhpM;
                                    $modPaketbmhp->attributes = $model->attributes;
                                    $modPaketbmhp->obatalkes_id = $_POST['PaketbmhpM']['obatalkes_id'][$i];
                                    $modPaketbmhp->daftartindakan_id = $_POST['PaketbmhpM']['daftartindakan_id'][$i];
                                    $modPaketbmhp->tipepaket_id = $_POST['PaketbmhpM']['tipepaket_id'][$i];
                                    $modPaketbmhp->paketbmhp_id = $_POST['PaketbmhpM']['paketbmhp_id'][$i];
                                    $modPaketbmhp->satuankecil_id = $_POST['PaketbmhpM']['satuankecil_id'][$i];
                                    $modPaketbmhp->qtypemakaian = $_POST['PaketbmhpM']['qtypemakaian'][$i];
                                    $modPaketbmhp->paketbmhp_id = $_POST['PaketbmhpM']['paketbmhp_id'][$i];
                                    $modPaketbmhp->qtystokout = 0;
                                    $modPaketbmhp->hargapemakaian = $_POST['PaketbmhpM']['hargapemakaian'][$i];
                                    
                                    if ($modPaketbmhp->save()) {
                                        $jumlahCek++;
                                    }
                                }
                            }

                            if ($jumlahCek == $jumlahObatDiceklist) {
                                $transaction->commit();
                                Yii::app()->user->setFlash('success', "Data Berhasil Disimpan ");
                                $this->redirect(Yii::app()->createUrl($this->module->id.'/'.paketbmhpM.'/admin'));
                            }
                        } catch (Exception $exc) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
                            $this->refresh();
                        }
		}

		$this->render('create',array(
			'model'=>$model, 'dataPaketBMHP'=>$dataPaketBMHP,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			//$this->loadModel($id)->delete();
                        SAPaketbmhpM::model()->deleteAllByAttributes(array('tipepaket_id'=>$id));

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SAPaketbmhpM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new SAPaketbmhpM('search');
                $modTipePaket = new SATipePaketM('search');
                $modTipePaket->unsetAttributes();
                if (isset($_GET['SATipePaketM'])){
                    $modTipePaket->attributes = $_GET['SATipePaketM'];
                }

		if(isset($_GET['SAPaketbmhpM']))
			$modTipePaket->tipepaket_nama=$_GET['SAPaketbmhpM']['tipepaketNama'];

		$this->render('admin',array(
			'model'=>$model,
                        'modTipePaket'=>$modTipePaket
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
                $dataModel = SAPaketbmhpM::model()->findByAttributes(array('tipepaket_id'=>$id), array('limit'=>1));
		$model=SAPaketbmhpM::model()->findByPk($dataModel->paketbmhp_id);
		if($model===null){
			$model = new SAPaketbmhpM();
                        $model->tipepaket_id = $id;
                }
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sapaketbmhp-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new SAPaketbmhpM;
            $model->attributes=$_REQUEST['SAPaketbmhpM'];
            $judulLaporan='Data Paket BMHP';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
