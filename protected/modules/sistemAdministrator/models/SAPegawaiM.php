<?php

class SAPegawaiM extends PegawaiM
{
    
    public $nama_pemakai;
    public $new_password;
    public $new_password_repeat;  
    public $tempPhoto;
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    /**
     * Overide function karena ada format tanggal yang salah saat simpan / update
     */
    protected function beforeValidate ()
    {
        return parent::beforeSave();
    }
    public function beforeSave() 
    {
        return parent::beforeSave();
    }
}