<?php

/**
 * This is the model class for table "jenisobatalkes_m".
 *
 * The followings are the available columns in table 'jenisobatalkes_m':
 * @property integer $jenisobatalkes_id
 * @property string $jenisobatalkes_nama
 * @property string $jenisobatalkes_namalain
 * @property boolean $jenisobatalkes_aktif
 */
class GFJenisObatAlkesM extends JenisobatalkesM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return JenisobatalkesM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


}