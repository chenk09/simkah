<?php

/**
 * This is the model class for table "pemeriksaanlab_m".
 *
 * The followings are the available columns in table 'pemeriksaanlab_m':
 * @property integer $pemeriksaanlab_id
 * @property integer $daftartindakan_id
 * @property integer $jenispemeriksaanlab_id
 * @property string $pemeriksaanlab_kode
 * @property integer $pemeriksaanlab_urutan
 * @property string $pemeriksaanlab_nama
 * @property string $pemeriksaanlab_namalainnya
 * @property boolean $pemeriksaanlab_aktif
 */
class SAPemeriksaanLABM extends PemeriksaanlabM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PemeriksaanlabM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
}