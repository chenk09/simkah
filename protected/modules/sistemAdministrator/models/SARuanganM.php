<?php

/**
 * This is the model class for table "ruangan_m".
 *
 * The followings are the available columns in table 'ruangan_m':
 * @property integer $ruangan_id
 * @property integer $instalasi_id
 * @property string $ruangan_nama
 * @property string $ruangan_namalainnya
 * @property string $ruangan_lokasi
 * @property boolean $ruangan_aktif
 */
class SARuanganM extends RuanganM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RuanganM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        /**
         * Untuk menampilkan lokasi dari lookup_m
         */
        public function getLokasiItems()
        {
            return LookupM::model()->findAllByAttributes(array('lookup_type'=>'ruangan_lokasi', 'lookup_aktif'=>true), array('order'=>'lookup_urutan'));
        }
        /**
         * Untuk menampilkan ruangan di pemeriksaan pasien
         */
        public function getRuanganTindakan(){
            $criteria = new CDbCriteria();
            $criteria->addCondition('ruangan_aktif = TRUE');
            $criteria->addInCondition('instalasi_id',array(
                Params::INSTALASI_ID_RI,
                Params::INSTALASI_ID_RJ,
                Params::INSTALASI_ID_RD,
            ));
            $criteria->order = "instalasi_id, ruangan_nama ASC";
            $models = $this->findAll($criteria);
            if(count($models) > 0){
                return $models;
            }else{
                return array();
            }
        }

}