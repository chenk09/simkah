<?php
class SAPaketpelayananM extends PaketpelayananM { 
    
    public $tipepaket_nama;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchData()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('daftartindakan', 'tipepaket', 'ruangan');
		$criteria->compare('paketpelayanan_id',$this->paketpelayanan_id);
		$criteria->compare('t.daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('t.tipepaket_id',$this->tipepaket_id);
		$criteria->compare('t.tipepaket_nama',$this->tipepaket_nama);                
		$criteria->compare('t.ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(t.namatindakan)',  strtolower($this->namatindakan), true);
		$criteria->compare('LOWER(daftartindakan.daftartindakan_nama)',  strtolower($this->daftartindakanNama), true);
		$criteria->compare('LOWER(tipepaket.tipepaket_nama)',  strtolower($this->tipepaketNama), true);
		$criteria->compare('LOWER(ruangan.ruangan_nama)',  strtolower($this->ruanganNama), true);
		$criteria->compare('LOWER(t.subsidiasuransi)',  strtolower($this->subsidiasuransi), true);                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}