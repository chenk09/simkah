<?php
$this->breadcrumbs=array(
	'Ppusers'=>array('index'),
	$model->id,
);

$this->menu=array(
        array('label'=>Yii::t('mds','View').' Pemakai #'.$model->id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master')),
	array('label'=>Yii::t('mds','List').' Pemakai', 'icon'=>'list', 'url'=>array('index')),
	array('label'=>Yii::t('mds','Create').' Pemakai', 'icon'=>'file', 'url'=>array('create')),
        array('label'=>Yii::t('mds','Update').' Pemakai', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>Yii::t('mds','Delete').' Pemakai','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('mds','Manage').' Pemakai', 'icon'=>'folder-open', 'url'=>array('admin')),
);

$this->widget('bootstrap.widgets.BootAlert');
?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'password',
		'email',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view')); ?>
