<?php
$this->breadcrumbs=array(
	'Ppusers',
);

$this->menu=array(
        array('label'=>Yii::t('mds','List').' Pemakai ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master')),
	array('label'=>Yii::t('mds','Create').' Pemakai', 'icon'=>'file', 'url'=>array('create')),
	array('label'=>Yii::t('mds','Manage').' Pemakai', 'icon'=>'folder-open', 'url'=>array('admin')),
);

$this->widget('bootstrap.widgets.BootAlert');
?>

<?php $this->widget('ext.bootstrap.widgets.BootListView',array(
	'dataProvider'=>$dataProvider,
        'template'=>"{pager}{summary}\n{items}\n{pager}",
	'itemView'=>'_view',
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'list')); ?>
