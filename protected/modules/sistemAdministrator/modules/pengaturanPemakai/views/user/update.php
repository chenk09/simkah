<?php
$this->breadcrumbs=array(
	'Ppusers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
        array('label'=>Yii::t('mds','Update').' Pemakai #'.$model->id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master')),
	array('label'=>Yii::t('mds','List').' Pemakai', 'icon'=>'list', 'url'=>array('index')),
	array('label'=>Yii::t('mds','Create').' Pemakai', 'icon'=>'file', 'url'=>array('create')),
	array('label'=>Yii::t('mds','View').' Pemakai', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->id)),
	array('label'=>Yii::t('mds','Manage').' Pemakai', 'icon'=>'folder-open', 'url'=>array('admin')),
);

$this->widget('bootstrap.widgets.BootAlert');
?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'update')); ?>
