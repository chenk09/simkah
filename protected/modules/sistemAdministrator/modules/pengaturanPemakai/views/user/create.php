<?php
$this->breadcrumbs=array(
	'Ppusers'=>array('index'),
	'Create',
);

$this->menu=array(
        array('label'=>Yii::t('mds','Create').' Pemakai ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master')),
	array('label'=>Yii::t('mds','List').' Pemakai', 'icon'=>'list', 'url'=>array('index')),
	array('label'=>Yii::t('mds','Manage').' Pemakai', 'icon'=>'folder-open', 'url'=>array('admin')),
);

$this->widget('bootstrap.widgets.BootAlert');
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'create')); ?>