
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
)); 
 ?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

            <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldRow($model,'username',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>30)); ?>
            <div class="control-group">
                <?php echo $form->labelEx($model,'old_password',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->passwordField($model,'old_password',array('value'=>'','class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>30)); ?><?php echo CHtml::link('<i class="icon-info-sign icon-white"></i>', '#', array('class'=>'btn btn-primary', 'data-title'=>Yii::t('mds','Tips'), 'data-content'=>Yii::t('mds','fill this field in case to change the password'), 'rel'=>'popover')); ?>
                    <?php echo $form->error($model,'old_password'); ?>
                </div>
            </div>
            
            <?php echo $form->passwordFieldRow($model,'new_password',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>30)); ?>
            <?php echo $form->passwordFieldRow($model,'new_password_repeat',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>50)); ?>
            <?php echo $form->dropDownListRow($model,'pegawai_id',CHtml::listData($model->getPegawai(),'pegawai_id','pegawai_nama'),array('empty'=>'- Pegawai -','class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')")); ?>
            <?php echo $form->textFieldRow($model,'email',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>50)); ?>
           
            
	<div class="form-actions">
                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                         Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                         array('class'=>'btn btn-primary', 'type'=>'submit')); 
                ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.user.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); 
                ?>
				<?php
$content = $this->renderPartial('../tips/tips',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
<?php

$js = <<< JSCRIPT
   kosongkanPassword();
       
   function kosongkanPassword(){
        $('#User_new_password').val('');
        $('#User_old_password').val('');
        $('#User_new_password_repeat').val('');
   }

JSCRIPT;

Yii::app()->clientScript->registerScript('kosongkanPassword', $js, CClientScript::POS_READY);
?>