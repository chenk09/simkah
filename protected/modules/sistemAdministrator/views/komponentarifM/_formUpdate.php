<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sakomponen-tarif-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#SAKomponenTarifM_komponentarif_nama',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldRow($model,'komponentarif_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>25)); ?>
            <?php echo $form->textFieldRow($model,'komponentarif_namalainnya',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>25)); ?>
            <?php echo $form->textFieldRow($model,'komponentarif_urutan',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->checkBoxRow($model,'komponentarif_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

        <?php  echo $form->labelEx($model,'Instalasi',array('class'=>'control-label required'));  ?>
            <div class="control-group">
                <div class="controls">

                     <?php 
                            $arrKomponenTarifInstalasi = array();
                             foreach($modKomponenTarifInstalasi as $jenisKomponenTarifInstalasi){
                                $arrKomponenTarifInstalasi[] = $jenisKomponenTarifInstalasi['komponentarif_id'];
                            } 
                           $this->widget('application.extensions.emultiselect.EMultiSelect',
                                         array('sortable'=>true, 'searchable'=>true)
                                    );
                            echo CHtml::dropDownList(
                            'instalasi_id[]',
                            $arrKomponenTarifInstalasi,
                            CHtml::listData(SAInstalasiM::model()->findAll(array('order'=>'instalasi_nama')), 'instalasi_id', 'instalasi_nama'),
                            array('multiple'=>'multiple','key'=>'instalasi_id', 'class'=>'multiselect','style'=>'width:500px;height:150px')
                                    );
                      ?>

                 </div>
            </div>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.komponentarifM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
		<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
