
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sapropinsi-m-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#SAPropinsiM_propinsi_nama',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        
	<?php echo $form->errorSummary($model); ?>
		<?php // echo $form->textFieldRow($model,'zatgizi_id'); ?>
                                <?php echo $form->dropDownListRow($model,'zatgizi_id',
                                CHtml::listData($model->ZatgiziItems, 'zatgizi_id', 'zatgizi_nama'),
                                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>
        
		<?php // echo $form->textFieldRow($model,'bahanmakanan_id'); ?>
                                <?php echo $form->dropDownListRow($model,'bahanmakanan_id',
                                CHtml::listData($model->BahanMakananItems, 'bahanmakanan_id', 'namabahanmakanan'),
                                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>
        
		<?php echo $form->textFieldRow($model,'kandunganbahan',array('class'=>'span2')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                     array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan','onKeypress'=>'return formSubmit(this,event)','onClick'=>'return formSubmit(this,event)')); ?>
                                               <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                                    Yii::app()->createUrl($this->module->id.'/'.zatBahanMakananM.'/admin'), 
                                                                    array('class'=>'btn btn-danger',
                                                                    'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
		<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>
<?php $this->endWidget(); ?>

</div><!-- form -->