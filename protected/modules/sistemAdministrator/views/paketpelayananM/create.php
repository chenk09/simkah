<?php
$this->breadcrumbs=array(
	'Sapaketpelayanan Ms'=>array('index'),
	'Create',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Paket Pelayanan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Paket Pelayanan', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Paket Pelayanan', 'icon'=>'folder-open', 'url'=>array('Admin'))) :  '' ;

$this->menu=$arrMenu;
$totaltarif = 0;
$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'dataPaketPelayanan'=>$modPaket, 'totaltarif'=>$totaltarif, 'modPaket'=>$modPaket, 'kelas'=>$kelas)); ?>

<?php //$this->widget('TipsMasterData',array('type'=>'create'));?>