
<?php
$this->breadcrumbs=array(
	'Sapaketpelayanan Ms'=>array('index'),
	$model->paketpelayanan_id,
);
$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Paket Pelayanan #'.$model->paketpelayanan_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' SAPaketpelayananM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' SAPaketpelayananM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' SAPaketpelayananM', 'icon'=>'pencil','url'=>array('update','id'=>$model->paketpelayanan_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' SAPaketpelayananM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->paketpelayanan_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Paket Pelayanan', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'paketpelayanan_id',
		//'daftartindakan.daftartindakan_nama',
                'tipepaket.tipepaket_nama',
                array(
                     'label'=>'Daftar Tindakan',
                     'type'=>'raw',
                     'value'=>$this->renderPartial('_daftarTindakan',array('tipepaket_id'=>$model->tipepaket_id),true),
                 ),
		'ruangan.ruangan_nama',
//		'namatindakan',
//		'subsidiasuransi',
//		'subsidipemerintah',
//		'subsidirumahsakit',
//		'iurbiaya',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>