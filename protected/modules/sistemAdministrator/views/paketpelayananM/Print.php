
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

  $table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)) {
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
         $data = $model->search();
         $template = "{pager}{summary}\n{items}";
    }
    
//$this->widget('ext.bootstrap.widgets.BootGridView',array(
//	'id'=>'sajenis-kelas-m-grid',
//        'enableSorting'=>false,
//	'dataProvider'=>$model->searchPrint(),
//        'template'=>"{pager}{summary}\n{items}",
//        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//	'columns'=>array(
//		////'paketpelayanan_id',
//		array(
//                        'header'=>'ID',
//                        'value'=>'$data->paketpelayanan_id',
//                ),
//		'daftartindakan.daftartindakan_nama',
//		'tipepaket.tipepaket_nama',
//		'ruangan.ruangan_nama',
//		'namatindakan',
//		'subsidiasuransi',
//		/*
//		'subsidipemerintah',
//		'subsidirumahsakit',
//		'iurbiaya',
//		*/
// 
//        ),
//    )); 
?>

<?php $modTipePaket = new SATipePaketM('search'); ?>
<?php $this->widget($table,array( 
    'id'=>'satipe-paket-m-grid', 
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    //'filter'=>$modTipePaket, 
    'template'=>$template, 
    'itemsCssClass'=>'table table-striped table-bordered table-condensed', 
    'columns'=>array( 
        ////'tipepaket_id',
//        array( 
//                        'name'=>'tipepaket_id', 
//                        'value'=>'$data->tipepaket_id', 
//                        'filter'=>false, 
//                ),
//        'kelaspelayanan_id',
//        'penjamin_id',
//        'carabayar_id',
        array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
        ),
        array(
            'header'=>'Tipe Paket',
            'value'=>'$data->tipepaket_nama',
        ),
        array(
                     'header'=>'Nama Tindakan',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_daftarTindakan\',array(\'tipepaket_id\'=>$data[tipepaket_id]),true)',
        ),
        //'tipepaket_singkatan',
        /*
        'tipepaket_namalainnya',
        'tglkesepakatantarif',
        'nokesepakatantarif',
        'tarifpaket',
        'paketsubsidiasuransi',
        'paketsubsidipemerintah',
        'paketsubsidirs',
        'paketiurbiaya',
        'nourut_tipepaket',
        'keterangan_tipepaket',
        'tipepaket_aktif',
        */
        
    ), 
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}', 
)); ?> 