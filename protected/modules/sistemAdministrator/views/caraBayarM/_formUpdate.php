<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sacara-bayar-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#SACaraBayarM_carabayar_nama',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldRow($model,'carabayar_nama',array('class'=>'span2', 'onkeypress'=>"return nextFocus(this,event,'SACaraBayarM_carabayar_namalainnya','')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'carabayar_namalainnya',array('class'=>'span2', 'onkeypress'=>"return nextFocus(this,event,'SACaraBayarM_metode_pembayaran','SACaraBayarM_carabayar_nama')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'metode_pembayaran',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'btn_simpan','SACaraBayarM_carabayar_namalainnya')", 'maxlength'=>50)); ?>
            <?php echo $form->checkBoxRow($model,'carabayar_aktif', array('onkeypress'=>"return nextFocus(this,event,'','')")); ?>
            <?php echo $form->textFieldRow($model,'carabayar_loket',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'SACaraBayarM_metode_pembayaran','SACaraBayarM_carabayar_nama')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'carabayar_singkatan',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'btn_simpan','SACaraBayarM_carabayar_namalainnya')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'carabayar_nourut',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'btn_simpan','SACaraBayarM_carabayar_namalainnya')", 'maxlength'=>50)); ?>

        <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.caraBayarM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
