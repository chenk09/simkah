
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'samenudiet-m-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#SAMenuDietM_menudiet_nama',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
                <table style="width:410px">
                    <tr>
                        <td colspan="2">
		<?php // echo $form->textFieldRow($model,'jenisdiet_id'); ?>
                                <?php echo $form->dropDownListRow($model,'jenisdiet_id',
                                CHtml::listData($model->JenisdietItems, 'jenisdiet_id', 'jenisdiet_nama'),
                                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                'empty'=>'-- Pilih --',)); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
		<?php echo $form->textFieldRow($model,'menudiet_nama',array('size'=>60,'maxlength'=>200)); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
		<?php echo $form->textFieldRow($model,'menudiet_namalain',array('size'=>60,'maxlength'=>200)); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
		<?php echo $form->textFieldRow($model,'jml_porsi',array('class'=>'span1')); ?>
                        </td>
                        <td>
                                <?php echo $form->dropDownList($model,'ukuranrumahtangga',
                                CHtml::listData($model->URTItems, 'lookup_name', 'lookup_value'),
                                array('class'=>'inputRequire span2', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                'empty'=>'-- Pilih --',)); ?>
                        </td>
                    </tr>
                </table>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                     array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan','onKeypress'=>'return formSubmit(this,event)','onClick'=>'return formSubmit(this,event)')); ?>
                                               <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                                    Yii::app()->createUrl($this->module->id.'/'.menuDietM.'/admin'), 
                                                                    array('class'=>'btn btn-danger',
                                                                    'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
		<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>
                              <fieldset>
                                <div id="divZatgizi">
                                    <span class="help-block">Kandungan Menu Diet :</span>
                                    <table id="tblinputZatgizi">
                                        <tbody>
                                            <?php
                                            $datas = ZatgiziM::model()->findAll($criteria);
                                            $returnVal = array();
                                            $tr = ''; $inputHiddenZatgizi = '<input type="hidden" size="4" name="zatgizi_id[]" readonly="true"/>';
                                            /* $returnVal = '<table id="tblinputzatgizi" class="table table-condensed table-bordered span3" style="width:500px;"><th> Pilih Semua <br/>'.CHtml::checkBox('checkUncheck', false, array('onclick'=>'checkUncheckAll(this);')).'</th>
                                                                <th>Nama Zatgizi</th><th>'.$inputHiddenZatgizi.'Kandungan</th>'; */
                                            $returnVal = '<table id="tblinputzatgizi" class="table table-condensed table-bordered span1" style="width:400px; float:left;"><th> Pilih </th>
                                                                <th>Nama Zatgizi</th><th>'.$inputHiddenZatgizi.'Kandungan</th>';
                                            foreach ($datas as $data)
                                            {
                                                $tr .= "<tr><td>";
                                                $tr .= CHtml::checkBox('zatgizi_id[]', false, array('value'=>$data->getAttribute('zatgizi_id')));
                                                $tr .= '</td><td>'.$data->getAttribute('zatgizi_nama');
                                                $tr .= '</td><td>'.CHtml::textField("kandunganmenudiet[$data->zatgizi_id]", '0', array('size'=>6,'class'=>'default'));
                                                $tr .= "</td></tr>";
                                            }
                                            $returnVal .= $tr;
                                            $returnVal .= '</table>';
                                            echo $returnVal;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
        
<?php $this->endWidget(); ?>
