<?php $this->renderPartial('_tabMenu',array()); ?>
<?php
$this->breadcrumbs=array(
	'Gfsumber Dana Ms'=>array('index'),
	$model->sumberdana_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Sumber Dana #'.$model->sumberdana_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Sumber Dana', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Sumber Dana', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Sumber Dana', 'icon'=>'pencil','url'=>array('update','id'=>$model->sumberdana_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Sumber Dana','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->sumberdana_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Sumber Dana', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'sumberdana_id',
		'sumberdana_nama',
		'sumberdana_namalainnya',
                 array(               // related city displayed as a link
                    'name'=>'sumberdana_aktif',
                    'type'=>'raw',
                    'value'=>(($model->sumberdana_aktif==1)? Yii::t('mds','Yes') : Yii::t('mds','No')),
                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>