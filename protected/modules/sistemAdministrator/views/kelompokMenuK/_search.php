<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sakelompok-menu-k-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'kelmenu_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'kelmenu_nama',array('class'=>'span3','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'kelmenu_namalainnya',array('class'=>'span3','maxlength'=>30)); ?>

	<?php echo $form->checkBoxRow($model,'kelmenu_aktif',array('checked'=>'kelmenu_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
