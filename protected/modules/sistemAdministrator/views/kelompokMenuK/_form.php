
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sakelompok-menu-k-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#SAKelompokMenuK_kelmenu_nama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldRow($model,'kelmenu_nama',array('class'=>'span2', 'onkeyup'=>"namaLain(this)", 'onkeypress'=>"return nextFocus(this,event,'SAKelompokMenuK_kelmenu_namalainnya','')", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'kelmenu_namalainnya',array('class'=>'span2', 'onkeypress'=>"return nextFocus(this,event,'SAKelompokMenuK_kelmenu_key','SAKelompokMenuK_kelmenu_nama')", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'kelmenu_key',array('class'=>'span2', 'onkeypress'=>"return nextFocus(this,event,'SAKelompokMenuK_kelmenu_url','SAKelompokMenuK_kelmenu_namalainnya')", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'kelmenu_url',array('class'=>'span2', 'onkeypress'=>"return nextFocus(this,event,'SAKelompokMenuK_kelmenu_urutan','SAKelompokMenuK_kelmenu_key')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'kelmenu_urutan',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'SAKelompokMenuK_kelmenu_aktif','SAKelompokMenuK_kelmenu_url')")); ?>
            <?php //echo $form->checkBoxRow($model,'kelmenu_aktif', array('onkeypress'=>"return nextFocus(this,event,'btn_simpan','SAKelompokMenuK_kelmenu_urutan')")); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.kelompokMenuK.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    function namaLain(nama)
    {
        document.getElementById('SAKelompokMenuK_kelmenu_namalainnya').value = nama.value.toUpperCase();
        document.getElementById('SAKelompokMenuK_kelmenu_key').value = nama.value;
    }
</script>