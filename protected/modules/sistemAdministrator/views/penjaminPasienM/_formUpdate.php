<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sapenjamin-pasien-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#SAPenjaminPasienM_carabayar_id',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

 	    <?php echo $form->errorSummary($model); ?>
            <?php echo $form->dropDownListRow($model,'carabayar_id',  CHtml::listData($model->CarabayarItems, 'carabayar_id', 'carabayar_nama'),array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'SAPenjaminPasienM_penjamin_nama','')",'empty'=>'-- Pilih --')); ?>
            <?php echo $form->textFieldRow($model,'penjamin_nama',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'SAPenjaminPasienM_penjamin_namalainnya','SAPenjaminPasienM_carabayar_id')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'penjamin_namalainnya',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'SAPenjaminPasienM_percentage_farmasi','SAPenjaminPasienM_penjamin_nama')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'percentage_farmasi',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'SAPenjaminPasienM_percentage_lab','SAPenjaminPasienM_penjamin_namalainnya')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'percentage_lab',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'SAPenjaminPasienM_penjamin_aktif','SAPenjaminPasienM_percentage_farmasi')", 'maxlength'=>50)); ?>
            <?php echo $form->checkBoxRow($model,'penjamin_aktif', array('onkeypress'=>"return nextFocus(this,event,'btn_simpan','SAPenjaminPasienM_penjamin_namalainnya')")); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.penjaminPasienM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
		<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
