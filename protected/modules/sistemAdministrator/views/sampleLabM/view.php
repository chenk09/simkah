<?php
$this->breadcrumbs=array(
	'Sasample Lab Ms'=>array('index'),
	$model->samplelab_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Sample Lab #'.$model->samplelab_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Sample Lab', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Sample Lab', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Sample Lab', 'icon'=>'pencil','url'=>array('update','id'=>$model->samplelab_id))) :  '' ;
                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Sample Lab','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->samplelab_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Sample Lab', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'samplelab_id',
		'samplelab_nama',
		'samplelab_namalainnya',
		array(            
                        'label'=>'Aktif',
                        'type'=>'raw',
                        'value'=>(($model->samplelab_aktif==1)? ''.Yii::t('mds','Yes').'' : ''.Yii::t('mds','No').''),
                     ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>