<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
                'id'=>'search-m-form'
)); ?>

		<?php // echo $form->textFieldRow($model,'jenissurat_id',array('class'=>'span3')); ?>
		<?php echo $form->textFieldRow($model,'jenissurat_nama',array('size'=>60,'maxlength'=>200,'class'=>'span3')); ?>
		<?php echo $form->textFieldRow($model,'jenissurat_namalain',array('size'=>60,'maxlength'=>200,'class'=>'span3')); ?>
		<?php echo $form->checkBoxRow($model,'jenissurat_aktif'); ?>

	<div class="form-actions">
	  <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>