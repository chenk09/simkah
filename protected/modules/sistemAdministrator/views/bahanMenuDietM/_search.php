
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
                'type'=>'horizontal',
	'method'=>'get',
                'id'=>'bahanmenudiet-m-search',
)); ?>

		<?php// echo $form->textFieldRow($model,'bahanmenudiet_id'); ?>
		<?php echo $form->dropDownListRow($model,'menudiet_id',CHtml::listData($model->getMenudietItems(),'menudiet_id','menudiet_nama'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
		<?php echo $form->dropDownListRow($model,'bahanmakanan_id',CHtml::listData($model->getBahanmakananItems(),'bahanmakanan_id','namabahanmakanan'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
		<?php echo $form->textFieldRow($model,'jmlbahan'); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>