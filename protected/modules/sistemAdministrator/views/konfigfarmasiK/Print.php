
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

  $table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
    
$this->widget($table,array(
	'id'=>'sakonfigfarmasi-k-grid',
        'enableSorting'=>$sort,
	'dataProvider'=>$data,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'konfigfarmasi_id',
		array(
                        'header'=>'ID',
                        'value'=>'$data->konfigfarmasi_id',
                ),
		'tglberlaku',
		'persenppn',
		'persenpph',
		'persehargajual',
		'totalpersenhargajual',
		/*
		'bayarlangsung',
		'pesandistruk',
		'pesandifaktur',
		'formulajasadokter',
		'formulajasaparamedis',
		'hargaygdigunakan',
		'pembulatanharga',
		'konfigfarmasi_aktif',
		*/
        ),
    )); 
?>