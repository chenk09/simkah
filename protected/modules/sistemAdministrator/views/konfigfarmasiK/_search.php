<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sakonfigfarmasi-k-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'konfigfarmasi_id',array('class'=>'span5')); ?>

	<?php// echo $form->textFieldRow($model,'tglberlaku',array('class'=>'span2')); ?>
                <div class="control-group">
                    <div class="control-label">
                        <?php echo CHtml::label('Tanggal Berlaku','SAKonfigfarmasiK_tglberlaku'); ?>
                    </div>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglberlaku',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                        )); 
                                ?>
                    </div>
                </div>
	<?php echo $form->textFieldRow($model,'persenppn',array('class'=>'span2')); ?>

	<?php echo $form->textFieldRow($model,'persenpph',array('class'=>'span2')); ?>

	<?php echo $form->textFieldRow($model,'persehargajual',array('class'=>'span2')); ?>

	<?php //echo $form->textFieldRow($model,'totalpersenhargajual',array('class'=>'span5')); ?>

	<?php //echo $form->checkBoxRow($model,'bayarlangsung'); ?>

	<?php //echo $form->textAreaRow($model,'pesandistruk',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php// echo $form->textAreaRow($model,'pesandifaktur',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textFieldRow($model,'formulajasadokter',array('class'=>'span5','maxlength'=>100)); ?>

	<?php //echo $form->textFieldRow($model,'formulajasaparamedis',array('class'=>'span5','maxlength'=>100)); ?>

	<?php //echo $form->textFieldRow($model,'hargaygdigunakan',array('class'=>'span5','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'pembulatanharga',array('class'=>'span5')); ?>

	<?php echo $form->checkBoxRow($model,'konfigfarmasi_aktif',array('checked'=>'konfigfarmasi_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
