<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'salokasiaset-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'lokasi_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'lokasiaset_kode',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'lokasiaset_namainstalasi',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'lokasiaset_namabagian',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'lokasiaset_namalokasi',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->checkBoxRow($model,'lokasiaset_aktif',array('checked'=>'lokasiaset_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
