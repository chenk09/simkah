
<?php
$this->breadcrumbs=array(
	'Sakelompok Modul Ks'=>array('index'),
	'Create',
);

$this->menu=array(
        array('label'=>Yii::t('mds','Create').' Kelompok Modul ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master')),
//	array('label'=>Yii::t('mds','List').' Kelompok Modul', 'icon'=>'list', 'url'=>array('index')),
	array('label'=>Yii::t('mds','Manage').' Kelompok Modul', 'icon'=>'folder-open', 'url'=>array('admin')),
);

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php //$this->widget('TipsMasterData',array('type'=>'create'));?>