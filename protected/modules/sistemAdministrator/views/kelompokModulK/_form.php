
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sakelompok-modul-k-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#SAKelompokModulK_kelompokmodul_nama',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldRow($model,'kelompokmodul_nama',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'SAKelompokModulK_kelompokmodul_namalainnya','')", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'kelompokmodul_namalainnya',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'SAKelompokModulK_kelompokmodul_fungsi','SAKelompokModulK_kelompokmodul_nama')", 'maxlength'=>50)); ?>
            <?php echo $form->textAreaRow($model,'kelompokmodul_fungsi',array('rows'=>6, 'cols'=>50, 'class'=>'span5', 'onkeypress'=>"return nextFocus(this,event,'SAKelompokModulK_kelompokmodul_aktif','SAKelompokModulK_kelompokmodul_namalainnya')")); ?>
            <?php //echo $form->checkBoxRow($model,'kelompokmodul_aktif', array('onkeypress'=>"return nextFocus(this,event,'btn_simpan','SAKelompokModulK_kelompokmodul_fungsi')")); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.kelompokModulK.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
