<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sapendidikankualifikasi-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'pendkualifikasi_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'pendkualifikasi_kode',array('class'=>'span3','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'pendkualifikasi_nama',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'pendkualifikasi_namalainnya',array('class'=>'span3','maxlength'=>50)); ?>

	<?php //echo $form->textAreaRow($model,'pendkualifikasi_keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->checkBoxRow($model,'pendkualifikasi_aktif',array('checked'=>'pendkualifikasi_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
