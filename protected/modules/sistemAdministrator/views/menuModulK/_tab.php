<?php
$this->widget('bootstrap.widgets.BootMenu',
    array(
        'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
        'stacked'=>false, // whether this is a stacked menu
        'items'=>array(
            array(
                'label' => 'Kelompok Menu',
                'url' => array(
                    '/sistemAdministrator/kelompokMenuK',
                    'modulId'=>(isset($_REQUEST['modulId']) ? $_REQUEST['modulId'] : '')
                )
            ),
            array('label'=>'Menu', 'url'=>'', 'active'=>true),
        )
    )
);