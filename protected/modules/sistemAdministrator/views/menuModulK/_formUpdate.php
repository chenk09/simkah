<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'samenu-modul-k-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
         'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

<table class="table">
    <tr>
        <td>
            <?php echo $form->dropDownListRow($model,'kelmenu_id', CHtml::listData($model->getKelompokMenuItems(), 'kelmenu_id', 'kelmenu_nama'),array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')")); ?>
            <?php echo $form->dropDownListRow($model,'modul_id', CHtml::listData($model->getModulItems(), 'modul_id', 'modul_nama'),array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')")); ?>
            <?php echo $form->textFieldRow($model,'menu_nama',array('class'=>'span2', 'onkeyup'=>"namaLain(this)", 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'menu_namalainnya',array('class'=>'span2', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>100)); ?>
            <?php echo $form->checkBoxRow($model,'menu_aktif', array('onkeypress'=>"return nextFocus(this,event,'','')")); ?>
        </td>
        <td>
            <?php echo $form->textFieldRow($model,'menu_key',array('class'=>'span2', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>100)); ?>
            <?php //echo $form->textFieldRow($model,'menu_url',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>50)); ?>
            <div class="control-group">
                <?php echo CHtml::label('Url', 'SAMenuModulK_menu_url', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo CHtml::textField('SAMenuModulK[menu_url]', $model->menu_url, array('class'=>'span3','maxlength'=>100)); ?>
                    <?php echo CHtml::link('<i class="icon-edit icon-white"></i>','#', array('class'=>'btn btn-primary', 'onclick'=>'$("#dialogUrl").dialog("open"); return false;')); ?>
                </div>   
            </div>
            <?php echo $form->textAreaRow($model,'menu_fungsi',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>100)); ?>
            <?php echo $form->fileFieldRow($model,'menu_icon',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>100)); ?>
            <div class="control-group">
                <div class="controls">
                    <?php echo CHtml::image(Params::urlIconMenuDirectory().$model->menu_icon, 'Icon '.$model->menu_nama, array()); ?>
                </div>   
            </div>
            <?php echo $form->textFieldRow($model,'menu_urutan',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'','')")); ?>
        </td>
    </tr>
</table>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/menuModulK/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
		<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>

<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogUrl',
    'options'=>array(
        'title'=>'URL',
        'autoOpen'=>false,
    'modal'=>true,
    ),
));
?>
        
<?php // echo CHtml::beginForm(Yii::app()->createUrl('ActionDynamic/GetControllers'), 'POST', ''); ?>
<?php // echo CHtml::label('Nama Modul', 'namaModul', array('class'=>'control-label')) ?>
<?php // echo CHtml::dropDownList('namaModul', '', Params::getModules(),array(
//                                'ajax' => array('type'=>'POST',
//                                                'url'=> Yii::app()->createUrl('ActionDynamic/GetControllers',array('encode'=>false)), 
//                                                'update'=>'#namaController'  //selector to update
//                ),)
//); ?>
<?php // echo CHtml::label('Nama Controller', 'namaController', array('class'=>'control-label')) ?>
<?php // echo CHtml::dropDownList('namaController', '', array(),array(
//                                'ajax' => array('type'=>'POST',
//                                                'url'=> Yii::app()->createUrl('ActionDynamic/GetActions',array('encode'=>false)), 
//                                                'update'=>'#namaAction'  //selector to update
//                ),)
//); ?>
<?php // echo CHtml::label('Nama Action', 'namaAction', array('class'=>'control-label')) ?>
<?php // echo CHtml::dropDownList('namaAction', '', array()); ?>

<div class="form-actions">
    <?php echo CHtml::link(Yii::t('mds','Ok'), '#', array('class'=>'btn btn-primary', 'onclick'=>'createURL(); return false;')); ?>
    <?php echo CHtml::link(Yii::t('mds','Cancel'), '#', array('class'=>'btn btn-danger', 'onclick'=>'$("#dialogUrl").dialog("close"); return false;')); ?>
</div>
<?php echo CHtml::endForm(); ?>
        
<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>

<script type="text/javascript">
function createURL()
{
    var url = $('#namaModul').val();
    if($('#namaController').val()!=null)
        url = url + '/' + $('#namaController').val();
    if($('#namaAction').val()!=null)
        url = url + '/' + $('#namaAction').val();
    
    $('#SAMenuModulK_menu_url').val(url);
    $("#dialogUrl").dialog("close");
}

function namaLain(nama)
{
    document.getElementById('SAMenuModulK_menu_namalainnya').value = nama.value.toUpperCase();
    document.getElementById('SAMenuModulK_menu_key').value = nama.value;
}
</script>
        