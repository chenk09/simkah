
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sabahanmakanan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
                <table class="table-condensed">
                    <tr>
                        <td>
		<?php // echo $form->textFieldRow($model,'jenisbahanmakanan',array('size'=>50,'maxlength'=>50)); ?>
                                <?php echo $form->dropDownListRow($model,'jenisbahanmakanan',JenisBahanMakanan::items(),
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'class'=>'inputRequire')); ?>
                            
                            
		<?php // echo $form->textFieldRow($model,'kelbahanmakanan',array('size'=>50,'maxlength'=>50)); ?>
                                <?php echo $form->dropDownListRow($model,'kelbahanmakanan',KelBahanMakanan::items(),
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'class'=>'inputRequire')); ?>
                            
		<?php echo $form->textFieldRow($model,'namabahanmakanan',array('size'=>60,'maxlength'=>100)); ?>
                                <?php echo $form->textFieldRow($model,'jmlpersediaan',array('class'=>'span2')); ?>
                            
                                <?php echo $form->dropDownListRow($model,'jmldlmkemasan',JmlDlmKemasan::items(),
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'class'=>'inputRequire')); ?>
                            
		<?php echo $form->textFieldRow($model,'jmlminimal',array('class'=>'span2')); ?>
                        </td>
                        <td>
		<?php // echo $form->textFieldRow($model,'sumberdanabhn',array('size'=>50,'maxlength'=>50)); ?>
                                <?php echo $form->dropDownListRow($model,'sumberdanabhn',SumberDanaBahan::items(),
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'class'=>'inputRequire')); ?>
                            
		<?php echo $form->textFieldRow($model,'harganettobahan',array('class'=>'span2')); ?>
		<?php echo $form->textFieldRow($model,'hargajualbahan',array('class'=>'span2')); ?>
		<?php // echo $form->textFieldRow($model,'tglkadaluarsabahan'); ?>
                                <?php echo $form->labelEx($model,'tglkadaluarsabahan', array('class'=>'control-label')) ?>
                                   <div class="controls">  
                                      <?php $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tglkadaluarsabahan',
                                                            'mode'=>'date',
                                                            'options'=> array(
                                                                'dateFormat'=>'yy-mm-dd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,
                                                                                  'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                                  'class'=>'dtPicker3',
                                                                ),
                                    )); ?> 
                                    </div> 
		<?php // echo $form->textFieldRow($model,'golbahanmakanan_id'); ?>
                                 <?php echo $form->dropDownListRow($model,'golbahanmakanan_id',
                                CHtml::listData($model->GolBahanMakananItems, 'golbahanmakanan_id', 'golbahanmakanan_nama'),
                                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>
		<?php // echo $form->textFieldRow($model,'satuanbahan',array('size'=>50,'maxlength'=>50)); ?>
                                <?php echo $form->dropDownListRow($model,'satuanbahan',SatuanBahanMakanan::items(),
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'class'=>'inputRequire')); ?>
		<?php echo $form->textFieldRow($model,'discount',array('class'=>'span1')); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                               <div class="form-actions">
                                                    <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                         Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                                                    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                                         Yii::app()->createUrl($this->module->id.'/'.bahanMakananM.'/admin'), 
                                                                            array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                               <?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'master','content'=>$content)); 
?>
                               </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="help-block">Zat Gizi Bahan Makanan :</span>
                              <fieldset>
                                <div id="divZatgizi">
                                    <table id="tblinputZatgizi">
                                        <tbody>
                                            <?php
                                            $datas = ZatgiziM::model()->findAll($criteria);
                                            $returnVal = array();
                                            $tr = ''; $inputHiddenZatgizi = '<input type="hidden" size="4" name="zatgizi_id[]" readonly="true"/>';
                                            /* $returnVal = '<table id="tblinputzatgizi" class="table table-condensed table-bordered span3" style="width:500px;"><th> Pilih Semua <br/>'.CHtml::checkBox('checkUncheck', false, array('onclick'=>'checkUncheckAll(this);')).'</th>
                                                                <th>Nama Zatgizi</th><th>'.$inputHiddenZatgizi.'Kandungan</th>'; */
                                            $returnVal = '<table id="tblinputzatgizi" class="table table-condensed table-bordered span1" style="width:400px; float:left;"><th> Pilih </th>
                                                                <th>Nama Zatgizi</th><th>'.$inputHiddenZatgizi.'Kandungan</th>';
                                            foreach ($datas as $data)
                                            {
                                                $tr .= "<tr><td>";
                                                $tr .= CHtml::checkBox('zatgizi_id[]', false, array('value'=>$data->getAttribute('zatgizi_id')));
                                                $tr .= '</td><td>'.$data->getAttribute('zatgizi_nama');
                                                $tr .= '</td><td>'.CHtml::textField("kandunganbahan[$data->zatgizi_id]", '0', array('size'=>6,'class'=>'default'));
                                                $tr .= "</td></tr>";
                                            }
                                            $returnVal .= $tr;
                                            $returnVal .= '</table>';
                                            echo $returnVal;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset> 
                        </td>
                    </tr>
                </table>
<?php $this->endWidget(); ?>