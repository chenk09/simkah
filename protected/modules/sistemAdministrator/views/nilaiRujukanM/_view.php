<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilairujukan_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nilairujukan_id),array('view','id'=>$data->nilairujukan_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pemeriksaanlab_id')); ?>:</b>
	<?php echo CHtml::encode($data->pemeriksaanlab_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilairujukan_jeniskelamin')); ?>:</b>
	<?php echo CHtml::encode($data->nilairujukan_jeniskelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelompokumur')); ?>:</b>
	<?php echo CHtml::encode($data->kelompokumur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilairujukan_nama')); ?>:</b>
	<?php echo CHtml::encode($data->nilairujukan_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilairujukan_min')); ?>:</b>
	<?php echo CHtml::encode($data->nilairujukan_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilairujukan_max')); ?>:</b>
	<?php echo CHtml::encode($data->nilairujukan_max); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nilairujukan_satuan')); ?>:</b>
	<?php echo CHtml::encode($data->nilairujukan_satuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilairujukan_metode')); ?>:</b>
	<?php echo CHtml::encode($data->nilairujukan_metode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilairujukan_keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->nilairujukan_keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilairujukan_aktif')); ?>:</b>
	<?php echo CHtml::encode($data->nilairujukan_aktif); ?>
	<br />

	*/ ?>

</div>