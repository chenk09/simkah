
<?php
$this->breadcrumbs=array(
	'Sanilai Rujukan Ms'=>array('index'),
	$model->nilairujukan_id=>array('view','id'=>$model->nilairujukan_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' SANilaiRujukanM #'.$model->nilairujukan_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                array_push($arrMenu,array('label'=>Yii::t('mds','List').' SANilaiRujukanM', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' SANilaiRujukanM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' SANilaiRujukanM', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->nilairujukan_id))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' SANilaiRujukanM', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model,'modPemeriksaanLab'=>$modPemeriksaanLab)); ?>
<?php //$this->widget('TipsMasterData',array('type'=>'update'));?>