<?php
$this->breadcrumbs=array(
	'Sanilai Rujukan Ms'=>array('index'),
	$model->nilairujukan_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' SANilaiRujukanM #'.$model->nilairujukan_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                array_push($arrMenu,array('label'=>Yii::t('mds','List').' SANilaiRujukanM', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' SANilaiRujukanM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' SANilaiRujukanM', 'icon'=>'pencil','url'=>array('update','id'=>$model->nilairujukan_id))) :  '' ;
                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' SANilaiRujukanM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->nilairujukan_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' SANilaiRujukanM', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nilairujukan_id',
		'pemeriksaanlab_id',
		'nilairujukan_jeniskelamin',
		'kelompokumur',
		'nilairujukan_nama',
		'nilairujukan_min',
		'nilairujukan_max',
		'nilairujukan_satuan',
		'nilairujukan_metode',
		'nilairujukan_keterangan',
		'nilairujukan_aktif',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>