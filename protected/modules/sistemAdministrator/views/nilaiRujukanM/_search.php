<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sanilai-rujukan-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'nilairujukan_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pemeriksaanlab_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_jeniskelamin',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'kelompokumur',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_nama',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_min',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_max',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_satuan',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'nilairujukan_metode',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textAreaRow($model,'nilairujukan_keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->checkBoxRow($model,'nilairujukan_aktif'); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
