<?php
$this->breadcrumbs=array(
	'Saalatmedis Ms'=>array('index'),
	$model->alatmedis_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Alat Medis ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' SAAlatmedisM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' SAAlatmedisM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' SAAlatmedisM', 'icon'=>'pencil','url'=>array('update','id'=>$model->alatmedis_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' SAAlatmedisM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->alatmedis_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Alat Medis', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'alatmedis_id',
		'instalasi_id',
		'jenisalatmedis_id',
		'alatmedis_nama',
		'alatmedis_namalain',
                                array(
                                    'label'=>'Aktif',
                                    'value'=>(($model->alatmedis_aktif==1)? "Ya" : "Tidak"),
                                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>