<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'saalatmedis-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'alatmedis_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'instalasi_id',array('class'=>'span5')); ?>

	<?php// echo $form->textFieldRow($model,'jenisalatmedis_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'alatmedis_nama',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'alatmedis_namalain',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->checkBoxRow($model,'alatmedis_aktif',array('checked'=>'alatmedis_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
