
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

    $table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchPrint();
         $template = "{pager}{summary}\n{items}";
    }

$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'alatmedis_id',
		array(
                                    'header'=>'ID',
                                    'value'=>'$data->alatmedis_id',
                                ),
		'instalasi.instalasi_nama',
		'jenisalatmedis.jenisalatmedis_nama',
		'alatmedis_nama',
		'alatmedis_namalain',
//		'alatmedis_aktif',
                                array
                                    (
                                            'name'=>'alatmedis_aktif',
                                            'type'=>'raw',
                                            'value'=>'($data->alatmedis_aktif==1)? Yii::t("mds","Yes") : Yii::t("mds","No")',
                                    ),
 
        ),
    )); 
?>