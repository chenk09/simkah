<?php
$this->breadcrumbs=array(
	'Sapemeriksaan Labms'=>array('index'),
	$model->pemeriksaanlab_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Pemeriksaan Laboratorium', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Pemeriksaan LAB', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Pemeriksaan LAB', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Pemeriksaan LAB', 'icon'=>'pencil','url'=>array('update','id'=>$model->pemeriksaanlab_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Pemeriksaan LAB','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->pemeriksaanlab_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').'Pemeriksaan Laboratorium', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'pemeriksaanlab_id',
		'daftartindakan_id',
		'jenispemeriksaanlab_id',
		'pemeriksaanlab_kode',
		'pemeriksaanlab_urutan',
		'pemeriksaanlab_nama',
		'pemeriksaanlab_namalainnya',
		'pemeriksaanlab_aktif',
                                array(
                                    'label'=>'Aktif',
                                    'value'=>(($model->pemeriksaanlab_aktif==1)? "Ya" : "Tidak"),
                                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>