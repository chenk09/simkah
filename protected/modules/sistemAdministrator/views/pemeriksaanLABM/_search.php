<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sapemeriksaan-labm-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'pemeriksaanlab_id',array('class'=>'span3')); ?>

	<?php //echo $form->textFieldRow($model,'daftartindakan_nama',array('class'=>'span3')); ?>
	<div class="control-group ">
        <label class="control-label" for="bidang">Daftar Tindakan</label>
        <div class="controls">
            <?php echo $form->hiddenField($model,'daftartindakan_id'); ?>
            
        <?php 
                $this->widget('MyJuiAutoComplete', array(
                                
                                'name'=>'daftartindakan_nama',
                                'source'=>'js: function(request, response) {
                                               $.ajax({
                                                   url: "'.Yii::app()->createUrl('ActionAutoComplete/Tindakan').'",
                                                   dataType: "json",
                                                   data: {
                                                       term: request.term,
                                                   },
                                                   success: function (data) {
                                                           response(data);
                                                   }
                                               })
                                            }',
                                 'options'=>array(
                                       'showAnim'=>'fold',
                                       'minLength' => 2,
                                       'focus'=> 'js:function( event, ui ) {
                                            $(this).val( ui.item.label);
                                            return false;
                                        }',
                                       'select'=>'js:function( event, ui ) { 
                                            $("#'.CHtml::activeId($model, 'daftartindakan_id').'").val(ui.item.daftartindakan_id);
                                            $("#daftartindakan_nama").val(ui.item.daftartindakan_nama);
                                            return false;
                                        }',
                                ),
                                'htmlOptions'=>array(
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                ),
                                'tombolDialog'=>array('idDialog'=>'dialogTindakan'),
                            )); 
             ?>
        </div>
    </div>

	<?php //echo $form->textFieldRow($model,'jenispemeriksaanlab_nama',array('class'=>'span3')); ?>
    <div class="control-group ">
	  <label class="control-label" for="bidang">Jenis Pemeriksaan </label>
        <div class="controls">
			<?php echo $form->dropDownList($model,'jenispemeriksaanlab_id',  CHtml::listData($modPemeriksaanLab->JenispemeriksaanLABItems, 'jenispemeriksaanlab_id', 'jenispemeriksaanlab_nama'),array('class'=>'span3','empty'=>'--Jenis Pemeriksaan Lab--')); ?>
		</div>
	</div>

	<?php echo $form->textFieldRow($model,'pemeriksaanlab_kode',array('class'=>'span3','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'pemeriksaanlab_urutan',array('class'=>'span3')); ?>

	<?php echo $form->textFieldRow($model,'pemeriksaanlab_nama',array('class'=>'span3','maxlength'=>40)); ?>

	<?php //echo $form->textFieldRow($model,'pemeriksaanlab_namalainnya',array('class'=>'span3','maxlength'=>40)); ?>

	<?php echo $form->checkBoxRow($model,'pemeriksaanlab_aktif',array('checked'=>true)); ?>




	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
<?php
//========= Dialog buat cari data Bidang =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogTindakan',
    'options'=>array(
        'title'=>'Daftar Tindakan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>750,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modTindakanRad = new TariftindakanperdatotalV('search');
$modTindakanRad->unsetAttributes();
if(isset($_GET['TariftindakanperdatotalV']))
    $modTindakanRad->attributes = $_GET['TariftindakanperdatotalV'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'sainstalasi-m-grid',
	'dataProvider'=>$modTindakanRad->search(),
	'filter'=>$modTindakanRad,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",
                                "#",
                                array(
                                    "class"=>"btn-small", 
                                    "id" => "selectTindakan",
                                    "onClick" => "
                                    $(\"#'.CHtml::activeId($modPemeriksaanLab, 'daftartindakan_id').'\").val(\'$data->daftartindakan_id\');
                                    $(\"#daftartindakan_nama\").val(\'$data->daftartindakan_nama\');
                                    $(\'#dialogTindakan\').dialog(\'close\');return false;"))'
                ),
            'kelompoktindakan_nama',
            'kategoritindakan_nama',
            'daftartindakan_kode',
            'daftartindakan_nama',
            'harga_tariftindakan',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget();
?>