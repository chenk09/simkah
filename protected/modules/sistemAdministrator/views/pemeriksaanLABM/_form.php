<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sanilai-rujukan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>


<table>
    <tr>
        <td>
            <div class="control-group ">
                    <label class="control-label" for="bidang">Daftar Tindakan / Pemeriksaan</label>
                    <div class="controls">
                        <?php echo $form->hiddenField($modPemeriksaanLab,'daftartindakan_id'); ?>
                        
                    <?php 
                            $this->widget('MyJuiAutoComplete', array(
                                            
                                            'name'=>'daftartindakan_nama',
                                            'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/Tindakan').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                             'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
                                                   'focus'=> 'js:function( event, ui ) {
                                                        $(this).val( ui.item.label);
                                                        return false;
                                                    }',
                                                   'select'=>'js:function( event, ui ) { 
                                                        $("#'.CHtml::activeId($modPemeriksaanLab, 'daftartindakan_id').'").val(ui.item.daftartindakan_id);
                                                        $("#daftartindakan_nama").val(ui.item.daftartindakan_nama);
                                                        return false;
                                                    }',
                                            ),
                                            'htmlOptions'=>array(
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogTindakan'),
                                        )); 
                         ?>
                    </div>
                </div>
            <?php //echo $form->dropDownList($modPemeriksaanLab,'daftartindakan_id',CHtml::listData($modPemeriksaanLab->DaftarTindakanItems, 'daftartindakan_id', 'daftartindakan_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'empty'=>'--'.$modPemeriksaanLab->getAttributeLabel('daftartindakan_id').'--')); ?>
        
            <br>
            <label class="control-label" for="bidang">Jenis Pemeriksaan </label>
                    <div class="controls">
                        
            <?php echo $form->dropDownList($modPemeriksaanLab,'jenispemeriksaanlab_id',  CHtml::listData($modPemeriksaanLab->JenispemeriksaanLABItems, 'jenispemeriksaanlab_id', 'jenispemeriksaanlab_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'empty'=>'--'.$modPemeriksaanLab->getAttributeLabel('jenispemeriksaanlab_id').'--')); ?>
         </div></br>
                
                    </td>
    
        <td>
            <label class="control-label" for="bidang">Kode / Urutan </label>
                    <div class="controls">
            <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_kode',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_kode'))); ?>
            <?php echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_urutan',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);",'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_urutan'))); ?>
                    </div>
            <br>
            <?php echo CHtml::css('ul.redactor_toolbar{z-index:10;}'); ?>
            <label class="control-label" for="bidang">Nama </label>
                    <div class="controls">
                        <?php// echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>40,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_nama'))); ?>
                        <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modPemeriksaanLab,'attribute'=>'pemeriksaanlab_nama','toolbar'=>'mini','height'=>'25px')) ?>
                    </div></br>
            <label class="control-label" for="bidang">Nama Lainnya </label>
                    <div class="controls">
                        <?php //echo $form->textField($modPemeriksaanLab,'pemeriksaanlab_namalainnya',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>40,'placeholder'=>$modPemeriksaanLab->getAttributeLabel('pemeriksaanlab_namalainnya'))); ?>
                        <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modPemeriksaanLab,'attribute'=>'pemeriksaanlab_namalainnya','toolbar'=>'mini','height'=>'25px')) ?>
                    </div></br>
            
                    </td>
    </tr>
</tr
    <tr>
                <td>
            
        </td>
        <td>
            
        </td>
    </tr>
</table>
                
        
         <?php echo CHtml::checkBox('sama',false,array('onclick'=>'samaIsi();'));?>  
        <?php echo $form->label($model,'NILAI RUJUKAN LAKI LAKI SAMA DENGAN NILAI RUJUKAN PEREMPUAN'); ?>
        

<table class="table">
    <tr>
<?php
$jenisKelamin=JenisKelamin::items();
$jumlahJK=count($jenisKelamin);
foreach($jenisKelamin AS $dataJK):
    echo "<td width=\"50%\">".$form->checkBox($model,'nilairujukan_jeniskelamin[]',array('value'=>$dataJK,'uncheckValue'=>'N', 'onclick'=>'checkIni(this);', 'checked'=>'checked'))." ".$dataJK."
            <table class='groupingIsi'>
                ";
        $kelompokUmur=KelompokUmur::items();
    foreach($kelompokUmur AS $dataKelUmur):
        echo "<tr><td>".$dataKelUmur.$form->hiddenField($model,'kelompokumur['.$dataJK.'][]',array('value'=>$dataKelUmur,'readonly'=>TRUE))."</td>
                  <td>".$form->textField($model,'nilairujukan_min['.$dataJK.'][]',array('class'=>'span1','group'=>'min', 'placeholder'=>$model->getAttributeLabel('nilairujukan_min'),'onkeyUp'=>'samaMin(this)'))."</td>
                  <td>".$form->textField($model,'nilairujukan_max['.$dataJK.'][]',array('class'=>'span1','group'=>'max', 'placeholder'=>$model->getAttributeLabel('nilairujukan_max'),'onkeyUp'=>'samaMax(this)'))."</td>
                  <td>".$form->textField($model,'nilairujukan_nama['.$dataJK.'][]',array('class'=>'span2', 'group'=>'nama' ,'placeholder'=>$model->getAttributeLabel('nilairujukan_nama')))."</td>
              </tr>";
    endforeach;
    echo "      
            </table>
            </td>";    
endforeach;    
 ?>
    </tr>   
</table>
<table class="table">
    <tr>
        <td>
              <?php echo $form->dropDownList($model,'nilairujukan_satuan',SatuanHasilLab::items(),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'empty'=>'--'.$model->getAttributeLabel('nilairujukan_satuan').'--')); ?>
        </td>
        <td>
              <?php echo $form->textField($model,'nilairujukan_metode',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30,'placeholder'=>$model->getAttributeLabel('nilairujukan_metode'))); ?>
        </td>
        <td>
              <?php echo $form->textArea($model,'nilairujukan_keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'placeholder'=>$model->getAttributeLabel('nilairujukan_keterangan'))); ?>
        </td>
    </tr>
</table>    
        <table class="table">
            <tr>
               <td>
                </td>
            </tr>
        </table>
            <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.nilaiRujukanM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
		<?php
$content = $this->renderPartial('../tips/tipsaddedit3a',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
            </div>

<?php $this->endWidget(); ?>

<?php
 $kelompokUmur=KelompokUmur::items();
 foreach ($kelompokUmur AS $data2) {
    
}
$jenisKelamin=JenisKelamin::items();
 
$js = <<< JS

function samaMin(obj)
{
        isiMin=$(obj).val();
        isiMax=$(obj).parent().next().find('input').val();
        MinMAxConcat = isiMin + '-' + isiMax;
        $(obj).parent().next().next().find('input').val(MinMAxConcat);
} 

function samaMax(obj)
{
        isiMax=$(obj).val();
        isiMin=$(obj).parent().prev().find('input').val();
        MinMAxConcat = isiMin + '-' + isiMax;
        $(obj).parent().next().find('input').val(MinMAxConcat);
} 

function checkIni(obj)
{
    if ($(obj).is(':checked')){
        $(obj).parents('td').find('.groupingIsi').slideDown('fast');
    }
    else{
        $(obj).parents('td').find('.groupingIsi').slideUp('fast');
    }
       
} 

function samaIsi()
{
    $('.table .groupingIsi tr:first input').each(function(){
        var group = $(this).attr('group');
        var value = $(this).val();
        $(this).parents('.table').find('input[group*='+group+']').val(value);
    });
} 

function samaIsi2(obj)
{
    $(obj).parents('td').find('.groupingIsi tr:first input').each(function(){
        var group = $(this).attr('group');
        var value = $(this).val();
        $(this).parents('.groupingIsi').find('input[group*='+group+']').val(value);
    });
} 


JS;
Yii::app()->clientScript->registerScript('samaInputan',$js,CClientScript::POS_HEAD);
?>
<?php
//========= Dialog buat cari data Bidang =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogTindakan',
    'options'=>array(
        'title'=>'Daftar Tindakan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>750,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modTindakanRad = new TariftindakanperdatotalV('search');
$modTindakanRad->unsetAttributes();
if(isset($_GET['TariftindakanperdatotalV']))
    $modTindakanRad->attributes = $_GET['TariftindakanperdatotalV'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'sainstalasi-m-grid',
	'dataProvider'=>$modTindakanRad->search(),
	'filter'=>$modTindakanRad,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",
                                "#",
                                array(
                                    "class"=>"btn-small", 
                                    "id" => "selectTindakan",
                                    "onClick" => "
                                    $(\"#'.CHtml::activeId($modPemeriksaanLab, 'daftartindakan_id').'\").val(\'$data->daftartindakan_id\');
                                    $(\"#daftartindakan_nama\").val(\'$data->daftartindakan_nama\');
                                    $(\'#dialogTindakan\').dialog(\'close\');return false;"))'
                ),
            'kelompoktindakan_nama',
            'kategoritindakan_nama',
            'daftartindakan_kode',
            'daftartindakan_nama',
            'harga_tariftindakan',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget();
?>

<?php 
//Yii::app()->clientScript->registerScript('ready', '
//    $("#groupingIsi tbody tr:first input").keyup(function(){
//
//    });
//', CClientScript::POS_READY);
