
<?php
$this->breadcrumbs=array(
	'Sapemeriksaan Labms'=>array('index'),
	$model->pemeriksaanlab_id=>array('view','id'=>$model->pemeriksaanlab_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Pemeriksaan Laboratorium', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Pemeriksaan LAB', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Pemeriksaan LAB', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Pemeriksaan LAB', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->pemeriksaanlab_id))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pemeriksaan Laboratorium', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>
 <?php echo $this->renderPartial('_formUpdate',array('model'=>$model,'modPemeriksaanLab'=>$modPemeriksaanLab,'tableJk'=>$tableJk)); ?>
<?php //$this->widget('TipsMasterData',array('type'=>'update'));?>
 <?php //echo $this->renderPartial('_form',array('model'=>$model,'modPemeriksaanLab'=>$modPemeriksaanLab,'tableJk'=>$tableJk)); ?>

