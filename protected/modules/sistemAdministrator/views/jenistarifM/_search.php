<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sajenis-tarif-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'jenistarif_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenistarif_nama',array('class'=>'span3','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'jenistarif_namalainnya',array('class'=>'span3','maxlength'=>25)); ?>

	<?php echo $form->checkBoxRow($model,'jenistarif_aktif', array('checked'=>'$data->jenistarif_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
