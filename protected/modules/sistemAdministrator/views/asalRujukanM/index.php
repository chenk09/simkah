<?php
$this->breadcrumbs=array(
	'Saasal Rujukan Ms',
);

$this->menu=array(
        array('label'=>Yii::t('mds','List').' Asal Rujukan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master')),
	//array('label'=>Yii::t('mds','Create').' Asal Rujukan', 'icon'=>'file', 'url'=>array('create')),
	array('label'=>Yii::t('mds','Manage').' Asal Rujukan', 'icon'=>'folder-open', 'url'=>array('admin')),
);

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'list'));?>