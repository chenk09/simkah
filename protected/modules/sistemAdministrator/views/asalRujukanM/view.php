<?php
$this->breadcrumbs=array(
	'Saasal Rujukan Ms'=>array('index'),
	$model->asalrujukan_id,
);

$this->menu=array(
        array('label'=>Yii::t('mds','View').' Asal Rujukan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master')),
//	array('label'=>Yii::t('mds','List').' Asal Rujukan', 'icon'=>'list', 'url'=>array('index')),
//	array('label'=>Yii::t('mds','Create').' Asal Rujukan', 'icon'=>'file', 'url'=>array('create')),
//        array('label'=>Yii::t('mds','Update').' Asal Rujukan', 'icon'=>'pencil','url'=>array('update','id'=>$model->asalrujukan_id)),
//	array('label'=>Yii::t('mds','Delete').' Asal Rujukan','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->asalrujukan_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('mds','Manage').' Asal Rujukan', 'icon'=>'folder-open', 'url'=>array('admin')),
);

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'asalrujukan_id',
		'asalrujukan_nama',
		'asalrujukan_institusi',
		'asalrujukan_namalainnya',
		'asalrujukan_aktif',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>