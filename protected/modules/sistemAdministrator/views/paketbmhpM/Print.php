
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

//$this->widget('ext.bootstrap.widgets.BootGridView',array(
//	'id'=>'sajenis-kelas-m-grid',
//        'enableSorting'=>false,
//	'dataProvider'=>$model->searchPrint(),
//        'template'=>"{pager}{summary}\n{items}",
//        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//	'columns'=>array(
//		////'paketbmhp_id',
//		array(
//                        'header'=>'ID',
//                        'value'=>'$data->paketbmhp_id',
//                ),
//		'daftartindakan.daftartindakan_nama',
//		'tipepaket.tipepaket_nama',
//		'satuankecil.satuankecil_nama',
//		'obatalkes.obatalkes_nama',
//		'qtypemakaian',
//		'qtystokout',
//		'hargapemakaian',
//		'kelompokumur.kelompokumur_nama',
//        ),
//    )); 
?>

<?php $modTipePaket = new SATipePaketM('search'); ?>
<?php 
      $table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $modTipePaket->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $modTipePaket->searchPrint();
         $template = "{pager}{summary}\n{items}";
    }
    
    $this->widget($table,array( 
    'id'=>'satipe-paket-m-grid', 
    'dataProvider'=>$data, 
    'enableSorting'=>$sort,
    //'filter'=>$modTipePaket, 
    'template'=>$template, 
    'itemsCssClass'=>'table table-striped table-bordered table-condensed', 
    'columns'=>array( 
        ////'tipepaket_id',
//        array( 
//                        'name'=>'tipepaket_id', 
//                        'value'=>'$data->tipepaket_id', 
//                        'filter'=>false, 
//                ),
//        'kelaspelayanan_id',
//        'penjamin_id',
//        'carabayar_id',
        array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
        ),
        'tipepaket_nama',
        array(
                     'header'=>'Nama Tindakan',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_daftarTindakan\',array(\'tipepaket_id\'=>$data[tipepaket_id]),true)',
        ),
        //'tipepaket_singkatan',
        /*
        'tipepaket_namalainnya',
        'tglkesepakatantarif',
        'nokesepakatantarif',
        'tarifpaket',
        'paketsubsidiasuransi',
        'paketsubsidipemerintah',
        'paketsubsidirs',
        'paketiurbiaya',
        'nourut_tipepaket',
        'keterangan_tipepaket',
        'tipepaket_aktif',
        */
        
    ), 
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}', 
)); ?> 