
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'sapaketbmhp-m-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
    'focus' => '#SAPaketbmhpM_tipepaket_id',
        ));
?>

<p class="help-block"><?php echo Yii::t('mds', 'Fields with <span class="required">*</span> are required.') ?></p>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->dropDownListRow($model, 'tipepaket_id', CHtml::listData(TipepaketM::model()->findAll(), 'tipepaket_id', 'tipepaket_nama'), array('empty' => '-- pilih --', 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
<?php echo $form->hiddenField($model, 'tipepaket_id', array('class' => 'idTipePaket')); ?>
<?php echo $form->dropDownListRow($model, 'kelompokumur_id', CHtml::listData(KelompokumurM::model()->findAll(), 'kelompokumur_id', 'kelompokumur_nama'), array('empty'=>'-- Pilih --' , 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->dropDownListRow($model,'daftartindakan_id',CHtml::listData(DaftartindakanM::model()->findAll(), 'daftartindakan_id','daftartindakan_nama') , array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>            
<div class="control-group ">
    <?php echo $form->label($model, 'daftartindakan_id', array('class' => 'control-label')); ?>
    <?php echo CHtml::hiddenField('idDaftarTindakan', ''); ?>
    <div class="controls">
        <div class="input-append" style='display:inline'>
                <?php
                    $this->widget('MyJuiAutoComplete', array(
                        'model' => $model,
                        'attribute' => 'daftartindakan_id',
                        'sourceUrl' => 'js: function(request, response) {
                                   $.ajax({
                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/getDaftarTindakan').'",
                                       dataType: "json",
                                       data: {
                                           term: request.term,
                                           idKelasPelayanan: $("#idKelasPelayanan").val(),
                                       },
                                       success: function (data) {
                                               response(data);
                                       }
                                   })
                                }',
                        'options' => array(
                            'showAnim' => 'fold',
                            'minLength' => 2,
                            'focus' => 'js:function( event, ui ) {
                                                        $(this).val( ui.item.label);
                                                        return false;
                                                    }',
                            'select' => 'js:function( event, ui ) {
                                                              $("#idDaftarTindakan").val(ui.item.daftartindakan_id);
                                                              //$("#idTarifTindakan").val(ui.item.tariftindakan_id);
                                                              $(this).val(ui.item.label);
                                                              return false;
                                                    }',
                        ),
                        'htmlOptions' => array('value' => '', 'onkeypress' => "return $(this).focusNextInputField(event)",
                            'class' => 'span3 ',
                        ),
                        'tombolDialog'=>array('idDialog'=>'dialogDaftarTindakan'),
                ));
                ?>
            <?php
            // $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            //     'model' => $model,
            //     'attribute' => 'daftartindakan_id',
            //     'sourceUrl' => 'js: function(request, response) {
            //                        $.ajax({
            //                            url: "'.Yii::app()->createUrl('ActionAutoComplete/getDaftarTindakan').'",
            //                            dataType: "json",
            //                            data: {
            //                                term: request.term,
            //                                idKelasPelayanan: $("#idKelasPelayanan").val(),
            //                            },
            //                            success: function (data) {
            //                                    response(data);
            //                            }
            //                        })
            //                     }',
            //     'options' => array(
            //         'showAnim' => 'fold',
            //         'minLength' => 2,
            //         'focus' => 'js:function( event, ui ) {
            //                                                 $(this).val( ui.item.label);
            //                                                 return false;
            //                                             }',
            //         'select' => 'js:function( event, ui ) {
            //                                                       $("#idDaftarTindakan").val(ui.item.daftartindakan_id);
            //                                                       $(this).val(ui.item.label);

            //                                             }',
            //     ),
            //     'htmlOptions' => array('value'=>'', 'onkeypress' => "return $(this).focusNextInputField(event)",
            //         'class' => 'span3 ',
            //     ),
            // ));
            ?>
            <!-- <span class="add-on" style='margin-right:3px;'>
                <i class="icon-list-alt"></i>
            </span> -->
            <?php
            // echo CHtml::htmlButton('<i class="icon-search icon-white"></i>', array('onclick' => '$("#dialogDaftarTindakan").dialog("open");return false;',
            //     'class' => 'btn btn-primary',
            //     'onkeypress' => "return $(this).focusNextInputField(event)",
            //     'rel' => "tooltip",
            //     'readOnly'=>true,
            //     'id' => 'searchDaftarTindakan',
            //     'title' => "Klik Untuk Menambahkan Daftar Tindakan",));
            ?>

        </div>      

    </div>

</div>
<div class="control-group ">
    <?php echo $form->label($model, 'obatalkes_id', array('class' => 'control-label')); ?>
    <?php echo CHtml::hiddenField('idObatAlkes', ''); ?>
    <?php echo CHtml::hiddenField('idKelasPelayanan', ''); ?>
    <div class="controls">
        <div class="input-append" style='display:inline'>
           <?php
                    $this->widget('MyJuiAutoComplete', array(
                        'model' => $model,
                        'attribute' => 'obatalkes_id',
                        'sourceUrl' => 'js: function(request, response) {
                                   $.ajax({
                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/obatAlkesWithSumberDana').'",
                                       dataType: "json",
                                       data: {
                                           term: request.term,
                                           //idKelasPelayanan: $("#idKelasPelayanan").val(),
                                       },
                                       success: function (data) {
                                               response(data);
                                       }
                                   })
                                }',
                        'options' => array(
                            'showAnim' => 'fold',
                            'minLength' => 2,
                            'focus' => 'js:function( event, ui ) {
                                                        $(this).val( ui.item.label);
                                                        return false;
                                                    }',
                            'select' => 'js:function( event, ui ) {
                                                              $("#idObatAlkes").val(ui.item.obatalkes_id);
                                                              $("#qtyPakai").val(0);
                                                              $("#hargaPakai").val(0);

                                                              $("#'.CHtml::activeId($model, 'obatalkes_id').'").val(ui.item.label);
                                                              return false;
                                                    }',
                        ),
                        'htmlOptions' => array('value' => '', 'onkeypress' => "return $(this).focusNextInputField(event)",
                            'class' => 'span3 ',
                        ),
                        'tombolDialog'=>array('idDialog'=>'dialogObatAlkes'),
                ));
                ?>
           <!--  <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'model' => $model,
                'attribute' => 'obatalkes_id',
                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/obatAlkesWithSumberDana'),
                'options' => array(
                    'showAnim' => 'fold',
                    'minLength' => 2,
                    'focus' => 'js:function( event, ui ) {
                                                                                                                       return false;
                                                        }',
                    'select' => 'js:function( event, ui ) {
                                                                  $("#idObatAlkes").val(ui.item.obatalkes_id);
                                                                  $("#qtyPakai").val(0);
                                                                  $("#hargaPakai").val(0);

                                                                  $("#'.CHtml::activeId($model, 'obatalkes_id').'").val(ui.item.label);

                                                        }',
                ),
                'htmlOptions' => array('value'=>'','onkeypress' => "return $(this).focusNextInputField(event)",
                    'class' => 'span3',
                ),
            ));
            ?>
            <span class="add-on" style='margin-right:3px;'>
                <i class="icon-list-alt"></i>
            </span>

            <?php
            echo CHtml::htmlButton('<i class="icon-search icon-white"></i>', array('onclick' => '$("#dialogObatAlkes").dialog("open");return false;',
                'class' => 'btn btn-primary',
                'onkeypress' => "return $(this).focusNextInputField(event)",
                'rel' => "tooltip",
                'readOnly'=>true,
                'id' => 'searchDaftarObat',
                'title' => "Klik Untuk Menambahkan Daftar Tindakan",));
            ?> -->
        </div>      

    </div>

</div>
<div class="control-group ">
    <label class="control-label">Qty / Harga Pemakaian</label>
    <div class="controls">
            <?php echo CHtml::textField('qtyPakai','',array('class'=>'span1 numbersOnly', 'placeholder'=>'qty', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?> / 
            <?php echo CHtml::textField('hargaPakai','',array('class'=>'span1 numbersOnly', 'placeholder'=>'harga', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>
            <?php
            echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>', array('onclick' => 'submitPaketBMHP();return false;',
                'class' => 'btn btn-primary',
                'onkeypress' => "submitPaketBMHP();return $(this).focusNextInputField(event)",
                'rel' => "tooltip",
                
                'id' => 'tambahDaftarTindakan',
                'title' => "Klik Untuk Menambahkan Daftar Tindakan",));
            ?>
    </div>
</div>
<?php //echo $form->dropDownListRow($model,'satuankecil_id', CHtml::listData(SatuankecilM::model()->findAll(), 'satuankecil_id', 'satuankecil_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->dropDownListRow($model,'obatalkes_id',CHtml::listData(ObatalkesM::model()->findAll(), 'obatalkes_id', 'obatalkes_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'qtypemakaian',array('class'=>'span3 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event); ")); ?>
<?php //echo $form->textFieldRow($model,'qtystokout',array('class'=>'span3 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'hargapemakaian',array('class'=>'span3 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);"));  ?>

<br/>
<table id="tablePaketBMHP" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th><?php echo CHtml::checkBox('checkListUtama', true, array('onclick' => 'checkAll(\'cekList\',this);hitungSemua();', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?></th>
            <th>No.Urut</th>
            <th>Tipe Paket</th>
            <th>Daftar Tindakan</th>
            <th>Satuan Kecil</th>
            <th>Nama Obat Alkes</th>
            <th>Qty Pemakaian</th>
            
            <th>Harga Pemakaian</th>            
        </tr>
    </thead>
    <?php if (count($dataPaketBMHP) > 0) { ?>
        <tbody>
            <?php
            $modPaketBMHP = new PaketbmhpM();
            $i = 1;

            foreach ($dataPaketBMHP as $tampilDetail) {
                ?>

                <?php

                echo "<tr>
                        <td>".CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                        <td>" . CHtml::TextField('noUrut', $i,  array('class' => 'span1 noUrut', 'readonly' => TRUE)) .
                                CHtml::activeHiddenField($modPaketBMHP, 'tipepaket_id[]', array('value' => $tampilDetail->tipepaket_id)) .
                                CHtml::activeHiddenField($modPaketBMHP, 'daftartindakan_id[]', array('value' => $tampilDetail->daftartindakan_id)) .
                                CHtml::activeHiddenField($modPaketBMHP, 'obatalkes_id[]', array('value'=>$tampilDetail->obatalkes_id)) .
                                CHtml::activeHiddenField($modPaketBMHP, 'paketbmhp_id[]', array('value'=>$tampilDetail->paketbmhp_id)) .
                                CHtml::activeHiddenField($modPaketBMHP, 'satuankecil_id[]', array('value'=>$tampilDetail->satuankecil_id)) .
                    "</td>
                        <td>" . $tampilDetail->tipepaket->tipepaket_nama . "</td>
                        <td>" . $tampilDetail->daftartindakan->daftartindakan_nama . "</td>
                        <td>" . $tampilDetail->satuankecil->satuankecil_nama . "</td>
                        <td>" . $tampilDetail->obatalkes->obatalkes_nama . "</td>
                        
                        <td>" . CHtml::activeTextField($modPaketBMHP, 'qtypemakaian[]', array('value' => $tampilDetail->qtypemakaian, 'class' => 'span1 qtypemakaian numbersOnly', 'onkeyup'=>'numberOnly(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                        
                        <td>" . CHtml::activeTextField($modPaketBMHP, 'hargapemakaian[]', array('value' => $tampilDetail->hargapemakaian, 'class' => 'span1 hargapemakaian numbersOnly', 'onkeyup'=>'numberOnly(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                    </tr>
                    ";
                $i++;
            }
            ?>
        </tbody>
    <?php } ?>
</table>
<br/>
<div class="form-actions">
    <?php
    echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')) :
                    Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)'));
    ?>
    <?php
    echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), Yii::app()->createUrl($this->module->id . '/' . paketbmhpM . '/admin'), array('class' => 'btn btn-danger',
        'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;'));
    ?>
	<?php
$content = $this->renderPartial('../tips/tips',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>

<?php $this->endWidget(); ?>

<?php
$idTipePaket = CHtml::activeId($model, 'tipepaket_id');
$idKelompokUmur = CHtml::activeId($model, 'kelompokumur_id');
$urlGetPaketBMHP = Yii::app()->createUrl('actionAjax/getPaketBMHP');
$urlGetTipePaket = Yii::app()->createUrl('actionAjax/getTipePaket');
//$urlHalamanIni = Yii::app()->createUrl($this->module->id.'/'.$this->getId().'/'.$this->action);

Yii::app()->clientScript->registerScript('angka', "
$(document).ready(function () {
        
        var idTipePaket = $('#${idTipePaket}').val();
        if (idTipePaket == ''){
            $('#${idTipePaket}').focus();
        }
        else{
            getPaket(idTipePaket);
            $('#${idTipePaket}').attr('disabled','disabled');
        }
        
        $('#${idTipePaket}').change(function(){
            if ($(this).val() != ''){
                $('.idTipePaket').val($(this).val());
                var tipePaket = $('.idTipePaket').val();
                getPaket(tipePaket);
                $(this).attr('disabled','disabled');
                $('#${idKelompokUmur}').focus();
                $.post('${urlGetPaketBMHP}', {tipePaket:tipePaket},
                function(data){
                    if (data.paket == 'Ada'){
                        var answer = confirm('Paket BMHP Telah Tersedia ingin Melanjutkan ?');
                        if (answer){
                           window.location = '".Yii::app()->createUrl($this->module->id.'/'.$this->getId().'/'.'update&id=')."'+tipePaket;
                        }
                        else{
                           window.location = '".Yii::app()->createUrl($this->module->id.'/'.$this->getId().'/'.'admin')."';
                        }
                    }
                    else if (data.paket == null){
                    }
                }, 'json');
            }
        });
        $('.numbersOnly').keypress(function(event) {
                var charCode = (event.which) ? event.which : event.keyCode
                if ((charCode >= 48 && charCode <= 57)
                        || charCode == 8
                        || charCode == 46
                        || charCode == 44)
                        return true;
                return false;
        });
        
        $('form').submit(function(){
                
                if (cekValidasi() == false)
                    return false;
                else{
                    return true;
                }
        });
});
", CClientScript::POS_HEAD);

$JSPaketBMHP = <<< JS
function getPaket(tipePaket){
        $.post('${urlGetTipePaket}', {idTipePaket:tipePaket},
        function(data){
           $('#idKelasPelayanan').val(data.kelaspelayanan_id);
            $.get('${urlHalamanIni}', {idKelasPelayanan:data.kelaspelayanan_id}, function(datas){
                $.fn.yiiGridView.update('satarif-tindakan-m-grid', {
                    url: document.URL+'&SATarifTindakanM%5Bkelaspelayanan_id%5D='+data.kelaspelayanan_id+'&SATarifTindakanM_page=1',
                    
                }); 
            });
        }, 'json');
        
        
        
}
function submitPaketBMHP()
{
    idDaftarTindakan = $('#idDaftarTindakan').val();
    idTipePaket = $('.idTipePaket').val();
    idObatAlkes = $('#idObatAlkes').val()
    qtyPemakaian = $('#qtyPakai').val()
    hargaPemakaian = $('#hargaPakai').val();
    
   
    if(idTipePaket==''){
        alert('Silahkan Pilih Tipe Paket Terlebih Dahulu');
    } 
    else if(idDaftarTindakan==''){
        alert('Silahkan Pilih Daftar Tindakan Terlebih Dahulu');
    }
    else if(idObatAlkes==''){
        alert('Silahkan Pilih Obat Alkes Terlebih Dahulu');
    }
     else{
        $.post("${urlGetPaketBMHP}", {qtyPemakaian:qtyPemakaian, hargaPemakaian:hargaPemakaian, idDaftarTindakan: idDaftarTindakan, idTipePaket:idTipePaket , idObatAlkes : idObatAlkes},
        function(data){
            $('#tablePaketBMHP').append(data.tr);
            
            $('.noUrut').each(function() {
                  $(this).val(noUrut);
                  $(this).parents('tr').find('#checkList').attr('name','checkList['+(noUrut-1)+']');
                  noUrut = noUrut + 1;

            });
        }, "json");
    }   
    
    noUrut = 1;
    total =0;
    
}

function numberOnly(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;


    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")

        if ($.isNumeric(orignalValue)){
                $(obj).val(orignalValue);
        }else{
         $(obj).val(0);
        }
    }else{
        $(obj).val(1);
    }
}

function cekValidasi(){
          banyaknyaTindakan = $('.cekList').length;
          jumlahCek = 0;
          noUrut = 1;
          $('.noUrut').each(function() {
                  $(this).val(noUrut);
                  $(this).parents('tr').find('#checkList').attr('name','checkList['+(noUrut-1)+']');
                  noUrut = noUrut + 1;

            });
          $('.cekList').each(function(){
                if ($(this).is(':checked')){
                    jumlahCek++;
                }
          });
          if ($('.isRequired').val()==''){
              alert ('Harap Isi Semua Data Yang Bertanda *');
                return false;
          }else if(banyaknyaTindakan<1){
             alert('Anda Belum memilih Daftar Tindakan');   
             return false;
          }else if(jumlahCek<1){
             alert('Anda Belum Memilih Daftar Tindakan');   
             return false;
          }else{
             return true;
          }
    }
JS;
Yii::app()->clientScript->registerScript('paketBMHP', $JSPaketBMHP, CClientScript::POS_HEAD);
?>

<?php
//========= Dialog buat cari data obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDaftarTindakan',
    'options' => array(
        'title' => 'Pencarian Daftar Tindakan',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 600,
        'resizable' => false,
    ),
));

$modTarifTindakan = new SATarifTindakanM('search');
$modTarifTindakan->unsetAttributes();
$modTarifTindakan->kelaspelayanan_id = 0;
if (isset($_GET['SATarifTindakanM'])) {
    $modTarifTindakan->attributes = $_GET['SATarifTindakanM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id'=>'satarif-tindakan-m-grid', 
    //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider' => $modTarifTindakan->searchDaftarTindakan(),
    'filter' => $modTarifTindakan,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectDaftarTindakan",
                                            "onClick" => "$(\"#idDaftarTindakan\").val(\"$data->daftartindakan_id\");
                                                          $(\"#idTarifTindakan\").val(\"$data->tariftindakan_id\");
                                                          $(\"#'.CHtml::activeId($model, 'daftartindakan_id') . '\").val(\"".$data->daftartindakan->daftartindakan_nama." - ".$data->kelaspelayanan->kelaspelayanan_nama." - ".$data->harga_tariftindakan."\");
                                                          $(\"#dialogDaftarTindakan\").dialog(\"close\");    
                                                "))',
        ),
        array( 
                        'name'=>'tariftindakan_id', 
                        'value'=>'$data->tariftindakan_id', 
                        'filter'=>false, 
                ),
        array( 
                        'name'=>'daftartindakan_id', 
                        'value'=>'$data->daftartindakan->daftartindakan_nama',
                ),
        array( 
                        'name'=>'kelaspelayanan_id', 
                        'value'=>'$data->kelaspelayanan->kelaspelayanan_nama',
                        'filter'=>CHtml::listData($modTarifTindakan->KelasPelayanan, 'kelaspelayanan_id', 'kelaspelayanan_nama'),
                ),
        //'daftartindakan.daftartindakan_nama',        
        //'kelaspelayanan.kelaspelayanan_nama',
        array( 
                        'name'=>'harga_tariftindakan', 
                        'value'=>'number_format($data->harga_tariftindakan,0,".",",")', 
                        'filter'=>false, 
                ),
        //'tglkadaluarsa',
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
//========= end obatAlkes dialog =============================
?>

<?php
//========= Dialog buat cari data obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogObatAlkes',
    'options' => array(
        'title' => 'Pencarian Obat Alkes',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 600,
        'resizable' => false,
    ),
));

$modObatAlkes = new ObatalkesM('search');

$modObatAlkes->unsetAttributes();
if (isset($_GET['idKelasPelayanan'])){
    $modObatAlkes->kelaspelayanan_id = $_GET['idKelasPelayanan'];
}
if (isset($_GET['ObatalkesM'])) {
    $modObatAlkes->attributes = $_GET['ObatalkesM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'obatAlkes-m-grid',
    //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider' => $modObatAlkes->search(),
    'filter' => $modObatAlkes,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "$(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                          $(\"#qtyPakai\").val(0);
                                                          $(\"#hargaPakai\").val(0);
                                                          $(\"#'.CHtml::activeId($model, 'obatalkes_id') . '\").val(\"".$data->obatalkes_nama." - ".$data->sumberdana->sumberdana_nama."\");
                                                          $(\"#dialogObatAlkes\").dialog(\"close\");    
                                                "))',
        ),
        'obatalkes_kategori',
        'obatalkes_golongan',
        'obatalkes_kode',
        'obatalkes_nama',
        array(
            'name' => 'sumberdanaNama',
            'type' => 'raw',
            'value' => '$data->sumberdana->sumberdana_nama',
        ),
        array(
            'name' => 'hargajual',
            'type' => 'raw',
            'value'=>'number_format($data->hargajual,0,".",",")', 
            'filter'=>false,
        ),
        array(
            'name' => 'harganetto',
            'type' => 'raw',
            'value'=>'number_format($data->harganetto,0,".",",")', 
            'filter'=>false,
        ),
        // 'hargajual',
        // 'harganetto',
//        'obatalkes_kadarobat',
//        'kemasanbesar',
//        'kekuatan',
        //'tglkadaluarsa',
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
//========= end obatAlkes dialog =============================
?>