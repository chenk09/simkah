
<fieldset>
    <legend class="rim">Data Pasien</legend>
    <table class="table table-condensed">
        <tr>
            <tr>
                <td><?php echo CHtml::label('No Pendaftaran', 'no_pendaftaran',array('class'=>'control-label')); ?></td>
                <td>
                    <?php 
                        $this->widget('MyJuiAutoComplete', array(
                                        'name'=>'no_pendaftaran',
                                        'source'=>'js: function(request, response) {
                                                       $.ajax({
                                                           url: "'.Yii::app()->createUrl('sistemAdministrator/ActionAutoComplete/daftarPasien').'",
                                                           dataType: "json",
                                                           data: {
                                                               noPendaftaran: request.term,
                                                           },
                                                           success: function (data) {
                                                                   response(data);
                                                           }
                                                       })
                                                    }',
                                         'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'focus'=> 'js:function( event, ui ) {
                                                    $(this).val("");
                                                    return false;
                                                }',
                                               'select'=>'js:function( event, ui ) {
                                                    $(this).val(ui.item.value);
                                                    isiDataPasien(ui.item);
                                                    return false;
                                                }',
                                        ),
                                    )); 
                    ?>
                </td>

                <td><?php echo CHtml::label('No Rekam Medik', 'no_rekam_medik',array('class'=>'control-label')); ?></td>
                <td><?php echo CHtml::textField('noRm', '', array('readonly'=>true)); ?></td>
            <td rowspan="4">
                <?php 
                   
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::label('Tgl Pendaftaran', 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('tglPendaftaran', '', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::label('Nama Pasien', 'nama_pasien',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('namaPasien', '', array('readonly'=>true)); ?></td>
        </tr>
            <td><?php echo CHtml::label('Jenis Kasus Penyakit', 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('jenisKasusPenyakit', '', array('readonly'=>true)); ?></td>
        
            
            <td><?php echo CHtml::label('Alias', 'bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('namaBin', '', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::label('Kelas Pelayanan', 'kelaspelayanan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('kelaspelayanan', '', array('readonly'=>true)); ?></td>
            
            
            <td><?php echo CHtml::label('Umur', 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('umur', '', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::label('Cara Bayar', 'carabayar_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('caraBayar', '', array('readonly'=>true)); ?></td>
            
            
            <td><?php echo CHtml::label('Jenis Kelamin', 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('jeniskelamin', '', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::label('Penjamin', 'penjamin_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('penjaminNama', '', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::label('Ruangan Tindakan', 'ruanganTindakanId',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::dropDownList('ruanganTindakanId', Null,CHtml::listData(SARuanganM::model()->RuanganTindakan, 'ruangan_id', 'ruangan_nama'), array('readonly'=>true, 'empty'=>'-- Pilih --')); ?></td>
        </tr>
    </table>
</fieldset>
<div id="inputId">
    <?php echo CHtml::hiddenField('idPasienKirimKeunitLain', '',array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'kelaspelayanan_id', array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'pendaftaran_id', array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'pasien_id', array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'carabayar_id', array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'penjamin_id', array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'jeniskasuspenyakit_id', array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'instalasi_id', array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'statuspasien', array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'statusperiksa', array('readonly'=>true)); ?>
    <?php echo CHtml::activeHiddenField($modPendaftaran,'statusmasuk', array('readonly'=>true)); ?>
</div>
<hr/>

<script type="text/javascript">
function isiDataPasien(data)
{
    if(cekStatusPeriksa(data.statusperiksa) == false){
        resetForm();
        return false;
    }
    $('#idPasienKirimKeunitLain').val(data.pasienkirimkeunitlain_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'kelaspelayanan_id');?>').val(data.kelaspelayanan_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'pendaftaran_id');?>').val(data.pendaftaran_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'pasien_id');?>').val(data.pasien_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'carabayar_id');?>').val(data.carabayar_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'penjamin_id');?>').val(data.penjamin_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'jeniskasuspenyakit_id');?>').val(data.jeniskasuspenyakit_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'instalasi_id');?>').val(data.instalasi_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'statuspasien');?>').val(data.statuspasien);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'statusperiksa');?>').val(data.statusperiksa);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'statusmasuk');?>').val(data.statusmasuk);
    $('#noRm').val(data.no_rekam_medik);
    $('#namaPasien').val(data.nama_pasien);
    $('#namaBin').val(data.nama_bin);
    $('#jeniskelamin').val(data.jeniskelamin);
    $('#tglPendaftaran').val(data.tgl_pendaftaran);
    $('#umur').val(data.umur);
    $('#noPendaftaran').val(data.no_pendaftaran);
    $('#jenisKasusPenyakit').val(data.jeniskasuspenyakit_nama);
    $('#caraBayar').val(data.carabayar_nama);
    $('#kelaspelayanan').val(data.kelaspelayanan_nama);
    $('#penjaminNama').val(data.penjamin_nama);
    $('#pegawaiNama').val(data.nama_pegawai);
}

function cekStatusPeriksa(statusPeriksa){
    if(statusPeriksa != "SUDAH PULANG"){
        if(confirm("Status Pasien '"+statusPeriksa+"'. Tindakan masih bisa di-input di ruangan. Tetap lanjutkan ?")){
            return true;
        }else{
            return false;
        }
    }
}
function resetForm(){
    $('#tindakan-pelayanan-t-form')[0].reset();
    $('#inputId').each(function(){
        $(this).find('input').val("");
    });
}
</script>
