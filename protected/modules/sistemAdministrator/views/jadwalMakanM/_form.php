
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sajadwalmakan-m-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
                        <?php echo CHtml::label('Jenis Diet','jjenisdiet',array('class'=>"control-label")) ?>
                        <div class="controls">
                                        <?php echo CHtml::hiddenField('jenisdietid', '', array('readonly'=>true)) ?>

                                        <?php $this->widget('MyJuiAutoComplete', array(
                                                               'name'=>'jenisdiet', 
                                                                'source'=>'js: function(request, response) {
                                                                       $.ajax({
                                                                           url: "'.Yii::app()->createUrl('ActionAutoComplete/Jenisdiet').'",
                                                                           dataType: "json",
                                                                           data: {
                                                                               term: request.term,
                                                                           },
                                                                           success: function (data) {
                                                                                   response(data);
                                                                           }
                                                                       })
                                                                    }',
                                                                'options'=>array(
                                                                           'showAnim'=>'fold',
                                                                           'minLength' => 2,
                                                                           'focus'=> 'js:function( event, ui )
                                                                               {
                                                                                $(this).val(ui.item.label);
                                                                                return false;
                                                                                }',
                                                                           'select'=>'js:function( event, ui ) {
                                                                               $(\'#jenisdietid\').val(ui.item.jenisdiet_id);
                                                                               $(\'#jenisdiet_nama\').val(ui.item.jenisdiet_nama);
                                                                                return false;
                                                                            }',
                                                                ),
                                                                'htmlOptions'=>array(
                                                                    'readonly'=>false,
                                                                    'placeholder'=>'Jenis Diet',
                                                                    'size'=>13,
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                                ),
                                                                'tombolDialog'=>array('idDialog'=>'dialogJenisdiet'),
                                                        )); ?>
                        </div>
        
                        <?php echo CHtml::label('Tipe Diet','ttipediet',array('class'=>"control-label")) ?>
                        <div class="controls">
                        <?php echo CHtml::hiddenField('tipedietid', '', array('readonly'=>true)) ?>
                            <?php 
                                /* $tipediet = TipeDietM::model()->findAll();
                                foreach($tipediet as $valuetipediet):
                                    $returnTipeDiet[] = array(    
                                      'label'=>$valuetipediet->tipediet_nama,
                                      'value'=>$valuetipediet->tipediet_id,
                                      'id'=>$valuetipediet->tipediet_id,);
                                endforeach; */
                             ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'tipediet', 
                                                    'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/TipeDiet').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                                    'options'=>array(
                                                               'showAnim'=>'fold',
                                                               'minLength' => 2,
                                                               'focus'=> 'js:function( event, ui )
                                                                   {
                                                                    $(this).val(ui.item.label);
                                                                    return false;
                                                                    }',
                                                               'select'=>'js:function( event, ui ) {
                                                                   $("#tipedietid").val(ui.item.tipediet_id);
                                                                   $("#tipediet_nama").val(ui.item.tipediet_nama);
                                                                    return false;
                                                                }',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'readonly'=>false,
                                                        'placeholder'=>'Tipe Diet',
                                                        'size'=>13,
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                    ),
                                                    'tombolDialog'=>array('idDialog'=>'dialogTipeDiet'),
                                            )); ?>
                        </div>
        
                        <?php echo CHtml::label('Menu Diet','mmenudiet',array('class'=>"control-label")) ?>
                        <div class="controls">
                            
                                        <?php echo CHtml::hiddenField('menudietid', '', array('readonly'=>true)) ?>
                                        <?php 
                                            /* $menudiet = MenuDietM::model()->findAll();
                                            foreach($menudiet as $valuemenudiet):
                                                $returnMenuDiet[] = array(    
                                                  'label'=>$valuemenudiet->menudiet_nama,
                                                  'value'=>$valuemenudiet->menudiet_nama,
                                                  'id'=>$valuemenudiet->menudiet_id,);
                                            endforeach; */
                                         ?>
                                        <?php $this->widget('MyJuiAutoComplete', array(
                                                               'name'=>'menudiet', 
                                                                'source'=>'js: function(request, response) {
                                                                       $.ajax({
                                                                           url: "'.Yii::app()->createUrl('ActionAutoComplete/MenuDiet').'",
                                                                           dataType: "json",
                                                                           data: {
                                                                               term: request.term,
                                                                           },
                                                                           success: function (data) {
                                                                                   response(data);
                                                                           }
                                                                       })
                                                                    }',
                                                                'options'=>array(
                                                                           'showAnim'=>'fold',
                                                                           'minLength' => 2,
                                                                           'focus'=> 'js:function( event, ui )
                                                                               {
                                                                                $(this).val(ui.item.label);
                                                                                return false;
                                                                                }',
                                                                           'select'=>'js:function( event, ui ) {
                                                                               $("#menudietid").val(ui.item.menudiet_id);
                                                                                return false;
                                                                            }',
                                                                ),
                                                                'htmlOptions'=>array(
                                                                    'readonly'=>false,
                                                                    'placeholder'=>'Menu Diet',
                                                                    'size'=>13,
                                                                ),
                                                                'tombolDialog'=>array('idDialog'=>'dialogMenuDiet'),
                                                        )); ?>
                        </div>
                        <div class="controls">
                            <table>
                                <tr>
                                    <td style="width:250px;">
                                        <?php
                                        $waktu = JenisWaktuM::model()->findAll("jeniswaktu_aktif=TRUE ORDER BY jeniswaktu_id");
                                        $returnVal = array();
                                        $returnVal = "<table style='width:250px;'><tr>";
                                        foreach($waktu as $data)
                                        {
                                            $tr .= "<td style='text-align:left;'>";
                                            $tr .= CHtml::checkbox('jeniswaktuid[]',false,array('value'=>$data->getAttribute('jeniswaktu_id'), 'class'=>'jeniswaktu'));
                                            $tr .= ' ' . $data->getAttribute('jeniswaktu_nama');
                                            $tr .= '</td>';
                                        }
                                        $returnVal .= $tr;
                                        $returnVal .= '</table>';
                                        echo $returnVal;
                                        ?>
                                    </td>
                                    <td>
                                         <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                                        array(
                                                                'onclick'=>'submitJadwalMakan();
                                                                                  return false;',
                                                                'class'=>'btn btn-primary',
                                                                'onkeypress'=>"\$(\"#menudiet_id\").val();
                                                                                          \$(\"#menudiet\").val();
                                                                                          \$(\"#dialogMenuDiet\").dialog(\"close\");
                                                                                            submitJadwalMakan();",
                                                                'rel'=>"tooltip",
                                                                'id'=>'tambahJadwal',  
                                                                'title'=>"Klik Untuk Menambahkan Jadwal",
                                                                )
                                                        );
                                         ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
        <?php echo CHtml::css('table .span2{float:left}'); ?>
                <table id="tableJadwalMakan" class="table table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th><?php echo CHtml::checkBox('checkListUtama',true,array('onclick'=>'checkAll(\'cekList\',this);'));?></th>
                        <th>Jenis Diet</th>
                        <th>Tipe Diet</th>
                        <?php foreach ($waktu as $row) { ?>
                        <th><?php echo $row->jeniswaktu_nama; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                </table>
        
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                     array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan','onKeypress'=>'return formSubmit(this,event)','onClick'=>'return formSubmit(this,event)')); ?>
                                               <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                                    Yii::app()->createUrl($this->module->id.'/'.jadwalMakanM.'/admin'), 
                                                                    array('class'=>'btn btn-danger',
                                                                    'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
$content = $this->renderPartial('../tips/tipsaddedit2b',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
<?php

   $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogJenisdiet',
    'options'=>array(
        'title'=>'Pencarian Jenis Diet',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
        ),
    ));
   
$modJenisdiet = new SAJenisdietM('search');
$modJenisdiet->unsetAttributes();
if(isset($_GET['SAJenisdietM'])) {
    $modJenisdiet->attributes = $_GET['SAJenisdietM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'jenisdiet-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modJenisdiet->search(),
	'filter'=>$modJenisdiet,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selectJenisdiet",
                                                    "onClick" => "\$(\"#jenisdietid\").val($data->jenisdiet_id);
                                                                          \$(\"#jenisdiet\").val(\"$data->jenisdiet_nama\");
                                                                          \$(\"#dialogJenisdiet\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'jenisdiet_nama',
                'jenisdiet_namalainnya',
                'jenisdiet_keterangan',
                array(
                    'header'=>'Catatan',
                    'type'=>'raw',
                    'value'=>'$data->jenisdiet_catatan',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
/* -------------------------------------------------------------------------- endWidget Jenisdiet ---------------------------------------------- */

   $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogTipeDiet',
    'options'=>array(
        'title'=>'Pencarian Tipe Diet',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>400,
        'resizable'=>false,
    ),
));
   
$modTipeDiet = new SATipeDietM('search');
$modTipeDiet->unsetAttributes();
if(isset($_GET['SATipeDietM'])) {
    $modTipeDiet->attributes = $_GET['SATipeDietM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'tipediet-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modTipeDiet->search(),
	'filter'=>$modTipeDiet,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selectTipeDiet",
                                                    "onClick" => "\$(\"#tipedietid\").val($data->tipediet_id);
                                                                          \$(\"#tipediet\").val(\"$data->tipediet_nama\");
                                                                          \$(\"#dialogTipeDiet\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'tipediet_nama',
                'tipediet_namalainnya',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
/* -------------------------------------------------------------------------- endWidget TipeDiet ---------------------------------------------- */

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogMenuDiet',
    'options'=>array(
        'title'=>'Pencarian Menu Diet',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>400,
        'resizable'=>false,
    ),
));
   
$modMenuDiet = new SAMenuDietM('search');
$modMenuDiet->unsetAttributes();
if(isset($_GET['SAMenuDietM'])) {
    $modMenuDiet->attributes = $_GET['SAMenuDietM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'menudiet-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modMenuDiet->search(),
	'filter'=>$modMenuDiet,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selectTipeDiet",
                                                    "onClick" => "\$(\"#menudietid\").val($data->menudiet_id);
                                                                          \$(\"#menudiet\").val(\"$data->menudiet_nama\");
                                                                          \$(\"#dialogMenuDiet\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'menudiet_nama',
                'menudiet_namalain',
                'jml_porsi',
                'ukuranrumahtangga',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogMenuDietForTest',
    'options'=>array(
        'title'=>'Pencarian Menu Diet',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>400,
        'resizable'=>false,
    ),
));
   
$modMenuDiet = new SAMenuDietM('search');
$modMenuDiet->unsetAttributes();
if(isset($_GET['SAMenuDietM'])) {
    $modMenuDiet->attributes = $_GET['SAMenuDietM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'menudiet-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modMenuDiet->search(),
	'filter'=>$modMenuDiet,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selectTipeMenuDiet",
                                                    "onClick" => "var parent = $(\"#dialogMenuDietForTest\").attr(\"parentclick\");
                                                            $(\"#\"+parent+\"\").val($data->menudiet_id);
                                                                          $(\"#\"+parent+\"\").parents(\"td\").find(\".adamenudiet\").val(\"$data->menudiet_nama\");
                                                                          $(\"#dialogMenuDietForTest\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'menudiet_nama',
                'menudiet_namalain',
                'jml_porsi',
                'ukuranrumahtangga',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
/* -------------------------------------------------------------------------- endWidget MenuDiet ---------------------------------------------- */
?>
        
<?php
$urlGetJadwalMakan = Yii::app()->createUrl('actionAjax/GetJadwalMakan');
?>
        
<?php
$jscript = <<< JS
function submitJadwalMakan()
{
    var jeniswaktu = new Array();
    jenisdietid = $('#jenisdietid').val();
    tipedietid = $('#tipedietid').val();
    jeniswaktuid = $('#jeniswaktuid').val();
    menudietid = $('#menudietid').val();
    i = 0;
    $('.jeniswaktu').each(function(){   
        if ($(this).is(':checked')){
            jeniswaktu[i] = $(this).val();
            i++;
        }
    });
    if(jenisdietid==''){
        alert('Silahkan Pilih Jenis Diet Terlebih Dahulu');
    }else{
        $.post("${urlGetJadwalMakan}", {jeniswaktu:jeniswaktu, jenisdietid: jenisdietid, tipedietid:tipedietid, jeniswaktuid:jeniswaktuid, menudietid:menudietid,},
        function(data){
            $('#tableJadwalMakan').append(data.return);
            renameInput();
            autoMenuDiet();
        }, "json");
    }   
}

function renameInput(){
    nourut = 0;
    $('.cekList').each(function(){
        $(this).parents('tr').find('[name*="JadwalMakanM"]').each(function(){
            var input = $(this).attr('name');
            var data = input.split('JadwalMakanM[]');
            if (typeof data[1] === 'undefined'){} else{
                $(this).attr('name','JadwalMakanM['+nourut+']'+data[1]);
            }
        });
        nourut++;
    });
}

function autoMenuDiet(){
    jQuery('.adamenudiet').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui )
    {
    $(this).val(ui.item.label);
    return false;
    },'select':function( event, ui ) {
    $(this).parents('td').find('.menudiet').val(ui.item.menudiet_id);
    return false;
    },'source': function(request, response) {
    $.ajax({
    url: "/simrs/index.php?r=ActionAutoComplete/MenuDiet",
    dataType: "json",
    data: {
    term: request.term,
    },
    success: function (data) {
    response(data);
    }
    })
    }}); 
}

function openDialog(obj){
    var idObj = $(obj).parents('td').find('.menudiet').attr('id');
    $('#dialogMenuDietForTest').attr('parentClick',idObj);
    $('#dialogMenuDietForTest').dialog('open');   
}
JS;

Yii::app()->clientScript->registerScript('jadwalMakan',$jscript, CClientScript::POS_HEAD);
?>
