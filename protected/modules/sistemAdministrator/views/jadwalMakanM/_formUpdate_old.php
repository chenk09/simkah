
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sajadwalmakan-m-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
                <?php echo $form->dropDownListRow($model,'jenisdiet_id',
                CHtml::listData($model->JenisdietItems, 'jenisdiet_id', 'jenisdiet_nama'),
                array('readonly'=>true),
                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>
        
                <?php echo $form->dropDownListRow($model,'tipediet_id',
                CHtml::listData($model->TipeDietItems, 'tipediet_id', 'tipediet_nama'),
                array('readonly'=>true),
                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>

                <?php echo $form->dropDownListRow($model,'jeniswaktu_id',
                CHtml::listData($model->JenisWaktuItems, 'jeniswaktu_id', 'jeniswaktu_nama'),
                array('readonly'=>true),
                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>
                <div class="control-label">Menu Diet</div>
                        <?php echo CHtml::hiddenField('menudiet_id', $model->menudiet_id, array('readonly'=>true)) ?>
                        <div class="controls">
                            <?php 
                                /* $menudiet = MenuDietM::model()->findAll();
                                foreach($menudiet as $valuemenudiet):
                                    $returnMenuDiet[] = array(    
                                      'label'=>$valuemenudiet->menudiet_nama,
                                      'value'=>$valuemenudiet->menudiet_nama,
                                      'id'=>$valuemenudiet->menudiet_id,);
                                endforeach; */
                             ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'menudiet', 
                                                    'value'=>$model->menudiet->menudiet_nama,
                                                    'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/MenuDiet').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                                    'options'=>array(
                                                               'showAnim'=>'fold',
                                                               'minLength' => 2,
                                                               'focus'=> 'js:function( event, ui )
                                                                   {
                                                                    $(this).val(ui.item.label);
                                                                    return false;
                                                                    }',
                                                               'select'=>'js:function( event, ui ) {
                                                                   $("#menudiet_id").val(ui.item.menudiet_id);
                                                                    return false;
                                                                }',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'readonly'=>false,
                                                        'size'=>13,
                                                    ),
                                            )); ?>
                        </div>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                     array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan','onKeypress'=>'return formSubmit(this,event)','onClick'=>'return formSubmit(this,event)')); ?>
                                               <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                                    Yii::app()->createUrl($this->module->id.'/'.jadwalMakanM.'/admin'), 
                                                                    array('class'=>'btn btn-danger',
                                                                    'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
		<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
<?php
$jscript = <<< JSCRIPT

JSCRIPT;
?>