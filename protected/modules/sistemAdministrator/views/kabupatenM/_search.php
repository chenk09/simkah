<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'id'=>'loginpemakai-m_search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'kabupaten_id',array('class'=>'span5')); ?>

	<?php //echo $form->dropDownListRow($model,'propinsi_nama',array('class'=>'span3')); ?>
	<?php echo $form->dropDownListRow($model,'propinsi_id',  CHtml::listData($model->PropinsiItems, 'propinsi_id', 'propinsi_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --')); ?>

	<?php echo $form->textFieldRow($model,'kabupaten_nama',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->checkBoxRow($model,'kabupaten_aktif',array('checked'=>'$data->kabupaten_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
