<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sakarcis-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'karcis_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'daftartindakan_nama',array('class'=>'span5')); ?>
	<?php //echo $form->textFieldRow($model,'ruangan_nama',array('class'=>'span5')); ?>
        <?php echo $form->dropDownListRow($model,'daftartindakan_id',
                       CHtml::listData($model->DaftarTindakanItems, 'daftartindakan_id', 'daftartindakan_nama'),
                       array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                       'empty'=>'-- Pilih --')); ?>
        <?php echo $form->dropDownListRow($model,'ruangan_id',
                       CHtml::listData($model->RuanganItems, 'ruangan_id', 'ruangan_nama'),
                       array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                       'empty'=>'-- Pilih --')); ?> 

	<?php echo $form->textFieldRow($model,'karcis_nama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'karcis_namalainnya',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->checkBoxRow($model,'karcis_aktif', array('checked'=>'$data->karcis_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
