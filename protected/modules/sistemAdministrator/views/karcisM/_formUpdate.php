<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
  'id'=>'sakarcis-m-form',
  'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

  <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

  <?php echo $form->errorSummary($model); ?>

             <?php echo $form->dropDownListRow($model,'daftartindakan_id',
                       CHtml::listData($model->DaftarTindakanItems, 'daftartindakan_id', 'daftartindakan_nama'),
                       array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                       'empty'=>'-- Pilih --')); ?>
             <?php echo $form->dropDownListRow($model,'ruangan_id',
                       CHtml::listData($model->RuanganItems, 'ruangan_id', 'ruangan_nama'),
                       array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                       'empty'=>'-- Pilih --')); ?> 
            <?php echo $form->textFieldRow($model,'karcis_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'karcis_namalainnya',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
              <?php
                  echo $form->radioButtonListInlineRow($model,'statuspasien', StatusPasien::items());
              ?>
            <?php echo $form->checkBoxRow($model,'karcis_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
  <div class="form-actions">
                    <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.karcisM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    <?php
$content = $this->renderPartial('../tips/tips',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
