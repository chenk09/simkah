<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('konfigsystem_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->konfigsystem_id),array('view','id'=>$data->konfigsystem_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isantrian')); ?>:</b>
	<?php echo CHtml::encode((empty($data->isantrian)?"Tidak":"Ya")); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iskarcis')); ?>:</b>
	<?php echo CHtml::encode($data->iskarcis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('printkartulsng')); ?>:</b>
	<?php echo CHtml::encode($data->printkartulsng); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('printkunjunganlsng')); ?>:</b>
	<?php echo CHtml::encode($data->printkunjunganlsng); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mr_lab')); ?>:</b>
	<?php echo CHtml::encode($data->mr_lab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mr_rad')); ?>:</b>
	<?php echo CHtml::encode($data->mr_rad); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mr_ibs')); ?>:</b>
	<?php echo CHtml::encode($data->mr_ibs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mr_rehabmedis')); ?>:</b>
	<?php echo CHtml::encode($data->mr_rehabmedis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mr_apotik')); ?>:</b>
	<?php echo CHtml::encode($data->mr_apotik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nopendaftaran_rj')); ?>:</b>
	<?php echo CHtml::encode($data->nopendaftaran_rj); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nopendaftaran_ri')); ?>:</b>
	<?php echo CHtml::encode($data->nopendaftaran_ri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nopendaftaran_gd')); ?>:</b>
	<?php echo CHtml::encode($data->nopendaftaran_gd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nopendaftaran_lab')); ?>:</b>
	<?php echo CHtml::encode($data->nopendaftaran_lab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nopendaftaran_rad')); ?>:</b>
	<?php echo CHtml::encode($data->nopendaftaran_rad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nopendaftaran_ibs')); ?>:</b>
	<?php echo CHtml::encode($data->nopendaftaran_ibs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nopendaftaran_rehabmedis')); ?>:</b>
	<?php echo CHtml::encode($data->nopendaftaran_rehabmedis); ?>
	<br />

	*/ ?>

</div>