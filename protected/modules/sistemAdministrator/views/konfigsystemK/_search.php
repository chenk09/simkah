<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sakonfigsystem-k-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'konfigsystem_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'mr_lab',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'mr_rad',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'mr_ibs',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'mr_rehabmedis',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'mr_apotik',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'nopendaftaran_rj',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'nopendaftaran_ri',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'nopendaftaran_gd',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'nopendaftaran_lab',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'nopendaftaran_rad',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'nopendaftaran_ibs',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'nopendaftaran_rehabmedis',array('class'=>'span1','maxlength'=>4)); ?>

        <?php echo $form->checkBoxRow($model,'isantrian',array('checked'=>'isantrian')); ?>

	<?php echo $form->checkBoxRow($model,'iskarcis',array('checked'=>'iskarcis')); ?>

	<?php echo $form->checkBoxRow($model,'printkartulsng',array('checked'=>'printkartulsng')); ?>

	<?php echo $form->checkBoxRow($model,'printkunjunganlsng',array('checked'=>'printkunjunganlsng')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
