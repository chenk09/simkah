<?php
$this->breadcrumbs=array(
	'Sakonfigsystem Ks'=>array('index'),
	$model->konfigsystem_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Konfigurasi System ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' SAKonfigsystemK', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' SAKonfigsystemK', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' SAKonfigsystemK', 'icon'=>'pencil','url'=>array('update','id'=>$model->konfigsystem_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' SAKonfigsystemK','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->konfigsystem_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Konfigurasi System', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'konfigsystem_id',
		array(
                    'name'=>'isantrian',
                    'value'=>(empty($model->isantrian)? "Tidak" : "Ya"),
                ),
		array(
                    'name'=>'iskarcis',
                    'value'=>(empty($model->iskarcis)? "Tidak" : "Ya"),
                ),
		array(
                    'name'=>'printkartulsng',
                    'value'=>(empty($model->printkartulsng)? "Tidak" : "Ya"),
                ),
		array(
                    'name'=>'printkunjunganlsng',
                    'value'=>(empty($model->printkunjunganlsng)? "Tidak" : "Ya"),
                ),
//		'iskarcis',
//		'printkartulsng',
//		'printkunjunganlsng',
		'mr_lab',
		'mr_rad',
		'mr_ibs',
		'mr_rehabmedis',
		'mr_apotik',
		'nopendaftaran_rj',
		'nopendaftaran_ri',
		'nopendaftaran_gd',
		'nopendaftaran_lab',
		'nopendaftaran_rad',
		'nopendaftaran_ibs',
		'nopendaftaran_rehabmedis',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>