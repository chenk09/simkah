<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sakonfigsystem-k-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

            
            <?php echo $form->textFieldRow($model,'mr_lab',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
            <?php echo $form->textFieldRow($model,'mr_rad',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
            <?php echo $form->textFieldRow($model,'mr_ibs',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
            <?php echo $form->textFieldRow($model,'mr_rehabmedis',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
            <?php echo $form->textFieldRow($model,'mr_apotik',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
        <div class="control-group ">
				 <?php echo $form->labelEx($model, 'no pendaftaran_rj', array('class' => 'control-label')) ?>
                    <div class="controls"> 
   <?php echo $form->textField($model,'nopendaftaran_rj',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
                    </div></div>
          <div class="control-group ">
				 <?php echo $form->labelEx($model, 'no pendaftaran_ri', array('class' => 'control-label')) ?>
                    <div class="controls">
 <?php echo $form->textField($model,'nopendaftaran_ri',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
         </div></div>
                        <div class="control-group ">
				 <?php echo $form->labelEx($model, 'no pendaftaran_gd', array('class' => 'control-label')) ?>
                    <div class="controls">    
  <?php echo $form->textField($model,'nopendaftaran_gd',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
             </div></div>
        <div class="control-group ">
				 <?php echo $form->labelEx($model, 'no pendaftaran_lab', array('class' => 'control-label')) ?>
                    <div class="controls">
  <?php echo $form->textField($model,'nopendaftaran_lab',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
         </div></div>
        <div class="control-group ">
				 <?php echo $form->labelEx($model, 'no pendaftaran_rad', array('class' => 'control-label')) ?>
                    <div class="controls">
  <?php echo $form->textField($model,'nopendaftaran_rad',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
         </div></div>
        <div class="control-group ">
				 <?php echo $form->labelEx($model, 'no pendaftaran_ibs', array('class' => 'control-label')) ?>
                    <div class="controls">
  <?php echo $form->textField($model,'nopendaftaran_ibs',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
         </div></div>
        <div class="control-group ">
				 <?php echo $form->labelEx($model, 'no pendaftaran_rehabmedis', array('class' => 'control-label')) ?>
                    <div class="controls">
  <?php echo $form->textField($model,'nopendaftaran_rehabmedis',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
        </div></div>
        <div class="control-group ">
            <?php echo CHtml::label('Host NodeJS', 'Host NodeJS',array('class' => 'control-label')) ?>
            <div class="controls">
                <?php echo $form->textField($model,'nodejs_host',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            </div>
        </div>
        <div class="control-group ">
            <?php echo CHtml::label('Post NodeJS', 'Post NodeJS',array('class' => 'control-label')) ?>
            <div class="controls">
                <?php echo $form->textField($model,'nodejs_port',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10)); ?>
            </div>
        </div>
                        
          <?php echo $form->checkBoxRow($model,'isantrian', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->checkBoxRow($model,'iskarcis', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->checkBoxRow($model,'printkartulsng', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->checkBoxRow($model,'printkunjunganlsng', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.konfigsystemK.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>

<?php Yii::app()->clientScript->registerScript('angka', "
$(document).ready(function () {
        $('.numbersOnly').keypress(function(event) {
                var charCode = (event.which) ? event.which : event.keyCode
                if ((charCode >= 48 && charCode <= 57)
                        || charCode == 46
                        || charCode == 44)
                        return true;
                return false;
        });
});
", CClientScript::POS_HEAD); ?>
