
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sajenisdiet-m-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#SAJenisdietM_jenisdiet_nama',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
		<?php echo $form->textFieldRow($model,'jenisdiet_nama',array('class'=>'span3','size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->textFieldRow($model,'jenisdiet_namalainnya',array('class'=>'span3','size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->textAreaRow($model,'jenisdiet_keterangan',array('class'=>'span3','rows'=>6, 'cols'=>50)); ?>
		<?php /* echo $form->textAreaRow($model,'jenisdiet_catatan',array('class'=>'ext.redactorjs.Redactor','id'=>'redactor_content1','rows'=>6, 'cols'=>50)); */ ?>
                                <div>
                                <table>
                                 <tr>
                                 <td width="140px" style="text-align:right;">
                                    <span class="control-label">Catatan</span>
                                 </td>
                                 <td>
                                     <div style="width:215px;">
                                     <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$model,'attribute'=>'jenisdiet_catatan','name'=>'jenisdiet_catatan','toolbar'=>'mini','height'=>'100px')) ?>
                                     </div>
                                 </td>
                                 </tr>
                                </table>
                                </div>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                     array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan','onKeypress'=>'return formSubmit(this,event)','onClick'=>'return formSubmit(this,event)')); ?>
                                               <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                                    Yii::app()->createUrl($this->module->id.'/'.JenisdietM.'/admin'), 
                                                                    array('class'=>'btn btn-danger',
                                                                    'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
