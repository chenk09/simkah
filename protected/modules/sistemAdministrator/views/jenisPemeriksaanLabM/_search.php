<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sajenis-pemeriksaan-lab-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'jenispemeriksaanlab_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenispemeriksaanlab_kode',array('class'=>'span5','maxlength'=>10)); ?>
	<?php echo $form->textFieldRow($model,'jenispemeriksaanlab_urutan',array('class'=>'span5')); ?>
	<?php echo $form->textFieldRow($model,'jenispemeriksaanlab_nama',array('class'=>'span5','maxlength'=>30)); ?>
	<?php echo $form->textFieldRow($model,'jenispemeriksaanlab_namalainnya',array('class'=>'span5','maxlength'=>30)); ?>
	<?php echo $form->textFieldRow($model,'jenispemeriksaanlab_kelompok',array('class'=>'span5','maxlength'=>100)); ?>
	<?php echo $form->checkBoxRow($model,'jenispemeriksaanlab_aktif'); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
