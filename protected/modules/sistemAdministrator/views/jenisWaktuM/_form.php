    
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sajeniswaktu-m-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#SAJenisWaktuM_jeniswaktu_nama',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
		<?php echo $form->textFieldRow($model,'jeniswaktu_nama',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->textFieldRow($model,'jeniswaktu_namalain',array('size'=>50,'maxlength'=>50)); ?>
<!--                               <div class="control-label">Jam *</div>
                                <div class="controls">
                                    <?php //echo CHtml::textField('jam','',array('size'=>20,'maxlength'=>2,'style'=>'width:20px;')); ?> /
                                    <?php //echo CHtml::textField('menit','',array('size'=>20,'maxlength'=>2,'style'=>'width:20px;')); ?>
                                </div>-->
		<?php echo $form->labelEx($model,'jeniswaktu_jam', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php   
                            $this->widget('MyDateTimePicker',array(
                                            'model'=>$model,
                                            'attribute'=>'jeniswaktu_jam',
    //                                        'mode'=>'date',
                                            'mode'=>'time',
                                            'options'=> array(
                                                'timeFormat'=>'',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                    )); ?>
                    <?php echo $form->error($model, 'jeniswaktu_jam'); ?>
                </div>
                <?php echo $form->checkBoxRow($model,'jeniswaktu_aktif'); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                     array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan','onKeypress'=>'return formSubmit(this,event)','onClick'=>'return formSubmit(this,event)')); ?>
                                               <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                                    Yii::app()->createUrl($this->module->id.'/'.jenisWaktuM.'/admin'), 
                                                                    array('class'=>'btn btn-danger',
                                                                    'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
