

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'id'=>'sajeniswaktu-m-search',
                 'type'=>'horizontal',
)); ?>

		<?php echo $form->textFieldRow($model,'jeniswaktu_nama',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->textFieldRow($model,'jeniswaktu_namalain',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->textFieldRow($model,'jeniswaktu_jam',array('size'=>20,'maxlength'=>20)); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>

