 
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'satarif-tindakan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#kategoritindakan',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
        <table class="table">
            <tr>
                <td>                    
                                      
                    <div class="control-group">
                        <?php echo CHtml::label('Nama Tindakan','kattindakan',array('class'=>"control-label")) ?>
                        <div class="controls">
                            <?php echo CHtml::hiddenField('daftartindakan_id',$var['daftartindakan_id'],array('readonly'=>true)) ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'daftartindakan', 
                                                   'value'=>$var['daftartindakan'],
                                                   'source'=>'js: function(request, response) {
                                                                   $.ajax({
                                                                       url: "'.Yii::app()->createUrl('sistemAdministrator/tarifTindakanM/tindakan').'", //tdk menggunakan /ObatAlkes
                                                                       dataType: "json",
                                                                       data: {
                                                                           term: request.term,
                                                                           idKategori: $("#kategoritindakan_id").val(),
                                                                       },
                                                                       success: function (data) {
                                                                               response(data);
                                                                       }
                                                                   })
                                                                }',
                                                     'options'=>array(
                                                           'showAnim'=>'fold',
                                                           'minLength' =>1,
                                                           'select'=>'js:function( event, ui ) {
                                                                $(this).val( ui.item.label);
                                                                $("#daftartindakan").val(ui.item.daftartindakan_nama);
                                                                $("#daftartindakan_id").val(ui.item.daftartindakan_id);
                                                                getDaftarTindakan();
                                                                return false;
                                                            }',
                                                    ),
                                                'htmlOptions'=>array(
                                                    'onChange'=>'getDaftarTindakan()',
                                                    'readonly'=>false,
                                                    'placeholder'=>'Nama Tindakan',
                                                    'class'=>'span3',
                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                ),
                                                'tombolDialog'=>array('idDialog'=>'dialogdaftartindakan'),
                                            )); ?>
                        </div>
                    </div>
                    <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->KelasPelayananItems, 'kelaspelayanan_id', 'kelaspelayanan_nama'),array('class'=>'span3', 'style'=>'width:230px;','onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --','onchange'=>'getDaftarTindakan();'));?>
                    <div class="control-group">
                        <?php echo CHtml::label('Komponen Tarif','kompTarif',array('class'=>"control-label")) ?>
                        <div class="controls">
                            <?php echo CHtml::hiddenField('komponentarif_id','',array('readonly'=>true)) ?>
                            <?php 
                                $komponen = KomponentarifM::model()->findAll('komponentarif_id != :komponentarif',array(':komponentarif'=>  Params::KOMPONENTARIF_ID_TOTAL));
                                foreach($komponen as $hasil):
                                    $arrHasil[] = array(    
                                      'label'=>$hasil->komponentarif_nama,
                                      'value'=>$hasil->komponentarif_nama,
                                      'id'=>$hasil->komponentarif_id,);
                                endforeach;
                             ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'komponentarif', 
                                                    'source'=>$arrHasil,
                                                    'options'=>array(
                                                      'minLength'=>'1',
                                                      'select'=>'js:function( event, ui ){$("#komponentarif").val(ui.item.value);$("#komponentarif_id").val(ui.item.id);}',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'readonly'=>false,
                                                        'placeholder'=>'Komponen Tarif',
                                                        'class'=>'span3',
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                    ),
                                            )); ?>
                        </div>
                     </div>
                    <div class="control-group">
                        <?php echo $form->labelex($model,'harga_tariftindakan',array('class'=>"control-label required")) ?>
                        <div class="controls">
                            <?php echo CHtml::textfield('harga_tariftindakan','',array('value'=>0,'class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                            <?php echo CHtml::button( '+', array('class'=>'btn btn-primary','onkeypress'=>"return addKomponenTarif(this,event,'kategoritindakan')",'onclick'=>"return addKomponenTarif(this,event,'kategoritindakan')",'id'=>'row1-plus')); ?>
                        </div>
                    </div>
                </td>
                <td>
                    <?php echo $form->hiddenfield($model,'perdatarif_id',array('value'=>  Params::DEFAULT_PERDA_TARIF)); ?>
                    <?php echo $form->dropDownListRow($model,'jenistarif_id',  CHtml::listData($model->JenisTarifItems, 'jenistarif_id', 'jenistarif_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --')); ?>
                    
                    <div class="control-group">
                        <?php echo $form->labelex($model,'Diskon',array('class'=>"control-label required")) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'persendiskon_tind',array('value'=>0,'class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> %
                        </div>
                    </div>


                    <div class="control-group">
                        <?php echo $form->labelex($model,'Diskon',array('class'=>"control-label required")) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'hargadiskon_tind',array('value'=>0,'class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> Rupiah
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <?php echo $form->labelex($model,'Cyto',array('class'=>"control-label required")) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'persencyto_tind',array('value'=>0,'class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> %
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <fieldset>
            <div class="table" id="divDaftartindakan">

            </div>
        </fieldset>    
        <div class="form-actions">
                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'button', 'onKeypress'=>'cekSubmit(this,event)', 'onClick'=>'cekSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.tarifTindakanM.'/admin'), 
                            array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
       	<?php
$content = $this->renderPartial('../tips/tipsaddedit2b',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>    

<?php $this->endWidget(); ?>
<?php
if (!empty($var)){
    echo '<script>
            $(document).ready(function(){
                getDaftarTindakan();
            });
            </script>';
}
$ajaxGetDaftarTindakan = CController::createUrl('tarifTindakanM/ajaxGetDaftarTindakan');
$jscript = <<< JSCRIPT
$("a.delete").live("click", function(){
    /* Better index-calculation from @activa */
    var myIndex = $(this).parent().prevAll().length;
    $(this).parents("table").find("tr").each(function(){
      $(this).find("td:eq("+myIndex+")").remove();
    });
    $(this).parent().remove();
    countTarifTotal();
  });
     
function getDaftarTindakan(item)
{
    kategoritindakan_id = $("#kategoritindakan_id").val();
    kelaspelayanan_id = $("#SATarifTindakanM_kelaspelayanan_id").val();
    daftartindakan_id = $("#daftartindakan_id").val();

    if (jQuery.isNumeric(kelaspelayanan_id) && jQuery.isNumeric(daftartindakan_id)){
    $('#divDaftartindakan').html('');
        $.post("${ajaxGetDaftarTindakan}", {kelaspelayanan_id:kelaspelayanan_id, 
            daftartindakan_id:daftartindakan_id}, function(data){
            if(data.status == 'Not Empty'){
                $('#divDaftartindakan').html(data.table);
                $('#tblInputTarifTindakan').append(data.tfooter);
            }else{
                $('#divDaftartindakan').append(data.messege);
                $('#komponentarif').val('');
                $('#daftartindakan').val('');
            }
        }, 'json');
    }
}
function refreshDialogPendaftaran(){
    var kategoritindakanId = $("#idKategori").val();
    var kategoritindakan_nama = $("#kategoritindakan_nama").val();
    $.fn.yiiGridView.update('daftartindakan-grid', {
        data: {
            "DaftartindakanM[idKategori]":kategoritindakanId,
            "DaftartindakanM[kategoritindakan_id]":kategoritindakanId,
            "DaftartindakanM[kategoritindakan_nama]":kategoritindakan_nama,
        }
    });
}

function countTarifTotal()
{
    var counttarif = 0;
    $('#tblInputTarifTindakan tbody tr').each(function(){        
        $(this).find(".tarif").each(function(){
            counttarif = counttarif + parseFloat($(this).val());            
        });
        $('#tblInputTarifTindakan tfoot').find(".default").val(parseFloat(counttarif));
    });
}

function addKomponenTarif(obj,evt,next_id)
{
    var i = 1;
        $('.komponen').each(function(){
                i++;
        });
    var tar = 1;
        $('.tarif').each(function(){
                tar++;
        });
    var tot = 0;
        $('.default').each(function(){
                tot++;
                 $(this).find("input[name='totalHarga[]']").attr('name','totalHarga['+tot+']');
        });
    var idKomponen = $('#komponentarif_id').val();
    var idKelasPelayanan = $('#SATarifTindakanM_kelaspelayanan_id').val();
    var defaultTarif = $('#harga_tariftindakan').val();
    var hapusIcon = '<a onclick="hapusKomponen(this);return false;" rel="tooltip" href="javascript:void(0);" data-original-title="Klik untuk menghapus Obat"><i class="icon-remove"></i></a>';
    var inputTarif = '<input type="text" size="4" name="tarif['+i+']" value="'+defaultTarif+'"  class="tarif" onkeyup="countTarifTotal();" />';
    var inputHiddenKomponen = '<input type="hidden" size="4" class="komponen" name="komponen['+i+']" id="komponen_'+i+'" readonly="true" value="'+$('#komponentarif_id').val()+'" />';
    if($('#komponentarif_id').val()==''){
        alert('Isi Komponen Tarif dahulu!');
        $('#komponentarif_id').focus();
        return false;  
    }
    if (cekList(idKomponen) == true) 
    {
        if(!jQuery.isNumeric(idKelasPelayanan)){
            alert ('Pilih Kelas Pelayanan Terlebih Dahulu');
        }else{
//            $('#tblInputTarifTindakan tr:first ').append("<th><a href='#' class='delete'>HAPUS</a><br/>"+inputHiddenKomponen+$('#komponentarif').val()+"</th>");    
//            $('#tblInputTarifTindakan tr:not(:first)').each(function(){
            $('#tblInputTarifTindakan').append("<tr><td></td><td></td><td>"+$('#komponentarif').val()+"</td><td>"+inputTarif+inputHiddenKomponen+"</td><td>"+hapusIcon+"</td></tr>");    
//            $('#tblInputTarifTindakan tr:not(:first)').each(function(){
//               $(this).append("<td>"+inputTarif+"</td>");
//            });
            countTarifTotal();
            $('#komponentarif').val('');
            $('#harga_tariftindakan').val('');
            $('#komponentarif').focus();
            return false;
        }
    }
}

function cekKomponenTarif(idKomponen)
{
    var not_sama = true;
    var inputan = $('#tblInputTarifTindakan').find('input[name="komponen[]"]');
    $(inputan).each(function(){
        if($(this).val() == idKomponen){
            not_sama = false;
            alert('Maaf Komponen Tarif Tersebut Sudah Anda Inputkan')
            $('#komponentarif').val('');
            $('#komponentarif').focus();
            return false;
        } else {
            not_sama = true;
            return true;
        }
    });
    return not_sama;

}

function nextFocus(obj,evt,next_id)
{
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
    if(charCode == 13) {
        $('#'+next_id).focus();
        $('#'+next_id).select();
        return false;
    } else {
        return true;
    }
} 

function checkUncheckAll(obj)
{
    var check = obj.checked;
    $('#tblInputTarifTindakan tr:not(:first)').each(function(){
       $(this).find("input[name='daftartindakan_id[]']").each(function(){
            if(!check)
                $(this).attr('checked',false);
            else
                $(this).attr('checked',true);
        });
    });
}

function cekSubmit(obj,evt){
     if($('#komponentarif_id').val()==''){
        alert('Isi Komponen Tarif dahulu!');
        $('#kategoritindakan').focus();
        return false;
    }else if($('#SATarifTindakanM_jenistarif_id').val()==''){
        alert('Isi Jenis Tarif');
        $('#SATarifTindakanM_jenistarif_id').focus();
        return false;
    }else{
      document.forms["satarif-tindakan-m-form"].submit();
    }
}

function clearForm(){
    $('#kategoritindakan').val('');
    $('#daftartindakan').val('');
    
    return false;
}
        
function hapusKomponen(obj)
{
    var obat = $(obj).parents("tr").find("td[id$='komponen']").text();
    if(confirm('Apakah anda akan menghapus Komponen Tarif ?'))
    {
        $(obj).parent().parent().remove();
        countTarifTotal();
    }
    
    
}      
function cekList(id){
    x = true;
    $('.komponen').each(function(){
        if ($(this).val() == id){
            alert('Komponen Tarif telah ada di List');
            x = false;
        }
    });
    return x;
}  
    
JSCRIPT;
Yii::app()->clientScript->registerScript('ambil_data_tarif',$jscript,CClientScript::POS_HEAD);
?>
<?php
/* ====================================== Widget Dialog Kategori Tindakan ====================================== */
    
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
        'id'=>'dialogkategoritindakan',
        'options'=>array(
            'title'=>'Pencarian Kategori Tindakan',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>400,
            'resizable'=>false,
            ),
    ));
   
$modKategoriTindakan = new KategoritindakanM('search');
$modKategoriTindakan->unsetAttributes();
if(isset($_GET['KategoritindakanM'])) {
    $modKategoriTindakan->attributes = $_GET['KategoritindakanM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'kategoritindakan-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider'=>$modKategoriTindakan->search(),
    'filter'=>$modKategoriTindakan,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                    array(
                                            "class"=>"btn-small",
                                            "id" => "selectbarang",
                                            "onClick" => "\$(\"#kategoritindakan_id\").val($data->kategoritindakan_id);
                                                                  \$(\"#kategoritindakan\").val(\"$data->kategoritindakan_nama\");
                                                                  \$(\"#dialogkategoritindakan\").dialog(\"close\");"
                                     )
                     )',
                ),
                'kategoritindakan_nama',
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
/* ====================================== endWidget Dialog Kategori Tindakan ====================================== */
?>

<?php
/* ====================================== Widget Dialog Daftar Tindakan ====================================== */
    
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
        'id'=>'dialogdaftartindakan',
        'options'=>array(
            'title'=>'Pencarian Daftar Tindakan',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>400,
            'resizable'=>false,
            ),
    ));
   
$modDaftarTindakan = new DaftartindakanM('search');
$modDaftarTindakan->unsetAttributes();
$modDaftarTindakan->kategoritindakan_id = $var['kategoritindakan_id'];
if(isset($_GET['DaftartindakanM'])) {
    $modDaftarTindakan->attributes = $_GET['DaftartindakanM'];
    $modDaftarTindakan->kategoritindakan_id = $_GET['DaftartindakanM']['kategoritindakan_id'];
    $modDaftarTindakan->kategoritindakan_nama = $_GET['DaftartindakanM']['kategoritindakan_nama'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'daftartindakan-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider'=>$modDaftarTindakan->searchDaftarTindakan(),
    'filter'=>$modDaftarTindakan,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                    array(
                                            "class"=>"btn-small",
                                            "id" => "selectbarang",
                                            "onClick" => "\$(\"#daftartindakan_id\").val($data->daftartindakan_id);
                                                                  \$(\"#daftartindakan\").val(\"$data->daftartindakan_nama\");
                                                                  \$(\"#dialogdaftartindakan\").dialog(\"close\");
                                                                  getDaftarTindakan();"
                                                                    
                                     )
                     )',
                ),
                'daftartindakan_nama',
                array(
                    'header'=>'Kelompok Tindakan',
                    // 'name'=>'kelompoktindakan_nama',
                    'value'=>'$data->kelompoktindakan->kelompoktindakan_nama',
                ),
                array(
                    'header'=>'Kategori Tindakan',
                     'name'=>'kategoritindakan_nama',
                    'value'=>'$data->kategoritindakan->kategoritindakan_nama',
                ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
            $("#kategoritindakan_id").val($("#idKategori").val());
        jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
/* ====================================== endWidget Dialog Daftar Tindakan ====================================== */
?>

<?php
/* ====================================== Widget Dialog Komponen Tarif ====================================== */
    
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
        'id'=>'dialogkomponentarif',
        'options'=>array(
            'title'=>'Pencarian Komponen Tarif',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>400,
            'resizable'=>false,
            ),
    ));
   
$modKomponenTarif = new KomponentarifM('search');
$modKomponenTarif->unsetAttributes();
if(isset($_GET['KomponentarifM'])) {
    $modKomponenTarif->attributes = $_GET['KomponentarifM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'komponentarif-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider'=>$modKomponenTarif->search(),
    'filter'=>$modKomponenTarif,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                    array(
                                            "class"=>"btn-small",
                                            "id" => "selectbarang",
                                            "onClick" => "\$(\"#komponentarif_id\").val($data->komponentarif_id);
                                                                  \$(\"#komponentarif\").val(\"$data->komponentarif_nama\");
                                                                  \$(\"#dialogkomponentarif\").dialog(\"close\");"
                                     )
                     )',
                ),
                'komponentarif_nama',
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
/* ====================================== endWidget Dialog Komponen Tarif ====================================== */
?>
