<?php
$this->breadcrumbs=array(
	'Satarif Tindakan Ms'=>array('index'),
	$model->tariftindakan_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Tarif Tindakan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Tarif Tindakan', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Tarif Tindakan', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Tarif Tindakan', 'icon'=>'pencil','url'=>array('update','id'=>$model->tariftindakan_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Tarif Tindakan','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->tariftindakan_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Tarif Tindakan', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'satarif-tindakan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#kategoritindakan',
)); ?>
<table class="table">
        <tr>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Perda Tarif','perda_tarif',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('perda',  $model->perdanama_sk,array('class'=>'span3', 'readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Jenis Tarif','jenis_tarif',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('jenistarif',  $model->jenistarif_nama,array('class'=>'span3', 'readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Kategori Tindakan','kategori_tariftindakan',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('daftartindakan',  $model->kategoritindakan_nama,array('class'=>'span3', 'readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Daftar Tindakan','daftar_tariftindakan',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('daftartindakan',  $model->daftartindakan_nama,array('class'=>'span3', 'readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
            </td>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Kelas Pelayanan','kelaspelayanan',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('kelaspelayanan_id', $model->kelaspelayanan_nama,array('onkeypress'=>"return $(this).focusNextInputField(event)",'readonly'=>TRUE,)) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Persen Diskon','persen_diskon',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('persendiskon_tind', $model->persendiskon_tind,array('onkeypress'=>"return $(this).focusNextInputField(event)",'readonly'=>TRUE,)) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Harga Diskon','harga_diskon',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('hargadiskon_tind', $model->hargadiskon_tind,array('onkeypress'=>"return $(this).focusNextInputField(event)",'readonly'=>TRUE,)) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Persen Cyto','persen_cyto',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('persencyto_tind', $model->persencyto_tind,array('onkeypress'=>"return $(this).focusNextInputField(event)",'readonly'=>TRUE,)) ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table class="table" id="tblInputTarifTindakan">
        <tr>
            <th>Komponen Tarif</th>
            <th>Tarif Tindakan</th>
        </tr>
<?php
     foreach ($modKomponenTarif as $tind) {
        echo  '<tr><td>'.KomponentarifM::model()->findByPk($tind->komponentarif_id)->komponentarif_nama.'</td>';
        /*
         * Kondisi untuk menentukan textfield dari komponentarif total harga dan yang komponen tarif biasa
         */
        if ($tind->komponentarif_id == Params::KOMPONENTARIF_ID_TOTAL) {
            $textField = CHtml::textField('totaltariftindakan',$tind->harga_tariftindakan,array("readonly"=>TRUE));
        }else
        {
            $textField = CHtml::textField('hargatariftindakan[]',$tind->harga_tariftindakan,array("readonly"=>TRUE,'onkeypress'=>"return $(this).focusNextInputField(event)"));
        }
        
        echo '<td>'.$textField.'</td>';
    }
?>
    </table>
<?php $this->endWidget() ?>
<?php $this->widget('TipsMasterData',array('type'=>'view'));?>