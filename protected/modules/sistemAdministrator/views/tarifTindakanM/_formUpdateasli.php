<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'satarif-tindakan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#kategoritindakan',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

            <div class="control-group">
                <?php echo CHtml::label('Kategori Tindakan','kattindakan',array('class'=>"control-label")) ?>
                <div class="controls">
                    <?php echo CHtml::textField('kategoritindakan',$kategoritindakan_nama,array('readonly'=>true)) ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo CHtml::label('Daftar Tindakan','daftindakan',array('class'=>"control-label")) ?>
                <div class="controls">
                    <?php echo CHtml::hiddenField('daftartindakan_id',$id,array('readonly'=>true)) ?>
                    <?php echo CHtml::textField('kategoritindakan',DaftartindakanM::model()->findByPk($id)->daftartindakan_nama,array('readonly'=>true)) ?>
                </div>
            </div>
            <fieldset>
                <div id="divDaftartindakan" class="table">
                    <table id="tblInputTarifTindakan">
                        <tbody>
                            <?php
                            $tr = ''; $inputHiddenKomponen = '<input type="hidden" size="4" name="komponen[]" readonly="true" value="'.Params::KOMPONENTARIF_ID_TOTAL.'" />';
                            $returnVal = '<table id="tblInputTarifTindakan"><th> Pilih Semua <br/>'.CHtml::checkBox('checkUncheck', true, array('onclick'=>'checkUncheckAll(this);')).'</th>
                                                <th>Tarif Tindakan</th><th>'.$inputHiddenKomponen.'Tarif Total</th>';
                            foreach($modDaftarTindakan as $data)
                            {
                                $tr .= "<tr><td>";
                                $tr .= CHtml::checkBox('daftartindakan_id[]', true, array('value'=>$data->getAttribute('daftartindakan_id')));
                                $tr .= '</td><td>'.$data->getAttribute('daftartindakan_nama');
                                $tr .= '</td><td>'.CHtml::textField('totalHarga[]', '0', array('size'=>6,'class'=>'default'));
                                $tr .= "</td></tr>";
                            }
                            $returnVal .= $tr;
                            $returnVal .= '</table>';
                            echo $returnVal;
                            ?>
                        </tbody>
                    </table>
                </div>
            </fieldset>
            <?php /*
             <?php echo $form->errorSummary($model); ?>
            <?php echo $form->textFieldRow($model,'jenistarif_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'daftartindakan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'komponentarif_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'perdatarif_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'harga_tariftindakan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'persendiskon_tind',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'hargadiskon_tind',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'persencyto_tind',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
             * 
             */?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.tarifTindakanM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
