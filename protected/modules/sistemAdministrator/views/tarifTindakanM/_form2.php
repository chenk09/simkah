
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'satarif-tindakan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#kategoritindakan',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
        <table class="table">
            <tr>
                <td>
                    <div class="control-group">
                        <?php echo CHtml::label('Kategori Tindakan','kattindakan',array('class'=>"control-label")) ?>
                        <div class="controls">
                            <?php echo CHtml::hiddenField('kategoritindakan_id','',array('readonly'=>true)) ?>
                            <?php 
                                $kategori = KategoritindakanM::model()->findAll();
                                foreach($kategori as $value):
                                    $returnVal[] = array(    
                                      'label'=>$value->kategoritindakan_nama,
                                      'value'=>$value->kategoritindakan_nama,
                                      'id'=>$value->kategoritindakan_id,);
                                endforeach;
                             ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'kategoritindakan', 
                                                    'source'=>$returnVal,
                                                    'options'=>array(
                                                      'minLength'=>'1',
                                                      'select'=>'js:function( event, ui ){$("#kategoritindakan").val(ui.item.value);$("#kategoritindakan_id").val(ui.item.id);}',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'onChange'=>'getDaftarTindakan()',
                                                        'readonly'=>false,
                                                        'placeholder'=>'Kategori Tindakan',
                                                        'class'=>'span3',
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                    ),
                                            )); ?>
                        </div>
                    </div>
                    <?php echo $form->dropDownListRow($model,'kelaspelayanan_id',  CHtml::listData($model->KelasPelayananItems, 'kelaspelayanan_id', 'kelaspelayanan_nama'),array('class'=>'span3', 'style'=>'width:230px;','onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --', 'onChange'=>'getDaftarTindakan()',)); ?>
                    <div class="control-group">
                        <?php echo CHtml::label('Nama Tindakan','kattindakan',array('class'=>"control-label")) ?>
                        <div class="controls">
                            <?php echo CHtml::hiddenField('daftartindakan_id','',array('readonly'=>true)) ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'daftartindakan', 
                                                    'source'=>$returnVal,
                                                    'options'=>array(
                                                      'minLength'=>'1',
                                                      'select'=>'js:function( event, ui ){
                                                          $("#daftartindakan").val(ui.item.value);
                                                          $("#kategoritindakan_id").val(ui.item.id);}',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'onChange'=>'getDaftarTindakan()',
                                                        'readonly'=>false,
                                                        'placeholder'=>'Nama Tindakan',
                                                        'class'=>'span3',
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                    ),
                                            )); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label('Komponen Tarif','kompTarif',array('class'=>"control-label")) ?>
                        <div class="controls">
                            <?php echo CHtml::hiddenField('komponentarif_id','',array('readonly'=>true)) ?>
                            <?php 
                                $komponen = KomponentarifM::model()->findAll('komponentarif_id != :komponentarif',array(':komponentarif'=>  Params::KOMPONENTARIF_ID_TOTAL));
                                foreach($komponen as $hasil):
                                    $arrHasil[] = array(    
                                      'label'=>$hasil->komponentarif_nama,
                                      'value'=>$hasil->komponentarif_nama,
                                      'id'=>$hasil->komponentarif_id,);
                                endforeach;
                             ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'komponentarif', 
                                                    'source'=>$arrHasil,
                                                    'options'=>array(
                                                      'minLength'=>'1',
                                                      'select'=>'js:function( event, ui ){$("#komponentarif").val(ui.item.value);$("#komponentarif_id").val(ui.item.id);}',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'readonly'=>false,
                                                        'placeholder'=>'Komponen Tarif',
                                                        'class'=>'span3',
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                    ),
                                            )); ?>
                        </div>
                     </div>
                    <div class="control-group">
                        <?php echo $form->labelex($model,'harga_tariftindakan',array('class'=>"control-label required")) ?>
                        <div class="controls">
                            <?php echo CHtml::textfield('harga_tariftindakan','',array('value'=>0,'class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                            <?php echo CHtml::button( '+', array('class'=>'btn btn-primary','onkeypress'=>"return addKomponenTarif(this,event,'kategoritindakan')",'onclick'=>"return addKomponenTarif(this,event,'kategoritindakan')",'id'=>'row1-plus')); ?>
                        </div>
                    </div>
                </td>
                <td>
                    <?php echo $form->hiddenfield($model,'perdatarif_id',array('value'=>  Params::DEFAULT_PERDA_TARIF)); ?>
                    <?php echo $form->dropDownListRow($model,'jenistarif_id',  CHtml::listData($model->JenisTarifItems, 'jenistarif_id', 'jenistarif_nama'),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --')); ?>
                    
                    <?php echo $form->textFieldRow($model,'persendiskon_tind',array('value'=>0,'class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php echo $form->textFieldRow($model,'hargadiskon_tind',array('value'=>0,'class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php echo $form->textFieldRow($model,'persencyto_tind',array('value'=>0,'class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </td>
            </tr>
        </table>
        <fieldset>
            <div class="table" id="divDaftartindakan">

            </div>
        </fieldset>    
        <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button', 'onKeypress'=>'cekSubmit(this,event)', 'onClick'=>'cekSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.tarifTindakanM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
       	<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>    

<?php $this->endWidget(); ?>
<?php
$ajaxGetDaftarTindakan = CController::createUrl('tarifTindakanM/ajaxGetDaftarTindakan');
$jscript = <<< JSCRIPT
$("a.delete").live("click", function(){
    /* Better index-calculation from @activa */
    var myIndex = $(this).parent().prevAll().length;
    $(this).parents("table").find("tr").each(function(){
      $(this).find("td:eq("+myIndex+")").remove();
    });
    $(this).parent().remove();
    countTarifTotal();
  });
     
function getDaftarTindakan(item)
{
    kategoritindakan_id = $("#kategoritindakan_id").val();
    kelaspelayanan_id = $("#SATarifTindakanM_kelaspelayanan_id").val();
    
    if (jQuery.isNumeric(kategoritindakan_id) && jQuery.isNumeric(kelaspelayanan_id)){
        $.post("${ajaxGetDaftarTindakan}", {kategoritindakan_id:kategoritindakan_id,kelaspelayanan_id:kelaspelayanan_id}, function(data){
            if(data.status == 'Not Empty'){
                $('#divDaftartindakan').html(data.table);
            }else{
                $('#divDaftartindakan').append(data.messege);
            }
        }, 'json');
    }
}

function countTarifTotal()
{
    $('#tblInputTarifTindakan').find("tr").each(function(){
        var counttarif = 0;
        $(this).find("input[name^='tarif']").each(function(){
            counttarif = counttarif + parseFloat($(this).val());
        });
        $(this).find("input[name='totalHarga[]']").val(counttarif);
    });
}

function addKomponenTarif(obj,evt,next_id)
{
    var idKomponen = $('#komponentarif_id').val();
    var defaultTarif = $('#harga_tariftindakan').val();
    var inputTarif = '<input type="text" size="4" name="tarif['+idKomponen+'][]" value="'+defaultTarif+'" onkeyup="countTarifTotal();" />';
    var inputHiddenKomponen = '<input type="hidden" size="4" name="komponen[]" readonly="true" value="'+$('#komponentarif_id').val()+'" />';
    if($('#komponentarif_id').val()==''){
        alert('Isi Komponen Tarif dahulu!');
        $('#komponentarif_id').focus();
        return false;  
    }
    if(cekKomponenTarif(idKomponen))
    {
        $('#tblInputTarifTindakan tr:first ').append("<th><a href='#' class='delete'>HAPUS</a><br/>"+inputHiddenKomponen+$('#komponentarif').val()+"</th>");    
        $('#tblInputTarifTindakan tr:not(:first)').each(function(){
           $(this).append("<td>"+inputTarif+"</td>");
        });
        countTarifTotal();
        $('#komponentarif').val('');
        $('#harga_tariftindakan').val('');
        $('#komponentarif').focus();
        return false;
    }
}

function cekKomponenTarif(idKomponen)
{
    var not_sama = true;
    var inputan = $('#tblInputTarifTindakan').find('input[name="komponen[]"]');
    $(inputan).each(function(){
        if($(this).val() == idKomponen){
            not_sama = false;
            alert('Maaf Komponen Tarif Tersebut Sudah Anda Inputkan')
            $('#komponentarif').val('');
            $('#komponentarif').focus();
            return false;
        } else {
            not_sama = true;
            return true;
        }
    });
    return not_sama;

}

function nextFocus(obj,evt,next_id)
{
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
    if(charCode == 13) {
        $('#'+next_id).focus();
        $('#'+next_id).select();
        return false;
    } else {
        return true;
    }
} 

function checkUncheckAll(obj)
{
    var check = obj.checked;
    $('#tblInputTarifTindakan tr:not(:first)').each(function(){
       $(this).find("input[name='daftartindakan_id[]']").each(function(){
            if(!check)
                $(this).attr('checked',false);
            else
                $(this).attr('checked',true);
        });
    });
}

function cekSubmit(obj,evt){
     if($('#komponentarif_id').val()==''){
        alert('Isi Komponen Tarif dahulu!');
        $('#kategoritindakan').focus();
        return false;
    }else{
      document.forms["satarif-tindakan-m-form"].submit();
    }
}
JSCRIPT;
Yii::app()->clientScript->registerScript('ambil_data_tarif',$jscript,CClientScript::POS_HEAD);