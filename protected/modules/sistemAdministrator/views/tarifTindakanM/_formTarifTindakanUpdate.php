<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'satarif-tindakan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onSubmit'=>'cekInputan();return false;'),
        'focus'=>'#hargatariftindakan',
)); ?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php echo CHtml::hiddenfield('tariftindakan_id', $model->tariftindakan_id,array('class'=>'span3', 'readonly'=>TRUE)) ?>
<table class="table">
        <tr>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Kategori Tindakan','kategori_tariftindakan',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('daftartindakan',  DaftartindakanM::model()->getKategoriTindakanNama($model->daftartindakan_id),array('class'=>'span3', 'readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Daftar Tindakan','daftar_tariftindakan',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('daftartindakan',  DaftartindakanM::model()->getDaftarTindakanNama($model->daftartindakan_id),array('class'=>'span3', 'readonly'=>TRUE, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                        <?php echo CHtml::hiddenfield('daftartindakan_id',  $model->daftartindakan_id,array('class'=>'span1', 'readonly'=>true)); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Jenis Tarif','jenis_tarif',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::dropDownList('jenistarif_id', $model->jenistarif_id , CHtml::listData(TariftindakanM::model()->getJenisTarifItems(),'jenistarif_id', 'jenistarif_nama'), array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>"inputRequire")) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Kelas Pelayanan','kelaspelayanan',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::dropDownList('kelaspelayanan_id', $model->kelaspelayanan_id , CHtml::listData(TariftindakanM::model()->getKelasPelayananItems(),'kelaspelayanan_id', 'kelaspelayanan_nama'), array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>"inputRequire",'onChange'=>'editKelasPelayanan();')) ?>
                    </div>
                </div>
<!--                <div class="control-group">
                    <?php echo CHtml::label('Perda Tarif','perda_tarif',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo  CHtml::dropDownList('perdatarif_id', $model->perdatarif_id , CHtml::listData(TariftindakanM::model()->getPerdaTarifItems(),'perdatarif_id', 'perdanama_sk'), array('empty'=>'- Pilih -', 'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>"inputRequire")) ?>
                    </div>
                </div>-->
            </td>
            <td>
                
                <div class="control-group">
                    <?php echo CHtml::label('Persen Diskon','persen_diskon',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('persendiskon_tind', $model->persendiskon_tind,array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>"inputRequire")) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Harga Diskon','harga_diskon',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('hargadiskon_tind', $model->hargadiskon_tind,array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>"inputRequire")) ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Persen Cyto','persen_cyto',array('class'=>"control-label required")) ?>
                    <div class="controls">
                        <?php echo CHtml::textfield('persencyto_tind', $model->persencyto_tind,array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>"inputRequire")) ?>
                    </div>
                </div>
                <div class="control-group">
                        <?php echo CHtml::label('Tambah Komponen Tarif','kompTarif',array('class'=>"control-label")) ?>
                        <div class="controls">
                            <?php echo CHtml::hiddenField('komponentarif_id','',array('readonly'=>true)) ?>
                            <?php 
                                $komponen = KomponentarifM::model()->findAll('komponentarif_id != :komponentarif',array(':komponentarif'=>  Params::KOMPONENTARIF_ID_TOTAL));
                                foreach($komponen as $hasil):
                                    $arrHasil[] = array(    
                                      'label'=>$hasil->komponentarif_nama,
                                      'value'=>$hasil->komponentarif_nama,
                                      'id'=>$hasil->komponentarif_id,);
                                endforeach;
                             ?>
                            <?php $this->widget('MyJuiAutoComplete', array(
                                                   'name'=>'komponentarif', 
                                                    'source'=>$arrHasil,
                                                    'options'=>array(
                                                      'minLength'=>'1',
                                                      'select'=>'js:function(event,ui){tambahKomponen(ui.item.id,ui.item.value);}',
                                                    ),
                                                    'htmlOptions'=>array(
                                                        'readonly'=>false,
                                                        'placeholder'=>'Tambah Komponen Tarif',
                                                        'size'=>13,
                                                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                    ),
                                            )); ?>
                        </div>
                </div>
            </td>
        </tr>
    </table>
    <table class="table table-bordered" id="tblInputTarifTindakan" oncLick="countTarifTotal()">
        <thead>
            <tr>
                <th>Komponen Tarif</th>
                <th>Kelas Pelayanan</th>
                <th>Tarif Tindakan</th>
                <th>Hapus Komponen Tarif</th>
            </tr>
        </thead>
        <tbody>
<?php
     foreach ($modDaftarTindakan as $i=>$tind) {
        /*
         * Kondisi untuk menentukan textfield dari komponentarif total harga dan yang komponen tarif biasa
         */
        if ($tind->komponentarif_id == Params::KOMPONENTARIF_ID_TOTAL) {
            $foot =  '<tr><td>'.KomponentarifM::model()->findByPk($tind->komponentarif_id)->komponentarif_nama.''
                            .CHtml::hiddenField("tariftind[$i][tariftindakanId]",$tind->tariftindakan_id,array("readonly"=>TRUE))
                            .CHtml::hiddenField("tariftind[$i][komponenId]",$tind->komponentarif_id,array("readonly"=>TRUE))
                            .CHtml::hiddenField("tariftind[$i][kelaspelayananId]",$tind->kelaspelayanan_id,array("readonly"=>TRUE)).'</td>';
                         
            $textField = CHtml::textField("tariftind[$i][hargatariftindakan]",$tind->harga_tariftindakan,array(
                    "id"=>"totaltariftindakan",
                    "onselect"=>"countTarifTotal(this)",
                    "onclick"=>"countTarifTotal(this)",
                    "readonly"=>TRUE
                )
             );
            $foot .= '<td></td>';
            $foot .= '<td>'.$textField.'</td>';
            $foot .= '<td></td>';
            $foot .= '</tr>';
            $idTarifTotalTindakan = $tind->tariftindakan_id;
        } else {
            echo  '<tr><td>'.KomponentarifM::model()->findByPk($tind->komponentarif_id)->komponentarif_nama.''
                            .CHtml::hiddenField("tariftind[$i][tariftindakanId]",$tind->tariftindakan_id,array("readonly"=>TRUE))
                            .CHtml::hiddenField("tariftind[$i][komponenId]",$tind->komponentarif_id,array("readonly"=>TRUE))
                            .CHtml::hiddenField("tariftind[$i][kelaspelayananId]",$tind->kelaspelayanan_id,array("readonly"=>TRUE)).'</td>';
            echo '<td>'.CHtml::textField("kelaspelayanan_nama",$tind->kelaspelayanan->kelaspelayanan_nama).'</td>';
            $textField = CHtml::textField(
                "tariftind[$i][hargatariftindakan]",
                $tind->harga_tariftindakan,
                array(
                    "onselect"=>"countTarifTotal(this)",
                    "onkeyup"=>"countTarifTotal(this)",
                    "onclick"=>"countTarifTotal(this)",
                    "onfocus"=>"countTarifTotal(this)",
                    "onblur"=>"countTarifTotal(this)",
                    "class"=>"harga",
                    'onkeypress'=>"return $(this).focusNextInputField(event)"
                )
            );
            echo '<td>'.$textField.'</td>';
            echo '<td> <a href="javascript:void(0);" onclick="deleteKomponen(this,'.$tind->tariftindakan_id.');return false;" rel="tooltip" data-original-title="Hapus"><i class="icon-trash"></i></a> </td>';
            echo '</tr>';
        }
        
    }
?>
        </tbody>
        <tfoot>
            <?php echo $foot; ?>
        </tfoot>
    </table>
    <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','onClick'=>'return formSubmit(this,event);','onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/tarifTindakanM/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    </div>
<?php $this->endWidget() ?>

<script>
function testing()
{
    var countTarif = 0;
    $('.harga').each(function(){
        countTarif += parseFloat(this.value);
    });
    $('#totaltariftindakan').val(countTarif);
}

function tambahKomponen(idKomp,namaKomp)
{
    var tidakada = true;
    var tr = '<tr>\n\
                <td>'+namaKomp+'\n\
                    <input type="hidden" name="new[komponenId][]" value="'+idKomp+'" readonly="readonly"></td>\n\
                <td></td>\n\
                <td><input type="text" name="new[hargatariftindakan][]" value="0" onkeypress="return $(this).focusNextInputField(event)" class="harga" onkeyup="countTarifTotal(this)" onselect="countTarifTotal(this)" onclick="countTarifTotal(this)" onfocus="countTarifTotal(this)" onblur="countTarifTotal(this)"></td>\n\
                <td> <a href="javascript:void(0);" onclick="removeKomponen(this);return false;"><i class="icon-trash"></i></a> </td></tr>';
    $('#tblInputTarifTindakan').find('input[name*="[komponenId]"]').each(function(){
        if(this.value == idKomp){
            alert('Komponen tersebut sudah ada!');
            tidakada = tidakada && false;
        } 
    });
            
    if(!tidakada)
        return false;
    
    $('#tblInputTarifTindakan').append(tr);
}

function removeKomponen(obj)
{
    if(confirm('Apakah anda yakin akan menghapus komponen tarif?'))
        $(obj).parents('tr').remove();
}

function deleteKomponen(obj,idTarif)
{
    var idTarifTotalTindakan = <?php echo $idTarifTotalTindakan; ?>;
    var idTarifTindakan = idTarif;
    if(confirm('Apakah anda yakin akan menghapus komponen tarif?')){
        $.post('<?php echo $this->createUrl('deleteKomponenTarif') ?>', {idTarifTotalTindakan:idTarifTotalTindakan, idTarifTindakan:idTarifTindakan}, function(data){
            $('#totaltariftindakan').val(data.totaltarif);
            $(obj).parents('tr').remove();
        }, 'json');
//        $(obj).parents('tr').remove();
    }  
}
</script>

<?php
$ajaxGetDaftarTindakan = CController::createUrl('tarifTindakanM/ajaxGetDaftarTindakan');
$jscript = <<< JSCRIPT
//function countTarifTotal_old()
//{
//    var countTarif = 0;
//    $('#tblInputTarifTindakan tr').each(function(){
//             countTarif = countTarif + parseFloat(this.value);
//    });
//    if(jQuery.isNumeric(countTarif) == false)
//        countTarif = 0
//    $('#totaltariftindakan').val(countTarif);
//}

function countTarifTotal()
{
    var counttarif = 0;
    $('#tblInputTarifTindakan tbody tr').each(function(){        
        $(this).find(".harga").each(function(){
            counttarif = counttarif + parseFloat($(this).val());  
            if($(this).val() <= 0){
                this.value = 0;
             }
        });
        if(jQuery.isNumeric(counttarif) == false)
        counttarif = 0
        $(this).parents().find("#totaltariftindakan").val(parseFloat(counttarif));
    });        
}
        
function addKomponenTarif(obj,evt,next_id)
{
    var idKomponen = $('#komponentarif_id').val();
    var defaultTarif = $('#harga_tariftindakan').val();
    var inputTarif = '<input type="text" size="4" name="tarif['+idKomponen+'][]" value="'+defaultTarif+'" onkeyup="countTarifTotal();" />';
    var inputHiddenKomponen = '<input type="hidden" size="4" name="komponen[]" readonly="true" value="'+$('#komponentarif_id').val()+'" />';
    if($('#komponentarif_id').val()==''){
        alert('Isi Komponen Tarif dahulu!');
        $('#komponentarif_id').focus();
        return false;  
    }
    if(cekKomponenTarif(idKomponen))
    {
        $('#tblInputTarifTindakan tr:first ').append("<th><a href='#' class='delete'>HAPUS</a><br/>"+inputHiddenKomponen+$('#komponentarif').val()+"</th>");    
        $('#tblInputTarifTindakan tr:not(:first)').each(function(){
           $(this).append("<td>"+inputTarif+"</td>");
        });
        countTarifTotal();
        $('#komponentarif').val('');
        $('#harga_tariftindakan').val('');
        $('#komponentarif').focus();
        return false;
    }
}

function cekKomponenTarif(idKomponen)
{
    var not_sama = true;
    var inputan = $('#tblInputTarifTindakan').find('input[name="komponen[]"]');
    $(inputan).each(function(){
        if($(this).val() == idKomponen){
            not_sama = false;
            alert('Maaf Komponen Tarif Tersebut Sudah Anda Inputkan')
            $('#komponentarif').val('');
            $('#komponentarif').focus();
            return false;
        } else {
            not_sama = true;
            return true;
        }
    });
    return not_sama;

}
        
function editKelasPelayanan(){
    var kelaspelayanan_id = $('#kelaspelayanan_id').val();
    var kelaspelayanan_nama = $("#kelaspelayanan_id option:selected").text()
    $('#tblInputTarifTindakan tr').each(function(){
        $(this).parent().find('#kelaspelayanan_nama').val(kelaspelayanan_nama);
        $(this).parent().find('input[name*="[kelaspelayananId]"]').val(kelaspelayanan_id);
     });
}
        
function cekInputan(){
    var totalTarif = $('#tblInputTarifTindakan tr').parent().find('#totaltariftindakan').val();
    if(totalTarif <= 0){
        alert('Maaf, total tarif tidak boleh bernilai 0');
        return false;
    }else{
        document.forms["satarif-tindakan-m-form"].submit();
    }
}
JSCRIPT;
Yii::app()->clientScript->registerScript('ambil_data_tarif',$jscript,CClientScript::POS_HEAD);

