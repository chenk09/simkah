<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'id'=>'sapropinsi-m-search',
                 'type'=>'horizontal',
)); ?>
		<?php /* echo $form->label($model,'zatgizi_id'); */ ?>
		<?php /* echo $form->textField($model,'zatgizi_id'); */ ?>
		<?php echo $form->textFieldRow($model,'zatgizi_nama',array('class'=>'span3','size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->textFieldRow($model,'zatgizi_namalainnya',array('class'=>'span3','size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->textFieldRow($model,'zatgizi_satuan',array('class'=>'span1','size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->checkboxRow($model,'zatgizi_aktif', array('checked'=>'zatgizi_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
    