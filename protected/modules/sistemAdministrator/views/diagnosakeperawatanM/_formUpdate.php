
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sadiagnosakeperawatan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#SADiagnosakeperawatanM_diagnosakeperawatan_kode',
)); ?>

	<p class="help-block"><?php //echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
            <?php //echo CHtml::textField('BKPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true)); ?>
<!--     <div class="control-group ">
            <label for="SADiagnosakeperawatanM_diagnosakeperawatan_id" class="control-label">Diagnosa</label>
            <div class="controls">-->
            <?php 
               
//               echo CHtml::activeHiddenField($model, 'diagnosa_id');
//                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
//                                    'name'=>'DiagnosaM_diagnosa_nama',
//                                    'value'=>$model->diagnosa_nama,
//                                    'source'=>'js: function(request, response) {
//                                                   $.ajax({
//                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/Diagnosa').'",
//                                                       dataType: "json",
//                                                       data: {
//                                                           term: request.term,
//                                                       },
//                                                       success: function (data) {
//                                                               response(data);
//                                                       }
//                                                   })
//                                                }',
//                                     'options'=>array(
//                                           'showAnim'=>'fold',
//                                           'minLength' => 2,
//                                           'focus'=> 'js:function( event, ui ) {
//                                                $(this).val(ui.item.value);
//                                                return false;
//                                            }',
//                                           'select'=>'js:function( event, ui ) {
//                                                $("#'.CHtml::activeId($model, 'diagnosa_id').'").val(ui.item.diagnosa_id)
//                                                return false;
//                                            }',
//                                    ),
//                                )); 
                ?>  
                <?php 
//                echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
//                            array('onclick'=>'$("#dialogDiagnosa").dialog("open");return false;',
//                                  'class'=>'btn btn-primary',
//                                  'onkeypress'=>"return $(this).focusNextInputField(event)",
//                                  'rel'=>"tooltip",
//                                  'title'=>"Klik untuk mencari diagnosa",)); 
                ?>
                <?php 
// Dialog buat nambah data diagnosa =========================
//$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
//    'id'=>'dialogDiagnosa',
//    'options'=>array(
//        'title'=>'Menambah data Diagnosa',
//        'autoOpen'=>false,
//        'modal'=>true,
//        'minWidth'=>600,
//        'minHeight'=>400,
//        'resizable'=>false,
//    ),
//));
//
// 
//$this->widget('ext.bootstrap.widgets.BootGridView',array( 
//    'id'=>'sadiagnosa-m-grid', 
//    'dataProvider'=>$modDiagnosa->search(), 
//    'filter'=>$modDiagnosa, 
//        'template'=>"{pager}{summary}\n{items}", 
//        'itemsCssClass'=>'table table-striped table-bordered table-condensed', 
//    'columns'=>array(
//        array(
//                    'header'=>'Pilih',
//                    'type'=>'raw',
//                    'value'=>'',
//            
//            'value'=>'CHtml::link("<i class=\"icon-check\"></i>","#", array("id" => "selectDiagnosa",
//                                                      "onClick"=>"
//                                                        $(\"#idDiagnosa\").val(\"$data->diagnosa_id\");
//                                                        $(\"#'.CHtml::activeId($model,'diagnosa_id').'\").val(\"$data->diagnosa_id\");
//                                                        $(\"#DiagnosaM_diagnosa_nama\").val(\"$data->diagnosa_nama\");
//                                                        $(\"#dialogDiagnosa\").dialog(\"close\");    
//                                                        "
//                                                 ))',
//                ), 
//        ////'diagnosa_id',
//        array( 
//                        'name'=>'diagnosa_id', 
//                        'value'=>'$data->diagnosa_id', 
//                        'filter'=>false, 
//                ),
//        'diagnosa_kode',
//        'diagnosa_nama',
//        
//        /*
//        'diagnosa_imunisasi',
//        'diagnosa_aktif',
//        */
//       ),
//        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
//)); 
?>

                <?php //$this->endWidget(); ?>
 

<!--            </div>
     </div>     -->
<table>
    <tr>
        <td>
            <?php echo $form->textFieldRow($model, 'diagnosa_nama', array('readonly'=>'readonly ')); ?>
            <?php echo $form->hiddenField($model, 'diagnosa_id'); ?>
            <?php echo $form->hiddenField($model,'diagnosakeperawatan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'diagnosakeperawatan_kode',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textAreaRow($model,'diagnosa_medis',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textAreaRow($model,'diagnosa_keperawatan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textAreaRow($model,'diagnosa_tujuan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->checkBoxRow($model,'diagnosa_keperawatan_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        </td>
    </tr>
    <tr>
        <td>
            <legend class="rim"> Data Kriteria Hasil </legend>
            <?php
                $modKriteria = SAKriteriahasilM::model()->findAllByAttributes(array('diagnosakeperawatan_id'=>$model->diagnosakeperawatan_id));
                
                if(count($modKriteria) > 0){
                foreach($modKriteria as $key=>$kriteria){ ?>    
                <table class="kriteria" style="margin-right:400px;width:500px;">
                    
                            <tr>
                                <td>
                                    <?php echo CHtml::activeTextField($modKriteriaHasil,'['.($key+1).']['.$key.']kriteriahasil_nama',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);",'value'=>$kriteria->kriteriahasil_nama)); ?>
                                    <?php echo CHtml::activeHIddenField($modKriteriaHasil,'['.($key+1).']['.$key.']kriteriahasil_id',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);",'value'=>$kriteria->kriteriahasil_id)); ?>
                                    <?php echo CHtml::activeHIddenField($modKriteriaHasil,'['.($key+1).']['.$key.']diagnosakeperawatan_id',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);",'value'=>$kriteria->diagnosakeperawatan_id)); ?>
                                </td>
                                <td style="margin-right: 400px;">
                                    <?php echo CHtml::link('<i class="icon-plus-sign icon-white"></i>', '#', array('class'=>'btn btn-primary','onclick'=>'addRow2(this)','id'=>'row1-plus','title'=>"Klik untuk menambah kriteria hasil")); ?>                                   
                                    <div id="button-hapus" style="margin-top: -30px;margin-left:35px;">
                                        <?php
                                            if(count($modKriteria) > 0 && $kriteria->kriteriahasil_nama !=''){
                                                echo CHtml::link('<i class="icon-trash icon-white"></i>', '#', array('class'=>'btn btn-primary','onclick'=>'deleteRow(this)','id'=>'row-delete','title'=>"Klik untuk Menghapus Kriteria Hasil"));
                                            }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
            <?php }
                }else{
            ?>
            <table class="kriteria" style="margin-right:400px;width:500px;">
                    
                <tr>
                    <td>
                        <?php echo CHtml::activeTextField($modKriteriaHasil,'[1][1]kriteriahasil_nama',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo CHtml::activeHIddenField($modKriteriaHasil,'[1][1]kriteriahasil_id',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo CHtml::activeHIddenField($modKriteriaHasil,'[1][1]diagnosakeperawatan_id',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    </td>
                    <td style="margin-right: 400px;">
                        <?php echo CHtml::link('<i class="icon-plus-sign icon-white"></i>', '#', array('class'=>'btn btn-primary','onclick'=>'addRow2(this)','id'=>'row1-plus','title'=>"Klik untuk menambah kriteria hasil")); ?>                                   
                        <div id="button-hapus" style="margin-top: -30px;margin-left:35px;">
                        </div>
                    </td>
                </tr>
            </table>
            <?php } ?>
        </td>
    </tr>
</table>            
   
        
        <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                           Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.diagnosakeperawatanM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
		<?php
$content = $this->renderPartial('../tips/tipsaddedit',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
<?php
$buttonMinus = CHtml::link('<i class="icon-minus-sign icon-white"></i>', '#', array('class'=>'btn btn-danger','onclick'=>'delRow(this); return false;'));
$confimMessage = Yii::t('mds','Do You want to remove?');
$js = <<< JSCRIPT
function addRow(obj)
{
    var tr = $('#tbl-RencanaKeperawatan tbody tr:first').html();
    $('#tbl-DiagnosaKeperawatan tr:last').after('<tr>'+tr+'</tr>');
    $('#tbl-DiagnosaKeperawatan tr:last td:last').append('$buttonMinus');
        
        renameInput('SADiagnosakeperawatanM','diagnosa_id');
        renameInput('SADiagnosakeperawatanM','diagnosakeperawatan_id');
        renameInput('SADiagnosakeperawatanM','diagnosakeperawatan_kode');
        renameInput('SADiagnosakeperawatanM','diagnosa_medis');
        renameInput('SADiagnosakeperawatanM','diagnosa_keperawatan');
        renameInput('SADiagnosakeperawatanM','diagnosa_tujuan');
    $('#tbl-DiagnosaKeperawatan tr:last').find('input').val('');
    $('#tbl-DiagnosaKeperawatan tr:last').find('textarea').val('');
    $('#tbl-DiagnosaKeperawatan tr:last').find('select').val('');

}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tbl-DiagnosaKeperawatan tbody tr').length;
    var i = 1;
    $('#tbl-DiagnosaKeperawatan tbody tr').each(function(){
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('textarea[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
    i++;    
    });
}

function delRow2(obj)
{
    if(!confirm("$confimMessage")) return false;
    else {
        $(obj).parent().parent().remove();
        renameInput('SADiagnosakeperawatanM','diagnosa_id');
        renameInput('SADiagnosakeperawatanM','diagnosakeperawatan_id');
        renameInput('SADiagnosakeperawatanM','diagnosakeperawatan_kode');
        renameInput('SADiagnosakeperawatanM','diagnosa_medis');
        renameInput('SADiagnosakeperawatanM','diagnosa_keperawatan');
        renameInput('SADiagnosakeperawatanM','diagnosa_tujuan');
    }
}
        
function addRow2(obj)
{
    var tr = $('.kriteria tbody tr:first').html();
    $(obj).parents('tr').find('.kriteria tbody tr:last').after('<tr>'+tr+'</tr>');
    $(obj).parents('tr').find('.kriteria tbody tr:last td:last').append('$buttonMinus');
    $('.kriteria tbody tr:last').find('#button-hapus').remove();
       
        renameInput2('SAKriteriahasilM','kriteriahasil_nama');
        renameInput2('SAKriteriahasilM','kriteriahasil_id');
        renameInput2('SAKriteriahasilM','diagnosakeperawatan_id');
        $('.kriteria tr:last').find('input').val('');

}
        
function renameInput2(modelName,attributeName)
{
    var trLength = $('#tbl-DiagnosaKeperawatan tbody tr').length;
    var i = 1;
    $('.kriteria').each(function(){
        var x = 1;
        $(this).find('tr').each(function(){
            $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+x+']['+attributeName+']');
            $(this).find('textarea[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+x+']['+attributeName+']');
            $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+x+']['+attributeName+']');
        
            $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+x+'_'+attributeName+'');
            $(this).find('textarea[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+x+'_'+attributeName+'');
            $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+x+'_'+attributeName+'');
            x++;
        });
    i++;    
    });
}
        
function delRow(obj)
{
    if(!confirm("$confimMessage")) return false;
    else {
        $(obj).parent().parent().remove();
        
        renameInput('DiagnosakeperawatanM','diagnosakeperawatan_kode');
        renameInput('DiagnosakeperawatanM','diagnosa_medis');
        renameInput('DiagnosakeperawatanM','diagnosa_keperawatan');
        renameInput('DiagnosakeperawatanM','diagnosa_tujuan');
    }
}
        
function delRow(obj)
{
    if(!confirm("$confimMessage")) return false;
    else {
        $(obj).parent().parent().remove();
        
        renameInput('DiagnosakeperawatanM','diagnosakeperawatan_kode');
        renameInput('DiagnosakeperawatanM','diagnosa_medis');
        renameInput('DiagnosakeperawatanM','diagnosa_keperawatan');
        renameInput('DiagnosakeperawatanM','diagnosa_tujuan');
    }
}        
JSCRIPT;
Yii::app()->clientScript->registerScript('multiple input',$js, CClientScript::POS_HEAD);
?>