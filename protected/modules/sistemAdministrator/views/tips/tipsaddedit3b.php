<span class="required"><i>Bagian dengan tanda * harus diisi.</i></span>
<p>
<table width="200" border="0" style="padding :none;">
  <tr>
    <td width="10px">1. </td>
    <td>Icon  <i class="icon-remove"></i> berfungsi untuk menghapus data yang belum tersimpan.</td>
  </tr>
  <tr>
    <td>2. </td>
    <td>Icon  <i class="icon-trash"></i> berfungsi untuk menghapus data yang sudah tersimpan.</td>
  </tr>
  <tr>
    <td>3. </td>
    <td>Gunakan tombol ini  <div class="btn btn-primary"><i class="icon-ok icon-white"></i>
Simpan
</div> berfungsi untuk menyimpan.</td>
  </tr>
  <tr>
    <td>4. </td>
    <td>Gunakan tombol ini <div class="btn btn-danger">
      <i class="icon-ban-circle icon-white"></i>
      Batal
      </div> berfungsi untuk kembali.</td>
  </tr>
</table>
</td>
  </tr>
</table>
</p>