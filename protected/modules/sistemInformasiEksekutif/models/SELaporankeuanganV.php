<?php
    /**
    * untuk Laporan keuangan
    * @author jembar hardian <jembarhardian@gmail.com> | 19-Mei-2014 | EHJ-1897
    */

class SELaporankeuanganV extends LaporanpemeriksaangroupV{
        public $tgl_awal;
        public $tgl_akhir;
        public $bln_awal;
        public $bln_akhir;
        public $thn_awal;
        public $thn_akhir;
        public $jumlahTampil;
        public $jns_periode;
        public $data_2;
        public $jumlah;

    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    /**
     * this method used in 
     * @return CActiveDataProvider 
     */
    // public function searchDashboard(){
    //     $criteria= new CDbCriteria;
            
    //     $criteria->select = 'count(carabayar_id) as jumlah, carabayar_id, carabayar_nama as data';
    //     $criteria->group = 'carabayar_id, carabayar_nama';
    //     // $criteria->order = 'jumlah DESC';
    //     $criteria->addCondition('tglmasukpenunjang BETWEEN \''.$this->tgl_awal.'\' AND \''.$this->tgl_akhir.'\'');
        

    //     $criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
    //     $criteria->compare('daftartindakan_id',$this->daftartindakan_id);
    //     $criteria->compare('LOWER(daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
    //     $criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
    //     $criteria->compare('LOWER(daftartindakan_katakunci)',strtolower($this->daftartindakan_katakunci),true);
    //     $criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
    //     $criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
    //     $criteria->compare('tarif_satuan',$this->tarif_satuan);
    //     $criteria->compare('qty_tindakan',$this->qty_tindakan);
    //     $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
    //     $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
    //     $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
    //     $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
    //     $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
    //     $criteria->compare('tindakansudahbayar_id',$this->tindakansudahbayar_id);
    //     $criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
    //     $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
    //     $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
    //     $criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
    //     $criteria->compare('carabayar_id',$this->carabayar_id);
    //     $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
    //     $criteria->compare('penjamin_id',$this->penjamin_id);
    //     $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
    //     $criteria->compare('pasien_id',$this->pasien_id);
    //     $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
    //     $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
    //     $criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
    //     $criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
    //     $criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
    //     $criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
    //     $criteria->compare('jmlbayar_tindakan',$this->jmlbayar_tindakan);
    //     $criteria->compare('jmlsisabayar_tindakan',$this->jmlsisabayar_tindakan);
    //     $criteria->compare('jumlahuangmuka',$this->jumlahuangmuka);
    //     $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);

    //     return new CActiveDataProvider($this, array(
    //             'criteria'=>$criteria,
    //             'pagination' => array('pageSize' => $this->jumlahTampil,),
    //             'totalItemCount' => $this->jumlahTampil,
    //     ));
    // }
    
    // public function searchTable()
    // {
    //         // Warning: Please modify the following code to remove attributes that
    //         // should not be searched.

    //         $criteria=new CDbCriteria;
            
    //     // $criteria->select = 'count(carabayar_id) as jumlah, carabayar_id, carabayar_nama as data';
    //     $criteria->group = 'carabayar_id, carabayar_nama';
    //     // $criteria->order = 'jumlah DESC';
    //     $criteria->addCondition('tglmasukpenunjang BETWEEN \''.$this->tgl_awal.'\' AND \''.$this->tgl_akhir.'\'');
        

    //     $criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
    //     $criteria->compare('daftartindakan_id',$this->daftartindakan_id);
    //     $criteria->compare('LOWER(daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
    //     $criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
    //     $criteria->compare('LOWER(daftartindakan_katakunci)',strtolower($this->daftartindakan_katakunci),true);
    //     $criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
    //     $criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
    //     $criteria->compare('tarif_satuan',$this->tarif_satuan);
    //     $criteria->compare('qty_tindakan',$this->qty_tindakan);
    //     $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
    //     $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
    //     $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
    //     $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
    //     $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
    //     $criteria->compare('tindakansudahbayar_id',$this->tindakansudahbayar_id);
    //     $criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
    //     $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
    //     $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
    //     $criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
    //     $criteria->compare('carabayar_id',$this->carabayar_id);
    //     $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
    //     $criteria->compare('penjamin_id',$this->penjamin_id);
    //     $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
    //     $criteria->compare('pasien_id',$this->pasien_id);
    //     $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
    //     $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
    //     $criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
    //     $criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
    //     $criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
    //     $criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
    //     $criteria->compare('jmlbayar_tindakan',$this->jmlbayar_tindakan);
    //     $criteria->compare('jmlsisabayar_tindakan',$this->jmlsisabayar_tindakan);
    //     $criteria->compare('jumlahuangmuka',$this->jumlahuangmuka);
    //     $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);

    //         return new CActiveDataProvider($this, array(
    //                 'criteria'=>$criteria,
    //         ));
    // }

    public function searchCriteria(){
        
            $criteria= new CDbCriteria;

        $criteria->addCondition('tglmasukpenunjang BETWEEN \''.$this->tgl_awal.'\' AND \''.$this->tgl_akhir.'\'');
        

        $criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
        $criteria->compare('daftartindakan_id',$this->daftartindakan_id);
        $criteria->compare('LOWER(daftartindakan_kode)',strtolower($this->daftartindakan_kode),true);
        $criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
        $criteria->compare('LOWER(daftartindakan_katakunci)',strtolower($this->daftartindakan_katakunci),true);
        $criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
        $criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
        $criteria->compare('tarif_satuan',$this->tarif_satuan);
        $criteria->compare('qty_tindakan',$this->qty_tindakan);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
        $criteria->compare('tindakansudahbayar_id',$this->tindakansudahbayar_id);
        $criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
        $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
        $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
        $criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
        $criteria->compare('carabayar_id',$this->carabayar_id);
        $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
        $criteria->compare('penjamin_id',$this->penjamin_id);
        $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
        $criteria->compare('pasien_id',$this->pasien_id);
        $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
        $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
        $criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
        $criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
        $criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
        $criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
        $criteria->compare('jmlbayar_tindakan',$this->jmlbayar_tindakan);
        $criteria->compare('jmlsisabayar_tindakan',$this->jmlsisabayar_tindakan);
        $criteria->compare('jumlahuangmuka',$this->jumlahuangmuka);
        $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);

            return $criteria;
    }


    public function searchGrafikGaris() {
        $criteria = $this->searchCriteria();
        $tgl_awal = date("Y-m-d",strtotime($this->tgl_awal));
        $tgl_akhir = date("Y-m-d",strtotime($this->tgl_akhir));
        $jmlhari = floor(abs(strtotime($this->tgl_awal)-strtotime($this->tgl_akhir))/(60*60*24));
        if($jmlhari > 30){
            $criteria->select = 'count(carabayar_id) as jumlah, EXTRACT(MONTH FROM tglmasukpenunjang::timestamp) as data, EXTRACT(YEAR FROM tglmasukpenunjang::timestamp) as data_2';
            $criteria->group = 'data_2,data';
        }else{
            $criteria->select = 'count(carabayar_id) as jumlah, date(tglmasukpenunjang) as data';
            $criteria->group = 'data';
        }
        $criteria->order = $criteria->group;
        $criteria->limit = -1;
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => false,
                ));
    }


    public function searchGrafikBatangPieCaraBayar() {
        $criteria = $this->searchCriteria();
        $criteria->select = 'count(carabayar_id) as jumlah, carabayar_nama as data';
        $criteria->group = 'carabayar_nama';
        $criteria->order = $criteria->group;
        $criteria->limit = -1;
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => false,
                ));
    }

    public function searchSpeedo() {
        $criteria = $this->searchCriteria();
        $criteria->select = 'count(carabayar_id) as data';
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }


}