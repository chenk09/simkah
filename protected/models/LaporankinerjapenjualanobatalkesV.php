<?php

/**
 * This is the model class for table "laporankinerjapenjualanobatalkes_v".
 *
 * The followings are the available columns in table 'laporankinerjapenjualanobatalkes_v':
 * @property string $tglpenjualan
 * @property integer $penjualanresep_id
 * @property integer $pegawai_id
 * @property string $gelardepan
 * @property string $dokter
 * @property integer $gelarbelakang_id
 * @property integer $obatalkes_id
 * @property string $obatalkes_kode
 * @property string $obatalkes_nama
 * @property string $obatalkes_golongan
 * @property string $obatalkes_kategori
 * @property string $obatalkes_kadarobat
 * @property integer $jenisobatalkes_id
 * @property string $jenisobatalkes_nama
 * @property string $jnskelompok
 * @property double $qty_oa
 * @property double $hargasatuan_oa
 * @property double $hargajual_oa
 * @property double $harganetto_oa
 * @property double $discount
 * @property double $ppn_persen
 * @property double $hpp
 * @property integer $returresep_id
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 * @property string $noresep
 * @property string $jenispenjualan
 * @property integer $oasudahbayar_id
 * @property string $tglpelayanan
 * @property integer $produsen_id
 * @property string $produsen
 */
class LaporankinerjapenjualanobatalkesV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporankinerjapenjualanobatalkesV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporankinerjapenjualanobatalkes_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penjualanresep_id, pegawai_id, gelarbelakang_id, obatalkes_id, jenisobatalkes_id, returresep_id, ruangan_id, oasudahbayar_id, produsen_id', 'numerical', 'integerOnly'=>true),
			array('qty_oa, hargasatuan_oa, hargajual_oa, harganetto_oa, discount, ppn_persen, hpp', 'numerical'),
			array('gelardepan', 'length', 'max'=>10),
			array('dokter, obatalkes_golongan, obatalkes_kategori, jenisobatalkes_nama, ruangan_nama, noresep', 'length', 'max'=>50),
			array('obatalkes_kode, obatalkes_nama', 'length', 'max'=>200),
			array('obatalkes_kadarobat, jnskelompok', 'length', 'max'=>20),
			array('jenispenjualan, produsen', 'length', 'max'=>100),
			array('tglpenjualan, tglpelayanan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tglpenjualan, penjualanresep_id, pegawai_id, gelardepan, dokter, gelarbelakang_id, obatalkes_id, obatalkes_kode, obatalkes_nama, obatalkes_golongan, obatalkes_kategori, obatalkes_kadarobat, jenisobatalkes_id, jenisobatalkes_nama, jnskelompok, qty_oa, hargasatuan_oa, hargajual_oa, harganetto_oa, discount, ppn_persen, hpp, returresep_id, ruangan_id, ruangan_nama, noresep, jenispenjualan, oasudahbayar_id, tglpelayanan, produsen_id, produsen', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tglpenjualan' => 'Tglpenjualan',
			'penjualanresep_id' => 'Penjualanresep',
			'pegawai_id' => 'Pegawai',
			'gelardepan' => 'Gelardepan',
			'dokter' => 'Dokter',
			'gelarbelakang_id' => 'Gelarbelakang',
			'obatalkes_id' => 'Obatalkes',
			'obatalkes_kode' => 'Obatalkes Kode',
			'obatalkes_nama' => 'Obatalkes Nama',
			'obatalkes_golongan' => 'Obatalkes Golongan',
			'obatalkes_kategori' => 'Obatalkes Kategori',
			'obatalkes_kadarobat' => 'Obatalkes Kadarobat',
			'jenisobatalkes_id' => 'Jenisobatalkes',
			'jenisobatalkes_nama' => 'Jenisobatalkes Nama',
			'jnskelompok' => 'Jnskelompok',
			'qty_oa' => 'Qty Oa',
			'hargasatuan_oa' => 'Hargasatuan Oa',
			'hargajual_oa' => 'Hargajual Oa',
			'harganetto_oa' => 'Harganetto Oa',
			'discount' => 'Discount',
			'ppn_persen' => 'Ppn Persen',
			'hpp' => 'Hpp',
			'returresep_id' => 'Returresep',
			'ruangan_id' => 'Ruangan',
			'ruangan_nama' => 'Ruangan Nama',
			'noresep' => 'Noresep',
			'jenispenjualan' => 'Jenispenjualan',
			'oasudahbayar_id' => 'Oasudahbayar',
			'tglpelayanan' => 'Tglpelayanan',
			'produsen_id' => 'Produsen',
			'produsen' => 'Produsen',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(tglpenjualan)',strtolower($this->tglpenjualan),true);
		$criteria->compare('penjualanresep_id',$this->penjualanresep_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(dokter)',strtolower($this->dokter),true);
		$criteria->compare('gelarbelakang_id',$this->gelarbelakang_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(jenisobatalkes_nama)',strtolower($this->jenisobatalkes_nama),true);
		$criteria->compare('LOWER(jnskelompok)',strtolower($this->jnskelompok),true);
		$criteria->compare('qty_oa',$this->qty_oa);
		$criteria->compare('hargasatuan_oa',$this->hargasatuan_oa);
		$criteria->compare('hargajual_oa',$this->hargajual_oa);
		$criteria->compare('harganetto_oa',$this->harganetto_oa);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('ppn_persen',$this->ppn_persen);
		$criteria->compare('hpp',$this->hpp);
		$criteria->compare('returresep_id',$this->returresep_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(noresep)',strtolower($this->noresep),true);
		$criteria->compare('LOWER(jenispenjualan)',strtolower($this->jenispenjualan),true);
		$criteria->compare('oasudahbayar_id',$this->oasudahbayar_id);
		$criteria->compare('LOWER(tglpelayanan)',strtolower($this->tglpelayanan),true);
		$criteria->compare('produsen_id',$this->produsen_id);
		$criteria->compare('LOWER(produsen)',strtolower($this->produsen),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(tglpenjualan)',strtolower($this->tglpenjualan),true);
		$criteria->compare('penjualanresep_id',$this->penjualanresep_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(dokter)',strtolower($this->dokter),true);
		$criteria->compare('gelarbelakang_id',$this->gelarbelakang_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(jenisobatalkes_nama)',strtolower($this->jenisobatalkes_nama),true);
		$criteria->compare('LOWER(jnskelompok)',strtolower($this->jnskelompok),true);
		$criteria->compare('qty_oa',$this->qty_oa);
		$criteria->compare('hargasatuan_oa',$this->hargasatuan_oa);
		$criteria->compare('hargajual_oa',$this->hargajual_oa);
		$criteria->compare('harganetto_oa',$this->harganetto_oa);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('ppn_persen',$this->ppn_persen);
		$criteria->compare('hpp',$this->hpp);
		$criteria->compare('returresep_id',$this->returresep_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(noresep)',strtolower($this->noresep),true);
		$criteria->compare('LOWER(jenispenjualan)',strtolower($this->jenispenjualan),true);
		$criteria->compare('oasudahbayar_id',$this->oasudahbayar_id);
		$criteria->compare('LOWER(tglpelayanan)',strtolower($this->tglpelayanan),true);
		$criteria->compare('produsen_id',$this->produsen_id);
		$criteria->compare('LOWER(produsen)',strtolower($this->produsen),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}