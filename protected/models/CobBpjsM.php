<?php

/**
 * This is the model class for table "cob_bpjs_m".
 *
 * The followings are the available columns in table 'cob_bpjs_m':
 * @property integer $cob_bpjs_id
 * @property string $cob_bpjs_kode
 * @property string $cob_bpjs_nama
 * @property boolean $cob_bpjs_aktif
 */
class CobBpjsM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CobBpjsM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cob_bpjs_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cob_bpjs_nama', 'required'),
			array('cob_bpjs_kode', 'length', 'max'=>20),
			array('cob_bpjs_nama', 'length', 'max'=>100),
			array('cob_bpjs_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cob_bpjs_id, cob_bpjs_kode, cob_bpjs_nama, cob_bpjs_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cob_bpjs_id' => 'Cob Bpjs',
			'cob_bpjs_kode' => 'Cob Bpjs Kode',
			'cob_bpjs_nama' => 'Cob Bpjs Nama',
			'cob_bpjs_aktif' => 'Cob Bpjs Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cob_bpjs_id',$this->cob_bpjs_id);
		$criteria->compare('LOWER(cob_bpjs_kode)',strtolower($this->cob_bpjs_kode),true);
		$criteria->compare('LOWER(cob_bpjs_nama)',strtolower($this->cob_bpjs_nama),true);
		$criteria->compare('cob_bpjs_aktif',$this->cob_bpjs_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('cob_bpjs_id',$this->cob_bpjs_id);
		$criteria->compare('LOWER(cob_bpjs_kode)',strtolower($this->cob_bpjs_kode),true);
		$criteria->compare('LOWER(cob_bpjs_nama)',strtolower($this->cob_bpjs_nama),true);
		$criteria->compare('cob_bpjs_aktif',$this->cob_bpjs_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}