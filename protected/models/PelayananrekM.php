<?php

/**
 * This is the model class for table "pelayananrek_m".
 *
 * The followings are the available columns in table 'pelayananrek_m':
 * @property integer $pelayananrek_id
 * @property integer $rekening4_id
 * @property integer $rekening5_id
 * @property integer $rekening3_id
 * @property integer $rekening1_id
 * @property integer $daftartindakan_id
 * @property integer $rekening2_id
 * @property string $saldonormal
 * @property string $jnspelayanan
 * @property integer $komponentarif_id
 *
 * The followings are the available model relations:
 * @property DaftartindakanM $daftartindakan
 * @property Rekening5M $rekening5
 * @property Rekening1M $rekening1
 * @property Rekening2M $rekening2
 * @property Rekening3M $rekening3
 * @property Rekening4M $rekening4
 * @property KomponentarifM $komponentarif
 */
ini_set('memory_limit', '-1');
class PelayananrekM extends CActiveRecord
{
        public $rekeningdebit_id,$rekDebit,$rekeningkredit_id,$rekKredit,$rekeningnama,$daftartindakan_nama,$komponentarif_nama,$ruanganNama;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PelayananrekM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pelayananrek_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('daftartindakan_id, saldonormal, jnspelayanan', 'required'),
			array('rekening4_id, rekening5_id, rekening3_id, rekening1_id, daftartindakan_id, rekening2_id, komponentarif_id', 'numerical', 'integerOnly'=>true),
			array('saldonormal', 'length', 'max'=>10),
			array('jnspelayanan', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pelayananrek_id, rekening4_id, rekening5_id, rekening3_id, rekening1_id, daftartindakan_id, rekening2_id, saldonormal, jnspelayanan, komponentarif_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daftartindakan' => array(self::BELONGS_TO, 'DaftartindakanM', 'daftartindakan_id'),
			'rekening5' => array(self::BELONGS_TO, 'Rekening5M', 'rekening5_id'),
			'rekening1' => array(self::BELONGS_TO, 'Rekening1M', 'rekening1_id'),
			'rekening2' => array(self::BELONGS_TO, 'Rekening2M', 'rekening2_id'),
			'rekening3' => array(self::BELONGS_TO, 'Rekening3M', 'rekening3_id'),
			'rekening4' => array(self::BELONGS_TO, 'Rekening4M', 'rekening4_id'),
			'komponentarif' => array(self::BELONGS_TO, 'KomponentarifM', 'komponentarif_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pelayananrek_id' => 'Pelayanan Rekening',
			'rekening4_id' => 'Rekening4',
			'rekening5_id' => 'Rekening5',
			'rekening3_id' => 'Rekening3',
			'rekening1_id' => 'Rekening1',
			'daftartindakan_id' => 'Daftar Tindakan',
			'rekening2_id' => 'Rekening2',
			'saldonormal' => 'Saldo Normal',
			'jnspelayanan' => 'Jenis Pelayanan',
			'komponentarif_id' => 'Komponen Tarif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pelayananrek_id',$this->pelayananrek_id);
		$criteria->compare('rekening4_id',$this->rekening4_id);
		$criteria->compare('rekening5_id',$this->rekening5_id);
		$criteria->compare('rekening3_id',$this->rekening3_id);
		$criteria->compare('rekening1_id',$this->rekening1_id);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('rekening2_id',$this->rekening2_id);
		$criteria->compare('LOWER(saldonormal)',strtolower($this->saldonormal),true);
		$criteria->compare('LOWER(jnspelayanan)',strtolower($this->jnspelayanan),true);
		$criteria->compare('komponentarif_id',$this->komponentarif_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pelayananrek_id',$this->pelayananrek_id);
		$criteria->compare('rekening4_id',$this->rekening4_id);
		$criteria->compare('rekening5_id',$this->rekening5_id);
		$criteria->compare('rekening3_id',$this->rekening3_id);
		$criteria->compare('rekening1_id',$this->rekening1_id);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('rekening2_id',$this->rekening2_id);
		$criteria->compare('LOWER(saldonormal)',strtolower($this->saldonormal),true);
		$criteria->compare('LOWER(jnspelayanan)',strtolower($this->jnspelayanan),true);
		$criteria->compare('komponentarif_id',$this->komponentarif_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}