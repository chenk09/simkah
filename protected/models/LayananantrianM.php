<?php

/**
 * This is the model class for table "layananantrian_m".
 *
 * The followings are the available columns in table 'layananantrian_m':
 * @property integer $layananantrian_id
 * @property integer $lokasiantrian_id
 * @property integer $jenisantrian_id
 * @property string $layananantrian_nama
 * @property boolean $layananantrian_aktif
 *
 * The followings are the available model relations:
 * @property JenisantrianM $jenisantrian
 * @property LokasiantrianM $lokasiantrian
 */
class LayananantrianM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LayananantrianM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'layananantrian_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lokasiantrian_id, jenisantrian_id, layananantrian_nama', 'required'),
			array('lokasiantrian_id, jenisantrian_id', 'numerical', 'integerOnly'=>true),
			array('layananantrian_nama', 'length', 'max'=>500),
			array('layananantrian_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('layananantrian_id, lokasiantrian_id, jenisantrian_id, layananantrian_nama, layananantrian_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisantrian' => array(self::BELONGS_TO, 'JenisantrianM', 'jenisantrian_id'),
			'lokasiantrian' => array(self::BELONGS_TO, 'LokasiantrianM', 'lokasiantrian_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'layananantrian_id' => 'Layananantrian',
			'lokasiantrian_id' => 'Lokasiantrian',
			'jenisantrian_id' => 'Jenisantrian',
			'layananantrian_nama' => 'Layananantrian Nama',
			'layananantrian_aktif' => 'Layananantrian Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('layananantrian_id',$this->layananantrian_id);
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('LOWER(layananantrian_nama)',strtolower($this->layananantrian_nama),true);
		$criteria->compare('layananantrian_aktif',$this->layananantrian_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('layananantrian_id',$this->layananantrian_id);
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('LOWER(layananantrian_nama)',strtolower($this->layananantrian_nama),true);
		$criteria->compare('layananantrian_aktif',$this->layananantrian_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}