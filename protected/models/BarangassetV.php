<?php

/**
 * This is the model class for table "barangasset_v".
 *
 * The followings are the available columns in table 'barangasset_v':
 * @property integer $golongan_id
 * @property string $golongan_kode
 * @property string $golongan_nama
 * @property integer $kelompok_id
 * @property string $kelompok_kode
 * @property string $kelompok_nama
 * @property integer $subkelompok_id
 * @property string $subkelompok_kode
 * @property string $subkelompok_nama
 * @property integer $bidang_id
 * @property string $bidang_kode
 * @property string $bidang_nama
 * @property integer $barang_id
 * @property string $barang_type
 * @property string $barang_kode
 * @property string $barang_nama
 * @property string $barang_namalainnya
 * @property string $barang_merk
 * @property string $barang_noseri
 * @property string $barang_ukuran
 * @property string $barang_bahan
 * @property string $barang_thnbeli
 * @property string $barang_warna
 * @property boolean $barang_statusregister
 * @property integer $barang_ekonomis_thn
 * @property string $barang_satuan
 * @property integer $barang_jmldlmkemasan
 * @property string $barang_image
 * @property boolean $barang_aktif
 * @property integer $terimapersdetail_id
 * @property integer $terimapersediaan_id
 * @property string $tglterima
 * @property string $nopenerimaan
 * @property string $tglsuratjalan
 * @property string $nosuratjalan
 * @property string $tglfaktur
 * @property string $nofaktur
 * @property string $keterangan_persediaan
 * @property double $hargabeli
 * @property double $hargasatuan
 * @property double $jmlterima
 * @property string $satuanbeli
 * @property integer $jmldalamkemasan
 * @property string $kondisibarang
 * @property double $totalharga
 * @property double $discount
 * @property double $biayaadministrasi
 * @property double $pajakpph
 * @property double $pajakppn
 * @property string $nofakturpajak
 * @property integer $peg_penerima_id
 * @property integer $peg_mengetahui_id
 * @property integer $ruanganpenerima_id
 */
class BarangassetV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BarangassetV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'barangasset_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('golongan_id, kelompok_id, subkelompok_id, bidang_id, barang_id, barang_ekonomis_thn, barang_jmldlmkemasan, terimapersdetail_id, terimapersediaan_id, jmldalamkemasan, peg_penerima_id, peg_mengetahui_id, ruanganpenerima_id', 'numerical', 'integerOnly'=>true),
			array('hargabeli, hargasatuan, jmlterima, totalharga, discount, biayaadministrasi, pajakpph, pajakppn', 'numerical'),
			array('golongan_kode, kelompok_kode, subkelompok_kode, bidang_kode, barang_type, barang_kode, barang_merk, barang_warna, barang_satuan, nopenerimaan, nosuratjalan, nofaktur, satuanbeli, kondisibarang', 'length', 'max'=>50),
			array('golongan_nama, kelompok_nama, subkelompok_nama, bidang_nama, barang_nama, barang_namalainnya, nofakturpajak', 'length', 'max'=>100),
			array('barang_noseri, barang_ukuran, barang_bahan', 'length', 'max'=>20),
			array('barang_thnbeli', 'length', 'max'=>5),
			array('barang_image', 'length', 'max'=>200),
			array('barang_statusregister, barang_aktif, tglterima, tglsuratjalan, tglfaktur, keterangan_persediaan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('golongan_id, golongan_kode, golongan_nama, kelompok_id, kelompok_kode, kelompok_nama, subkelompok_id, subkelompok_kode, subkelompok_nama, bidang_id, bidang_kode, bidang_nama, barang_id, barang_type, barang_kode, barang_nama, barang_namalainnya, barang_merk, barang_noseri, barang_ukuran, barang_bahan, barang_thnbeli, barang_warna, barang_statusregister, barang_ekonomis_thn, barang_satuan, barang_jmldlmkemasan, barang_image, barang_aktif, terimapersdetail_id, terimapersediaan_id, tglterima, nopenerimaan, tglsuratjalan, nosuratjalan, tglfaktur, nofaktur, keterangan_persediaan, hargabeli, hargasatuan, jmlterima, satuanbeli, jmldalamkemasan, kondisibarang, totalharga, discount, biayaadministrasi, pajakpph, pajakppn, nofakturpajak, peg_penerima_id, peg_mengetahui_id, ruanganpenerima_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'golongan_id' => 'Golongan',
			'golongan_kode' => 'Kode Golongan',
			'golongan_nama' => 'Nama Golongan',
			'kelompok_id' => 'Kelompok',
			'kelompok_kode' => 'Kode Kelompok',
			'kelompok_nama' => 'Nama Kelompok',
			'subkelompok_id' => 'Subkelompok',
			'subkelompok_kode' => 'Kode Subkelompok',
			'subkelompok_nama' => 'Nama Subkelompok',
			'bidang_id' => 'Bidang',
			'bidang_kode' => 'Kode Bidang',
			'bidang_nama' => 'Nama Bidang',
			'barang_id' => 'Barang',
			'barang_type' => 'Type Barang',
			'barang_kode' => 'Kode Barang',
			'barang_nama' => 'Nama Barang',
			'barang_namalainnya' => 'Nama Lain',
			'barang_merk' => 'Merk Barang',
			'barang_noseri' => 'Noseri Barang',
			'barang_ukuran' => 'Ukuran Barang',
			'barang_bahan' => 'Bahan Barang',
			'barang_thnbeli' => 'Tahun Beli Barang',
			'barang_warna' => ' Warna Barang',
			'barang_statusregister' => 'Status Register',
			'barang_ekonomis_thn' => 'Tahun Ekonomis',
			'barang_satuan' => 'Satuan Barang',
			'barang_jmldlmkemasan' => 'Jml dalam kemasan',
			'barang_image' => 'Gambar',
			'barang_aktif' => 'Barang Aktif',
			'terimapersdetail_id' => 'Terima Persediaan Detail',
			'terimapersediaan_id' => 'Terima Persediaan',
			'tglterima' => 'Tgl Terima',
			'nopenerimaan' => 'No Penerimaan',
			'tglsuratjalan' => 'Tgl Surat Jalan',
			'nosuratjalan' => 'No Surat Jalan',
			'tglfaktur' => 'Tglfaktur',
			'nofaktur' => 'NO Faktur',
			'keterangan_persediaan' => 'Keterangan Persediaan',
			'hargabeli' => 'Harga Beli',
			'hargasatuan' => 'Harga Satuan',
			'jmlterima' => 'Jml Terima',
			'satuanbeli' => 'Satuan Beli',
			'jmldalamkemasan' => 'Jml dalam kemasan',
			'kondisibarang' => 'Kondisi Barang',
			'totalharga' => 'Total Harga',
			'discount' => 'Discount',
			'biayaadministrasi' => 'Biaya Administrasi',
			'pajakpph' => 'Pajak Pph',
			'pajakppn' => 'Pajak Ppn',
			'nofakturpajak' => 'No Faktur Pajak',
			'peg_penerima_id' => 'Peg Penerima',
			'peg_mengetahui_id' => 'Peg Mengetahui',
			'ruanganpenerima_id' => 'Ruangan Penerima',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('golongan_id',$this->golongan_id);
		$criteria->compare('golongan_kode',$this->golongan_kode,true);
		$criteria->compare('golongan_nama',$this->golongan_nama,true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('kelompok_kode',$this->kelompok_kode,true);
		$criteria->compare('kelompok_nama',$this->kelompok_nama,true);
		$criteria->compare('subkelompok_id',$this->subkelompok_id);
		$criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
		$criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
		$criteria->compare('bidang_id',$this->bidang_id);
		$criteria->compare('bidang_kode',$this->bidang_kode,true);
		$criteria->compare('bidang_nama',$this->bidang_nama,true);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('barang_type',$this->barang_type,true);
		$criteria->compare('barang_kode',$this->barang_kode,true);
		$criteria->compare('barang_nama',$this->barang_nama,true);
		$criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
		$criteria->compare('barang_merk',$this->barang_merk,true);
		$criteria->compare('barang_noseri',$this->barang_noseri,true);
		$criteria->compare('barang_ukuran',$this->barang_ukuran,true);
		$criteria->compare('barang_bahan',$this->barang_bahan,true);
		$criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
		$criteria->compare('barang_warna',$this->barang_warna,true);
		$criteria->compare('barang_statusregister',$this->barang_statusregister);
		$criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
		$criteria->compare('barang_satuan',$this->barang_satuan,true);
		$criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
		$criteria->compare('barang_image',$this->barang_image,true);
		$criteria->compare('barang_aktif',$this->barang_aktif);
		$criteria->compare('terimapersdetail_id',$this->terimapersdetail_id);
		$criteria->compare('terimapersediaan_id',$this->terimapersediaan_id);
		$criteria->compare('tglterima',$this->tglterima,true);
		$criteria->compare('nopenerimaan',$this->nopenerimaan,true);
		$criteria->compare('tglsuratjalan',$this->tglsuratjalan,true);
		$criteria->compare('nosuratjalan',$this->nosuratjalan,true);
		$criteria->compare('tglfaktur',$this->tglfaktur,true);
		$criteria->compare('nofaktur',$this->nofaktur,true);
		$criteria->compare('keterangan_persediaan',$this->keterangan_persediaan,true);
		$criteria->compare('hargabeli',$this->hargabeli);
		$criteria->compare('hargasatuan',$this->hargasatuan);
		$criteria->compare('jmlterima',$this->jmlterima);
		$criteria->compare('satuanbeli',$this->satuanbeli,true);
		$criteria->compare('jmldalamkemasan',$this->jmldalamkemasan);
		$criteria->compare('kondisibarang',$this->kondisibarang,true);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('biayaadministrasi',$this->biayaadministrasi);
		$criteria->compare('pajakpph',$this->pajakpph);
		$criteria->compare('pajakppn',$this->pajakppn);
		$criteria->compare('nofakturpajak',$this->nofakturpajak,true);
		$criteria->compare('peg_penerima_id',$this->peg_penerima_id);
		$criteria->compare('peg_mengetahui_id',$this->peg_mengetahui_id);
		$criteria->compare('ruanganpenerima_id',$this->ruanganpenerima_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}