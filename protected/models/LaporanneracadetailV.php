<?php

/**
 * This is the model class for table "laporanneracadetail_v".
 *
 * The followings are the available columns in table 'laporanneracadetail_v':
 * @property integer $rekening1_id
 * @property string $nmrekening1
 * @property integer $rekening2_id
 * @property string $nmrekening2
 * @property integer $rekening3_id
 * @property string $nmrekening3
 * @property integer $rekening4_id
 * @property string $nmrekening4
 * @property integer $rekening5_id
 * @property string $nmrekening5
 * @property double $saldo
 * @property string $tgljurnalpost
 * @property integer $rekperiod_id
 */
class LaporanneracadetailV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanneracadetailV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanneracadetail_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rekening1_id, rekening2_id, rekening3_id, rekening4_id, rekening5_id, rekperiod_id', 'numerical', 'integerOnly'=>true),
			array('saldo', 'numerical'),
			array('nmrekening1', 'length', 'max'=>100),
			array('nmrekening2', 'length', 'max'=>200),
			array('nmrekening3', 'length', 'max'=>300),
			array('nmrekening4', 'length', 'max'=>400),
			array('nmrekening5', 'length', 'max'=>500),
			array('tgljurnalpost', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('rekening1_id, nmrekening1, rekening2_id, nmrekening2, rekening3_id, nmrekening3, rekening4_id, nmrekening4, rekening5_id, nmrekening5, saldo, tgljurnalpost, rekperiod_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rekening1_id' => 'Rekening1',
			'nmrekening1' => 'Nmrekening1',
			'rekening2_id' => 'Rekening2',
			'nmrekening2' => 'Nmrekening2',
			'rekening3_id' => 'Rekening3',
			'nmrekening3' => 'Nmrekening3',
			'rekening4_id' => 'Rekening4',
			'nmrekening4' => 'Nmrekening4',
			'rekening5_id' => 'Rekening5',
			'nmrekening5' => 'Nmrekening5',
			'saldo' => 'Saldo',
			'tgljurnalpost' => 'Tgljurnalpost',
			'rekperiod_id' => 'Rekperiod',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rekening1_id',$this->rekening1_id);
		$criteria->compare('LOWER(nmrekening1)',strtolower($this->nmrekening1),true);
		$criteria->compare('rekening2_id',$this->rekening2_id);
		$criteria->compare('LOWER(nmrekening2)',strtolower($this->nmrekening2),true);
		$criteria->compare('rekening3_id',$this->rekening3_id);
		$criteria->compare('LOWER(nmrekening3)',strtolower($this->nmrekening3),true);
		$criteria->compare('rekening4_id',$this->rekening4_id);
		$criteria->compare('LOWER(nmrekening4)',strtolower($this->nmrekening4),true);
		$criteria->compare('rekening5_id',$this->rekening5_id);
		$criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
		$criteria->compare('saldo',$this->saldo);
		$criteria->compare('LOWER(tgljurnalpost)',strtolower($this->tgljurnalpost),true);
		$criteria->compare('rekperiod_id',$this->rekperiod_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('rekening1_id',$this->rekening1_id);
		$criteria->compare('LOWER(nmrekening1)',strtolower($this->nmrekening1),true);
		$criteria->compare('rekening2_id',$this->rekening2_id);
		$criteria->compare('LOWER(nmrekening2)',strtolower($this->nmrekening2),true);
		$criteria->compare('rekening3_id',$this->rekening3_id);
		$criteria->compare('LOWER(nmrekening3)',strtolower($this->nmrekening3),true);
		$criteria->compare('rekening4_id',$this->rekening4_id);
		$criteria->compare('LOWER(nmrekening4)',strtolower($this->nmrekening4),true);
		$criteria->compare('rekening5_id',$this->rekening5_id);
		$criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
		$criteria->compare('saldo',$this->saldo);
		$criteria->compare('LOWER(tgljurnalpost)',strtolower($this->tgljurnalpost),true);
		$criteria->compare('rekperiod_id',$this->rekperiod_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}