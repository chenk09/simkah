<?php

/**
 * This is the model class for table "outbox".
 *
 * The followings are the available columns in table 'outbox':
 * @property string $updatedindb
 * @property string $insertintodb
 * @property string $sendingdatetime
 * @property string $text
 * @property string $destinationnumber
 * @property string $coding
 * @property string $udh
 * @property integer $class
 * @property string $textdecoded
 * @property integer $id
 * @property boolean $multipart
 * @property integer $relativevalidity
 * @property string $senderid
 * @property string $sendingtimeout
 * @property string $deliveryreport
 * @property string $creatorid
 */
class Outbox extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Outbox the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'outbox';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('destinationnumber, textdecoded, creatorid', 'required'),
			array('class, relativevalidity', 'numerical', 'integerOnly'=>true),
			array('destinationnumber', 'length', 'max'=>20),
			array('coding, senderid', 'length', 'max'=>255),
			array('deliveryreport', 'length', 'max'=>10),
			array('insertintodb, sendingdatetime, text, udh, textdecoded, multipart, sendingtimeout', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('updatedindb, insertintodb, sendingdatetime, text, destinationnumber, coding, udh, class, textdecoded, id, multipart, relativevalidity, senderid, sendingtimeout, deliveryreport, creatorid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'updatedindb' => 'Update Di Database',
			'insertintodb' => 'Insert Di Database',
			'sendingdatetime' => 'Sending Date Time',
			'text' => 'Text',
			'destinationnumber' => 'No. Tujuan',
			'coding' => 'Coding',
			'udh' => 'Udh',
			'class' => 'Class',
			'textdecoded' => 'Pesan Teks',
			'id' => 'ID',
			'multipart' => 'Multipart',
			'relativevalidity' => 'Relativevalidity',
			'senderid' => 'ID Sender',
			'sendingtimeout' => 'Sending Time Out',
			'deliveryreport' => 'Delivery Report',
			'creatorid' => 'Pembuat Pesan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(updatedindb)',strtolower($this->updatedindb),true);
		$criteria->compare('LOWER(insertintodb)',strtolower($this->insertintodb),true);
		$criteria->compare('LOWER(sendingdatetime)',strtolower($this->sendingdatetime),true);
		$criteria->compare('LOWER(text)',strtolower($this->text),true);
		$criteria->compare('LOWER(destinationnumber)',strtolower($this->destinationnumber),true);
		$criteria->compare('LOWER(coding)',strtolower($this->coding),true);
		$criteria->compare('LOWER(udh)',strtolower($this->udh),true);
		$criteria->compare('class',$this->class);
		$criteria->compare('LOWER(textdecoded)',strtolower($this->textdecoded),true);
		$criteria->compare('id',$this->id);
		$criteria->compare('multipart',$this->multipart);
		$criteria->compare('relativevalidity',$this->relativevalidity);
		$criteria->compare('LOWER(senderid)',strtolower($this->senderid),true);
		$criteria->compare('LOWER(sendingtimeout)',strtolower($this->sendingtimeout),true);
		$criteria->compare('LOWER(deliveryreport)',strtolower($this->deliveryreport),true);
		$criteria->compare('LOWER(creatorid)',strtolower($this->creatorid),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(updatedindb)',strtolower($this->updatedindb),true);
		$criteria->compare('LOWER(insertintodb)',strtolower($this->insertintodb),true);
		$criteria->compare('LOWER(sendingdatetime)',strtolower($this->sendingdatetime),true);
		$criteria->compare('LOWER(text)',strtolower($this->text),true);
		$criteria->compare('LOWER(destinationnumber)',strtolower($this->destinationnumber),true);
		$criteria->compare('LOWER(coding)',strtolower($this->coding),true);
		$criteria->compare('LOWER(udh)',strtolower($this->udh),true);
		$criteria->compare('class',$this->class);
		$criteria->compare('LOWER(textdecoded)',strtolower($this->textdecoded),true);
		$criteria->compare('id',$this->id);
		$criteria->compare('multipart',$this->multipart);
		$criteria->compare('relativevalidity',$this->relativevalidity);
		$criteria->compare('LOWER(senderid)',strtolower($this->senderid),true);
		$criteria->compare('LOWER(sendingtimeout)',strtolower($this->sendingtimeout),true);
		$criteria->compare('LOWER(deliveryreport)',strtolower($this->deliveryreport),true);
		$criteria->compare('LOWER(creatorid)',strtolower($this->creatorid),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}