<?php

/**
 * This is the model class for table "mutasiasetperalatan_v".
 *
 * The followings are the available columns in table 'mutasiasetperalatan_v':
 * @property integer $invperalatan_id
 * @property string $invperalatan_noregister
 * @property string $nolama
 * @property string $tglmutasi
 * @property string $invperalatan_namabrg
 * @property string $invperalatan_ket
 */
class MutasiasetperalatanV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MutasiasetperalatanV the static model class
	 */
	public $tglAwal, $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mutasiasetperalatan_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invperalatan_id', 'numerical', 'integerOnly'=>true),
			array('invperalatan_noregister, nolama', 'length', 'max'=>50),
			array('invperalatan_namabrg', 'length', 'max'=>100),
			array('tglmutasi, invperalatan_ket', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('invperalatan_id, invperalatan_noregister, nolama, tglmutasi, invperalatan_namabrg, invperalatan_ket', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'invperalatan_id' => 'Invperalatan',
			'invperalatan_noregister' => 'Nomor Inventarisasi Akhir',
			'nolama' => 'Nomor Inventarisasi Awal',
			'tglmutasi' => 'Tgl. Mutasi',
			'invperalatan_namabrg' => 'Nama Aset',
			'invperalatan_ket' => 'Keterangan Mutasi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('invperalatan_id',$this->invperalatan_id);
		$criteria->compare('LOWER(invperalatan_noregister)',strtolower($this->invperalatan_noregister),true);
		$criteria->compare('LOWER(nolama)',strtolower($this->nolama),true);
		$criteria->addBetweenCondition('date(tglmutasi)', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(invperalatan_namabrg)',strtolower($this->invperalatan_namabrg),true);
		$criteria->compare('LOWER(invperalatan_ket)',strtolower($this->invperalatan_ket),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('invperalatan_id',$this->invperalatan_id);
		$criteria->compare('LOWER(invperalatan_noregister)',strtolower($this->invperalatan_noregister),true);
		$criteria->compare('LOWER(nolama)',strtolower($this->nolama),true);
		$criteria->compare('LOWER(tglmutasi)',strtolower($this->tglmutasi),true);
		$criteria->compare('LOWER(invperalatan_namabrg)',strtolower($this->invperalatan_namabrg),true);
		$criteria->compare('LOWER(invperalatan_ket)',strtolower($this->invperalatan_ket),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }

    protected function afterFind(){
        foreach($this->metadata->tableSchema->columns as $columnName => $column){
            if (!strlen($this->$columnName)) continue;
            if ($column->dbType == 'date'){                         
                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
            }elseif ($column->dbType == 'timestamp without time zone'){
                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
            }
        }
        return true;
    }
}