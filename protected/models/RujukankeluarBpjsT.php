<?php

/**
 * This is the model class for table "rujukankeluar_bpjs_t".
 *
 * The followings are the available columns in table 'rujukankeluar_bpjs_t':
 * @property integer $rujukankeluar_bpjs_id
 * @property integer $sep_id
 * @property integer $pendaftaran_id
 * @property integer $pasien_id
 * @property string $tgl_dirujuk
 * @property string $ppk_dirujuk
 * @property integer $jnspelayanan
 * @property string $diagnosa_rujukan_kode
 * @property string $diagnosa_rujukan_nama
 * @property integer $tipe_rujukan
 * @property string $poli_rujukan
 * @property string $catatan_rujukan
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_pemakai_id
 * @property integer $update_loginpemakai_id
 * @property string $create_user_bpjs
 * @property string $update_user_bpjs
 */
class RujukankeluarBpjsT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RujukankeluarBpjsT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rujukankeluar_bpjs_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sep_id, pendaftaran_id, pasien_id, ppk_dirujuk, create_time', 'required'),
			array('sep_id, pendaftaran_id, pasien_id, jnspelayanan, tipe_rujukan, create_pemakai_id, update_loginpemakai_id', 'numerical', 'integerOnly'=>true),
			array('ppk_dirujuk, diagnosa_rujukan_kode, poli_rujukan, update_user_bpjs', 'length', 'max'=>20),
			array('diagnosa_rujukan_nama', 'length', 'max'=>50),
			array('tgl_dirujuk, catatan_rujukan, update_time, create_user_bpjs', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('rujukankeluar_bpjs_id, sep_id, pendaftaran_id, pasien_id, tgl_dirujuk, ppk_dirujuk, jnspelayanan, diagnosa_rujukan_kode, diagnosa_rujukan_nama, tipe_rujukan, poli_rujukan, catatan_rujukan, create_time, update_time, create_pemakai_id, update_loginpemakai_id, create_user_bpjs, update_user_bpjs', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'pendaftaran' => array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
                    'pasien' => array(self::BELONGS_TO, 'PasienM', 'pasien_id'),
                    'sep' => array(self::BELONGS_TO, 'SepT', 'sep_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rujukankeluar_bpjs_id' => 'Rujukankeluar Bpjs',
			'sep_id' => 'Sep',
			'pendaftaran_id' => 'Pendaftaran',
			'pasien_id' => 'Pasien',
			'tgl_dirujuk' => 'Tgl Dirujuk',
			'ppk_dirujuk' => 'Ppk Dirujuk',
			'jnspelayanan' => 'Jnspelayanan',
			'diagnosa_rujukan_kode' => 'Diagnosa Rujukan Kode',
			'diagnosa_rujukan_nama' => 'Diagnosa Rujukan Nama',
			'tipe_rujukan' => 'Tipe Rujukan',
			'poli_rujukan' => 'Poli Rujukan',
			'catatan_rujukan' => 'Catatan Rujukan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_pemakai_id' => 'Create Pemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_user_bpjs' => 'Create User Bpjs',
			'update_user_bpjs' => 'Update User Bpjs',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rujukankeluar_bpjs_id',$this->rujukankeluar_bpjs_id);
		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(tgl_dirujuk)',strtolower($this->tgl_dirujuk),true);
		$criteria->compare('LOWER(ppk_dirujuk)',strtolower($this->ppk_dirujuk),true);
		$criteria->compare('jnspelayanan',$this->jnspelayanan);
		$criteria->compare('LOWER(diagnosa_rujukan_kode)',strtolower($this->diagnosa_rujukan_kode),true);
		$criteria->compare('LOWER(diagnosa_rujukan_nama)',strtolower($this->diagnosa_rujukan_nama),true);
		$criteria->compare('tipe_rujukan',$this->tipe_rujukan);
		$criteria->compare('LOWER(poli_rujukan)',strtolower($this->poli_rujukan),true);
		$criteria->compare('LOWER(catatan_rujukan)',strtolower($this->catatan_rujukan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_pemakai_id',$this->create_pemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('LOWER(create_user_bpjs)',strtolower($this->create_user_bpjs),true);
		$criteria->compare('LOWER(update_user_bpjs)',strtolower($this->update_user_bpjs),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('rujukankeluar_bpjs_id',$this->rujukankeluar_bpjs_id);
		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(tgl_dirujuk)',strtolower($this->tgl_dirujuk),true);
		$criteria->compare('LOWER(ppk_dirujuk)',strtolower($this->ppk_dirujuk),true);
		$criteria->compare('jnspelayanan',$this->jnspelayanan);
		$criteria->compare('LOWER(diagnosa_rujukan_kode)',strtolower($this->diagnosa_rujukan_kode),true);
		$criteria->compare('LOWER(diagnosa_rujukan_nama)',strtolower($this->diagnosa_rujukan_nama),true);
		$criteria->compare('tipe_rujukan',$this->tipe_rujukan);
		$criteria->compare('LOWER(poli_rujukan)',strtolower($this->poli_rujukan),true);
		$criteria->compare('LOWER(catatan_rujukan)',strtolower($this->catatan_rujukan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_pemakai_id',$this->create_pemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('LOWER(create_user_bpjs)',strtolower($this->create_user_bpjs),true);
		$criteria->compare('LOWER(update_user_bpjs)',strtolower($this->update_user_bpjs),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}