<?php

/**
 * This is the model class for table "pasien_m".
 *
 * The followings are the available columns in table 'pasien_m':
 * @property integer $pasien_id
 * @property integer $pekerjaan_id
 * @property integer $kelurahan_id
 * @property integer $pendidikan_id
 * @property integer $propinsi_id
 * @property integer $kecamatan_id
 * @property integer $suku_id
 * @property integer $profilrs_id
 * @property integer $kabupaten_id
 * @property string $no_rekam_medik
 * @property string $tgl_rekam_medik
 * @property string $jenisidentitas
 * @property string $no_identitas_pasien
 * @property string $namadepan
 * @property string $nama_pasien
 * @property string $nama_bin
 * @property string $jeniskelamin
 * @property string $kelompokumur_id
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $alamat_pasien
 * @property integer $rt
 * @property integer $rw
 * @property string $statusperkawinan
 * @property string $agama
 * @property string $golongandarah
 * @property string $rhesus
 * @property integer $anakke
 * @property integer $jumlah_bersaudara
 * @property string $no_telepon_pasien
 * @property string $no_mobile_pasien
 * @property string $warga_negara
 * @property string $photopasien
 * @property string $alamatemail
 * @property string $statusrekammedis
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 * @property string $tgl_meninggal
 * @property boolean $ispasienluar
 * @property string $nama_ibu
 * @property string $nama_ayah
 */
class PasienM extends CActiveRecord
{
    public $propinsiNama;
    public $kabupatenNama;
    public $kecamatanNama;
    public $kelurahanNama;
    public $no_pendaftaran;
    public $tgl_pendaftaran;
    public $tgl_rm_awal;
    public $tgl_rm_akhir;
    public $jeniskasuspenyakit_nama;
    public $ceklis;
    public $tipe_pencarian;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PasienM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pasien_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_rekam_medik, tgl_rekam_medik, nama_pasien, tanggal_lahir, alamat_pasien, agama, warga_negara, statusrekammedis, jeniskelamin', 'required'),
			array('pekerjaan_id, kelurahan_id, pendidikan_id, propinsi_id, kecamatan_id, suku_id, profilrs_id, kabupaten_id, rt, rw, anakke, jumlah_bersaudara', 'numerical', 'integerOnly'=>true),
			array('no_rekam_medik, statusrekammedis', 'length', 'max'=>10),
			array('jenisidentitas, namadepan, jeniskelamin, kelompokumur_id, statusperkawinan, agama, rhesus, no_mobile_pasien', 'length', 'max'=>20),
			array('no_identitas_pasien, nama_bin', 'length', 'max'=>30),
			array('nama_pasien, nama_ibu, nama_ayah', 'length', 'max'=>50),
			array('tempat_lahir, warga_negara', 'length', 'max'=>25),
			array('golongandarah', 'length', 'max'=>2),
			array('no_telepon_pasien', 'length', 'max'=>15),
			array('photopasien', 'length', 'max'=>200),
			array('alamatemail', 'length', 'max'=>100),
			array('tipe_pencarian, ispasienluar, update_time, update_loginpemakai_id, tgl_meninggal, no_identitas_pasien, propinsi_id, kecamatan_id, kelurahan_id', 'safe'),
                    
                        array('create_time','default','value'=>date( 'Y-m-d H:i:s', time()),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s', time()),'setOnEmpty'=>false,'on'=>'update,insert'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update'),
                        array('create_ruangan', 'default','value'=>Yii::app()->user->getState('ruangan_id'), 'setOnEmpty'=>false, 'on'=>'insert'),
                    
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idInstalasi,pasien_id, pekerjaan_id, kelurahan_id, pendidikan_id, propinsi_id, kecamatan_id, suku_id, profilrs_id, kabupaten_id, no_rekam_medik, tgl_rekam_medik, jenisidentitas, no_identitas_pasien, namadepan, nama_pasien, nama_bin, jeniskelamin, kelompokumur_id, tempat_lahir, tanggal_lahir, alamat_pasien, rt, rw, statusperkawinan, agama, golongandarah, rhesus, anakke, jumlah_bersaudara, no_telepon_pasien, no_mobile_pasien, warga_negara, photopasien, alamatemail, statusrekammedis, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan, tgl_meninggal, nama_ibu, nama_ayah', 'safe', 'on'=>'search'),
			array('tipe_pencarian, ceklis, propinsiNama, ispasienluar,kabupatenNama, statusrekammedis, kecamatanNama, kelurahanNama','safe','on'=>'searchWithDaerah'),
		);
	}

	/**
                 * @return array relational rules.
                 */
                public function relations()
                {
                        // NOTE: you may need to adjust the relation name and the related
                        // class name for the relations automatically generated below.
                        return array(
                            'propinsi'=>array(self::BELONGS_TO, 'PropinsiM', 'propinsi_id'),
                            'kabupaten' => array(self::BELONGS_TO, 'KabupatenM', 'kabupaten_id'),
                            'kelurahan' => array(self::BELONGS_TO, 'KelurahanM', 'kelurahan_id'),
                            'kecamatan' => array(self::BELONGS_TO, 'KecamatanM', 'kecamatan_id'),
                            'pendaftaran' => array(self::HAS_MANY, 'PendaftaranT', 'pasien_id'),
                            // 'pendaftaran2' => array(self::BELONGS_TO, 'PendaftaranT', 'pasien_id'),
                            'kelompokumur' => array(self::BELONGS_TO, 'KelompokumurM', 'kelompokumur_id'),
                            'pekerjaan' => array(self::BELONGS_TO, 'PekerjaanM', 'pekerjaan_id'),
                            'ruangan'=>array(self::BELONGS_TO, 'RuanganM','create_ruangan'),
                        );
                }

    public function statusBayar($i)
	{
		if($i==null)
			return "Belum Lunas";
		else 
			return "Lunas";
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pasien_id' => 'Pasien',
			'pekerjaan_id' => 'Pekerjaan',
			'kelurahan_id' => 'Kelurahan',
			'pendidikan_id' => 'Pendidikan',
			'propinsi_id' => 'Propinsi',
			'kecamatan_id' => 'Kecamatan',
			'suku_id' => 'Suku',
			'profilrs_id' => 'Profilrs',
			'kabupaten_id' => 'Kabupaten',
			'no_rekam_medik' => 'No Rekam Medik',
			'tgl_rekam_medik' => 'Tgl Rekam Medik',
			'jenisidentitas' => 'Jenis Identitas',
			'no_identitas_pasien' => 'No Identitas',
			'namadepan' => 'Nama Depan',
			'nama_pasien' => 'Nama Pasien',
			'nama_bin' => 'Alias',
			'jeniskelamin' => 'Jenis Kelamin',
			'kelompokumur_id' => 'Kelompok Umur',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'alamat_pasien' => 'Alamat Pasien',
			'rt' => 'Rt',
			'rw' => 'Rw',
			'statusperkawinan' => 'Status Perkawinan',
			'agama' => 'Agama',
			'golongandarah' => 'Golongan Darah',
			'rhesus' => 'Rhesus',
			'anakke' => 'Anak ke',
			'jumlah_bersaudara' => 'Jumlah Bersaudara',
			'no_telepon_pasien' => 'No Telepon Pasien',
			'no_mobile_pasien' => 'No Mobile Pasien',
			'warga_negara' => 'Warga Negara',
			'photopasien' => 'Photo Pasien',
			'alamatemail' => 'Alamat Email',
			'statusrekammedis' => 'Status Rekam medis',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
			'tgl_meninggal' => 'Tgl Meninggal',
                        'propinsiNama'=>'Propinsi',
                        'NamaNamaBIN' => 'Nama Pasien',
                        'CaraBayarPenjamin'=>'Cara Bayar/Penjamin',
                        'ispasienluar'=>'Pasien Luar',
			'nama_ibu' => 'Nama Ibu',
			'nama_ayah' => 'Nama Ayah',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pekerjaan_id',$this->pekerjaan_id);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('kelompokumur_id',$this->kelompokumur_id);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('rt',$this->rt);
		$criteria->compare('rw',$this->rw);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('anakke',$this->anakke);
		$criteria->compare('jumlah_bersaudara',$this->jumlah_bersaudara);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(no_mobile_pasien)',strtolower($this->no_mobile_pasien),true);
		$criteria->compare('LOWER(warga_negara)',strtolower($this->warga_negara),true);
		$criteria->compare('LOWER(photopasien)',strtolower($this->photopasien),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(statusrekammedis)',strtolower($this->statusrekammedis),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('LOWER(tgl_meninggal)',strtolower($this->tgl_meninggal),true);
		$criteria->compare('LOWER(nama_ibu)',strtolower($this->nama_ibu),true);
		$criteria->compare('LOWER(nama_ayah)',strtolower($this->nama_ayah),true);
		$criteria->limit=5;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>false,
		));
	}
        
        public function searchNoMobile()
        {
                $criteria=new CDbCriteria;

                $criteria->addCondition('TRIM(no_mobile_pasien) != \'\'');
		$criteria->compare('LOWER(no_mobile_pasien)',strtolower($this->no_mobile_pasien),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchWithDaerah()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pekerjaan_id',$this->pekerjaan_id);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('t.propinsi_id',$this->propinsi_id);
		$criteria->compare('t.kabupaten_id',$this->kabupaten_id);
		$criteria->compare('t.kecamatan_id',$this->kecamatan_id);
		$criteria->compare('t.kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(propinsi.propinsi_nama)',strtolower($this->propinsiNama),true);
		$criteria->compare('LOWER(kabupaten.kabupaten_nama)',strtolower($this->kabupatenNama),true);
		$criteria->compare('LOWER(kecamatan.kecamatan_nama)',strtolower($this->kecamatanNama),true);
		$criteria->compare('LOWER(kelurahan.kelurahan_nama)',strtolower($this->kelurahanNama),true);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('kelompokumur_id',$this->kelompokumur_id);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('rt',$this->rt);
		$criteria->compare('rw',$this->rw);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('anakke',$this->anakke);
		$criteria->compare('jumlah_bersaudara',$this->jumlah_bersaudara);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(no_mobile_pasien)',strtolower($this->no_mobile_pasien),true);
		$criteria->compare('LOWER(warga_negara)',strtolower($this->warga_negara),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
//		$criteria->compare('LOWER(statusrekammedis)',strtolower(Params::statusRM(1)),true);
		$criteria->addCondition("statusrekammedis ='AKTIF'");
		$criteria->compare('LOWER(tgl_meninggal)',strtolower($this->tgl_meninggal),true);
		$criteria->compare('ispasienluar','false');
		
		if($this->tipe_pencarian == "depan")
		{
			$criteria->compare('LOWER(nama_pasien)', strtolower($this->nama_pasien));
		}else if($this->tipe_pencarian == "belakang")
		{
			$criteria->condition = "LOWER(nama_pasien) LIKE LOWER(:nama_pasien)";
			$criteria->params = array(
				":nama_pasien"=>"% " . $this->nama_pasien
			);
		}else
		{
			$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien), true);
		}
		$data_ruangan = array(18, 19);
		$criteria->addNotInCondition('create_ruangan', $data_ruangan);
		
		$criteria->with = array('propinsi','kabupaten','kecamatan','kelurahan');
		$criteria->order = 'pasien_id DESC';
		//$criteria->limit = 10;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			//'pagination'=>false,
		));
	}
        
        public function searchWithDaerahPenunjang()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pekerjaan_id',$this->pekerjaan_id);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('t.propinsi_id',$this->propinsi_id);
		$criteria->compare('t.kabupaten_id',$this->kabupaten_id);
		$criteria->compare('t.kecamatan_id',$this->kecamatan_id);
		$criteria->compare('t.kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(propinsi.propinsi_nama)',strtolower($this->propinsiNama),true);
		$criteria->compare('LOWER(kabupaten.kabupaten_nama)',strtolower($this->kabupatenNama),true);
		$criteria->compare('LOWER(kecamatan.kecamatan_nama)',strtolower($this->kecamatanNama),true);
		$criteria->compare('LOWER(kelurahan.kelurahan_nama)',strtolower($this->kelurahanNama),true);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('kelompokumur_id',$this->kelompokumur_id);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('rt',$this->rt);
		$criteria->compare('rw',$this->rw);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('anakke',$this->anakke);
		$criteria->compare('jumlah_bersaudara',$this->jumlah_bersaudara);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(no_mobile_pasien)',strtolower($this->no_mobile_pasien),true);
		$criteria->compare('LOWER(warga_negara)',strtolower($this->warga_negara),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(statusrekammedis)',strtolower(Params::statusRM(1)),true);
		$criteria->compare('LOWER(tgl_meninggal)',strtolower($this->tgl_meninggal),true);
		$criteria->compare('t.ispasienluar',1);
                //Jika di filter berdasarkan No. Lab dan Rad
                $criteria->addCondition('create_ruangan IN ('.Params::RUANGAN_ID_RAD.','.Params::RUANGAN_ID_LAB.')');
                $criteria->with = array('propinsi','kabupaten','kecamatan','kelurahan');
                $criteria->order = 'pasien_id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pekerjaan_id',$this->pekerjaan_id);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('t.propinsi_id',$this->propinsi_id);
		$criteria->compare('t.kabupaten_id',$this->kabupaten_id);
		$criteria->compare('t.kecamatan_id',$this->kecamatan_id);
		$criteria->compare('t.kelurahan_id',$this->kelurahan_id);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('kelompokumur_id',$this->kelompokumur_id);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('rt',$this->rt);
		$criteria->compare('rw',$this->rw);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('anakke',$this->anakke);
		$criteria->compare('jumlah_bersaudara',$this->jumlah_bersaudara);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(no_mobile_pasien)',strtolower($this->no_mobile_pasien),true);
		$criteria->compare('LOWER(warga_negara)',strtolower($this->warga_negara),true);
		$criteria->compare('LOWER(photopasien)',strtolower($this->photopasien),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(statusrekammedis)',strtolower($this->statusrekammedis),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('LOWER(tgl_meninggal)',strtolower($this->tgl_meninggal),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date'){
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                    }elseif ($column->dbType == 'timestamp without time zone'){
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);//date('Y-m-d H:i:s', CDateTimeParser::parse($this->$columnName, Yii::app()->locale->dateFormat));
                    }

            }

            return parent::beforeValidate ();
        }

        public function beforeSave() {         
            if($this->tgl_meninggal===null || trim($this->tgl_meninggal)=='')
            {
	        $this->setAttribute('tgl_meninggal', null);
            }
            
            if(strlen(trim($this->kelurahan_id)) == 0)
            {
	        $this->setAttribute('kelurahan_id', null);
            }
            return parent::beforeSave();
        }

        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                }elseif ($column->dbType == 'timestamp without time zone'){
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                }
            }
            return true;
        }
        
        /**
         * Mengambil daftar semua pendidikan
         * @return CActiveDataProvider 
         */
        public function getPendidikanItems()
        {
            return PendidikanM::model()->findAllByAttributes(array('pendidikan_aktif'=>true),array('order'=>'pendidikan_nama'));
        }
        
        /**
         * Mengambil daftar semua pekerjaan
         * @return CActiveDataProvider 
         */
        public function getPekerjaanItems()
        {
            return PekerjaanM::model()->findAllByAttributes(array('pekerjaan_aktif'=>true),array('order'=>'pekerjaan_nama'));
        }
        
        /**
         * Mengambil daftar semua propinsi
         * @return CActiveDataProvider 
         */
        public function getPropinsiItems()
        {
            return PropinsiM::model()->findAllByAttributes(array('propinsi_aktif'=>true),array('order'=>'propinsi_nama'));
        }
        
        /**
         * Mengambil daftar semua kabupaten berdasarkan propinsi
         * @return CActiveDataProvider 
         */
        public function getKabupatenItems($propinsi_id=null)
        {
            if(!empty($propinsi_id))
                return KabupatenM::model()->findAllByAttributes(array('propinsi_id'=>$propinsi_id,'kabupaten_aktif'=>true),array('order'=>'kabupaten_nama'));
            else {
                return array();
            }
        }
        
        /**
         * Mengambil daftar semua kecamatan berdasarkan kabupaten
         * @return CActiveDataProvider 
         */
        public function getKecamatanItems($kabupaten_id=null)
        {
            if(!empty($kabupaten_id))
                return KecamatanM::model()->findAllByAttributes(array('kabupaten_id'=>$kabupaten_id,'kecamatan_aktif'=>true),array('order'=>'kecamatan_nama'));
            else {
                return array();
            }
        }
        
        /**
         * Mengambil daftar semua kelurahan berdasarkan kecamatan
         * @return CActiveDataProvider 
         */
        public function getKelurahanItems($kecamatan_id=null)
        {
            if(!empty($kecamatan_id))
                return KelurahanM::model()->findAllByAttributes(array('kecamatan_id'=>$kecamatan_id,'kelurahan_aktif'=>true),array('order'=>'kelurahan_nama'));
            else {
                return array();
            }
        }
        
        /**
         * Mengambil daftar semua propinsi
         * @return CActiveDataProvider 
         */
        public function getSukuItems()
        {
            return SukuM::model()->findAllByAttributes(array('suku_aktif'=>true),array('order'=>'suku_nama'));
        }
        
        public function getNamaNamaBIN()
        {
            return $this->nama_pasien.' / Alias '.$this->nama_bin;
        }
        
        public function getCaraBayarPenjamin()
        {
                return $this->carabayar_nama.'/'.$this->penjamin_nama;
        }
        
        public function getCekUmurValid()
        {
            $format = new CustomFormat;
            $tglLahir = $format->formatDateMediumForDB($this->tanggal_lahir);
            $timeLahir = strtotime($tglLahir);
            $now = time();
            $datediff = $now - $timeLahir;
            $umur = floor($datediff/86400);
            if($umur > 0)
                return true;
            else
                return false;
        }
        
         public function getKelas(){
            return $this->pendaftaran->kelaspelayanan_id;
        }
}