<?php

/**
 * This is the model class for table "rl3_4_kegiatankebidanan_v".
 *
 * The followings are the available columns in table 'rl3_4_kegiatankebidanan_v':
 * @property double $tahun
 * @property integer $profilrs_id
 * @property string $nokode_rumahsakit
 * @property string $nama_rumahsakit
 * @property integer $daftartindakan_id
 * @property string $daftartindakan_nama
 * @property double $rujukanrs
 * @property double $rujukanbidan
 * @property double $rujukanpuskemas
 * @property double $rujukanfaskeslainnya
 * @property double $rujukanjumlahhidup
 * @property double $rujukanjumlahmati
 * @property double $rujukanjumlahtotal
 * @property double $rujukannonmedisjumlahhidup
 * @property double $rujukannonmedisjumlahmati
 * @property double $rujukannonmedisjumlahtotal
 * @property double $nonrujukanjumlahhidup
 * @property double $nonrujukanjumlahmati
 * @property double $nonrujukanjumlahtotal
 */
class Rl34KegiatankebidananV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rl34KegiatankebidananV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rl3_4_kegiatankebidanan_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profilrs_id, daftartindakan_id', 'numerical', 'integerOnly'=>true),
			array('tahun, rujukanrs, rujukanbidan, rujukanpuskemas, rujukanfaskeslainnya, rujukanjumlahhidup, rujukanjumlahmati, rujukanjumlahtotal, rujukannonmedisjumlahhidup, rujukannonmedisjumlahmati, rujukannonmedisjumlahtotal, nonrujukanjumlahhidup, nonrujukanjumlahmati, nonrujukanjumlahtotal', 'numerical'),
			array('nokode_rumahsakit', 'length', 'max'=>10),
			array('nama_rumahsakit', 'length', 'max'=>100),
			array('daftartindakan_nama', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tahun, profilrs_id, nokode_rumahsakit, nama_rumahsakit, daftartindakan_id, daftartindakan_nama, rujukanrs, rujukanbidan, rujukanpuskemas, rujukanfaskeslainnya, rujukanjumlahhidup, rujukanjumlahmati, rujukanjumlahtotal, rujukannonmedisjumlahhidup, rujukannonmedisjumlahmati, rujukannonmedisjumlahtotal, nonrujukanjumlahhidup, nonrujukanjumlahmati, nonrujukanjumlahtotal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tahun' => 'Tahun',
			'profilrs_id' => 'Profilrs',
			'nokode_rumahsakit' => 'Nokode Rumahsakit',
			'nama_rumahsakit' => 'Nama Rumahsakit',
			'daftartindakan_id' => 'Daftartindakan',
			'daftartindakan_nama' => 'Daftartindakan Nama',
			'rujukanrs' => 'Rujukanrs',
			'rujukanbidan' => 'Rujukanbidan',
			'rujukanpuskemas' => 'Rujukanpuskemas',
			'rujukanfaskeslainnya' => 'Rujukanfaskeslainnya',
			'rujukanjumlahhidup' => 'Rujukanjumlahhidup',
			'rujukanjumlahmati' => 'Rujukanjumlahmati',
			'rujukanjumlahtotal' => 'Rujukanjumlahtotal',
			'rujukannonmedisjumlahhidup' => 'Rujukannonmedisjumlahhidup',
			'rujukannonmedisjumlahmati' => 'Rujukannonmedisjumlahmati',
			'rujukannonmedisjumlahtotal' => 'Rujukannonmedisjumlahtotal',
			'nonrujukanjumlahhidup' => 'Nonrujukanjumlahhidup',
			'nonrujukanjumlahmati' => 'Nonrujukanjumlahmati',
			'nonrujukanjumlahtotal' => 'Nonrujukanjumlahtotal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('nokode_rumahsakit',$this->nokode_rumahsakit,true);
		$criteria->compare('nama_rumahsakit',$this->nama_rumahsakit,true);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('daftartindakan_nama',$this->daftartindakan_nama,true);
		$criteria->compare('rujukanrs',$this->rujukanrs);
		$criteria->compare('rujukanbidan',$this->rujukanbidan);
		$criteria->compare('rujukanpuskemas',$this->rujukanpuskemas);
		$criteria->compare('rujukanfaskeslainnya',$this->rujukanfaskeslainnya);
		$criteria->compare('rujukanjumlahhidup',$this->rujukanjumlahhidup);
		$criteria->compare('rujukanjumlahmati',$this->rujukanjumlahmati);
		$criteria->compare('rujukanjumlahtotal',$this->rujukanjumlahtotal);
		$criteria->compare('rujukannonmedisjumlahhidup',$this->rujukannonmedisjumlahhidup);
		$criteria->compare('rujukannonmedisjumlahmati',$this->rujukannonmedisjumlahmati);
		$criteria->compare('rujukannonmedisjumlahtotal',$this->rujukannonmedisjumlahtotal);
		$criteria->compare('nonrujukanjumlahhidup',$this->nonrujukanjumlahhidup);
		$criteria->compare('nonrujukanjumlahmati',$this->nonrujukanjumlahmati);
		$criteria->compare('nonrujukanjumlahtotal',$this->nonrujukanjumlahtotal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}