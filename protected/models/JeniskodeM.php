<?php

/**
 * This is the model class for table "jeniskode_m".
 *
 * The followings are the available columns in table 'jeniskode_m':
 * @property integer $jeniskode_id
 * @property string $jeniskode_nama
 * @property string $singkatan
 * @property boolean $jeniskode_aktif
 */
class JeniskodeM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return JeniskodeM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jeniskode_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jeniskode_nama, singkatan', 'required'),
			array('jeniskode_nama', 'length', 'max'=>100),
			array('singkatan', 'length', 'max'=>10),
			array('jeniskode_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('jeniskode_id, jeniskode_nama, singkatan, jeniskode_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'jeniskode_id' => 'Jeniskode',
			'jeniskode_nama' => 'Jeniskode Nama',
			'singkatan' => 'Singkatan',
			'jeniskode_aktif' => 'Jeniskode Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('jeniskode_id',$this->jeniskode_id);
		$criteria->compare('jeniskode_nama',$this->jeniskode_nama,true);
		$criteria->compare('singkatan',$this->singkatan,true);
		$criteria->compare('jeniskode_aktif',$this->jeniskode_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}