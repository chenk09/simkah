<?php

/**
 * This is the model class for table "terimamutasi_t".
 *
 * The followings are the available columns in table 'terimamutasi_t':
 * @property integer $terimamutasi_id
 * @property integer $mutasioaruangan_id
 * @property string $tglterima
 * @property string $noterimamutasi
 * @property double $totalharganetto
 * @property double $totalhargajual
 * @property string $keterangan_terima
 * @property integer $ruanganpenerima_id
 * @property integer $ruanganasal_id
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class TerimamutasiT extends CActiveRecord
{
        public $tglAwal;
        public $tglAkhir;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TerimamutasiT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'terimamutasi_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tglterima, noterimamutasi, totalharganetto, totalhargajual, ruanganpenerima_id, ruanganasal_id', 'required'),
			array('mutasioaruangan_id, ruanganpenerima_id, ruanganasal_id', 'numerical', 'integerOnly'=>true),
			array('totalharganetto, totalhargajual', 'numerical'),
			array('noterimamutasi', 'length', 'max'=>20),
			array('keterangan_terima, update_time, update_loginpemakai_id', 'safe'),
                        
                        array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
			array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('terimamutasi_id, mutasioaruangan_id, tglterima, noterimamutasi, totalharganetto, totalhargajual, keterangan_terima, ruanganpenerima_id, ruanganasal_id, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'ruanganasal'=>array(self::BELONGS_TO,'RuanganM','ruanganasal_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'terimamutasi_id' => 'ID',
			'mutasioaruangan_id' => 'Mutasi Ruangan',
			'tglterima' => 'Tanggal Terima',
                        'tglAwal'=>'Tanggal Awal',
                        'tglAkhir'=>'Tanggal Akhir',
			'noterimamutasi' => 'No Terima Mutasi',
			'totalharganetto' => 'Total Harga Netto',
			'totalhargajual' => 'Total Harga Jual',
			'keterangan_terima' => 'Keterangan Terima',
			'ruanganpenerima_id' => 'Ruangan Penerima',
			'ruanganasal_id' => 'Ruangan Asal',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('terimamutasi_id',$this->terimamutasi_id);
		$criteria->compare('mutasioaruangan_id',$this->mutasioaruangan_id);
		$criteria->compare('LOWER(tglterima)',strtolower($this->tglterima),true);
		$criteria->compare('LOWER(noterimamutasi)',strtolower($this->noterimamutasi),true);
		$criteria->compare('totalharganetto',$this->totalharganetto);
		$criteria->compare('totalhargajual',$this->totalhargajual);
		$criteria->compare('LOWER(keterangan_terima)',strtolower($this->keterangan_terima),true);
		$criteria->compare('ruanganpenerima_id',$this->ruanganpenerima_id);
		$criteria->compare('ruanganasal_id',$this->ruanganasal_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('terimamutasi_id',$this->terimamutasi_id);
		$criteria->compare('mutasioaruangan_id',$this->mutasioaruangan_id);
		$criteria->compare('date(tglterima)',$this->tglterima,true);
		$criteria->compare('LOWER(noterimamutasi)',strtolower($this->noterimamutasi),true);
		$criteria->compare('totalharganetto',$this->totalharganetto);
		$criteria->compare('totalhargajual',$this->totalhargajual);
		$criteria->compare('LOWER(keterangan_terima)',strtolower($this->keterangan_terima),true);
		$criteria->compare('ruanganpenerima_id',$this->ruanganpenerima_id);
		$criteria->compare('ruanganasal_id',$this->ruanganasal_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                        }
            }
            return true;
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date')
                        {
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                        }
                    else if ( $column->dbType == 'timestamp without time zone')
                        {
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                        }
            }

            return parent::beforeValidate ();
        }
}