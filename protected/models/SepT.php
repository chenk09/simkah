<?php

/**
 * This is the model class for table "sep_t".
 *
 * The followings are the available columns in table 'sep_t':
 * @property integer $sep_id
 * @property string $tglsep
 * @property string $no_sep
 * @property integer $pendaftaran_id
 * @property integer $pasien_id
 * @property string $nopeserta_bpjs
 * @property string $namapeserta_bpjs
 * @property integer $hakkelas_kode
 * @property string $hakkelas_nama
 * @property string $notelpon_peserta
 * @property string $norujukan_bpjs
 * @property string $tglrujukan_bpjs
 * @property integer $jenisrujukan_kode_bpjs
 * @property string $jenisrujukan_nama_bpjs
 * @property integer $ppkrujukanasal_kode
 * @property string $ppkrujukanasal_nama
 * @property integer $jnspelayanan_kode
 * @property string $jnspelayanan_nama
 * @property string $diagnosaawal_kode
 * @property string $diagnosaawal_nama
 * @property integer $politujuan_kode
 * @property string $politujuan_nama
 * @property integer $kelasrawat_kode
 * @property string $kelasrawat_nama
 * @property string $tanggalpulang_sep
 * @property string $cob_bpjs
 * @property integer $polieksekutif
 * @property integer $lakalantas_kode
 * @property string $lakalantas_nama
 * @property string $penjaminlakalantas
 * @property string $lokasilakalantas
 * @property string $catatan_sep
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $upate_loginpemakai_id
 * @property integer $create_ruangan
 */
class SepT extends CActiveRecord
{   
        public $ppkpelayanan,$ppkpelayanan_nama,$pembuat_sep,$cob_status;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SepT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sep_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tglsep, no_sep, pendaftaran_id, pasien_id, nopeserta_bpjs, namapeserta_bpjs, notelpon_peserta, diagnosaawal_kode, diagnosaawal_nama, catatan_sep, create_time, create_loginpemakai_id, create_ruangan', 'required'),
			array('pendaftaran_id, pasien_id, create_loginpemakai_id, upate_loginpemakai_id, create_ruangan', 'numerical', 'integerOnly'=>true),
			array('no_sep, nopeserta_bpjs, hakkelas_nama, norujukan_bpjs, jenisrujukan_nama_bpjs, ppkrujukanasal_nama, jnspelayanan_nama, politujuan_nama, cob_bpjs, lakalantas_nama', 'length', 'max'=>50),
			array('namapeserta_bpjs, notelpon_peserta, diagnosaawal_nama, penjaminlakalantas, lokasilakalantas', 'length', 'max'=>100),
			array('diagnosaawal_kode', 'length', 'max'=>10),
			array('kelasrawat_nama', 'length', 'max'=>20),
			array('catatan_sep', 'length', 'max'=>500),
			array('tglrujukan_bpjs, tanggalpulang_sep, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('sep_id, tglsep, no_sep, pendaftaran_id, pasien_id, nopeserta_bpjs, namapeserta_bpjs, hakkelas_kode, hakkelas_nama, notelpon_peserta, norujukan_bpjs, tglrujukan_bpjs, jenisrujukan_kode_bpjs, jenisrujukan_nama_bpjs, ppkrujukanasal_kode, ppkrujukanasal_nama, jnspelayanan_kode, jnspelayanan_nama, diagnosaawal_kode, diagnosaawal_nama, politujuan_kode, politujuan_nama, kelasrawat_kode, kelasrawat_nama, tanggalpulang_sep, cob_bpjs, polieksekutif, lakalantas_kode, lakalantas_nama, penjaminlakalantas, lokasilakalantas, catatan_sep, create_time, update_time, create_loginpemakai_id, upate_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
        public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pendaftaran' => array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
			'pasien' => array(self::BELONGS_TO, 'PasienM', 'pasien_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sep_id' => 'Sep',
			'tglsep' => 'Tglsep',
			'no_sep' => 'No Sep',
			'pendaftaran_id' => 'Pendaftaran',
			'pasien_id' => 'Pasien',
			'nopeserta_bpjs' => 'Nopeserta Bpjs',
			'namapeserta_bpjs' => 'Namapeserta Bpjs',
			'hakkelas_kode' => 'Hakkelas Kode',
			'hakkelas_nama' => 'Hakkelas Nama',
			'notelpon_peserta' => 'Notelpon Peserta',
			'norujukan_bpjs' => 'Norujukan Bpjs',
			'tglrujukan_bpjs' => 'Tglrujukan Bpjs',
			'jenisrujukan_kode_bpjs' => 'Jenisrujukan Kode Bpjs',
			'jenisrujukan_nama_bpjs' => 'Jenisrujukan Nama Bpjs',
			'ppkrujukanasal_kode' => 'Ppkrujukanasal Kode',
			'ppkrujukanasal_nama' => 'Ppkrujukanasal Nama',
			'jnspelayanan_kode' => 'Jnspelayanan Kode',
			'jnspelayanan_nama' => 'Jnspelayanan Nama',
			'diagnosaawal_kode' => 'Diagnosaawal Kode',
			'diagnosaawal_nama' => 'Diagnosaawal Nama',
			'politujuan_kode' => 'Politujuan Kode',
			'politujuan_nama' => 'Politujuan Nama',
			'kelasrawat_kode' => 'Kelasrawat Kode',
			'kelasrawat_nama' => 'Kelasrawat Nama',
			'tanggalpulang_sep' => 'Tanggalpulang Sep',
			'cob_bpjs' => 'Cob Bpjs',
			'polieksekutif' => 'Polieksekutif',
			'lakalantas_kode' => 'Lakalantas Kode',
			'lakalantas_nama' => 'Lakalantas Nama',
			'penjaminlakalantas' => 'Penjaminlakalantas',
			'lokasilakalantas' => 'Lokasilakalantas',
			'catatan_sep' => 'Catatan Sep',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'upate_loginpemakai_id' => 'Upate Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('LOWER(tglsep)',strtolower($this->tglsep),true);
		$criteria->compare('LOWER(no_sep)',strtolower($this->no_sep),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(nopeserta_bpjs)',strtolower($this->nopeserta_bpjs),true);
		$criteria->compare('LOWER(namapeserta_bpjs)',strtolower($this->namapeserta_bpjs),true);
		$criteria->compare('hakkelas_kode',$this->hakkelas_kode);
		$criteria->compare('LOWER(hakkelas_nama)',strtolower($this->hakkelas_nama),true);
		$criteria->compare('LOWER(notelpon_peserta)',strtolower($this->notelpon_peserta),true);
		$criteria->compare('LOWER(norujukan_bpjs)',strtolower($this->norujukan_bpjs),true);
		$criteria->compare('LOWER(tglrujukan_bpjs)',strtolower($this->tglrujukan_bpjs),true);
		$criteria->compare('jenisrujukan_kode_bpjs',$this->jenisrujukan_kode_bpjs);
		$criteria->compare('LOWER(jenisrujukan_nama_bpjs)',strtolower($this->jenisrujukan_nama_bpjs),true);
		$criteria->compare('ppkrujukanasal_kode',$this->ppkrujukanasal_kode);
		$criteria->compare('LOWER(ppkrujukanasal_nama)',strtolower($this->ppkrujukanasal_nama),true);
		$criteria->compare('jnspelayanan_kode',$this->jnspelayanan_kode);
		$criteria->compare('LOWER(jnspelayanan_nama)',strtolower($this->jnspelayanan_nama),true);
		$criteria->compare('LOWER(diagnosaawal_kode)',strtolower($this->diagnosaawal_kode),true);
		$criteria->compare('LOWER(diagnosaawal_nama)',strtolower($this->diagnosaawal_nama),true);
		$criteria->compare('politujuan_kode',$this->politujuan_kode);
		$criteria->compare('LOWER(politujuan_nama)',strtolower($this->politujuan_nama),true);
		$criteria->compare('kelasrawat_kode',$this->kelasrawat_kode);
		$criteria->compare('LOWER(kelasrawat_nama)',strtolower($this->kelasrawat_nama),true);
		$criteria->compare('LOWER(tanggalpulang_sep)',strtolower($this->tanggalpulang_sep),true);
		$criteria->compare('LOWER(cob_bpjs)',strtolower($this->cob_bpjs),true);
		$criteria->compare('polieksekutif',$this->polieksekutif);
		$criteria->compare('lakalantas_kode',$this->lakalantas_kode);
		$criteria->compare('LOWER(lakalantas_nama)',strtolower($this->lakalantas_nama),true);
		$criteria->compare('LOWER(penjaminlakalantas)',strtolower($this->penjaminlakalantas),true);
		$criteria->compare('LOWER(lokasilakalantas)',strtolower($this->lokasilakalantas),true);
		$criteria->compare('LOWER(catatan_sep)',strtolower($this->catatan_sep),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('upate_loginpemakai_id',$this->upate_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('LOWER(tglsep)',strtolower($this->tglsep),true);
		$criteria->compare('LOWER(no_sep)',strtolower($this->no_sep),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(nopeserta_bpjs)',strtolower($this->nopeserta_bpjs),true);
		$criteria->compare('LOWER(namapeserta_bpjs)',strtolower($this->namapeserta_bpjs),true);
		$criteria->compare('hakkelas_kode',$this->hakkelas_kode);
		$criteria->compare('LOWER(hakkelas_nama)',strtolower($this->hakkelas_nama),true);
		$criteria->compare('LOWER(notelpon_peserta)',strtolower($this->notelpon_peserta),true);
		$criteria->compare('LOWER(norujukan_bpjs)',strtolower($this->norujukan_bpjs),true);
		$criteria->compare('LOWER(tglrujukan_bpjs)',strtolower($this->tglrujukan_bpjs),true);
		$criteria->compare('jenisrujukan_kode_bpjs',$this->jenisrujukan_kode_bpjs);
		$criteria->compare('LOWER(jenisrujukan_nama_bpjs)',strtolower($this->jenisrujukan_nama_bpjs),true);
		$criteria->compare('ppkrujukanasal_kode',$this->ppkrujukanasal_kode);
		$criteria->compare('LOWER(ppkrujukanasal_nama)',strtolower($this->ppkrujukanasal_nama),true);
		$criteria->compare('jnspelayanan_kode',$this->jnspelayanan_kode);
		$criteria->compare('LOWER(jnspelayanan_nama)',strtolower($this->jnspelayanan_nama),true);
		$criteria->compare('LOWER(diagnosaawal_kode)',strtolower($this->diagnosaawal_kode),true);
		$criteria->compare('LOWER(diagnosaawal_nama)',strtolower($this->diagnosaawal_nama),true);
		$criteria->compare('politujuan_kode',$this->politujuan_kode);
		$criteria->compare('LOWER(politujuan_nama)',strtolower($this->politujuan_nama),true);
		$criteria->compare('kelasrawat_kode',$this->kelasrawat_kode);
		$criteria->compare('LOWER(kelasrawat_nama)',strtolower($this->kelasrawat_nama),true);
		$criteria->compare('LOWER(tanggalpulang_sep)',strtolower($this->tanggalpulang_sep),true);
		$criteria->compare('LOWER(cob_bpjs)',strtolower($this->cob_bpjs),true);
		$criteria->compare('polieksekutif',$this->polieksekutif);
		$criteria->compare('lakalantas_kode',$this->lakalantas_kode);
		$criteria->compare('LOWER(lakalantas_nama)',strtolower($this->lakalantas_nama),true);
		$criteria->compare('LOWER(penjaminlakalantas)',strtolower($this->penjaminlakalantas),true);
		$criteria->compare('LOWER(lokasilakalantas)',strtolower($this->lokasilakalantas),true);
		$criteria->compare('LOWER(catatan_sep)',strtolower($this->catatan_sep),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('upate_loginpemakai_id',$this->upate_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}