<?php

/**
 * This is the model class for table "tindakanpelayanan_t".
 *
 * The followings are the available columns in table 'tindakanpelayanan_t':
 * @property integer $tindakanpelayanan_id
 * @property integer $penjamin_id
 * @property integer $pasienadmisi_id
 * @property integer $pasien_id
 * @property integer $kelaspelayanan_id
 * @property integer $tipepaket_id
 * @property integer $instalasi_id
 * @property integer $pendaftaran_id
 * @property integer $shift_id
 * @property integer $pasienmasukpenunjang_id
 * @property integer $daftartindakan_id
 * @property integer $carabayar_id
 * @property integer $jeniskasuspenyakit_id
 * @property string $tgl_tindakan
 * @property double $tarif_tindakan
 * @property string $satuantindakan
 * @property string $qty_tindakan
 * @property boolean $cyto_tindakan
 * @property double $tarifcyto_tindakan
 * @property string $dokterpemeriksa1_id
 * @property string $dokterpemeriksa2_id
 * @property string $dokterpendamping_id
 * @property string $dokteranastesi_id
 * @property string $dokterdelegasi_id
 * @property string $bidan_id
 * @property string $suster_id
 * @property string $perawat_id
 * @property integer $kelastanggungan_id
 * @property double $discount_tindakan
 * @property double $subsidiasuransi_tindakan
 * @property double $subsidipemerintah_tindakan
 * @property double $subsisidirumahsakit_tindakan
 * @property double $iurbiaya_tindakan
 * @property string $tm 
 * 
 * @property string $kategoriTindakanNama
 * @property string $daftartindakanNama
 * @property double $jumlahTarif
 * @property double $persenCyto
 * 
 * @property double $tarif_satuan
 * @property integer $rencanaoperasi_id
 * @property integer $hasilpemeriksaanpa_id
 * @property integer $hasilpemeriksaanrm_id
 * @property integer $konsulpoli_id
 * @property integer $hasilpemeriksaanrad_id
 * @property integer $detailhasilpemeriksaanlab_id
 * 
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class TindakanpelayananT extends CActiveRecord
{
    public $jumlahTarif;
    public $persenCyto;
    public $kategoriTindakanNama;
    public $daftartindakanNama;
    public $tglAwal;
    public $tglAkhir;
    public $total_biaya;
    public $jmlbayar_tindakan,$bayartindakan;
    public $jmlsisabayar_tindakan,$sisatindakan;
    public $no_pendaftaran,$nama_pasien,$alamat_pasien;

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TindakanpelayananT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tindakanpelayanan_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penjamin_id, pasien_id, kelaspelayanan_id, instalasi_id, pendaftaran_id, shift_id, daftartindakan_id, carabayar_id, jeniskasuspenyakit_id, tgl_tindakan, tarif_tindakan, satuantindakan, qty_tindakan, cyto_tindakan, tarifcyto_tindakan, discount_tindakan, subsidiasuransi_tindakan, subsidipemerintah_tindakan, subsisidirumahsakit_tindakan, iurbiaya_tindakan', 'required'),
			array('penjamin_id, pasienadmisi_id, pasien_id, kelaspelayanan_id, tipepaket_id, instalasi_id, pendaftaran_id, shift_id, pasienmasukpenunjang_id, daftartindakan_id, carabayar_id, jeniskasuspenyakit_id, kelastanggungan_id,
                               dokterpemeriksa1_id, dokterpemeriksa2_id, dokterpendamping_id, dokteranastesi_id, dokterdelegasi_id, bidan_id, suster_id, perawat_id', 'numerical', 'integerOnly'=>true),
			array('tarifcyto_tindakan, discount_tindakan, subsidiasuransi_tindakan, subsidipemerintah_tindakan, subsisidirumahsakit_tindakan, iurbiaya_tindakan', 'numerical'),
			array('satuantindakan', 'length', 'max'=>10),
			array('tm', 'length', 'max'=>2),
			array('ruangan_id,kategoriTindakanNama, daftartindakanNama, tarif_satuan, tarif_tindakan, persenCyto, jumlahTarif, dokterpemeriksa1_id, dokterpemeriksa2_id, dokterpendamping_id, dokteranastesi_id, dokterdelegasi_id, bidan_id, suster_id, perawat_id', 'safe'),
			                    
                        array('create_time','default','value'=>date('Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date('Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update,insert'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
                        array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
                        
                        // The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tindakanpelayanan_id, penjamin_id, pasienadmisi_id, pasien_id, kelaspelayanan_id, tipepaket_id, instalasi_id, pendaftaran_id, shift_id, pasienmasukpenunjang_id, daftartindakan_id, carabayar_id, jeniskasuspenyakit_id, tgl_tindakan, tarif_tindakan, satuantindakan, qty_tindakan, cyto_tindakan, tarifcyto_tindakan, dokterpemeriksa1_id, dokterpemeriksa2_id, dokterpendamping_id, dokteranastesi_id, dokterdelegasi_id, bidan_id, suster_id, perawat_id, kelastanggungan_id, discount_tindakan, subsidiasuransi_tindakan, subsidipemerintah_tindakan, subsisidirumahsakit_tindakan, iurbiaya_tindakan, tm, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                        'daftartindakan'=>array(self::BELONGS_TO,'DaftartindakanM','daftartindakan_id'),
                        'dokter1'=>array(self::BELONGS_TO,'PegawaiM','dokterpemeriksa1_id'),
                        'dokter2'=>array(self::BELONGS_TO,'PegawaiM','dokterpemeriksa2_id'),
                        'dokterPendamping'=>array(self::BELONGS_TO,'PegawaiM','dokterpendamping_id'),
                        'dokterAnastesi'=>array(self::BELONGS_TO,'PegawaiM','dokteranastesi_id'),
                        'dokterDelegasi'=>array(self::BELONGS_TO,'PegawaiM','dokterdelegasi_id'),
                        'bidan'=>array(self::BELONGS_TO,'PegawaiM','bidan_id'),
                        'suster'=>array(self::BELONGS_TO,'PegawaiM','suster_id'),
                        'perawat'=>array(self::BELONGS_TO,'PegawaiM','perawat_id'),
                        'tipePaket'=>array(self::BELONGS_TO,'TipepaketM','tipepaket_id'),
                        'tipepaket'=>array(self::BELONGS_TO,'TipepaketM','tipepaket_id'),
                        'pendaftaran'=>array(self::BELONGS_TO,'PendaftaranT','pendaftaran_id'),
                        'pasien'=>array(self::BELONGS_TO,'PasienM','pasien_id'),
                        'alatmedis'=>array(self::BELONGS_TO,'AlatmedisM','alatmedis_id'),
                        'pasienmasukpenunjang'=>array(self::BELONGS_TO,'PasienmasukpenunjangT','pasienmasukpenunjang_id'),
                        'rencanaoperasi'=>array(self::BELONGS_TO,'RencanaoperasiT','rencanaoperasi_id'),
                        'ruangan'=>array(self::BELONGS_TO,'RuanganM','ruangan_id'),
                        'detailhasilpemeriksaanlab'=>array(self::BELONGS_TO,'DetailhasilpemeriksaanlabT','detailhasilpemeriksaanlab_id'),
                        'hasilpemeriksaanrad'=>array(self::BELONGS_TO,'HasilpemeriksaanradT','hasilpemeriksaanrad_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tindakanpelayanan_id' => 'ID Tindakan Pelayanan',
			'penjamin_id' => 'Penjamin',
			'pasienadmisi_id' => 'Pasien Admisi',
			'pasien_id' => 'Pasien',
			'kelaspelayanan_id' => 'Kelas Pelayanan',
			'tipepaket_id' => 'Tipe Paket',
			'instalasi_id' => 'Instalasi',
			'pendaftaran_id' => 'Pendaftaran',
			'shift_id' => 'Shift',
			'pasienmasukpenunjang_id' => 'Pasien Masuk Penunjang',
			'daftartindakan_id' => 'Daftar Tindakan',
			'carabayar_id' => 'Cara Bayar',
			'jeniskasuspenyakit_id' => 'Kasus Penyakit',
			'tgl_tindakan' => 'Tanggal Tindakan',
			'tarif_tindakan' => 'Tarif',
			'satuantindakan' => 'Satuan',
			'qty_tindakan' => 'Qty',
			'cyto_tindakan' => 'Cyto',
			'tarifcyto_tindakan' => 'Tarifcyto',
			'dokterpemeriksa1_id' => 'Dokter Pemeriksa 1',
			'dokterpemeriksa2_id' => 'Dokter Pemeriksa 2',
			'dokterpendamping_id' => 'Dokter Pendamping',
			'dokteranastesi_id' => 'Dokter Anastesi',
			'dokterdelegasi_id' => 'Dokter Delegasi',
			'bidan_id' => 'Bidan',
			'suster_id' => 'Suster',
			'perawat_id' => 'Perawat',
			'kelastanggungan_id' => 'Kelas Tanggungan',
			'discount_tindakan' => 'Discount',
			'subsidiasuransi_tindakan' => 'Subsidi Asuransi',
			'subsidipemerintah_tindakan' => 'Subsidi Pemerintah',
			'subsisidirumahsakit_tindakan' => 'Subsisidi Rumah Sakit',
			'iurbiaya_tindakan' => 'Iur Biaya',
			'tm' => 'Tm',
                    
                        'jumlahTarif' => 'Jumlah Tarif',
                        'persenCyto' => 'Persen Cyto',
                        'kategoriTindakanNama' => 'Kategori Tindakan',
                        'ruangan_id'=>'Ruangan',
                    
                        'hasilpemeriksaanrm_id' => 'Hasil Pemeriksaan',
			'konsulpoli_id' => 'Konsulpoli',
			'hasilpemeriksaanrad_id' => 'Hasil Pemeriksaan Rad',
                        'detailhasilpemeriksaanlab_id' => 'Detail Hasil Pemeriksaan Lab',
                        'rencanaoperasi_id' => 'Rencana Operasi',
                        'hasilpemeriksaanpa_id' => 'Hasil Pemeriksaan Pa',
                        'tarif_satuan' => 'Tarif Satuan',
                    
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Login Pemakai',
			'update_loginpemakai_id' => 'Update Login Pemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('tipepaket_id',$this->tipepaket_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
		$criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
		$criteria->compare('tarif_tindakan',$this->tarif_tindakan);
		$criteria->compare('LOWER(satuantindakan)',strtolower($this->satuantindakan),true);
		$criteria->compare('LOWER(qty_tindakan)',strtolower($this->qty_tindakan),true);
		$criteria->compare('cyto_tindakan',$this->cyto_tindakan);
		$criteria->compare('tarifcyto_tindakan',$this->tarifcyto_tindakan);
		$criteria->compare('LOWER(dokterpemeriksa1_id)',strtolower($this->dokterpemeriksa1_id),true);
		$criteria->compare('LOWER(dokterpemeriksa2_id)',strtolower($this->dokterpemeriksa2_id),true);
		$criteria->compare('LOWER(dokterpendamping_id)',strtolower($this->dokterpendamping_id),true);
		$criteria->compare('LOWER(dokteranastesi_id)',strtolower($this->dokteranastesi_id),true);
		$criteria->compare('LOWER(dokterdelegasi_id)',strtolower($this->dokterdelegasi_id),true);
		$criteria->compare('LOWER(bidan_id)',strtolower($this->bidan_id),true);
		$criteria->compare('LOWER(suster_id)',strtolower($this->suster_id),true);
		$criteria->compare('LOWER(perawat_id)',strtolower($this->perawat_id),true);
		$criteria->compare('kelastanggungan_id',$this->kelastanggungan_id);
		$criteria->compare('discount_tindakan',$this->discount_tindakan);
		$criteria->compare('subsidiasuransi_tindakan',$this->subsidiasuransi_tindakan);
		$criteria->compare('subsidipemerintah_tindakan',$this->subsidipemerintah_tindakan);
		$criteria->compare('subsisidirumahsakit_tindakan',$this->subsisidirumahsakit_tindakan);
		$criteria->compare('iurbiaya_tindakan',$this->iurbiaya_tindakan);
		$criteria->compare('rencanaoperasi_id',$this->rencanaoperasi_id);
		$criteria->compare('LOWER(tm)',strtolower($this->tm),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('tipepaket_id',$this->tipepaket_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
		$criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
		$criteria->compare('tarif_tindakan',$this->tarif_tindakan);
		$criteria->compare('LOWER(satuantindakan)',strtolower($this->satuantindakan),true);
		$criteria->compare('LOWER(qty_tindakan)',strtolower($this->qty_tindakan),true);
		$criteria->compare('cyto_tindakan',$this->cyto_tindakan);
		$criteria->compare('tarifcyto_tindakan',$this->tarifcyto_tindakan);
		$criteria->compare('LOWER(dokterpemeriksa1_id)',strtolower($this->dokterpemeriksa1_id),true);
		$criteria->compare('LOWER(dokterpemeriksa2_id)',strtolower($this->dokterpemeriksa2_id),true);
		$criteria->compare('LOWER(dokterpendamping_id)',strtolower($this->dokterpendamping_id),true);
		$criteria->compare('LOWER(dokteranastesi_id)',strtolower($this->dokteranastesi_id),true);
		$criteria->compare('LOWER(dokterdelegasi_id)',strtolower($this->dokterdelegasi_id),true);
		$criteria->compare('LOWER(bidan_id)',strtolower($this->bidan_id),true);
		$criteria->compare('LOWER(suster_id)',strtolower($this->suster_id),true);
		$criteria->compare('LOWER(perawat_id)',strtolower($this->perawat_id),true);
		$criteria->compare('kelastanggungan_id',$this->kelastanggungan_id);
		$criteria->compare('discount_tindakan',$this->discount_tindakan);
		$criteria->compare('subsidiasuransi_tindakan',$this->subsidiasuransi_tindakan);
		$criteria->compare('subsidipemerintah_tindakan',$this->subsidipemerintah_tindakan);
		$criteria->compare('subsisidirumahsakit_tindakan',$this->subsisidirumahsakit_tindakan);
		$criteria->compare('iurbiaya_tindakan',$this->iurbiaya_tindakan);
		$criteria->compare('LOWER(tm)',strtolower($this->tm),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        protected function beforeSave() {
            if(trim($this->dokterpemeriksa1_id == "")) $this->setAttribute('dokterpemeriksa1_id', null);
            if(trim($this->dokterpemeriksa2_id == "")) $this->setAttribute('dokterpemeriksa2_id', null);
            if(trim($this->dokterpendamping_id == "")) $this->setAttribute('dokterpendamping_id', null);
            if(trim($this->dokteranastesi_id == "")) $this->setAttribute('dokteranastesi_id', null);
            if(trim($this->dokterdelegasi_id == "")) $this->setAttribute('dokterdelegasi_id', null);
            if(trim($this->bidan_id == "")) $this->setAttribute('bidan_id', null);
            if(trim($this->suster_id == "")) $this->setAttribute('suster_id', null);
            if(trim($this->perawat_id == "")) $this->setAttribute('perawat_id', null);
            
            
            return parent::beforeSave();
        }
            
                
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                        }
            }
            return true;
        }
        
        protected function beforeValidate ()
        {
            $format = new CustomFormat();
            foreach($this->metadata->tableSchema->columns as $columnName => $column)
            {
                if ($column->dbType == 'date')
                {
                        $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                }elseif ($column->dbType == 'timestamp without time zone')
                {
                        $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                }
            }
            return parent::beforeValidate();
        }        

        
        public function getTipePaketItems($carabayar_id = '')
        {
            if(!empty($carabayar_id))
            {
                $tipePaket = TipepaketM::model()->findAllByAttributes(
                    array(
                        'tipepaket_aktif' => true,
                        'carabayar_id' => $carabayar_id
                    )
                );
                
                if(empty($tipePaket))
                {
                    $tipePaket = TipepaketM::model()->findAllByPk((int)Params::TIPEPAKET_NONPAKET);
                }
                return $tipePaket;
            }else
                return TipepaketM::model()->findAllByAttributes(array('tipepaket_aktif'=>true));
        }
}