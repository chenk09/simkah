<?php

/**
 * This is the model class for table "inbox".
 *
 * The followings are the available columns in table 'inbox':
 * @property string $updatedindb
 * @property string $receivingdatetime
 * @property string $text
 * @property string $sendernumber
 * @property string $coding
 * @property string $udh
 * @property string $smscnumber
 * @property integer $class
 * @property string $textdecoded
 * @property integer $id
 * @property string $recipientid
 * @property boolean $processed
 */
class Inbox extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Inbox the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inbox';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('updatedindb, text, udh, class, recipientid', 'required'),
			array('class', 'numerical', 'integerOnly'=>true),
			array('sendernumber, smscnumber', 'length', 'max'=>20),
			array('coding', 'length', 'max'=>255),
			array('receivingdatetime, textdecoded, processed', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('updatedindb, receivingdatetime, text, sendernumber, coding, udh, smscnumber, class, textdecoded, id, recipientid, processed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'updatedindb' => 'Updatedindb',
			'receivingdatetime' => 'Waktu Diterima',
			'text' => 'Text',
			'sendernumber' => 'Nomor Pengirim',
			'coding' => 'Coding',
			'udh' => 'Udh',
			'smscnumber' => 'Nomor SMSC',
			'class' => 'Class',
			'textdecoded' => 'Isi Pesan Teks',
			'id' => 'ID',
			'recipientid' => 'Recipientid',
			'processed' => 'Processed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->order = 'receivingdatetime DESC';
		$criteria->compare('LOWER(updatedindb)',strtolower($this->updatedindb),true);
		$criteria->compare('LOWER(receivingdatetime)',strtolower($this->receivingdatetime),true);
		$criteria->compare('LOWER(text)',strtolower($this->text),true);
		$criteria->compare('LOWER(sendernumber)',strtolower($this->sendernumber),true);
		$criteria->compare('LOWER(coding)',strtolower($this->coding),true);
		$criteria->compare('LOWER(udh)',strtolower($this->udh),true);
		$criteria->compare('LOWER(smscnumber)',strtolower($this->smscnumber),true);
		$criteria->compare('class',$this->class);
		$criteria->compare('LOWER(textdecoded)',strtolower($this->textdecoded),true);
		$criteria->compare('id',$this->id);
		$criteria->compare('LOWER(recipientid)',strtolower($this->recipientid),true);
		$criteria->compare('processed',$this->processed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(updatedindb)',strtolower($this->updatedindb),true);
		$criteria->compare('LOWER(receivingdatetime)',strtolower($this->receivingdatetime),true);
		$criteria->compare('LOWER(text)',strtolower($this->text),true);
		$criteria->compare('LOWER(sendernumber)',strtolower($this->sendernumber),true);
		$criteria->compare('LOWER(coding)',strtolower($this->coding),true);
		$criteria->compare('LOWER(udh)',strtolower($this->udh),true);
		$criteria->compare('LOWER(smscnumber)',strtolower($this->smscnumber),true);
		$criteria->compare('class',$this->class);
		$criteria->compare('LOWER(textdecoded)',strtolower($this->textdecoded),true);
		$criteria->compare('id',$this->id);
		$criteria->compare('LOWER(recipientid)',strtolower($this->recipientid),true);
		$criteria->compare('processed',$this->processed);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                }elseif ($column->dbType == 'timestamp(0) without time zone'){
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                }
            }
            return true;
        }
        
}