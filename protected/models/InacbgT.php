<?php

/**
 * This is the model class for table "inacbg_t".
 *
 * The followings are the available columns in table 'inacbg_t':
 * @property integer $inacbg_id
 * @property integer $sep_id
 * @property integer $pendaftaran_id
 * @property integer $pasien_id
 * @property integer $jenisrawat_inacbg
 * @property integer $hak_kelasrawat_inacbg
 * @property boolean $is_naikkelas
 * @property boolean $is_rawatintesif
 * @property string $tglrawat_masuk
 * @property string $tglrawat_keluar
 * @property integer $adlscore_subaccute
 * @property integer $adlscore_chronic
 * @property string $naik_kelasrawat_inacbg
 * @property integer $lamarawat_naikkelas
 * @property integer $lamarawat_icu
 * @property integer $ventilator_icu
 * @property integer $total_lamarawat
 * @property integer $umur_pasien
 * @property integer $berat_lahir
 * @property string $nama_dpjp
 * @property string $cara_pulang
 * @property double $tarif_prosedur_nonbedah
 * @property double $tarif_prosedur_bedah
 * @property double $tarif_konsultasi
 * @property double $tarif_tenaga_ahli
 * @property double $tarif_keperawatan
 * @property double $tarif_penunjang
 * @property double $tarif_radiologi
 * @property double $tarif_laboratorium
 * @property double $tarif_pelayanan_darah
 * @property double $tarif_rehabilitasi
 * @property double $tarif_akomodasi
 * @property double $tarif_rawat_intensif
 * @property double $tarif_obat
 * @property double $tarif_alkes
 * @property double $tarif_bhp
 * @property double $tarif_sewa_alat
 * @property double $total_tarif_rs
 * @property integer $jaminan_id
 * @property string $jaminan_nama
 * @property string $cob_id
 * @property string $create_coder_nik
 * @property string $update_coder_nik
 * @property string $create_tanggal
 * @property string $update_tanggal
 * @property integer $create_loginpemakai_id
 * @property integer $update_loginpemakai_id
 * @property integer $create_ruangan_id
 * @property integer $update_ruangan_id
 *
 * The followings are the available model relations:
 * @property PendaftaranT $pendaftaran
 * @property SepT $sep
 * @property PasienM $pasien
 * @property DiagnosaxInacbgsT[] $diagnosaxInacbgsTs
 */
class InacbgT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InacbgT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inacbg_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sep_id, pendaftaran_id, jenisrawat_inacbg, hak_kelasrawat_inacbg, nama_dpjp, create_coder_nik, create_tanggal, create_loginpemakai_id, create_ruangan_id', 'required'),
			array('sep_id, pendaftaran_id, pasien_id, jenisrawat_inacbg, hak_kelasrawat_inacbg, adlscore_subaccute, adlscore_chronic, lamarawat_naikkelas, lamarawat_icu, ventilator_icu, total_lamarawat, umur_pasien, berat_lahir, jaminan_id, create_loginpemakai_id, update_loginpemakai_id, create_ruangan_id, update_ruangan_id', 'numerical', 'integerOnly'=>true),
			array('tarif_prosedur_nonbedah, tarif_prosedur_bedah, tarif_konsultasi, tarif_tenaga_ahli, tarif_keperawatan, tarif_penunjang, tarif_radiologi, tarif_laboratorium, tarif_pelayanan_darah, tarif_rehabilitasi, tarif_akomodasi, tarif_rawat_intensif, tarif_obat, tarif_alkes, tarif_bhp, tarif_sewa_alat, total_tarif_rs', 'numerical'),
			array('naik_kelasrawat_inacbg, cob_id', 'length', 'max'=>10),
			array('nama_dpjp, cara_pulang', 'length', 'max'=>100),
			array('jaminan_nama, create_coder_nik, update_coder_nik', 'length', 'max'=>50),
			array('is_naikkelas, is_rawatintesif, tglrawat_masuk, tglrawat_keluar, update_tanggal', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('inacbg_id, sep_id, pendaftaran_id, pasien_id, jenisrawat_inacbg, hak_kelasrawat_inacbg, is_naikkelas, is_rawatintesif, tglrawat_masuk, tglrawat_keluar, adlscore_subaccute, adlscore_chronic, naik_kelasrawat_inacbg, lamarawat_naikkelas, lamarawat_icu, ventilator_icu, total_lamarawat, umur_pasien, berat_lahir, nama_dpjp, cara_pulang, tarif_prosedur_nonbedah, tarif_prosedur_bedah, tarif_konsultasi, tarif_tenaga_ahli, tarif_keperawatan, tarif_penunjang, tarif_radiologi, tarif_laboratorium, tarif_pelayanan_darah, tarif_rehabilitasi, tarif_akomodasi, tarif_rawat_intensif, tarif_obat, tarif_alkes, tarif_bhp, tarif_sewa_alat, total_tarif_rs, jaminan_id, jaminan_nama, cob_id, create_coder_nik, update_coder_nik, create_tanggal, update_tanggal, create_loginpemakai_id, update_loginpemakai_id, create_ruangan_id, update_ruangan_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pendaftaran' => array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
			'sep' => array(self::BELONGS_TO, 'SepT', 'sep_id'),
			'pasien' => array(self::BELONGS_TO, 'PasienM', 'pasien_id'),
			'diagnosaxInacbgsTs' => array(self::HAS_MANY, 'DiagnosaxInacbgsT', 'inacbg_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'inacbg_id' => 'Inacbg',
			'sep_id' => 'Sep',
			'pendaftaran_id' => 'Pendaftaran',
			'pasien_id' => 'Pasien',
			'jenisrawat_inacbg' => 'Jenisrawat Inacbg',
			'hak_kelasrawat_inacbg' => 'Hak Kelasrawat Inacbg',
			'is_naikkelas' => 'Is Naikkelas',
			'is_rawatintesif' => 'Is Rawatintesif',
			'tglrawat_masuk' => 'Tglrawat Masuk',
			'tglrawat_keluar' => 'Tglrawat Keluar',
			'adlscore_subaccute' => 'Adlscore Subaccute',
			'adlscore_chronic' => 'Adlscore Chronic',
			'naik_kelasrawat_inacbg' => 'Naik Kelasrawat Inacbg',
			'lamarawat_naikkelas' => 'Lamarawat Naikkelas',
			'lamarawat_icu' => 'Lamarawat Icu',
			'ventilator_icu' => 'Ventilator Icu',
			'total_lamarawat' => 'Total Lamarawat',
			'umur_pasien' => 'Umur Pasien',
			'berat_lahir' => 'Berat Lahir',
			'nama_dpjp' => 'Nama Dpjp',
			'cara_pulang' => 'Cara Pulang',
			'tarif_prosedur_nonbedah' => 'Tarif Prosedur Nonbedah',
			'tarif_prosedur_bedah' => 'Tarif Prosedur Bedah',
			'tarif_konsultasi' => 'Tarif Konsultasi',
			'tarif_tenaga_ahli' => 'Tarif Tenaga Ahli',
			'tarif_keperawatan' => 'Tarif Keperawatan',
			'tarif_penunjang' => 'Tarif Penunjang',
			'tarif_radiologi' => 'Tarif Radiologi',
			'tarif_laboratorium' => 'Tarif Laboratorium',
			'tarif_pelayanan_darah' => 'Tarif Pelayanan Darah',
			'tarif_rehabilitasi' => 'Tarif Rehabilitasi',
			'tarif_akomodasi' => 'Tarif Akomodasi',
			'tarif_rawat_intensif' => 'Tarif Rawat Intensif',
			'tarif_obat' => 'Tarif Obat',
			'tarif_alkes' => 'Tarif Alkes',
			'tarif_bhp' => 'Tarif Bhp',
			'tarif_sewa_alat' => 'Tarif Sewa Alat',
			'total_tarif_rs' => 'Total Tarif Rs',
			'jaminan_id' => 'Jaminan',
			'jaminan_nama' => 'Jaminan Nama',
			'cob_id' => 'Cob',
			'create_coder_nik' => 'Create Coder Nik',
			'update_coder_nik' => 'Update Coder Nik',
			'create_tanggal' => 'Create Tanggal',
			'update_tanggal' => 'Update Tanggal',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan_id' => 'Create Ruangan',
			'update_ruangan_id' => 'Update Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		if(!empty($this->inacbg_id)){
			$criteria->addCondition('inacbg_id = '.$this->inacbg_id);
		}
		if(!empty($this->sep_id)){
			$criteria->addCondition('sep_id = '.$this->sep_id);
		}
		if(!empty($this->pendaftaran_id)){
			$criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
		}
		if(!empty($this->pasien_id)){
			$criteria->addCondition('pasien_id = '.$this->pasien_id);
		}
		if(!empty($this->jenisrawat_inacbg)){
			$criteria->addCondition('jenisrawat_inacbg = '.$this->jenisrawat_inacbg);
		}
		if(!empty($this->hak_kelasrawat_inacbg)){
			$criteria->addCondition('hak_kelasrawat_inacbg = '.$this->hak_kelasrawat_inacbg);
		}
		$criteria->compare('is_naikkelas',$this->is_naikkelas);
		$criteria->compare('is_rawatintesif',$this->is_rawatintesif);
		$criteria->compare('LOWER(tglrawat_masuk)',strtolower($this->tglrawat_masuk),true);
		$criteria->compare('LOWER(tglrawat_keluar)',strtolower($this->tglrawat_keluar),true);
		if(!empty($this->adlscore_subaccute)){
			$criteria->addCondition('adlscore_subaccute = '.$this->adlscore_subaccute);
		}
		if(!empty($this->adlscore_chronic)){
			$criteria->addCondition('adlscore_chronic = '.$this->adlscore_chronic);
		}
		$criteria->compare('LOWER(naik_kelasrawat_inacbg)',strtolower($this->naik_kelasrawat_inacbg),true);
		if(!empty($this->lamarawat_naikkelas)){
			$criteria->addCondition('lamarawat_naikkelas = '.$this->lamarawat_naikkelas);
		}
		if(!empty($this->lamarawat_icu)){
			$criteria->addCondition('lamarawat_icu = '.$this->lamarawat_icu);
		}
		if(!empty($this->ventilator_icu)){
			$criteria->addCondition('ventilator_icu = '.$this->ventilator_icu);
		}
		if(!empty($this->total_lamarawat)){
			$criteria->addCondition('total_lamarawat = '.$this->total_lamarawat);
		}
		if(!empty($this->umur_pasien)){
			$criteria->addCondition('umur_pasien = '.$this->umur_pasien);
		}
		if(!empty($this->berat_lahir)){
			$criteria->addCondition('berat_lahir = '.$this->berat_lahir);
		}
		$criteria->compare('LOWER(nama_dpjp)',strtolower($this->nama_dpjp),true);
		$criteria->compare('LOWER(cara_pulang)',strtolower($this->cara_pulang),true);
		$criteria->compare('tarif_prosedur_nonbedah',$this->tarif_prosedur_nonbedah);
		$criteria->compare('tarif_prosedur_bedah',$this->tarif_prosedur_bedah);
		$criteria->compare('tarif_konsultasi',$this->tarif_konsultasi);
		$criteria->compare('tarif_tenaga_ahli',$this->tarif_tenaga_ahli);
		$criteria->compare('tarif_keperawatan',$this->tarif_keperawatan);
		$criteria->compare('tarif_penunjang',$this->tarif_penunjang);
		$criteria->compare('tarif_radiologi',$this->tarif_radiologi);
		$criteria->compare('tarif_laboratorium',$this->tarif_laboratorium);
		$criteria->compare('tarif_pelayanan_darah',$this->tarif_pelayanan_darah);
		$criteria->compare('tarif_rehabilitasi',$this->tarif_rehabilitasi);
		$criteria->compare('tarif_akomodasi',$this->tarif_akomodasi);
		$criteria->compare('tarif_rawat_intensif',$this->tarif_rawat_intensif);
		$criteria->compare('tarif_obat',$this->tarif_obat);
		$criteria->compare('tarif_alkes',$this->tarif_alkes);
		$criteria->compare('tarif_bhp',$this->tarif_bhp);
		$criteria->compare('tarif_sewa_alat',$this->tarif_sewa_alat);
		$criteria->compare('total_tarif_rs',$this->total_tarif_rs);
		if(!empty($this->jaminan_id)){
			$criteria->addCondition('jaminan_id = '.$this->jaminan_id);
		}
		$criteria->compare('LOWER(jaminan_nama)',strtolower($this->jaminan_nama),true);
		$criteria->compare('LOWER(cob_id)',strtolower($this->cob_id),true);
		$criteria->compare('LOWER(create_coder_nik)',strtolower($this->create_coder_nik),true);
		$criteria->compare('LOWER(update_coder_nik)',strtolower($this->update_coder_nik),true);
		$criteria->compare('LOWER(create_tanggal)',strtolower($this->create_tanggal),true);
		$criteria->compare('LOWER(update_tanggal)',strtolower($this->update_tanggal),true);
		if(!empty($this->create_loginpemakai_id)){
			$criteria->addCondition('create_loginpemakai_id = '.$this->create_loginpemakai_id);
		}
		if(!empty($this->update_loginpemakai_id)){
			$criteria->addCondition('update_loginpemakai_id = '.$this->update_loginpemakai_id);
		}
		if(!empty($this->create_ruangan_id)){
			$criteria->addCondition('create_ruangan_id = '.$this->create_ruangan_id);
		}
		if(!empty($this->update_ruangan_id)){
			$criteria->addCondition('update_ruangan_id = '.$this->update_ruangan_id);
		}

		return $criteria;
	}
        
        
        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=10;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }


        public function searchPrint()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
}