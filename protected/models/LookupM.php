<?php

/**
 * This is the model class for table "lookup_m".
 *
 * The followings are the available columns in table 'lookup_m':
 * @property string $lookup_id
 * @property string $lookup_name
 * @property string $lookup_value
 * @property string $lookup_type
 * @property string $lookup_urutan
 * @property string $lookup_aktif
 */
class LookupM extends CActiveRecord
{
	private static $_items=array();
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LookupM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lookup_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('lookup_urutan', 'numerical', 'integerOnly'=>true),
			array('lookup_id, lookup_name, lookup_value, lookup_type, lookup_urutan, lookup_kode, lookup_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('lookup_id, lookup_name, lookup_value, lookup_type, lookup_urutan, lookup_kode, lookup_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	 public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'lookup' => array(self::BELONGS_TO, 'LookupM', 'lookup_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lookup_id' => 'ID',
			'lookup_name' => 'Nama',
			'lookup_value' => 'Value',
			'lookup_type' => 'Tipe',
			'lookup_urutan' => 'Urutan',
                        'lookup_kode' => 'Kode',
			'lookup_aktif' => 'Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lookup_id',$this->lookup_id);
		$criteria->compare('LOWER(lookup_name)',strtolower($this->lookup_name),true);
		$criteria->compare('LOWER(lookup_value)',strtolower($this->lookup_value),true);
		$criteria->compare('LOWER(lookup_type)',strtolower($this->lookup_type),true);
		$criteria->compare('lookup_urutan',$this->lookup_urutan);
		$criteria->compare('LOWER(lookup_kode)',strtolower($this->lookup_kode),true);
		$criteria->compare('lookup_aktif',isset($this->lookup_aktif)?$this->lookup_aktif:true);
                $criteria->order = 'lookup_type, lookup_urutan';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchCaraPembayaran()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lookup_id',$this->lookup_id);
		$criteria->compare('LOWER(lookup_name)',strtolower($this->lookup_name),true);
		$criteria->compare('LOWER(lookup_value)',strtolower($this->lookup_value),true);
		$criteria->compare('LOWER(lookup_type)',strtolower($this->lookup_type),true);
		$criteria->compare('lookup_urutan',$this->lookup_urutan);
		$criteria->compare('LOWER(lookup_kode)',strtolower($this->lookup_kode),true);
		$criteria->compare('lookup_aktif',isset($this->lookup_aktif)?$this->lookup_aktif:true);
		$criteria->addCondition("lookup_type='carapembayaran' ");
                $criteria->order = 'lookup_type, lookup_urutan';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
        $criteria->compare('lookup_id',$this->lookup_id);
        $criteria->compare('LOWER(lookup_type)',strtolower($this->lookup_type),true);
        $criteria->compare('LOWER(lookup_name)',strtolower($this->lookup_name),true);
        $criteria->compare('LOWER(lookup_value)',strtolower($this->lookup_value),true);
        $criteria->compare('lookup_urutan',$this->lookup_urutan);
        $criteria->compare('LOWER(lookup_kode)',strtolower($this->lookup_kode),true);
		$criteria->compare('lookup_aktif',isset($this->lookup_aktif)?$this->lookup_aktif:true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function beforeSave() {
        	//DI COMMENT KARENA DI DATABASE ADA YANG KAPITAL 06 NOVEMBER 2013
            // $this->lookup_name = ucwords(strtolower($this->lookup_name));
            // $this->lookup_kode = ucwords(strtoupper($this->lookup_kode));
            // switch ($this->lookup_type) {
            //     case 'namadepan'            : $this->lookup_value = ucwords(strtolower($this->lookup_value));break;
            //     case 'jenispemeriksaanrad'  : $this->lookup_value = ucwords(strtolower($this->lookup_value));break;

            //     default:$this->lookup_value = strtoupper($this->lookup_value);break;
            // }

            //COMMENT SELESAI

        	$this->lookup_name = $this->lookup_name;
            $this->lookup_kode = $this->lookup_kode;
            switch ($this->lookup_type) {
                case 'namadepan'            : $this->lookup_value = $this->lookup_value;break;
                case 'jenispemeriksaanrad'  : $this->lookup_value = $this->lookup_value;break;

                default:$this->lookup_value = strtoupper($this->lookup_value);break;
            }
//            if($this->lookup_type === 'NamaDepan')
//                $this->lookup_value = ucwords(strtoupper($this->lookup_value));
//            else
//                $this->lookup_value = strtoupper($this->lookup_value);
            return parent::beforeSave();
        }
        
        /**
	 * Returns the items for the specified type.
	 * @param string item type (e.g. 'PostStatus').
	 * @return array item names indexed by item code. The items are order by their lookup_urutan values.
	 * An empty array is returned if the item type does not exist.
	 */
	public static function items($type)
	{
		if(!isset(self::$_items[$type]))
			self::loadItems($type);
		return self::$_items[$type];
	}

	/**
	 * Returns the item name for the specified type and code.
	 * @param string the item type (e.g. 'PostStatus').
	 * @param integer the item code (corresponding to the 'code' column value)
	 * @return string the item name for the specified the code. False is returned if the item type or code does not exist.
	 */
	public static function item($type,$code)
	{
		if(!isset(self::$_items[$type]))
			self::loadItems($type);
		return isset(self::$_items[$type][$code]) ? self::$_items[$type][$code] : false;
	}

	/**
	 * Loads the lookup items for the specified type from the database.
	 * @param string the item type
	 */
	private static function loadItems($type)
	{
		self::$_items[$type]=array();
		$models=self::model()->findAll(array(
			'condition'=>'lookup_type=:type and lookup_aktif is true',
			'params'=>array(':type'=>$type),
			'order'=>'lookup_urutan',
		));
		foreach($models as $model)
			self::$_items[$type][$model->lookup_value]=$model->lookup_name;
	}
        
        public static function getAllType()
        {
            $criteria = new CDbCriteria;
            $criteria->select = 'lookup_type, lookup_name, lookup_value';
            $criteria->group = 'lookup_type, lookup_name, lookup_value';
            $criteria->order = 'lookup_type';
            return self::model()->findAll($criteria);
        }
}