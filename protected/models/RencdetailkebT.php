<?php

/**
 * This is the model class for table "rencdetailkeb_t".
 *
 * The followings are the available columns in table 'rencdetailkeb_t':
 * @property integer $rencdetailkeb_id
 * @property integer $obatalkes_id
 * @property integer $rencanakebfarmasi_id
 * @property integer $sumberdana_id
 * @property integer $satuankecil_id
 * @property integer $satuanbesar_id
 * @property double $harganettorenc
 * @property double $hargappnrenc
 * @property double $hargapphrenc
 * @property double $hargatotalrenc
 * @property double $stokakhir
 * @property integer $maksimalstok
 * @property integer $minimalstok
 * @property double $jmlpermintaan
 * @property string $tglkadaluarsa
 * @property integer $jmlkemasan
 */
class RencdetailkebT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RencdetailkebT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rencdetailkeb_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('obatalkes_id, rencanakebfarmasi_id, harganettorenc, hargappnrenc, hargapphrenc, hargatotalrenc, jmlpermintaan', 'required'),
			array('obatalkes_id, rencanakebfarmasi_id, sumberdana_id, satuankecil_id, satuanbesar_id, maksimalstok, minimalstok, jmlkemasan', 'numerical', 'integerOnly'=>true),
			array('harganettorenc, hargappnrenc, hargapphrenc, hargatotalrenc, stokakhir, jmlpermintaan', 'numerical'),
			array('tglkadaluarsa', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('rencdetailkeb_id, obatalkes_id, rencanakebfarmasi_id, sumberdana_id, satuankecil_id, satuanbesar_id, harganettorenc, hargappnrenc, hargapphrenc, hargatotalrenc, stokakhir, maksimalstok, minimalstok, jmlpermintaan, tglkadaluarsa, jmlkemasan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'satuanbesar'=>array(self::BELONGS_TO, 'SatuanbesarM','satuanbesar_id'),
                    'sumberdana'=>array(self::BELONGS_TO, 'SumberdanaM','sumberdana_id'),
                    'satuankecil'=>array(self::BELONGS_TO, 'SatuankecilM','satuankecil_id'),
                    'obatalkes'=>array(self::BELONGS_TO, 'ObatalkesM','obatalkes_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rencdetailkeb_id' => 'Rencdetailkeb',
			'obatalkes_id' => 'Obatalkes',
			'rencanakebfarmasi_id' => 'Rencanakebfarmasi',
			'sumberdana_id' => 'Sumberdana',
			'satuankecil_id' => 'Satuankecil',
			'satuanbesar_id' => 'Satuanbesar',
			'harganettorenc' => 'Harganettorenc',
			'hargappnrenc' => 'Hargappnrenc',
			'hargapphrenc' => 'Hargapphrenc',
			'hargatotalrenc' => 'Hargatotalrenc',
			'stokakhir' => 'Stokakhir',
			'maksimalstok' => 'Maksimalstok',
			'minimalstok' => 'Minimalstok',
			'jmlpermintaan' => 'Jmlpermintaan',
			'tglkadaluarsa' => 'Tglkadaluarsa',
			'jmlkemasan' => 'Jmlkemasan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rencdetailkeb_id',$this->rencdetailkeb_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('rencanakebfarmasi_id',$this->rencanakebfarmasi_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('harganettorenc',$this->harganettorenc);
		$criteria->compare('hargappnrenc',$this->hargappnrenc);
		$criteria->compare('hargapphrenc',$this->hargapphrenc);
		$criteria->compare('hargatotalrenc',$this->hargatotalrenc);
		$criteria->compare('stokakhir',$this->stokakhir);
		$criteria->compare('maksimalstok',$this->maksimalstok);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('jmlpermintaan',$this->jmlpermintaan);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('rencdetailkeb_id',$this->rencdetailkeb_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('rencanakebfarmasi_id',$this->rencanakebfarmasi_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('harganettorenc',$this->harganettorenc);
		$criteria->compare('hargappnrenc',$this->hargappnrenc);
		$criteria->compare('hargapphrenc',$this->hargapphrenc);
		$criteria->compare('hargatotalrenc',$this->hargatotalrenc);
		$criteria->compare('stokakhir',$this->stokakhir);
		$criteria->compare('maksimalstok',$this->maksimalstok);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('jmlpermintaan',$this->jmlpermintaan);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}