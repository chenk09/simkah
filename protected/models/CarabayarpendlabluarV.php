<?php

/**
 * This is the model class for table "carabayarpendlabluar_v".
 *
 * The followings are the available columns in table 'carabayarpendlabluar_v':
 * @property integer $carabayar_id
 * @property string $carabayar_nama
 * @property string $carabayar_namalainnya
 * @property string $metode_pembayaran
 * @property string $carabayar_loket
 * @property string $carabayar_singkatan
 * @property integer $carabayar_nourut
 * @property boolean $issubsidiasuransi
 * @property boolean $issubsidipemerintah
 * @property boolean $issubsidirs
 * @property boolean $carabayar_aktif
 */
class CarabayarpendlabluarV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CarabayarpendlabluarV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'carabayarpendlabluar_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('carabayar_id, carabayar_nourut', 'numerical', 'integerOnly'=>true),
			array('carabayar_nama, carabayar_namalainnya, metode_pembayaran, carabayar_loket', 'length', 'max'=>50),
			array('carabayar_singkatan', 'length', 'max'=>1),
			array('issubsidiasuransi, issubsidipemerintah, issubsidirs, carabayar_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('carabayar_id, carabayar_nama, carabayar_namalainnya, metode_pembayaran, carabayar_loket, carabayar_singkatan, carabayar_nourut, issubsidiasuransi, issubsidipemerintah, issubsidirs, carabayar_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'carabayar_id' => 'Carabayar',
			'carabayar_nama' => 'Carabayar Nama',
			'carabayar_namalainnya' => 'Carabayar Namalainnya',
			'metode_pembayaran' => 'Metode Pembayaran',
			'carabayar_loket' => 'Carabayar Loket',
			'carabayar_singkatan' => 'Carabayar Singkatan',
			'carabayar_nourut' => 'Carabayar Nourut',
			'issubsidiasuransi' => 'Issubsidiasuransi',
			'issubsidipemerintah' => 'Issubsidipemerintah',
			'issubsidirs' => 'Issubsidirs',
			'carabayar_aktif' => 'Carabayar Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('carabayar_nama',$this->carabayar_nama,true);
		$criteria->compare('carabayar_namalainnya',$this->carabayar_namalainnya,true);
		$criteria->compare('metode_pembayaran',$this->metode_pembayaran,true);
		$criteria->compare('carabayar_loket',$this->carabayar_loket,true);
		$criteria->compare('carabayar_singkatan',$this->carabayar_singkatan,true);
		$criteria->compare('carabayar_nourut',$this->carabayar_nourut);
		$criteria->compare('issubsidiasuransi',$this->issubsidiasuransi);
		$criteria->compare('issubsidipemerintah',$this->issubsidipemerintah);
		$criteria->compare('issubsidirs',$this->issubsidirs);
		$criteria->compare('carabayar_aktif',$this->carabayar_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}