<?php

/**
 * This is the model class for table "laporantransferbri_v".
 *
 * The followings are the available columns in table 'laporantransferbri_v':
 * @property integer $pegawai_id
 * @property string $nomorindukpegawai
 * @property string $no_kartupegawainegerisipil
 * @property string $no_karis_karsu
 * @property string $no_taspen
 * @property string $no_askes
 * @property string $jenisidentitas
 * @property string $noidentitas
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property string $nama_keluarga
 * @property string $gelarbelakang_nama
 * @property string $tempatlahir_pegawai
 * @property string $tgl_lahirpegawai
 * @property string $jeniskelamin
 * @property string $statusperkawinan
 * @property string $alamat_pegawai
 * @property integer $kelurahan_id
 * @property string $kelurahan_nama
 * @property string $kode_pos
 * @property integer $kecamatan_id
 * @property string $kecamatan_nama
 * @property integer $kabupaten_id
 * @property string $kabupaten_nama
 * @property string $agama
 * @property string $golongandarah
 * @property string $rhesus
 * @property string $alamatemail
 * @property string $notelp_pegawai
 * @property string $nomobile_pegawai
 * @property string $warganegara_pegawai
 * @property string $jeniswaktukerja
 * @property integer $suku_id
 * @property string $suku_nama
 * @property integer $statuskepemilikanrumah_id
 * @property string $statuskepemilikanrumah_nama
 * @property integer $propinsi_id
 * @property string $propinsi_nama
 * @property integer $profilrs_id
 * @property string $nokode_rumahsakit
 * @property string $nama_rumahsakit
 * @property string $kelompokjabatan
 * @property integer $golonganpegawai_id
 * @property string $golonganpegawai_nama
 * @property integer $pangkat_id
 * @property string $pangkat_nama
 * @property integer $jabatan_id
 * @property string $jabatan_nama
 * @property integer $esselon_id
 * @property string $esselon_nama
 * @property integer $kelompokpegawai_id
 * @property string $kelompokpegawai_nama
 * @property string $kategoripegawai
 * @property string $kategoripegawaiasal
 * @property string $photopegawai
 * @property string $nofingerprint
 * @property double $tinggibadan
 * @property double $beratbadan
 * @property string $kemampuanbahasa
 * @property string $warnakulit
 * @property string $nip_lama
 * @property string $kode_bank
 * @property string $no_rekening
 * @property string $npwp
 * @property string $tglditerima
 * @property string $tglberhenti
 * @property string $suratizinpraktek
 * @property integer $penggajianpeg_id
 * @property string $tglpenggajian
 * @property string $nopenggajian
 * @property string $keterangan
 * @property string $mengetahui
 * @property string $menyetujui
 * @property double $totalterima
 * @property double $totalpajak
 * @property double $totalpotongan
 * @property double $nominalgaji
 * @property string $periodegaji
 * @property integer $pengeluaranumum_id
 * @property integer $jenispengeluaran_id
 * @property string $jenispengeluaran_kode
 * @property string $jenispengeluaran_nama
 * @property integer $tandabuktikeluar_id
 * @property string $tahun
 * @property string $tglkaskeluar
 * @property string $nokaskeluar
 * @property string $carabayarkeluar
 * @property string $melaluibank
 * @property string $denganrekening
 * @property string $atasnamarekening
 * @property string $namapenerima
 * @property string $alamatpenerima
 * @property string $untukpembayaran
 * @property string $kelompoktransaksi
 * @property string $nopengeluaran
 * @property string $tglpengeluaran
 * @property string $keterangankeluar
 * @property boolean $isurainkeluarumum
 * @property double $totalharga
 * @property double $biayaadministrasi
 * @property double $jmlkaskeluar
 * @property string $uraiantransaksi
 */
class LaporantransferbriV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporantransferbriV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporantransferbri_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pegawai_id, kelurahan_id, kecamatan_id, kabupaten_id, suku_id, statuskepemilikanrumah_id, propinsi_id, profilrs_id, golonganpegawai_id, pangkat_id, jabatan_id, esselon_id, kelompokpegawai_id, penggajianpeg_id, pengeluaranumum_id, jenispengeluaran_id, tandabuktikeluar_id', 'numerical', 'integerOnly'=>true),
			array('tinggibadan, beratbadan, totalterima, totalpajak, totalpotongan, nominalgaji, totalharga, biayaadministrasi, jmlkaskeluar', 'numerical'),
			array('nomorindukpegawai, no_kartupegawainegerisipil, no_karis_karsu, no_taspen, no_askes, tempatlahir_pegawai, kelompokjabatan, kelompokpegawai_nama', 'length', 'max'=>30),
			array('jenisidentitas, jeniskelamin, statusperkawinan, agama, rhesus, jeniswaktukerja, nofingerprint, jenispengeluaran_kode', 'length', 'max'=>20),
			array('noidentitas, alamatemail, nama_rumahsakit, jabatan_nama, kemampuanbahasa, nip_lama, kode_bank, no_rekening, suratizinpraktek, mengetahui, menyetujui, jenispengeluaran_nama, melaluibank, denganrekening, atasnamarekening, namapenerima, untukpembayaran', 'length', 'max'=>100),
			array('gelardepan, statuskepemilikanrumah_nama, nokode_rumahsakit', 'length', 'max'=>10),
			array('nama_pegawai, nama_keluarga, kelurahan_nama, kecamatan_nama, kabupaten_nama, notelp_pegawai, nomobile_pegawai, suku_nama, propinsi_nama, golonganpegawai_nama, pangkat_nama, kategoripegawaiasal, nopenggajian, nokaskeluar, carabayarkeluar, kelompoktransaksi, nopengeluaran', 'length', 'max'=>50),
			array('gelarbelakang_nama, kode_pos, esselon_nama', 'length', 'max'=>15),
			array('golongandarah', 'length', 'max'=>2),
			array('warganegara_pegawai, npwp', 'length', 'max'=>25),
			array('kategoripegawai', 'length', 'max'=>128),
			array('photopegawai', 'length', 'max'=>200),
			array('tahun', 'length', 'max'=>4),
			array('tgl_lahirpegawai, alamat_pegawai, warnakulit, tglditerima, tglberhenti, tglpenggajian, keterangan, periodegaji, tglkaskeluar, alamatpenerima, tglpengeluaran, keterangankeluar, isurainkeluarumum, uraiantransaksi', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pegawai_id, nomorindukpegawai, no_kartupegawainegerisipil, no_karis_karsu, no_taspen, no_askes, jenisidentitas, noidentitas, gelardepan, nama_pegawai, nama_keluarga, gelarbelakang_nama, tempatlahir_pegawai, tgl_lahirpegawai, jeniskelamin, statusperkawinan, alamat_pegawai, kelurahan_id, kelurahan_nama, kode_pos, kecamatan_id, kecamatan_nama, kabupaten_id, kabupaten_nama, agama, golongandarah, rhesus, alamatemail, notelp_pegawai, nomobile_pegawai, warganegara_pegawai, jeniswaktukerja, suku_id, suku_nama, statuskepemilikanrumah_id, statuskepemilikanrumah_nama, propinsi_id, propinsi_nama, profilrs_id, nokode_rumahsakit, nama_rumahsakit, kelompokjabatan, golonganpegawai_id, golonganpegawai_nama, pangkat_id, pangkat_nama, jabatan_id, jabatan_nama, esselon_id, esselon_nama, kelompokpegawai_id, kelompokpegawai_nama, kategoripegawai, kategoripegawaiasal, photopegawai, nofingerprint, tinggibadan, beratbadan, kemampuanbahasa, warnakulit, nip_lama, kode_bank, no_rekening, npwp, tglditerima, tglberhenti, suratizinpraktek, penggajianpeg_id, tglpenggajian, nopenggajian, keterangan, mengetahui, menyetujui, totalterima, totalpajak, totalpotongan, nominalgaji, periodegaji, pengeluaranumum_id, jenispengeluaran_id, jenispengeluaran_kode, jenispengeluaran_nama, tandabuktikeluar_id, tahun, tglkaskeluar, nokaskeluar, carabayarkeluar, melaluibank, denganrekening, atasnamarekening, namapenerima, alamatpenerima, untukpembayaran, kelompoktransaksi, nopengeluaran, tglpengeluaran, keterangankeluar, isurainkeluarumum, totalharga, biayaadministrasi, jmlkaskeluar, uraiantransaksi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pegawai_id' => 'Pegawai',
			'nomorindukpegawai' => 'Nomorindukpegawai',
			'no_kartupegawainegerisipil' => 'No Kartupegawainegerisipil',
			'no_karis_karsu' => 'No Karis Karsu',
			'no_taspen' => 'No Taspen',
			'no_askes' => 'No Askes',
			'jenisidentitas' => 'Jenisidentitas',
			'noidentitas' => 'Noidentitas',
			'gelardepan' => 'Gelardepan',
			'nama_pegawai' => 'Nama',
			'nama_keluarga' => 'Nama Keluarga',
			'gelarbelakang_nama' => 'Gelarbelakang Nama',
			'tempatlahir_pegawai' => 'Tempatlahir Pegawai',
			'tgl_lahirpegawai' => 'Tgl Lahirpegawai',
			'jeniskelamin' => 'Jeniskelamin',
			'statusperkawinan' => 'Statusperkawinan',
			'alamat_pegawai' => 'Alamat Pegawai',
			'kelurahan_id' => 'Kelurahan',
			'kelurahan_nama' => 'Kelurahan Nama',
			'kode_pos' => 'Kode Pos',
			'kecamatan_id' => 'Kecamatan',
			'kecamatan_nama' => 'Kecamatan Nama',
			'kabupaten_id' => 'Kabupaten',
			'kabupaten_nama' => 'Kabupaten Nama',
			'agama' => 'Agama',
			'golongandarah' => 'Golongandarah',
			'rhesus' => 'Rhesus',
			'alamatemail' => 'Alamatemail',
			'notelp_pegawai' => 'Notelp Pegawai',
			'nomobile_pegawai' => 'Nomobile Pegawai',
			'warganegara_pegawai' => 'Warganegara Pegawai',
			'jeniswaktukerja' => 'Jeniswaktukerja',
			'suku_id' => 'Suku',
			'suku_nama' => 'Suku Nama',
			'statuskepemilikanrumah_id' => 'Statuskepemilikanrumah',
			'statuskepemilikanrumah_nama' => 'Statuskepemilikanrumah Nama',
			'propinsi_id' => 'Propinsi',
			'propinsi_nama' => 'Propinsi Nama',
			'profilrs_id' => 'Profilrs',
			'nokode_rumahsakit' => 'Nokode Rumahsakit',
			'nama_rumahsakit' => 'Nama Rumahsakit',
			'kelompokjabatan' => 'Kelompokjabatan',
			'golonganpegawai_id' => 'Golonganpegawai',
			'golonganpegawai_nama' => 'Golonganpegawai Nama',
			'pangkat_id' => 'Pangkat',
			'pangkat_nama' => 'Pangkat Nama',
			'jabatan_id' => 'Jabatan',
			'jabatan_nama' => 'Jabatan Nama',
			'esselon_id' => 'Esselon',
			'esselon_nama' => 'Esselon Nama',
			'kelompokpegawai_id' => 'Kelompokpegawai',
			'kelompokpegawai_nama' => 'Kelompokpegawai Nama',
			'kategoripegawai' => 'Kategoripegawai',
			'kategoripegawaiasal' => 'Kategoripegawaiasal',
			'photopegawai' => 'Photopegawai',
			'nofingerprint' => 'Nofingerprint',
			'tinggibadan' => 'Tinggibadan',
			'beratbadan' => 'Beratbadan',
			'kemampuanbahasa' => 'Kemampuanbahasa',
			'warnakulit' => 'Warnakulit',
			'nip_lama' => 'Nip Lama',
			'kode_bank' => 'Kode Bank',
			'no_rekening' => 'No Rekening',
			'npwp' => 'Npwp',
			'tglditerima' => 'Tglditerima',
			'tglberhenti' => 'Tglberhenti',
			'suratizinpraktek' => 'Suratizinpraktek',
			'penggajianpeg_id' => 'Penggajianpeg',
			'tglpenggajian' => 'Tglpenggajian',
			'nopenggajian' => 'Nopenggajian',
			'keterangan' => 'Keterangan',
			'mengetahui' => 'Mengetahui',
			'menyetujui' => 'Menyetujui',
			'totalterima' => 'Totalterima',
			'totalpajak' => 'Totalpajak',
			'totalpotongan' => 'Totalpotongan',
			'nominalgaji' => 'Nominalgaji',
			'periodegaji' => 'Periodegaji',
			'pengeluaranumum_id' => 'Pengeluaranumum',
			'jenispengeluaran_id' => 'Jenispengeluaran',
			'jenispengeluaran_kode' => 'Jenispengeluaran Kode',
			'jenispengeluaran_nama' => 'Jenispengeluaran Nama',
			'tandabuktikeluar_id' => 'Tandabuktikeluar',
			'tahun' => 'Tahun',
			'tglkaskeluar' => 'Tglkaskeluar',
			'nokaskeluar' => 'Nokaskeluar',
			'carabayarkeluar' => 'Carabayarkeluar',
			'melaluibank' => 'Melaluibank',
			'denganrekening' => 'Denganrekening',
			'atasnamarekening' => 'Atasnamarekening',
			'namapenerima' => 'Namapenerima',
			'alamatpenerima' => 'Alamatpenerima',
			'untukpembayaran' => 'Untukpembayaran',
			'kelompoktransaksi' => 'Kelompoktransaksi',
			'nopengeluaran' => 'Nopengeluaran',
			'tglpengeluaran' => 'Tglpengeluaran',
			'keterangankeluar' => 'Keterangankeluar',
			'isurainkeluarumum' => 'Isurainkeluarumum',
			'totalharga' => 'Totalharga',
			'biayaadministrasi' => 'Biayaadministrasi',
			'jmlkaskeluar' => 'Jumlah',
			'uraiantransaksi' => 'Uraiantransaksi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('nomorindukpegawai',$this->nomorindukpegawai,true);
		$criteria->compare('no_kartupegawainegerisipil',$this->no_kartupegawainegerisipil,true);
		$criteria->compare('no_karis_karsu',$this->no_karis_karsu,true);
		$criteria->compare('no_taspen',$this->no_taspen,true);
		$criteria->compare('no_askes',$this->no_askes,true);
		$criteria->compare('jenisidentitas',$this->jenisidentitas,true);
		$criteria->compare('noidentitas',$this->noidentitas,true);
		$criteria->compare('gelardepan',$this->gelardepan,true);
		$criteria->compare('nama_pegawai',$this->nama_pegawai,true);
		$criteria->compare('nama_keluarga',$this->nama_keluarga,true);
		$criteria->compare('gelarbelakang_nama',$this->gelarbelakang_nama,true);
		$criteria->compare('tempatlahir_pegawai',$this->tempatlahir_pegawai,true);
		$criteria->compare('tgl_lahirpegawai',$this->tgl_lahirpegawai,true);
		$criteria->compare('jeniskelamin',$this->jeniskelamin,true);
		$criteria->compare('statusperkawinan',$this->statusperkawinan,true);
		$criteria->compare('alamat_pegawai',$this->alamat_pegawai,true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('kelurahan_nama',$this->kelurahan_nama,true);
		$criteria->compare('kode_pos',$this->kode_pos,true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('kecamatan_nama',$this->kecamatan_nama,true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('kabupaten_nama',$this->kabupaten_nama,true);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('golongandarah',$this->golongandarah,true);
		$criteria->compare('rhesus',$this->rhesus,true);
		$criteria->compare('alamatemail',$this->alamatemail,true);
		$criteria->compare('notelp_pegawai',$this->notelp_pegawai,true);
		$criteria->compare('nomobile_pegawai',$this->nomobile_pegawai,true);
		$criteria->compare('warganegara_pegawai',$this->warganegara_pegawai,true);
		$criteria->compare('jeniswaktukerja',$this->jeniswaktukerja,true);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('suku_nama',$this->suku_nama,true);
		$criteria->compare('statuskepemilikanrumah_id',$this->statuskepemilikanrumah_id);
		$criteria->compare('statuskepemilikanrumah_nama',$this->statuskepemilikanrumah_nama,true);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('propinsi_nama',$this->propinsi_nama,true);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('nokode_rumahsakit',$this->nokode_rumahsakit,true);
		$criteria->compare('nama_rumahsakit',$this->nama_rumahsakit,true);
		$criteria->compare('kelompokjabatan',$this->kelompokjabatan,true);
		$criteria->compare('golonganpegawai_id',$this->golonganpegawai_id);
		$criteria->compare('golonganpegawai_nama',$this->golonganpegawai_nama,true);
		$criteria->compare('pangkat_id',$this->pangkat_id);
		$criteria->compare('pangkat_nama',$this->pangkat_nama,true);
		$criteria->compare('jabatan_id',$this->jabatan_id);
		$criteria->compare('jabatan_nama',$this->jabatan_nama,true);
		$criteria->compare('esselon_id',$this->esselon_id);
		$criteria->compare('esselon_nama',$this->esselon_nama,true);
		$criteria->compare('kelompokpegawai_id',$this->kelompokpegawai_id);
		$criteria->compare('kelompokpegawai_nama',$this->kelompokpegawai_nama,true);
		$criteria->compare('kategoripegawai',$this->kategoripegawai,true);
		$criteria->compare('kategoripegawaiasal',$this->kategoripegawaiasal,true);
		$criteria->compare('photopegawai',$this->photopegawai,true);
		$criteria->compare('nofingerprint',$this->nofingerprint,true);
		$criteria->compare('tinggibadan',$this->tinggibadan);
		$criteria->compare('beratbadan',$this->beratbadan);
		$criteria->compare('kemampuanbahasa',$this->kemampuanbahasa,true);
		$criteria->compare('warnakulit',$this->warnakulit,true);
		$criteria->compare('nip_lama',$this->nip_lama,true);
		$criteria->compare('kode_bank',$this->kode_bank,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('tglditerima',$this->tglditerima,true);
		$criteria->compare('tglberhenti',$this->tglberhenti,true);
		$criteria->compare('suratizinpraktek',$this->suratizinpraktek,true);
		$criteria->compare('penggajianpeg_id',$this->penggajianpeg_id);
		$criteria->compare('tglpenggajian',$this->tglpenggajian,true);
		$criteria->compare('nopenggajian',$this->nopenggajian,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('mengetahui',$this->mengetahui,true);
		$criteria->compare('menyetujui',$this->menyetujui,true);
		$criteria->compare('totalterima',$this->totalterima);
		$criteria->compare('totalpajak',$this->totalpajak);
		$criteria->compare('totalpotongan',$this->totalpotongan);
		$criteria->compare('nominalgaji',$this->nominalgaji);
		$criteria->compare('periodegaji',$this->periodegaji,true);
		$criteria->compare('pengeluaranumum_id',$this->pengeluaranumum_id);
		$criteria->compare('jenispengeluaran_id',$this->jenispengeluaran_id);
		$criteria->compare('jenispengeluaran_kode',$this->jenispengeluaran_kode,true);
		$criteria->compare('jenispengeluaran_nama',$this->jenispengeluaran_nama,true);
		$criteria->compare('tandabuktikeluar_id',$this->tandabuktikeluar_id);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('tglkaskeluar',$this->tglkaskeluar,true);
		$criteria->compare('nokaskeluar',$this->nokaskeluar,true);
		$criteria->compare('carabayarkeluar',$this->carabayarkeluar,true);
		$criteria->compare('melaluibank',$this->melaluibank,true);
		$criteria->compare('denganrekening',$this->denganrekening,true);
		$criteria->compare('atasnamarekening',$this->atasnamarekening,true);
		$criteria->compare('namapenerima',$this->namapenerima,true);
		$criteria->compare('alamatpenerima',$this->alamatpenerima,true);
		$criteria->compare('untukpembayaran',$this->untukpembayaran,true);
		$criteria->compare('kelompoktransaksi',$this->kelompoktransaksi,true);
		$criteria->compare('nopengeluaran',$this->nopengeluaran,true);
		$criteria->compare('tglpengeluaran',$this->tglpengeluaran,true);
		$criteria->compare('keterangankeluar',$this->keterangankeluar,true);
		$criteria->compare('isurainkeluarumum',$this->isurainkeluarumum);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('biayaadministrasi',$this->biayaadministrasi);
		$criteria->compare('jmlkaskeluar',$this->jmlkaskeluar);
		$criteria->compare('uraiantransaksi',$this->uraiantransaksi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}