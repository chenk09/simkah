<?php

/**
 * This is the model class for table "tunjangan_m".
 *
 * The followings are the available columns in table 'tunjangan_m':
 * @property integer $tunjangan_id
 * @property integer $pangkat_id
 * @property integer $jabatan_id
 * @property integer $komponengaji_id
 * @property double $nominaltunjangan
 */
class TunjanganM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TunjanganM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tunjangan_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tunjangan_id, komponengaji_id', 'required'),
			array('tunjangan_id, pangkat_id, jabatan_id, komponengaji_id', 'numerical', 'integerOnly'=>true),
			array('nominaltunjangan', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tunjangan_id, pangkat_id, jabatan_id, komponengaji_id, nominaltunjangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'pangkat'=>array(self::BELONGS_TO,'PangkatM','pangkat_id'),
                    'jabatan'=>array(self::BELONGS_TO,'JabatanM','jabatan_id'),
                    'komponengaji'=>array(self::BELONGS_TO,'KomponengajiM','komponengaji_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tunjangan_id' => 'Nomor Tunjangan',
			'pangkat_id' => 'Pangkat',
			'jabatan_id' => 'Jabatan',
			'komponengaji_id' => 'Komponen Gaji',
			'nominaltunjangan' => 'Nominal Tunjangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tunjangan_id',$this->tunjangan_id);
		$criteria->compare('pangkat_id',$this->pangkat_id);
		$criteria->compare('jabatan_id',$this->jabatan_id);
		$criteria->compare('komponengaji_id',$this->komponengaji_id);
		$criteria->compare('nominaltunjangan',$this->nominaltunjangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('tunjangan_id',$this->tunjangan_id);
		$criteria->compare('pangkat_id',$this->pangkat_id);
		$criteria->compare('jabatan_id',$this->jabatan_id);
		$criteria->compare('komponengaji_id',$this->komponengaji_id);
		$criteria->compare('nominaltunjangan',$this->nominaltunjangan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}