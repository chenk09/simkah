<?php

/**
 * This is the model class for table "infosepinacbg_v".
 *
 * The followings are the available columns in table 'infosepinacbg_v':
 * @property string $nosep
 * @property string $nokartuasuransi
 * @property integer $pendaftaran_id
 * @property string $no_pendaftaran
 * @property integer $pasien_id
 * @property string $no_rekam_medik
 * @property string $nama_pasien
 * @property integer $jnspelayanan
 * @property integer $hakkelas_kode
 * @property string $tgl_pendaftaran
 * @property string $tglpulang
 * @property integer $los
 * @property string $nama_pegawai
 * @property string $carakeluar_nama
 * @property string $tgl_lahir
 * @property double $prosedurenonbedah
 * @property double $prosedurebedah
 * @property double $konsultasi
 * @property double $tenagaahli
 * @property double $keperawatan
 * @property double $penunjang
 * @property double $radiologi
 * @property double $laboratorium
 * @property double $pelayanandarah
 * @property double $rehabilitasi
 * @property double $kamar_akomodasi
 * @property double $rawatintensif
 * @property double $obat
 * @property double $alkes
 * @property double $bmhp
 * @property double $sewaalat
 * @property double $total
 */
class InfosepinacbgV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InfosepinacbgV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'infosepinacbg_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pendaftaran_id, pasien_id, jnspelayanan, hakkelas_kode, los', 'numerical', 'integerOnly'=>true),
			array('prosedurenonbedah, prosedurebedah, konsultasi, tenagaahli, keperawatan, penunjang, radiologi, laboratorium, pelayanandarah, rehabilitasi, kamar_akomodasi, rawatintensif, obat, alkes, bmhp, sewaalat, total', 'numerical'),
			array('nosep, carakeluar_nama', 'length', 'max'=>100),
			array('nokartuasuransi, nama_pasien, nama_pegawai', 'length', 'max'=>50),
			array('no_pendaftaran', 'length', 'max'=>20),
			array('no_rekam_medik', 'length', 'max'=>10),
			array('tgl_pendaftaran, tglpulang, tgl_lahir', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nosep, nokartuasuransi, pendaftaran_id, no_pendaftaran, pasien_id, no_rekam_medik, nama_pasien, jnspelayanan, hakkelas_kode, tgl_pendaftaran, tglpulang, los, nama_pegawai, carakeluar_nama, tgl_lahir, prosedurenonbedah, prosedurebedah, konsultasi, tenagaahli, keperawatan, penunjang, radiologi, laboratorium, pelayanandarah, rehabilitasi, kamar_akomodasi, rawatintensif, obat, alkes, bmhp, sewaalat, total', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nosep' => 'Nosep',
			'nokartuasuransi' => 'Nokartuasuransi',
			'namaasuransi_cob' => 'Namaasuransicob',
			'pendaftaran_id' => 'Pendaftaran',
			'no_pendaftaran' => 'No Pendaftaran',
			'pasien_id' => 'Pasien',
			'no_rekam_medik' => 'No Rekam Medik',
			'nama_pasien' => 'Nama Pasien',
			'jnspelayanan' => 'Jnspelayanan',
			'hakkelas_kode' => 'Hakkelas Kode',
			'tgl_pendaftaran' => 'Tgl Pendaftaran',
			'tglpulang' => 'Tglpulang',
			'los' => 'Los',
			'nama_pegawai' => 'Nama Pegawai',
			'carakeluar_nama' => 'Carakeluar Nama',
			'tgl_lahir' => 'Tgl Lahir',
			'prosedurenonbedah' => 'Prosedurenonbedah',
			'prosedurebedah' => 'Prosedurebedah',
			'konsultasi' => 'Konsultasi',
			'tenagaahli' => 'Tenagaahli',
			'keperawatan' => 'Keperawatan',
			'penunjang' => 'Penunjang',
			'radiologi' => 'Radiologi',
			'laboratorium' => 'Laboratorium',
			'pelayanandarah' => 'Pelayanandarah',
			'rehabilitasi' => 'Rehabilitasi',
			'kamar_akomodasi' => 'Kamar Akomodasi',
			'rawatintensif' => 'Rawatintensif',
			'obat' => 'Obat',
			'alkes' => 'Alkes',
			'bmhp' => 'Bmhp',
			'sewaalat' => 'Sewaalat',
			'total' => 'Total',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(nosep)',strtolower($this->nosep),true);
		$criteria->compare('LOWER(nokartuasuransi)',strtolower($this->nokartuasuransi),true);
		if(!empty($this->pendaftaran_id)){
			$criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
		}
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		if(!empty($this->pasien_id)){
			$criteria->addCondition('pasien_id = '.$this->pasien_id);
		}
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		if(!empty($this->jnspelayanan)){
			$criteria->addCondition('jnspelayanan = '.$this->jnspelayanan);
		}
		if(!empty($this->hakkelas_kode)){
			$criteria->addCondition('hakkelas_kode = '.$this->hakkelas_kode);
		}
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('LOWER(tglpulang)',strtolower($this->tglpulang),true);
		if(!empty($this->los)){
			$criteria->addCondition('los = '.$this->los);
		}
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(carakeluar_nama)',strtolower($this->carakeluar_nama),true);
		$criteria->compare('LOWER(tgl_lahir)',strtolower($this->tgl_lahir),true);
		$criteria->compare('prosedurenonbedah',$this->prosedurenonbedah);
		$criteria->compare('prosedurebedah',$this->prosedurebedah);
		$criteria->compare('konsultasi',$this->konsultasi);
		$criteria->compare('tenagaahli',$this->tenagaahli);
		$criteria->compare('keperawatan',$this->keperawatan);
		$criteria->compare('penunjang',$this->penunjang);
		$criteria->compare('radiologi',$this->radiologi);
		$criteria->compare('laboratorium',$this->laboratorium);
		$criteria->compare('pelayanandarah',$this->pelayanandarah);
		$criteria->compare('rehabilitasi',$this->rehabilitasi);
		$criteria->compare('kamar_akomodasi',$this->kamar_akomodasi);
		$criteria->compare('rawatintensif',$this->rawatintensif);
		$criteria->compare('obat',$this->obat);
		$criteria->compare('alkes',$this->alkes);
		$criteria->compare('bmhp',$this->bmhp);
		$criteria->compare('sewaalat',$this->sewaalat);
		$criteria->compare('total',$this->total);

		return $criteria;
	}
        
        
        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=10;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }


        public function searchPrint()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
}