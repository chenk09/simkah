<?php

/**
 * This is the model class for table "permintaanpembelian_t".
 *
 * The followings are the available columns in table 'permintaanpembelian_t':
 * @property integer $permintaanpembelian_id
 * @property integer $pegawai_id
 * @property integer $supplier_id
 * @property integer $rencanakebfarmasi_id
 * @property integer $instalasi_id
 * @property integer $syaratbayar_id
 * @property integer $ruangan_id
 * @property integer $suratpesanan_id
 * @property string $tglpermintaanpenawaran
 * @property string $nopermintaan
 * @property string $tglsuratjalan
 * @property string $nosuratjalan
 * @property string $tglterimabarang
 * @property string $alamatpengiriman
 * @property boolean $istermasukppn
 * @property boolean $istermasukpph
 * @property string $keteranganpermintaan
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class PermintaanpembelianT extends CActiveRecord
{
        public $obatAlkes;
        public $tglAwal;
        public $tglAkhir;
        
        public $harganetto,$jmlppn,$jmldiscount,$jmlkemasan,$hargabelibesar,$obatalkes_kode,$obatalkes_nama,$satuanbesar_nama,$satuankecil_nama;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PermintaanpembelianT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'permintaanpembelian_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pegawai_id, supplier_id, instalasi_id, ruangan_id, tglpermintaanpembelian', 'required'),
			array('pegawai_id, supplier_id, rencanakebfarmasi_id,permintaanpenawaran_id, instalasi_id, syaratbayar_id, permintaanpembelian_id, ruangan_id', 'numerical', 'integerOnly'=>true),
			array('nopermintaan', 'length', 'max'=>50),
//                        array('tglpermintaanpembelian','setValidation'),
			array('tglsuratjalan, permintaanpenawaran_id, tglterimabarang, alamatpengiriman, istermasukppn, istermasukpph, keteranganpermintaan, update_time, update_loginpemakai_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
                        array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
			array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
			array('permintaanpembelian_id, permintaanpenawaran_id, pegawai_id, supplier_id, rencanakebfarmasi_id, instalasi_id, syaratbayar_id, ruangan_id, suratpesanan_id, tglpermintaanpembelian, nopermintaan, tglsuratjalan, nosuratjalan, tglterimabarang, alamatpengiriman, istermasukppn, istermasukpph, keteranganpermintaan, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}
        
        /**
         * custom validation for tglperencanaan
         */
        public function setValidation(){
            if (!$this->hasErrors()){
                if (date('Y-m-d',strtotime($this->tglpermintaanpembelian)) != date('Y-m-d')){
                    $this->addError('tglpermintaanpembelian','Tanggal permintaan pembelian tidak boleh lebih dari '.date('d M Y'));
                }
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                                    'supplier'=>array(self::BELONGS_TO, 'SupplierM','supplier_id'),
                                    'syaratbayar'=>array(self::BELONGS_TO, 'SyaratbayarM','syaratbayar_id'),
                                    'obatalkes'=>array(self::BELONGS_TO, 'ObatalkesM','obatalkes_id'),
                                    'loginpemakai'=>array(self::BELONGS_TO,'LoginpemakaiK','create_loginpemakai_id'),
                                    'permintaandetail'=>array(self::HAS_MANY,'PermintaandetailT','permintaanpembelian_id'),
                                    'ruangans'=>array(self::BELONGS_TO,'RuanganM','ruangan_id'),
                                    'pegawai'=>array(self::BELONGS_TO,'PegawaiM','pegawai_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'permintaanpembelian_id' => 'Permintaanpembelian',
			'pegawai_id' => 'Pegawai',
			'supplier_id' => 'Supplier',
			'rencanakebfarmasi_id' => 'Rencanakebfarmasi',
			'instalasi_id' => 'Instalasi',
			'syaratbayar_id' => 'Syarat Bayar',
			'ruangan_id' => 'Ruangan',
			'suratpesanan_id' => 'Suratpesanan',
			'tglpermintaanpembelian' => 'Tanggal Pembelian',
			'nopermintaan' => 'No PO',
			'tglsuratjalan' => 'Tanggal Surat Jalan',
			'nosuratjalan' => 'No Surat Jalan',
			'tglterimabarang' => 'Tanggal Terima Barang',
			'alamatpengiriman' => 'Alamat Pengiriman',
			'istermasukppn' => 'Istermasukppn',
			'istermasukpph' => 'Istermasukpph',
			'keteranganpermintaan' => 'Keterangan Permintaan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
                        'obatAlkes'=>'Obat Alkes',
                        'tglAwal'=>'Tgl Permintaan Pembelian',
                        'tglAkhir'=>'Sampai Dengan'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('rencanakebfarmasi_id',$this->rencanakebfarmasi_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('syaratbayar_id',$this->syaratbayar_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('suratpesanan_id',$this->suratpesanan_id);
		$criteria->compare('LOWER(tglpermintaanpembelian)',strtolower($this->tglpermintaanpembelian),true);
		$criteria->compare('LOWER(nopermintaan)',strtolower($this->nopermintaan),true);
		$criteria->compare('LOWER(tglsuratjalan)',strtolower($this->tglsuratjalan),true);
		$criteria->compare('LOWER(nosuratjalan)',strtolower($this->nosuratjalan),true);
		$criteria->compare('LOWER(tglterimabarang)',strtolower($this->tglterimabarang),true);
		$criteria->compare('LOWER(alamatpengiriman)',strtolower($this->alamatpengiriman),true);
		$criteria->compare('istermasukppn',$this->istermasukppn);
		$criteria->compare('istermasukpph',$this->istermasukpph);
		$criteria->compare('LOWER(keteranganpermintaan)',strtolower($this->keteranganpermintaan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('rencanakebfarmasi_id',$this->rencanakebfarmasi_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('syaratbayar_id',$this->syaratbayar_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('suratpesanan_id',$this->suratpesanan_id);
		$criteria->compare('LOWER(tglpermintaanpembelian)',strtolower($this->tglpermintaanpembelian),true);
		$criteria->compare('LOWER(nopermintaan)',strtolower($this->nopermintaan),true);
		$criteria->compare('LOWER(tglsuratjalan)',strtolower($this->tglsuratjalan),true);
		$criteria->compare('LOWER(nosuratjalan)',strtolower($this->nosuratjalan),true);
		$criteria->compare('LOWER(tglterimabarang)',strtolower($this->tglterimabarang),true);
		$criteria->compare('LOWER(alamatpengiriman)',strtolower($this->alamatpengiriman),true);
		$criteria->compare('istermasukppn',$this->istermasukppn);
		$criteria->compare('istermasukpph',$this->istermasukpph);
		$criteria->compare('LOWER(keteranganpermintaan)',strtolower($this->keteranganpermintaan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit
                $criteria->limit=-1;

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function getSupplierItems()
        {
            return SupplierM::model()->findAll("supplier_aktif=TRUE AND supplier_jenis ilike '%".PARAMS::DEFAULT_JENIS_SUPPLIER."%' ORDER BY supplier_nama");
        }
        
        public function getSyaratBayarItems()
        {
            return SyaratbayarM::model()->findAll('syaratbayar_aktif=TRUE ORDER BY syaratbayar_nama');
        }
        
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                        }
            }
            return true;
        }
        
        public function beforeSave()
        {
            if($this->tglterimabarang===null || trim($this->tglterimabarang)==''){
	        $this->setAttribute('tglterimabarang', null);
            }
            
            return parent::beforeSave();
        }

        public function getSupplierforInformasi()
        {
            return SupplierM::model()->findAll('supplier_aktif=TRUE ORDER BY supplier_nama');
        }
}