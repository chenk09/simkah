<?php

/**
 * This is the model class for table "antrian_t".
 *
 * The followings are the available columns in table 'antrian_t':
 * @property integer $antrian_id
 * @property integer $pendaftaran_id
 * @property integer $profilrs_id
 * @property integer $ruangan_id
 * @property string $tglantrian
 * @property string $noantrian
 * @property string $statuspasien
 * @property string $loket_antrian
 */
class AntrianT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AntrianT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'antrian_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profilrs_id, ruangan_id, tglantrian, noantrian', 'required'),
			array('pendaftaran_id, profilrs_id, ruangan_id', 'numerical', 'integerOnly'=>true),
			array('noantrian', 'length', 'max'=>6),
			array('statuspasien', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('antrian_id, pendaftaran_id, profilrs_id, ruangan_id, tglantrian, noantrian, statuspasien', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'antrian_id' => 'Antrian',
			'pendaftaran_id' => 'Pendaftaran',
			'profilrs_id' => 'Profilrs',
			'ruangan_id' => 'Ruangan',
			'tglantrian' => 'Tglantrian',
			'noantrian' => 'Noantrian',
			'statuspasien' => 'Statuspasien',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('antrian_id',$this->antrian_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(tglantrian)',strtolower($this->tglantrian),true);
		$criteria->compare('LOWER(noantrian)',strtolower($this->noantrian),true);
		$criteria->compare('LOWER(statuspasien)',strtolower($this->statuspasien),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('antrian_id',$this->antrian_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(tglantrian)',strtolower($this->tglantrian),true);
		$criteria->compare('LOWER(noantrian)',strtolower($this->noantrian),true);
		$criteria->compare('LOWER(statuspasien)',strtolower($this->statuspasien),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}