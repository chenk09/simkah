<?php

/**
 * This is the model class for table "laporaninventarisasiperalatan_v".
 *
 * The followings are the available columns in table 'laporaninventarisasiperalatan_v':
 * @property integer $inventaris_id
 * @property string $inventaris_kode
 * @property string $no_register
 * @property string $tgl_register
 * @property integer $barang_id
 * @property integer $bidang_id
 * @property integer $golongan_id
 * @property string $golongan_kode
 * @property string $golongan_nama
 * @property integer $kelompok_id
 * @property string $kelompok_kode
 * @property string $kelompok_nama
 * @property integer $subkelompok_id
 * @property string $subkelompok_kode
 * @property string $subkelompok_nama
 * @property string $bidang_kode
 * @property string $bidang_nama
 * @property string $barang_type
 * @property string $barang_kode
 * @property string $barang_nama
 * @property string $barang_namalainnya
 * @property string $barang_merk
 * @property string $barang_noseri
 * @property string $barang_ukuran
 * @property string $barang_bahan
 * @property string $barang_thnbeli
 * @property string $barang_warna
 * @property boolean $barang_statusregister
 * @property integer $barang_ekonomis_thn
 * @property string $barang_satuan
 * @property integer $barang_jmldlmkemasan
 * @property string $barang_image
 * @property double $barang_harga
 * @property string $invperalatan_merk
 * @property string $invperalatan_ukuran
 * @property string $invperalatan_bahan
 * @property string $invperalatan_thnpembelian
 * @property string $invperalatan_tglguna
 * @property string $invperalatan_nopabrik
 * @property string $invperalatan_norangka
 * @property string $invperalatan_nomesin
 * @property string $invperalatan_nopolisi
 * @property string $invperalatan_nobpkb
 * @property double $invperalatan_harga
 * @property double $invperalatan_akumsusut
 * @property string $invperalatan_ket
 * @property string $invperalatan_kapasitasrata
 * @property boolean $invperalatan_ijinoperasional
 * @property string $invperalatan_serftkkalibrasi
 * @property integer $invperalatan_umurekonomis
 * @property string $invperalatan_keadaan
 * @property integer $lokasi_id
 * @property string $lokasiaset_kode
 * @property string $lokasiaset_namainstalasi
 * @property string $lokasiaset_namabagian
 * @property string $lokasiaset_namalokasi
 * @property integer $asalaset_id
 * @property string $asalaset_nama
 * @property string $asalaset_singkatan
 * @property integer $pemilikbarang_id
 * @property string $pemilikbarang_kode
 * @property string $pemilikbarang_nama
 */
class LaporaninventarisasiperalatanV extends CActiveRecord
{
	public $tglAwal, $tglAkhir;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporaninventarisasiperalatanV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporaninventarisasiperalatan_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('inventaris_id, barang_id, bidang_id, golongan_id, kelompok_id, subkelompok_id, barang_ekonomis_thn, barang_jmldlmkemasan, invperalatan_umurekonomis, lokasi_id, asalaset_id, pemilikbarang_id', 'numerical', 'integerOnly'=>true),
			array('barang_harga, invperalatan_harga, invperalatan_akumsusut', 'numerical'),
			array('inventaris_kode, no_register, golongan_kode, kelompok_kode, subkelompok_kode, bidang_kode, barang_type, barang_kode, barang_merk, barang_warna, barang_satuan, invperalatan_merk, invperalatan_ukuran, invperalatan_nopabrik, invperalatan_norangka, invperalatan_nomesin, invperalatan_nopolisi, invperalatan_nobpkb, invperalatan_keadaan, lokasiaset_kode, lokasiaset_namabagian, asalaset_nama', 'length', 'max'=>50),
			array('golongan_nama, kelompok_nama, subkelompok_nama, bidang_nama, barang_nama, barang_namalainnya, invperalatan_bahan, lokasiaset_namainstalasi, lokasiaset_namalokasi, pemilikbarang_nama', 'length', 'max'=>100),
			array('barang_noseri, barang_ukuran, barang_bahan, invperalatan_serftkkalibrasi, pemilikbarang_kode', 'length', 'max'=>20),
			array('barang_thnbeli, invperalatan_thnpembelian', 'length', 'max'=>5),
			array('barang_image', 'length', 'max'=>200),
			array('invperalatan_kapasitasrata, asalaset_singkatan', 'length', 'max'=>10),
			array('tgl_register, barang_statusregister, invperalatan_tglguna, invperalatan_ket, invperalatan_ijinoperasional', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('inventaris_id, inventaris_kode, no_register, tgl_register, barang_id, bidang_id, golongan_id, golongan_kode, golongan_nama, kelompok_id, kelompok_kode, kelompok_nama, subkelompok_id, subkelompok_kode, subkelompok_nama, bidang_kode, bidang_nama, barang_type, barang_kode, barang_nama, barang_namalainnya, barang_merk, barang_noseri, barang_ukuran, barang_bahan, barang_thnbeli, barang_warna, barang_statusregister, barang_ekonomis_thn, barang_satuan, barang_jmldlmkemasan, barang_image, barang_harga, invperalatan_merk, invperalatan_ukuran, invperalatan_bahan, invperalatan_thnpembelian, invperalatan_tglguna, invperalatan_nopabrik, invperalatan_norangka, invperalatan_nomesin, invperalatan_nopolisi, invperalatan_nobpkb, invperalatan_harga, invperalatan_akumsusut, invperalatan_ket, invperalatan_kapasitasrata, invperalatan_ijinoperasional, invperalatan_serftkkalibrasi, invperalatan_umurekonomis, invperalatan_keadaan, lokasi_id, lokasiaset_kode, lokasiaset_namainstalasi, lokasiaset_namabagian, lokasiaset_namalokasi, asalaset_id, asalaset_nama, asalaset_singkatan, pemilikbarang_id, pemilikbarang_kode, pemilikbarang_nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'inventaris_id' => 'Inventaris',
			'inventaris_kode' => 'Inventaris Kode',
			'no_register' => 'No Register',
			'tgl_register' => 'Tgl Register',
			'barang_id' => 'Barang',
			'bidang_id' => 'Bidang',
			'golongan_id' => 'Golongan',
			'golongan_kode' => 'Golongan Kode',
			'golongan_nama' => 'Golongan Nama',
			'kelompok_id' => 'Kelompok',
			'kelompok_kode' => 'Kelompok Kode',
			'kelompok_nama' => 'Kelompok Nama',
			'subkelompok_id' => 'Subkelompok',
			'subkelompok_kode' => 'Subkelompok Kode',
			'subkelompok_nama' => 'Subkelompok Nama',
			'bidang_kode' => 'Bidang Kode',
			'bidang_nama' => 'Bidang Nama',
			'barang_type' => 'Barang Type',
			'barang_kode' => 'Barang Kode',
			'barang_nama' => 'Barang Nama',
			'barang_namalainnya' => 'Barang Namalainnya',
			'barang_merk' => 'Barang Merk',
			'barang_noseri' => 'Barang Noseri',
			'barang_ukuran' => 'Barang Ukuran',
			'barang_bahan' => 'Barang Bahan',
			'barang_thnbeli' => 'Barang Thnbeli',
			'barang_warna' => 'Barang Warna',
			'barang_statusregister' => 'Barang Statusregister',
			'barang_ekonomis_thn' => 'Barang Ekonomis Thn',
			'barang_satuan' => 'Barang Satuan',
			'barang_jmldlmkemasan' => 'Barang Jmldlmkemasan',
			'barang_image' => 'Barang Image',
			'barang_harga' => 'Barang Harga',
			'invperalatan_merk' => 'Invperalatan Merk',
			'invperalatan_ukuran' => 'Invperalatan Ukuran',
			'invperalatan_bahan' => 'Invperalatan Bahan',
			'invperalatan_thnpembelian' => 'Invperalatan Thnpembelian',
			'invperalatan_tglguna' => 'Invperalatan Tglguna',
			'invperalatan_nopabrik' => 'Invperalatan Nopabrik',
			'invperalatan_norangka' => 'Invperalatan Norangka',
			'invperalatan_nomesin' => 'Invperalatan Nomesin',
			'invperalatan_nopolisi' => 'Invperalatan Nopolisi',
			'invperalatan_nobpkb' => 'Invperalatan Nobpkb',
			'invperalatan_harga' => 'Invperalatan Harga',
			'invperalatan_akumsusut' => 'Invperalatan Akumsusut',
			'invperalatan_ket' => 'Invperalatan Ket',
			'invperalatan_kapasitasrata' => 'Invperalatan Kapasitasrata',
			'invperalatan_ijinoperasional' => 'Invperalatan Ijinoperasional',
			'invperalatan_serftkkalibrasi' => 'Invperalatan Serftkkalibrasi',
			'invperalatan_umurekonomis' => 'Invperalatan Umurekonomis',
			'invperalatan_keadaan' => 'Invperalatan Keadaan',
			'lokasi_id' => 'Lokasi',
			'lokasiaset_kode' => 'Lokasiaset Kode',
			'lokasiaset_namainstalasi' => 'Lokasiaset Namainstalasi',
			'lokasiaset_namabagian' => 'Lokasiaset Namabagian',
			'lokasiaset_namalokasi' => 'Lokasiaset Namalokasi',
			'asalaset_id' => 'Asalaset',
			'asalaset_nama' => 'Asalaset Nama',
			'asalaset_singkatan' => 'Asalaset Singkatan',
			'pemilikbarang_id' => 'Pemilikbarang',
			'pemilikbarang_kode' => 'Pemilikbarang Kode',
			'pemilikbarang_nama' => 'Pemilikbarang Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('inventaris_id',$this->inventaris_id);
		$criteria->compare('inventaris_kode',$this->inventaris_kode,true);
		$criteria->compare('no_register',$this->no_register,true);
		$criteria->compare('tgl_register',$this->tgl_register,true);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('bidang_id',$this->bidang_id);
		$criteria->compare('golongan_id',$this->golongan_id);
		$criteria->compare('golongan_kode',$this->golongan_kode,true);
		$criteria->compare('golongan_nama',$this->golongan_nama,true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('kelompok_kode',$this->kelompok_kode,true);
		$criteria->compare('kelompok_nama',$this->kelompok_nama,true);
		$criteria->compare('subkelompok_id',$this->subkelompok_id);
		$criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
		$criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
		$criteria->compare('bidang_kode',$this->bidang_kode,true);
		$criteria->compare('bidang_nama',$this->bidang_nama,true);
		$criteria->compare('barang_type',$this->barang_type,true);
		$criteria->compare('barang_kode',$this->barang_kode,true);
		$criteria->compare('barang_nama',$this->barang_nama,true);
		$criteria->compare('barang_namalainnya',$this->barang_namalainnya,true);
		$criteria->compare('barang_merk',$this->barang_merk,true);
		$criteria->compare('barang_noseri',$this->barang_noseri,true);
		$criteria->compare('barang_ukuran',$this->barang_ukuran,true);
		$criteria->compare('barang_bahan',$this->barang_bahan,true);
		$criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
		$criteria->compare('barang_warna',$this->barang_warna,true);
		$criteria->compare('barang_statusregister',$this->barang_statusregister);
		$criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
		$criteria->compare('barang_satuan',$this->barang_satuan,true);
		$criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
		$criteria->compare('barang_image',$this->barang_image,true);
		$criteria->compare('barang_harga',$this->barang_harga);
		$criteria->compare('invperalatan_merk',$this->invperalatan_merk,true);
		$criteria->compare('invperalatan_ukuran',$this->invperalatan_ukuran,true);
		$criteria->compare('invperalatan_bahan',$this->invperalatan_bahan,true);
		$criteria->compare('invperalatan_thnpembelian',$this->invperalatan_thnpembelian,true);
		$criteria->compare('invperalatan_tglguna',$this->invperalatan_tglguna,true);
		$criteria->compare('invperalatan_nopabrik',$this->invperalatan_nopabrik,true);
		$criteria->compare('invperalatan_norangka',$this->invperalatan_norangka,true);
		$criteria->compare('invperalatan_nomesin',$this->invperalatan_nomesin,true);
		$criteria->compare('invperalatan_nopolisi',$this->invperalatan_nopolisi,true);
		$criteria->compare('invperalatan_nobpkb',$this->invperalatan_nobpkb,true);
		$criteria->compare('invperalatan_harga',$this->invperalatan_harga);
		$criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
		$criteria->compare('invperalatan_ket',$this->invperalatan_ket,true);
		$criteria->compare('invperalatan_kapasitasrata',$this->invperalatan_kapasitasrata,true);
		$criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
		$criteria->compare('invperalatan_serftkkalibrasi',$this->invperalatan_serftkkalibrasi,true);
		$criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
		$criteria->compare('invperalatan_keadaan',$this->invperalatan_keadaan,true);
		$criteria->compare('lokasi_id',$this->lokasi_id);
		$criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
		$criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
		$criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
		$criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
		$criteria->compare('asalaset_id',$this->asalaset_id);
		$criteria->compare('asalaset_nama',$this->asalaset_nama,true);
		$criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
		$criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
		$criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
		$criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}