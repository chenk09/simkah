<?php

/**
 * This is the model class for table "permintaanbarangdetail_t".
 *
 * The followings are the available columns in table 'permintaanbarangdetail_t':
 * @property integer $permintaanbarangdetail_id
 * @property integer $barang_id
 * @property integer $permintaanpembelian_id
 * @property double $jmlpermintaan
 * @property string $created
 * @property string $create_time
 * @property string $updated
 * @property string $update_time
 * @property string $satuanbarang
 */
class PermintaanbarangdetailT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PermintaanbarangdetailT the static model class
	 */
	public $nama_barang;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'permintaanbarangdetail_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('barang_id, permintaanpembelian_id, jmlpermintaan', 'required'),
			array('barang_id, permintaanpembelian_id', 'numerical', 'integerOnly'=>true),
			array('jmlpermintaan', 'numerical'),
			array('created, updated', 'length', 'max'=>16),
			array('satuanbarang', 'length', 'max'=>50),
			
			array('created','default','value'=>Yii::app()->user->id,'on'=>'insert'),
            array('create_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
			array('updated','default','value'=>Yii::app()->user->id,'on'=>'update'),
            array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('permintaanbarangdetail_id, barang_id, permintaanpembelian_id, jmlpermintaan, created, create_time, updated, update_time, satuanbarang', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'barang'=>array(self::BELONGS_TO, 'BarangM', 'barang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'permintaanbarangdetail_id' => 'Permintaanbarangdetail',
			'barang_id' => 'Barang',
			'permintaanpembelian_id' => 'Permintaanpembelian',
			'jmlpermintaan' => 'Jmlpermintaan',
			'created' => 'Created',
			'create_time' => 'Create Time',
			'updated' => 'Updated',
			'update_time' => 'Update Time',
			'satuanbarang' => 'Satuanbarang',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('permintaanbarangdetail_id',$this->permintaanbarangdetail_id);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('jmlpermintaan',$this->jmlpermintaan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('satuanbarang',$this->satuanbarang,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}