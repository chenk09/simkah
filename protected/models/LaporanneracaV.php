<?php

/**
 * This is the model class for table "laporanneraca_v".
 *
 * The followings are the available columns in table 'laporanneraca_v':
 * @property integer $struktur_id
 * @property string $kdstruktur
 * @property string $nmstruktur
 * @property integer $kelompok_id
 * @property string $kdkelompok
 * @property string $nmkelompok
 * @property integer $jenis_id
 * @property string $kdjenis
 * @property string $nmjenis
 * @property string $tglbukubesar
 * @property integer $rekperiod_id
 * @property double $saldonominal
 * @property string $kelompokrek
 */
class LaporanneracaV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanneracaV the static model class
	 */
	public $tglAwal, $tglAkhir, $nominal;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanneraca_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('struktur_id, kelompok_id, jenis_id, rekperiod_id', 'numerical', 'integerOnly'=>true),
			array('saldonominal', 'numerical'),
			array('kdstruktur, kdkelompok, kdjenis', 'length', 'max'=>5),
			array('nmstruktur', 'length', 'max'=>100),
			array('nmkelompok', 'length', 'max'=>200),
			array('nmjenis', 'length', 'max'=>300),
			array('kelompokrek', 'length', 'max'=>20),
			array('tglbukubesar', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('struktur_id, kdstruktur, nmstruktur, kelompok_id, kdkelompok, nmkelompok, jenis_id, kdjenis, nmjenis, tglbukubesar, rekperiod_id, saldonominal, kelompokrek, instalasi_id, instalasi_nama, ruangan_id, ruangan_nama, profilrs_id, nama_rumahsakit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'struktur_id' => 'Struktur',
			'kdstruktur' => 'Kdstruktur',
			'nmstruktur' => 'Nmstruktur',
			'kelompok_id' => 'Kelompok',
			'kdkelompok' => 'Kdkelompok',
			'nmkelompok' => 'Nmkelompok',
			'jenis_id' => 'Jenis',
			'kdjenis' => 'Kdjenis',
			'nmjenis' => 'Nmjenis',
			'tglbukubesar' => 'Tanggal Buku Besar',
			'rekperiod_id' => 'Rekperiod',
			'saldonominal' => 'Saldonominal',
			'kelompokrek' => 'Kelompokrek',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('struktur_id',$this->struktur_id);
		$criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur),true);
		$criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(kdkelompok)',strtolower($this->kdkelompok),true);
		$criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
		$criteria->compare('jenis_id',$this->jenis_id);
		$criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
		$criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
		$criteria->compare('LOWER(tglbukubesar)',strtolower($this->tglbukubesar),true);
		$criteria->compare('rekperiod_id',$this->rekperiod_id);
		$criteria->compare('saldonominal',$this->saldonominal);
		$criteria->compare('LOWER(kelompokrek)',strtolower($this->kelompokrek),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('struktur_id',$this->struktur_id);
		$criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur),true);
		$criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(kdkelompok)',strtolower($this->kdkelompok),true);
		$criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
		$criteria->compare('jenis_id',$this->jenis_id);
		$criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
		$criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
		$criteria->compare('LOWER(tglbukubesar)',strtolower($this->tglbukubesar),true);
		$criteria->compare('rekperiod_id',$this->rekperiod_id);
		$criteria->compare('saldonominal',$this->saldonominal);
		$criteria->compare('LOWER(kelompokrek)',strtolower($this->kelompokrek),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }

        public function searchNeraca()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

                $criteria=new CDbCriteria;
                $criteria->addBetweenCondition('tglbukubesar', $this->tglAwal, $this->tglAkhir);
                $criteria->select = 'kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal';
                $criteria->group = 'nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis';
                $criteria->order = 'kdstruktur, kdkelompok, kdjenis';

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
	}
        
        public function getRuangan(){
            $criteria= new CDbCriteria;
            $criteria->group = 'instalasi_id, instalasi_nama, ruangan_id, ruangan_nama';
            $criteria->select = $criteria->group;
            
            $modRuangans = LaporanneracaV::model()->findAll($criteria);
            return $modRuangans;
        }
        
        public function getSumRuangan($nmjenis = null, $id_ruangan = null, $id_instalasi = null){
            $format = new CustomFormat();
            $criteria= new CDbCriteria;
            $criteria->select = 'kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal';
            $criteria->group = 'nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis';
            $criteria->order = 'kdstruktur, kdkelompok, kdjenis';
            $jml = 0;
            $criteria->compare('nmjenis',$nmjenis);
            if ($id_ruangan != 'bebas'){
                if($id_ruangan == Params::RUANGAN_ID_DIREKTUR){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_DIREKTUR);
                }else if ($id_ruangan == Params::RUANGAN_ID_SUBBAG_AKUNTANSI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_SUBBAG_AKUNTANSI);
                }else if ($id_ruangan == Params::RUANGAN_ID_SUBBAG_BENDAHARA){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_SUBBAG_BENDAHARA);
                }else if ($id_ruangan == Params::RUANGAN_ID_SEKRETARIS){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_SEKRETARIS);
                }else if ($id_ruangan == Params::RUANGAN_ID_STAFF_ME){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_STAFF_ME);
                }else if ($id_ruangan == Params::RUANGAN_ID_KEPEGAWAIAN){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_KEPEGAWAIAN);
                }else if ($id_ruangan == Params::RUANGAN_ID_REKAM_MEDIK){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_REKAM_MEDIK);
                }else if ($id_ruangan == Params::RUANGAN_ID_KEUANGAN){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_KEUANGAN);
                }else if ($id_ruangan == Params::RUANGAN_ID_SUBBAG_SUSUN_ANGGARAN){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_SUBBAG_SUSUN_ANGGARAN);
                }else if ($id_ruangan == Params::RUANGAN_ID_PELAYANAN_KHUSUS){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_PELAYANAN_KHUSUS);
                }else if ($id_ruangan == Params::RUANGAN_ID_PERSALINAN){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_PERSALINAN);
                }else if ($id_ruangan == Params::RUANGAN_ID_LAB){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_LAB);
                }else if ($id_ruangan == Params::RUANGAN_ID_RAD){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_RAD);
                }else if ($id_ruangan == Params::RUANGAN_ID_REHAB){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_REHAB);
                }else if ($id_ruangan == Params::RUANGAN_ID_IRM){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_IRM);
                }else if ($id_ruangan == Params::RUANGAN_ID_IBS){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_IBS);
                }else if ($id_ruangan == Params::RUANGAN_ID_APOTEK_RJ){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_APOTEK_RJ);
                }else if ($id_ruangan == Params::RUANGAN_ID_APOTEK_DEPO){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_APOTEK_DEPO);
                }else if ($id_ruangan == Params::RUANGAN_ID_GUDANG_FARMASI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_GUDANG_FARMASI);
                }else if ($id_ruangan == Params::RUANGAN_ID_JZ){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_JZ);
                }else if ($id_ruangan == Params::RUANGAN_ID_GIZI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_GIZI);
                }else if ($id_ruangan == Params::RUANGAN_ID_OBSGYN){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_OBSGYN);
                }else if ($id_ruangan == Params::RUANGAN_ID_KONSULTASI_GIZI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_KONSULTASI_GIZI);
                }else if ($id_ruangan == Params::RUANGAN_ID_KASIR_LAB){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_KASIR_LAB);
                }else if ($id_ruangan == Params::RUANGAN_ID_KASIR_SENTRAL){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_KASIR_SENTRAL);
                }else if ($id_ruangan == Params::RUANGAN_ID_2A){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_2A);
                }else if ($id_ruangan == Params::RUANGAN_ID_2B){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_2B);
                }else if ($id_ruangan == Params::RUANGAN_ID_2C){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_2C);
                }else if ($id_ruangan == Params::RUANGAN_ID_3A){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_3A);
                }else if ($id_ruangan == Params::RUANGAN_ID_3B){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_3B);
                }else if ($id_ruangan == Params::RUANGAN_ID_3C){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_3C);
                }else if ($id_ruangan == Params::RUANGAN_ID_4A){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_4A);
                }else if ($id_ruangan == Params::RUANGAN_ID_4B){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_4B);
                }else if ($id_ruangan == Params::RUANGAN_ID_4C){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_4C);
                }else if ($id_ruangan == Params::RUANGAN_ID_ICU){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_ICU);
                }else if ($id_ruangan == Params::RUANGAN_ID_OBSERVASI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_OBSERVASI);
                }else if ($id_ruangan == Params::RUANGAN_ID_BANGSAL){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_BANGSAL);
                }else if ($id_ruangan == Params::RUANGAN_ID_PENDAFTARAN){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_PENDAFTARAN);
                }else if ($id_ruangan == Params::RUANGAN_ID_PERINATOLOGI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_PERINATOLOGI);
                }else if ($id_ruangan == Params::RUANGAN_ID_2A){
                    $criteria->adInCondition('ruangan_id',array(
                        Params::RUANGAN_ID_2A,
                        Params::RUANGAN_ID_2B,
                        Params::RUANGAN_ID_2C,
                        ));
                }else if ($id_ruangan == Params::RUANGAN_ID_3A){
                    $criteria->adInCondition('ruangan_id',array(
                        Params::RUANGAN_ID_3A,
                        Params::RUANGAN_ID_3B,
                        Params::RUANGAN_ID_3C,
                        ));
                }else if ($id_ruangan == Params::RUANGAN_ID_RAWAT_DARURAT){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_RAWAT_DARURAT);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_ALERGY){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_ALERGY);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_ANAK){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_ANAK);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_BEDAH){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_BEDAH);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_BEDAH_MULUT){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_BEDAH_MULUT);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_BEDAH_SYARAF){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_BEDAH_SYARAF);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_BEDAH_UMUM){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_BEDAH_UMUM);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_GIGI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_GIGI);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_KONSERVASI_GIGI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_KONSERVASI_GIGI);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_ORTHOPEDI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_ORTHOPEDI);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_PENYAKIT_DALAM){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_PENYAKIT_DALAM);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_PENYAKIT_SYARAF){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_PENYAKIT_SYARAF);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_THT){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_THT);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_UMUM){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_UMUM);
                }else if ($id_ruangan == Params::RUANGAN_ID_POLY_UROLOGI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_POLY_UROLOGI);
                }else if ($id_ruangan == Params::RUANGAN_ID_ICU){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_ICU);
                }else if ($id_ruangan == Params::RUANGAN_ID_HEMODIALISA){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_HEMODIALISA);
                }else if ($id_ruangan == Params::RUANGAN_ID_ESWL){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_ESWL);
                }else if ($id_ruangan == Params::RUANGAN_ID_ENDOSCOPY){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_ENDOSCOPY);
                }else if ($id_ruangan == Params::RUANGAN_ID_LAUNDRY){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_LAUNDRY);
                }else if ($id_ruangan == Params::RUANGAN_ID_JENAZAH){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_JENAZAH);
                }else if ($id_ruangan == Params::RUANGAN_ID_KEAMANAN_TRANSPORTASI){
                    $criteria->addCondition('ruangan_id = '.Params::RUANGAN_ID_KEAMANAN_TRANSPORTASI);
                }else {
                    $ruangan_ga_ketemu = 1;
                }
            }
            
            
            if ($id_instalasi != 'bebas'){
                if ($id_instalasi == Params::INSTALASI_ID_RJ){
                $criteria->compare('instalasi_id',Params::INSTALASI_ID_RJ);
                }else if ($id_instalasi == Params::INSTALASI_ID_RM){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_RM);
                }else if ($id_instalasi == Params::INSTALASI_ID_RD){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_RD);
                }else if ($id_instalasi == Params::INSTALASI_ID_RI){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_RI);
                }else if ($id_instalasi == Params::INSTALASI_ID_IBS){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_IBS);
                }else if ($id_instalasi == Params::INSTALASI_ID_LAB){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_LAB);
                }else if ($id_instalasi == Params::INSTALASI_ID_RAD){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_RAD);
                }else if ($id_instalasi == Params::INSTALASI_ID_REHAB){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_REHAB);
                }else if ($id_instalasi == Params::INSTALASI_ID_GIZI){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_GIZI);
                }else if ($id_instalasi == Params::INSTALASI_ID_JZ){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_JZ);
                }else if ($id_instalasi == Params::INSTALASI_ID_FARMASI){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_FARMASI);
                }else if ($id_instalasi == Params::INSTALASI_ID_RAWAT_INTENSIF){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_RAWAT_INTENSIF);
                }else if ($id_instalasi == Params::INSTALASI_ID_STERILISASI_SENTRAL){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_STERILISASI_SENTRAL);
                }else if ($id_instalasi == Params::INSTALASI_ID_HEMODIALISA){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_HEMODIALISA);
                }else if ($id_instalasi == Params::INSTALASI_ID_SANITASI){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_SANITASI);
                }else if ($id_instalasi == Params::INSTALASI_ID_KEAMANAN_TRANSPORTASI){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_KEAMANAN_TRANSPORTASI);
                }else if ($id_instalasi == Params::INSTALASI_ID_NON_INSTALASI){
                    $criteria->compare('instalasi_id',Params::INSTALASI_ID_NON_INSTALASI);
                }else {
                    $instalasi_ga_ketemu = 1;
                }
            }
            
            if(isset($_GET['LaporanneracaV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporanneracaV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporanneracaV']['tglAkhir']);
                $ruangan_id =$_GET['LaporanneracaV']['ruangan_id'];
                $criteria->addBetweenCondition('tglbukubesar',$tglAwal,$tglAkhir);
                $criteria->compare('ruangan_id',$ruangan_id);
            }
            if (isset($ruangan_ga_ketemu) || isset($instalasi_ga_ketemu)){
                return 0;
            } else {
                $modNominal = LaporanneracaV::model()->findAll($criteria);                

                foreach ($modNominal as $nominal){
                    $jml += $nominal->nominal;
                }
                return $jml;
            }
            
            
            
            
        }
}