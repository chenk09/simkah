<?php

/**
 * This is the model class for table "map_diagnosa_obat_det".
 *
 * The followings are the available columns in table 'map_diagnosa_obat_det':
 * @property string $begda
 * @property string $endda
 * @property string $chgdt
 * @property integer $chgus
 * @property string $mapping_diag_obat_det_id
 * @property string $mapping_diag_id
 * @property integer $obat_alkes_id
 * @property double $qty
 * @property double $harga_satuan
 * @property double $harga_netto
 * @property double $harga_jual
 */
class MapDiagnosaObatDet extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MapDiagnosaObatDet the static model class
	 */
    public $obatalkes_kode;
    public $obatalkes_nama;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'map_diagnosa_obat_det';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('begda','default','value'=>date('Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
            array('endda','default','value'=>'9999-12-31','setOnEmpty'=>false,'on'=>'insert'),
            array('chgdt','default','value'=>date('Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
            array('chgus','default','value'=>Yii::app()->user->id,'on'=>'insert'),

			array('chgus, obat_alkes_id', 'numerical', 'integerOnly'=>true),
			array('qty, harga_satuan, harga_netto, harga_jual', 'numerical'),
			array('begda, endda, chgdt, mapping_diag_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('begda, endda, chgdt, chgus, mapping_diag_obat_det_id, mapping_diag_id, obat_alkes_id, qty, harga_satuan, harga_netto, harga_jual', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'obats'=>array(self::BELONGS_TO, 'ObatalkesM','obat_alkes_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'begda' => 'Begda',
			'endda' => 'Endda',
			'chgdt' => 'Chgdt',
			'chgus' => 'Chgus',
			'mapping_diag_obat_det_id' => 'Mapping Diag Obat Det',
			'mapping_diag_id' => 'Mapping Diag',
			'obat_alkes_id' => 'Obat Alkes',
			'qty' => 'Qty',
			'harga_satuan' => 'Harga Satuan',
			'harga_netto' => 'Harga Netto',
			'harga_jual' => 'Harga Jual',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('begda',$this->begda);
		$criteria->compare('endda',$this->endda);
		$criteria->compare('chgdt',$this->chgdt);
		$criteria->compare('chgus',$this->chgus);
		$criteria->compare('mapping_diag_obat_det_id',$this->mapping_diag_obat_det_id);
		$criteria->compare('mapping_diag_id',$this->mapping_diag_id);
		$criteria->compare('obat_alkes_id',$this->obat_alkes_id);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('harga_satuan',$this->harga_satuan);
		$criteria->compare('harga_netto',$this->harga_netto);
		$criteria->compare('harga_jual',$this->harga_jual);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(begda)',strtolower($this->begda),true);
		$criteria->compare('LOWER(endda)',strtolower($this->endda),true);
		$criteria->compare('LOWER(chgdt)',strtolower($this->chgdt),true);
		$criteria->compare('chgus',$this->chgus);
		$criteria->compare('LOWER(mapping_diag_obat_det_id)',strtolower($this->mapping_diag_obat_det_id),true);
		$criteria->compare('LOWER(mapping_diag_id)',strtolower($this->mapping_diag_id),true);
		$criteria->compare('obat_alkes_id',$this->obat_alkes_id);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('harga_satuan',$this->harga_satuan);
		$criteria->compare('harga_netto',$this->harga_netto);
		$criteria->compare('harga_jual',$this->harga_jual);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}