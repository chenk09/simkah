<?php

/**
 * This is the model class for table "tandabuktikeluar_t".
 *
 * The followings are the available columns in table 'tandabuktikeluar_t':
 * @property integer $tandabuktikeluar_id
 * @property integer $returbayarpelayanan_id
 * @property integer $shift_id
 * @property integer $ruangan_id
 * @property integer $uangmukabeli_id
 * @property integer $bayarkesupplier_id
 * @property integer $pengeluaranumum_id
 * @property string $tahun
 * @property string $tglkaskeluar
 * @property string $nokaskeluar
 * @property string $carabayarkeluar
 * @property string $melalubank
 * @property string $denganrekening
 * @property string $atasnamarekening
 * @property string $namapenerima
 * @property string $alamatpenerima
 * @property string $untukpembayaran
 * @property double $jmlkaskeluar
 * @property double $biayaadministrasi
 * @property string $keterangan_pengeluaran
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class TandabuktikeluarT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TandabuktikeluarT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tandabuktikeluar_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tahun, ruangan_id, tglkaskeluar, nokaskeluar, carabayarkeluar, namapenerima, alamatpenerima, jmlkaskeluar, biayaadministrasi', 'required'),
			array('returbayarpelayanan_id, shift_id, ruangan_id, uangmukabeli_id, bayarkesupplier_id, pengeluaranumum_id', 'numerical', 'integerOnly'=>true),
			array('jmlkaskeluar, biayaadministrasi', 'numerical'),
			array('tahun', 'length', 'max'=>4),
			array('nokaskeluar, carabayarkeluar', 'length', 'max'=>50),
			array('melalubank, denganrekening, atasnamarekening, namapenerima, untukpembayaran', 'length', 'max'=>100),
			array('keterangan_pengeluaran, update_time, update_loginpemakai_id', 'safe'),
                    
                        array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
                        array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
                        array('shift_id','default','value'=>Yii::app()->user->getState('shift_id'),'on'=>'insert'),
                    
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tandabuktikeluar_id, returbayarpelayanan_id, shift_id, ruangan_id, uangmukabeli_id, bayarkesupplier_id, pengeluaranumum_id, tahun, tglkaskeluar, nokaskeluar, carabayarkeluar, melalubank, denganrekening, atasnamarekening, namapenerima, alamatpenerima, untukpembayaran, jmlkaskeluar, biayaadministrasi, keterangan_pengeluaran, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan, pembatalanuangmuka_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'bayarsupplier'=>array(self::BELONGS_TO, 'BayarkesupplierT', 'bayarkesupplier_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tandabuktikeluar_id' => 'Tandabuktikeluar',
			'returbayarpelayanan_id' => 'Returbayarpelayanan',
                        'pembatalanuangmuka_id'=>'pembatalanuangmuka_id',
			'shift_id' => 'Shift',
			'ruangan_id' => 'Ruangan',
			'uangmukabeli_id' => 'Uangmukabeli',
			'bayarkesupplier_id' => 'Bayarkesupplier',
			'pengeluaranumum_id' => 'Pengeluaranumum',
			'tahun' => 'Tahun',
			'tglkaskeluar' => 'Tgl Kas Keluar',
			'nokaskeluar' => 'No Kas Keluar',
			'carabayarkeluar' => 'Cara Bayar',
			'melalubank' => 'Melalui Bank',
			'denganrekening' => 'Dengan Rekening',
			'atasnamarekening' => 'Atas Nama Rekening',
			'namapenerima' => 'Nama Penerima',
			'alamatpenerima' => 'Alamat Penerima',
			'untukpembayaran' => 'Untuk Pembayaran',
			'jmlkaskeluar' => 'Jml Kas Keluar',
			'biayaadministrasi' => 'Biaya Administrasi',
			'keterangan_pengeluaran' => 'Keterangan Pengeluaran',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tandabuktikeluar_id',$this->tandabuktikeluar_id);
		$criteria->compare('returbayarpelayanan_id',$this->returbayarpelayanan_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('uangmukabeli_id',$this->uangmukabeli_id);
		$criteria->compare('bayarkesupplier_id',$this->bayarkesupplier_id);
		$criteria->compare('pengeluaranumum_id',$this->pengeluaranumum_id);
		$criteria->compare('LOWER(tahun)',strtolower($this->tahun),true);
		$criteria->compare('LOWER(tglkaskeluar)',strtolower($this->tglkaskeluar),true);
		$criteria->compare('LOWER(nokaskeluar)',strtolower($this->nokaskeluar),true);
		$criteria->compare('LOWER(carabayarkeluar)',strtolower($this->carabayarkeluar),true);
		$criteria->compare('LOWER(melalubank)',strtolower($this->melalubank),true);
		$criteria->compare('LOWER(denganrekening)',strtolower($this->denganrekening),true);
		$criteria->compare('LOWER(atasnamarekening)',strtolower($this->atasnamarekening),true);
		$criteria->compare('LOWER(namapenerima)',strtolower($this->namapenerima),true);
		$criteria->compare('LOWER(alamatpenerima)',strtolower($this->alamatpenerima),true);
		$criteria->compare('LOWER(untukpembayaran)',strtolower($this->untukpembayaran),true);
		$criteria->compare('jmlkaskeluar',$this->jmlkaskeluar);
		$criteria->compare('biayaadministrasi',$this->biayaadministrasi);
		$criteria->compare('LOWER(keterangan_pengeluaran)',strtolower($this->keterangan_pengeluaran),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('tandabuktikeluar_id',$this->tandabuktikeluar_id);
		$criteria->compare('returbayarpelayanan_id',$this->returbayarpelayanan_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('uangmukabeli_id',$this->uangmukabeli_id);
		$criteria->compare('bayarkesupplier_id',$this->bayarkesupplier_id);
		$criteria->compare('pengeluaranumum_id',$this->pengeluaranumum_id);
		$criteria->compare('LOWER(tahun)',strtolower($this->tahun),true);
		$criteria->compare('LOWER(tglkaskeluar)',strtolower($this->tglkaskeluar),true);
		$criteria->compare('LOWER(nokaskeluar)',strtolower($this->nokaskeluar),true);
		$criteria->compare('LOWER(carabayarkeluar)',strtolower($this->carabayarkeluar),true);
		$criteria->compare('LOWER(melalubank)',strtolower($this->melalubank),true);
		$criteria->compare('LOWER(denganrekening)',strtolower($this->denganrekening),true);
		$criteria->compare('LOWER(atasnamarekening)',strtolower($this->atasnamarekening),true);
		$criteria->compare('LOWER(namapenerima)',strtolower($this->namapenerima),true);
		$criteria->compare('LOWER(alamatpenerima)',strtolower($this->alamatpenerima),true);
		$criteria->compare('LOWER(untukpembayaran)',strtolower($this->untukpembayaran),true);
		$criteria->compare('jmlkaskeluar',$this->jmlkaskeluar);
		$criteria->compare('biayaadministrasi',$this->biayaadministrasi);
		$criteria->compare('LOWER(keterangan_pengeluaran)',strtolower($this->keterangan_pengeluaran),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date'){
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                    }elseif ($column->dbType == 'timestamp without time zone'){
                            //$this->$columnName = date('Y-m-d H:i:s', CDateTimeParser::parse($this->$columnName, Yii::app()->locale->dateFormat));
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                    }
            }

            return parent::beforeValidate ();
        }
                
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                        }
            }
            return true;
        }
}