<?php

/**
 * This is the model class for table "konfigsystem_k".
 *
 * The followings are the available columns in table 'konfigsystem_k':
 * @property integer $konfigsystem_id
 * @property boolean $isantrian
 * @property boolean $iskarcis
 * @property boolean $printkartulsng
 * @property boolean $printkunjunganlsng
 * @property boolean $nama_huruf_capital
 * @property boolean $alamat_huruf_capital
 * @property string $mr_lab
 * @property string $mr_rad
 * @property string $mr_ibs
 * @property string $mr_rehabmedis
 * @property string $mr_apotik
 * @property string $mr_jenazah
 * @property string $nopendaftaran_rj
 * @property string $nopendaftaran_ri
 * @property string $nopendaftaran_gd
 * @property string $nopendaftaran_lab
 * @property string $nopendaftaran_rad
 * @property string $nopendaftaran_ibs
 * @property string $nopendaftaran_rehabmedis
 * @property string $nopendaftaran_jenazah
 * @property string $running_text_display
 * @property string $running_text_kiosk
 * @property boolean $dokterruangan
 * @property boolean $tindakanruangan
 * @property boolean $tindakankelas
 * @property boolean $krngistokgizi
 * @property boolean $krngistokumum
 * @property double $persentasirujin
 * @property double $persentasirujout
 */
class KonfigsystemK extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KonfigsystemK the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'konfigsystem_k';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('persentasirujin, persentasirujout', 'numerical'),
			array('mr_lab, mr_rad, mr_ibs, mr_rehabmedis, mr_apotik, mr_jenazah, nopendaftaran_rj, nopendaftaran_ri, nopendaftaran_gd, nopendaftaran_lab, nopendaftaran_rad, nopendaftaran_ibs, nopendaftaran_rehabmedis, nopendaftaran_jenazah', 'length', 'max'=>4),
			array('isantrian, iskarcis, printkartulsng, printkunjunganlsng, nama_huruf_capital, alamat_huruf_capital, running_text_display, running_text_kiosk, dokterruangan, tindakanruangan, tindakankelas, krngistokgizi, krngistokumum', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('konfigsystem_id, isantrian, iskarcis, printkartulsng, printkunjunganlsng, nama_huruf_capital, alamat_huruf_capital, mr_lab, mr_rad, mr_ibs, mr_rehabmedis, mr_apotik, mr_jenazah, nopendaftaran_rj, nopendaftaran_ri, nopendaftaran_gd, nopendaftaran_lab, nopendaftaran_rad, nopendaftaran_ibs, nopendaftaran_rehabmedis, nopendaftaran_jenazah, running_text_display, running_text_kiosk, dokterruangan, tindakanruangan, tindakankelas, krngistokgizi, krngistokumum, persentasirujin, persentasirujout', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'konfigsystem_id' => 'ID',
			'isantrian' => 'Antrian',
			'iskarcis' => 'Karcis',
			'printkartulsng' => 'Pring kartu langsung',
			'printkunjunganlsng' => 'Print Kunjungan langsung',
			'nama_huruf_capital' => 'Nama Huruf Capital',
			'alamat_huruf_capital' => 'Alamat Huruf Capital',
			'mr_lab' => 'Mr Lab',
			'mr_rad' => 'Mr Rad',
			'mr_ibs' => 'Mr Ibs',
			'mr_rehabmedis' => 'Mr Rehabmedis',
			'mr_apotik' => 'Mr Apotik',
			'mr_jenazah' => 'Mr Jenazah',
			'nopendaftaran_rj' => 'No pendaftaran Rawat Jalan',
			'nopendaftaran_ri' => 'No pendaftaran Rawat inap',
			'nopendaftaran_gd' => 'No pendaftaran Gawat darurat',
			'nopendaftaran_lab' => 'No pendaftaran Laboratorium',
			'nopendaftaran_rad' => 'No pendaftaran Radiologi',
			'nopendaftaran_ibs' => 'No pendaftaran Ibs',
			'nopendaftaran_rehabmedis' => 'No pendaftaran Rehabmedis',
			'nopendaftaran_jenazah' => 'No pendaftaran Jenazah',
			'running_text_display' => 'Running Text Display',
			'running_text_kiosk' => 'Running Text Kiosk',
			'dokterruangan' => 'Ruangan dokter',
			'tindakanruangan' => 'Ruangan tindakan',
			'tindakankelas' => 'Kelas Tindakan',
			'krngistokgizi' => 'Stok gizi',
			'krngistokumum' => 'Stok umum',
			'persentasirujin' => 'Persentasi in',
			'persentasirujout' => 'Persentasi Out',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('konfigsystem_id',$this->konfigsystem_id);
		$criteria->compare('isantrian',$this->isantrian);
		$criteria->compare('iskarcis',$this->iskarcis);
		$criteria->compare('printkartulsng',$this->printkartulsng);
		$criteria->compare('printkunjunganlsng',$this->printkunjunganlsng);
		$criteria->compare('nama_huruf_capital',$this->nama_huruf_capital);
		$criteria->compare('alamat_huruf_capital',$this->alamat_huruf_capital);
		$criteria->compare('LOWER(mr_lab)',strtolower($this->mr_lab),true);
		$criteria->compare('LOWER(mr_rad)',strtolower($this->mr_rad),true);
		$criteria->compare('LOWER(mr_ibs)',strtolower($this->mr_ibs),true);
		$criteria->compare('LOWER(mr_rehabmedis)',strtolower($this->mr_rehabmedis),true);
		$criteria->compare('LOWER(mr_apotik)',strtolower($this->mr_apotik),true);
		$criteria->compare('LOWER(mr_jenazah)',strtolower($this->mr_jenazah),true);
		$criteria->compare('LOWER(nopendaftaran_rj)',strtolower($this->nopendaftaran_rj),true);
		$criteria->compare('LOWER(nopendaftaran_ri)',strtolower($this->nopendaftaran_ri),true);
		$criteria->compare('LOWER(nopendaftaran_gd)',strtolower($this->nopendaftaran_gd),true);
		$criteria->compare('LOWER(nopendaftaran_lab)',strtolower($this->nopendaftaran_lab),true);
		$criteria->compare('LOWER(nopendaftaran_rad)',strtolower($this->nopendaftaran_rad),true);
		$criteria->compare('LOWER(nopendaftaran_ibs)',strtolower($this->nopendaftaran_ibs),true);
		$criteria->compare('LOWER(nopendaftaran_rehabmedis)',strtolower($this->nopendaftaran_rehabmedis),true);
		$criteria->compare('LOWER(nopendaftaran_jenazah)',strtolower($this->nopendaftaran_jenazah),true);
		$criteria->compare('LOWER(running_text_display)',strtolower($this->running_text_display),true);
		$criteria->compare('LOWER(running_text_kiosk)',strtolower($this->running_text_kiosk),true);
		$criteria->compare('dokterruangan',$this->dokterruangan);
		$criteria->compare('tindakanruangan',$this->tindakanruangan);
		$criteria->compare('tindakankelas',$this->tindakankelas);
		$criteria->compare('krngistokgizi',$this->krngistokgizi);
		$criteria->compare('krngistokumum',$this->krngistokumum);
		$criteria->compare('persentasirujin',$this->persentasirujin);
		$criteria->compare('persentasirujout',$this->persentasirujout);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('konfigsystem_id',$this->konfigsystem_id);
		$criteria->compare('isantrian',$this->isantrian);
		$criteria->compare('iskarcis',$this->iskarcis);
		$criteria->compare('printkartulsng',$this->printkartulsng);
		$criteria->compare('printkunjunganlsng',$this->printkunjunganlsng);
		$criteria->compare('nama_huruf_capital',$this->nama_huruf_capital);
		$criteria->compare('alamat_huruf_capital',$this->alamat_huruf_capital);
		$criteria->compare('LOWER(mr_lab)',strtolower($this->mr_lab),true);
		$criteria->compare('LOWER(mr_rad)',strtolower($this->mr_rad),true);
		$criteria->compare('LOWER(mr_ibs)',strtolower($this->mr_ibs),true);
		$criteria->compare('LOWER(mr_rehabmedis)',strtolower($this->mr_rehabmedis),true);
		$criteria->compare('LOWER(mr_apotik)',strtolower($this->mr_apotik),true);
		$criteria->compare('LOWER(mr_jenazah)',strtolower($this->mr_jenazah),true);
		$criteria->compare('LOWER(nopendaftaran_rj)',strtolower($this->nopendaftaran_rj),true);
		$criteria->compare('LOWER(nopendaftaran_ri)',strtolower($this->nopendaftaran_ri),true);
		$criteria->compare('LOWER(nopendaftaran_gd)',strtolower($this->nopendaftaran_gd),true);
		$criteria->compare('LOWER(nopendaftaran_lab)',strtolower($this->nopendaftaran_lab),true);
		$criteria->compare('LOWER(nopendaftaran_rad)',strtolower($this->nopendaftaran_rad),true);
		$criteria->compare('LOWER(nopendaftaran_ibs)',strtolower($this->nopendaftaran_ibs),true);
		$criteria->compare('LOWER(nopendaftaran_rehabmedis)',strtolower($this->nopendaftaran_rehabmedis),true);
		$criteria->compare('LOWER(nopendaftaran_jenazah)',strtolower($this->nopendaftaran_jenazah),true);
		$criteria->compare('LOWER(running_text_display)',strtolower($this->running_text_display),true);
		$criteria->compare('LOWER(running_text_kiosk)',strtolower($this->running_text_kiosk),true);
		$criteria->compare('dokterruangan',$this->dokterruangan);
		$criteria->compare('tindakanruangan',$this->tindakanruangan);
		$criteria->compare('tindakankelas',$this->tindakankelas);
		$criteria->compare('krngistokgizi',$this->krngistokgizi);
		$criteria->compare('krngistokumum',$this->krngistokumum);
		$criteria->compare('persentasirujin',$this->persentasirujin);
		$criteria->compare('persentasirujout',$this->persentasirujout);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public static function getKonfigKurangiStokGizi(){
            $result = KonfigsystemK::model()->find()->krngistokgizi;
            return $result;
        }
        
        public static function getKonfigKurangiStokUmum(){
            $result = KonfigsystemK::model()->find()->krngistokumum;
            return $result;
        }
        
        public static function getKonfigurasiKarcisPasien(){
            $result = KonfigsystemK::model()->find()->karcisbarulama;
            return $result;
        }
}