<?php

/**
 * This is the model class for table "lokasiantrian_m".
 *
 * The followings are the available columns in table 'lokasiantrian_m':
 * @property integer $lokasiantrian_id
 * @property string $lokasiantrian_nama
 * @property boolean $lokasiantrian_aktif
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property AntrianpasienT[] $antrianpasienTs
 * @property LayananantrianM[] $layananantrianMs
 * @property LoketantrianM[] $loketantrianMs
 */
class LokasiantrianM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LokasiantrianM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lokasiantrian_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lokasiantrian_nama', 'required'),
			array('lokasiantrian_nama', 'length', 'max'=>100),
			array('lokasiantrian_aktif, keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('lokasiantrian_id, lokasiantrian_nama, lokasiantrian_aktif, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'antrianpasienTs' => array(self::HAS_MANY, 'AntrianpasienT', 'lokasiantrian_id'),
			'layananantrianMs' => array(self::HAS_MANY, 'LayananantrianM', 'lokasiantrian_id'),
			'loketantrianMs' => array(self::HAS_MANY, 'LoketantrianM', 'lokasiantrian_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lokasiantrian_id' => 'Lokasiantrian',
			'lokasiantrian_nama' => 'Lokasiantrian Nama',
			'lokasiantrian_aktif' => 'Lokasiantrian Aktif',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('LOWER(lokasiantrian_nama)',strtolower($this->lokasiantrian_nama),true);
		$criteria->compare('lokasiantrian_aktif',$this->lokasiantrian_aktif);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('LOWER(lokasiantrian_nama)',strtolower($this->lokasiantrian_nama),true);
		$criteria->compare('lokasiantrian_aktif',$this->lokasiantrian_aktif);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}