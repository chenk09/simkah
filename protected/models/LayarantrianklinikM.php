<?php

/**
 * This is the model class for table "layarantrianklinik_m".
 *
 * The followings are the available columns in table 'layarantrianklinik_m':
 * @property integer $layarantrianklinik_id
 * @property integer $lokasiantrianklinik_id
 * @property integer $ruangan_id
 * @property string $layarklinikantrian_keterangan
 * @property boolean $layarklinikantrian_aktif
 *
 * The followings are the available model relations:
 * @property LokasiantrianklinikM $lokasiantrianklinik
 * @property RuanganM $ruangan
 */
class LayarantrianklinikM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LayarantrianklinikM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'layarantrianklinik_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lokasiantrianklinik_id, ruangan_id', 'required'),
			array('lokasiantrianklinik_id, ruangan_id', 'numerical', 'integerOnly'=>true),
			array('layarklinikantrian_keterangan, layarklinikantrian_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('layarantrianklinik_id, lokasiantrianklinik_id, ruangan_id, layarklinikantrian_keterangan, layarklinikantrian_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lokasiantrianklinik' => array(self::BELONGS_TO, 'LokasiantrianklinikM', 'lokasiantrianklinik_id'),
			'ruangan' => array(self::BELONGS_TO, 'RuanganM', 'ruangan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'layarantrianklinik_id' => 'Layarantrianklinik',
			'lokasiantrianklinik_id' => 'Lokasiantrianklinik',
			'ruangan_id' => 'Ruangan',
			'layarklinikantrian_keterangan' => 'Layarklinikantrian Keterangan',
			'layarklinikantrian_aktif' => 'Layarklinikantrian Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('layarantrianklinik_id',$this->layarantrianklinik_id);
		$criteria->compare('lokasiantrianklinik_id',$this->lokasiantrianklinik_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(layarklinikantrian_keterangan)',strtolower($this->layarklinikantrian_keterangan),true);
		$criteria->compare('layarklinikantrian_aktif',$this->layarklinikantrian_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('layarantrianklinik_id',$this->layarantrianklinik_id);
		$criteria->compare('lokasiantrianklinik_id',$this->lokasiantrianklinik_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(layarklinikantrian_keterangan)',strtolower($this->layarklinikantrian_keterangan),true);
		$criteria->compare('layarklinikantrian_aktif',$this->layarklinikantrian_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}