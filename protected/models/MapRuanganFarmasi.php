<?php

/**
 * This is the model class for table "map_ruangan_farmasi".
 *
 * The followings are the available columns in table 'map_ruangan_farmasi':
 * @property string $begda
 * @property string $endda
 * @property string $chgdt
 * @property integer $map_ruangan_id
 * @property integer $ruangan_asal_id
 * @property integer $ruangan_target_id
 */
class MapRuanganFarmasi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MapRuanganFarmasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'map_ruangan_farmasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ruangan_asal_id, ruangan_target_id', 'numerical', 'integerOnly'=>true),
			array('begda, endda, chgdt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('begda, endda, chgdt, map_ruangan_id, ruangan_asal_id, ruangan_target_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'begda' => 'Begda',
			'endda' => 'Endda',
			'chgdt' => 'Chgdt',
			'map_ruangan_id' => 'Map Ruangan',
			'ruangan_asal_id' => 'Ruangan Asal',
			'ruangan_target_id' => 'Ruangan Target',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(begda)',strtolower($this->begda),true);
		$criteria->compare('LOWER(endda)',strtolower($this->endda),true);
		$criteria->compare('LOWER(chgdt)',strtolower($this->chgdt),true);
		$criteria->compare('map_ruangan_id',$this->map_ruangan_id);
		$criteria->compare('ruangan_asal_id',$this->ruangan_asal_id);
		$criteria->compare('ruangan_target_id',$this->ruangan_target_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(begda)',strtolower($this->begda),true);
		$criteria->compare('LOWER(endda)',strtolower($this->endda),true);
		$criteria->compare('LOWER(chgdt)',strtolower($this->chgdt),true);
		$criteria->compare('map_ruangan_id',$this->map_ruangan_id);
		$criteria->compare('ruangan_asal_id',$this->ruangan_asal_id);
		$criteria->compare('ruangan_target_id',$this->ruangan_target_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}