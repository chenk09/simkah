<?php

/**
 * This is the model class for table "laporanjasadokterlabrad_v".
 *
 * The followings are the available columns in table 'laporanjasadokterlabrad_v':
 * @property integer $profilrs_id
 * @property integer $pasien_id
 * @property string $no_rekam_medik
 * @property string $nama_pasien
 * @property string $alamat_pasien
 * @property integer $pendaftaran_id
 * @property string $no_pendaftaran
 * @property string $tgl_pendaftaran
 * @property string $dokterpemeriksa1_id
 * @property string $nama_pegawai
 * @property string $no_masukpenunjang
 * @property string $tglmasukpenunjang
 * @property string $namadepan
 * @property string $ruanganasal_nama
 * @property double $harga
 * @property double $dibayar
 */
class LaporanjasadokterlabradV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanjasadokterlabradV the static model class
	 */
        public $tglAwal,$tglAkhir,$bulan,$dokterpemeriksa1_id;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanjasadokterlabrad_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profilrs_id, pasien_id, pendaftaran_id', 'numerical', 'integerOnly'=>true),
			array('harga, dibayar', 'numerical'),
			array('no_rekam_medik', 'length', 'max'=>10),
			array('nama_pasien, nama_pegawai, namaperujuk, ruanganasal_nama', 'length', 'max'=>50),
			array('no_pendaftaran, no_masukpenunjang, namadepan', 'length', 'max'=>20),
			array('alamat_pasien, tgl_pendaftaran, dokterpemeriksa1_id, tglmasukpenunjang', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nama_perujuk, namapegawai, carabayar_nama, carabayar_id, penjamin_id, penjamin_nama, profilrs_id, tglAwal,tglAkhir,bulan,pasien_id, no_rekam_medik, nama_pasien, alamat_pasien, pendaftaran_id, no_pendaftaran, tgl_pendaftaran, dokterpemeriksa1_id, nama_pegawai, no_masukpenunjang, tglmasukpenunjang, namadepan, ruanganasal_nama, harga, dibayar, namaperujuk', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'profilrs_id' => 'Profilrs',
			'pasien_id' => 'Pasien',
			'no_rekam_medik' => 'No Rekam Medik',
			'nama_pasien' => 'Nama Pasien',
			'alamat_pasien' => 'Alamat Pasien',
			'pendaftaran_id' => 'Pendaftaran',
			'no_pendaftaran' => 'No Pendaftaran',
			'tgl_pendaftaran' => 'Tgl Pendaftaran',
			'dokterpemeriksa1_id' => 'Dokterpemeriksa1',
			'nama_pegawai' => 'Dokter',
			'rujukan_id' => 'Rujukan',
			'no_masukpenunjang' => 'No Masukpenunjang',
			'tglmasukpenunjang' => 'Tglmasukpenunjang',
			'namadepan' => 'Namadepan',
			'ruanganasal_nama' => 'Ruanganasal Nama',
			'harga' => 'Harga',
			'dibayar' => 'Dibayar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('LOWER(dokterpemeriksa1_id)',strtolower($this->dokterpemeriksa1_id),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
		$criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(ruanganasal_nama)',strtolower($this->ruanganasal_nama),true);
		$criteria->compare('harga',$this->harga);
		$criteria->compare('dibayar',$this->dibayar);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('LOWER(dokterpemeriksa1_id)',strtolower($this->dokterpemeriksa1_id),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
		$criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(ruanganasal_nama)',strtolower($this->ruanganasal_nama),true);
		$criteria->compare('harga',$this->harga);
		$criteria->compare('dibayar',$this->dibayar);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}