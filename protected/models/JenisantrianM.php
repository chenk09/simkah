<?php

/**
 * This is the model class for table "jenisantrian_m".
 *
 * The followings are the available columns in table 'jenisantrian_m':
 * @property integer $jenisantrian_id
 * @property string $jenisantrian_nama
 * @property boolean $jenisantrian_aktif
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property AntrianpasienT[] $antrianpasienTs
 * @property LayananantrianM[] $layananantrianMs
 */
class JenisantrianM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return JenisantrianM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jenisantrian_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenisantrian_nama', 'required'),
			array('jenisantrian_nama', 'length', 'max'=>250),
			array('jenisantrian_aktif, keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('jenisantrian_id, jenisantrian_nama, jenisantrian_aktif, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'antrianpasienTs' => array(self::HAS_MANY, 'AntrianpasienT', 'jenisantrian_id'),
			'layananantrianMs' => array(self::HAS_MANY, 'LayananantrianM', 'jenisantrian_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'jenisantrian_id' => 'Jenisantrian',
			'jenisantrian_nama' => 'Jenisantrian Nama',
			'jenisantrian_aktif' => 'Jenisantrian Aktif',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('LOWER(jenisantrian_nama)',strtolower($this->jenisantrian_nama),true);
		$criteria->compare('jenisantrian_aktif',$this->jenisantrian_aktif);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('LOWER(jenisantrian_nama)',strtolower($this->jenisantrian_nama),true);
		$criteria->compare('jenisantrian_aktif',$this->jenisantrian_aktif);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}