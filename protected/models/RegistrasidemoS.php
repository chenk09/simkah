<?php

/**
 * This is the model class for table "registrasidemo_s".
 *
 * The followings are the available columns in table 'registrasidemo_s':
 * @property integer $registrasidemo_id
 * @property string $tglregisterdemo
 * @property string $namalengkapreg
 * @property string $alamatlngkpreg
 * @property string $propinsi
 * @property string $kotakabupaten
 * @property string $notelp
 * @property string $nomobile
 * @property string $email
 * @property string $namainstansi
 * @property string $alamatinstansi
 * @property string $telpinstansi
 * @property boolean $statusrequest
 * @property string $website
 */
class RegistrasidemoS extends CActiveRecord
{
	public $verifyCode;
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RegistrasidemoS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'registrasidemo_s';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tglregisterdemo, namalengkapreg, alamatlngkpreg, propinsi, kotakabupaten, nomobile, email, namainstansi, alamatinstansi, telpinstansi', 'required'),
			array('namalengkapreg, namainstansi', 'length', 'max'=>200),
			array('propinsi, kotakabupaten, notelp, nomobile, website', 'length', 'max'=>100),
			array('email, telpinstansi', 'length', 'max'=>50),
			array('statusrequest', 'safe'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!extension_loaded('gd')),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('registrasidemo_id, tglregisterdemo, namalengkapreg, alamatlngkpreg, propinsi, kotakabupaten, notelp, nomobile, email, namainstansi, alamatinstansi, telpinstansi, statusrequest, website', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'registrasidemo_id' => 'Registrasidemo',
			'tglregisterdemo' => 'Tgl Register',
			'namalengkapreg' => 'Nama Lengkap',
			'alamatlngkpreg' => 'Alamat Lengkap',
			'propinsi' => 'Propinsi',
			'kotakabupaten' => 'Kota/Kabupaten',
			'notelp' => 'No Telepon',
			'nomobile' => 'No Mobile',
			'email' => 'Email',
			'namainstansi' => 'Nama Instansi',
			'alamatinstansi' => 'Alamat Instansi',
			'telpinstansi' => 'No. Telp. Instansi',
			'statusrequest' => 'Statusrequest',
			'website' => 'Website',
			'verifyCode'=>'Verification Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('registrasidemo_id',$this->registrasidemo_id);
		$criteria->compare('LOWER(tglregisterdemo)',strtolower($this->tglregisterdemo),true);
		$criteria->compare('LOWER(namalengkapreg)',strtolower($this->namalengkapreg),true);
		$criteria->compare('LOWER(alamatlngkpreg)',strtolower($this->alamatlngkpreg),true);
		$criteria->compare('LOWER(propinsi)',strtolower($this->propinsi),true);
		$criteria->compare('LOWER(kotakabupaten)',strtolower($this->kotakabupaten),true);
		$criteria->compare('LOWER(notelp)',strtolower($this->notelp),true);
		$criteria->compare('LOWER(nomobile)',strtolower($this->nomobile),true);
		$criteria->compare('LOWER(email)',strtolower($this->email),true);
		$criteria->compare('LOWER(namainstansi)',strtolower($this->namainstansi),true);
		$criteria->compare('LOWER(alamatinstansi)',strtolower($this->alamatinstansi),true);
		$criteria->compare('LOWER(telpinstansi)',strtolower($this->telpinstansi),true);
		$criteria->compare('statusrequest',$this->statusrequest);
		$criteria->compare('LOWER(website)',strtolower($this->website),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('registrasidemo_id',$this->registrasidemo_id);
		$criteria->compare('LOWER(tglregisterdemo)',strtolower($this->tglregisterdemo),true);
		$criteria->compare('LOWER(namalengkapreg)',strtolower($this->namalengkapreg),true);
		$criteria->compare('LOWER(alamatlngkpreg)',strtolower($this->alamatlngkpreg),true);
		$criteria->compare('LOWER(propinsi)',strtolower($this->propinsi),true);
		$criteria->compare('LOWER(kotakabupaten)',strtolower($this->kotakabupaten),true);
		$criteria->compare('LOWER(notelp)',strtolower($this->notelp),true);
		$criteria->compare('LOWER(nomobile)',strtolower($this->nomobile),true);
		$criteria->compare('LOWER(email)',strtolower($this->email),true);
		$criteria->compare('LOWER(namainstansi)',strtolower($this->namainstansi),true);
		$criteria->compare('LOWER(alamatinstansi)',strtolower($this->alamatinstansi),true);
		$criteria->compare('LOWER(telpinstansi)',strtolower($this->telpinstansi),true);
		$criteria->compare('statusrequest',$this->statusrequest);
		$criteria->compare('LOWER(website)',strtolower($this->website),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}