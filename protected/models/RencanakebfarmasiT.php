<?php

/**
 * This is the model class for table "rencanakebfarmasi_t".
 *
 * The followings are the available columns in table 'rencanakebfarmasi_t':
 * @property integer $rencanakebfarmasi_id
 * @property integer $ruangan_id
 * @property integer $otorisasikeuangan_id
 * @property integer $pegawai_id
 * @property integer $otorisasipimpinan_id
 * @property string $tglperencanaan
 * @property string $noperencnaan
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class RencanakebfarmasiT extends CActiveRecord
{
        public $obatAlkes;
        public $tglAwal;
        public $tglAkhir;
        public $jumlah;
        public $data;
        public $tick;

        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RencanakebfarmasiT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rencanakebfarmasi_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ruangan_id, tglperencanaan, noperencnaan', 'required'),
			array('ruangan_id, otorisasikeuangan_id, pegawai_id, otorisasipimpinan_id', 'numerical', 'integerOnly'=>true),
			array('noperencnaan', 'length', 'max'=>50),
			array('update_time, update_loginpemakai_id, tglperencanaan', 'safe'),
//                        array('tglperencanaan', 'setValidation','on'=>'insert'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
                        array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
			array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),

                        array('rencanakebfarmasi_id, ruangan_id, otorisasikeuangan_id, pegawai_id, otorisasipimpinan_id, tglperencanaan, noperencnaan, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}
        
        /**
         *  custom validation 
         */
        public function setValidation(){
            if (!$this->hasErrors()){
                if ($this->tglperencanaan != date('Y-m-d')){
                    $this->addError('tglperencanaan','Tanggal perencanaan tidak boleh lebih dari '.date('d M Y'));
                }
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'ruangan'=>array(self::BELONGS_TO, 'RuanganM','ruangan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rencanakebfarmasi_id' => 'ID',
			'ruangan_id' => 'Ruangan',
			'otorisasikeuangan_id' => 'Otorisasi Keuangan',
			'pegawai_id' => 'Pegawai',
			'otorisasipimpinan_id' => 'Otorisasi Pimpinan',
			'tglperencanaan' => 'Tanggal Rencana',
			'noperencnaan' => 'No Rencana',
                        'tglAwal'=>'Tanggal Awal',
                        'tglAkhir'=>'Tanggal Akhir',
			'create_time' => 'Tanggal Perencanaan',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
                        'obatAlkes' =>'Obat Alkes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rencanakebfarmasi_id',$this->rencanakebfarmasi_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('otorisasikeuangan_id',$this->otorisasikeuangan_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('otorisasipimpinan_id',$this->otorisasipimpinan_id);
		$criteria->compare('date(tglperencanaan)',$this->tglperencanaan);
		$criteria->compare('LOWER(noperencnaan)',strtolower($this->noperencnaan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('rencanakebfarmasi_id',$this->rencanakebfarmasi_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('otorisasikeuangan_id',$this->otorisasikeuangan_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('otorisasipimpinan_id',$this->otorisasipimpinan_id);
		$criteria->compare('LOWER(tglperencanaan)',strtolower($this->tglperencanaan),true);
		$criteria->compare('LOWER(noperencnaan)',strtolower($this->noperencnaan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
         protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                        }
            }
            return true;
        }
        
        public function getRuanganItems()
        {
            return RuanganM::model()->findAll('ruangan_aktif=TRUE AND instalasi_id='.Yii::app()->user->getState('instalasi_id').' ORDER BY ruangan_nama') ;
        }
}