<?php

/**
 * This is the model class for table "laporanpasienpulang_v".
 *
 * The followings are the available columns in table 'laporanpasienpulang_v':
 * @property string $no_pendaftaran
 * @property string $no_rekam_medik
 * @property string $nama_pasien
 * @property integer $pasien_id
 * @property string $no_telepon_pasien
 * @property integer $pasienpulang_id
 * @property integer $pendaftaran_id
 * @property string $tglpasienpulang
 * @property string $keterangankeluar
 * @property string $kondisipulang
 * @property string $carakeluar
 * @property integer $dokterpemeriksa
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property string $gelarbelakang_nama
 * @property integer $instalasi_id
 * @property string $instalasi_nama
 * @property integer $ruangan_id
 * @property string $subspesialistik
 */
class LaporanpasienpulangV2 extends CActiveRecord
{


	 	public $tglAwal;
        public $tglAkhir;
        public $jumlah;
        public $data;
        public $tick;
        public $pilihanx;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanpasienpulangV the static model class
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanpasienpulang_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pasien_id, pasienpulang_id, pendaftaran_id, dokterpemeriksa, instalasi_id, ruangan_id', 'numerical', 'integerOnly'=>true),
			array('no_pendaftaran', 'length', 'max'=>20),
			array('no_rekam_medik, gelardepan', 'length', 'max'=>10),
			array('nama_pasien, carakeluar, nama_pegawai, instalasi_nama, subspesialistik', 'length', 'max'=>50),
			array('no_telepon_pasien, gelarbelakang_nama', 'length', 'max'=>15),
			array('tglpasienpulang, keterangankeluar, kondisipulang', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('no_pendaftaran, no_rekam_medik, nama_pasien, pasien_id, no_telepon_pasien, pasienpulang_id, pendaftaran_id, tglpasienpulang, keterangankeluar, kondisipulang, carakeluar, dokterpemeriksa, gelardepan, nama_pegawai, gelarbelakang_nama, instalasi_id, instalasi_nama, ruangan_id, subspesialistik', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_pendaftaran' => 'No Pendaftaran',
			'no_rekam_medik' => 'No Rekam Medik',
			'nama_pasien' => 'Nama Pasien',
			'pasien_id' => 'Pasien',
			'no_telepon_pasien' => 'No Telepon Pasien',
			'pasienpulang_id' => 'Pasien pulang',
			'pendaftaran_id' => 'Pendaftaran',
			'tglpasienpulang' => 'Tgl pasien pulang',
			'keterangankeluar' => 'Keterangan keluar',
			'kondisipulang' => 'Kondisi pulang',
			'carakeluar' => 'Cara keluar',
			'dokterpemeriksa' => 'Dokter pemeriksa',
			'gelardepan' => 'Gelar depan',
			'nama_pegawai' => 'Dokter Pemeriksa',
			'gelarbelakang_nama' => 'Gelar belakang Nama',
			'instalasi_id' => 'Instalasi',
			'instalasi_nama' => 'Instalasi',
			'ruangan_id' => 'Ruangan',
			'subspesialistik' => 'Subspesialistik',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no_pendaftaran',$this->no_pendaftaran,true);
		$criteria->compare('no_rekam_medik',$this->no_rekam_medik,true);
		$criteria->compare('nama_pasien',$this->nama_pasien,true);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('no_telepon_pasien',$this->no_telepon_pasien,true);
		$criteria->compare('pasienpulang_id',$this->pasienpulang_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('tglpasienpulang',$this->tglpasienpulang,true);
		$criteria->compare('keterangankeluar',$this->keterangankeluar,true);
		$criteria->compare('kondisipulang',$this->kondisipulang,true);
		$criteria->compare('carakeluar',$this->carakeluar,true);
		$criteria->compare('dokterpemeriksa',$this->dokterpemeriksa);
		$criteria->compare('gelardepan',$this->gelardepan,true);
		$criteria->compare('nama_pegawai',$this->nama_pegawai,true);
		$criteria->compare('gelarbelakang_nama',$this->gelarbelakang_nama,true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('instalasi_nama',$this->instalasi_nama,true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('subspesialistik',$this->subspesialistik,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}