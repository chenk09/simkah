<?php

/**
 * This is the model class for table "rekapgajipegawai_v".
 *
 * The followings are the available columns in table 'rekapgajipegawai_v':
 * @property integer $penggajianpeg_id
 * @property string $periodegaji
 * @property integer $pegawai_id
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property string $nama_keluarga
 * @property string $tglpenggajian
 * @property string $nopenggajian
 * @property string $statusperkawinan
 * @property string $jeniskelamin
 * @property integer $nourutgaji
 * @property string $komponengaji_kode
 * @property string $komponengaji_nama
 * @property string $komponengaji_singkt
 * @property boolean $ispotongan
 * @property double $jumlah
 * @property string $keterangan
 * @property string $mengetahui
 * @property string $menyetujui
 * @property integer $pengeluaranumum_id
 * @property string $nomorindukpegawai
 * @property integer $komponengaji_id
 * @property string $kategoripegawai
 */
class RekapgajipegawaiV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RekapgajipegawaiV the static model class
	 */
	public $periodegaji_view;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rekapgajipegawai_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penggajianpeg_id, pegawai_id, nourutgaji, pengeluaranumum_id, komponengaji_id', 'numerical', 'integerOnly'=>true),
			array('jumlah', 'numerical'),
			array('gelardepan', 'length', 'max'=>10),
			array('nama_pegawai, nama_keluarga, nopenggajian, komponengaji_kode', 'length', 'max'=>50),
			array('statusperkawinan, jeniskelamin, komponengaji_singkt', 'length', 'max'=>20),
			array('komponengaji_nama, mengetahui, menyetujui', 'length', 'max'=>100),
			array('nomorindukpegawai', 'length', 'max'=>30),
			array('kategoripegawai', 'length', 'max'=>128),
			array('periodegaji, tglpenggajian, ispotongan, keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penggajianpeg_id, periodegaji, pegawai_id, gelardepan, nama_pegawai, nama_keluarga, tglpenggajian, nopenggajian, statusperkawinan, jeniskelamin, nourutgaji, komponengaji_kode, komponengaji_nama, komponengaji_singkt, ispotongan, jumlah, keterangan, mengetahui, menyetujui, pengeluaranumum_id, nomorindukpegawai, komponengaji_id, kategoripegawai', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pegawai' => array(self::BELONGS_TO, 'PegawaiM', 'pegawai_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penggajianpeg_id' => 'Penggajianpeg',
			'periodegaji' => 'Periode Gaji',
			'pegawai_id' => 'Pegawai',
			'gelardepan' => 'Gelardepan',
			'nama_pegawai' => 'Nama Pegawai',
			'nama_keluarga' => 'Nama Keluarga',
			'tglpenggajian' => 'Tglpenggajian',
			'nopenggajian' => 'Nopenggajian',
			'statusperkawinan' => 'Statusperkawinan',
			'jeniskelamin' => 'Jeniskelamin',
			'nourutgaji' => 'Nourutgaji',
			'komponengaji_kode' => 'Komponengaji Kode',
			'komponengaji_nama' => 'Komponengaji Nama',
			'komponengaji_singkt' => 'Komponengaji Singkt',
			'ispotongan' => 'Ispotongan',
			'jumlah' => 'Jumlah',
			'keterangan' => 'Keterangan',
			'mengetahui' => 'Mengetahui',
			'menyetujui' => 'Menyetujui',
			'pengeluaranumum_id' => 'Pengeluaranumum',
			'nomorindukpegawai' => 'NRK',
			'komponengaji_id' => 'Komponengaji',
			'kategoripegawai' => 'Kategoripegawai',
			'gapok' => 'Gaji Pokok',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->select = "periodegaji, nama_pegawai, penggajianpeg_id, pegawai_id, keterangan";
        $criteria->group = 'periodegaji, penggajianpeg_id, nama_pegawai, pegawai_id, keterangan';
		$criteria->compare('penggajianpeg_id',$this->penggajianpeg_id);
		$criteria->compare('date(periodegaji)',$this->periodegaji);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('LOWER(tglpenggajian)',strtolower($this->tglpenggajian),true);
		$criteria->compare('LOWER(nopenggajian)',strtolower($this->nopenggajian),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('nourutgaji',$this->nourutgaji);
		$criteria->compare('LOWER(komponengaji_kode)',strtolower($this->komponengaji_kode),true);
		$criteria->compare('LOWER(komponengaji_nama)',strtolower($this->komponengaji_nama),true);
		$criteria->compare('LOWER(komponengaji_singkt)',strtolower($this->komponengaji_singkt),true);
		$criteria->compare('ispotongan',$this->ispotongan);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('LOWER(mengetahui)',strtolower($this->mengetahui),true);
		$criteria->compare('LOWER(menyetujui)',strtolower($this->menyetujui),true);
		$criteria->compare('pengeluaranumum_id',$this->pengeluaranumum_id);
		$criteria->compare('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai),true);
		$criteria->compare('komponengaji_id',$this->komponengaji_id);
		$criteria->compare('LOWER(kategoripegawai)',strtolower($this->kategoripegawai),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penggajianpeg_id',$this->penggajianpeg_id);
		$criteria->compare('LOWER(periodegaji)',strtolower($this->periodegaji),true);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('LOWER(tglpenggajian)',strtolower($this->tglpenggajian),true);
		$criteria->compare('LOWER(nopenggajian)',strtolower($this->nopenggajian),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('nourutgaji',$this->nourutgaji);
		$criteria->compare('LOWER(komponengaji_kode)',strtolower($this->komponengaji_kode),true);
		$criteria->compare('LOWER(komponengaji_nama)',strtolower($this->komponengaji_nama),true);
		$criteria->compare('LOWER(komponengaji_singkt)',strtolower($this->komponengaji_singkt),true);
		$criteria->compare('ispotongan',$this->ispotongan);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('LOWER(mengetahui)',strtolower($this->mengetahui),true);
		$criteria->compare('LOWER(menyetujui)',strtolower($this->menyetujui),true);
		$criteria->compare('pengeluaranumum_id',$this->pengeluaranumum_id);
		$criteria->compare('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai),true);
		$criteria->compare('komponengaji_id',$this->komponengaji_id);
		$criteria->compare('LOWER(kategoripegawai)',strtolower($this->kategoripegawai),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }

    public function getPeriode()
    {
    	$format     = new CustomFormat;
        $periode    = explode('-', $this->periodegaji);
        $periode_bulan  = $periode[1];
        $periode_tahun  = $periode[0];
        $bulan      = $format->formatBulanInaLengkap($periode_bulan);

        return $bulan.' '.$periode_tahun;
    }

    public function getGapok()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('gaji_pokok') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getHarian()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_harian') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getJabatan()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_jabatan') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getFungsional()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_fungsional') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getKhusus()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_khusus') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getSosial()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_sosial') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getPph21()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('pph_pasal21')));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getEssensial()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_essensial') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getPensiun()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_pensiun') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getKesehatan()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_kesehatan') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getKompetensi()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_kompetensi') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getKemahalan()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tunjangan_kemahalan') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getInsentif()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('insentif') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getJasa()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('jasa_lainnya') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getLembur()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('lembur') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getGajikotor()
    {	
		$nilai = $this->getLembur() + $this->getJasa() + $this->getInsentif() + $this->getKemahalan() + $this->getKompetensi() + $this->getKesehatan() + $this->getPensiun() + $this->getEssensial() + $this->getPph21() + $this->getSosial() + $this->getKhusus() + $this->getFungsional() + $this->getJabatan() + $this->getHarian() + $this->getGapok();
		return $nilai;
    }

    public function getKoperasi()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('koperasi') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getJamsostek()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('jamsostek') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getPotPensiun()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('potongan_pensiun') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getPotKesehatan()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('potongan_kesehatan') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getPotBRI()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('bri') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getPotBSJ()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('bsj') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getPotBTN()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('btn') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getPotIntern()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('potongan_intern') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getApotek()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('apotek') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getLab()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('laboratorium') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getTmkrj()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('tmkrj') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getPajak()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('pajak') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getLain2()
    {
		$gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>Params::komponenGaji('lain_lain') ));
		$nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		return $nilai;
    }

    public function getTotalPotongan()
    {
		$nilai = $this->getLain2() + $this->getPajak() + $this->getTmkrj() + $this->getLab() + $this->getApotek() + $this->getPotIntern() + $this->getPotBTN() + $this->getPotBSJ() + $this->getPotBRI() + $this->getPotKesehatan() + $this->getPotPensiun() + $this->getJamsostek() + $this->getKoperasi();
		return $nilai;
    }

    public function getGajiBersih()
    {
    	$nilai = $this->getGajikotor() - $this->getTotalPotongan();
    	return $nilai;	
    }

  //   public function getPotongan()
  //   {
		// $gapok = RekapgajipegawaiV::model()->findbyAttributes(array('penggajianpeg_id'=>$this->penggajianpeg_id, 'komponengaji_id'=>31 ));
		// $nilai = isset($gapok->jumlah) ? $gapok->jumlah : 0;
		// return $nilai;
  //   }
}