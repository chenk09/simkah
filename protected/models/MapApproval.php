<?php

/**
 * This is the model class for table "inbox.map_approval".
 *
 * The followings are the available columns in table 'inbox.map_approval':
 * @property integer $id_approver
 * @property string $approver
 * @property integer $idx
 * @property integer $app_id
 * @property string $keterangan
 */
class MapApproval extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MapApproval the static model class
	 */
	public $userid;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inbox.map_approval';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_approver', 'required'),
			array('id_approver, idx, app_id', 'numerical', 'integerOnly'=>true),
			array('approver, keterangan', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_approver, approver, idx, app_id, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_approver' => 'Id Approver',
			'approver' => 'Approver',
			'idx' => 'Idx',
			'app_id' => 'App',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_approver',$this->id_approver);
		$criteria->compare('approver',$this->approver,true);
		$criteria->compare('idx',$this->idx);
		$criteria->compare('app_id',$this->app_id);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}