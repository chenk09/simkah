<?php

/**
 * This is the model class for table "pegawaidiklat_t".
 *
 * The followings are the available columns in table 'pegawaidiklat_t':
 * @property integer $pegawaidiklat_id
 * @property integer $jenisdiklat_id
 * @property integer $pegawai_id
 * @property string $pegawaidiklat_nama
 * @property string $pegawaidiklat_namalainnya
 * @property string $pegawaidiklat_lamanya
 * @property string $pegawaidiklat_tahun
 * @property string $pegawaidiklat_tempat
 * @property string $nomorkeputusandiklat
 * @property string $tglditetapkandiklat
 * @property string $pejabatygmemdiklat
 * @property string $pegawaidiklat_keterangan
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class PegawaidiklatT extends CActiveRecord
{
                public $no;
                public $pegawaidiklat_lamanyasatuan;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PegawaidiklatT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pegawaidiklat_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenisdiklat_id, pegawai_id, create_time, create_loginpemakai_id, create_ruangan', 'required'),
			array('jenisdiklat_id, pegawai_id', 'numerical', 'integerOnly'=>true),
			array('pegawaidiklat_nama, pegawaidiklat_namalainnya, nomorkeputusandiklat, pejabatygmemdiklat, pegawaidiklat_keterangan', 'length', 'max'=>50),
			array('pegawaidiklat_lamanya', 'length', 'max'=>10),
                        array('pegawaidiklat_tempat', 'length', 'max'=>30),
                        array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
			array('pegawaidiklat_tahun, tglditetapkandiklat, update_time, update_loginpemakai_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pegawaidiklat_id, jenisdiklat_id, pegawai_id, pegawaidiklat_nama, pegawaidiklat_namalainnya, pegawaidiklat_lamanya, pegawaidiklat_tahun, pegawaidiklat_tempat, nomorkeputusandiklat, tglditetapkandiklat, pejabatygmemdiklat, pegawaidiklat_keterangan, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                                    'jenisdiklat'=>array(self::BELONGS_TO,'JenisdiklatM','jenisdiklat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pegawaidiklat_id' => 'Pegawaidiklat',
			'jenisdiklat_id' => 'Jenisdiklat',
			'pegawai_id' => 'Pegawai',
			'pegawaidiklat_nama' => 'Pegawaidiklat Nama',
			'pegawaidiklat_namalainnya' => 'Pegawaidiklat Namalainnya',
			'pegawaidiklat_lamanya' => 'Pegawaidiklat Lamanya',
			'pegawaidiklat_tahun' => 'Pegawaidiklat Tahun',
			'pegawaidiklat_tempat' => 'Pegawaidiklat Tempat',
			'nomorkeputusandiklat' => 'Nomorkeputusandiklat',
			'tglditetapkandiklat' => 'Tglditetapkandiklat',
			'pejabatygmemdiklat' => 'Pejabatygmemdiklat',
			'pegawaidiklat_keterangan' => 'Pegawaidiklat Keterangan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pegawaidiklat_id',$this->pegawaidiklat_id);
		$criteria->compare('jenisdiklat_id',$this->jenisdiklat_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(pegawaidiklat_nama)',strtolower($this->pegawaidiklat_nama),true);
		$criteria->compare('LOWER(pegawaidiklat_namalainnya)',strtolower($this->pegawaidiklat_namalainnya),true);
		$criteria->compare('LOWER(pegawaidiklat_lamanya)',strtolower($this->pegawaidiklat_lamanya),true);
		$criteria->compare('LOWER(pegawaidiklat_tahun)',strtolower($this->pegawaidiklat_tahun),true);
		$criteria->compare('LOWER(pegawaidiklat_tempat)',strtolower($this->pegawaidiklat_tempat),true);
		$criteria->compare('LOWER(nomorkeputusandiklat)',strtolower($this->nomorkeputusandiklat),true);
		$criteria->compare('LOWER(tglditetapkandiklat)',strtolower($this->tglditetapkandiklat),true);
		$criteria->compare('LOWER(pejabatygmemdiklat)',strtolower($this->pejabatygmemdiklat),true);
		$criteria->compare('LOWER(pegawaidiklat_keterangan)',strtolower($this->pegawaidiklat_keterangan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pegawaidiklat_id',$this->pegawaidiklat_id);
		$criteria->compare('jenisdiklat_id',$this->jenisdiklat_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(pegawaidiklat_nama)',strtolower($this->pegawaidiklat_nama),true);
		$criteria->compare('LOWER(pegawaidiklat_namalainnya)',strtolower($this->pegawaidiklat_namalainnya),true);
		$criteria->compare('LOWER(pegawaidiklat_lamanya)',strtolower($this->pegawaidiklat_lamanya),true);
		$criteria->compare('LOWER(pegawaidiklat_tahun)',strtolower($this->pegawaidiklat_tahun),true);
		$criteria->compare('LOWER(pegawaidiklat_tempat)',strtolower($this->pegawaidiklat_tempat),true);
		$criteria->compare('LOWER(nomorkeputusandiklat)',strtolower($this->nomorkeputusandiklat),true);
		$criteria->compare('LOWER(tglditetapkandiklat)',strtolower($this->tglditetapkandiklat),true);
		$criteria->compare('LOWER(pejabatygmemdiklat)',strtolower($this->pejabatygmemdiklat),true);
		$criteria->compare('LOWER(pegawaidiklat_keterangan)',strtolower($this->pegawaidiklat_keterangan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function getJenisdiklatItems() {
            return JenisdiklatM::model()->findAll('jenisdiklat_aktif=TRUE ORDER BY jenisdiklat_nama');
        }

        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                if ($column->dbType == 'date'){
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                }elseif ($column->dbType == 'timestamp without time zone'){
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                            CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                }
            }

            return parent::beforeValidate ();
        }

        public function beforeSave() {          
            if($this->tglditetapkandiklat===null || trim($this->tglditetapkandiklat)==''){
	        $this->setAttribute('tglditetapkandiklat', null);
            }
            if($this->pegawaidiklat_tahun===null || trim($this->pegawaidiklat_tahun)==''){
	        $this->setAttribute('pegawaidiklat_tahun', null);
            }
            return parent::beforeSave();
        }
                
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                if (!strlen($this->$columnName)) continue;
                if ($column->dbType == 'date'){
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                }elseif ($column->dbType == 'timestamp without time zone'){
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                            CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                }
            }
            return true;
        }        
        
}