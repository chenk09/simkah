<?php

/**
 * This is the model class for table "informasipelamar_v".
 *
 * The followings are the available columns in table 'informasipelamar_v':
 * @property integer $pelamar_id
 * @property integer $pendkualifikasi_id
 * @property string $pendkualifikasi_kode
 * @property string $pendkualifikasi_nama
 * @property string $pendkualifikasi_keterangan
 * @property integer $profilrs_id
 * @property string $nama_rumahsakit
 * @property integer $suku_id
 * @property string $suku_nama
 * @property integer $pendidikan_id
 * @property string $pendidikan_nama
 * @property string $tgllowongan
 * @property string $jenisidentitas
 * @property string $noidentitas
 * @property string $nama_pelamar
 * @property string $nama_keluarga
 * @property string $tempatlahir_pelamar
 * @property string $tgl_lahirpelamar
 * @property string $jeniskelamin
 * @property string $statusperkawinan
 * @property integer $jmlanak
 * @property string $alamat_pelamar
 * @property string $kodepos
 * @property string $agama
 * @property string $alamatemail
 * @property string $notelp_pelamar
 * @property string $nomobile_pelamar
 * @property string $warganegara_pelamar
 * @property string $photopelamar
 * @property double $gajiygdiharapkan
 * @property string $tglditerima
 * @property string $tglmulaibekerja
 * @property string $ingintunjangan
 * @property string $keterangan_pelamar
 * @property string $minatpekerjaan
 * @property string $filelamaran
 */
class InformasipelamarV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InformasipelamarV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'informasipelamar_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pelamar_id, pendkualifikasi_id, profilrs_id, suku_id, pendidikan_id, jmlanak', 'numerical', 'integerOnly'=>true),
			array('gajiygdiharapkan', 'numerical'),
			array('pendkualifikasi_kode', 'length', 'max'=>10),
			array('pendkualifikasi_nama, suku_nama, pendidikan_nama, nama_keluarga, kodepos, notelp_pelamar, nomobile_pelamar', 'length', 'max'=>50),
			array('nama_rumahsakit, noidentitas, nama_pelamar, alamatemail, minatpekerjaan', 'length', 'max'=>100),
			array('jenisidentitas, jeniskelamin, statusperkawinan, agama', 'length', 'max'=>20),
			array('tempatlahir_pelamar', 'length', 'max'=>30),
			array('warganegara_pelamar', 'length', 'max'=>25),
			array('photopelamar', 'length', 'max'=>200),
			array('pendkualifikasi_keterangan, tgllowongan, tgl_lahirpelamar, alamat_pelamar, tglditerima, tglmulaibekerja, ingintunjangan, keterangan_pelamar, filelamaran', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pelamar_id, pendkualifikasi_id, pendkualifikasi_kode, pendkualifikasi_nama, pendkualifikasi_keterangan, profilrs_id, nama_rumahsakit, suku_id, suku_nama, pendidikan_id, pendidikan_nama, tgllowongan, jenisidentitas, noidentitas, nama_pelamar, nama_keluarga, tempatlahir_pelamar, tgl_lahirpelamar, jeniskelamin, statusperkawinan, jmlanak, alamat_pelamar, kodepos, agama, alamatemail, notelp_pelamar, nomobile_pelamar, warganegara_pelamar, photopelamar, gajiygdiharapkan, tglditerima, tglmulaibekerja, ingintunjangan, keterangan_pelamar, minatpekerjaan, filelamaran', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pelamar_id' => 'Pelamar',
			'pendkualifikasi_id' => 'Pendkualifikasi',
			'pendkualifikasi_kode' => 'Pendkualifikasi Kode',
			'pendkualifikasi_nama' => 'Pendkualifikasi Nama',
			'pendkualifikasi_keterangan' => 'Pendkualifikasi Keterangan',
			'profilrs_id' => 'Profilrs',
			'nama_rumahsakit' => 'Nama Rumahsakit',
			'suku_id' => 'Suku',
			'suku_nama' => 'Suku Nama',
			'pendidikan_id' => 'Pendidikan',
			'pendidikan_nama' => 'Pendidikan Nama',
			'tgllowongan' => 'Tgllowongan',
			'jenisidentitas' => 'Jenisidentitas',
			'noidentitas' => 'Noidentitas',
			'nama_pelamar' => 'Nama Pelamar',
			'nama_keluarga' => 'Nama Keluarga',
			'tempatlahir_pelamar' => 'Tempatlahir Pelamar',
			'tgl_lahirpelamar' => 'Tgl Lahirpelamar',
			'jeniskelamin' => 'Jeniskelamin',
			'statusperkawinan' => 'Statusperkawinan',
			'jmlanak' => 'Jmlanak',
			'alamat_pelamar' => 'Alamat Pelamar',
			'kodepos' => 'Kodepos',
			'agama' => 'Agama',
			'alamatemail' => 'Alamatemail',
			'notelp_pelamar' => 'Notelp Pelamar',
			'nomobile_pelamar' => 'Nomobile Pelamar',
			'warganegara_pelamar' => 'Warganegara Pelamar',
			'photopelamar' => 'Photopelamar',
			'gajiygdiharapkan' => 'Gajiygdiharapkan',
			'tglditerima' => 'Tglditerima',
			'tglmulaibekerja' => 'Tglmulaibekerja',
			'ingintunjangan' => 'Ingintunjangan',
			'keterangan_pelamar' => 'Keterangan Pelamar',
			'minatpekerjaan' => 'Minatpekerjaan',
			'filelamaran' => 'Filelamaran',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pelamar_id',$this->pelamar_id);
		$criteria->compare('pendkualifikasi_id',$this->pendkualifikasi_id);
		$criteria->compare('LOWER(pendkualifikasi_kode)',strtolower($this->pendkualifikasi_kode),true);
		$criteria->compare('LOWER(pendkualifikasi_nama)',strtolower($this->pendkualifikasi_nama),true);
		$criteria->compare('LOWER(pendkualifikasi_keterangan)',strtolower($this->pendkualifikasi_keterangan),true);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('LOWER(nama_rumahsakit)',strtolower($this->nama_rumahsakit),true);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('LOWER(suku_nama)',strtolower($this->suku_nama),true);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('LOWER(pendidikan_nama)',strtolower($this->pendidikan_nama),true);
		$criteria->compare('LOWER(tgllowongan)',strtolower($this->tgllowongan),true);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(noidentitas)',strtolower($this->noidentitas),true);
		$criteria->compare('LOWER(nama_pelamar)',strtolower($this->nama_pelamar),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('LOWER(tempatlahir_pelamar)',strtolower($this->tempatlahir_pelamar),true);
		$criteria->compare('LOWER(tgl_lahirpelamar)',strtolower($this->tgl_lahirpelamar),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('jmlanak',$this->jmlanak);
		$criteria->compare('LOWER(alamat_pelamar)',strtolower($this->alamat_pelamar),true);
		$criteria->compare('LOWER(kodepos)',strtolower($this->kodepos),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(notelp_pelamar)',strtolower($this->notelp_pelamar),true);
		$criteria->compare('LOWER(nomobile_pelamar)',strtolower($this->nomobile_pelamar),true);
		$criteria->compare('LOWER(warganegara_pelamar)',strtolower($this->warganegara_pelamar),true);
		$criteria->compare('LOWER(photopelamar)',strtolower($this->photopelamar),true);
		$criteria->compare('gajiygdiharapkan',$this->gajiygdiharapkan);
		$criteria->compare('LOWER(tglditerima)',strtolower($this->tglditerima),true);
		$criteria->compare('LOWER(tglmulaibekerja)',strtolower($this->tglmulaibekerja),true);
		$criteria->compare('LOWER(ingintunjangan)',strtolower($this->ingintunjangan),true);
		$criteria->compare('LOWER(keterangan_pelamar)',strtolower($this->keterangan_pelamar),true);
		$criteria->compare('LOWER(minatpekerjaan)',strtolower($this->minatpekerjaan),true);
		$criteria->compare('LOWER(filelamaran)',strtolower($this->filelamaran),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pelamar_id',$this->pelamar_id);
		$criteria->compare('pendkualifikasi_id',$this->pendkualifikasi_id);
		$criteria->compare('LOWER(pendkualifikasi_kode)',strtolower($this->pendkualifikasi_kode),true);
		$criteria->compare('LOWER(pendkualifikasi_nama)',strtolower($this->pendkualifikasi_nama),true);
		$criteria->compare('LOWER(pendkualifikasi_keterangan)',strtolower($this->pendkualifikasi_keterangan),true);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('LOWER(nama_rumahsakit)',strtolower($this->nama_rumahsakit),true);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('LOWER(suku_nama)',strtolower($this->suku_nama),true);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('LOWER(pendidikan_nama)',strtolower($this->pendidikan_nama),true);
		$criteria->compare('LOWER(tgllowongan)',strtolower($this->tgllowongan),true);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(noidentitas)',strtolower($this->noidentitas),true);
		$criteria->compare('LOWER(nama_pelamar)',strtolower($this->nama_pelamar),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('LOWER(tempatlahir_pelamar)',strtolower($this->tempatlahir_pelamar),true);
		$criteria->compare('LOWER(tgl_lahirpelamar)',strtolower($this->tgl_lahirpelamar),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('jmlanak',$this->jmlanak);
		$criteria->compare('LOWER(alamat_pelamar)',strtolower($this->alamat_pelamar),true);
		$criteria->compare('LOWER(kodepos)',strtolower($this->kodepos),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(notelp_pelamar)',strtolower($this->notelp_pelamar),true);
		$criteria->compare('LOWER(nomobile_pelamar)',strtolower($this->nomobile_pelamar),true);
		$criteria->compare('LOWER(warganegara_pelamar)',strtolower($this->warganegara_pelamar),true);
		$criteria->compare('LOWER(photopelamar)',strtolower($this->photopelamar),true);
		$criteria->compare('gajiygdiharapkan',$this->gajiygdiharapkan);
		$criteria->compare('LOWER(tglditerima)',strtolower($this->tglditerima),true);
		$criteria->compare('LOWER(tglmulaibekerja)',strtolower($this->tglmulaibekerja),true);
		$criteria->compare('LOWER(ingintunjangan)',strtolower($this->ingintunjangan),true);
		$criteria->compare('LOWER(keterangan_pelamar)',strtolower($this->keterangan_pelamar),true);
		$criteria->compare('LOWER(minatpekerjaan)',strtolower($this->minatpekerjaan),true);
		$criteria->compare('LOWER(filelamaran)',strtolower($this->filelamaran),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}