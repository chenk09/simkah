<?php

/**
 * This is the model class for table "penjualanbarang_t".
 *
 * The followings are the available columns in table 'penjualanbarang_t':
 * @property integer $penjualanbarang_id
 * @property integer $pegawai_id
 * @property integer $ruangan_id
 * @property string $tglpenjualanbrg
 * @property string $nopenjualan
 * @property string $nopemesanan
 * @property string $unitpemesan
 * @property string $penerimabarang
 * @property string $keterangan
 * @property integer $profilepemesan_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $update_loginpemakai_id
 * @property integer $create_ruangan
 */
class PenjualanbarangT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PenjualanbarangT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penjualanbarang_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pegawai_id, ruangan_id, tglpenjualanbrg, nopenjualan, profilepemesan_id, create_time, create_loginpemakai_id, create_ruangan', 'required'),
			array('pegawai_id, ruangan_id, profilepemesan_id, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'numerical', 'integerOnly'=>true),
			array('nopenjualan, nopemesanan, unitpemesan', 'length', 'max'=>50),
			array('penerimabarang', 'length', 'max'=>100),
			array('keterangan', 'length', 'max'=>200),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penjualanbarang_id, pegawai_id, ruangan_id, tglpenjualanbrg, nopenjualan, nopemesanan, unitpemesan, penerimabarang, keterangan, profilepemesan_id, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ruangan'=>array(self::BELONGS_TO, 'RuanganM', 'ruangan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penjualanbarang_id' => 'Penjualan Barang',
			'pegawai_id' => 'Pegawai',
			'ruangan_id' => 'Ruangan',
			'tglpenjualanbrg' => 'Tgl Penjualan',
			'nopenjualan' => 'No. Penjualan',
			'nopemesanan' => 'No. Pemesanan',
			'unitpemesan' => 'Unit Pemesan',
			'penerimabarang' => 'Penerima Barang',
			'keterangan' => 'Keterangan',
			'profilepemesan_id' => 'Profil Pemesan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penjualanbarang_id',$this->penjualanbarang_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(tglpenjualanbrg)',strtolower($this->tglpenjualanbrg),true);
		$criteria->compare('LOWER(nopenjualan)',strtolower($this->nopenjualan),true);
		$criteria->compare('LOWER(nopemesanan)',strtolower($this->nopemesanan),true);
		$criteria->compare('LOWER(unitpemesan)',strtolower($this->unitpemesan),true);
		$criteria->compare('LOWER(penerimabarang)',strtolower($this->penerimabarang),true);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('profilepemesan_id',$this->profilepemesan_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penjualanbarang_id',$this->penjualanbarang_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(tglpenjualanbrg)',strtolower($this->tglpenjualanbrg),true);
		$criteria->compare('LOWER(nopenjualan)',strtolower($this->nopenjualan),true);
		$criteria->compare('LOWER(nopemesanan)',strtolower($this->nopemesanan),true);
		$criteria->compare('LOWER(unitpemesan)',strtolower($this->unitpemesan),true);
		$criteria->compare('LOWER(penerimabarang)',strtolower($this->penerimabarang),true);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('profilepemesan_id',$this->profilepemesan_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}