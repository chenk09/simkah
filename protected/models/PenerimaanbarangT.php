<?php

/**
 * This is the model class for table "penerimaanbarang_t".
 *
 * The followings are the available columns in table 'penerimaanbarang_t':
 * @property integer $penerimaanbarang_id
 * @property integer $supplier_id
 * @property integer $permintaanpembelian_id
 * @property integer $fakturpembelian_id
 * @property string $noterima
 * @property string $tglterima
 * @property string $tglterimafaktur
 * @property string $nosuratjalan
 * @property string $tglsuratjalan
 * @property integer $gudangpenerima_id
 * @property string $keteranganterima
 * @property double $harganetto
 * @property double $jmldiscount
 * @property double $persendiscount
 * @property double $totalpajakppn
 * @property double $totalpajakpph
 * @property double $totalharga
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class PenerimaanbarangT extends CActiveRecord
{
        public $tglAwal;
        public $tglAkhir;
        public $ruangan_id;
        public $hargabruto;
        public $jenisobatalkes_id, $sumberdana_id;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PenerimaanbarangT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penerimaanbarang_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('supplier_id, noterima', 'required'),
			array('supplier_id, permintaanpembelian_id, fakturpembelian_id, gudangpenerima_id', 'numerical', 'integerOnly'=>true),
			array('harganetto, jmldiscount, persendiscount, totalpajakppn, totalpajakpph, totalharga', 'numerical'),
			array('noterima', 'length', 'max'=>20),
			array('nosuratjalan', 'length', 'max'=>50),
			array('tglterima, tglterimafaktur, tglsuratjalan, keteranganterima, update_time, update_loginpemakai_id, returpembelian_id', 'safe'),
//			array('tglterima','setValidation', 'on'=>'insert'),
                        array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
                        array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
			
                        array('supplier_nama, penerimaanbarang_id, supplier_id, sumberdana_id, jenisobatalkes_id, permintaanpembelian_id, returpembelian_id, fakturpembelian_id, noterima, tglterima, tglterimafaktur, nosuratjalan, tglsuratjalan, gudangpenerima_id, keteranganterima, harganetto, jmldiscount, persendiscount, totalpajakppn, totalpajakpph, totalharga, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}
        
        public function setValidation(){
            if (!$this->hasErrors()){
                if (date('Y-m-d',strtotime($this->tglterima)) != date('Y-m-d')){
                    $this->addError("tglterima",'Tanggal terima tidak boleh lebih atau kurang dari '.date('d M Y'));
                }
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'supplier'=>array(self::BELONGS_TO, 'SupplierM','supplier_id'),
                    'permintaanpembelian'=>array(self::BELONGS_TO,'PermintaanpembelianT','permintaanpembelian_id'),
                    'loginpemakai'=>array(self::BELONGS_TO,'LoginpemakaiK','create_loginpemakai_id'),
                    'fakturpembelian'=>array(self::BELONGS_TO,'FakturpembelianT','fakturpembelian_id'),
                    'ruangan'=>array(self::BELONGS_TO, 'RuanganM','create_ruangan'),
                    'returdetail'=>array(self::BELONGS_TO, 'ReturdetailT','penerimaanbarang_id'),
                    'retur'=>array(self::BELONGS_TO, 'ReturpembelianT','returpembelian_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
                    'penerimaanbarang_id' => 'Penerimaanbarang',
                    'supplier_id' => 'Supplier',
                    'permintaanpembelian_id' => 'Permintaanpembelian',
                    'fakturpembelian_id' => 'Fakturpembelian',
                    'noterima' => 'No Penerimaan',
                    'tglterima' => 'Receive Date',
                    'tglterimafaktur' => 'Tglterimafaktur',
                    'nosuratjalan' => 'No Surat Jalan',
                    'tglsuratjalan' => 'Tanggal',
                    'gudangpenerima_id' => 'Gudangpenerima',
                    'keteranganterima' => 'Keterangan',
                    'harganetto' => 'Price/Unit',
                    'jmldiscount' => 'Discount',
                    'persendiscount' => 'Persen Discount',
                    'totalpajakppn' => 'RCV PPN',
                    'totalpajakpph' => 'RCV PPH',
                    'totalharga' => 'Total',
                    'create_time' => 'Create Time',
                    'update_time' => 'Update Time',
                    'create_loginpemakai_id' => 'Create Loginpemakai',
                    'update_loginpemakai_id' => 'Update Loginpemakai',
                    'create_ruangan' => 'Create Ruangan',
                    'jenisobatalkes_id'=>'Jenis Obat Alkes',
                    'sumberdana_id'=>'Sumber Dana',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('LOWER(noterima)',strtolower($this->noterima),true);
		$criteria->compare('LOWER(tglterima)',strtolower($this->tglterima),true);
		$criteria->compare('LOWER(tglterimafaktur)',strtolower($this->tglterimafaktur),true);
		$criteria->compare('LOWER(nosuratjalan)',strtolower($this->nosuratjalan),true);
		$criteria->compare('LOWER(tglsuratjalan)',strtolower($this->tglsuratjalan),true);
		$criteria->compare('gudangpenerima_id',$this->gudangpenerima_id);
		$criteria->compare('LOWER(keteranganterima)',strtolower($this->keteranganterima),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('totalpajakppn',$this->totalpajakppn);
		$criteria->compare('totalpajakpph',$this->totalpajakpph);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('LOWER(noterima)',strtolower($this->noterima),true);
		$criteria->compare('LOWER(tglterima)',strtolower($this->tglterima),true);
		$criteria->compare('LOWER(tglterimafaktur)',strtolower($this->tglterimafaktur),true);
		$criteria->compare('LOWER(nosuratjalan)',strtolower($this->nosuratjalan),true);
		$criteria->compare('LOWER(tglsuratjalan)',strtolower($this->tglsuratjalan),true);
		$criteria->compare('gudangpenerima_id',$this->gudangpenerima_id);
		$criteria->compare('LOWER(keteranganterima)',strtolower($this->keteranganterima),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('totalpajakppn',$this->totalpajakppn);
		$criteria->compare('totalpajakpph',$this->totalpajakpph);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function getSupplierItems()
        {
            return SupplierM::model()->findAll("supplier_aktif=TRUE AND supplier_jenis ilike '%".PARAMS::DEFAULT_JENIS_SUPPLIER."%' ORDER BY supplier_nama");
        }
        
        public function getSyaratBayarItems()
        {
            return SyaratbayarM::model()->findAll('syaratbayar_aktif=TRUE ORDER BY syaratbayar_nama');
        }
        
        public function beforeSave()
        {
            if($this->tglsuratjalan === null || trim($this->tglsuratjalan) == '')
            {
	        $this->setAttribute('tglsuratjalan', null);
            }
            
            if($this->tglterimafaktur === null || trim($this->tglterimafaktur) == '')
            {
	        $this->setAttribute('tglterimafaktur', null);
            }
            return parent::beforeSave();
        }
        
        public function getSupplierforInformasi()
        {
            return SupplierM::model()->findAll("supplier_aktif=TRUE AND supplier_jenis='Farmasi' ORDER BY supplier_nama");
        }
        
        protected function beforeValidate()
        {
            $format = new CustomFormat();
            foreach($this->metadata->tableSchema->columns as $columnName => $column)
            {
                if ($column->dbType == 'date')
                {
                    $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                }elseif ($column->dbType == 'timestamp without time zone')
                {
                    $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                }
            }
            return parent::beforeValidate();
        }        
}