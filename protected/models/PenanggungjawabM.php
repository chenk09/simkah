<?php

/**
 * This is the model class for table "penanggungjawab_m".
 *
 * The followings are the available columns in table 'penanggungjawab_m':
 * @property integer $penanggungjawab_id
 * @property string $pengantar
 * @property string $jenisidentitas
 * @property string $no_identitas
 * @property string $hubungankeluarga
 * @property string $nama_pj
 * @property string $tgllahir_pj
 * @property string $jeniskelamin
 * @property string $tempatlahir_pj
 * @property string $alamat_pj
 * @property string $no_identitas_pj
 * @property string $no_teleponpj
 * @property string $no_mobilepj
 * @property boolean $penanggungjawab_aktif
 */
class PenanggungjawabM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PenanggungjawabM the static model class
	 */
	public $umur_pj;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penanggungjawab_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pengantar, nama_pj, jeniskelamin', 'required'),
			array('pengantar, no_identitas, hubungankeluarga, nama_pj', 'length', 'max'=>50),
			array('jenisidentitas, jeniskelamin, tempatlahir_pj', 'length', 'max'=>20),
			array('no_teleponpj, no_mobilepj', 'length', 'max'=>15),
			array('tgllahir_pj, alamat_pj, penanggungjawab_aktif', 'safe'),
                        array('no_teleponpj', 'match', 'pattern'=>"/^([0-9()]*)$/", 'message'=>'Hanya inputan numerik, "(", ")",dan "+" yang diperbolehkan'),
                        array('no_mobilepj', 'match', 'pattern'=>"/^([0-9+]*)$/", 'message'=>'Hanya inputan numerik dan "+" yang diperbolehkan'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penanggungjawab_id, pengantar, jenisidentitas, no_identitas, hubungankeluarga, nama_pj, tgllahir_pj, jeniskelamin, tempatlahir_pj, alamat_pj, no_teleponpj, no_mobilepj, penanggungjawab_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penanggungjawab_id' => 'ID',
			'pengantar' => 'Pengantar',
			'jenisidentitas' => 'Jenis Identitas',
			'no_identitas' => 'No Identitas',
			'hubungankeluarga' => 'Hubungan Keluarga',
			'nama_pj' => 'Nama',
			'tgllahir_pj' => 'Tgl Lahir',
			'jeniskelamin' => 'Jenis Kelamin',
			'tempatlahir_pj' => 'Tempat Lahir',
			'alamat_pj' => 'Alamat',
			'no_teleponpj' => 'No Telepon',
			'no_mobilepj' => 'No Mobile',
			'penanggungjawab_aktif' => 'Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penanggungjawab_id',$this->penanggungjawab_id);
		$criteria->compare('LOWER(pengantar)',strtolower($this->pengantar),true);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(no_identitas)',strtolower($this->no_identitas),true);
		$criteria->compare('LOWER(hubungankeluarga)',strtolower($this->hubungankeluarga),true);
		$criteria->compare('LOWER(nama_pj)',strtolower($this->nama_pj),true);
		$criteria->compare('LOWER(tgllahir_pj)',strtolower($this->tgllahir_pj),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tempatlahir_pj)',strtolower($this->tempatlahir_pj),true);
		$criteria->compare('LOWER(alamat_pj)',strtolower($this->alamat_pj),true);
		$criteria->compare('LOWER(no_teleponpj)',strtolower($this->no_teleponpj),true);
		$criteria->compare('LOWER(no_mobilepj)',strtolower($this->no_mobilepj),true);
		$criteria->compare('penanggungjawab_aktif',$this->penanggungjawab_aktif);
                $criteria->addCondition('penanggungjawab_aktif is true');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penanggungjawab_id',$this->penanggungjawab_id);
		$criteria->compare('LOWER(pengantar)',strtolower($this->pengantar),true);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(no_identitas)',strtolower($this->no_identitas),true);
		$criteria->compare('LOWER(hubungankeluarga)',strtolower($this->hubungankeluarga),true);
		$criteria->compare('LOWER(nama_pj)',strtolower($this->nama_pj),true);
		$criteria->compare('LOWER(tgllahir_pj)',strtolower($this->tgllahir_pj),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tempatlahir_pj)',strtolower($this->tempatlahir_pj),true);
		$criteria->compare('LOWER(alamat_pj)',strtolower($this->alamat_pj),true);
		$criteria->compare('LOWER(no_teleponpj)',strtolower($this->no_teleponpj),true);
		$criteria->compare('LOWER(no_mobilepj)',strtolower($this->no_mobilepj),true);
		$criteria->compare('penanggungjawab_aktif',$this->penanggungjawab_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date'){
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                    }elseif ($column->dbType == 'timestamp without time zone'){
                            $this->$columnName = date('Y-m-d H:i:s', CDateTimeParser::parse($this->$columnName, Yii::app()->locale->dateFormat));
                    }
            }

            return parent::beforeValidate ();
        }

        public function beforeSave() {    
            if($this->tgllahir_pj===null || trim($this->tgllahir_pj)==''){
	        $this->setAttribute('tgllahir_pj', null);
            }
            return parent::beforeSave();
        }

        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                        }
            }
            return true;
        }
}
