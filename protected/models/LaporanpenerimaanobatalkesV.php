<?php

/**
 * This is the model class for table "laporanpenerimaanobatalkes_v".
 *
 * The followings are the available columns in table 'laporanpenerimaanobatalkes_v':
 * @property string $noterima
 * @property string $tglterima
 * @property string $nofaktur
 * @property string $tglfaktur
 * @property double $hargabelisatuan
 * @property double $qty
 * @property double $bruto
 * @property double $persendiscount
 * @property double $nominaldiscount
 * @property integer $persenppn
 * @property double $nominalppn
 * @property double $netto
 * @property string $obatalkes_kode
 * @property string $namaobat
 * @property string $kelompokobat
 * @property string $satuanbesar_nama
 * @property integer $supplier_id
 * @property string $supplier_nama
 * @property integer $produsen_id
 * @property string $produsen_nama
 */
class LaporanpenerimaanobatalkesV extends CActiveRecord
{
public $tglAwal;
public $tglAkhir;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanpenerimaanobatalkesV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanpenerimaanobatalkes_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('persenppn, supplier_id, produsen_id', 'numerical', 'integerOnly'=>true),
			array('hargabelisatuan, qty, bruto, persendiscount, nominaldiscount, nominalppn, netto', 'numerical'),
			array('noterima, kelompokobat', 'length', 'max'=>20),
			array('nofaktur, satuanbesar_nama', 'length', 'max'=>50),
			array('obatalkes_kode, namaobat', 'length', 'max'=>200),
			array('supplier_nama, produsen_nama', 'length', 'max'=>100),
			array('tglterima, tglfaktur', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('noterima, tglterima, nofaktur, tglfaktur, hargabelisatuan, qty, bruto, persendiscount, nominaldiscount, persenppn, nominalppn, netto, obatalkes_kode, namaobat, kelompokobat, satuanbesar_nama, supplier_id, supplier_nama, produsen_id, produsen_nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'noterima' => 'Noterima',
			'tglterima' => 'Tglterima',
			'nofaktur' => 'Nofaktur',
			'tglfaktur' => 'Tglfaktur',
			'hargabelisatuan' => 'Hargabelisatuan',
			'qty' => 'Qty',
			'bruto' => 'Bruto',
			'persendiscount' => 'Persendiscount',
			'nominaldiscount' => 'Nominaldiscount',
			'persenppn' => 'Persenppn',
			'nominalppn' => 'Nominalppn',
			'netto' => 'Netto',
			'obatalkes_kode' => 'Obatalkes Kode',
			'namaobat' => 'Namaobat',
			'kelompokobat' => 'Kelompokobat',
			'satuanbesar_nama' => 'Satuanbesar Nama',
			'supplier_id' => 'Supplier',
			'supplier_nama' => 'Supplier Nama',
			'produsen_id' => 'Produsen',
			'produsen_nama' => 'Produsen Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('noterima',$this->noterima,true);
		$criteria->compare('tglterima',$this->tglterima,true);
		$criteria->compare('nofaktur',$this->nofaktur,true);
		$criteria->compare('tglfaktur',$this->tglfaktur,true);
		$criteria->compare('hargabelisatuan',$this->hargabelisatuan);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('bruto',$this->bruto);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('nominaldiscount',$this->nominaldiscount);
		$criteria->compare('persenppn',$this->persenppn);
		$criteria->compare('nominalppn',$this->nominalppn);
		$criteria->compare('netto',$this->netto);
		$criteria->compare('obatalkes_kode',$this->obatalkes_kode,true);
		$criteria->compare('namaobat',$this->namaobat,true);
		$criteria->compare('kelompokobat',$this->kelompokobat,true);
		$criteria->compare('satuanbesar_nama',$this->satuanbesar_nama,true);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('supplier_nama',$this->supplier_nama,true);
		$criteria->compare('produsen_id',$this->produsen_id);
		$criteria->compare('produsen_nama',$this->produsen_nama,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


    public function getObatAlkesItems()
    {
        return ObatalkesM::model()->findAll("obatalkes_aktif=TRUE ORDER BY obatalkes_nama");
    }


    public function getSupplierItems()
    {
        return SupplierM::model()->findAll("supplier_aktif=TRUE AND supplier_jenis='Farmasi' ORDER BY supplier_nama");
    }




}
