<?php

/**
 * This is the model class for table "informasifaktursupplier_v".
 *
 * The followings are the available columns in table 'informasifaktursupplier_v':
 * @property integer $fakturpembelian_id
 * @property integer $supplier_id
 * @property string $supplier_kode
 * @property string $supplier_nama
 * @property string $supplier_alamat
 * @property string $supplier_telp
 * @property string $supplier_fax
 * @property string $nofaktur
 * @property string $tglfaktur
 * @property string $tgljatuhtempo
 * @property string $keteranganfaktur
 * @property double $biayamaterai
 * @property integer $obatalkes_id
 * @property string $obatalkes_kode
 * @property string $obatalkes_nama
 * @property string $obatalkes_kategori
 * @property integer $sumberdana_id
 * @property string $sumberdana_nama
 * @property integer $satuanbesar_id
 * @property string $satuanbesar_nama
 * @property integer $satuankecil_id
 * @property string $satuankecil_nama
 * @property double $jmlterima
 * @property double $harganettofaktur
 * @property double $hargappnfaktur
 * @property double $hargapphfaktur
 * @property double $jmldiscount
 * @property double $hargasatuan
 * @property string $tglkadaluarsa
 * @property double $jmlkemasan
 */
class InformasifaktursupplierV extends CActiveRecord
{
        public $tglAwal,$tglAkhir,$tglAwalJatuhTempo,$tglAkhirJatuhTempo,$subtot;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InformasifaktursupplierV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'informasifaktursupplier_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fakturpembelian_id, supplier_id, obatalkes_id, sumberdana_id, satuanbesar_id, satuankecil_id', 'numerical', 'integerOnly'=>true),
			array('biayamaterai, jmlterima, harganettofaktur, hargappnfaktur, hargapphfaktur, jmldiscount, hargasatuan, jmlkemasan', 'numerical'),
			array('supplier_kode', 'length', 'max'=>10),
			array('supplier_nama', 'length', 'max'=>100),
			array('supplier_telp, supplier_fax, nofaktur, obatalkes_kategori, sumberdana_nama, satuanbesar_nama, satuankecil_nama', 'length', 'max'=>50),
			array('obatalkes_kode, obatalkes_nama', 'length', 'max'=>200),
			array('supplier_alamat, tglfaktur, tgljatuhtempo, keteranganfaktur, tglkadaluarsa', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tglAwal,tglAkhir,tglAwalJatuhTempo,tglAkhirJatuhTempo,fakturpembelian_id, supplier_id, supplier_kode, supplier_nama, supplier_alamat, supplier_telp, supplier_fax, nofaktur, tglfaktur, tgljatuhtempo, keteranganfaktur, biayamaterai, obatalkes_id, obatalkes_kode, obatalkes_nama, obatalkes_kategori, sumberdana_id, sumberdana_nama, satuanbesar_id, satuanbesar_nama, satuankecil_id, satuankecil_nama, jmlterima, harganettofaktur, hargappnfaktur, hargapphfaktur, jmldiscount, hargasatuan, tglkadaluarsa, jmlkemasan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fakturpembelian_id' => 'Faktur Pembelian',
			'supplier_id' => 'Supplier',
			'supplier_kode' => 'Kode Supplier',
			'supplier_nama' => 'Nama Supplier',
			'supplier_alamat' => 'Alamat Supplier',
			'supplier_telp' => 'Telp Supplier',
			'supplier_fax' => 'Fax Supplier',
			'nofaktur' => 'No Faktur',
			'tglfaktur' => 'Tgl Faktur',
			'tgljatuhtempo' => 'Tgl Jatuh Tempo',
			'keteranganfaktur' => 'Keterangan Faktur',
			'biayamaterai' => 'Biaya Materai',
			'obatalkes_id' => 'Obat Alkes',
			'obatalkes_kode' => 'Kode Obat Alkes',
			'obatalkes_nama' => 'Nama Obat Alkes',
			'obatalkes_kategori' => 'Kategori Obat Alkes',
			'sumberdana_id' => 'Sumber Dana',
			'sumberdana_nama' => 'Sumber Dana Nama',
			'satuanbesar_id' => 'Satuan Besar',
			'satuanbesar_nama' => 'Nama Satuan Besar',
			'satuankecil_id' => 'Satuan Kecil',
			'satuankecil_nama' => 'Nama Satuan Kecil',
			'jmlterima' => 'Jml Terima',
			'harganettofaktur' => 'Harga Netto Faktur',
			'hargappnfaktur' => 'Harga Ppn Faktur',
			'hargapphfaktur' => 'Harga Pph Faktur',
			'jmldiscount' => 'Jml Discount',
			'hargasatuan' => 'Harga Satuan',
			'tglkadaluarsa' => 'Tgl Kadaluarsa',
			'jmlkemasan' => 'Jml Kemasan',
			'tglAwal' => 'Tgl Awal',
			'tglAkhir' => 'Tgl Akhir',
			'tglAwalJatuhTempo' => 'Tgl Awal Jatuh Tempo',
			'tglAkhirJatuhTempo' => 'Tgl Akhir Jatuh Tempo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('LOWER(supplier_kode)',strtolower($this->supplier_kode),true);
		$criteria->compare('LOWER(supplier_nama)',strtolower($this->supplier_nama),true);
		$criteria->compare('LOWER(supplier_alamat)',strtolower($this->supplier_alamat),true);
		$criteria->compare('LOWER(supplier_telp)',strtolower($this->supplier_telp),true);
		$criteria->compare('LOWER(supplier_fax)',strtolower($this->supplier_fax),true);
		$criteria->compare('LOWER(nofaktur)',strtolower($this->nofaktur),true);
		$criteria->compare('LOWER(tglfaktur)',strtolower($this->tglfaktur),true);
		$criteria->compare('LOWER(tgljatuhtempo)',strtolower($this->tgljatuhtempo),true);
		$criteria->compare('LOWER(keteranganfaktur)',strtolower($this->keteranganfaktur),true);
		$criteria->compare('biayamaterai',$this->biayamaterai);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('LOWER(sumberdana_nama)',strtolower($this->sumberdana_nama),true);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('LOWER(satuanbesar_nama)',strtolower($this->satuanbesar_nama),true);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('LOWER(satuankecil_nama)',strtolower($this->satuankecil_nama),true);
		$criteria->compare('jmlterima',$this->jmlterima);
		$criteria->compare('harganettofaktur',$this->harganettofaktur);
		$criteria->compare('hargappnfaktur',$this->hargappnfaktur);
		$criteria->compare('hargapphfaktur',$this->hargapphfaktur);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('hargasatuan',$this->hargasatuan);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('LOWER(supplier_kode)',strtolower($this->supplier_kode),true);
		$criteria->compare('LOWER(supplier_nama)',strtolower($this->supplier_nama),true);
		$criteria->compare('LOWER(supplier_alamat)',strtolower($this->supplier_alamat),true);
		$criteria->compare('LOWER(supplier_telp)',strtolower($this->supplier_telp),true);
		$criteria->compare('LOWER(supplier_fax)',strtolower($this->supplier_fax),true);
		$criteria->compare('LOWER(nofaktur)',strtolower($this->nofaktur),true);
		$criteria->compare('LOWER(tglfaktur)',strtolower($this->tglfaktur),true);
		$criteria->compare('LOWER(tgljatuhtempo)',strtolower($this->tgljatuhtempo),true);
		$criteria->compare('LOWER(keteranganfaktur)',strtolower($this->keteranganfaktur),true);
		$criteria->compare('biayamaterai',$this->biayamaterai);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('LOWER(sumberdana_nama)',strtolower($this->sumberdana_nama),true);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('LOWER(satuanbesar_nama)',strtolower($this->satuanbesar_nama),true);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('LOWER(satuankecil_nama)',strtolower($this->satuankecil_nama),true);
		$criteria->compare('jmlterima',$this->jmlterima);
		$criteria->compare('harganettofaktur',$this->harganettofaktur);
		$criteria->compare('hargappnfaktur',$this->hargappnfaktur);
		$criteria->compare('hargapphfaktur',$this->hargapphfaktur);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('hargasatuan',$this->hargasatuan);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function getSyaratBayarItems()
        {
            return SyaratbayarM::model()->findAll('syaratbayar_aktif=TRUE ORDER BY syaratbayar_nama');
        }
        
         public function getSupplierItems()
        {
            return SupplierM::model()->findAll("supplier_aktif=TRUE AND supplier_jenis='Farmasi' ORDER BY supplier_nama");
        }
}
?>