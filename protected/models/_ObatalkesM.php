<?php

/**
 * This is the model class for table "obatalkes_m".
 *
 * The followings are the available columns in table 'obatalkes_m':
 * @property integer $obatalkes_id
 * @property integer $lokasigudang_id
 * @property integer $therapiobat_id
 * @property integer $pbf_id
 * @property integer $generik_id
 * @property integer $satuanbesar_id
 * @property integer $sumberdana_id
 * @property integer $satuankecil_id
 * @property integer $jenisobatalkes_id
 * @property string $obatalkes_kode
 * @property string $obatalkes_nama
 * @property string $obatalkes_golongan
 * @property string $obatalkes_kategori
 * @property string $obatalkes_kadarobat
 * @property integer $kemasanbesar
 * @property integer $kekuatan
 * @property string $satuankekuatan
 * @property double $harganetto
 * @property double $hargajual
 * @property double $discount
 * @property string $tglkadaluarsa
 * @property integer $minimalstok
 * @property string $formularium
 * @property boolean $discountinue
 * @property boolean $obatalkes_aktif
 * @property boolean $isjasadibayarkan
 * @property integer $jeniskode_id
 * @property string $jnskelompok
 */

ini_set('memory_limit', '512M'); //Raise to 512 MB

class ObatalkesM extends CActiveRecord
{		
        public $satuankecilNama;
        public $sumberdanaNama;
        public $satuanbesarNama;
        public $generikNama;
        public $pbfNama;
        public $lokasigudangNama;
        public $therapiobatNama;
        public $formObatAlkesDetail = false;
        public $tglkadaluarsa_akhir, $tglkadaluarsa_awal,$obatAlkes,$sumberdana_id;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObatalkesM the static model class
	 */
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'obatalkes_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('harga_besar, jenisobatalkes_id,jeniskode_id,sumberdana_id, satuanbesar_id, satuankecil_id, obatalkes_kode, obatalkes_nama, harganetto, hargajual, discount,jnskelompok', 'required'),
			array('lokasigudang_id, therapiobat_id, pbf_id, generik_id, satuanbesar_id, sumberdana_id, satuankecil_id, jenisobatalkes_id, kemasanbesar, kekuatan, minimalstok', 'numerical', 'integerOnly'=>true),
			array('harga_besar, harganetto, hargajual, discount, marginresep, jasadokter, hjaresep, marginnonresep, hjanonresep,hpp', 'numerical'),
			array('obatalkes_kode, obatalkes_golongan, obatalkes_kategori, formularium', 'length', 'max'=>50),
			array('obatalkes_nama, noregister, nobatch', 'length', 'max'=>200),
			array('obatalkes_kadarobat, satuankekuatan', 'length', 'max'=>20),
			array('activedate, tglkadaluarsa, discountinue, obatalkes_aktif, isjasadibayarkan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('jeniskode_id,kadaluarsa, tglkadaluarsa_akhir, sumberdanaNama, tglkadaluarsa_awal, hpp, therapiobatNama,lokasigudangNama,pbfNama,generikNama,satuanbesarNama, sumberdanaNama, satuankecilNama, obatalkes_id, lokasigudang_id, therapiobat_id, pbf_id, generik_id, satuanbesar_id, sumberdana_id, satuankecil_id, jenisobatalkes_id, obatalkes_kode, obatalkes_nama, obatalkes_golongan, obatalkes_kategori, obatalkes_kadarobat, kemasanbesar, kekuatan, satuankekuatan, harganetto, hargajual, discount, tglkadaluarsa, minimalstok, formularium, discountinue, obatalkes_aktif, marginresep, jasadokter, hjaresep, marginnonresep, hjanonresep, jnskelompok, isjasadibayarkan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                                    'lokasigudang'=>array(self::BELONGS_TO, 'LokasigudangM','lokasigudang_id'),
                                    'therapiobat'=>array(self::BELONGS_TO, 'TherapiobatM','therapiobat_id'),
                                    'pbf'=>array(self::BELONGS_TO, 'PbfM','pbf_id'),
                                    'generik'=>array(self::BELONGS_TO, 'GenerikM','generik_id'),
                                    'satuanbesar'=>array(self::BELONGS_TO, 'SatuanbesarM','satuanbesar_id'),
                                    'sumberdana'=>array(self::BELONGS_TO, 'SumberdanaM','sumberdana_id'),
                                    'satuankecil'=>array(self::BELONGS_TO, 'SatuankecilM','satuankecil_id'),
                                    'jenisobatalkes'=>array(self::BELONGS_TO, 'JenisobatalkesM','jenisobatalkes_id'),
                                    'detailobat'=>array(self::HAS_MANY,  'ObatalkesdetailM', 'obatalkes_id'),
                                    'subjenis'=>array(self::BELONGS_TO, 'SubjenisM','subjenis_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'harga_besar' => 'Harga Besar',
			'jeniskode_id' => 'Jenis Kode',
			'obatalkes_id' => 'ID',
			'lokasigudang_id' => 'Lokasi Gudang',
			'therapiobat_id' => 'Therapi Obat',
			'pbf_id' => 'PBF',
			'generik_id' => 'Generik',
			'satuanbesar_id' => 'Satuan Besar',
			'sumberdana_id' => 'Asal Barang',
			'sumberdana_nama' => 'Asal Barang',
			'satuankecil_id' => 'Satuan Kecil', 
			'jenisobatalkes_id' => 'Jenis Obat Alkes',
			'obatalkes_kode' => 'Kode',
			'obatalkes_nama' => ' Nama',
			'obatalkes_golongan' => 'Golongan',
			'obatalkes_kategori' => 'Kategori',
			'obatalkes_kadarobat' => 'Kadar Obat',
			'kemasanbesar' => 'Isi Kemasan',
			'kekuatan' => 'Kekuatan',
			'satuankekuatan' => 'Satuan Kekuatan',
			'harganetto' => 'Harga Netto',
			'hargajual' => 'Harga Jual',
			'discount' => 'Discount',
			'tglkadaluarsa' => 'Tanggal Kadaluarsa',
			'tglkadaluarsa_akhir' => 'Sampai Dengan',
			'minimalstok' => 'Stok Minimal',
			'formularium' => 'Formularium',
			'discountinue' => 'Discountinue',
			'obatalkes_aktif' => 'Aktif',
                        'satuankecilNama' => 'Satuan Kecil',
                        'sumberdanaNama' => 'Sumber Dana',

                        'lokasigudangNama'=>'Lokasi Gudang',
                        'satuanbesarNama'=>'Satuan Besar',
                        'noregister'=>'No Register', 
                        'nobatch' => 'No Batch',
                        'ppn_persen'=>'PPN',
                    
                        'marginresep'=>'Margin Resep',
                        'jasadokter'=>'Jasa Dokter',
                        'hjaresep'=>'HJA Resep',
                        'marginnonresep'=>'Margin Non Resep',
                        'hjanonresep'=>'HJA Non Resep',
                        'hpp'=>'HPP',
                    
                        'isjasadibayarkan'=>'Jasa dibayarkan ke Dokter',
                        'jnskelompok'=>'Jenis Kelompok',
                    
                        'hjanonresep_asal'=>'HJA Non Resep Sebelumnya',
                        'hjaresep_asal'=>'HJA Resep Sebelumnya',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

        $criteria->with = array('sumberdana','satuankecil');
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('lokasigudang_id',$this->lokasigudang_id);
		$criteria->compare('therapiobat_id',$this->therapiobat_id);
		$criteria->compare('pbf_id',$this->pbf_id);
		$criteria->compare('generik_id',$this->generik_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('t.sumberdana_id',$this->sumberdana_id);
		$criteria->compare('t.satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
        $criteria->compare('LOWER(noregister)',strtolower($this->noregister),true);
		$criteria->compare('LOWER(nobatch)',strtolower($this->nobatch),true);
		$criteria->compare('kemasanbesar',$this->kemasanbesar);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('LOWER(satuankekuatan)',strtolower($this->satuankekuatan),true);
                
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('marginresep',$this->marginresep);
		$criteria->compare('jasadokter',$this->jasadokter);
		$criteria->compare('hjaresep',$this->hjaresep);
		$criteria->compare('marginnonresep',$this->marginnonresep);
		$criteria->compare('hjanonresep',$this->hjanonresep);
		$criteria->compare('hpp',$this->hpp);
                
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('LOWER(formularium)',strtolower($this->formularium),true);
		$criteria->compare('discountinue',$this->discountinue);
		$criteria->compare('obatalkes_aktif',isset($this->obatalkes_aktif)?$this->obatalkes_aktif:true);
		$criteria->compare('LOWER(satuankecil.satuankecil_nama)',strtolower($this->satuankecilNama),true);
		$criteria->compare('LOWER(sumberdana.sumberdana_nama)',strtolower($this->sumberdanaNama),true);
		$criteria->compare('t.obatalkes_farmasi',$this->obatalkes_farmasi);
                
                $criteria->compare('isjasadibayarkan',$this->isjasadibayarkan);
                $criteria->compare('LOWER(jnskelompok)',strtolower($this->jnskelompok),true);
                $criteria->order='obatalkes_nama ASC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
                                $criteria->with = array('sumberdana','satuankecil');
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('lokasigudang_id',$this->lokasigudang_id);
		$criteria->compare('therapiobat_id',$this->therapiobat_id);
		$criteria->compare('pbf_id',$this->pbf_id);
		$criteria->compare('generik_id',$this->generik_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('t.sumberdana_id',$this->sumberdana_id);
		$criteria->compare('t.satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
                $criteria->compare('LOWER(noregister)',strtolower($this->noregister),true);
		$criteria->compare('LOWER(nobatch)',strtolower($this->nobatch),true);
		$criteria->compare('kemasanbesar',$this->kemasanbesar);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('LOWER(satuankekuatan)',strtolower($this->satuankekuatan),true);
                
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('marginresep',$this->marginresep);
		$criteria->compare('jasadokter',$this->jasadokter);
		$criteria->compare('hjaresep',$this->hjaresep);
		$criteria->compare('marginnonresep',$this->marginnonresep);
		$criteria->compare('hjanonresep',$this->hjanonresep);
                $criteria->compare('hpp',$this->hpp);
                
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('LOWER(formularium)',strtolower($this->formularium),true);
		$criteria->compare('discountinue',$this->discountinue);
		$criteria->compare('obatalkes_aktif',isset($this->obatalkes_aktif)?$this->obatalkes_aktif:true);
		$criteria->compare('LOWER(satuankecil.satuankecil_nama)',strtolower($this->satuankecilNama),true);
		$criteria->compare('LOWER(sumberdana.sumberdana_nama)',strtolower($this->sumberdanaNama),true);
                
                $criteria->compare('isjasadibayarkan',$this->isjasadibayarkan);
                $criteria->compare('LOWER(jnskelompok)',strtolower($this->jnskelompok),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function searchGudangFarmasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array('sumberdana','satuankecil', 'satuanbesar');
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('t.lokasigudang_id',$this->lokasigudang_id);
                $criteria->compare('LOWER(lokasigudang.lokasigudang_nama)',  strtolower($this->lokasigudangNama));
		$criteria->compare('t.therapiobat_id',$this->therapiobat_id);
                $criteria->compare('LOWER(therapiobat.therapiobat_nama)',  strtolower($this->therapiobatNama));
		$criteria->compare('t.pbf_id',$this->pbf_id);
		$criteria->compare('t.generik_id',$this->generik_id);
		$criteria->compare('t.satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('LOWER(satuanbesar.satuanbesar_nama)',  strtolower($this->satuanbesarNama));
		$criteria->compare('sumberdana.sumberdana_id',$this->sumberdana_id);
		$criteria->compare('t.satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
                $criteria->compare('LOWER(noregister)',strtolower($this->noregister),true);
		$criteria->compare('LOWER(nobatch)',strtolower($this->nobatch),true);
		$criteria->compare('kemasanbesar',$this->kemasanbesar);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('LOWER(satuankekuatan)',strtolower($this->satuankekuatan),true);
                
                $criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('marginresep',$this->marginresep);
		$criteria->compare('jasadokter',$this->jasadokter);
		$criteria->compare('hjaresep',$this->hjaresep);
		$criteria->compare('marginnonresep',$this->marginnonresep);
		$criteria->compare('hjanonresep',$this->hjanonresep);
                $criteria->compare('hpp',$this->hpp);
                
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('LOWER(formularium)',strtolower($this->formularium),true);
		$criteria->compare('discountinue',$this->discountinue);
		$criteria->compare('obatalkes_aktif',isset($this->obatalkes_aktif)?$this->obatalkes_aktif:true);
		$criteria->compare('LOWER(satuankecil.satuankecil_nama)',strtolower($this->satuankecilNama),true);
		$criteria->compare('LOWER(sumberdana.sumberdana_nama)',strtolower($this->sumberdanaNama),true);
                
                $criteria->compare('isjasadibayarkan',$this->isjasadibayarkan);
                $criteria->compare('LOWER(jnskelompok)',strtolower($this->jnskelompok),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        /**
         * searchObatFarmasi digunakan di :
         * 1. Grid Pemilihan Obat - Penjualan Resep
         * @return \CActiveDataProvider
         */
        public function searchObatFarmasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
                $format = new CustomFormat();
		$criteria=new CDbCriteria;

                $criteria->with = array('sumberdana','satuankecil', 'satuanbesar');
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('t.lokasigudang_id',$this->lokasigudang_id);
                $criteria->compare('LOWER(lokasigudang.lokasigudang_nama)',  strtolower($this->lokasigudangNama));
		$criteria->compare('t.therapiobat_id',$this->therapiobat_id);
                $criteria->compare('LOWER(therapiobat.therapiobat_nama)',  strtolower($this->therapiobatNama));
		$criteria->compare('t.pbf_id',$this->pbf_id);
		$criteria->compare('t.generik_id',$this->generik_id);
		$criteria->compare('t.satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('LOWER(satuanbesar.satuanbesar_nama)',  strtolower($this->satuanbesarNama));
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('t.satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
                $criteria->compare('LOWER(noregister)',strtolower($this->noregister),true);
		$criteria->compare('LOWER(nobatch)',strtolower($this->nobatch),true);
		$criteria->compare('kemasanbesar',$this->kemasanbesar);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('LOWER(satuankekuatan)',strtolower($this->satuankekuatan),true);
	
                $criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('marginresep',$this->marginresep);
		$criteria->compare('jasadokter',$this->jasadokter);
		$criteria->compare('hjaresep',$this->hjaresep);
		$criteria->compare('marginnonresep',$this->marginnonresep);
		$criteria->compare('hjanonresep',$this->hjanonresep);
                $criteria->compare('hpp',$this->hpp);
                
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('LOWER(formularium)',strtolower($this->formularium),true);
		$criteria->compare('discountinue',$this->discountinue);
		$criteria->compare('obatalkes_aktif',isset($this->obatalkes_aktif)?$this->obatalkes_aktif:true);
		$criteria->compare('LOWER(satuankecil.satuankecil_nama)',strtolower($this->satuankecilNama),true);
		$criteria->compare('LOWER(sumberdana.sumberdana_nama)',strtolower($this->sumberdanaNama),true);
                $criteria->addCondition('obatalkes_farmasi is true');
                
                $criteria->compare('isjasadibayarkan',$this->isjasadibayarkan);
                $criteria->compare('LOWER(jnskelompok)',strtolower($this->jnskelompok),true);
                
                return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
         public function beforeSave() {         
            if($this->tglkadaluarsa===null || trim($this->tglkadaluarsa)==''){
	        $this->setAttribute('tglkadaluarsa', null);
            }
            return parent::beforeSave();
        }

        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                        }
            }
            return true;
        }
        
        public function getLokasiGudangItems()
        {
            return LokasigudangM::model()->findAll('lokasigudang_aktif=true ORDER BY lokasigudang_nama');
        
        }
        
        public function getTherapiObatItems()
        {
            return TherapiobatM::model()->findAll('therapiobat_aktif=true ORDER BY therapiobat_nama');
        }
        
        public function getPbfItems()
        {
            return PbfM::model()->findAll('pbf_aktif=true ORDER BY pbf_nama');
        }
        
        public function getGenerikItems()
        {
            return GenerikM::model()->findAll('generik_aktif=true ORDER BY generik_nama');
        }
        
        public function getSatuanBesarItems()
        {
            return SatuanbesarM::model()->findAll('satuanbesar_aktif=true ORDER BY satuanbesar_nama');
        }
        
        public function getSatuanKecilItems()
        {
            return GFSatuanKecilM::model()->findAll('satuankecil_aktif=TRUE ORDER BY satuankecil_nama');
        }
        
        public function getJenisObatAlkesItems()
        {
            return GFJenisObatAlkesM::model()->findAll('jenisobatalkes_aktif=TRUE ORDER BY jenisobatalkes_nama');
        }
        
         public function getSumberDanaItems()
        {
            return GFSumberDanaM::model()->findAll('sumberdana_aktif=TRUE ORDER BY sumberdana_nama');
        }
         public function getJenisKodeitems()
        {
            return GFJenisKode::model()->findAll('jeniskode_aktif=TRUE ORDER BY jeniskode_nama');
        }
        
        public function showKomposisi($detailObat)
        {
            //$tmp = '<div class="raw">';
            $tmp = '';
            foreach ($detailObat as $obat) {
                $tmp = $tmp.$obat->komposisi.'';
            }
            //$tmp = $tmp.'</div>';
            return $tmp;

        }
        public function getKadaluarsa(){ //menampilkan status kadaluarsa saat input obat pada transaksi: penjualan
            $format = new CustomFormat;
            $kadaluarsa = ((strtotime($format->formatDateMediumForDB($this->tglkadaluarsa)) - strtotime(date('Y-m-d'))) > 0) ? 0 : 1 ;
            return $kadaluarsa;
        }
        public function getStokObatRuangan(){ // menampilkan stok obat per ruangan login
            return StokobatalkesT::getStokBarang($this->obatalkes_id,Yii::app()->user->getState('ruangan_id'));
        }
        public function getNamaSumberDana(){ // menampilkan sumber dana pada dialog box (grid)
            $sumberdana = SumberdanaM::model()->findByPk($this->sumberdana_id);
            return (!empty($sumberdana->sumberdana_nama)) ? $sumberdana->sumberdana_nama : "";
        }
        /**
         * getDiskonJual diskon dari stokobatalkes_t untuk :
         * 1. semua penjualan obat
         * 2. mutasi
         * 3. retur
         * @return type
         */
        public function getDiskonJual(){
            $diskon = 0;
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            if($konfigFarmasi->persdiskpasien > 0){ //jika persdiskpasien di aktifkan
                //cari obat yang terakhir di terima
                $modStokObat = StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$this->obatalkes_id), array('order'=>'tglstok_in DESC'));
                if(!empty($modStokObat[0]->obatalkes_id)){
                    $diskon = $modStokObat[0]->discount; 
                }
            }
            return $diskon;
        }
        
        public function getSatuankecilNama(){
            return $this->satuankecil->satuankecil_nama;
        }
}
