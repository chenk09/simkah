<?php

/**
 * This is the model class for table "pelayananrekening_v".
 *
 * The followings are the available columns in table 'pelayananrekening_v':
 * @property integer $struktur_id
 * @property string $kdstruktur
 * @property string $nmstruktur
 * @property integer $kelompok_id
 * @property string $kdkelompok
 * @property string $nmkelompok
 * @property integer $jenis_id
 * @property string $kdjenis
 * @property string $nmjenis
 * @property integer $obyek_id
 * @property string $kdobyek
 * @property string $nmobyek
 * @property integer $rincianobyek_id
 * @property string $kdrincianobyek
 * @property string $nmrincianobyek
 * @property integer $daftartindakan_id
 * @property string $saldonormal
 * @property string $jnspelayanan
 * @property integer $kelaspelayanan_id
 * @property double $tariftindakan
 */
class PelayananrekeningV extends CActiveRecord
{
    public $rekDebit,$rekKredit,$namaRekening, $nama_rekening, $saldokredit,$saldodebit, $saldonormal, $jnspelayanan;
    public $daftartindakan_id, $obatalkes_id, $jenis_biaya; //untuk pembeda di form jurnal rekening
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PelayananrekeningV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pelayananrekening_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('struktur_id, kelompok_id, jenis_id, obyek_id, rincianobyek_id, daftartindakan_id, kelaspelayanan_id', 'numerical', 'integerOnly'=>true),
			array('tariftindakan', 'numerical'),
			array('kdstruktur, kdkelompok, kdjenis, kdobyek, kdrincianobyek', 'length', 'max'=>5),
			array('nmstruktur', 'length', 'max'=>100),
			array('nmkelompok', 'length', 'max'=>200),
			array('nmjenis', 'length', 'max'=>300),
			array('nmobyek', 'length', 'max'=>400),
			array('nmrincianobyek', 'length', 'max'=>500),
			array('saldonormal', 'length', 'max'=>10),
			array('jnspelayanan', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('struktur_id, kdstruktur, nmstruktur, kelompok_id, kdkelompok, nmkelompok, jenis_id, kdjenis, nmjenis, obyek_id, kdobyek, nmobyek, rincianobyek_id, kdrincianobyek, nmrincianobyek, daftartindakan_id, saldonormal, jnspelayanan, kelaspelayanan_id, tariftindakan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'struktur_id' => 'Struktur',
			'kdstruktur' => 'Kdstruktur',
			'nmstruktur' => 'Nmstruktur',
			'kelompok_id' => 'Kelompok',
			'kdkelompok' => 'Kdkelompok',
			'nmkelompok' => 'Nmkelompok',
			'jenis_id' => 'Jenis',
			'kdjenis' => 'Kdjenis',
			'nmjenis' => 'Nmjenis',
			'obyek_id' => 'Obyek',
			'kdobyek' => 'Kdobyek',
			'nmobyek' => 'Nmobyek',
			'rincianobyek_id' => 'Rincianobyek',
			'kdrincianobyek' => 'Kdrincianobyek',
			'nmrincianobyek' => 'Nmrincianobyek',
			'daftartindakan_id' => 'Daftartindakan',
			'saldonormal' => 'Saldonormal',
			'jnspelayanan' => 'Jnspelayanan',
			'kelaspelayanan_id' => 'Kelaspelayanan',
			'tariftindakan' => 'Tarftindakan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('struktur_id',$this->struktur_id);
		$criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur),true);
		$criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(kdkelompok)',strtolower($this->kdkelompok),true);
		$criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
		$criteria->compare('jenis_id',$this->jenis_id);
		$criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
		$criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
		$criteria->compare('obyek_id',$this->obyek_id);
		$criteria->compare('LOWER(kdobyek)',strtolower($this->kdobyek),true);
		$criteria->compare('LOWER(nmobyek)',strtolower($this->nmobyek),true);
		$criteria->compare('rincianobyek_id',$this->rincianobyek_id);
		$criteria->compare('LOWER(kdrincianobyek)',strtolower($this->kdrincianobyek),true);
		$criteria->compare('LOWER(nmrincianobyek)',strtolower($this->nmrincianobyek),true);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('LOWER(saldonormal)',strtolower($this->saldonormal),true);
		$criteria->compare('LOWER(jnspelayanan)',strtolower($this->jnspelayanan),true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('tariftindakan',$this->tariftindakan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('struktur_id',$this->struktur_id);
		$criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur),true);
		$criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(kdkelompok)',strtolower($this->kdkelompok),true);
		$criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
		$criteria->compare('jenis_id',$this->jenis_id);
		$criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
		$criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
		$criteria->compare('obyek_id',$this->obyek_id);
		$criteria->compare('LOWER(kdobyek)',strtolower($this->kdobyek),true);
		$criteria->compare('LOWER(nmobyek)',strtolower($this->nmobyek),true);
		$criteria->compare('rincianobyek_id',$this->rincianobyek_id);
		$criteria->compare('LOWER(kdrincianobyek)',strtolower($this->kdrincianobyek),true);
		$criteria->compare('LOWER(nmrincianobyek)',strtolower($this->nmrincianobyek),true);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('LOWER(saldonormal)',strtolower($this->saldonormal),true);
		$criteria->compare('LOWER(jnspelayanan)',strtolower($this->jnspelayanan),true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('tariftindakan',$this->tariftindakan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}