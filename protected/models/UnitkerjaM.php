<?php

/**
 * This is the model class for table "unitkerja_m".
 *
 * The followings are the available columns in table 'unitkerja_m':
 * @property integer $unitkerja_id
 * @property integer $ruangan_id
 * @property string $kodeunitkerja
 * @property string $namaunitkerja
 * @property string $namalain
 * @property boolean $unitkerja_aktif
 *
 * The followings are the available model relations:
 * @property RuanganM $ruangan
 */
class UnitkerjaM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UnitkerjaM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'unitkerja_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kodeunitkerja, namaunitkerja', 'required'),
			// array('ruangan_id', 'numerical', 'integerOnly'=>true),
			array('kodeunitkerja', 'length', 'max'=>50),
			array('namaunitkerja, namalain', 'length', 'max'=>200),
			array('unitkerja_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('unitkerja_id, kodeunitkerja, namaunitkerja, namalain, unitkerja_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		// return array(
		// 	'ruangan' => array(self::BELONGS_TO, 'RuanganM', 'ruangan_id'),
		// );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'unitkerja_id' => 'Id',
			'kodeunitkerja' => 'Kode Unit',
			'namaunitkerja' => 'Nama Unit',
			'namalain' => 'Nama lain',
			'unitkerja_aktif' => 'Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('unitkerja_id',$this->unitkerja_id);
		$criteria->compare('kodeunitkerja',$this->kodeunitkerja,true);
		$criteria->compare('namaunitkerja',$this->namaunitkerja,true);
		$criteria->compare('namalain',$this->namalain,true);
		$criteria->compare('unitkerja_aktif',$this->unitkerja_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}