<?php

/**
 * This is the model class for table "laporanrekappendapatandetail_v".
 *
 * The followings are the available columns in table 'laporanrekappendapatandetail_v':
 * @property integer $pasien_id
 * @property string $no_rekam_medik
 * @property string $namadepan
 * @property string $nama_pasien
 * @property string $nama_bin
 * @property string $jeniskelamin
 * @property integer $carabayar_id
 * @property string $carabayar_nama
 * @property integer $penjamin_id
 * @property string $penjamin_nama
 * @property integer $ruangankasir_m_id
 * @property string $ruangankasir_nama
 * @property integer $tandabuktibayar_id
 * @property string $nobuktibayar
 * @property string $tglbuktibayar
 * @property integer $pendaftaran_id
 * @property string $no_pendaftaran
 * @property string $tgl_pendaftaran
 * @property integer $pasienadmisi_id
 * @property string $nopembayaran
 * @property string $tglpembayaran
 * @property string $noresep
 * @property string $nosjp
 * @property double $totalbiayaoa
 * @property double $totalbiayatindakan
 * @property double $totalbiayapelayanan
 * @property double $totalsubsidiasuransi
 * @property double $totalsubsidipemerintah
 * @property double $totalsubsidirs
 * @property double $totaliurbiaya
 * @property double $totalbayartindakan
 * @property double $totaldiscount
 * @property double $totalpembebasan
 * @property double $totalsisatagihan
 * @property string $statusbayar
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $carapembayaran
 * @property integer $loginpemakai_id
 * @property string $nama_pemakai
 * @property integer $ruanganpelakhir_id
 * @property string $ruanganakhir_nama
 * @property string $tglpulang
 * @property double $jmlbiaya_tindakan
 * @property string $daftartindakan_nama
 * @property integer $qty_tindakan
 * @property string $tgl_tindakan
 * @property string $nama_pegawai
 * @property integer $pegawai_id
 * @property string $kelaspelayanan_nama
 */
class LaporanrekappendapatandetailV extends CActiveRecord
{
	
	public $tglAwal,$tglAkhir,$bulan,$hari,$tahun;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanrekappendapatandetailV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanrekappendapatandetail_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pasien_id, carabayar_id, penjamin_id, ruangankasir_m_id, tandabuktibayar_id, pendaftaran_id, pasienadmisi_id, loginpemakai_id, ruanganpelakhir_id, qty_tindakan, pegawai_id', 'numerical', 'integerOnly'=>true),
			array('totalbiayaoa, totalbiayatindakan, totalbiayapelayanan, totalsubsidiasuransi, totalsubsidipemerintah, totalsubsidirs, totaliurbiaya, totalbayartindakan, totaldiscount, totalpembebasan, totalsisatagihan, jmlbiaya_tindakan', 'numerical'),
			array('no_rekam_medik', 'length', 'max'=>10),
			array('namadepan, jeniskelamin, no_pendaftaran, nama_pemakai', 'length', 'max'=>20),
			array('nama_pasien, carabayar_nama, penjamin_nama, ruangankasir_nama, nobuktibayar, nopembayaran, noresep, nosjp, carapembayaran, ruanganakhir_nama, nama_pegawai, kelaspelayanan_nama', 'length', 'max'=>50),
			array('nama_bin, statusbayar', 'length', 'max'=>30),
			array('daftartindakan_nama', 'length', 'max'=>200),
			array('tglbuktibayar, tgl_pendaftaran, tglpembayaran, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, tglpulang, tgl_tindakan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tglAwal,tglAkhir,bulan,hari,tahun,pasien_id, no_rekam_medik, namadepan, nama_pasien, nama_bin, jeniskelamin, carabayar_id, carabayar_nama, penjamin_id, penjamin_nama, ruangankasir_m_id, ruangankasir_nama, tandabuktibayar_id, nobuktibayar, tglbuktibayar, pendaftaran_id, no_pendaftaran, tgl_pendaftaran, pasienadmisi_id, nopembayaran, tglpembayaran, noresep, nosjp, totalbiayaoa, totalbiayatindakan, totalbiayapelayanan, totalsubsidiasuransi, totalsubsidipemerintah, totalsubsidirs, totaliurbiaya, totalbayartindakan, totaldiscount, totalpembebasan, totalsisatagihan, statusbayar, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, carapembayaran, loginpemakai_id, nama_pemakai, ruanganpelakhir_id, ruanganakhir_nama, tglpulang, jmlbiaya_tindakan, daftartindakan_nama, qty_tindakan, tgl_tindakan, nama_pegawai, pegawai_id, kelaspelayanan_nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pasien_id' => 'Pasien',
			'no_rekam_medik' => 'No Rekam Medik',
			'namadepan' => 'Namadepan',
			'nama_pasien' => 'Nama Pasien',
			'nama_bin' => 'Nama Bin',
			'jeniskelamin' => 'Jeniskelamin',
			'carabayar_id' => 'Carabayar',
			'carabayar_nama' => 'Carabayar Nama',
			'penjamin_id' => 'Penjamin',
			'penjamin_nama' => 'Penjamin Nama',
			'ruangankasir_m_id' => 'Ruangankasir M',
			'ruangankasir_nama' => 'Ruangankasir Nama',
			'tandabuktibayar_id' => 'Tandabuktibayar',
			'nobuktibayar' => 'Nobuktibayar',
			'tglbuktibayar' => 'Tglbuktibayar',
			'pendaftaran_id' => 'Pendaftaran',
			'no_pendaftaran' => 'No Pendaftaran',
			'tgl_pendaftaran' => 'Tgl Pendaftaran',
			'pasienadmisi_id' => 'Pasienadmisi',
			'nopembayaran' => 'Nopembayaran',
			'tglpembayaran' => 'Tglpembayaran',
			'noresep' => 'Noresep',
			'nosjp' => 'Nosjp',
			'totalbiayaoa' => 'Totalbiayaoa',
			'totalbiayatindakan' => 'Totalbiayatindakan',
			'totalbiayapelayanan' => 'Totalbiayapelayanan',
			'totalsubsidiasuransi' => 'Totalsubsidiasuransi',
			'totalsubsidipemerintah' => 'Totalsubsidipemerintah',
			'totalsubsidirs' => 'Totalsubsidirs',
			'totaliurbiaya' => 'Totaliurbiaya',
			'totalbayartindakan' => 'Totalbayartindakan',
			'totaldiscount' => 'Totaldiscount',
			'totalpembebasan' => 'Totalpembebasan',
			'totalsisatagihan' => 'Totalsisatagihan',
			'statusbayar' => 'Statusbayar',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'carapembayaran' => 'Carapembayaran',
			'loginpemakai_id' => 'Loginpemakai',
			'nama_pemakai' => 'Nama Pemakai',
			'ruanganpelakhir_id' => 'Ruanganpelakhir',
			'ruanganakhir_nama' => 'Ruanganakhir Nama',
			'tglpulang' => 'Tglpulang',
			'jmlbiaya_tindakan' => 'Jmlbiaya Tindakan',
			'daftartindakan_nama' => 'Daftartindakan Nama',
			'qty_tindakan' => 'Qty Tindakan',
			'tgl_tindakan' => 'Tgl Tindakan',
			'nama_pegawai' => 'Nama Pegawai',
			'pegawai_id' => 'Pegawai',
			'kelaspelayanan_nama' => 'Kelaspelayanan Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('no_rekam_medik',$this->no_rekam_medik,true);
		$criteria->compare('namadepan',$this->namadepan,true);
		$criteria->compare('nama_pasien',$this->nama_pasien,true);
		$criteria->compare('nama_bin',$this->nama_bin,true);
		$criteria->compare('jeniskelamin',$this->jeniskelamin,true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('carabayar_nama',$this->carabayar_nama,true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('penjamin_nama',$this->penjamin_nama,true);
		$criteria->compare('ruangankasir_m_id',$this->ruangankasir_m_id);
		$criteria->compare('ruangankasir_nama',$this->ruangankasir_nama,true);
		$criteria->compare('tandabuktibayar_id',$this->tandabuktibayar_id);
		$criteria->compare('nobuktibayar',$this->nobuktibayar,true);
		$criteria->compare('tglbuktibayar',$this->tglbuktibayar,true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('no_pendaftaran',$this->no_pendaftaran,true);
		$criteria->compare('tgl_pendaftaran',$this->tgl_pendaftaran,true);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('nopembayaran',$this->nopembayaran,true);
		$criteria->compare('tglpembayaran',$this->tglpembayaran,true);
		$criteria->compare('noresep',$this->noresep,true);
		$criteria->compare('nosjp',$this->nosjp,true);
		$criteria->compare('totalbiayaoa',$this->totalbiayaoa);
		$criteria->compare('totalbiayatindakan',$this->totalbiayatindakan);
		$criteria->compare('totalbiayapelayanan',$this->totalbiayapelayanan);
		$criteria->compare('totalsubsidiasuransi',$this->totalsubsidiasuransi);
		$criteria->compare('totalsubsidipemerintah',$this->totalsubsidipemerintah);
		$criteria->compare('totalsubsidirs',$this->totalsubsidirs);
		$criteria->compare('totaliurbiaya',$this->totaliurbiaya);
		$criteria->compare('totalbayartindakan',$this->totalbayartindakan);
		$criteria->compare('totaldiscount',$this->totaldiscount);
		$criteria->compare('totalpembebasan',$this->totalpembebasan);
		$criteria->compare('totalsisatagihan',$this->totalsisatagihan);
		$criteria->compare('statusbayar',$this->statusbayar,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id,true);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id,true);
		$criteria->compare('carapembayaran',$this->carapembayaran,true);
		$criteria->compare('loginpemakai_id',$this->loginpemakai_id);
		$criteria->compare('nama_pemakai',$this->nama_pemakai,true);
		$criteria->compare('ruanganpelakhir_id',$this->ruanganpelakhir_id);
		$criteria->compare('ruanganakhir_nama',$this->ruanganakhir_nama,true);
		$criteria->compare('tglpulang',$this->tglpulang,true);
		$criteria->compare('jmlbiaya_tindakan',$this->jmlbiaya_tindakan);
		$criteria->compare('daftartindakan_nama',$this->daftartindakan_nama,true);
		$criteria->compare('qty_tindakan',$this->qty_tindakan);
		$criteria->compare('tgl_tindakan',$this->tgl_tindakan,true);
		$criteria->compare('nama_pegawai',$this->nama_pegawai,true);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('kelaspelayanan_nama',$this->kelaspelayanan_nama,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
        public function searchTable()
	{
		$criteria=new CDbCriteria;

        $criteria->addBetweenCondition('tglpembayaran',$this->tglAwal,$this->tglAkhir);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
                public function searchPrint()
	{
		$criteria=new CDbCriteria;

        $criteria->addBetweenCondition('tglpembayaran',$this->tglAwal,$this->tglAkhir);
$criteria->limit = -1;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>false,
		));
	}
	
	 protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                }elseif ($column->dbType == 'timestamp without time zone'){
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                }
            }
            return true;
        }
}