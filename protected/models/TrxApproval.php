<?php

/**
 * This is the model class for table "inbox.trx_approval".
 *
 * The followings are the available columns in table 'inbox.trx_approval':
 * @property string $id_approval
 * @property string $id_pegawai
 * @property string $approver
 * @property string $keterangan
 * @property string $status
 * @property string $created
 * @property string $create_time
 * @property integer $id_transaksi
 * @property string $app_id
 * @property integer $idx
 * @property string $updated
 * @property string $update_time
 */
class TrxApproval extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TrxApproval the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inbox.trx_approval';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_approval', 'required'),
			array('keterangan', 'required', 'on'=>'update'),
			array('id_transaksi, idx', 'numerical', 'integerOnly'=>true),
			array('id_approval, approver, app_id', 'length', 'max'=>32),
			array('id_pegawai, created, updated', 'length', 'max'=>16),
			array('keterangan', 'length', 'max'=>64),
			array('status', 'length', 'max'=>1),
            array(
                'created',
                'default',
                'value'=>Yii::app()->user->id,
                'setOnEmpty'=>false,
                'on'=>'insert'
            ),
            array(
                'create_time',
                'default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,
                'on'=>'insert'
            ),
            array(
                'updated',
                'default',
                'value'=>Yii::app()->user->id,
                'setOnEmpty'=>false,
                'on'=>'update'
            ),
            array(
                'update_time',
                'default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,
                'on'=>'update'
            ),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_approval, id_pegawai, approver, keterangan, status, created, create_time, id_transaksi, app_id, idx, updated, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_approval' => 'Id Approval',
			'id_pegawai' => 'Id Pegawai',
			'approver' => 'Approver',
			'keterangan' => 'Keterangan',
			'status' => 'Status',
			'created' => 'Created',
			'create_time' => 'Create Time',
			'id_transaksi' => 'Id Transaksi',
			'app_id' => 'App',
			'idx' => 'Idx',
			'updated' => 'Updated',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_approval',$this->id_approval,true);
		$criteria->compare('id_pegawai',$this->id_pegawai,true);
		$criteria->compare('approver',$this->approver,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('id_transaksi',$this->id_transaksi);
		$criteria->compare('app_id',$this->app_id,true);
		$criteria->compare('idx',$this->idx);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}