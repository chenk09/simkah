<?php

/**
 * This is the model class for table "antrianfarmasi_informasi_v".
 *
 * The followings are the available columns in table 'antrianfarmasi_informasi_v':
 * @property string $jenisantrian_nama
 * @property string $jumlah
 * @property string $is_dilayani
 * @property string $is_belumdilayani
 */
class AntrianfarmasiInformasiV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AntrianfarmasiInformasiV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'antrianfarmasi_informasi_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenisantrian_nama', 'length', 'max'=>250),
			array('jumlah, is_dilayani, is_belumdilayani', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('jenisantrian_nama, jumlah, is_dilayani, is_belumdilayani', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'jenisantrian_nama' => 'Jenisantrian Nama',
			'jumlah' => 'Jumlah',
			'is_dilayani' => 'Is Dilayani',
			'is_belumdilayani' => 'Is Belumdilayani',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(jenisantrian_nama)',strtolower($this->jenisantrian_nama),true);
		$criteria->compare('LOWER(jumlah)',strtolower($this->jumlah),true);
		$criteria->compare('LOWER(is_dilayani)',strtolower($this->is_dilayani),true);
		$criteria->compare('LOWER(is_belumdilayani)',strtolower($this->is_belumdilayani),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(jenisantrian_nama)',strtolower($this->jenisantrian_nama),true);
		$criteria->compare('LOWER(jumlah)',strtolower($this->jumlah),true);
		$criteria->compare('LOWER(is_dilayani)',strtolower($this->is_dilayani),true);
		$criteria->compare('LOWER(is_belumdilayani)',strtolower($this->is_belumdilayani),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}