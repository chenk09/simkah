<?php

/**
 * This is the model class for table "rl3_1_kegiatanpelayananrawatinap_v".
 *
 * The followings are the available columns in table 'rl3_1_kegiatanpelayananrawatinap_v':
 * @property double $tahun
 * @property string $nokode_rumahsakit
 * @property string $nama_rumahsakit
 * @property integer $jeniskasuspenyakit_id
 * @property string $jeniskasuspenyakit_nama
 * @property double $pasienawaltahun
 * @property double $pasienmasuk
 * @property double $pasienkeluar
 * @property double $pasienmatikurang48jam
 * @property double $pasienmatilebih48jam
 * @property double $lamadirawat
 * @property double $pasienakhirtahun
 * @property double $hariperawatan
 * @property double $rincianhariperawatanvvip
 * @property double $rincianhariperawatanvip
 * @property double $rincianhariperawatan1
 * @property double $rincianhariperawatan2
 * @property double $rincianhariperawatan3
 * @property double $rincianhariperawatankelaskhusus
 */
class Rl31KegiatanpelayananrawatinapV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rl31KegiatanpelayananrawatinapV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rl3_1_kegiatanpelayananrawatinap_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jeniskasuspenyakit_id', 'numerical', 'integerOnly'=>true),
			array('tahun, pasienawaltahun, pasienmasuk, pasienkeluar, pasienmatikurang48jam, pasienmatilebih48jam, lamadirawat, pasienakhirtahun, hariperawatan, rincianhariperawatanvvip, rincianhariperawatanvip, rincianhariperawatan1, rincianhariperawatan2, rincianhariperawatan3, rincianhariperawatankelaskhusus', 'numerical'),
			array('nokode_rumahsakit', 'length', 'max'=>10),
			array('nama_rumahsakit, jeniskasuspenyakit_nama', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tahun, nokode_rumahsakit, nama_rumahsakit, jeniskasuspenyakit_id, jeniskasuspenyakit_nama, pasienawaltahun, pasienmasuk, pasienkeluar, pasienmatikurang48jam, pasienmatilebih48jam, lamadirawat, pasienakhirtahun, hariperawatan, rincianhariperawatanvvip, rincianhariperawatanvip, rincianhariperawatan1, rincianhariperawatan2, rincianhariperawatan3, rincianhariperawatankelaskhusus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tahun' => 'Tahun',
			'nokode_rumahsakit' => 'Nokode Rumahsakit',
			'nama_rumahsakit' => 'Nama Rumahsakit',
			'jeniskasuspenyakit_id' => 'Jeniskasuspenyakit',
			'jeniskasuspenyakit_nama' => 'Jeniskasuspenyakit Nama',
			'pasienawaltahun' => 'Pasienawaltahun',
			'pasienmasuk' => 'Pasienmasuk',
			'pasienkeluar' => 'Pasienkeluar',
			'pasienmatikurang48jam' => 'Pasienmatikurang48jam',
			'pasienmatilebih48jam' => 'Pasienmatilebih48jam',
			'lamadirawat' => 'Lamadirawat',
			'pasienakhirtahun' => 'Pasienakhirtahun',
			'hariperawatan' => 'Hariperawatan',
			'rincianhariperawatanvvip' => 'Rincianhariperawatanvvip',
			'rincianhariperawatanvip' => 'Rincianhariperawatanvip',
			'rincianhariperawatan1' => 'Rincianhariperawatan1',
			'rincianhariperawatan2' => 'Rincianhariperawatan2',
			'rincianhariperawatan3' => 'Rincianhariperawatan3',
			'rincianhariperawatankelaskhusus' => 'Rincianhariperawatankelaskhusus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('nokode_rumahsakit',$this->nokode_rumahsakit,true);
		$criteria->compare('nama_rumahsakit',$this->nama_rumahsakit,true);
		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
		$criteria->compare('jeniskasuspenyakit_nama',$this->jeniskasuspenyakit_nama,true);
		$criteria->compare('pasienawaltahun',$this->pasienawaltahun);
		$criteria->compare('pasienmasuk',$this->pasienmasuk);
		$criteria->compare('pasienkeluar',$this->pasienkeluar);
		$criteria->compare('pasienmatikurang48jam',$this->pasienmatikurang48jam);
		$criteria->compare('pasienmatilebih48jam',$this->pasienmatilebih48jam);
		$criteria->compare('lamadirawat',$this->lamadirawat);
		$criteria->compare('pasienakhirtahun',$this->pasienakhirtahun);
		$criteria->compare('hariperawatan',$this->hariperawatan);
		$criteria->compare('rincianhariperawatanvvip',$this->rincianhariperawatanvvip);
		$criteria->compare('rincianhariperawatanvip',$this->rincianhariperawatanvip);
		$criteria->compare('rincianhariperawatan1',$this->rincianhariperawatan1);
		$criteria->compare('rincianhariperawatan2',$this->rincianhariperawatan2);
		$criteria->compare('rincianhariperawatan3',$this->rincianhariperawatan3);
		$criteria->compare('rincianhariperawatankelaskhusus',$this->rincianhariperawatankelaskhusus);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}