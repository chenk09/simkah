<?php

/**
 * This is the model class for table "laporanpendapatantindakan_v".
 *
 * The followings are the available columns in table 'laporanpendapatantindakan_v':
 * @property string $tglpembayaran
 * @property integer $pasien_id
 * @property string $no_rekam_medik
 * @property string $namadepan
 * @property string $nama_pasien
 * @property integer $pendaftaran_id
 * @property string $no_pendaftaran
 * @property string $tgl_pendaftaran
 * @property string $umur
 * @property string $no_asuransi
 * @property string $namapemilik_asuransi
 * @property string $nopokokperusahaan
 * @property string $namaperusahaan
 * @property string $tglselesaiperiksa
 * @property integer $tindakanpelayanan_id
 * @property integer $penjamin_id
 * @property string $penjamin_nama
 * @property integer $kelaspelayanan_id
 * @property string $kelaspelayanan_nama
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 * @property string $tgl_tindakan
 * @property integer $daftartindakan_id
 * @property string $daftartindakan_kode
 * @property string $daftartindakan_nama
 * @property integer $tipepaket_id
 * @property string $tipepaket_nama
 * @property boolean $daftartindakan_karcis
 * @property boolean $daftartindakan_visite
 * @property boolean $daftartindakan_akomodasi
 * @property boolean $daftartindakan_tindakan
 * @property boolean $daftartindakan_konsul
 * @property double $tarif_tindakan
 * @property string $satuantindakan
 * @property integer $qty_tindakan
 * @property boolean $cyto_tindakan
 * @property double $tarifcyto_tindakan
 * @property double $discount_tindakan
 * @property double $pembebasan_tindakan
 * @property double $subsidiasuransi_tindakan
 * @property double $subsidipemerintah_tindakan
 * @property double $subsisidirumahsakit_tindakan
 * @property double $iurbiaya_tindakan
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 * @property integer $pembayaranpelayanan_id
 * @property integer $kategoritindakan_id
 * @property string $kategoritindakan_nama
 * @property integer $pegawai_id
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property integer $gelarbelakang_id
 * @property string $gelarbelakang_nama
 * @property integer $ruanganpendaftaran_id
 * @property integer $tindakansudahbayar_id
 * @property integer $tindakankomponen_id
 * @property integer $komponentarif_id
 * @property string $komponentarif_nama
 * @property double $tarif_tindakankomp
 * @property double $tarifcyto_tindakankomp
 * @property double $subsidiasuransikomp
 * @property double $subsidipemerintahkomp
 * @property double $subsidirumahsakitkomp
 * @property double $iurbiayakomp
 * @property double $tarif_satuan
 */
class LaporanpendapatantindakanV extends CActiveRecord {

    public $tglAwal, $tglAkhir, $bulan, $hari, $tahun;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LaporanpendapatantindakanV the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'laporanpendapatantindakan_v';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('pasien_id, pendaftaran_id, tindakanpelayanan_id, penjamin_id, kelaspelayanan_id, ruangan_id, daftartindakan_id, tipepaket_id, qty_tindakan, pembayaranpelayanan_id, kategoritindakan_id, pegawai_id, gelarbelakang_id, ruanganpendaftaran_id, tindakansudahbayar_id, tindakankomponen_id, komponentarif_id', 'numerical', 'integerOnly' => true),
            array('tarif_tindakan, tarifcyto_tindakan, discount_tindakan, pembebasan_tindakan, subsidiasuransi_tindakan, subsidipemerintah_tindakan, subsisidirumahsakit_tindakan, iurbiaya_tindakan, tarif_tindakankomp, tarifcyto_tindakankomp, subsidiasuransikomp, subsidipemerintahkomp, subsidirumahsakitkomp, iurbiayakomp, tarif_satuan', 'numerical'),
            array('no_rekam_medik, satuantindakan, gelardepan', 'length', 'max' => 10),
            array('namadepan, no_pendaftaran, daftartindakan_kode', 'length', 'max' => 20),
            array('nama_pasien, no_asuransi, namapemilik_asuransi, nopokokperusahaan, namaperusahaan, penjamin_nama, kelaspelayanan_nama, ruangan_nama, tipepaket_nama, nama_pegawai', 'length', 'max' => 50),
            array('umur', 'length', 'max' => 30),
            array('daftartindakan_nama', 'length', 'max' => 200),
            array('kategoritindakan_nama', 'length', 'max' => 150),
            array('gelarbelakang_nama', 'length', 'max' => 15),
            array('komponentarif_nama', 'length', 'max' => 25),
            array('tglpembayaran, tgl_pendaftaran, tglselesaiperiksa, tgl_tindakan, daftartindakan_karcis, daftartindakan_visite, daftartindakan_akomodasi, daftartindakan_tindakan, daftartindakan_konsul, cyto_tindakan, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('tglAwal, tglAkhir, tglpembayaran, pasien_id, no_rekam_medik, namadepan, nama_pasien, pendaftaran_id, no_pendaftaran, tgl_pendaftaran, umur, no_asuransi, namapemilik_asuransi, nopokokperusahaan, namaperusahaan, tglselesaiperiksa, tindakanpelayanan_id, penjamin_id, penjamin_nama, kelaspelayanan_id, kelaspelayanan_nama, ruangan_id, ruangan_nama, tgl_tindakan, daftartindakan_id, daftartindakan_kode, daftartindakan_nama, tipepaket_id, tipepaket_nama, daftartindakan_karcis, daftartindakan_visite, daftartindakan_akomodasi, daftartindakan_tindakan, daftartindakan_konsul, tarif_tindakan, satuantindakan, qty_tindakan, cyto_tindakan, tarifcyto_tindakan, discount_tindakan, pembebasan_tindakan, subsidiasuransi_tindakan, subsidipemerintah_tindakan, subsisidirumahsakit_tindakan, iurbiaya_tindakan, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan, pembayaranpelayanan_id, kategoritindakan_id, kategoritindakan_nama, pegawai_id, gelardepan, nama_pegawai, gelarbelakang_id, gelarbelakang_nama, ruanganpendaftaran_id, tindakansudahbayar_id, tindakankomponen_id, komponentarif_id, komponentarif_nama, tarif_tindakankomp, tarifcyto_tindakankomp, subsidiasuransikomp, subsidipemerintahkomp, subsidirumahsakitkomp, iurbiayakomp, tarif_satuan', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'tglpembayaran' => 'Tglpembayaran',
            'pasien_id' => 'Pasien',
            'no_rekam_medik' => 'No Rekam Medik',
            'namadepan' => 'Namadepan',
            'nama_pasien' => 'Nama Pasien',
            'pendaftaran_id' => 'Pendaftaran',
            'no_pendaftaran' => 'No Pendaftaran',
            'tgl_pendaftaran' => 'Tgl Pendaftaran',
            'umur' => 'Umur',
            'no_asuransi' => 'No Asuransi',
            'namapemilik_asuransi' => 'Namapemilik Asuransi',
            'nopokokperusahaan' => 'Nopokokperusahaan',
            'namaperusahaan' => 'Namaperusahaan',
            'tglselesaiperiksa' => 'Tglselesaiperiksa',
            'tindakanpelayanan_id' => 'Tindakanpelayanan',
            'penjamin_id' => 'Penjamin',
            'penjamin_nama' => 'Penjamin Nama',
            'kelaspelayanan_id' => 'Kelaspelayanan',
            'kelaspelayanan_nama' => 'Kelaspelayanan Nama',
            'ruangan_id' => 'Ruangan',
            'ruangan_nama' => 'Ruangan Nama',
            'tgl_tindakan' => 'Tgl Tindakan',
            'daftartindakan_id' => 'Daftartindakan',
            'daftartindakan_kode' => 'Daftartindakan Kode',
            'daftartindakan_nama' => 'Daftartindakan Nama',
            'tipepaket_id' => 'Tipepaket',
            'tipepaket_nama' => 'Tipepaket Nama',
            'daftartindakan_karcis' => 'Daftartindakan Karcis',
            'daftartindakan_visite' => 'Daftartindakan Visite',
            'daftartindakan_akomodasi' => 'Daftartindakan Akomodasi',
            'daftartindakan_tindakan' => 'Daftartindakan Tindakan',
            'daftartindakan_konsul' => 'Daftartindakan Konsul',
            'tarif_tindakan' => 'Tarif Tindakan',
            'satuantindakan' => 'Satuantindakan',
            'qty_tindakan' => 'Qty Tindakan',
            'cyto_tindakan' => 'Cyto Tindakan',
            'tarifcyto_tindakan' => 'Tarifcyto Tindakan',
            'discount_tindakan' => 'Discount Tindakan',
            'pembebasan_tindakan' => 'Pembebasan Tindakan',
            'subsidiasuransi_tindakan' => 'Subsidiasuransi Tindakan',
            'subsidipemerintah_tindakan' => 'Subsidipemerintah Tindakan',
            'subsisidirumahsakit_tindakan' => 'Subsisidirumahsakit Tindakan',
            'iurbiaya_tindakan' => 'Iurbiaya Tindakan',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'create_loginpemakai_id' => 'Create Loginpemakai',
            'update_loginpemakai_id' => 'Update Loginpemakai',
            'create_ruangan' => 'Create Ruangan',
            'pembayaranpelayanan_id' => 'Pembayaranpelayanan',
            'kategoritindakan_id' => 'Kategoritindakan',
            'kategoritindakan_nama' => 'Kategoritindakan Nama',
            'pegawai_id' => 'Pegawai',
            'gelardepan' => 'Gelardepan',
            'nama_pegawai' => 'Nama Pegawai',
            'gelarbelakang_id' => 'Gelarbelakang',
            'gelarbelakang_nama' => 'Gelarbelakang Nama',
            'ruanganpendaftaran_id' => 'Ruanganpendaftaran',
            'tindakansudahbayar_id' => 'Tindakansudahbayar',
            'tindakankomponen_id' => 'Tindakankomponen',
            'komponentarif_id' => 'Komponentarif',
            'komponentarif_nama' => 'Komponentarif Nama',
            'tarif_tindakankomp' => 'Tarif Tindakankomp',
            'tarifcyto_tindakankomp' => 'Tarifcyto Tindakankomp',
            'subsidiasuransikomp' => 'Subsidiasuransikomp',
            'subsidipemerintahkomp' => 'Subsidipemerintahkomp',
            'subsidirumahsakitkomp' => 'Subsidirumahsakitkomp',
            'iurbiayakomp' => 'Iurbiayakomp',
            'tarif_satuan' => 'Tarif Satuan',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('tglpembayaran', $this->tglpembayaran, true);
        $criteria->compare('pasien_id', $this->pasien_id);
        $criteria->compare('no_rekam_medik', $this->no_rekam_medik, true);
        $criteria->compare('namadepan', $this->namadepan, true);
        $criteria->compare('nama_pasien', $this->nama_pasien, true);
        $criteria->compare('pendaftaran_id', $this->pendaftaran_id);
        $criteria->compare('no_pendaftaran', $this->no_pendaftaran, true);
        $criteria->compare('tgl_pendaftaran', $this->tgl_pendaftaran, true);
        $criteria->compare('umur', $this->umur, true);
        $criteria->compare('no_asuransi', $this->no_asuransi, true);
        $criteria->compare('namapemilik_asuransi', $this->namapemilik_asuransi, true);
        $criteria->compare('nopokokperusahaan', $this->nopokokperusahaan, true);
        $criteria->compare('namaperusahaan', $this->namaperusahaan, true);
        $criteria->compare('tglselesaiperiksa', $this->tglselesaiperiksa, true);
        $criteria->compare('tindakanpelayanan_id', $this->tindakanpelayanan_id);
        $criteria->compare('penjamin_id', $this->penjamin_id);
        $criteria->compare('penjamin_nama', $this->penjamin_nama, true);
        $criteria->compare('kelaspelayanan_id', $this->kelaspelayanan_id);
        $criteria->compare('kelaspelayanan_nama', $this->kelaspelayanan_nama, true);
        $criteria->compare('ruangan_id', $this->ruangan_id);
        $criteria->compare('ruangan_nama', $this->ruangan_nama, true);
        $criteria->compare('tgl_tindakan', $this->tgl_tindakan, true);
        $criteria->compare('daftartindakan_id', $this->daftartindakan_id);
        $criteria->compare('daftartindakan_kode', $this->daftartindakan_kode, true);
        $criteria->compare('daftartindakan_nama', $this->daftartindakan_nama, true);
        $criteria->compare('tipepaket_id', $this->tipepaket_id);
        $criteria->compare('tipepaket_nama', $this->tipepaket_nama, true);
        $criteria->compare('daftartindakan_karcis', $this->daftartindakan_karcis);
        $criteria->compare('daftartindakan_visite', $this->daftartindakan_visite);
        $criteria->compare('daftartindakan_akomodasi', $this->daftartindakan_akomodasi);
        $criteria->compare('daftartindakan_tindakan', $this->daftartindakan_tindakan);
        $criteria->compare('daftartindakan_konsul', $this->daftartindakan_konsul);
        $criteria->compare('tarif_tindakan', $this->tarif_tindakan);
        $criteria->compare('satuantindakan', $this->satuantindakan, true);
        $criteria->compare('qty_tindakan', $this->qty_tindakan);
        $criteria->compare('cyto_tindakan', $this->cyto_tindakan);
        $criteria->compare('tarifcyto_tindakan', $this->tarifcyto_tindakan);
        $criteria->compare('discount_tindakan', $this->discount_tindakan);
        $criteria->compare('pembebasan_tindakan', $this->pembebasan_tindakan);
        $criteria->compare('subsidiasuransi_tindakan', $this->subsidiasuransi_tindakan);
        $criteria->compare('subsidipemerintah_tindakan', $this->subsidipemerintah_tindakan);
        $criteria->compare('subsisidirumahsakit_tindakan', $this->subsisidirumahsakit_tindakan);
        $criteria->compare('iurbiaya_tindakan', $this->iurbiaya_tindakan);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('create_loginpemakai_id', $this->create_loginpemakai_id, true);
        $criteria->compare('update_loginpemakai_id', $this->update_loginpemakai_id, true);
        $criteria->compare('create_ruangan', $this->create_ruangan, true);
        $criteria->compare('pembayaranpelayanan_id', $this->pembayaranpelayanan_id);
        $criteria->compare('kategoritindakan_id', $this->kategoritindakan_id);
        $criteria->compare('kategoritindakan_nama', $this->kategoritindakan_nama, true);
        $criteria->compare('pegawai_id', $this->pegawai_id);
        $criteria->compare('gelardepan', $this->gelardepan, true);
        $criteria->compare('nama_pegawai', $this->nama_pegawai, true);
        $criteria->compare('gelarbelakang_id', $this->gelarbelakang_id);
        $criteria->compare('gelarbelakang_nama', $this->gelarbelakang_nama, true);
        $criteria->compare('ruanganpendaftaran_id', $this->ruanganpendaftaran_id);
        $criteria->compare('tindakansudahbayar_id', $this->tindakansudahbayar_id);
        $criteria->compare('tindakankomponen_id', $this->tindakankomponen_id);
        $criteria->compare('komponentarif_id', $this->komponentarif_id);
        $criteria->compare('komponentarif_nama', $this->komponentarif_nama, true);
        $criteria->compare('tarif_tindakankomp', $this->tarif_tindakankomp);
        $criteria->compare('tarifcyto_tindakankomp', $this->tarifcyto_tindakankomp);
        $criteria->compare('subsidiasuransikomp', $this->subsidiasuransikomp);
        $criteria->compare('subsidipemerintahkomp', $this->subsidipemerintahkomp);
        $criteria->compare('subsidirumahsakitkomp', $this->subsidirumahsakitkomp);
        $criteria->compare('iurbiayakomp', $this->iurbiayakomp);
        $criteria->compare('tarif_satuan', $this->tarif_satuan);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    // -- REKAP JASA DOKTER -- //        
    public function searchJasaDokter($pagination = true) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tglpembayaran', $this->tglAwal, $this->tglAkhir);
        $criteria->addInCondition('komponentarif_id', $this->komponentarif_id);
        if (!empty($this->pegawai_id)) {
            $criteria->compare('pegawai_id', $this->pegawai_id);
        }

        if ($pagination == false) {
            $pagination = false;
            $criteria->limit = -1;
        } else {
            $pagination = array('pageSize' => 10);
        }
        //$criteria->limit = 1;
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => $pagination,
        ));
    }

}
