<?php

/**
 * This is the model class for table "pendaftaran_t".
 *
 * The followings are the available columns in table 'pendaftaran_t':
 * @property integer $pendaftaran_id
 * @property integer $penjamin_id
 * @property integer $caramasuk_id
 * @property integer $carabayar_id
 * 
 * @property integer $pegawai_id
 * 
 * @property integer $pasien_id
 * @property integer $shift_id
 * @property integer $golonganumur_id
 * @property integer $kelaspelayanan_id
 * @property integer $rujukan_id
 * @property integer $penanggungjawab_id
 * @property integer $ruangan_id
 * @property integer $instalasi_id
 * @property integer $jeniskasuspenyakit_id
 * @property string $no_pendaftaran
 * @property string $tgl_pendaftaran
 * @property integer $no_urutantri
 * @property string $transportasi
 * @property string $keadaanmasuk
 * @property string $statusperiksa
 * @property string $statuspasien
 * @property string $kunjungan
 * @property boolean $alihstatus
 * @property boolean $byphone
 * @property boolean $kunjunganrumah
 * @property string $statusmasuk
 * @property string $umur
 * 
 * @property string $no_asuransi
 * @property string $namapemilik_asuransi
 * @property string $nopokokperusahaan
 * @property integer $kelastanggungan_id
 * @property string $namaperusahaan
 * 
 * @property string $create_time
 * @property string $upate_time
 * @property string $create_loginpemakai_id
 * @property string $upate_loginpemakai_id
 * @property string $create_ruangan
 * @property boolean $nopendaftaran_aktif
 * @property string $status_konfirmasi
 * @property date $tgl_konfirmasi
 * @property date $tglrenkontrol
 * @proeprty boolean $statusfarmasi
 */
class PendaftaranT extends CActiveRecord
{
        public $noRekamMedik;
        public $namaPasien;
        public $namaBin;
        public $alamat;
        public $propinsi;
        public $propinsi_id, $kabupaten_id;
        public $pendidikan_id, $pekerjaan_id, $suku_id;
        public $kabupaten;
        public $kecamatan;
        public $kelurahan;
        public $rt;
        public $rw;
        public $jenisidentitas;
        public $no_identitas_pasien;
        public $namadepan;
        public $nama_pasien;
        public $nama_bin;
        public $jeniskelamin;
        public $tempat_lahir;
        public $tanggal_lahir;
        public $alamat_pasien;
        public $no_rekam_medik;
        public $tgl_rekam_medik;
        public $kelaspelayanan_nama;
        public $profilrs_id;
        
        public $isRujukan = false;
        public $isPasienLama = false;
        public $adaKarcis = false;
        public $adaTindakanKonsul = false;
        public $adaKarcisKonsul = false;
        public $pakeAsuransi = false;
        public $adaPenanggungJawab = false;
        public $pakeSample = false;
        public $isKecelakaan = false;
        public $jeniskasuspenyakit_nama;
        public $isUpdatePasien = false;
        
        public $tglAwal, $tglAkhir;
        
        public $dokter_nama;
        public $dokter_id;
        public $gelardokter;
        
        public $instalasi_nama;
        public $ruangan_nama;
        public $kunjunganbaru, $kunjunganlama;
        public $bulan,$tahun;
        
        public $data,$jumlah,$tick;
        
        public $isPemeriksaanLab = true; //Jika Pemeriksaan Laboratorium di ceklis
        public $isPemeriksaanRad = false; //Jika Pemeriksaan Radiologi di ceklis
        
        public $adaBiayaAdministrasi = false;
        public $tarifAdministrasi;
		public $jml;
		
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PendaftaranT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pendaftaran_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pegawai_id, pasien_id, no_pendaftaran, tgl_pendaftaran, no_urutantri, statusperiksa, statuspasien, kunjungan, statusmasuk, umur, ruangan_id, penanggungjawab_id, carabayar_id, penjamin_id, jeniskasuspenyakit_id', 'required'),
			array('penjamin_id, caramasuk_id, carabayar_id, pasien_id, shift_id, golonganumur_id, kelaspelayanan_id, rujukan_id, penanggungjawab_id, ruangan_id, instalasi_id, jeniskasuspenyakit_id, no_urutantri', 'numerical', 'integerOnly'=>true),
			array('no_pendaftaran', 'length', 'max'=>20),
			array('transportasi, keadaanmasuk, statusperiksa, statuspasien, kunjungan, statusmasuk, status_konfirmasi', 'length', 'max'=>50),
			array('umur, thn, bln, hr', 'length', 'max'=>30),
			array('pegawai_id, statusfarmasi, alihstatus, byphone, kunjunganrumah, nopendaftaran_aktif, no_asuransi, namapemilik_asuransi
                              nopokokperusahaan, kelastanggungan_id, namaperusahaan, status_konfirmasi, tgl_konfirmasi,tglrenkontrol', 'safe'),
                    
                        array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
                    
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pendaftaran_id, status_konfirmasi, tglAwal, bulan, no_rekam_medik, nama_pasien, alamat_pasien, data,jumlah,tick,tahun, tglAkhir, propinsi_id, kabupaten_id, pendidikan_id, suku_id, pekerjaan_id, tgl_konfirmasi, penjamin_id, caramasuk_id, carabayar_id, tglselesaiperiksa, pasien_id, shift_id, golonganumur_id, kelaspelayanan_id, rujukan_id, penanggungjawab_id, ruangan_id, instalasi_id, jeniskasuspenyakit_id, no_pendaftaran, tgl_pendaftaran, no_urutantri, transportasi, keadaanmasuk, statusperiksa, statuspasien, kunjungan, alihstatus, byphone, kunjunganrumah, statusmasuk, umur, create_time, update_time, create_loginpemakai_id, upate_loginpemakai_id, create_ruangan, nopendaftaran_aktif,noRekamMedik, namaPasien, namaBin, alamat, propinsi, kabupaten, kecamatan, kelurahan, rt, rw,tglrenkontrol,statusfarmasi', 'safe', 'on'=>'search'),
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{ 
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                        'pasien'=>array(self::BELONGS_TO,'PasienM','pasien_id'),
                        'kelaspelayanan'=>array(self::BELONGS_TO, 'KelaspelayananM', 'kelaspelayanan_id'),
                        'penanggungJawab'=>array(self::BELONGS_TO,'PenanggungjawabM','penanggungjawab_id'),
                        'ruangan'=>array(self::BELONGS_TO,'RuanganM','ruangan_id'),
                        'kasuspenyakit'=>array(self::BELONGS_TO,'JeniskasuspenyakitM','jeniskasuspenyakit_id'),
                        'dokter'=>array(self::BELONGS_TO,'PegawaiM','pegawai_id'),
                        'penjamin'=>array(self::BELONGS_TO, 'PenjaminpasienM', 'penjamin_id'),
                        'instalasi'=>array(self::BELONGS_TO, 'InstalasiM', 'instalasi_id'),
                        'carabayar'=>array(self::BELONGS_TO,  'CarabayarM', 'carabayar_id'),
                        'kirimkeunitlain'=>array(self::HAS_MANY, 'PasienkirimkeunitlainT', 'pendaftaran_id'),
                        'anamnesa'=>array(self::HAS_ONE, 'AnamnesaT', 'pendaftaran_id'),
                        'piutangasuransi'=>array(self::HAS_ONE, 'PiutangasuransiT', 'pendaftaran_id'),
                        'pemeriksaanfisik'=>array(self::HAS_ONE, 'PemeriksaanfisikT', 'pendaftaran_id'),
                        'hasilpemeriksaanlab'=>array(self::HAS_ONE, 'HasilpemeriksaanlabT', 'pendaftaran_id'),
                        'pasienmasukpenunjang'=>array(self::HAS_ONE, 'PasienmasukpenunjangT', 'pendaftaran_id'),
                        'diagnosa'=>array(self::HAS_MANY, 'PasienmorbiditasT', 'pendaftaran_id'),
                        'pegawai'=>array(self::BELONGS_TO, 'PegawaiM', 'pegawai_id'),
                        'pasienadmisi'=>array(self::BELONGS_TO, 'PasienadmisiT','pasienadmisi_id'),
                        'pasienpulang'=>array(self::BELONGS_TO, 'PasienpulangT','pasienpulang_id'),
                        'pembayaranpelayanan'=>array(self::HAS_MANY,'PembayaranpelayananT','pembayaranpelayanan_id'),
                        'obatalkespasien'=>array(self::HAS_MANY,'ObatalkespasienT','pendaftaran_id'),
                        'tindakanpelayanan'=>array(self::HAS_MANY,'TindakanpelayananT','pendaftaran_id'),
                        'penjualanresep'=>array(self::HAS_MANY,'PenjualanresepT','pendaftaran_id'),
                        'rujukan'=>array(self::BELONGS_TO,'RujukanT','rujukan_id'),
                        'rujukanNama'=>array(self::BELONGS_TO,'RujukanM','rujukan_id'),
                        'golonganumur'=>array(self::BELONGS_TO,'GolonganumurM','golonganumur_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pendaftaran_id' => 'Pendaftaran',
			'penjamin_id' => 'Penjamin',
			'caramasuk_id' => 'Cara Masuk',
			'carabayar_id' => 'Cara Bayar',
                        'pegawai_id' => 'Pegawai',
                    
			'pasien_id' => 'Pasien',
			'shift_id' => 'Shift',
			'golonganumur_id' => 'Golongan Umur',
			'kelaspelayanan_id' => 'Kelas Pelayanan',
			'rujukan_id' => 'Rujukan',
			'penanggungjawab_id' => 'Penanggung Jawab',
			'ruangan_id' => 'Ruangan',
			'instalasi_id' => 'Instalasi',
			'jeniskasuspenyakit_id' => 'Jenis Kasus Penyakit',
			'no_pendaftaran' => 'No Pendaftaran',
			'tgl_pendaftaran' => 'Tgl Pendaftaran',
			'no_urutantri' => 'No Urut Antri',
			'transportasi' => 'Transportasi',
			'keadaanmasuk' => 'Keadaan Masuk',
			'statusperiksa' => 'Status Periksa',
			'statuspasien' => 'Status Pasien',
			'kunjungan' => 'Kunjungan',
			'alihstatus' => 'Alih Status',
			'byphone' => 'By Phone',
			'kunjunganrumah' => 'Kunjungan Rumah',
			'statusmasuk' => 'Status Masuk',
			'umur' => 'Umur',
                    
                        'no_asuransi' => 'No Asuransi',
			'namapemilik_asuransi' => 'Nama Pemilik Asuransi',
			'nopokokperusahaan' => 'No Pokok Perusahaan',
			'kelastanggungan_id' => 'Kelas Tanggungan',
			'namaperusahaan' => 'Nama Perusahaan',
                    
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Login Pemakai',
			'upate_loginpemakai_id' => 'Upate Login Pemakai',
			'create_ruangan' => 'Create Ruangan',
			'nopendaftaran_aktif' => 'No Pendaftaran Aktif',
                        'tglselesaiperiksa'=>'Tanggal Selesai Periksa',
                        'noRekamMedik'=>'No Rekam Medik',
                        'status_konfirmasi'=>'Status Konfirmasi',
                        'tgl_konfirmasi'=>'Tgl Konfirmasi',
                        'tglrenkontrol'=>'Tgl Rencana Kontrol',
                        'statusfarmasi'=>'Status Farmasi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('caramasuk_id',$this->caramasuk_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('golonganumur_id',$this->golonganumur_id);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('rujukan_id',$this->rujukan_id);
		$criteria->compare('penanggungjawab_id',$this->penanggungjawab_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('no_urutantri',$this->no_urutantri);
		$criteria->compare('LOWER(transportasi)',strtolower($this->transportasi),true);
		$criteria->compare('LOWER(keadaanmasuk)',strtolower($this->keadaanmasuk),true);
		$criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
		$criteria->compare('LOWER(statuspasien)',strtolower($this->statuspasien),true);
		$criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
		$criteria->compare('alihstatus',$this->alihstatus);
		$criteria->compare('tglrenkontrol',$this->tglrenkontrol);
		$criteria->compare('byphone',$this->byphone);
		$criteria->compare('kunjunganrumah',$this->kunjunganrumah);
		$criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
		$criteria->compare('LOWER(umur)',strtolower($this->umur),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('nopendaftaran_aktif',$this->nopendaftaran_aktif);
		$criteria->compare('status_konfirmasi',$this->status_konfirmasi);		
		$criteria->compare('date(tgl_konfirmasi)',$this->tgl_konfirmasi);
                $criteria->compare('statusfarmasi',$this->statusfarmasi);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('caramasuk_id',$this->caramasuk_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('golonganumur_id',$this->golonganumur_id);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('rujukan_id',$this->rujukan_id);
		$criteria->compare('penanggungjawab_id',$this->penanggungjawab_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('no_urutantri',$this->no_urutantri);
		$criteria->compare('LOWER(transportasi)',strtolower($this->transportasi),true);
		$criteria->compare('LOWER(keadaanmasuk)',strtolower($this->keadaanmasuk),true);
		$criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
		$criteria->compare('LOWER(statuspasien)',strtolower($this->statuspasien),true);
		$criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
		$criteria->compare('alihstatus',$this->alihstatus);
                $criteria->compare('tglrenkontrol',$this->tglrenkontrol);
		$criteria->compare('byphone',$this->byphone);
		$criteria->compare('kunjunganrumah',$this->kunjunganrumah);
		$criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
		$criteria->compare('LOWER(umur)',strtolower($this->umur),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(upate_time)',strtolower($this->upate_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(upate_loginpemakai_id)',strtolower($this->upate_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('nopendaftaran_aktif',$this->nopendaftaran_aktif);
		$criteria->compare('status_konfirmasi',$this->status_konfirmasi);
		$criteria->compare('date(tgl_konfirmasi)',$this->tgl_konfirmasi);
                $criteria->compare('statusfarmasi',$this->statusfarmasi);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
        
        public function searchPasienPendaftaran()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
                $criteria->with=array('pasien','penanggungJawab');
                $criteria->compare('LOWER(pasien.no_rekam_medik)',  strtolower($this->noRekamMedik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)',  strtolower($this->namaPasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)',  strtolower($this->namaBin),true);
                $criteria->compare('LOWER(pasien.alamat_pasien)',  strtolower($this->alamat),true);
                $criteria->compare('pasien.propinsi_id',  strtolower($this->propinsi));
                $criteria->compare('pasien.kabupaten_id',  strtolower($this->kabupaten));
                $criteria->compare('pasien.kecamatan_id',  strtolower($this->kecamatan));
                $criteria->compare('pasien.kelurahan_id',  strtolower($this->kelurahan));
                $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
                $criteria->compare('penjamin_id',$this->penjamin_id);
                $criteria->compare('caramasuk_id',$this->caramasuk_id);
                $criteria->compare('carabayar_id',$this->carabayar_id);
                $criteria->compare('pasien_id',$this->pasien_id);
                $criteria->compare('shift_id',$this->shift_id);
                $criteria->compare('tglrenkontrol',$this->tglrenkontrol);
                $criteria->compare('golonganumur_id',$this->golonganumur_id);
                $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
                $criteria->compare('rujukan_id',$this->rujukan_id);
                $criteria->compare('penanggungjawab_id',$this->penanggungjawab_id);
                $criteria->compare('ruangan_id',$this->ruangan_id);
                $criteria->compare('instalasi_id',$this->instalasi_id);
                $criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
                $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
                $criteria->compare('no_urutantri',$this->no_urutantri);
                $criteria->compare('LOWER(transportasi)',strtolower($this->transportasi),true);
                $criteria->compare('LOWER(keadaanmasuk)',strtolower($this->keadaanmasuk),true);
                $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
                $criteria->compare('LOWER(statuspasien)',strtolower($this->statuspasien),true);
                $criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
                $criteria->compare('alihstatus',$this->alihstatus);
                $criteria->compare('byphone',$this->byphone);
                $criteria->compare('kunjunganrumah',$this->kunjunganrumah);
                $criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
                $criteria->compare('LOWER(umur)',strtolower($this->umur),true);
                $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
                $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
                $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
                $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
                $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->compare('nopendaftaran_aktif',$this->nopendaftaran_aktif);
                $criteria->compare('status_konfirmasi',$this->status_konfirmasi);
		$criteria->compare('date(tgl_konfirmasi)',$this->tgl_konfirmasi);
                $criteria->compare('statusfarmasi',$this->statusfarmasi);
                
                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date'){
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                    }elseif ($column->dbType == 'timestamp without time zone'){
//                            $this->$columnName = date('Y-m-d H:i:s', CDateTimeParser::parse($this->$columnName, Yii::app()->locale->dateFormat));
//                            $this->$columnName = date('Y-m-d H:i:s', strtotime($this->$columnName));
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                    }
            }

            return parent::beforeValidate();
        }

        public function beforeSave()
        {
            if($this->tglselesaiperiksa===null || trim($this->tglselesaiperiksa)==''){
	        $this->setAttribute('tglselesaiperiksa', null);
            }
            if($this->tgl_konfirmasi===null || trim($this->tgl_konfirmasi)==''){
	        $this->setAttribute('tgl_konfirmasi', null);
            }            
            if($this->tglrenkontrol===null || trim($this->tglrenkontrol)==''){
	        $this->setAttribute('tglrenkontrol', null);
            }            
            
            return parent::beforeSave();
        }
                
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                        }
            }
            return true;
        }
        
        /**
         * Mengambil daftar semua kelaspelayanan
         * @return CActiveDataProvider 
         */
        public function getKelasPelayananItems()
        {
            return KelaspelayananM::model()->findAllByAttributes(array('kelaspelayanan_aktif'=>true),array('order'=>'kelaspelayanan_id'));
        }
        
        /**
         * Mengambil daftar semua carabayar
         * @return CActiveDataProvider 
         */
        public function getCaraBayarItems()
        {
            return CarabayarM::model()->findAllByAttributes(array('carabayar_aktif'=>true),array('order'=>'carabayar_nourut'));
        }
        
        public function getCaraBayarItemsLab()
        {
            return CarabayarpendlabluarV::model()->findAllByAttributes(array('carabayar_aktif'=>true),array('order'=>'carabayar_nourut'));
        }
        
        /**
         * Mengambil daftar semua caramasuk
         * @return CActiveDataProvider 
         */
        public function getCaraMasukItems()
        {
            return CaramasukM::model()->findAllByAttributes(array('caramasuk_aktif'=>true),array('order'=>'caramasuk_nama'));
        }
        
        /**
         * Mengambil daftar semua penjamin
         * @return CActiveDataProvider 
         */
        public function getPenjaminItems($carabayar_id=null)
        {
            if(!empty($carabayar_id))
                    return PenjaminpasienM::model()->findAllByAttributes(array('penjamin_aktif'=>true,'carabayar_id'=>$carabayar_id),array('order'=>'penjamin_nama'));
            else
                    return array();
                    //return PenjaminpasienM::model()->findAllByAttributes(array('penjamin_aktif'=>true),array('order'=>'penjamin_nama'));
        }
        
        /**
         * Mengambil daftar semua ruangan
         * @return CActiveDataProvider 
         */
        public function getRuanganItems($instalasiId='')
        {
            if($instalasiId!='')
                return RuanganM::model()->findAllByAttributes(array('instalasi_id'=>$instalasiId,'ruangan_aktif'=>true),array('order'=>'ruangan_nama'));
            else
                return RuanganM::model()->findAllByAttributes(array('ruangan_aktif'=>true),array('order'=>'ruangan_nama'));
        }
        
        public function getJenisKasusPenyakitItems($ruangan_id = '')
        {            
            if($ruangan_id == ''){
                $ruangan_id = Yii::app()->user->getState('ruangan_id');
            }
            $criteria = new CDbCriteria();
            $criteria->join =  "
                JOIN jeniskasuspenyakit_m ON t.jeniskasuspenyakit_id = jeniskasuspenyakit_m.jeniskasuspenyakit_id
                JOIN ruangan_m ON t.ruangan_id = ruangan_m.ruangan_id
            ";
            $criteria->compare('t.ruangan_id', $ruangan_id);
            $criteria->order = "jeniskasuspenyakit_nama";
            $criteria->select = "jeniskasuspenyakit_m.jeniskasuspenyakit_id, jeniskasuspenyakit_m.jeniskasuspenyakit_nama";
            $modJenKasusPenyakit = KasuspenyakitruanganM::model()->findAll($criteria);
            return $modJenKasusPenyakit;
        }
        
        public function getDokterItems($ruangan_id='')
        {
            if(!empty($ruangan_id))
                return DokterV::model()->findAllByAttributes(array('ruangan_id'=>$ruangan_id), array('order'=>'pegawai_id'));
            else
                return DokterV::model()->findAllByAttributes(array('pegawai_aktif'=>true), array('order'=>'pegawai_id'));
        }
        
        public function getDokterItemsInstalasi($instalasi_id='')
        {
            if(!empty($instalasi_id))
                return DokterV::model()->findAllByAttributes(array('instalasi_id'=>$instalasi_id));
            else
                return array();
        }
        
        public function getParamedisItems($ruangan_id='')
        {
            if(!empty($ruangan_id))
                return ParamedisV::model()->findAllByAttributes(array('ruangan_id'=>$ruangan_id));
            else
                return array();
        }
        
        public function searchListKunjungan(){
            $criteria = new CDBCriteria();
            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->order = 'tgl_pendaftaran DESC';
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
        
        public function getPropinsiItems()
        {
            return PropinsiM::model()->findAll('propinsi_aktif=TRUE ORDER BY propinsi_nama');
        }
        
        public function getNamaNamaBIN()
        {
            return $this->nama_pasien.' bin '.$this->nama_bin;
        }
        
        public function getCaraBayarPenjamin()
        {
                return $this->carabayar_nama.' / '.$this->penjamin_nama;
        }
        
        public function getRTRW()
        {
            return $this->rt.' / '.$this->rw;
        }
        
         public function getPekerjaanItems()
        {
            return PekerjaanM::model()->findAll('pekerjaan_aktif=TRUE ORDER BY pekerjaan_nama');
        }
        
         public function getPendidikanItems()
        {
            return PendidikanM::model()->findAll('pendidikan_aktif=TRUE ORDER BY pendidikan_nama');
        }
        
         public function getSukuItems()
        {
            return SukuM::model()->findAll('suku_aktif=TRUE ORDER BY suku_nama');
        }
        public function getLamaRawat(){
            $format = new CustomFormat();
            $date1 = $format->formatDateTimeMediumForDB($this->tgl_pendaftaran);
            $date2 = date('Y-m-d H:i:s');
            $diff = abs(strtotime($date2) - strtotime($date1));
            $hours   = floor(($diff)/3600); 
            return $hours;
        }
        public function getPulang()
        {
            if(empty($this->alihstatus))
                return ' - ';
            else
                return $this->pasienpulang->carakeluar.' / '.$this->pasienpulang->kondisipulang;
        }
		
		public function getJumlahKunjungan($statuspasien,$ruangan_id){
			$criteria = new CDbCriteria;
			$criteria->select = 'count(*) as jml';
			$criteria->compare('statuspasien', $statuspasien);
			$criteria->compare('ruangan_id', $ruangan_id);
			$modKunjungan = PendaftaranT::model()->find($criteria);
			return $modKunjungan->jml;
		}
}
