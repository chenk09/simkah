<?php

/**
 * This is the model class for table "permintaan_beli_barang_t".
 *
 * The followings are the available columns in table 'permintaan_beli_barang_t':
 * @property integer $permintaanbelibarang_id
 * @property integer $ruangan_id
 * @property integer $instalasi_id
 * @property string $tanggal_pengajuan
 * @property string $no_permintaan_beli
 * @property string $keterangan
 * @property string $create_time
 * @property string $update_time
 * @property string $created
 * @property string $updated
 */
class PermintaanBeliBarangT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PermintaanBeliBarangT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'permintaan_beli_barang_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ruangan_id, instalasi_id, tanggal_pengajuan, no_permintaan_beli', 'required'),
			array('ruangan_id, instalasi_id', 'numerical', 'integerOnly'=>true),
			array('no_permintaan_beli', 'length', 'max'=>50),
			array('keterangan, update_time, updated', 'safe'),

			array('create_time', 'default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
			array('update_time', 'default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
			array('created','default','value'=>Yii::app()->user->id,'on'=>'insert'),
			array('updated','default','value'=>Yii::app()->user->id,'on'=>'update'),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('permintaanbelibarang_id, ruangan_id, instalasi_id, tanggal_pengajuan, no_permintaan_beli, keterangan, create_time, update_time, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ruangans'=>array(self::BELONGS_TO,'RuanganM','ruangan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'permintaanbelibarang_id' => 'Permintaanbelibarang',
			'ruangan_id' => 'Ruangan',
			'instalasi_id' => 'Instalasi',
			'tanggal_pengajuan' => 'Tanggal Pengajuan',
			'no_permintaan_beli' => 'No Permintaan Beli',
			'keterangan' => 'Keterangan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('permintaanbelibarang_id',$this->permintaanbelibarang_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('tanggal_pengajuan',$this->tanggal_pengajuan,true);
		$criteria->compare('no_permintaan_beli',$this->no_permintaan_beli,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}