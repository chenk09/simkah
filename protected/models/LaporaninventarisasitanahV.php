<?php

/**
 * This is the model class for table "laporaninventarisasitanah_v".
 *
 * The followings are the available columns in table 'laporaninventarisasitanah_v':
 * @property integer $inventaris_id
 * @property string $inventaris_kode
 * @property string $no_register
 * @property string $tgl_register
 * @property integer $golongan_id
 * @property string $golongan_kode
 * @property string $golongan_nama
 * @property integer $kelompok_id
 * @property string $kelompok_kode
 * @property string $kelompok_nama
 * @property integer $subkelompok_id
 * @property string $subkelompok_kode
 * @property string $subkelompok_nama
 * @property integer $bidang_id
 * @property string $bidang_kode
 * @property string $bidang_nama
 * @property integer $barang_id
 * @property string $barang_type
 * @property string $barang_kode
 * @property string $barang_nama
 * @property string $barang_merk
 * @property string $barang_noseri
 * @property string $barang_ukuran
 * @property string $barang_bahan
 * @property string $barang_thnbeli
 * @property string $barang_warna
 * @property boolean $barang_statusregister
 * @property integer $barang_ekonomis_thn
 * @property string $barang_satuan
 * @property integer $barang_jmldlmkemasan
 * @property string $barang_image
 * @property double $barang_harga
 * @property integer $asalaset_id
 * @property string $asalaset_nama
 * @property string $asalaset_singkatan
 * @property integer $lokasi_id
 * @property string $lokasiaset_kode
 * @property string $lokasiaset_namainstalasi
 * @property string $lokasiaset_namabagian
 * @property string $lokasiaset_namalokasi
 * @property string $invtanah_namabrg
 * @property string $invtanah_luas
 * @property string $invtanah_thnpengadaan
 * @property string $invtanah_tglguna
 * @property string $invtanah_alamat
 * @property string $invtanah_status
 * @property string $invtanah_tglsertifikat
 * @property string $invtanah_nosertifikat
 * @property string $invtanah_penggunaan
 * @property double $invtanah_harga
 * @property string $invtanah_ket
 */
class LaporaninventarisasitanahV extends CActiveRecord
{

	public $tglAwal, $tglAkhir;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporaninventarisasitanahV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporaninventarisasitanah_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('inventaris_id, golongan_id, kelompok_id, subkelompok_id, bidang_id, barang_id, barang_ekonomis_thn, barang_jmldlmkemasan, asalaset_id, lokasi_id', 'numerical', 'integerOnly'=>true),
			array('barang_harga, invtanah_harga', 'numerical'),
			array('inventaris_kode, no_register, golongan_kode, kelompok_kode, subkelompok_kode, bidang_kode, barang_type, barang_kode, barang_merk, barang_warna, barang_satuan, asalaset_nama, lokasiaset_kode, lokasiaset_namabagian, invtanah_status', 'length', 'max'=>50),
			array('golongan_nama, kelompok_nama, subkelompok_nama, bidang_nama, barang_nama, lokasiaset_namainstalasi, lokasiaset_namalokasi, invtanah_namabrg, invtanah_nosertifikat, invtanah_penggunaan, invtanah_ket', 'length', 'max'=>100),
			array('barang_noseri, barang_ukuran, barang_bahan', 'length', 'max'=>20),
			array('barang_thnbeli, invtanah_thnpengadaan', 'length', 'max'=>5),
			array('barang_image', 'length', 'max'=>200),
			array('asalaset_singkatan', 'length', 'max'=>10),
			array('invtanah_luas', 'length', 'max'=>30),
			array('tgl_register, barang_statusregister, invtanah_tglguna, invtanah_alamat, invtanah_tglsertifikat', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('inventaris_id, inventaris_kode, no_register, tgl_register, golongan_id, golongan_kode, golongan_nama, kelompok_id, kelompok_kode, kelompok_nama, subkelompok_id, subkelompok_kode, subkelompok_nama, bidang_id, bidang_kode, bidang_nama, barang_id, barang_type, barang_kode, barang_nama, barang_merk, barang_noseri, barang_ukuran, barang_bahan, barang_thnbeli, barang_warna, barang_statusregister, barang_ekonomis_thn, barang_satuan, barang_jmldlmkemasan, barang_image, barang_harga, asalaset_id, asalaset_nama, asalaset_singkatan, lokasi_id, lokasiaset_kode, lokasiaset_namainstalasi, lokasiaset_namabagian, lokasiaset_namalokasi, invtanah_namabrg, invtanah_luas, invtanah_thnpengadaan, invtanah_tglguna, invtanah_alamat, invtanah_status, invtanah_tglsertifikat, invtanah_nosertifikat, invtanah_penggunaan, invtanah_harga, invtanah_ket', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'inventaris_id' => 'Inventaris',
			'inventaris_kode' => 'Inventaris Kode',
			'no_register' => 'No Register',
			'tgl_register' => 'Tgl Register',
			'golongan_id' => 'Golongan',
			'golongan_kode' => 'Golongan Kode',
			'golongan_nama' => 'Golongan Nama',
			'kelompok_id' => 'Kelompok',
			'kelompok_kode' => 'Kelompok Kode',
			'kelompok_nama' => 'Kelompok Nama',
			'subkelompok_id' => 'Subkelompok',
			'subkelompok_kode' => 'Subkelompok Kode',
			'subkelompok_nama' => 'Subkelompok Nama',
			'bidang_id' => 'Bidang',
			'bidang_kode' => 'Bidang Kode',
			'bidang_nama' => 'Bidang Nama',
			'barang_id' => 'Barang',
			'barang_type' => 'Barang Type',
			'barang_kode' => 'Barang Kode',
			'barang_nama' => 'Barang Nama',
			'barang_merk' => 'Barang Merk',
			'barang_noseri' => 'Barang Noseri',
			'barang_ukuran' => 'Barang Ukuran',
			'barang_bahan' => 'Barang Bahan',
			'barang_thnbeli' => 'Barang Thnbeli',
			'barang_warna' => 'Barang Warna',
			'barang_statusregister' => 'Barang Statusregister',
			'barang_ekonomis_thn' => 'Barang Ekonomis Thn',
			'barang_satuan' => 'Barang Satuan',
			'barang_jmldlmkemasan' => 'Barang Jmldlmkemasan',
			'barang_image' => 'Barang Image',
			'barang_harga' => 'Barang Harga',
			'asalaset_id' => 'Asalaset',
			'asalaset_nama' => 'Asalaset Nama',
			'asalaset_singkatan' => 'Asalaset Singkatan',
			'lokasi_id' => 'Lokasi',
			'lokasiaset_kode' => 'Lokasiaset Kode',
			'lokasiaset_namainstalasi' => 'Lokasiaset Namainstalasi',
			'lokasiaset_namabagian' => 'Lokasiaset Namabagian',
			'lokasiaset_namalokasi' => 'Lokasiaset Namalokasi',
			'invtanah_namabrg' => 'Invtanah Namabrg',
			'invtanah_luas' => 'Invtanah Luas',
			'invtanah_thnpengadaan' => 'Invtanah Thnpengadaan',
			'invtanah_tglguna' => 'Invtanah Tglguna',
			'invtanah_alamat' => 'Invtanah Alamat',
			'invtanah_status' => 'Invtanah Status',
			'invtanah_tglsertifikat' => 'Invtanah Tglsertifikat',
			'invtanah_nosertifikat' => 'Invtanah Nosertifikat',
			'invtanah_penggunaan' => 'Invtanah Penggunaan',
			'invtanah_harga' => 'Invtanah Harga',
			'invtanah_ket' => 'Invtanah Ket',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('inventaris_id',$this->inventaris_id);
		$criteria->compare('inventaris_kode',$this->inventaris_kode,true);
		$criteria->compare('no_register',$this->no_register,true);
		$criteria->compare('tgl_register',$this->tgl_register,true);
		$criteria->compare('golongan_id',$this->golongan_id);
		$criteria->compare('golongan_kode',$this->golongan_kode,true);
		$criteria->compare('golongan_nama',$this->golongan_nama,true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('kelompok_kode',$this->kelompok_kode,true);
		$criteria->compare('kelompok_nama',$this->kelompok_nama,true);
		$criteria->compare('subkelompok_id',$this->subkelompok_id);
		$criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
		$criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
		$criteria->compare('bidang_id',$this->bidang_id);
		$criteria->compare('bidang_kode',$this->bidang_kode,true);
		$criteria->compare('bidang_nama',$this->bidang_nama,true);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('barang_type',$this->barang_type,true);
		$criteria->compare('barang_kode',$this->barang_kode,true);
		$criteria->compare('barang_nama',$this->barang_nama,true);
		$criteria->compare('barang_merk',$this->barang_merk,true);
		$criteria->compare('barang_noseri',$this->barang_noseri,true);
		$criteria->compare('barang_ukuran',$this->barang_ukuran,true);
		$criteria->compare('barang_bahan',$this->barang_bahan,true);
		$criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
		$criteria->compare('barang_warna',$this->barang_warna,true);
		$criteria->compare('barang_statusregister',$this->barang_statusregister);
		$criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
		$criteria->compare('barang_satuan',$this->barang_satuan,true);
		$criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
		$criteria->compare('barang_image',$this->barang_image,true);
		$criteria->compare('barang_harga',$this->barang_harga);
		$criteria->compare('asalaset_id',$this->asalaset_id);
		$criteria->compare('asalaset_nama',$this->asalaset_nama,true);
		$criteria->compare('asalaset_singkatan',$this->asalaset_singkatan,true);
		$criteria->compare('lokasi_id',$this->lokasi_id);
		$criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
		$criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
		$criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
		$criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
		$criteria->compare('invtanah_namabrg',$this->invtanah_namabrg,true);
		$criteria->compare('invtanah_luas',$this->invtanah_luas,true);
		$criteria->compare('invtanah_thnpengadaan',$this->invtanah_thnpengadaan,true);
		$criteria->compare('invtanah_tglguna',$this->invtanah_tglguna,true);
		$criteria->compare('invtanah_alamat',$this->invtanah_alamat,true);
		$criteria->compare('invtanah_status',$this->invtanah_status,true);
		$criteria->compare('invtanah_tglsertifikat',$this->invtanah_tglsertifikat,true);
		$criteria->compare('invtanah_nosertifikat',$this->invtanah_nosertifikat,true);
		$criteria->compare('invtanah_penggunaan',$this->invtanah_penggunaan,true);
		$criteria->compare('invtanah_harga',$this->invtanah_harga);
		$criteria->compare('invtanah_ket',$this->invtanah_ket,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}