<?php

/**
 * This is the model class for table "fakturpembelian_t".
 *
 * The followings are the available columns in table 'fakturpembelian_t':
 * @property integer $fakturpembelian_id
 * @property integer $supplier_id
 * @property integer $syaratbayar_id
 * @property integer $suratpesanan_id
 * @property integer $ruangan_id
 * @property string $nofaktur
 * @property string $tglfaktur
 * @property string $tgljatuhtempo
 * @property string $keteranganfaktur
 * @property double $totharganetto
 * @property double $persendiscount
 * @property double $jmldiscount
 * @property double $biayamaterai
 * @property double $totalpajakpph
 * @property double $totalpajakppn
 * @property double $totalhargabruto
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class FakturpembelianT extends CActiveRecord
{
        public $tglAwal;
        public $tglAkhir;
        public $tglAwalJatuhTempo;
        public $tglAkhirJatuhTempo;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FakturpembelianT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fakturpembelian_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('supplier_id, ruangan_id, nofaktur, tglfaktur, tgljatuhtempo', 'required'),
			array('supplier_id, penerimaanbarang_id, syaratbayar_id, ruangan_id, terimapersediaan_id', 'numerical', 'integerOnly'=>true),
			array('totharganetto, persendiscount, jmldiscount, biayamaterai, totalpajakpph, totalpajakppn, totalhargabruto', 'numerical'),
			array('nofaktur', 'length', 'max'=>50),
			array('keteranganfaktur, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe'),
			array('create_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
			array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update,insert'),
			array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
			array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
			array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('fakturpembelian_id, tglAwal,tglAkhir,syaratbayar_id, suratpesanan_id, penerimaanbarang_id, ruangan_id, nofaktur, tglfaktur, tgljatuhtempo, keteranganfaktur, totharganetto, persendiscount, jmldiscount, biayamaterai, totalpajakpph, totalpajakppn, totalhargabruto, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan, terimapersediaan_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                   'supplier'=>array(self::BELONGS_TO, 'SupplierM','supplier_id'),
                   'syaratbayar'=>array(self::BELONGS_TO, 'SyaratbayarM','syaratbayar_id'), 
                   'ruangan'=>array(self::BELONGS_TO, 'RuanganM','ruangan_id') ,
                   'bayarkesupplier'=>array(self::BELONGS_TO, 'BayarkesupplierT','bayarkesupplier_id'),
                   'penerimaan'=>array(self::BELONGS_TO,'PenerimaanbarangT','penerimaanbarang_id'),
                   'terimapersediaan'=>array(self::BELONGS_TO,'TerimapersediaanT','terimapersediaan_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fakturpembelian_id' => 'Fakturpembelian',
			'penerimaanbarang_id'=>'Penerimaan Barang',
			'supplier_id' => 'Supplier',
			'syaratbayar_id' => 'Syarat Bayar',
			'suratpesanan_id' => 'Suratpesanan',
			'ruangan_id' => 'Ruangan',
			'nofaktur' => 'No Faktur',
			'tglfaktur' => 'Tanggal',
			'tgljatuhtempo' => 'Tanggal Jatuh Tempo',
			'keteranganfaktur' => 'Keterangan',
			'totharganetto' => 'Total Harga (Netto)',
			'persendiscount' => 'Persen Discount',
			'jmldiscount' => 'Discount',
			'biayamaterai' => 'Biaya Materai',
			'totalpajakpph' => 'Pph (Total)',
			'totalpajakppn' => 'Ppn (Total)',
			'totalhargabruto' => 'Total Harga (Bruto) / HPP',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('syaratbayar_id',$this->syaratbayar_id);
		$criteria->compare('suratpesanan_id',$this->suratpesanan_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(nofaktur)',strtolower($this->nofaktur),true);
		$criteria->compare('LOWER(tglfaktur)',strtolower($this->tglfaktur),true);
		$criteria->compare('LOWER(tgljatuhtempo)',strtolower($this->tgljatuhtempo),true);
		$criteria->compare('LOWER(keteranganfaktur)',strtolower($this->keteranganfaktur),true);
		$criteria->compare('totharganetto',$this->totharganetto);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('biayamaterai',$this->biayamaterai);
		$criteria->compare('totalpajakpph',$this->totalpajakpph);
		$criteria->compare('totalpajakppn',$this->totalpajakppn);
		$criteria->compare('totalhargabruto',$this->totalhargabruto);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
	public function searchPrint()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('syaratbayar_id',$this->syaratbayar_id);
		$criteria->compare('suratpesanan_id',$this->suratpesanan_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(nofaktur)',strtolower($this->nofaktur),true);
		$criteria->compare('LOWER(tglfaktur)',strtolower($this->tglfaktur),true);
		$criteria->compare('LOWER(tgljatuhtempo)',strtolower($this->tgljatuhtempo),true);
		$criteria->compare('LOWER(keteranganfaktur)',strtolower($this->keteranganfaktur),true);
		$criteria->compare('totharganetto',$this->totharganetto);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('biayamaterai',$this->biayamaterai);
		$criteria->compare('totalpajakpph',$this->totalpajakpph);
		$criteria->compare('totalpajakppn',$this->totalpajakppn);
		$criteria->compare('totalhargabruto',$this->totalhargabruto);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		// Klo limit lebih kecil dari nol itu berarti ga ada limit 
		$criteria->limit=-1; 

			return new CActiveDataProvider($this, array(
					'criteria'=>$criteria,
					'pagination'=>false,
			));
	}
        
	public function getSyaratBayarItems()
	{
		return SyaratbayarM::model()->findAll('syaratbayar_aktif=TRUE ORDER BY syaratbayar_nama');
	}

	 public function getSupplierItems()
	{
		return SupplierM::model()->findAll("supplier_aktif=TRUE AND supplier_jenis='Farmasi' ORDER BY supplier_nama");
	}
		
	public function searchLapFaktur()
	{

		$format = new CustomFormat;
		$criteria=new CDbCriteria;
		$this->tglAwal  = $format->formatDateMediumForDB($this->tglAwal);
		$this->tglAkhir = $format->formatDateMediumForDB($this->tglAkhir);

		$criteria->addBetweenCondition('date(tglfaktur)',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('t.fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('t.penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('t.supplier_id',$this->supplier_id);
		$criteria->compare('t.syaratbayar_id',$this->syaratbayar_id);
		$criteria->compare('t.ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(t.nofaktur)',strtolower($this->nofaktur),true);
		$criteria->compare('LOWER(t.tgljatuhtempo)',strtolower($this->tgljatuhtempo),true);
		$criteria->compare('LOWER(t.keteranganfaktur)',strtolower($this->keteranganfaktur),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
							'pageSize'=>10
						  )
		));
	}
			
	public function searchLapFakturPrint()
	{

		$format = new CustomFormat;
		$criteria=new CDbCriteria;
		$this->tglAwal  = $format->formatDateMediumForDB($this->tglAwal);
		$this->tglAkhir = $format->formatDateMediumForDB($this->tglAkhir);

		$criteria->addBetweenCondition('date(tglfaktur)',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('t.fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('t.penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('t.supplier_id',$this->supplier_id);
		$criteria->compare('t.syaratbayar_id',$this->syaratbayar_id);
		$criteria->compare('t.ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(t.nofaktur)',strtolower($this->nofaktur),true);
		$criteria->compare('LOWER(t.tgljatuhtempo)',strtolower($this->tgljatuhtempo),true);
		$criteria->compare('LOWER(t.keteranganfaktur)',strtolower($this->keteranganfaktur),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->limit=-1;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false
		));
	}
}