<?php

/**
 * This is the model class for table "laporanlabarugi_v".
 *
 * The followings are the available columns in table 'laporanlabarugi_v':
 * @property integer $rekperiod_id
 * @property string $perideawal
 * @property string $sampaidgn
 * @property string $deskripsi
 * @property integer $rekening1_id
 * @property string $kdrekening1
 * @property string $nmrekening1
 * @property integer $rekening2_id
 * @property string $kdrekening2
 * @property string $nmrekening2
 * @property integer $rekening3_id
 * @property string $kdrekening3
 * @property string $nmrekening3
 * @property integer $rekening4_id
 * @property string $kdrekening4
 * @property string $nmrekening4
 * @property double $saldo
 */
class LaporanlabarugiV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanlabarugiV the static model class
	 */
	public $tglAwal, $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanlabarugi_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rekperiod_id, rekening1_id, rekening2_id, rekening3_id, rekening4_id', 'numerical', 'integerOnly'=>true),
			array('saldo', 'numerical'),
			array('deskripsi, nmrekening2', 'length', 'max'=>200),
			array('kdrekening1, kdrekening2, kdrekening3, kdrekening4', 'length', 'max'=>5),
			array('nmrekening1', 'length', 'max'=>100),
			array('nmrekening3', 'length', 'max'=>300),
			array('nmrekening4', 'length', 'max'=>400),
			array('perideawal, sampaidgn', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('rekperiod_id, perideawal, sampaidgn, deskripsi, rekening1_id, kdrekening1, nmrekening1, rekening2_id, kdrekening2, nmrekening2, rekening3_id, kdrekening3, nmrekening3, rekening4_id, kdrekening4, nmrekening4, saldo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rekperiod_id' => 'Rekperiod',
			'perideawal' => 'Perideawal',
			'sampaidgn' => 'Sampaidgn',
			'deskripsi' => 'Deskripsi',
			'rekening1_id' => 'Rekening1',
			'kdrekening1' => 'Kdrekening1',
			'nmrekening1' => 'Nmrekening1',
			'rekening2_id' => 'Rekening2',
			'kdrekening2' => 'Kdrekening2',
			'nmrekening2' => 'Nmrekening2',
			'rekening3_id' => 'Rekening3',
			'kdrekening3' => 'Kdrekening3',
			'nmrekening3' => 'Nmrekening3',
			'rekening4_id' => 'Rekening4',
			'kdrekening4' => 'Kdrekening4',
			'nmrekening4' => 'Nmrekening4',
			'saldo' => 'Saldo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rekperiod_id',$this->rekperiod_id);
		$criteria->compare('LOWER(perideawal)',strtolower($this->perideawal),true);
		$criteria->compare('LOWER(sampaidgn)',strtolower($this->sampaidgn),true);
		$criteria->compare('LOWER(deskripsi)',strtolower($this->deskripsi),true);
		$criteria->compare('rekening1_id',$this->rekening1_id);
		$criteria->compare('LOWER(kdrekening1)',strtolower($this->kdrekening1),true);
		$criteria->compare('LOWER(nmrekening1)',strtolower($this->nmrekening1),true);
		$criteria->compare('rekening2_id',$this->rekening2_id);
		$criteria->compare('LOWER(kdrekening2)',strtolower($this->kdrekening2),true);
		$criteria->compare('LOWER(nmrekening2)',strtolower($this->nmrekening2),true);
		$criteria->compare('rekening3_id',$this->rekening3_id);
		$criteria->compare('LOWER(kdrekening3)',strtolower($this->kdrekening3),true);
		$criteria->compare('LOWER(nmrekening3)',strtolower($this->nmrekening3),true);
		$criteria->compare('rekening4_id',$this->rekening4_id);
		$criteria->compare('LOWER(kdrekening4)',strtolower($this->kdrekening4),true);
		$criteria->compare('LOWER(nmrekening4)',strtolower($this->nmrekening4),true);
		$criteria->compare('saldo',$this->saldo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getRuangan(){
            $format = new CustomFormat();
            $criteria= new CDbCriteria;
            $criteria->group = 'ruangan_id, ruangan_nama';
            $criteria->select = $criteria->group;
            
            $modRuangans = LaporanlabarugiV::model()->findAll($criteria);
            return $modRuangans;
        }
        
        public function getSaldoRuangan($nmrekening, $ruangan_id=null, $tiperekening=null){
            $format = new CustomFormat();
            $criteria= new CDbCriteria;
            $criteria->group = 'ruangan_id, ruangan_nama';
            $criteria->select = $criteria->group.', sum(saldo) as saldo';
            $criteria->compare('ruangan_id',$ruangan_id);
            
            if ($tiperekening == 'rekening1'){
                $criteria->compare('nmrekening1',$nmrekening);
            } else if ($tiperekening == 'rekening3'){
                $criteria->compare('nmrekening3',$nmrekening);
            } else if ($tiperekening == 'rekening4'){
                $criteria->compare('nmrekening4',$nmrekening);
            }
            
            $modRuangans = LaporanlabarugiV::model()->findAll($criteria);
            
            $jml = 0;
            foreach($modRuangans as $ruangan){
                $jml += $ruangan->saldo;
            }
            return $jml;
        }
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('rekperiod_id',$this->rekperiod_id);
		$criteria->compare('LOWER(perideawal)',strtolower($this->perideawal),true);
		$criteria->compare('LOWER(sampaidgn)',strtolower($this->sampaidgn),true);
		$criteria->compare('LOWER(deskripsi)',strtolower($this->deskripsi),true);
		$criteria->compare('rekening1_id',$this->rekening1_id);
		$criteria->compare('LOWER(kdrekening1)',strtolower($this->kdrekening1),true);
		$criteria->compare('LOWER(nmrekening1)',strtolower($this->nmrekening1),true);
		$criteria->compare('rekening2_id',$this->rekening2_id);
		$criteria->compare('LOWER(kdrekening2)',strtolower($this->kdrekening2),true);
		$criteria->compare('LOWER(nmrekening2)',strtolower($this->nmrekening2),true);
		$criteria->compare('rekening3_id',$this->rekening3_id);
		$criteria->compare('LOWER(kdrekening3)',strtolower($this->kdrekening3),true);
		$criteria->compare('LOWER(nmrekening3)',strtolower($this->nmrekening3),true);
		$criteria->compare('rekening4_id',$this->rekening4_id);
		$criteria->compare('LOWER(kdrekening4)',strtolower($this->kdrekening4),true);
		$criteria->compare('LOWER(nmrekening4)',strtolower($this->nmrekening4),true);
		$criteria->compare('saldo',$this->saldo);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}