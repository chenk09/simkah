<?php

/**
 * This is the model class for table "laporanmutasibarang_v".
 *
 * The followings are the available columns in table 'laporanmutasibarang_v':
 * @property integer $mutasibrg_id
 * @property string $nomutasibrg
 * @property string $tglmutasibrg
 * @property integer $karyawanpemesan_id
 * @property string $karyawanpemesan_gelardepan
 * @property string $karyawanpemesan_nama
 * @property string $karyawan_gelarbelakang
 * @property integer $golongan_id
 * @property string $golongan_kode
 * @property string $golongan_nama
 * @property integer $kelompok_id
 * @property string $kelompok_kode
 * @property string $kelompok_nama
 * @property integer $subkelompok_id
 * @property string $subkelompok_kode
 * @property string $subkelompok_nama
 * @property integer $bidang_id
 * @property string $bidang_kode
 * @property string $bidang_nama
 * @property integer $barang_id
 * @property string $barang_kode
 * @property string $barang_nama
 * @property string $barang_type
 * @property string $barang_merk
 * @property string $barang_noseri
 * @property string $barang_ukuran
 * @property string $barang_bahan
 * @property string $barang_thnbeli
 * @property string $barang_warna
 * @property boolean $barang_statusregister
 * @property integer $barang_ekonomis_thn
 * @property double $qty_mutasi
 * @property double $totalhargamutasi
 * @property string $keterangan_mutasi
 * @property integer $pesanbarang_id
 * @property string $nopemesanan
 * @property integer $instalasi_id
 * @property string $instalasi_nama
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 */
class LaporanmutasibarangV extends CActiveRecord
{

	public $tglAwal, $tglAkhir, $peg_penerima_nama, $peg_mengetahui_nama, $instalasi_id;
        public $jumlah;
        public $data;
        public $tick;
        public $pilihanx;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanmutasibarangV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanmutasibarang_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mutasibrg_id, karyawanpemesan_id, golongan_id, kelompok_id, subkelompok_id, bidang_id, barang_id, barang_ekonomis_thn, pesanbarang_id, instalasi_id, ruangan_id', 'numerical', 'integerOnly'=>true),
			array('qty_mutasi, totalhargamutasi', 'numerical'),
			array('nomutasibrg, karyawanpemesan_nama, golongan_kode, kelompok_kode, subkelompok_kode, bidang_kode, barang_kode, barang_type, barang_merk, barang_warna, nopemesanan, instalasi_nama, ruangan_nama', 'length', 'max'=>50),
			array('karyawanpemesan_gelardepan', 'length', 'max'=>10),
			array('karyawan_gelarbelakang', 'length', 'max'=>15),
			array('golongan_nama, kelompok_nama, subkelompok_nama, bidang_nama, barang_nama', 'length', 'max'=>100),
			array('barang_noseri, barang_ukuran, barang_bahan', 'length', 'max'=>20),
			array('barang_thnbeli', 'length', 'max'=>5),
			array('tglmutasibrg, barang_statusregister, keterangan_mutasi', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('mutasibrg_id, nomutasibrg, tglmutasibrg, karyawanpemesan_id, karyawanpemesan_gelardepan, karyawanpemesan_nama, karyawan_gelarbelakang, golongan_id, golongan_kode, golongan_nama, kelompok_id, kelompok_kode, kelompok_nama, subkelompok_id, subkelompok_kode, subkelompok_nama, bidang_id, bidang_kode, bidang_nama, barang_id, barang_kode, barang_nama, barang_type, barang_merk, barang_noseri, barang_ukuran, barang_bahan, barang_thnbeli, barang_warna, barang_statusregister, barang_ekonomis_thn, qty_mutasi, totalhargamutasi, keterangan_mutasi, pesanbarang_id, nopemesanan, instalasi_id, instalasi_nama, ruangan_id, ruangan_nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mutasibrg_id' => 'Mutasibrg',
			'nomutasibrg' => 'Nomutasibrg',
			'tglmutasibrg' => 'Tglmutasibrg',
			'karyawanpemesan_id' => 'Karyawanpemesan',
			'karyawanpemesan_gelardepan' => 'Karyawanpemesan Gelardepan',
			'karyawanpemesan_nama' => 'Karyawanpemesan Nama',
			'karyawan_gelarbelakang' => 'Karyawan Gelarbelakang',
			'golongan_id' => 'Golongan',
			'golongan_kode' => 'Golongan Kode',
			'golongan_nama' => 'Golongan Nama',
			'kelompok_id' => 'Kelompok',
			'kelompok_kode' => 'Kelompok Kode',
			'kelompok_nama' => 'Kelompok Nama',
			'subkelompok_id' => 'Subkelompok',
			'subkelompok_kode' => 'Subkelompok Kode',
			'subkelompok_nama' => 'Subkelompok Nama',
			'bidang_id' => 'Bidang',
			'bidang_kode' => 'Bidang Kode',
			'bidang_nama' => 'Bidang Nama',
			'barang_id' => 'Barang',
			'barang_kode' => 'Barang Kode',
			'barang_nama' => 'Barang Nama',
			'barang_type' => 'Barang Type',
			'barang_merk' => 'Barang Merk',
			'barang_noseri' => 'Barang Noseri',
			'barang_ukuran' => 'Barang Ukuran',
			'barang_bahan' => 'Barang Bahan',
			'barang_thnbeli' => 'Barang Thnbeli',
			'barang_warna' => 'Barang Warna',
			'barang_statusregister' => 'Barang Statusregister',
			'barang_ekonomis_thn' => 'Barang Ekonomis Thn',
			'qty_mutasi' => 'Qty Mutasi',
			'totalhargamutasi' => 'Totalhargamutasi',
			'keterangan_mutasi' => 'Keterangan Mutasi',
			'pesanbarang_id' => 'Pesanbarang',
			'nopemesanan' => 'Nopemesanan',
			'instalasi_id' => 'Instalasi',
			'instalasi_nama' => 'Instalasi Nama',
			'ruangan_id' => 'Ruangan',
			'ruangan_nama' => 'Ruangan Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mutasibrg_id',$this->mutasibrg_id);
		$criteria->compare('nomutasibrg',$this->nomutasibrg,true);
		$criteria->compare('tglmutasibrg',$this->tglmutasibrg,true);
		$criteria->compare('karyawanpemesan_id',$this->karyawanpemesan_id);
		$criteria->compare('karyawanpemesan_gelardepan',$this->karyawanpemesan_gelardepan,true);
		$criteria->compare('karyawanpemesan_nama',$this->karyawanpemesan_nama,true);
		$criteria->compare('karyawan_gelarbelakang',$this->karyawan_gelarbelakang,true);
		$criteria->compare('golongan_id',$this->golongan_id);
		$criteria->compare('golongan_kode',$this->golongan_kode,true);
		$criteria->compare('golongan_nama',$this->golongan_nama,true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('kelompok_kode',$this->kelompok_kode,true);
		$criteria->compare('kelompok_nama',$this->kelompok_nama,true);
		$criteria->compare('subkelompok_id',$this->subkelompok_id);
		$criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
		$criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
		$criteria->compare('bidang_id',$this->bidang_id);
		$criteria->compare('bidang_kode',$this->bidang_kode,true);
		$criteria->compare('bidang_nama',$this->bidang_nama,true);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('barang_kode',$this->barang_kode,true);
		$criteria->compare('barang_nama',$this->barang_nama,true);
		$criteria->compare('barang_type',$this->barang_type,true);
		$criteria->compare('barang_merk',$this->barang_merk,true);
		$criteria->compare('barang_noseri',$this->barang_noseri,true);
		$criteria->compare('barang_ukuran',$this->barang_ukuran,true);
		$criteria->compare('barang_bahan',$this->barang_bahan,true);
		$criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
		$criteria->compare('barang_warna',$this->barang_warna,true);
		$criteria->compare('barang_statusregister',$this->barang_statusregister);
		$criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
		$criteria->compare('qty_mutasi',$this->qty_mutasi);
		$criteria->compare('totalhargamutasi',$this->totalhargamutasi);
		$criteria->compare('keterangan_mutasi',$this->keterangan_mutasi,true);
		$criteria->compare('pesanbarang_id',$this->pesanbarang_id);
		$criteria->compare('nopemesanan',$this->nopemesanan,true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('instalasi_nama',$this->instalasi_nama,true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('ruangan_nama',$this->ruangan_nama,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}