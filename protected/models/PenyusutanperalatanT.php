<?php

/**
 * This is the model class for table "penyusutanperalatan_t".
 *
 * The followings are the available columns in table 'penyusutanperalatan_t':
 * @property integer $penyusutanperalatan_id
 * @property integer $invperalatan_id
 * @property integer $ruangan_id
 * @property integer $pegawai_id
 * @property string $tglpenyusutanperalatan
 * @property string $bulanpenyusutanperalatan
 * @property double $hargaperolehanperalatan
 * @property double $nilairesiduperalatan
 * @property integer $umurekonmisbln
 * @property string $tglterimaperalatan
 * @property double $bbnpenyusutanbrjlnper
 * @property double $akumpenyusutanper
 * @property double $nilaibukuperalatan
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $update_loginpemakai_id
 * @property integer $create_ruangan
 */
class PenyusutanperalatanT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PenyusutanperalatanT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penyusutanperalatan_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invperalatan_id, ruangan_id, pegawai_id, tglpenyusutanperalatan, bulanpenyusutanperalatan, hargaperolehanperalatan, nilairesiduperalatan, umurekonmisbln, tglterimaperalatan, bbnpenyusutanbrjlnper, akumpenyusutanper, nilaibukuperalatan, create_time, create_loginpemakai_id, create_ruangan', 'required'),
			array('penyusutanperalatan_id, invperalatan_id, ruangan_id, pegawai_id, umurekonmisbln, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'numerical', 'integerOnly'=>true),
			array('hargaperolehanperalatan, nilairesiduperalatan, bbnpenyusutanbrjlnper, akumpenyusutanper, nilaibukuperalatan', 'numerical'),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penyusutanperalatan_id, invperalatan_id, ruangan_id, pegawai_id, tglpenyusutanperalatan, bulanpenyusutanperalatan, hargaperolehanperalatan, nilairesiduperalatan, umurekonmisbln, tglterimaperalatan, bbnpenyusutanbrjlnper, akumpenyusutanper, nilaibukuperalatan, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penyusutanperalatan_id' => 'Penyusutanperalatan',
			'invperalatan_id' => 'Invperalatan',
			'ruangan_id' => 'Ruangan',
			'pegawai_id' => 'Pegawai',
			'tglpenyusutanperalatan' => 'Tgl. Penyusutan',
			'bulanpenyusutanperalatan' => 'Bulanpenyusutanperalatan',
			'hargaperolehanperalatan' => 'Hargaperolehanperalatan',
			'nilairesiduperalatan' => 'Nilairesiduperalatan',
			'umurekonmisbln' => 'Umurekonmisbln',
			'tglterimaperalatan' => 'Tglterimaperalatan',
			'bbnpenyusutanbrjlnper' => 'Bbnpenyusutanbrjlnper',
			'akumpenyusutanper' => 'Akumpenyusutanper',
			'nilaibukuperalatan' => 'Nilaibukuperalatan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penyusutanperalatan_id',$this->penyusutanperalatan_id);
		$criteria->compare('invperalatan_id',$this->invperalatan_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(tglpenyusutanperalatan)',strtolower($this->tglpenyusutanperalatan),true);
		$criteria->compare('LOWER(bulanpenyusutanperalatan)',strtolower($this->bulanpenyusutanperalatan),true);
		$criteria->compare('hargaperolehanperalatan',$this->hargaperolehanperalatan);
		$criteria->compare('nilairesiduperalatan',$this->nilairesiduperalatan);
		$criteria->compare('umurekonmisbln',$this->umurekonmisbln);
		$criteria->compare('LOWER(tglterimaperalatan)',strtolower($this->tglterimaperalatan),true);
		$criteria->compare('bbnpenyusutanbrjlnper',$this->bbnpenyusutanbrjlnper);
		$criteria->compare('akumpenyusutanper',$this->akumpenyusutanper);
		$criteria->compare('nilaibukuperalatan',$this->nilaibukuperalatan);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penyusutanperalatan_id',$this->penyusutanperalatan_id);
		$criteria->compare('invperalatan_id',$this->invperalatan_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(tglpenyusutanperalatan)',strtolower($this->tglpenyusutanperalatan),true);
		$criteria->compare('LOWER(bulanpenyusutanperalatan)',strtolower($this->bulanpenyusutanperalatan),true);
		$criteria->compare('hargaperolehanperalatan',$this->hargaperolehanperalatan);
		$criteria->compare('nilairesiduperalatan',$this->nilairesiduperalatan);
		$criteria->compare('umurekonmisbln',$this->umurekonmisbln);
		$criteria->compare('LOWER(tglterimaperalatan)',strtolower($this->tglterimaperalatan),true);
		$criteria->compare('bbnpenyusutanbrjlnper',$this->bbnpenyusutanbrjlnper);
		$criteria->compare('akumpenyusutanper',$this->akumpenyusutanper);
		$criteria->compare('nilaibukuperalatan',$this->nilaibukuperalatan);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}