<?php

class Formularium extends LookupM
{
    private static $_items=array();
    private static $_type = 'formularium';
    /**
     * Returns the items for the specified type.
     * @param string item type (e.g. 'PostStatus').
     * @return array item names indexed by item code. The items are order by their lookup_urutan values.
     * An empty array is returned if the item type does not exist.
     */
    public static function items($type=null)
    {
        $type = self::$_type;
            if(!isset(self::$_items[$type]))
                    self::loadItems($type);
            return self::$_items[$type];
    }

    /**
     * Returns the item name for the specified type and code.
     * @param string the item type (e.g. 'PostStatus').
     * @param integer the item code (corresponding to the 'code' column value)
     * @return string the item name for the specified the code. False is returned if the item type or code does not exist.
     */
    public static function item($type,$code)
    {
            if(!isset(self::$_items[$type]))
                    self::loadItems($type);
            return isset(self::$_items[$type][$code]) ? self::$_items[$type][$code] : false;
    }

    /**
     * Loads the lookup items for the specified type from the database.
     * @param string the item type
     */
    private static function loadItems($type)
    {
            self::$_items[$type]=array();
            $models=self::model()->findAll(array(
                    'condition'=>'lookup_type=:type',
                    'params'=>array(':type'=>$type),
                    'order'=>'lookup_urutan',
            ));
            foreach($models as $model)
                    self::$_items[$type][$model->lookup_value]=$model->lookup_name;
    }
}
?>
