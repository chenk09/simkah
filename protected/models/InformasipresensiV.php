<?php

/**
 * This is the model class for table "informasipresensi_v".
 *
 * The followings are the available columns in table 'informasipresensi_v':
 * @property integer $presensi_id
 * @property integer $pegawai_id
 * @property integer $kelompokpegawai_id
 * @property string $kelompokpegawai_nama
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property string $gelarbelakang_nama
 * @property integer $statuskehadiran_id
 * @property string $statuskehadiran_nama
 * @property string $statuskehadiran_singkatan
 * @property integer $statusscan_id
 * @property string $statusscan_nama
 * @property string $statusscan_singkatan
 * @property string $tglpresensi
 * @property string $no_fingerprint
 * @property string $nofingerprint
 * @property boolean $verifikasi
 * @property string $keterangan
 * @property string $jamkerjamasuk
 * @property string $jamkerjapulang
 * @property integer $terlambat_mnt
 * @property integer $pulangawal_mnt
 */
class InformasipresensiV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InformasipresensiV the static model class
	 */
	public $tglpresensi_akhir, $nomorindukpegawai;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'informasipresensi_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('presensi_id, pegawai_id, kelompokpegawai_id, statuskehadiran_id, statusscan_id, terlambat_mnt, pulangawal_mnt', 'numerical', 'integerOnly'=>true),
			array('kelompokpegawai_nama, no_fingerprint', 'length', 'max'=>30),
			array('gelardepan', 'length', 'max'=>10),
			array('nama_pegawai, statuskehadiran_nama, statusscan_singkatan', 'length', 'max'=>50),
			array('gelarbelakang_nama', 'length', 'max'=>15),
			array('statuskehadiran_singkatan', 'length', 'max'=>1),
			array('statusscan_nama', 'length', 'max'=>100),
			array('nofingerprint', 'length', 'max'=>20),
			array('tglpresensi, verifikasi, keterangan, jamkerjamasuk, jamkerjapulang', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('presensi_id, pegawai_id, kelompokpegawai_id, kelompokpegawai_nama, gelardepan, nama_pegawai, gelarbelakang_nama, statuskehadiran_id, statuskehadiran_nama, statuskehadiran_singkatan, statusscan_id, statusscan_nama, statusscan_singkatan, tglpresensi, no_fingerprint, nofingerprint, verifikasi, keterangan, jamkerjamasuk, jamkerjapulang, terlambat_mnt, pulangawal_mnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pegawai'=>array(self::BELONGS_TO,'PegawaiM','pegawai_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'presensi_id' => 'Presensi',
			'pegawai_id' => 'Pegawai',
			'kelompokpegawai_id' => 'Kelompok Pegawai',
			'kelompokpegawai_nama' => 'Kelompok Pegawai Nama',
			'gelardepan' => 'Gelar Depan',
			'nama_pegawai' => 'Nama Pegawai',
			'gelarbelakang_nama' => 'Gelar Belakang Nama',
			'statuskehadiran_id' => 'Status Kehadiran',
			'statuskehadiran_nama' => 'Status Kehadiran Nama',
			'statuskehadiran_singkatan' => 'Status Kehadiran Singkatan',
			'statusscan_id' => 'Status Scan',
			'statusscan_nama' => 'Status Scan Nama',
			'statusscan_singkatan' => 'Status Scan Singkatan',
			'tglpresensi' => 'Tgl Presensi',
			'no_fingerprint' => 'No Fingerprint',
			'nofingerprint' => 'No Fingerprint',
			'verifikasi' => 'Verifikasi',
			'keterangan' => 'Keterangan',
			'jamkerjamasuk' => 'Jam Kerja Masuk',
			'jamkerjapulang' => 'Jam Kerja Pulang',
			'terlambat_mnt' => 'Terlambat Mnt',
			'pulangawal_mnt' => 'Pulang Awal Mnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('presensi_id',$this->presensi_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('kelompokpegawai_id',$this->kelompokpegawai_id);
		$criteria->compare('LOWER(kelompokpegawai_nama)',strtolower($this->kelompokpegawai_nama),true);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
		$criteria->compare('statuskehadiran_id',$this->statuskehadiran_id);
		$criteria->compare('LOWER(statuskehadiran_nama)',strtolower($this->statuskehadiran_nama),true);
		$criteria->compare('LOWER(statuskehadiran_singkatan)',strtolower($this->statuskehadiran_singkatan),true);
		$criteria->compare('statusscan_id',$this->statusscan_id);
		$criteria->compare('LOWER(statusscan_nama)',strtolower($this->statusscan_nama),true);
		$criteria->compare('LOWER(statusscan_singkatan)',strtolower($this->statusscan_singkatan),true);
		// $criteria->compare('LOWER(tglpresensi)',strtolower($this->tglpresensi),true);
		$criteria->addBetweenCondition('tglpresensi',$this->tglpresensi, $this->tglpresensi_akhir);
		$criteria->compare('LOWER(no_fingerprint)',strtolower($this->no_fingerprint),true);
		$criteria->compare('LOWER(nofingerprint)',strtolower($this->nofingerprint),true);
		$criteria->compare('verifikasi',$this->verifikasi);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('LOWER(jamkerjamasuk)',strtolower($this->jamkerjamasuk),true);
		$criteria->compare('LOWER(jamkerjapulang)',strtolower($this->jamkerjapulang),true);
		$criteria->compare('terlambat_mnt',$this->terlambat_mnt);
		$criteria->compare('pulangawal_mnt',$this->pulangawal_mnt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('presensi_id',$this->presensi_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('kelompokpegawai_id',$this->kelompokpegawai_id);
		$criteria->compare('LOWER(kelompokpegawai_nama)',strtolower($this->kelompokpegawai_nama),true);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
		$criteria->compare('statuskehadiran_id',$this->statuskehadiran_id);
		$criteria->compare('LOWER(statuskehadiran_nama)',strtolower($this->statuskehadiran_nama),true);
		$criteria->compare('LOWER(statuskehadiran_singkatan)',strtolower($this->statuskehadiran_singkatan),true);
		$criteria->compare('statusscan_id',$this->statusscan_id);
		$criteria->compare('LOWER(statusscan_nama)',strtolower($this->statusscan_nama),true);
		$criteria->compare('LOWER(statusscan_singkatan)',strtolower($this->statusscan_singkatan),true);
		$criteria->compare('LOWER(tglpresensi)',strtolower($this->tglpresensi),true);
		$criteria->compare('LOWER(no_fingerprint)',strtolower($this->no_fingerprint),true);
		$criteria->compare('LOWER(nofingerprint)',strtolower($this->nofingerprint),true);
		$criteria->compare('verifikasi',$this->verifikasi);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('LOWER(jamkerjamasuk)',strtolower($this->jamkerjamasuk),true);
		$criteria->compare('LOWER(jamkerjapulang)',strtolower($this->jamkerjapulang),true);
		$criteria->compare('terlambat_mnt',$this->terlambat_mnt);
		$criteria->compare('pulangawal_mnt',$this->pulangawal_mnt);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}