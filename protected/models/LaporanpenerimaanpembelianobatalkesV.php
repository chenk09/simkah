<?php

/**
 * This is the model class for table "laporanpenerimaanpembelianobatalkes_v".
 *
 * The followings are the available columns in table 'laporanpenerimaanpembelianobatalkes_v':
 * @property integer $penerimaanbarang_id
 * @property string $noterima
 * @property string $tglterima
 * @property double $totalharga
 * @property integer $fakturpembelian_id
 * @property string $nofaktur
 * @property string $tglfaktur
 * @property integer $penerimaandetail_id
 * @property double $hargabelibesar
 * @property double $jumlahterima
 * @property double $hargappn
 * @property double $persendiscount
 * @property integer $obatalkes_id
 * @property string $obatalkes_kode
 * @property string $namaobat
 * @property string $kelompokobat
 * @property integer $satuanbesar_id
 * @property string $satuanbesar_nama
 * @property integer $supplier_id
 * @property string $supplier_nama
 */

/**
* modules/gudangFarmasi/views/laporan/penerimaanPerSupplier/index.php
* Updated by    : Jembar
* Date          : 25-04-2014
* Issue         : EHJ-1080
* Deskripsi     : Model digunakan di laporan penerimaan obat alkes
**/

class LaporanpenerimaanpembelianobatalkesV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanpenerimaanpembelianobatalkesV the static model class
	 */
	public $tglAwal, $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanpenerimaanpembelianobatalkes_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('persenppn, supplier_id, produsen_id', 'numerical', 'integerOnly'=>true),
			array('hargabelisatuan, qty, bruto, persendiscount, nominaldiscount, nominalppn, netto', 'numerical'),
			array('noterima, kelompokobat', 'length', 'max'=>20),
			array('nofaktur, satuanbesar_nama', 'length', 'max'=>50),
			array('obatalkes_kode, namaobat', 'length', 'max'=>200),
			array('supplier_nama, produsen_nama', 'length', 'max'=>100),
			array('tglterima, tglfaktur', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('noterima, tglterima, nofaktur, tglfaktur, hargabelisatuan, qty, bruto, persendiscount, nominaldiscount, persenppn, nominalppn, netto, obatalkes_kode, namaobat, kelompokobat, satuanbesar_nama, supplier_id, supplier_nama, produsen_id, produsen_nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'penerimaanbarang'=>array(self::BELONGS_TO,'PenerimaanbarangT','penerimaanbarang_id'),
			'supplier'=>array(self::BELONGS_TO,'SupplierM','supplier_id'),
			'fakturpembelian'=>array(self::BELONGS_TO,'FakturpembelianT','fakturpembelian_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'noterima' => 'No. Terimaan',
			'tglterima' => 'Tglterima',
			'nofaktur' => 'No. Faktur',
			'tglfaktur' => 'Tglfaktur',
			'hargabelisatuan' => 'Hargabelisatuan',
			'qty' => 'Qty',
			'bruto' => 'Bruto',
			'persendiscount' => 'Persendiscount',
			'nominaldiscount' => 'Nominaldiscount',
			'persenppn' => 'Persenppn',
			'nominalppn' => 'Nominalppn',
			'netto' => 'Netto',
			'obatalkes_kode' => 'Obatalkes Kode',
			'namaobat' => 'Namaobat',
			'kelompokobat' => 'Kelompokobat',
			'satuanbesar_nama' => 'Satuanbesar Nama',
			'supplier_id' => 'Supplier',
			'supplier_nama' => 'Supplier Nama',
			'produsen_id' => 'Produsen',
			'produsen_nama' => 'Produsen Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->addBetweenCondition('date(tglterima)',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('noterima',$this->noterima,true);
		$criteria->compare('tglterima',$this->tglterima,true);
		$criteria->compare('nofaktur',$this->nofaktur,true);
		$criteria->compare('tglfaktur',$this->tglfaktur,true);
		$criteria->compare('hargabelisatuan',$this->hargabelisatuan);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('bruto',$this->bruto);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('nominaldiscount',$this->nominaldiscount);
		$criteria->compare('persenppn',$this->persenppn);
		$criteria->compare('nominalppn',$this->nominalppn);
		$criteria->compare('netto',$this->netto);
		$criteria->compare('obatalkes_kode',$this->obatalkes_kode,true);
		$criteria->compare('namaobat',$this->namaobat,true);
		$criteria->compare('kelompokobat',$this->kelompokobat,true);
		$criteria->compare('satuanbesar_nama',$this->satuanbesar_nama,true);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('supplier_nama',$this->supplier_nama,true);
		$criteria->compare('produsen_id',$this->produsen_id);
		$criteria->compare('produsen_nama',$this->produsen_nama,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
    public function searchPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
		
		$criteria->addBetweenCondition('date(tglterima)',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('noterima',$this->noterima,true);
		$criteria->compare('tglterima',$this->tglterima,true);
		$criteria->compare('nofaktur',$this->nofaktur,true);
		$criteria->compare('tglfaktur',$this->tglfaktur,true);
		$criteria->compare('hargabelisatuan',$this->hargabelisatuan);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('bruto',$this->bruto);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('nominaldiscount',$this->nominaldiscount);
		$criteria->compare('persenppn',$this->persenppn);
		$criteria->compare('nominalppn',$this->nominalppn);
		$criteria->compare('netto',$this->netto);
		$criteria->compare('obatalkes_kode',$this->obatalkes_kode,true);
		$criteria->compare('namaobat',$this->namaobat,true);
		$criteria->compare('kelompokobat',$this->kelompokobat,true);
		$criteria->compare('satuanbesar_nama',$this->satuanbesar_nama,true);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('supplier_nama',$this->supplier_nama,true);
		$criteria->compare('produsen_id',$this->produsen_id);
		$criteria->compare('produsen_nama',$this->produsen_nama,true);

        // Klo limit lebih kecil dari nol itu berarti ga ada limit 
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
    }

    public function getSupplierItems()
    {
        return SupplierM::model()->findAll("supplier_aktif=TRUE AND supplier_jenis='Farmasi' ORDER BY supplier_nama");
    }

    public function getProdusenItems()
    {
        return PbfM::model()->findAll("pbf_aktif=TRUE ORDER BY pbf_nama");
    }

    public function getObatAlkesItems()
    {
        return ObatalkesM::model()->findAll("obatalkes_aktif=TRUE ORDER BY obatalkes_nama");
    }

    public function getHargappnToPersen(){
        $persen = 10;
        if(!empty($this->hargappn))
            $persen = 10;
        else
            $persen = 0;
        return $persen;
    }

    public function getTotal($pilih = ""){
        $modDetail = GFPenerimaanDetailT::model()->findAllByAttributes(array('penerimaanbarang_id'=>$this->penerimaanbarang_id));
        foreach($modDetail as $mod){
            $totalBruto += ($mod->hargabelibesar * $mod->jmlterima);
            $hargaDiskon = $mod->hargabelibesar * $mod->persendiscount / 100;
            $hargaPPN = ($mod->hargabelibesar - $hargaDiskon) * $mod->HargappnToPersen / 100;
            $totalDiskon += ($hargaDiskon * $mod->jmlterima);
            $totalPPN += ($hargaPPN * $mod->jmlterima);
            $totalNetto += (($mod->hargabelibesar - $hargaDiskon + $hargaPPN) * $mod->jmlterima);
        }
        if($pilih == 'bruto')
            return $totalBruto;
        else if($pilih == 'diskon')
            return $totalDiskon;
        else if($pilih == 'ppn')
            return $totalPPN;
        else if($pilih == 'netto')
//              HITUNGANNYA ADA SELISIH >>  return $totalNetto + $this->fakturpembelian->biayamaterai;
            return $this->totalharga + $this->fakturpembelian->biayamaterai;
        else
            return 0.00;
    }
}