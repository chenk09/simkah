<?php

/**
 * This is the model class for table "log_kirim_farmasi".
 *
 * The followings are the available columns in table 'log_kirim_farmasi':
 * @property integer $pendaftaran_id
 * @property string $komentar
 * @property string $status_kirim
 * @property string $created_time
 * @property integer $created
 * @property string $updated_time
 * @property integer $updated
 */
class LogKirimFarmasi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LogKirimFarmasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_kirim_farmasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pendaftaran_id, komentar', 'required'),
			array('pendaftaran_id, created, updated', 'numerical', 'integerOnly'=>true),
			array('status_kirim', 'length', 'max'=>1),
			array('created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pendaftaran_id, komentar, status_kirim, created_time, created, updated_time, updated', 'safe', 'on'=>'search'),
			
			array('created_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
			array('updated_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
			array('created','default','value'=>Yii::app()->user->id,'on'=>'insert'),
			array('updated','default','value'=>Yii::app()->user->id,'on'=>'update'),
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pendaftaran_id' => 'Pendaftaran',
			'komentar' => 'Komentar',
			'status_kirim' => 'Status Kirim',
			'created_time' => 'Created Time',
			'created' => 'Created',
			'updated_time' => 'Updated Time',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('komentar',$this->komentar,true);
		$criteria->compare('status_kirim',$this->status_kirim,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('updated',$this->updated);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}