<?php

/**
 * This is the model class for table "laporansensusbedahsentral_v".
 *
 * The followings are the available columns in table 'laporansensusbedahsentral_v':
 * @property integer $pasien_id
 * @property string $jenisidentitas
 * @property string $no_identitas_pasien
 * @property string $namadepan
 * @property string $nama_pasien
 * @property string $nama_bin
 * @property string $jeniskelamin
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $alamat_pasien
 * @property string $no_rekam_medik
 * @property string $tgl_rekam_medik
 * @property integer $pendaftaran_id
 * @property string $no_pendaftaran
 * @property string $umur
 * @property integer $kelaspelayanan_id
 * @property string $kelaspelayanan_nama
 * @property string $no_masukpenunjang
 * @property string $tglmasukpenunjang
 * @property integer $pasienadmisi_id
 * @property string $kunjungan
 * @property integer $pasienkirimkeunitlain_id
 * @property integer $carabayar_id
 * @property string $carabayar_nama
 * @property integer $penjamin_id
 * @property string $penjamin_nama
 * @property integer $daftartindakan_id
 * @property string $daftartindakan_nama
 * @property integer $operasi_id
 * @property string $operasi_kode
 * @property string $operasi_nama
 * @property integer $kegiatanoperasi_id
 * @property string $kegiatanoperasi_kode
 * @property string $kegiatanoperasi_nama
 * @property integer $golonganoperasi_id
 * @property string $golonganoperasi_nama
 * @property integer $anastesi_id
 * @property string $anastesi_nama
 * @property integer $jenisanastesi_id
 * @property string $jenisanastesi_nama
 * @property integer $pegawai_id
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property string $gelarbelakang_nama
 */
class LaporansensusbedahsentralV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporansensusbedahsentralV the static model class
	 */
	public $tglAwal, $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporansensusbedahsentral_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pasien_id, pendaftaran_id, kelaspelayanan_id, pasienadmisi_id, pasienkirimkeunitlain_id, carabayar_id, penjamin_id, daftartindakan_id, operasi_id, kegiatanoperasi_id, golonganoperasi_id, anastesi_id, jenisanastesi_id, pegawai_id', 'numerical', 'integerOnly'=>true),
			array('jenisidentitas, namadepan, jeniskelamin, no_pendaftaran, no_masukpenunjang, operasi_kode, kegiatanoperasi_kode', 'length', 'max'=>20),
			array('no_identitas_pasien, nama_bin, umur', 'length', 'max'=>30),
			array('nama_pasien, kelaspelayanan_nama, kunjungan, carabayar_nama, penjamin_nama, golonganoperasi_nama, anastesi_nama, jenisanastesi_nama, nama_pegawai', 'length', 'max'=>50),
			array('tempat_lahir', 'length', 'max'=>25),
			array('no_rekam_medik, gelardepan', 'length', 'max'=>10),
			array('daftartindakan_nama, operasi_nama', 'length', 'max'=>200),
			array('kegiatanoperasi_nama', 'length', 'max'=>100),
			array('gelarbelakang_nama', 'length', 'max'=>15),
			array('tanggal_lahir, alamat_pasien, tgl_rekam_medik, tglmasukpenunjang', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pasien_id, jenisidentitas, no_identitas_pasien, namadepan, nama_pasien, nama_bin, jeniskelamin, tempat_lahir, tanggal_lahir, alamat_pasien, no_rekam_medik, tgl_rekam_medik, pendaftaran_id, no_pendaftaran, umur, kelaspelayanan_id, kelaspelayanan_nama, no_masukpenunjang, tglmasukpenunjang, pasienadmisi_id, kunjungan, pasienkirimkeunitlain_id, carabayar_id, carabayar_nama, penjamin_id, penjamin_nama, daftartindakan_id, daftartindakan_nama, operasi_id, operasi_kode, operasi_nama, kegiatanoperasi_id, kegiatanoperasi_kode, kegiatanoperasi_nama, golonganoperasi_id, golonganoperasi_nama, anastesi_id, anastesi_nama, jenisanastesi_id, jenisanastesi_nama, pegawai_id, gelardepan, nama_pegawai, gelarbelakang_nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pasien_id' => 'Pasien',
			'jenisidentitas' => 'Jenis Identitas',
			'no_identitas_pasien' => 'No. Identitas Pasien',
			'namadepan' => 'Namadepan',
			'nama_pasien' => 'Nama Pasien',
			'nama_bin' => 'Nama Bin',
			'jeniskelamin' => 'Jenis Kelamin',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'alamat_pasien' => 'Alamat Pasien',
			'no_rekam_medik' => 'No. Rekam Medik',
			'tgl_rekam_medik' => 'Tanggal Rekam Medik',
			'pendaftaran_id' => 'Pendaftaran',
			'no_pendaftaran' => 'No. Pendaftaran',
			'umur' => 'Umur',
			'kelaspelayanan_id' => 'Kelaspelayanan',
			'kelaspelayanan_nama' => 'Kelas Pelayanan',
			'no_masukpenunjang' => 'No. Masuk Penunjang',
			'tglmasukpenunjang' => 'Tanggal Masuk Penunjang',
			'pasienadmisi_id' => 'Pasien Admisi',
			'kunjungan' => 'Kunjungan',
			'pasienkirimkeunitlain_id' => 'Pasien Kirim Ke Unit Lain',
			'carabayar_id' => 'Cara Bayar',
			'carabayar_nama' => 'Cara Bayar',
			'penjamin_id' => 'Penjamin',
			'penjamin_nama' => 'Penjamin',
			'daftartindakan_id' => 'Daftartindakan',
			'daftartindakan_nama' => 'Tindakan Nama',
			'operasi_id' => 'Operasi',
			'operasi_kode' => 'Kode Operasi',
			'operasi_nama' => 'Operasi',
			'kegiatanoperasi_id' => 'Kegiatan Operasi',
			'kegiatanoperasi_kode' => 'Kode Kegiatan Operasi',
			'kegiatanoperasi_nama' => 'Kegiatan Operasi',
			'golonganoperasi_id' => 'Golongan Operasi',
			'golonganoperasi_nama' => 'Golongan Operasi',
			'anastesi_id' => 'Anastesi',
			'anastesi_nama' => 'Anastesi',
			'jenisanastesi_id' => 'Jenis Anastesi',
			'jenisanastesi_nama' => 'Jenis Anastesi',
			'pegawai_id' => 'Pegawai',
			'gelardepan' => 'Gelar Depan',
			'nama_pegawai' => 'Dokter',
			'gelarbelakang_nama' => 'Gelar Belakang',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(umur)',strtolower($this->umur),true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
		$criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
		$criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
		$criteria->compare('pasienkirimkeunitlain_id',$this->pasienkirimkeunitlain_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
		$criteria->compare('operasi_id',$this->operasi_id);
		$criteria->compare('LOWER(operasi_kode)',strtolower($this->operasi_kode),true);
		$criteria->compare('LOWER(operasi_nama)',strtolower($this->operasi_nama),true);
		$criteria->compare('kegiatanoperasi_id',$this->kegiatanoperasi_id);
		$criteria->compare('LOWER(kegiatanoperasi_kode)',strtolower($this->kegiatanoperasi_kode),true);
		$criteria->compare('LOWER(kegiatanoperasi_nama)',strtolower($this->kegiatanoperasi_nama),true);
		$criteria->compare('golonganoperasi_id',$this->golonganoperasi_id);
		$criteria->compare('LOWER(golonganoperasi_nama)',strtolower($this->golonganoperasi_nama),true);
		$criteria->compare('anastesi_id',$this->anastesi_id);
		$criteria->compare('LOWER(anastesi_nama)',strtolower($this->anastesi_nama),true);
		$criteria->compare('jenisanastesi_id',$this->jenisanastesi_id);
		$criteria->compare('LOWER(jenisanastesi_nama)',strtolower($this->jenisanastesi_nama),true);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
		$criteria->addBetweenCondition('tgl_rekam_medik',$this->tglAwal,$this->tglAkhir);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(umur)',strtolower($this->umur),true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
		$criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
		$criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
		$criteria->compare('pasienkirimkeunitlain_id',$this->pasienkirimkeunitlain_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
		$criteria->compare('operasi_id',$this->operasi_id);
		$criteria->compare('LOWER(operasi_kode)',strtolower($this->operasi_kode),true);
		$criteria->compare('LOWER(operasi_nama)',strtolower($this->operasi_nama),true);
		$criteria->compare('kegiatanoperasi_id',$this->kegiatanoperasi_id);
		$criteria->compare('LOWER(kegiatanoperasi_kode)',strtolower($this->kegiatanoperasi_kode),true);
		$criteria->compare('LOWER(kegiatanoperasi_nama)',strtolower($this->kegiatanoperasi_nama),true);
		$criteria->compare('golonganoperasi_id',$this->golonganoperasi_id);
		$criteria->compare('LOWER(golonganoperasi_nama)',strtolower($this->golonganoperasi_nama),true);
		$criteria->compare('anastesi_id',$this->anastesi_id);
		$criteria->compare('LOWER(anastesi_nama)',strtolower($this->anastesi_nama),true);
		$criteria->compare('jenisanastesi_id',$this->jenisanastesi_id);
		$criteria->compare('LOWER(jenisanastesi_nama)',strtolower($this->jenisanastesi_nama),true);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
		$criteria->addBetweenCondition('tgl_rekam_medik',$this->tglAwal,$this->tglAkhir);
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }

    public function getNamaPegawai()
    {
    	$nama_pegawai = $this->nama_pegawai.' '.$this->gelarbelakang_nama;

    	return $nama_pegawai;
    }
}