<?php

/**
 * This is the model class for table "rincianfakturgudangfarmasi_v".
 *
 * The followings are the available columns in table 'rincianfakturgudangfarmasi_v':
 * @property integer $fakturpembelian_id
 * @property string $nofaktur
 * @property string $tglfaktur
 * @property string $tgljatuhtempo
 * @property integer $penerimaanbarang_id
 * @property string $noterima
 * @property string $tglterima
 * @property integer $permintaanpembelian_id
 * @property string $nopermintaan
 * @property integer $supplier_id
 * @property string $supplier_nama
 * @property integer $obatalkes_id
 * @property string $obatalkes_kode
 * @property string $obatalkes_nama
 * @property integer $satuanbesar_id
 * @property string $satuanbesar_nama
 * @property integer $fakturdetail_id
 * @property double $hargabelisatuan
 * @property double $qty
 * @property double $bruto
 * @property double $persendiscount
 * @property double $nominaldiscount
 * @property double $brutomindiskon
 * @property double $persenppn
 * @property double $hargappnfaktur
 * @property double $persenpph
 * @property double $hargapphfaktur
 * @property double $netto
 * @property string $tglkadaluarsa
 * @property double $jmlkemasan
 * @property integer $bayarkesupplier_id
 * @property string $tglbayarkesupplier
 * @property double $jmldibayarkan
 * @property integer $uangmukabeli_id
 * @property string $namabank
 * @property string $norekening
 * @property string $atasnama
 * @property double $jumlahuang
 * @property double $persendiscountfaktur
 * @property double $biayamaterai
 * @property integer $jurnalrekening_id
 */

/**
* modules/gudangFarmasi/views/laporan/penerimaanObatAlkes/index.php
* Updated by    : Jembar
* Date          : 28-04-2014
* Issue         : EHJ-1708
* Deskripsi     : Model digunakan di laporan detail faktur penerimaan obat alkes
**/

class RincianfakturgudangfarmasiV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RincianfakturgudangfarmasiV the static model class
	 */

	public $tglAwal, $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rincianfakturgudangfarmasi_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fakturpembelian_id, penerimaanbarang_id, permintaanpembelian_id, supplier_id, obatalkes_id, satuanbesar_id, fakturdetail_id, bayarkesupplier_id, uangmukabeli_id, jurnalrekening_id', 'numerical', 'integerOnly'=>true),
			array('hargabelisatuan, qty, bruto, persendiscount, nominaldiscount, brutomindiskon, persenppn, hargappnfaktur, persenpph, hargapphfaktur, netto, jmlkemasan, jmldibayarkan, jumlahuang, persendiscountfaktur, biayamaterai', 'numerical'),
			array('nofaktur, nopermintaan, satuanbesar_nama', 'length', 'max'=>50),
			array('noterima', 'length', 'max'=>20),
			array('supplier_nama, namabank, norekening, atasnama', 'length', 'max'=>100),
			array('obatalkes_kode, obatalkes_nama', 'length', 'max'=>200),
			array('tglfaktur, tgljatuhtempo, tglterima, tglkadaluarsa, tglbayarkesupplier', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('fakturpembelian_id, nofaktur, tglfaktur, tgljatuhtempo, penerimaanbarang_id, noterima, tglterima, permintaanpembelian_id, nopermintaan, supplier_id, supplier_nama, obatalkes_id, obatalkes_kode, obatalkes_nama, satuanbesar_id, satuanbesar_nama, fakturdetail_id, hargabelisatuan, qty, bruto, persendiscount, nominaldiscount, brutomindiskon, persenppn, hargappnfaktur, persenpph, hargapphfaktur, netto, tglkadaluarsa, jmlkemasan, bayarkesupplier_id, tglbayarkesupplier, jmldibayarkan, uangmukabeli_id, namabank, norekening, atasnama, jumlahuang, persendiscountfaktur, biayamaterai, jurnalrekening_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fakturpembelian_id' => 'Fakturpembelian',
			'nofaktur' => 'Nofaktur',
			'tglfaktur' => 'Tglfaktur',
			'tgljatuhtempo' => 'Tgljatuhtempo',
			'penerimaanbarang_id' => 'Penerimaanbarang',
			'noterima' => 'Noterima',
			'tglterima' => 'Tglterima',
			'permintaanpembelian_id' => 'Permintaanpembelian',
			'nopermintaan' => 'Nopermintaan',
			'supplier_id' => 'Supplier',
			'supplier_nama' => 'Supplier Nama',
			'obatalkes_id' => 'Obatalkes',
			'obatalkes_kode' => 'Obatalkes Kode',
			'obatalkes_nama' => 'Obatalkes Nama',
			'satuanbesar_id' => 'Satuanbesar',
			'satuanbesar_nama' => 'Satuanbesar Nama',
			'fakturdetail_id' => 'Fakturdetail',
			'hargabelisatuan' => 'Hargabelisatuan',
			'qty' => 'Qty',
			'bruto' => 'Bruto',
			'persendiscount' => 'Persendiscount',
			'nominaldiscount' => 'Nominaldiscount',
			'brutomindiskon' => 'Brutomindiskon',
			'persenppn' => 'Persenppn',
			'hargappnfaktur' => 'Hargappnfaktur',
			'persenpph' => 'Persenpph',
			'hargapphfaktur' => 'Hargapphfaktur',
			'netto' => 'Netto',
			'tglkadaluarsa' => 'Tglkadaluarsa',
			'jmlkemasan' => 'Jmlkemasan',
			'bayarkesupplier_id' => 'Bayarkesupplier',
			'tglbayarkesupplier' => 'Tglbayarkesupplier',
			'jmldibayarkan' => 'Jmldibayarkan',
			'uangmukabeli_id' => 'Uangmukabeli',
			'namabank' => 'Namabank',
			'norekening' => 'Norekening',
			'atasnama' => 'Atasnama',
			'jumlahuang' => 'Jumlahuang',
			'persendiscountfaktur' => 'Persendiscountfaktur',
			'biayamaterai' => 'Biayamaterai',
			'jurnalrekening_id' => 'Jurnalrekening',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('nofaktur',$this->nofaktur,true);
		$criteria->compare('tglfaktur',$this->tglfaktur,true);
		$criteria->compare('tgljatuhtempo',$this->tgljatuhtempo,true);
		$criteria->compare('penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('noterima',$this->noterima,true);
		$criteria->compare('tglterima',$this->tglterima,true);
		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('nopermintaan',$this->nopermintaan,true);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('supplier_nama',$this->supplier_nama,true);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('obatalkes_kode',$this->obatalkes_kode,true);
		$criteria->compare('obatalkes_nama',$this->obatalkes_nama,true);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('satuanbesar_nama',$this->satuanbesar_nama,true);
		$criteria->compare('fakturdetail_id',$this->fakturdetail_id);
		$criteria->compare('hargabelisatuan',$this->hargabelisatuan);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('bruto',$this->bruto);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('nominaldiscount',$this->nominaldiscount);
		$criteria->compare('brutomindiskon',$this->brutomindiskon);
		$criteria->compare('persenppn',$this->persenppn);
		$criteria->compare('hargappnfaktur',$this->hargappnfaktur);
		$criteria->compare('persenpph',$this->persenpph);
		$criteria->compare('hargapphfaktur',$this->hargapphfaktur);
		$criteria->compare('netto',$this->netto);
		$criteria->compare('tglkadaluarsa',$this->tglkadaluarsa,true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
		$criteria->compare('bayarkesupplier_id',$this->bayarkesupplier_id);
		$criteria->compare('tglbayarkesupplier',$this->tglbayarkesupplier,true);
		$criteria->compare('jmldibayarkan',$this->jmldibayarkan);
		$criteria->compare('uangmukabeli_id',$this->uangmukabeli_id);
		$criteria->compare('namabank',$this->namabank,true);
		$criteria->compare('norekening',$this->norekening,true);
		$criteria->compare('atasnama',$this->atasnama,true);
		$criteria->compare('jumlahuang',$this->jumlahuang);
		$criteria->compare('persendiscountfaktur',$this->persendiscountfaktur);
		$criteria->compare('biayamaterai',$this->biayamaterai);
		$criteria->compare('jurnalrekening_id',$this->jurnalrekening_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function searchPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

		$criteria=new CDbCriteria;

		$criteria->addBetweenCondition('date(tglterima)',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('nofaktur',$this->nofaktur,true);
		$criteria->compare('tglfaktur',$this->tglfaktur,true);
		$criteria->compare('tgljatuhtempo',$this->tgljatuhtempo,true);
		$criteria->compare('penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('noterima',$this->noterima,true);
		$criteria->compare('tglterima',$this->tglterima,true);
		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('nopermintaan',$this->nopermintaan,true);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('supplier_nama',$this->supplier_nama,true);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('obatalkes_kode',$this->obatalkes_kode,true);
		$criteria->compare('obatalkes_nama',$this->obatalkes_nama,true);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('satuanbesar_nama',$this->satuanbesar_nama,true);
		$criteria->compare('fakturdetail_id',$this->fakturdetail_id);
		$criteria->compare('hargabelisatuan',$this->hargabelisatuan);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('bruto',$this->bruto);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('nominaldiscount',$this->nominaldiscount);
		$criteria->compare('brutomindiskon',$this->brutomindiskon);
		$criteria->compare('persenppn',$this->persenppn);
		$criteria->compare('hargappnfaktur',$this->hargappnfaktur);
		$criteria->compare('persenpph',$this->persenpph);
		$criteria->compare('hargapphfaktur',$this->hargapphfaktur);
		$criteria->compare('netto',$this->netto);
		$criteria->compare('tglkadaluarsa',$this->tglkadaluarsa,true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
		$criteria->compare('bayarkesupplier_id',$this->bayarkesupplier_id);
		$criteria->compare('tglbayarkesupplier',$this->tglbayarkesupplier,true);
		$criteria->compare('jmldibayarkan',$this->jmldibayarkan);
		$criteria->compare('uangmukabeli_id',$this->uangmukabeli_id);
		$criteria->compare('namabank',$this->namabank,true);
		$criteria->compare('norekening',$this->norekening,true);
		$criteria->compare('atasnama',$this->atasnama,true);
		$criteria->compare('jumlahuang',$this->jumlahuang);
		$criteria->compare('persendiscountfaktur',$this->persendiscountfaktur);
		$criteria->compare('biayamaterai',$this->biayamaterai);
		$criteria->compare('jurnalrekening_id',$this->jurnalrekening_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
    }

    public function searchNoTerima()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('nofaktur',$this->nofaktur,true);
		$criteria->compare('tglfaktur',$this->tglfaktur,true);
		$criteria->compare('tgljatuhtempo',$this->tgljatuhtempo,true);
		$criteria->compare('penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('noterima',$this->noterima,true);
		$criteria->compare('tglterima',$this->tglterima,true);
		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('nopermintaan',$this->nopermintaan,true);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('supplier_nama',$this->supplier_nama,true);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('obatalkes_kode',$this->obatalkes_kode,true);
		$criteria->compare('obatalkes_nama',$this->obatalkes_nama,true);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('satuanbesar_nama',$this->satuanbesar_nama,true);
		$criteria->compare('fakturdetail_id',$this->fakturdetail_id);
		$criteria->compare('hargabelisatuan',$this->hargabelisatuan);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('bruto',$this->bruto);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('nominaldiscount',$this->nominaldiscount);
		$criteria->compare('brutomindiskon',$this->brutomindiskon);
		$criteria->compare('persenppn',$this->persenppn);
		$criteria->compare('hargappnfaktur',$this->hargappnfaktur);
		$criteria->compare('persenpph',$this->persenpph);
		$criteria->compare('hargapphfaktur',$this->hargapphfaktur);
		$criteria->compare('netto',$this->netto);
		$criteria->compare('tglkadaluarsa',$this->tglkadaluarsa,true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
		$criteria->compare('bayarkesupplier_id',$this->bayarkesupplier_id);
		$criteria->compare('tglbayarkesupplier',$this->tglbayarkesupplier,true);
		$criteria->compare('jmldibayarkan',$this->jmldibayarkan);
		$criteria->compare('uangmukabeli_id',$this->uangmukabeli_id);
		$criteria->compare('namabank',$this->namabank,true);
		$criteria->compare('norekening',$this->norekening,true);
		$criteria->compare('atasnama',$this->atasnama,true);
		$criteria->compare('jumlahuang',$this->jumlahuang);
		$criteria->compare('persendiscountfaktur',$this->persendiscountfaktur);
		$criteria->compare('biayamaterai',$this->biayamaterai);
		$criteria->compare('jurnalrekening_id',$this->jurnalrekening_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}