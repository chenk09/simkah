<?php

/**
 * This is the model class for table "jenislayananlokasiantrian_v".
 *
 * The followings are the available columns in table 'jenislayananlokasiantrian_v':
 * @property integer $lokasiantrian_id
 * @property string $lokasiantrian_nama
 * @property integer $jenisantrian_id
 * @property string $jenisantrian_nama
 * @property string $layananantrian_nama
 */
class JenislayananlokasiantrianV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return JenislayananlokasiantrianV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jenislayananlokasiantrian_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lokasiantrian_id, jenisantrian_id', 'numerical', 'integerOnly'=>true),
			array('lokasiantrian_nama', 'length', 'max'=>100),
			array('jenisantrian_nama', 'length', 'max'=>250),
			array('layananantrian_nama', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('lokasiantrian_id, lokasiantrian_nama, jenisantrian_id, jenisantrian_nama, layananantrian_nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lokasiantrian_id' => 'Lokasiantrian',
			'lokasiantrian_nama' => 'Lokasiantrian Nama',
			'jenisantrian_id' => 'Jenisantrian',
			'jenisantrian_nama' => 'Jenisantrian Nama',
			'layananantrian_nama' => 'Layananantrian Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('LOWER(lokasiantrian_nama)',strtolower($this->lokasiantrian_nama),true);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('LOWER(jenisantrian_nama)',strtolower($this->jenisantrian_nama),true);
		$criteria->compare('LOWER(layananantrian_nama)',strtolower($this->layananantrian_nama),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('LOWER(lokasiantrian_nama)',strtolower($this->lokasiantrian_nama),true);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('LOWER(jenisantrian_nama)',strtolower($this->jenisantrian_nama),true);
		$criteria->compare('LOWER(layananantrian_nama)',strtolower($this->layananantrian_nama),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}