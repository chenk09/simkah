<?php

/**
 * This is the model class for table "jurnalrekeningblmposting_v".
 *
 * The followings are the available columns in table 'jurnalrekeningblmposting_v':
 * @property integer $jurnalrekening_id
 * @property string $tglbuktijurnal
 * @property string $nobuktijurnal
 * @property integer $jenisjurnal_id
 * @property string $jenisjurnal_nama
 * @property integer $rekperiod_id
 * @property string $kodejurnal
 * @property string $noreferensi
 * @property string $tglreferensi
 * @property integer $nobku
 * @property string $urianjurnal
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 * @property integer $ruangan_id
 * @property integer $jurnaldetail_id
 * @property integer $jurnalposting_id
 * @property string $nourut
 * @property string $uraiantransaksi
 * @property string $kdstruktur
 * @property string $nmstruktur
 * @property integer $kelompok_id
 * @property string $nmkelompok
 * @property string $kdjenis
 * @property string $nmjenis
 * @property string $kdobyek
 * @property string $nmobyek
 * @property integer $rincianobyek_id
 * @property string $kdrincianobyek
 * @property string $nmrincianobyek
 * @property double $saldodebit
 * @property double $saldokredit
 * @property boolean $koreksi
 * @property string $catatan
 */
class JurnalrekeningblmpostingV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return JurnalrekeningblmpostingV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jurnalrekeningblmposting_new_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jurnalrekening_id, jenisjurnal_id, rekperiod_id, nobku, ruangan_id, jurnaldetail_id, jurnalposting_id, kelompok_id, rincianobyek_id, struktur_id', 'numerical', 'integerOnly'=>true),
			array('saldodebit, saldokredit', 'numerical'),
			array('nobuktijurnal', 'length', 'max'=>50),
			array('jenisjurnal_nama, uraiantransaksi, nmstruktur', 'length', 'max'=>100),
			array('kodejurnal', 'length', 'max'=>20),
			array('nourut', 'length', 'max'=>3),
			array('kdstruktur, kdjenis, kdobyek, kdrincianobyek', 'length', 'max'=>5),
			array('nmkelompok', 'length', 'max'=>200),
			array('nmjenis', 'length', 'max'=>300),
			array('nmobyek', 'length', 'max'=>400),
			array('nmrincianobyek', 'length', 'max'=>500),
			array('tglbuktijurnal, noreferensi, tglreferensi, urianjurnal, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan, koreksi, catatan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('jurnalrekening_id,kdkelompok, tglbuktijurnal, nobuktijurnal, jenisjurnal_id, jenisjurnal_nama, rekperiod_id, kodejurnal, noreferensi, tglreferensi, nobku, urianjurnal, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan, ruangan_id, jurnaldetail_id, jurnalposting_id, nourut, uraiantransaksi, kdstruktur, nmstruktur, kelompok_id, nmkelompok, kdjenis, nmjenis, kdobyek, nmobyek, rincianobyek_id, kdrincianobyek, nmrincianobyek, saldodebit, saldokredit, koreksi, catatan, struktur_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'jurnalrekening_id' => 'Jurnalrekening',
			'tglbuktijurnal' => 'Tglbuktijurnal',
			'nobuktijurnal' => 'No Bukti Jurnal',
			'jenisjurnal_id' => 'Jenis Jurnal',
			'jenisjurnal_nama' => 'Jenis Jurnal',
			'rekperiod_id' => 'Rekperiod',
			'kodejurnal' => 'Kode Jurnal',
			'noreferensi' => 'Noreferensi',
			'tglreferensi' => 'Tglreferensi',
			'nobku' => 'Nobku',
			'urianjurnal' => 'Urianjurnal',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
			'ruangan_id' => 'Ruangan',
			'jurnaldetail_id' => 'Jurnaldetail',
			'jurnalposting_id' => 'Jurnalposting',
			'nourut' => 'Nourut',
			'uraiantransaksi' => 'Uraiantransaksi',
			'kdstruktur' => 'Kdstruktur',
			'nmstruktur' => 'Nmstruktur',
			'kelompok_id' => 'Kelompok',
			'nmkelompok' => 'Nmkelompok',
			'kdjenis' => 'Kdjenis',
			'nmjenis' => 'Nmjenis',
			'kdobyek' => 'Kdobyek',
			'nmobyek' => 'Nmobyek',
			'rincianobyek_id' => 'Rincianobyek',
			'kdrincianobyek' => 'Kdrincianobyek',
			'nmrincianobyek' => 'Nmrincianobyek',
			'saldodebit' => 'Saldodebit',
			'saldokredit' => 'Saldokredit',
			'koreksi' => 'Koreksi',
			'catatan' => 'Catatan',
			'struktur_id' => 'Struktur',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('jurnalrekening_id',$this->jurnalrekening_id);
		$criteria->compare('tglbuktijurnal',$this->tglbuktijurnal);
		$criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
		$criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
		$criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
		$criteria->compare('rekperiod_id',$this->rekperiod_id);
		$criteria->compare('LOWER(kodejurnal)',strtolower($this->kodejurnal),true);
		$criteria->compare('LOWER(noreferensi)',strtolower($this->noreferensi),true);
		$criteria->compare('LOWER(tglreferensi)',strtolower($this->tglreferensi),true);
		$criteria->compare('nobku',$this->nobku);
		$criteria->compare('LOWER(urianjurnal)',strtolower($this->urianjurnal),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('jurnaldetail_id',$this->jurnaldetail_id);
		$criteria->compare('jurnalposting_id',$this->jurnalposting_id);
		$criteria->compare('LOWER(nourut)',strtolower($this->nourut),true);
		$criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
		$criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur),true);
		$criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
		$criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
		$criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
		$criteria->compare('LOWER(kdobyek)',strtolower($this->kdobyek),true);
		$criteria->compare('LOWER(nmobyek)',strtolower($this->nmobyek),true);
		$criteria->compare('rincianobyek_id',$this->rincianobyek_id);
		$criteria->compare('LOWER(kdrincianobyek)',strtolower($this->kdrincianobyek),true);
		$criteria->compare('LOWER(nmrincianobyek)',strtolower($this->nmrincianobyek),true);
		$criteria->compare('saldodebit',$this->saldodebit);
		$criteria->compare('saldokredit',$this->saldokredit);
		$criteria->compare('koreksi',$this->koreksi);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
		$criteria->compare('struktur_id',$this->struktur_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('jurnalrekening_id',$this->jurnalrekening_id);
		$criteria->compare('LOWER(tglbuktijurnal)',strtolower($this->tglbuktijurnal),true);
		$criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
		$criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
		$criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
		$criteria->compare('rekperiod_id',$this->rekperiod_id);
		$criteria->compare('LOWER(kodejurnal)',strtolower($this->kodejurnal),true);
		$criteria->compare('LOWER(noreferensi)',strtolower($this->noreferensi),true);
		$criteria->compare('LOWER(tglreferensi)',strtolower($this->tglreferensi),true);
		$criteria->compare('nobku',$this->nobku);
		$criteria->compare('LOWER(urianjurnal)',strtolower($this->urianjurnal),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('jurnaldetail_id',$this->jurnaldetail_id);
		$criteria->compare('jurnalposting_id',$this->jurnalposting_id);
		$criteria->compare('LOWER(nourut)',strtolower($this->nourut),true);
		$criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
		$criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur),true);
		$criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
		$criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
		$criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
		$criteria->compare('LOWER(kdobyek)',strtolower($this->kdobyek),true);
		$criteria->compare('LOWER(nmobyek)',strtolower($this->nmobyek),true);
		$criteria->compare('rincianobyek_id',$this->rincianobyek_id);
		$criteria->compare('LOWER(kdrincianobyek)',strtolower($this->kdrincianobyek),true);
		$criteria->compare('LOWER(nmrincianobyek)',strtolower($this->nmrincianobyek),true);
		$criteria->compare('saldodebit',$this->saldodebit);
		$criteria->compare('saldokredit',$this->saldokredit);
		$criteria->compare('koreksi',$this->koreksi);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
		$criteria->compare('struktur_id',$this->struktur_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}