<?php

/**
 * This is the model class for table "pemeriksaanlabdet_m".
 *
 * The followings are the available columns in table 'pemeriksaanlabdet_m':
 * @property integer $pemeriksaanlabdet_id
 * @property integer $nilairujukan_id
 * @property integer $pemeriksaanlab_id
 * @property integer $pemeriksaanlabdet_nourut
 */
class PemeriksaanlabdetM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PemeriksaanlabdetM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pemeriksaanlabdet_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nilairujukan_id, pemeriksaanlab_id, pemeriksaanlabdet_nourut', 'required'),
			array('nilairujukan_id, pemeriksaanlab_id, pemeriksaanlabdet_nourut', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pemeriksaanlabdet_id, nilairujukan_id, pemeriksaanlab_id, pemeriksaanlabdet_nourut', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                                    'nilairujukan'=>array(self::BELONGS_TO, 'NilairujukanM', 'nilairujukan_id'),
                                    'pemeriksaanlab'=>array(self::BELONGS_TO, 'PemeriksaanlabM', 'pemeriksaanlab_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pemeriksaanlabdet_id' => 'ID',
			'nilairujukan_id' => 'Nilai rujukan',
			'pemeriksaanlab_id' => 'Pemeriksaan lab',
			'pemeriksaanlabdet_nourut' => 'No urut pemeriksaan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

        $criteria->with = array('nilairujukan','pemeriksaanlab');
        $criteria->order = 't.pemeriksaanlab_id, nilairujukan.kelompokdet, nilairujukan.kelompokumur, nilairujukan.nilairujukan_jeniskelamin';
		$criteria->compare('t.pemeriksaanlabdet_nama',$this->pemeriksaanlabdet_nama);
		$criteria->compare('t.nilairujukan_id',$this->nilairujukan_id);
		$criteria->compare('t.pemeriksaanlab_nama',$this->pemeriksaanlab_nama);
		$criteria->compare('pemeriksaanlabdet_nourut',$this->pemeriksaanlabdet_nourut);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->with = array('nilairujukan','pemeriksaanlab');
        $criteria->order = 't.pemeriksaanlab_id, nilairujukan.kelompokdet, nilairujukan.kelompokumur, nilairujukan.nilairujukan_jeniskelamin';
		$criteria->compare('t.pemeriksaanlabdet_nama',$this->pemeriksaanlabdet_nama);
		$criteria->compare('t.nilairujukan_id',$this->nilairujukan_id);
		$criteria->compare('t.pemeriksaanlab_nama',$this->pemeriksaanlab_nama);
		$criteria->compare('pemeriksaanlabdet_nourut',$this->pemeriksaanlabdet_nourut);
		// $criteria->compare('pemeriksaanlabdet_id',$this->pemeriksaanlabdet_id);
		// $criteria->compare('nilairujukan_id',$this->nilairujukan_id);
		// $criteria->compare('pemeriksaanlab_id',$this->pemeriksaanlab_id);
		// $criteria->compare('pemeriksaanlabdet_nourut',$this->pemeriksaanlabdet_nourut);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
        }
        
        /**
         * mencari detail pemeriksaanlab beserta nilai rujukannya
         */
        public function pemeriksaanDetail($modHasilPemeriksaan,$pemeriksaanId)
        {
            $criteria = new CDbCriteria;
            $criteria->with = array('nilairujukan');
            $criteria->compare('pemeriksaanlab_id',$pemeriksaanId);
            $criteria->compare('UPPER(nilairujukan.nilairujukan_jeniskelamin)',  strtoupper($modHasilPemeriksaan->hasil_jeniskelamin));
            //$criteria->compare('nilairujukan.kelompokumur',  strtoupper($modHasilPemeriksaan->hasil_kelompokumur));
            
            $criteria1 = new CDbCriteria;
            $criteria1->with = array('nilairujukan');
            $criteria1->compare('pemeriksaanlab_id',$pemeriksaanId);
            $criteria1->compare('UPPER(nilairujukan.nilairujukan_jeniskelamin)',  strtoupper($modHasilPemeriksaan->hasil_jeniskelamin));
            
            $criteria2 = new CDbCriteria;
            $criteria2->with = array('nilairujukan');
            $criteria2->compare('pemeriksaanlab_id',$pemeriksaanId);
            
            $details = $this->findAll($criteria);
            
            if(!empty($details))
            {
                return $details;
            }else{
                $details = $this->findAll($criteria1);
                if(!empty($details)){
                    return $details;
                }
                else{
                    $details = $this->findAll($criteria2);
                }
            }
            return $details;
        }
}