<?php
 
/**
 * This is the model class for table "mutasioaruangan_t".
 *
 * The followings are the available columns in table 'mutasioaruangan_t':
 * @property integer $mutasioaruangan_id
 * @property integer $pesanobatalkes_id
 * @property integer $terimamutasi_id
 * @property string $tglmutasioa
 * @property string $nomutasioa
 * @property integer $ruanganasal_id
 * @property integer $ruangantujuan_id
 * @property string $keteranganmutasi
 * @property double $totalharganettomutasi
 * @property double $totalhargajual
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class MutasioaruanganT extends CActiveRecord
{
        public $tglAwal;
        public $tglAkhir;
        public $instalasi_id;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MutasioaruanganT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mutasioaruangan_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tglmutasioa, nomutasioa, ruanganasal_id, ruangantujuan_id, totalharganettomutasi, totalhargajual', 'required'),
			array('pesanobatalkes_id, terimamutasi_id, ruanganasal_id, ruangantujuan_id', 'numerical', 'integerOnly'=>true),
			array('totalharganettomutasi, totalhargajual', 'numerical'),
			array('nomutasioa', 'length', 'max'=>20),
			array('instalasi_id, keteranganmutasi, update_time, update_loginpemakai_id', 'safe'),
                    
                        array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
			array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
                    
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('mutasioaruangan_id, pesanobatalkes_id, terimamutasi_id, tglmutasioa, nomutasioa, ruanganasal_id, ruangantujuan_id, keteranganmutasi, totalharganettomutasi, totalhargajual, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'ruangantujuan'=>array(self::BELONGS_TO,'RuanganM','ruangantujuan_id'),
                    'ruanganasal'=>array(self::BELONGS_TO,'RuanganM','ruanganasal_id'),
                    'pesanobatalkes'=>array(self::BELONGS_TO,'PesanobatalkesT','pesanobatalkes_id'),
                    'terimamutasi'=>array(self::BELONGS_TO,'TerimamutasiT','terimamutasi_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mutasioaruangan_id' => 'ID',
			'pesanobatalkes_id' => 'Pesan Obat Alkes',
			'terimamutasi_id' => 'Terima Mutasi',
			'tglmutasioa' => 'Tanggal Mutasi',
			'nomutasioa' => 'No Mutasi',
                        'tglAwal'=>'Tanggal Awal',
                        'tglAkhir'=>'Tanggal Akhir',
			'ruanganasal_id' => 'Ruangan Asal',
			'ruangantujuan_id' => 'Ruangan Tujuan',
			'keteranganmutasi' => 'Keterangan Mutasi',
			'totalharganettomutasi' => 'Total Netto',
			'totalhargajual' => 'Total Harga Jual',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mutasioaruangan_id',$this->mutasioaruangan_id);
		$criteria->compare('pesanobatalkes_id',$this->pesanobatalkes_id);
		$criteria->compare('terimamutasi_id',$this->terimamutasi_id);
                if (trim($this->tglmutasioa) != ''){
                    $criteria->compare('date(tglmutasioa)',$this->tglmutasioa);
                }
		$criteria->compare('LOWER(nomutasioa)',strtolower($this->nomutasioa),true);
		$criteria->compare('ruanganasal_id',$this->ruanganasal_id);
		$criteria->compare('ruangantujuan_id',$this->ruangantujuan_id);
		$criteria->compare('LOWER(keteranganmutasi)',strtolower($this->keteranganmutasi),true);
		$criteria->compare('totalharganettomutasi',$this->totalharganettomutasi);
		$criteria->compare('totalhargajual',$this->totalhargajual);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->addCondition('pesanobatalkes_id is null and terimamutasi_id is null and ruangantujuan_id ='.Yii::app()->user->getState('ruangan_id'));
        $criteria->order='tglmutasioa DESC';
                //$criteria->condition = 'terimamutasi_id is null';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('mutasioaruangan_id',$this->mutasioaruangan_id);
		$criteria->compare('pesanobatalkes_id',$this->pesanobatalkes_id);
		$criteria->compare('terimamutasi_id',$this->terimamutasi_id);
		$criteria->compare('LOWER(tglmutasioa)',strtolower($this->tglmutasioa),true);
		$criteria->compare('LOWER(nomutasioa)',strtolower($this->nomutasioa),true);
		$criteria->compare('ruanganasal_id',$this->ruanganasal_id);
		$criteria->compare('ruangantujuan_id',$this->ruangantujuan_id);
		$criteria->compare('LOWER(keteranganmutasi)',strtolower($this->keteranganmutasi),true);
		$criteria->compare('totalharganettomutasi',$this->totalharganettomutasi);
		$criteria->compare('totalhargajual',$this->totalhargajual);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->order='tglmutasioa DESC';
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                        }
            }
            return true;
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date')
                        {
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                        }
                    else if ( $column->dbType == 'timestamp without time zone')
                        {
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                        }
            }

            return parent::beforeValidate ();
        }
}