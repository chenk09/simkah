<?php

/**
 * This is the model class for table "penyusutangedung_t".
 *
 * The followings are the available columns in table 'penyusutangedung_t':
 * @property integer $penyusutangedung_id
 * @property integer $ruangan_id
 * @property integer $invgedung_id
 * @property integer $pegawai_id
 * @property string $tglpenyusutangedung
 * @property string $bulanpenyusutangedung
 * @property double $hargaperolehangedung
 * @property double $nilairesidugedung
 * @property integer $umurekonmisbln
 * @property string $tglterimagedung
 * @property double $bbnpenyusutanbrjlngdng
 * @property double $akumpenyusutangdng
 * @property double $nilaibukugedung
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $update_loginpemakai_id
 * @property integer $create_ruangan
 */
class PenyusutangedungT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PenyusutangedungT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penyusutangedung_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ruangan_id, invgedung_id, pegawai_id, tglpenyusutangedung, bulanpenyusutangedung, hargaperolehangedung, nilairesidugedung, umurekonmisbln, tglterimagedung, bbnpenyusutanbrjlngdng, akumpenyusutangdng, nilaibukugedung, create_time, create_loginpemakai_id, create_ruangan', 'required'),
			array('penyusutangedung_id, ruangan_id, invgedung_id, pegawai_id, umurekonmisbln, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'numerical', 'integerOnly'=>true),
			array('hargaperolehangedung, nilairesidugedung, bbnpenyusutanbrjlngdng, akumpenyusutangdng, nilaibukugedung', 'numerical'),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penyusutangedung_id, ruangan_id, invgedung_id, pegawai_id, tglpenyusutangedung, bulanpenyusutangedung, hargaperolehangedung, nilairesidugedung, umurekonmisbln, tglterimagedung, bbnpenyusutanbrjlngdng, akumpenyusutangdng, nilaibukugedung, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penyusutangedung_id' => 'Penyusutan Gedung',
			'ruangan_id' => 'Ruangan',
			'invgedung_id' => 'Inv gedung',
			'pegawai_id' => 'Pegawai',
			'tglpenyusutangedung' => 'Tgl Penyusutan',
			'bulanpenyusutangedung' => 'Bulan Penyusutan',
			'hargaperolehangedung' => 'Harga Perolehan',
			'nilairesidugedung' => 'Nilai Residu',
			'umurekonmisbln' => 'Umur Ekonomis',
			'tglterimagedung' => 'Tgl Terima',
			'bbnpenyusutanbrjlngdng' => 'Beban Penyusutan Berjalan',
			'akumpenyusutangdng' => 'Akumulasi Penyusutan',
			'nilaibukugedung' => 'Nilai Buku Gedung',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penyusutangedung_id',$this->penyusutangedung_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('invgedung_id',$this->invgedung_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(tglpenyusutangedung)',strtolower($this->tglpenyusutangedung),true);
		$criteria->compare('LOWER(bulanpenyusutangedung)',strtolower($this->bulanpenyusutangedung),true);
		$criteria->compare('hargaperolehangedung',$this->hargaperolehangedung);
		$criteria->compare('nilairesidugedung',$this->nilairesidugedung);
		$criteria->compare('umurekonmisbln',$this->umurekonmisbln);
		$criteria->compare('LOWER(tglterimagedung)',strtolower($this->tglterimagedung),true);
		$criteria->compare('bbnpenyusutanbrjlngdng',$this->bbnpenyusutanbrjlngdng);
		$criteria->compare('akumpenyusutangdng',$this->akumpenyusutangdng);
		$criteria->compare('nilaibukugedung',$this->nilaibukugedung);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penyusutangedung_id',$this->penyusutangedung_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('invgedung_id',$this->invgedung_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(tglpenyusutangedung)',strtolower($this->tglpenyusutangedung),true);
		$criteria->compare('LOWER(bulanpenyusutangedung)',strtolower($this->bulanpenyusutangedung),true);
		$criteria->compare('hargaperolehangedung',$this->hargaperolehangedung);
		$criteria->compare('nilairesidugedung',$this->nilairesidugedung);
		$criteria->compare('umurekonmisbln',$this->umurekonmisbln);
		$criteria->compare('LOWER(tglterimagedung)',strtolower($this->tglterimagedung),true);
		$criteria->compare('bbnpenyusutanbrjlngdng',$this->bbnpenyusutanbrjlngdng);
		$criteria->compare('akumpenyusutangdng',$this->akumpenyusutangdng);
		$criteria->compare('nilaibukugedung',$this->nilaibukugedung);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}