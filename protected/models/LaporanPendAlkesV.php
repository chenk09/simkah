<?php

/**
 * This is the model class for table "laporan_pend_alkes_v".
 *
 * The followings are the available columns in table 'laporan_pend_alkes_v':
 * @property string $tglpenjualan
 * @property integer $penjualanresep_id
 * @property integer $obatalkes_id
 * @property string $obatalkes_kode
 * @property string $obatalkes_nama
 * @property string $obatalkes_golongan
 * @property string $obatalkes_kategori
 * @property string $obatalkes_kadarobat
 * @property integer $jenisobatalkes_id
 * @property string $jenisobatalkes_nama
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 * @property string $noresep
 * @property string $jenispenjualan
 * @property integer $oasudahbayar_id
 * @property integer $returresep_id
 * @property double $harga_satuan
 * @property double $ppn_persen
 * @property double $hpp
 * @property double $qty_jual
 * @property double $bruto_jual
 * @property double $netto_jual
 * @property double $hpp_jual
 * @property double $discount_jual
 * @property double $qty_retur
 * @property double $total_ret_bruto
 * @property double $total_ret_netto
 * @property double $total_ret_hpp
 * @property double $total_bruto
 * @property double $total_netto
 * @property double $total_hpp
 */
class LaporanPendAlkesV extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $tglAwal, $tglAkhir, $print, $typePrint;
	public function tableName()
	{
		return 'laporan_pend_alkes_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penjualanresep_id, obatalkes_id, jenisobatalkes_id, ruangan_id, oasudahbayar_id, returresep_id', 'numerical', 'integerOnly'=>true),
			array('harga_satuan, harga_satuan, harga_satuan_jual, ppn_persen, hpp, qty_jual, bruto_jual, netto_jual, hpp_jual, discount_jual, qty_retur, total_ret_bruto, total_ret_netto, total_ret_hpp, total_bruto, total_netto, total_hpp', 'numerical'),
			array('obatalkes_kode, obatalkes_nama', 'length', 'max'=>200),
			array('obatalkes_golongan, obatalkes_kategori, jenisobatalkes_nama, ruangan_nama, noresep', 'length', 'max'=>50),
			array('obatalkes_kadarobat', 'length', 'max'=>20),
			array('jenispenjualan', 'length', 'max'=>100),
			array('tglpenjualan', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tglpenjualan, penjualanresep_id, obatalkes_id, obatalkes_kode, obatalkes_nama, obatalkes_golongan, obatalkes_kategori, obatalkes_kadarobat, jenisobatalkes_id, jenisobatalkes_nama, ruangan_id, ruangan_nama, noresep, jenispenjualan, oasudahbayar_id, returresep_id, harga_satuan, ppn_persen, hpp, qty_jual, bruto_jual, netto_jual, hpp_jual, discount_jual, qty_retur, total_ret_bruto, total_ret_netto, total_ret_hpp, total_bruto, total_netto, total_hpp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tglpenjualan' => 'Tglpenjualan',
			'penjualanresep_id' => 'Penjualanresep',
			'obatalkes_id' => 'Obatalkes',
			'obatalkes_kode' => 'Obatalkes Kode',
			'obatalkes_nama' => 'Obatalkes Nama',
			'obatalkes_golongan' => 'Obatalkes Golongan',
			'obatalkes_kategori' => 'Obatalkes Kategori',
			'obatalkes_kadarobat' => 'Obatalkes Kadarobat',
			'jenisobatalkes_id' => 'Jenisobatalkes',
			'jenisobatalkes_nama' => 'Jenisobatalkes Nama',
			'ruangan_id' => 'Ruangan',
			'ruangan_nama' => 'Ruangan Nama',
			'noresep' => 'Noresep',
			'jenispenjualan' => 'Jenispenjualan',
			'oasudahbayar_id' => 'Oasudahbayar',
			'returresep_id' => 'Returresep',
			'harga_satuan' => 'Harga Satuan',
			'harga_satuan_jual' => 'Harga Satuan',
			'ppn_persen' => 'Ppn Persen',
			'hpp' => 'Hpp',
			'qty_jual' => 'Qty Jual',
			'bruto_jual' => 'Bruto Jual',
			'netto_jual' => 'Netto Jual',
			'hpp_jual' => 'Hpp Jual',
			'discount_jual' => 'Discount Jual',
			'qty_retur' => 'Qty Retur',
			'total_ret_bruto' => 'Total Ret Bruto',
			'total_ret_netto' => 'Total Ret Netto',
			'total_ret_hpp' => 'Total Ret Hpp',
			'total_bruto' => 'Total Bruto',
			'total_netto' => 'Total Netto',
			'total_hpp' => 'Total Hpp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tglpenjualan',$this->tglpenjualan,true);
		$criteria->compare('penjualanresep_id',$this->penjualanresep_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('obatalkes_kode',$this->obatalkes_kode,true);
		$criteria->compare('obatalkes_nama',$this->obatalkes_nama,true);
		$criteria->compare('obatalkes_golongan',$this->obatalkes_golongan,true);
		$criteria->compare('obatalkes_kategori',$this->obatalkes_kategori,true);
		$criteria->compare('obatalkes_kadarobat',$this->obatalkes_kadarobat,true);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('jenisobatalkes_nama',$this->jenisobatalkes_nama,true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('ruangan_nama',$this->ruangan_nama,true);
		$criteria->compare('noresep',$this->noresep,true);
		$criteria->compare('jenispenjualan',$this->jenispenjualan,true);
		$criteria->compare('oasudahbayar_id',$this->oasudahbayar_id);
		$criteria->compare('returresep_id',$this->returresep_id);
		$criteria->compare('harga_satuan',$this->harga_satuan);
		$criteria->compare('ppn_persen',$this->ppn_persen);
		$criteria->compare('hpp',$this->hpp);
		$criteria->compare('qty_jual',$this->qty_jual);
		$criteria->compare('bruto_jual',$this->bruto_jual);
		$criteria->compare('netto_jual',$this->netto_jual);
		$criteria->compare('hpp_jual',$this->hpp_jual);
		$criteria->compare('discount_jual',$this->discount_jual);
		$criteria->compare('qty_retur',$this->qty_retur);
		$criteria->compare('total_ret_bruto',$this->total_ret_bruto);
		$criteria->compare('total_ret_netto',$this->total_ret_netto);
		$criteria->compare('total_ret_hpp',$this->total_ret_hpp);
		$criteria->compare('total_bruto',$this->total_bruto);
		$criteria->compare('total_netto',$this->total_netto);
		$criteria->compare('total_hpp',$this->total_hpp);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LaporanPendAlkesV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
