<?php

/**
 * This is the model class for table "laporanpenyusutanaset_v".
 *
 * The followings are the available columns in table 'laporanpenyusutanaset_v':
 * @property integer $penyusutan_id
 * @property string $tgl_penyusutan
 * @property string $bulanpenyusutan
 * @property double $hargaperolehan
 * @property double $nilairesidu
 * @property integer $umurekonmisbln
 * @property string $tglterimagedung
 * @property double $bebanpenyusutanberjalan
 * @property double $akumulasipenyusutan
 * @property double $nilaibukuaktiva
 * @property integer $instalasi_id
 * @property string $instalasi_nama
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 * @property integer $inventarisasi_id
 * @property string $inventarisasi_kode
 * @property string $inventarisasi_noregister
 * @property integer $pemilikbarang_id
 * @property string $pemilikbarang_kode
 * @property string $pemilikbarang_nama
 * @property integer $golongan_id
 * @property string $golongan_kode
 * @property string $golongan_nama
 * @property integer $kelompok_id
 * @property string $kelompok_kode
 * @property string $kelompok_nama
 * @property integer $subkelompok_id
 * @property string $subkelompok_kode
 * @property string $subkelompok_nama
 * @property integer $bidang_id
 * @property string $bidang_kode
 * @property string $bidang_nama
 * @property integer $barang_id
 * @property string $barang_type
 * @property string $barang_kode
 * @property string $barang_nama
 * @property string $barang_namalainnya
 * @property string $barang_merk
 * @property string $barang_noseri
 * @property string $barang_ukuran
 * @property string $barang_bahan
 * @property string $barang_thnbeli
 * @property string $barang_warna
 * @property boolean $barang_statusregister
 * @property integer $barang_ekonomis_thn
 * @property string $barang_satuan
 * @property integer $barang_jmldlmkemasan
 * @property string $barang_image
 * @property double $barang_harganetto
 * @property double $barang_persendiskon
 * @property double $barang_ppn
 * @property double $barang_hpp
 * @property double $barang_hargajual
 * @property integer $lokasi_id
 * @property string $lokasiaset_kode
 * @property string $lokasiaset_namainstalasi
 * @property string $lokasiaset_namabagian
 * @property string $lokasiaset_namalokasi
 * @property integer $asalaset_id
 * @property string $asalaset_nama
 * @property string $asalaset_singkatan
 * @property string $invgedung_kontruksi
 * @property double $invgedung_luaslantai
 * @property string $invgedung_alamat
 * @property string $invgedung_tgldokumen
 * @property string $inventarisasi_tglguna
 * @property string $invgedung_nodokumen
 * @property double $inventarisasi_harga
 * @property double $inventarisasi_akumsusut
 * @property string $keterangan
 * @property integer $umurekonomis
 * @property double $inventarisasi_nilairesidu
 * @property string $tglpenghapusan
 * @property string $tipepenghapusan
 * @property double $hargajualaktiva
 * @property double $kerugian
 * @property double $keuntungan
 * @property boolean $invperalatan_ijinoperasional
 * @property string $invperalatan_kapasitasrata
 * @property string $invperalatan_keadaan
 * @property string $invperalatan_merk
 * @property string $invperalatan_nobpkb
 * @property string $invperalatan_nomesin
 * @property string $invperalatan_nopabrik
 * @property string $invperalatan_nopolisi
 * @property string $invperalatan_norangka
 * @property string $invperalatan_serftkkalibrasi
 * @property string $invperalatan_thnpembelian
 * @property string $invperalatan_ukuran
 * @property integer $invperalatan_umurekonomis
 * @property string $jenisaset
 */
class LaporanpenyusutanasetV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanpenyusutanasetV the static model class
	 */
	public $tglAwal, $tglAkhir, $kodeinventaris;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanpenyusutanaset_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penyusutan_id, umurekonmisbln, instalasi_id, ruangan_id, inventarisasi_id, pemilikbarang_id, golongan_id, kelompok_id, subkelompok_id, bidang_id, barang_id, barang_ekonomis_thn, barang_jmldlmkemasan, lokasi_id, asalaset_id, umurekonomis, invperalatan_umurekonomis', 'numerical', 'integerOnly'=>true),
			array('hargaperolehan, nilairesidu, bebanpenyusutanberjalan, akumulasipenyusutan, nilaibukuaktiva, barang_harganetto, barang_persendiskon, barang_ppn, barang_hpp, barang_hargajual, invgedung_luaslantai, inventarisasi_harga, inventarisasi_akumsusut, inventarisasi_nilairesidu, hargajualaktiva, kerugian, keuntungan', 'numerical'),
			array('instalasi_nama, ruangan_nama, inventarisasi_kode, inventarisasi_noregister, golongan_kode, kelompok_kode, subkelompok_kode, bidang_kode, barang_type, barang_kode, barang_merk, barang_warna, barang_satuan, lokasiaset_kode, lokasiaset_namabagian, asalaset_nama', 'length', 'max'=>50),
			array('pemilikbarang_kode, barang_noseri, barang_ukuran, barang_bahan', 'length', 'max'=>20),
			array('pemilikbarang_nama, golongan_nama, kelompok_nama, subkelompok_nama, bidang_nama, barang_nama, barang_namalainnya, lokasiaset_namainstalasi, lokasiaset_namalokasi', 'length', 'max'=>100),
			array('barang_thnbeli', 'length', 'max'=>5),
			array('barang_image', 'length', 'max'=>200),
			array('asalaset_singkatan', 'length', 'max'=>10),
			array('tipepenghapusan', 'length', 'max'=>25),
			array('jenisaset', 'length', 'max'=>15),
			array('tgl_penyusutan, bulanpenyusutan, tglterimagedung, barang_statusregister, invgedung_kontruksi, invgedung_alamat, invgedung_tgldokumen, inventarisasi_tglguna, invgedung_nodokumen, keterangan, tglpenghapusan, invperalatan_ijinoperasional, invperalatan_kapasitasrata, invperalatan_keadaan, invperalatan_merk, invperalatan_nobpkb, invperalatan_nomesin, invperalatan_nopabrik, invperalatan_nopolisi, invperalatan_norangka, invperalatan_serftkkalibrasi, invperalatan_thnpembelian, invperalatan_ukuran', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penyusutan_id, tgl_penyusutan, bulanpenyusutan, hargaperolehan, nilairesidu, umurekonmisbln, tglterimagedung, bebanpenyusutanberjalan, akumulasipenyusutan, nilaibukuaktiva, instalasi_id, instalasi_nama, ruangan_id, ruangan_nama, inventarisasi_id, inventarisasi_kode, inventarisasi_noregister, pemilikbarang_id, pemilikbarang_kode, pemilikbarang_nama, golongan_id, golongan_kode, golongan_nama, kelompok_id, kelompok_kode, kelompok_nama, subkelompok_id, subkelompok_kode, subkelompok_nama, bidang_id, bidang_kode, bidang_nama, barang_id, barang_type, barang_kode, barang_nama, barang_namalainnya, barang_merk, barang_noseri, barang_ukuran, barang_bahan, barang_thnbeli, barang_warna, barang_statusregister, barang_ekonomis_thn, barang_satuan, barang_jmldlmkemasan, barang_image, barang_harganetto, barang_persendiskon, barang_ppn, barang_hpp, barang_hargajual, lokasi_id, lokasiaset_kode, lokasiaset_namainstalasi, lokasiaset_namabagian, lokasiaset_namalokasi, asalaset_id, asalaset_nama, asalaset_singkatan, invgedung_kontruksi, invgedung_luaslantai, invgedung_alamat, invgedung_tgldokumen, inventarisasi_tglguna, invgedung_nodokumen, inventarisasi_harga, inventarisasi_akumsusut, keterangan, umurekonomis, inventarisasi_nilairesidu, tglpenghapusan, tipepenghapusan, hargajualaktiva, kerugian, keuntungan, invperalatan_ijinoperasional, invperalatan_kapasitasrata, invperalatan_keadaan, invperalatan_merk, invperalatan_nobpkb, invperalatan_nomesin, invperalatan_nopabrik, invperalatan_nopolisi, invperalatan_norangka, invperalatan_serftkkalibrasi, invperalatan_thnpembelian, invperalatan_ukuran, invperalatan_umurekonomis, jenisaset', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penyusutan_id' => 'Penyusutan',
			'tgl_penyusutan' => 'Tgl Penyusutan',
			'bulanpenyusutan' => 'Bulanpenyusutan',
			'hargaperolehan' => 'Hargaperolehan',
			'nilairesidu' => 'Nilairesidu',
			'umurekonmisbln' => 'Umurekonmisbln',
			'tglterimagedung' => 'Tglterimagedung',
			'bebanpenyusutanberjalan' => 'Bebanpenyusutanberjalan',
			'akumulasipenyusutan' => 'Akumulasipenyusutan',
			'nilaibukuaktiva' => 'Nilaibukuaktiva',
			'instalasi_id' => 'Instalasi',
			'instalasi_nama' => 'Instalasi Nama',
			'ruangan_id' => 'Ruangan',
			'ruangan_nama' => 'Ruangan Nama',
			'inventarisasi_id' => 'Inventarisasi',
			'inventarisasi_kode' => 'Inventarisasi Kode',
			'inventarisasi_noregister' => 'Inventarisasi Noregister',
			'pemilikbarang_id' => 'Pemilikbarang',
			'pemilikbarang_kode' => 'Pemilikbarang Kode',
			'pemilikbarang_nama' => 'Pemilikbarang Nama',
			'golongan_id' => 'Golongan',
			'golongan_kode' => 'Golongan Kode',
			'golongan_nama' => 'Golongan Nama',
			'kelompok_id' => 'Kelompok',
			'kelompok_kode' => 'Kelompok Kode',
			'kelompok_nama' => 'Kelompok Nama',
			'subkelompok_id' => 'Subkelompok',
			'subkelompok_kode' => 'Subkelompok Kode',
			'subkelompok_nama' => 'Subkelompok Nama',
			'bidang_id' => 'Bidang',
			'bidang_kode' => 'Bidang Kode',
			'bidang_nama' => 'Bidang Nama',
			'barang_id' => 'Barang',
			'barang_type' => 'Barang Type',
			'barang_kode' => 'Barang Kode',
			'barang_nama' => 'Barang Nama',
			'barang_namalainnya' => 'Barang Namalainnya',
			'barang_merk' => 'Barang Merk',
			'barang_noseri' => 'Barang Noseri',
			'barang_ukuran' => 'Barang Ukuran',
			'barang_bahan' => 'Barang Bahan',
			'barang_thnbeli' => 'Barang Thnbeli',
			'barang_warna' => 'Barang Warna',
			'barang_statusregister' => 'Barang Statusregister',
			'barang_ekonomis_thn' => 'Barang Ekonomis Thn',
			'barang_satuan' => 'Barang Satuan',
			'barang_jmldlmkemasan' => 'Barang Jmldlmkemasan',
			'barang_image' => 'Barang Image',
			'barang_harganetto' => 'Barang Harganetto',
			'barang_persendiskon' => 'Barang Persendiskon',
			'barang_ppn' => 'Barang Ppn',
			'barang_hpp' => 'Barang Hpp',
			'barang_hargajual' => 'Barang Hargajual',
			'lokasi_id' => 'Lokasi',
			'lokasiaset_kode' => 'Lokasiaset Kode',
			'lokasiaset_namainstalasi' => 'Lokasiaset Namainstalasi',
			'lokasiaset_namabagian' => 'Lokasiaset Namabagian',
			'lokasiaset_namalokasi' => 'Lokasiaset Namalokasi',
			'asalaset_id' => 'Asalaset',
			'asalaset_nama' => 'Asalaset Nama',
			'asalaset_singkatan' => 'Asalaset Singkatan',
			'invgedung_kontruksi' => 'Invgedung Kontruksi',
			'invgedung_luaslantai' => 'Invgedung Luaslantai',
			'invgedung_alamat' => 'Invgedung Alamat',
			'invgedung_tgldokumen' => 'Invgedung Tgldokumen',
			'inventarisasi_tglguna' => 'Inventarisasi Tglguna',
			'invgedung_nodokumen' => 'Invgedung Nodokumen',
			'inventarisasi_harga' => 'Inventarisasi Harga',
			'inventarisasi_akumsusut' => 'Inventarisasi Akumsusut',
			'keterangan' => 'Keterangan',
			'umurekonomis' => 'Umurekonomis',
			'inventarisasi_nilairesidu' => 'Inventarisasi Nilairesidu',
			'tglpenghapusan' => 'Tglpenghapusan',
			'tipepenghapusan' => 'Tipepenghapusan',
			'hargajualaktiva' => 'Hargajualaktiva',
			'kerugian' => 'Kerugian',
			'keuntungan' => 'Keuntungan',
			'invperalatan_ijinoperasional' => 'Invperalatan Ijinoperasional',
			'invperalatan_kapasitasrata' => 'Invperalatan Kapasitasrata',
			'invperalatan_keadaan' => 'Invperalatan Keadaan',
			'invperalatan_merk' => 'Invperalatan Merk',
			'invperalatan_nobpkb' => 'Invperalatan Nobpkb',
			'invperalatan_nomesin' => 'Invperalatan Nomesin',
			'invperalatan_nopabrik' => 'Invperalatan Nopabrik',
			'invperalatan_nopolisi' => 'Invperalatan Nopolisi',
			'invperalatan_norangka' => 'Invperalatan Norangka',
			'invperalatan_serftkkalibrasi' => 'Invperalatan Serftkkalibrasi',
			'invperalatan_thnpembelian' => 'Invperalatan Thnpembelian',
			'invperalatan_ukuran' => 'Invperalatan Ukuran',
			'invperalatan_umurekonomis' => 'Invperalatan Umurekonomis',
			'jenisaset' => 'Jenisaset',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penyusutan_id',$this->penyusutan_id);
		$criteria->compare('LOWER(tgl_penyusutan)',strtolower($this->tgl_penyusutan),true);
		$criteria->compare('LOWER(bulanpenyusutan)',strtolower($this->bulanpenyusutan),true);
		$criteria->compare('hargaperolehan',$this->hargaperolehan);
		$criteria->compare('nilairesidu',$this->nilairesidu);
		$criteria->compare('umurekonmisbln',$this->umurekonmisbln);
		$criteria->compare('LOWER(tglterimagedung)',strtolower($this->tglterimagedung),true);
		$criteria->compare('bebanpenyusutanberjalan',$this->bebanpenyusutanberjalan);
		$criteria->compare('akumulasipenyusutan',$this->akumulasipenyusutan);
		$criteria->compare('nilaibukuaktiva',$this->nilaibukuaktiva);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('inventarisasi_id',$this->inventarisasi_id);
		$criteria->compare('LOWER(inventarisasi_kode)',strtolower($this->inventarisasi_kode),true);
		$criteria->compare('LOWER(inventarisasi_noregister)',strtolower($this->inventarisasi_noregister),true);
		$criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
		$criteria->compare('LOWER(pemilikbarang_kode)',strtolower($this->pemilikbarang_kode),true);
		$criteria->compare('LOWER(pemilikbarang_nama)',strtolower($this->pemilikbarang_nama),true);
		$criteria->compare('golongan_id',$this->golongan_id);
		$criteria->compare('LOWER(golongan_kode)',strtolower($this->golongan_kode),true);
		$criteria->compare('LOWER(golongan_nama)',strtolower($this->golongan_nama),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(kelompok_kode)',strtolower($this->kelompok_kode),true);
		$criteria->compare('LOWER(kelompok_nama)',strtolower($this->kelompok_nama),true);
		$criteria->compare('subkelompok_id',$this->subkelompok_id);
		$criteria->compare('LOWER(subkelompok_kode)',strtolower($this->subkelompok_kode),true);
		$criteria->compare('LOWER(subkelompok_nama)',strtolower($this->subkelompok_nama),true);
		$criteria->compare('bidang_id',$this->bidang_id);
		$criteria->compare('LOWER(bidang_kode)',strtolower($this->bidang_kode),true);
		$criteria->compare('LOWER(bidang_nama)',strtolower($this->bidang_nama),true);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('LOWER(barang_type)',strtolower($this->barang_type),true);
		$criteria->compare('LOWER(barang_kode)',strtolower($this->barang_kode),true);
		$criteria->compare('LOWER(barang_nama)',strtolower($this->barang_nama),true);
		$criteria->compare('LOWER(barang_namalainnya)',strtolower($this->barang_namalainnya),true);
		$criteria->compare('LOWER(barang_merk)',strtolower($this->barang_merk),true);
		$criteria->compare('LOWER(barang_noseri)',strtolower($this->barang_noseri),true);
		$criteria->compare('LOWER(barang_ukuran)',strtolower($this->barang_ukuran),true);
		$criteria->compare('LOWER(barang_bahan)',strtolower($this->barang_bahan),true);
		$criteria->compare('LOWER(barang_thnbeli)',strtolower($this->barang_thnbeli),true);
		$criteria->compare('LOWER(barang_warna)',strtolower($this->barang_warna),true);
		$criteria->compare('barang_statusregister',$this->barang_statusregister);
		$criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
		$criteria->compare('LOWER(barang_satuan)',strtolower($this->barang_satuan),true);
		$criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
		$criteria->compare('LOWER(barang_image)',strtolower($this->barang_image),true);
		$criteria->compare('barang_harganetto',$this->barang_harganetto);
		$criteria->compare('barang_persendiskon',$this->barang_persendiskon);
		$criteria->compare('barang_ppn',$this->barang_ppn);
		$criteria->compare('barang_hpp',$this->barang_hpp);
		$criteria->compare('barang_hargajual',$this->barang_hargajual);
		$criteria->compare('lokasi_id',$this->lokasi_id);
		$criteria->compare('LOWER(lokasiaset_kode)',strtolower($this->lokasiaset_kode),true);
		$criteria->compare('LOWER(lokasiaset_namainstalasi)',strtolower($this->lokasiaset_namainstalasi),true);
		$criteria->compare('LOWER(lokasiaset_namabagian)',strtolower($this->lokasiaset_namabagian),true);
		$criteria->compare('LOWER(lokasiaset_namalokasi)',strtolower($this->lokasiaset_namalokasi),true);
		$criteria->compare('asalaset_id',$this->asalaset_id);
		$criteria->compare('LOWER(asalaset_nama)',strtolower($this->asalaset_nama),true);
		$criteria->compare('LOWER(asalaset_singkatan)',strtolower($this->asalaset_singkatan),true);
		$criteria->compare('LOWER(invgedung_kontruksi)',strtolower($this->invgedung_kontruksi),true);
		$criteria->compare('invgedung_luaslantai',$this->invgedung_luaslantai);
		$criteria->compare('LOWER(invgedung_alamat)',strtolower($this->invgedung_alamat),true);
		$criteria->compare('LOWER(invgedung_tgldokumen)',strtolower($this->invgedung_tgldokumen),true);
		$criteria->compare('LOWER(inventarisasi_tglguna)',strtolower($this->inventarisasi_tglguna),true);
		$criteria->compare('LOWER(invgedung_nodokumen)',strtolower($this->invgedung_nodokumen),true);
		$criteria->compare('inventarisasi_harga',$this->inventarisasi_harga);
		$criteria->compare('inventarisasi_akumsusut',$this->inventarisasi_akumsusut);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('umurekonomis',$this->umurekonomis);
		$criteria->compare('inventarisasi_nilairesidu',$this->inventarisasi_nilairesidu);
		$criteria->compare('LOWER(tglpenghapusan)',strtolower($this->tglpenghapusan),true);
		$criteria->compare('LOWER(tipepenghapusan)',strtolower($this->tipepenghapusan),true);
		$criteria->compare('hargajualaktiva',$this->hargajualaktiva);
		$criteria->compare('kerugian',$this->kerugian);
		$criteria->compare('keuntungan',$this->keuntungan);
		$criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
		$criteria->compare('LOWER(invperalatan_kapasitasrata)',strtolower($this->invperalatan_kapasitasrata),true);
		$criteria->compare('LOWER(invperalatan_keadaan)',strtolower($this->invperalatan_keadaan),true);
		$criteria->compare('LOWER(invperalatan_merk)',strtolower($this->invperalatan_merk),true);
		$criteria->compare('LOWER(invperalatan_nobpkb)',strtolower($this->invperalatan_nobpkb),true);
		$criteria->compare('LOWER(invperalatan_nomesin)',strtolower($this->invperalatan_nomesin),true);
		$criteria->compare('LOWER(invperalatan_nopabrik)',strtolower($this->invperalatan_nopabrik),true);
		$criteria->compare('LOWER(invperalatan_nopolisi)',strtolower($this->invperalatan_nopolisi),true);
		$criteria->compare('LOWER(invperalatan_norangka)',strtolower($this->invperalatan_norangka),true);
		$criteria->compare('LOWER(invperalatan_serftkkalibrasi)',strtolower($this->invperalatan_serftkkalibrasi),true);
		$criteria->compare('LOWER(invperalatan_thnpembelian)',strtolower($this->invperalatan_thnpembelian),true);
		$criteria->compare('LOWER(invperalatan_ukuran)',strtolower($this->invperalatan_ukuran),true);
		$criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
		$criteria->compare('LOWER(jenisaset)',strtolower($this->jenisaset),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penyusutan_id',$this->penyusutan_id);
		$criteria->compare('LOWER(tgl_penyusutan)',strtolower($this->tgl_penyusutan),true);
		$criteria->compare('LOWER(bulanpenyusutan)',strtolower($this->bulanpenyusutan),true);
		$criteria->compare('hargaperolehan',$this->hargaperolehan);
		$criteria->compare('nilairesidu',$this->nilairesidu);
		$criteria->compare('umurekonmisbln',$this->umurekonmisbln);
		$criteria->compare('LOWER(tglterimagedung)',strtolower($this->tglterimagedung),true);
		$criteria->compare('bebanpenyusutanberjalan',$this->bebanpenyusutanberjalan);
		$criteria->compare('akumulasipenyusutan',$this->akumulasipenyusutan);
		$criteria->compare('nilaibukuaktiva',$this->nilaibukuaktiva);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('inventarisasi_id',$this->inventarisasi_id);
		$criteria->compare('LOWER(inventarisasi_kode)',strtolower($this->inventarisasi_kode),true);
		$criteria->compare('LOWER(inventarisasi_noregister)',strtolower($this->inventarisasi_noregister),true);
		$criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
		$criteria->compare('LOWER(pemilikbarang_kode)',strtolower($this->pemilikbarang_kode),true);
		$criteria->compare('LOWER(pemilikbarang_nama)',strtolower($this->pemilikbarang_nama),true);
		$criteria->compare('golongan_id',$this->golongan_id);
		$criteria->compare('LOWER(golongan_kode)',strtolower($this->golongan_kode),true);
		$criteria->compare('LOWER(golongan_nama)',strtolower($this->golongan_nama),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(kelompok_kode)',strtolower($this->kelompok_kode),true);
		$criteria->compare('LOWER(kelompok_nama)',strtolower($this->kelompok_nama),true);
		$criteria->compare('subkelompok_id',$this->subkelompok_id);
		$criteria->compare('LOWER(subkelompok_kode)',strtolower($this->subkelompok_kode),true);
		$criteria->compare('LOWER(subkelompok_nama)',strtolower($this->subkelompok_nama),true);
		$criteria->compare('bidang_id',$this->bidang_id);
		$criteria->compare('LOWER(bidang_kode)',strtolower($this->bidang_kode),true);
		$criteria->compare('LOWER(bidang_nama)',strtolower($this->bidang_nama),true);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('LOWER(barang_type)',strtolower($this->barang_type),true);
		$criteria->compare('LOWER(barang_kode)',strtolower($this->barang_kode),true);
		$criteria->compare('LOWER(barang_nama)',strtolower($this->barang_nama),true);
		$criteria->compare('LOWER(barang_namalainnya)',strtolower($this->barang_namalainnya),true);
		$criteria->compare('LOWER(barang_merk)',strtolower($this->barang_merk),true);
		$criteria->compare('LOWER(barang_noseri)',strtolower($this->barang_noseri),true);
		$criteria->compare('LOWER(barang_ukuran)',strtolower($this->barang_ukuran),true);
		$criteria->compare('LOWER(barang_bahan)',strtolower($this->barang_bahan),true);
		$criteria->compare('LOWER(barang_thnbeli)',strtolower($this->barang_thnbeli),true);
		$criteria->compare('LOWER(barang_warna)',strtolower($this->barang_warna),true);
		$criteria->compare('barang_statusregister',$this->barang_statusregister);
		$criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
		$criteria->compare('LOWER(barang_satuan)',strtolower($this->barang_satuan),true);
		$criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
		$criteria->compare('LOWER(barang_image)',strtolower($this->barang_image),true);
		$criteria->compare('barang_harganetto',$this->barang_harganetto);
		$criteria->compare('barang_persendiskon',$this->barang_persendiskon);
		$criteria->compare('barang_ppn',$this->barang_ppn);
		$criteria->compare('barang_hpp',$this->barang_hpp);
		$criteria->compare('barang_hargajual',$this->barang_hargajual);
		$criteria->compare('lokasi_id',$this->lokasi_id);
		$criteria->compare('LOWER(lokasiaset_kode)',strtolower($this->lokasiaset_kode),true);
		$criteria->compare('LOWER(lokasiaset_namainstalasi)',strtolower($this->lokasiaset_namainstalasi),true);
		$criteria->compare('LOWER(lokasiaset_namabagian)',strtolower($this->lokasiaset_namabagian),true);
		$criteria->compare('LOWER(lokasiaset_namalokasi)',strtolower($this->lokasiaset_namalokasi),true);
		$criteria->compare('asalaset_id',$this->asalaset_id);
		$criteria->compare('LOWER(asalaset_nama)',strtolower($this->asalaset_nama),true);
		$criteria->compare('LOWER(asalaset_singkatan)',strtolower($this->asalaset_singkatan),true);
		$criteria->compare('LOWER(invgedung_kontruksi)',strtolower($this->invgedung_kontruksi),true);
		$criteria->compare('invgedung_luaslantai',$this->invgedung_luaslantai);
		$criteria->compare('LOWER(invgedung_alamat)',strtolower($this->invgedung_alamat),true);
		$criteria->compare('LOWER(invgedung_tgldokumen)',strtolower($this->invgedung_tgldokumen),true);
		$criteria->compare('LOWER(inventarisasi_tglguna)',strtolower($this->inventarisasi_tglguna),true);
		$criteria->compare('LOWER(invgedung_nodokumen)',strtolower($this->invgedung_nodokumen),true);
		$criteria->compare('inventarisasi_harga',$this->inventarisasi_harga);
		$criteria->compare('inventarisasi_akumsusut',$this->inventarisasi_akumsusut);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('umurekonomis',$this->umurekonomis);
		$criteria->compare('inventarisasi_nilairesidu',$this->inventarisasi_nilairesidu);
		$criteria->compare('LOWER(tglpenghapusan)',strtolower($this->tglpenghapusan),true);
		$criteria->compare('LOWER(tipepenghapusan)',strtolower($this->tipepenghapusan),true);
		$criteria->compare('hargajualaktiva',$this->hargajualaktiva);
		$criteria->compare('kerugian',$this->kerugian);
		$criteria->compare('keuntungan',$this->keuntungan);
		$criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
		$criteria->compare('LOWER(invperalatan_kapasitasrata)',strtolower($this->invperalatan_kapasitasrata),true);
		$criteria->compare('LOWER(invperalatan_keadaan)',strtolower($this->invperalatan_keadaan),true);
		$criteria->compare('LOWER(invperalatan_merk)',strtolower($this->invperalatan_merk),true);
		$criteria->compare('LOWER(invperalatan_nobpkb)',strtolower($this->invperalatan_nobpkb),true);
		$criteria->compare('LOWER(invperalatan_nomesin)',strtolower($this->invperalatan_nomesin),true);
		$criteria->compare('LOWER(invperalatan_nopabrik)',strtolower($this->invperalatan_nopabrik),true);
		$criteria->compare('LOWER(invperalatan_nopolisi)',strtolower($this->invperalatan_nopolisi),true);
		$criteria->compare('LOWER(invperalatan_norangka)',strtolower($this->invperalatan_norangka),true);
		$criteria->compare('LOWER(invperalatan_serftkkalibrasi)',strtolower($this->invperalatan_serftkkalibrasi),true);
		$criteria->compare('LOWER(invperalatan_thnpembelian)',strtolower($this->invperalatan_thnpembelian),true);
		$criteria->compare('LOWER(invperalatan_ukuran)',strtolower($this->invperalatan_ukuran),true);
		$criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
		$criteria->compare('LOWER(jenisaset)',strtolower($this->jenisaset),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchGroupBy()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->select = "t.barang_type, t.barang_nama, t.inventarisasi_noregister, t.pemilikbarang_nama, t.inventarisasi_harga, t.bebanpenyusutanberjalan, t.tglterimagedung, split_part(inventarisasi_noregister, '-',3) as kodeinventaris";
		$criteria->group = 't.inventarisasi_noregister, t.barang_type, t.barang_nama,t.pemilikbarang_nama, t.inventarisasi_harga, t.bebanpenyusutanberjalan, t.tglterimagedung';
  		$criteria->addBetweenCondition('date(tglterimagedung)', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(pemilikbarang_nama)',strtolower($this->pemilikbarang_nama),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	 /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchGroupByPrint()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->select = "t.barang_type, t.barang_nama, t.inventarisasi_noregister, t.pemilikbarang_nama, t.inventarisasi_harga, t.bebanpenyusutanberjalan, t.tglterimagedung, split_part(inventarisasi_noregister, '-',3) as kodeinventaris";
		$criteria->group = 't.inventarisasi_noregister, t.barang_type, t.barang_nama,t.pemilikbarang_nama, t.inventarisasi_harga, t.bebanpenyusutanberjalan, t.tglterimagedung';
  		$criteria->addBetweenCondition('date(tglterimagedung)', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(pemilikbarang_nama)',strtolower($this->pemilikbarang_nama),true);

		$criteria->limit=-1; 
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
	}
}