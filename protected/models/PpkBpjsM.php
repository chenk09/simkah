<?php

/**
 * This is the model class for table "ppk_bpjs_m".
 *
 * The followings are the available columns in table 'ppk_bpjs_m':
 * @property integer $ppk_bpjs_id
 * @property string $ppk_bpjs_kode
 * @property string $ppk_bpjs_nama
 * @property boolean $ppk_bpjs_aktif
 */
class PpkBpjsM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PpkBpjsM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ppk_bpjs_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ppk_bpjs_kode', 'length', 'max'=>20),
			array('ppk_bpjs_nama', 'length', 'max'=>50),
			array('ppk_bpjs_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ppk_bpjs_id, ppk_bpjs_kode, ppk_bpjs_nama, ppk_bpjs_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ppk_bpjs_id' => 'Ppk Bpjs',
			'ppk_bpjs_kode' => 'Ppk Bpjs Kode',
			'ppk_bpjs_nama' => 'Ppk Bpjs Nama',
			'ppk_bpjs_aktif' => 'Ppk Bpjs Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ppk_bpjs_id',$this->ppk_bpjs_id);
		$criteria->compare('LOWER(ppk_bpjs_kode)',strtolower($this->ppk_bpjs_kode),true);
		$criteria->compare('LOWER(ppk_bpjs_nama)',strtolower($this->ppk_bpjs_nama),true);
		$criteria->compare('ppk_bpjs_aktif',$this->ppk_bpjs_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('ppk_bpjs_id',$this->ppk_bpjs_id);
		$criteria->compare('LOWER(ppk_bpjs_kode)',strtolower($this->ppk_bpjs_kode),true);
		$criteria->compare('LOWER(ppk_bpjs_nama)',strtolower($this->ppk_bpjs_nama),true);
		$criteria->compare('ppk_bpjs_aktif',$this->ppk_bpjs_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}