<?php

/**
 * This is the model class for table "pasiendirujukkeluar_t".
 *
 * The followings are the available columns in table 'pasiendirujukkeluar_t':
 * @property integer $pasiendirujukkeluar_id
 * @property integer $pasienadmisi_id
 * @property integer $pendaftaran_id
 * @property integer $pegawai_id
 * @property integer $pasien_id
 * @property integer $rujukankeluar_id
 * @property string $nosuratrujukan
 * @property string $tgldirujuk
 * @property string $ythdokter
 * @property string $dirujukkebagian
 * @property string $alasandirujuk
 * @property string $hasilpemeriksaan_ruj
 * @property string $diagnosasementara_ruj
 * @property string $pengobatan_ruj
 * @property string $lainlain_ruj
 * @property string $catatandokterperujuk
 * @property string $ruanganasal_id
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class PasiendirujukkeluarT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PasiendirujukkeluarT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pasiendirujukkeluar_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pendaftaran_id, pegawai_id, pasien_id, rujukankeluar_id, tgldirujuk, ruanganasal_id', 'required'),
			array('pasienadmisi_id, pendaftaran_id, pegawai_id, pasien_id, rujukankeluar_id', 'numerical', 'integerOnly'=>true),
			array('nosuratrujukan', 'length', 'max'=>50),
			array('ythdokter', 'length', 'max'=>100),
			array('dirujukkebagian', 'length', 'max'=>30),
			array('alasandirujuk, hasilpemeriksaan_ruj, diagnosasementara_ruj, pengobatan_ruj, lainlain_ruj, catatandokterperujuk, update_time, update_loginpemakai_id', 'safe'),
                    
                        array('create_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update,insert'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
//                        array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
                    
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pasiendirujukkeluar_id, pasienadmisi_id, pendaftaran_id, pegawai_id, pasien_id, rujukankeluar_id, nosuratrujukan, tgldirujuk, ythdokter, dirujukkebagian, alasandirujuk, hasilpemeriksaan_ruj, diagnosasementara_ruj, pengobatan_ruj, lainlain_ruj, catatandokterperujuk, ruanganasal_id, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'pendaftaran'=>array(self::BELONGS_TO,'PendaftaranT','pendaftaran_id'),
                    'rujukankeluar'=>array(self::BELONGS_TO, 'RujukankeluarM', 'rujukankeluar_id'),
                    'pegawai'=>array(self::BELONGS_TO, 'PegawaiM', 'pegawai_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pasiendirujukkeluar_id' => 'Pasiendirujukkeluar',
			'pasienadmisi_id' => 'Pasienadmisi',
			'pendaftaran_id' => 'Pendaftaran',
			'pegawai_id' => 'Dokter',
			'pasien_id' => 'Pasien',
			'rujukankeluar_id' => 'Dirujuk Ke',
			'nosuratrujukan' => 'No Surat Rujukan',
			'tgldirujuk' => 'Tgl Dirujuk',
			'ythdokter' => 'Dokter Tujuan',
			'dirujukkebagian' => 'Dirujuk Ke Bagian',
			'alasandirujuk' => 'Alasan Dirujuk',
			'hasilpemeriksaan_ruj' => 'Hasil Pemeriksaan',
			'diagnosasementara_ruj' => 'Diagnosa Sementara',
			'pengobatan_ruj' => 'Pengobatan',
			'lainlain_ruj' => 'Lain-lain',
			'catatandokterperujuk' => 'Catatan Dokter',
			'ruanganasal_id' => 'Ruangan Asal',
                    
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
//			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pasiendirujukkeluar_id',$this->pasiendirujukkeluar_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('rujukankeluar_id',$this->rujukankeluar_id);
		$criteria->compare('LOWER(nosuratrujukan)',strtolower($this->nosuratrujukan),true);
		$criteria->compare('LOWER(tgldirujuk)',strtolower($this->tgldirujuk),true);
		$criteria->compare('LOWER(ythdokter)',strtolower($this->ythdokter),true);
		$criteria->compare('LOWER(dirujukkebagian)',strtolower($this->dirujukkebagian),true);
		$criteria->compare('LOWER(alasandirujuk)',strtolower($this->alasandirujuk),true);
		$criteria->compare('LOWER(hasilpemeriksaan_ruj)',strtolower($this->hasilpemeriksaan_ruj),true);
		$criteria->compare('LOWER(diagnosasementara_ruj)',strtolower($this->diagnosasementara_ruj),true);
		$criteria->compare('LOWER(pengobatan_ruj)',strtolower($this->pengobatan_ruj),true);
		$criteria->compare('LOWER(lainlain_ruj)',strtolower($this->lainlain_ruj),true);
		$criteria->compare('LOWER(catatandokterperujuk)',strtolower($this->catatandokterperujuk),true);
		$criteria->compare('LOWER(ruanganasal_id)',strtolower($this->ruanganasal_id),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
//		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pasiendirujukkeluar_id',$this->pasiendirujukkeluar_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('rujukankeluar_id',$this->rujukankeluar_id);
		$criteria->compare('LOWER(nosuratrujukan)',strtolower($this->nosuratrujukan),true);
		$criteria->compare('LOWER(tgldirujuk)',strtolower($this->tgldirujuk),true);
		$criteria->compare('LOWER(ythdokter)',strtolower($this->ythdokter),true);
		$criteria->compare('LOWER(dirujukkebagian)',strtolower($this->dirujukkebagian),true);
		$criteria->compare('LOWER(alasandirujuk)',strtolower($this->alasandirujuk),true);
		$criteria->compare('LOWER(hasilpemeriksaan_ruj)',strtolower($this->hasilpemeriksaan_ruj),true);
		$criteria->compare('LOWER(diagnosasementara_ruj)',strtolower($this->diagnosasementara_ruj),true);
		$criteria->compare('LOWER(pengobatan_ruj)',strtolower($this->pengobatan_ruj),true);
		$criteria->compare('LOWER(lainlain_ruj)',strtolower($this->lainlain_ruj),true);
		$criteria->compare('LOWER(catatandokterperujuk)',strtolower($this->catatandokterperujuk),true);
		$criteria->compare('LOWER(ruanganasal_id)',strtolower($this->ruanganasal_id),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
//		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date'){
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                    }elseif ($column->dbType == 'timestamp without time zone'){
                            //$this->$columnName = date('Y-m-d H:i:s', CDateTimeParser::parse($this->$columnName, Yii::app()->locale->dateFormat));
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                    }
            }

            return parent::beforeValidate ();
        }

        public function beforeSave() {          
            return parent::beforeSave();
        }
                
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                        }
            }
            return true;
        }
        
        /**
         * Mengambil daftar semua rujukankeluar
         * @return CActiveDataProvider 
         */
        public function getRujukanItems()
        {
            return RujukankeluarM::model()->findAllByAttributes(array('rujukankeluar_aktif'=>true),array('order'=>'rumahsakitrujukan'));
        }
        
        /**
         * Mengambil daftar semua dokter ruangan
         * @return CActiveDataProvider 
         */
        public function getDokterItems()
        {
            return DokterV::model()->findAllByAttributes(array('ruangan_id'=>Yii::app()->user->getState('ruangan_id')),array('order'=>'nama_pegawai'));
        }
        
        /**
         * Mengambil daftar semua ruangan dari instalasi
         * @return CActiveDataProvider 
         */
        public function getRuanganInstalasiItems($idInstalasi)
        {
            if(!empty($idInstalasi))
                return RuanganM::model()->findAllByAttributes(array('instalasi_id'=>$idInstalasi,'ruangan_aktif'=>true),array('order'=>'ruangan_nama'));
            else
                return RuanganM::model()->findAllByAttributes(array('ruangan_id'=>$this->ruanganasal_id));
        }
    
        public function getDiagnosaSementara($idPendaftaran)
        {
            $modMorbiditas = PasienmorbiditasT::model()->with('diagnosa')->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            $diagnosa = '';
            foreach ($modMorbiditas as $i => $morbiditas) {
                $diagnosa .= $morbiditas->diagnosa->diagnosa_nama.', ';
            }

            return $diagnosa;
        }
        
}
