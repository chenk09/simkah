<?php

/**
 * This is the model class for table "penjualanbrgdetail_t".
 *
 * The followings are the available columns in table 'penjualanbrgdetail_t':
 * @property integer $penjualanbrgdetail_id
 * @property integer $penjualanbarang_id
 * @property integer $barang_id
 * @property integer $jmljual
 * @property string $satuanjual
 * @property double $harganetto
 * @property double $ppn
 * @property double $disc
 * @property double $hpp
 * @property double $hargajual
 * @property string $catatan
 */
class PenjualanbrgdetailT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PenjualanbrgdetailT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penjualanbrgdetail_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penjualanbarang_id, barang_id, jmljual, satuanjual, harganetto, ppn, disc, hpp, hargajual', 'required'),
			array('penjualanbarang_id, barang_id, jmljual', 'numerical', 'integerOnly'=>true),
			array('harganetto, ppn, disc, hpp, hargajual', 'numerical'),
			array('satuanjual', 'length', 'max'=>50),
			array('catatan', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penjualanbrgdetail_id, penjualanbarang_id, barang_id, jmljual, satuanjual, harganetto, ppn, disc, hpp, hargajual, catatan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'barang'=>array(self::BELONGS_TO, 'BarangM', 'barang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penjualanbrgdetail_id' => 'Penjualanbrgdetail',
			'penjualanbarang_id' => 'Penjualanbarang',
			'barang_id' => 'Barang',
			'jmljual' => 'Jmljual',
			'satuanjual' => 'Satuanjual',
			'harganetto' => 'Harganetto',
			'ppn' => 'Ppn',
			'disc' => 'Disc',
			'hpp' => 'Hpp',
			'hargajual' => 'Hargajual',
			'catatan' => 'Catatan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penjualanbrgdetail_id',$this->penjualanbrgdetail_id);
		$criteria->compare('penjualanbarang_id',$this->penjualanbarang_id);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('jmljual',$this->jmljual);
		$criteria->compare('LOWER(satuanjual)',strtolower($this->satuanjual),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('ppn',$this->ppn);
		$criteria->compare('disc',$this->disc);
		$criteria->compare('hpp',$this->hpp);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penjualanbrgdetail_id',$this->penjualanbrgdetail_id);
		$criteria->compare('penjualanbarang_id',$this->penjualanbarang_id);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('jmljual',$this->jmljual);
		$criteria->compare('LOWER(satuanjual)',strtolower($this->satuanjual),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('ppn',$this->ppn);
		$criteria->compare('disc',$this->disc);
		$criteria->compare('hpp',$this->hpp);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}