<?php

/**
 * This is the model class for table "peminjamanrm_t".
 *
 * The followings are the available columns in table 'peminjamanrm_t':
 * @property integer $peminjamanrm_id
 * @property integer $pengirimanrm_id
 * @property integer $dokrekammedis_id
 * @property integer $pasien_id
 * @property integer $pendaftaran_id
 * @property integer $kembalirm_id
 * @property integer $ruangan_id
 * @property string $nourut_pinjam
 * @property string $tglpeminjamanrm
 * @property string $untukkepentingan
 * @property string $keteranganpeminjaman
 * @property string $tglakandikembalikan
 * @property string $namapeminjam
 * @property boolean $printpeminjaman
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class PeminjamanrmT extends CActiveRecord
{
        public $no_rekam_medik;
        public $nama_pasien;
        public $jenis_kelamin;
        public $tanggal_lahir;
        public $printArray;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PeminjamanrmT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'peminjamanrm_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dokrekammedis_id, pasien_id, ruangan_id, nourut_pinjam, tglpeminjamanrm', 'required'),
			array('pengirimanrm_id, dokrekammedis_id, pasien_id, pendaftaran_id, kembalirm_id, ruangan_id', 'numerical', 'integerOnly'=>true),
			array('nourut_pinjam', 'length', 'max'=>5),
			array('untukkepentingan', 'length', 'max'=>50),
			array('namapeminjam', 'length', 'max'=>100),
			array('printArray, keteranganpeminjaman, tglakandikembalikan, printpeminjaman, update_time, update_loginpemakai_id', 'safe'),
                    
                        array('create_time','default','value'=>date( 'Y-m-d H:i:s', time()),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s', time()),'setOnEmpty'=>false,'on'=>'update,insert'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('printArray, peminjamanrm_id, pengirimanrm_id, dokrekammedis_id, pasien_id, pendaftaran_id, kembalirm_id, ruangan_id, nourut_pinjam, tglpeminjamanrm, untukkepentingan, keteranganpeminjaman, tglakandikembalikan, namapeminjam, printpeminjaman, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'dokrekammedis'=>array(self::BELONGS_TO, 'DokrekammedisM', 'dokrekammedis_id'),
                    'pasien'=>array(self::BELONGS_TO, 'PasienM', 'pasien_id'),
                    'pendaftaran'=>array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
                    'ruangantujuan'=>array(self::BELONGS_TO,'RuanganM', 'ruangan_id'),
                    'kembali'=>array(self::BELONGS_TO, 'KembalirmT', 'kembalirm_id'),
                    'pengiriman'=>array(self::BELONGS_TO, 'PengirimanrmT', 'pengirimanrm_id'),
                    'warnadokrm'=>array(self::HAS_ONE, 'WarnadokrmM', array('warnadokrm_id'=>'warnadokrm_id'), 'through'=>'dokrekammedis'),
                    //'lokasirak'=>array(self::HAS_ONE, 'LokasirakM', array('lokasirak_id'=>'dokrekammedis.lokasirak_id'), 'through'=>'warnadokrm'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'peminjamanrm_id' => 'Peminjaman RM',
			'pengirimanrm_id' => 'Pengiriman RM',
			'dokrekammedis_id' => 'Dokumen RM',
			'pasien_id' => 'Pasien',
			'pendaftaran_id' => 'Pendaftaran',
			'kembalirm_id' => 'Kembali RM',
			'ruangan_id' => 'Ruangan',
			'nourut_pinjam' => 'Nourut Pinjam',
			'tglpeminjamanrm' => 'Tanggal Peminjaman RM',
			'untukkepentingan' => 'Untuk Kepentingan',
			'keteranganpeminjaman' => 'Keterangan Peminjaman',
			'tglakandikembalikan' => 'Tanggal akan Dikembalikan',
			'namapeminjam' => 'Nama Peminjam',
			'printpeminjaman' => 'Sudah di Print',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('peminjamanrm_id',$this->peminjamanrm_id);
		$criteria->compare('pengirimanrm_id',$this->pengirimanrm_id);
		$criteria->compare('dokrekammedis_id',$this->dokrekammedis_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('kembalirm_id',$this->kembalirm_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(nourut_pinjam)',strtolower($this->nourut_pinjam),true);
		$criteria->compare('LOWER(tglpeminjamanrm)',strtolower($this->tglpeminjamanrm),true);
		$criteria->compare('LOWER(untukkepentingan)',strtolower($this->untukkepentingan),true);
		$criteria->compare('LOWER(keteranganpeminjaman)',strtolower($this->keteranganpeminjaman),true);
		$criteria->compare('LOWER(tglakandikembalikan)',strtolower($this->tglakandikembalikan),true);
		$criteria->compare('LOWER(namapeminjam)',strtolower($this->namapeminjam),true);
		$criteria->compare('printpeminjaman',$this->printpeminjaman);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('peminjamanrm_id',$this->peminjamanrm_id);
		$criteria->compare('pengirimanrm_id',$this->pengirimanrm_id);
		$criteria->compare('dokrekammedis_id',$this->dokrekammedis_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('kembalirm_id',$this->kembalirm_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(nourut_pinjam)',strtolower($this->nourut_pinjam),true);
		$criteria->compare('LOWER(tglpeminjamanrm)',strtolower($this->tglpeminjamanrm),true);
		$criteria->compare('LOWER(untukkepentingan)',strtolower($this->untukkepentingan),true);
		$criteria->compare('LOWER(keteranganpeminjaman)',strtolower($this->keteranganpeminjaman),true);
		$criteria->compare('LOWER(tglakandikembalikan)',strtolower($this->tglakandikembalikan),true);
		$criteria->compare('LOWER(namapeminjam)',strtolower($this->namapeminjam),true);
		$criteria->compare('printpeminjaman',$this->printpeminjaman);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date')
                        {
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                        }
                     else if ($column->dbType == 'timestamp without time zone')
                        {
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                        }    
            }

            return parent::beforeValidate ();
        }

        public function beforeSave() {         
            if($this->tglpeminjamanrm===null || trim($this->tglpeminjamanrm)==''){
	        $this->setAttribute('tglpeminjamanrm', null);
            }
            if($this->tglakandikembalikan===null || trim($this->tglakandikembalikan)==''){
	        $this->setAttribute('tglakandikembalikan', null);
            }
            
            return parent::beforeSave();
        }

        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                        }
            }
            return true;
        }
}