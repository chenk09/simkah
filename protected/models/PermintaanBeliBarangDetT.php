<?php

/**
 * This is the model class for table "permintaan_beli_barang_det_t".
 *
 * The followings are the available columns in table 'permintaan_beli_barang_det_t':
 * @property integer $permintaanbelibarang_det_id
 * @property integer $permintaanbelibarang_id
 * @property integer $barang_id
 * @property string $satuanbarang
 * @property double $jml_permintaan
 * @property string $created
 * @property string $create_time
 * @property string $updated
 * @property string $update_time
 */
class PermintaanBeliBarangDetT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PermintaanBeliBarangDetT the static model class
	 */
	public $nama_barang;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'permintaan_beli_barang_det_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('permintaanbelibarang_id, barang_id', 'required'),
			array('permintaanbelibarang_id, barang_id', 'numerical', 'integerOnly'=>true),
			array('jml_permintaan', 'numerical'),
			array('satuanbarang', 'length', 'max'=>50),
			array('created, updated', 'length', 'max'=>16),
			array('create_time, update_time', 'safe'),

			array('create_time', 'default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
			array('update_time', 'default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
			array('created','default','value'=>Yii::app()->user->id,'on'=>'insert'),
			array('updated','default','value'=>Yii::app()->user->id,'on'=>'update'),


			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('permintaanbelibarang_det_id, permintaanbelibarang_id, barang_id, satuanbarang, jml_permintaan, created, create_time, updated, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'barang'=>array(self::BELONGS_TO, 'BarangM', 'barang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'permintaanbelibarang_det_id' => 'Permintaanbelibarang Det',
			'permintaanbelibarang_id' => 'Permintaanbelibarang',
			'barang_id' => 'Barang',
			'satuanbarang' => 'Satuanbarang',
			'jml_permintaan' => 'Jml Permintaan',
			'created' => 'Created',
			'create_time' => 'Create Time',
			'updated' => 'Updated',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('permintaanbelibarang_det_id',$this->permintaanbelibarang_det_id);
		$criteria->compare('permintaanbelibarang_id',$this->permintaanbelibarang_id);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('satuanbarang',$this->satuanbarang,true);
		$criteria->compare('jml_permintaan',$this->jml_permintaan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}