<?php

/**
 * This is the model class for table "alatmedis_m".
 *
 * The followings are the available columns in table 'alatmedis_m':
 * @property integer $alatmedis_id
 * @property integer $instalasi_id
 * @property integer $jenisalatmedis_id
 * @property string $alatmedis_nama
 * @property string $alatmedis_namalain
 * @property boolean $alatmedis_aktif
 */
class AlatmedisM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AlatmedisM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'alatmedis_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('instalasi_id, jenisalatmedis_id, alatmedis_nama', 'required'),
			array('instalasi_id, jenisalatmedis_id', 'numerical', 'integerOnly'=>true),
			array('alatmedis_nama, alatmedis_namalain', 'length', 'max'=>100),
			array('alatmedis_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('alatmedis_id, instalasi_id, jenisalatmedis_id, alatmedis_nama, alatmedis_namalain, alatmedis_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'jenisalatmedis' => array(self::BELONGS_TO, 'JenisalatmedisM', 'jenisalatmedis_id'),
                    'instalasi'=>array(self::BELONGS_TO,'InstalasiM','instalasi_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'alatmedis_id' => 'Id Alat Medis',
			'instalasi_id' => ' Instalasi',
			'jenisalatmedis_id' => 'Jenis Alat Medis',
			'alatmedis_nama' => 'Nama Alat Medis',
			'alatmedis_namalain' => 'Nama Lainnya',
			'alatmedis_aktif' => 'Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

        	$criteria->compare('alatmedis_id',$this->alatmedis_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('jenisalatmedis_id',$this->jenisalatmedis_id);
		$criteria->compare('LOWER(alatmedis_nama)',strtolower($this->alatmedis_nama),true);
		$criteria->compare('LOWER(alatmedis_namalain)',strtolower($this->alatmedis_namalain),true);
		$criteria->compare('alatmedis_aktif',isset($this->alatmedis_aktif)?$this->alatmedis_aktif:true);
//                $criteria->addCondition('alatmedis_aktif is true');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

            
                $criteria=new CDbCriteria;
		$criteria->compare('alatmedis_id',$this->alatmedis_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('jenisalatmedis_id',$this->jenisalatmedis_id);
		$criteria->compare('LOWER(alatmedis_nama)',strtolower($this->alatmedis_nama),true);
		$criteria->compare('LOWER(alatmedis_namalain)',strtolower($this->alatmedis_namalain),true);
		//$criteria->compare('alatmedis_aktif',$this->alatmedis_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }

        public function getJenisAlatMedisItems()
        {
            return JenisalatmedisM::model()->findAll('jenisalatmedis_aktif=TRUE ORDER BY jenisalatmedis_nama');
        }
        
        public function getInstalasiItems()
        {
            return InstalasiM::model()->findAll('instalasi_aktif=TRUE ORDER BY instalasi_nama');
        }

}