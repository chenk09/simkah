<?php

/**
 * This is the model class for table "invperalatan_t".
 *
 * The followings are the available columns in table 'invperalatan_t':
 * @property integer $invperalatan_id
 * @property integer $lokasi_id
 * @property integer $barang_id
 * @property integer $asalaset_id
 * @property integer $pemilikbarang_id
 * @property string $invperalatan_kode
 * @property string $invperalatan_noregister
 * @property string $invperalatan_namabrg
 * @property string $invperalatan_merk
 * @property string $invperalatan_ukuran
 * @property string $invperalatan_bahan
 * @property string $invperalatan_thnpembelian
 * @property string $invperalatan_tglguna
 * @property string $invperalatan_nopabrik
 * @property string $invperalatan_norangka
 * @property string $invperalatan_nomesin
 * @property string $invperalatan_nopolisi
 * @property string $invperalatan_nobpkb
 * @property double $invperalatan_harga
 * @property double $invperalatan_akumsusut
 * @property string $invperalatan_ket
 * @property string $invperalatan_kapasitasrata
 * @property boolean $invperalatan_ijinoperasional
 * @property string $invperalatan_serftkkalibrasi
 * @property integer $invperalatan_umurekonomis
 * @property string $invperalatan_keadaan
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 * @property integer $terimapersdetail_id
 * @property double $invperalatan_nilairesidu
 * @property integer $umurekonomis
 * @property string $tglpenghapusan
 * @property string $tipepenghapusan
 * @property double $hargajualaktiva
 * @property double $kerugian
 * @property double $keuntungan
 */
class InvperalatanT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InvperalatanT the static model class
	 */
	public $tglAwal, $tglAkhir, $bbnpenyusutanbrjlnper, $tglpenyusutanperalatan;
	public $barang_nama, $lokasiaset_namalokasi, $pemilikbarang_nama, $asalaset_nama, $id_pemilik;
	public $is_checked, $barang_kode;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invperalatan_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('barang_id, pemilikbarang_id, invperalatan_kode, invperalatan_noregister, invperalatan_namabrg, invperalatan_umurekonomis, invperalatan_keadaan, umurekonomis, invperalatan_harga', 'required'),
			array('lokasi_id, barang_id, asalaset_id, pemilikbarang_id, invperalatan_umurekonomis, terimapersdetail_id, umurekonomis', 'numerical', 'integerOnly'=>true),
			array('invperalatan_harga, invperalatan_akumsusut, invperalatan_nilairesidu, hargajualaktiva, kerugian, keuntungan', 'numerical'),
			array('invperalatan_kode, invperalatan_noregister, invperalatan_merk, invperalatan_ukuran, invperalatan_nopabrik, invperalatan_norangka, invperalatan_nomesin, invperalatan_nopolisi, invperalatan_nobpkb, invperalatan_keadaan', 'length', 'max'=>50),
			array('invperalatan_namabrg, invperalatan_bahan', 'length', 'max'=>100),
			array('invperalatan_thnpembelian', 'length', 'max'=>5),
			array('invperalatan_kapasitasrata', 'length', 'max'=>10),
			array('invperalatan_serftkkalibrasi', 'length', 'max'=>20),
			array('tipepenghapusan', 'length', 'max'=>25),
			array('invperalatan_tglguna, invperalatan_ket, invperalatan_ijinoperasional, update_time, update_loginpemakai_id, tglpenghapusan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
            array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
            array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
            array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
            array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
			
			array('invperalatan_id, lokasi_id, barang_id, asalaset_id, pemilikbarang_id, invperalatan_kode, invperalatan_noregister, invperalatan_namabrg, invperalatan_merk, invperalatan_ukuran, invperalatan_bahan, invperalatan_thnpembelian, invperalatan_tglguna, invperalatan_nopabrik, invperalatan_norangka, invperalatan_nomesin, invperalatan_nopolisi, invperalatan_nobpkb, invperalatan_harga, invperalatan_akumsusut, invperalatan_ket, invperalatan_kapasitasrata, invperalatan_ijinoperasional, invperalatan_serftkkalibrasi, invperalatan_umurekonomis, invperalatan_keadaan, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan, terimapersdetail_id, invperalatan_nilairesidu, umurekonomis, tglpenghapusan, tipepenghapusan, hargajualaktiva, kerugian, keuntungan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pemilik' => array(self::BELONGS_TO, 'PemilikbarangM', 'pemilikbarang_id'),
            'barang'=>array(self::BELONGS_TO,'BarangM','barang_id'),
            'lokasi' => array(self::BELONGS_TO, 'LokasiasetM', 'lokasi_id'),
            'asal'=>array(self::BELONGS_TO,'AsalasetM','asalaset_id'),
            'terimapers'=>array(self::BELONGS_TO,'TerimapesdetailT','terimapersdetail_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'invperalatan_id' => 'ID ',
			'lokasi_id' => 'Lokasi',
			'barang_id' => 'Barang',
			'asalaset_id' => 'Asal Aset',
			'pemilikbarang_id' => 'Pemilik Barang',
			'invperalatan_kode' => 'Kode',
			'invperalatan_noregister' => 'No Register',
			'invperalatan_namabrg' => ' Nama Barang',
			'invperalatan_merk' => 'Merk',
			'invperalatan_ukuran' => 'Ukuran',
			'invperalatan_bahan' => 'Bahan',
			'invperalatan_thnpembelian' => 'Tahun Pembelian',
			'invperalatan_tglguna' => 'Tanggal Pengunaan',
			'invperalatan_nopabrik' => 'Nomer Pabrik',
			'invperalatan_norangka' => 'Nomer Rangka',
			'invperalatan_nomesin' => 'Nomer Mesin',
			'invperalatan_nopolisi' => 'Nomer Polisi',
			'invperalatan_nobpkb' => 'Nomer BPKB',
			'invperalatan_harga' => 'Harga',
			'invperalatan_akumsusut' => 'Akum Susut',
			'invperalatan_ket' => 'Keterangan',
			'invperalatan_kapasitasrata' => 'Kapasitas Rata',
			'invperalatan_ijinoperasional' => 'Izin Operasional',
			'invperalatan_serftkkalibrasi' => 'Sertifikat Kalibrasi',
			'invperalatan_umurekonomis' => 'Umur Ekonomis',
			'umurekonomis' => 'Umur Ekonomis',
			'invperalatan_keadaan' => 'Keadaan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
                        'terimapersdetail_id'=>'Terima Persediaan',
            'invperalatan_nilairesidu'=>'Nilai Residu', 
            'tglpenyusutanperalatan'=>'Tanggal Penyusutan',
            'lokasiaset_namalokasi'=>'Lokasi Aset',
            'pemilikbarang_nama'=>'Pemilik Barang',
            'asalaset_nama'=>'Asal Aset',
			'umurekonomis' => 'Umur ekonomis',
			'tglpenghapusan' => 'Tgl penghapusan',
			'tipepenghapusan' => 'Tipe penghapusan',
			'hargajualaktiva' => 'Harga jual aktiva',
			'kerugian' => 'Kerugian',
			'keuntungan' => 'Keuntungan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->addBetweenCondition('date(t.create_time)', $this->tglAwal, $this->tglAkhir);
		// $criteria->select = 't.*, barang_m.*, asalaset_m.*, lokasiaset_m.*, pemilikbarang_m.*';
		$criteria->compare('invperalatan_id',$this->invperalatan_id);
		$criteria->compare('lokasi_id',$this->lokasi_id);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('asalaset_id',$this->asalaset_id);
		$criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
		$criteria->compare('LOWER(invperalatan_kode)',strtolower($this->invperalatan_kode),true);
		$criteria->compare('LOWER(invperalatan_noregister)',strtolower($this->invperalatan_noregister),true);
		$criteria->compare('LOWER(invperalatan_namabrg)',strtolower($this->invperalatan_namabrg),true);
		$criteria->compare('LOWER(invperalatan_merk)',strtolower($this->invperalatan_merk),true);
		$criteria->compare('LOWER(invperalatan_ukuran)',strtolower($this->invperalatan_ukuran),true);
		$criteria->compare('LOWER(invperalatan_bahan)',strtolower($this->invperalatan_bahan),true);
		$criteria->compare('LOWER(invperalatan_thnpembelian)',strtolower($this->invperalatan_thnpembelian),true);
		$criteria->compare('LOWER(invperalatan_tglguna)',strtolower($this->invperalatan_tglguna),true);
		$criteria->compare('LOWER(invperalatan_nopabrik)',strtolower($this->invperalatan_nopabrik),true);
		$criteria->compare('LOWER(invperalatan_norangka)',strtolower($this->invperalatan_norangka),true);
		$criteria->compare('LOWER(invperalatan_nomesin)',strtolower($this->invperalatan_nomesin),true);
		$criteria->compare('LOWER(invperalatan_nopolisi)',strtolower($this->invperalatan_nopolisi),true);
		$criteria->compare('LOWER(invperalatan_nobpkb)',strtolower($this->invperalatan_nobpkb),true);
		$criteria->compare('invperalatan_harga',$this->invperalatan_harga);
		$criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
		$criteria->compare('LOWER(invperalatan_ket)',strtolower($this->invperalatan_ket),true);
		$criteria->compare('LOWER(invperalatan_kapasitasrata)',strtolower($this->invperalatan_kapasitasrata),true);
		$criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
		$criteria->compare('LOWER(invperalatan_serftkkalibrasi)',strtolower($this->invperalatan_serftkkalibrasi),true);
		$criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
		$criteria->compare('LOWER(invperalatan_keadaan)',strtolower($this->invperalatan_keadaan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->compare('terimapersdetail_id',$this->terimapersdetail_id);
		$criteria->compare('umurekonomis',$this->umurekonomis);
		$criteria->compare('LOWER(tglpenghapusan)',strtolower($this->tglpenghapusan),true);
		$criteria->compare('LOWER(tipepenghapusan)',strtolower($this->tipepenghapusan),true);
		$criteria->compare('hargajualaktiva',$this->hargajualaktiva);
		$criteria->compare('kerugian',$this->kerugian);
		$criteria->compare('keuntungan',$this->keuntungan);
        // $criteria->join = 'LEFT JOIN barang_m on barang_m.barang_id = t.barang_id 
        // 	LEFT JOIN asalaset_m on asalaset_m.asalaset_id = t.asalaset_id
        //    	LEFT JOIN lokasiaset_m on lokasiaset_m.lokasi_id = t.lokasi_id
        //    	LEFT JOIN pemilikbarang_m on pemilikbarang_m.pemilikbarang_id = t.pemilikbarang_id';
           	$criteria->addCondition("t.tipepenghapusan is null AND split_part(t.invperalatan_noregister, '-',3)='04'");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('invperalatan_id',$this->invperalatan_id);
		$criteria->compare('lokasi_id',$this->lokasi_id);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('asalaset_id',$this->asalaset_id);
		$criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
		$criteria->compare('LOWER(invperalatan_kode)',strtolower($this->invperalatan_kode),true);
		$criteria->compare('LOWER(invperalatan_noregister)',strtolower($this->invperalatan_noregister),true);
		$criteria->compare('LOWER(invperalatan_namabrg)',strtolower($this->invperalatan_namabrg),true);
		$criteria->compare('LOWER(invperalatan_merk)',strtolower($this->invperalatan_merk),true);
		$criteria->compare('LOWER(invperalatan_ukuran)',strtolower($this->invperalatan_ukuran),true);
		$criteria->compare('LOWER(invperalatan_bahan)',strtolower($this->invperalatan_bahan),true);
		$criteria->compare('LOWER(invperalatan_thnpembelian)',strtolower($this->invperalatan_thnpembelian),true);
		$criteria->compare('LOWER(invperalatan_tglguna)',strtolower($this->invperalatan_tglguna),true);
		$criteria->compare('LOWER(invperalatan_nopabrik)',strtolower($this->invperalatan_nopabrik),true);
		$criteria->compare('LOWER(invperalatan_norangka)',strtolower($this->invperalatan_norangka),true);
		$criteria->compare('LOWER(invperalatan_nomesin)',strtolower($this->invperalatan_nomesin),true);
		$criteria->compare('LOWER(invperalatan_nopolisi)',strtolower($this->invperalatan_nopolisi),true);
		$criteria->compare('LOWER(invperalatan_nobpkb)',strtolower($this->invperalatan_nobpkb),true);
		$criteria->compare('invperalatan_harga',$this->invperalatan_harga);
		$criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
		$criteria->compare('LOWER(invperalatan_ket)',strtolower($this->invperalatan_ket),true);
		$criteria->compare('LOWER(invperalatan_kapasitasrata)',strtolower($this->invperalatan_kapasitasrata),true);
		$criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
		$criteria->compare('LOWER(invperalatan_serftkkalibrasi)',strtolower($this->invperalatan_serftkkalibrasi),true);
		$criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
		$criteria->compare('LOWER(invperalatan_keadaan)',strtolower($this->invperalatan_keadaan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('terimapersdetail_id',$this->terimapersdetail_id);
		$criteria->compare('invperalatan_nilairesidu',$this->invperalatan_nilairesidu);
		$criteria->compare('umurekonomis',$this->umurekonomis);
		$criteria->compare('LOWER(tglpenghapusan)',strtolower($this->tglpenghapusan),true);
		$criteria->compare('LOWER(tipepenghapusan)',strtolower($this->tipepenghapusan),true);
		$criteria->compare('hargajualaktiva',$this->hargajualaktiva);
		$criteria->compare('kerugian',$this->kerugian);
		$criteria->compare('keuntungan',$this->keuntungan);
		$criteria->addCondition('tipepenghapusan is null');
		$criteria->addCondition("t.tipepenghapusan is null AND split_part(t.invperalatan_noregister, '-',3)='05'");
//		$criteria->addBetweenCondition('date(t.create_time)', $this->tglAwal, $this->tglAkhir);
		// Klo limit lebih kecil dari nol itu berarti ga ada limit 
		$criteria->limit=-1; 

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination'=>false,
		));
        }

    public function getBarangItems()
        {
            return BarangM::model()->findAll(array('order'=>'barang_nama'));
        }
                public function getPemilikItems()
        {
            return PemilikbarangM::model()->findAll(array('order'=>'pemilikbarang_nama'));
        }
        public function getAsalAsetItems()
        {
            return AsalasetM::model()->findAll(array('order'=>'asalaset_nama'));
        }
                public function getLokasiAsetItems()
        {
            return LokasiasetM::model()->findAll(array('order'=>'lokasiaset_namalokasi'));
        }
        
         protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date'){
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                    }elseif ($column->dbType == 'timestamp without time zone'){
                            //$this->$columnName = date('Y-m-d H:i:s', CDateTimeParser::parse($this->$columnName, Yii::app()->locale->dateFormat));
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                    }
            }

            return parent::beforeValidate ();
        }

        protected function beforeSave() {  
            if($this->invperalatan_tglguna===null || trim($this->invperalatan_tglguna)==''){
	        $this->setAttribute('invperalatan_tglguna', null);
            }
            return parent::beforeSave();
        }
                
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                        }
            }
            return true;
        }

    public function searchMutasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->select = 't.*, barang_m.*, asalaset_m.*, lokasiaset_m.*, pemilikbarang_m.*';
		$criteria->compare('t.invperalatan_id',$this->invperalatan_id);
		$criteria->compare('t.lokasi_id',$this->lokasi_id);
		$criteria->compare('t.barang_id',$this->barang_id);
		$criteria->compare('t.asalaset_id',$this->asalaset_id);
		$criteria->compare('t.pemilikbarang_id',$this->pemilikbarang_id);
		$criteria->compare('LOWER(t.invperalatan_kode)',strtolower($this->invperalatan_kode),true);
		$criteria->compare('LOWER(t.invperalatan_noregister)',strtolower($this->invperalatan_noregister),true);
		$criteria->compare('LOWER(t.invperalatan_namabrg)',strtolower($this->invperalatan_namabrg),true);
		$criteria->compare('LOWER(t.invperalatan_merk)',strtolower($this->invperalatan_merk),true);
		$criteria->compare('LOWER(t.invperalatan_ukuran)',strtolower($this->invperalatan_ukuran),true);
		$criteria->compare('LOWER(t.invperalatan_bahan)',strtolower($this->invperalatan_bahan),true);
		$criteria->compare('LOWER(t.invperalatan_thnpembelian)',strtolower($this->invperalatan_thnpembelian),true);
		$criteria->compare('LOWER(t.invperalatan_tglguna)',strtolower($this->invperalatan_tglguna),true);
		$criteria->compare('LOWER(t.invperalatan_nopabrik)',strtolower($this->invperalatan_nopabrik),true);
		$criteria->compare('LOWER(t.invperalatan_norangka)',strtolower($this->invperalatan_norangka),true);
		$criteria->compare('LOWER(t.invperalatan_nomesin)',strtolower($this->invperalatan_nomesin),true);
		$criteria->compare('LOWER(t.invperalatan_nopolisi)',strtolower($this->invperalatan_nopolisi),true);
		$criteria->compare('LOWER(t.invperalatan_nobpkb)',strtolower($this->invperalatan_nobpkb),true);
		$criteria->compare('t.invperalatan_harga',$this->invperalatan_harga);
		$criteria->compare('t.invperalatan_akumsusut',$this->invperalatan_akumsusut);
		$criteria->compare('LOWER(t.invperalatan_ket)',strtolower($this->invperalatan_ket),true);
		$criteria->compare('LOWER(t.invperalatan_kapasitasrata)',strtolower($this->invperalatan_kapasitasrata),true);
		$criteria->compare('t.invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
		$criteria->compare('LOWER(t.invperalatan_serftkkalibrasi)',strtolower($this->invperalatan_serftkkalibrasi),true);
		$criteria->compare('t.invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
		$criteria->compare('LOWER(t.invperalatan_keadaan)',strtolower($this->invperalatan_keadaan),true);
		$criteria->compare('LOWER(t.create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(t.update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(t.create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(t.update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(t.create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->compare('t.terimapersdetail_id',$this->terimapersdetail_id);
        $criteria->addCondition("t.invperalatan_ket !='Dimutasikan' AND tipepenghapusan is NULL");

        $criteria->join = 'LEFT JOIN barang_m on barang_m.barang_id = t.barang_id 
        	LEFT JOIN asalaset_m on asalaset_m.asalaset_id = t.asalaset_id
           	LEFT JOIN lokasiaset_m on lokasiaset_m.lokasi_id = t.lokasi_id
           	LEFT JOIN pemilikbarang_m on pemilikbarang_m.pemilikbarang_id = t.pemilikbarang_id';
        
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}