<?php

/**
 * This is the model class for table "laporanrekapitulasigaji_v".
 *
 * The followings are the available columns in table 'laporanrekapitulasigaji_v':
 * @property integer $pegawai_id
 * @property string $nomorindukpegawai
 * @property string $no_kartupegawainegerisipil
 * @property string $no_karis_karsu
 * @property string $no_taspen
 * @property string $no_askes
 * @property string $jenisidentitas
 * @property string $noidentitas
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property string $nama_keluarga
 * @property string $gelarbelakang_nama
 * @property string $tempatlahir_pegawai
 * @property string $tgl_lahirpegawai
 * @property string $jeniskelamin
 * @property string $statusperkawinan
 * @property string $alamat_pegawai
 * @property integer $kelurahan_id
 * @property string $kelurahan_nama
 * @property string $kode_pos
 * @property integer $kecamatan_id
 * @property string $kecamatan_nama
 * @property integer $kabupaten_id
 * @property string $kabupaten_nama
 * @property string $agama
 * @property string $golongandarah
 * @property string $rhesus
 * @property string $alamatemail
 * @property string $notelp_pegawai
 * @property string $nomobile_pegawai
 * @property string $warganegara_pegawai
 * @property string $jeniswaktukerja
 * @property integer $suku_id
 * @property string $suku_nama
 * @property integer $statuskepemilikanrumah_id
 * @property string $statuskepemilikanrumah_nama
 * @property integer $propinsi_id
 * @property string $propinsi_nama
 * @property integer $profilrs_id
 * @property string $nokode_rumahsakit
 * @property string $nama_rumahsakit
 * @property integer $instalasi_id
 * @property string $instalasi_nama
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 * @property string $kelompokjabatan
 * @property integer $golonganpegawai_id
 * @property string $golonganpegawai_nama
 * @property integer $pangkat_id
 * @property string $pangkat_nama
 * @property integer $jabatan_id
 * @property string $jabatan_nama
 * @property integer $esselon_id
 * @property string $esselon_nama
 * @property integer $kelompokpegawai_id
 * @property string $kelompokpegawai_nama
 * @property string $kategoripegawai
 * @property string $kategoripegawaiasal
 * @property string $photopegawai
 * @property string $nofingerprint
 * @property double $tinggibadan
 * @property double $beratbadan
 * @property string $kemampuanbahasa
 * @property string $warnakulit
 * @property string $nip_lama
 * @property string $kode_bank
 * @property string $no_rekening
 * @property string $npwp
 * @property string $tglditerima
 * @property string $tglberhenti
 * @property integer $penggajianpeg_id
 * @property string $tglpenggajian
 * @property string $nopenggajian
 * @property string $keterangan
 * @property string $mengetahui
 * @property string $menyetujui
 * @property double $totalterima
 * @property double $totalpajak
 * @property double $totalpotongan
 * @property double $penerimaanbersih
 * @property string $periodegaji
 * @property integer $penggajiankomp_id
 * @property integer $komponengaji_id
 * @property string $komponengaji_nama
 * @property double $jumlah
 * @property integer $pengeluaranumum_id
 * @property integer $jenispengeluaran_id
 * @property string $jenispengeluaran_kode
 * @property string $jenispengeluaran_nama
 * @property string $kelompoktransaksi
 * @property string $nopengeluaran
 * @property string $tglpengeluaran
 * @property double $volume
 * @property string $satuanvol
 * @property double $bank
 * @property double $tunai
 * @property string $statuspembayaran
 * @property integer $tandabuktikeluar_id
 * @property string $tahun
 * @property string $tglkaskeluar
 * @property string $nokaskeluar
 * @property string $carabayarkeluar
 * @property string $melaluibank
 * @property string $denganrekening
 * @property string $atasnamarekening
 * @property string $namapenerima
 * @property string $alamatpenerima
 * @property string $untukpembayaran
 * @property string $keterangan_pengeluaran
 * @property double $jmlkaskeluar
 * @property double $biayaadministrasi
 * @property string $keterangankeluar
 */
class LaporanrekapitulasigajiV extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LaporanrekapitulasigajiV the static model class
     */
    public $periodegaji_view, $pegawai, $print = false;
    public $itemPegawai = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'laporanrekapitulasigaji_v';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('pegawai_id, kelurahan_id,  kecamatan_id, kabupaten_id, suku_id, statuskepemilikanrumah_id, propinsi_id, profilrs_id, instalasi_id, ruangan_id, golonganpegawai_id, pangkat_id, jabatan_id, esselon_id, kelompokpegawai_id, penggajianpeg_id, penggajiankomp_id, komponengaji_id, pengeluaranumum_id, jenispengeluaran_id, tandabuktikeluar_id', 'numerical', 'integerOnly' => true),
            array('tinggibadan, beratbadan, totalterima, totalpajak, totalpotongan, penerimaanbersih, jumlah, volume, bank, tunai, jmlkaskeluar, biayaadministrasi', 'numerical'),
            array('nomorindukpegawai, no_kartupegawainegerisipil, no_karis_karsu, no_taspen, no_askes, tempatlahir_pegawai, kelompokjabatan, kelompokpegawai_nama', 'length', 'max' => 30),
            array('jenisidentitas, jeniskelamin, statusperkawinan, agama, rhesus, jeniswaktukerja, nofingerprint, jenispengeluaran_kode', 'length', 'max' => 20),
            array('noidentitas, alamatemail, nama_rumahsakit, jabatan_nama, kemampuanbahasa, nip_lama, kode_bank, no_rekening, mengetahui, menyetujui, komponengaji_nama, jenispengeluaran_nama, melaluibank, denganrekening, atasnamarekening, namapenerima, untukpembayaran', 'length', 'max' => 100),
            array('gelardepan, statuskepemilikanrumah_nama, nokode_rumahsakit', 'length', 'max' => 10),
            array('nama_pegawai, nama_keluarga, kelurahan_nama, kecamatan_nama, kabupaten_nama, notelp_pegawai, nomobile_pegawai, suku_nama, propinsi_nama, instalasi_nama, ruangan_nama, golonganpegawai_nama, pangkat_nama, kategoripegawaiasal, nopenggajian, kelompoktransaksi, nopengeluaran, satuanvol, nokaskeluar, carabayarkeluar', 'length', 'max' => 50),
            array('gelarbelakang_nama, kode_pos, esselon_nama', 'length', 'max' => 15),
            array('golongandarah', 'length', 'max' => 2),
            array('warganegara_pegawai, npwp, statuspembayaran', 'length', 'max' => 25),
            array('kategoripegawai', 'length', 'max' => 128),
            array('photopegawai', 'length', 'max' => 200),
            array('tahun', 'length', 'max' => 4),
            array('tgl_lahirpegawai, alamat_pegawai, warnakulit, tglditerima, tglberhenti, tglpenggajian, keterangan, periodegaji, tglpengeluaran, tglkaskeluar, alamatpenerima, keterangan_pengeluaran, keterangankeluar', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('pegawai_id, nomorindukpegawai, no_kartupegawainegerisipil, no_karis_karsu, no_taspen, no_askes, jenisidentitas, noidentitas, gelardepan, nama_pegawai, nama_keluarga, gelarbelakang_nama, tempatlahir_pegawai, tgl_lahirpegawai, jeniskelamin, statusperkawinan, alamat_pegawai, kelurahan_id, kelurahan_nama, kode_pos, kecamatan_id, kecamatan_nama, kabupaten_id, kabupaten_nama, agama, golongandarah, rhesus, alamatemail, notelp_pegawai, nomobile_pegawai, warganegara_pegawai, jeniswaktukerja, suku_id, suku_nama, statuskepemilikanrumah_id, statuskepemilikanrumah_nama, propinsi_id, propinsi_nama, profilrs_id, nokode_rumahsakit, nama_rumahsakit, instalasi_id, instalasi_nama, ruangan_id, ruangan_nama, kelompokjabatan, golonganpegawai_id, golonganpegawai_nama, pangkat_id, pangkat_nama, jabatan_id, jabatan_nama, esselon_id, esselon_nama, kelompokpegawai_id, kelompokpegawai_nama, kategoripegawai, kategoripegawaiasal, photopegawai, nofingerprint, tinggibadan, beratbadan, kemampuanbahasa, warnakulit, nip_lama, kode_bank, no_rekening, npwp, tglditerima, tglberhenti, penggajianpeg_id, tglpenggajian, nopenggajian, keterangan, mengetahui, menyetujui, totalterima, totalpajak, totalpotongan, penerimaanbersih, periodegaji, penggajiankomp_id, komponengaji_id, komponengaji_nama, jumlah, pengeluaranumum_id, jenispengeluaran_id, jenispengeluaran_kode, jenispengeluaran_nama, kelompoktransaksi, nopengeluaran, tglpengeluaran, volume, satuanvol, bank, tunai, statuspembayaran, tandabuktikeluar_id, tahun, tglkaskeluar, nokaskeluar, carabayarkeluar, melaluibank, denganrekening, atasnamarekening, namapenerima, alamatpenerima, untukpembayaran, keterangan_pengeluaran, jmlkaskeluar, biayaadministrasi, keterangankeluar', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'pegawai_id' => 'Pegawai',
            'nomorindukpegawai' => 'Nomorindukpegawai',
            'no_kartupegawainegerisipil' => 'No Kartupegawainegerisipil',
            'no_karis_karsu' => 'No Karis Karsu',
            'no_taspen' => 'No Taspen',
            'no_askes' => 'No Askes',
            'jenisidentitas' => 'Jenisidentitas',
            'noidentitas' => 'Noidentitas',
            'gelardepan' => 'Gelardepan',
            'nama_pegawai' => 'Nama Pegawai',
            'nama_keluarga' => 'Nama Keluarga',
            'gelarbelakang_nama' => 'Gelarbelakang Nama',
            'tempatlahir_pegawai' => 'Tempatlahir Pegawai',
            'tgl_lahirpegawai' => 'Tgl Lahirpegawai',
            'jeniskelamin' => 'Jeniskelamin',
            'statusperkawinan' => 'Statusperkawinan',
            'alamat_pegawai' => 'Alamat Pegawai',
            'kelurahan_id' => 'Kelurahan',
            'kelurahan_nama' => 'Kelurahan Nama',
            'kode_pos' => 'Kode Pos',
            'kecamatan_id' => 'Kecamatan',
            'kecamatan_nama' => 'Kecamatan Nama',
            'kabupaten_id' => 'Kabupaten',
            'kabupaten_nama' => 'Kabupaten Nama',
            'agama' => 'Agama',
            'golongandarah' => 'Golongandarah',
            'rhesus' => 'Rhesus',
            'alamatemail' => 'Alamatemail',
            'notelp_pegawai' => 'Notelp Pegawai',
            'nomobile_pegawai' => 'Nomobile Pegawai',
            'warganegara_pegawai' => 'Warganegara Pegawai',
            'jeniswaktukerja' => 'Jeniswaktukerja',
            'suku_id' => 'Suku',
            'suku_nama' => 'Suku Nama',
            'statuskepemilikanrumah_id' => 'Statuskepemilikanrumah',
            'statuskepemilikanrumah_nama' => 'Statuskepemilikanrumah Nama',
            'propinsi_id' => 'Propinsi',
            'propinsi_nama' => 'Propinsi Nama',
            'profilrs_id' => 'Profilrs',
            'nokode_rumahsakit' => 'Nokode Rumahsakit',
            'nama_rumahsakit' => 'Nama Rumahsakit',
            'instalasi_id' => 'Instalasi',
            'instalasi_nama' => 'Instalasi Nama',
            'ruangan_id' => 'Ruangan',
            'ruangan_nama' => 'Ruangan Nama',
            'kelompokjabatan' => 'Kelompokjabatan',
            'golonganpegawai_id' => 'Golonganpegawai',
            'golonganpegawai_nama' => 'Golonganpegawai Nama',
            'pangkat_id' => 'Pangkat',
            'pangkat_nama' => 'Pangkat Nama',
            'jabatan_id' => 'Jabatan',
            'jabatan_nama' => 'Jabatan Nama',
            'esselon_id' => 'Esselon',
            'esselon_nama' => 'Esselon Nama',
            'kelompokpegawai_id' => 'Kelompokpegawai',
            'kelompokpegawai_nama' => 'Kelompokpegawai Nama',
            'kategoripegawai' => 'Kategoripegawai',
            'kategoripegawaiasal' => 'Kategoripegawaiasal',
            'photopegawai' => 'Photopegawai',
            'nofingerprint' => 'Nofingerprint',
            'tinggibadan' => 'Tinggibadan',
            'beratbadan' => 'Beratbadan',
            'kemampuanbahasa' => 'Kemampuanbahasa',
            'warnakulit' => 'Warnakulit',
            'nip_lama' => 'Nip Lama',
            'kode_bank' => 'Kode Bank',
            'no_rekening' => 'No Rekening',
            'npwp' => 'Npwp',
            'tglditerima' => 'Tglditerima',
            'tglberhenti' => 'Tglberhenti',
            'penggajianpeg_id' => 'Penggajianpeg',
            'tglpenggajian' => 'Tglpenggajian',
            'nopenggajian' => 'Nopenggajian',
            'keterangan' => 'Keterangan',
            'mengetahui' => 'Mengetahui',
            'menyetujui' => 'Menyetujui',
            'totalterima' => 'Totalterima',
            'totalpajak' => 'Totalpajak',
            'totalpotongan' => 'Totalpotongan',
            'penerimaanbersih' => 'Penerimaanbersih',
            'periodegaji' => 'Periodegaji',
            'penggajiankomp_id' => 'Penggajiankomp',
            'komponengaji_id' => 'Komponengaji',
            'komponengaji_nama' => 'Komponengaji Nama',
            'jumlah' => 'Jumlah',
            'pengeluaranumum_id' => 'Pengeluaranumum',
            'jenispengeluaran_id' => 'Jenispengeluaran',
            'jenispengeluaran_kode' => 'Jenispengeluaran Kode',
            'jenispengeluaran_nama' => 'Jenispengeluaran Nama',
            'kelompoktransaksi' => 'Kelompoktransaksi',
            'nopengeluaran' => 'Nopengeluaran',
            'tglpengeluaran' => 'Tglpengeluaran',
            'volume' => 'Volume',
            'satuanvol' => 'Satuanvol',
            'bank' => 'Bank',
            'tunai' => 'Tunai',
            'statuspembayaran' => 'Statuspembayaran',
            'tandabuktikeluar_id' => 'Tandabuktikeluar',
            'tahun' => 'Tahun',
            'tglkaskeluar' => 'Tglkaskeluar',
            'nokaskeluar' => 'Nokaskeluar',
            'carabayarkeluar' => 'Carabayarkeluar',
            'melaluibank' => 'Melaluibank',
            'denganrekening' => 'Denganrekening',
            'atasnamarekening' => 'Atasnamarekening',
            'namapenerima' => 'Namapenerima',
            'alamatpenerima' => 'Alamatpenerima',
            'untukpembayaran' => 'Untukpembayaran',
            'keterangan_pengeluaran' => 'Keterangan Pengeluaran',
            'jmlkaskeluar' => 'Jmlkaskeluar',
            'biayaadministrasi' => 'Biayaadministrasi',
            'keterangankeluar' => 'Keterangankeluar',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        // $criteria->select = "periodegaji, nama_pegawai, penggajianpeg_id, pegawai_id, keterangan";
        //       $criteria->group = 'periodegaji, penggajianpeg_id, nama_pegawai, pegawai_id, keterangan';
        $criteria->compare('pegawai_id', $this->pegawai_id);
        $criteria->compare('nomorindukpegawai', $this->nomorindukpegawai, true);
        $criteria->compare('no_kartupegawainegerisipil', $this->no_kartupegawainegerisipil, true);
        $criteria->compare('no_karis_karsu', $this->no_karis_karsu, true);
        $criteria->compare('no_taspen', $this->no_taspen, true);
        $criteria->compare('no_askes', $this->no_askes, true);
        $criteria->compare('jenisidentitas', $this->jenisidentitas, true);
        $criteria->compare('noidentitas', $this->noidentitas, true);
        $criteria->compare('gelardepan', $this->gelardepan, true);
        $criteria->compare('nama_pegawai', $this->nama_pegawai, true);
        $criteria->compare('nama_keluarga', $this->nama_keluarga, true);
        $criteria->compare('gelarbelakang_nama', $this->gelarbelakang_nama, true);
        $criteria->compare('tempatlahir_pegawai', $this->tempatlahir_pegawai, true);
        $criteria->compare('tgl_lahirpegawai', $this->tgl_lahirpegawai, true);
        $criteria->compare('jeniskelamin', $this->jeniskelamin, true);
        $criteria->compare('statusperkawinan', $this->statusperkawinan, true);
        $criteria->compare('alamat_pegawai', $this->alamat_pegawai, true);
        $criteria->compare('kelurahan_id', $this->kelurahan_id);
        $criteria->compare('kelurahan_nama', $this->kelurahan_nama, true);
        $criteria->compare('kode_pos', $this->kode_pos, true);
        $criteria->compare('kecamatan_id', $this->kecamatan_id);
        $criteria->compare('kecamatan_nama', $this->kecamatan_nama, true);
        $criteria->compare('kabupaten_id', $this->kabupaten_id);
        $criteria->compare('kabupaten_nama', $this->kabupaten_nama, true);
        $criteria->compare('agama', $this->agama, true);
        $criteria->compare('golongandarah', $this->golongandarah, true);
        $criteria->compare('rhesus', $this->rhesus, true);
        $criteria->compare('alamatemail', $this->alamatemail, true);
        $criteria->compare('notelp_pegawai', $this->notelp_pegawai, true);
        $criteria->compare('nomobile_pegawai', $this->nomobile_pegawai, true);
        $criteria->compare('warganegara_pegawai', $this->warganegara_pegawai, true);
        $criteria->compare('jeniswaktukerja', $this->jeniswaktukerja, true);
        $criteria->compare('suku_id', $this->suku_id);
        $criteria->compare('suku_nama', $this->suku_nama, true);
        $criteria->compare('statuskepemilikanrumah_id', $this->statuskepemilikanrumah_id);
        $criteria->compare('statuskepemilikanrumah_nama', $this->statuskepemilikanrumah_nama, true);
        $criteria->compare('propinsi_id', $this->propinsi_id);
        $criteria->compare('propinsi_nama', $this->propinsi_nama, true);
        $criteria->compare('profilrs_id', $this->profilrs_id);
        $criteria->compare('nokode_rumahsakit', $this->nokode_rumahsakit, true);
        $criteria->compare('nama_rumahsakit', $this->nama_rumahsakit, true);
        $criteria->compare('instalasi_id', $this->instalasi_id);
        $criteria->compare('instalasi_nama', $this->instalasi_nama, true);
        $criteria->compare('ruangan_id', $this->ruangan_id);
        $criteria->compare('ruangan_nama', $this->ruangan_nama, true);
        $criteria->compare('kelompokjabatan', $this->kelompokjabatan, true);
        $criteria->compare('golonganpegawai_id', $this->golonganpegawai_id);
        $criteria->compare('golonganpegawai_nama', $this->golonganpegawai_nama, true);
        $criteria->compare('pangkat_id', $this->pangkat_id);
        $criteria->compare('pangkat_nama', $this->pangkat_nama, true);
        $criteria->compare('jabatan_id', $this->jabatan_id);
        $criteria->compare('jabatan_nama', $this->jabatan_nama, true);
        $criteria->compare('esselon_id', $this->esselon_id);
        $criteria->compare('esselon_nama', $this->esselon_nama, true);
        $criteria->compare('kelompokpegawai_id', $this->kelompokpegawai_id);
        $criteria->compare('kelompokpegawai_nama', $this->kelompokpegawai_nama, true);
        $criteria->compare('kategoripegawai', $this->kategoripegawai, true);
        $criteria->compare('kategoripegawaiasal', $this->kategoripegawaiasal, true);
        $criteria->compare('photopegawai', $this->photopegawai, true);
        $criteria->compare('nofingerprint', $this->nofingerprint, true);
        $criteria->compare('tinggibadan', $this->tinggibadan);
        $criteria->compare('beratbadan', $this->beratbadan);
        $criteria->compare('kemampuanbahasa', $this->kemampuanbahasa, true);
        $criteria->compare('warnakulit', $this->warnakulit, true);
        $criteria->compare('nip_lama', $this->nip_lama, true);
        $criteria->compare('kode_bank', $this->kode_bank, true);
        $criteria->compare('no_rekening', $this->no_rekening, true);
        $criteria->compare('npwp', $this->npwp, true);
        $criteria->compare('tglditerima', $this->tglditerima, true);
        $criteria->compare('tglberhenti', $this->tglberhenti, true);
        $criteria->compare('penggajianpeg_id', $this->penggajianpeg_id);
        $criteria->compare('tglpenggajian', $this->tglpenggajian, true);
        $criteria->compare('nopenggajian', $this->nopenggajian, true);
        $criteria->compare('keterangan', $this->keterangan, true);
        $criteria->compare('mengetahui', $this->mengetahui, true);
        $criteria->compare('menyetujui', $this->menyetujui, true);
        $criteria->compare('totalterima', $this->totalterima);
        $criteria->compare('totalpajak', $this->totalpajak);
        $criteria->compare('totalpotongan', $this->totalpotongan);
        $criteria->compare('penerimaanbersih', $this->penerimaanbersih);
        $criteria->compare('periodegaji', $this->periodegaji, true);
        $criteria->compare('penggajiankomp_id', $this->penggajiankomp_id);
        $criteria->compare('komponengaji_id', $this->komponengaji_id);
        $criteria->compare('komponengaji_nama', $this->komponengaji_nama, true);
        $criteria->compare('jumlah', $this->jumlah);
        $criteria->compare('pengeluaranumum_id', $this->pengeluaranumum_id);
        $criteria->compare('jenispengeluaran_id', $this->jenispengeluaran_id);
        $criteria->compare('jenispengeluaran_kode', $this->jenispengeluaran_kode, true);
        $criteria->compare('jenispengeluaran_nama', $this->jenispengeluaran_nama, true);
        $criteria->compare('kelompoktransaksi', $this->kelompoktransaksi, true);
        $criteria->compare('nopengeluaran', $this->nopengeluaran, true);
        $criteria->compare('tglpengeluaran', $this->tglpengeluaran, true);
        $criteria->compare('volume', $this->volume);
        $criteria->compare('satuanvol', $this->satuanvol, true);
        $criteria->compare('bank', $this->bank);
        $criteria->compare('tunai', $this->tunai);
        $criteria->compare('statuspembayaran', $this->statuspembayaran, true);
        $criteria->compare('tandabuktikeluar_id', $this->tandabuktikeluar_id);
        $criteria->compare('tahun', $this->tahun, true);
        $criteria->compare('tglkaskeluar', $this->tglkaskeluar, true);
        $criteria->compare('nokaskeluar', $this->nokaskeluar, true);
        $criteria->compare('carabayarkeluar', $this->carabayarkeluar, true);
        $criteria->compare('melaluibank', $this->melaluibank, true);
        $criteria->compare('denganrekening', $this->denganrekening, true);
        $criteria->compare('atasnamarekening', $this->atasnamarekening, true);
        $criteria->compare('namapenerima', $this->namapenerima, true);
        $criteria->compare('alamatpenerima', $this->alamatpenerima, true);
        $criteria->compare('untukpembayaran', $this->untukpembayaran, true);
        $criteria->compare('keterangan_pengeluaran', $this->keterangan_pengeluaran, true);
        $criteria->compare('jmlkaskeluar', $this->jmlkaskeluar);
        $criteria->compare('biayaadministrasi', $this->biayaadministrasi);
        $criteria->compare('keterangankeluar', $this->keterangankeluar, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchInformasi() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        // $criteria->select = "periodegaji, nama_pegawai, penggajianpeg_id, pegawai_id, keterangan";
        //       $criteria->group = 'periodegaji, penggajianpeg_id, nama_pegawai, pegawai_id, keterangan';
        $criteria->select = "periodegaji, nomorindukpegawai, nama_pegawai, penggajianpeg_id, pegawai_id, keterangan";
        $criteria->group = 'periodegaji, nomorindukpegawai, penggajianpeg_id, nama_pegawai, pegawai_id, keterangan';
        $criteria->compare('instalasi_id', $this->instalasi_id);
        $criteria->compare('ruangan_id', $this->ruangan_id);
        $criteria->compare('penggajianpeg_id', $this->penggajianpeg_id);
        $criteria->compare('date(periodegaji)', $this->periodegaji);
        $criteria->compare('pegawai_id', $this->pegawai_id);
        $criteria->compare('LOWER(gelardepan)', strtolower($this->gelardepan), true);
        $criteria->compare('LOWER(nama_pegawai)', strtolower($this->nama_pegawai), true);
        $criteria->compare('LOWER(nama_keluarga)', strtolower($this->nama_keluarga), true);
        $criteria->compare('LOWER(tglpenggajian)', strtolower($this->tglpenggajian), true);
        $criteria->compare('LOWER(nopenggajian)', strtolower($this->nopenggajian), true);
        $criteria->compare('LOWER(statusperkawinan)', strtolower($this->statusperkawinan), true);
        $criteria->compare('LOWER(jeniskelamin)', strtolower($this->jeniskelamin), true);
        // $criteria->compare('nourutgaji',$this->nourutgaji);
        // $criteria->compare('LOWER(komponengaji_kode)',strtolower($this->komponengaji_kode),true);
        $criteria->compare('LOWER(komponengaji_nama)', strtolower($this->komponengaji_nama), true);
        // $criteria->compare('LOWER(komponengaji_singkt)',strtolower($this->komponengaji_singkt),true);
        // $criteria->compare('ispotongan',$this->ispotongan);
        $criteria->compare('jumlah', $this->jumlah);
        $criteria->compare('LOWER(keterangan)', strtolower($this->keterangan), true);
        $criteria->compare('LOWER(mengetahui)', strtolower($this->mengetahui), true);
        $criteria->compare('LOWER(menyetujui)', strtolower($this->menyetujui), true);
        $criteria->compare('pengeluaranumum_id', $this->pengeluaranumum_id);
        $criteria->compare('LOWER(nomorindukpegawai)', strtolower($this->nomorindukpegawai), true);
        $criteria->compare('komponengaji_id', $this->komponengaji_id);
        $criteria->compare('LOWER(kategoripegawai)', strtolower($this->kategoripegawai), true);

        if ($this->print) {
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => false
            ));
        }
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getInstalasiItems() {
        return InstalasiM::model()->findAll('instalasi_aktif=TRUE ORDER BY instalasi_id');
    }

    public function getRuanganItems() {
        return RuanganM::model()->findAll('ruangan_aktif=TRUE');
    }

    public function getPeriode() {
        $format = new CustomFormat;
        $periode = explode('-', $this->periodegaji);
        $periode_bulan = $periode[1];
        $periode_tahun = $periode[0];
        $bulan = $format->formatBulanInaLengkap($periode_bulan);

        return $bulan . ' ' . $periode_tahun;
    }

    private function MyItemPegawai() {
        if (!isset($this->itemPegawai[$this->penggajianpeg_id])) {
            $list = LaporanrekapitulasigajiV::model()->findAllByAttributes(array('penggajianpeg_id' => $this->penggajianpeg_id));
			
            $newList = array();
            foreach ($list as $data) {
                $newList[$data->komponengaji_id] = isset($data->jumlah) ? $data->jumlah : 0;
            }
            $this->itemPegawai[$this->penggajianpeg_id] = $newList;
        }
    }

    public function getGapok() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('gaji_pokok')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('gaji_pokok')] : 0;
        return $nilai;
    }

    public function getHarian() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_harian')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_harian')] : 0;
        return $nilai;
    }

    public function getJabatan() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_jabatan')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_jabatan')] : 0;
        return $nilai;
    }

    public function getFungsional() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_fungsional')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_fungsional')] : 0;
        return $nilai;
    }

    public function getKhusus() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_khusus')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_khusus')] : 0;
        return $nilai;
    }

    public function getSosial() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_sosial')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_sosial')] : 0;
        return $nilai;
    }

    public function getPph21() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('pph_pasal21')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('pph_pasal21')] : 0;
        return $nilai;
    }

    public function getEssensial() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_essensial')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_essensial')] : 0;
        return $nilai;
    }

    public function getPensiun() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_pensiun')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_pensiun')] : 0;
        return $nilai;
    }

    public function getKesehatan() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_kesehatan')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_kesehatan')] : 0;
        return $nilai;
    }

    public function getKompetensi() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_kompetensi')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_kompetensi')] : 0;
        return $nilai;
    }

    public function getKemahalan() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_kemahalan')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tunjangan_kemahalan')] : 0;
        return $nilai;
    }

    public function getInsentif() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('insentif')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('insentif')] : 0;
        return $nilai;
    }

    public function getJasa() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('jasa_lainnya')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('jasa_lainnya')] : 0;
        return $nilai;
    }

    public function getLembur() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('lembur')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('lembur')] : 0;
        return $nilai;
    }

    public function getGajikotor() {
        $nilai = $this->getLembur() + $this->getJasa() + $this->getInsentif() + $this->getKemahalan() + $this->getKompetensi() + $this->getKesehatan() + $this->getPensiun() + $this->getEssensial() + $this->getPph21() + $this->getSosial() + $this->getKhusus() + $this->getFungsional() + $this->getJabatan() + $this->getHarian() + $this->getGapok();
        return $nilai;
    }

    public function getKoperasi() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('koperasi')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('koperasi')] : 0;
        return $nilai;
    }

    public function getJamsostek() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('jamsostek')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('jamsostek')] : 0;
        return $nilai;
    }

    public function getPotPensiun() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('potongan_pensiun')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('potongan_pensiun')] : 0;
        return $nilai;
    }

    public function getPotKesehatan() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('potongan_kesehatan')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('potongan_kesehatan')] : 0;
        return $nilai;
    }

    public function getPotBRI() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('bri')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('bri')] : 0;
        return $nilai;
    }

    public function getPotBSJ() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('bsj')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('bsj')] : 0;
        return $nilai;
    }

    public function getPotBTN() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('btn')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('btn')] : 0;
        return $nilai;
    }

    public function getPotIntern() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('potongan_intern')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('potongan_intern')] : 0;
        return $nilai;
    }

    public function getApotek() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('apotek')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('apotek')] : 0;
        return $nilai;
    }

    public function getLab() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('laboratorium')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('laboratorium')] : 0;
        return $nilai;
    }

    public function getTmkrj() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tmkrj')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('tmkrj')] : 0;
        return $nilai;
    }

    public function getPajak() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('pajak')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('pajak')] : 0;
        return $nilai;
    }

    public function getLain2() {
        $this->MyItemPegawai();

        $nilai = isset($this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('lain_lain')]) ? $this->itemPegawai[$this->penggajianpeg_id][Params::komponenGaji('lain_lain')] : 0;
        return $nilai;
    }

    public function getTotalPotongan() {
        $nilai = $this->getLain2() + $this->getPajak() + $this->getTmkrj() + $this->getLab() + $this->getApotek() + $this->getPotIntern() + $this->getPotBTN() + $this->getPotBSJ() + $this->getPotBRI() + $this->getPotKesehatan() + $this->getPotPensiun() + $this->getJamsostek() + $this->getKoperasi();
        return $nilai;
    }

    public function getGajiBersih() {
        $nilai = $this->getGajikotor() - $this->getTotalPotongan();
        return $nilai;
    }

    public function getNamaModel() {
        return __CLASS__;
    }

}
