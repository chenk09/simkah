<?php

/**
 * This is the model class for table "penerimaandetail_t".
 *
 * The followings are the available columns in table 'penerimaandetail_t':
 * @property integer $penerimaandetail_id
 * @property integer $stokobatalkes_id
 * @property integer $returdetail_id
 * @property integer $penerimaanbarang_id
 * @property integer $satuankecil_id
 * @property integer $sumberdana_id
 * @property integer $fakturdetail_id
 * @property integer $obatalkes_id
 * @property integer $satuanbesar_id
 * @property double $jmlpermintaan
 * @property double $jmlterima
 * @property double $persendiscount
 * @property double $jmldiscount
 * @property double $harganettoper
 * @property double $hargappnper
 * @property double $hargapphper
 * @property double $hargasatuanper
 * @property string $tglkadaluarsa
 * @property integer $jmlkemasan
 * @property double $biaya_lainlain
 */
class PenerimaandetailT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PenerimaandetailT the static model class
	 */
        public $harganetto,$jmlppn,$jmldiscount,$jumlahterima,$hargabelibesar,$hargabruto,$total_harga,$hargappn,$persendiscount,$total_hpp;
        public $obatalkes_kode,$obatalkes_nama,$satuanbesar_nama,$nama_obat;
        public $jmlSudahTerima = 0; //untuk mengetahui jumlah yang sebelumnya telah diterima

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penerimaandetail_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penerimaanbarang_id, jmlpermintaan, jmlterima, persendiscount, jmldiscount, harganettoper, hargappnper, hargapphper, hargasatuanper', 'required'),
			array('stokobatalkes_id, returdetail_id, penerimaanbarang_id, satuankecil_id, sumberdana_id, fakturdetail_id, obatalkes_id, satuanbesar_id, jmlkemasan', 'numerical', 'integerOnly'=>true),
			array('hargabelibesar, jmlpermintaan, jmlterima, persendiscount, jmldiscount, harganettoper, hargappnper, hargapphper, hargasatuanper, biaya_lainlain', 'numerical'),
			array('tglkadaluarsa', 'safe'),
                        array('jmlterima', 'setValidasi','on'=>'dariPermintaanPembelian'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('hargabelibesar,penerimaandetail_id, stokobatalkes_id, returdetail_id, penerimaanbarang_id, satuankecil_id, sumberdana_id, fakturdetail_id, obatalkes_id, satuanbesar_id, jmlpermintaan, jmlterima, persendiscount, jmldiscount, harganettoper, hargappnper, hargapphper, hargasatuanper, tglkadaluarsa, jmlkemasan, biaya_lainlain', 'safe', 'on'=>'search'),
		);
	}
        
        public function setValidasi(){
            if (!$this->hasErrors()){
                if ($this->jmlterima > $this->jmlpermintaan){
                    $this->addError('jmlterima', 'Jumlah Terima tidak boleh lebih dari jumlah permintaan');
                }
                if ($this->persendiscount > 100){
                    $this->addError('persendiscount', 'Jumlah Persen Discount tidak boleh lebih dari jumlah permintaan');
                }
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'obatalkes'=>array(self::BELONGS_TO,'ObatalkesM','obatalkes_id'),
                    'satuankecil'=>array(self::BELONGS_TO,'SatuankecilM','satuankecil_id'),
                    'satuanbesar'=>array(self::BELONGS_TO,'SatuanbesarM','satuanbesar_id'),
                    'penerimaanbarang'=>array(self::BELONGS_TO,'PenerimaanbarangT','penerimaanbarang_id'),
                    'sumberdana'=>array(self::BELONGS_TO,  'SumberdanaM','sumberdana_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penerimaandetail_id' => 'Penerimaandetail',
			'stokobatalkes_id' => 'Stokobatalkes',
			'returdetail_id' => 'Returdetail',
			'penerimaanbarang_id' => 'Penerimaanbarang',
			'satuankecil_id' => 'Satuankecil',
			'sumberdana_id' => 'Sumberdana',
			'fakturdetail_id' => 'Fakturdetail',
			'obatalkes_id' => 'Obatalkes',
			'satuanbesar_id' => 'Satuanbesar',
			'jmlpermintaan' => 'Jmlpermintaan',
			'jmlterima' => 'Jmlterima',
			'persendiscount' => 'Persendiscount',
			'jmldiscount' => 'Jmldiscount',
			'harganettoper' => 'Harganettoper',
			'hargappnper' => 'Hargappnper',
			'hargapphper' => 'Hargapphper',
			'hargasatuanper' => 'Hargasatuanper',
			'tglkadaluarsa' => 'Tglkadaluarsa',
			'jmlkemasan' => 'Jmlkemasan',
			'biaya_lainlain' => 'Biaya Lainlain',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penerimaandetail_id',$this->penerimaandetail_id);
		$criteria->compare('stokobatalkes_id',$this->stokobatalkes_id);
		$criteria->compare('returdetail_id',$this->returdetail_id);
		$criteria->compare('penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('fakturdetail_id',$this->fakturdetail_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('jmlpermintaan',$this->jmlpermintaan);
		$criteria->compare('jmlterima',$this->jmlterima);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('harganettoper',$this->harganettoper);
		$criteria->compare('hargappnper',$this->hargappnper);
		$criteria->compare('hargapphper',$this->hargapphper);
		$criteria->compare('hargasatuanper',$this->hargasatuanper);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
		$criteria->compare('biaya_lainlain',$this->biaya_lainlain);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penerimaandetail_id',$this->penerimaandetail_id);
		$criteria->compare('stokobatalkes_id',$this->stokobatalkes_id);
		$criteria->compare('returdetail_id',$this->returdetail_id);
		$criteria->compare('penerimaanbarang_id',$this->penerimaanbarang_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('fakturdetail_id',$this->fakturdetail_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('jmlpermintaan',$this->jmlpermintaan);
		$criteria->compare('jmlterima',$this->jmlterima);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('harganettoper',$this->harganettoper);
		$criteria->compare('hargappnper',$this->hargappnper);
		$criteria->compare('hargapphper',$this->hargapphper);
		$criteria->compare('hargasatuanper',$this->hargasatuanper);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
		$criteria->compare('biaya_lainlain',$this->biaya_lainlain);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}