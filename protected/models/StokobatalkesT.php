<?php

/**
 * This is the model class for table "stokobatalkes_t".
 *
 * The followings are the available columns in table 'stokobatalkes_t':
 * @property integer $stokobatalkes_id
 * @property integer $penerimaandetail_id
 * @property integer $satuankecil_id
 * @property integer $obatalkes_id
 * @property integer $sumberdana_id
 * @property integer $ruangan_id
 * @property string $tglstok_in
 * @property string $tglstok_out
 * @property double $qtystok_in
 * @property double $qtystok_out
 * @property double $qtystok_current
 * @property double $harganetto_oa
 * @property double $hargajual_oa
 * @property double $discount
 * @property string $tglkadaluarsa
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 * @property double $qtystok_current
 */
class StokobatalkesT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StokobatalkesT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stokobatalkes_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			array('obatalkes_id, sumberdana_id, ruangan_id, tglstok_in, qtystok_in, qtystok_out, qtystok_current, harganetto_oa, hargajual_oa', 'required'),
			array('penerimaandetail_id, satuankecil_id, obatalkes_id, sumberdana_id, ruangan_id', 'numerical', 'integerOnly'=>true),
			array('qtystok_in, qtystok_out, qtystok_current, harganetto_oa, hargajual_oa, discount', 'numerical'),
			array('tglstok_out, tglkadaluarsa, update_time, update_loginpemakai_id', 'safe'),                   

                        array('create_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update,insert'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
                        array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
			// Please remove those attributes that should not be searched.
			array('stokobatalkes_id, penerimaandetail_id, satuankecil_id, obatalkes_id, sumberdana_id, ruangan_id, tglstok_in, tglstok_out, qtystok_in, qtystok_out, qtystok_current, harganetto_oa, hargajual_oa, discount, tglkadaluarsa, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan, marginresep, jasadokter, hjaresep, marginnonresep, hjanonresep, hpp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'obatalkes'=>array(self::BELONGS_TO, 'ObatalkesM','obatalkes_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'stokobatalkes_id' => 'Stokobatalkes',
			'penerimaandetail_id' => 'Penerimaandetail',
			'satuankecil_id' => 'Satuankecil',
			'obatalkes_id' => 'Obatalkes',
			'sumberdana_id' => 'Sumberdana',
			'ruangan_id' => 'Ruangan',
			'tglstok_in' => 'Tglstok In',
			'tglstok_out' => 'Tglstok Out',
			'qtystok_in' => 'Qtystok In',
			'qtystok_out' => 'Qtystok Out',
			'qtystok_current' => 'Qtystok Current',
			'harganetto_oa' => 'Harganetto Oa',
			'hargajual_oa' => 'Hargajual Oa',
			'discount' => 'Discount',
			'tglkadaluarsa' => 'Tglkadaluarsa',
                                                'qtystok_current' => 'Qty Sekarang',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('stokobatalkes_id',$this->stokobatalkes_id);
		$criteria->compare('penerimaandetail_id',$this->penerimaandetail_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(tglstok_in)',strtolower($this->tglstok_in),true);
		$criteria->compare('LOWER(tglstok_out)',strtolower($this->tglstok_out),true);
		$criteria->compare('qtystok_in',$this->qtystok_in);
		$criteria->compare('qtystok_out',$this->qtystok_out);
		$criteria->compare('qtystok_current',$this->qtystok_current);
		$criteria->compare('harganetto_oa',$this->harganetto_oa);
		$criteria->compare('hargajual_oa',$this->hargajual_oa);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('stokobatalkes_id',$this->stokobatalkes_id);
		$criteria->compare('penerimaandetail_id',$this->penerimaandetail_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(tglstok_in)',strtolower($this->tglstok_in),true);
		$criteria->compare('LOWER(tglstok_out)',strtolower($this->tglstok_out),true);
		$criteria->compare('qtystok_in',$this->qtystok_in);
		$criteria->compare('qtystok_out',$this->qtystok_out);
		$criteria->compare('qtystok_current',$this->qtystok_current);
		$criteria->compare('harganetto_oa',$this->harganetto_oa);
		$criteria->compare('hargajual_oa',$this->hargajual_oa);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        /**
         * method untuk mendapatkan stok obat alkes berdasarkan id obat alkes dan ruangan
         * @param int $idobatAlkes
         * @param int $ruangan_id
         * @return int
         */
        public static function getStokBarang($idobatAlkes, $ruangan_id = null) {
            if (isset($ruangan_id))
            {
                $sql = "
                    SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t
                    WHERE obatalkes_id  = $idobatAlkes AND ruangan_id = $ruangan_id 
                    ORDER BY tglstok_in ".((Params::KONFIG_LIFO) ? "DESC" : "ASC");
            }else{
                $sql = "
                    SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current
                    FROM stokobatalkes_t
                    WHERE obatalkes_id = $idobatAlkes ORDER BY tglstok_in ".((Params::KONFIG_LIFO) ? "DESC" : "ASC");
            }
            
            $ruangan = RuanganM::model()->findByPk($ruangan_id);
            if(empty($ruangan_id))
            {
                $status = "Stok Obat di ruangan .$ruangan->ruangan_nama. Tidak Ada";
            }
            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
            
            $selesai = false;
            $hasil = true;
            $current = 0;
            $out = 0;
            $in = 0;
            
            if (count($stoks) == 0)
            {
                $hasil = 0;
            }
            
            foreach ($stoks as $i => $stok)
            {
                  $current += $stok['qtystok_current'];
                  $in += $stok['qtystok_in'];
                  $out += $stok['qtystok_out'];
            }
//            $hasil = $in - $out;
            $hasil = $current;
            return $hasil;
        }
        
        /**
         * method untuk melihat jumlah stok di stokobatalkes_t masih cukup atau tidak untuk dikurangi
         * @param int $qty
         * @param int $id
         * @param int $ruangan
         * @return boolean 
         */
        public static function validasiStok($qty, $id, $ruangan=null){
            if (isset($ruangan))
            {
                $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $id and ruangan_id = $ruangan ORDER BY tglstok_in ".((Params::KONFIG_LIFO) ? "DESC" : "ASC");
            }
            else
            {
                $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $id ORDER BY tglstok_in ".((Params::KONFIG_LIFO) ? "DESC" : "ASC");
            }
            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
            $selesai = false;
            $hasil = true;
            $out = 0;
            $in = 0;
            if (count($stoks) == 0){
                $hasil = false;
            }
            foreach ($stoks as $i => $stok) {
                  $in += $stok['qtystok_in'];
                  $out += $stok['qtystok_out'];
            }
            $selisih = $in - $out;
            $selisih = $selisih-$qty;

            if ($selisih < 0){
                $hasil = false;
            } else {
                $hasil = true;
            }

            return $hasil;
        }
        /**
         * tambahStok = tambah stok record baru dari:
         * 1. transaksi ubah penjualan
         * 2. transaksi retur penjualan
         * @param type $data
         * @param type $model
         * @param type $qty
         * @return \StokobatalkesT
         */
        public static function tambahStok($dataObat, $qty){
            $modStokObat = new StokobatalkesT();
            $modStokObat->attributes = $dataObat->attributes;
            $modStokObat->qtystok_in = $qty;
            $modStokObat->qtystok_out = 0;
            $modStokObat->qtystok_current = $qty;
            $modStokObat->hargajual_oa = empty($dataObat->hargasatuan) ? 0 : $dataObat->hargasatuan;
            $modStokObat->harganetto_oa = empty($dataObat->harganetto) ? 0 : $dataObat->harganetto;
            
            $modObat = ObatalkesM::model()->findByPk($dataObat->obatalkes_id);
            if(!empty($modObat->obatalkes_id)){
                $modStokObat->hjaresep = $modObat->hjaresep;
                $modStokObat->hjanonresep = $modObat->hjanonresep;
                $modStokObat->marginresep = $modObat->marginresep;
                $modStokObat->marginnonresep = $modObat->marginnonresep;
                $modStokObat->hpp = $modObat->hpp;
            }
            
            $modStokObat->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modStokObat->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modStokObat->tglstok_in = date('Y-m-d H:i:s');
            $modStokObat->tglstok_out = null;
            $modStokObat->create_time = date('Y-m-d H:i:s');
            $modStokObat->update_time = null;
            $modStokObat->create_loginpemakai_id = Yii::app()->user->id;
            $modStokObat->save();
            return $modStokObat;
        }
        
        /**
         * function kurangiStok untuk setiap transaksi obat
         * @param int $qty = untuk qty atau selisih
         * @param int $idobatAlkes = obatalkes_id
         * @param int $ruangan_id = optional
         */
        public static function kurangiStok($qty,$idObatalkes,$idRuangan = null)
        {
            $sukses = true;
            if($idRuangan == null)
                $idRuangan = Yii::app()->user->getState('ruangan_id');
            $criteria = new CDbCriteria();
            $criteria->condition = "obatalkes_id = '$idObatalkes' AND ruangan_id = '$idRuangan' AND qtystok_current > 0 ORDER BY tglstok_in ASC";
            $modStok = StokobatalkesT::model()->findAll($criteria);
            $selisih = 0;
            $sisa = 0;
            if($qty <= StokobatalkesT::getStokBarang($idObatalkes, $idRuangan)){ //cekstok
                if($modStok){ //jika obat diruangan ada
                    foreach ($modStok as $i => $stok) {
                        if($sisa > 0){ //jika ada sisa
                            $qty = $sisa;
                        }

                        $selisih = $stok->qtystok_in - ($stok->qtystok_out + $qty);
                        
                        if($selisih == 0){ //Jika selisih 0
                            $stok->qtystok_out = $stok->qtystok_in;
                            $stok->qtystok_current = 0;
                            $sisa = 0;
                        }else{
                            if($selisih > 0){ //jika selisih positif
                                $stok->qtystok_out = $stok->qtystok_out + $qty;
                                $stok->qtystok_current = $selisih;
                                $sisa = 0;
                            }else{ //jika selisih negatif
                                $stok->qtystok_out = $stok->qtystok_in;
                                $stok->qtystok_current = 0;
                                $sisa = abs($selisih);
                            }
                        }

                        $stok->tglstok_out = date("Y-m-d H:i:s");
                        $stok->save();
                        if($sisa == 0) break; //berhenti jika sudah tidak ada sisa
                    }
                }else{
                    $sukses = false;
                }
            }else{
                $sukses = false;
            }
            return $sukses;
        }
        /**
         * method untuk mengurangi stok obatalkes 
         * @param int $qty
         * @param int $idobatAlkes 
         * @param int $ruangan_id optional
         */
        public static function kurangiStokOldTiar($qty, $idobatAlkes, $ruangan_id = null) {
            if (is_null($ruangan_id)){
                $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $idobatAlkes ORDER BY tglkadaluarsa ".((Params::KONFIG_LIFO) ? "ASC" : "DESC");
            }else{
                $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $idobatAlkes and ruangan_id = $ruangan_id ORDER BY tglkadaluarsa ".((Params::KONFIG_LIFO) ? "ASC" : "DESC");
            }
            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
            $selesai = false;
            foreach ($stoks as $i => $stok) {
                if ($qty <= $stok['qtystok_current']) {
                    $stok_current = $stok['qtystok_current'] - $qty;
                    $stok_out = $stok['qtystok_out'] + $qty;
                    StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_current' => $stok_current, 'qtystok_out' => $stok_out));
                    $selesai = true;
                    break;
                } else {
                    $qty = $qty - $stok['qtystok_current'];
                    $stok_current = 0;
                    $stok_out = $stok['qtystok_out'] + $stok['qtystok_current'];
//                    
                    //-- baru --//
                    //$qty = $qty - $stok['qtystok_current'];
//                    $stok_current = 0;
//                    $stok_current = $stok['qtystok_current'] - $qty;
//                    $stok_out = $stok['qtystok_out'] + $stok['qtystok_current'];
//                    $stok_out = $stok['qtystok_out'] + $qty ;
                    //-- baru --//
                    StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_current' => $stok_current, 'qtystok_out' => $stok_out));
                }
            }
        }
        
        /**
         * method untuk mendapatkan stok maksimal atau minimal dari obat alkes
         * @param int $idobatAlkes 
         * @param int $ruangan_id optional
         * @param string $type default 'in'
         * @return string output berupa string  
         */
        public static function MaxStokInOut($idobatAlkes, $ruangan_id = null,$type='in') {
            if (is_null($ruangan_id)){
                $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $idobatAlkes ORDER BY tglstok_in ".((Params::KONFIG_LIFO) ? "DESC" : "ASC");
            }else{
                $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $idobatAlkes and ruangan_id = $ruangan_id ORDER BY tglstok_in ".((Params::KONFIG_LIFO) ? "DESC" : "ASC");
            }
            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
            $selesai = false;
            $result = 0;
            if ($stoks > 0){
                foreach ($stoks as $i => $stok) {
                        $result += (isset($stoks['qtystok_'.$type]) ? $stoks['qtystok_'.$type] : 0);
                }
            }
            return $result;
        }
}