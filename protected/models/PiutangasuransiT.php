<?php

/**
 * This is the model class for table "piutangasuransi_t".
 *
 * The followings are the available columns in table 'piutangasuransi_t':
 * @property integer $piutangasuransi_id
 * @property integer $kelaspelayanan_id
 * @property integer $pasien_id
 * @property integer $pasienadmisi_id
 * @property integer $pendaftaran_id
 * @property integer $pembayaranpelayanan_id
 * @property integer $penjamin_id
 * @property integer $carabayar_id
 * @property string $tglpiutangasuransi
 * @property double $jmlpiutangasuransi
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $update_loginpemakai_id
 * @property integer $create_ruangan
 */
class PiutangasuransiT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PiutangasuransiT the static model class
	 */
        public $tgl_awal,$tgl_akhir,$bulan,$hari,$tahun;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'piutangasuransi_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kelaspelayanan_id, pasien_id, pendaftaran_id, penjamin_id, carabayar_id, tglpiutangasuransi, jmlpiutangasuransi, create_time, create_loginpemakai_id, create_ruangan', 'required'),
			array('kelaspelayanan_id, pasien_id, pasienadmisi_id, pendaftaran_id, pembayaranpelayanan_id, penjamin_id, carabayar_id, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'numerical', 'integerOnly'=>true),
			array('jmlpiutangasuransi', 'numerical'),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('piutangasuransi_id, kelaspelayanan_id, pasien_id, pasienadmisi_id, pendaftaran_id, pembayaranpelayanan_id, penjamin_id, carabayar_id, tglpiutangasuransi, jmlpiutangasuransi, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'pendaftaran'=>array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
                    'admisi'=>array(self::BELONGS_TO, 'PasienadmisiT', 'pasienadmisi_id'),
                    'pelayanan'=>array(self::BELONGS_TO, 'PembayaranpelayananT', 'pembayaranpelayanan_id'),
                    'pasien'=>array(self::BELONGS_TO, 'PasienM', 'pasien_id'),
                    'penjamin' => array(self::BELONGS_TO, 'PenjaminpasienM', 'penjamin_id'),
                    'carabayar' => array(self::BELONGS_TO, 'CarabayarM', 'carabayar_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'piutangasuransi_id' => 'Piutang Asuransi',
			'kelaspelayanan_id' => 'Kelas Pelayanan',
			'pasien_id' => 'Pasien',
			'pasienadmisi_id' => 'Pasien Admisi',
			'pendaftaran_id' => 'Pendaftaran',
			'pembayaranpelayanan_id' => 'Pembayaran Pelayanan',
			'penjamin_id' => 'Penjamin',
			'carabayar_id' => 'Cara Bayar',
			'tglpiutangasuransi' => 'Tanggal Piutang Asuransi',
			'jmlpiutangasuransi' => 'Jumlah Piutang Asuransi',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('piutangasuransi_id',$this->piutangasuransi_id);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pembayaranpelayanan_id',$this->pembayaranpelayanan_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(tglpiutangasuransi)',strtolower($this->tglpiutangasuransi),true);
		$criteria->compare('jmlpiutangasuransi',$this->jmlpiutangasuransi);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('piutangasuransi_id',$this->piutangasuransi_id);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pembayaranpelayanan_id',$this->pembayaranpelayanan_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(tglpiutangasuransi)',strtolower($this->tglpiutangasuransi),true);
		$criteria->compare('jmlpiutangasuransi',$this->jmlpiutangasuransi);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}