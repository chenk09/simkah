<?php

/**
 * This is the model class for table "map_diagnosa_obat".
 *
 * The followings are the available columns in table 'map_diagnosa_obat':
 * @property string $begda
 * @property string $endda
 * @property string $chgdt
 * @property string $chgus
 * @property string $mapping_diag_obat_id
 * @property integer $diagnosa_id
 * @property integer $kelas_pelayanan_id
 */
class MapDiagnosaObat extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MapDiagnosaObat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'map_diagnosa_obat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('diagnosa_id, kelas_pelayanan_id', 'numerical', 'integerOnly'=>true),
			array('begda, endda, chgdt, chgus', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('begda, endda, chgdt, chgus, mapping_diag_obat_id, diagnosa_id, kelas_pelayanan_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'begda' => 'Begda',
			'endda' => 'Endda',
			'chgdt' => 'Chgdt',
			'chgus' => 'Chgus',
			'mapping_diag_obat_id' => 'Mapping Diag Obat',
			'diagnosa_id' => 'Diagnosa',
			'kelas_pelayanan_id' => 'Kelas Pelayanan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(begda)',strtolower($this->begda),true);
		$criteria->compare('LOWER(endda)',strtolower($this->endda),true);
		$criteria->compare('LOWER(chgdt)',strtolower($this->chgdt),true);
		$criteria->compare('LOWER(chgus)',strtolower($this->chgus),true);
		$criteria->compare('LOWER(mapping_diag_obat_id)',strtolower($this->mapping_diag_obat_id),true);
		$criteria->compare('diagnosa_id',$this->diagnosa_id);
		$criteria->compare('kelas_pelayanan_id',$this->kelas_pelayanan_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(begda)',strtolower($this->begda),true);
		$criteria->compare('LOWER(endda)',strtolower($this->endda),true);
		$criteria->compare('LOWER(chgdt)',strtolower($this->chgdt),true);
		$criteria->compare('LOWER(chgus)',strtolower($this->chgus),true);
		$criteria->compare('LOWER(mapping_diag_obat_id)',strtolower($this->mapping_diag_obat_id),true);
		$criteria->compare('diagnosa_id',$this->diagnosa_id);
		$criteria->compare('kelas_pelayanan_id',$this->kelas_pelayanan_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}