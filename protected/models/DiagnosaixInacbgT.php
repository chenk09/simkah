<?php

/**
 * This is the model class for table "diagnosaix_inacbg_t".
 *
 * The followings are the available columns in table 'diagnosaix_inacbg_t':
 * @property integer $diagnosaix_inacbg_id
 * @property integer $inacbg_id
 * @property integer $sep_id
 * @property integer $pendaftaran_id
 * @property string $diagnosaix_kode
 * @property string $diagnosaix_nama
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $update_loginpemakai_id
 * @property integer $create_ruangan_id
 * @property integer $update_ruangan_id
 *
 * The followings are the available model relations:
 * @property PendaftaranT $pendaftaran
 * @property SepT $sep
 */
class DiagnosaixInacbgT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DiagnosaixInacbgT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'diagnosaix_inacbg_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('inacbg_id, sep_id, pendaftaran_id, diagnosaix_nama, create_time, create_loginpemakai_id, create_ruangan_id', 'required'),
			array('inacbg_id, sep_id, pendaftaran_id, create_loginpemakai_id, update_loginpemakai_id, create_ruangan_id, update_ruangan_id', 'numerical', 'integerOnly'=>true),
			array('diagnosaix_kode', 'length', 'max'=>10),
			array('diagnosaix_nama', 'length', 'max'=>250),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('diagnosaix_inacbg_id, inacbg_id, sep_id, pendaftaran_id, diagnosaix_kode, diagnosaix_nama, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan_id, update_ruangan_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pendaftaran' => array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
			'sep' => array(self::BELONGS_TO, 'SepT', 'sep_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'diagnosaix_inacbg_id' => 'Diagnosaix Inacbg',
			'inacbg_id' => 'Inacbg',
			'sep_id' => 'Sep',
			'pendaftaran_id' => 'Pendaftaran',
			'diagnosaix_kode' => 'Diagnosaix Kode',
			'diagnosaix_nama' => 'Diagnosaix Nama',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan_id' => 'Create Ruangan',
			'update_ruangan_id' => 'Update Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		if(!empty($this->diagnosaix_inacbg_id)){
			$criteria->addCondition('diagnosaix_inacbg_id = '.$this->diagnosaix_inacbg_id);
		}
		if(!empty($this->inacbg_id)){
			$criteria->addCondition('inacbg_id = '.$this->inacbg_id);
		}
		if(!empty($this->sep_id)){
			$criteria->addCondition('sep_id = '.$this->sep_id);
		}
		if(!empty($this->pendaftaran_id)){
			$criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
		}
		$criteria->compare('LOWER(diagnosaix_kode)',strtolower($this->diagnosaix_kode),true);
		$criteria->compare('LOWER(diagnosaix_nama)',strtolower($this->diagnosaix_nama),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		if(!empty($this->create_loginpemakai_id)){
			$criteria->addCondition('create_loginpemakai_id = '.$this->create_loginpemakai_id);
		}
		if(!empty($this->update_loginpemakai_id)){
			$criteria->addCondition('update_loginpemakai_id = '.$this->update_loginpemakai_id);
		}
		if(!empty($this->create_ruangan_id)){
			$criteria->addCondition('create_ruangan_id = '.$this->create_ruangan_id);
		}
		if(!empty($this->update_ruangan_id)){
			$criteria->addCondition('update_ruangan_id = '.$this->update_ruangan_id);
		}

		return $criteria;
	}
        
        
        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=10;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }


        public function searchPrint()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
}