<?php

/**
 * This is the model class for table "konfig_bpjs_inacbg_k".
 *
 * The followings are the available columns in table 'konfig_bpjs_inacbg_k':
 * @property integer $konfig_bpjs_inacbg_id
 * @property string $user_bpjs_name
 * @property string $secret_bpjs_key
 * @property string $bpjs_host_tester
 * @property string $bpjs_host_production
 * @property string $bpjs_service_name
 * @property string $bpjs_port
 * @property boolean $is_bridging
 * @property string $kode_ppk_bpjs
 * @property string $nama_ppk_pelayanan
 * @property boolean $konfig_bpjs_inacbg_aktif
 */
class KonfigBpjsInacbgK extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KonfigBpjsInacbgK the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'konfig_bpjs_inacbg_k';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_bpjs_name, secret_bpjs_key', 'required'),
			array('user_bpjs_name, secret_bpjs_key, bpjs_service_name', 'length', 'max'=>100),
			array('bpjs_host_tester, bpjs_host_production', 'length', 'max'=>250),
			array('bpjs_port', 'length', 'max'=>10),
			array('kode_ppk_bpjs, nama_ppk_pelayanan', 'length', 'max'=>50),
			array('is_bridging, konfig_bpjs_inacbg_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('konfig_bpjs_inacbg_id, user_bpjs_name, secret_bpjs_key, bpjs_host_tester, bpjs_host_production, bpjs_service_name, bpjs_port, is_bridging, kode_ppk_bpjs, nama_ppk_pelayanan, konfig_bpjs_inacbg_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'konfig_bpjs_inacbg_id' => 'Konfig Bpjs Inacbg',
			'user_bpjs_name' => 'User Bpjs Name',
			'secret_bpjs_key' => 'Secret Bpjs Key',
			'bpjs_host_tester' => 'Bpjs Host Tester',
			'bpjs_host_production' => 'Bpjs Host Production',
			'bpjs_service_name' => 'Bpjs Service Name',
			'bpjs_port' => 'Bpjs Port',
			'is_bridging' => 'Is Bridging',
			'kode_ppk_bpjs' => 'Kode Ppk Bpjs',
			'nama_ppk_pelayanan' => 'Nama Ppk Pelayanan',
			'konfig_bpjs_inacbg_aktif' => 'Konfig Bpjs Inacbg Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('konfig_bpjs_inacbg_id',$this->konfig_bpjs_inacbg_id);
		$criteria->compare('LOWER(user_bpjs_name)',strtolower($this->user_bpjs_name),true);
		$criteria->compare('LOWER(secret_bpjs_key)',strtolower($this->secret_bpjs_key),true);
		$criteria->compare('LOWER(bpjs_host_tester)',strtolower($this->bpjs_host_tester),true);
		$criteria->compare('LOWER(bpjs_host_production)',strtolower($this->bpjs_host_production),true);
		$criteria->compare('LOWER(bpjs_service_name)',strtolower($this->bpjs_service_name),true);
		$criteria->compare('LOWER(bpjs_port)',strtolower($this->bpjs_port),true);
		$criteria->compare('is_bridging',$this->is_bridging);
		$criteria->compare('LOWER(kode_ppk_bpjs)',strtolower($this->kode_ppk_bpjs),true);
		$criteria->compare('LOWER(nama_ppk_pelayanan)',strtolower($this->nama_ppk_pelayanan),true);
		$criteria->compare('konfig_bpjs_inacbg_aktif',$this->konfig_bpjs_inacbg_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('konfig_bpjs_inacbg_id',$this->konfig_bpjs_inacbg_id);
		$criteria->compare('LOWER(user_bpjs_name)',strtolower($this->user_bpjs_name),true);
		$criteria->compare('LOWER(secret_bpjs_key)',strtolower($this->secret_bpjs_key),true);
		$criteria->compare('LOWER(bpjs_host_tester)',strtolower($this->bpjs_host_tester),true);
		$criteria->compare('LOWER(bpjs_host_production)',strtolower($this->bpjs_host_production),true);
		$criteria->compare('LOWER(bpjs_service_name)',strtolower($this->bpjs_service_name),true);
		$criteria->compare('LOWER(bpjs_port)',strtolower($this->bpjs_port),true);
		$criteria->compare('is_bridging',$this->is_bridging);
		$criteria->compare('LOWER(kode_ppk_bpjs)',strtolower($this->kode_ppk_bpjs),true);
		$criteria->compare('LOWER(nama_ppk_pelayanan)',strtolower($this->nama_ppk_pelayanan),true);
		$criteria->compare('konfig_bpjs_inacbg_aktif',$this->konfig_bpjs_inacbg_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}