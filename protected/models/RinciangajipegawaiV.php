<?php

/**
 * This is the model class for table "rinciangajipegawai_v".
 *
 * The followings are the available columns in table 'rinciangajipegawai_v':
 * @property integer $penggajianpeg_id
 * @property string $periodegaji
 * @property integer $pegawai_id
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property string $nama_keluarga
 * @property string $tglpenggajian
 * @property string $nopenggajian
 * @property string $statusperkawinan
 * @property string $jeniskelamin
 * @property integer $nourutgaji
 * @property string $komponengaji_kode
 * @property string $komponengaji_nama
 * @property string $komponengaji_singkt
 * @property boolean $ispotongan
 * @property double $jumlah
 * @property string $keterangan
 * @property string $mengetahui
 * @property string $menyetujui
 * @property integer $pengeluaranumum_id
 * @property string $nomorindukpegawai
 */
class RinciangajipegawaiV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RinciangajipegawaiV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rinciangajipegawai_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penggajianpeg_id, pegawai_id, nourutgaji, pengeluaranumum_id', 'numerical', 'integerOnly'=>true),
			array('jumlah', 'numerical'),
			array('gelardepan', 'length', 'max'=>10),
			array('nama_pegawai, nama_keluarga, nopenggajian, komponengaji_kode', 'length', 'max'=>50),
			array('statusperkawinan, jeniskelamin, komponengaji_singkt', 'length', 'max'=>20),
			array('komponengaji_nama, mengetahui, menyetujui', 'length', 'max'=>100),
			array('nomorindukpegawai', 'length', 'max'=>30),
			array('periodegaji, tglpenggajian, ispotongan, keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penggajianpeg_id, periodegaji, pegawai_id, gelardepan, nama_pegawai, nama_keluarga, tglpenggajian, nopenggajian, statusperkawinan, jeniskelamin, nourutgaji, komponengaji_kode, komponengaji_nama, komponengaji_singkt, ispotongan, jumlah, keterangan, mengetahui, menyetujui, pengeluaranumum_id, nomorindukpegawai', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penggajianpeg_id' => 'Penggajianpeg',
			'periodegaji' => 'Periodegaji',
			'pegawai_id' => 'Pegawai',
			'gelardepan' => 'Gelardepan',
			'nama_pegawai' => 'Nama Pegawai',
			'nama_keluarga' => 'Nama Keluarga',
			'tglpenggajian' => 'Tglpenggajian',
			'nopenggajian' => 'Nopenggajian',
			'statusperkawinan' => 'Statusperkawinan',
			'jeniskelamin' => 'Jeniskelamin',
			'nourutgaji' => 'Nourutgaji',
			'komponengaji_kode' => 'Komponengaji Kode',
			'komponengaji_nama' => 'Komponengaji Nama',
			'komponengaji_singkt' => 'Komponengaji Singkt',
			'ispotongan' => 'Ispotongan',
			'jumlah' => 'Jumlah',
			'keterangan' => 'Keterangan',
			'mengetahui' => 'Mengetahui',
			'menyetujui' => 'Menyetujui',
			'pengeluaranumum_id' => 'Pengeluaranumum',
			'nomorindukpegawai' => 'Nomorindukpegawai',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penggajianpeg_id',$this->penggajianpeg_id);
		$criteria->compare('LOWER(periodegaji)',strtolower($this->periodegaji),true);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('LOWER(tglpenggajian)',strtolower($this->tglpenggajian),true);
		$criteria->compare('LOWER(nopenggajian)',strtolower($this->nopenggajian),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('nourutgaji',$this->nourutgaji);
		$criteria->compare('LOWER(komponengaji_kode)',strtolower($this->komponengaji_kode),true);
		$criteria->compare('LOWER(komponengaji_nama)',strtolower($this->komponengaji_nama),true);
		$criteria->compare('LOWER(komponengaji_singkt)',strtolower($this->komponengaji_singkt),true);
		$criteria->compare('ispotongan',$this->ispotongan);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('LOWER(mengetahui)',strtolower($this->mengetahui),true);
		$criteria->compare('LOWER(menyetujui)',strtolower($this->menyetujui),true);
		$criteria->compare('pengeluaranumum_id',$this->pengeluaranumum_id);
		$criteria->compare('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penggajianpeg_id',$this->penggajianpeg_id);
		$criteria->compare('LOWER(periodegaji)',strtolower($this->periodegaji),true);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('LOWER(tglpenggajian)',strtolower($this->tglpenggajian),true);
		$criteria->compare('LOWER(nopenggajian)',strtolower($this->nopenggajian),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('nourutgaji',$this->nourutgaji);
		$criteria->compare('LOWER(komponengaji_kode)',strtolower($this->komponengaji_kode),true);
		$criteria->compare('LOWER(komponengaji_nama)',strtolower($this->komponengaji_nama),true);
		$criteria->compare('LOWER(komponengaji_singkt)',strtolower($this->komponengaji_singkt),true);
		$criteria->compare('ispotongan',$this->ispotongan);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('LOWER(mengetahui)',strtolower($this->mengetahui),true);
		$criteria->compare('LOWER(menyetujui)',strtolower($this->menyetujui),true);
		$criteria->compare('pengeluaranumum_id',$this->pengeluaranumum_id);
		$criteria->compare('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}