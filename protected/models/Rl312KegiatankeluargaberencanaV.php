<?php

/**
 * This is the model class for table "rl3_12_kegiatankeluargaberencana_v".
 *
 * The followings are the available columns in table 'rl3_12_kegiatankeluargaberencana_v':
 * @property double $tahun
 * @property integer $profilrs_id
 * @property string $nokode_rumahsakit
 * @property string $nama_rumahsakit
 * @property integer $daftartindakan_id
 * @property string $daftartindakan_kode
 * @property string $daftartindakan_nama
 * @property double $anc
 * @property double $pascapersalinan
 * @property string $bukanrujukan
 * @property double $rujukanri
 * @property double $rujukanrj
 * @property string $total
 * @property double $pascapersalinannifas
 * @property double $abortus
 * @property double $lainnya
 * @property double $kunjunganulang
 * @property double $keluhanefeksampingjumlah
 * @property double $keluhanefeksampingdirujuk
 */
class Rl312KegiatankeluargaberencanaV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rl312KegiatankeluargaberencanaV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rl3_12_kegiatankeluargaberencana_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profilrs_id, daftartindakan_id', 'numerical', 'integerOnly'=>true),
			array('tahun, anc, pascapersalinan, rujukanri, rujukanrj, pascapersalinannifas, abortus, lainnya, kunjunganulang, keluhanefeksampingjumlah, keluhanefeksampingdirujuk', 'numerical'),
			array('nokode_rumahsakit', 'length', 'max'=>10),
			array('nama_rumahsakit', 'length', 'max'=>100),
			array('daftartindakan_kode', 'length', 'max'=>20),
			array('daftartindakan_nama', 'length', 'max'=>200),
			array('bukanrujukan, total', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tahun, profilrs_id, nokode_rumahsakit, nama_rumahsakit, daftartindakan_id, daftartindakan_kode, daftartindakan_nama, anc, pascapersalinan, bukanrujukan, rujukanri, rujukanrj, total, pascapersalinannifas, abortus, lainnya, kunjunganulang, keluhanefeksampingjumlah, keluhanefeksampingdirujuk', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tahun' => 'Tahun',
			'profilrs_id' => 'Profilrs',
			'nokode_rumahsakit' => 'Nokode Rumahsakit',
			'nama_rumahsakit' => 'Nama Rumahsakit',
			'daftartindakan_id' => 'Daftartindakan',
			'daftartindakan_kode' => 'Daftartindakan Kode',
			'daftartindakan_nama' => 'Daftartindakan Nama',
			'anc' => 'Anc',
			'pascapersalinan' => 'Pascapersalinan',
			'bukanrujukan' => 'Bukanrujukan',
			'rujukanri' => 'Rujukanri',
			'rujukanrj' => 'Rujukanrj',
			'total' => 'Total',
			'pascapersalinannifas' => 'Pascapersalinannifas',
			'abortus' => 'Abortus',
			'lainnya' => 'Lainnya',
			'kunjunganulang' => 'Kunjunganulang',
			'keluhanefeksampingjumlah' => 'Keluhanefeksampingjumlah',
			'keluhanefeksampingdirujuk' => 'Keluhanefeksampingdirujuk',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('nokode_rumahsakit',$this->nokode_rumahsakit,true);
		$criteria->compare('nama_rumahsakit',$this->nama_rumahsakit,true);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('daftartindakan_kode',$this->daftartindakan_kode,true);
		$criteria->compare('daftartindakan_nama',$this->daftartindakan_nama,true);
		$criteria->compare('anc',$this->anc);
		$criteria->compare('pascapersalinan',$this->pascapersalinan);
		$criteria->compare('bukanrujukan',$this->bukanrujukan,true);
		$criteria->compare('rujukanri',$this->rujukanri);
		$criteria->compare('rujukanrj',$this->rujukanrj);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('pascapersalinannifas',$this->pascapersalinannifas);
		$criteria->compare('abortus',$this->abortus);
		$criteria->compare('lainnya',$this->lainnya);
		$criteria->compare('kunjunganulang',$this->kunjunganulang);
		$criteria->compare('keluhanefeksampingjumlah',$this->keluhanefeksampingjumlah);
		$criteria->compare('keluhanefeksampingdirujuk',$this->keluhanefeksampingdirujuk);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}