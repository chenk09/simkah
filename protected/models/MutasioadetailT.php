<?php

/**
 * This is the model class for table "mutasioadetail_t".
 *
 * The followings are the available columns in table 'mutasioadetail_t':
 * @property integer $mutasioadetail_id
 * @property integer $mutasioaruangan_id
 * @property integer $obatalkes_id
 * @property integer $sumberdana_id
 * @property integer $satuankecil_id
 * @property double $jmlmutasi
 * @property double $jmlpesan
 * @property double $harganetto
 * @property double $hargajualsatuan
 * @property double $persendiscount
 * @property double $totalharga
 * @property string $tglkadaluarsa
 * @property integer $jmlkemasan
 */
class MutasioadetailT extends CActiveRecord
{
        public $stok, $subTotal;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MutasioadetailT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mutasioadetail_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mutasioaruangan_id, obatalkes_id, sumberdana_id, satuankecil_id, jmlmutasi, harganetto, hargajualsatuan, persendiscount', 'required'),
			array('mutasioaruangan_id, obatalkes_id, sumberdana_id, satuankecil_id, jmlkemasan', 'numerical', 'integerOnly'=>true),
			array('jmlmutasi, jmlpesan, harganetto, hargajualsatuan, persendiscount, totalharga', 'numerical'),
			array('stok, subTotal, tglkadaluarsa', 'safe'),
                        array('jmlmutasi', 'cekStok'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('mutasioadetail_id, mutasioaruangan_id, obatalkes_id, sumberdana_id, satuankecil_id, jmlmutasi, jmlpesan, harganetto, hargajualsatuan, persendiscount, totalharga, tglkadaluarsa, jmlkemasan', 'safe', 'on'=>'search'),
		);
	}
        
        public function cekStok($attribute, $params){
            $stokMinus = Params::konfigGudangFarmasi('stokminus');
            if ($stokMinus){
                if (!$this->hasErrors()){
                    if ($this->jmlmutasi > $this->stok){
                        $this->addError('jmlmutasi','Jumlah Mutasi tidak boleh lebih dari '.$this->stok);
                    }
                }
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'pesanobatalkes'=>array(self::BELONGS_TO,'PesanobatalkesT','pesanobatalkes_id'),
                    'obatalkes'=>array(self::BELONGS_TO,'ObatalkesM','obatalkes_id'),
                    'satuankecil'=>array(self::BELONGS_TO,'SatuankecilM','satuankecil_id'),
                    'sumberdana'=>array(self::BELONGS_TO,'SumberdanaM','sumberdana_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mutasioadetail_id' => 'Mutasioadetail',
			'mutasioaruangan_id' => 'Mutasi Obat dan Alkes Ruangan',
			'obatalkes_id' => 'Obat Alkes',
			'sumberdana_id' => 'Sumber Dana',
			'satuankecil_id' => 'Satuan Kecil',
			'jmlmutasi' => 'Jumlah Mutasi',
			'jmlpesan' => 'Jumlah Pesan',
			'harganetto' => 'Harga Netto',
			'hargajualsatuan' => 'Harga Jual Satuan',
			'persendiscount' => 'Persen Discount',
			'totalharga' => 'Total Harga',
			'tglkadaluarsa' => 'Tanggal Kadaluarsa',
			'jmlkemasan' => 'Jumlah Kemasan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mutasioadetail_id',$this->mutasioadetail_id);
		$criteria->compare('mutasioaruangan_id',$this->mutasioaruangan_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jmlmutasi',$this->jmlmutasi);
		$criteria->compare('jmlpesan',$this->jmlpesan);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajualsatuan',$this->hargajualsatuan);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('mutasioadetail_id',$this->mutasioadetail_id);
		$criteria->compare('mutasioaruangan_id',$this->mutasioaruangan_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jmlmutasi',$this->jmlmutasi);
		$criteria->compare('jmlpesan',$this->jmlpesan);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajualsatuan',$this->hargajualsatuan);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function beforeSave() {
            if($this->tglkadaluarsa===null || trim($this->tglkadaluarsa)==''){
	        $this->setAttribute('tglkadaluarsa', null);
            }
            
            return parent::beforeSave();
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date')
                        {
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                        }
                    else if ( $column->dbType == 'timestamp without time zone')
                        {
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                        }
            }

            return parent::beforeValidate ();
        }
}