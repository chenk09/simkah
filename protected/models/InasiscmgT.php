<?php

/**
 * This is the model class for table "inasiscmg_t".
 *
 * The followings are the available columns in table 'inasiscmg_t':
 * @property integer $inasiscmg_id
 * @property integer $pendaftaran_id
 * @property integer $inacbg_id
 * @property string $inasiscmg_tgl
 * @property string $kode_spesialprosedure
 * @property string $nama_spesialprosedure
 * @property double $plafon_spesialprosedure
 * @property string $kode_spesialprosthesis
 * @property string $nama_spesialprosthesis
 * @property double $plafon_spesialprosthesis
 * @property string $kode_spesialinvestigation
 * @property string $nama_spesialinvestigation
 * @property double $plafon_spesialinvestigation
 * @property string $kode_spesialdrug
 * @property string $nama_spesialdrug
 * @property double $plafon_spesialdrug
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $update_loginpemakai_id
 * @property integer $create_ruangan
 */
class InasiscmgT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InasiscmgT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inasiscmg_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pendaftaran_id, inasiscmg_tgl, create_time, create_loginpemakai_id, create_ruangan', 'required'),
			array('pendaftaran_id, inacbg_id, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'numerical', 'integerOnly'=>true),
			array('plafon_spesialprosedure, plafon_spesialprosthesis, plafon_spesialinvestigation, plafon_spesialdrug', 'numerical'),
			array('kode_spesialprosedure, kode_spesialprosthesis, kode_spesialinvestigation, kode_spesialdrug', 'length', 'max'=>10),
			array('nama_spesialprosedure, nama_spesialprosthesis, nama_spesialinvestigation, nama_spesialdrug', 'length', 'max'=>250),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('inasiscmg_id, pendaftaran_id, inacbg_id, inasiscmg_tgl, kode_spesialprosedure, nama_spesialprosedure, plafon_spesialprosedure, kode_spesialprosthesis, nama_spesialprosthesis, plafon_spesialprosthesis, kode_spesialinvestigation, nama_spesialinvestigation, plafon_spesialinvestigation, kode_spesialdrug, nama_spesialdrug, plafon_spesialdrug, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'inasiscmg_id' => 'Inasiscmg',
			'pendaftaran_id' => 'Pendaftaran',
			'inacbg_id' => 'Inacbg',
			'inasiscmg_tgl' => 'Inasiscmg Tgl',
			'kode_spesialprosedure' => 'Kode Spesialprosedure',
			'nama_spesialprosedure' => 'Nama Spesialprosedure',
			'plafon_spesialprosedure' => 'Plafon Spesialprosedure',
			'kode_spesialprosthesis' => 'Kode Spesialprosthesis',
			'nama_spesialprosthesis' => 'Nama Spesialprosthesis',
			'plafon_spesialprosthesis' => 'Plafon Spesialprosthesis',
			'kode_spesialinvestigation' => 'Kode Spesialinvestigation',
			'nama_spesialinvestigation' => 'Nama Spesialinvestigation',
			'plafon_spesialinvestigation' => 'Plafon Spesialinvestigation',
			'kode_spesialdrug' => 'Kode Spesialdrug',
			'nama_spesialdrug' => 'Nama Spesialdrug',
			'plafon_spesialdrug' => 'Plafon Spesialdrug',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		if(!empty($this->inasiscmg_id)){
			$criteria->addCondition('inasiscmg_id = '.$this->inasiscmg_id);
		}
		if(!empty($this->pendaftaran_id)){
			$criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
		}
		if(!empty($this->inacbg_id)){
			$criteria->addCondition('inacbg_id = '.$this->inacbg_id);
		}
		$criteria->compare('LOWER(inasiscmg_tgl)',strtolower($this->inasiscmg_tgl),true);
		$criteria->compare('LOWER(kode_spesialprosedure)',strtolower($this->kode_spesialprosedure),true);
		$criteria->compare('LOWER(nama_spesialprosedure)',strtolower($this->nama_spesialprosedure),true);
		$criteria->compare('plafon_spesialprosedure',$this->plafon_spesialprosedure);
		$criteria->compare('LOWER(kode_spesialprosthesis)',strtolower($this->kode_spesialprosthesis),true);
		$criteria->compare('LOWER(nama_spesialprosthesis)',strtolower($this->nama_spesialprosthesis),true);
		$criteria->compare('plafon_spesialprosthesis',$this->plafon_spesialprosthesis);
		$criteria->compare('LOWER(kode_spesialinvestigation)',strtolower($this->kode_spesialinvestigation),true);
		$criteria->compare('LOWER(nama_spesialinvestigation)',strtolower($this->nama_spesialinvestigation),true);
		$criteria->compare('plafon_spesialinvestigation',$this->plafon_spesialinvestigation);
		$criteria->compare('LOWER(kode_spesialdrug)',strtolower($this->kode_spesialdrug),true);
		$criteria->compare('LOWER(nama_spesialdrug)',strtolower($this->nama_spesialdrug),true);
		$criteria->compare('plafon_spesialdrug',$this->plafon_spesialdrug);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		if(!empty($this->create_loginpemakai_id)){
			$criteria->addCondition('create_loginpemakai_id = '.$this->create_loginpemakai_id);
		}
		if(!empty($this->update_loginpemakai_id)){
			$criteria->addCondition('update_loginpemakai_id = '.$this->update_loginpemakai_id);
		}
		if(!empty($this->create_ruangan)){
			$criteria->addCondition('create_ruangan = '.$this->create_ruangan);
		}

		return $criteria;
	}
        
        
        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=10;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }


        public function searchPrint()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=$this->criteriaSearch();
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
}