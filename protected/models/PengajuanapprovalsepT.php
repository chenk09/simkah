<?php

/**
 * This is the model class for table "pengajuanapprovalsep_t".
 *
 * The followings are the available columns in table 'pengajuanapprovalsep_t':
 * @property integer $pengajuanapprovalsep_id
 * @property integer $pendaftaran_id
 * @property string $no_kartu_bpjs
 * @property string $tgl_sep
 * @property string $kode_ppk_pelayanan
 * @property string $nama_ppk_pelayanan
 * @property string $jenis_pelayanan
 * @property string $kelas_tanggungan
 * @property string $asal_rujukan
 * @property string $no_rujukan
 * @property string $kode_ppk_rujukan
 * @property string $nama_ppk_rujukan
 * @property string $tgl_rujukan
 * @property string $diagnosa_awal
 * @property boolean $poli_eksekutif
 * @property boolean $cob
 * @property boolean $lakalantas
 * @property string $penjamin
 * @property string $no_telepon_pasien
 * @property string $userpembuat_bpjs
 * @property string $catatan
 * @property string $create_time
 * @property integer $create_loginpemakai_id
 * @property boolean $is_approval
 * @property integer $sep_id
 * @property string $user_approval_bpjs
 *
 * The followings are the available model relations:
 * @property PendaftaranT $pendaftaran
 */
class PengajuanapprovalsepT extends CActiveRecord
{
        public $cob_status;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PengajuanapprovalsepT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pengajuanapprovalsep_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_kartu_bpjs, tgl_sep, nama_ppk_pelayanan, userpembuat_bpjs, create_time, create_loginpemakai_id', 'required'),
			array('pendaftaran_id, create_loginpemakai_id, sep_id', 'numerical', 'integerOnly'=>true),
			array('no_kartu_bpjs, kode_ppk_pelayanan, jenis_pelayanan, kode_ppk_rujukan, userpembuat_bpjs, user_approval_bpjs', 'length', 'max'=>50),
			array('nama_ppk_pelayanan, asal_rujukan, no_rujukan, nama_ppk_rujukan, penjamin', 'length', 'max'=>100),
			array('kelas_tanggungan', 'length', 'max'=>10),
			array('diagnosa_awal', 'length', 'max'=>250),
			array('no_telepon_pasien', 'length', 'max'=>20),
			array('tgl_rujukan, poli_eksekutif, cob, lakalantas, catatan, is_approval', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pengajuanapprovalsep_id, pendaftaran_id, no_kartu_bpjs, tgl_sep, kode_ppk_pelayanan, nama_ppk_pelayanan, jenis_pelayanan, kelas_tanggungan, asal_rujukan, no_rujukan, kode_ppk_rujukan, nama_ppk_rujukan, tgl_rujukan, diagnosa_awal, poli_eksekutif, cob, lakalantas, penjamin, no_telepon_pasien, userpembuat_bpjs, catatan, create_time, create_loginpemakai_id, is_approval, sep_id, user_approval_bpjs', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pendaftaran' => array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
			'sep' => array(self::BELONGS_TO, 'SepT', 'sep_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pengajuanapprovalsep_id' => 'Pengajuanapprovalsep',
			'pendaftaran_id' => 'Pendaftaran',
			'no_kartu_bpjs' => 'No Kartu Bpjs',
			'tgl_sep' => 'Tgl Sep',
			'kode_ppk_pelayanan' => 'Kode Ppk Pelayanan',
			'nama_ppk_pelayanan' => 'Nama Ppk Pelayanan',
			'jenis_pelayanan' => 'Jenis Pelayanan',
			'kelas_tanggungan' => 'Kelas Tanggungan',
			'asal_rujukan' => 'Asal Rujukan',
			'no_rujukan' => 'No Rujukan',
			'kode_ppk_rujukan' => 'Kode Ppk Rujukan',
			'nama_ppk_rujukan' => 'Nama Ppk Rujukan',
			'tgl_rujukan' => 'Tgl Rujukan',
			'diagnosa_awal' => 'Diagnosa Awal',
			'poli_eksekutif' => 'Poli Eksekutif',
			'cob' => 'Cob',
			'lakalantas' => 'Lakalantas',
			'penjamin' => 'Penjamin',
			'no_telepon_pasien' => 'No Telepon Pasien',
			'userpembuat_bpjs' => 'Userpembuat Bpjs',
			'catatan' => 'Catatan',
			'create_time' => 'Create Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'is_approval' => 'Is Approval',
			'sep_id' => 'Sep',
			'user_approval_bpjs' => 'User Approval Bpjs',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pengajuanapprovalsep_id',$this->pengajuanapprovalsep_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_kartu_bpjs)',strtolower($this->no_kartu_bpjs),true);
		$criteria->compare('LOWER(tgl_sep)',strtolower($this->tgl_sep),true);
		$criteria->compare('LOWER(kode_ppk_pelayanan)',strtolower($this->kode_ppk_pelayanan),true);
		$criteria->compare('LOWER(nama_ppk_pelayanan)',strtolower($this->nama_ppk_pelayanan),true);
		$criteria->compare('LOWER(jenis_pelayanan)',strtolower($this->jenis_pelayanan),true);
		$criteria->compare('LOWER(kelas_tanggungan)',strtolower($this->kelas_tanggungan),true);
		$criteria->compare('LOWER(asal_rujukan)',strtolower($this->asal_rujukan),true);
		$criteria->compare('LOWER(no_rujukan)',strtolower($this->no_rujukan),true);
		$criteria->compare('LOWER(kode_ppk_rujukan)',strtolower($this->kode_ppk_rujukan),true);
		$criteria->compare('LOWER(nama_ppk_rujukan)',strtolower($this->nama_ppk_rujukan),true);
		$criteria->compare('LOWER(tgl_rujukan)',strtolower($this->tgl_rujukan),true);
		$criteria->compare('LOWER(diagnosa_awal)',strtolower($this->diagnosa_awal),true);
		$criteria->compare('poli_eksekutif',$this->poli_eksekutif);
		$criteria->compare('cob',$this->cob);
		$criteria->compare('lakalantas',$this->lakalantas);
		$criteria->compare('LOWER(penjamin)',strtolower($this->penjamin),true);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(userpembuat_bpjs)',strtolower($this->userpembuat_bpjs),true);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('is_approval',$this->is_approval);
		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('LOWER(user_approval_bpjs)',strtolower($this->user_approval_bpjs),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pengajuanapprovalsep_id',$this->pengajuanapprovalsep_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_kartu_bpjs)',strtolower($this->no_kartu_bpjs),true);
		$criteria->compare('LOWER(tgl_sep)',strtolower($this->tgl_sep),true);
		$criteria->compare('LOWER(kode_ppk_pelayanan)',strtolower($this->kode_ppk_pelayanan),true);
		$criteria->compare('LOWER(nama_ppk_pelayanan)',strtolower($this->nama_ppk_pelayanan),true);
		$criteria->compare('LOWER(jenis_pelayanan)',strtolower($this->jenis_pelayanan),true);
		$criteria->compare('LOWER(kelas_tanggungan)',strtolower($this->kelas_tanggungan),true);
		$criteria->compare('LOWER(asal_rujukan)',strtolower($this->asal_rujukan),true);
		$criteria->compare('LOWER(no_rujukan)',strtolower($this->no_rujukan),true);
		$criteria->compare('LOWER(kode_ppk_rujukan)',strtolower($this->kode_ppk_rujukan),true);
		$criteria->compare('LOWER(nama_ppk_rujukan)',strtolower($this->nama_ppk_rujukan),true);
		$criteria->compare('LOWER(tgl_rujukan)',strtolower($this->tgl_rujukan),true);
		$criteria->compare('LOWER(diagnosa_awal)',strtolower($this->diagnosa_awal),true);
		$criteria->compare('poli_eksekutif',$this->poli_eksekutif);
		$criteria->compare('cob',$this->cob);
		$criteria->compare('lakalantas',$this->lakalantas);
		$criteria->compare('LOWER(penjamin)',strtolower($this->penjamin),true);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(userpembuat_bpjs)',strtolower($this->userpembuat_bpjs),true);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('is_approval',$this->is_approval);
		$criteria->compare('sep_id',$this->sep_id);
		$criteria->compare('LOWER(user_approval_bpjs)',strtolower($this->user_approval_bpjs),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}