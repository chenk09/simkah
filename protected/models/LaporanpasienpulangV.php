<?php

/**
 * This is the model class for table "laporanpasienpulang_v".
 *
 * The followings are the available columns in table 'laporanpasienpulang_v':
 * @property string $no_pendaftaran
 * @property string $no_rekam_medik
 * @property string $nama_pasien
 * @property integer $pasien_id
 * @property string $no_telepon_pasien
 * @property integer $pasienpulang_id
 * @property integer $pendaftaran_id
 * @property string $tglpasienpulang
 * @property string $keterangankeluar
 * @property string $kondisipulang
 * @property string $carakeluar
 * @property integer $dokterpenanggungjawab_id
 * @property string $dokterpenanggungjawab_gelardepan
 * @property string $dokterpenanggungjawab_nama
 * @property string $dokterpenanggungjawab_gelarbelakang
 * @property integer $dokterspesialistik_id
 * @property string $dokterspesialistik_gelardepan
 * @property string $dokterspesialistik_nama
 * @property string $dokterspesialistik_gelarbelakang
 * @property integer $instalasi_id
 * @property string $instalasi_nama
 * @property integer $ruangan_id
 * @property string $subspesialistik
 * @property integer $jeniskasuspenyakit_id
 * @property string $jeniskasuspenyakit_nama
 * @property integer $kelaspelayanan_id
 * @property string $kelaspelayanan_nama
 * @property integer $penanggungjawab_id
 * @property string $pengantar
 * @property string $jenisidentitas
 * @property string $no_identitas
 * @property string $hubungankeluarga
 * @property string $nama_pj
 * @property string $tgllahir_pj
 * @property string $jeniskelamin
 * @property string $tempatlahir_pj
 * @property string $alamat_pj
 * @property string $no_teleponpj
 * @property string $no_mobilepj
 * @property string $no_identitas_pj
 */
class LaporanpasienpulangV extends CActiveRecord
{
        public $tglAwal;
        public $tglAkhir;
        public $jumlah;
        public $data;
        public $tick;
        public $pilihanx;
        public $dokterpemeriksa;
        public $gelardepan,$nama_pegawai,$gelarbelakang_nama, $pasienbatalpulang_id;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanpasienpulangV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanpasienpulang_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pasien_id, pasienpulang_id, pendaftaran_id, dokterpenanggungjawab_id, dokterspesialistik_id, instalasi_id, ruangan_id, jeniskasuspenyakit_id, kelaspelayanan_id, penanggungjawab_id', 'numerical', 'integerOnly'=>true),
			array('no_pendaftaran, jenisidentitas, jeniskelamin, tempatlahir_pj', 'length', 'max'=>20),
			array('no_rekam_medik, dokterpenanggungjawab_gelardepan, dokterspesialistik_gelardepan', 'length', 'max'=>10),
			array('nama_pasien, carakeluar, dokterpenanggungjawab_nama, dokterspesialistik_nama, instalasi_nama, subspesialistik, kelaspelayanan_nama, pengantar, no_identitas, hubungankeluarga, nama_pj, no_identitas_pj', 'length', 'max'=>50),
			array('no_telepon_pasien, dokterpenanggungjawab_gelarbelakang, dokterspesialistik_gelarbelakang, no_teleponpj, no_mobilepj', 'length', 'max'=>15),
			array('jeniskasuspenyakit_nama', 'length', 'max'=>100),
			array('tglpasienpulang, keterangankeluar, kondisipulang, tgllahir_pj, alamat_pj', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tglAwal, tglAkhir, carabayar_id, penjamin_id, no_pendaftaran, no_rekam_medik, nama_pasien, pasien_id, no_telepon_pasien, pasienpulang_id, pendaftaran_id, tglpasienpulang, keterangankeluar, kondisipulang, carakeluar, dokterpenanggungjawab_id, dokterpenanggungjawab_gelardepan, dokterpenanggungjawab_nama, dokterpenanggungjawab_gelarbelakang, dokterspesialistik_id, dokterspesialistik_gelardepan, dokterspesialistik_nama, dokterspesialistik_gelarbelakang, instalasi_id, instalasi_nama, ruangan_id, subspesialistik, jeniskasuspenyakit_id, jeniskasuspenyakit_nama, kelaspelayanan_id, kelaspelayanan_nama, penanggungjawab_id, pengantar, jenisidentitas, no_identitas, hubungankeluarga, nama_pj, tgllahir_pj, jeniskelamin, tempatlahir_pj, alamat_pj, no_teleponpj, no_mobilepj, no_identitas_pj', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pasienpulang' => array(self::BELONGS_TO, 'PasienpulangT', 'pasienbatalpulang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_pendaftaran' => 'No Pendaftaran',
			'no_rekam_medik' => 'No Rekam Medik',
			'nama_pasien' => 'Nama Pasien',
			'pasien_id' => 'Pasien',
			'no_telepon_pasien' => 'No Telepon Pasien',
			'pasienpulang_id' => 'Pasienpulang',
			'pendaftaran_id' => 'Pendaftaran',
			'tglpasienpulang' => 'Tgl Pasien Pulang',
			'keterangankeluar' => 'Keterangankeluar',
			'kondisipulang' => 'Kondisi Pulang',
			'carakeluar' => 'Cara Keluar',
			'dokterpenanggungjawab_id' => 'Dokterpenanggungjawab',
			'dokterpenanggungjawab_gelardepan' => 'Gelar Depan',
			'dokterpenanggungjawab_nama' => 'Nama Dokter',
			'dokterpenanggungjawab_gelarbelakang' => 'Gelar Belakang',
			'dokterspesialistik_id' => 'Dokter Spesialistik',
			'dokterspesialistik_gelardepan' => 'Gelar Depan Dokter Spesialistik',
			'dokterspesialistik_nama' => 'Nama Dokter Spesialistik',
			'dokterspesialistik_gelarbelakang' => 'Gelar Belakang Dokter Spesialistik',
			'instalasi_id' => 'Instalasi',
			'instalasi_nama' => 'Instalasi Nama',
			'ruangan_id' => 'Ruangan',
			'subspesialistik' => 'Sub Spesialistik',
			'jeniskasuspenyakit_id' => 'Jeniskasuspenyakit',
			'jeniskasuspenyakit_nama' => 'Jeniskasuspenyakit Nama',
			'kelaspelayanan_id' => 'Kelaspelayanan',
			'kelaspelayanan_nama' => 'Kelaspelayanan Nama',
			'penanggungjawab_id' => 'Penanggungjawab',
			'pengantar' => 'Pengantar',
			'jenisidentitas' => 'Jenisidentitas',
			'no_identitas' => 'No Identitas',
			'hubungankeluarga' => 'Hubungankeluarga',
			'nama_pj' => 'Nama Pj',
			'tgllahir_pj' => 'Tgllahir Pj',
			'jeniskelamin' => 'Jeniskelamin',
			'tempatlahir_pj' => 'Tempatlahir Pj',
			'alamat_pj' => 'Alamat Pj',
			'no_teleponpj' => 'No Teleponpj',
			'no_mobilepj' => 'No Mobilepj',
			'no_identitas_pj' => 'No Identitas Pj',
			'carabayar_id' => 'ID Cara Bayar',
			'carabayar_nama' => 'Cara Bayar',
			'penjamin_id' => 'ID Penjamin',
			'penjamin_nama' => 'Nama Penjamin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no_pendaftaran',$this->no_pendaftaran,true);
		$criteria->compare('no_rekam_medik',$this->no_rekam_medik,true);
		$criteria->compare('nama_pasien',$this->nama_pasien,true);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('no_telepon_pasien',$this->no_telepon_pasien,true);
		$criteria->compare('pasienpulang_id',$this->pasienpulang_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('tglpasienpulang',$this->tglpasienpulang,true);
		$criteria->compare('keterangankeluar',$this->keterangankeluar,true);
		$criteria->compare('kondisipulang',$this->kondisipulang,true);
		$criteria->compare('carakeluar',$this->carakeluar,true);
		$criteria->compare('dokterpenanggungjawab_id',$this->dokterpenanggungjawab_id);
		$criteria->compare('dokterpenanggungjawab_gelardepan',$this->dokterpenanggungjawab_gelardepan,true);
		$criteria->compare('dokterpenanggungjawab_nama',$this->dokterpenanggungjawab_nama,true);
		$criteria->compare('dokterpenanggungjawab_gelarbelakang',$this->dokterpenanggungjawab_gelarbelakang,true);
		$criteria->compare('dokterspesialistik_id',$this->dokterspesialistik_id);
		$criteria->compare('dokterspesialistik_gelardepan',$this->dokterspesialistik_gelardepan,true);
		$criteria->compare('dokterspesialistik_nama',$this->dokterspesialistik_nama,true);
		$criteria->compare('dokterspesialistik_gelarbelakang',$this->dokterspesialistik_gelarbelakang,true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('instalasi_nama',$this->instalasi_nama,true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('subspesialistik',$this->subspesialistik,true);
		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
		$criteria->compare('jeniskasuspenyakit_nama',$this->jeniskasuspenyakit_nama,true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('kelaspelayanan_nama',$this->kelaspelayanan_nama,true);
		$criteria->compare('penanggungjawab_id',$this->penanggungjawab_id);
		$criteria->compare('pengantar',$this->pengantar,true);
		$criteria->compare('jenisidentitas',$this->jenisidentitas,true);
		$criteria->compare('no_identitas',$this->no_identitas,true);
		$criteria->compare('hubungankeluarga',$this->hubungankeluarga,true);
		$criteria->compare('nama_pj',$this->nama_pj,true);
		$criteria->compare('tgllahir_pj',$this->tgllahir_pj,true);
		$criteria->compare('jeniskelamin',$this->jeniskelamin,true);
		$criteria->compare('tempatlahir_pj',$this->tempatlahir_pj,true);
		$criteria->compare('alamat_pj',$this->alamat_pj,true);
		$criteria->compare('no_teleponpj',$this->no_teleponpj,true);
		$criteria->compare('no_mobilepj',$this->no_mobilepj,true);
		$criteria->compare('no_identitas_pj',$this->no_identitas_pj,true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
        $criteria->compare('carabayar_nama',$this->carabayar_nama);
        $criteria->compare('penjamin_id',$this->penjamin_id);
        $criteria->compare('penjamin_nama',$this->penjamin_nama);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}