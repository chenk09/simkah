<?php

/**
 * This is the model class for table "rincianpenerimaankasrekening_v".
 *
 * The followings are the available columns in table 'rincianpenerimaankasrekening_v':
 * @property integer $tandabuktibayar_id
 * @property string $nobuktibayar
 * @property string $tglbuktibayar
 * @property double $jmlpembayaran
 * @property integer $struktur_id
 * @property string $kdstruktur
 * @property string $nmstruktur
 * @property integer $kelompok_id
 * @property string $kdkelompok
 * @property string $nmkelompok
 * @property integer $jenis_id
 * @property string $kdjenis
 * @property string $nmjenis
 * @property integer $obyek_id
 * @property string $kdobyek
 * @property string $nmobyek
 * @property integer $rincianobyek_id
 * @property string $kdrincianobyek
 * @property string $nmrincianobyek
 * @property string $darinama_bkm
 * @property string $alamat_bkm
 * @property string $sebagaipembayaran_bkm
 */
class RincianpenerimaankasrekeningV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RincianpenerimaankasrekeningV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rincianpenerimaankasrekening_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tandabuktibayar_id, struktur_id, kelompok_id, jenis_id, obyek_id, rincianobyek_id', 'numerical', 'integerOnly'=>true),
			array('jmlpembayaran', 'numerical'),
			array('nobuktibayar', 'length', 'max'=>50),
			array('kdstruktur, kdkelompok, kdjenis, kdobyek, kdrincianobyek', 'length', 'max'=>5),
			array('nmstruktur, darinama_bkm, sebagaipembayaran_bkm', 'length', 'max'=>100),
			array('nmkelompok', 'length', 'max'=>200),
			array('nmjenis', 'length', 'max'=>300),
			array('nmobyek', 'length', 'max'=>400),
			array('nmrincianobyek', 'length', 'max'=>500),
			array('tglbuktibayar, alamat_bkm', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('jenisNama, tglAwal,tglAkhir,tandabuktibayar_id, nobuktibayar, tglbuktibayar, jmlpembayaran, struktur_id, kdstruktur, nmstruktur, kelompok_id, kdkelompok, nmkelompok, jenis_id, kdjenis, nmjenis, obyek_id, kdobyek, nmobyek, rincianobyek_id, kdrincianobyek, nmrincianobyek, darinama_bkm, alamat_bkm, sebagaipembayaran_bkm', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'tandabukti'=>array(self::HAS_ONE,'TandabuktibayarT','tandabuktibayar_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tandabuktibayar_id' => 'Tanda Bukti Bayar',
			'nobuktibayar' => 'No Bukti Bayar',
			'nobuktibayar' => 'No Penerimaan',
			'tglbuktibayar' => 'Tgl Bukti Bayar',
			'jmlpembayaran' => 'Jml Pembayaran',
			'struktur_id' => 'Struktur',
			'kdstruktur' => 'Kode Struktur',
			'nmstruktur' => 'Nama Struktur',
			'kelompok_id' => 'Kelompok',
			'kdkelompok' => 'Kode Kelompok',
			'nmkelompok' => 'Nama Kelompok',
			'jenis_id' => 'Jenis Penerimaan',
			'kdjenis' => 'Kode Jenis',
			'nmjenis' => 'Nama Jenis',
			'obyek_id' => 'Obyek',
			'kdobyek' => 'Kode Obyek',
			'nmobyek' => 'Nama Obyek',
			'rincianobyek_id' => 'Rincian Obyek',
			'kdrincianobyek' => 'Kode Rincian Obyek',
			'nmrincianobyek' => 'Nama Rincian Obyek',
			'darinama_bkm' => 'Dari Nama Bkm',
			'alamat_bkm' => 'Alamat Bkm',
			'sebagaipembayaran_bkm' => 'Sebagai Pembayaran Bkm',
                        'tglAwal'=>'Tgl Penerimaan',
                        'tglAkhir'=>'Sampai Dengan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tandabuktibayar_id',$this->tandabuktibayar_id);
		$criteria->compare('LOWER(nobuktibayar)',strtolower($this->nobuktibayar),true);
		$criteria->compare('LOWER(tglbuktibayar)',strtolower($this->tglbuktibayar),true);
		$criteria->compare('jmlpembayaran',$this->jmlpembayaran);
		$criteria->compare('struktur_id',$this->struktur_id);
		$criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur),true);
		$criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(kdkelompok)',strtolower($this->kdkelompok),true);
		$criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
		$criteria->compare('jenis_id',$this->jenis_id);
		$criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
		$criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
		$criteria->compare('obyek_id',$this->obyek_id);
		$criteria->compare('LOWER(kdobyek)',strtolower($this->kdobyek),true);
		$criteria->compare('LOWER(nmobyek)',strtolower($this->nmobyek),true);
		$criteria->compare('rincianobyek_id',$this->rincianobyek_id);
		$criteria->compare('LOWER(kdrincianobyek)',strtolower($this->kdrincianobyek),true);
		$criteria->compare('LOWER(nmrincianobyek)',strtolower($this->nmrincianobyek),true);
		$criteria->compare('LOWER(darinama_bkm)',strtolower($this->darinama_bkm),true);
		$criteria->compare('LOWER(alamat_bkm)',strtolower($this->alamat_bkm),true);
		$criteria->compare('LOWER(sebagaipembayaran_bkm)',strtolower($this->sebagaipembayaran_bkm),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('tandabuktibayar_id',$this->tandabuktibayar_id);
		$criteria->compare('LOWER(nobuktibayar)',strtolower($this->nobuktibayar),true);
		$criteria->compare('LOWER(tglbuktibayar)',strtolower($this->tglbuktibayar),true);
		$criteria->compare('jmlpembayaran',$this->jmlpembayaran);
		$criteria->compare('struktur_id',$this->struktur_id);
		$criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur),true);
		$criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('LOWER(kdkelompok)',strtolower($this->kdkelompok),true);
		$criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
		$criteria->compare('jenis_id',$this->jenis_id);
		$criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
		$criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
		$criteria->compare('obyek_id',$this->obyek_id);
		$criteria->compare('LOWER(kdobyek)',strtolower($this->kdobyek),true);
		$criteria->compare('LOWER(nmobyek)',strtolower($this->nmobyek),true);
		$criteria->compare('rincianobyek_id',$this->rincianobyek_id);
		$criteria->compare('LOWER(kdrincianobyek)',strtolower($this->kdrincianobyek),true);
		$criteria->compare('LOWER(nmrincianobyek)',strtolower($this->nmrincianobyek),true);
		$criteria->compare('LOWER(darinama_bkm)',strtolower($this->darinama_bkm),true);
		$criteria->compare('LOWER(alamat_bkm)',strtolower($this->alamat_bkm),true);
		$criteria->compare('LOWER(sebagaipembayaran_bkm)',strtolower($this->sebagaipembayaran_bkm),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}