<?php

/**
 * This is the model class for table "laporaninventarisasiasetlain_v".
 *
 * The followings are the available columns in table 'laporaninventarisasiasetlain_v':
 * @property integer $inventaris_id
 * @property string $inventaris_kode
 * @property string $no_register
 * @property string $tgl_register
 * @property integer $asalaset_id
 * @property string $asalaset_nama
 * @property integer $golongan_id
 * @property string $golongan_kode
 * @property string $golongan_nama
 * @property integer $kelompok_id
 * @property string $kelompok_kode
 * @property string $kelompok_nama
 * @property integer $subkelompok_id
 * @property string $subkelompok_kode
 * @property string $subkelompok_nama
 * @property integer $bidang_id
 * @property string $bidang_kode
 * @property string $bidang_nama
 * @property integer $barang_id
 * @property string $barang_type
 * @property string $barang_kode
 * @property string $barang_nama
 * @property string $barang_merk
 * @property string $barang_noseri
 * @property string $barang_ukuran
 * @property string $barang_bahan
 * @property string $barang_thnbeli
 * @property string $barang_warna
 * @property boolean $barang_statusregister
 * @property integer $barang_ekonomis_thn
 * @property string $barang_satuan
 * @property integer $barang_jmldlmkemasan
 * @property string $barang_image
 * @property double $barang_harga
 * @property integer $lokasi_id
 * @property string $lokasiaset_kode
 * @property string $lokasiaset_namainstalasi
 * @property string $lokasiaset_namabagian
 * @property string $lokasiaset_namalokasi
 * @property integer $pemilikbarang_id
 * @property string $pemilikbarang_kode
 * @property string $pemilikbarang_nama
 * @property string $invasetlain_namabrg
 * @property string $invasetlain_judulbuku
 * @property string $invasetlain_spesifikasibuku
 * @property string $invasetlain_asalkesenian
 * @property double $invasetlain_jumlah
 * @property string $invasetlain_thncetak
 * @property double $invasetlain_harga
 * @property string $invasetlain_tglguna
 * @property double $invasetlain_akumsusut
 * @property string $invasetlain_ket
 * @property string $invasetlain_penciptakesenian
 * @property string $invasetlain_bahankesenian
 * @property string $invasetlain_jenishewan_tum
 * @property string $invasetlain_ukuranhewan_tum
 */
class LaporaninventarisasiasetlainV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporaninventarisasiasetlainV the static model class
	 */

	public $tglAwal, $tglAkhir, $tick;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporaninventarisasiasetlain_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('inventaris_id, asalaset_id, golongan_id, kelompok_id, subkelompok_id, bidang_id, barang_id, barang_ekonomis_thn, barang_jmldlmkemasan, lokasi_id, pemilikbarang_id', 'numerical', 'integerOnly'=>true),
			array('barang_harga, invasetlain_jumlah, invasetlain_harga, invasetlain_akumsusut', 'numerical'),
			array('inventaris_kode, no_register, asalaset_nama, golongan_kode, kelompok_kode, subkelompok_kode, bidang_kode, barang_type, barang_kode, barang_merk, barang_warna, barang_satuan, lokasiaset_kode, lokasiaset_namabagian, invasetlain_judulbuku, invasetlain_spesifikasibuku, invasetlain_asalkesenian, invasetlain_penciptakesenian, invasetlain_bahankesenian, invasetlain_jenishewan_tum, invasetlain_ukuranhewan_tum', 'length', 'max'=>50),
			array('golongan_nama, kelompok_nama, subkelompok_nama, bidang_nama, barang_nama, lokasiaset_namainstalasi, lokasiaset_namalokasi, pemilikbarang_nama, invasetlain_namabrg, invasetlain_ket', 'length', 'max'=>100),
			array('barang_noseri, barang_ukuran, barang_bahan, pemilikbarang_kode', 'length', 'max'=>20),
			array('barang_thnbeli, invasetlain_thncetak', 'length', 'max'=>5),
			array('barang_image', 'length', 'max'=>200),
			array('tgl_register, barang_statusregister, invasetlain_tglguna', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('inventaris_id, inventaris_kode, no_register, tgl_register, asalaset_id, asalaset_nama, golongan_id, golongan_kode, golongan_nama, kelompok_id, kelompok_kode, kelompok_nama, subkelompok_id, subkelompok_kode, subkelompok_nama, bidang_id, bidang_kode, bidang_nama, barang_id, barang_type, barang_kode, barang_nama, barang_merk, barang_noseri, barang_ukuran, barang_bahan, barang_thnbeli, barang_warna, barang_statusregister, barang_ekonomis_thn, barang_satuan, barang_jmldlmkemasan, barang_image, barang_harga, lokasi_id, lokasiaset_kode, lokasiaset_namainstalasi, lokasiaset_namabagian, lokasiaset_namalokasi, pemilikbarang_id, pemilikbarang_kode, pemilikbarang_nama, invasetlain_namabrg, invasetlain_judulbuku, invasetlain_spesifikasibuku, invasetlain_asalkesenian, invasetlain_jumlah, invasetlain_thncetak, invasetlain_harga, invasetlain_tglguna, invasetlain_akumsusut, invasetlain_ket, invasetlain_penciptakesenian, invasetlain_bahankesenian, invasetlain_jenishewan_tum, invasetlain_ukuranhewan_tum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'inventaris_id' => 'Inventaris',
			'inventaris_kode' => 'Inventaris Kode',
			'no_register' => 'No Register',
			'tgl_register' => 'Tgl Register',
			'asalaset_id' => 'Asalaset',
			'asalaset_nama' => 'Asalaset Nama',
			'golongan_id' => 'Golongan',
			'golongan_kode' => 'Golongan Kode',
			'golongan_nama' => 'Golongan Nama',
			'kelompok_id' => 'Kelompok',
			'kelompok_kode' => 'Kelompok Kode',
			'kelompok_nama' => 'Kelompok Nama',
			'subkelompok_id' => 'Subkelompok',
			'subkelompok_kode' => 'Subkelompok Kode',
			'subkelompok_nama' => 'Subkelompok Nama',
			'bidang_id' => 'Bidang',
			'bidang_kode' => 'Bidang Kode',
			'bidang_nama' => 'Bidang Nama',
			'barang_id' => 'Barang',
			'barang_type' => 'Barang Type',
			'barang_kode' => 'Barang Kode',
			'barang_nama' => 'Barang Nama',
			'barang_merk' => 'Barang Merk',
			'barang_noseri' => 'Barang Noseri',
			'barang_ukuran' => 'Barang Ukuran',
			'barang_bahan' => 'Barang Bahan',
			'barang_thnbeli' => 'Barang Thnbeli',
			'barang_warna' => 'Barang Warna',
			'barang_statusregister' => 'Barang Statusregister',
			'barang_ekonomis_thn' => 'Barang Ekonomis Thn',
			'barang_satuan' => 'Barang Satuan',
			'barang_jmldlmkemasan' => 'Barang Jmldlmkemasan',
			'barang_image' => 'Barang Image',
			'barang_harga' => 'Barang Harga',
			'lokasi_id' => 'Lokasi',
			'lokasiaset_kode' => 'Lokasiaset Kode',
			'lokasiaset_namainstalasi' => 'Lokasiaset Namainstalasi',
			'lokasiaset_namabagian' => 'Lokasiaset Namabagian',
			'lokasiaset_namalokasi' => 'Lokasiaset Namalokasi',
			'pemilikbarang_id' => 'Pemilikbarang',
			'pemilikbarang_kode' => 'Pemilikbarang Kode',
			'pemilikbarang_nama' => 'Pemilikbarang Nama',
			'invasetlain_namabrg' => 'Invasetlain Namabrg',
			'invasetlain_judulbuku' => 'Invasetlain Judulbuku',
			'invasetlain_spesifikasibuku' => 'Invasetlain Spesifikasibuku',
			'invasetlain_asalkesenian' => 'Invasetlain Asalkesenian',
			'invasetlain_jumlah' => 'Invasetlain Jumlah',
			'invasetlain_thncetak' => 'Invasetlain Thncetak',
			'invasetlain_harga' => 'Invasetlain Harga',
			'invasetlain_tglguna' => 'Invasetlain Tglguna',
			'invasetlain_akumsusut' => 'Invasetlain Akumsusut',
			'invasetlain_ket' => 'Invasetlain Ket',
			'invasetlain_penciptakesenian' => 'Invasetlain Penciptakesenian',
			'invasetlain_bahankesenian' => 'Invasetlain Bahankesenian',
			'invasetlain_jenishewan_tum' => 'Invasetlain Jenishewan Tum',
			'invasetlain_ukuranhewan_tum' => 'Invasetlain Ukuranhewan Tum',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('inventaris_id',$this->inventaris_id);
		$criteria->compare('inventaris_kode',$this->inventaris_kode,true);
		$criteria->compare('no_register',$this->no_register,true);
		$criteria->compare('tgl_register',$this->tgl_register,true);
		$criteria->compare('asalaset_id',$this->asalaset_id);
		$criteria->compare('asalaset_nama',$this->asalaset_nama,true);
		$criteria->compare('golongan_id',$this->golongan_id);
		$criteria->compare('golongan_kode',$this->golongan_kode,true);
		$criteria->compare('golongan_nama',$this->golongan_nama,true);
		$criteria->compare('kelompok_id',$this->kelompok_id);
		$criteria->compare('kelompok_kode',$this->kelompok_kode,true);
		$criteria->compare('kelompok_nama',$this->kelompok_nama,true);
		$criteria->compare('subkelompok_id',$this->subkelompok_id);
		$criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
		$criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
		$criteria->compare('bidang_id',$this->bidang_id);
		$criteria->compare('bidang_kode',$this->bidang_kode,true);
		$criteria->compare('bidang_nama',$this->bidang_nama,true);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('barang_type',$this->barang_type,true);
		$criteria->compare('barang_kode',$this->barang_kode,true);
		$criteria->compare('barang_nama',$this->barang_nama,true);
		$criteria->compare('barang_merk',$this->barang_merk,true);
		$criteria->compare('barang_noseri',$this->barang_noseri,true);
		$criteria->compare('barang_ukuran',$this->barang_ukuran,true);
		$criteria->compare('barang_bahan',$this->barang_bahan,true);
		$criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
		$criteria->compare('barang_warna',$this->barang_warna,true);
		$criteria->compare('barang_statusregister',$this->barang_statusregister);
		$criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
		$criteria->compare('barang_satuan',$this->barang_satuan,true);
		$criteria->compare('barang_jmldlmkemasan',$this->barang_jmldlmkemasan);
		$criteria->compare('barang_image',$this->barang_image,true);
		$criteria->compare('barang_harga',$this->barang_harga);
		$criteria->compare('lokasi_id',$this->lokasi_id);
		$criteria->compare('lokasiaset_kode',$this->lokasiaset_kode,true);
		$criteria->compare('lokasiaset_namainstalasi',$this->lokasiaset_namainstalasi,true);
		$criteria->compare('lokasiaset_namabagian',$this->lokasiaset_namabagian,true);
		$criteria->compare('lokasiaset_namalokasi',$this->lokasiaset_namalokasi,true);
		$criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
		$criteria->compare('pemilikbarang_kode',$this->pemilikbarang_kode,true);
		$criteria->compare('pemilikbarang_nama',$this->pemilikbarang_nama,true);
		$criteria->compare('invasetlain_namabrg',$this->invasetlain_namabrg,true);
		$criteria->compare('invasetlain_judulbuku',$this->invasetlain_judulbuku,true);
		$criteria->compare('invasetlain_spesifikasibuku',$this->invasetlain_spesifikasibuku,true);
		$criteria->compare('invasetlain_asalkesenian',$this->invasetlain_asalkesenian,true);
		$criteria->compare('invasetlain_jumlah',$this->invasetlain_jumlah);
		$criteria->compare('invasetlain_thncetak',$this->invasetlain_thncetak,true);
		$criteria->compare('invasetlain_harga',$this->invasetlain_harga);
		$criteria->compare('invasetlain_tglguna',$this->invasetlain_tglguna,true);
		$criteria->compare('invasetlain_akumsusut',$this->invasetlain_akumsusut);
		$criteria->compare('invasetlain_ket',$this->invasetlain_ket,true);
		$criteria->compare('invasetlain_penciptakesenian',$this->invasetlain_penciptakesenian,true);
		$criteria->compare('invasetlain_bahankesenian',$this->invasetlain_bahankesenian,true);
		$criteria->compare('invasetlain_jenishewan_tum',$this->invasetlain_jenishewan_tum,true);
		$criteria->compare('invasetlain_ukuranhewan_tum',$this->invasetlain_ukuranhewan_tum,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}