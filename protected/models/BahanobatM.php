<?php

/**
 * This is the model class for table "bahanobat_m".
 *
 * The followings are the available columns in table 'bahanobat_m':
 * @property integer $bahanobat_id
 * @property integer $obatalkesproduksi_id
 * @property integer $satuankecil_id
 * @property integer $obatalkes_id
 * @property double $permintaandosis
 * @property double $jmlkemasan
 * @property double $kekuatan
 * @property double $qtybahanobt
 *
 * The followings are the available model relations:
 * @property ObatalkesM $obatalkes
 * @property SatuankecilM $satuankecil
 * @property ObatalkesproduksiM $obatalkesproduksi
 */
class BahanobatM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BahanobatM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bahanobat_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('qtybahanobt', 'required'),
			array('obatalkesproduksi_id, satuankecil_id, obatalkes_id', 'numerical', 'integerOnly'=>true),
			array('permintaandosis, jmlkemasan, kekuatan, qtybahanobt', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('bahanobat_id, obatalkesproduksi_id, satuankecil_id, obatalkes_id, permintaandosis, jmlkemasan, kekuatan, qtybahanobt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'obatalkes' => array(self::BELONGS_TO, 'ObatalkesM', 'obatalkes_id'),
			'satuankecil' => array(self::BELONGS_TO, 'SatuankecilM', 'satuankecil_id'),
			'obatalkesproduksi' => array(self::BELONGS_TO, 'ObatalkesproduksiM', 'obatalkesproduksi_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bahanobat_id' => 'Bahanobat',
			'obatalkesproduksi_id' => 'Obatalkesproduksi',
			'satuankecil_id' => 'Satuankecil',
			'obatalkes_id' => 'Obatalkes',
			'permintaandosis' => 'Permintaandosis',
			'jmlkemasan' => 'Jmlkemasan',
			'kekuatan' => 'Kekuatan',
			'qtybahanobt' => 'Qtybahanobt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bahanobat_id',$this->bahanobat_id);
		$criteria->compare('obatalkesproduksi_id',$this->obatalkesproduksi_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('permintaandosis',$this->permintaandosis);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('qtybahanobt',$this->qtybahanobt);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}