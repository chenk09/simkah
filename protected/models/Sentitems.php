<?php

/**
 * This is the model class for table "sentitems".
 *
 * The followings are the available columns in table 'sentitems':
 * @property string $updatedindb
 * @property string $insertintodb
 * @property string $sendingdatetime
 * @property string $deliverydatetime
 * @property string $text
 * @property string $destinationnumber
 * @property string $coding
 * @property string $udh
 * @property string $smscnumber
 * @property integer $class
 * @property string $textdecoded
 * @property integer $id
 * @property string $senderid
 * @property integer $sequenceposition
 * @property string $status
 * @property integer $statuserror
 * @property integer $tpmr
 * @property integer $relativevalidity
 * @property string $creatorid
 */
class Sentitems extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sentitems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sentitems';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('updatedindb, text, udh, class, senderid, statuserror, tpmr, relativevalidity, creatorid', 'required'),
			array('class, sequenceposition, statuserror, tpmr, relativevalidity', 'numerical', 'integerOnly'=>true),
			array('destinationnumber, smscnumber', 'length', 'max'=>20),
			array('coding, senderid, status', 'length', 'max'=>255),
			array('insertintodb, sendingdatetime, deliverydatetime, textdecoded', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('updatedindb, insertintodb, sendingdatetime, deliverydatetime, text, destinationnumber, coding, udh, smscnumber, class, textdecoded, id, senderid, sequenceposition, status, statuserror, tpmr, relativevalidity, creatorid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'updatedindb' => 'Updatedindb',
			'insertintodb' => 'Insert ke Database',
			'sendingdatetime' => 'Waktu Terkirim',
			'deliverydatetime' => 'Deliverydatetime',
			'text' => 'Text',
			'destinationnumber' => 'Nomor Tujuan',
			'coding' => 'Coding',
			'udh' => 'Udh',
			'smscnumber' => 'Nomor SMS Center',
			'class' => 'Class',
			'textdecoded' => 'Isi Pesan Teks',
			'id' => 'ID',
			'senderid' => 'Senderid',
			'sequenceposition' => 'Sequenceposition',
			'status' => 'Status',
			'statuserror' => 'Statuserror',
			'tpmr' => 'Tpmr',
			'relativevalidity' => 'Relativevalidity',
			'creatorid' => 'Creatorid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->order = 'sendingdatetime DESC';
		$criteria->compare('LOWER(updatedindb)',strtolower($this->updatedindb),true);
		$criteria->compare('LOWER(insertintodb)',strtolower($this->insertintodb),true);
		$criteria->compare('LOWER(sendingdatetime)',strtolower($this->sendingdatetime),true);
		$criteria->compare('LOWER(deliverydatetime)',strtolower($this->deliverydatetime),true);
		$criteria->compare('LOWER(text)',strtolower($this->text),true);
		$criteria->compare('LOWER(destinationnumber)',strtolower($this->destinationnumber),true);
		$criteria->compare('LOWER(coding)',strtolower($this->coding),true);
		$criteria->compare('LOWER(udh)',strtolower($this->udh),true);
		$criteria->compare('LOWER(smscnumber)',strtolower($this->smscnumber),true);
		$criteria->compare('class',$this->class);
		$criteria->compare('LOWER(textdecoded)',strtolower($this->textdecoded),true);
		$criteria->compare('id',$this->id);
		$criteria->compare('LOWER(senderid)',strtolower($this->senderid),true);
		$criteria->compare('sequenceposition',$this->sequenceposition);
		$criteria->compare('LOWER(status)',strtolower($this->status),true);
		$criteria->compare('statuserror',$this->statuserror);
		$criteria->compare('tpmr',$this->tpmr);
		$criteria->compare('relativevalidity',$this->relativevalidity);
		$criteria->compare('LOWER(creatorid)',strtolower($this->creatorid),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(updatedindb)',strtolower($this->updatedindb),true);
		$criteria->compare('LOWER(insertintodb)',strtolower($this->insertintodb),true);
		$criteria->compare('LOWER(sendingdatetime)',strtolower($this->sendingdatetime),true);
		$criteria->compare('LOWER(deliverydatetime)',strtolower($this->deliverydatetime),true);
		$criteria->compare('LOWER(text)',strtolower($this->text),true);
		$criteria->compare('LOWER(destinationnumber)',strtolower($this->destinationnumber),true);
		$criteria->compare('LOWER(coding)',strtolower($this->coding),true);
		$criteria->compare('LOWER(udh)',strtolower($this->udh),true);
		$criteria->compare('LOWER(smscnumber)',strtolower($this->smscnumber),true);
		$criteria->compare('class',$this->class);
		$criteria->compare('LOWER(textdecoded)',strtolower($this->textdecoded),true);
		$criteria->compare('id',$this->id);
		$criteria->compare('LOWER(senderid)',strtolower($this->senderid),true);
		$criteria->compare('sequenceposition',$this->sequenceposition);
		$criteria->compare('LOWER(status)',strtolower($this->status),true);
		$criteria->compare('statuserror',$this->statuserror);
		$criteria->compare('tpmr',$this->tpmr);
		$criteria->compare('relativevalidity',$this->relativevalidity);
		$criteria->compare('LOWER(creatorid)',strtolower($this->creatorid),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}