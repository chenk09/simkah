<?php

/**
 * This is the model class for table "rl3_14_kegiatanrujukan_v".
 *
 * The followings are the available columns in table 'rl3_14_kegiatanrujukan_v':
 * @property double $tahun
 * @property integer $profilrs_id
 * @property string $nokode_rumahsakit
 * @property string $nama_rumahsakit
 * @property integer $jeniskasuspenyakit_id
 * @property string $jeniskasuspenyakit_nama
 * @property double $rujukanpuskesmas
 * @property double $rujukanfaskeslain
 * @property double $rujukanrs
 * @property double $kembalipuskesmas
 * @property double $kembalifaskeslain
 * @property double $kembalirslain
 * @property double $pasienrujukankeluar
 * @property double $pasiendatangsendiri
 * @property double $diterimakembali
 */
class Rl314KegiatanrujukanV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rl314KegiatanrujukanV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rl3_14_kegiatanrujukan_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profilrs_id, jeniskasuspenyakit_id', 'numerical', 'integerOnly'=>true),
			array('tahun, rujukanpuskesmas, rujukanfaskeslain, rujukanrs, kembalipuskesmas, kembalifaskeslain, kembalirslain, pasienrujukankeluar, pasiendatangsendiri, diterimakembali', 'numerical'),
			array('nokode_rumahsakit', 'length', 'max'=>10),
			array('nama_rumahsakit, jeniskasuspenyakit_nama', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tahun, profilrs_id, nokode_rumahsakit, nama_rumahsakit, jeniskasuspenyakit_id, jeniskasuspenyakit_nama, rujukanpuskesmas, rujukanfaskeslain, rujukanrs, kembalipuskesmas, kembalifaskeslain, kembalirslain, pasienrujukankeluar, pasiendatangsendiri, diterimakembali', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tahun' => 'Tahun',
			'profilrs_id' => 'Profilrs',
			'nokode_rumahsakit' => 'Nokode Rumahsakit',
			'nama_rumahsakit' => 'Nama Rumahsakit',
			'jeniskasuspenyakit_id' => 'Jeniskasuspenyakit',
			'jeniskasuspenyakit_nama' => 'Jeniskasuspenyakit Nama',
			'rujukanpuskesmas' => 'Rujukanpuskesmas',
			'rujukanfaskeslain' => 'Rujukanfaskeslain',
			'rujukanrs' => 'Rujukanrs',
			'kembalipuskesmas' => 'Kembalipuskesmas',
			'kembalifaskeslain' => 'Kembalifaskeslain',
			'kembalirslain' => 'Kembalirslain',
			'pasienrujukankeluar' => 'Pasienrujukankeluar',
			'pasiendatangsendiri' => 'Pasiendatangsendiri',
			'diterimakembali' => 'Diterimakembali',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('nokode_rumahsakit',$this->nokode_rumahsakit,true);
		$criteria->compare('nama_rumahsakit',$this->nama_rumahsakit,true);
		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
		$criteria->compare('jeniskasuspenyakit_nama',$this->jeniskasuspenyakit_nama,true);
		$criteria->compare('rujukanpuskesmas',$this->rujukanpuskesmas);
		$criteria->compare('rujukanfaskeslain',$this->rujukanfaskeslain);
		$criteria->compare('rujukanrs',$this->rujukanrs);
		$criteria->compare('kembalipuskesmas',$this->kembalipuskesmas);
		$criteria->compare('kembalifaskeslain',$this->kembalifaskeslain);
		$criteria->compare('kembalirslain',$this->kembalirslain);
		$criteria->compare('pasienrujukankeluar',$this->pasienrujukankeluar);
		$criteria->compare('pasiendatangsendiri',$this->pasiendatangsendiri);
		$criteria->compare('diterimakembali',$this->diterimakembali);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}