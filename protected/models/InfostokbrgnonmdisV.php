<?php
/**
* models/InfostokbrgnonmdisV.php : Model InfostokbrgnonmdisV untuk informasi Stok Barang
* Author    : Hardi
* Date      : 17-04-2014
* Issue     : EHJ-1602
**/

/**
 * This is the model class for table "infostokbrgnonmdis_v".
 *
 * The followings are the available columns in table 'infostokbrgnonmdis_v':
 * @property integer $barang_id
 * @property string $barang_type
 * @property string $barang_kode
 * @property string $barang_nama
 * @property string $barang_merk
 * @property string $barang_ukuran
 * @property string $barang_bahan
 * @property string $barang_warna
 * @property string $barang_satuan
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 * @property string $inventarisasi_kode
 * @property double $qty
 * @property string $inventarisasi_keadaan
 * @property string $inventarisasi_keterangan
 */
class InfostokbrgnonmdisV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InfostokbrgnonmdisV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'infostokbrgnonmdis_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('barang_id, ruangan_id', 'numerical', 'integerOnly'=>true),
			array('qty', 'numerical'),
			array('barang_type, barang_kode, barang_merk, barang_warna, barang_satuan, ruangan_nama, inventarisasi_kode, inventarisasi_keadaan', 'length', 'max'=>50),
			array('barang_nama', 'length', 'max'=>100),
			array('barang_ukuran, barang_bahan', 'length', 'max'=>20),
			array('inventarisasi_keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('barang_id, barang_type, barang_kode, barang_nama, barang_merk, barang_ukuran, barang_bahan, barang_warna, barang_satuan, ruangan_id, ruangan_nama, inventarisasi_kode, qty, inventarisasi_keadaan, inventarisasi_keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'barang_id' => 'Barang',
			'barang_type' => 'Type Barang',
			'barang_kode' => 'Kode Barang',
			'barang_nama' => 'Nama Barang',
			'barang_merk' => 'Merk',
			'barang_ukuran' => 'Ukuran Barang',
			'barang_bahan' => 'Bahan Barang',
			'barang_warna' => 'Warna Barang',
			'barang_satuan' => 'Satuan',
			'ruangan_id' => 'Ruangan',
			'ruangan_nama' => 'Ruangan',
			'inventarisasi_kode' => 'Kode Inventarisasi',
			'qty' => 'Qty',
			'inventarisasi_keadaan' => 'Inventarisasi Keadaan',
			'inventarisasi_keterangan' => 'Inventarisasi Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('LOWER(barang_type)',strtolower($this->barang_type),true);
		$criteria->compare('LOWER(barang_kode)',strtolower($this->barang_kode),true);
		$criteria->compare('LOWER(barang_nama)',strtolower($this->barang_nama),true);
		$criteria->compare('LOWER(barang_merk)',strtolower($this->barang_merk),true);
		$criteria->compare('LOWER(barang_ukuran)',strtolower($this->barang_ukuran),true);
		$criteria->compare('LOWER(barang_bahan)',strtolower($this->barang_bahan),true);
		$criteria->compare('LOWER(barang_warna)',strtolower($this->barang_warna),true);
		$criteria->compare('LOWER(barang_satuan)',strtolower($this->barang_satuan),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(inventarisasi_kode)',strtolower($this->inventarisasi_kode),true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('LOWER(inventarisasi_keadaan)',strtolower($this->inventarisasi_keadaan),true);
		$criteria->compare('LOWER(inventarisasi_keterangan)',strtolower($this->inventarisasi_keterangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('LOWER(barang_type)',strtolower($this->barang_type),true);
		$criteria->compare('LOWER(barang_kode)',strtolower($this->barang_kode),true);
		$criteria->compare('LOWER(barang_nama)',strtolower($this->barang_nama),true);
		$criteria->compare('LOWER(barang_merk)',strtolower($this->barang_merk),true);
		$criteria->compare('LOWER(barang_ukuran)',strtolower($this->barang_ukuran),true);
		$criteria->compare('LOWER(barang_bahan)',strtolower($this->barang_bahan),true);
		$criteria->compare('LOWER(barang_warna)',strtolower($this->barang_warna),true);
		$criteria->compare('LOWER(barang_satuan)',strtolower($this->barang_satuan),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(inventarisasi_kode)',strtolower($this->inventarisasi_kode),true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('LOWER(inventarisasi_keadaan)',strtolower($this->inventarisasi_keadaan),true);
		$criteria->compare('LOWER(inventarisasi_keterangan)',strtolower($this->inventarisasi_keterangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}