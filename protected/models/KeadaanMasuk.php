<?php

class KeadaanMasuk extends LookupM
{
    private static $_items=array();
    private static $_type = 'keadaanmasuk';
    /**
     * Returns the items for the specified type.
     * @param string item type (e.g. 'PostStatus').
     * @return array item names indexed by item code. The items are order by their position values.
     * An empty array is returned if the item type does not exist.
     */
    public static function items($type=null)
    {
        $type = self::$_type;
            if(!isset(self::$_items[$type]))
                    self::loadItems($type);
            return self::$_items[$type];
    }

    /**
     * Returns the item name for the specified type and code.
     * @param string the item type (e.g. 'PostStatus').
     * @param integer the item code (corresponding to the 'code' column value)
     * @return string the item name for the specified the code. False is returned if the item type or code does not exist.
     */
    public static function item($type,$code)
    {
            if(!isset(self::$_items[$type]))
                    self::loadItems($type);
            return isset(self::$_items[$type][$code]) ? self::$_items[$type][$code] : false;
    }

    /**
     * Loads the lookup items for the specified type from the database.
     * @param string the item type
     */
    private static function loadItems($type)
    {
            self::$_items[$type]=array();
            $models=self::model()->findAll(array(
                    'condition'=>'lookup_type=:type',
                    'params'=>array(':type'=>$type),
                    'order'=>'lookup_urutan',
            ));
            foreach($models as $model)
                    self::$_items[$type][$model->lookup_value]=$model->lookup_name;
    }
    
      
    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchKeadaanMasuk()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
                
               $this->lookup_type = self::$_type;
		$criteria=new CDbCriteria;
                
		$criteria->compare('lookup_id',$this->lookup_id);
		$criteria->compare('LOWER(lookup_name)',strtolower($this->lookup_name),true);
		$criteria->compare('LOWER(lookup_value)',strtolower($this->lookup_value),true);
		$criteria->compare('LOWER(lookup_type)',strtolower($this->lookup_type),true);
		$criteria->compare('lookup_urutan',$this->lookup_urutan);
		$criteria->compare('LOWER(lookup_kode)',strtolower($this->lookup_kode),true);
		$criteria->compare('lookup_aktif',isset($this->lookup_aktif)?$this->lookup_aktif:true);
//                $criteria->order = 'lookup_type, lookup_urutan';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function searchPrint()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
                
        $this->lookup_type = self::$_type;
		$criteria=new CDbCriteria;
                
		$criteria->compare('lookup_id',$this->lookup_id);
		$criteria->compare('LOWER(lookup_name)',strtolower($this->lookup_name),true);
		$criteria->compare('LOWER(lookup_value)',strtolower($this->lookup_value),true);
		$criteria->compare('LOWER(lookup_type)',strtolower($this->lookup_type),true);
//		$criteria->compare('lookup_urutan',$this->lookup_urutan);
		$criteria->compare('LOWER(lookup_kode)',strtolower($this->lookup_kode),true);
        $criteria->compare('lookup_aktif',isset($this->lookup_aktif)?$this->lookup_aktif:true);
//		$criteria->compare('lookup_aktif',$this->lookup_aktif);
                $criteria->order = 'lookup_type, lookup_urutan';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
?>
