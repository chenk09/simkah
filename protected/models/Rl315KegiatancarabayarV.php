<?php

/**
 * This is the model class for table "rl3_15_kegiatancarabayar_v".
 *
 * The followings are the available columns in table 'rl3_15_kegiatancarabayar_v':
 * @property double $tahun
 * @property integer $profilrs_id
 * @property string $nokode_rumahsakit
 * @property string $nama_rumahsakit
 * @property integer $carabayar_id
 * @property string $carabayar_nama
 * @property integer $penjamin_id
 * @property string $penjamin_nama
 * @property double $pasienrikeluar
 * @property double $pasienrilamadirawat
 * @property double $pasienrj
 * @property double $pasienrjlab
 * @property double $pasienrjrad
 * @property double $pasienrjlain
 */
class Rl315KegiatancarabayarV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rl315KegiatancarabayarV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rl3_15_kegiatancarabayar_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profilrs_id, carabayar_id, penjamin_id', 'numerical', 'integerOnly'=>true),
			array('tahun, pasienrikeluar, pasienrilamadirawat, pasienrj, pasienrjlab, pasienrjrad, pasienrjlain', 'numerical'),
			array('nokode_rumahsakit', 'length', 'max'=>10),
			array('nama_rumahsakit', 'length', 'max'=>100),
			array('carabayar_nama, penjamin_nama', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tahun, profilrs_id, nokode_rumahsakit, nama_rumahsakit, carabayar_id, carabayar_nama, penjamin_id, penjamin_nama, pasienrikeluar, pasienrilamadirawat, pasienrj, pasienrjlab, pasienrjrad, pasienrjlain', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tahun' => 'Tahun',
			'profilrs_id' => 'Profilrs',
			'nokode_rumahsakit' => 'Nokode Rumahsakit',
			'nama_rumahsakit' => 'Nama Rumahsakit',
			'carabayar_id' => 'Carabayar',
			'carabayar_nama' => 'Carabayar Nama',
			'penjamin_id' => 'Penjamin',
			'penjamin_nama' => 'Penjamin Nama',
			'pasienrikeluar' => 'Pasienrikeluar',
			'pasienrilamadirawat' => 'Pasienrilamadirawat',
			'pasienrj' => 'Pasienrj',
			'pasienrjlab' => 'Pasienrjlab',
			'pasienrjrad' => 'Pasienrjrad',
			'pasienrjlain' => 'Pasienrjlain',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('nokode_rumahsakit',$this->nokode_rumahsakit,true);
		$criteria->compare('nama_rumahsakit',$this->nama_rumahsakit,true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('carabayar_nama',$this->carabayar_nama,true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('penjamin_nama',$this->penjamin_nama,true);
		$criteria->compare('pasienrikeluar',$this->pasienrikeluar);
		$criteria->compare('pasienrilamadirawat',$this->pasienrilamadirawat);
		$criteria->compare('pasienrj',$this->pasienrj);
		$criteria->compare('pasienrjlab',$this->pasienrjlab);
		$criteria->compare('pasienrjrad',$this->pasienrjrad);
		$criteria->compare('pasienrjlain',$this->pasienrjlain);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}