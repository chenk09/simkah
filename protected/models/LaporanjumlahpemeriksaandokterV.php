<?php

/**
 * This is the model class for table "laporanjumlahpemeriksaandokter_v".
 *
 * The followings are the available columns in table 'laporanjumlahpemeriksaandokter_v':
 * @property string $no_pendaftaran
 * @property string $no_rekam_medik
 * @property string $nama_pasien
 * @property string $instalasi_nama
 * @property string $ruangan_nama
 * @property string $daftartindakan_nama
 * @property string $dokter_id
 * @property string $dokter_nama
 * @property string $penjamin_nama
 * @property double $tarif_satuan
 * @property integer $qty_tindakan
 * @property double $tarif_tindakan
 * @property string $tgl_tindakan
 * @property integer $penjamin_id
 * @property string $gelardepan
 * @property string $gelarbelakang_nama
 * @property integer $ruangan_id
 * @property integer $instalasi_id
 * @property string $statusdokter
 */
class LaporanjumlahpemeriksaandokterV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanjumlahpemeriksaandokterV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanjumlahpemeriksaandokter_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('qty_tindakan, penjamin_id, ruangan_id, instalasi_id', 'numerical', 'integerOnly'=>true),
			array('tarif_satuan, tarif_tindakan', 'numerical'),
			array('no_pendaftaran', 'length', 'max'=>20),
			array('no_rekam_medik, gelardepan', 'length', 'max'=>10),
			array('nama_pasien, instalasi_nama, ruangan_nama, dokter_nama, penjamin_nama', 'length', 'max'=>50),
			array('daftartindakan_nama', 'length', 'max'=>200),
			array('gelarbelakang_nama', 'length', 'max'=>15),
			array('dokter_id, tgl_tindakan, statusdokter', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('no_pendaftaran, no_rekam_medik, nama_pasien, instalasi_nama, ruangan_nama, daftartindakan_nama, dokter_id, dokter_nama, penjamin_nama, tarif_satuan, qty_tindakan, tarif_tindakan, tgl_tindakan, penjamin_id, gelardepan, gelarbelakang_nama, ruangan_id, instalasi_id, statusdokter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_pendaftaran' => 'No Pendaftaran',
			'no_rekam_medik' => 'No Rekam Medik',
			'nama_pasien' => 'Nama Pasien',
			'instalasi_nama' => 'Instalasi Nama',
			'ruangan_nama' => 'Ruangan Nama',
			'daftartindakan_nama' => 'Daftartindakan Nama',
			'dokter_id' => 'Dokter',
			'dokter_nama' => 'Dokter Nama',
			'penjamin_nama' => 'Penjamin Nama',
			'tarif_satuan' => 'Tarif Satuan',
			'qty_tindakan' => 'Qty Tindakan',
			'tarif_tindakan' => 'Tarif Tindakan',
			'tgl_tindakan' => 'Tgl Tindakan',
			'penjamin_id' => 'Penjamin',
			'gelardepan' => 'Gelardepan',
			'gelarbelakang_nama' => 'Gelarbelakang Nama',
			'ruangan_id' => 'Ruangan',
			'instalasi_id' => 'Instalasi',
			'statusdokter' => 'Statusdokter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
		$criteria->compare('LOWER(dokter_id)',strtolower($this->dokter_id),true);
		$criteria->compare('LOWER(dokter_nama)',strtolower($this->dokter_nama),true);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('tarif_satuan',$this->tarif_satuan);
		$criteria->compare('qty_tindakan',$this->qty_tindakan);
		$criteria->compare('tarif_tindakan',$this->tarif_tindakan);
		$criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(statusdokter)',strtolower($this->statusdokter),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
		$criteria->compare('LOWER(dokter_id)',strtolower($this->dokter_id),true);
		$criteria->compare('LOWER(dokter_nama)',strtolower($this->dokter_nama),true);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('tarif_satuan',$this->tarif_satuan);
		$criteria->compare('qty_tindakan',$this->qty_tindakan);
		$criteria->compare('tarif_tindakan',$this->tarif_tindakan);
		$criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(statusdokter)',strtolower($this->statusdokter),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}