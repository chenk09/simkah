<?php

/**
 * This is the model class for table "laporanjurnal_v".
 *
 * The followings are the available columns in table 'laporanjurnal_v':
 * @property string $tgljurnalpost
 * @property string $tglbuktijurnal
 * @property string $nobuktijurnal
 * @property string $uraiantransaksi
 * @property string $kode_rekening
 * @property string $nmrekening5
 * @property double $saldodebit
 * @property double $saldokredit
 * @property string $catatan
 * @property integer $jenisjurnal_id
 * @property string $jenisjurnal_nama
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 */
class LaporanjurnalV extends CActiveRecord
{
	public $tglAwal, $tglAkhir;
        public $data, $jumlah, $tick;
        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanjurnalV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanjurnal_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenisjurnal_id, ruangan_id', 'numerical', 'integerOnly'=>true),
			array('saldodebit, saldokredit', 'numerical'),
			array('nobuktijurnal, ruangan_nama', 'length', 'max'=>50),
			array('uraiantransaksi', 'length', 'max'=>250),
			array('nmrekening5', 'length', 'max'=>500),
			array('jenisjurnal_nama', 'length', 'max'=>100),
			array('tgljurnalpost, tglbuktijurnal, kode_rekening, catatan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tgljurnalpost, tglbuktijurnal, nobuktijurnal, uraiantransaksi, kode_rekening, nmrekening5, saldodebit, saldokredit, catatan, jenisjurnal_id, jenisjurnal_nama, ruangan_id, ruangan_nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tgljurnalpost' => 'Tgljurnalpost',
			'tglbuktijurnal' => 'Tglbuktijurnal',
			'nobuktijurnal' => 'Nobuktijurnal',
			'uraiantransaksi' => 'Uraiantransaksi',
			'kode_rekening' => 'Kode Rekening',
			'nmrekening5' => 'Nmrekening5',
			'saldodebit' => 'Saldodebit',
			'saldokredit' => 'Saldokredit',
			'catatan' => 'Catatan',
			'jenisjurnal_id' => 'Jenisjurnal',
			'jenisjurnal_nama' => 'Jenisjurnal Nama',
			'ruangan_id' => 'Ruangan',
			'ruangan_nama' => 'Ruangan Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(tgljurnalpost)',strtolower($this->tgljurnalpost),true);
		$criteria->compare('LOWER(tglbuktijurnal)',strtolower($this->tglbuktijurnal),true);
		$criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
		$criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
		$criteria->compare('LOWER(kode_rekening)',strtolower($this->kode_rekening),true);
		$criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
		$criteria->compare('saldodebit',$this->saldodebit);
		$criteria->compare('saldokredit',$this->saldokredit);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
		$criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
		$criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('LOWER(tgljurnalpost)',strtolower($this->tgljurnalpost),true);
		$criteria->compare('LOWER(tglbuktijurnal)',strtolower($this->tglbuktijurnal),true);
		$criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
		$criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
		$criteria->compare('LOWER(kode_rekening)',strtolower($this->kode_rekening),true);
		$criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
		$criteria->compare('saldodebit',$this->saldodebit);
		$criteria->compare('saldokredit',$this->saldokredit);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
		$criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
		$criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}