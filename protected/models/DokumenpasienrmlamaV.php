<?php

/**
 * This is the model class for table "dokumenpasienrmlama".
 *
 * The followings are the available columns in table 'dokumenpasienrmlama':
 * @property integer $warnadokrm_id
 * @property string $warnadokrm_namawarna
 * @property string $warnadokrm_kodewarna
 * @property integer $lokasirak_id
 * @property string $lokasirak_nama
 * @property string $nodokumenrm
 * @property string $tglrekammedis
 * @property integer $pasien_id
 * @property string $no_rekam_medik
 * @property string $tgl_rekam_medik
 * @property string $nama_pasien
 * @property string $nama_bin
 * @property string $jeniskelamin
 * @property string $tanggal_lahir
 * @property string $alamat_pasien
 * @property string $tempat_lahir
 * @property string $tglmasukrak
 * @property string $statusrekammedis
 * @property string $nomortertier
 * @property string $nomorsekunder
 * @property string $nomorprimer
 * @property string $warnanorm_i
 * @property string $warnanorm_ii
 * @property string $tglkeluarakhir
 * @property string $tglmasukakhir
 * @property integer $dokrekammedis_id
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 * @property string $no_pendaftaran
 * @property integer $instalasi_id
 * @property string $instalasi_nama
 * @property integer $pendaftaran_id
 */
class DokumenpasienrmlamaV extends CActiveRecord
{
        public $printArray;
        public $tgl_rekam_medik_akhir; 
        public $no_rekam_medik_akhir; 
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DokumenpasienrmlamaV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dokumenpasienrmlama_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('warnadokrm_id, lokasirak_id, pasien_id, dokrekammedis_id, ruangan_id, pendaftaran_id, subrak_id, peminjamanrm_id, pengirimanrm_id', 'numerical', 'integerOnly'=>true),
			array('warnadokrm_namawarna, warnadokrm_kodewarna, nodokumenrm, jeniskelamin, no_pendaftaran', 'length', 'max'=>20),
			array('lokasirak_nama', 'length', 'max'=>100),
			array('no_rekam_medik, statusrekammedis', 'length', 'max'=>10),
			array('nama_pasien, warnanorm_i, warnanorm_ii, ruangan_nama, instalasi_nama', 'length', 'max'=>50),
			array('nama_bin, subrak_nama', 'length', 'max'=>30),
			array('tempat_lahir', 'length', 'max'=>25),
			array('nomortertier, nomorsekunder, nomorprimer', 'length', 'max'=>2),
			array('tglrekammedis, tgl_rekam_medik, tanggal_lahir, alamat_pasien, tglmasukrak, tglkeluarakhir, tglmasukakhir, printpeminjaman', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('printArray, tgl_rekam_medik_akhir, no_rekam_medik_akhir, warnadokrm_id, warnadokrm_namawarna, warnadokrm_kodewarna, lokasirak_id, lokasirak_nama, nodokumenrm, tglrekammedis, pasien_id, no_rekam_medik, tgl_rekam_medik, nama_pasien, nama_bin, jeniskelamin, tanggal_lahir, alamat_pasien, tempat_lahir, tglmasukrak, statusrekammedis, nomortertier, nomorsekunder, nomorprimer, warnanorm_i, warnanorm_ii, tglkeluarakhir, tglmasukakhir, dokrekammedis_id, ruangan_id, ruangan_nama, no_pendaftaran, instalasi_id, instalasi_nama, pendaftaran_id, peminjamanrm_id, pengirimanrm_id, printpeminjaman', 'safe',  'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'dokumenrekammedis'=>array(self::BELONGS_TO, 'DokrekammedisM', 'dokrekammedis_id'),
                    'subrak'=>  array(self::HAS_ONE, 'SubrakM', array('subrak_id'=>'subrak_id'), 'through'=>'dokumenrekammedis'),
                    'pendaftaran'=>array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
                    //'peminjaman'=>array(self::HAS_ONE, 'PeminjamanrmT', array('dokrekammedis_id'=>'dokrekammedis_id'), 'through'=>'dokumenrekammedis'),
                    'pengiriman'=>array(self::HAS_ONE, 'PengirimanrmT', array('pengirimanrm_id'=>'pengirimanrm_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'warnadokrm_id' => 'Warna Dokumen RM',
			'warnadokrm_namawarna' => 'Nama Warna',
			'warnadokrm_kodewarna' => 'Kode Warna',
			'lokasirak_id' => 'Lokasi Rak',
			'lokasirak_nama' => 'Nama Lokasi Rak',
			'nodokumenrm' => 'No Dokumen RM',
			'tglrekammedis' => 'Tanggal Rekam Medis',
			'pasien_id' => 'Pasien',
			'no_rekam_medik' => 'No Rekam Medik',
			'tgl_rekam_medik' => 'Tgl Rekam Medik',
			'nama_pasien' => 'Nama Pasien',
			'nama_bin' => 'Nama Bin',
			'jeniskelamin' => 'Jenis Kelamin',
			'tanggal_lahir' => 'Tanggal Lahir',
			'alamat_pasien' => 'Alamat Pasien',
			'tempat_lahir' => 'Tempat Lahir',
			'tglmasukrak' => 'Tanggal Masuk Rak',
			'statusrekammedis' => 'Status Rekam Medis',
			'nomortertier' => 'Nomor Tertier',
			'nomorsekunder' => 'Nomor Sekunder',
			'nomorprimer' => 'Nomor Primer',
			'warnanorm_i' => 'Warna Norm I',
			'warnanorm_ii' => 'Warna Norm II',
			'tglkeluarakhir' => 'Tanggal Terakhir Keluar',
			'tglmasukakhir' => 'Tanggal Terakhir Masuk',
			'dokrekammedis_id' => 'Dokumen Rekam Medis',
			'ruangan_id' => 'Ruangan',
			'ruangan_nama' => 'Ruangan Nama',
			'no_pendaftaran' => 'No Pendaftaran',
			'instalasi_id' => 'Instalasi',
			'instalasi_nama' => 'Instalasi Nama',
			'pendaftaran_id' => 'Pendaftaran',
                        'subrak_id' => 'Subrak',
			'subrak_nama' => 'Subrak Nama',
			'peminjamanrm_id' => 'Peminjaman RM',
			'pengirimanrm_id' => 'Pengiriman RM',
			'printpeminjaman' => 'Print Peminjaman',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('warnadokrm_id',$this->warnadokrm_id);
		$criteria->compare('LOWER(warnadokrm_namawarna)',strtolower($this->warnadokrm_namawarna),true);
		$criteria->compare('LOWER(warnadokrm_kodewarna)',strtolower($this->warnadokrm_kodewarna),true);
		$criteria->compare('lokasirak_id',$this->lokasirak_id);
		$criteria->compare('LOWER(lokasirak_nama)',strtolower($this->lokasirak_nama),true);
		$criteria->compare('LOWER(nodokumenrm)',strtolower($this->nodokumenrm),true);
		$criteria->compare('LOWER(tglrekammedis)',strtolower($this->tglrekammedis),true);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tglmasukrak)',strtolower($this->tglmasukrak),true);
		$criteria->compare('LOWER(statusrekammedis)',strtolower($this->statusrekammedis),true);
		$criteria->compare('LOWER(nomortertier)',strtolower($this->nomortertier),true);
		$criteria->compare('LOWER(nomorsekunder)',strtolower($this->nomorsekunder),true);
		$criteria->compare('LOWER(nomorprimer)',strtolower($this->nomorprimer),true);
		$criteria->compare('LOWER(warnanorm_i)',strtolower($this->warnanorm_i),true);
		$criteria->compare('LOWER(warnanorm_ii)',strtolower($this->warnanorm_ii),true);
		$criteria->compare('LOWER(tglkeluarakhir)',strtolower($this->tglkeluarakhir),true);
		$criteria->compare('LOWER(tglmasukakhir)',strtolower($this->tglmasukakhir),true);
		$criteria->compare('dokrekammedis_id',$this->dokrekammedis_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('warnadokrm_id',$this->warnadokrm_id);
		$criteria->compare('LOWER(warnadokrm_namawarna)',strtolower($this->warnadokrm_namawarna),true);
		$criteria->compare('LOWER(warnadokrm_kodewarna)',strtolower($this->warnadokrm_kodewarna),true);
		$criteria->compare('lokasirak_id',$this->lokasirak_id);
		$criteria->compare('LOWER(lokasirak_nama)',strtolower($this->lokasirak_nama),true);
		$criteria->compare('LOWER(nodokumenrm)',strtolower($this->nodokumenrm),true);
		$criteria->compare('LOWER(tglrekammedis)',strtolower($this->tglrekammedis),true);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tglmasukrak)',strtolower($this->tglmasukrak),true);
		$criteria->compare('LOWER(statusrekammedis)',strtolower($this->statusrekammedis),true);
		$criteria->compare('LOWER(nomortertier)',strtolower($this->nomortertier),true);
		$criteria->compare('LOWER(nomorsekunder)',strtolower($this->nomorsekunder),true);
		$criteria->compare('LOWER(nomorprimer)',strtolower($this->nomorprimer),true);
		$criteria->compare('LOWER(warnanorm_i)',strtolower($this->warnanorm_i),true);
		$criteria->compare('LOWER(warnanorm_ii)',strtolower($this->warnanorm_ii),true);
		$criteria->compare('LOWER(tglkeluarakhir)',strtolower($this->tglkeluarakhir),true);
		$criteria->compare('LOWER(tglmasukakhir)',strtolower($this->tglmasukakhir),true);
		$criteria->compare('dokrekammedis_id',$this->dokrekammedis_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date')
                        {
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                        }
                     else if ($column->dbType == 'timestamp without time zone')
                        {
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                        }    
            }

            return parent::beforeValidate ();
        }

        public function beforeSave() {         
            if($this->tglrekammedis===null || trim($this->tglrekammedis)==''){
	        $this->setAttribute('tglrekammedis', null);
            }
            if($this->tglkeluarakhir===null || trim($this->tglkeluarakhir)==''){
	        $this->setAttribute('tglkeluarakhir', null);
            }
            if($this->tglmasukakhir===null || trim($this->tglmasukakhir)==''){
	        $this->setAttribute('tglmasukakhir', null);
            }
            if($this->tanggal_lahir===null || trim($this->tanggal_lahir)==''){
	        $this->setAttribute('tanggal_lahir', null);
            }
            if($this->tglmasukrak===null || trim($this->tglmasukrak)==''){
	        $this->setAttribute('tglmasukrak', null);
            }
            if($this->tgl_rekam_medik===null || trim($this->tgl_rekam_medik)==''){
	        $this->setAttribute('tgl_rekam_medik', null);
            }
            return parent::beforeSave();
        }

        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                        }
            }
            return true;
        }
        
        public function primaryKey() {
            return 'pendaftaran_id';
        }
}