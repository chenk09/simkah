<?php

/**
 * This is the model class for table "antrianpasienfarmasi_t".
 *
 * The followings are the available columns in table 'antrianpasienfarmasi_t':
 * @property integer $antrianpasienfarmasi_id
 * @property integer $lokasiantrian_id
 * @property integer $jenisantrian_id
 * @property integer $pendaftaran_id
 * @property integer $pasien_id
 * @property string $nourutantrian
 * @property string $tglantrianpasien
 * @property string $tglpemanggilanpasien
 * @property integer $jmlpemanggilan
 * @property boolean $is_dilayani
 * @property boolean $is_batalantrian
 * @property string $create_time
 * @property integer $create_loginpemakai_id
 *
 * The followings are the available model relations:
 * @property JenisantrianM $jenisantrian
 * @property LokasiantrianM $lokasiantrian
 * @property PasienM $pasien
 * @property PendaftaranT $pendaftaran
 */
class AntrianpasienfarmasiT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AntrianpasienfarmasiT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'antrianpasienfarmasi_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lokasiantrian_id, jenisantrian_id, nourutantrian, tglantrianpasien', 'required'),
			array('lokasiantrian_id, jenisantrian_id, pendaftaran_id, pasien_id, jmlpemanggilan, create_loginpemakai_id', 'numerical', 'integerOnly'=>true),
			array('nourutantrian', 'length', 'max'=>5),
			array('tglpemanggilanpasien, is_dilayani, is_batalantrian, create_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('antrianpasienfarmasi_id, lokasiantrian_id, jenisantrian_id, pendaftaran_id, pasien_id, nourutantrian, tglantrianpasien, tglpemanggilanpasien, jmlpemanggilan, is_dilayani, is_batalantrian, create_time, create_loginpemakai_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisantrian' => array(self::BELONGS_TO, 'JenisantrianM', 'jenisantrian_id'),
			'lokasiantrian' => array(self::BELONGS_TO, 'LokasiantrianM', 'lokasiantrian_id'),
			'pasien' => array(self::BELONGS_TO, 'PasienM', 'pasien_id'),
			'pendaftaran' => array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'antrianpasienfarmasi_id' => 'Antrianpasienfarmasi',
			'lokasiantrian_id' => 'Lokasiantrian',
			'jenisantrian_id' => 'Jenisantrian',
			'pendaftaran_id' => 'Pendaftaran',
			'pasien_id' => 'Pasien',
			'nourutantrian' => 'Nourutantrian',
			'tglantrianpasien' => 'Tglantrianpasien',
			'tglpemanggilanpasien' => 'Tglpemanggilanpasien',
			'jmlpemanggilan' => 'Jmlpemanggilan',
			'is_dilayani' => 'Is Dilayani',
			'is_batalantrian' => 'Is Batalantrian',
			'create_time' => 'Create Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('antrianpasienfarmasi_id',$this->antrianpasienfarmasi_id);
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(nourutantrian)',strtolower($this->nourutantrian),true);
		$criteria->compare('LOWER(tglantrianpasien)',strtolower($this->tglantrianpasien),true);
		$criteria->compare('LOWER(tglpemanggilanpasien)',strtolower($this->tglpemanggilanpasien),true);
		$criteria->compare('jmlpemanggilan',$this->jmlpemanggilan);
		$criteria->compare('is_dilayani',$this->is_dilayani);
		$criteria->compare('is_batalantrian',$this->is_batalantrian);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('antrianpasienfarmasi_id',$this->antrianpasienfarmasi_id);
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(nourutantrian)',strtolower($this->nourutantrian),true);
		$criteria->compare('LOWER(tglantrianpasien)',strtolower($this->tglantrianpasien),true);
		$criteria->compare('LOWER(tglpemanggilanpasien)',strtolower($this->tglpemanggilanpasien),true);
		$criteria->compare('jmlpemanggilan',$this->jmlpemanggilan);
		$criteria->compare('is_dilayani',$this->is_dilayani);
		$criteria->compare('is_batalantrian',$this->is_batalantrian);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}