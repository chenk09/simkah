<?php

/**
 * This is the model class for table "antrianfarmasi_pemanggilanpasien_v".
 *
 * The followings are the available columns in table 'antrianfarmasi_pemanggilanpasien_v':
 * @property integer $antrianpasienfarmasi_id
 * @property string $nourutantrian
 * @property integer $pendaftaran_id
 * @property string $no_pendaftaran
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 * @property integer $pasien_id
 * @property string $no_rekam_medik
 * @property string $nama_pasien
 * @property string $alamat_pasien
 * @property integer $jenisantrian_id
 * @property string $jenisantrian_nama
 * @property boolean $is_dilayani
 */
class AntrianfarmasiPemanggilanpasienV extends CActiveRecord
{
        public $tglAwal,$tglAkhir;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AntrianfarmasiPemanggilanpasienV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'antrianfarmasi_pemanggilanpasien_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('antrianpasienfarmasi_id, pendaftaran_id, ruangan_id, pasien_id, jenisantrian_id', 'numerical', 'integerOnly'=>true),
			array('nourutantrian', 'length', 'max'=>5),
			array('no_pendaftaran', 'length', 'max'=>20),
			array('ruangan_nama, nama_pasien', 'length', 'max'=>50),
			array('no_rekam_medik', 'length', 'max'=>10),
			array('jenisantrian_nama', 'length', 'max'=>250),
			array('alamat_pasien, is_dilayani', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('antrianpasienfarmasi_id, nourutantrian, pendaftaran_id, no_pendaftaran, ruangan_id, ruangan_nama, pasien_id, no_rekam_medik, nama_pasien, alamat_pasien, jenisantrian_id, jenisantrian_nama, is_dilayani', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'antrianpasienfarmasi_id' => 'Antrianpasienfarmasi',
			'nourutantrian' => 'Nourutantrian',
			'pendaftaran_id' => 'Pendaftaran',
			'no_pendaftaran' => 'No Pendaftaran',
			'ruangan_id' => 'Ruangan',
			'ruangan_nama' => 'Ruangan Nama',
			'pasien_id' => 'Pasien',
			'no_rekam_medik' => 'No Rekam Medik',
			'nama_pasien' => 'Nama Pasien',
			'alamat_pasien' => 'Alamat Pasien',
			'jenisantrian_id' => 'Jenisantrian',
			'jenisantrian_nama' => 'Jenisantrian Nama',
			'is_dilayani' => 'Is Dilayani',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->addBetweenCondition('DATE(tglantrianpasien)',  $this->tglAwal, $this->tglAkhir);
		$criteria->compare('antrianpasienfarmasi_id',$this->antrianpasienfarmasi_id);
		$criteria->compare('LOWER(nourutantrian)',strtolower($this->nourutantrian),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('LOWER(jenisantrian_nama)',strtolower($this->jenisantrian_nama),true);
		$criteria->compare('is_dilayani',$this->is_dilayani);
                $criteria->order = "tgl_pendaftaran ASC";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('antrianpasienfarmasi_id',$this->antrianpasienfarmasi_id);
		$criteria->compare('LOWER(nourutantrian)',strtolower($this->nourutantrian),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('LOWER(jenisantrian_nama)',strtolower($this->jenisantrian_nama),true);
		$criteria->compare('is_dilayani',$this->is_dilayani);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}