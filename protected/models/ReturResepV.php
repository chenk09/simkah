<?php

/**
 * This is the model class for table "retur_resep_v".
 *
 * The followings are the available columns in table 'retur_resep_v':
 * @property string $noreturresep
 * @property string $jenispenjualan
 * @property string $obatalkes_nama
 * @property double $qty_retur
 * @property double $hargasatuan
 * @property double $totalretur
 * @property string $alasanretur
 * @property string $tglretur
 */
class ReturResepV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ReturResepV the static model class
	 */
	public $tglAwal, $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'retur_resep_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('qty_retur, hargasatuan, totalretur', 'numerical'),
			array('noreturresep', 'length', 'max'=>50),
			array('jenispenjualan', 'length', 'max'=>100),
			array('obatalkes_nama, alasanretur', 'length', 'max'=>200),
			array('tglretur', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('noreturresep, jenispenjualan, obatalkes_nama, qty_retur, hargasatuan, totalretur, alasanretur, tglretur', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'noreturresep' => 'Noreturresep',
			'jenispenjualan' => 'Jenispenjualan',
			'obatalkes_nama' => 'Obatalkes Nama',
			'qty_retur' => 'Qty Retur',
			'hargasatuan' => 'Hargasatuan',
			'totalretur' => 'Totalretur',
			'alasanretur' => 'Alasanretur',
			'tglretur' => 'Tglretur',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('noreturresep',$this->noreturresep,true);
		$criteria->compare('jenispenjualan',$this->jenispenjualan,true);
		$criteria->compare('obatalkes_nama',$this->obatalkes_nama,true);
		$criteria->compare('qty_retur',$this->qty_retur);
		$criteria->compare('hargasatuan',$this->hargasatuan);
		$criteria->compare('totalretur',$this->totalretur);
		$criteria->compare('alasanretur',$this->alasanretur,true);
		$criteria->compare('tglretur',$this->tglretur,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchPrint()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('noreturresep',$this->noreturresep,true);
		$criteria->compare('jenispenjualan',$this->jenispenjualan,true);
		$criteria->compare('obatalkes_nama',$this->obatalkes_nama,true);
		$criteria->compare('qty_retur',$this->qty_retur);
		$criteria->compare('hargasatuan',$this->hargasatuan);
		$criteria->compare('totalretur',$this->totalretur);
		$criteria->compare('alasanretur',$this->alasanretur,true);
		$criteria->compare('tglretur',$this->tglretur,true);
		
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination'=>false,
		));
	}
}