<?php

/**
 * This is the model class for table "konfigfarmasi_k".
 *
 * The followings are the available columns in table 'konfigfarmasi_k':
 * @property integer $konfigfarmasi_id
 * @property string $tglberlaku
 * @property double $persenppn
 * @property double $persenpph
 * @property double $persehargajual
 * @property double $totalpersenhargajual
 * @property boolean $bayarlangsung
 * @property string $pesandistruk
 * @property string $pesandifaktur
 * @property string $formulajasadokter
 * @property string $formulajasaparamedis
 * @property string $hargaygdigunakan
 * @property double $pembulatanharga
 * @property double $ri_persjual
 * @property double $rd_persjual
 * @property double $rj_persjual
 * @property double $persdiskpasien
 * @property double $addppnpenjualan
 * @property boolean $konfigfarmasi_aktif
 */
class KonfigfarmasiK extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KonfigfarmasiK the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'konfigfarmasi_k';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tglberlaku, persenppn, persenpph, persehargajual, totalpersenhargajual', 'required'),
			array('persdiskpasien, persenppn, persenpph, persehargajual, totalpersenhargajual, pembulatanharga, ri_persjual, persjualbebas, administrasi, rd_persjual, rj_persjual,addppnpenjualan', 'numerical'),
			array('formulajasadokter, formulajasaparamedis', 'length', 'max'=>100),
			array('hargaygdigunakan', 'length', 'max'=>50),
			array('persdiskpasien, bayarlangsung, pesandistruk, pesandifaktur, persjualbebas, admracikan, administrasi, konfigfarmasi_aktif, marginresep, marginnonresep, hargajualglobal,addppnpenjualan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('persdiskpasien, konfigfarmasi_id, tglberlaku, persenppn, admracikan, persenpph, persehargajual, marginresep, marginnonresep, hargajualglobal, totalpersenhargajual, bayarlangsung, pesandistruk, pesandifaktur, formulajasadokter, formulajasaparamedis, hargaygdigunakan, pembulatanharga, ri_persjual, rd_persjual, rj_persjual, administrasi, persjualbebas, addppnpenjualan, konfigfarmasi_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'konfigfarmasi_id' => 'Konfigfarmasi',
			'tglberlaku' => 'Tgl berlaku',
			'persenppn' => 'Persen PPN',
			'persenpph' => 'Persen PPH',
			'persehargajual' => 'Persen Harga Jual',
			'totalpersenhargajual' => 'Total Persen Harga Jual',
			'bayarlangsung' => 'Bayar Langsung',
			'pesandistruk' => 'Pesan Di Struk',
			'pesandifaktur' => 'Pesan Di Faktur',
			'formulajasadokter' => 'Formula Jasa Dokter',
			'formulajasaparamedis' => 'Formula Jasa Para Medis',
			'hargaygdigunakan' => 'Harga yang digunakan',
			'pembulatanharga' => 'Pembulatan Harga',
			'ri_persjual' => 'RI Persen Jual',
			'rd_persjual' => 'RD Persen Jual',
			'rj_persjual' => 'RJ Persen Jual',
			'konfigfarmasi_aktif' => 'Konfigfarmasi Aktif',
                        'persjualbebas'=>'Persen Harga Jual Bebas',
                        'admracikan'=>'Adm. Racikan',
                        'marginresep'=>'Margin Resep',
                        'marginnonresep'=>'Margin Non Resep',
                        'hargajualglobal'=>'Harga Jual Global',
                        'addppnpenjualan'=>'Ppn Penjualan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('konfigfarmasi_id',$this->konfigfarmasi_id);
		$criteria->compare('tglberlaku',$this->tglberlaku);
		$criteria->compare('persenppn',$this->persenppn);
		$criteria->compare('persenpph',$this->persenpph);
		$criteria->compare('persehargajual',$this->persehargajual);
		$criteria->compare('totalpersenhargajual',$this->totalpersenhargajual);
		$criteria->compare('bayarlangsung',$this->bayarlangsung);
		$criteria->compare('LOWER(pesandistruk)',strtolower($this->pesandistruk),true);
		$criteria->compare('LOWER(pesandifaktur)',strtolower($this->pesandifaktur),true);
		$criteria->compare('LOWER(formulajasadokter)',strtolower($this->formulajasadokter),true);
		$criteria->compare('LOWER(formulajasaparamedis)',strtolower($this->formulajasaparamedis),true);
		$criteria->compare('LOWER(hargaygdigunakan)',strtolower($this->hargaygdigunakan),true);
		$criteria->compare('pembulatanharga',$this->pembulatanharga);
		$criteria->compare('ri_persjual',$this->ri_persjual);
		$criteria->compare('rd_persjual',$this->rd_persjual);
		$criteria->compare('rj_persjual',$this->rj_persjual);
                $criteria->compare('admracikan',$this->admracikan);
                $criteria->compare('marginresep',$this->marginresep);
                $criteria->compare('marginnonresep',$this->marginnonresep);
                $criteria->compare('hargajualglobal',$this->hargajualglobal);
		$criteria->compare('konfigfarmasi_aktif',$this->konfigfarmasi_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('konfigfarmasi_id',$this->konfigfarmasi_id);
		$criteria->compare('tglberlaku',$this->tglberlaku);
		$criteria->compare('persenppn',$this->persenppn);
		$criteria->compare('persenpph',$this->persenpph);
		$criteria->compare('persehargajual',$this->persehargajual);
		$criteria->compare('totalpersenhargajual',$this->totalpersenhargajual);
		// $criteria->compare('bayarlangsung',$this->bayarlangsung);
		$criteria->compare('LOWER(pesandistruk)',strtolower($this->pesandistruk),true);
		$criteria->compare('LOWER(pesandifaktur)',strtolower($this->pesandifaktur),true);
		$criteria->compare('LOWER(formulajasadokter)',strtolower($this->formulajasadokter),true);
		$criteria->compare('LOWER(formulajasaparamedis)',strtolower($this->formulajasaparamedis),true);
		$criteria->compare('LOWER(hargaygdigunakan)',strtolower($this->hargaygdigunakan),true);
		$criteria->compare('pembulatanharga',$this->pembulatanharga);
		$criteria->compare('ri_persjual',$this->ri_persjual);
		$criteria->compare('rd_persjual',$this->rd_persjual);
		$criteria->compare('rj_persjual',$this->rj_persjual);
                $criteria->compare('admracikan',$this->admracikan);
                $criteria->compare('marginresep',$this->marginresep);
                $criteria->compare('marginnonresep',$this->marginnonresep);
                $criteria->compare('hargajualglobal',$this->hargajualglobal);
		//$criteria->compare('konfigfarmasi_aktif',$this->konfigfarmasi_aktif);
                $criteria->limit = -1;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
        }
        
        public static function statusKonfigFarmasi(){
            return (boolean)self::model()->find()->konfigfarmasi_aktif;
        }
        
        public function afterFind() {
            parent::afterFind();
            if (!$this->konfigfarmasi_aktif){
                $this->persehargajual = 0;
                $this->pembulatanharga = 0;
                $this->totalpersenhargajual = 0;
                $this->persjualbebas = 0;
                $this->administrasi = 0;
                $this->admracikan = 0;
                $this->formulajasadokter = 0;
                $this->persenpph = 0;
                $this->persenppn = 0;
                $this->rj_persjual = 0;
                $this->rd_persjual = 0;
                $this->ri_persjual = 0;
                $this->marginresep = 0;
                $this->marginnonresep = 0;
            }
        }
}