<?php

/**
 * This is the model class for table "laporanpajakpegawai_v".
 *
 * The followings are the available columns in table 'laporanpajakpegawai_v':
 * @property integer $penggajianpeg_id
 * @property string $periodegaji
 * @property integer $pegawai_id
 * @property string $gelardepan
 * @property string $nama_pegawai
 * @property string $nama_keluarga
 * @property string $tglpenggajian
 * @property string $nopenggajian
 * @property string $statusperkawinan
 * @property string $jeniskelamin
 * @property string $keterangan
 * @property string $mengetahui
 * @property string $menyetujui
 * @property integer $pengeluaranumum_id
 * @property string $nomorindukpegawai
 * @property string $notelp_pegawai
 * @property string $nomobile_pegawai
 * @property string $photopegawai
 * @property string $alamatemail
 * @property string $tgl_lahirpegawai
 * @property string $tempatlahir_pegawai
 * @property string $noidentitas
 * @property string $jenisidentitas
 * @property double $gajibersih
 * @property double $totalpajak
 */
class LaporanpajakpegawaiV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanpajakpegawaiV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanpajakpegawai_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penggajianpeg_id, pegawai_id, pengeluaranumum_id', 'numerical', 'integerOnly'=>true),
			array('gajibersih, totalpajak', 'numerical'),
			array('gelardepan', 'length', 'max'=>10),
			array('nama_pegawai, nama_keluarga, nopenggajian, notelp_pegawai, nomobile_pegawai', 'length', 'max'=>50),
			array('statusperkawinan, jeniskelamin, jenisidentitas', 'length', 'max'=>20),
			array('mengetahui, menyetujui, alamatemail, noidentitas', 'length', 'max'=>100),
			array('nomorindukpegawai, tempatlahir_pegawai', 'length', 'max'=>30),
			array('photopegawai', 'length', 'max'=>200),
			array('periodegaji, tglpenggajian, keterangan, tgl_lahirpegawai', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penggajianpeg_id, periodegaji, pegawai_id, gelardepan, nama_pegawai, nama_keluarga, tglpenggajian, nopenggajian, statusperkawinan, jeniskelamin, keterangan, mengetahui, menyetujui, pengeluaranumum_id, nomorindukpegawai, notelp_pegawai, nomobile_pegawai, photopegawai, alamatemail, tgl_lahirpegawai, tempatlahir_pegawai, noidentitas, jenisidentitas, gajibersih, totalpajak', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penggajianpeg_id' => 'Penggajianpeg',
			'periodegaji' => 'Periode Gaji',
			'pegawai_id' => 'Pegawai',
			'gelardepan' => 'Gelar depan',
			'nama_pegawai' => 'Nama Pegawai',
			'nama_keluarga' => 'Nama Keluarga',
			'tglpenggajian' => 'Tgl penggajian',
			'nopenggajian' => 'No penggajian',
			'statusperkawinan' => 'Status perkawinan',
			'jeniskelamin' => 'Jenis kelamin',
			'keterangan' => 'Keterangan',
			'mengetahui' => 'Mengetahui',
			'menyetujui' => 'Menyetujui',
			'pengeluaranumum_id' => 'Pengeluaranumum',
			'nomorindukpegawai' => 'Nomor Induk Kariawan',
			'notelp_pegawai' => 'Notelp Pegawai',
			'nomobile_pegawai' => 'Nomobile Pegawai',
			'photopegawai' => 'Photopegawai',
			'alamatemail' => 'Alamatemail',
			'tgl_lahirpegawai' => 'Tgl Lahir pegawai',
			'tempatlahir_pegawai' => 'Tempat lahir Pegawai',
			'noidentitas' => 'No identitas',
			'jenisidentitas' => 'Jenis identitas',
			'gajibersih' => 'Gaji bersih',
			'totalpajak' => 'Total pajak',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penggajianpeg_id',$this->penggajianpeg_id);
		$criteria->compare('periodegaji',$this->periodegaji,true);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('gelardepan',$this->gelardepan,true);
		$criteria->compare('nama_pegawai',$this->nama_pegawai,true);
		$criteria->compare('nama_keluarga',$this->nama_keluarga,true);
		$criteria->compare('tglpenggajian',$this->tglpenggajian,true);
		$criteria->compare('nopenggajian',$this->nopenggajian,true);
		$criteria->compare('statusperkawinan',$this->statusperkawinan,true);
		$criteria->compare('jeniskelamin',$this->jeniskelamin,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('mengetahui',$this->mengetahui,true);
		$criteria->compare('menyetujui',$this->menyetujui,true);
		$criteria->compare('pengeluaranumum_id',$this->pengeluaranumum_id);
		$criteria->compare('nomorindukpegawai',$this->nomorindukpegawai,true);
		$criteria->compare('notelp_pegawai',$this->notelp_pegawai,true);
		$criteria->compare('nomobile_pegawai',$this->nomobile_pegawai,true);
		$criteria->compare('photopegawai',$this->photopegawai,true);
		$criteria->compare('alamatemail',$this->alamatemail,true);
		$criteria->compare('tgl_lahirpegawai',$this->tgl_lahirpegawai,true);
		$criteria->compare('tempatlahir_pegawai',$this->tempatlahir_pegawai,true);
		$criteria->compare('noidentitas',$this->noidentitas,true);
		$criteria->compare('jenisidentitas',$this->jenisidentitas,true);
		$criteria->compare('gajibersih',$this->gajibersih);
		$criteria->compare('totalpajak',$this->totalpajak);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}