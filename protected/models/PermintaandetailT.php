<?php

/**
 * This is the model class for table "permintaandetail_t".
 *
 * The followings are the available columns in table 'permintaandetail_t':
 * @property integer $permintaandetail_id
 * @property integer $obatalkes_id
 * @property integer $satuankecil_id
 * @property integer $sumberdana_id
 * @property integer $permintaanpembelian_id
 * @property integer $satuanbesar_id
 * @property double $stokakhir
 * @property integer $maksimalstok
 * @property integer $minimalstok
 * @property double $jmlpermintaan
 * @property double $persendiscount
 * @property double $jmldiscount
 * @property double $harganettoper
 * @property double $hargappnper
 * @property double $hargapphper
 * @property double $hargasatuanper
 * @property double $biaya_lainlain
 * @property string $tglkadaluarsa
 * @property integer $jmlkemasan
 * @property integer $hargabelibesar
 */
class PermintaandetailT extends CActiveRecord
{
    public $jmlPenawaran = 0; //jumlah penawaran sebelum pembelian
    public $totalHargaBersih; //jumlah penawaran sebelum pembelian
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PermintaandetailT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'permintaandetail_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('obatalkes_id, permintaanpembelian_id, jmlpermintaan, persendiscount, jmldiscount, harganettoper, hargappnper, hargapphper, hargasatuanper', 'required'),
			array('obatalkes_id, satuankecil_id, sumberdana_id, permintaanpembelian_id, satuanbesar_id, maksimalstok, minimalstok, jmlkemasan', 'numerical', 'integerOnly'=>true),
			array('hargabelibesar,stokakhir, jmlpermintaan, persendiscount, jmldiscount, harganettoper, hargappnper, hargapphper, hargasatuanper', 'numerical'),
			array('tglkadaluarsa, biaya_lainlain', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('hargabelibesar,permintaandetail_id, obatalkes_id, satuankecil_id, sumberdana_id, permintaanpembelian_id, satuanbesar_id, stokakhir, maksimalstok, minimalstok, jmlpermintaan, persendiscount, jmldiscount, harganettoper, hargappnper, hargapphper, hargasatuanper, tglkadaluarsa, jmlkemasan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'satuanbesar'=>array(self::BELONGS_TO, 'SatuanbesarM','satuanbesar_id'),
                    'sumberdana'=>array(self::BELONGS_TO, 'SumberdanaM','sumberdana_id'),
                    'satuankecil'=>array(self::BELONGS_TO, 'SatuankecilM','satuankecil_id'),
                    'obatalkes'=>array(self::BELONGS_TO, 'ObatalkesM','obatalkes_id'),
                    'permintaanpembelian'=>array(self::BELONGS_TO,'PermintaanpembelianT','permintaanpembelian_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'permintaandetail_id' => 'Permintaandetail',
			'obatalkes_id' => 'Obatalkes',
			'satuankecil_id' => 'Satuankecil',
			'sumberdana_id' => 'Sumberdana',
			'permintaanpembelian_id' => 'Permintaanpembelian',
			'satuanbesar_id' => 'Satuanbesar',
			'stokakhir' => 'Stokakhir',
			'maksimalstok' => 'Maksimalstok',
			'minimalstok' => 'Minimalstok',
			'jmlpermintaan' => 'Jmlpermintaan',
			'persendiscount' => 'Persendiscount',
			'jmldiscount' => 'Jmldiscount',
			'harganettoper' => 'Harganettoper',
			'hargappnper' => 'Hargappnper',
			'hargapphper' => 'Hargapphper',
			'hargasatuanper' => 'Hargasatuanper',
			'tglkadaluarsa' => 'Tglkadaluarsa',
			'jmlkemasan' => 'Jmlkemasan',
                        'biaya_lainlain' => 'Other Cost'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('permintaandetail_id',$this->permintaandetail_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('stokakhir',$this->stokakhir);
		$criteria->compare('maksimalstok',$this->maksimalstok);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('jmlpermintaan',$this->jmlpermintaan);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('harganettoper',$this->harganettoper);
		$criteria->compare('hargappnper',$this->hargappnper);
		$criteria->compare('hargapphper',$this->hargapphper);
		$criteria->compare('hargasatuanper',$this->hargasatuanper);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('permintaandetail_id',$this->permintaandetail_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('stokakhir',$this->stokakhir);
		$criteria->compare('maksimalstok',$this->maksimalstok);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('jmlpermintaan',$this->jmlpermintaan);
		$criteria->compare('persendiscount',$this->persendiscount);
		$criteria->compare('jmldiscount',$this->jmldiscount);
		$criteria->compare('harganettoper',$this->harganettoper);
		$criteria->compare('hargappnper',$this->hargappnper);
		$criteria->compare('hargapphper',$this->hargapphper);
		$criteria->compare('hargasatuanper',$this->hargasatuanper);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('jmlkemasan',$this->jmlkemasan);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}