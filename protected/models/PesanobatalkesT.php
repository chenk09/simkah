<?php
 
/**
 * This is the model class for table "pesanobatalkes_t".
 *
 * The followings are the available columns in table 'pesanobatalkes_t':
 * @property integer $pesanobatalkes_id
 * @property integer $mutasioaruangan_id
 * @property integer $ruangan_id
 * @property string $tglpemesanan
 * @property string $nopemesanan
 * @property string $statuspesan
 * @property string $tglmintadikirim
 * @property string $keterangan_pesan
 * @property integer $ruanganpemesan_id
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class PesanobatalkesT extends CActiveRecord
{   
        public $tglAwal;
        public $tglAkhir;
        public $instalasi_id, $instalasipemesan_id;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PesanobatalkesT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pesanobatalkes_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ruangan_id, tglpemesanan, nopemesanan, statuspesan', 'required'),
			array('mutasioaruangan_id, ruangan_id, ruanganpemesan_id', 'numerical', 'integerOnly'=>true),
			array('nopemesanan', 'length', 'max'=>50),
			array('statuspesan', 'length', 'max'=>30),
			array('instalasi_id, tglmintadikirim, keterangan_pesan, update_time, update_loginpemakai_id', 'safe'),
                    
                        array('create_time,update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s'),'setOnEmpty'=>false,'on'=>'update'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
			array('create_ruangan','default','value'=>Yii::app()->user->getState('ruangan_id'),'on'=>'insert'),
                    
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pesanobatalkes_id, mutasioaruangan_id, ruangan_id, tglpemesanan, nopemesanan, statuspesan, tglmintadikirim, keterangan_pesan, ruanganpemesan_id, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                     'ruangantujuan'=>array(self::BELONGS_TO,'RuanganM','ruangan_id'),
                     'ruanganpemesan'=>array(self::BELONGS_TO,'RuanganM','ruanganpemesan_id'),
                     'instalasi'=>array(self::BELONGS_TO,'InstalasiM','instalasi_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pesanobatalkes_id' => 'ID',
			'mutasioaruangan_id' => 'Ruangan Mutasi',
			'ruangan_id' => 'Pesan Ke',
			'tglpemesanan' => 'Tanggal Pemesanan',
                        'tglAwal'=>'Tanggal Awal',
                        'tglAkhir'=>'Tanggal Akhir',
			'nopemesanan' => 'No Pemesanan',
			'statuspesan' => 'Status Pesan',
			'tglmintadikirim' => 'Tanggal Minta Dikirim',
			'keterangan_pesan' => 'Keterangan Pesan',
			'ruanganpemesan_id' => 'Ruangan Pemesan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pesanobatalkes_id',$this->pesanobatalkes_id);
		$criteria->compare('mutasioaruangan_id',$this->mutasioaruangan_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('date(tglpemesanan)',$this->tglpemesanan,true);
		$criteria->compare('LOWER(nopemesanan)',strtolower($this->nopemesanan),true);
		$criteria->compare('LOWER(statuspesan)',strtolower($this->statuspesan),true);
                if (trim($this->tglmintadikirim) != ""){
                    $criteria->compare('date(tglmintadikirim)',$this->tglmintadikirim);
                }
		$criteria->compare('LOWER(keterangan_pesan)',strtolower($this->keterangan_pesan),true);
		$criteria->compare('ruanganpemesan_id',$this->ruanganpemesan_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->addCondition('mutasioaruangan_id is null and ruangan_id ='.Yii::app()->user->getState('ruangan_id'));
        $criteria->order='tglpemesanan DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pesanobatalkes_id',$this->pesanobatalkes_id);
		$criteria->compare('mutasioaruangan_id',$this->mutasioaruangan_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(tglpemesanan)',strtolower($this->tglpemesanan),true);
		$criteria->compare('LOWER(nopemesanan)',strtolower($this->nopemesanan),true);
		$criteria->compare('LOWER(statuspesan)',strtolower($this->statuspesan),true);
		$criteria->compare('LOWER(tglmintadikirim)',strtolower($this->tglmintadikirim),true);
		$criteria->compare('LOWER(keterangan_pesan)',strtolower($this->keterangan_pesan),true);
		$criteria->compare('ruanganpemesan_id',$this->ruanganpemesan_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
        $criteria->order='tglpemesanan DESC';
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                        }
            }
            return true;
        }
        
        protected function beforeValidate ()
        {
            // convert to storage format
            //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
            $format = new CustomFormat();
            //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                    if ($column->dbType == 'date')
                        {
                            $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
                        }
                    else if ( $column->dbType == 'timestamp without time zone')
                        {
                            $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
                        }
            }

            return parent::beforeValidate ();
        }
}