<?php

/**
 * This is the model class for table "loketantrian_m".
 *
 * The followings are the available columns in table 'loketantrian_m':
 * @property integer $loketantrian_id
 * @property integer $lokasiantrian_id
 * @property string $loketantrian_nama
 * @property boolean $loketantrian_aktif
 * @property string $formatnomor
 *
 * The followings are the available model relations:
 * @property AntrianpasienT[] $antrianpasienTs
 * @property LokasiantrianM $lokasiantrian
 */
class LoketantrianM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LoketantrianM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'loketantrian_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('loketantrian_nama', 'required'),
			array('lokasiantrian_id', 'numerical', 'integerOnly'=>true),
			array('loketantrian_nama', 'length', 'max'=>25),
			array('formatnomor', 'length', 'max'=>5),
			array('loketantrian_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('loketantrian_id, lokasiantrian_id, loketantrian_nama, loketantrian_aktif, formatnomor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'antrianpasienTs' => array(self::HAS_MANY, 'AntrianpasienT', 'loketantrian_id'),
			'lokasiantrian' => array(self::BELONGS_TO, 'LokasiantrianM', 'lokasiantrian_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'loketantrian_id' => 'Loketantrian',
			'lokasiantrian_id' => 'Lokasiantrian',
			'loketantrian_nama' => 'Loketantrian Nama',
			'loketantrian_aktif' => 'Loketantrian Aktif',
			'formatnomor' => 'Formatnomor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('loketantrian_id',$this->loketantrian_id);
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('LOWER(loketantrian_nama)',strtolower($this->loketantrian_nama),true);
		$criteria->compare('loketantrian_aktif',$this->loketantrian_aktif);
		$criteria->compare('LOWER(formatnomor)',strtolower($this->formatnomor),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('loketantrian_id',$this->loketantrian_id);
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('LOWER(loketantrian_nama)',strtolower($this->loketantrian_nama),true);
		$criteria->compare('loketantrian_aktif',$this->loketantrian_aktif);
		$criteria->compare('LOWER(formatnomor)',strtolower($this->formatnomor),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}