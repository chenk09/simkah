<?php

/**
 * This is the model class for table "penjamin_bpjs_m".
 *
 * The followings are the available columns in table 'penjamin_bpjs_m':
 * @property integer $penjamin_bpjs_id
 * @property string $penjamin_bpjs_kode
 * @property string $penjamin_bpjs_nama
 * @property boolean $penjamin_bpjs_aktif
 */
class PenjaminBpjsM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PenjaminBpjsM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penjamin_bpjs_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('penjamin_bpjs_nama', 'required'),
			array('penjamin_bpjs_kode', 'length', 'max'=>3),
			array('penjamin_bpjs_nama', 'length', 'max'=>100),
			array('penjamin_bpjs_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('penjamin_bpjs_id, penjamin_bpjs_kode, penjamin_bpjs_nama, penjamin_bpjs_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'penjamin_bpjs_id' => 'Penjamin Bpjs',
			'penjamin_bpjs_kode' => 'Penjamin Bpjs Kode',
			'penjamin_bpjs_nama' => 'Penjamin Bpjs Nama',
			'penjamin_bpjs_aktif' => 'Penjamin Bpjs Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penjamin_bpjs_id',$this->penjamin_bpjs_id);
		$criteria->compare('LOWER(penjamin_bpjs_kode)',strtolower($this->penjamin_bpjs_kode),true);
		$criteria->compare('LOWER(penjamin_bpjs_nama)',strtolower($this->penjamin_bpjs_nama),true);
		$criteria->compare('penjamin_bpjs_aktif',$this->penjamin_bpjs_aktif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('penjamin_bpjs_id',$this->penjamin_bpjs_id);
		$criteria->compare('LOWER(penjamin_bpjs_kode)',strtolower($this->penjamin_bpjs_kode),true);
		$criteria->compare('LOWER(penjamin_bpjs_nama)',strtolower($this->penjamin_bpjs_nama),true);
		$criteria->compare('penjamin_bpjs_aktif',$this->penjamin_bpjs_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}