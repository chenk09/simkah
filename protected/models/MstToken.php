<?php

/**
 * This is the model class for table "inbox.mst_token".
 *
 * The followings are the available columns in table 'inbox.mst_token':
 * @property integer $id_token
 * @property string $aplikasi
 * @property string $module
 * @property string $token
 * @property string $secret_key
 * @property string $app_id
 */
class MstToken extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MstToken the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inbox.mst_token';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_token', 'required'),
			array('id_token', 'numerical', 'integerOnly'=>true),
			array('aplikasi, module', 'length', 'max'=>32),
			array('token, secret_key, app_id', 'length', 'max'=>64),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_token, aplikasi, module, token, secret_key, app_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_token' => 'Id Token',
			'aplikasi' => 'Aplikasi',
			'module' => 'Module',
			'token' => 'Token',
			'secret_key' => 'Secret Key',
			'app_id' => 'App',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_token',$this->id_token);
		$criteria->compare('aplikasi',$this->aplikasi,true);
		$criteria->compare('module',$this->module,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('secret_key',$this->secret_key,true);
		$criteria->compare('app_id',$this->app_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}