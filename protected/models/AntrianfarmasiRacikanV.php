<?php

/**
 * This is the model class for table "antrianfarmasi_racikan_v".
 *
 * The followings are the available columns in table 'antrianfarmasi_racikan_v':
 * @property integer $antrianpasienfarmasi_id
 * @property string $tglantrianpasien
 * @property integer $lokasiantrian_id
 * @property string $lokasiantrian_nama
 * @property integer $jenisantrian_id
 * @property boolean $is_dilayani
 * @property string $status
 */
class AntrianfarmasiRacikanV extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AntrianfarmasiRacikanV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'antrianfarmasi_racikan_v';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('antrianpasienfarmasi_id, lokasiantrian_id, jenisantrian_id', 'numerical', 'integerOnly'=>true),
			array('lokasiantrian_nama', 'length', 'max'=>100),
			array('tglantrianpasien, is_dilayani, status', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('antrianpasienfarmasi_id, tglantrianpasien, lokasiantrian_id, lokasiantrian_nama, jenisantrian_id, is_dilayani, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'antrianpasienfarmasi_id' => 'Antrianpasienfarmasi',
			'tglantrianpasien' => 'Tglantrianpasien',
			'lokasiantrian_id' => 'Lokasiantrian',
			'lokasiantrian_nama' => 'Lokasiantrian Nama',
			'jenisantrian_id' => 'Jenisantrian',
			'is_dilayani' => 'Is Dilayani',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('antrianpasienfarmasi_id',$this->antrianpasienfarmasi_id);
//		$criteria->compare('LOWER(tglantrianpasien)',strtolower($this->tglantrianpasien),true);
                $criteria->addBetweenCondition('DATE(tglantrianpasien)', date('Y-m-d'), date('Y-m-d'));
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('LOWER(lokasiantrian_nama)',strtolower($this->lokasiantrian_nama),true);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('is_dilayani',$this->is_dilayani);
		$criteria->compare('LOWER(status)',strtolower($this->status),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('antrianpasienfarmasi_id',$this->antrianpasienfarmasi_id);
		$criteria->compare('LOWER(tglantrianpasien)',strtolower($this->tglantrianpasien),true);
		$criteria->compare('lokasiantrian_id',$this->lokasiantrian_id);
		$criteria->compare('LOWER(lokasiantrian_nama)',strtolower($this->lokasiantrian_nama),true);
		$criteria->compare('jenisantrian_id',$this->jenisantrian_id);
		$criteria->compare('is_dilayani',$this->is_dilayani);
		$criteria->compare('LOWER(status)',strtolower($this->status),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function getNomorUrut(){
            $model = AntrianpasienfarmasiT::model()->findByPk($this->antrianpasienfarmasi_id);
            return $model->nourutantrian;
        }
}