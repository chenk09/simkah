/*
 * fungsi untuk menangani tombol Enter
 * agar focus input berpindah ke input berikutnya
 * @obj merupakan objek element (this)
 * @evt merupakan event
 * @next_id id element yg dituju
 * @before_id id element sebelumnya
 */
function nextFocus(obj,evt,next_id,before_id)
{    
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
    
    if(charCode == 13 && obj.type != "textarea" ) {
        $('#'+next_id).focus();
        $('#'+next_id).select();
        return false;
    } else if (charCode == 93) { //klik kanan kibor
        if (obj.type == "text" || obj.type == "button" || obj.type == "select-one" || obj.type == "password" || obj.type == "textarea" || obj.type == "checkbox" || obj.type == "radio")
        {
        $('#'+before_id).focus();
        $('#'+before_id).select();
        return false;
        }
    } else if (charCode == 40 || charCode == 34) { //arrow down || pg down
        if (obj.type == "text" || obj.type == "button" || obj.type == "password" || obj.type == "textarea" || obj.type == "checkbox")
        {
        $('#'+next_id).focus();
        $('#'+next_id).select();
        return false;
        }
    } else if (charCode == 38 || charCode == 33) { //arrow up || pg up
        if (obj.type == "text" || obj.type == "button" || obj.type == "password" || obj.type == "textarea" || obj.type == "checkbox")
        {
        $('#'+before_id).focus();
        $('#'+before_id).select();
        return false;
        }
    } else {
        return true;
    }
}

$.fn.focusNextInputField = function(evt) {
    
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
     if(charCode == 13){
        return this.each(function() {
            var fields = $(this).parents('form:eq(0),body').find('button,input,textarea,select,link,checkbox').not('[type=hidden],[readonly]').not(':hidden').not(':disabled');
                var index = fields.index( this );
                if ( index > -1 && ( index + 1 ) < fields.length ) {
                    fields.eq( index + 1 ).focus();
                    fields.eq( index + 1 ).select();
                }
                return false;
        });
            return false;
        }
//        charCode == 40 ||
        else if ( charCode == 34) { //arrow down || pg down
            return this.each(function() {
            var fields = $(this).parents('form:eq(0),body').find('button,input,textarea,link,checkbox').not('[type=hidden]');
            var index = fields.index( this );
            if ( index > -1 && ( index + 1 ) < fields.length ) {
                fields.eq( index + 1 ).focus();
                fields.eq( index + 1 ).select();
            }
            return false;
        });
            return false;
        }
//        charCode == 38 || 
        else if (charCode == 33) { //arrow down || pg down
            return this.each(function() {
            var fields = $(this).parents('form:eq(0),body').find('button,input,textarea,link,checkbox').not('[type=hidden]');
            var index = fields.index( this );
            if ( index > -1 && ( index + 1 ) < fields.length ) {
                fields.eq( index - 1 ).focus();
                fields.eq( index + 1 ).select();
            }
            return false;
        });
            return false;
        }
};
$.fn.nextFocus = function() {
    
        return this.each(function() {
            var fields = $(this).parents('form:eq(0),body').find('button,input,textarea,select,link').not('[type=hidden]');
            var index = fields.index( this );
            if ( index > -1 && ( index + 1 ) < fields.length ) {
                fields.eq( index + 1 ).focus();
            }
            return false;
        });

};
function disableKeyPress(e)
{
     var key;     
     if(window.event)
     {
        key = window.event.keyCode; //IE
     }  
     else{
         key = e.which; //firefox
     }
     
     if(key != 13)
     {
         return true;
     }else{
         if((e.target.type == 'textarea'))
         {
             return (e.shiftKey);
         }else{
             return false;
         }         
     }
//     return (key != 13);
}
function formSubmit(obj,evt)
{
     evt = (evt) ? evt : event;
     var form_id = $(obj).closest('form').attr('id');
     var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
     if(charCode == 13){
        document.getElementById(form_id).submit();
     }
}

function simpan(obj,evt)
{
    evt = (evt) ? evt : event;
    var form_id = $(obj).closest('form').attr('id');
    var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
    if(charCode != 116)
       { 
            var kosong = false; 
             $('.inputRequire').each(function(){
                    if(this.value == '')
                    {
                        kosong = true;
                    }
                }); 
            if (kosong == true)
               {
                   alert ('Harap Isi Semua Bagian Yang Bertanda *');
               }
            else
               {
                   document.getElementById(form_id).submit();
               }
       }       
}

function addslashes (str) {

    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

function checkRahasia(patokan,kelas)
{
    if($('#'+patokan+'').is(':checked')) {
        $('#'+patokan+'').removeAttr('checked');
        $('.'+kelas+'').each(function() {
            $(this).removeAttr('checked');
        });
    }
    else
    {
         $('#'+patokan+'').attr('checked', 'checked');
        $('.'+kelas+'').each(function() {
            $(this).attr('checked', 'checked');
        });
    }
}


function checkAll(kelas,obj)
{
    if(obj.checked) {
        $('.'+kelas+'').each(function() {
            $(this).attr('checked', 'checked');
        });
    }
    else
    {
        obj.checked = false;
        $('.'+kelas+'').each(function() {
            $(this).removeAttr('checked');
        });
    }
}
function checkIni(obj){
    if(obj.checked){
        $(obj).parent().addClass('checked');
    }else{
        $(obj).parent().removeClass('checked');
    }
}

//
//$( document ).ready(function() {
//    window.onbeforeunload = confirmExit;
//    function confirmExit(){
//    alert("confirm exit is being called");
//    return false;
//}
//});